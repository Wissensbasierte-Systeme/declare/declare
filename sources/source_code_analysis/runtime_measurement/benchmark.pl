

/******************************************************************/
/***                                                            ***/
/***         Statistics: Clone-Detection                        ***/
/***                                                            ***/
/******************************************************************/


:- module( runtime_measurement, []).


/*** interface ****************************************************/


/* benchmark(+Integers) <-
      */

benchmark(Integers) :-
   random_integers:load_random_integers(Integers, X),

   determine_runtime( sort(X, _), Sort_RT ),
   writeln('Laufzeit sort':Sort_RT),

   determine_runtime( msort(X, _), MSort_RT ),
   writeln('Laufzeit Msort':MSort_RT),

   determine_runtime( user:mergesort(X, _), M_RT ),
   writeln('Laufzeit mergesort':M_RT),

   determine_runtime( runtime_measurement:mergesort(X, _), MA_RT ),
   writeln('Laufzeit mergesort advanced':MA_RT),

   determine_runtime( quicksort(X, _), Q_RT ),
   writeln('Laufzeit quicksort':Q_RT),
   !.

mergesort_2([], []).
mergesort_2([X], [X]).
mergesort_2(L, SL) :-
  divide_2(L, L1, L2),
  mergesort_2(L1, SL1),
  mergesort_2(L2, SL2),
  merge_2_(SL1, SL2, SL),
  !.

divide_2([], [], []).
divide_2([X], [X], []).
divide_2([X, Y|L], [X|L1], [Y|L2]) :-
  divide_2(L, L1, L2).

merge_2_([], SL, SL).
merge_2_(SL, [], SL).
merge_2_([X|SL1], [Y|SL2], [X|SL]) :-
  X < Y,
  merge_2_(SL1,
  [Y|SL2], SL).
merge_2_([X|SL1], [Y|SL2], [Y|SL]) :-
  not(X < Y),
  merge_2_([X|SL1], SL2, SL).


/* mergesort(List, Sorted_List) <-
      */

mergesort(Xs, Xs) :-
   length(Xs, N),
   N =< 1,
   !.
mergesort(Xs, Ys) :-
   middle_split_advanced(Xs, Xs1, Xs2),
   mergesort(Xs1, Ys1),
   mergesort(Xs2, Ys2),
   mergesort_merge(Ys1, Ys2, Ys).

mergesort_merge([], Xs, Xs) :-
   !.
mergesort_merge(Xs, [], Xs) :-
   !.
mergesort_merge([X1|Xs1], [X2|Xs2], [X|Xs]) :-
   ( X1 < X2,
     X = X1,
     mergesort_merge(Xs1, [X2|Xs2], Xs)
   ; X = X2,
     mergesort_merge([X1|Xs1], Xs2, Xs) ).


/* middle_split_advanced(+Xs, -Xs1, -Xs2) <-
      */

middle_split_advanced(Xs, Xs1, Xs2) :-
   length(Xs, L),
   L1 is L//2,
   length(Xs1, L1),
   append(Xs1, Xs2, Xs),
   !.


/*** implementation ***********************************************/


/* determine_runtime(+Goal) <-
      */

determine_runtime(Goal) :-
   determine_runtime(Goal, Time),
   write(Time),
   writeln(' msec.').


/* determine_runtime(+Goal, -Time) <-
      */

determine_runtime(Goal, Time) :-
   statistics(runtime, [T1|_]),
   call(Goal),
   statistics(runtime, [T2|_]),
   Time is T2 - T1.


/******************************************************************/


% Find all solutions of an 11 by 11 board.
% The \+ with the fail is a trick to make it find all solutions.

queensBenchmark :-
   time( \+ (queens(11, _Qs), fail) ).
queens(N,Qs) :-
   rangeList(1,N,Ns),
   queens3(Ns,[],Qs).

queens3(UnplacedQs, SafeQs, Qs) :-
  selectq(Q, UnplacedQs, UnplacedQs1),
  \+ attack(Q,SafeQs),
  queens3(UnplacedQs1,[Q|SafeQs],Qs).
queens3([],Qs,Qs).

attack(X,Xs) :-
   attack3(X, 1, Xs).

attack3(X,N,[Y|_]) :-
   X =:= Y+N
 ; X =:= Y-N.
attack3(X,N,[_|Ys]) :-
   N1 is N+1,
   attack3(X,N1,Ys).

rangeList(M,N,[M]) :-
   M >= N, !.
rangeList(M,N,[M|Tail]) :-
   M1 is M+1,
   rangeList(M1,N,Tail).

selectq(X,[X|Xs],Xs).
selectq(X,[Y|Ys],[Y|Zs]) :-
   selectq(X,Ys,Zs).


/******************************************************************/


