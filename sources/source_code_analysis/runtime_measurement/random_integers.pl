

/******************************************************************/
/***                                                            ***/
/***          SCA: Random Integers                              ***/
/***                                                            ***/
/******************************************************************/


:- module( random_integers, [
      save_random_integers/1,
      save_random_integers/2,
      load_random_integers/2,
      random_integers/2,
      delete_nth0/4 ]).


/*** interface ****************************************************/


/* save_random_integers(+Max) <-
      */

save_random_integers(Number) :-
   random_integers(Number, Random_Ints),
   sca_variable_get(sca, SCA),
   concat_atom([SCA, 'runtime_measurement/r_ints_', Number, '.txt'], File),
   save_random_integers(Random_Ints, File).


/* save_random_integers(+Random_Ints, +File) <-
      */

save_random_integers(Rs, File) :-
   tell(File),
   checklist( writeln,
      Rs ),
   told.


/* load_random_integers(+File|Number, -Random_Integers) <-
      */

load_random_integers(File, Random_Integers) :-
   exists_file(File),
   read_content_from_file(File, Random_Integers),
   write_list(['Einlesen der Datei ', File, ' beendet.\n']),
   !.
load_random_integers(Number, Random_Integers) :-
   integer(Number),
   sca_variable_get(sca, SCA),
   concat_atom([SCA, 'runtime_measurement/r_ints_', Number, '.txt'], File),
   load_random_integers(File, Random_Integers),
   !.


asserted_stream_content(end_of_file, _, _) :-
   !.
asserted_stream_content(A, Stream, []) :-
   !,
   writeln(A),
   read(Stream, T2),
   asserted_stream_content(T2, Stream, _).


/* random_integers(+Max, -Random_Ints) <-
      */

random_integers(Max, Random_Ints) :-
   statistics(runtime, [T1|_]),
   Max2 is Max - 1,
   numlist(0, Max2, Ordered_Ints),
   list_to_random_list(Max, Ordered_Ints, Random_Ints),
   statistics(runtime, [T2|_]),
   Time is T2 - T1,
   write_list(['Generated Random Integers: ', Max,
               '; Time: ', Time, ' ms.\n']).


/*** implementation ***********************************************/


/* list_to_random_list(+Numbers, +Ordered_Ints, -Random_Ints) <-
      */

list_to_random_list(0, [], []) :-
   !.
list_to_random_list(Max, Ints, [Element|R]) :-
   RN is random(Max),
   delete_nth0(RN, Ints, A2, Element),
   Max2 is Max - 1,
   list_to_random_list(Max2, A2, R).

delete_nth0(Nth, List_1, List_2, Element) :-
   length(L_1, Nth),
   append(L_1, [Element|L_2], List_1),
   append(L_1, L_2, List_2).


/* read_content_from_file(+File, -Is) <-
      */

read_content_from_file(File, Is) :-
   open(File, read, Stream),
   read_code_from_file(Stream, Is),
   close(Stream).

read_code_from_file(Stream, Is) :-
   read_line_to_codes(Stream, Codes),
   read_from_file(Codes, Stream, Is).

read_from_file(end_of_file, _, []) :-
   !.
read_from_file(Codes, Stream, [T|Is]) :-
   string_to_list(S, Codes),
   string_to_atom(S, A),
   term_to_atom(T, A),
   read_code_from_file(Stream, Is).


/******************************************************************/

