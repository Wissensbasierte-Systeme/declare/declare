import java.io.*;
import java.util.*;

public class benchmark {
  public static void main(String[] args)
  {
    int Anzahl = Integer.valueOf(args[0]);
    int[] feld;
    feld = new int[Anzahl];

    readIntegers(Anzahl, feld);
    long startTime = System.currentTimeMillis();
    mergesort(feld, 0, Anzahl - 1);
    System.err.println("Mergesort:" + (System.currentTimeMillis() - startTime) + " msec.");

    readIntegers(Anzahl, feld);
    startTime = System.currentTimeMillis();
    quicksort(feld, 0, Anzahl - 1);
    System.err.println("Quicksort:" + (System.currentTimeMillis() - startTime) + " msec.");
  }

  public static void quicksort (int[] a, int links, int rechts)
  {
    int l, r, pivot;
    if (links < rechts)
    {
      /* Divide */
      //Pivotelement v ist rechtestes Element
      pivot = a[rechts];
      l = links;
      r = rechts-1;
      do
      {
        while (a[l] < pivot && l < rechts) l++;
        while (a[r] > pivot && r > links) r--;
        if (l < r)
        vertausche(a, l, r);
      }
      while (l < r);
      //Pivotelement an die richtige Position
      vertausche(a, l, rechts);
      /* Conquer */
      quicksort(a, links, l-1);
      quicksort(a, l+1, rechts);
    }
  }

  public static void vertausche (int[] a, int i, int j)
  {
    int v = a[i];
    a[i] = a[j];
    a[j] = v;
  }


  public static void mergesort(int[] a, int l, int r)
  {
    int i, j, k, m;
    int[] b = new int[a.length];
    if (r > l)
    {
      /* Divide */
      m = (l + r) / 2;

      /* Conquer */
      mergesort(a, l, m);
      mergesort(a, m+1, r);
      /* Merge */
      // zuerst alle Werte ins Feld b kopieren:
      // b = a[l],...,a[m],a[r],...a[m+1]
      for (i = m; i >= l; i--)
        b[i] = a[i];
      for (j = m+1; j <= r; j++)
        b[r + m + 1 - j] = a[j];
      // dann der Groesse nach die Werte
      // zurueckschreiben
      i = l;
      j = r;
      for (k = l; k <= r; k++)
      {
        if (b[i] < b[j])
        {
          a[k] = b[i++];
        }
        else
        {
          a[k] = b[j--];
        }
      }
    }
  }

  public static void readIntegers(int Anzahl, int[] a)
  {
    try
    {
      String FileName = "r_ints_" + Anzahl + ".txt";
      Reader fr = new FileReader(FileName);
      LineNumberReader f =   new LineNumberReader( fr );
      int z = 0;
      for ( String line; (line = f.readLine()) != null; )
        a[z++] = Integer.valueOf(line);
      f.close();
      System.out.println("Einlesen der Datei " + FileName + " beendet.");
    }
    catch ( IOException e )
    {
      System.out.println( "Fehler beim Lesen der Datei" );
    }
  }

}


