

/******************************************************************/
/***                                                            ***/
/***         02.06.2008                                         ***/
/***                                                            ***/
/******************************************************************/


:- module( chart2, [
      ]).

:- use_module(library('plot/barchart')).


/*** interface ****************************************************/


:- use_module(library('plot/barchart')).

barchart :-
   multis(Multisets),
   maplist( barchart,
      Multisets ),
   !.

barchart(Unit:Multiset_1) :-
   member(Unit:Amount, Multiset_1),
   delete(Multiset_1, Unit:Amount, Multiset_2),
   list_to_ord_set([Unit:0|Multiset_2], Multiset_3),
   reverse(Multiset_3, Multiset_4),
   dislog_variable_get(dislog_unit_colours, Cs),
   length(Multiset_4, N),
   first_n_elements(N, Cs, Colours),
   maplist( triple_list,
      Multiset_4, Colours, Triple_List ),
   concat_atom([Unit, ': ', Amount], Title),
   barchart(Title, Triple_List, horizontal).


barchart(Title, Triple_List, HV) :-
   new(Picture, picture(Title, size(400, 320))),
   length(Triple_List, N),
   sca_bar_chart_required_scale(Triple_List, Scale),
   send(Picture, display, new(BC, bar_chart(HV, 0, Scale, 200, N))),
   forall(member((Name:Amount)-_Colour, Triple_List),
      ( result(Name, Colour, _),
        send(BC, append,
           bar_group(Name,
              bar(x, Amount, Colour) ) ) ) ),
   send(Picture, open).


sca_bar_chart_required_scale(Multiset, Scale) :-
   findall( Amount,
      member((_:Amount)-_, Multiset),
      Amounts ),
   maximum(Amounts, Scale).


triple_list(E, C, E-C).


dislog_variable_get(dislog_unit_colours, X) :-
    X = [ red, sienna3, orange, yellow, green, grey, lightskyblue, blue,
          lemon_chiffon,
          cornflower_blue,
          pale_green,
          black,
          orange,
          yellow,
          light_blue,
          navajo_white,
          lightslate_grey,
          yellow_green,
          light_pink,
          cyan ].

maximum([Max], Max) :-
   !.
maximum([A, B], Max) :-
   Max is max(A, B),
   !.
maximum([A, B|As], Max) :-
   M is max(A, B),
   maximum([M|As], Max).

first_n_elements(0, _, []) :-
   !.
first_n_elements(N, [A|As], [A|Bs]) :-
   N2 is N - 1,
   first_n_elements(N2, As, Bs),
   !.


multis(Multisets) :-
   package_names(unit, Paths),
   findall( T:CV,
      ( result(T, _Color, V),
        complete_result(V, Paths, CV) ),
      Multisets ).

complete_result(Multiset, Paths, Ys) :-
   findall( V:Z,
      ( member(Elt, Paths),
        member_in_list(Elt, Multiset, Y:Z),
        concat('sources/', V, Y) ),
      Ys ).

member_in_list(library, Multiset, 'sources/library':Value) :-
   member(library:Value, Multiset),
   !.
member_in_list(library, _, 'sources/library':0) :-
   !.
member_in_list(Elt, Multiset, Elt:Value) :-
   member(Elt:Value, Multiset),
   !.
member_in_list(Elt, _, Elt:0).

package_names(unit, Paths) :-
   Paths = [
      'library',
      'sources/basic_algebra',
      'sources/databases',
      'sources/development',
      'sources/reasoning',
      'sources/projects',
      'sources/source_code_analysis',
      'sources/stock_tool',
      'sources/xml'].

result(library, red, ['library':934, 'sources/basic_algebra':1]).
result(basic_algebra, sienna3, ['library':910, 'sources/basic_algebra':6398, 'sources/databases':152, 'sources/development':27, 'sources/reasoning':1147, 'sources/projects':108, 'sources/source_code_analysis':22, 'sources/stock_tool':4, 'sources/xml':110]).
result(databases, orange, [library:334,'sources/basic_algebra':582, 'sources/databases':24505, 'sources/development':4, 'sources/reasoning':1412, 'sources/projects':141, 'sources/source_code_analysis':4, 'sources/stock_tool':2, 'sources/xml':1458] ).
result(development, yellow, [library:98, 'sources/basic_algebra':257, 'sources/databases':15, 'sources/development':1829, 'sources/reasoning':104, 'sources/projects':14, 'sources/source_code_analysis':67, 'sources/stock_tool':3, 'sources/xml':899]).
result(nm_reasoning, green, [library:872, 'sources/basic_algebra':2032, 'sources/databases':316, 'sources/development':4, 'sources/reasoning':5949, 'sources/projects':219, 'sources/xml':90]).
result(projects, grey, [library:514, 'sources/basic_algebra':3283, 'sources/databases':298, 'sources/development':48, 'sources/reasoning':1651, 'sources/projects':8967, 'sources/source_code_analysis':2, 'sources/stock_tool':36, 'sources/xml':7675]).
result(source_code_analysis, blue, [library:228, 'sources/basic_algebra':309, 'sources/databases':22, 'sources/development':3, 'sources/projects':9, 'sources/source_code_analysis':5243, 'sources/stock_tool':2, 'sources/xml':5758]).
result(stock_tool, lightskyblue, [library:118, 'sources/basic_algebra':1424, 'sources/databases':160, 'sources/reasoning':400, 'sources/projects':45, 'sources/source_code_analysis':5, 'sources/stock_tool':6660, 'sources/xml':999]).
result(xml, yellow_green, [library:103, 'sources/basic_algebra':607, 'sources/databases':85, 'sources/development':15, 'sources/reasoning':511, 'sources/projects':86, 'sources/source_code_analysis':14, 'sources/stock_tool':2, 'sources/xml':15937]).

