

/******************************************************************/
/***                                                            ***/
/***         Diagrams: Charts                                   ***/
/***                                                            ***/
/******************************************************************/


:- module( chart, [
      sca_chart/3 ]).

:- use_module(library('plot/barchart')).


/*** interface ****************************************************/


/* sca_chart(+Title, +Multiset) <-
      */

sca_chart(Title, XY_Title, Multiset) :-
   new(Pic, picture('Chart', size(600, 600))),
   send(Pic, open),
   maplist( split_multiset,
      Multiset, Titles, Values ),
   chart(Pic, Title, XY_Title, Titles, [Values]),
   !.


/*** implementation ***********************************************/


/* chart(+Pic, +Title, +XY_Titles, +Titles, +Valuess) <-
      */

chart(Pic, Title, (String_X, String_Y), Titles, Valuess) :-
   send(Pic, label, Title),
   flatten(Valuess, Values),
   max(Values, Max),
   send(Pic, display,
      new(Line_Y, line(0, 0, 0, (Max + 20)*(-1)))),
   send(Line_Y, arrows, second),
   send(Pic, display,
      new(Line_X, line(0, 0, 570, 0))),
   send(Line_X, arrows, second),
   length(Values, N),
   new(Text_X, text(String_X)),
   send(Pic, display, Text_X, point(N*25, 10)),
   new(Text_Y, text(String_Y)),
   get(Text_Y, size, size(Text_Width, _)),
   send(Pic, display, Text_Y, point((-1)*Text_Width-10, Max*(-1))),
   length(Titles, T_L_1),
   T_L_2 is T_L_1 - 1,
   numlist(0, T_L_2, Titles_Numlist),
   length(Valuess, V_L_1),
   V_L_2 is V_L_1 - 1,
   numlist(0, V_L_2, Colors_Numlist),
   maplist( send_values(Pic, Titles_Numlist),
      Colors_Numlist, Valuess ).


/* send_values(+Pic, +Titles_Numlist, +Color_No, +Values) <-
      */

send_values(Pic, Titles_Numlist, Color_No, Values) :-
   color(Color_No, Color),
   maplist( send_values(Pic, Color),
      Titles_Numlist, Values ).


/* send_values(+Pic, +Color, +No, +Y) <-
      */

send_values(Pic, Color, No, Y) :-
   send(Pic, display,
     new(Box, box(25, Y*(-1))), point(10 + No*25, 0)),
   send(Box, fill_pattern, colour(Color)).


/* split_multiset(A:B, A, B) <-
      */

split_multiset(A:B, A, B).


/* color(+No, -Color) <-
      */

color(No, Color) :-
   memberchk((No, Color),
      [(0, red), (1, blue),
       (2, green), (3, black),
       (4, orange), (5, yellow)] ),
   !.
color(_, black).


/******************************************************************/

barchart :-
        barchart(vertical).

barchart(HV) :-
        new(W, picture),
%         active_classes(Classes),
%         length(Classes, N),
%         required_scale(Classes, Scale),
        Scale = 100,
        N = 10,
        Classes = [class(a, 20, 10)],
        send(W, display, new(BC, bar_chart(HV, 0, Scale, 200, N))),
        forall(member(class(Name, Created, Freed), Classes),
               send(BC, append,
                    bar_group(Name,
                              bar(created, Created, green),
                              bar(freed, Freed, red)))),
        send(W, open).

