

/******************************************************************/
/***                                                            ***/
/***         Diagrams: Polar-Diagram                            ***/
/***                                                            ***/
/******************************************************************/


:- module( polar_diagram, [
      polar_diagram/4,
      polar_diagram_to_picture/4 ]).


/*** interface ****************************************************/


/* polar_diagram(+Title, +Color, +Multiset) <-
      */

multiset_to_polar_diagram(Title, Color, Multiset) :-
   new(Pic, picture('Polar Diagram', size(600, 600))),
   send(Pic, open),
   maplist( split_multiset,
      Multiset, Radial_Titles, Values ),
   polar_diagram(Pic, Title, Radial_Titles, [Color], [Values]).


/* polar_diagram(+Title, +Radial_Titles, +Colors, +Valuess) <-
      */

polar_diagram(Title, Radial_Titles, Colors, Valuess) :-
   new(Pic, picture('Polar Diagram', size(600, 600))),
   send(Pic, open),
   polar_diagram(Pic, Title, Radial_Titles, Colors, Valuess).


/*** implementation ***********************************************/


/* polar_diagram_to_picture(+Pic, +Title, +Color, +Multiset) <-
      */

polar_diagram_to_picture(Pic, Title, Color, Multiset) :-
   maplist( split_multiset,
      Multiset, Radial_Titles, Values ),
   polar_diagram(Pic, Title, Radial_Titles, [Color], [Values]).


/* split_multiset(A:B, A, B) <-
      */

split_multiset(A:B, A, B).


/* polar_diagram(
         +Pic, +Title, +Radial_Titles, +Colors, +Valuess) <-
      */

polar_diagram(Pic, Title, Radial_Titles, Colors, Valuess) :-
   send(Pic, clear),
   send(Pic, label, Title),
   send(Pic, display,
      new(_, circle(100) ), point(150, 150) ),
   send(Pic, display,
      new(_, circle(200) ), point(100, 100) ),
   send(Pic, display,
      new(_, circle(300) ), point(50, 50) ),
   send(Pic, display,
      new(_, circle(400) ) ),
   polar_diagram_sub(Pic, Radial_Titles, Colors, Valuess).

polar_diagram_sub(Pic, Radial_Titles, Colors, Valuess) :-
   length(Radial_Titles, RT_L_1),
   RT_L_2 is RT_L_1 - 1,
   numlist(0, RT_L_2, Radial_Titles_Numlist),
   Slope is 2*pi/RT_L_1,
   maplist( send_lines(Pic, Slope),
      Radial_Titles, Radial_Titles_Numlist ),
   flatten(Valuess, Values),
   max(Values, Max),
   maplist( send_values(Pic, Slope, Max, Radial_Titles_Numlist),
      Colors, Valuess ),
   !.
polar_diagram_sub(_, _, _, _).


/* send_lines(+Pic, +Slope, +Radial_Title, +Radial_Title_No) <-
      */

send_lines(Pic, Slope, Radial_Title, Radial_Title_No) :-
   X is cos(Slope*Radial_Title_No)*210 + 200,
   Y is sin(Slope*Radial_Title_No)*210 + 200,
%  term_to_atom_sure(Radial_Title, Atom),
   new(Text, text(Radial_Title)),
   get(Text, size, size(Text_Width, Text_Height)),
   ( X - 200 < 0 ->
     XP is X - Text_Width
     ;
     XP = X ),
   ( Y - 200 < 0 ->
     YP is Y - Text_Height
     ;
     YP = Y ),
   !,
   send(Pic, display,
     Text, point(XP, YP)),
   send(Pic, display,
      new(_, line(200, 200, X, Y) ) ).


/* send_values(+Pic, +Slope, +Max,
      +Radial_Title_No, +Color, +Values) <-
      */

send_values(Pic, Slope, Max, Radial_Title_No, Color, Values) :-
%  max(Values, Max),
%  sumlist(Values, Sum),
%  Dilation is 200/Sum,
   ( Max = 0 ->
     Dilation = 200
     ;
     Dilation is 200/Max ),
   send(Pic, display, new(Path, path)),
   send(Path, pen, 2),
   send(Path, colour, Color),
   maplist( send_values_s(Path, Slope, Dilation),
      Values, Radial_Title_No ),
   Values = [Value|_],
   send(Path, append, point(Value*Dilation + 200, 200)).


/* send_values_s(
         +Path, +Slope, +Dilation, +Value, +Radial_Title_No) <-
      */

send_values_s(Path, Slope, Dilation, Value, Radial_Title_No) :-
   X is cos(Slope*Radial_Title_No)*Value*Dilation + 200,
   Y is sin(Slope*Radial_Title_No)*Value*Dilation + 200,
   send(Path, append, point(X, Y)).


/*** Tests ********************************************************/


test(polar_diagram, multiset) :-
   multiset_to_polar_diagram(
      'My own Title', red, [a:2,b:6,c:5,d:4,e:8,f:9,g:3]).

test(polar_diagram, polar_diagram) :-
   polar_diagram('My own Title',
      [x, y, z], [red, green, blue],
      [[2, 3, 9], [4, 9, 2], [8, 5, 7]]).


/******************************************************************/


