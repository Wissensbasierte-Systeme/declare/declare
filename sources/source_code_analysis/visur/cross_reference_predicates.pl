

/******************************************************************/
/***                                                            ***/
/***          Visur: cross references rule goal                 ***/
/***                                                            ***/
/******************************************************************/


:- dynamic leaf_to_path_tmp/3, calls_pp_tmp/4.


/*** interface ****************************************************/


/* cross_reference_rule_goal(_, _, +Level, +Name, -Gxl) <-
      */

cross_reference_rule_goal(_Config, _Tree, Level_1, Name_1, Gxl) :-
   ask_for_scaling(Level_1, Name_1, Level_S),
   visur:assert_facts_tmp(Level_S, Level_1),
   sca_variable_get(graph_m, Prmtrs),
   ( member(' in<->in', Prmtrs) ->
     findall( Name_S_1:MPA_1-Name_S_2:MPA_2,
        ( visur:calls_pp_tmp(MPA_1, File_1, MPA_2, File_2),
          visur:leaf_to_path_tmp(File_1, Name_1, _),
          visur:leaf_to_path_tmp(File_2, Name_1, _),
          visur:leaf_to_path_tmp(File_1, _, Name_S_1),
          visur:leaf_to_path_tmp(File_2, _, Name_S_2),
          not( Name_S_1 = Name_S_2 ) ),
        MPAs_A_1 )
   ; MPAs_A_1 = [] ),
   ( member(' out->in', Prmtrs) ->
     findall( Name_S_1:MPA_1-Name_S_2:MPA_2,
        ( visur:calls_pp_tmp(MPA_1, File_1, MPA_2, File_2),
          not( visur:leaf_to_path_tmp(File_1, Name_1, _) ),
          visur:leaf_to_path_tmp(File_2, Name_1, _),
          visur:leaf_to_path_tmp(File_1, _, Name_S_1),
          visur:leaf_to_path_tmp(File_2, _, Name_S_2),
          not( Name_S_1 = Name_S_2 ) ),
        MPAs_B_1 )
   ; MPAs_B_1 = [] ),
   ( member(' in->out', Prmtrs) ->
     findall( Name_S_1:MPA_1-Name_S_2:MPA_2,
        ( visur:calls_pp_tmp(MPA_1, File_1, MPA_2, File_2),
          visur:leaf_to_path_tmp(File_1, Name_1, _),
          not( visur:leaf_to_path_tmp(File_2, Name_1, _) ),
          visur:leaf_to_path_tmp(File_1, _, Name_S_1),
          visur:leaf_to_path_tmp(File_2, _, Name_S_2),
          not( Name_S_1 = Name_S_2 ) ),
        MPAs_C_1 )
   ; MPAs_C_1 = [] ),
   maplist( list_to_ord_set,
      [MPAs_A_1, MPAs_B_1, MPAs_C_1],
      [MPAs_A_2, MPAs_B_2, MPAs_C_2] ),
   length(MPAs_A_2, L_A),
   length(MPAs_B_2, L_B),
   length(MPAs_C_2, L_C),
   nl,
   writeln('calls in<->in':L_A),
   writeln('calls in->out':L_C),
   writeln('calls out->in':L_B),
   checklist( add_nodes_and_edges_to_gxl(orange, orange, orange),
      MPAs_A_2),
   checklist( add_nodes_and_edges_to_gxl(blue, blue, orange),
      MPAs_B_2),
   checklist( add_nodes_and_edges_to_gxl(green, orange, green),
      MPAs_C_2),
   gxl_db_to_gxl(Gxl).


/*** implementation ***********************************************/


/* add_nodes_and_edges_to_gxl(+Color_Edge, +Color_Node_In,
      +Color_Node_Out, +Name_S_1:MPA_1-Name_S_2:MPA_2) <-
      */

add_nodes_and_edges_to_gxl(Color_Edge, Color_Node_In,
      Color_Node_Out, Name_S_1:MPA_1-Name_S_2:MPA_2) :-
   rg_node(MPA_1, MPA_1, Color_Node_In, circle),
   file_base_name(Name_S_1, Label_S_1),
   rg_node(Name_S_1, Label_S_1, white, honeycomb),
   rg_edge(Name_S_1-MPA_1, Name_S_1, MPA_1, none, grey),
   rg_node(MPA_2, MPA_2, Color_Node_Out, circle),
   file_base_name(Name_S_2, Label_S_2),
   rg_node(Name_S_2, Label_S_2, white, honeycomb),
   rg_edge(Name_S_2-MPA_2, Name_S_2, MPA_2, none, grey),
   rg_edge(MPA_1-MPA_2, MPA_1, MPA_2, second, Color_Edge).


/* rg_node(+Id, +Label, +Color, +Symbol) <-
      */

rg_node(Id, Label, Color, Symbol) :-
   Node = node:[id:Id]:[
      prmtr:[mouse_click:aaa,
         color:Color, symbol:Symbol, size:medium]:[
         string:[bubble:'']:[Label] ] ],
   rar:update(gxl_node, Id, Node).


/* rg_edge(+Id, +From, +To, +Arrows, +Color) <-
      */

rg_edge(Id, From, To, Arrows, Color) :-
   Edge = edge:[id:Id,
         from:From, to:To]:[
      prmtr:[mouse_click:ccc, arrows:Arrows, color:Color]:[
            string:[bubble:'']:[],
            calls:[]:[]] ],
   rar:update(gxl_edge, From, To, Edge).


/* assert_facts_tmp(Level_S, Level_1) <-
      */

assert_facts_tmp(Level_S, Level_1) :-
   assert_leaf_to_path_tmp(Level_S, Level_1),
   assert_calls_pp_tmp.


/* assert_leaf_to_path_tmp(+Level_Scale, +Level_Current) <-
      */

assert_leaf_to_path_tmp(Level_S, Level_1) :-
   visur:leaf_to_path_tmp(levels, Level_S, Level_1),
   !.
assert_leaf_to_path_tmp(Level_S, Level_1) :-
   write('Please wait, generating temporary database ... '),
   retractall( visur:leaf_to_path_tmp(_, _, _) ),
   assert(
      leaf_to_path_tmp(levels, Level_S, Level_1) ),
   forall( rar:select(leaf_to_path, File, Path),
      visur:assert_leaf_to_path_tmp(Level_S, Level_1, File, Path) ),
   writeln(' ... done.').


assert_leaf_to_path_tmp(Level_S, Level_1, File, Path) :-
   rar:parse_element(Path, _, Level_S, Name_S),
   rar:parse_element(Path, _, Level_1, Name_1),
   assert( leaf_to_path_tmp(File, Name_1, Name_S) ),
   !.
assert_leaf_to_path_tmp(_, _, _, _).


/* assert_calls_pp_tmp <-
      */

assert_calls_pp_tmp :-
   visur:calls_pp_tmp(_, _, _, _),
   !.
assert_calls_pp_tmp :-
   write('Please wait, generating temporary database ... '),
   visur:assert_calls_pp_tmp_,
   writeln(' ... done.').

assert_calls_pp_tmp_ :-
   retractall( visur:calls_pp_tmp(_, _, _, _) ),
   forall( rar:select(rule, MPA_1, File_1, Rule),
      ( forall( rar:calls_pp(Rule, _, MPA_2),
           ( visur:select(rule, MPA_2, File_2, _),
             assert_with_filter(MPA_1, File_1, MPA_2, File_2 )
           )
        )
      )
    ).

assert_with_filter(_, _, (user:(:=))/2, _) :-
   !.
assert_with_filter((user:(:=))/2, _, _, _) :-
   !.
assert_with_filter(MPA_1, File_1, MPA_2, File_2) :-
    assert( visur:calls_pp_tmp(MPA_1, File_1, MPA_2, File_2) ).


/* select(Mode, ?MPA, ?File, ?Rule) <-
      */

select(Mode, MPA, File, Rule) :-
   rar:select(Mode, MPA, File, Rule).
select(Mode, MPA, nv, nv) :-
   not( rar:select(Mode, MPA, _, _) ).


/* ask_for_scaling(_Mode) <-
      */

ask_for_scaling(Level, Name, Mode) :-
   concat(['Scale in ', Level, ' ', Name], Label),
   new(D, dialog(Label)),
   send(D, append,
      new(Scale, menu('Scale', cycle))),
   send_list(Scale, append, ['sources', 'unit', 'module', 'file']),
   send(D, append,
      new(T, menu(style, toggle))),
   send_list(T, append,
      [' in<->in', ' in->out', ' out->in']),
   send(D, append,
       button('OK',
          and( message(@prolog, set_prmtrs, T?selection),
               message(D, return, Scale?selection) ) ) ),
   send(D, default_button, 'OK'),
   send(D, append,
      button(cancel, message(D, destroy)) ),
   get(D, confirm, Answer),
   get(Answer, value, Mode),
   send(D, destroy),
   Answer \== @nil.

set_prmtrs(P) :-
   chain_list(P, L),
   sca_variable_set(graph_m, L).


/******************************************************************/


