

/******************************************************************/
/***                                                            ***/
/***          Visur: Dependency Graphs                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* zoom_module_to_file(+Config, +M_A:N_A, +M_B:N_B, -Gxl) <-
      */

zoom_module_to_file(Config, M_A:N_A, M_B:N_B, Gxl) :-
   findall( File_1-File_2-Calls,
      ( rar:contains_ll(M_A:N_A, file:File_1),
        rar:who_calls_ff(File_1, File_2, Calls),
        rar:contains_ll(M_B:N_B, file:File_2) ),
      Edges ),
   forall(
      member(File-_-_, Edges),
      add_node_to_gxl_db(Config, file-file, File) ),
   forall(
      member(_-File-_, Edges),
      add_node_to_gxl_db(Config, file-file, File) ),
   forall(
      member(F_1-F_2-C, Edges),
      visur:add_edge_to_gxl_db([F_1, F_2], F_1,
         F_2-[call:[from:F_1, to:F_2]:C],
         file_file_edge) ),
   gxl_db_to_gxl(Gxl),
   !.


/* cross_reference_graph(+Config, +Type, +Sector, -Gxl) <-
      */

cross_reference_graph(Config, Type, Sector, GXL) :-
   %call dependencies INSIDE a unit, module, file
   member(Type, [unit, module, file]),
   findall( Name,
      rar:contains_ll(Sector, Type:Name),
      Names_1 ),
   list_to_ord_set(Names_1, Names_2),
   nodes_and_edges_to_gxl_db(Config, Type, Names_2),
   gxl_db_to_gxl(GXL),
   !.
%call dependencies BETWEEN units, modules, files
cross_reference_graph(Config, in_out, file:File, GXL) :-
   references_1(outgoing, File, Nodes_Out),
   references_1(incoming, File, Nodes_In),
   references_2(
      Config, Nodes_Out, Nodes_In, file:File, GXL),
   !.
cross_reference_graph(Config, Type, file:File, GXL) :-
   member(Type, [incoming, outgoing]),
   references_1(Type, File, Nodes),
   references_3(Config, Type, file, File, Nodes, GXL),
   !.
cross_reference_graph(Config, in_out, Level:Name, GXL) :-
   references_1b(outgoing, Level:Name, Nodes_Out),
   references_1b(incoming, Level:Name, Nodes_In),
   references_2(
      Config, Nodes_Out, Nodes_In, Level:Name, GXL),
   !.
cross_reference_graph(Config, Type, Level:Name, GXL) :-
   member(Type, [incoming, outgoing]),
   references_1b(Type, Level:Name, Nodes),
   references_3(Config, Type, Level, Name, Nodes, GXL),
   !.


/*** implementation ***********************************************/


/* nodes_and_edges_to_gxl_db(+Config, +Type, +Nodes) <-
      */

nodes_and_edges_to_gxl_db(Config, Type, Nodes) :-
   checklist( add_node_to_gxl_db(Config, Type-Type),
      Nodes ),
   length(Nodes, Amount),
   write_list([Amount,
      ' Node(s) added to Graph, now determing the Edges:']),
   nl,
   checklist(
      add_edges_to_gxl_db_with_calls(Type, Nodes),
      Nodes ).


/* add_edges_to_gxl_db_with_calls(_Level, Nodes, Name) <-
      */

add_edges_to_gxl_db_with_calls(file, Nodes, File_1) :-
   write_list(['Searching for outgoing edges from file ',
      File_1, ' ... ']),
   forall(
      rar:who_calls_ff(File_1, File_2, Calls),
      visur:add_edge_to_gxl_db(Nodes, File_1,
         File_2-[call:[from:File_1, to:File_2]:Calls],
         file_file_edge) ),
   writeln('done.'),
   !.
add_edges_to_gxl_db_with_calls(Level_1, Nodes, Name_1) :-
   write_list(['Searching for outgoing edges from ',
      Level_1, ' ', Name_1, ' ... ']),
   findall( Name_2-File_1-File_2:Call_List,
      ( rar:contains_ll(Level_1:Name_1, file:File_1),
        rar:who_calls_ff(File_1, File_2, Call_List),
        rar:contains_ll(Level_1:Name_2, file:File_2) ),
      Calls ),
   findall( Name_2,
      member(Name_2-_-_:_, Calls),
      Names_2 ),
   list_to_ord_set(Names_2, Names_3),
   concat([Level_1, '_', Level_1, '_edge'], Edge_Click),
   forall( member(N, Names_3),
      ( findall( call:[from:F_1, to:F_2]:Calls_2,
           member(N-F_1-F_2:Calls_2, Calls),
           Edge_Calls ),
        visur:add_edge_to_gxl_db(Nodes, Name_1,
         N-Edge_Calls, Edge_Click)
      )
   ),
   writeln('done.').


/* references_1b(+Mode, +Level:Name, -Nodes) <-
      */

references_1b(outgoing, Level_1:Name_1, Nodes_2) :-
   findall( Name_2-File_1-File_2:Call_List,
      ( rar:contains_ll(Level_1:Name_1, file:File_1),
        rar:who_calls_ff(File_1, File_2, Call_List),
        rar:contains_ll(Level_1:Name_2, file:File_2) ),
      Calls ),
   findall( Node,
      member(Node-_-_:_, Calls),
      Nodes_1 ),
   list_to_ord_set(Nodes_1, Nodes_2),
   concat([Level_1, '_', Level_1, '_edge'], Edge_Click),
   forall( member(N, Nodes_2),
      ( findall( call:[from:F_1, to:F_2]:Calls_2,
           member(N-F_1-F_2:Calls_2, Calls),
           Edge_Calls ),
        visur:add_edge_to_gxl_db([Name_1|Nodes_2], Name_1,
           N-Edge_Calls, Edge_Click)
      )
   ).
references_1b(incoming, Level_1:Name_1, Nodes_2) :-
   findall( Name-File-File_1:Call_List,
      ( rar:contains_ll(Level_1:Name_1, file:File_1),
        rar:who_calls_ff(File, File_1, Call_List),
        rar:contains_ll(Level_1:Name, file:File) ),
      Calls ),
   findall( Node,
      member(Node-_-_:_, Calls),
      Nodes_1 ),
   list_to_ord_set(Nodes_1, Nodes_2),
   concat([Level_1, '_', Level_1, '_edge'], Edge_Click),
   forall( member(N, Nodes_2),
      ( findall( call:[from:F_1, to:F_2]:Calls_2,
           member(N-F_1-F_2:Calls_2, Calls),
           Edge_Calls ),
        visur:add_edge_to_gxl_db([Name_1|Nodes_2], N,
           Name_1-Edge_Calls, Edge_Click)
      )
   ).


/* references_1(+Mode, +File, -Nodes) <-
      */

references_1(incoming, File_1, Nodes) :-
   findall( File-Calls,
      rar:who_calls_ff(File, File_1, Calls),
      Nodes_Calls ),
   findall( Node,
      member(Node-_, Nodes_Calls),
      Nodes ),
   forall(
      member( File-Calls, Nodes_Calls),
      visur:add_edge_to_gxl_db([File_1|Nodes],
         File,
         File_1-[call:[from:File, to:File_1]:Calls],
         file_file_edge) ).
references_1(outgoing, File_1, Nodes) :-
   findall( File-Calls,
      rar:who_calls_ff(File_1, File, Calls),
      Nodes_Calls ),
   findall( Node,
      member(Node-_, Nodes_Calls),
      Nodes ),
   forall(
      member( File-Calls, Nodes_Calls),
      visur:add_edge_to_gxl_db([File_1|Nodes],
         File_1,
         File-[call:[from:File_1, to:File]:Calls],
         file_file_edge) ).


/* references_2(
      +Config, +Nodes_Out, +Nodes_In, +Level:Name, -GXL) <-
      */

references_2(
      Config, Nodes_Out_1, Nodes_In_1, Level_1:Name_1, GXL) :-
   intersection(Nodes_Out_1, Nodes_In_1, Nodes_In_Out),
   subtract(Nodes_Out_1, Nodes_In_Out, Nodes_Out_2),
   subtract(Nodes_In_1, Nodes_In_Out, Nodes_In_2),
   add_node_to_gxl_db(Config, Level_1-Level_1, Name_1),
   checklist( add_node_to_gxl_db(Config, in_out-Level_1),
      Nodes_In_Out ),
   checklist( add_node_to_gxl_db(Config, outgoing-Level_1),
      Nodes_Out_2 ),
   checklist( add_node_to_gxl_db(Config, incoming-Level_1),
      Nodes_In_2 ),
   gxl_db_to_gxl(GXL),
   !.


/* references_3(+Config, +Mode, +Level, +Name, +Nodes, -GXL) <-
      */

references_3(Config, Mode, Level, Name, Nodes, GXL) :-
   add_node_to_gxl_db(Config, Level-Level, Name),
   checklist( add_node_to_gxl_db(Config, Mode-Level),
      Nodes ),
   gxl_db_to_gxl(GXL).


/******************************************************************/


