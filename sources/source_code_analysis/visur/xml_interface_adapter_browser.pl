

/******************************************************************/
/***                                                            ***/
/***          Visur: Adaption of Calls from a Browser           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xia_create_graph_view_calls(+XML, +Type) <-
      */

xia_create_graph_view_calls(Object, Type) :-
   xia_get_parameters(Object, Tree, Level, Name),
   create_graph_view_calls(Tree, Type, Level:Name).


/* xia_create_graph_visualize_graph(+Object, +Type) <-
      */

xia_create_graph_visualize_graph(Object, Type) :-
   xia_get_parameters(Object, _, Level, Name),
   create_graph_visualize_graph(Type, Level:Name).


/* graph_layout(Frame, Mode) <-
      */

graph_layout(Picture, xpce) :-
   !,
   writeln(xpce),
   xpce_graph_layout(xpce, Picture).
graph_layout(Picture, Mode) :-
   !,
   writeln(Mode),
   get(Picture, xml, Gxl_1),
   get(Picture, config, Config),
   gxl_graph_layout(Config, Mode, _, Gxl_1, Gxl_2),
   xpce_picture_clear(Picture),
   gxl_to_picture(Config, Gxl_2, Picture).


/* view_file(+Object) <-
      */

view_file(Object) :-
   get(Object, xml, XML_Node),
   File := XML_Node@path,
   send(new(V, view(File)), open),
   send(V, load(File)).


/* view_with_emacs(Node) <-
      */

view_with_emacs(Node) :-
   get(Node, xml, XML_Node),
   File := XML_Node@path,
   emacs(File).

view_with_emacs(Browser, Node) :-
   get(Browser, xml, XML),
   get(Node, xml, XML_Node),
   File := XML_Node@path,
   Root := XML@root,
   concat(Root, File, Abs_File),
   emacs(Abs_File).


/* visur_viewer(+XML) <-
      */

visur_viewer(XML) :-
   File := XML@path,
   sca_variable_get(visur_viewer, Viewer),
   send(Viewer, load(File)).

visur_viewer(Browser, XML_Node) :-
   get(Browser, xml, XML),
   File := XML_Node@path,
   Root := XML@root,
   concat(Root, File, Abs_File),
   sca_variable_get(visur_viewer, Viewer),
   send(Viewer, load(Abs_File)).



/* xia_topology_to_polar_diagram(Object) <-
      */

xia_topology_to_polar_diagram(Object) :-
   xia_get_parameters(Object, _Tree, Level, Name),
   topology:topology_to_polar_diagram(Level:Name).


/* xia_transitive_cross_references_to_polar_diagram(+Object) <-
      */

xia_transitive_cross_references_to_polar_diagram(Object) :-
   xia_get_parameters(Object, _Tree, Level, Name),
   ask_for_scaling_2(Level, Name, Scale),
   transitive_cross_references_to_polar_diagram(Scale, Level:Name).


/* xia_cross_references_to_polar_diagram(+Object) <-
      */

xia_cross_references_to_polar_diagram(Object) :-
   xia_get_parameters(Object, _Tree, Level, Name),
   ask_for_scaling_2(Level, Name, Scale),
   cross_references_to_polar_diagram(Scale, Level:Name).

ask_for_scaling_2(Level, Name, Scale) :-
   concat(['Scale in ', Level, ' ', Name], Title),
   Label = 'Scale',
   Choice = ['sources', 'unit', 'module', 'file'],
   ask_for_type(Title, Label, Choice, Scale).


/* xia_table_predicate_statistics(Object) <-
      */

xia_table_predicate_statistics(Object) :-
   get(Object, xml, XML_Node),
   File := XML_Node@path,
   mpas_rules_calls:table_mpa_statistics(File).


/* xia_dead_code(+Object) <-
      */

xia_dead_code(Object) :-
   xia_get_parameters(Object, _, Level, Name),
   dead_code:dead_code_of_package(Level:Name, List_of_Lists),
   list_to_dag:list_of_lists_to_ord_dag(List_of_Lists, Tree),
   length(List_of_Lists, L),
   concat_atom(['Dead Code of ', Level, ' ', Name, ': ', L],
      Label),
   list_to_dag:tree_to_table(Label,
      ['File', 'Predicate', 'Rules', 'Called'], Tree).


/* xia_undefined_code(+Object) <-
      */

xia_undefined_code(Object) :-
   xia_get_parameters(Object, _, Level, Name),
   undefined_code:undefined_code_of_package(Level:Name, List),
   undefined_code:undefined_code_statistics(List, Statistics, L),
   concat_atom(['Undefined Predicates (Statistics) of ',
      Level, ' ', Name, ': ', L], Label),
   topology:statistic_table(Label,
      ['Undefined Predicate', 'Rules', 'Called'], Statistics).


/* xia_undefined_code_ext(+Object) <-
      */

xia_undefined_code_ext(Object) :-
   xia_get_parameters(Object, _, Level, Name),
   undefined_code:undefined_code_of_package_extended(
      Level:Name, List),
   findall( MPA,
      member([MPA, _, _], List),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   length(MPAs_2, L),
   concat_atom(['Undefined Predicates (Extended) of ',
      Level, ' ', Name, ': ', L], Label),
   list_to_dag:list_of_lists_to_ord_dag(List, Tree),
   list_to_dag:tree_to_table(Label,
      ['Undefined Predicate', 'Caller File', 'Caller Predicate'], Tree).


/* mpas_defined_as <-
      */

mpas_defined_as :-
   Title = 'Choose Definition of ...',
   Label = 'Definition of',
   Choice = ['multifile', 'dynamic', 'discontiguous'],
   ask_for_type(Title, Label, Choice, Type),
   mpas_rules_calls:table_defined_mpas(Type).


/* xia_histogram_basic_mpa_call_statistics <-
      */

xia_histogram_basic_mpa_call_statistics :-
   Title = 'Sorting of Histogram...',
   Label = 'Please choose Sorting',
   Choice = [alphabetical, calls],
   ask_for_type(Title, Label, Choice, Mode),
   mpa_statistics:histogram_basic_mpa_call_statistics(Mode).


/* xia_table_basic_mpa_call_statistics(+normal|ext) <-
      */

xia_table_basic_mpa_call_statistics(Type) :-
   Title = 'Sorting of Table...',
   Label = 'Please choose Sorting',
   Choice = ['alphabetical', 'calls'],
   ask_for_type(Title, Label, Choice, Mode),
   ask_for_integer('Number of Calls at least...', 'Number', Scale),
   xia_table_basic_mpa_call_statistics(Type, Mode, Scale).


/*** implementation ***********************************************/


/* xia_table_basic_mpa_call_statistics(
      +normal|ext, +alphabetical|calls, +Int) <-
      */

xia_table_basic_mpa_call_statistics(normal, Mode, Scale) :-
   xia_table_basic_mpa_call_statistics(Mode, Scale),
   mpa_statistics:
      table_basic_mpa_call_statistics(Mode, Scale),
   !.
xia_table_basic_mpa_call_statistics(ext, Mode, Scale) :-
   mpa_statistics:
      table_basic_mpa_call_statistics_ext(Mode, Scale).


/* ask_for_type(+Title, +Label, +Choice,-Type) <-
      */

ask_for_type(Title, Label, Choice, Result) :-
   new(D, dialog(Title)),
   send(D, append,
      new(Scale, menu(Label, cycle))),
   send_list(Scale, append, Choice),
   send(D, append,
       button('OK',
          and( message(D, return, Scale?selection)))),
   send(D, default_button, 'OK'),
   send(D, append,
      button(cancel, message(D, destroy)) ),
   get(D, confirm, Answer),
   get(Answer, value, Result),
   send(D, destroy),
   Answer \== @nil.


/* xia_get_parameters(+Object, -Tree, -Level, -Name) <-
      */

xia_get_parameters(Object, Tree, Level, Name) :-
   get(Object, xml, XML_Node),
   ( Name := XML_Node@path
   ; Name := XML_Node@name ),
   !,
   sca_variable_get(source_tree, Tree),
   XML_Node = Level:_:_,
   !.


/******************************************************************/


