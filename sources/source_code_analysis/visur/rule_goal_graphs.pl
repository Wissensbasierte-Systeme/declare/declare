

/******************************************************************/
/***                                                            ***/
/***          Visur: Rule/Goal Graph                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* rule_goal_graph(+Config, +Type, +Sector) <-
      */

rule_goal_graph(Config, Type, predicate:MPA, GXL) :-
   !,
   forall( rar:select(rule, MPA, File, Rule),
      add_rule_to_gxl_db(Config, Type, File, Rule) ),
   gxl_db_to_gxl(GXL).
rule_goal_graph(Config, Type, file:File, GXL) :-
   !,
   forall( rar:select(rule, _, File, Rule),
      add_rule_to_gxl_db(Config, Type, File, Rule) ),
   gxl_db_to_gxl(GXL).
rule_goal_graph(Config, Type, Sector, GXL) :-
   !,
   forall(
      ( rar:contains_ll(Sector, file:File),
        rar:select(rule, _, File, Rule) ),
      add_rule_to_gxl_db(Config, Type, File, Rule) ),
   gxl_db_to_gxl(GXL).


/* inverse_rule_goal_graph_1(+Mode, +Config, +MPA_2, -Gxl) <-
      */

inverse_rule_goal_graph(inv_calls_detailed, Config, MPA_2, Gxl) :-
   findall( MPA,
      rar:calls_pp_inv(MPA_2, MPA),
      Calls ),
   findall( File-Rule,
      ( member(MPA, Calls),
        rar:select(rule, MPA, File, Rule) ),
      Rules ),
   forall( member(File-Rule, Rules),
      add_rule_to_gxl_db(Config, rule_goal, File, Rule) ),
   gxl_db_to_gxl(Gxl),
   !.
inverse_rule_goal_graph(inv_calls_normal, Config, MPA_2, Gxl) :-
   findall( File-MPA,
      rar:calls_pp_inv(MPA_2, File, MPA),
      Calls ),
   forall( member(File-MPA, Calls),
      add_mpa_to_gxl_db(Config, rule_goal, MPA_2, File, MPA) ),
   gxl_db_to_gxl(Gxl),
   !.


/*** implementation ***********************************************/


/* add_mpa_to_gxl_db(+Config, +Mode, +MPA2, +File1, +MPA1) <-
      */

add_mpa_to_gxl_db(Config, rule_goal, (M2:P2)/A2,
      File1, (M1:P1)/A1) :-
   Rule = rule:[module:M1, file:File1]:[
      head:[]:[atom:[module:M1, predicate:P1, arity:A1]:[]],
      body:[]:[atom:[module:M2, predicate:P2, arity:A2]:[]] ],
   add_rule_to_gxl_db(Config, rule_goal, File1, Rule).


/* add_rule_to_gxl_db(+Config, +Mode, +File, +Rule) <-
      */

add_rule_to_gxl_db(Config, rule_goal, File, Rule) :-
   add_head_node(Config, Rule, Head),
   add_rule_node(Config, File, Rule, Rule_Node_Id),
   add_edge_to_gxl_db_1(Head-Rule_Node_Id, [], predicate),
   body:_:Body := Rule^body,
   node_edge_node_bfs(Config, Rule_Node_Id, Body).
add_rule_to_gxl_db(Config, goal, _, Rule) :-
   add_head_node(Config, Rule, Head),
   body:_:Body := Rule^body,
   node_edge_node_bfs(Config, Head, Body).


/* add_head_node(+Config, +Rule, -Head) <-
      */

add_head_node(Config, Rule, Head) :-
   [M, P, A] := Rule^head^atom@[module, predicate, arity],
   Head = (M:P)/A,
   rar:special_predicate_attributes(Head, Group, Class, _, _),
   add_node_to_gxl_db_1(Config, Class-Group, Head, ''-Head).


/* node_edge_node_bfs(+Config, +Start, +Body) <-
      */

node_edge_node_bfs(Config, Node_1, Nodes) :-
   forall( member(Node_2, Nodes),
      node_edge_node_dfs(Config, Node_1, Node_2) ).


/* node_edge_node_dfs(+Config, +Node, +FN_Triple) <-
      */

node_edge_node_dfs(Config, Node_1, Tag:Attr:Cont) :-
   rar:term_to_mpa(Tag:Attr:Cont, Node_2),
   rar:special_predicate_attributes(
      Node_2, meta, Class_2, single, attr:Calls:[]),
   add_node_to_gxl_db_1(
      Config, Class_2-meta, Node_2, ''-Node_2, rule:Attr:Cont),
   add_edge_to_gxl_db_1(Node_1-Node_2, [], predicate),
   meta_to_called_content(Calls, Cont, Atoms),
   node_edge_node_bfs(Config, Node_2, Atoms).
node_edge_node_dfs(Config, Node_1, Tag:Attr:Cont) :-
   rar:term_to_mpa(Tag:Attr:Cont, Node_2),
   rar:special_predicate_attributes(
      Node_2, meta, Class_2, duplicate_node, attr:Calls:[]),
   gensym(meta, Meta),
   add_node_to_gxl_db_1(
      Config, Class_2-meta, Meta, ''-Node_2, rule:Attr:Cont),
   add_edge_to_gxl_db_1(Node_1-Meta, [], predicate),
   meta_to_called_content(Calls, Cont, Atoms),
   node_edge_node_bfs(Config, Meta, Atoms).
node_edge_node_dfs(Config, Node_1, Tag:Attr:Cont) :-
   rar:term_to_mpa(Tag:Attr:Cont, Node_2),
   rar:special_predicate_attributes(
      Node_2, meta, _, hide_node, attr:Calls:[]),
   meta_to_called_content(Calls, Cont, Atoms),
   node_edge_node_bfs(Config, Node_1, Atoms).
node_edge_node_dfs(Config, Node_1, Tag:Attr:Cont) :-
   rar:term_to_mpa(Tag:Attr:Cont, Node_2),
   rar:special_predicate_attributes(
      Node_2, _, _, hide_node, _),
   node_edge_node_bfs(Config, Node_1, Cont).
node_edge_node_dfs(_, _, Tag:Attr:Cont) :-
   rar:term_to_mpa(Tag:Attr:Cont, Node_2),
   rar:special_predicate_attributes(
      Node_2, _, _, hide_node_hide_subtree, _).
node_edge_node_dfs(Config, Node_1, Tag:Attr:Cont) :-
   rar:term_to_mpa(Tag:Attr:Cont, Node_2),
   rar:special_predicate_attributes(
      Node_2, Group_2, Class_2, single_node_hide_subtree, _),
   add_node_to_gxl_db_1(
      Config, Class_2-Group_2, Node_2, ''-Node_2),
   add_edge_to_gxl_db_1(Node_1-Node_2, [Node_1-Node_2],
      predicate).
node_edge_node_dfs(Config, Node_1, Tag:Attr:Cont) :-
   rar:term_to_mpa(Tag:Attr:Cont, Node_2),
   rar:special_predicate_attributes(
      Node_2, Group_2, Class_2, duplicate_node_hide_subtree, _),
   Id = Node_1-Node_2,
   add_node_to_gxl_db_1(Config, Class_2-Group_2, Id, ''-Node_2),
   add_edge_to_gxl_db_1(Node_1-Id, [Node_1-Id],
      predicate).
node_edge_node_dfs(Config, Node_1, Tag:Attr:Cont) :-
   rar:term_to_mpa(Tag:Attr:Cont, Node_2),
   rar:special_predicate_attributes(
      Node_2, Group_2, Class_2, duplicate_node, _),
   Id = Node_1-Node_2,
   add_node_to_gxl_db_1(Config, Class_2-Group_2, Id, ''-Node_2),
   add_edge_to_gxl_db_1(Node_1-Id, [Node_1-Id],
      predicate),
   node_edge_node_bfs(Config, Id, Cont).
node_edge_node_dfs(Config, Node_1, Tag:Attr:Cont) :-
   rar:term_to_mpa(Tag:Attr:Cont, Node_2),
   rar:special_predicate_attributes(Node_2, Group_2, Class_2, _, _),
   add_node_to_gxl_db_1(Config, Class_2-Group_2, Node_2, ''-Node_2),
   add_edge_to_gxl_db_1(Node_1-Node_2, [Node_1-Node_2],
      predicate),
   node_edge_node_bfs(Config, Node_2, Cont).
node_edge_node_dfs(_, _, _).


/* add_rule_node(+Config, +File, Rule, +Body_Id) <-
      */

add_rule_node(Config, File, Rule, Body_Id) :-
   gensym(body, Body_Id),
%  file_base_name(File, F),
   node_parameters_to_fn(Config,
      Body_Id, File-'', Rule, rule_node-rule_node, Node),
   rar:update(gxl_node, Body_Id, Node).


/* meta_to_called_content(+Calls, +Cont, -Atoms) <-
      */

meta_to_called_content(Calls, Cont, Atoms) :-
   member(calls:Modes, Calls),
   findall( Atom,
      ( nth(I, Modes, Mode),
        Mode \= n,
        nth(I, Cont, Atom) ),
      Atoms ).


/******************************************************************/


