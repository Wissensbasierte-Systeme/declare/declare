

/******************************************************************/
/***                                                            ***/
/***            Visur Graph Common Methods                      ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


/* add_node_to_gxl_db(+Config, +Group, +Name) <-
      */

add_node_to_gxl_db(Config, Class-Group, Name) :-
   ( atom(Name) ->
     file_base_name(Name, Base_Name)
   ; Base_Name = Name ),
   add_node_to_gxl_db_1(
      Config, Class-Group, Name, ''-Base_Name).


/* node_parameters_to_fn(+Config, +Id, +Text, +Rule,
      +Group, -Node) <-
      */

node_parameters_to_fn(Config, Id, Bubble-Text, Rule,
      Class-Group, Node) :-
   retrieve_node_prmtrs(Config, Class, Group,
      [Size, Symbol, Color]),
   node_parameters_to_fn_sub(Id, Class, Group, Color,
      Symbol, Size, Bubble, Text, Rule, Node),
   !.

node_parameters_to_fn_sub(Id, Class, Group, Color,
      Symbol, Size, Bubble, Text, Rule, Node) :-
   term_to_atom(Class-Group, Mouse_Click),
   Node = node:[id:Id]:[
      prmtr:[mouse_click:Mouse_Click,
         color:Color, symbol:Symbol, size:Size]:[
            string:[bubble:Bubble]:[Text],
            Rule ] ].


/* node_parameters_to_fn(+Config, +Id, +Text, +Group, -Node) <-
      */

node_parameters_to_fn(Config, Id, Bubble-Text,
      Class-Group, Node) :-
   retrieve_node_prmtrs(Config, Class, Group,
      [Size, Symbol, Color]),
   node_parameters_to_fn_sub(Id, Class, Group, Color,
      Symbol, Size, Bubble, Text, Node),
   !.

node_parameters_to_fn_sub(Id, Class, Group, Color,
      Symbol, Size, Bubble, Text, Node) :-
   term_to_atom(Class-Group, Mouse_Click),
   Node = node:[id:Id]:[
      prmtr:[mouse_click:Mouse_Click,
         color:Color, symbol:Symbol, size:Size]:[
            string:[bubble:Bubble]:[Text] ] ].


/* retrieve_node_prmtrs(+Config, +Group, +Class, -Prmtrs) <-
      */

retrieve_node_prmtrs(Config, Class, Group, Prmtrs) :-
   Prmtrs :=
      Config^node_symbols^node_symbol::[
         @group=Group, @class=Class]@[size, symbol, color],
   !.
retrieve_node_prmtrs(Config, _, _, Prmtrs) :-
   Prmtrs :=
      Config^node_symbols^node_symbol::[
         @group=default, @class=default]@[size, symbol, color],
   !.
retrieve_node_prmtrs(_, _, _, Prmtrs) :-
   alias_to_default_fn_triple(visur_config, Config),
   Prmtrs :=
      Config^node_symbols^node_symbol::[
         @group=default, @class=default]@[size, symbol, color].


/* add_edge_to_gxl_db(+Nodes, +V, +W-Calls, +Mouse_Click) <-
      */

add_edge_to_gxl_db(Nodes, V, W-Calls, Mouse_Click) :-
   member(V, Nodes),
   member(W, Nodes),
   add_edge_to_gxl_db_1(V-W, Calls, Mouse_Click),
   !.
add_edge_to_gxl_db(_, _, _, _).


/*** implementation ***********************************************/


/* add_node_to_gxl_db_1(+Config, +Class-Group, +Id,
      +Bubble-Label, +Rule) <-
      */

add_node_to_gxl_db_1(Config, Class-Group, Id, Bubble-Label, Rule) :-
   node_parameters_to_fn(Config,
      Id, Bubble-Label, Rule, Class-Group, Node),
   rar:update(gxl_node, Id, Node).


/* add_node_to_gxl_db_1(+Config, +Class-Group, +Id,
      +Bubble-Label) <-
      */

add_node_to_gxl_db_1(Config, Class-Group, Id, Bubble-Label) :-
   node_parameters_to_fn(Config,
      Id, Bubble-Label, Class-Group, Node),
   rar:update(gxl_node, Id, Node).


/* add_edge_to_gxl_db_1(+(V-W), +Calls, +Mouse_Click) <-
      */

add_edge_to_gxl_db_1(V-W, Calls, Mouse_Click) :-
   Edge = edge:[id:V-W, from:V, to:W]:[
      prmtr:[mouse_click:Mouse_Click,
         arrows:second, color:grey]:[
            string:[bubble:'']:[],
            calls:[]:Calls] ],
   rar:update(gxl_edge, V, W, Edge),
   !.


/******************************************************************/


