

/******************************************************************/
/***                                                            ***/
/***  VisuR:  Messages                                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


message_1 :-
   Message = 'Layouting nodes ... ',
   message_window_ts(Message).
message_2 :-
   Message = 'Displaying nodes and edges ... ',
   message_window_ts(Message).
message_3(Time) :-
   concat(Time, ' sec. ... done.', Message),
   message_window_ln(Message).
message_5(List_1) :-
   Message = 'Roots:',
   maplist( term_to_atom,
      List_1, List_2 ),
   message_window_sub(Message, List_2).


/*** implementation ***********************************************/


/* message_window(+Message) <-
      */

message_window(Message) :-
   message_window('VisuR/RaR Messages', Message).


/* message_window_ln(+Message) <-
      */

message_window_ln(Message) :-
   message_window(Message),
   message_window('\n').


/* message_window_ts(+Message) <-
      */

message_window_ts(Message) :-
   current_time_to_string(Time_String),
   message_window(Time_String),
   message_window(' '),
   message_window(Message).


/* message_window_ts_ln(+Message) <-
      */

message_window_ts_ln(Message) :-
   message_window_ts(Message),
   message_window('\n').


/* message_window(+Title, +Message) <-
      */

message_window(Title, Message) :-
   message_window(Title, @message_window, Message).

message_window(Title, Id, Message) :-
   ( object(Id) ->
     true
   ; send(new(Id, view(Title)), open) ),
   send(Id, append, Message).


/* message_window_sub(+Message, +List) <-
      */

message_window_sub(Message, List_1) :-
   message_window_ts_ln(Message),
   maplist( concat('   '),
      List_1, List_2 ),
   checklist( message_window_ln,
      List_2 ).


/******************************************************************/


