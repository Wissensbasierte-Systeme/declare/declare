

/******************************************************************/
/***                                                            ***/
/***         Visur: Adaption of Calls from a Picture            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* visualize_java_calls(+XML_Node) <-
      */

visualize_java_calls(XML_Node) :-
   Name := XML_Node@id,
   create_graph_visualize_graph(java, dir:Name).


/* visualize_edge_calls(+XML_Edge) <-
      */

visualize_edge_calls(XML_Edge) :-
   From := XML_Edge@from,
   To := XML_Edge@to,
   create_graph_visualize_graph(zoom, From:To).


/* visualize_calls(+XML_Node) <-
      */

visualize_calls(XML_Node) :-
   File := XML_Node@id,
   create_graph_visualize_graph(tree:[]:[], _,
      rule_goal, file:File).


/* visualize_calls(+XML, +Type) <-
      */

visualize_calls(XML, Type) :-
   ( [MPA] := XML^prmtr^string^content::'*'
   ; MPA := XML@id ),
   term_to_atom(MPA, Atom),
   create_graph_visualize_graph(tree:[]:[], _,
      Type, predicate:Atom).


/* visualize_calls(+Object, +Level, +Type) <-
      */

visualize_calls(XML_Node, Level, Type) :-
   Name := XML_Node@id,
   create_graph_visualize_graph(Type, Level:Name).


/* view_with_emacs_double_click(+XPCE_Addr) <-
      */

view_with_emacs_double_click(XPCE_Addr) :-
   xpce_address_to_gxl_id(XPCE_Addr, File),
   emacs(File).


/* view_with_emacs_popup(+XML_Node) <-
      */

view_with_emacs_popup(XML_Node) :-
   File := XML_Node@id,
   emacs(File).


/* view_inverse_calls(+XML) <-
      */

view_inverse_calls(XML) :-
   ( [Id_1] := XML^prmtr^string^content::'*'
   ; Id_1 := XML@id ),
   findall( P,
      rar:calls_pp_inv(Id_1, P),
      Ps ),
   term_to_atom(Id_1, Id_2),
   concat(['Predicates calling ', '\'', Id_2, '\''], Label),
   viewer(writelq_list, Ps, Label).


/* view_rule(+XML) <-
      */

view_rule(XML) :-
   Rule := XML^prmtr^rule,
   rar:term_to_mpa(Rule, MPA),
   term_to_atom(MPA, Label),
   viewer(fn_to_xml, Rule, Label).


/* view_calls(+XML) <-
      */

view_calls(XML) :-
   [From_1, To_1] := XML@[from, to],
   file_base_name(From_1, From_2),
   file_base_name(To_1, To_2),
   term_to_atom(From_2-To_2, Label),
   Calls := XML^prmtr^calls^content::'*',
   viewer(fn_to_xml, Calls, Label).


/* view_xml_source_code(+XML) <-
      */

view_xml_source_code(XML) :-
   [Id] := XML^prmtr^string^content::'*',
   findall( Rule,
      rar:select(rule, Id, _, Rule),
      Rules ),
   view_xml(rules:[]:Rules).


/* view_listing(+XML) <-
      */

view_listing(XML) :-
   Id := XML@id,
   Id = _-(M:P)/A,
   tmp_file(visur, tmp, F),
   tell(F),
   catch( listing(M:P/A), _, fail),
   told,
   send(new(V, view('')), open),
   send(V, load(F)),
   delete_file(F).
view_listing(XML) :-
   [Id] := XML^prmtr^string^content::'*',
   Id = (M:P)/A,
   tmp_file(visur, tmp, F),
   tell(F),
   catch( listing(M:P/A), _, fail),
   told,
   send(new(V, view('')), open),
   send(V, load(F)),
   delete_file(F).


/* view_gxl(+Picture) <-
      */

view_gxl(Picture) :-
   picture_to_gxl(Picture, Gxl, _Config),
   view_xml(Gxl).


/* view_config(+Picture) <-
      */

view_config(Picture) :-
   get(Picture, config, Config),
   view_xml(Config).


/* picture_to_eps_file(+Picture) <-
      */

picture_to_eps_file(Picture) :-
   get(@finder, file, @off, '.eps', FileName),
   writeln(Picture),
   new(File, file(FileName)),
   send(File, open, write),
   send(File, append, Picture?postscript),
   send(File, close),
   send(File, done).


/* view_text(+Texts) <-
      */

view_text(Texts) :-
   viewer(writelq_list, Texts, '').


/* view_xml(XML) <-
      */

view_xml(XML) :-
   viewer(fn_to_xml, XML, '').


/* viewer(+Goal,  +Text, +Label) <-
      */

viewer(Goal, Text, Label) :-
   Call =.. [Goal, Text],
   tmp_file(visur, tmp, File),
   tell(File),
   call(Call),
   told,
   send(new(View, view(Label)), open),
   send(View, load(File)),
   delete_file(File).


/* file_selection_window_gxl_graph(+Picture) <-
      */

file_selection_window_gxl_graph(Picture) :-
   get(@finder, file, @on, [gxl, xml], File),
   load_gxl_graph(Picture, File).


/* load_gxl_graph(+Picture, +File) <-
      */

load_gxl_graph(Picture, File) :-
   dread_(xml(gxl_graph), File, Gxl),
%  alias_to_fn_triple(visur_gxl_config, Config),
   xpce_picture_clear(Picture),
   gxl_to_picture(_Config, Gxl, Picture),
   add_file_to_alias(graph_files, File).


/* save_gxl_graph(+Picture) <-
      */

save_gxl_graph(Picture) :-
   get(@finder, file, @off, [gxl, xml], File),
   picture_to_gxl(Picture, Gxl, _),
   dwrite(xml_2, File, Gxl),
   add_file_to_alias(graph_files, File).


/* delete_light_weighted_edges(+Picture) <-
      */

delete_light_weighted_edges(Picture) :-
   picture_to_gxl(Picture, Gxl_1, Config),
   ask_for_integer('Delete light weighted Edges',
      'Delete edges with less than x cross references:', Weight),
   T_Config = config:[]:[
      action:[name:delete_light_weighted_edges,
         limit:Weight]:[]],
   gxl_to_gxl(T_Config, Gxl_1, Gxl_2),
   gxl_to_picture(Config, Gxl_2, _).


/* delete_small_circles(+Picture) <-
      */

delete_small_circles(Picture) :-
   picture_to_gxl(Picture, Gxl_1, Config),
   delete_small_circles(Gxl_1, Gxl_2),
   gxl_to_picture(Config, Gxl_2, _).


/*** implementation ***********************************************/


/* delete_small_circles(+Gxl_1, -Gxl_2) <-
      */

delete_small_circles(Gxl_1, Gxl_2) :-
   ask_for_integer('Delete Edges',
      'Ratio:', Ratio),
   gxl_to_weighted_edges(Ratio, Gxl_1, Edge_Ids),
   findall( edge:[id:Id]:[],
      member(Id, Edge_Ids),
      Cont ),
   Config = config:[]:[
      action:[name:delete]:Cont ],
   gxl_to_gxl(Config, Gxl_1, Gxl_2).


/*gxl_to_weighted_edges(+Ratio, +Gxl, -Edges) <-
      */

gxl_to_weighted_edges(Ratio, Gxl, Edges_2) :-
   gxl_to_vertices_and_edges(0-7, Gxl, _, Edges_1),
   findall( Id_1,
      ( member(Id_1-V-W-_-_-_-Weight_1, Edges_1),
        member(_-W-V-_-_-_-Weight_2, Edges_1),
        R is (Weight_1 / Weight_2)*100,
        R =< Ratio ),
      Edges_2 ).


/******************************************************************/


