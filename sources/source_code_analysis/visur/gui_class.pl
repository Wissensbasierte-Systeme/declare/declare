

/******************************************************************/
/***                                                            ***/
/***          Visur:  Visualization of Rules                    ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library(persistent_frame)).
:- use_module(library(pce)).
:- use_module(library(pce_style_item)).

:- pce_autoload(toc_window, library(pce_toc)).
:- pce_autoload(report_dialog, library(pce_report)).
:- pce_autoload(tool_bar, library(toolbar)).
:- pce_autoload(finder, library(find_file)).
:- pce_global(@finder, new(finder)).

/*** interface ****************************************************/


/*** implementation ***********************************************/


/*** Classes ******************************************************/


:- pce_begin_class(
      visur_gui, persistent_frame, 'VisuR/RaR').


initialise(Frame) :->
   Pen = 1,
   sca_variable_get(source_tree, Tree),
   alias_to_fn_triple(visur_browser, HB_Config),

   send_super(Frame, initialise, 'VisuR/RaR'),
   new(Dialog_1, dialog),
   new(Menu_Bar, menu_bar),
   new(Tool_Bar, tool_bar(Frame)),
   new(View, view),
   new(Navigation_Hierarchy,
      hierarchy_browser_ddk(HB_Config, Tree)),
   new(Report_Dialog, report_dialog),

   send(Frame, append, Dialog_1),
   send(Dialog_1, append, Menu_Bar),
   send(Tool_Bar, below, Menu_Bar),
   send(Navigation_Hierarchy, left, View),
   send(Dialog_1, above, Navigation_Hierarchy),
   send(Report_Dialog, below, Navigation_Hierarchy),

   send(Dialog_1, pen, Pen),
   send(View, pen, Pen),
   send(Navigation_Hierarchy, pen, Pen),
   send(Report_Dialog, pen, Pen),

   send(Frame, menu_bar_actualize),
   send(Frame, fill_tool_bar, Tool_Bar),
   sca_variable_set(visur_viewer, View),
   sca_variable_set(visur_browser, Navigation_Hierarchy),
   send(Frame, redraw),
   sca_variable_set(visur_gui, Frame).


file_selection_window_load_menu(Frame) :->
   get(@finder, file, @on, [xml], File),
   send(Frame, fill_menu_bar, File),
   add_file_to_alias(visur_menu_bar, File).


menu_bar_actualize(Frame) :->
   alias_to_file(visur_menu_bar, File),
   send(Frame, fill_menu_bar, File).


fill_menu_bar(Frame, File) :->
   dread_(xml(xml_menu), File, Menu_1),
   create_reload_entries(add_file_to_alias(visur_config),
      visur_config, Items_1),
   fn_patch_2(Menu_1, name:'Visur Config', Items_1, Menu_2),
   create_reload_entries(add_file_to_alias(visur_gxl_config),
      visur_gxl_config, Items_2),
   fn_patch_2(Menu_2, name:'GXL Config', Items_2, Menu_3),
   create_reload_entries(add_file_to_alias(gxl_presettings),
      gxl_presettings, Items_3),
   fn_patch_2(Menu_3, name:'GXL Presettings', Items_3, Menu_4),
   create_reload_entries(add_file_to_alias(visur_browser),
      visur_browser, Items_4),
   fn_patch_2(Menu_4, name:'Hierarchy Browser Config', Items_4, Menu_5),
   create_reload_entries(add_file_to_alias(predicate_groups),
      predicate_groups, Items_5),
   fn_patch_2(Menu_5, name:'Special Predicates', Items_5, Menu_6),
   create_reload_entries(add_file_to_alias(visur_menu_bar),
      visur_menu_bar, Items_6),
   fn_patch_2(Menu_6, name:'Menu Bar', Items_6, Menu_7),
   create_reload_entries(load_rar_database, db_files, Items_7),
   fn_patch_3(Menu_7, name:'Reload Project', Items_7, Menu_8),
   create_reload_entries(load_gxl_graph, graph_files, Items_8),
   fn_patch_3(Menu_8, name:'Reload GXL Graph', Items_8, Menu_9),
   fill_menu_reload_projects(dislog, Items_9),
   fn_patch_3(Menu_9, name:'DisLog Source Location', Items_9, Menu_10),
   fill_menu_reload_projects(prolog, Items_10),
   fn_patch_3(Menu_10, name:'Prolog Source Location', Items_10, Menu_11),
   fill_menu_reload_projects(java, Items_11),
   fn_patch_3(Menu_11, name:'Java Source Location', Items_11, Menu_12),
   fill_menu_reload_projects(php, Items_12),
   fn_patch_3(Menu_12, name:'PHP Source Location', Items_12, Menu_13),
   get(Frame, member, dialog, Dialog),
   get(Dialog, member, menu_bar, Menu_Bar),
   xml_menu_to_xpce(Menu_Bar, [Frame], Menu_13),
   !.


fill_tool_bar(Frame, Tool_Bar) :->
   send_list(Tool_Bar, append, [
      tool_button(file_selection_window_rar_db,
         image('16x16/new.xpm'), 'Load Database'),
      gap,
      tool_button(message(Frame, load_options, visur_config),
         image('16x16/book2.xpm'), 'Load Options'),
      gap,
      tool_button(xml_hierarchy_to_browser,
         image('16x16/hierarchy.xpm'), 'Load Hierarchy'),
      gap,
      gap,
%     tool_button(print,
%        image('16x16/print.xpm'), 'Print'),
      gap,
      tool_button(visur_gui_exit,
         image('16x16/stop.xpm'), 'Exit') ]).


file_selection_window_rar_db(Frame) :->
   get(@finder, file, @on, [rar, xml], File),
   send(Frame, load_rar_database, File).


load_rar_database(Frame, DB_File) :->
   send(Frame, report, progress,
      'Loading %s ...', DB_File),
   rar:load_rar_database(DB_File),
   sca_variable_get(source_tree, Tree),
   get(Frame, member, hierarchy_browser_ddk, Browser),
   alias_to_fn_triple(visur_browser, HB_Config),
   hierarchy_to_browser_ddk(HB_Config, Tree, Browser),
   send(Frame, visur_label, DB_File),
   send(Frame, menu_bar_actualize),
   send(Frame, report, done).


visur_label(Frame, Name) :->
   concat(['VisuR/RaR - ', Name], Label_1),
   send(Frame, label, Label_1).


dislog_hierarchy_to_browser(Frame) :->
   rar:dislog_sources_to_tree(Tree),
   xml_hierarchy_to_browser(Frame, Tree).


xml_hierarchy_to_browser(Frame) :->
   get(@finder, file, @on, xml, File),
   read_structure(File, [Tree],
      [dialect(xml), types([])] ),
   xml_hierarchy_to_browser(Frame, Tree).


reminder(_Frame) :->
   new(@Reminder, dialog( 'Reminder' )),
   send_list(@Reminder,
      append,
      [text('You need to generate the database new after you'),
       text('changed the hierarchy in the hierarchy browser!'),
       text('Call:'),
       text('"Database->Generate Database"'),
       text(''),
       button('OK', message(@Reminder, destroy)) ]),
   send(@Reminder, default_button, 'OK'),
   send(@Reminder, open).


select_filesystem(Frame) :->
   ask_for_directory(Dir),
   file_system_to_xml(Dir, Tree),
   alias_to_fn_triple(visur_browser, HB_Config),
   get(Frame, member, hierarchy_browser_ddk, Browser),
   hierarchy_to_browser_ddk(HB_Config, Tree, Browser),
   sca_variable_set(source_tree, Tree).


show_filesystem(_Frame) :->
   file_system_to_xml('~/DisLog', Tree),
%  sca_variable_set(source_tree, Tree),
%  get(Frame, member, hierarchy_browser_ddk, Browser),
   alias_to_fn_triple(visur_browser, HB_Config),
   hierarchy_to_browser_ddk(HB_Config, Tree, _Browser).



generate_db(Frame) :->
   get(Frame, member, hierarchy_browser_ddk, Browser),
   get(Browser, xml, Tree_1),
   rar:parse_sources_to_rar_database(Tree_1, Tree_2),
   send(Browser, xml, Tree_2),
   send(Frame, visur_label, none).


visur_gui_exit(Frame) :->
   rar:reset_rar_database,
   send(Frame, destroy).


save_rar_database(Frame) :->
   get(Frame, label, Label),
   ( Label = 'VisuR/RaR - none' ->
     send(Frame, save_rar_database_as)
   ; ( alias_to_file(db_files, File),
       rar:save_rar_database(File) ) ).


save_rar_database_as(Frame) :->
   get(@finder, file, @off, [rar, xml], File),
   rar:save_rar_database(File),
   send(Frame, visur_label, File),
   send(Frame, menu_bar_actualize).


load_options(Frame, Alias) :->
   get(@finder, file, @on, xml, File),
   add_file_to_alias(Alias, File),
   send(Frame, menu_bar_actualize).


picture_to_eps_file(Frame) :->
   get(Frame, member, visur_workspace, Picture),
   get(@finder, file, @off, '.eps', FileName),
   new(File, file(FileName)),
   send(File, open, write),
   send(File, append, Picture?postscript),
   send(File, close),
   send(File, done).


new_project(Frame) :->
   sca_variable_get(visur_viewer, Viewer),
   send(Viewer, clear),
   rar:reset_rar_database,
   get(Frame, member, hierarchy_browser_ddk, Browser),
   send(Browser, xml, t:[name:'']:[]),
   send(Frame, visur_label, none).


visur_db_clear(Frame) :->
   sca_variable_get(source_tree, Tree),
   rar:reset_rar_database,
   sca_variable_set(source_tree, Tree),
   send(Frame, visur_label, none).


incremental_update(Frame) :->
   rar:update_rar_database,
   sca_variable_get(source_tree, Tree),
   get(Frame, member, hierarchy_browser_ddk, Browser),
   send(Browser, xml, Tree).


version(_) :->
   new(@Version, dialog( 'Version Information' )),
   send(@Version,
      append(text('Visur/RAR GUI: Version 2.2.5'))),
   send(@Version,
      append(text('Visur: Version 2.2'))),
   send(@Version,
      append(text('RAR: Version 2.2'))),
   send(@Version,
      append(text('GXL: Version 1.2'))),
   sca_variable_get(rar_db_created, (Type, Date, Prolog_Version)),
   term_to_atom(Date, Date_Atom),
   concat(['RAR Database created: ', Date_Atom], D_Text),
   send(@Version,
      append(text(D_Text))),
   term_to_atom(Type, Type_Atom),
   concat(['RAR Database Type: ', Type_Atom], T_Text),
   send(@Version,
      append(text(T_Text))),
   term_to_atom(Prolog_Version, PV_Atom),
   concat(['Used XPCE Version: ', PV_Atom], PV_Text),
   send(@Version,
      append(text(PV_Text))),
   send(@Version, open).


add_file_to_alias(Frame, Type, Text) :->
   add_file_to_alias(Type, Text),
   send(Frame, menu_bar_actualize).


:- pce_end_class(visur_gui).


/******************************************************************/


