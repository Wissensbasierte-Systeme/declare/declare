

/******************************************************************/
/***                                                            ***/
/***          Visur:  Visualization of Rules                    ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


/* visur <-
      */

visur_gui :-
   sca_variable_set(source_tree, test:[name:'']:[]),
   new(Frame, visur_gui),
   send(Frame, open),
   ( alias_to_file(db_files, File) ->
     send(Frame, load_rar_database, File)
   ; true ),
   !.


/* xml_hierarchy_to_browser(+Frame, +Tree) <-
      */

xml_hierarchy_to_browser(Frame, Tree) :-
   get(Frame, member, hierarchy_browser_ddk, Browser),
   alias_to_fn_triple(visur_browser, HB_Config),
   hierarchy_to_browser_ddk(HB_Config, Tree, Browser),
   sca_variable_set(source_tree, Tree),
%  send(Frame, generate_db),
   send(Frame, reminder),
   send(Frame, visur_label, none).


/* create_reload_entries(+Goal, +Type, Menu_Item) <-
      */

create_reload_entries(Goal, Type, Menu_Items) :-
   alias_to_files(Type, Files),
   maplist( menu_reload_to_xml(Goal),
      Files, Menu_Items ).

fill_menu_reload_projects(Type, Names) :-
%    writeln(Frame),
   alias_to_fn_triple(visur_projects, VP),
   findall( XML,
      ( Name := VP^visur_project::[@type=Type]@name,
        XML = menu:[name:Name, module:visur,
           predicate:load_project,
           prmtrs:[@1, Type, Name], end_group:off]:[] ),
      Names ).


/* save_xml_hierarchy_to_file <-
      */

save_xml_hierarchy_to_file :-
   get(@finder, file, @off, [xml], File),
   sca_variable_get(source_tree, Tree),
   dwrite(xml, File, Tree).

/*** implementation ***********************************************/


/* menu_reload_to_xml(+Goal, +Text, -XML) <-
      */

menu_reload_to_xml(Call_Variables, Text, XML) :-
   Call_Variables =.. [Call|Variables_1],
   append(Variables_1, [Text], Variables_2),
   Prmtrs = [@1|Variables_2],
   XML = menu:[name:Text, module:visur, predicate:Call,
      prmtrs:Prmtrs, message:class,
      end_group:off]:[].


/* ask_for_directory(Name) <-
      */

ask_for_directory(Name) :-
   new(D, dialog('Prompting for Directory')),
   send(D, append,
   new(TI, text_item('Directory', ''))),
   send(D, append,
   button(ok, message(D, return,
      TI?selection))),
   send(D, append,
      button(cancel, message(D, return, @nil))),
   send(D, default_button, ok),
   get(D, confirm, Answer),
   send(D, destroy),
   Answer \== @nil,
   Name = Answer.


/* view_visur_gui_settings <-
      */

view_visur_gui_settings :-
   findall( [Alias, File],
      ( member(Alias, [visur_gxl_config, gxl_presettings,
          visur_config, predicate_groups,
          visur_browser, visur_menu_bar] ),
        alias_to_file(Alias, File) ),
      Rows_1 ),
   list_to_ord_set(Rows_1, Rows_2),
   xpce_display_table(_, _, 'Used Configuration',
      ['Alias', 'File'], Rows_2).


/******************************************************************/


