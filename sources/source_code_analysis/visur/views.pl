

/******************************************************************/
/***                                                            ***/
/***          Visur: Views                                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* create_graph_view_calls(+Tree, +Type, +Level:Name) <-
      */

create_graph_view_calls(Tree, Type, Level:Name) :-
   alias_to_fn_triple(visur_config, Config),
   alias_to_fn_triple(predicate_groups, PG),
   create_graph(Config, PG, Tree, Type, Level:Name, Gxl),
   findall(Call,
      ( Call := Gxl^graph^edge^prmtr^calls^call,
        From := Call@from,
        rar:select(leaf_to_path, From, Element),
        rar:parse_element(Element, _, module, Module_2),
        not(member(Module_2, [gxl_graphs, visur_rar]))
       ),
      Calls ),
   findall( atom:[module:M, predicate:P, arity:A]:[],
      ( member(X, Calls),
        C := X^content::'*',
        member(_-(M:P)/A, C)
      ),
      Xs ),
   list_to_ord_set(Xs, Ys),
   finding_test(Calls),
   V = overview:[]:Calls,
   V_2 = from_the_outside_called_atoms:[dislog_module:Name]:Ys,
   view_xml(V_2),
   view_xml(V).


/*** implementation ***********************************************/


/* finding_test(+Calls) <-
      */

finding_test(Calls) :-
   findall( (_, (M:P)/A),
      ( member(X, Calls),
        C := X^content::'*',
        member(_-(M:P)/A, C)
      ),
      For_Test_Searching_1 ),
   list_to_ord_set(For_Test_Searching_1, For_Test_Searching_2),
   rar:transitive_closure_inv(For_Test_Searching_2, FMPAs_1),
   sublist( constraint_1,
      FMPAs_1, FMPAs_2 ),
   nl,
   writeln('Tests calling predicates in this module:'),
   nl,
   writeln_list(FMPAs_2).

constraint_1((_, (_:test)/_)).


/******************************************************************/


