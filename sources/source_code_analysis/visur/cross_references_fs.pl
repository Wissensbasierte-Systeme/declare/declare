

/******************************************************************/
/***                                                            ***/
/***      cross references between files, or directories        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* cross_references_fs(+Visur_Conf, +Tree, +Path, -Gxl) <-
      */

cross_references_fs(Visur_Conf, Tree, Path, Gxl) :-
   ask_for_integer('Question', 'Scale:', Scale),
   findall( Type_2:Name-Files,
      ( rar:fs_contains_ll(Tree, Scale,
           dir:Path, Type_1:Name, Files),
        member(Type_1:Type_2, [dir:cross_reference_dir,
           file:cross_reference_file]) ),
      Names ),
   rar:contains_dir_files(Tree, Path, Pure_Files),
   add_nodes_to_gxl_db(Visur_Conf, Names),
   add_edges_to_gxl_db(Pure_Files),
   gxl_db_to_gxl(Gxl).


/* method_calls_in_files(+Visur_Conf, +Tree, +Path, -Gxl) <-
      */

method_calls_in_files(Visur_Conf, Type, Tree, Path, Gxl) :-
   rar:contains_dir_files(Tree, Path, Pure_Files),
   forall(
      ( member(File, Pure_Files),
        rar:select(rule, _, File, Rule) ),
      visur:add_rule_to_gxl_db(Visur_Conf, Type, File, Rule) ),
   gxl_db_to_gxl(Gxl).


/*** implementation ***********************************************/


/* ask_for_integer(-Integer) <-
      */

ask_for_integer(Integer) :-
   ask_for_integer('Request for an integer:', 'Please type an Integer', Integer).

ask_for_integer(Title, Text, Integer) :-
   new(D, dialog(Title)),
   send(D, append,
      new(A, int_item(Text, low := 0, high := 1000)) ),
   send(D, append,
      button('OK', message(D, return, A?selection))),
   send(D, default_button, 'OK'),
   send(D, append,
      button(cancel, message(D, destroy)) ),
   get(D, confirm, Answer),
   get(Answer, value, Integer),
   send(D, destroy),
   Answer \== @nil.


/* add_nodes_to_gxl_db(+Node_Names) <-
      */

add_nodes_to_gxl_db(V_Config, Node_Names) :-
   checklist( add_node_to_gxl_db(V_Config),
      Node_Names ),
   length(Node_Names, Amount),
   write_list([Amount,
      ' Node(s) added to Graph.']).


/* add_node_to_gxl_db(+Config, +Group:Name-Files) <-
      */

add_node_to_gxl_db(Config, Group:Name-Files) :-
   file_base_name(Name, Base_Name),
   retrieve_node_prmtrs(Config, Group, Group,
      [Size, Symbol, Color]),
   create_fs_node(Name, Group, Group, Color,
      Symbol, Size, Name, Base_Name, Files, Node),
   rar:update(gxl_node, Name, Node).

create_fs_node(Id, file, file, Color,
      Symbol, Size, Bubble, Text, _, Node) :-
   term_to_atom(file-file, Mouse_Click),
   Node = node:[id:Id]:[
      prmtr:[mouse_click:Mouse_Click,
         color:Color, symbol:Symbol, size:Size]:[
         string:[bubble:Bubble]:[Text] ] ].
create_fs_node(Id, Class, Group, Color,
      Symbol, Size, Bubble, Text, files:_:Embedded_1, Node) :-
   term_to_atom(Class-Group, Mouse_Click),
   maplist( file_name_to_embedded_gxl_node,
      Embedded_1, Embedded_2 ),
   Node = node:[id:Id]:[
      prmtr:[mouse_click:Mouse_Click,
             color:Color,
             symbol:Symbol,
             size:Size]:[
         string:[bubble:Bubble]:[Text] ],
      graph:[id:graph_2, edgeids:true, edgemode:directed,
         hypergraph:false]:Embedded_2 ].

file_name_to_embedded_gxl_node(File_Name, Node) :-
   Id := File_Name@path,
   file_base_name(Id, Name),
   Node = node:[id:Id]:[prmtr:[]:[string:[bubble:Id]:[Name]]].


/* add_edges_to_gxl_db(+Pure_Files) <-
      */

add_edges_to_gxl_db(Pure_Files) :-
   writeln('Now determing the Edges:'),
   nl,
   checklist( add_edges_to_gxl_db(Pure_Files),
      Pure_Files ).

add_edges_to_gxl_db(Pure_Files, File) :-
   add_edges_to_gxl_db_with_calls(file, Pure_Files, File).


/******************************************************************/


