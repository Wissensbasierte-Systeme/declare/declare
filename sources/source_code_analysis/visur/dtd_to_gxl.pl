

/******************************************************************/
/***                                                            ***/
/***          Visur:  Graphs                                    ***/
/***                                                            ***/
/******************************************************************/


:- dynamic edge_tmp/2.


/*** interface ****************************************************/


/* dtd_to_gxl(+File, -Gxl) <-
      */

dtd_to_gxl(File, Gxl) :-
   rar:reset_gxl_database,
   read_file_to_codes(File, Codes, []),
   alias_to_fn_triple(visur_config, Config),
   extract_elements(Config, Codes),
   gxl_db_to_gxl(Gxl),
   !.


/*** implementation ***********************************************/


/* extract_elements(+Config, +Codes) <-
      */

extract_elements(Config, [60, 33, 69, 76, 69, 77, 69, 78, 84, 32|Codes_1]) :-
   visur:extract_word(Codes_1, Node, Codes_2),
   string_to_list(Node_2, Node),
   visur:extract_elements(Codes_2, Subnodes_1, Codes_3),
   sublist( visur:filter_empty_lists,
      Subnodes_1, Subnodes_2 ),
   maplist( string_to_list,
      Strings, Subnodes_2 ),
   maplist( string_to_atom,
      Strings, Str ),
   string_to_atom(Node_2, Node_3),
   add_node_to_gxl_db(Config, dtd-element, Node_3),
   checklist( add_dtd_node_to_gxl_db(Config),
      Str ),
   checklist( add_dtd_edge_to_gxl_db(Config, Node_3),
      Str ),
   extract_elements(Config, Codes_3),
   !.
extract_elements(Config, [_|Codes]) :-
   extract_elements(Config, Codes),
   !.
extract_elements(_, []) :-
   !.


/* add_dtd_node_to_gxl_db(+Config, +Node) <-
      */

add_dtd_node_to_gxl_db(_, 'EMPTY') :-
   !.
add_dtd_node_to_gxl_db(Config, Node) :-
   add_node_to_gxl_db(Config, dtd-element, Node).


/* add_dtd_edge_to_gxl_db(+Config, +V, +W) <-
      */

add_dtd_edge_to_gxl_db(Config, V, 'EMPTY') :-
   gensym(empty_, W),
   add_dtd_node_to_gxl_db(Config, W),
   visur:add_edge_to_gxl_db([V, W], V, W-[], dtd_element),
   !.
add_dtd_edge_to_gxl_db(_, V, W) :-
   visur:add_edge_to_gxl_db([V, W], V, W-[], dtd_element).


/******************************************************************/


