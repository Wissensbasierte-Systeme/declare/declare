

/******************************************************************/
/***                                                            ***/
/***           Visur: Init                                      ***/
/***                                                            ***/
/******************************************************************/


:- module( visur, [
      visur_gui/0,
      add_node_to_gxl_db/3,
      dtd_to_tree/2 ] ).


/*** interface ****************************************************/


:- [ 'messages.pl',
     'cross_references.pl',
     'cross_references_fs.pl',
     'cross_reference_predicates.pl',
     'rule_goal_graphs.pl',
     'gui.pl',
     'gui_class.pl',
     'graph_common_methods.pl',
     'xml_interface_adapter_browser.pl',
     'xml_interface_adapter_picture.pl',
     'graphs.pl',
     'dtd_to_tree.pl',
     'dtd_metrics.pl',
     'dtd_to_gxl.pl',
     'gui_new_project.pl',
     'views.pl',
     'gui_dependency_graph.pl' ].


/*** implementation ***********************************************/



/******************************************************************/


