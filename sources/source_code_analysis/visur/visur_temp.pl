fileviewer(Dir) :-
   new(F, frame('File Viewer')),
   send(F, append(new(B, browser))),
   send(new(D, dialog), below(B)),
   send(D, append(button(view,
      message(@prolog, view,
      B?selection?key)))),
   send(D, append(button(quit,
      message(F, destroy)))),
   send(B, members(directory(Dir)?directories)),
   send(F, open).

view(F) :-
   send(new(V, view(F)), open),
   send(V, load(F)).



:- pce_begin_class(explorer, frame, "Explore the filesystem").

initialise(E, Dir:directory) :->
   send_super(E, initialise, 'Simple explorer'),
   send(E, append, new(DH, directory_hierarchy(Dir))),

   send(new(D, dialog), below(DH)),
   send(D, append(button(view,
      message(@prolog, tam,
         DH?selection)))),
   send(D, append(button(close,
      message(E, destroy))) ),
   send(new(report_dialog), below, D).

:- pce_end_class.

tam(U) :-
   chain_list(U, L),
   forall( member(M, L),
      ( get(M, name, N),
        writeln(M-N),
        get(M, label, La),
        writeln(La) ) ).


:- pce_begin_class(directory_hierarchy, toc_window,

   "Browser for a directory-hierarchy").

initialise(FB, Root:directory) :->
   send(FB, send_super, initialise),
   get(Root, name, Name),
   send(FB, root, toc_folder(Name, Root)).

expand_node(FB, D:directory) :->
   new(SubDirsNames, chain),
   new(FileNames, chain),
   send(D, scan, FileNames, SubDirsNames),
   get(SubDirsNames, map, ?(D, directory, @arg1), SubDirs),
   writelq(D),
   send(SubDirs, for_all,
      message(FB, son, D,
         create(toc_folder, @arg1?name, @arg1))).


open_node(FB, Node:file) :->
   "Called if a file is double-clicked"::
   send(FB?frame, open_node, Node).

:- pce_end_class.

