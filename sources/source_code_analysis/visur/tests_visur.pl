

/******************************************************************/
/***                                                            ***/
/***          Visur: Tests                                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(source_code_analysis:visur_new, rule_goal_graph) :-
   rar:init(sca_tests_1),
   visur:create_graph_visualize_graph(rule_goal, unit:rar).


test(source_code_analysis:dtd, dtd_to_tree) :-
   sca_variable_get(sca, H),
   concat(H, 'common/tests/squashml.dtd', File),
   visur:dtd_to_browser(File, Frame),
   send(Frame, destroy).

test(source_code_analysis:dtd, dtd_to_gxl) :-
   sca_variable_get(sca, H),
   concat(H, 'common/tests/squashml.dtd', File),
   visur:dtd_to_gxl(File, Gxl_1),
   alias_to_fn_triple(gxl_config, Gxl_Config),
   gxl_graph_layout(Gxl_Config, dfs_inv, _, Gxl_1, Gxl_2),
   gxl_to_picture(Gxl_Config, Gxl_2, _).


/*** implementation ***********************************************/


/******************************************************************/


