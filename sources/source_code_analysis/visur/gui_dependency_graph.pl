

/******************************************************************/
/***                                                            ***/
/***          VisuR:  Project GUI                               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gui_dependency_graph <-
      */

gui_dependency_graph :-
   new(Frame, persistent_frame),
   send(Frame, label, 'Dependency Graph'),
   new(Dialog_MPA_Graph, dialog),
   new(Dialog_File_Graph, dialog),
   send(Frame, append, Dialog_MPA_Graph),
   send(Dialog_MPA_Graph, left, Dialog_File_Graph),
   send(Dialog_File_Graph, pen, 1),
   send(Dialog_MPA_Graph, pen, 1),
   send_list(Dialog_MPA_Graph, append,
      [ new(N1, text_item('Module')),
        new(N2, text_item('Predicate')),
        new(A, int_item('Arity', low := 0, high := 165)),
        new(S1, new(S1, menu('Type', cycle))),
        button('Visualize', and(message(@prolog,
           create_mpa_graph,
           S1?selection,
           N1?selection,
           N2?selection,
           A?selection))) ]),
   send_list(S1, append, ['Calls',
      'Is Called By (detailed)', 'Is Called By (normal)']),
   send_list(Dialog_File_Graph, append,
      [ new(N3, text_item('File')),
        new(S2, new(S2, menu('Type', cycle))),
        button('Visualize', and(message(@prolog,
           cross_reference_graph,
           S2?selection,
           N3?selection))) ]),
   send_list(S2, append, ['Incoming & Outgoing',
      'Incoming', 'Outgoing', 'Rule_Goal']),
   send(Frame, open).


/*** implementation ***********************************************/


/* create_mpa_graph(+Type, +M, +P, +A) <-
      */

create_mpa_graph(Type, M, P, A) :-
   Tree = tree:[]:[],
   term_to_atom((M:P)/A, MPA),
   ( Type = 'Calls',
     create_graph_visualize_graph(Tree, _, rule_goal, predicate:MPA)
   ; Type = 'Is Called By (detailed)',
     create_graph_visualize_graph(Tree, _, inv_calls_detailed,
        predicate:MPA)
   ; Type = 'Is Called By (normal)',
     create_graph_visualize_graph(Tree, _, inv_calls_normal,
        predicate:MPA) ).


/* cross_reference_graph(+Mode, +File) <-
      */

cross_reference_graph(Mode, File) :-
   ( Mode = 'Incoming & Outgoing',
     create_graph_visualize_graph(in_out, file:File)
   ; Mode = 'Incoming',
     create_graph_visualize_graph(incoming, file:File)
   ; Mode = 'Outgoing',
     create_graph_visualize_graph(outgoing, file:File)
   ; create_graph_visualize_graph(rule_goal, file:File) ).


/******************************************************************/


