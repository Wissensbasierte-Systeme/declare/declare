

/******************************************************************/
/***                                                            ***/
/***          Visur:  Graphs                                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* create_graph_visualize_graph(+Type, +Level:Name) <-
      */

create_graph_visualize_graph(Type, Level:Name) :-
   sca_variable_get(source_tree, Tree),
   create_graph_visualize_graph(Tree, _, Type, Level:Name).


/* create_graph_visualize_graph(
      +Tree, ?Picture, +Type, +Level:Name) <-
      */

create_graph_visualize_graph(Tree, Picture, Type, Level:Name) :-
   alias_to_fn_triple(visur_config, Config),
   alias_to_fn_triple(predicate_groups, PG),
   create_graph(Config, PG, Tree, Type, Level:Name, Gxl_1),
   make_some_graph_operations(Type, Gxl_1, Gxl_2),
   alias_to_fn_triple(visur_gxl_config, Gxl_Conf),
   decide_about_layout(Gxl_Conf, Type, Gxl_2, Gxl_3),
   message_2,
   gxl_graph_label(Type, Level, Name, Label),
   Gxl_4 := Gxl_3*[^prmtr@label:Label],
   sca_variable_set(gxl, Gxl_4),
   determine_runtime(
      gxl_to_picture(Gxl_Conf, Gxl_4, Picture), RT ),
   decide_about_layout(Type, Picture),
   message_3(RT).


/*** implementation ***********************************************/


/* create_graph(+Type, +Level:Name, -Gxl) <-
      */

create_graph(Type, Level:Name, Gxl) :-
   alias_to_fn_triple(visur_config, Config),
   alias_to_fn_triple(predicate_groups, PG),
   sca_variable_get(source_tree, Tree),
   create_graph(Config, PG, Tree, Type, Level:Name, Gxl).


/* create_graph(+Tree, +PG, +Type, +Level:Name, -Gxl) <-
      */

create_graph(Config, PG, Tree, Type, Level:Name, Gxl) :-
   rar:reset_gxl_database,
   rar:assert_predicate_groups(PG),
   ( Level = predicate ->
     term_to_atom(Name_1, Name)
   ; Name_1 = Name ),
   build_graph(Config, Tree, Type, Level:Name_1, Gxl),
   ground(Gxl).

build_graph(Config, _Tree, zoom, Start:End, Gxl) :-
   !,
   zoom_module_to_file(Config, module:Start, module:End, Gxl).
build_graph(Visur_Conf, _Tree, Type, Sector, Gxl) :-
   memberchk(Type, [unit, module, file,
      incoming, outgoing, in_out]),
   !,
   cross_reference_graph(Visur_Conf, Type, Sector, Gxl).
build_graph(Visur_Conf, _Tree, Type, Sector, Gxl) :-
   memberchk(Type, [rule_goal, goal]),
   !,
   rule_goal_graph(Visur_Conf, Type, Sector, Gxl).
build_graph(Visur_Conf, _, Type, _:MPA, Gxl) :-
   memberchk(Type, [inv_calls_detailed, inv_calls_normal]),
   !,
   inverse_rule_goal_graph(Type, Visur_Conf, MPA, Gxl).
build_graph(Visur_Conf, Tree, cross_references_fs, _:Path, Gxl) :-
   !,
   cross_references_fs(Visur_Conf, Tree, Path, Gxl).
build_graph(Visur_Conf, Tree, cross_rule_goal, A:Path, Gxl) :-
   !,
   cross_reference_rule_goal(Visur_Conf, Tree, A, Path, Gxl).
build_graph(Visur_Conf, Tree, method_calls, _:Path, Gxl) :-
   !,
   method_calls_in_files(Visur_Conf, goal, Tree, Path, Gxl).


/* make_some_graph_operations(+Type, +Gxl_1, -Gxl_2) <-
      */

make_some_graph_operations(Type, Gxl_1, Gxl_4) :-
   gxl_to_gxl_without_loops(Gxl_1, Gxl_2),
   gxl_to_gxl_without_single_nodes(Gxl_2, Gxl_3, Singletons),
   memberchk(Type:Name, [cross_references_fs:'Directory',
      Type:Type]),
   singletons_to_table(Name, Singletons),
   gxl_edge_weight(Gxl_3, Gxl_4).


/* decide_about_layout(+Config, +Type, +Gxl_1, -Gxl_2) <-
      */

decide_about_layout(Gxl_Config, Type, Gxl_1, Gxl_2) :-
   memberchk(Type, [file, rule_goal, goal,
      inv_calls_detailed, inv_calls_normal, method_calls]),
   message_1,
   determine_runtime(
      gxl_graph_layout(Gxl_Config, xpce, Roots, Gxl_1, Gxl_2),
      RT ),
   message_5(Roots),
   message_3(RT),
   !.
decide_about_layout(Gxl_Config, Type, Gxl_1, Gxl_2) :-
   memberchk(Type, [cross_rule_goal]),
   message_1,
   determine_runtime(
      gxl_graph_layout(Gxl_Config, xpce, Roots, Gxl_1, Gxl_2),
      RT ),
   message_5(Roots),
   message_3(RT),
   !.
decide_about_layout(Gxl_Config, Type, Gxl_1, Gxl_2) :-
   memberchk(Type, [zoom]),
   message_1,
   determine_runtime(
      gxl_graph_layout(Gxl_Config, dfs_inv, Roots, Gxl_1, Gxl_2),
      RT ),
   message_5(Roots),
   message_3(RT),
   !.
decide_about_layout(_, _, Gxl, Gxl).


/* decide_about_layout(+Type, +Picture) <-
      */

decide_about_layout(Type, Picture) :-
   memberchk(Type, [cross_references_fs, module, unit, incoming,
      outcoming, in_out] ),
   xpce_graph_layout(xpce, Picture),
   !.
decide_about_layout(_, _).


/* singletons_to_table(+Type, +Singletons) <-
      */

singletons_to_table(Type, Singletons) :-
   findall( [Id],
      member(Id, Singletons),
      Rows ),
   visur_list_length_to_title(Rows, 'Singleton', Title),
   ( ( not( Rows = []),
       xpce_display_table(_, _, Title, [Type], Rows) )
   ; true ).

visur_list_length_to_title(Xs, Suffix, Title) :-
   length(Xs, N),
   ( N = 1 ->
     concat([N, ' ', Suffix], Title)
   ; concat([N, ' ', Suffix, 's'], Title) ).


/* gxl_graph_label(+Type, +Level, +Name, -Label) <-
      */

gxl_graph_label(cross_references_fs, _, Name, Label) :-
   concat(['type: cross references, dir: ',
      Name], Label),
   !.
gxl_graph_label(method_calls, Level, Name, Label) :-
   concat(['type: method calls, ', Level, ': ', Name], Label),
   !.
gxl_graph_label(Type, Level, Name, Label) :-
   concat(['type: ', Type, ', ', Level, ': ', Name], Label),
   !.


/******************************************************************/


