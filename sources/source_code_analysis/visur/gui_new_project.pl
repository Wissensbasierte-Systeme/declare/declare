

/******************************************************************/
/***                                                            ***/
/***          VisuR:  Project GUI                               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


insert_new_project(Frame) :-
   new(Dialog, dialog('Add Source Location ...')),
   send_list(Dialog, append,
     [ new(Name, text_item('Source Location Name' )),
       new(D, menu('Type', cycle)),
       new(_, label('Directories:')),
       new(N1, text_item('Source Code Files' )),
       new(N2, text_item('XML representation Files' )),
       new(_, label('File:')),
       new(N3, text_item('XML Hierarchy File' )),
       new(S, new(S, menu('Hierarchy'))),
       new(_, label('')),
       button(cancel, message(Dialog, destroy)),
       button(enter,
          and(message(@prolog,
                project_to_xml_file,
                Frame,
                Name?selection,
                D?selection,
                N1?selection,
                N2?selection,
                N3?selection,
                S?selection),
             message(Dialog, destroy))) ] ),
   send_list(D, append, ['dislog', 'prolog', 'java', 'php']),
   send_list(S, append, ['XML File', 'File System']),
   send(Dialog, default_button, enter),
   send(Dialog, open).


/* load_project(+Frame, +Type, +Name) <-
      */

load_project(Frame, Type, Name) :-
   alias_to_fn_triple(visur_projects, Visur_Projects),
   Project := Visur_Projects^visur_project::[@type=Type, @name=Name],
   H := Project@hierarchy,
   get_source_hierarchy(Type, H, Project, Tree),
   xml_hierarchy_to_browser(Frame, Tree).


/*** implementation ***********************************************/


/* project_to_xml_file(+Frame,
      +Name, +Type, +Source, +XML, +Hierarchy, +Choice) <-
      */

project_to_xml_file(Frame,
      Name, Type, Source, XML, Hierarchy, Choice) :-
   check_directory(Source),
   check_directory(XML),
   check_file(Hierarchy),
   alias_to_file(visur_projects, File),
   alias_to_fn_triple(visur_projects, Projects_1),
   V = visur_project:[name:Name, hierarchy:Choice,
      xml_sources:XML, sources:Source,
      xml_hierarchy:Hierarchy, type:Type]:[],
   Projects_1 = visur_projects:Attr:Cont_1,
   Cont_2 = [V|Cont_1],
   Projects_2 = visur_projects:Attr:Cont_2,
   dwrite(xml, File, Projects_2),
   send(Frame, menu_bar_actualize),
   load_project(Frame, Type, Name).


/* check_directory(+Dir) <-
      */

check_directory('') :-
   !.
check_directory(Dir) :-
   exists_directory_with_tilde(Dir),
   !.
check_directory(Dir) :-
   concat(['Failure: Directory ', Dir,
      ' does not exist! Saving aborted.'],
   M),
   writeln(M),
   fail.


/* check_file(+File) <-
      */

check_file('') :-
   !.
check_file(File) :-
   exists_file_with_tilde(File),
   !.
check_file(File) :-
   concat(['Failure: File ', File,
      ' does not exist! Saving aborted.'],
   M),
   writeln(M),
   fail.


/* get_source_hierarchy(+Type, +Choice, +Project, -Tree) <-
      */

get_source_hierarchy(Type, 'File System', Project, Tree) :-
   !,
   Directory := Project@sources,
   XML_Directory := Project@xml_sources,
   rar:sources_to_tree(Type, Directory, XML_Directory, Tree).
get_source_hierarchy(_, 'XML File', Project, Tree) :-
   H_File := Project@xml_hierarchy,
   exists_file_with_tilde(H_File),
   dread(xml(dtd:[]:[]), H_File, Tree),
   !.


/******************************************************************/


