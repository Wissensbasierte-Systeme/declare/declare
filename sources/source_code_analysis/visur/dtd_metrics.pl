

/******************************************************************/
/***                                                            ***/
/***          Visur: dtd metrics                                ***/
/***                                                            ***/
/******************************************************************/


:- dynamic dtd_element_tmp/2.


/*** interface ****************************************************/


/* mark_cycles_in_tree(+Tree_1, -Tree_2) <-
      */

mark_cycles_in_tree(Tree_1, Tree_2) :-
   mark_cycles_in_tree([], Tree_1, Tree_2).


/* add_call_statistics(+Tree_1, -Tree_2) <-
      */

add_call_statistics(Tree_1, Tree_2) :-
   retractall( dtd_node_tmp(_, _) ),
   retractall( dtd_edge_tmp(_, _) ),
   add_elements_to_db_tmp(Tree_1),
   tree_to_tmp_node_statistics(Tree_1),
   add_statistics_to_tree(Tree_1, Tree_2),
   retractall( dtd_node_tmp(_, _) ),
   retractall( dtd_edge_tmp(_, _) ).


/*** implementation ***********************************************/


/* mark_cycles_in_tree(+List, +Tree_1, -Tree_2]) <-
      */

mark_cycles_in_tree(List, dtd_element:Attr_1:[],
      dtd_element:Attr_2:[]) :-
   memberchk(name:Name, Attr_1),
   memberchk(Name, List),
   !,
   dtd_image(element_recursive, open, Open),
   dtd_image(element_recursive, close, Close),
   (tag:Attr_2:[]) := (tag:Attr_1:[])*[@open:Open, @close:Close],
   !.
mark_cycles_in_tree(_, dtd_attribute:Attr:Cont,
      dtd_attribute:Attr:Cont) :-
   !.
mark_cycles_in_tree(List_1, Tag:Attr:Cont_1, Tag:Attr:Cont_2) :-
   memberchk(name:Name, Attr),
   List_2 = [Name|List_1],
   maplist( mark_cycles_in_tree(List_2),
      Cont_1, Cont_2).


/* add_elements_to_db_tmp(+V, +Tree) <-
      */

add_elements_to_db_tmp(dtd_element:Attr:Cont) :-
   !,
   memberchk(name:Name, Attr),
   assert( visur:dtd_element_tmp(Name, 1) ),
   checklist( add_elements_to_db_tmp,
      Cont ).
add_elements_to_db_tmp(dtd_attribute:_:_) :-
   !.
add_elements_to_db_tmp(_:_:Cont) :-
   checklist( add_elements_to_db_tmp,
      Cont ).


/* tree_to_tmp_node_statistics(+Tree) <-
      */

tree_to_tmp_node_statistics(Tree) :-
   Tree = dtd:_:Cont,
   checklist( delete_roots,
      Cont ),
   setof( Element,
      visur:dtd_element_tmp(Element, _),
      Elements ),
   ( foreach(Element, Elements) do
     ( findall( N,
          visur:dtd_element_tmp(Element, N),
          Ns ),
       retractall( visur:dtd_element_tmp(Element, _) ),
       length(Ns, L),
       assert( visur:dtd_element_tmp(Element, L) ) ) ).

delete_roots(_:Attr:_) :-
   memberchk(name:Name, Attr),
   retractall( visur:dtd_element_tmp(Name, _) ).


/* add_statistics_to_tree(+Tree_1, -Tree_2) <-
      */

add_statistics_to_tree(dtd_element:Attr_1:Cont_1,
      dtd_element:Attr_2:Cont_2) :-
   memberchk(name:Name_1, Attr_1),
   get_dtd_element_tmp(Name_1, L),
   concat([Name_1, ' (', L, ')'], Name_2),
   (dtd_element:Attr_2:[]) :=
      (dtd_element:Attr_1:[])*[@name:Name_2],
   maplist( add_statistics_to_tree,
      Cont_1, Cont_2).
add_statistics_to_tree(dtd_attribute:Attr:Cont,
      dtd_attribute:Attr:Cont) :-
   !.
add_statistics_to_tree(Tag:Attr:Cont_1, Tag:Attr:Cont_2) :-
   maplist( add_statistics_to_tree,
      Cont_1, Cont_2).

get_dtd_element_tmp(Name_1, L) :-
   visur:dtd_element_tmp(Name_1, L),
   !.
get_dtd_element_tmp(_, 0).


/******************************************************************/


