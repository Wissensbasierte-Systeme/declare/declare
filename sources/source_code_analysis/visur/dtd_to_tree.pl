

/******************************************************************/
/***                                                            ***/
/***          Visur: dtd to tree for hierarchy browser          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dtd_to_browser(+File, -Frame) <-
      */

dtd_to_browser(File) :-
   dtd_to_browser(File, _).

dtd_to_browser(File, Frame) :-
   term_to_atom(File, A_File),
   concat('DTD Browser: ', A_File, Label),
   new(Frame, persistent_frame(Label)),
   dtd_to_tree(File, Tree),
   new(HB, hierarchy_browser_ddk(config:[]:[], Tree)),
   send(Frame, append, HB),
   send(Frame, open),
   !.


/* dtd_to_tree(+File, -Tree) <-
      */

dtd_to_tree(File, Tree_3) :-
   rar:reset_dtd_database,
   read_file_to_codes(File, Codes, []),
   extract_elements_and_attributes(Codes),
   file_base_name(File, Base),
   dtd_db_to_tree(Tag:Attr:Cont),
   Tree_1 = Tag:[name:Base|Attr]:Cont,
   mark_cycles_in_tree(Tree_1, Tree_2),
   add_call_statistics(Tree_2, Tree_3).


/*** implementation ***********************************************/


/* extract_elements_and_attributes(+Codes) <-
      */

extract_elements_and_attributes(
      [A, B, E, L, E, M, E, N, T, J|Codes_1]) :-
   code_to_lower_atom([A, B, E, L, E, M, E, N, T, J],
      '<!element '),
   extract_word(Codes_1, Word, Codes_2),
   extract_elements(Codes_2, Elements, Codes_3),
   checklist( add_node_to_dtd_db,
      [Word|Elements] ),
   checklist( add_edge_to_dtd_db(Word),
      Elements ),
   extract_elements_and_attributes(Codes_3),
   !.
extract_elements_and_attributes(
      [P, B, A, T, T, L, I, S, T, U|Codes_1]) :-
   code_to_lower_atom([P, B, A, T, T, L, I, S, T, U],
      '<!attlist '),
   extract_word(Codes_1, Word, Codes_2),
   extract_attributes(Codes_2, Attributes, Codes_3),
   checklist( add_attr_to_dtd_db(Word),
      Attributes ),
   extract_elements_and_attributes(Codes_3).
extract_elements_and_attributes([_|Codes]) :-
   extract_elements_and_attributes(Codes),
   !.
extract_elements_and_attributes([]) :-
   !.


/* extract_word(+Codes_1, -Node, -Codes_2) <-
      */

extract_word([Code|Codes_1], Word, Codes_2) :-
   word_delimiter(Delimiters),
   memberchk(Code, Delimiters),
   extract_word(Codes_1, Word, Codes_2),
   !.
extract_word(Codes_1, Atom, Codes_2) :-
   extract_word_sub(Codes_1, Word, Codes_2),
   string_to_list(String, Word),
   string_to_atom(String, Atom).

extract_word_sub([62|Codes], [], [62|Codes]) :-
   !.
extract_word_sub([Code|Codes], [], Codes) :-
   word_delimiter(Delimiters),
   memberchk(Code, Delimiters),
   !.
extract_word_sub([C|Codes_1], [C|Word], Codes_2) :-
   extract_word_sub(Codes_1, Word, Codes_2).


/* extract_elements(+Codes_1, -Elements, -Codes_2) <-
      */

extract_elements(Codes_1, Elements_4, Codes_2) :-
   extract_elements_sub(Codes_1, Elements_1, Codes_2),
   sublist( filter_empty_lists,
      Elements_1, Elements_2 ),
   maplist( string_to_list,
      Elements_3, Elements_2 ),
   maplist( string_to_atom,
      Elements_3, Elements_4 ).

extract_elements_sub([62|Codes], [], Codes) :-
   !.
extract_elements_sub(Codes_1, List, Codes_3) :-
   extract_word(Codes_1, Word, Codes_2),
   extract_elements_sub(Codes_2, Words, Codes_3),
   extract_elements_sub_sub(Word, Words, List).

extract_elements_sub_sub('', Words, Words) :-
   !.
extract_elements_sub_sub(Word, Words, [Word|Words]) :-
   !.


/* extract_attributes(+Codes, -Attributes, -Codes) <-
      */

extract_attributes([C|Codes_1], Attributes, Codes_2) :-
   memberchk(C, [10, 13, 32]),
   extract_attributes(Codes_1, Attributes, Codes_2),
   !.
extract_attributes(Codes_1, Attributes, Codes_2) :-
   extract_attributes_sub(Codes_1, Attributes, Codes_2).

extract_attributes_sub([C|Codes_1], Attributes, Codes_2) :-
   memberchk(C, [10, 13, 32]),
   extract_attributes_sub(Codes_1, Attributes, Codes_2),
   !.
extract_attributes_sub([62|Codes], [], Codes) :-
   !.
extract_attributes_sub(Codes_1, [A|As], Codes_4) :-
   extract_word(Codes_1, Word, Codes_2),
   extract_values(Codes_2, Values, Type, Codes_3),
   A = Word:Type:Values,
   extract_attributes_sub(Codes_3, As, Codes_4).


/* extract_values(+Codes_1, -Values, -Type, -Codes_2) <-
      */

extract_values(Codes_1, Values_4, Type, Codes_2) :-
   extract_values_sub(Codes_1, Values_1, Type, Codes_2),
   sublist( filter_empty_lists,
      Values_1, Values_2 ),
   maplist( string_to_list,
      Values_3, Values_2 ),
   maplist( string_to_atom,
      Values_3, Values_4 ).

extract_values_sub([C, I, M, P, L, I, E, D|Codes], [],
      implied, Codes) :-
   code_to_lower_atom([C, I, M, P, L, I, E, D],
      '#implied').
extract_values_sub([C, R, E, Q, U, I, R, E, D|Codes], [],
      required, Codes) :-
   code_to_lower_atom([C, R, E, Q, U, I, R, E, D],
      '#required').
extract_values_sub(Codes_1, [Subnode|Subnodes], T, Codes_3) :-
   extract_sub_value(Codes_1, Subnode, Codes_2),
   extract_values_sub(Codes_2, Subnodes, T, Codes_3).

extract_sub_value([C|Codes], [], Codes) :-
   word_delimiter(D),
   memberchk(C, D).
extract_sub_value([C|Codes_1], [C|Node], Codes_2) :-
   extract_sub_value(Codes_1, Node, Codes_2).


/* filter_empty_lists(List) <-
      */

filter_empty_lists([]) :-
   !,
   fail.
filter_empty_lists(_).


/* add_node_to_dtd_db(+Id) <-
      */

add_node_to_dtd_db(Id) :-
   rar:insert(dtd_element, Id, node).


/* add_attr_to_dtd_db(Name:IR:Values) <-
      */

add_attr_to_dtd_db(Node, Attr:IR:Values) :-
   concat([Attr, ' (', IR, ')'], Name),
   maplist( value_to_xml_value,
      Values, XML_Values ),
   dtd_image(attribute, open, Open),
   dtd_image(attribute, close, Close),
   Attribute = dtd_attribute:[name:Name,
   open:Open, close:Close]:XML_Values,
   rar:insert(dtd_attribute, Node, Attribute).

value_to_xml_value(V, XML_V) :-
   dtd_image(value, open, Open),
   dtd_image(value, close, Close),
   XML_V = dtd_attr_value:[name:V, open:Open,
         close:Close]:[].


/* add_edge_to_dtd_db(+V, +W) <-
      */

add_edge_to_dtd_db(V, W) :-
   rar:update(dtd_edge, V, W, edge).


/* dtd_db_to_tree(-Tree) <-
      */

dtd_db_to_tree(Tree) :-
   findall( Node,
      rar:select(dtd_element, Node, _),
      Nodes_1 ),
   list_to_ord_set(Nodes_1, Nodes_2),
   findall( V-W,
      rar:select(dtd_edge,  V, W, _),
      Edges ),
   vertices_and_edges_to_tree(Nodes_2, Edges, Tree).


/* vertices_and_edges_to_tree(+Nodes, +Edges, -Tree) <-
      */

vertices_and_edges_to_tree(Nodes, Edges, Tree_2) :-
   vertices_and_edges_to_possible_roots(Nodes, Edges, Roots),
   maplist( node_to_xml_node,
      Roots, XML_Roots ),
   Tree_1 = dtd:[open:'16x16/book2.xpm',
      close:'16x16/manual.xpm']:XML_Roots,
   edges_to_tree(Tree_1, Tree_2).


/* edges_to_tree(+Tree_1, -Tree_2) <-
      */

edges_to_tree(dtd_attribute:Attr:Cont, dtd_attribute:Attr:Cont) :-
   !.
edges_to_tree(Tag:Attr:Cont_1, Tag:Attr:Cont_3) :-
   maplist( add_subnodes,
      Cont_1, Cont_2 ),
   maplist( edges_to_tree,
      Cont_2, Cont_3 ).

add_subnodes(Tag:Attr:Cont_1, Tag:Attr:Cont_3) :-
   memberchk(name:Name, Attr),
   findall( W,
      rar:select(dtd_edge,  Name, W, _),
      Ws ),
   rar:delete( dtd_edge, Name, _, _),
   maplist( node_to_xml_node,
      Ws, Cont_2 ),
   append(Cont_1, Cont_2, Cont_3).


/* node_to_xml_node(+Node, -XML_Node) <-
      */

node_to_xml_node(Id, XML_Node) :-
   findall( Attribute,
      rar:select(dtd_attribute, Id, Attribute),
      Attributes ),
   dtd_image(element, open, Open),
   dtd_image(element, close, Close),
   XML_Node =
      dtd_element:[name:Id,
         open:Open, close:Close]:Attributes.


/* code_to_lower_atom(+Codes, -Atom) <-
      */

code_to_lower_atom(Codes, Atom) :-
   maplist( char_code,
      Chars, Codes),
   maplist( downcase_atom,
      Chars, Lower_Chars ),
   atom_chars(Atom, Lower_Chars).


/* dtd_image(+Type, +Open|Close, -Image) <-
      */

dtd_image(element, open, 'images/text.gif').
dtd_image(element, close, 'images/text.gif').
dtd_image(attribute, open, 'images/options.gif').
dtd_image(attribute, close, 'images/options.gif').
dtd_image(value, open, 'images/diagnosis_orange.gif').
dtd_image(value, close, 'images/diagnosis_orange.gif').
dtd_image(element_recursive, open, 'images/ecl_refresh.gif').
dtd_image(element_recursive, close, 'images/ecl_refresh.gif').


word_delimiter([10, 13, 32, 40, 41, 42, 43, 44, 63, 124]).


/******************************************************************/


