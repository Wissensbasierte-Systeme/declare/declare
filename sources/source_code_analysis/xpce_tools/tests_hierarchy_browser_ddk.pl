

/******************************************************************/
/***                                                            ***/
/***      Hierarchy Browser: Tests                              ***/
/***                                                            ***/
/******************************************************************/


:- dynamic test_hierarchy_browser_ddk_tmp/1.
:- multifile test_hierarchy_browser_ddk_tmp/1.
:- discontiguous test_hierarchy_browser_ddk_tmp/1.


/*** interface ****************************************************/


test(source_code_analysis:hierarchy_browser_ddk, 0) :-
   dislog_variable_get(sca, SCA_Path),
   !,
   concat(SCA_Path, 'gxl/tests/hierarchy_gxl.xml', File),
   dread_(xml, File, [XML]),
   hierarchy_to_browser_ddk(XML, E),
   assert( test_hierarchy_browser_ddk_tmp(E) ).
test(source_code_analysis:hierarchy_browser_ddk, 1) :-
   file_system_to_xml('~/Declare/', XML),
   hierarchy_to_browser_ddk(XML, E),
   assert( test_hierarchy_browser_ddk_tmp(E) ).
test(source_code_analysis:hierarchy_browser_ddk, 2) :-
   rar:dislog_sources_to_tree(Tree_1),
   hierarchy_to_browser_ddk(Tree_1, Explorer),
   assert( test_hierarchy_browser_ddk_tmp(Explorer) ),
   alias_to_default_fn_triple(hierarchy_browser_config, Config),
   Tree_2 = system:[name:'System']:[
      unit:[name:unit_1]:[
         module:[name:module_1]:[
            file:[]:[],
            file:[name:file_2,
                  open:'16x16/ghost.xpm',
                  close:'16x16/ghost.xpm',
                  path:'~/Declare/sources/declare']:[],
            file:[name:file_3,
                  open:'16x16/binocular.xpm',
                  close:'16x16/binocular.xpm',
                  mouse_click:'system']:[],
            individual:[name:file_3_individual,
                  open:'16x16/binocular.xpm',
                  close:'16x16/binocular.xpm']:[],
            file:[name:file_4]:[]
         ],
         module:[name:module_2,
               open:'16x16/new.xpm',
               close:'16x16/font.xpm']:[],
         module:[name:module_3]:[]
      ],
      unit:[name:unit_2,
            open:'16x16/ghost.xpm',
            close:'16x16/foot.xpm']:[
         module:[name:module_4]:[]
      ]
   ],
   hierarchy_to_browser_ddk(Config, Tree_2, Explorer),
   assert( test_hierarchy_browser_ddk_tmp(Explorer) ).
test(source_code_analysis:hierarchy_browser_ddk, 3) :-
   alias_to_default_fn_triple(hierarchy_browser_config, Config),
   rar:dislog_sources_to_tree(Tree),
   hierarchy_to_browser_ddk(Config, Tree, Explorer),
   assert( test_hierarchy_browser_ddk_tmp(Explorer) ).


/*** implementation ***********************************************/


/******************************************************************/


