

/******************************************************************/
/***                                                            ***/
/***      Hierarchy Browser: Prolog Methods                     ***/
/***                                                            ***/
/******************************************************************/


:- module( hierarchy_to_browser_ddk, [
      hierarchy_to_browser_ddk/2,
      hierarchy_to_browser_ddk/3,
      file_system_to_xml/2] ).


/*** interface ****************************************************/


/* hierarchy_to_browser_ddk(+Config, +Tree, ?Browser) <-
      */

hierarchy_to_browser_ddk(Config, Tree, Browser) :-
   var(Browser),
   new(Browser, hierarchy_browser_ddk(Config, Tree)),
   send(Browser, open),
   !.
hierarchy_to_browser_ddk(Config, Tree, Browser) :-
   send(Browser, xml, Config, Tree).


/* hierarchy_to_browser_ddk(Tree, Browser) <-
      */

hierarchy_to_browser_ddk(Tree, Browser) :-
   var(Browser),
   new(Browser, hierarchy_browser_ddk(Tree)),
   send(Browser, open),
   !.
hierarchy_to_browser_ddk(Tree, Browser) :-
   send(Browser, xml, Tree).


/* file_system_to_xml(+Directory, -XML) <-
      */

file_system_to_xml(Directory_1, XML) :-
   delete_slash_suffix(Directory_1, Directory_2),
   file_system_to_xml_(Directory_2, XML).

file_system_to_xml_(Directory, XML) :-
   get(directory(Directory), directories, Directories_1),
   chain_list(Directories_1, Directories_2),
   get(directory(Directory), files, Files_1),
   chain_list(Files_1, Files_2),
   findall( dir:[name:D_1, path:D_2]:Sub_Dirs,
      ( member(D_1, Directories_2),
        concat([Directory, '/', D_1], D_2),
        file_system_to_xml_(D_2, dir:_:Sub_Dirs) ),
      XML_Dirs ),
   findall( file:[name:File, path:Path]:[],
      ( member(File, Files_2),
        concat([Directory, '/', File], Path) ),
      XML_Files ),
   append(XML_Dirs, XML_Files, Content),
   file_base_name(Directory, Label),
   XML = dir:[name:Label, path:Directory]:Content.


/* hierarchy_browser_popup_menu(+Container, +Object, -Popup) <-
      */

hierarchy_browser_popup_menu(Container, Object, Popup) :-
   get(Object, xml, XML),
   add_popup_to_graphical(Container, XML,
      [Container, Object], Popup).


/*** implementation ***********************************************/


/* add_icon_to_hierarchy_browser(
      +Explorer, +XML_Node:prolog, +HasSub:bool, -Tuple:tuple) <-
      */

add_icon_to_hierarchy_browser(
      _E, XML_Node:prolog, _HasSub:bool, Tuple:tuple) :-
   [Open, Close] := XML_Node@[open, close],
   new(Tuple, tuple(image(Open),
      image(Close))),
   !.
add_icon_to_hierarchy_browser(
      Explorer, XML_Node:prolog, _HasSub:bool, Tuple:tuple) :-
   Tag := XML_Node@alias,
   get(Explorer, config, Config),
   icon_alias_to_icon(Config, Tag, Open, Close),
   new(Tuple, tuple(image(Open),
      image(Close))),
   !.
add_icon_to_hierarchy_browser(
      Explorer, XML_Node:prolog, _HasSub:bool, Tuple:tuple) :-
   XML_Node = Tag:_:_,
   get(Explorer, config, Config),
   icon_alias_to_icon(Config, Tag, Open, Close),
   new(Tuple, tuple(image(Open),
      image(Close))),
   !.
add_icon_to_hierarchy_browser(
      Explorer, _XML:prolog, _HasSub:bool, Tuple:tuple) :-
   get(Explorer, config, Config),
   [Open, Close] := Config^nodes@[open, close],
   new(Tuple, tuple(image(Open),
      image(Close))),
   !.
add_icon_to_hierarchy_browser(
      _E, _XML:prolog, _HasSub:bool, Tuple:tuple) :-
   new( Tuple, tuple(image('16x16/opendir.xpm'),
      image('16x16/closedir.xpm')) ).


/* icon_alias_to_icon(
      +Config, +Alias, -Open_File, -Close_File) <-
      */

icon_alias_to_icon(Config, Alias, Open_File, Close_File) :-
   [Open_Alias, Close_Alias] :=
      Config^nodes^node::[@alias=Alias]@[
         open, close],
   Open_File := Config^icons^icon::[
      @alias=Open_Alias]@file,
   Close_File := Config^icons^icon::[
      @alias=Close_Alias]@file.


/******************************************************************/


