

/******************************************************************/
/***                                                            ***/
/***      Adds XML Click Gesture and Popup Menus                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* add_popup_to_graphical(+Container, +XML, +Variables,
      -Popup) <-
      */

add_popup_to_graphical(Container, XML, Variables, Popup) :-
   xml_to_click_or_popup_alias(XML, Alias),
   catch( get(Container, config, Config), _,
      Config = config:[]:[] ),
   XML_Popup_Menu :=
      Config^mouse_clicks^mouse_click::[@alias=Alias]^popup,
   new(Popup, popup),
   xml_menu_to_xpce(Popup, Variables, XML_Popup_Menu).


/* xml_menu_to_xpce(+Popup, +Variables, +XML) <-
      */

xml_menu_to_xpce(Popup, Variables, XML) :-
   send(Popup, clear),
   forall( Menu := XML^menu,
      add_popup(Popup, Variables, Menu) ).


/* add_click_gesture_to_graphical(+Container, +Object, +XML_Object) <-
      */

add_click_gesture_to_graphical(Container, Object, XML_Object) :-
   xml_to_click_or_popup_alias(XML_Object, Alias),
   catch( get(Container, config, Config), _,
      Config = config:[]:[] ),
   ( Mouse_Click :=
        Config^mouse_clicks^mouse_click::[@alias=Alias]
   ; Mouse_Click = [] ),
   !,
   forall(
      ( On_Click := Mouse_Click^on_click,
        [Button, Type] := On_Click@[button, type],
        Args = [Container, Object, Button, Type, XML_Object] ),
      xml_click_action_to_xpce(Object, Args, On_Click) ).


/* xml_click_action_to_xpce(+Object, +Dyn_Args, +XML_On_Click) <-
      */

xml_click_action_to_xpce(Object, Dyn_Args_1, On_Click) :-
   [Pred, Button, Type, Prmtrs_1] := On_Click@[
      predicate, button, type, prmtrs],
   fn_to_value_with_default(On_Click, message, prolog, Message),
   fn_to_value_with_default(On_Click, module, user, Module),
   prmtrs_to_prmtrs_list(Prmtrs_1, Prmtrs_2),
   findall( Dyn_Arg,
      ( member(I, Prmtrs_2),
        prmtr_to_value(I, Dyn_Args_1, Dyn_Arg) ),
      Arguments ),
   append([Module, Pred], Arguments, Call),
   add_click_with_call(Object, Button, Type, Message, Call),
   !.


/*** implementation ***********************************************/


/* add_popup(+Popup, +Variables, +Item) <-
      */

add_popup(Popup, Dyn_Args_1, Item) :-
   [Pred, Name, End_Group, Prmtrs_1] := Item@[
      predicate, name, end_group, prmtrs],
   fn_to_value_with_default(Item, message, prolog, Message),
   fn_to_value_with_default(Item, module, user, Module),
   prmtrs_to_prmtrs_list(Prmtrs_1, Prmtrs_2),
   findall( Dyn_Arg,
      ( member(I, Prmtrs_2),
        prmtr_to_value(I, Dyn_Args_1, Dyn_Arg) ),
      Arguments ),
   append([Module, Pred], Arguments, Call),
   add_item_with_call(Popup, Name, Message, Call, End_Group),
   !.
add_popup(Popup_1, Dyn_Args, Menu) :-
   [Name, End_Group] := Menu@[name, end_group],
   new(Menu_Item, menu_item(Name, end_group := @End_Group)),
   send(Popup_1, append, Menu_Item),
   send(Menu_Item, popup, new(Popup_2, popup(Name))),
   Menu = _:_:Items,
   forall( member(Item, Items),
      add_popup(Popup_2, Dyn_Args, Item) ),
   !.
add_popup(Popup_1, Dyn_Args, Menu) :-
   Name := Menu@name,
   send(Popup_1, append, new(Popup_2, popup(Name))),
   Menu = _:_:Items,
   forall( member(Item, Items),
      add_popup(Popup_2, Dyn_Args, Item) ),
   !.
add_popup(_, _, '').


/* add_item_with_call(+Popup, +Name, +prolog|class,
      +Call, +End_Group) <-
      */

add_item_with_call(Popup, Name, prolog, Call, End_Group) :-
   term_to_atom(Call, Call_1),
   Call_2 =.. [message, @prolog, call_with_module, Call_1],
   new(Menu_Item, menu_item(Name, Call_2,
      end_group := @End_Group)),
   send(Popup, append, Menu_Item).
add_item_with_call(Popup, Name, class, [_, Goal, Arg|Args],
      End_Group) :-
   Call =.. [message, Arg, Goal|Args],
   new(Menu_Item, menu_item(Name, Call,
      end_group := @End_Group)),
   send(Popup, append, Menu_Item).


/* add_click_with_call(+Object, +Button, +Type, +prolog|class,
      +Call) <-
      */

add_click_with_call(Object, Button, Type, prolog, Call) :-
   term_to_atom(Call, Call_1),
   Call_2 =.. [message, @prolog, call_with_module, Call_1],
   send( Object, recogniser,
      click_gesture(Button, '', Type, Call_2) ).
add_click_with_call(Object, Button, Type, class,
      [_, Goal, Arg|Args]) :-
   Call =.. [message, Arg, Goal|Args],
   send( Object, recogniser,
      click_gesture(Button, '', Type, Call) ).


/* xml_to_click_or_popup_alias(XML_Object, Click_Alias) <-
      */

xml_to_click_or_popup_alias(XML_Object, Click_Alias) :-
   Click_Alias := XML_Object^prmtr@mouse_click,
   !.
xml_to_click_or_popup_alias(XML_Object, Click_Alias) :-
   Click_Alias := XML_Object@mouse_click,
   !.
xml_to_click_or_popup_alias(XML_Object, Click_Alias) :-
   XML_Object = Click_Alias:_:_,
   !.


/* fn_to_value_with_default(+XML, +Attr, +V_1, -V_2) <-
      */

fn_to_value_with_default(XML, Attr, _, Value) :-
   Value := XML@Attr,
   !.
fn_to_value_with_default(_, _, Value, Value) :-
   !.


/* prmtr_to_value(+@I, +Dyn_Args_1, -Dyn_Arg) <-
      */

prmtr_to_value(@I, Dyn_Args_1, Dyn_Arg) :-
   integer(I),
   nth1(I, Dyn_Args_1, Dyn_Arg),
   !.
prmtr_to_value(I, _, I) :-
   !.


/* prmtrs_to_prmtrs_list(+Prmtrs_1, -Prmtrs_2) <-
      */

prmtrs_to_prmtrs_list(Prmtrs, Prmtrs) :-
   is_list(Prmtrs),
   !.
prmtrs_to_prmtrs_list(Prmtrs_1, Prmtrs_2) :-
   term_to_atom(Prmtrs_2, Prmtrs_1).


/* call_with_module(A, B) <-
      */

call_with_module(Atom) :-
   term_to_atom([Module, Goal|Args], Atom),
   Call =.. [Goal|Args],
   call(Module:Call).


/******************************************************************/


