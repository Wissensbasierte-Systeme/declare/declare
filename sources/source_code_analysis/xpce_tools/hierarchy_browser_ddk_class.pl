

/******************************************************************/
/***                                                            ***/
/***      Hierarchy Browser: XPCE Class Methods                 ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library(pce)).
:- use_module(library(pce_toc)).

:- require([
      memberchk/2,
      append/3,
      nth1/3 ]).


/*** interface ****************************************************/


/*** class hierarchy_browser_ddk **********************************/

% orginal:
% /usr/lib/pl-5.5.18/xpce/prolog/lib/doc/xml_hierarchy.pl

:- pce_begin_class(hierarchy_browser_ddk, toc_window).


variable(xml, prolog, get).
variable(config, prolog, get).


initialise(Explorer, Config_Tree:prolog ...) :->
   ground(Config_Tree),
   ( Config_Tree = [Config, Tree]
   ; ( Config_Tree = [Tree],
       Config = config:[]:[] ) ),
   send_super(Explorer, initialise),
%  send_super(Explorer, label, 'dies ist ein test'),
%  send(Explorer, size, size(200, 300)),
   send(Explorer, slot, config, Config),
   send(Explorer, xml, Tree).


xml(Explorer, Config_Tree:prolog ...) :->
   ground(Config_Tree),
   ( ( Config_Tree = [Config, Tree],
       send(Explorer, slot, config, Config) )
   ; Config_Tree = [Tree] ),
   send(Explorer, slot, xml, Tree),
   get(Explorer, icon, Tree, @on, Icons),
   get(Explorer, caption, Tree, Caption),
   send(Explorer, root,
      my_xml_node(Caption, [], Icons, @on, Explorer)),
   send(Explorer, expand_root).


expand_node(Explorer, Node:my_xml_node) :->
   get(Node, xml, _:_:Content),
   get(Node, path, Loc0),
   append(Loc0, [Index], Loc),
   ( nth1(Index, Content, Esub),
     get(Explorer, caption, Esub, Name),
     content(Esub, SubContent),
     ( memberchk(_:_:_, SubContent) ->
       get(Explorer, icon, Esub, @on, Icons),
       send(Explorer, son, Node,
          my_xml_node(Name, Loc, Icons, @on, Explorer))
     ; get(Explorer, icon, Esub, @on, Icons),
       send(Explorer, son, Node,
          my_xml_node(Name, Loc, Icons, @off, Explorer)) ),
     fail
   ; true ).

content(_:_:Content, Content).


popup(Explorer, Node:any, Popup:popup) :<-
   not(Node = @default),
   hierarchy_to_browser_ddk:hierarchy_browser_popup_menu(
      Explorer, Node, Popup).


:- pce_group(refine).


caption(_, XML:prolog, Title:name) :<-
   ( Title := XML@name,
     !
   ; Title:_:_ = XML,
     ! ).


icon(Explorer, XML_Node:prolog, HasSub:bool, Tuple:tuple) :<-
   hierarchy_to_browser_ddk:add_icon_to_hierarchy_browser(
      Explorer, XML_Node:prolog, HasSub:bool, Tuple:tuple).


:- pce_group(path).

node_from_path(Explorer, Path:prolog, Node:my_xml_node) :<-
   get(Explorer, root, Root),
   find_node(Path, Root, Node).

find_node([], Node, Node).
find_node([N|T], Node, Sub) :-
   ( get(Node, sons, Sons),
     get(Sons, nth1, N, Sub0)
     ->  find_node(T, Sub0, Sub)
   ; send(Node, collapsed, @off),
     get(Node, sons, Sons),
     get(Sons, nth1, N, Sub0)
     ->  find_node(T, Sub0, Sub) ).


:- pce_end_class(hierarchy_browser_ddk).


/*** class my_xml_node ********************************************/


:- pce_begin_class(my_xml_node, toc_folder).

variable(path, prolog, get).


initialise(Node, Name:name, Path:prolog,
      Icons:tuple, CanExpand:bool, Explorer) :->
   get(Icons, first, Open),
   get(Icons, second, Close),
   send_super(Node, initialise, Name, @default,
      Close, Open, CanExpand),
   get(Explorer, xml, XML),
   find_xml(Path, XML, XML_Node),
   add_click_gesture_to_graphical(
      Explorer, Node, XML_Node),
   send(Node, slot, path, Path).


xml(Node, XML:prolog) :<-
   get(Node?tree, window, Window),
   get(Window, xml, Content),
   get(Node, path, Path),
   find_xml(Path, Content, XML).


find_xml([], XML, XML) :-
   !.
find_xml([X|Xs], _:_:Content, XML) :-
   nth1(X, Content, XML0),
   find_xml(Xs, XML0, XML).

/*
event(TF, Ev:event) :->
   get(Ev, name, Name),
   get(TF, xml, XML),
   dwrite(xml, XML),
   writeln(TF-Name).
*/

:- pce_end_class(my_xml_node).


/******************************************************************/


