

/******************************************************************/
/***                                                            ***/
/***     XPCE: xpce picture                                     ***/
/***                                                            ***/
/******************************************************************/


:- module( xpce_picture, [
      xpce_picture_clear/1,
      send_color_to_object/2 ] ).


/*** interface ****************************************************/


/* image_resize(Im, Factor, Scaled) <-
      */

image_resize(Im, Factor, Scaled) :-
   ( float(Factor)
   ; integer(Factor) ),
   get(Im, size, size(W, H)),
   NW is round(W * Factor),
   NH is round(H * Factor),
   get(Im, scale, size(NW, NH), Scaled).
image_resize(Im, (Width, Heigth), Scaled) :-
   ground((Width, Heigth)),
   get(Im, scale, size(Width, Heigth), Scaled).
image_resize(Im, (Width, Heigth), Scaled) :-
   var(Width),
   nonvar(Heigth),
   get(Im, size, size(W, H)),
   Factor is (Heigth/H),
   NW is round(W * Factor),
   get(Im, scale, size(NW, Heigth), Scaled).
image_resize(Im, (Width, Heigth), Scaled) :-
   nonvar(Width),
   var(Heigth),
   get(Im, size, size(W, H)),
   Factor is (Width/W),
   NH is round(H * Factor),
   get(Im, scale, size(Width, NH), Scaled).


/* send_color_to_object(Object, Colour) <-
      */

send_color_to_object(Object, Colour) :-
   functor(Colour, colour, _),
   !,
   send(Object, colour, Colour),
   send(Object, fill_pattern, Colour).
send_color_to_object(Object, Colour) :-
   ( Colour = white ->
     send(Object, colour, colour(black))
   ; send(Object, colour, colour(Colour)) ),
   send(Object, fill_pattern, colour(Colour)).


/* xpce_picture_clear(Picture) <-
      */

xpce_picture_clear(Picture) :-
   object(Picture),
   get(Picture?graphicals, find_all,
      message(@arg1, instance_of, connection),
         Connections),
   chain_list(Connections, List),
   catch(
      send(Picture?graphicals, for_all, message(@arg1, free)),
      _, fail ),
   checklist(free, List),
   gxl_presetting(head, gxl:A:_),
   send(Picture, attribute, xml, prolog(gxl:A:[])),
   send(Picture, clear),
   !.
xpce_picture_clear(_).

/*
color_weighted_edges(Config, Picture).
color_outgoing_edges(Picture, Color, Node_Id).
color_incoming_edges(Picture, Color, Node_Id).
color_call_path(Picture, Color, Node_Id).
edge_type_to_color(Config, Picture).
fade_out_edge(Picture, Edge_Id).
fade_out_light_weighted_edges(Config, Picture).
fade_out_edges(Picture, Color).
*/


/*** implementation ***********************************************/


/* picture_to_nodes(Picture, Nodes) <-
     */

picture_to_nodes(Picture, Nodes) :-
   get(Picture?graphicals, find_all,
      message(@arg1, instance_of, device),
         Devices),
   chain_list(Devices, Nodes).


/* picture_to_edges(Picture, Edges) <-
      */

picture_to_edges(Picture, Edges) :-
   picture_to_connections(Picture, Connections),
   maplist( connection_to_name,
      Connections, Edges ).

connection_to_name(Connection, Start-End) :-
   object(Connection, connection(Start, End, _, _, _)).


/* picture_to_links(Picture, Links) <-
      */

picture_to_links(Picture, Links) :-
   picture_to_connections(Picture, Connections),
   maplist( connection_to_link,
      Connections, Links ).

connection_to_link(Connection, Link) :-
   get(Connection, link, Link).


/* picture_to_connections(Picture, Connections) <-
      */

picture_to_connections(Picture, Connections) :-
   get( Picture?graphicals, find_all,
      message(@arg1, instance_of, connection),
      Cs),
   chain_list(Cs, Connections).


/******************************************************************/


