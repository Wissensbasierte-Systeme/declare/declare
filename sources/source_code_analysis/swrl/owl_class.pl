

/******************************************************************/
/***                                                            ***/
/***                  SWRL                                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


visualize_class(Picture, Address) :-
   rar:reset_gxl_database,
   get(Picture, config, Config),
   get(Picture, swrl, SWRL),
   xpce_address_to_gxl_id(Address, Id),
   gxl_id_to_owl_class(SWRL, Id, Tag:Attr:Content),
   owl_content_to_gxl(SWRL, root, (Tag:Attr:Content)),
   gxl_db_to_gxl(Gxl_1),
   gxl_graph_layout(Config, dot, [Id], Gxl_1, Gxl_2),
   Gxl_3 := Gxl_2*[^prmtr@width:1000, ^prmtr@heigth:800],
   gxl_to_picture(Config, Gxl_3, Picture_1),
   send(Picture_1, attribute, swrl, prolog(SWRL)).


/*** implementation ***********************************************/


owl_union_attribute_and_content('owl:ObjectProperty', yellow).
owl_union_attribute_and_content('owl:FunctionalProperty', orange).
owl_union_attribute_and_content('owl:subPropertyOf', green).
owl_union_attribute_and_content('owl:Class', red).
owl_union_attribute_and_content('owl:hasValue', red).

owl_replacement('owl:equivalentClass', '<=>').
owl_replacement('owl:someValuesFrom', 'some').
owl_replacement('owl:intersectionOf', 'and').
owl_replacement('owl:allValuesFrom', 'only').
owl_replacement('owl:unionOf', 'or').
owl_replacement('owl:complementOf', 'not').
owl_replacement('owl:hasValues', 'has').
owl_replacement('owl:cardinality', 'exactly').



owl_replacement(A, B) :-
   concat('owl:', B, A).
owl_replacement(A, B) :-
   concat('rdfs:', B, A).

owl_fade_out('owl:onProperty').
owl_fade_out('owl:Class').


owl_content_to_gxl(SWRL, Id_V, Element) :-
   Element = 'owl:minCardinality':_:[Number],
   gensym(minCardinality, Id_W),
   concat('>=', Number, Label),
   Node = node:[id:Id_W]:[
      prmtr:[mouse_click:sub,
         color:white, symbol:text_in_box, size:medium]:[
            string:[bubble:'']:[Label] ] ],
   owl_content_to_gxl_2(SWRL, Id_V, Id_W, Node, [], []),
   !.
owl_content_to_gxl(SWRL, Id_V, Element) :-
   Element = Tag:[]:Content,
   owl_fade_out(Tag),
   checklist( owl_content_to_gxl(SWRL, Id_V),
      Content ),
   !.
owl_content_to_gxl(SWRL, Id_V, Element) :-
   Element = Tag:Attr:Content,
   owl_union_attribute_and_content(Tag, Color),
   rdf_reference(Element, Id_W),
   exists_owl_definition(SWRL, Id_W, Symbol),
   Node = node:[id:Id_W]:[
      prmtr:[mouse_click:class_node,
         color:Color, symbol:Symbol, size:medium]:[
            string:[bubble:'']:[Id_W] ] ],
   owl_content_to_gxl_1(SWRL, Id_V, Id_W, Node, Attr, Content),
   !.
owl_content_to_gxl(SWRL, Id_V, Element) :-
   Element = Tag:Attr:Content,
   owl_replacement(Tag, Label),
   gensym(Label, Id_W),
   Node = node:[id:Id_W]:[
      prmtr:[mouse_click:sub,
         color:white, symbol:text_in_box, size:medium]:[
            string:[bubble:'']:[Label] ] ],
   owl_content_to_gxl_2(SWRL, Id_V, Id_W, Node, Attr, Content),
   !.
owl_content_to_gxl(SWRL, Id_V, Element) :-
   Element = Tag:Attr:Content,
   gensym(Tag, Id_W),
   Node = node:[id:Id_W]:[
      prmtr:[mouse_click:Tag,
         color:white, symbol:text_in_box, size:medium]:[
            string:[bubble:'']:[Tag] ] ],
   owl_content_to_gxl_2(SWRL, Id_V, Id_W, Node, Attr, Content),
   !.
owl_content_to_gxl(_, Id_V, Element) :-
   gensym(id_content, Id_W),
   term_to_atom(Element, Label),
   Node = node:[id:Id_W]:[
      prmtr:[mouse_click:content,
         color:white, symbol:circle, size:medium]:[
            string:[bubble:'']:[Label] ] ],
   rar:replace_or_insert(gxl_node, Id_W, Node),
   add_xml_edge(none, Id_V, Id_W).
owl_content_to_gxl(_, _, _).


owl_attribute_to_gxl(SWRL, Id_V, Attr:Value) :-
   class_id(Attr:Value, Id_W),
   exists_owl_definition(SWRL, Id_W, Symbol),
   Node = node:[id:Id_W]:[
      prmtr:[mouse_click:class_node,
         color:red, symbol:Symbol, size:medium]:[
            string:[bubble:'']:[Id_W] ] ],
   rar:update(gxl_node, Id_W, Node),
   add_xml_edge(none, Id_V, Id_W),
   !.
owl_attribute_to_gxl(_, Id_V, Attr:Value) :-
   gensym(Attr, Id_W),
   term_to_atom(Attr:Value, Label),
   Node = node:[id:Id_W]:[
      prmtr:[mouse_click:attribute,
         color:white, symbol:triangle, size:medium]:[
            string:[bubble:'']:[Label] ] ],
   rar:update(gxl_node, Id_W, Node),
   add_xml_edge(out_attr, Id_V, Id_W),
   !.
owl_attribute_to_gxl(_, A, B) :-
   writeln(failure:A-B).


owl_content_to_gxl_1(SWRL, Id_V, Id_W, Node,
      Attributes_1, Content) :-
   subtract(Attributes_1, ['rdf:about':_, 'rdf:resource':_,
      'rdf:ID':_], Attributes_2),
   owl_content_to_gxl_2(SWRL, Id_V, Id_W, Node,
      Attributes_2, Content).

owl_content_to_gxl_2(SWRL, Id_V, Id_W, Node,
      Attributes, Content) :-
   rar:update(gxl_node, Id_W, Node),
   checklist( owl_attribute_to_gxl(SWRL, Id_W),
      Attributes ),
   add_xml_edge(none, Id_V, Id_W),
   checklist( owl_content_to_gxl(SWRL, Id_W),
      Content ).


gxl_id_to_owl_class(SWRL, Id, Class) :-
   member(X, ['owl:Class', 'owl:ObjectProperty',
      'owl:FunctionalProperty']),
   Class := SWRL^X::[@_=Id],
   !.
gxl_id_to_owl_class(SWRL, Id_1, Class) :-
   concat(#, Id_1, Id_2),
   member(X, ['owl:Class', 'owl:ObjectProperty',
      'owl:FunctionalProperty']),
   Class := SWRL^X::[@_=Id_2],
   !.

exists_owl_definition(SWRL, Id, honeycomb) :-
   gxl_id_to_owl_class(SWRL, Id, _),
   !.
exists_owl_definition(_, _, circle).


class_id(Attr:Value, Id) :-
   member(Attr, ['rdf:about', 'rdf:resource']),
   concat(#, Id, Value),
   !.
class_id(Attr:Value, Value) :-
   member(Attr, ['rdf:ID']).



/******************************************************************/


