

/******************************************************************/
/***                                                            ***/
/***        Visualization of OWL Classes                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* owl_to_gxl(+OWL, -Gxl) <-
      */

owl_to_gxl(OWL, Gxl) :-
   rar:reset_gxl_database,
   Content := OWL^content::'*',
   checklist( add_owl_node(OWL),
      Content ),
   gxl_db_to_gxl(Gxl).


/*** implementation ***********************************************/


/* add_owl_node(+Owl, +Element) <-
      */

add_owl_node(Owl, Element) :-
   Element = 'owl:Class':_:Content,
   rdf_reference(Element, Id, Label),
   forall( member(C, Content),
      add_owl_edges(Owl, Id, blue, C) ),
   Node = node:[id:Id]:[
          prmtr:[mouse_click:class_node,
             color:red, symbol:honeycomb, size:medium]:[
             string:[]:[Label] ] ],
   rar:replace_or_insert(gxl_node, Id, Node),
   !.
add_owl_node(_, _).


/* add_owl_edges(+Owl, +Id_V, +Element) <-
      */

add_owl_edges(OWL, Id_V, Color, Element) :-
   Element = Tag:_:Content,
   member(Tag, ['owl:inverseOf','owl:ObjectProperty',
                'owl:FunctionalProperty'] ),
   checklist( add_owl_edges(OWL, Id_V, Color),
      Content ).
add_owl_edges(OWL, Id_V, Color, Element) :-
   Element = Tag:_:Content,
   member(Tag, ['owl:ObjectProperty',
                'owl:FunctionalProperty'] ),
   rdf_reference(Element, Id),
   Y := OWL^Tag::[@_=E],
   ( concat(#, Id, E)
   ; Id = E ),
   Y = _:_:C_2,
   checklist( add_owl_edges(OWL, Id_V, black),
      C_2 ),
   checklist( add_owl_edges(OWL, Id_V, Color),
      Content ),
   !.
add_owl_edges(OWL, Id_V, Color, Element) :-
   rdf_reference(Element, Id_W),
   add_owl_edges_2(Id_V-Id_W, Color),
   Element = _:_:Content,
   checklist( add_owl_edges(OWL, Id_V, Color),
      Content ),
   !.
add_owl_edges(Owl, Id_V, Color, Element) :-
   Element = _:_:Content,
   checklist( add_owl_edges(Owl, Id_V, Color),
      Content ).
add_owl_edges(_, _Id_V, _Color, _Element) :-
%  writeln(Id_V-Element),
   !.

add_owl_edges_2(Id_V-Id_W, Color) :-
   Node = node:[id:Id_W]:[
          prmtr:[mouse_click:class_node,
             color:red, symbol:circle, size:medium]:[
             string:[]:[Id_W] ] ],
   rar:update(gxl_node, Id_W, Node),
   add_xml_edge_1(second, Id_V, Id_W, Color).

add_xml_edge_1(_, Id_V, Id_W, _) :-
   rar:select(gxl_edge, Id_V, Id_W, _),
   !.
add_xml_edge_1(Arrow, Id_V, Id_W, Color) :-
   add_xml_edge(Arrow, Id_V, Id_W, Color).


/* add_owl_dependency_attribute(+Id_V, +Element) <-
      */

add_owl_dependency_attribute(Id_V, Attr:Value) :-
   rdf_reference('Class':[Attr:Value]:[], Id_W, Label),
   Node = node:[id:Id_W]:[
      prmtr:[mouse_click:dummy,
         color:yellow, symbol:text_in_ellipse, size:medium]:[
            string:[bubble:'']:[Label] ] ],
   sca_namespace_get(Current_DB),
   sca_variable_set(db, Current_DB),
   sca_namespace_set(standard),
   rar:update(gxl_node, Id_W, Node),
   sca_variable_get(db, Old_DB),
   sca_namespace_set(Old_DB),
   add_xml_edge(out, Id_V, Id_W),
   !.
add_owl_dependency_attribute(Id_V, Attr:Value) :-
   gensym(Attr, Id_W),
   term_to_atom(Attr:Value, Label),
   Node = node:[id:Id_W]:[
      prmtr:[mouse_click:attribute,
         color:white, symbol:triangle, size:medium]:[
            string:[bubble:'']:[Label] ] ],
   rar:update(gxl_node, Id_W, Node),
   add_xml_edge(out_attr, Id_V, Id_W),
   !.
add_owl_dependency_attribute(A, B) :-
   writeln(failure:A-B).


/* rdf_reference(+Element, -Id, -Label) <-
      */

rdf_reference(Element, Id, Id) :-
   rdf_reference(Element, Id),
   not( Id = '' ),
   !.

rdf_reference(Element, Id) :-
   ( Id_T := Element@'rdf:ID'
   ; Id_T := Element@'rdf:about'
   ; Id_T := Element@'rdf:resource' ),
   ( concat('#', Id, Id_T)
   ; Id = Id_T ),
   !.


/******************************************************************/


