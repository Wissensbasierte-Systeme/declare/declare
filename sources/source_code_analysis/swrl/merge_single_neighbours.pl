

/******************************************************************/
/***                                                            ***/
/***        Combine Nodes to Components                         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* merge_single_neighbours(+Gxl_1, -Gxl_2) <-
      */

merge_single_neighbours(Gxl_1, Gxl_2) :-
   gxl_to_vertices_and_edges(
      node_id-v_w, Gxl_1, Nodes_1, Edges),
   maplist( name_list,
      Nodes_1, Nodes_2 ),
   repeat_until_no_changes(Edges, Nodes_2, 0, _, Partitions, _It),
   !,
   generate_merge_configuration(name, Gxl_1, Partitions,
      Config),
   gxl_to_gxl_with_contracted_nodes(Config, Gxl_1, Gxl_2),
   !.


/*** implementation ***********************************************/


name_list(A, A:[A]).


/* repeat_until_no_changes(
      +Edges_1, +Nodes_1, +It_1, -Edges_2, -Nodes_2, -It_2) <-
            */

repeat_until_no_changes(
      Edges_1, Nodes_1, It_1, Edges_3, Nodes_3, It_3) :-
   iterate_list( concentrate,
      Nodes_1-Edges_1, Nodes_1, Nodes_2-Edges_2 ),
   list_to_ord_set(Nodes_1, Nodes_1_Ord),
   list_to_ord_set(Nodes_2, Nodes_2_Ord),
   not( Nodes_1_Ord = Nodes_2_Ord ),
   It_2 is It_1 + 1,
   repeat_until_no_changes(
      Edges_2, Nodes_2_Ord, It_2, Edges_3, Nodes_3, It_3).
repeat_until_no_changes(Edges, Nodes_1, It, Edges, Nodes_2, It) :-
   list_to_ord_set(Nodes_1, Nodes_2).

concentrate(List_of_Ns_1-Edges_1, Name:Nodes,
      [NName:Partition|List_of_Ns_3]-Edges_2) :-
   get_neighbours(Edges_1, List_of_Ns_1, Nodes, [NName:Neighbour]),
   ord_union_2(Nodes, Neighbour, Partition),
   subtract_edges_1(Edges_1, Partition, Edges_2),
   delete(List_of_Ns_1, NName:Neighbour, List_of_Ns_2),
   delete(List_of_Ns_2, Name:Nodes, List_of_Ns_3).
concentrate(Ns-Edges, _, Ns-Edges).

get_neighbours(Edges, Lists, Vs, NBs) :-
   findall( W,
      ( member(V, Vs),
        member(V-W, Edges) ),
      Nodes_A ),
   findall( W,
      ( member(V, Vs),
        member(W-V, Edges) ),
      Nodes_B ),
   ord_union_2(Nodes_A, Nodes_B, Nodes),
   findall( Name:List,
      ( member(Name:List, Lists),
        member(U, Nodes),
        member(U, List) ),
      NBs ).


/******************************************************************/


