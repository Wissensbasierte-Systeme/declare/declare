

/******************************************************************/
/***                                                            ***/
/***        Visualization of SWRL Rules                         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* swrl_to_gxl(+SWRL, -Gxl) <-
      */

swrl_to_gxl(SWRL, Gxl) :-
   rar:reset_gxl_database,
   Content := SWRL^content::'*',
   checklist( add_swrl_node(SWRL),
      Content ),
   gxl_db_to_gxl(Gxl).




/*** implementation ***********************************************/


add_swrl_node(_SWRL, Element) :-
   Element = 'swrl:Imp':_:_,
   rdf_reference(Element, Id),
   Node = node:[id:Id]:[
          prmtr:[mouse_click:swrl_rule_node,
             color:white, symbol:box, size:medium]:[
             string:[]:[] ] ],
   rar:replace_or_insert(gxl_node, Id, Node),
   Head := Element^'swrl:head',
   Body := Element^'swrl:body',
   swrl_head_to_gxl(Id, Head),
   swrl_body_to_gxl(Id, Body),
   !.
add_swrl_node(_, _).



swrl_head_to_gxl(Id, Head) :-
   Atom_List := Head^'swrl:AtomList',
   add_swrl_atom_list(in, Id, Atom_List).


swrl_body_to_gxl(Id, Body) :-
   Atom_List := Body^'swrl:AtomList',
   add_swrl_atom_list(out, Id, Atom_List).


add_swrl_atom_list(Direction, Rule, Atom_List) :-
   First := Atom_List^'rdf:first',
   add_swrl_content(Direction, Rule, First),
   Rest := Atom_List^'rdf:rest'@'rdf:resource',
   Rest = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#nil'.
add_swrl_atom_list(Direction, Rule, Atom_List) :-
   First := Atom_List^'rdf:first',
   add_swrl_content(Direction, Rule, First),
   Rest := Atom_List^'rdf:rest'^'swrl:AtomList',
   add_swrl_atom_list(Direction, Rule, Rest).


add_swrl_content(Direction, Id, XML) :-
   Content := XML^content::'*',
   forall( member(Element, Content),
      add_swrl_element(Direction, Id, Element) ).


add_swrl_element(Direction, Id, Element) :-
   Element = Tag:_:_,
   member(Tag, ['swrl:IndividualPropertyAtom',
      'swrl:argument1', 'swrl:argument2', 'swrl:Variable',
      'swrl:ClassAtom',
      'swrl:DifferentIndividualsAtom']),
   add_swrl_content(Direction, Id, Element),
   !.
add_swrl_element(Direction, Id, Element) :-
   Element = Tag:_:_,
   member(Tag, ['swrl:propertyPredicate',
   'swrl:classPredicate']),
   add_swrl_content(Direction, Id, Element),
   add_swrl_attr(Direction, Id, Element),
   !.

add_swrl_element(Direction, Id_1, Element) :-
   gensym(swrl_node, Id_2),
   Element = Label:_:_,
   Node = node:[id:Id_2]:[
      prmtr:[mouse_click:swrl_rule,
         color:white, symbol:text_in_box, size:medium]:[
            string:[bubble:'SWRL-Rule']:[Label] ] ],
   rar:update(gxl_node, Id_2, Node),
   add_swrl_attr(Direction, Id_2, Element),
   add_swrl_edge(Direction, Id_1, Id_2),
   add_swrl_content(Direction, Id_2, Element).

add_swrl_edge(out, Id_1, Id_2) :-
   Edge = edge:[id:Id_1-Id_2, from:Id_1, to:Id_2]:[
      prmtr:[mouse_click:mouse_click,
         arrows:second, color:black]:[
            string:[bubble:'']:[] ] ],
   rar:update(gxl_edge, Id_1, Id_2, Edge).
add_swrl_edge(in, Id_1, Id_2) :-
   add_swrl_edge(out, Id_2, Id_1).


add_swrl_attr(in, Id_1, Element) :-
   rdf_reference(Element, Id_2),
   Node = node:[id:Id_2]:[
          prmtr:[mouse_click:swrl_head,
          color:red, symbol:circle, size:medium]:[
          string:[]:[Id_2] ] ],
   rar:update(gxl_node, Id_2, Node),
   Edge = edge:[id:Id_2-Id_1, from:Id_2, to:Id_1]:[
          prmtr:[mouse_click:mouse_click,
          arrows:second, color:black]:[string:[]:[] ] ],
   rar:update(gxl_edge, Id_2, Id_1, Edge).
add_swrl_attr(out, Id_1, Element) :-
   rdf_reference(Element, Id_2),
   Node = node:[id:Id_2]:[
          prmtr:[mouse_click:swrl_head,
          color:red, symbol:circle, size:medium]:[
          string:[]:[Id_2] ] ],
   rar:update(gxl_node, Id_2, Node),
   Edge = edge:[id:Id_1-Id_2, from:Id_1, to:Id_2]:[
          prmtr:[mouse_click:mouse_click,
          arrows:second, color:black]:[string:[]:[] ] ],
   rar:update(gxl_edge, Id_1, Id_2, Edge).


/******************************************************************/


