

/******************************************************************/
/***                                                            ***/
/***                  Layout                                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* combine_nodes_1(+Border, +Gxl_1, -Gxl_2) <-
      */

combine_nodes_1(Border, Gxl_1, Gxl_2) :-
   gxl_to_vertices_and_edges(
      node_id-v_w, Gxl_1, Nodes, Edges),
   determine_hot_spots(Nodes, Edges, Border, Spots),
   spots_to_partitions(Nodes, Edges, Spots, Partitions),
   generate_merge_configuration(bubble, Gxl_1, Partitions,
      Config),
   gxl_to_gxl_with_contracted_nodes(Config, Gxl_1, Gxl_2).


/* combine_nodes_2(+Border, +Gxl_1, -Gxl_2) <-
      */

combine_nodes_2(Border, Gxl_1, Gxl_2) :-
   gxl_to_vertices_and_edges(
      node_id-v_w, Gxl_1, Nodes, Edges),
   border_to_partitions(Nodes, Edges, Border, Partitions),
   generate_merge_configuration(bubble, Gxl_1, Partitions,
      Config),
   gxl_to_gxl(Config, Gxl_1, Gxl_2).


/*** implementation ***********************************************/


/* border_to_partitions(+Nodes, +Edges, +Border, -Partitions) <-
      */

border_to_partitions(Nodes, Edges, Border, Partitions_2) :-
   iterate_list(border_to_partition(Border),
      Edges-[], Nodes, _-Partitions_1),
   remaining_nodes_to_components(Nodes,
      Partitions_1, Partitions_2).

border_to_partition(Border, Edges_1-P_1, Node,
      Edges_2-[Partition|P_1]) :-
   is_spot_node(Edges_1, Border, Node),
   spot_to_environment(Edges_1, Node, Partition),
   subtract_edges_2(Edges_1, Partition, Edges_2),
   !.
border_to_partition(_, Edges-Partitions, _, Edges-Partitions).


/* spots_to_partitions(+Nodes, +Edges, +Spots, -Partitions) <-
      */

spots_to_partitions(Nodes, Edges_1, Spots, Partitions_2) :-
   subtract_edges_1(Edges_1, Spots, Edges_2),
   iterate_list(spot_to_partition,
      Edges_2-[], Spots, _-Partitions_1),
   remaining_nodes_to_components(Nodes,
      Partitions_1, Partitions_2).

spot_to_partition(Edges_1-Ps, Spot, Edges_2-[Partition|Ps]) :-
   spot_to_environment(Edges_1, Spot, Partition),
   subtract_edges_2(Edges_1, Partition, Edges_2).


/* subtract_edges_1(+Edges_1, +Nodes, -Edges_2) <-
      */

subtract_edges_1(Edges_1, Nodes, Edges_2) :-
   findall( V-W,
      ( member(V, Nodes),
        member(W, Nodes),
        member(V-W, Edges_1) ),
      VWs ),
   subtract(Edges_1, VWs, Edges_2).


/* subtract_edges_2(+Edges_1, +Nodes, -Edges_2) <-
      */

subtract_edges_2(Edges_1, Nodes, Edges_2) :-
   sublist( filter_a(Nodes),
      Edges_1, Edges_2 ).

filter_a(Nodes, V-W) :-
   not( member(V, Nodes) ),
   not( member(W, Nodes) ).


/* spot_to_environment(+Edges, +Spot, -Environment) <-
      */

spot_to_environment(Edges, Spot, Environment) :-
   findall( W,
      member(Spot-W, Edges),
      Ws ),
   findall( V,
      member(V-Spot, Edges),
      Vs ),
   ord_union_2(Vs, Ws, Vs_Ws),
   list_to_ord_set([Spot|Vs_Ws], Environment).


/* remaining_nodes_to_components(+Nodes,
      +Environments_1, -Environments_2) <-
      */
remaining_nodes_to_components(Nodes,
      Environments_1, Environments_2) :-
   flatten(Environments_1, Env_Nodes),
   subtract(Nodes, Env_Nodes, Single_Nodes_1),
   maplist( flatten,
      Single_Nodes_1, Single_Nodes_2 ),
   append(Environments_1, Single_Nodes_2, Environments_2).


/* determine_hot_spots(+Nodes, +Edges, +Border, -Spots) <-
      */

determine_hot_spots(Nodes, V_W_Edges, Border, Hot_Spots) :-
   sublist( is_spot_node(V_W_Edges, Border),
      Nodes, Hot_Spots).


/* is_spot_node(+Edges, +Border, +Node) <-
      */

is_spot_node(Edges, Border, Node) :-
   node_to_edge_amount(in_out, Edges, Node, In_Out),
   In_Out >= Border.


/* node_to_edge_amount(in_out|in|out, +Edges, +Node, -Amount) <-
      */

node_to_edge_amount(in, V_W_Edges, Node, Amount) :-
   findall( W,
      member(Node-W, V_W_Edges),
      Ws ),
   length(Ws, Amount).
node_to_edge_amount(out, V_W_Edges, Node, Amount) :-
   findall( V,
      member(V-Node, V_W_Edges),
      Vs ),
   length(Vs, Amount).
node_to_edge_amount(in_out, V_W_Edges, Node, Amount) :-
   node_to_edge_amount(in, V_W_Edges, Node, In_Amount),
   node_to_edge_amount(out, V_W_Edges, Node, Out_Amount),
   Amount is In_Amount + Out_Amount.


/******************************************************************/


