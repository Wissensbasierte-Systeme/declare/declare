

/******************************************************************/
/***                                                            ***/
/***        Visualization of OWL Instances                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* owl_instances_to_gxl(+OWL, -Gxl) <-
      */

owl_instances_to_gxl(OWL, Gxl) :-
   rar:reset_gxl_database,
   Content := OWL^content::'*',
   maplist( add_owl_instance(OWL, root-root),
      Content, _ ),
   gxl_db_to_gxl(Gxl).


/*** implementation ***********************************************/


/* add_owl_node(+Owl, +V, +Element, -W) <-
      */

add_owl_instance(Owl, _V-_, Element, W-Label) :-
   Element = Tag:_Attr:Content_1,
   Element_Type := Owl^'owl:Class',
   rdf_reference(Element_Type, Tag),
   !,
   rdf_reference(Element, W),
   subtract(Content_1, [name:_:_], Content_2),
   Label := Element^name^content::'*',
   maplist( add_owl_instance(Owl, W-Label),
      Content_2, _ ),
   Node = node:[id:W]:[
          prmtr:[mouse_click:instance_node,
             color:blue, symbol:honeycomb, size:medium]:[
             string:[]:Label ] ],
   rar:replace_or_insert(gxl_node, W, Node),
   !.
add_owl_instance(Owl, V-Label, Element, edge-edge) :-
   Element = Tag:[]:Content,
   member(Type, ['owl:ObjectProperty', 'owl:FunctionalProperty']),
   Element_Type := Owl^Type,
   rdf_reference(Element_Type, Tag),
   !,
   maplist( add_owl_instance(Owl, V-Label),
      Content, Ws),
   checklist( add_instance_edge(Tag, V-Label),
      Ws ).
add_owl_instance(Owl, V-Label, Element, edge-edge) :-
   Element = Tag:_:Content,
   rdf_reference(Element, W),
   add_instance_edge(Tag, V-Label, W-['']),
   Element_Type := Owl^Type,
   member(Type, ['owl:ObjectProperty', 'owl:FunctionalProperty']),
   rdf_reference(Element_Type, Tag),
   !,
   maplist( add_owl_instance(Owl, V-Label),
      Content, Ws),
   checklist( add_instance_edge(Tag, V-Label),
      Ws ).
add_owl_instance(_, _, _, unknown-unkown).


add_instance_edge(_, root-_, _) :-
   !.
add_instance_edge(Label, V-[V1], W-[W1]) :-
   concat([V1, ' ', Label, ' ', W1], Bubble),
   term_to_atom(V-W, E_Id),
   Edge = edge:[id:E_Id, from:V, to:W]:[
      prmtr:[mouse_click:mouse_click,
         arrows:second, color:black]:[
            string:[bubble:Bubble]:[] ] ],
   rar:update(gxl_edge, V, W, Edge).


view_instance_node(Picture, Address) :-
   get(Picture, swrl, SWRL),
   xpce_address_to_gxl_id(Address, Id),
   writeln(Id),
   Id := SWRL^Path@'rdf:ID',
   path_to_class(Path, Class),
   term_to_atom(Class-11111, Id_2),
   visualize_class(Picture, @Id_2).


path_to_class(_^Path, Class) :-
   path_to_class(Path, Class).
path_to_class(Class, Class).


/******************************************************************/


