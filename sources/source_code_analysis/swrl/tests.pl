

/******************************************************************/
/***                                                            ***/
/***        Test combine/contract Nodes to Super Nodes          ***/
/***                                                            ***/
/******************************************************************/



/*** Tests ********************************************************/


test(source_code_analysis:owl, neighbours_1) :-
   Ns = [a:[a], b:[b], c:[c], d:[d], e:[e], f:[f]],
   Edges = [a-b, c-b, b-d, d-f, d-e],
   V = [a],
   get_neighbours(Edges, Ns, V, NBs),
   NBs = [b:[b]].

test(source_code_analysis:owl, neighbours_2) :-
   Ns = [b:[a, b], c:[c], d:[d], e:[e], f:[f]],
   Edges = [c-b, b-d, d-f, d-e],
   V = [a, b],
   get_neighbours(Edges, Ns, V, NBs),
   NBs = [c:[c], d:[d]].

test(source_code_analysis:owl, neighbours_3) :-
   Nodes_1 = [a:[a], b:[b], c:[c], d:[d], e:[e],
      f:[f], g:[g], h:[h], i:[i], j:[j], k:[k]],
   Edges_1 = [a-b, c-b, b-d, d-f, d-e, e-h, f-h, f-g,
   e-i, i-k, i-j, i-l],
   repeat_until_no_changes(
      Edges_1, Nodes_1, 0, Edges_2, Nodes_2, It_1),
   It_1 = 2,
   Nodes_2 = [d:[a, b, c, d], e:[e, i, j, k], f:[f, g], h:[h]],
   Edges_2 = [d-f, d-e, e-h, f-h, i-l],
   repeat_until_no_changes(
      Edges_2, Nodes_2, 0, _Edges_3, _Nodes_3, It_2),
   It_2 = 0.

test(source_code_analysis:owl, combine_1_2_3) :-
   N_1 = [
      a-[a]-honeycomb-red-medium,
      b-[b]-honeycomb-red-medium,
      c-[c]-honeycomb-red-medium,
      d-[d]-honeycomb-red-medium,
      e-[e]-honeycomb-red-medium,
      f-[f]-honeycomb-red-medium,
      g-[g]-honeycomb-red-medium,
      h-[h]-honeycomb-red-medium,
      i-[i]-honeycomb-red-medium,
      j-[j]-honeycomb-red-medium,
      k-[k]-honeycomb-red-medium,
      l-[l]-honeycomb-red-medium,
      cc-[cc]-honeycomb-red-medium],
   E = [a-b, c-b, cc-c, b-d, g-f,
      d-f, d-e, e-h, f-h,
      e-i, i-j, i-k, i-l
      ],
   alias_to_fn_triple(owl_config, Config),
   vertices_and_edges_to_gxl(N_1, E, Gxl),
   Gxl_1 := Gxl*[^prmtr@label:'Example'],
   gxl_to_picture(Config, Gxl_1, _),
   combine_nodes_1(2, Gxl_1, Gxl_4),
   test_owl_pizza_sub_2(Gxl_4, swrl:[]:[]),
   merge_single_neighbours(Gxl_1, Gxl_2),
   Gxl_2_L := Gxl_2*[^prmtr@label:'merge_single_neighbours'],
   test_owl_pizza_sub_2(Gxl_2_L, swrl:[]:[]),
   combine_nodes_2(3, Gxl_2, Gxl_3),
   test_owl_pizza_sub_2(Gxl_3, swrl:[]:[]).


/*** Further Tests ************************************************/


test_source_code_analysis(swrl, rules) :-
   sca_variable_get(sca, SCA),
   concat(SCA, 'swrl/tests/family.swrl.owl.xml', File),
   dread_(xml(dtd:[]:[]), File, SWRL),
   swrl_to_gxl(SWRL, Gxl_1),
   alias_to_fn_triple(owl_config, Gxl_Conf),
   gxl_graph_layout(Gxl_Conf, dot, _, Gxl_1, Gxl_2),
   Gxl_3 := Gxl_2*[^prmtr@label:'SWRL Rules'],
   gxl_to_missing_nodes(Gxl_3, _),
   determine_runtime(
      gxl_to_picture(Gxl_Conf, Gxl_3, Picture), RT2),
   send(Picture, attribute, swrl, prolog(SWRL)),
   writeln(displaying:RT2).


test_source_code_analysis(owl, family) :-
   test_owl_pizza_sub_1('family.swrl.owl.xml', Gxl_1, SWRL),
   Gxl_2 := Gxl_1*[^prmtr@label:'OWL Family Classes'],
   test_owl_pizza_sub_2(Gxl_2, SWRL).

test_source_code_analysis(owl, family_instance) :-
   sca_variable_get(sca, SCA),
   concat(SCA, 'swrl/tests/family.swrl.owl.xml', File),
   dread_(xml(dtd:[]:[]), File, SWRL),
   owl_instances_to_gxl(SWRL, Gxl_1),
   Gxl_2 := Gxl_1*[^prmtr@label:'OWL Family Instances'],
   test_owl_pizza_sub_2(Gxl_2, SWRL),
   gxl_to_missing_nodes(Gxl_2, _).

test_source_code_analysis(owl, pizza) :-
   test_owl_pizza_sub_1('pizza.owl.xml', Gxl_1, SWRL),
   Gxl_2 := Gxl_1*[^prmtr@label:'OWL Pizza Classes'],
   test_owl_pizza_sub_2(Gxl_2, SWRL).

test_source_code_analysis(owl, pizza_layout_1) :-
   test_owl_pizza_sub_1('pizza.owl.xml', Gxl_1, SWRL),
   Gxl_2 := Gxl_1*[^prmtr@label:'OWL Pizza Classes Layout 1'],
   combine_nodes_1(20, Gxl_2, Gxl_3),
   test_owl_pizza_sub_2(Gxl_3, SWRL).

test_source_code_analysis(owl, pizza_layout_2) :-
   test_owl_pizza_sub_1('pizza.owl.xml', Gxl_1, SWRL),
   Gxl_2 := Gxl_1*[^prmtr@label:'OWL Pizza Classes Layout 2'],
   combine_nodes_2(4, Gxl_2, Gxl_3),
   test_owl_pizza_sub_2(Gxl_3, SWRL).

test_source_code_analysis(owl, pizza_layout_3) :-
   test_owl_pizza_sub_1('pizza.owl.xml', Gxl_1, SWRL),
   Gxl_2 := Gxl_1*[^prmtr@label:'OWL Pizza Classes Layout 3'],
   merge_single_neighbours(Gxl_2, Gxl_3),
   test_owl_pizza_sub_2(Gxl_3, SWRL),
   combine_nodes_2(5, Gxl_3, Gxl_4),
   test_owl_pizza_sub_2(Gxl_4, SWRL).


/*** implementation ***********************************************/


test_owl_pizza_sub_1(File_In, Gxl, SWRL) :-
   sca_variable_get(sca, SCA),
   concat(SCA, 'swrl/tests/', File_1),
   concat(File_1, File_In, File_2),
   dread_(xml(dtd:[]:[]), File_2, SWRL),
   owl_to_gxl(SWRL, Gxl).

test_owl_pizza_sub_2(Gxl_1, SWRL) :-
   alias_to_fn_triple(owl_config, Config),
   gxl_graph_layout(config:[]:[], fdp, _, Gxl_1, Gxl_2),
   gxl_to_picture(Config, Gxl_2, Picture),
   send(Picture, attribute, swrl, prolog(SWRL)),
   xpce_graph_layout(xpce, Picture).


/******************************************************************/


