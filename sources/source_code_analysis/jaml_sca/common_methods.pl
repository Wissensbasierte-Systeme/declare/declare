

/******************************************************************/
/***                                                            ***/
/***          common methods for jaml and php                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* filter_files(+Extension, +Tree_1, -Tree_2) <-
      */

filter_files(Extension, Tag:Attr:C_1, Tag:Attr:C_3) :-
   sublist( filter_files(Extension),
      C_1, C_2 ),
   maplist( filter_files(Extension),
      C_2, C_3 ),
   !.
filter_files(_, Tree, Tree).

filter_files(Extension, file:Attr:Cont) :-
   Name := (file:Attr:Cont)@name,
   file_name_extension(_, Extension, Name).
filter_files(_, dir:_:_).


/* set_mouse_click_attributes(+Types, +Tree_1, -Tree_2) <-
      */

set_mouse_click_attributes(Types, Tag:Attr_1:Cont_1,
      Tag:Attr_2:Cont_2) :-
   memberchk(Tag:Mouse_Click, Types),
   (Tag:Attr_2:[]) := (Tag:Attr_1:[])*[@mouse_click:Mouse_Click],
   maplist( set_mouse_click_attributes(Types),
      Cont_1, Cont_2 ),
   !.
set_mouse_click_attributes(Types, Tag:Attr:Cont_1,
      Tag:Attr:Cont_2) :-
   maplist( set_mouse_click_attributes(Types),
      Cont_1, Cont_2 ),
   !.
set_mouse_click_attributes(_, Tree, Tree).


/*** implementation ***********************************************/


/******************************************************************/


