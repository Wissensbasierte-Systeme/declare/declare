

/******************************************************************/
/***                                                            ***/
/***          Java: source tree                                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* jaml_sources_to_tree(+Directory, +XML_Directory, -Tree) <-
      */

jaml_sources_to_tree(Directory, XML_Directory_1, Tree_3) :-
   file_system_to_xml(Directory, Tree_1),
   filter_files(java, Tree_1, Tree_2),
   set_mouse_click_attributes([dir:java_dir, file:java_file],
      Tree_2, Tag:Attr_1:Cont),
   add_slash_suffix(XML_Directory_1, XML_Directory_2),
%  append_classes_to_files(Tree_1, Tree_2),
   Attr_2 = [xml_root:XML_Directory_2,
      source_code:java|Attr_1],
   list_to_ord_set(Attr_2, Attr_3),
   Tree_3 = Tag:Attr_3:Cont.


/*** implementation ***********************************************/


/* append_classes_to_files(+Tree_1, -Tree_2) <-
      */

append_classes_to_files(file:Attr:C_1, file:Attr:Classes_2) :-
   File := (file:Attr:C_1)@path,
   dread_(xml, File, [Jaml|_]),
   findall( class:[name:Class_Name]:[],
      owns_cm(Jaml, Class_Name, _),
      Classes_1 ),
   list_to_ord_set(Classes_1, Classes_2),
   !.
append_classes_to_files(Tag:Attr:C_1, Tag:Attr:C_2) :-
   maplist( append_classes_to_files,
      C_1, C_2 ).


/******************************************************************/


