

/******************************************************************/
/***                                                            ***/
/***  Statistic: help predicates                                ***/
/***                                                            ***/
/******************************************************************/


:- module( mpas_rules_calls, []).


/*** interface ****************************************************/


/* table_mpa_statistics <-
      */

table_mpa_statistics :-
   new(Frame, persistent_frame),
   send(Frame, label, 'Predicate Statistics of File'),
   new(Dialog_MPA_Graph, dialog),
   send(Frame, append, Dialog_MPA_Graph),
   send(Dialog_MPA_Graph, pen, 1),
   send_list(Dialog_MPA_Graph, append,
      [ new(S1, new(S1, menu('Level', cycle))),
        button('Show Predicates in Table', and(message(@prolog,
           table_mpa_statistics_gui,
           S1?selection ))) ]),
   rar:package_names(file, Filenames),
   maplist( term_to_atom,
      Filenames, Filenames_2 ),
   send_list(S1, append, Filenames_2),
   send(Frame, open).

table_mpa_statistics_gui(File_1) :-
   term_to_atom(File_2, File_1),
   table_mpa_statistics(File_2).


/* table_mpa_statistics(+File) <-
      */

table_mpa_statistics(File) :-
   rar:file_to_directives_and_mpas(File, DMs),
   maplist( number_of_rules_calls_called(File),
      DMs, Results ),
   concat_atom(['Predicate Statistics of File: ', File], Label),
   topology:statistic_table(Label,
      ['Predicate/Directive', 'Rules',
       'From Internal', 'To Internal',
       'From External', 'To External'],
      Results).


/* table_multifile_mpas <-
      */

table_multifile_mpas :-
   really_multifile_mpas(MPA_Files, R_Lenght),
   statistics:mpas_of_type(multifile, Multi_Defs),
   findall( [MPA, File],
      ( member(MPA, Multi_Defs),
        rar:select(rule, MPA, File, _) ),
      Multi_Defs_Files ),
   append(MPA_Files, Multi_Defs_Files, Multifile_MPAs_1),
   list_to_ord_set(Multifile_MPAs_1, Multifile_MPAs_2),
   list_to_dag:list_of_lists_to_ord_dag(Multifile_MPAs_2, Tree),
   statistics:mpas_of_type(multifile, Multi_Def),
   length(Multi_Def, Length_D),
   concat_atom(['Multifile Predicates: ', Length_D,
      '; distributed Predicates: ', R_Lenght], Label),
   list_to_dag:tree_to_table(Label,
      ['Predicate', 'Rules in File'], Tree).


/* table_multifile_defined_mpas(multifile|dynamic|discontiguous) <-
      */

table_defined_mpas(Type) :-
   findall( [MPA, File],
      statistics:mpa_of_type(Type, File, MPA),
      MPA_Files_1 ),
   list_to_ord_set(MPA_Files_1, MPA_Files_2),
   list_to_dag:list_of_lists_to_ord_dag(MPA_Files_2, Tree),
   statistics:number_of_all_mpas_of_type(Type, Number),
   concat_atom(['Predicates defined as ', Type, ': ', Number], Label),
   concat_atom(['Defined as ', Type, ' in File'], Label_Column),
   list_to_dag:tree_to_table(Label,
      ['Predicate', Label_Column], Tree).


/* multilevel_mpa(+L_1:?N_1, +L_2:?N_2, -MPA) <-
      */

multilevel_mpa(L_1:N_1, L_2:N_2, MPA) :-
   findall( MPA-N_1-N_2,
      ( rar:select(rule, MPA, File_1, _),
        rar:select(rule, MPA, File_2, _),
        not( File_1 = File_2 ),
        rar:select(leaf_to_path, File_1, Element_1),
        rar:parse_element(Element_1, _, L_1, N_1),
        rar:select(leaf_to_path, File_2, Element_2),
        rar:parse_element(Element_2, _, L_2, N_2),
        not( N_1 = N_2 ) ),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   member(MPA-N_1-N_2, MPAs_2).


/*** implementation ***********************************************/


/* really_multifile_mpas(-MPAs) <-
      */

really_multifile_mpas(MPAs_4, Length) :-
   rar:package_to_mpas(file:_, MPAs_1),
   sublist( is_multifile_mpa,
      MPAs_1, MPAs_2 ),
   length(MPAs_2, Length),
   findall( [MPA, File],
      ( member(MPA, MPAs_2),
        rar:select(rule, MPA, File, _) ),
      MPAs_3 ),
   list_to_ord_set(MPAs_3, MPAs_4).


/* multifile_mpa(?MPA, ?File) <-
      */

is_multifile_mpa(MPA) :-
   rar:select(rule, MPA, File_1, _),
   rar:select(rule, MPA, File_2, _),
   not( File_1 = File_2 ).


/* number_of_rules_calls_called(+File_A, +MPA_A, -Results) <-
      */

number_of_rules_calls_called(File_A, MPA_A,
      [MPA_A_Atom, N_o_Rules, N_o_Internal_Called, N_o_Internal_Calls,
       N_o_External_Called, N_o_External_Calls]) :-
   term_to_atom(MPA_A, MPA_A_Atom),
   findall( MPA_A,
      rar:select(rule, MPA_A, File_A, _),
      Rules ),
   length(Rules, N_o_Rules),
   findall( MPA_B,
      rar:calls_pp_inv(MPA_A, File_A, MPA_B),
      Internal_Called ),
   length(Internal_Called, N_o_Internal_Called),
   findall( MPA_B,
      ( rar:select(rule, MPA_A, File_A, Rule),
        rar:calls_pp(Rule, MPA_A, MPA_B),
        rar:select(rule, MPA_B, File_A, _) ),
      Internal_Calls ),
   length(Internal_Calls, N_o_Internal_Calls),
   findall( MPA_B,
      file_mpa_is_external_called(File_A:MPA_A, _:MPA_B),
      External_Called ),
   length(External_Called, N_o_External_Called),
   findall( MPA_B,
      file_mpa_to_external_calls(File_A:MPA_A, _:MPA_B),
      External_Calls ),
   length(External_Calls, N_o_External_Calls).


/* file_mpa_is_external_called(?File_A:MPA_A, -File_B:MPA_B) <-
      */

file_mpa_is_external_called(File_A:MPA_A, File_B:MPA_B) :-
   rar:select(rule, MPA_A, File_A, _),
   rar:calls_pp_inv(MPA_A, File_B, MPA_B),
   not( File_A = File_B ).


/* file_mpa_to_external_calls(File_A:MPA_A, File_B:MPA_B) <-
      */

file_mpa_to_external_calls(File_A:MPA_A, File_B:MPA_B) :-
   rar:select(rule, MPA_A, File_A, Rule),
   rar:calls_pp(Rule, MPA_A, MPA_B),
   rar:select(rule, MPA_B, File_B, _),
   not( File_A = File_B ).


/******************************************************************/


