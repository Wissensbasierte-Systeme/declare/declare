

/******************************************************************/
/***                                                            ***/
/***         Statistics: Bar Graph                              ***/
/***                                                            ***/
/******************************************************************/


:- module( bar_graph, []).

:- dynamic temporary_fact/3.


/*** interface ****************************************************/


/* create_graph_visualize_graph(+Type, +Files, +Preds) <-
      */

create_graph_visualize_graph(Type, Files, Preds) :-
   rar:reset_gxl_database,
   create_bar_graph(Type, Files, Preds, Gxl_1),
   visur:make_some_graph_operations(Type, Gxl_1, Gxl_2),
   alias_to_fn_triple(visur_gxl_config, Gxl_Conf),
   visur:message_2,
   concat(['type: ', Type, ' statistic'], Label),
   Gxl_3 := Gxl_2*[^prmtr@label:Label],
   determine_runtime(
      gxl_to_picture(Gxl_Conf, Gxl_3, Picture), RT ),
   xpce_graph_layout(xpce, Picture),
   visur:message_3(RT).


/*** implementation ***********************************************/


/* create_bar_graph(file|module|unit, +Preds, -Gxl) <-
      */

create_bar_graph(file, Files, Preds, Gxl) :-
   checklist( add_node_to_gxl_db(file),
      Files ),
   checklist( add_edges(Files, Preds),
      Files ),
   gxl_db_to_gxl(Gxl),
   !.
create_bar_graph(Type, Files, Preds, Gxl) :-
   findall( Name,
      ( member(File, Files),
        rar:contains_ll(Type:Name, file:File) ),
      Names_1 ),
   list_to_ord_set(Names_1, Names_2),
   checklist( add_node_to_gxl_db(Type),
      Names_2 ),
   checklist( add_edges(Files, Preds, Type),
      Names_2 ),
   gxl_db_to_gxl(Gxl).


/* add_node_to_gxl_db(+Type, +Id) <-
      */

add_node_to_gxl_db(Type, Id) :-
   file_base_name(Id, BN),
   temporary_fact(Type, Id, R-M-C-N),
   concat(['Rules:', R, ', ',
           'MPAs:', M, ', ',
           'Choosen:', C, ', ',
           'Necessary:', N, ', ',
           'Name:', Id], Bubble),
   term_to_atom(Type-Type, Mouse_Click),
   ( Type = file ->
     ( Bar_0 = bar:[height:R, color:white, position:0]:[],
       Size = medium )
     ;
     ( Bar_0 = nv,
       Size = small ) ),
   Node = node:[id:Id]:[
      prmtr:[mouse_click:Mouse_Click,
         color:blue, symbol:chart, size:Size]:[
            chart:[type:v_bar_graph]:[
               Bar_0,
               bar:[height:M, color:blue, position:0]:[],
               bar:[height:C, color:red, position:1]:[],
               bar:[height:N, color:green, position:2]:[]] ,
            string:[bubble:Bubble]:[BN] ] ],
   rar:update(gxl_node, Id, Node),
   !.


/* add_edges(+Files, +Preds, +File) <-
      */

add_edges(Files, Preds, File_1) :-
   forall(
      who_calls_ff_stat(Files, Preds, File_1, File_2, Calls),
      visur:add_edge_to_gxl_db([File_1, File_2], File_1,
         File_2-[call:[from:File_1, to:File_2]:Calls],
         file_file_edge) ).


/* add_edges(+Files, +Preds, +Level_1, +Name_1) <-
      */

add_edges(Files, Preds, Level_1, Name_1) :-
   findall( Name_2-File_1-File_2:Call_List,
      ( rar:contains_ll(Level_1:Name_1, file:File_1),
        who_calls_ff_stat(Files, Preds,
           File_1, File_2, Call_List),
        rar:contains_ll(Level_1:Name_2, file:File_2) ),
      Calls ),
   findall( Name_2,
      member(Name_2-_-_:_, Calls),
      Names_2 ),
   list_to_ord_set(Names_2, Names_3),
   concat([Level_1, '_', Level_1, '_edge'], Edge_Click),
   forall( member(N, Names_3),
      ( findall( call:[from:F_1, to:F_2]:Calls_2,
           member(N-F_1-F_2:Calls_2, Calls),
           Edge_Calls ),
        visur:add_edge_to_gxl_db([Name_1, N], Name_1,
         N-Edge_Calls, Edge_Click)
      )
   ).


/* who_calls_ff_stat(+Files, +Preds, +File_1,
      -File_2, -Calls_2) <-
      */

who_calls_ff_stat(Files, Preds, File_1, File_2, Calls_2) :-
   rar:who_calls_ff(File_1, File_2, Calls_1),
   not( File_1 = File_2 ),
   member(File_2, Files),
   findall( P_1-P_2,
      ( member(P_1-P_2, Calls_1),
        member(P_1, Preds),
        member(P_2, Preds) ),
      Calls_2 ),
   not( Calls_2 = [] ).


/* statistics_tree_to_facts(+Tree) <-
      */

statistics_tree_to_facts(Tree) :-
   retractall( temporary_fact(_, _, _) ),
   statistics_tree_to_facts_sub(Tree).

statistics_tree_to_facts_sub(file:Attr:Cont) :-
   [R, M, C, N, Name] := (file:Attr:Cont)@
      [rules, mpas, choosen, necessary, path],
   assert( temporary_fact(file, Name, R-M-C-N) ),
   !.
statistics_tree_to_facts_sub(Root:Attr:Cont) :-
   [R, M, C, N, Name] := (Root:Attr:Cont)@
      [rules, mpas, choosen, necessary, name],
   assert( temporary_fact(Root, Name, R-M-C-N) ),
   forall( member(X, Cont),
      statistics_tree_to_facts_sub(X) ).


/******************************************************************/


