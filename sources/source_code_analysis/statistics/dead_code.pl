

/******************************************************************/
/***                                                            ***/
/***  Statistic: dead code in source code                       ***/
/***                                                            ***/
/******************************************************************/


:- module( dead_code, []).

:- dynamic exported_mpa/1.


/*** interface ****************************************************/


/* table_dead_code <-
      */

table_dead_code :-
   list_dead_code(Dead_Code),
   maplist( number_of_mpa_rules,
      Dead_Code, Dead_Code_with_Rules ),
   length(Dead_Code_with_Rules, L),
   atom_concat('Dead Code: ', L, Label),
   list_to_dag:list_of_lists_to_ord_dag(Dead_Code_with_Rules, Tree),
   list_to_dag:tree_to_table(Label,
      ['File', 'MPA', 'Rules', 'Called'], Tree).



/* dead_code_of_package(+Package:Name, -List) <-
      */

dead_code_of_package(Package:Name_1, File_MPAs_2) :-
   list_dead_code(Dead_Code),
   delete_slash(Name_1, Name_2),
   add_slash_suffix(Name_2, Name_3),
   findall( [Suffix, MPA],
      ( member([File, MPA], Dead_Code),
        rar:contains_ll_ext(Package:Name_2, file:File),
        atom_concat(Name_3, Suffix, File) ),
      File_MPAs_1 ),
   maplist( number_of_mpa_rules(Name_3),
      File_MPAs_1, Dead_Code_with_Rules ),
   list_to_ord_set(Dead_Code_with_Rules, File_MPAs_2).


/* table_excluded_dead_code <-
      */

table_excluded_dead_code :-
   excluded_dead_code_mpas(Excluded_MPAs),
   not_existing_but_excluded_dead_code(Not_Existing_DC),
   called_but_excluded_dead_code(Called_DC),
   maplist( generate_table(Not_Existing_DC, Called_DC),
      Excluded_MPAs, List_of_Lists ),
   length(Excluded_MPAs, No_of_Excluded_MPAs),
   length(Not_Existing_DC, No_2),
   length(Called_DC, No_3),
   concat_atom(['Excluded MPAs: ', No_of_Excluded_MPAs], Label),
   topology:statistic_table(Label,
      ['Excluded', 'Rules', 'Not Called'], List_of_Lists),
   nl,
   writeln('Not existing, but excluded Dead Code':No_2),
   writeln('Called, but excluded Code':No_3),
   nl,
   writeln('Use "sources/source_code_analysis/dead_and_undefined_code_excluded.pl" in order to fix the list of excluded code manually!').


/*** implementation ***********************************************/


/* list_dead_code(-Dead_Code) <-
      */

list_dead_code(Dead_Code_5) :-
   generate_list_of_excluded_dead_code,
   rar:package_to_mpas(file:_, MPAs),
   sublist( is_not_called_from_another_predicate,
      MPAs, Dead_Code_2 ),
   sublist( is_not_built_in,
      Dead_Code_2, Dead_Code_3 ),
   findall( [File, MPA],
      ( member(MPA, Dead_Code_3),
        rar:select(rule, MPA, File, _) ),
      Dead_Code_4 ),
   list_to_ord_set(Dead_Code_4, Dead_Code_5).

is_not_called_from_another_predicate(MPA) :-
   not( is_called_from_another_predicate(MPA) ),
   not( exported_mpa(MPA) ),
   not( dead_and_undefined_code_excluded:excluded_dead_code(MPA) ).

is_not_built_in(MPA) :-
   not( built_in:mpa(MPA) ).


/* is_called_from_another_predicate(+MPA) <-
      */

is_called_from_another_predicate(MPA_2) :-
   rar:calls_pp_inv(MPA_2, MPA_1),
   not( MPA_1 = MPA_2 ).


/* generate_list_of_excluded_dead_code <-
      */

generate_list_of_excluded_dead_code :-
   retractall( exported_mpa(_) ),
   exported_mpas(Exported_MPAs),
   forall( member(Exported_MPA, Exported_MPAs),
      assert( exported_mpa(Exported_MPA) ) ).


/* exported_mpas(-Exported_MPAs) <-
      */

exported_mpas(MPAs_2) :-
   sca_variable_get(exported_predicates, Modules),
   findall( (M:P)/A,
      ( member(Module, Modules),
        [P, A] := Module^atom@[predicate, arity],
        M := Module@name ),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2).


/* number_of_mpa_rules(+MPA, -[MPA, Rules, 0]) <-
      */

number_of_mpa_rules(Prefix, [Suffix, MPA], List) :-
   atom_concat(Prefix, Suffix, File),
   number_of_mpa_rules([File, MPA], List).

number_of_mpa_rules([File, MPA], [File, MPA, Rules, 0]) :-
   findall( MPA,
      rar:select(rule, MPA, File, _),
      MPAs ),
   length(MPAs, Rules).


/* not_existing_but_excluded_dead_code(-MPAs) <-
      */

not_existing_but_excluded_dead_code(MPAs_2) :-
   findall( DC,
      ( dead_and_undefined_code_excluded:excluded_dead_code(DC),
        not( rar:select(rule, DC, _, _) ) ),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2).


/* called_and_excluded_dead_code(-MPAs) <-
      */

called_but_excluded_dead_code(MPAs_3) :-
   excluded_dead_code_mpas(MPAs_1),
   sublist( is_called_from_another_predicate,
      MPAs_1, MPAs_2 ),
   list_to_ord_set(MPAs_2, MPAs_3).


/* excluded_dead_code_mpas(-MPAs) <-
      */

excluded_dead_code_mpas(MPAs_2) :-
   findall( DC,
      dead_and_undefined_code_excluded:excluded_dead_code(DC),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2).


/* generate_table(
      +Not_Existing_DC, +Called_DC, +MPA, -[MPA, X, Y]) <-
      */

generate_table(Not_Existing_DC, Called_DC, MPA, [MPA, X, Y]) :-
   ( member(MPA, Not_Existing_DC) ->
     X = '-'
   ; X = x ),
   ( member(MPA, Called_DC) ->
     Y = '-'
   ; Y = x ),
   !.


/******************************************************************/


