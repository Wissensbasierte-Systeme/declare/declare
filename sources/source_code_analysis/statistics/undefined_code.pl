

/******************************************************************/
/***                                                            ***/
/***  Statistic: dead code in source code                       ***/
/***                                                            ***/
/******************************************************************/


:- module( undefined_code, []).

:- dynamic temporary_fact_undefined_code/1.


/*** interface ****************************************************/


/* table_undefined_code <-
      */

table_undefined_code :-
   list_undefined_code(Undefined_MPAs),
   undefined_code_statistics(Undefined_MPAs, Statistics, L),
   atom_concat('Undefined MPAs (Statistics): ', L, Label),
   topology:statistic_table(Label,
      ['Undefined MPA', 'Rules', 'Called'],
      Statistics).


/* table_undefined_code_extended <-
      */

table_undefined_code_extended :-
   list_undefined_code(Undefined_MPAs),
   findall( [MPA_Called, File_Caller, MPA_Caller],
      ( member(MPA_Called, Undefined_MPAs),
        rar:calls_pp_inv(MPA_Called, File_Caller, MPA_Caller),
        not( MPA_Called = MPA_Caller ) ),
      MPAs ),
   list_to_dag:list_of_lists_to_ord_dag(MPAs, Tree),
   length(Undefined_MPAs, L),
   atom_concat('Undefined MPAs (Extended): ', L, Label),
   list_to_dag:tree_to_table(Label,
      ['Undefined MPA', 'Caller File', 'Caller MPA'], Tree).


/* undefined_code_of_package(+Package:Name, -File_MPAs_2) <-
      */

undefined_code_of_package(Package:Name_1, MPAs_2) :-
   list_undefined_code(Undefined_Code),
   delete_slash(Name_1, Name_2),
   findall( MPA_1,
      ( member(MPA_1, Undefined_Code),
        rar:calls_pp_inv(MPA_1, File_2, _),
        rar:contains_ll_ext(Package:Name_2, file:File_2) ),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2).


/* undefined_code_of_package_extended(
      +Package:Name, -File_MPAs_2) <-
      */

undefined_code_of_package_extended(Package:Name_1, File_MPAs_2) :-
   list_undefined_code(Undefined_Code),
   delete_slash(Name_1, Name_2),
   add_slash_suffix(Name_2, Name_3),
   findall( [MPA_1, Suffix, MPA_2],
      ( member(MPA_1, Undefined_Code),
        rar:calls_pp_inv(MPA_1, File_2, MPA_2),
        rar:contains_ll_ext(Package:Name_2, file:File_2),
        atom_concat(Name_3, Suffix, File_2) ),
      File_MPAs_1 ),
   list_to_ord_set(File_MPAs_1, File_MPAs_2).


/*** implementation ***********************************************/


/* undefined_code_statistics(+MPAs, -Statistics, -Length) <-
      */

undefined_code_statistics(Selected_MPAs, Statistics, Length) :-
   findall( [MPA_Called, 0, Calls],
      ( member(MPA_Called, Selected_MPAs),
        findall( MPA_Caller,
           ( rar:calls_pp_inv(MPA_Called, MPA_Caller),
             not( MPA_Called = MPA_Caller ) ),
           Caller_MPAs ),
        length(Caller_MPAs, Calls) ),
      MPAs ),
   list_to_ord_set(MPAs, Statistics),
   length(Statistics, Length).


/* list_undefined_code(-Undefined) <-
      */

list_undefined_code(Undefined_2) :-
   retractall( temporary_fact_undefined_code(_) ),
   statistics:mpas_of_type(dynamic, Dynamics),
   forall( member(Dynamic, Dynamics),
      assert( temporary_fact_undefined_code(Dynamic) ) ),
   findall( MPA,
      undefined_code(MPA),
      Undefined_1 ),
   list_to_ord_set(Undefined_1, Undefined_2).

undefined_code(MPA) :-
   rar:calls_pp_inv(MPA, _),
   not( rar:select(rule, MPA, _, _) ),
   not( excluded_undefined_code(MPA) ).

excluded_undefined_code(MPA) :-
   built_in:mpa(MPA),
   !.
excluded_undefined_code(MPA) :-
   temporary_fact_undefined_code(MPA),
   !.
excluded_undefined_code(MPA) :-
   dead_and_undefined_code_excluded:excluded_undefined_code(MPA, _).


/******************************************************************/


