

/******************************************************************/
/***                                                            ***/
/***                                                            ***/
/***                                                            ***/
/******************************************************************/


:- module( statistics, []).


/*** interface ****************************************************/


/* table_system_statistics <-
      */

table_system_statistics :-
%  rar:init(dislog),
   number_of_all_mpas_rules_directives_loc(MPAs, Rules,
      Directives, LoC),
   module_dependency:number_of_prolog_modules(PM),
   number_of_packages(file, Files),
   number_of_packages(unit, Units),
   number_of_packages(module, Modules),
   number_of_all_mpas_of_type(dynamic, Dyn),
   number_of_all_mpas_of_type(multifile, Multi),
   number_of_all_mpas_of_type(discontiguous, Discont),
   List_1 = [
       ['Units', Units],
       ['Modules', Modules],
       ['Files', Files],
       ['Prolog Modules', PM],
       ['Predicates', MPAs],
       ['Rules', Rules],
       ['Directives', Directives],
       ['Dynamic Defined', Dyn],
       ['Multifile Defined', Multi],
       ['Discontiguous Defined', Discont],
       ['LoC', LoC]],
   maplist( statistics_2004:statistics_of_2004,
      List_1, List_2 ),
   sca_variable_get(rar_db_created, (_, Date, _)),
   sub_atom(Date, 0, 4, _, Year),
   topology:statistic_table('System Statistics',
      ['Attribute', Year, '2004', ''], List_2),
   !.


/* table_units_mpas_rules_loc  <-
      */

table_units_mpas_rules_loc :-
   rar:init(dislog),
   rar:package_names(unit, Units),
%  Units = ['sources/source_code_analysis'],
   maplist( unit_to_mpas_rules_loc,
      Units, E ),
   iterate_list( add_lists,
      [0, 0, 0, 0, 0, 0], E, [_, _, _, LoC, _, _] ),
   maplist( percentage_of_ddk_loc(LoC),
      E, F ),
   topology:statistic_table('Unit Statistics',
      ['Unit', 'MPAs', 'Rules', 'LoC',
       'Rules/MPA', 'LoC/MPA', 'LoC of DDK'],
      F),
   !.


/*** implementation ***********************************************/


/* number_of_all_mpas_of_type(+dynamic|multifile|discontiguous,
      -Number) <-
      */

number_of_all_mpas_of_type(Type, Number) :-
   mpas_of_type(Type, MPAs),
   length(MPAs, Number).


/* number_of_all_mpas_rules_directives_loc(
      -MPAs, -Rules, -Directives, -LoC) <-
      */

number_of_all_mpas_rules_directives_loc(MPAs, Rules,
      Directives, LoC_2) :-
   file_statistics:number_of_directives_in_file(_, Directives),
   rar:file_to_rules(_, MPAs_2),
   list_to_ord_set(MPAs_2, MPAs_3),
   length(MPAs_2, Rules),
   length(MPAs_3, MPAs),
   findall( File,
      ( rar:select(rule, _, File, _) ),
       Files_1 ),
   list_to_ord_set(Files_1, Files_2),
   filename:absolute_filenames(Files_2, Absolute_Files),
   maplist( wc_lines_t,
      Absolute_Files, LoC_1 ),
   iterate_list( add,
      0, LoC_1, LoC_2 ).

wc_lines_t(A, Lines) :-
   wc_lines(A, Lines),
   !.
wc_lines_t(A, 0) :-
   writeln('File not found':A).


/* number_of_packages(+system|sources|unit|module|file, -Number_of) <-
      */

number_of_packages(Package, Number_of) :-
   rar:package_names(Package, Names),
   length(Names, Number_of).


/* unit_to_mpas_rules_loc(+Root, +Unit_1,
      -[Unit_2, MPAs, Rules, LoC, R_M, L_M]) <-
      */

unit_to_mpas_rules_loc(Unit,
      [Unit_2, MPAs, Rules, LoC, R_M, L_M]) :-
   file_base_name(Unit, Unit_2),
   findall( Rel_File,
      rar:contains_ll(unit:Unit, file:Rel_File),
      Files ),
   maplist( number_of_mpas_rules_loc_in_file,
      Files, Predicates_LoC ),
   iterate_list( add_lists,
      [x, 0, 0, 0], Predicates_LoC, [_, MPAs, Rules, LoC]),
   round(Rules/MPAs, 1, R_M),
   round(LoC/MPAs, 1, L_M).


/* percentage_of_ddk_loc(+DDK_LoC, +[A, B, C, LoC, E, F],
      -[A, B, C, LoC, E, F, DDK_3]) <-
      */

percentage_of_ddk_loc(DDK_LoC, [A, B, C, LoC, E, F],
      [A, B, C, LoC, E, F, DDK_3]) :-
   round(LoC/DDK_LoC, 3, DDK_1),
   DDK_2 is DDK_1 * 100,
   atom_concat(DDK_2, ' %', DDK_3).


/* number_of_mpas_rules_loc_in_file(
      +File, - [File, MPAs, Rules, LoC]) <-
      */

number_of_mpas_rules_loc_in_file(File, [File, MPAs, Rules, LoC]) :-
   file_statistics:
      number_of_mpas_rules_directives_loc_in_file(
      File, Result),
   Result = [File, LoC, _, _, MPAs, Rules, _, _, _, _].


/* number_of_mpas_of_package(+Level:Name, -Quantity) <-
      */

number_of_mpas_of_package(Level:Name, Quantity) :-
   rar:package_to_mpas(Level:Name, MPAs),
   length(MPAs, Quantity).


/* find_mpa_property(
      +dynamic|multifile|discontiguous, -MPA_Files) <-
      */

find_mpa_property(Flag, MPA_Files) :-
   findall( MPA-File,
      mpa_of_type(Flag, File, MPA),
      Predicates_2 ),
   list_to_ord_set(Predicates_2, MPA_Files).

find_mpa_property(Flag, Ground_Set_Files, Predicates_3) :-
   findall( MPA,
      ( member(File, Ground_Set_Files),
        mpa_of_type(Flag, File, MPA) ),
      Predicates_2 ),
   list_to_ord_set(Predicates_2, Predicates_3).


/* mpas_of_type(+dynamic|multifile|discontiguous, -MPAs) <-
      */

mpas_of_type(Type, MPAs_2) :-
   findall( MPA,
      mpa_of_type(Type, _, MPA),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2).


/* mpa_of_type(+dynamic|multifile|discontiguous, ?File, ?MPA) <-
      */

mpa_of_type(Flag, File, (M:P2)/A) :-
   rar:select(rule, (M:P)/0, File, Rule),
   atom_prefix(P, no_head),
   Atom := Rule^body^atom::[
      @module=user, @predicate=Flag, @arity=1],
   ( Term := Atom^_^term::[@functor='/']
   ; Term := Atom^term::[@functor='/'] ),
   P2 := Term-nth(1)^term@functor,
   A := Term-nth(2)^term@functor.


/******************************************************************/


/* confusing_mpas(MPAs) <-
      */

confusing_mpas(Not_Atomic_MPAs_2) :-
   rar:init(dislog),
   findall( (M:P)/A,
      ( rar:select(rule, (M:P)/A, _, _),
        not( atomic(P) ) ),
      Not_Atomic_MPAs_1 ),
   list_to_ord_set(Not_Atomic_MPAs_1, Not_Atomic_MPAs_2),
   length(Not_Atomic_MPAs_2, NA_L),
   writeln_list(Not_Atomic_MPAs_2),
   writeln(NA_L).


difference_between_dc_and_leaves_in_topology_db :-
   dead_code:list_dead_code(L),
   findall( DC_MPA,
      member([_, DC_MPA], L),
      Dead_Codes ),
   findall(Lf,
      topology_db:topology(Lf, _, leaf),
      Lfs ),
   subtract(Lfs, Dead_Codes, S),
   writeln_list(S),
   length(S, Length),
   writeln(Length),
   subtract(Dead_Codes, Lfs, S_2),
   writeln_list(['vice verca'|S_2]).


/******************************************************************/


