

/******************************************************************/
/***                                                            ***/
/***        Statistic: xml_tree_to_table                        ***/
/***                                                            ***/
/******************************************************************/


:- module( xml_tree_to_table, []).

:- use_module(library(tabular)).
:- use_module(library(autowin)).


/*** interface ****************************************************/


/* slice_statistics_table(+Tree, +Headlines, +Border) <-
      */

slice_statistics_table(Tree, Headlines, Border) :-
   length(Headlines, L),
   new(Picture, auto_sized_picture('Percentage Use of Predicates')),
   send(Picture, display, new(Table, tabular)),
   send(Table, border, 1),
   send(Table, cell_spacing, -1),
   send(Table, rules, all),
   send_list(Table, [
      append(new(graphical), rowspan := 2),
      append('Name', bold, center, colspan := L),
      append('Statistic', bold, center, colspan := 5),
      next_row ]),
   ( foreach((_, Head), Headlines) do
        send(Table, append(Head, bold, center)) ),
   send_list(Table, [
      append('LoC', bold, center),
      append('Predicates', bold, center),
      append('Choosen', bold, center),
      append('Necessary', bold, center),
      append('Percentage', bold, center) ]),
   tree_to_table(Table, Headlines, Border, Tree),
   Root := Tree@root,
   term_to_atom('root':Root, R_Atom),
   send_list(Table, [
      next_row,
      append(R_Atom, normal, left, colspan := 11)]),
   send(Picture, open),
   !.


/* file_to_slice_statistics_table(+File, +Headlines, +Border) <-
      */

file_to_slice_statistics_table(File, Headlines, Border) :-
   dread_(xml(dtd:[loc:int, choosen:int,
      mpas:int, necessary:int, rules:int]:[]), File, Tree),
   slice_statistics_table(Tree, Headlines, Border).


/*** implementation ***********************************************/


/* tree_to_table(+Table, +Headlines, +Tree) <-
      */

tree_to_table(Table, Headlines, Border, R:Attr:Cont) :-
   element_to_table(Table, Headlines, R:Attr:[], Border),
   checklist( tree_to_table(Table, Headlines, Border),
      Cont ).


/* element_to_table(+Table, +Headlines, +Tree, all|choosen) <-
      */

element_to_table(_, _, Tree, Border) :-
   [MPAs, Nec] := Tree@[mpas, necessary],
   not( MPAs = 0 ),
   Percent is (round((Nec / MPAs) * 1000)) / 10,
   Percent =< Border,
   !.
element_to_table(Table, Headlines, Root:Attr:Cont, _) :-
   length(Headlines, L),
   nth1(X, Headlines, (Root, _)),
   Name := (Root:Attr:Cont)@name,
   append_row_values(Table, L, X, Name),
   element_statistics_to_table(Table, Root:Attr:Cont).
element_to_table(_Table, _, _R:_Attr:_Cont, _).


/* append_row_values(+Table, +Lenght, +Position, +Name) <-
      */

append_row_values(Table, L, X, Name) :-
   create_value_list(L, X, Name, Values),
   send(Table, next_row),
   ( foreach(Value, Values) do
        send(Table, append(Value))).

create_value_list(L_1, X_1, Name, Values_2) :-
   L_2 is L_1 + 1,
   X_2 is X_1 + 1,
   length(Values_1, L_2),
   nth1(X_2, Values_1, Name),
   maplist( term_to_atom_sure_extended,
      Values_1, Values_2).


/* element_statistics_to_table(+Table, +Tree) <-
      */

element_statistics_to_table(Table, Tree) :-
   [LoC, MPAs, Choosen, Nec] :=
      Tree@[loc, mpas, choosen, necessary],
   not(MPAs = 0),
   Percent is (round((Nec / MPAs) * 1000)) / 10,
   get_color(Percent, Color),
   send_list(Table, [
      append(LoC),
      append(MPAs),
      append(Choosen),
      append(Nec),
      append(Percent, background := Color) ]),
   !.
element_statistics_to_table(Table, Tree) :-
   [LoC, MPAs, Choosen, Nec] :=
      Tree@[loc, mpas, choosen, necessary],
   get_color(nv, Color),
   send_list(Table, [
      append(LoC),
      append(MPAs, background := Color),
      append(Choosen),
      append(Nec),
      append(nv, background := Color) ]),
   !.

get_color(nv, light_blue) :-
   !.
get_color(Percent, red) :-
   Percent =< 33.
get_color(Percent, yellow) :-
   Percent > 33,
   Percent =< 66,
   !.
get_color(Percent, green) :-
   Percent > 66.


/******************************************************************/


