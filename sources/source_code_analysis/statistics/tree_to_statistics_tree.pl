

/******************************************************************/
/***                                                            ***/
/***         Statistics                                         ***/
/***                                                            ***/
/******************************************************************/


:- module( tree_to_statistics_tree, []).


/*** interface ****************************************************/


/* tree_to_statistics_tree(+Files, +MPAs, +Tree_1, -Tree_2) <-
      */

tree_to_statistics_tree(Ch_Files, N_MPAs, Tree_1, Tree_3) :-
   Root := Tree_1@root,
   rar:delete_file_content(Tree_1, Tree_2),
   tree_to_statistics_tree(Root, Ch_Files, N_MPAs, Tree_2, Tree_3).


/*** implementation ***********************************************/


/* tree_to_statistics_tree(
      +Root, +Ch_Files, +N_MPAs, +Tree_1, -Tree_2) <-
      */

tree_to_statistics_tree(
      Root, Ch_Files, N_MPAs, file:A:C, File_Stat) :-
   memberchk(path:Rel_File, A),
   concat(Root, Rel_File, Abs_File),
   rar:file_to_rules(Rel_File, Rules_in_File),
   wc_lines(Abs_File, LoC),
   list_to_ord_set(Rules_in_File, MPAs_in_File),
   length(Rules_in_File, Rules_in_File_Quantity),
   length(MPAs_in_File, MPAs_in_File_Quantity),
   intersection(MPAs_in_File, N_MPAs, N_MPAs_in_File),
   length(N_MPAs_in_File, Necessary),
   ( member(Rel_File, Ch_Files) ->
     Choosen = MPAs_in_File_Quantity
     ;
     Choosen = 0 ),
   File_Stat := (file:A:C)*[@loc:LoC,
                      @rules:Rules_in_File_Quantity,
                      @mpas:MPAs_in_File_Quantity,
                      @choosen:Choosen,
                      @necessary:Necessary],
   !.
tree_to_statistics_tree(
      Root, Ch_Files, N_MPAs, R:A_1:C_1, R:A_3:C_2) :-
   maplist( tree_to_statistics_tree(Root, Ch_Files, N_MPAs),
      C_1, C_2 ),
   Name := (R:A_1:C_1)@path,
   statistics:number_of_mpas_of_package(R:Name, Quantity),
   R:A_2:[] := (R:A_1:[])*[@loc:0, @mpas:Quantity,
      @rules:0, @choosen:0, @necessary:0],
   iterate_list( tree_to_statistics_tree:add_attributes,
      R:A_2:[], C_2, R:A_3:[]),
   !.


/* add_attributes(+A, +B, -C) <-
      */

add_attributes(A, B, C) :-
   A_LoC := A@loc,
   A_Preds := A@rules,
   A_Choosen := A@choosen,
   A_Necessary := A@necessary,
   B_LoC := B@loc,
   B_Preds := B@rules,
   B_Choosen := B@choosen,
   B_Necessary := B@necessary,
   C_LoC is A_LoC + B_LoC,
   C_Preds is A_Preds + B_Preds,
   C_Choosen is A_Choosen + B_Choosen,
   C_Necessary is A_Necessary + B_Necessary,
   C := A*[@loc:C_LoC,
           @rules:C_Preds,
           @choosen:C_Choosen,
           @necessary:C_Necessary].


/******************************************************************/


