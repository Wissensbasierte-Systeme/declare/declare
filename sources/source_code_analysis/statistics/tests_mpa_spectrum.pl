

/******************************************************************/
/***                                                            ***/
/***       MPA Spectrum: Tests                                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test_mpa_spectra(MPA) :-
   graph_equivalent_spectra,
   table_equivalent_spectra,
   member(MPA, [
      (basic_gxl_operations:assert_edges_to_delete)/3,
      (basic_gxl_operations:gxl_color_set)/5,
      (dda:cmdb_all)/0,
      (dda_config:extra_sourcefile_in_module)/2,
      (dda_read_source:read_token)/4,
      (dda_dislog_help:dhelp)/2 ]),
   spectrograph(MPA).


test_spec :-
   A = [[ ((user:cc)/0, 1), ((user:cu)/0, 1), ((user:dd)/0, 1),
          ((user:u)/0, 1), ((user:w)/0, 1)],
        [ ((user:cc)/0, 1), ((user:cu)/0, 1), ((user:dd)/0, 1),
          ((user:v)/0, 1), ((user:z)/0, 1)],
        [ ((user:cu)/0, 1), ((user:u)/0, 2), ((user:v)/0, 1),
          ((user:w)/0, 1)],
        [ ((user:cu)/0, 1), ((user:u)/0, 1), ((user:v)/0, 2),
          ((user:z)/0, 1)],
        [ ((user:cu)/0, 1), ((user:u)/0, 2), ((user:w)/0, 1),
          ((user:z)/0, 1)],
        [ ((user:cu)/0, 1), ((user:u)/0, 1), ((user:v)/0, 1),
          ((user:z)/0, 2)]],
   B = [[ ((user:cz)/0, 1)]],
   mpa_spectrum_db:create_spectrum([], (user:a)/0, [U, V]),
   checklist( member_spec(U),
      A ),
   checklist( member_spec(V),
      B ).

member_spec(List, El) :-
   member(El, List).


add_test :-
   S_1 = [[(u, 1), (v, 1)],
          [(u, 1), (z, 1)]],
   S_2 = [[(u, 1), (w, 1)],
          [(v, 1), (z, 1)]],
   mpa_spectrum:add_spectra(S_1, S_2, S_3),
   writeln_list(S_3).

add_test2 :-
   S_1 = [[ ((user:cu)/0, 1)]],
%    S_1 = [ ((user:cu)/0, 1)],
   S_2 = [ ((user:u)/0, 1), ((user:z)/0, 1)],
%    S_2 = [[ ((user:u)/0, 1), ((user:z)/0, 1)]],
   mpa_spectrum:add_spectra(S_1, S_2, S_3),
   writeln_list(S_3).


sttt :-
   retractall( spectrum_colored:s_temp(_, _) ),
   mpa_spectrum:create_s_picture(757, P),
   Spectrum = [[((user:fail)/0, 10)],
      [((user:fail)/0, 10)],
      [((user:fail)/0, 10)]],
   iterate_list( spectrum_colored:spectrum_colored(P),
      0, Spectrum, _ ).


/******************************************************************/



just_another_test :-
   findall( MPA,
      rar:rule(_, _, MPA, _, _, _),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   determine_runtime( abc1(MPAs_2, I) ),
   flatten(I, II),
   length(II, LI),
   writelq(LI),
   determine_runtime( abc2(MPAs_2, I2) ),
   length(I2, LI2),
   writelq(LI2),
   determine_runtime( abc3(MPAs_2, I3) ),
   length(I3, LI3),
   writelq(LI3).


abc1(MPAs, Calls) :-
   findall( Calls_2,
      (  member((M:P)/A, MPAs),
         findall( Term,
           ( term_to_atom(P, PP),
             functor(Term_1, PP, A),
             referenced(Term, M:Term_1, _) ),
            Calls_1 ),
         list_to_ord_set(Calls_1, Calls_2) ),
       Calls ).

abc2(MPAs, Calls) :-
   findall( MPA_2,
      ( member(MPA_1, MPAs),
%         rar:select(rule, MPA_1, _, MPA_2) ),
        rar:calls_pp(MPA_1, MPA_2) ),
      Calls ).

abc3(MPAs, Calls) :-
   findall( MPA_2,
      ( member(MPA_1, MPAs),
        just_another_test_2(MPA_1, MPA_2) ),
      Calls ).

just_another_test_2(A, B) :-
   hash_term(A, AH),
   just_another_test_3(AH, _, A, B).

just_another_test_4 :-
   findall( MPA,
      rar:rule(_, _, MPA, _, _, _),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   determine_runtime( cba2(MPAs_2, I) ),
   flatten(I, II),
   length(II, LI),
   writelq(LI),
   determine_runtime( cba1(MPAs_2, I2) ),
   length(I2, LI2),
   writelq(LI2).

cba1(MPAs, Calls) :-
   findall( Calls_2,
      (  member((M:P)/A, MPAs),
         term_to_atom(P, PP),
         functor(Term_1, PP, A),
         findall( Term,
            referenced(M:Term_1, Term, _),
            Calls_1 ),
         writeln(Calls_1),
         list_to_ord_set(Calls_1, Calls_2) ),
       Calls ).


cba2(MPAs, Calls) :-
   findall( MPA_2,
      ( member(MPA_1, MPAs),
        rar:calls_pp(MPA_2, MPA_1) ),
      Calls ).


:- dynamic just_another_test_3/4.

sss :-
   findall( MPA,
      rar:rule(_, _, MPA, _, _, _),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   findall( _,
      ( member(MPA_1, MPAs_2),
        rar:calls_pp(MPA_1, MPA_2),
        assert_just_another_test_5(MPA_1, MPA_2) ),
      _ ).

assert_just_another_test_5(A, B) :-
   hash_term(A, AH),
   hash_term(B, BH),
   just_another_test_3(AH, BH, A, B).
assert_just_another_test_5(A, B) :-
   hash_term(A, AH),
   hash_term(B, BH),
   assert( just_another_test_3(AH, BH, A, B) ).

/*
oder
        hash_term(MPA_1, H),
        rar:rule(H, _, MPA_1, _, R, _),
        Body := R^body,
        rar:term_to_subterm(Body, Term) ),
*/

referenced(Term, Module:Head, Ref) :-
   current_predicate(_, Module:Head),
   nth_clause(Module:Head, _, Ref),
   '$xr_member'(Ref, Term).


get_clause_locations(Ref, File, Line):-
   clause_property(Ref, file(File)),
   clause_property(Ref, line_count(Line)).



eee(SL, A, S) :-
   mpa_spectrum_db:spectrum_molecule(SL, S, A, MPAs),
   checklist( listing_ext, MPAs ).


listing_ext((_:P)/_) :-
   concat(no_head, _, P).

listing_ext((M:P)/A) :-
   listing(M:P/A).







