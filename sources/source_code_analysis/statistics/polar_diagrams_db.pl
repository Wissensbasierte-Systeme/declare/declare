

/******************************************************************/
/***                                                            ***/
/***         Statistics: Clone-Detection                        ***/
/***                                                            ***/
/******************************************************************/


:- module( polar_diagrams_db, [
      reset_polar_db/0,
      create_polar_diagram_db/0 ]).


:- dynamic mpa_calls_files/2.


/*** interface ****************************************************/


/* create_polar_diagram_db <-
      */

create_polar_diagram_db :-
   reset_polar_db,
   findall( MPA,
      rar:select(rule, MPA, _, _),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   maplist( mpa_to_mpas_slice,
      MPAs_2, MPAs ),
   maplist( mpa_to_level_file,
      MPAs, MPAs_Files ),
   checklist( assert_mpa_calls_files,
      MPAs_Files ).


/* reset_polar_db <-
      */

reset_polar_db :-
   write('reset of polar diagram facts ... '),
   determine_runtime(
      retractall( polar_diagrams_db:mpa_calls_files(_, _) ),
      RT ),
   write(RT),
   writeln(' sec. ... done.').


/*** implementation ***********************************************/


/* mpa_to_mpas_slice(+MPA, -MPA:MPAs) <-
      */

mpa_to_mpas_slice(MPA, MPA:MPAs) :-
   slice:mpas_to_necessary_mpas_and_files([MPA], MPAs, _).


/* mpa_to_level_file(+MPA:MPAs, -MPA:Files_4) <-
      */

mpa_to_level_file(MPA:MPAs, MPA:Files_3) :-
   maplist( polar_diagrams:mpa_to_level(file),
      MPAs, Files_1 ),
   flatten(Files_1, Files_2),
   multiset:multiset_normalize(Files_2, Files_3),
   !.


/* assert_mpa_calls_files(+MPA:Files) <-
      */

assert_mpa_calls_files(MPA:Files) :-
   assert( mpa_calls_files(MPA, Files) ).


/******************************************************************/


