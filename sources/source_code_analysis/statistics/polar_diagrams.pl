

/******************************************************************/
/***                                                            ***/
/***         Statistics: Clone-Detection                        ***/
/***                                                            ***/
/******************************************************************/


:- module( polar_diagrams, [
      transitive_mpa_calls_to_polar_diagram/2,
      transitive_cross_references_to_polar_diagram/2,
      cross_references_to_polar_diagram/2 ]).


/*** interface ****************************************************/


/* transitive_mpa_calls_to_polar_diagram(
      +sources|unit|module|file, ?MPA) <-
      */

transitive_mpa_calls_to_polar_diagram(Scale, MPA) :-
   findall( MPA,
      rar:select(rule, MPA, _, _),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   !,
   mpa_statistics:packages_to_multiset(
      Scale, Available_Levels),
   new(Pic, picture('Transitive Distribution of Cross References',
      size(600, 600))),
   send(Pic, open),
   member(MPA, MPAs_2),
   mpa_calls_to_polar_diagram(Pic, Scale, Available_Levels, MPA).


/* transitive_cross_references_to_polar_diagram(
      +sources|unit|module|file, +sources|unit|module|file:?Name) <-
      */

transitive_cross_references_to_polar_diagram(Scale, Level:Name) :-
   mpa_statistics:packages_to_multiset(Level, Levels_1),
   list_to_ord_set(Levels_1, Levels_2),
   !,
   mpa_statistics:packages_to_multiset(
      Scale, Available_Levels),
%  Available_Levels  = [],
   new(Pic, picture('Transitive Distribution of Cross References', size(600, 600))),
   send(Pic, open),
   member(Name:0, Levels_2),
   cross_references_to_polar_diagram(Pic, Scale,
      Available_Levels, Level:Name).


/* cross_references_to_polar_diagram(+Scale, ?Level:Name) <-
      */

cross_references_to_polar_diagram(Scale, Level:Name) :-
   mpa_statistics:packages_to_multiset(Level, Levels_1),
   list_to_ord_set(Levels_1, Levels_2),
   new(Pic, picture('Distribution of Cross References',
         size(600, 600))),
   send(Pic, open),
   !,
   member(Name:0, Levels_2),
   term_to_atom(Name, Name_Atom),
   references(outgoing, Scale, Level:Name, Multiset_O),
   references(incoming, Scale, Level:Name, Multiset_I),
   writeln('Outgoing calls (red)':Multiset_O),
   writeln('Incoming calls (green)':Multiset_I),
   mpa_statistics:packages_to_multiset(Scale, Available),
   append(Available, Multiset_O, Multiset_O_A_1),
   append(Available, Multiset_I, Multiset_I_A_1),
   multiset:multiset_normalize(Multiset_O_A_1, Multiset_O_A_2),
   multiset:multiset_normalize(Multiset_I_A_1, Multiset_I_A_2),
   concat_atom(['Level: ', Level, ', Name: ', Name_Atom,
      ', Scale: ', Scale], Title),
   maplist( polar_diagram:split_multiset,
      Multiset_O_A_2, Radial_Titles_1, Values_1 ),
   maplist( polar_diagram:split_multiset,
      Multiset_I_A_2, Radial_Titles_1, Values_2 ),
   maplist( remove_prefix('sources/'),
      Radial_Titles_1, Radial_Titles_2 ),
   polar_diagram:polar_diagram(Pic, Title, Radial_Titles_2,
      [red, green],
      [Values_1, Values_2]).


/*** implementation ***********************************************/


/* cross_references_to_polar_diagram(+Pic, +Scale,
      +Available_Levels, +Level:Name) <-
      */

cross_references_to_polar_diagram(Pic, Scale,
      Available_Levels, Level:Name) :-
   rar:package_to_mpas(Level:Name, MPAs),
   findall( Called_Files,
      ( member(MPA, MPAs),
        polar_diagrams_db:mpa_calls_files(MPA, Called_Files) ),
      Files_1 ),
   flatten(Files_1, Files_2),
   maplist( file_to_level(Scale),
      Files_2, Called_Levels ),
   append(Available_Levels, Called_Levels, Ms_3),
   multiset:multiset_normalize(Ms_3, Ms_4),
   writeln(Ms_4),
   term_to_atom(Name, Name_Atom),
   concat_atom(['Level: ', Level, ', Name: ', Name_Atom,
      ', Scale: ', Scale], Title),
   maplist( remove_prefix('sources/'),
      Ms_4, Ms_5 ),
   polar_diagram_to_picture(Pic, Title, red, Ms_5).


/* mpa_calls_to_polar_diagram(+Pic, +Scale, +Available_Levels, +MPA) <-
      */

mpa_calls_to_polar_diagram(Pic, Scale, Available_Levels, MPA) :-
   polar_diagrams_db:mpa_calls_files(MPA, Called_Files),
   !,
   maplist( file_to_level(Scale),
      Called_Files, Called_Levels ),
   mpa_calls_to_polar_diagram(Pic, Scale,
      Available_Levels, Called_Levels, MPA),
   !.
mpa_calls_to_polar_diagram(Pic, Scale, Available_Levels, MPA) :-
   slice:mpas_to_necessary_mpas_and_files([MPA], MPAs, _),
   maplist( mpa_to_level(Scale),
      MPAs, Called_Levels_1 ),
   flatten(Called_Levels_1, Called_Levels_2),
   mpa_calls_to_polar_diagram(Pic, Scale,
      Available_Levels, Called_Levels_2, MPA),
   !.

mpa_calls_to_polar_diagram(Pic, Scale, A_Levels, C_Levels, MPA) :-
   append(A_Levels, C_Levels, Ms_3),
   multiset:multiset_normalize(Ms_3, Ms_4),
   writeln(Ms_4),
   term_to_atom(MPA, MPA_Atom),
   concat_atom(['Call Distribution of ', MPA_Atom,
      ', Scale: ', Scale], Title),
   maplist( remove_prefix('sources/'),
      Ms_4, Ms_5 ),
   polar_diagram_to_picture(Pic, Title, red, Ms_5).


/* mpa_to_level(+Scale, +MPA, -Levels) <-
      */

mpa_to_level(file, MPA, Files_2) :-
   !,
   findall( MPA,
      rar:select(rule, MPA, _, _),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   !,
   member(MPA, MPAs_2),
   findall( File:1,
      rar:select(rule, MPA, File, _),
      Files_1 ),
   multiset_normalize(Files_1, Files_2).
mpa_to_level(Scale, MPA, Levels_2) :-
   findall( MPA,
      rar:select(rule, MPA, _, _),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   !,
   member(MPA, MPAs_2),
   findall( File:1,
      rar:select(rule, MPA, File, _),
      Files ),
   maplist( file_to_level(Scale),
      Files, Levels_1 ),
   multiset_normalize(Levels_1, Levels_2).

mpa_to_level_2(Scale, MPA, Levels_2) :-
   findall( Level:1,
      ( rar:select(rule, MPA, File, _),
        rar:contains_ll(Scale:Level, file:File) ),
      Levels_1 ),
   multiset_normalize(Levels_1, Levels_2),
   !.

/*
member(file:File, mpa:MPA) :-
   rar:select(rule, MPA, File, _).
member(Level:Name, mpa:MPA) :-
   ground(Level),
   not( Level = file ),
   var(MPA),
   sca_variable_get(source_tree, Tree),
   rar:contains_ll(Level:Name, file:File),
   rar:select(rule, MPA, File, _).
member(Level:Name, mpa:MPA) :-
   ground(Level),
   not( Level = file ),
   ground(MPA),
   rar:select(rule, MPA, File, _),
   sca_variable_get(source_tree, Tree),
   rar:contains_ll(Level:Name, file:File).
*/


/* file_to_level(file|module|unit, +File:Number, -Level:Number) <-
      */

file_to_level(file, File:Number, File:Number) :-
   !.
file_to_level(Scale, File:Number, Level:Number) :-
   rar:select(leaf_to_path, File, Path),
   path_to_level(Scale, Path, Level),
   !.
file_to_level(_, File:Number, Level:Number) :-
   rar:select(leaf_to_path, File, Path),
   path_to_level(sources, Path, Level),
   !.
file_to_level(_, File:Number, 'unknown level':Number) :-
   writeln('unknown level':File:Number),
   !.


/* path_to_level(+Level, +Tree, -Level_Name) <-
      */

path_to_level(Level, _:Attr:_, Name) :-
   memberchk(level:Level, Attr),
   memberchk(name:Name, Attr),
   !.
path_to_level(Level, _:_:[Content], Level_Name) :-
   path_to_level(Level, Content, Level_Name),
   !.


/* references(outgoing|incoming, +Level_2,
      +Level_1:Name_1, -Multiset) <-
      */

references(outgoing, Level_2, Level_1:Name_1, MS_3) :-
   findall( Name_2:Calls_L,
      ( rar:contains_ll(Level_1:Name_1, file:File_1),
        rar:who_calls_ff(File_1, File_2, Call_List),
        not( rar:contains_ll(Level_1:Name_1, file:File_2) ),
        length(Call_List, Calls_L),
        rar:contains_ll(Level_2:Name_2, file:File_2) ),
      MS_1 ),
   multiset:multiset_normalize(MS_1, MS_2),
   list_to_ord_set(MS_2, MS_3),
   !.
references(incoming, Level_2, Level_1:Name_1, MS_3) :-
   findall( Name_2:Calls_L,
      ( rar:contains_ll(Level_1:Name_1, file:File_1),
        rar:who_calls_ff(File_2, File_1, Call_List),
        not( rar:contains_ll(Level_1:Name_1, file:File_2) ),
        length(Call_List, Calls_L),
        rar:contains_ll(Level_2:Name_2, file:File_2) ),
      MS_1 ),
   multiset:multiset_normalize(MS_1, MS_2),
   list_to_ord_set(MS_2, MS_3),
   !.


/******************************************************************/


