

/******************************************************************/
/***                                                            ***/
/***  Statistics:                                               ***/
/***                                                            ***/
/******************************************************************/


:- module( dead_and_undefined_code_excluded,
      []).


/*** interface ****************************************************/


/* excluded_dead_code(?MPA, ?Rules) <-
      */

excluded_dead_code(MPA) :-
   excluded_dead_code(xpce, MPA).
excluded_dead_code(MPA) :-
   excluded_dead_code(sca, MPA).

excluded_dead_code(xpce, MPA) :-
   member( MPA,
      [(_:test)/_,
       (user:popup)/3,
       (user:node_from_path)/3,
       (user:initialise)/6,
       (user:expand_node)/2 ] ).
excluded_dead_code(sca, MPA) :-
   member( MPA,
      [(user:protokoll)/0,
       (user:gui_add_alias_to_file_history)/1,
       (user:gui_add_file_to_alias)/2,
       (multiset:list_to_multiset)/2,
       (user:sca_variable_delete)/2,
       (user:sca_variable_switch)/3,
       (user:sca_variables)/0,
       (user:test_sca)/0,
       (user:wc_bytes)/2,
       (user:check_atomic_ids_in_gxl)/1,
       (user:group_nodes_vertically)/1,
       (user:gxl_to_ugraph)/2,
       (user:picture_to_ugraph)/2,
       (user:picture_to_vertices_and_edges)/3,
       (user:ugraph_to_picture)/3,
       (user:alias_browser)/0,
       (user:gui_delete_alias)/2,
       (user:view_file_with_emacs)/1,
       (user:call_with_module)/1,
       (xpce_picture:picture_to_nodes)/2,
       (xpce_picture:picture_to_links)/2,
       (xpce_picture:picture_to_edges)/2,
       (user:current_date_to_string)/1,
       (user:extract_embedded_graph)/2,
       (user:show_embedded_graph)/2,
       (user:show_embedded_graph_2)/2,
       (user:show_embedded_graph_3)/2,
       (user:export_gxl_and_layout)/1,
       (user:import_gxl_and_layout)/1,
       (user:gxl_to_gxl)/4,
       (user:gxl_edge_to_picture)/4,
       (user:gxl_to_pic)/1,
       (user:gxl_to_pic)/2,
       (user:nodes_to_not_called_nodes)/3,
       (user:xpce_graph_layout)/1,
       (user:edge_weight_2)/2,
       (user:edge_weight_min_average_max)/4,
       (user:test_sub_1)/1,
       (user:test_sub_2)/2,
       (user:test_sub_3)/1,
       (user:gxl_reset_gensym_variables)/0,
       (aaa:a_dead_code_testing_mpa)/0,
       (user:test_sub_higlight_config)/2,
       (user:test_source_code_analysis)/3,
       (user:xml_tree_to_gxl)/3,
       (user:append_classes_to_files)/2,
       (user:append_classes_to_files)/2,
       (user:php_class_calls)/2,
       (rar:who_calls)/3,
       (rar:calls_ll)/2,
       (visur:visualize_java_calls)/1,
       (visur:visualize_edge_calls)/1,
       (visur:visualize_calls)/3,
       (visur:visualize_calls)/2,
       (visur:visualize_calls)/1,
       (visur:view_xml_source_code)/1,
       (visur:view_with_emacs_popup)/1,
       (visur:view_with_emacs_double_click)/1,
       (visur:view_text)/1,
       (visur:view_rule)/1,
       (visur:graph_layout)/2,
       (visur:mpas_defined_as)/0,
       (visur:view_file)/1,
       (visur:view_with_emacs)/1,
       (visur:view_with_emacs)/2,
       (visur:visur_viewer)/1,
       (visur:visur_viewer)/2,
       (visur:xia_create_graph_view_calls)/2,
       (visur:xia_create_graph_visualize_graph)/2,
       (visur:xia_cross_references_to_polar_diagram)/1,
       (visur:xia_dead_code)/1,
       (visur:xia_table_predicate_statistics)/1,
       (visur:xia_topology_to_polar_diagram)/1,
       (visur:xia_transitive_cross_references_to_polar_diagram)/1,
       (visur:xia_undefined_code)/1,
       (visur:xia_undefined_code_ext)/1,
       (visur:delete_light_weighted_edges)/1,
       (visur:delete_small_circles)/1,
       (visur:file_selection_window_gxl_graph)/1,
       (visur:picture_to_eps_file)/1,
       (visur:save_gxl_graph)/1,
       (visur:view_calls)/1,
       (visur:view_config)/1,
       (visur:view_gxl)/1,
       (visur:view_inverse_calls)/1,
       (visur:view_listing)/1,
       (visur:add_file_to_alias)/3,
       (visur:dislog_hierarchy_to_browser)/1,
       (visur:file_selection_window_load_menu)/1,
       (visur:file_selection_window_rar_db)/1,
       (visur:fill_menu_bar)/2,
       (visur:fill_tool_bar)/2,
       (visur:generate_db)/1,
       (visur:incremental_update)/1,
       (visur:initialise)/1,
       (visur:load_options)/2,
       (visur:load_rar_database)/2,
       (visur:menu_bar_actualize)/1,
       (visur:new_project)/1,
       (visur:reminder)/1,
       (visur:save_rar_database)/1,
       (visur:save_rar_database_as)/1,
       (visur:select_filesystem)/1,
       (visur:show_filesystem)/1,
       (visur:version)/1,
       (visur:visur_db_clear)/1,
       (visur:visur_gui_exit)/1,
       (visur:visur_label)/2,
       (visur:xml_hierarchy_to_browser)/1,
       (runtime_measurement:attack)/2,
       (runtime_measurement:benchmark)/1,
       (runtime_measurement:determine_runtime)/1,
       (runtime_measurement:mergesort)/2,
       (runtime_measurement:mergesort_2)/2,
       (runtime_measurement:queens)/2,
       (runtime_measurement:queensBenchmark)/0,
       (user:test_sub_1)/0,
       (visur:create_mpa_graph)/4,
       (visur:cross_reference_graph)/2,
       (visur:gui_dependency_graph)/0,
       (visur:save_xml_hierarchy_to_file)/0,
       (visur:view_visur_gui_settings)/0,
       (slice:find_possible_meta_predicates)/1,
       (slice:tar_files)/2,
       (slice:write_all_rules_to_one_file)/6,
       (visur:insert_new_project)/1,
       (visur:project_to_xml_file)/7,
       (user:test_1)/0,
       (user:test_2)/0,
       (user:test_3)/0,
       (user:test_4)/0,
       (user:test_5)/0,
       (user:add_slash)/2 ] ).


/* excluded_undefined_mpas(-MPAs) <-
      */

excluded_undefined_mpas(MPAs_2) :-
   findall( MPA,
      excluded_undefined_code(MPA, 1),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2).


/* excluded_undefined_code(?MPA, ?Calls) <-
      */

excluded_undefined_code(MPA, 1) :-
   member(MPA, [
      (user:(or))/2,
      (user:find_all)/3,
      (user:config)/2,
      (user:window)/2,
      (user:path)/2,
      (user:root)/2,
      (user:second)/2,
      (user:sons)/2,
      (user:files)/2,
      (user:directories)/2,
      (user:file)/4,
      (user:value)/2,
      (user:confirm)/2,
      (user:member)/3,
      (user:swrl)/2,
      (user:y)/2,
      (user:x)/2,
      (user:file)/3,
      (user:contains)/2,
      (user:find)/3,
      (user:string)/2,
      (user:arrows)/2,
      (user:colour)/2,
      (user:from)/2,
      (user:pen)/2,
      (user:position)/2,
      (user:device)/2,
      (user:label)/2,
      (user:member)/3,
      (user:to)/2 ] ).


/*** implementation ***********************************************/


/******************************************************************/


