

/******************************************************************/
/***                                                            ***/
/***         Statistics: Cyles                                  ***/
/***                                                            ***/
/******************************************************************/


:- module( topology, []).


/*** interface ****************************************************/


/* table_topology <-
      */

table_topology :-
   topology_db:create_topology_db_if_not_exists,
   topologic_levels_to_amount(Values),
   iterate_list(add_lists,
      [0, 0, 0], Values, [_, DC, MPAs]),
/*   reverse(Values, Values_R),
   statistic_table('Levels',
      ['Level', 'Dead Predicates', 'Predicates'],
      Values_R).*/
    statistic_table('Levels',
       ['Level', 'Dead Predicates', 'Predicates'],
       [['All', DC, MPAs]|Values]).


/* histogram_topology <-
      */

histogram_topology :-
   topology_db:create_topology_db_if_not_exists,
   topologic_levels_to_amount(Values),
   length(Values, N),
   spectra:create_s_picture(
      'Distribution of the Levels', 10*N + 60, P),
   send(P, display,
      new(Line, line(0, 0, 10*N + 30, 0))),
   send(Line, arrows, second),
   checklist( histogram(P),
      Values ).


/* topology_to_polar_diagram(+sources|unit|module|file:?Name) <-
      */

topology_to_polar_diagram(Level:Name) :-
   topology_db:create_topology_db_if_not_exists,
   mpa_statistics:packages_to_multiset(Level, Levels_1),
   list_to_ord_set(Levels_1, Levels_2),
   !,
   findall( L:0,
      topology_db:topology(_, L, _),
      Available_Levels_1 ),
   list_to_ord_set(Available_Levels_1, Available_Levels_2),
%  Available_Levels  = [],
   new(Pic, picture(
      'Distribution of the Predicates',
      size(600, 600))),
   send(Pic, open),
   member(Name:0, Levels_2),
   topology_to_polar_diagram(Pic, Available_Levels_2, Level:Name).


/* table_strong_components <-
      */

table_strong_components :-
   topology_db:create_strong_components_db_if_not_exists,
   findall( L,
      ( topology_db:strong_component(Cycle),
        length(Cycle, L) ),
      Lengths_1 ),
   length(Lengths_1, SC_Amount_1),
   list_to_ord_set(Lengths_1, Lengths_2),
   maplist( strong_components,
      Lengths_2, Values ),
   findall( MPA,
      rar:calls_pp_inv(MPA, MPA),
      Recursive_MPAs_1 ),
   list_to_ord_set(Recursive_MPAs_1, Recursive_MPAs_2),
   length(Recursive_MPAs_2, Recursive_Length),
   SC_Amount_2 is SC_Amount_1 + Recursive_Length,
   statistic_table('Strongly Connected Components',
      ['Number of Predicates in SCC', 'Number of SCC'],
      [['All', SC_Amount_2], ['1 (Recursives)', Recursive_Length]|Values]).


/*** implementation ***********************************************/


/* topology_to_polar_diagram(+Pic, +Available_Levels, +Level:Name) <-
      */

topology_to_polar_diagram(Pic, Available_Levels, Level:Name) :-
   rar:package_to_mpas(Level:Name, MPAs),
   findall( L:1,
      ( member(MPA, MPAs),
        topology_db:topology(MPA, L, _) ),
      Levels ),
   append(Available_Levels, Levels, Ms_3),
   multiset_normalize(Ms_3, Levels_Multiset),
   writeln(Levels_Multiset),
   term_to_atom(Name, Name_Atom),
   concat_atom(['Level: ', Level, ', Name: ', Name_Atom], Title),
   polar_diagram_to_picture(Pic, Title, red, Levels_Multiset).


/* topologic_levels_to_amount(-Values) <-
      */

topologic_levels_to_amount(Values) :-
   findall( Level,
      topology_db:topology(_, Level, _),
      Levels_1 ),
   list_to_ord_set(Levels_1, Levels_2),
   maplist( topology_data,
      Levels_2, Values ).


/* histogram(+Pic, +List) <-
      */

histogram(P, [X_1, YB_1, Y_1]) :-
%    YR_1 = 1,
   X_2 is (X_1*10),
   Y_2 is (Y_1 * (-1)),
%    YR_2 is (YR_1 * (-1)),
   YB_2 is (YB_1 * (-1)),
   send(P, display,
      new(_, line(X_2, 0, X_2, Y_2))),
/*   send(P, display,
      new(LineR, line(X_2, 0, X_2, YR_2))),*/
%    send(LineR, colour, red),
   send(P, display,
      new(LineB, line(X_2, 0, X_2, YB_2))),
   send(LineB, colour, blue),
   send(P, display,
      new(_, line(X_2+1, 0, X_2+1, Y_2))),
/*   send(P, display,
      new(LineR2, line(X_2+1, 0, X_2+1, YR_2))),*/
%    send(LineR2, colour, red),
   send(P, display,
      new(LineB2, line(X_2+1, 0, X_2+1, YB_2))),
   send(LineB2, colour, blue),
   !.


/* topology_data(+Level, -Statistics) <-
      */

topology_data(Level, [Level, LL, Length]) :-
   findall( (M:P)/A,
      ( topology_db:topology((M:P)/A, Level, leaf),
        atom(P),
        not( topology_db:cyclic((M:P)/A, _) ) ),
      Leaves ),
   length(Leaves, LL),
   findall( MPA,
      ( topology_db:topology(MPA, Level, _),
        not( rar:is_directive(MPA) ) ),
      MPAs ),
   length(MPAs, Length).


/* strong_components(+Length, -Statistics) <-
      */

strong_components(Number_of_Elements, [Number_of_Elements, Amount]) :-
   findall( Strong_Component,
      ( topology_db:strong_component(Strong_Component),
        length(Strong_Component, Number_of_Elements) ),
      Strong_Components ),
   length(Strong_Components, Amount).


/* statistic_table(+Title, +Columns, +Values) <-
      */

statistic_table(Title, Columns, Values) :-
   new(Picture, auto_sized_picture(Title)),
   send(Picture, display, new(Table, tabular)),
   send(Table, border, 1),
   send(Table, cell_spacing, -1),
   send(Table, rules, all),
   checklist( append_column(Table),
      Columns ),
   ( foreach(Value, Values) do
       topology:fact_to_table(Table, Value) ) ,
   send(Picture, open).

append_column(Table, Column) :-
   send_list(Table, [
      append(Column, bold, center) ]).

fact_to_table(Table, Values) :-
   send_list(Table, [
      next_row] ),
   checklist( value_to_table(Table),
      Values ).

value_to_table(Table, Value_1) :-
   term_to_atom_sure(Value_1, Value_2),
   send(Table, append(Value_2) ).


/* gui_topology_mpas <-
      */

gui_topology_mpas :-
   topology_db:create_topology_db_if_not_exists,
   new(Frame, persistent_frame),
   send(Frame, label, 'Show Predicates of Level'),
   new(Dialog_MPA_Graph, dialog),
   send(Frame, append, Dialog_MPA_Graph),
   send(Dialog_MPA_Graph, pen, 1),
   send_list(Dialog_MPA_Graph, append,
      [ new(S1, new(S1, menu('Level', cycle))),
        button('Show Predicates in Table', and(message(@prolog,
           table_mpas_of_level,
           S1?selection ))) ]),
   findall( Level,
      topology_db:topology(_, Level, _),
      Levels_1 ),
   list_to_ord_set(Levels_1, Levels_2),
   send_list(S1, append, Levels_2),
   send(Frame, open).


/* table_mpas_of_level(+Level) <-
      */

table_mpas_of_level(Level) :-
   findall( MPA,
      topology_db:topology(MPA, Level, _),
      MPAs_1 ),
   maplist( embrace_and_term_to_atom,
      MPAs_1, MPAs_2 ),
   length(MPAs_1, L),
   concat_atom(['Predicates of Level ', Level,
      ': ', L], Label),
   statistic_table(Label,
      ['Predicate', 'Called'],
      MPAs_2),
   !.

embrace_and_term_to_atom(A, [B, 'No']) :-
   topology_db:topology(A, _, leaf),
   term_to_atom(A, B),
   !.
embrace_and_term_to_atom(A, [B, 'Yes']) :-
   topology_db:topology(A, _, no_leaf),
   term_to_atom(A, B),
   !.
embrace_and_term_to_atom(A, [B, 'Excluded Dead Code']) :-
   topology_db:topology(A, _, excluded_dead_code),
   term_to_atom(A, B),
   !.
embrace_and_term_to_atom(A, [B, X]) :-
   topology_db:topology(A, _, X),
   term_to_atom(A, B).
/*
embrace_and_term_to_atom(MPA_1, [B, Number_1/Number_2]) :-
   topology_db:topology(MPA_1, _, no_leaf),
   findall( MPA_2,
      rar:calls_pp_inv(MPA_2, MPA_1),
      Calls_1 ),
   list_to_ord_set(Calls_1, Calls_2),
   length(Calls_1, Number_1),
   length(Calls_2, Number_2),
   term_to_atom(A, B),
   !.
*/


/******************************************************************/

