

/******************************************************************/
/***                                                            ***/
/***                                                            ***/
/***                                                            ***/
/******************************************************************/


:- module( file_statistics, []).

:- dynamic file_statistics/2.


/*** interface ****************************************************/


/* table_file_statistics <-
      */

table_file_statistics :-
   rar:package_names(file, Files),
   table_file_statistics(Files).


/* table_file_statistics(+Files) <-
      */

table_file_statistics(Files_1) :-
   ( foreach( File, Files_1),
     foreach( Result, Statistics_1) do
        file_statistics:
           number_of_mpas_rules_directives_loc_in_file(
           File, Result) ),
   list_to_ord_set(Statistics_1, Statistics_2),
   sublist( is_file(red),
      Statistics_2, Statistics_3 ),
   sublist( is_file(yellow),
      Statistics_2, Statistics_4 ),
   length(Statistics_3, L_DC),
   length(Statistics_4, D_DC),
   length(Files_1, FL),
   concat_atom(['File Statistics: ', 'Files: ', FL,
      '; Red Files: ', L_DC, '; Yellow Files: ', D_DC], Label),
   statistic_to_table(Label, Statistics_2).


/* reset_file_statistics_db <-
      */

reset_file_statistics_db :-
   write('reset of statistics facts ... '),
   retractall( file_statistics(_, _) ),
   writeln(' ... done.').


/*** implementation ***********************************************/


/* number_of_mpas_rules_directives_loc_in_file(
      +File, -MPAs, -Rules, -Directives, -LoC) <-
      */

number_of_mpas_rules_directives_loc_in_file(
      File, Result) :-
   file_statistics(File, Result),
   !.
number_of_mpas_rules_directives_loc_in_file(
      File, Result) :-
   ground(File),
   writeln(File),
   filename:absolute_filenames([File], [Absolute_Filename]),
   exists_file(Absolute_Filename),
   filename:relative_filename(File, Relative_Filename),
   number_of_directives_in_file(Relative_Filename, Directives),
   wc_lines(Absolute_Filename, LoC),
   rar:file_to_rules(Relative_Filename, MPAs_1),
   list_to_ord_set(MPAs_1, MPAs_2),
   number_of_called_mpas_from_external(
      Relative_Filename, Called_MPAs_from_External),
   number_of_calls_from_external(
      Relative_Filename, Calls_from_External_MPAs),
   number_of_mpas_calling_to_external(
      Relative_Filename, MPAs_Calling_to_External),
   number_of_calls_to_external(File,
      Directives_Calling_to_External, Calls_Calling_to_External),
   length(MPAs_1, Rules),
   length(MPAs_2, MPAs),
   Result = [File, LoC,
      Directives, Directives_Calling_to_External,
      MPAs, Rules,
      Called_MPAs_from_External, Calls_from_External_MPAs,
      MPAs_Calling_to_External, Calls_Calling_to_External],
   assert( file_statistics(File, Result) ),
   !.
number_of_mpas_rules_directives_loc_in_file(
      File, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) :-
   writeln('File not found':File).


/* number_of_calls_from_external(+File, -External_Calls) <-
      */

number_of_calls_from_external(File, External_Calls) :-
   file_dependency:file_called_from_external_mpas(
      File, F_MPA_MPAs),
   length(F_MPA_MPAs, External_Calls).


/* number_of_calls_to_external(+File, -Number_of_Calls) <-
      */

number_of_calls_to_external(File, Directives, Number_of_Calls) :-
   file_dependency:file_calls_external_mpas(File, F_MPA_MPAs),
   file_dependency:directive_of_file_calls_external_mpas(
      File, D),
   length(D, Directives),
   length(F_MPA_MPAs, Number_of_Calls).


/* number_of_directives(+MPAs_1, -MPAs_2, -Directives) <-
      */

number_of_directives_in_file(File, Number_of_Directives) :-
   findall( (M:P)/A,
      ( rar:select(rule, (M:P)/A, File, _),
        atomic(P),
        rar:is_directive((M:P)/A) ),
      Directives ),
   length(Directives, Number_of_Directives).


/* number_of_mpas_in_files(+Files, -Number) <-
      */

number_of_mpas_in_files(Files, Number) :-
   findall( MPAs,
      ( member(File, Files),
        rar:file_to_rules(File, MPAs) ),
      MPAs_Lists ),
   flatten(MPAs_Lists, MPAs_List),
   list_to_ord_set(MPAs_List, MPAs),
   length(MPAs, Number).


/* number_of_called_mpas_from_external(
      +File, -External_Calls) <-
      */

number_of_called_mpas_from_external(File_A, External_Calls) :-
   findall( MPA_B,
      mpas_rules_calls:file_mpa_is_external_called(
         File_A:_, _:MPA_B),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   length(MPAs_2, External_Calls).


/* number_of_mpas_calling_to_external(
      +File, -Calls_to_External) <-
      */

number_of_mpas_calling_to_external(File, External_Calls) :-
   findall( MPA,
      file_dependency:file_calls_external_mpa(File, MPA-_:_),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   length(MPAs_2, External_Calls).


/* statistic_to_table(+Title, +Statistics) <-
      */

statistic_to_table(Title, Statistics) :-
   new(Picture, auto_sized_picture(Title)),
   send(Picture, display, new(Table, tabular)),
   send(Table, border, 1),
   send(Table, cell_spacing, -1),
   send(Table, rules, all),
   send_list(Table, [
      append(new(graphical), rowspan := 3),
      append('Name', bold, center, colspan := 1),
      append('Statistics', bold, center, colspan := 11),
      next_row,
      append('File', bold, center, rowspan := 2),
      append('LoC', bold, center, rowspan := 2),
      append('Directives', bold, center, colspan := 2),
      append('MPAs', bold, center, colspan := 4),
      append('Incoming', bold, center, colspan := 2),
      append('Outgoing', bold, center, colspan := 2),
      next_row,
      append('Directives', bold, center),
      append('Calls Out', bold, center),
      append('MPAs', bold, center),
      append('Rules', bold, center),
      append('Rules/MPA', bold, center),
      append('LoC/MPA', bold, center),
      append('Called MPAs', bold, center),
      append('Calls', bold, center),
      append('MPAs Calling', bold, center),
      append('Calls', bold, center) ]),
   b_setval(counter, 0),
   ( foreach(Record, Statistics) do
       file_statistics:row_to_table(Table, Record) ),
   send(Picture, open).

row_to_table(Table,
     [File, LoC, Directives, Directives_Calling_to_External,
      MPAs, Rules,
      Called_MPAs_from_External, Calls_from_External_MPAs,
      MPAs_Calling_to_External, Calls_Calling_to_External]) :-
   get_table_color(file, Directives, Called_MPAs_from_External,
      MPAs_Calling_to_External, Color_File),
   term_to_atom(File, File_Atom),
   ( not( MPAs = 0 ) ->
     ( round(Rules/MPAs, 1, A),
       round(LoC/MPAs, 1, B) )
     ; ( A = '-',
         B = '-' ) ),
   increment_line_counter(Line),
   send_list(Table, [
      next_row,
      append(Line),
      append(File_Atom, background := Color_File),
      append(LoC, background := Color_File),
      append(Directives, background := Color_File),
      append(Directives_Calling_to_External, background := Color_File),
      append(MPAs, background := Color_File),
      append(Rules, background := Color_File),
      append(A, background := Color_File),
      append(B, background := Color_File),
      append(Called_MPAs_from_External, background := Color_File),
      append(Calls_from_External_MPAs, background := Color_File),
      append(MPAs_Calling_to_External, background := Color_File),
      append(Calls_Calling_to_External, background := Color_File)
      ] ),
   !.


/* increment_line_counter(-Counter) <-
      */

increment_line_counter(Counter_2) :-
   b_getval(counter, Counter_1),
   Counter_2 is Counter_1 + 1,
   b_setval(counter, Counter_2).


/* get_table_color(Mode, +Directives, +Calls_From_Ext, -Color) <-
      */

get_table_color(file, 0, 0, 0, red) :-
   !.
get_table_color(file, _, 0, 0, yellow) :-
   !.
get_table_color(file, _, _, _, white) :-
   !.


/* is_file(+Color, +List) <-
      */

is_file(red, [_, _, 0, _, _, _, 0, _, 0, _]).
is_file(yellow, [_, X, _, _, _, _, 0, _, 0, _]) :-
   X > 0.


/******************************************************************/


