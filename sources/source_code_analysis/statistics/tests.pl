

/******************************************************************/
/***                                                            ***/
/***       Statistic: Tests                                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(source_code_analysis:statistics, slice_graphs) :-
   slice_test_sub,
   Slice =
      slice:[multi:false, dyn:false, discont:false,
         twins:false, current_num:false,
         directives:true, tests:false]:[
      predicates:[type:include]:[
         atom:[module:user,
               predicate:a, arity:0]:[] ],
      predicates:[type:exclude]:[
         atom:[module:user,
               predicate:b, arity:0]:[] ],
      predicates:[type:dynamic]:[],
      predicates:[type:multifile]:[],
      predicates:[type:discontiguous]:[],
      files:[type:include]:[],
      files:[type:libraries]:[],
      files:[type:exclude]:[] ],
   measure_runtime(slice:test_2,
      slice:slice(Slice, Files, Preds) ),
   slice_test_output_1(Files, Preds),
   sca_variable_get(source_tree, Tree_1),
   rar:delete_file_content(Tree_1, Tree_2),
   tree_to_statistics_tree:tree_to_statistics_tree(Files, Preds,
      Tree_2, Tree_3),
   xml_tree_to_table:slice_statistics_table(Tree_3, [
        (system, 'System'),
        (sources, 'Sources'),
        (unit, 'Unit'),
        (module, 'Module'),
        (file, 'File') ], 0),
   bar_graph:statistics_tree_to_facts(Tree_3),
   bar_graph:create_graph_visualize_graph(
      file, Files, Preds),
   !.


example(dislog_predicates_and_loc) :-
   rar:init(dislog),
   sca_variable_get(source_tree, Tree_1),
   tree_to_statistics_tree:tree_to_statistics_tree([], [], Tree_1, Tree_2),
   xml_tree_to_table:slice_statistics_table(Tree_2, [
        (system, 'System'),
        (sources, 'Sources'),
        (unit, 'Unit'),
        (module, 'Module'),
        (file, 'File') ], -1),
   !.

test(source_code_analysis:statistics, dead_code_sca) :-
   dead_code:dead_code_of_package(
      unit:'sources/source_code_analysis', List_of_Lists),
   list_to_dag:list_of_lists_to_ord_dag(List_of_Lists, Tree),
   length(List_of_Lists, L),
   atom_concat('Dead Code of Package SCA: ', L, Label),
   list_to_dag:tree_to_table(Label,
      ['File', 'MPA', 'Rules', 'Called'], Tree),
   maplist( transform,
      List_of_Lists, List_of_Lists_1 ),
   writeln_list(List_of_Lists_1),
   writeln('Length':L),
   !.

transform([_, A, _, _], C):-
   term_to_atom(A, B),
   concat_atom(['       ', B, ','], C).

test(source_code_analysis:statistics, undefined_code_sca) :-
   undefined_code:undefined_code_of_package(
      unit:'sources/source_code_analysis', List),
   undefined_code:undefined_code_statistics(List, Statistics, L),
   atom_concat('Undefined Code of Package SCA (Statistics): ', L,
      Label_Stat),
   list_to_dag:list_of_lists_to_ord_dag(Statistics, Tree_Stat),
   list_to_dag:tree_to_table(Label_Stat,
      ['Undefined MPA', 'Rules', 'Called'], Tree_Stat),

   undefined_code:undefined_code_of_package_extended(
      unit:'sources/source_code_analysis', List_of_Lists),
   atom_concat('Undefined Code of Package SCA (Extended): ', L,
      Label_Ext),
   list_to_dag:list_of_lists_to_ord_dag(List_of_Lists, Tree_Extendend),
   list_to_dag:tree_to_table(Label_Ext,
      ['Undefined MPA', 'Caller File', 'Caller MPA'], Tree_Extendend),
   !.


/*** implementation ***********************************************/


/* slice_test_sub <-
      */

slice_test_sub :-
   dislog_variable_get(sca, SCA),
   !,
   concat(SCA, 'slice/', Slice_Path),
   maplist( concat('tests/'),
      ['a.pl', 'b.pl', 'c.pl', 'd.pl', 'e.pl'],
      [File_1, File_2, File_3, File_4, File_5] ),
   Tree =
      unit:[source_code:prolog, path:dislog, name:rar,
            root:Slice_Path]:[
         module:[name:test_1, path:'dislog/test_1']:[
            file:[name:File_1, path:File_1]:[]],
         module:[name:test_2, path:'dislog/test_2']:[
            file:[name:File_2, path:File_2]:[],
            file:[name:File_3, path:File_3]:[],
            file:[name:File_4, path:File_4]:[],
            file:[name:File_5, path:File_5]:[] ] ],
   rar:parse_sources_to_rar_database(Tree, _).


/* slice_test_output_1(+Files, Preds) <-
      */

slice_test_output_1(Files, Preds) :-
   nl,
   writeln('Calculating some statistics parameters:'),
   sca_variable_set(statistics_files, Files),
   sca_variable_set(statistics_preds, Preds),
   sca_variable_get(source_tree, Tree_1),
   rar:delete_file_content(Tree_1, Tree_2),
   tree_to_statistics_tree:tree_to_statistics_tree(Files, Preds,
      Tree_2, Tree_3),
   sca_variable_set(statistics_tree, Tree_3),
%  dwrite(xml_2, 'tree_stat.xml', Tree_3),
   length(Preds, N),
   file_statistics:number_of_mpas_in_files(Files, Amount),
   length(Files, M),
   write('Files':M),
   write(', Predicates':Amount),
   writeln(', thereof necessary Predicates':N),
   nl.


/******************************************************************/


