

/******************************************************************/
/***                                                            ***/
/***         Statistics: Cyles                                  ***/
/***                                                            ***/
/******************************************************************/


:- module( topology_db, []).

:- dynamic topology/3,
           strong_component/1,
           cyclic/2.


/*** interface ****************************************************/


/* topology_db <-
      */

topology_db :-
   topology_db:create_strong_components_db_if_not_exists,
   writeln('generating topology ... '),
   determine_runtime(
      ( topology_db:get_start_predicates(MPAs),
        topology_db:topo(MPAs, 1)
      ) ),
   writeln(' sec. ... done.').


/* reset_topology_db <-
      */

reset_topology_db :-
   write('reset of topology facts ... '),
   retractall( topology_db:topology(_, _, _) ),
   writeln(' ... done.').


/* reset_scc_db <-
      */

reset_scc_db :-
   write('reset of scc facts ... '),
   retractall( topology_db:strong_component(_) ),
   retractall( topology_db:cyclic(_, _) ),
   writeln(' ... done.').


/* load_topology_facts(+File) <-
      */

load_topology_facts(File_1) :-
   tilde_to_home_path(File_1, File_2),
   reset_topology_db,
   open(File_2, read, Stream),
   read(Stream, T0),
   asserted_stream_content(T0, Stream),
   close(Stream).


/* save_topology_facts(File_1) <-
      */

save_topology_facts(File_1) :-
   tilde_to_home_path(File_1, File_2),
   predicate_to_file( File_2,
      topology_db:write_topology_facts ),
   !.


/*** implementation ***********************************************/


/* get_start_predicates(-MPAs) <-
      */

get_start_predicates(MPAs_4) :-
   rar:file_to_rules(_, MPAs_1),
   list_to_ord_set(MPAs_1, MPAs_2),
   findall( X,
      ( rar:calls_pp_inv(MPA, _, X),
        rar:select(rule, MPA, _, _),
        not( X = MPA ),
        not( ( topology_db:cyclic(X, SCC),
                topology_db:cyclic(MPA, SCC) ) ) ),
      Called_1 ),
   list_to_ord_set(Called_1, Called_2),
   subtract(MPAs_2, Called_2, MPAs_3),
   list_to_ord_set(MPAs_3, MPAs_4),
   !.


/* topo(+MPAs, +Level) <-
      */

topo([], _) :-
   !.
topo(MPAs, Level_1) :-
   length(MPAs, L),
   write_list(['Checking about ', L,
      ' possible MPAs of topological Level ', Level_1, '.\n']),
   maplist( create_topology(Level_1),
      MPAs, MPAs_Out_1 ),
   flatten(MPAs_Out_1, MPAs_Out_2),
   list_to_ord_set(MPAs_Out_2, MPAs_Out_3),
   findall( X,
      ( member(MPA, MPAs_Out_3),
        rar:calls_pp_inv(MPA, _, X),
        not( topology_db:cyclic(X, _) ),
        not( X = MPA ) ),
      Called ),
   subtract(MPAs_Out_3, Called, MPAs_Out_4),
   Level_2 is Level_1 + 1,
   topo(MPAs_Out_4, Level_2).


/* create_topology(+MPA, +List, +Level) <-
      */

create_topology(Level, MPA, []) :-
   topology(MPA, Level, _),
   !.
create_topology(Level, MPA, MPAs_3) :-
   topology_db:cyclic(MPA, SCC),
   findall( MPA_B,
      ( topology_db:cyclic(MPA_A, SCC),
        call_dependency(MPA_A, MPA_B),
        not( topology_db:cyclic(MPA_B, SCC) ) ),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_3),
   findall( MPA_SCC,
      topology_db:cyclic(MPA_SCC, SCC),
      Strong_Component ),
   checklist( assert_topology(MPAs_3, Level),
      Strong_Component ),
   !.
create_topology(Level, MPA_A, MPAs_2) :-
   findall( MPA_B,
      call_dependency(MPA_A, MPA_B),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   assert_topology(MPAs_2, Level, MPA_A).


/* call_dependency(+MPA_A, -MPA_B) <-
      */

call_dependency(MPA_A, (M:P)/A) :-
   rar:calls_pp_inv(MPA_A, _, (M:P)/A),
   atom(P),
   not( MPA_A = (M:P)/A).


/* assert_topology(+Leaf, +Level, +MPA) <-
      */

assert_topology([], _, MPA) :-
   rar:is_directive(MPA),
   !.
assert_topology([], Level, MPA) :-
   dead_and_undefined_code_excluded:excluded_dead_code(MPA),
   retract_and_assert(MPA, Level, excluded_dead_code),
   !.
assert_topology([], Level, MPA) :-
   retract_and_assert(MPA, Level, leaf),
   !.
assert_topology(_, Level, MPA) :-
   retract_and_assert(MPA, Level, no_leaf).

retract_and_assert(MPA, Level, Type) :-
   retract( topology(MPA, _, _) ),
   assert( topology(MPA, Level, Type) ),
   !.
retract_and_assert(MPA, Level, Type) :-
   assert( topology(MPA, Level, Type) ).


/* determine_strong_components <-
      */

determine_strong_components :-
   write('determing strong components ... '),
   findall(A-B,
      call_dependency(A, B),
      Edges ),
   ugraphs:vertices_edges_to_ugraph([], Edges, U_Graph),
   ugraphs:strong_components(U_Graph, Components_1, _),
   sublist( is_not_single_component,
      Components_1, Components_2 ),
   iterate_list( topology_db:assert_component,
      0, Components_2, L ),
   write(L),
   writeln(' ...done').

is_not_single_component(List) :-
   length(List, Length),
   Length > 1.

assert_component(SCC_No_1, C, SCC_No_2) :-
   SCC_No_2 is SCC_No_1 + 1,
   assert( topology_db:strong_component(C) ),
   forall( member(Elt, C),
      assert( topology_db:cyclic(Elt, SCC_No_1) ) ).


/* asserted_stream_content(+Term, +Stream) <-
      */

asserted_stream_content(end_of_file, _) :-
   !.
asserted_stream_content(topology(A, B, C), Stream) :-
   !,
   assert( topology_db:topology(A, B, C) ),
   read(Stream, T2),
   rar:asserted_stream_content(T2, Stream).
asserted_stream_content(strong_component(A), Stream) :-
   !,
   assert( topology_db:strong_component(A) ),
   read(Stream, T2),
   rar:asserted_stream_content(T2, Stream).
asserted_stream_content(cyclic(A, B), Stream) :-
   !,
   assert( topology_db:cyclic(A, B) ),
   read(Stream, T2),
   rar:asserted_stream_content(T2, Stream).


/* write_topology_facts <-
      */

write_topology_facts :-
   forall( topology_db:topology(A, B, C),
      rar:write_term_with_dot(
         topology(A, B, C)) ),
   forall( topology_db:strong_component(A),
      rar:write_term_with_dot(
         strong_component(A)) ),
   forall( topology_db:cyclic(A, B),
      rar:write_term_with_dot(
         cyclic(A, B)) ).


/* create_topology_db_if_not_exists <-
      */

create_topology_db_if_not_exists :-
   topology_db:topology(_, _, _),
   !.
create_topology_db_if_not_exists :-
   topology_db:topology_db.


/* create_strong_components_db_if_not_exists <-
      */

create_strong_components_db_if_not_exists :-
   topology_db:strong_component(_),
   !.
create_strong_components_db_if_not_exists :-
   topology_db:determine_strong_components.


/******************************************************************/


