

/******************************************************************/
/***                                                            ***/
/***         Statistics: Clone-Detection                        ***/
/***                                                            ***/
/******************************************************************/


:- module( polar_diagrams_save_and_load, [
   save_polar_facts/1,
   load_polar_facts/1]).


/*** interface ****************************************************/


/* load_polar_facts(+File) <-
      */

load_polar_facts(File_1) :-
   tilde_to_home_path(File_1, File_2),
   reset_polar_db,
   open(File_2, read, Stream),
   read(Stream, T0),
   asserted_stream_content(T0, Stream),
   close(Stream).


/* save_polar_facts(+File) <-
      */

save_polar_facts(File_1) :-
   tilde_to_home_path(File_1, File_2),
   predicate_to_file( File_2,
      polar_diagrams_save_and_load:write_polar_facts ),
   !.


/*** implementation ***********************************************/


/* asserted_stream_content(+Term, +Stream) <-
      */

asserted_stream_content(end_of_file, _) :-
   !.
asserted_stream_content(mpa_calls_files(A, B), Stream) :-
   !,
   assert( polar_diagrams_db:mpa_calls_files(A, B) ),
   read(Stream, T2),
   rar:asserted_stream_content(T2, Stream).


/* write_histogram_molecules <-
      */

write_polar_facts :-
   forall( polar_diagrams_db:mpa_calls_files(A, B),
      rar:write_term_with_dot(
         mpa_calls_files(A, B)) ).


/******************************************************************/


