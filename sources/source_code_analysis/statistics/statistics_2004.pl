

/******************************************************************/
/***                                                            ***/
/***          Statistics                                        ***/
/***                                                            ***/
/******************************************************************/


:- module( statistics_2004, []).


/*** interface ****************************************************/


statistics_of_2004(['Predicates', S_2007],
      ['Predicates', S_2007, S_2004, X]) :-
   S_2004 = 7657,
   changed_statistics_of_2004(S_2007, S_2004, X).
statistics_of_2004(['Dynamic Defined', S_2007],
      ['Dynamic Defined', S_2007, S_2004, X]) :-
   S_2004 = 110,
   changed_statistics_of_2004(S_2007, S_2004, X).
statistics_of_2004(['Multifile Defined', S_2007],
      ['Multifile Defined', S_2007, S_2004, X]) :-
   S_2004 = 63,
   changed_statistics_of_2004(S_2007, S_2004, X).
statistics_of_2004(['Discontiguous Defined', S_2007],
      ['Discontiguous Defined', S_2007, S_2004, X]) :-
   S_2004 = 58,
   changed_statistics_of_2004(S_2007, S_2004, X).
statistics_of_2004(['Rules', S_2007],
      ['Rules', S_2007, S_2004, X]) :-
   S_2004 = 14039,
   changed_statistics_of_2004(S_2007, S_2004, X).
statistics_of_2004(['Files', S_2007],
      ['Files', S_2007, S_2004, X]) :-
   S_2004 = 444,
   changed_statistics_of_2004(S_2007, S_2004, X).
statistics_of_2004(['LoC', S_2007],
      ['LoC', S_2007, S_2004, X]) :-
   S_2004 = 100653,
   changed_statistics_of_2004(S_2007, S_2004, X).
statistics_of_2004(['Prolog Modules', S_2007],
      ['Prolog Modules', S_2007, S_2004, X]) :-
   S_2004 = 47,
   changed_statistics_of_2004(S_2007, S_2004, X).
statistics_of_2004(['Units', S_2007],
      ['Units', S_2007, S_2004, X]) :-
   S_2004 = 8,
   changed_statistics_of_2004(S_2007, S_2004, X).
statistics_of_2004(['Modules', S_2007],
      ['Modules', S_2007, S_2004, X]) :-
   S_2004 = 58,
   changed_statistics_of_2004(S_2007, S_2004, X).
statistics_of_2004(['Directives', S_2007],
      ['Directives', S_2007, S_2004, X]) :-
   S_2004 = 443,
   changed_statistics_of_2004(S_2007, S_2004, X).

changed_statistics_of_2004(S_2007, S_2004, X) :-
   X_1 is round(((S_2007 - S_2004)/S_2004)*100),
   concat_atom(['+', X_1, '%'], X).


/*** implementation ***********************************************/


/******************************************************************/


