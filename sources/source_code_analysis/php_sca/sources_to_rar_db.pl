

/******************************************************************/
/***                                                            ***/
/***          PHP: source tree and call methods                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* phpml_sources_to_tree(+Directory, +XML_Directory, -Tree) <-
      */

phpml_sources_to_tree(Directory, XML_Directory_1, Tree_3) :-
   file_system_to_xml(Directory, Tree_1),
   filter_files(php, Tree_1, Tree_2),
   set_mouse_click_attributes([dir:php_dir, file:php_file],
      Tree_2, Tag:Attr_1:Cont),
   add_slash_suffix(XML_Directory_1, XML_Directory_2),
   Attr_2 = [xml_root:XML_Directory_2,
      source_code:php|Attr_1],
   list_to_ord_set(Attr_2, Attr_3),
   Tree_3 = Tag:Attr_3:Cont.


/*** implementation ***********************************************/


/* php_method_calls(+Xml, -Calls) <-
      */

php_method_calls(Xml, Calls) :-
   findall( Call,
      ( D := Xml^_^'method-declaration',
        not( true := D@'is-constructor' ),
        A := D@name,
        I := D^_^'method-invocation',
        B := I@name,
        Call = calls:[]:[
           source:[class:user, method:A]:[],
           target:[class:user, method:B]:[] ] ),
      Calls ).


/* php_class_calls(+Xml, -Calls) <-
      */

php_class_calls(Xml, Calls) :-
   findall( Call,
      ( Class := Xml^_^'class-declaration',
        V := Class@name,
        ( W := Class^superclass^class@name,
          Colour = red
          ; W := Class^interfaces^class@name,
          Colour = blue ),
        Call = calls:[]:[
           source:[class:user, method:V]:[],
           target:[class:user, method:W]:[] ] ),
       Calls ).


/******************************************************************/


