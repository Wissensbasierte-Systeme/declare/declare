

/******************************************************************/
/***                                                            ***/
/***      xml_io: Tests                                         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(source_code_analysis:xml_io, write_read_write_read) :-
   DTD = dtd:[
      a:int, font_style:sequence,size:int,
      handle_offset:sequence, y_pos:list ]:[],
   Gxl = graph:[id:cross_desc_necess]:[
      type:[name:test_graph]:[],
      gxl_node:[id:n_id_1]:[
         type:[name:test_node]:[],
         complex_type:[
            triple:(helvetica, roman, 10),
            string:predicate,
            path:'./visur_rar/a.gif',
            float:0.25,
            list:[a, b, c] ]:[
               element_1:[]:['Dietmar', seipel],
               element_2:[]:[
                  '[[1, 2, 3], a, b, c, (z,z)]', [abs] ],
               element_3:[]:[
                  (a, b), attr:[name:tara]:[a,b,c] ] ] ] ],
   checklist( tmp_file(test, ''),
      [XML_IO_dwrite_File, USERS_dwrite_File_1, USERS_dwrite_File_2] ),
   not( exists_file_with_tilde(XML_IO_dwrite_File) ),
   not( exists_file_with_tilde(USERS_dwrite_File_1) ),
   not( exists_file_with_tilde(USERS_dwrite_File_2) ),

   dwrite(xml_2, XML_IO_dwrite_File, Gxl),
   dwrite(xml, USERS_dwrite_File_1, Gxl),
   dread_(xml(DTD), XML_IO_dwrite_File, XML_IO_dwrite_1),
   dread_(xml(DTD), USERS_dwrite_File_1, USERS_dwrite),
   not(XML_IO_dwrite_1 = USERS_dwrite),

   dwrite(xml_2, USERS_dwrite_File_2, XML_IO_dwrite_1),
   dread_(xml(DTD), USERS_dwrite_File_2, XML_IO_dwrite_2),
   checklist( delete_file,
      [XML_IO_dwrite_File, USERS_dwrite_File_2, USERS_dwrite_File_1] ),
   XML_IO_dwrite_1 = XML_IO_dwrite_2,
   nl.


/******************************************************************/


