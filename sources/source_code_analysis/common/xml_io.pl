

/******************************************************************/
/***                                                            ***/
/***          RAR:  XML                                         ***/
/***                                                            ***/
/******************************************************************/


:- module( xml_io, [
      write_structure/3,
      read_structure/3 ] ).


/*** interface ****************************************************/


/* read_structure(+File, -Content, +Options) <-
      */

read_structure(File, FN_Term_2, Options_1) :-
   memberchk(dialect(xml), Options_1),
   memberchk(types(DTD), Options_1),
   Options_2 = [ dialect(xml), max_errors(5000), space(preserve) ],
   load_structure(File, FN_Term_1, Options_2),
   xml_swi_to_fn(DTD, FN_Term_1, FN_Term_2),
   !.


/* write_structure(+File, +Content, +Options) <-
      */

write_structure(File, FN_Term, Options) :-
   memberchk(dialect(xml), Options),
   fn_term_to_xml_file(FN_Term, File).


/*** implementation ***********************************************/


/* fn_term_to_xml_file(+FN_Term, +File) <-
      */

fn_term_to_xml_file(FN_Term, File) :-
   predicate_to_file( File,
      ( writeln(
           '<?xml version=''1.0'' encoding=''ISO-8859-1'' ?>'),
        nl,
        xml_io:field_notation_to_xml(FN_Term) ) ).


/* field_notation_to_xml(+FN_Term) <-
      */

field_notation_to_xml(T:As:Es) :-
   !,
   field_notation_to_xml_write(0, T:As:Es).

field_notation_to_xml_write(I, T:As:[]) :-
   !,
   tab(I), write_list(['<', T]),
   checklist( field_notation_write_attribute,
      As ),
   writeln('/>').
field_notation_to_xml_write(I, T:As:Es) :-
   tab(I), write_list(['<', T]),
   checklist( field_notation_write_attribute,
      As ),
   writeln('>'),
   J is I + 3,
   field_notation_write_content(J, Es),
   tab(I), write_list(['</',T,'>\n']).
field_notation_to_xml_write(I, X) :-
   is_list(X),
   tab(I),
   writeln(X).
field_notation_to_xml_write(I, X) :-
   functor(X, ',', 2),
   tab(I),
   write_list(['(', X, ')\n']).
field_notation_to_xml_write(I, X) :-
   tab(I),
   writeln(X).

field_notation_write_attribute(A:V) :-
   functor(V, ',', 2),
   write_list([' ', A, '="(', V, ')"']).
field_notation_write_attribute(A:V) :-
   write_list([' ', A, '="', V, '"']).

field_notation_write_content(J, Es) :-
   checklist( xml_io:field_notation_to_xml_write(J),
      Es ).


/* xml_swi_to_fn(+DTD, +Element_1, -Element_2) <-
      */

xml_swi_to_fn(DTD, element(string, [], Es1), string:[]:Es3) :-
   !,
   xml_swi_content_to_fn(DTD, Es1, Es2),
   maplist(term_to_atom_sure,
      Es3, Es2).
xml_swi_to_fn(DTD, element(Name, As1, Es1), Name:As2:Es2) :-
   !,
   maplist( xml_swi_attribute_to_fn(DTD),
      As1, As2 ),
   xml_swi_content_to_fn(DTD, Es1, Es2 ).
xml_swi_to_fn(DTD, Xs, Ys) :-
   is_list(Xs),
   !,
   maplist( xml_swi_to_fn(DTD),
      Xs, Ys ).
xml_swi_to_fn(_, X, X).

xml_swi_attribute_to_fn(DTD, A=Value_1, A:Value_2) :-
   memberchk(A:Type, DTD),
   string_to_type(Type, Value_1, Value_2).
xml_swi_attribute_to_fn(_, A=V, A:V).

string_to_type(Type, String, Term) :-
   memberchk(Type, [list, sequence, int, term]),
   term_to_atom(Term, String).
string_to_type(string, String, Term) :-
   term_to_atom_sure(Term, String).
string_to_type(mpa_term, String, (M:P)/A) :-
   catch(
      term_to_atom((M:P)/A, String), _, fail ),
      ground((M:P)/A).
string_to_type(mpa_term, String, String).


/* xml_swi_content_to_fn(+DTD, +C, -G) <-
      */

xml_swi_content_to_fn(DTD, C, G) :-
   !,
   maplist( xml_swi_to_fn(DTD),
      C, D ),
   maplist( transform,
      D, E ),
   delete(E, [], F),
   flatten(F, G).

transform(A, List) :-
   atom(A),
   name(A, L),
   transform_a(L, List).
transform(A, A).

transform_a([], []) :-
   !.
transform_a(As, []) :-
   transform_1(As, [], []),
   !.
transform_a(As, [B|Bs]) :-
   transform_1(As, Elt, As_Rest),
   name(B, Elt),
   transform_a(As_Rest, Bs).

transform_1([10|As], Elt, As_Rest) :-
   transform_2(As, Elt, As_Rest),
   !.
transform_1(As, Elt, As_Rest) :-
   transform_3(As, Elt, As_Rest).

transform_2([], [], []).
transform_2([32|As], Elt, As_Rest) :-
   transform_2(As, Elt, As_Rest),
   !.
transform_2(As, Elt, As_Rest) :-
   transform_3(As, Elt, As_Rest).

transform_3([], [], []).
transform_3([10|As], [], [10|As]) :-
   !.
transform_3([A|As], [A|Bs], Cs) :-
   transform_3(As, Bs, Cs).


/* resolve_file_references_in_xml(
      +Dir, +Type, +Elmt_1, -Elmt_2) <-
      */

resolve_file_references_in_xml(
      Dir, Type, Tag:[ref:File]:[], Tag:Attr:Cont_2) :-
   exists_file_with_tilde(File),
   dread_(xml(Type), File, Tag:Attr:Cont_1),
   resolve_file_references_in_xml(
      Dir, Type, Tag:Attr:Cont_1, Tag:Attr:Cont_2),
   !.
resolve_file_references_in_xml(
      Dir, Type, Tag:[ref:File_1]:[], Tag:Attr:Cont_2) :-
   concat([Dir, '/', File_1], File_2),
   exists_file_with_tilde(File_2),
   dread_(xml(Type), File_2, Tag:Attr:Cont_1),
   xml_io:resolve_file_references_in_xml(
      Dir, Type, Tag:Attr:Cont_1, Tag:Attr:Cont_2),
   !.
resolve_file_references_in_xml(
      Dir, Type, Tag:Attr:Cont_1, Tag:Attr:Cont_2) :-
   resolve_file_references_in_xml_sub(Dir, Type, Cont_1, Cont_2),
   !.
resolve_file_references_in_xml(_, _, E, E).

resolve_file_references_in_xml_sub(
      Dir, Type, [E_1|Es_1], [E_2|Es_2]) :-
   resolve_file_references_in_xml(Dir, Type, E_1, E_2),
   resolve_file_references_in_xml_sub(Dir, Type, Es_1, Es_2).
resolve_file_references_in_xml_sub(_, _, [], []).


/******************************************************************/


