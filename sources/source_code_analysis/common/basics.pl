

/******************************************************************/
/***                                                            ***/
/***       Source Code Analysis:  Basic Methods                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/

/* add_lists(+List_1, +List_2, -List_3) <-
      */

add_lists(List_1, List_2, List_3) :-
   is_list(List_1),
   is_list(List_2),
   maplist( add_extended,
      List_1, List_2, List_3 ).

add_extended(A, B, C) :-
   number(A),
   number(B),
   C is A + B,
   !.
add_extended(_, _, nv).


/* dread_(+A, +B, -C) <-
      */

dread_(A, B, C) :-
   dread(A, B, C),
   !.


/* term_to_atom_sure(Term, Term) <-
      */

/*
term_to_atom_sure(Number, Atom) :-
   catch( atom_number(Atom, Number), _, fail ),
   !.
*/
term_to_atom_sure(Term, Term) :-
   atom(Term),
   !.
term_to_atom_sure(Term, Atom) :-
   catch( term_to_atom(Term, Atom), _, fail ),
   !.
term_to_atom_sure(Atom, Atom).


/* term_to_atom_sure_extended(?Term, ?Atom) <-
      */

term_to_atom_sure_extended(A, '') :-
   var(A),
   !.
term_to_atom_sure_extended(A, B) :-
   term_to_atom_sure(A, B),
   !.


/* determine_runtime(Goal) <-
      */

determine_runtime(Goal) :-
   determine_runtime(Goal, Time),
   write_list([Time, ' sec.']).

determine_runtime(Goal, Time) :-
   statistics(runtime, [T1|_]),
   call(Goal),
   statistics(runtime, [T2|_]),
   round((T2 - T1) / 1000, 2, Time).


/* protokoll <-
      */

protokoll :-
   home_directory(Home),
   concat(Home, '/prolog.log', File),
   protocola(File),
   get_time_with_leading_zeros(Year, Month, Day,
      Hour, Minute, Second, _),
   write_list(['Today is ', Day, '.', Month, '.', Year, '.\n']),
   write_list(['It is ', Hour, ':', Minute, ':', Second, '.\n']),
   current_prolog_flag(xpce_version, XPCE_Version),
   current_prolog_flag(version, Version),
   write_list(['XPCE Version: ', XPCE_Version, ', ']),
   write_list(['Version: ', Version, '.', '\n']).
%  dislog_variable_get(home, Dir),
%  fs_save_info(Dir).


/* current_date_to_string(-Date_String) <-
      */

current_date_to_string(Date_String) :-
   get_time_with_leading_zeros(Year, Month, Day, _, _, _, _),
   concat([Year, '-', Month, '-', Day], Date_String),
   !.


/* current_date_and_time_to_string(-Time_String) <-
      */

current_date_and_time_to_string(Time_String) :-
   get_time_with_leading_zeros(Year, Month, Day,
      Hour, Minute, _, _),
   concat([Year, '-', Month, '-', Day,
      '_', Hour, Minute], Time_String),
   !.


/* current_time_to_string(-Time_String) <-
      */

current_time_to_string(Time_String) :-
   get_time_with_leading_zeros(_, _, _,
      Hour, Minute, Second, _),
   concat([Hour, ':', Minute, ':', Second],
      Time_String),
   !.


/* get_time_with_leading_zeros(-Year, -Month, -Day,
      -Hour, -Minute, -Second, -MilliSeconds) <-
      */

get_time_with_leading_zeros(Year, Month_2, Day_2,
      Hour_2, Minute_2, Second_2, MilliSeconds_2) :-
   get_time(Time),
   convert_time(Time, Year, Month_1, Day_1,
      Hour_1, Minute_1, Second_1, MilliSeconds_1),
   maplist( number_to_atom(2),
      [Month_1, Day_1, Hour_1, Minute_1, Second_1],
      [Month_2, Day_2, Hour_2, Minute_2, Second_2] ),
   number_to_atom(3, MilliSeconds_1, MilliSeconds_2).


/* ord_union_2(A, B, C) <-
      */

ord_union_2(A, B, C_2) :-
   union(A, B, C_1),
   list_to_ord_set(C_1, C_2).

ord_union_2(A, B_2) :-
   union(A, B_1),
   list_to_ord_set(B_1, B_2).


/* concat_path(+Path_A, +Path_B, -Path_C) <-
   */

concat_path(Path_A, Path_B, Path_C) :-
   concat_path([Path_A, Path_B], Path_C),
   !.

concat_path([A_1|Path_As_1], Path_B) :-
   delete_slash_suffix(A_1, A_2),
   maplist( delete_slash,
      Path_As_1, Path_As_2 ),
   concat_atom([A_2|Path_As_2], '/', Path_B).


/* delete_slash(+Dir_1, -Dir_2) <-
      */

delete_slash(A_1, A_3) :-
   delete_slash_suffix(A_1, A_2),
   delete_slash_prefix(A_2, A_3),
   !.

delete_slash_suffix(A_1, A_3) :-
   concat(A_2, '/', A_1),
   delete_slash_suffix(A_2, A_3),
   !.
delete_slash_suffix(A, A) :-
   !.

delete_slash_prefix(A_1, A_3) :-
   concat('/', A_2, A_1),
   delete_slash_prefix(A_2, A_3),
   !.
delete_slash_prefix(A, A) :-
   !.


/* add_slash(+Dir_1, -Dir_2) <-
      */

add_slash(Dir_1, Dir_3) :-
   add_slash_suffix(Dir_1, Dir_2),
   add_slash_prefix(Dir_2, Dir_3).

add_slash_suffix(Dir, Dir) :-
   sub_atom(Dir, _, 1, 0, '/'),
   !.
add_slash_suffix(Dir_1, Dir_2) :-
   concat(Dir_1, '/', Dir_2),
   !.

add_slash_prefix(Dir, Dir) :-
   sub_atom(Dir, 0, 1, _, '/'),
   !.
add_slash_prefix(Dir_1, Dir_2) :-
   concat('/', Dir_1, Dir_2),
   !.


/* remove_prefix(+Prefix, +Complete, -Suffix) <-
      */

remove_prefix(Prefix, Complete:Number, Suffix:Number) :-
   concat_atom([Prefix, Suffix], Complete),
   !.
remove_prefix(_, Complete:Number, Complete:Number) :-
   !.
remove_prefix(Prefix, Complete, Suffix) :-
   concat_atom([Prefix, Suffix], Complete),
   !.
remove_prefix(_, Complete, Complete) :-
   !.


/*** implementation ***********************************************/


/* number_to_atom(+Digits, +Number_1, -Number_2) <-
      */

number_to_atom(Digits, Number_1, Number_4) :-
   number_chars(Number_1, Number_2),
   length(Number_2, Length),
   Zeros_To_Add is Digits - Length,
   Zeros_To_Add > 0,
   append_zeros_in_front_of_number(Zeros_To_Add,
      Number_2, Number_3),
   concat(Number_3, Number_4),
   !.
number_to_atom(_, Number_1, Number_2) :-
   atom_number(Number_2, Number_1),
   !.

append_zeros_in_front_of_number(Zeros, Number, Number) :-
   Zeros <= 0,
   !.
append_zeros_in_front_of_number(Zeros_1, Number_1, Number_3) :-
   Zeros_2 is Zeros_1 - 1,
   Number_2 = ['0'|Number_1],
   append_zeros_in_front_of_number(Zeros_2, Number_2, Number_3),
   !.


/* tmp_file(+Base, +Extension, -File_3) <-
      */

tmp_file(Base, Extension, File_3) :-
   tmp_file(Base, File_1),
   file_base_name(File_1, File_2),
   file_name_extension(File_2, Extension, File_3).


/* writelq(+A) <-
      */

writelq(A) :-
   writeq(A),
   nl.


/* writelq_list(A) <-
      */

writelq_list(A) :-
   checklist( writelq, A ).


/******************************************************************/


