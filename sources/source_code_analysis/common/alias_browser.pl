

/******************************************************************/
/***                                                            ***/
/***       Source Code Analysis:  Basic Methods                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* alias_browser <-
      */

alias_browser :-
   alias_browser_update(Xml, Config),
   new(Frame, persistent_frame('Alias Browser')),
   new(HB, hierarchy_browser_ddk(Config, Xml)),
   send(Frame, append, HB),
   send(Frame, open).


/* gui_add_alias_to_file_history(+Browser) <-
      */

gui_add_alias_to_file_history(Browser) :-
   ask_for_an_alias(Alias),
   add_alias_to_file_history(Alias),
   alias_browser_update(Xml, Config),
   hierarchy_to_browser_ddk(Config, Xml, Browser).


/* gui_add_file_to_alias(+Browser, +Alias_Address) <-
      */

gui_add_file_to_alias(Browser, Alias_Address) :-
   get(Alias_Address, xml, Alias_Xml),
   Alias := Alias_Xml@name,
   get(@finder, file, @on, File),
   add_file_to_alias(Alias, File),
   alias_browser_update(Xml, Config),
   hierarchy_to_browser_ddk(Config, Xml, Browser).


/* gui_delete_alias(+Browser, Alias_Address) <-
      */

gui_delete_alias(Browser, Alias_Address) :-
   get(Alias_Address, xml, Alias_Xml),
   Alias := Alias_Xml@name,
   delete_alias(Alias),
   alias_browser_update(Xml, Config),
   hierarchy_to_browser_ddk(Config, Xml, Browser).


/*** implementation ***********************************************/


/* alias_browser_update(-Xml, -Config) <-
      */

alias_browser_update(Xml_2, Config) :-
   history_file_to_fn_triple(Xml_1),
   add_and_mark_default_files(Xml_1, Xml_2),
   alias_to_fn_triple(alias_browser, Config).


/* add_and_mark_default_files(file_history:Attr:C_1, Xml_2) <-
      */

add_and_mark_default_files(file_history:Attr_A:C_1, Xml_2) :-
   C_1 = [aliases:Attr_B:Cont_1],
   maplist( add_default_file_to_alias,
      Cont_1, Cont_2),
   list_to_ord_set(Cont_2, Cont_3),
   C_2 = [aliases:Attr_B:Cont_3],
   Xml_2 = file_history:Attr_A:C_2.

add_default_file_to_alias(alias:Attr:Files_1, alias:Attr:Files_2) :-
   maplist( mark_files,
      Files_1, Files_2 ),
   !.

mark_files(file:Attr_1:[], file:Attr_3:[]) :-
   memberchk(prefix:Prefix, Attr_1),
   memberchk(path:File_1, Attr_1),
   file_alias:concat_prefix_and_suffix(Prefix, File_1, Path, File_2),
   exists_file_with_tilde(File_2),
   concat(['Prefix: ', Path, ', Suffix: ', File_1], Name),
   ( memberchk(type:default, Attr_1) ->
     Attr_2 = [name:Name, mouse_click:default_file,
           open:'~/DisLog/images/diagnosis_p6.gif',
           close:'~/DisLog/images/diagnosis_p6.gif'|Attr_1]
   ; Attr_2 = [name:Name|Attr_1] ),
   list_to_ord_set(Attr_2, Attr_3),
   !.
mark_files(file:Attr_1:[], file:Attr_3:[]) :-
   memberchk(path:File, Attr_1),
   not( exists_file_with_tilde(File) ),
   Attr_2 = [name:File, mouse_click:non_existing,
      open:'~/DisLog/images/alert.xpm',
      close:'~/DisLog/images/alert.xpm'|Attr_1],
   list_to_ord_set(Attr_2, Attr_3),
   !.
mark_files(file:Attr_1:[], file:Attr_3:[]) :-
   memberchk(type:default, Attr_1),
   memberchk(path:File, Attr_1),
   Attr_2 = [name:File, mouse_click:default_file,
      open:'~/DisLog/images/diagnosis_p6.gif',
      close:'~/DisLog/images/diagnosis_p6.gif'|Attr_1],
   list_to_ord_set(Attr_2, Attr_3),
   !.
mark_files(file:Attr_1:[], file:Attr_3:[]) :-
   memberchk(path:File, Attr_1),
   Attr_2 = [name:File|Attr_1],
   list_to_ord_set(Attr_2, Attr_3),
   !.


/* view_file_with_emacs(+Node) <-
      */

view_file_with_emacs(Node) :-
   get(Node, xml, Xml_Node),
   xml_file_to_pure_file(Xml_Node, File),
   emacs(File).


/* ask_for_an_alias(Alias) <-
      */

ask_for_an_alias(Alias) :-
   new(D, dialog('Prompting for an alias name')),
   send(D, append,
   new(TI, text_item('Alias Name', ''))),
   send(D, append,
   button(ok, message(D, return,
      TI?selection))),
   send(D, append,
      button(cancel, message(D, return, @nil))),
   send(D, default_button, ok),
   get(D, confirm, Answer),
   send(D, destroy),
   Answer \== @nil,
   Alias = Answer.


/******************************************************************/


