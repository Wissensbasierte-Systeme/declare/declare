

/******************************************************************/
/***                                                            ***/
/***         DTD Types                                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* alias_to_dtd(+Type, -DTD) <-
      */

alias_to_dtd(dtd:A:[], A) :-
   is_list(A),
   !.
alias_to_dtd(Alias, DTD) :-
   atomic(Alias),
   memberchk(Alias, [gxl_graph, gxl_presettings,
      visur_config, visur_gxl_config, gxl_config, owl_config,
      swrl_config,
      common, calls_pp_test,
      file_history]),
   DTD =
      [pen:int, width:int, length:int, factor:int, points:int,
       x_pos:int, y_pos:int, height:int,
       x_step:int, y_step:int, x_start:int, y_start:int,
       weight:int,
       call:term, prmtrs:list,
%      symbol:sequence,
       size:int,
       color:term, font_style:sequence],
   !.
alias_to_dtd(rar_database, DTD) :-
   DTD =
      [arity:int],
   !.
alias_to_dtd(Alias, DTD) :-
   atomic(Alias),
   memberchk(Alias, [hierarchy_browser_config, visur_browser,
     kbms_browser_config, alias_browser,
     xml_menu, visur_menu_bar]),
   DTD =
      [prmtrs:list],
   !.
alias_to_dtd(predicate_groups, DTD) :-
   DTD =
      [arity:int, calls:sequence],
   !.
alias_to_dtd(_, []).

/*** implementation ***********************************************/


/******************************************************************/


