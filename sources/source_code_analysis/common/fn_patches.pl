

/******************************************************************/
/***                                                            ***/
/***   FN Patches or FN equivalent Methods, which are faster    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


% instead of:
%  Tree_2 :=
%     Tree_1*[^_^submenu::[@name=Name]^submenu^content:Menus].
fn_patch_2(R:A_1:C_1, name:Name, Menus, R:A_1:C_2) :-
   memberchk(name:Name, A_1),
   C_1 = [E_1, E_2:Attr:_],
   C_2 = [E_1, E_2:Attr:Menus],
   !.
fn_patch_2(R:A:C_1, name:Name, Menus, R:A:C_2) :-
   fn_patch_2_sub(C_1, name:Name, Menus, C_2).
fn_patch_2('', _, _, '').

fn_patch_2_sub([C_1|Cs_1], name:Name, Menus, [C_2|Cs_2]) :-
   fn_patch_2(C_1, name:Name, Menus, C_2),
   fn_patch_2_sub(Cs_1, name:Name, Menus, Cs_2).
fn_patch_2_sub([], _, _, []).


fn_patch_3(Tag:Attr:_, name:Name, Menus, Tag:Attr:Menus) :-
   memberchk(name:Name, Attr),
   !.
fn_patch_3(Tag:A:C_1, name:Name, Menus, Tag:A:C_2) :-
   fn_patch_3_sub(C_1, name:Name, Menus, C_2).
fn_patch_3('', _, _, '').

fn_patch_3_sub([C_1|Cs_1], name:Name, Menus, [C_2|Cs_2]) :-
   fn_patch_3(C_1, name:Name, Menus, C_2),
   fn_patch_3_sub(Cs_1, name:Name, Menus, Cs_2).
fn_patch_3_sub([], _, _, []).



%  Gxl_1 = gxl:[]:[graph:[id:a]:[], graph:[id:b]:[]],
%  Gxl_2 := (g:[]:[Gxl_1])^gxl::[^graph@id=a]*[^graph^element:1].
/*
fn_patch_4(Gxl_1, Gxl_Element, Gxl_2) :-
   graph:Attr_1:Cont_1 := Gxl_1^graph::[@id='added_graph'],
   Added_Graph = graph:Attr_1:[Gxl_Element|Cont_1],
   Gxl_1 = gxl:Attr_2:[_Old_Added_Graph| B],
   Gxl_2 = gxl:Attr_2:[Added_Graph|B].
*/

/* change_fn_value(+Tree_1, +Attr:Value, -Tree_2) <-
      Tree_2 := Tree_1*[@Attr:Value]
      */

change_fn_value(Tag:Attr_1:Cont, Attr:Value, Tag:Attr_2:Cont) :-
   change_fn_value_fast(Attr_1, Attr:Value, Attr_2).

/*
change_fn_value(Tag:Attr_1:Cont, List_1, Tag:Attr_3:Cont) :-
   is_list(List_1),
   maplist( clear_attr_value,
      List_1, List_2 ),
   subtract(Attr_1, List_2, Attr_2),
   append(List_1, Attr_2, Attr_3).

clear_attr_value(A:_, A:_).
*/

/* change_fn_value_fast(+Attr_1, +Attr:Value, -Attr_3) <-
      */

change_fn_value_fast(Attr_1, Attr:Value, Attr_3) :-
   subtract(Attr_1, [Attr:_], Attr_2),
   Attr_3 = [Attr:Value|Attr_2],
   !.



/*** implementation ***********************************************/


/******************************************************************/


