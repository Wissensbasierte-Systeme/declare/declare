

/******************************************************************/
/***                                                            ***/
/***           DisLog:  additonal to Word Count                 ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


/* exists_file_with_tilde(+File) <-
      */

exists_file_with_tilde(File) :-
   tilde_to_home_path(File, Expanded_File),
   exists_file(Expanded_File).


/* exists_directory_with_tilde(+Dir) <-
      */

exists_directory_with_tilde(Dir) :-
   tilde_to_home_path(Dir, Expanded_Dir),
   exists_directory(Expanded_Dir),
   !.


/* tilde_to_home_path(+String_1, -String_2) <-
      */

tilde_to_home_path(F_1, F_3) :-
   ground(F_1),
   concat('~/', F_2, F_1),
   getenv('HOME', Home),
   concat([Home, '/', F_2], F_3),
   !.
tilde_to_home_path(F_1, F_3) :-
   ground(F_3),
   getenv('HOME', Home),
   concat(Home, F_2, F_3),
   concat('~', F_2, F_1),
   !.
tilde_to_home_path(F, F) :-
   !.


/*** implementation ***********************************************/



/******************************************************************/


