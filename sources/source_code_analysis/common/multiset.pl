

/******************************************************************/
/***                                                            ***/
/***         Statistics: Clone-Detection                        ***/
/***                                                            ***/
/******************************************************************/


:- module( multiset, [ ]).

:- dynamic multiset_elt/2.


/*** interface ****************************************************/


/* list_to_multiset(+List, -Multiset_2) <-
      */

list_to_multiset(List, Multiset_2) :-
   maplist( list_to_multiset(1),
      List, Multiset_1 ),
   multiset_normalize(Multiset_1, Multiset_2).


/* multiset_normalize(+Ms_1, -Ms_3) <-
      */

multiset_normalize(Ms_1, Ms_3) :-
   retractall( multiset_elt(_, _) ),
   checklist( assert_multiset,
      Ms_1 ),
   multiset_to_elements(Ms_1, Set),
   maplist( count_elements,
      Set, Ms_2 ),
   retractall( multiset_elt(_, _) ),
   flatten(Ms_2, Ms_3).


/* multiset_sum(+MS, -Sum) <-
      */

multiset_sum(MS, Sum) :-
   iterate_list( multiset:sum,
      _:0, MS, sum:Sum ).

sum(_:A, _:B, _:C) :-
   C is A + B.


/* list_to_multiset2(+List, -Multiset) <-
      */

list_to_multiset2(List, Multiset_2) :-
   retractall( multiset_elt(_, _) ),
   checklist( assert_multiset2,
      List ),
   findall( A:B,
      multiset_elt(A, B),
      Multiset_1 ),
   list_to_ord_set(Multiset_1, Multiset_2),
   retractall( multiset_elt(_, _) ).


/* multiset_to_elements(+Multiset, -Set) <-
      */

multiset_to_elements(Multiset, Set_2) :-
   maplist( multiset_to_element,
      Multiset, Set_1 ),
   list_to_set(Set_1, Set_2).


/* multiset_max(+Ms, -Max) <-
      */

multiset_max(Ms, Max) :-
   findall( V,
      member(_:V, Ms),
      Vs ),
   max(Vs, Max).


/*** implementation ***********************************************/


/* assert_multiset2(+A) <-
      */

assert_multiset2(A) :-
   multiset_elt(A, B_1),
   retract( multiset_elt(A, B_1) ),
   B_2 is B_1 + 1,
   assert( multiset_elt(A, B_2) ),
   !.
assert_multiset2(A) :-
   assert( multiset_elt(A, 1) ),
   !.


/* list_to_multiset(?A, ?B, ?B:A) <-
      */

list_to_multiset(A, B, B:A).


/* multiset_to_element(?A:_, ?A) <-
      */

multiset_to_element(A:_, A).


/* fill_multiset(+Border, +Mulitset_1, -Mulitset_2) <-
      */

fill_multiset(Border, [A:_|Multiset], MS) :-
   A > Border,
   !,
   fill_multiset(Border, Multiset, MS),
   !.
fill_multiset(Border, [A:X, B:Y|Multiset], [A:X|MS]) :-
   B is A + 1,
   !,
   fill_multiset(Border, [B:Y|Multiset], MS),
   !.
fill_multiset(Border, [A:X, C:Z|Multiset], [A:X|MS]) :-
   A < C,
   B is A +1,
   !,
   fill_multiset(Border, [B:0, C:Z|Multiset], MS),
   !.
fill_multiset(_, [A:X], [A:X]) :-
   !.
fill_multiset(_, A, A) :-
   !.


/* assert_multiset(+A:B) <-
      */

assert_multiset(A:B) :-
   assert( multiset_elt(A, B) ).


/* count_elements(+Elt, -Elt:Sum) <-
      */

count_elements(Elt, Elt:Sum) :-
   findall( Amount,
      multiset_elt(Elt, Amount),
      Amounts ),
   sumlist(Amounts, Sum).


/*** tests ********************************************************/


/*** test, how long a list can be, so that multiset can manage it */

test :-
   reset_gensym(loop_),
   List = [a,b,c,s,d,e,f,r,t,z,u,i,t,r,f,g,h,j,z,t,r,e,d,f,g,h,b,
           v,c,x,s,w,e,r,t,g,f,d,e,a,b,c,s,d,e,f,r,t,z,u,i,t,r,f,
           g,h,j,z,t,r,e,d,f,g,h,b,v,c,x,s,w,e,r,t,g,f,d,e,a,b,c,
           s,d,e,f,r,t,z,u,i,t,r,f,g,h,j,z,t,r,e,d,f,g,h,b,v,c,x,
           s,w,e,r,t,g,f,d,e,a,b,c,s,d,e,f,r,t,z,u,i,t,r,f,g,h,j,
           z,t,r,e,d,f,g,h,b,v,c,x,s,w,e,r,t,g,f,d,e,a,b,c,s,d,e,
           f,r,t,z,u,i,t,r,f,g,h,j,z,t,r,e,d,f,g,h,b,v,c,x,s,w,e,
           r,t,g,f,d,e,a,b,c,s,d,e,f,r,t,z,u,i,t,r,f,g,h,j,z,t,r,
           e,d,f,g,h,b,v,c,x,s,w,e,r,t,g,f,d,e,a,b,c,s,d,e,f,r,t,
           z,u,i,t,r,f,g,h,j,t,r,e,d,f,g,h,b,v,c,x,s,w,e,r,t,g,f,
           d,e,a,b,c,s,d,e,f,r,t,z,u,i,t,r,f,g,h,j,z,t,r,e,d,f,g,
           h,b,v,c,x,s,w,e,r,t,g,f,d,e,a,b,c,s,d,e,f,r,t,z,u,i,t,
           r,f,g,h,j,z,t,r,e,d,f,g,h,b,v,c,x,s,w,e,r,t,g,f,d,e,a,
           b,c,s,d,e,f,r,t,z,u,i,t,r,f,g,h,j,z,t,r,e,d,f,g,h,b,v,
           c,x,s,w,e,r,t,g,f,d,e,a,b,c,s,d,e,f,r,t,z,u,i,t,r,f,g,
           h,j,z,t,r,e,d,f,g,h,b,v,c,x,s,w,e,r,t,g,f,d,e,a,b,c,s,
           d,e,f,r,t,z,u,i,t,r,f,g,h,j,z,t,r,e,d,f,g,h,b,v,c,x,s,
           w,e,r,t,g,f,d,e,a,b,c,s,d,e,f,r,t,z,u,i,t,r,f,g,h,j,z,
           t,r,e,d,f,g,h,b,v,c,x,s,w,e,r,t,g,f,d,e],
   test(List).

test(List) :-
   determine_runtime(
      ( length(List, Length),
        gensym(loop_, Loop),
        writeln(Loop:Length),
        multiset:list_to_multiset2(List, _Multiset),
        writeln('multiset done'),
        append(List, List, L2),
        writeln('lists appended') ) ),
   nl,
   writeln('---'),
   test(L2).


/******************************************************************/