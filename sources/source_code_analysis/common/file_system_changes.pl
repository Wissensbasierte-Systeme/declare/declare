


/******************************************************************/
/***                                                            ***/
/***          File System Changes - Protocol                    ***/
/***                                                            ***/
/******************************************************************/


:- module( file_system_changes, [
      fs_save_info/1,
      fs_changes/1 ]).

:- dynamic xml_file/2, xml_dir/2.


/*** interface ****************************************************/


/* fs_save_info(+Dir) <-
      */

fs_save_info(Dir) :-
   get_time_with_leading_zeros(Year, Month, Day,
      Hour, Minute, _, _),
   concat([dir, '_', Year, '-', Month, '-', Day, '_',
      Hour, Minute, '.xml'], File),
   file_system_to_xml_2(Dir, XML),
   dwrite(xml_2, File, XML),
   !.


/* fs_changes(+XML_File) <-
      */

fs_changes(XML_File) :-
   dread(xml, XML_File, [Xml]),
   xml_dirs_to_asserted_facts(Xml),
   findall( File,
      ( xml_file(File, _),
        not( exists_file_with_tilde(File) ) ),
      Deleted_Files ),
   findall( Dir,
      ( xml_dir(Dir, _),
        not( exists_directory(Dir) ) ),
      Deleted_Dirs ),
   findall( File,
      ( xml_file(File, Date_Org),
        rar:time_file_to_atom(File, Date_Actual),
        not( Date_Org = Date_Actual) ),
      Changed_Files ),
   findall( Dir,
      ( xml_dir(Dir, Date_Org),
        rar:time_file_to_atom(Dir, Date_Actual),
        not( Date_Org = Date_Actual) ),
      Changed_Dirs ),
   Xml = _:Attr:_,
   memberchk(path:Path, Attr),
   find_added_files(Path, Added_Dirs, Added_Files),
   retractall( xml_dir(_, _) ),
   retractall( xml_file(_, _) ),
   nl,
   writeln('Deleted Directories:'),
   writeln_list(Deleted_Dirs),
   nl,
   writeln('Deleted Files:'),
   writeln_list(Deleted_Files),
   writeln('---'),
   nl,
   writeln('Changed Directories:'),
   writeln_list(Changed_Dirs),
   nl,
   writeln('Changed Files:'),
   writeln_list(Changed_Files),
   writeln('---'),
   nl,
   writeln('Added Directories:'),
   writeln_list(Added_Dirs),
   nl,
   writeln('Added Files:'),
   writeln_list(Added_Files),
   writeln('---'),
   !.


/*** implementation ***********************************************/


/* find_added_files(+Directory, -Added_Dirs, -Added_Files) <-
      */

find_added_files(Directory, Added_Dirs_C, Added_Files_C) :-
   get(directory(Directory), directories, Directories_1),
   chain_list(Directories_1, Directories_2),
   get(directory(Directory), files, Files_1),
   chain_list(Files_1, Files_2),
   findall( D_2,
      ( member(D_1, Directories_2),
        concat_path([Directory, D_1], D_2),
        not( xml_dir(D_2, _) ) ),
      Added_Dirs_A ),
   findall( Path,
      ( member(File, Files_2),
        concat_path([Directory, File], Path),
        not( xml_file(Path, _) ) ),
      Added_Files_A ),
   findall( Added_Ds-Added_Fs,
      ( member(D_1, Directories_2),
        concat_path([Directory, D_1], D_2),
        find_added_files(D_2, Added_Ds, Added_Fs ) ),
      Addeds ),
   iterate_list( file_system_changes:ord_union_added,
      []-[], Addeds, Added_Dirs_B-Added_Files_B ),
   ord_union(Added_Dirs_A, Added_Dirs_B, Added_Dirs_C),
   ord_union(Added_Files_A, Added_Files_B, Added_Files_C).

ord_union_added(A_1-B_1, A_2-B_2, A_3-B_3) :-
   ord_union(A_1, A_2, A_3),
   ord_union(B_1, B_2, B_3).


/* file_system_to_xml_2(+Directory, -XML) <-
      */

file_system_to_xml_2(Directory, XML) :-
   file_system_to_xml_2_(Directory, XML).

file_system_to_xml_2_(Directory, XML) :-
   get(directory(Directory), directories, Directories_1),
   chain_list(Directories_1, Directories_2),
   get(directory(Directory), files, Files_1),
   chain_list(Files_1, Files_2),
   findall( dir:[path:D_1, date:Time_Atom_D]:Sub_Dirs,
      ( member(D_1, Directories_2),
        concat_path([Directory, D_1], D_2),
        file_system_to_xml_2_(D_2, dir:_:Sub_Dirs),
        rar:time_file_to_atom(D_2, Time_Atom_D) ),
      XML_Dirs ),
   findall( file:[path:File, date:Time_Atom_F]:[],
      ( member(File, Files_2),
        concat_path([Directory, File], Path),
        rar:time_file_to_atom(Path, Time_Atom_F) ),
      XML_Files ),
   append(XML_Dirs, XML_Files, Content),
   rar:time_file_to_atom(Directory, Time_Atom),
   XML = dir:[path:Directory, date:Time_Atom]:Content.


/* xml_dirs_to_asserted_facts(+Xml) <-
      */

xml_dirs_to_asserted_facts(Xml) :-
   retractall( xml_dir(_, _) ),
   retractall( xml_file(_, _) ),
   xml_dirs_to_asserted_facts('', Xml).

xml_dirs_to_asserted_facts(Path_1, file:Attr:_) :-
   memberchk(path:Path_2, Attr),
   memberchk(date:Date, Attr),
   concat_path(Path_1, Path_2, Path_3),
   assert( xml_file(Path_3, Date) ).
xml_dirs_to_asserted_facts(Path_1, dir:Attr:Cont) :-
   memberchk(path:Path_2, Attr),
   memberchk(date:Date, Attr),
   concat_path(Path_1, Path_2, Path_3),
   assert( xml_dir(Path_3, Date) ),
   checklist( xml_dirs_to_asserted_facts(Path_3),
      Cont ).


/******************************************************************/


