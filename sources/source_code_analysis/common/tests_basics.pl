

/******************************************************************/
/***                                                            ***/
/***         Source Code Analysis:  Tests                       ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* test_sca <-
      */

test_sca :-
   test_all(source_code_analysis).
%    test_all(gxl),
%    test_all(gxl_layout),
%    test_all(rar_new),
%    test_all(basics),
%    test_all(xml_io),
%    test_all(hierarchy_browser_ddk),
%    test_all(statistics),
%    test_all(slice).


test(source_code_analysis:basics, fn_to_xml_with_references) :-
   dislog_variable_get(sca, SCA_Path),
   concat(SCA_Path, 'common/tests/', Path),
   concat(Path, 'config_all.xml', File_1),
   concat(Path, 'config_ref.xml', File_2),
   dread_(xml(visur_config), File_1, X),
   dread_(xml(visur_config), File_2, Y),
   X = Y.


/******************************************************************/


