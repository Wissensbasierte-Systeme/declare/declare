

/******************************************************************/
/***                                                            ***/
/***       Source Code Analysis:  Basic Methods                 ***/
/***                                                            ***/
/******************************************************************/


:- module( file_alias, [
      history_file/1,
      history_file_to_fn_triple/1,
      alias_to_file/2,
      alias_to_files/2,
      alias_to_default_file/2,
      alias_to_fn_triple/2,
      alias_to_default_fn_triple/2,
      add_file_to_alias/2,
      delete_alias/1,
      delete_file_from_alias/2,
      add_alias_to_file_history/1,
      xml_file_to_pure_file/2 ]).


/*** interface ****************************************************/


/* history_file(-File) <-
      */

history_file(File) :-
   dislog_variable_get(configuration, Config_Path),
   !,
   concat(Config_Path, 'file_history/file_history.xml', File).


/* history_file_to_fn_triple(-FN_Triple) <-
      */

history_file_to_fn_triple(FN_Triple) :-
   history_file(File),
   ( exists_file_with_tilde(File) ->
     dread_(xml, File, [FN_Triple])
   ; FN_Triple = file_history:[]:[aliases:[]:[]] ),
   !.


/* alias_to_file(+Alias, -File) <-
      */

alias_to_file(Alias, File) :-
   alias_to_files(Alias, [File|_]).


/* alias_to_files(+Alias, -History) <-
      */

alias_to_files(Alias, Files_3) :-
   history_file_to_fn_triple(Xml),
   Xml_Alias := Xml^aliases^alias::[@name=Alias],
   findall( File,
      ( Xml_File := Xml_Alias^file,
        xml_file_to_pure_file(Xml_File, File) ),
      Files_1 ),
   sublist( exists_file_with_tilde,
      Files_1, Files_2 ),
   list_to_set(Files_2, Files_3),
   !.
alias_to_files(special_predicates, F) :-
   alias_to_files(predicate_groups, F).


/* alias_to_default_file(+Alias, -File) <-
      */

alias_to_default_file(Alias, File) :-
   history_file_to_fn_triple(Xml),
   Xml_File := Xml^aliases^alias::[@name=Alias]^file::[
      @type=default],
   xml_file_to_pure_file(Xml_File, File),
   !.


/* alias_to_fn_triple(+Alias, -FN_Triple) <-
      */

alias_to_fn_triple(Alias, FN_Triple) :-
   alias_to_file(Alias, File),
   ( ( dread_(xml(Alias), File, FN_Triple),
       ! )
   ; dread_(xml(dtd:[]:[]), File, FN_Triple) ).


/* alias_to_default_fn_triple(+Type, -FN_Triple) <-
      */

alias_to_default_fn_triple(Alias, FN_Triple) :-
   alias_to_default_file(Alias, File),
   dread_(xml(Alias), File, FN_Triple).


/* add_file_to_alias(+Alias, +File) <-
      */

add_file_to_alias(_Alias, _Prefix:_File) :-
   writeln('Relative files with prefix are not yet implemented!').
add_file_to_alias(Alias, File) :-
   exists_file_with_tilde(File),
   history_file_to_fn_triple(Xml_1),
   add_alias_to_file_history(Alias, Xml_1, Xml_2, Xml_Files_1),
   Xml_File = file:[path:File]:[],
   sublist( filter_repeated_file(Xml_File),
      Xml_Files_1, Xml_Files_2),
   sublist( filter_not_existing_file,
      Xml_Files_2, Xml_Files_3),
   Xml_Files_4 = [Xml_File|Xml_Files_3],
   Xml_3 := Xml_2*[^aliases^alias::[@name=Alias]:Xml_Files_4],
   Xml_3 = file_history:[]:[aliases:[]:Cont_1],
   list_to_ord_set(Cont_1, Cont_2),
   Xml_4 = file_history:[]:[aliases:[]:Cont_2],
   history_file(History_File),
   dwrite(xml_2, History_File, Xml_4).


/* delete_alias(+Alias) <-
      */

delete_alias(Alias) :-
   history_file_to_fn_triple(Xml_1),
   Xml_1 = file_history:Attr_A:[
      aliases:Attr_B:Cont_1],
   sublist( delete_alias_filter(Alias),
      Cont_1, Cont_2 ),
   Xml_2 = file_history:Attr_A:[
      aliases:Attr_B:Cont_2],
   history_file(History_File),
   dwrite(xml_2, History_File, Xml_2).


/* delete_file_from_alias(+Alias, +File) <-
      */

delete_file_from_alias(A, B) :-
   writeln_list([A, B]).


/* add_alias_to_file_history(+Alias) <-
      */

add_alias_to_file_history(Alias) :-
   history_file_to_fn_triple(Xml_1),
   add_alias_to_file_history(Alias, Xml_1, Xml_2),
   history_file(History_File),
   dwrite(xml_2, History_File, Xml_2).


/*** implementation ***********************************************/


/* add_alias_to_file_history(+Alias, +Xml_1, -Xml_2, -Xml_Files) <-
      */

add_alias_to_file_history(Alias, Xml_1, Xml_2, Xml_Files) :-
   add_alias_to_file_history(Alias, Xml_1, Xml_2),
   alias_to_files_xml(Alias, Xml_2, Xml_Files).


/* add_alias_to_file_history(+Alias, +Xml_1, -Xml_2) <-
      */

add_alias_to_file_history(Alias, Xml, Xml) :-
   Alias := Xml^aliases^alias@name,
   !.
add_alias_to_file_history(Alias, Xml_1, Xml_2) :-
   Xml_1 = file_history:Attr_A:[aliases:Attr_B:Cont],
   XML_Alias = alias:[name:Alias]:[],
   Xml_2 = file_history:Attr_A:[aliases:Attr_B:[XML_Alias|Cont]].


/* alias_to_files_xml(+Alias, +Xml, -Xml_Files) <-
      */

alias_to_files_xml(Alias, Xml, Xml_Files_3) :-
   Xml_Files_1 := Xml^aliases^alias::[@name=Alias]^content::'*',
   maplist( order_attributes,
      Xml_Files_1, Xml_Files_2 ),
   list_to_set(Xml_Files_2, Xml_Files_3).

order_attributes(Tag:Attr_1:Cont, Tag:Attr_2:Cont) :-
   list_to_ord_set(Attr_1, Attr_2).


/* filter_repeated_file(+Xml_File_1, +Xml_File_2) <-
      */

filter_repeated_file(file:Attr_A:[], file:Attr_B:[]) :-
   not( Attr_A = Attr_B ).


/* filter_not_existing_file(+XML_File) <-
      */

filter_not_existing_file(XML_File) :-
   xml_file_to_pure_file(XML_File, File),
   exists_file_with_tilde(File).


/* xml_file_to_pure_file(+Xml_File, -Pure_File) <-
      */

xml_file_to_pure_file(Xml, File_2) :-
   Prefix := Xml@prefix,
   File_1 := Xml@path,
   !,
   concat_prefix_and_suffix(Prefix, File_1, _, File_2),
   !.
xml_file_to_pure_file(Xml, File) :-
   File := Xml@path,
   !.


/* concat_prefix_and_suffix(+Alias, +Suffix, -Prefix, -File) <-
      */

concat_prefix_and_suffix(Alias, Suffix, Prefix, File) :-
   dislog_variable_get(Alias, Prefix),
   !,
   ( concat(_, '/', Prefix) ->
     concat(Prefix, Suffix, File)
   ; concat([Prefix, '/', Suffix], File) ),
   !.


/* delete_alias_filter(+Alias, +Element) <-
      */

delete_alias_filter(Alias, alias:Attr:_) :-
   not( memberchk(name:Alias, Attr) ).


/******************************************************************/


