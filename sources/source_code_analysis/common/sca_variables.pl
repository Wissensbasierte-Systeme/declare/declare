

/******************************************************************/
/***                                                            ***/
/***          SCA:  Global Variables                            ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      sca_variable/3,
      sca_namespace/1.


:- dislog_variable_get(home, Home),
   !,
   concat(Home, '/configuration/', Path),
   dislog_variable_set(configuration, Path).

:- dislog_variable_get(source_path, SP),
   !,
   concat(SP, 'source_code_analysis/', SCA_Path),
   concat(SCA_Path, 'configuration/', Config_Backup),
   dislog_variable_set(sca, SCA_Path),
   assert( sca_variable(sca, SCA_Path, _) ),
   assert( sca_variable(sca_config_backup, Config_Backup, _) ).


/*** interface ****************************************************/


sca_namespace(standard).


/* sca_namespace_set(+Task) <-
      */

sca_namespace_set(Task) :-
   retractall( sca_namespace(_) ),
   assert( sca_namespace(Task) ).


/* sca_namespace(-Task) <-
      */

sca_namespace_get(Task) :-
   sca_namespace(Task).


/* sca_variables <-
      */

sca_variables :-
   nl,
   sca_namespace_get(Task),
   sca_variable(Variable, Value, Task),
   write_list([Variable, ' = ', Value, '\n']),
   fail.
sca_variables.


/* sca_variable_get(+Variable, -Value) <-
      */

sca_variable_get(Variable, Value) :-
   sca_namespace_get(Task),
   sca_variable(Variable, Value, Task).


/* sca_variable_set(+Variable, +Value) <-
      */

sca_variable_set(Variable, Value) :-
   sca_namespace_get(Task),
   retractall( sca_variable(Variable, _, Task) ),
   asserta( sca_variable(Variable, Value, Task) ).


/* sca_variable_delete(Variable, Value) <-
      */

sca_variable_delete(Variable, Value) :-
   sca_namespace_get(Task),
   retractall( sca_variable(Variable, Value, Task) ).


/* sca_variable_switch(Variable, Old, New) <-
      */

sca_variable_switch(Variable, Old, New) :-
   sca_variable_get(Variable, Old),
   sca_variable_set(Variable, New).


/* sca_variable_increment(+Variable) <-
      */

sca_variable_increment(Variable) :-
   sca_variable_get(Variable, Old_Value),
   New_Value is Old_Value + 1,
   sca_variable_set(Variable, New_Value).


/*** implementation ***********************************************/


/******************************************************************/


