

/******************************************************************/
/***                                                            ***/
/***           DisLog:  additonal to Word Count                 ***/
/***                                                            ***/
/******************************************************************/

% /basic_algebra/utilities/word_count

/*** interface ****************************************************/


/* wc_bytes(+File, -Bytes) <-
      */

wc_bytes(File, Bytes) :-
   size_file(File, Bytes).


/* wc_lines(+File, -Lines) <-
      */

wc_lines(File_1, Lines) :-
   tilde_to_home_path(File_1, File_2),
   exists_file(File_2),
   open(File_2, read, Stream),
   wc_lines(Stream, 0, Lines),
   close(Stream),
   !.


/*** implementation ***********************************************/


/* wc_lines(+Stream, +Lines_1, -Lines_2) <-
      */

wc_lines(Stream, Lines_1, Lines_2) :-
   read_line_to_codes(Stream, Codes),
   wc_lines(Codes, Stream, Lines_1, Lines_2).

wc_lines(end_of_file, _, Lines, Lines) :-
   !.
wc_lines(_, Stream, Lines_1, Lines_3) :-
   Lines_2 is Lines_1 + 1,
   wc_lines(Stream, Lines_2, Lines_3).


/******************************************************************/


