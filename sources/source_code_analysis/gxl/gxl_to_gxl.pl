

/******************************************************************/
/***                                                            ***/
/***      GXL: Tests Basic Methods                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gxl_to_gxl(+Config, +Gxl_1, -Gxl_2) <-
      */

gxl_to_gxl(config:_:Cont, Gxl_1, Gxl_2) :-
   iterate_list( gxl_to_gxl_sub,
      Gxl_1, Cont, Gxl_2 ).

gxl_to_gxl_sub(Gxl_1, action:A:C, Gxl_2) :-
   !,
   gxl_to_gxl_1(action:A:C, Gxl_1, Gxl_2).
gxl_to_gxl_sub(Gxl, _, Gxl).


/* gxl_to_gxl(+Config, +Gxl_1, -Result, -Gxl_2) <-
      */

gxl_to_gxl(config:_:Cont, Gxl_1, Result, Gxl_2) :-
   member(action:A:C, Cont),
   gxl_to_gxl_1(action:A:C, Gxl_1, Result, Gxl_2).


/*** implementation ***********************************************/


gxl_to_gxl_1(action:Attr:Cont, Gxl_1, Gxl_2) :-
   memberchk(name:layout, Attr),
   !,
   gxl_graph_layout(config:[]:Cont,
      Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Gxl_2) :-
   memberchk(name:validate, Attr),
   !,
   gxl_to_gxl_valid(Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Gxl_2) :-
   memberchk(name:delete_prmtr_tags, Attr),
   !,
   gxl_delete_prmtr_tag(Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Gxl_2) :-
   memberchk(name:change_ids, Attr),
   !,
   change_ids_in_gxl(Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:Cont, Gxl_1, Gxl_2) :-
   memberchk(name:delete, Attr),
   !,
   findall( Id,
      ( member(_:A:_, Cont),
        memberchk(id:Id, A) ),
      Ids ),
   gxl_delete_elements(Ids, Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:Cont, Gxl_1, Gxl_2) :-
   memberchk(name:delete_single_nodes, Attr),
   !,
   gxl_to_gxl_1(action:Attr:Cont, Gxl_1, _, Gxl_2).
gxl_to_gxl_1(action:Attr:Cont, Gxl_1, Gxl_2) :-
   memberchk(name:separate_gxl_and_layout, Attr),
   !,
   gxl_to_gxl_1(action:Attr:Cont, Gxl_1, _, Gxl_2).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Gxl_2) :-
   memberchk(name:set_color, Attr),
   ( memberchk(mode:all_nodes, Attr)
   ; memberchk(mode:all_edges, Attr) ),
   !,
   memberchk(color:Color, Attr),
   gxl_color_set(nodes, Color, all, Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:Cont, Gxl_1, Gxl_2) :-
   memberchk(name:set_color, Attr),
   !,
   memberchk(mode:Mode, Attr),
   memberchk(color:Color, Attr),
   findall( Id,
      ( member(_:A:_, Cont),
        memberchk(id:Id, A) ),
      Ids ),
   gxl_color_set(Mode, Color, Ids, Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Gxl_2) :-
   memberchk(name:edge_weight, Attr),
   !,
   gxl_edge_weight(Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Gxl_2) :-
   memberchk(name:delete_light_weighted_edges, Attr),
   !,
   memberchk(limit:Limit, Attr),
   gxl_delete(light_weighted_edges, Limit, Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Gxl_2) :-
   memberchk(name:delete_colored_edges, Attr),
   !,
   memberchk(color:Color, Attr),
   gxl_delete(colored_edges, Color, Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Gxl_2) :-
   memberchk(name:scc, Attr),
   !,
   gxl_to_gxl_scc(Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:Cont, Gxl_1, Gxl_3) :-
   memberchk(name:highlight_scc, Attr),
   !,
   gxl_to_gxl_scc(Gxl_1, Gxl_2),
   gxl_scc_to_gxl_highlighted_scc(config:[]:Cont,
      Gxl_2, Gxl_3).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Gxl_2) :-
   memberchk(name:delete_loops, Attr),
   !,
   gxl_to_gxl_without_loops(Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Gxl_2) :-
   memberchk(name:delete_embedded_graphs, Attr),
   !,
   gxl_to_gxl_without_embedded_graphs(Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:Cont, Gxl_1, Gxl_2) :-
   memberchk(name:merge_nodes, Attr),
   !,
   gxl_to_gxl_with_contracted_nodes(config:[]:[
      action:Attr:Cont], Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:Cont, Gxl_1, Gxl_2) :-
   memberchk(name:highlight_tfbc_edges, Attr),
   !,
   gxl_to_gxl_highlighted_tfbc_edges(config:[]:Cont,
      Gxl_1, Gxl_2).
gxl_to_gxl_1(action:Attr:Cont, Gxl_1, Gxl_2) :-
   memberchk(name:delete_missing_references, Attr),
   !,
   gxl_to_gxl_1(action:Attr:Cont, Gxl_1, _, Gxl_2).


/* gxl_to_gxl_1(+Config, +Gxl_1, ?Result, -Gxl_2) <-
      */

gxl_to_gxl_1(action:Attr:_, Gxl_1, List, Gxl_2) :-
   memberchk(name:delete_single_nodes, Attr),
   !,
   gxl_to_gxl_without_single_nodes(Gxl_1, Gxl_2, List).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Layout, Gxl_2) :-
   memberchk(name:separate_gxl_and_layout, Attr),
   !,
   separate_gxl_from_layout(Gxl_1, Gxl_2, Layout).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Layout, Gxl_2) :-
   memberchk(name:merge_gxl_and_layout, Attr),
   !,
   merge_gxl_and_layout(Gxl_1, Layout, Gxl_2).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Config, Gxl_2) :-
   memberchk(name:call_weight, Attr),
   !,
   gxl_to_call_weight(Gxl_1, Gxl_2, Config).
gxl_to_gxl_1(action:Attr:_, Gxl_1, Edges, Gxl_2) :-
   memberchk(name:delete_missing_references, Attr),
   !,
   gxl_delete_missing_references(Gxl_1, Gxl_2, Edges).


gxl_to_call_weight(Gxl, Gxl, config:[]:[]).


/******************************************************************/


