

/******************************************************************/
/***                                                            ***/
/***           GXL: Basic Transformations                       ***/
/***                                                            ***/
/******************************************************************/


:- dynamic gxl_id_tmp/2.


/*** interface ****************************************************/


/* gxl_to_gxl_valid(Gxl_1, Gxl_2) <-
      */

gxl_to_gxl_valid(Tag:A_1:C_1, Tag:A_2:C_3) :-
   memberchk(Tag, [gxl, graph, edge, node]),
   gxl_element_to_element_complete(Tag:A_1:C_1, Tag:A_2:C_2),
   maplist( gxl_to_gxl_valid,
      C_2, C_3 ),
   !.
gxl_to_gxl_valid(Element, Element).


/* change_ids_in_gxl(+Gxl_1, -Gxl_2) <-
      */

change_ids_in_gxl(Gxl_1, Gxl_2) :-
   retractall( gxl_id_tmp(_, _) ),
   change_ids_in_gxl_sub(Gxl_1, Gxl_2),
   retractall( gxl_id_tmp(_, _) ).


/*** implementation ***********************************************/


/* change_ids_in_gxl_sub(+Gxl_1, -Gxl_2) <-
      */

change_ids_in_gxl_sub(T:A_1:C_1, T:A_2:C_2) :-
   maplist( change_gxl_id,
      A_1, A_2 ),
   maplist( change_ids_in_gxl_sub,
      C_1, C_2 ),
   !.
change_ids_in_gxl_sub(Element, Element).

change_gxl_id(Attr:Id_1, Attr:Id_2) :-
   memberchk(Attr, [from, to, id]),
   gxl_id_tmp(Id_1, Id_2),
   !.
change_gxl_id(Attr:Id_1, Attr:Id_2) :-
   memberchk(Attr, [from, to, id]),
   gensym(id_, Id_2),
   asserta( gxl_id_tmp(Id_1, Id_2) ),
   !.
change_gxl_id(A, A).


/******************************************************************/


