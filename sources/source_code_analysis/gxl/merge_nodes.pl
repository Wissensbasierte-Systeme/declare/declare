

/******************************************************************/
/***                                                            ***/
/***  GXL: Contract Components in GXL                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gxl_to_gxl_with_contracted_nodes(+Config, +Gxl_1, -Gxl_2) <-
      */

gxl_to_gxl_with_contracted_nodes(Config, Gxl_1, Gxl_3) :-
   assert_gxl_nodes_and_gxl_edges(Gxl_1),
   gxl_to_vertices_and_edges(node_id-v_w, Gxl_1, _, All_Edges),
   Grouped_Nodes := Config^action^components^content::'*',
   iterate_list( add_component_node,
      []-All_Edges, Grouped_Nodes,
      Outer_Gxl_Nodes-Remaining_Edges),
   Mode := Config^action^components@mode,
   change_edge_ids(Mode, Outer_Gxl_Nodes,
      Remaining_Edges, Outer_Gxl_Edges),
   gxl_element_to_element_complete(Gxl_1, Gxl_2),
   Gxl_2 = gxl:Attr_1:_,
   Prmtr := Gxl_2^prmtr,
   graph:Attr_2:_ := Gxl_2^graph,
   append(Outer_Gxl_Nodes, Outer_Gxl_Edges,
      Outer_Gxl_Nodes_Edges),
   Gxl_3 = gxl:Attr_1:[Prmtr, graph:Attr_2:Outer_Gxl_Nodes_Edges],
   retractall( generate_merge_conf:gxl_edge_2_tmp(_, _, _) ),
   retractall( generate_merge_conf:gxl_node_2_tmp(_, _) ),
   !.


/* v_w_to_gxl_edge_ids(+V_Ws, +Gxl, -Ids) <-
      */

v_w_to_gxl_edge_ids(V_Ws, Gxl, Ids) :-
   assert_gxl_nodes_and_gxl_edges(Gxl),
   findall( Id,
      ( member(V-W, V_Ws),
        generate_merge_conf:gxl_edge_2_tmp(V, W, edge:Attr:_),
        memberchk(id:Id, Attr) ),
      Ids ),
   retractall( generate_merge_conf:gxl_edge_2_tmp(_, _, _) ),
   retractall( generate_merge_conf:gxl_node_2_tmp(_, _) ).


/*** implementation ***********************************************/


/* change_edge_ids(+Mode, +Nodes, +Edges_1, -Edges_2) <-
      */

change_edge_ids(keep_uninvolved_edge_ids, _, Edges_1, Edges_3) :-
   maplist( v_w_to_gxl_edges,
      Edges_1, Edges_2),
   append(Edges_2, Edges_3),
   !.
change_edge_ids(change_uninvolved_edge_ids,
      Nodes, Edges_1, Edges_2) :-
   outer_nodes_and_remaining_edges_to_outer_gxl_edges(Nodes,
      Edges_1, Edges_2).


/* add_component_node(+Outer_Gxl_Nodes_1-Remaining_Edges,
      +[Node_Id], -Outer_Gxl_Nodes_2-Remaining_Edges) <-
      */

add_component_node(Outer_Gxl_Nodes_1-Remaining_Edges,
      C_Node, Outer_Gxl_Nodes_2-Remaining_Edges) :-
   [Node] := C_Node^nodes^content::'*',
   Node_Id := Node@id,
   generate_merge_conf:gxl_node_2_tmp(Node_Id, Gxl_Node_1),
%  Gxl_Node_2 := Gxl_Node_1*[@scc:false],
   change_fn_value(Gxl_Node_1, scc:false, Gxl_Node_2),
   Outer_Gxl_Nodes_2 = [Gxl_Node_2|Outer_Gxl_Nodes_1],
   !.
add_component_node(Outer_Gxl_Nodes_1-Remaining_Edges_1,
      C_Node, Outer_Gxl_Nodes_2-Remaining_Edges_2) :-
   findall( Node_Id,
      Node_Id := C_Node^nodes^node@id,
      Node_Ids ),
   ( foreach(Node_Id, Node_Ids),
     foreach(Gxl_Node, Inner_Gxl_Nodes) do
       generate_merge_conf:gxl_node_2_tmp(Node_Id, Gxl_Node) ),
   findall( V-W,
      ( member(V-W, Remaining_Edges_1),
        member(V, Node_Ids),
        member(W, Node_Ids) ),
      Inner_Edges ),
   maplist( v_w_to_gxl_edges,
      Inner_Edges, Inner_Gxl_Edges_1),
   append(Inner_Gxl_Edges_1, Inner_Gxl_Edges_2 ),
   subtract(Remaining_Edges_1, Inner_Edges, Remaining_Edges_2),
   append(Inner_Gxl_Nodes, Inner_Gxl_Edges_2, Inner_Gxl_Nodes_Edges),
   gensym(id7_, Graph_Id),
   Sub_Graph =
      graph:[id:Graph_Id, edgeids:true, edgemode:directed,
         hypergraph:false]:Inner_Gxl_Nodes_Edges,
   Prmtr := C_Node^prmtr,
   Outer_Gxl_Node_1 = node:[]:[Prmtr, Sub_Graph],
   gxl_element_to_element_complete(Outer_Gxl_Node_1,
      Outer_Gxl_Node_2),
%  Outer_Gxl_Node_3 := Outer_Gxl_Node_2*[@scc:true],
   change_fn_value(Outer_Gxl_Node_2, scc:true, Outer_Gxl_Node_3),
   Outer_Gxl_Nodes_2= [Outer_Gxl_Node_3|Outer_Gxl_Nodes_1],
   !.


/* v_w_to_gxl_edges(+(V-W), -Gxl_Edges) <-
      */

v_w_to_gxl_edges(V-W, Gxl_Edges) :-
   findall( Gxl_Edge,
      generate_merge_conf:gxl_edge_2_tmp(V, W, Gxl_Edge),
      Gxl_Edges ).


/* outer_nodes_and_remaining_edges_to_outer_gxl_edges(
      +Outer_Gxl_Nodes, +Remaining_Edges, -Outer_Gxl_Edges) <-
      */

outer_nodes_and_remaining_edges_to_outer_gxl_edges(Outer_Gxl_Nodes,
      Remaining_Edges, Outer_Gxl_Edges_5) :-
   maplist( outer_node_to_inner_nodes,
      Outer_Gxl_Nodes, Inner_Node_Table ),
   maplist( v_w_to_gxl_edges,
      Remaining_Edges, Outer_Gxl_Edges_1),
   append(Outer_Gxl_Edges_1, Outer_Gxl_Edges_2),
   maplist( change_from_and_to(from, Inner_Node_Table),
      Outer_Gxl_Edges_2, Outer_Gxl_Edges_3),
   maplist( change_from_and_to(to, Inner_Node_Table),
      Outer_Gxl_Edges_3, Outer_Gxl_Edges_4),
   findall(From-To,
      ( member(Edge, Outer_Gxl_Edges_4),
        [From, To] := Edge@[from, to] ),
      VWs_1 ),
   list_to_ord_set(VWs_1, VWs_2),
   findall( Edge,
      ( member(V-W, VWs_2),
        edges_to_contract_edge(Outer_Gxl_Edges_4, V, W, Edge) ),
      Outer_Gxl_Edges_5 ).

change_from_and_to(From_or_To, Table, Edge_1, Edge_2) :-
   X := Edge_1@From_or_To,
   member(Out-Inner, Table),
   member(X, Inner),
%  Edge_2 := Edge_1*[@From_or_To:Out],
   change_fn_value(Edge_1, From_or_To:Out, Edge_2),
   !.
change_from_and_to(_, _, Edge, Edge).

outer_node_to_inner_nodes(Gxl_Node, Id-Inner_Nodes) :-
   Id := Gxl_Node@id,
   findall( I,
      I := Gxl_Node^graph^node@id,
      Inner_Nodes ).

edges_to_contract_edge(Outer_Gxl_Edges, V, W, Edge) :-
   Dummy = edges:[]:Outer_Gxl_Edges,
   findall( X,
      ( Call := Dummy^edge::[@from=V, @to=W]^prmtr^calls^content::'*',
        member(X, Call) ),
      Calls ),
%  append(Calls, Calls_2),
   Edge = edge:[id:V-W, from:V, to:W]:[
      prmtr:[
         arrows:second, color:blue,
         mouse_click:file_file_edge ]:[
            string:[bubble:'']:[],
            calls:[]:Calls] ].


/******************************************************************/


