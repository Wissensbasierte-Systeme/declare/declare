

/******************************************************************/
/***                                                            ***/
/***      GXL: vertices_and_edges_to_gxl                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gxl_db_to_gxl(-GXL) <-
      */

gxl_db_to_gxl(GXL) :-
   writeln('Creating GXL document ... '),
   determine_runtime(
      ( findall( Node,
           rar:select(gxl_node, _, Node),
           Nodes ),
        findall( Edge,
           rar:select(gxl_edge,  _, _, Edge),
           Edges ),
        vertices_and_edges_to_gxl(Nodes, Edges, GXL) ) ),
   writeln(' ... done.').


/* vertices_and_edges_to_gxl(+Vertices, +Edges, -Gxl) <-
      */

vertices_and_edges_to_gxl(Vertices, Edges, Gxl) :-
   gxl_presetting(head, Head),
   Head = gxl:Attr_1:_,
   graph:Attr_2:_ := Head^graph,
   Prmtr := Head^prmtr,
   maplist( vertex_to_gxl_vertex_complete,
      Vertices, Gxl_Nodes ),
   maplist( edge_to_gxl_edge_complete,
      Edges, Gxl_Edges ),
   append(Gxl_Nodes, Gxl_Edges, Gxl_Nodes_Edges),
   Gxl = gxl:Attr_1:[Prmtr, graph:Attr_2:Gxl_Nodes_Edges],
   !.


/*** implementation ***********************************************/


/* vertex_to_gxl_vertex_complete(+Diverse_Parameters, -Node) <-
      */

vertex_to_gxl_vertex_complete(node:Attr:Content, Gxl_Node) :-
   !,
   gxl_element_to_element_complete(node:Attr:Content, Gxl_Node).
vertex_to_gxl_vertex_complete(
      Node_Id-Label_1-Symbol-Color-Size-(X, Y), Gxl_Node_2) :-
   gxl_label_to_list(Label_1, Label_2),
   gxl_presetting(node, Gxl_Node_1),
   term_to_atom(Label_1, Bubble),
   Gxl_Node_2 := Gxl_Node_1*[@id:Node_Id,
      ^prmtr@x_pos:X,
      ^prmtr@y_pos:Y,
      ^prmtr@color:Color,
      ^prmtr@symbol:Symbol,
      ^prmtr@size:Size,
      ^prmtr^string@bubble:Bubble,
      ^prmtr^string:Label_2],
   !.
vertex_to_gxl_vertex_complete(
      Node_Id-Label_1-Symbol-Color-Size, Gxl_Node_2) :-
   gxl_label_to_list(Label_1, Label_2),
   gxl_presetting(node, Gxl_Node_1),
   term_to_atom(Label_1, Bubble),
   Gxl_Node_2 := Gxl_Node_1*[@id:Node_Id,
      ^prmtr@color:Color,
      ^prmtr@symbol:Symbol,
      ^prmtr@size:Size,
      ^prmtr^string@bubble:Bubble,
      ^prmtr^string:Label_2],
   !.
vertex_to_gxl_vertex_complete(
      Node_Id-Label-Symbol-Color, Gxl_Node) :-
   vertex_to_gxl_vertex_complete(
      Node_Id-Label-Symbol-Color-medium, Gxl_Node).
vertex_to_gxl_vertex_complete(
      Node_Id-Label_1-(X, Y), Gxl_Node_2) :-
   gxl_label_to_list(Label_1, Label_2),
   gxl_presetting(node, Gxl_Node_1),
   term_to_atom(Label_1, Bubble),
   Gxl_Node_2 := Gxl_Node_1*[
      @id:Node_Id,
      ^prmtr@x_pos:X,
      ^prmtr@y_pos:Y,
      ^prmtr^string@bubble:Bubble,
      ^prmtr^string:Label_2],
   !.
vertex_to_gxl_vertex_complete(
      Node_Id-Color, Gxl_Node) :-
   vertex_to_gxl_vertex_complete(
      Node_Id-[Node_Id]-circle-Color-medium, Gxl_Node).
% new
vertex_to_gxl_vertex_complete(Node_Id, Gxl_Node_2) :-
   gxl_presetting(node, Gxl_Node_1),
   term_to_atom(Node_Id, Bubble),
   name_contains_name(Bubble, ':-'),
   name_exchange_sublist(
      [["~",""], ["not", "not "]], Bubble, Bubble_2),
   listvars_in_atom(Bubble_2, Bubble_3),
   Gxl_Node_2 := Gxl_Node_1*[
      @id:Node_Id,
      ^prmtr@symbol:triangle,
      ^prmtr@color:blue,
      ^prmtr@size:large,
      ^prmtr^string@bubble:Bubble_3,
      ^prmtr^string:[Bubble_3]],
   dwrite_(xml, user, Gxl_Node_2),
   !.
vertex_to_gxl_vertex_complete(Node_Id, Gxl_Node_2) :-
   gxl_presetting(node, Gxl_Node_1),
   term_to_atom(Node_Id, Bubble),
   name_contains_name(Bubble, 'not_'),
   name_exchange_sublist(
      [["not_", "not"]], Bubble, Bubble_2),
   Gxl_Node_2 := Gxl_Node_1*[
      @id:Node_Id,
      ^prmtr@symbol:circle,
      ^prmtr@color:orange,
      ^prmtr@size:large,
      ^prmtr^string@bubble:Bubble_2,
      ^prmtr^string:[Bubble_2]],
   dwrite_(xml, user, Gxl_Node_2),
   !.
vertex_to_gxl_vertex_complete(Node_Id, Gxl_Node_2) :-
   gxl_presetting(node, Gxl_Node_1),
   term_to_atom(Node_Id, Bubble),
   name_contains_name(Bubble, 'count'),
   listvars_in_atom(Bubble, Bubble_2),
   Gxl_Node_2 := Gxl_Node_1*[
      @id:Node_Id,
      ^prmtr@symbol:circle,
      ^prmtr@color:orange,
      ^prmtr@size:large,
      ^prmtr^string@bubble:Bubble_2,
      ^prmtr^string:[Bubble_2]],
   dwrite_(xml, user, Gxl_Node_2),
   !.
vertex_to_gxl_vertex_complete(Node_Id, Gxl_Node_2) :-
   gxl_presetting(node, Gxl_Node_1),
   term_to_atom(Node_Id, Bubble),
   Gxl_Node_2 := Gxl_Node_1*[
      @id:Node_Id,
      ^prmtr@symbol:circle,
      ^prmtr@color:red,
%     ^prmtr@size:large,
      ^prmtr@size:16,
      ^prmtr^string@bubble:Bubble,
      ^prmtr^string:[Node_Id]],
   dwrite_(xml, user, Gxl_Node_2),
   !.
% previously
vertex_to_gxl_vertex_complete(Node_Id, Gxl_Node_2) :-
   gxl_presetting(node, Gxl_Node_1),
   term_to_atom(Node_Id, Bubble),
   Gxl_Node_2 := Gxl_Node_1*[
      @id:Node_Id,
      ^prmtr@symbol:no_symbol,
      ^prmtr^string@bubble:Bubble,
      ^prmtr^string:[Node_Id]],
   dwrite_(xml, user, Gxl_Node_2),
   !.

dwrite_(xml, user, _).


/* listvars_in_atom(Atom_1, Atom_2) <-
      */

listvars_in_atom(Atom_1, Atom_2) :-
   name_exchange_sublist(
      [["#count","count"]], Atom_1, Atom_A),
   atom_to_term_atom_to_term(Atom_A, Term_A),
   term_copy_listvars(Term_A, Term_B),
   term_to_atom(Term_B, Atom_B),
   name_exchange_sublist([ ["count","#count"],
      ["\'X1\'","X"], ["\'X2\'","Y"], ["\'X3\'","Z"] ],
      Atom_B, Atom_2).

atom_to_term_atom_to_term(Atom, Term) :-
   term_to_atom(X, Atom),
   term_to_atom(Term, X).
 
term_copy_listvars(Term_1, Term_2) :-
   copy_term(Term_1, Term_2),
   listvars(Term_2, 1).


/* gxl_label_to_list(+Label_1, -Label_2) <-
      */

gxl_label_to_list(Label, Label) :-
   is_list(Label),
   !.
gxl_label_to_list(Label, [Label]).


/* edge_to_gxl_edge_complete(+Diverse_Parameters, -Edge) <-
      */


edge_to_gxl_edge_complete(edge:Attr:Content, Gxl_Edge) :-
   gxl_element_to_element_complete(edge:Attr:Content, Gxl_Edge),
   !.
edge_to_gxl_edge_complete(Paramtrs-Sec_Arrow, Gxl_Edge_3) :-
   Paramtrs = _-_-_-_-_-_,
   gxl_presetting(edge, Gxl_Edge_1),
   edge_to_gxl_edge_complete_sub(Gxl_Edge_1, Paramtrs, Gxl_Edge_2),
   Gxl_Edge_3 := Gxl_Edge_2*[^prmtr@second_arrow:Sec_Arrow],
   !.
edge_to_gxl_edge_complete(Paramtrs, Gxl_Edge_2) :-
   Paramtrs = _-_-_-_-_-_,
   gxl_presetting(edge, Gxl_Edge_1),
   edge_to_gxl_edge_complete_sub(Gxl_Edge_1, Paramtrs, Gxl_Edge_2).
edge_to_gxl_edge_complete(V-W-Color, Gxl_Edge_2) :-
   gxl_presetting(edge, Gxl_Edge_1),
   Gxl_Edge_2 := Gxl_Edge_1*[
      @from:V,
      @to:W,
      ^prmtr@color:Color],
   !.
edge_to_gxl_edge_complete(V-W, Gxl_Edge_2) :-
   gxl_presetting(edge, Gxl_Edge_1),
   Gxl_Edge_2 := Gxl_Edge_1*[
      @from:V,
      @to:W],
   !.

edge_to_gxl_edge_complete_sub(
      Gxl_Edge_1, Edge_Id-V-W-Color-Arrows-Pen, Gxl_Edge_2) :-
   Gxl_Edge_2 := Gxl_Edge_1*[
      @id:Edge_Id,
      @from:V,
      @to:W,
      ^prmtr@color:Color,
      ^prmtr@arrows:Arrows,
      ^prmtr@pen:Pen],
   !.


/******************************************************************/


