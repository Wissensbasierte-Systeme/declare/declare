

/******************************************************************/
/***                                                            ***/
/***      GXL: picture_to_gxl                          ***/
/***                                                            ***/
/******************************************************************/


:- dynamic picture_to_gxl_temp/2.


/*** interface ****************************************************/


/* picture_to_gxl(+Picture, -Gxl, -Config) <-
      */

picture_to_gxl(Picture, Gxl_3, Config) :-
   catch( get(Picture, config, Config), _,
      Config = config:[]:[] ),
   catch( get(Picture, xml, Gxl_1), _,
      gxl_presetting(head_pure, Gxl_1) ),
   retractall( picture_to_gxl_temp(_, _) ),
   picture_to_type(Picture, device),
   picture_to_type(Picture, connection),
   update_gxl(Gxl_1, Gxl_2),
   add_remaining_nodes_and_edges_to_gxl(Gxl_2, Gxl_3),
   !.


/*** implementation ***********************************************/


/* picture_to_type(+Picture, connection|device, +Gxl_1, -Gxl_2) <-
      */

picture_to_type(Picture, Type) :-
   get(Picture?graphicals, find_all,
      message(@arg1, instance_of, Type),
         Chain),
   chain_list(Chain, List),
   checklist( device_or_connection_to_gxl(Type),
      List ).


/* device_or_connection_to_gxl(device|connection, +Address) <-
      */

device_or_connection_to_gxl(device, Address) :-
   xpce_address_to_gxl_id(Address, Node_Id),
   get(Address, position, point(X_1, Y)),
   get(Address, size, size(Width, _)),
   X_2 is X_1 + Width/2,
   address_to_connected_text(Address, Text),
   assert( picture_to_gxl_temp(Node_Id, Text-(X_2, Y)) ),
   !.
device_or_connection_to_gxl(connection, Connection) :-
   xpce_address_to_gxl_id(Connection, Edge_Id),
   get(Connection, from, @V_1),
   get(Connection, to, @W_1),
   term_to_atom(V_2-_, V_1),
   term_to_atom(W_2-_, W_1),
   term_to_atom_sure(V_2, V_3),
   term_to_atom_sure(W_2, W_3),
   get(Connection, link, Link),
   get(Link, colour, Current_Color),
   get(Current_Color, name, Color),
   get(Link, arrows, Arrows),
   get(Link, pen, Pen),
   assert( picture_to_gxl_temp(Edge_Id, V_3-W_3-Color-Arrows-Pen) ),
   !.
device_or_connection_to_gxl(_, _).


/* update_gxl(+Element_1, -Element_2) <-
      */

update_gxl(node:Attr:Cont_1, node:Attr:Cont_3) :-
   memberchk(id:Id, Attr),
   picture_to_gxl_temp(Id, Text-(X, Y)),
   (node:Attr:Cont_2) := (node:Attr:Cont_1)*[
      ^prmtr@x_pos:X,
      ^prmtr@y_pos:Y],
%     ^prmtr^string:Text],
   retractall( picture_to_gxl_temp(Id, Text-(X, Y)) ),
   maplist( update_gxl,
      Cont_2, Cont_3 ),
   !.
update_gxl(edge:Attr_1:Cont_1, edge:Attr_2:Cont_3) :-
   memberchk(id:Id, Attr_1),
   picture_to_gxl_temp(Id, V-W-Color-Arrows-Pen),
   (edge:Attr_2:Cont_2) := (edge:Attr_1:Cont_1)*[
      @from:V,
      @to:W,
      ^prmtr@color:Color,
      ^prmtr@arrows:Arrows,
      ^prmtr@pen:Pen ],
   retractall( picture_to_gxl_temp(Id, V-W-Color-Arrows-Pen) ),
   maplist( update_gxl,
      Cont_2, Cont_3 ),
   !.
update_gxl(Tag:Attr:Cont_1, Tag:Attr:Cont_2) :-
   maplist( update_gxl,
      Cont_1, Cont_2 ),
   !.
update_gxl(Gxl, Gxl).


/* address_to_connected_text(+Address, -Text) <-
      */

address_to_connected_text(Address, Text) :-
   get(Address, contains, Chain_1),
   get(Chain_1, find, @arg1?name == device, Device),
   get(Device, contains, Chain_2),
   chain_list(Chain_2, List),
   findall( String,
      ( member(Element, List),
        get(Element, name, text),
        get(Element, string, String) ),
      Text ).


/* add_remaining_nodes_and_edges_to_gxl(+Gxl_1, -Gxl_2) <-
      */

add_remaining_nodes_and_edges_to_gxl(gxl:Attr_1:Cont_1,
      gxl:Attr_1:[Graph_2|Cont_1]) :-
   picture_to_gxl_temp(_, _),
   findall( (Node_Id, Text-(X, Y)),
      picture_to_gxl_temp(Node_Id, Text-(X, Y)),
      Nodes_1 ),
   findall( (Edge_Id, V-W-Color-Arrows-Pen),
      picture_to_gxl_temp(Edge_Id, V-W-Color-Arrows-Pen),
      Edges_1 ),
   maplist( convert_to_gxl_node,
      Nodes_1, Nodes_2 ),
   maplist( convert_to_gxl_edge,
      Edges_1, Edges_2 ),
   gensym(added_graph_id_, Id),
   gxl_presetting(graph, graph:Attr_2:_),
   append(Nodes_2, Edges_2, Content),
   Graph_1 := graph:Attr_2:Content,
   Graph_2 := Graph_1*[@id:Id, @added_graph:true].
add_remaining_nodes_and_edges_to_gxl(Gxl, Gxl).


/* convert_to_gxl_node(+(Node_Id, Text-(X, Y)), -Node) <-
      */

convert_to_gxl_node((Node_Id, Text-(X, Y)), Node_2) :-
   Node_1 = node:[id:Node_Id]:[
      prmtr:[x_pos:X, y_pos:Y]:[
         string:[]:Text] ],
   gxl_element_to_element_complete(Node_1, Node_2).


/* convert_to_gxl_edge(+(Id, V-W-Color-Arrows-Pen), -Edge) <-
      */

convert_to_gxl_edge((Id, V-W-Color-Arrows-Pen), Edge_2) :-
   Edge_1 = edge:[id:Id, from:V, to:W]:[
      prmtr:[arrows:Arrows,
         pen:Pen, color:Color]:[
         string:[]:[] ] ],
   gxl_element_to_element_complete(Edge_1, Edge_2).


/******************************************************************/


