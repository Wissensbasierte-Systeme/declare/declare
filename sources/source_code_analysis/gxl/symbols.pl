

/******************************************************************/
/***                                                            ***/
/***      GXL: Symbols                                          ***/
/***                                                            ***/
/******************************************************************/


:- pce_autoload(drag_and_drop_gesture, library(dragdrop)).
:- pce_global(@dragdevice_recogniser,
      make_dragdevice_recogniser).

:- multifile send_object_to_device/4.

:- discontiguous send_object_to_device/4.


/*** interface ****************************************************/


/* add_symbol_and_text_to_device(
      +Device, +[Size, Symbol, Color],
      +Font, +Texts, -Bitmap) <-
      */

add_symbol_and_text_to_device(Address, SSC, Font, Texts, Object) :-
   multi_line_text_field(Texts, Font, MLTF),
   send_object_to_device(Address, SSC, MLTF, Object),
   !.
add_symbol_and_text_to_device(_, _, _, _, _) :-
   writeln('failed in: add_symbol_and_text_to_device/5'),
   fail.

/*** implementation ***********************************************/


/* send_object_to_device(
      +Device, [+Size, +Symbol, +Color], +MLTF, -Bitmap) <-
      */

send_object_to_device(Device, [_, no_symbol, _], MLTF, Device_2) :-
   send(Device, display,
      new(Device_2, dragdevice)),
   send_object_to_device_sub(Device, Device_2, nil, MLTF).
send_object_to_device(Device, [Size, rhombus, Color],
      MLTF, Rhombus) :-
   send(Device, display,
      new(Drag_Device, dragdevice)),
   send(Drag_Device, display,
      new(Rhombus, path)),
   send_list(Rhombus, append, [
      point(0, Size/2),
      point(Size/2, 0),
      point(Size, Size/2),
      point(Size/2, Size),
      point(0, Size/2) ]),
   send_object_to_device_sub(Device, Rhombus, Color, MLTF).
send_object_to_device(Device, [Size, box, Color], MLTF, Box) :-
   send(Device, display,
      new(Drag_Device, dragdevice)),
   send(Drag_Device, display,
      new(Box, box(Size, Size))),
   send_object_to_device_sub(Device, Box, Color, MLTF).
send_object_to_device(Device,
      [Size, circle, Color], MLTF, Circle) :-
   send(Device, display,
      new(Drag_Device, dragdevice)),
   send(Drag_Device, display,
      new(Circle, circle(Size))),
   send_object_to_device_sub(Device, Circle, Color, MLTF).
send_object_to_device(Device,
      [Size, honeycomb, Color], MLTF, Honeycomb) :-
   send(Device, display,
      new(Drag_Device, dragdevice)),
   send(Drag_Device, display,
      new(Honeycomb, path)),
   S23 is (Size/3)*2,
   S13 is Size/3,
   send_list(Honeycomb, append, [
      point(S13, Size),
      point(S23, Size),
      point(Size, S23),
      point(Size, S13),
      point(S23, 0),
      point(S13, 0),
      point(0, S13),
      point(0, S23) ]),
   send_object_to_device_sub(Device, Honeycomb, Color, MLTF).
send_object_to_device(Device,
      [Size, triangle, Color], MLTF, Triangle) :-
   send(Device, display,
      new(Drag_Device, dragdevice)),
   send(Drag_Device, display,
      new(Triangle, path)),
   send_list(Triangle, append, [
      point(0, Size),
      point(Size/2, 0),
      point(Size, Size),
      point(0, Size) ]),
   send_object_to_device_sub(Device, Triangle, Color, MLTF).
send_object_to_device(Device, [_, text_in_ellipse, Color],
      MLTF, Box) :-
   get(MLTF, size, size(W, H)),
   send(Device, display,
      new(Box, ellipse(W + 14, H + 10))),
   send(Device, display,
      MLTF, point(7, 5)),
   send_color_to_object(Box, Color),
   new(_, constraint(Box, MLTF, identity(center_x))).
send_object_to_device(Device, [_, text_in_circle, Color],
      MLTF, Box) :-
   get(MLTF, size, size(W, H)),
   max([W, H], Max),
   send(Device, display,
      new(Box, circle(Max + 8))),
   send(Device, display,
      MLTF, point(7, 5)),
   send_color_to_object(Box, Color),
   new(_, constraint(Box, MLTF, identity(center_x))).
send_object_to_device(Device, [_, text_in_box, Color],
      MLTF, Box) :-
   get(MLTF, size, size(W, H)),
   send(Device, display,
      new(Box, box(W + 8, H + 4))),
   send(Box, radius, 10),
   send(Device, display,
      MLTF, point(4, 2)),
   send_color_to_object(Box, Color),
   new(_, constraint(Box, MLTF, identity(center_x))).
send_object_to_device(Device, [Size, Image_File, _Colour],
      MLTF, Bitmap) :-
   atom(Image_File),
   test_if_exists_image(Image_File),
   new(Image, image(Image_File)),
   xpce_picture:image_resize(Image, Size, Resized_Image),
   new(Bitmap, bitmap(Resized_Image)),
   send(Device, display, Bitmap),
   send(Device, center, Bitmap?center),
   send_object_to_device_sub(Device, Bitmap, nil, MLTF).

send_object_to_device_sub(Address, Object, Color, MLTF) :-
   ( Color = nil ->
     true
   ; send_color_to_object(Object, Color) ),
   get(Object, size, size(_, H)),
   send(Address, display, MLTF, point(0, H + 4)),
   new(_,
      constraint(MLTF, Object, identity(center_x))).

test_if_exists_image(Image_File) :-
   exists_file_with_tilde(Image_File),
   !.
test_if_exists_image(Image_File) :-
   pce_host:pce_home(PCE_Home),
   concat([PCE_Home, '/bitmaps/', Image_File], Path),
   exists_file_with_tilde(Path),
   !.
test_if_exists_image(Image_File) :-
   dislog_variable_get(image_directory, Home),
   !,
   concat(Home, Image_File, Path_and_File),
   exists_file_with_tilde(Path_and_File),
   !.


:- pce_begin_class(dragdevice, device).

make_dragdevice_recogniser(G) :-
   new(G, handler_group(drag_and_drop_gesture(left))).


drop(P, Gr:graphical, Pos:point) :->
   writeln(P-Gr-Pos),
   get(Pos, x, N),
   get(Pos, y, M),
   writeln(N-M),
   get(Gr, device, Dropped_Device),
   get(P, device, Device),
   get(Device, device, Drop_Picture),
   writeln(Dropped_Device-Device-Drop_Picture),
   get(Device, y, Y),
   get(Device, x, X),
   writeln(X-Y).


event(_B, Ev:event) :->
   send(@dragdevice_recogniser, event, Ev).

:- pce_end_class(dragdevice).


/******************************************************************/


