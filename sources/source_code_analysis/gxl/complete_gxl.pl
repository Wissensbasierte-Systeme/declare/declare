

/******************************************************************/
/***                                                            ***/
/***     GXL: Complete incomplete Gxl Documents                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gxl_element_to_element_complete(+Gxl_1, -Gxl_2) <-
      */

gxl_element_to_element_complete(Gxl_1, Gxl_2) :-
   ground(Gxl_1),
   Gxl_1 = gxl:_:_,
   get_element_prmtr(Gxl_1, label, Label),
   get_element_prmtr(Gxl_1, width, W),
   get_element_prmtr(Gxl_1, height, H),
   get_element_prmtr(Gxl_1, background, Background),
   get_element_prmtr(Gxl_1, mouse_click, Mouse_Click),
   Gxl_2 := Gxl_1*[
      ^prmtr@label:Label,
      ^prmtr@width:W,
      ^prmtr@height:H,
      ^prmtr@background:Background,
      ^prmtr@mouse_click:Mouse_Click],
   !.
gxl_element_to_element_complete(Gxl_Graph_1, Gxl_Graph_2) :-
   gxl_ids_to_valid_ids(graph, Gxl_Graph_1, Gxl_Graph_2).
gxl_element_to_element_complete(Gxl_Node_1, Gxl_Node_3) :-
   gxl_ids_to_valid_ids(node, Gxl_Node_1, Gxl_Node_2),
   get_element_prmtr(Gxl_Node_2, x_pos, X),
   get_element_prmtr(Gxl_Node_2, y_pos, Y),
   get_element_prmtr(Gxl_Node_2, color, Color),
   get_element_prmtr(Gxl_Node_2, symbol, Symbol),
   get_element_prmtr(Gxl_Node_2, size, Size),
   get_element_prmtr(Gxl_Node_2, string@font_style, Font_Style),
   get_element_prmtr(Gxl_Node_2, string, string:Attr:Label),
   get_element_prmtr(Gxl_Node_2, handles, Handle_Alias),
   get_element_prmtr(Gxl_Node_2, mouse_click, Mouse_Click_Alias),
   ( Bubble := (string:Attr:Label)@bubble,
     !
   ; term_to_atom(Label, Bubble),
     ! ),
   Gxl_Node_3 := Gxl_Node_2*[
      ^prmtr@x_pos:X,
      ^prmtr@y_pos:Y,
      ^prmtr@color:Color,
      ^prmtr@symbol:Symbol,
      ^prmtr@size:Size,
      ^prmtr@handles:Handle_Alias,
      ^prmtr@mouse_click:Mouse_Click_Alias,
      ^prmtr^string@font_style:Font_Style,
      ^prmtr^string@bubble:Bubble,
      ^prmtr^string:Label],
   !.
gxl_element_to_element_complete(Gxl_Edge_1, Gxl_Edge_3) :-
   gxl_ids_to_valid_ids(edge, Gxl_Edge_1, Gxl_Edge_2),
   get_element_prmtr(Gxl_Edge_2, color, Color),
   get_element_prmtr(Gxl_Edge_2, arrows, Arrows),
   get_element_prmtr(Gxl_Edge_2, pen, Pen),
   get_element_prmtr(Gxl_Edge_2, string@font_style, Font_Style),
   get_element_prmtr(Gxl_Edge_2, first_arrow, First_Arrow),
   get_element_prmtr(Gxl_Edge_2, second_arrow, Second_Arrow),
   get_element_prmtr(Gxl_Edge_2, weight, Weight),
   get_element_prmtr(Gxl_Edge_2, mouse_click, Mouse_Click_Alias),
   get_element_prmtr(Gxl_Edge_2, string, string:Attr:Label),
   ( Bubble := (string:Attr:Label)@bubble,
     !
   ; term_to_atom(Label, Bubble),
     ! ),
   Gxl_Edge_3 := Gxl_Edge_2*[
      ^prmtr@color:Color,
      ^prmtr@arrows:Arrows,
      ^prmtr@first_arrow:First_Arrow,
      ^prmtr@second_arrow:Second_Arrow,
      ^prmtr@weight:Weight,
      ^prmtr@mouse_click:Mouse_Click_Alias,
      ^prmtr^string@font_style:Font_Style,
      ^prmtr^string@bubble:Bubble,
      ^prmtr^string:Label,
      ^prmtr@pen:Pen],
   !.


/*** implementation ***********************************************/


/* gxl_ids_to_valid_ids(+Type, +Gxl_1, -Gxl_2) <-
      */

gxl_ids_to_valid_ids(Type, Gxl_1, Gxl_2) :-
   no_singleton_variable(Id),
   Gxl_1 = Type:A_1:Content,
   is_list(A_1),
   is_list(Content),
   maplist( gxl_id_to_valid_id,
      A_1, A_2 ),
   ( ( memberchk(id:Id, A_2),
       A_3 = A_2 )
   ; ( gensym(id_, Id),
       A_3 = [id:Id|A_2] ) ),
   Gxl_2 = Type:A_3:Content,
   !.

gxl_id_to_valid_id(Attr:Id_1, Attr:Id_2) :-
   atom(Attr),
   memberchk(Attr, [id, from ,to]),
   gxl_id_to_valid_id_sub(Id_1, Id_2),
   !.
gxl_id_to_valid_id(A, A).

gxl_id_to_valid_id_sub(Id, Id) :-
   atomic(Id),
   !.
gxl_id_to_valid_id_sub(Id_1, Id_2) :-
   var(Id_1),
   gensym(id_, Id_2),
   !.
gxl_id_to_valid_id_sub(Id_1, Id_2) :-
   term_to_atom(Id_1, Id_2).


/* get_element_prmtr(+Gxl, +Parameter, -Value) <-
      */


get_element_prmtr(Gxl, Prmtr, Value) :-
   get_element_prmtr(_, Gxl, Prmtr, Value).

get_element_prmtr(_, Gxl, string, Value) :-
   Value := Gxl^prmtr^string,
   !.
get_element_prmtr(_, node:A:C, string, Value) :-
   Id := (node:A:C)@id,
   gxl_presetting(node, Gxl),
   string:Attr:_ := Gxl^prmtr^string,
   Value = string:Attr:[Id],
   !.
get_element_prmtr(_, node:_:_, string, Value) :-
   gxl_presetting(node, Gxl),
   Value := Gxl^prmtr^string,
   !.
get_element_prmtr(_, Gxl, Parameter, Value) :-
   Value := Gxl^prmtr@Parameter,
   !.
get_element_prmtr(_, Gxl, Parameter, Value) :-
   Value := Gxl^prmtr^Parameter,
   !.
get_element_prmtr(_, _, Parameter, Value) :-
   memberchk(Parameter, [x_pos, y_pos]),
   random(0, 300, Value).
get_element_prmtr(_, Gxl, Parameter, Value) :-
   Value := Gxl@Parameter,
   !.
get_element_prmtr(false, Type:_:_, Parameter, Value) :-
   gxl_presetting(Type, Gxl),
   get_element_prmtr(true, Gxl, Parameter, Value),
   !.


/******************************************************************/


