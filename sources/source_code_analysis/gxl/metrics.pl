

/******************************************************************/
/***                                                            ***/
/***             GXL: Metric Methods                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gxl_edge_weight(+Gxl_1, -Gxl_2) <-
      */

gxl_edge_weight(Gxl_1, Gxl_2) :-
   findall( Id-Weight,
      ( Edge := Gxl_1^graph^edge,
        [Id, From, To] := Edge@[id, from, to],
        not(From = To),
        edge_weight(Edge, Weight) ),
      Id_Ws ),
   findall( W,
      member(_-W, Id_Ws),
      Ws ),
   sum(Ws, Sum),
   length(Ws, Length),
   ( not(Length = 0) ->
     Average is Sum / Length
   ; Average is 0 ),
   minimum(Ws, Min),
   maximum(Ws, Max),
   Lower is (Min + Average)/2,
   Upper is (Max + Average)/2,
   ( (Min = Max) ->
     Gxl_1 = Gxl_2
   ; iterate_list( set_gxl_edge_weight(Lower, Upper),
        Gxl_1, Id_Ws, Gxl_2 ) ),
   !.
gxl_edge_weight(Gxl, Gxl).


/* edge_weight_min_average_max(
      +Gxl, -Min, -Average, -Max) <-
      */

edge_weight_min_average_max(Gxl, Min, Average, Max) :-
   findall( Weight,
      ( Edge := Gxl^graph^edge,
        Id1-Id2 := Edge@id,
        not(Id1 = Id2),
        edge_weight(Edge, Weight) ),
      Ws ),
   sum(Ws, Sum),
   length(Ws, L),
   ( not(L = 0) ->
     Average is Sum / L
   ; Average = 0 ),
   minimum(Ws, Min),
   maximum(Ws, Max).


edge_weight_2(Edge_1, Edge_2) :-
   edge_weight(Edge_1, Weight),
   Edge_2 := Edge_1*[^prmtr@weight:Weight].



/*** implementation ***********************************************/


/* edge_weight(+Edge, -Weight) <-
      */

edge_weight(Edge, Weight) :-
   findall( X,
      ( Call := Edge^prmtr^calls^call^content::'*',
        member(X, Call) ),
      Calls),
%  append(Calls_1, Calls_2),
   length(Calls, Weight).


/* set_edge_weight(L, U, Gxl_1, Id-Weight, Gxl_2) <-
      */

set_gxl_edge_weight(L, U, Gxl_1, Id-Weight, Gxl_2) :-
   Weight >= L,
   Weight =< U,
   term_to_atom(calls:Weight, Bubble),
   Gxl_2 := Gxl_1*[^graph^edge::[@id=Id]^prmtr@weight:Weight,
      ^graph^edge::[@id=Id]^prmtr@pen:2,
      ^graph^edge::[@id=Id]^prmtr^string@bubble:Bubble],
   !.
set_gxl_edge_weight(_L, U, Gxl_1, Id-Weight, Gxl_2) :-
   Weight > U,
   term_to_atom(calls:Weight, Bubble),
   Gxl_2 := Gxl_1*[^graph^edge::[@id=Id]^prmtr@weight:Weight,
      ^graph^edge::[@id=Id]^prmtr@pen:3,
      ^graph^edge::[@id=Id]^prmtr^string@bubble:Bubble],
   !.
set_gxl_edge_weight(L, _, Gxl_1, Id-Weight, Gxl_2) :-
   Weight < L,
   term_to_atom(calls:Weight, Bubble),
   Gxl_2 := Gxl_1*[^graph^edge::[@id=Id]^prmtr@weight:Weight,
      ^graph^edge::[@id=Id]^prmtr@pen:1,
      ^graph^edge::[@id=Id]^prmtr^string@bubble:Bubble],
   !.


/******************************************************************/


