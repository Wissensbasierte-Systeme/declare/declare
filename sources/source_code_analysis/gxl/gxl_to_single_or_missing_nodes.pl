

/******************************************************************/
/***                                                            ***/
/***           GXL: GXL to single or missing nodes              ***/
/***                                                            ***/
/******************************************************************/


:- module(gxl_to_sm, [
      gxl_to_missing_nodes/2,
      gxl_to_single_nodes/2,
      gxl_to_gxl_without_single_nodes/2,
      gxl_to_gxl_without_single_nodes/3,
      gxl_to_node_types/6,
      gxl_delete_missing_references/3,
      gxl_to_edges_with_empty_reference/2 ] ).

:- dynamic
      gxl_node_id_tmp/1,
      gxl_node_id_deduced_tmp/1,
      gxl_edge_id_tmp/3.


/*** interface ****************************************************/


/* gxl_to_missing_nodes(+Gxl, -Missing_Nodes) <-
      */

gxl_to_missing_nodes(Gxl, Missing_Nodes) :-
   gxl_to_node_types(Gxl,
      Fathers, Embedded, Not_Embedded,
      Deduced_Nodes, D_Node_Fathers),
   ord_union(Deduced_Nodes, D_Node_Fathers, Deduced),
   ord_union([Fathers, Embedded, Not_Embedded],
      All_Nodes),
   subtract(Deduced, All_Nodes, Missing_Nodes).


/* gxl_to_single_nodes(+Gxl, -Single_Nodes) <-
      */

gxl_to_single_nodes(Gxl, Single_Nodes) :-
   gxl_to_node_types(Gxl, Fathers, _, Not_Embedded,
      Deduced_Nodes, D_Node_Fathers),
   ord_union(Fathers, Not_Embedded, All_Visible_Nodes),
   ord_union(Deduced_Nodes, D_Node_Fathers, Deduced),
   subtract(All_Visible_Nodes, Deduced, Single_Nodes).


/* gxl_to_gxl_without_single_nodes(+Gxl_1, -Gxl_2) <-
      */

gxl_to_gxl_without_single_nodes(Gxl_1, Gxl_2) :-
   gxl_to_gxl_without_single_nodes(Gxl_1, Gxl_2, _).


/* gxl_to_gxl_without_single_nodes(
      +Gxl_1, -Gxl_2, -Singletons) <-
      */

gxl_to_gxl_without_single_nodes(Gxl_1, Gxl_2, Singles) :-
   gxl_to_single_nodes(Gxl_1, Singles),
   gxl_delete(nodes, Singles, Gxl_1, Gxl_2).


/* gxl_delete_missing_references(+Gxl_1, +Gxl_2, -Edges) <-
      */

gxl_delete_missing_references(Gxl_1, Gxl_2, Edge_Ids) :-
   gxl_to_edges_with_empty_reference(Gxl_1, Edge_Ids),
   gxl_delete_elements(Edge_Ids, Gxl_1, Gxl_2).


/* gxl_to_edges_with_empty_reference(+Gxl, -Edges) <-
      */

gxl_to_edges_with_empty_reference(Gxl, Edges) :-
   delete_tmp_facts,
   assert_gxl_node_id_and_deduced_node_id(Gxl),
   findall( Id,
      ( gxl_to_sm:gxl_edge_id_tmp(Id, From, To),
        ( not( gxl_to_sm:gxl_node_id_tmp(From) )
        ; not( gxl_to_sm:gxl_node_id_tmp(To) ) ) ),
      Edges ).


/*** implementation ***********************************************/


/* gxl_to_node_types(+Gxl, -Fathers, -Embedded, -Not_Embedded,
      -Deduced_Nodes, -D_Nodes_Fathers) <-
      */

gxl_to_node_types(Gxl, Fathers_2, Embedded, Not_Embedded,
      Deduced_Nodes, D_Nodes_Fathers_2) :-
   delete_tmp_facts,
   forall( Gxl_Node := Gxl^graph^node,
      node_to_embedded_node_index(Gxl_Node) ),
   assert_gxl_node_id_and_deduced_node_id(Gxl),
   findall( Node,
      gxl_to_sm:gxl_node_id_tmp(Node),
      Nodes ),
   findall( Node,
      user:gxl_node_1_tmp(_, Node),
      Fathers_1 ),
   list_to_ord_set(Fathers_1, Fathers_2),
   findall( Node,
      user:gxl_node_1_tmp(Node, _),
      Embedded ),
   ord_union(Fathers_2, Embedded, Father_and_Embedded),
   subtract(Nodes, Father_and_Embedded, Not_Embedded),
   findall( Node,
      gxl_to_sm:gxl_node_id_deduced_tmp(Node),
      Deduced_Nodes ),
   findall( Father,
      ( gxl_to_sm:gxl_node_id_deduced_tmp(Node),
        user:gxl_node_1_tmp(Node, Father) ),
      D_Nodes_Fathers_1 ),
   list_to_ord_set(D_Nodes_Fathers_1, D_Nodes_Fathers_2),
   delete_tmp_facts.


/* assert_gxl_node_id_and_deduced_node_id(+Gxl) <-
      */

assert_gxl_node_id_and_deduced_node_id(node:A:C) :-
   memberchk(id:Id, A),
   assert( gxl_to_sm:gxl_node_id_tmp(Id) ),
   checklist( assert_gxl_node_id_and_deduced_node_id,
      C ),
   !.
assert_gxl_node_id_and_deduced_node_id(edge:A:C) :-
   memberchk(from:From, A),
   memberchk(to:To, A),
   memberchk(id:Id, A),
   ( not( gxl_to_sm:gxl_node_id_deduced_tmp(From) ) ->
     assert( gxl_to_sm:gxl_node_id_deduced_tmp(From) )
   ; true ),
   ( not( gxl_to_sm:gxl_node_id_deduced_tmp(To) ) ->
     assert( gxl_to_sm:gxl_node_id_deduced_tmp(To) )
   ; true ),
   assert( gxl_to_sm:gxl_edge_id_tmp(Id, From, To) ),
   checklist( assert_gxl_node_id_and_deduced_node_id,
      C ),
   !.
assert_gxl_node_id_and_deduced_node_id(_:_:C) :-
   checklist( assert_gxl_node_id_and_deduced_node_id,
      C ),
   !.
assert_gxl_node_id_and_deduced_node_id(_) :-
   !.


/* delete_tmp_facts <-
      */

delete_tmp_facts :-
   retractall( gxl_to_sm:gxl_node_id_deduced_tmp(_) ),
   retractall( gxl_to_sm:gxl_node_id_tmp(_) ),
   retractall( user:gxl_node_1_tmp(_, _) ),
   retractall( gxl_to_sm:gxl_edge_id_tmp(_, _, _) ).


/* gxl_to_single_nodes(+Gxl, -Single_Vertices) <-
      */
/*
gxl_to_single_nodes(Gxl, Single_Vertices) :-
   gxl_to_vertices_and_edges(
      node_id-v_w, Gxl, Vertices, Edges_1),
writelq(Vertices),nl,
   findall( V-W,
      ( member(V-W, Edges_1),
        not( V = W ) ),
      Edges_2 ),
   findall( N,
      ( member(N, Vertices),
        not( member(_-N, Edges_2) ),
        not( member(N-_, Edges_2) ) ),
      Single_Vertices ).
*/

/******************************************************************/


