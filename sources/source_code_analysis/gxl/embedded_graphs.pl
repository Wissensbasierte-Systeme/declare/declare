

/******************************************************************/
/***                                                            ***/
/***        Visualize Embedded Graph                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* show_embedded_graph(+Picture, +Node_Address) <-
      */

show_embedded_graph(Picture, Node_Address) :-
   get(Picture, xml, GXL),
   xpce_address_to_gxl_id(Node_Address, Node_Id),
   GXL_Node := GXL^_^node::[@id=Node_Id],
   show_embedded_graph_1(Picture, GXL_Node).


/* show_embedded_graph_1(+Picture, +GXL_Node) <-
      */

show_embedded_graph_1(Picture, GXL_Node) :-
   get(Picture, config, Config),
   Embedded_Gxl_Graph := GXL_Node^graph,
   gxl_presetting(head, gxl:Attr_1:_),
   Gxl = gxl:Attr_1:[Embedded_Gxl_Graph],
   gxl_to_picture(Config, Gxl, _Picture1).


/* show_embedded_graph_2(+Picture, +Gxl_Node) <-
      */

show_embedded_graph_2(Picture, GXL_Node) :-
   get(Picture, xml, GXL),
   Embedded_Gxl_Graph_1 := GXL_Node^graph,
   find_incoming_and_outgoing_nodes_and_edges(GXL,
      Embedded_Gxl_Graph_1, Incoming_Outgoing_Nodes_Edges),
   gxl_presetting(head, gxl:Attr_1:_),
   Embedded_Gxl_Graph_1 = graph:Attr_Graph:Content_Graph_1,
   append(Content_Graph_1, Incoming_Outgoing_Nodes_Edges,
      Content_Graph_2),
   Embedded_Gxl_Graph_2 = graph:Attr_Graph:Content_Graph_2,
   Gxl = gxl:Attr_1:[Embedded_Gxl_Graph_2],
   get(Picture, config, Config),
   gxl_to_picture(Config, Gxl, _Picture1).


/* show_embedded_graph_3(+Picture, +Gxl_Node) <-
      */

show_embedded_graph_3(Picture, Gxl_Node) :-
   get(Picture, xml, Gxl),
   Embedded_Gxl_Graph_1 := Gxl_Node^graph,
   generate_in_out_super_nodes(Gxl,
      Embedded_Gxl_Graph_1, In_Out_Super_Nodes),
   gxl_presetting(head, gxl:Attr_1:_),
   Embedded_Gxl_Graph_1 = graph:Attr_Graph:Content_Graph_1,
   append(Content_Graph_1, In_Out_Super_Nodes,
      Content_Graph_2),
   Embedded_Gxl_Graph_2 = graph:Attr_Graph:Content_Graph_2,
   Gxl_Embedded = gxl:Attr_1:[Embedded_Gxl_Graph_2],
   get(Picture, config, Config),
   dwrite(xml_2, 'xxx.xml', Gxl_Embedded),
   gxl_to_picture(Config, Gxl_Embedded, _Picture1).


/* extract_embedded_graph(+Picture, +Gxl_Node) <-
      */

extract_embedded_graph(Picture, Gxl_Node) :-
   get(Picture, xml, (gxl:Attr:Content_1)),
   Embedded_Gxl_Graph := Gxl_Node^graph,
   findall( Edge,
      Edge := Embedded_Gxl_Graph^edge,
      Gxl_Edges_1 ),
   findall( Node,
      Node := Embedded_Gxl_Graph^node,
      Gxl_Nodes_1 ),
   maplist( change_color(white),
      Gxl_Nodes_1, Gxl_Nodes_2 ),
   maplist( change_color(blue),
      Gxl_Edges_1, Gxl_Edges_2 ),
   member((graph:Attr_Graph:Cont_Graph_1), Content_1),
   subtract(Content_1, [graph:Attr_Graph:Cont_Graph_1],
      Content_2),
   Gxl_Node = node:Node_Attr:_,
   subtract(Cont_Graph_1, [node:Node_Attr:_], Cont_Graph_2),
   append([Cont_Graph_2, Gxl_Edges_2, Gxl_Nodes_2], Cont_Graph_3),
   append(Content_2, [(graph:Attr_Graph:Cont_Graph_3)], Content_3),
   get(Picture, config, Config),
   gxl_to_picture(Config, (gxl:Attr:Content_3), _).


/*** implementation ***********************************************/


/* generate_in_out_super_nodes(+Gxl, +Graph, -In_Out_Nodes) <-
      */

generate_in_out_super_nodes(Gxl, Graph, In_Out_Nodes) :-
   assert_gxl_nodes_and_gxl_edges(Gxl),
   gxl_to_vertices_and_edges(node_id-v_w,
      (gxl:[]:[Graph]), Nodes, _),
   find_nodes_and_edges(incoming, Nodes,
      From_Gxl_Nodes, In_Gxl_Edges_1),
   maplist( change_color(blue),
      In_Gxl_Edges_1, In_Gxl_Edges_2),
   find_nodes_and_edges(outgoing, Nodes,
      To_Gxl_Nodes, Out_Gxl_Edges_1),
   maplist( change_color(blue),
      Out_Gxl_Edges_1, Out_Gxl_Edges_2),
   gensym(id_, Extern_Id),
   ord_union_2(To_Gxl_Nodes, From_Gxl_Nodes, Extern_Gxl_Nodes),
   Extern_Node = node:[id:Extern_Id]:[
      prmtr:[
          color:white, handles:default,
          mouse_click:embedded_node, size:large,
          symbol:honeycomb]:[string:[
          bubble:'',
          font_style: (helvetica, roman, 12)]:
          ['Incoming and Outgoing', 'Connections']],
      graph:[]:Extern_Gxl_Nodes ],
   append(Out_Gxl_Edges_2, In_Gxl_Edges_2, Edges),
   In_Out_Nodes = [Extern_Node|Edges],
   retractall( generate_merge_conf:gxl_edge_2_tmp(_, _, _) ),
   retractall( generate_merge_conf:gxl_node_2_tmp(_, _) ).


/* find_incoming_and_outgoing_nodes_and_edges(+Gxl,
      +Graph, -Incoming_Outgoing_Nodes_Edges) <-
      */

find_incoming_and_outgoing_nodes_and_edges(Gxl,
      Graph, Incoming_Outgoing_Nodes_Edges) :-
   assert_gxl_nodes_and_gxl_edges(Gxl),
   gxl_to_vertices_and_edges(node_id-v_w,
      (gxl:[]:[Graph]), Nodes, _Edges),
   find_nodes_and_edges(incoming, Nodes,
      From_Gxl_Nodes, In_Gxl_Edges),
   find_nodes_and_edges(outgoing, Nodes,
      To_Gxl_Nodes, Out_Gxl_Edges),
   ord_union_2(In_Gxl_Edges, Out_Gxl_Edges, Extern_Gxl_Edges_1),
   ord_union_2(From_Gxl_Nodes, To_Gxl_Nodes, Extern_Gxl_Nodes_1),
   maplist( change_color(white),
      Extern_Gxl_Nodes_1, Extern_Gxl_Nodes_2 ),
   maplist( change_color(blue),
      Extern_Gxl_Edges_1, Extern_Gxl_Edges_2 ),
   append(Extern_Gxl_Edges_2, Extern_Gxl_Nodes_2,
      Incoming_Outgoing_Nodes_Edges),
   retractall( generate_merge_conf:gxl_edge_2_tmp(_, _, _) ),
   retractall( generate_merge_conf:gxl_node_2_tmp(_, _) ).


/* find_nodes_and_edges(incoming|outgoing, +Nodes,
      -Gxl_Nodes, -Gxl_Edges) <-
      */

find_nodes_and_edges(incoming, Nodes, Gxl_Nodes, Gxl_Edges) :-
   findall( Gxl_Edge,
      ( member(W, Nodes),
        generate_merge_conf:gxl_edge_2_tmp(V, W, Gxl_Edge),
        not( member(V, Nodes ) ) ),
      Gxl_Edges ),
   find_nodes_and_edges(from, Gxl_Edges, Gxl_Nodes).
find_nodes_and_edges(outgoing, Nodes, Gxl_Nodes, Gxl_Edges) :-
   findall( Gxl_Edge,
      ( member(V, Nodes),
        generate_merge_conf:gxl_edge_2_tmp(V, W, Gxl_Edge),
        not( member(W, Nodes ) ) ),
      Gxl_Edges ),
   find_nodes_and_edges(to, Gxl_Edges, Gxl_Nodes).

find_nodes_and_edges(Attr, Gxl_Edges, Gxl_Nodes) :-
   findall( Node_Id,
      ( member(Edge, Gxl_Edges),
        Node_Id := Edge@Attr ),
      Extern_Node_Ids),
   findall( Gxl_Node,
      ( member(Node_Id, Extern_Node_Ids),
        generate_merge_conf:gxl_node_2_tmp(Node_Id, Gxl_Node) ),
      Gxl_Nodes ).


/* change_color(+Color, +Element_1, -Element_2) <-
      */

change_color(Color, Element_1, Element_2) :-
   Element_2 := Element_1*[^prmtr@color:Color,
      ^prmtr@pen:1].


/******************************************************************/


