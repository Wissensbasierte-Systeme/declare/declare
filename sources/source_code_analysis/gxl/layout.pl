

/******************************************************************/
/***                                                            ***/
/***      GXL: Layout                                           ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      xpce_graph_layout/2.
:- discontiguous
      xpce_graph_layout/2.
:- dynamic
      gxl_layout_position_tmp/2.


/*** interface ****************************************************/


/* gxl_graph_layout(+Config, +Gxl_1, -Gxl_2) <-
      */

gxl_graph_layout_2(Config, Vs, Gxl_1, Gxl_2) :-
   ground(Config),
   Mode := Config^gxl_layout@mode,
   gxl_graph_layout_2(Config, Mode, Vs, Gxl_1, Gxl_2).

gxl_graph_layout(Config, Gxl_1, Gxl_2) :-
   ground(Config),
   Mode := Config^gxl_layout@mode,
   findall( R,
      R := Config^gxl_layout^node@id,
      Roots ),
   ( Roots = [] ->
     gxl_graph_layout(Config, Mode, _, Gxl_1, Gxl_2)
   ; gxl_graph_layout(Config, Mode, Roots, Gxl_1, Gxl_2) ).


/* gxl_graph_layout(+Config, +Mode, ?Roots, +Gxl_1, -Gxl_2) <-
      */

gxl_graph_layout(_, Mode, _, Gxl_1, Gxl_2) :-
   member(Mode, [dot, neato, twopi, circo, fdp]),
   layout_graphviz(Mode, Gxl_1, Gxl_2),
   !.
gxl_graph_layout(Config, Mode_1, Roots, Gxl_1, Gxl_2) :-
   ground(Config),
   ground(Gxl_1),
   [X_Start, Y_Start, X_Step_1, Y_Step_1] :=
      Config^gxl_layout@[x_start, y_start, x_step, y_step],
   determine_mode(Mode_1, X_Step_1, Y_Step_1,
      Mode_2, X_Step_2, Y_Step_2, X_Pos, Y_Pos),
   gxl_to_vertices_and_edges(
      node_id-v_w, Gxl_1, Vertices, Edges),
   ( var(Roots),
     vertices_and_edges_to_possible_roots(Vertices, Edges, Roots)
   ; true ),
   ( var(Roots) ->
     Roots = []
   ; true ),
   tree_traversals:traverse(Mode_2, Roots, Edges, Tree_Edges),
   retractall( tree_traversals:node_visited(_) ),
   retractall( tree_traversals:edge_tmp(_, _) ),
   forall( member(V-W, Tree_Edges),
      assert( tree_traversals:edge_tmp(V, W) )  ),
   retractall( gxl_layout_position_tmp(_, _) ),
   !,
   iterate_list( gxl_graph_layout((X_Step_2, Y_Step_2)),
      (X_Start, Y_Start), Roots, (_, _) ),
   add_layout_information_to_gxl((X_Pos, Y_Pos), Gxl_1, Gxl_2),
   !.

gxl_graph_layout_2(Config, Mode_1, Vs, Gxl_1, Gxl_2) :-
   ground(Config),
   ground(Gxl_1),
   [X_Start, Y_Start, X_Step_1, Y_Step_1] :=
      Config^gxl_layout@[x_start, y_start, x_step, y_step],
   determine_mode(Mode_1, X_Step_1, Y_Step_1,
      Mode_2, X_Step_2, Y_Step_2, X_Pos, Y_Pos),
   gxl_to_vertices_and_edges(
      node_id-v_w, Gxl_1, Vertices, Edges),
   vertices_and_edges_to_possible_roots(Vertices, Edges, Ws),
   intersection(Vs, Ws, Roots),
%  writelnq(Roots), ttyflush,
   tree_traversals:traverse(Mode_2, Roots, Edges, Tree_Edges),
   retractall( tree_traversals:node_visited(_) ),
   retractall( tree_traversals:edge_tmp(_, _) ),
   forall( member(V-W, Tree_Edges),
      assert( tree_traversals:edge_tmp(V, W) )  ),
   retractall( gxl_layout_position_tmp(_, _) ),
   !,
   iterate_list( gxl_graph_layout((X_Step_2, Y_Step_2)),
      (X_Start, Y_Start), Roots, (_, _) ),
   add_layout_information_to_gxl((X_Pos, Y_Pos), Gxl_1, Gxl_2),
   !.


/* xpce_graph_layout(+Mode, +Picture) <-
      */

xpce_graph_layout(xpce, Picture) :-
   !,
   send(Picture?graphicals, for_all,
      if(message(@arg1, instance_of, device),
         message(@prolog, xpce_graph_layout,
            @arg1))).
xpce_graph_layout(Mode, Picture) :-
   memberchk(Mode, [w, wielemaker]),
   !,
   xpce_graph_layout(xpce, Picture).
xpce_graph_layout(Mode, Picture) :-
   picture_to_gxl(Picture, Gxl_1, Config),
   gxl_graph_layout(Config, Mode, _Rs, Gxl_1, Gxl_2),
   xpce_picture_clear(Picture),
   gxl_to_picture(Config, Gxl_2, Picture).


/* gxl_picture_update(Picture_1, Picture_2) <-
      */

gxl_picture_update(Picture_1, Picture_2) :-
   picture_to_gxl(Picture_1, Gxl_1, _),
   Config = config:[]:[
      gxl_layout:[
         x_start:70, y_start:70,
         x_step:100,  y_step:170,  y_variance:10]:[]],
   gxl_graph_layout(Config, dfs, _Rs, Gxl_1, Gxl_2),
   dwrite(xml, gxl_1, Gxl_1),
   dwrite(xml, gxl_2, Gxl_2),
   gxl_to_picture(Config, Gxl_2, Picture_2).

gxl_picture_update(Roots, Picture_1, Picture_2) :-
   picture_to_gxl(Picture_1, Gxl_1, _),
   Config = config:[]:[
      gxl_layout:[
         x_start:70, y_start:70,
         x_step:200,  y_step:70,  y_variance:10]:[]],
   gxl_graph_layout(Config, dfs, Roots, Gxl_1, Gxl_3),
%  gxl_graph_layout(Config, dfs, _, Gxl_2, Gxl_3),
%  dwrite(xml, gxl_1, Gxl_1),
%  dwrite(xml, gxl_2, Gxl_2),
%  Gxl_1 = Gxl_2,
   gxl_to_picture(Config, Gxl_3, Picture_2).


/*** implementation ***********************************************/


/* split_mode(+Mode_1, +X_Step, +Y_Step,
      -Mode_2, -X_Step_inv, -Y_Step_inv, -X_Pos, -Y_Pos) :-
      */

determine_mode(Mode_1, X_Step, Y_Step,
      Mode_2, Y_Step, X_Step, X_Pos, Y_Pos) :-
   concat_atom([Mode_2, '_inv'], Mode_1),
   Y_Pos = x_pos,
   X_Pos = y_pos,
   !.
determine_mode(Mode, X_Step, Y_Step,
      Mode, X_Step, Y_Step, X_Pos, Y_Pos) :-
   X_Pos = x_pos,
   Y_Pos = y_pos.


/* gxl_graph_layout(+Tree_Edges-(X_Step, Y_Step)-(X, Y),
      +(X_1, Y_1), +Node, -(X_2, Y_2)) <-
      */

gxl_graph_layout((X_Step, _), (X_1, Y_1), Node, (X_2, Y_1)) :-
   tree_traversals:node_to_sons(Node, Sons),
   Sons = [],
   assert( gxl_layout_position_tmp(Node, (X_1, Y_1)) ),
   X_2 is X_1 + X_Step,
   !.
gxl_graph_layout((X_Step, Y_Step), (X_1, Y_1), Node, (X_2, Y_1)) :-
   tree_traversals:node_to_sons(Node, Sons),
   Y_2 is Y_1 + Y_Step,
   iterate_list( gxl_graph_layout((X_Step, Y_Step)),
      (X_1, Y_2), Sons, (X_2, _) ),
   first(Sons, First_Son),
   last(Sons, Last_Son),
   gxl_layout_position_tmp(First_Son, (X_First, _)),
   gxl_layout_position_tmp(Last_Son, (X_Last, _)),
   X_3 is (X_First + X_Last)/2,
   assert( gxl_layout_position_tmp(Node, (X_3, Y_1)) ),
   !.


/* xpce_graph_layout(Node) <-
      */

xpce_graph_layout(Node) :-
   catch(
      send(Node, layout, 10, 100, 6, 7, 8),
%     send(Node, layout, 5, 30, 6, 7, 8),
%     send(Node, layout, 30, 50, 6, 10, 150),
      writeln(error),
      fail ).


/* vertices_and_edges_to_possible_roots(
      +Vertices, +Edges, -Roots) <-
      */

vertices_and_edges_to_possible_roots(Vertices_1, Edges_1, Roots) :-
   findall( V-W,
      ( member(V-W, Edges_1),
        not( V = W ) ),
      Edges_2 ),
   findall( V,
      ( member(V, Vertices_1),
        \+ member(_-V, Edges_2) ),
      Roots_1 ),
%  nodes_to_not_called_nodes(Vertices_1, Edges_2, Nodes_3),
   vertices_edges_to_ugraph(Vertices_1, Edges_1, Graph_1),
   reduce(Graph_1, Graph_2),
   top_sort(Graph_2, [Roots_2|_]),
   ord_union_2(Roots_1, Roots_2, Roots_3),
%  ord_union_2(Nodes_3, Roots_3, Roots_4),
   sort(Roots_3, Roots).


/* nodes_to_not_called_nodes(Nodes_1, Edges, Nodes_3) <-
      */

nodes_to_not_called_nodes(Nodes_1, Edges, Nodes_3) :-
   maplist( nodes_to_number_of_calls(Edges),
      Nodes_1, Nodes_2),
   findall( N,
      member(N:0, Nodes_2),
      Nodes_3 ).


/* nodes_to_number_of_calls(Edges, Node, Node:In_Calls) <-
      */

nodes_to_number_of_calls(Edges, Node, Node:In_Calls) :-
   findall( Node,
      ( member(V-Node, Edges),
        not( V = Node ) ),
      Calls ),
   length(Calls, In_Calls).


/* add_layout_information_to_gxl(+(X_Pos, Y_Pos), +Gxl_1, -Gxl_2) <-
      */

add_layout_information_to_gxl(_, prmtr:A:C, prmtr:A:C) :-
   !.
add_layout_information_to_gxl((X_Pos, Y_Pos), Gxl_1, Gxl_2) :-
   Id := Gxl_1@id,
   gxl_layout_position_tmp(Id, (X, Y)),
   Gxl_2 := Gxl_1*[^prmtr@X_Pos:X, ^prmtr@Y_Pos:Y],
   !.
add_layout_information_to_gxl((X_Pos, Y_Pos), T:A:C_1, T:A:C_2) :-
   maplist( add_layout_information_to_gxl((X_Pos, Y_Pos)),
      C_1, C_2),
   !.
add_layout_information_to_gxl((_, _), Gxl, Gxl).


/******************************************************************/


