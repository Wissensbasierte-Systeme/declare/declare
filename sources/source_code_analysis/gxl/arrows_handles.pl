

/******************************************************************/
/***                                                            ***/
/***      GXL: Arrows, Handles and Labels                       ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


/* add_arrow_to_link(+Config, +Gxl_Edge, +Link, +Arrows) <-
      */

add_arrow_to_link(Config, Gxl_Edge, Link, first) :-
   Arrow_Alias := Gxl_Edge^prmtr@first_arrow,
   XML_Arrow_1 := Config^arrows^arrow::[@alias=Arrow_Alias],
   xml_arrow_to_color_pen(XML_Arrow_1, Gxl_Edge, XML_Arrow_2),
   add_arrow_to_link(XML_Arrow_2, Link, first_arrow),
   !.
add_arrow_to_link(Config, Gxl_Edge, Link, second) :-
   Arrow_Alias := Gxl_Edge^prmtr@second_arrow,
   XML_Arrow_1 := Config^arrows^arrow::[@alias=Arrow_Alias],
   xml_arrow_to_color_pen(XML_Arrow_1, Gxl_Edge, XML_Arrow_2),
   add_arrow_to_link(XML_Arrow_2, Link, second_arrow),
   !.
add_arrow_to_link(Config, Gxl_Edge, Link, both) :-
   add_arrow_to_link(Config, Gxl_Edge, Link, first),
   add_arrow_to_link(Config, Gxl_Edge, Link, second),
   !.
add_arrow_to_link(_, _, _, _) :-
   !.


/* add_handles_to_node(+Address, +Handles, +Txt_Size, +Img_Size) <-
      */

add_handles_to_node(Address, Handles, Txt_Size, Img_Size) :-
   forall( Handle := Handles^handle,
      add_handle_to_node(Address, Txt_Size, Img_Size, Handle) ).


/*** implementation ***********************************************/


/* xml_arrow_to_color_pen(
      +XML_Arrow_1, +Gxl_Edge, -XML_Arrow_3) <-
      */

xml_arrow_to_color_pen(XML_Arrow_1, Gxl_Edge, XML_Arrow_3) :-
   [Line_Color, Line_Pen] := Gxl_Edge^prmtr@[color, pen],
   [Color, Pen] := XML_Arrow_1@[color, pen],
   xml_arrow_to_cp(color, XML_Arrow_1,
      Color, Line_Color, XML_Arrow_2),
   xml_arrow_to_cp(pen, XML_Arrow_2,
      Pen, Line_Pen, XML_Arrow_3).

xml_arrow_to_cp(Type, XML_Arrow_1, line, Color, XML_Arrow_2) :-
   change_fn_value(XML_Arrow_1, Type:Color, XML_Arrow_2),
%  XML_Arrow_2 := XML_Arrow_1*[@Type:Color],
   !.
xml_arrow_to_cp(_, XML_Arrow, _, _, XML_Arrow).


/* add_arrow_to_link(+XML_Arrow, +Link, +Arrow_Pos) <-
      */

add_arrow_to_link(XML_Arrow, Link, Arrow_Pos) :-
   [Width, Length, Fill_pattern, Color, Style, Pen] :=
      XML_Arrow@[width, length,
         fill_pattern, color, style, pen],
   new(Arrow, arrow(Length, Width, Style, @Fill_pattern)),
   send(Arrow, pen, Pen),
   send(Arrow, colour, Color),
   send(Link, Arrow_Pos, Arrow).


/* add_handle_to_node(
      +Address, +Text_Size, +Image_Size, +Parameters) <-
      */

add_handle_to_node(Address, (X_T, Y_T), (X_I, Y_I), Handle) :-
   handle_to_paramter(Handle, top, Reference, Factor, Direction),
   reference_to_size(Reference, (X_T, Y_T), (X_I, Y_I), (_, Y_R)),
   X is X_T/2,
   Y is Y_I/2 - Y_R*(Factor/200),
   send(Address, handle, handle(X, Y, Direction)).
add_handle_to_node(Address, (X_T, Y_T), (X_I, Y_I), Handle) :-
   handle_to_paramter(Handle, bottom, Reference, Factor, Direction),
   reference_to_size(Reference, (X_T, Y_T), (X_I, Y_I), (_, Y_R)),
   X is X_T/2,
   Y is Y_I/2 + (Y_R - Y_I/2)*(Factor/100),
   send(Address, handle, handle(X, Y, Direction)).
add_handle_to_node(Address, (X_T, Y_T), (X_I, Y_I), Handle) :-
   handle_to_paramter(Handle, left, Reference, Factor, Direction),
   reference_to_size(Reference, (X_T, Y_T), (X_I, Y_I), (X_R, _)),
   X is X_T/2 - X_R*(Factor/200),
   Y is Y_I/2,
   send(Address, handle, handle(X, Y, Direction)).
add_handle_to_node(Address, (X_T, Y_T), (X_I, Y_I), Handle) :-
   handle_to_paramter(Handle, right, Reference, Factor, Direction),
   reference_to_size(Reference, (X_T, Y_T), (X_I, Y_I), (X_R, _)),
   X is X_T/2 + X_R*(Factor/200),
   Y is Y_I/2,
   send(Address, handle, handle(X, Y, Direction)).
add_handle_to_node(Address, (X_T, Y_T), (X_I, Y_I), Handle) :-
   handle_to_paramter(Handle, top_left, Reference, Factor, Direction),
   reference_to_size(Reference, (X_T, Y_T), (X_I, Y_I), (X_R, Y_R)),
   X is X_T/2 - X_R*(Factor/200),
   Y is Y_I/2 - Y_R*(Factor/200),
   send(Address, handle, handle(X, Y, Direction)).
add_handle_to_node(Address, (X_T, Y_T), (X_I, Y_I), Handle) :-
   handle_to_paramter(Handle, top_right, Reference, Factor, Direction),
   reference_to_size(Reference, (X_T, Y_T), (X_I, Y_I), (X_R, Y_R)),
   X is X_T/2 + X_R*(Factor/200),
   Y is Y_I/2 - Y_R*(Factor/200),
   send(Address, handle, handle(X, Y, Direction)).
add_handle_to_node(Address, (X_T, Y_T), (X_I, Y_I), Handle) :-
   handle_to_paramter(Handle, bottom_left, Reference, Factor, Direction),
   reference_to_size(Reference, (X_T, Y_T), (X_I, Y_I), (X_R, Y_R)),
   X is X_T/2 - X_R*(Factor/200),
   Y is Y_I/2 + (Y_R - Y_I/2)*(Factor/100),
   send(Address, handle, handle(X, Y, Direction)).
add_handle_to_node(Address, (X_T, Y_T), (X_I, Y_I), Handle) :-
   handle_to_paramter(Handle, bottom_right, Reference, Factor, Direction),
   reference_to_size(Reference, (X_T, Y_T), (X_I, Y_I), (X_R, Y_R)),
   X is X_T/2 + X_R*(Factor/200),
   Y is Y_I/2 + (Y_R - Y_I/2)*(Factor/100),
   send(Address, handle, handle(X, Y, Direction)).


/* handle_to_paramter(+Handle, -Pos, -Reference, -Factor, -Direction) <-
      */

handle_to_paramter(handle:Attr:_, Pos, Reference, Factor, Direction) :-
   memberchk(pos:Pos, Attr),
   memberchk(direction:Direction, Attr),
   memberchk(reference:Reference, Attr),
   memberchk(factor:Factor, Attr).


/* reference_to_size(+Type, +Txt_Size, +Img_Size, -Size).
      */

reference_to_size(text, Txt_Size, _, Txt_Size) :-
   !.
reference_to_size(symbol, _, Img_Size, Img_Size) :-
   !.


/******************************************************************/


