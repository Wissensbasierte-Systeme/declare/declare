

/******************************************************************/
/***                                                            ***/
/***      GXL: Symbols                                          ***/
/***                                                            ***/
/******************************************************************/


:- multifile send_object_to_device/4.

:- discontiguous send_object_to_device/4.


/*** interface ****************************************************/


/* send_object_to_device(
      +Device, [+Size, +Symbol, +Color], +MLTF, -Bitmap) <-
      */

send_object_to_device(Device,
      [Size, chart:Attr:Cont, _Color], MLTF, Drag_Device) :-
   Type := (chart:Attr:Cont)@type,
   send(Device, display,
      new(Drag_Device, dragdevice)),
   findall( H,
      H := (chart:Attr:Cont)^bar@height,
      Hs ),
   max(Hs, Max),
   findall( (Max, Height, Pos, Color),
      ( Bar := (chart:_:Cont)^bar,
        [Height, Pos, Color] :=
           Bar@[height, position, color] ),
      Prmtrs ),
   checklist( add_bar(Drag_Device, Type, Size),
      Prmtrs ),
   get(Drag_Device, size, size(_, H)),
   send(Device, display, MLTF, point(0, H + 4)),
   new(_,
      constraint(MLTF, Drag_Device, identity(center_x))).


/*** implementation ***********************************************/


/* add_bar(+Device, +Type, +Size, +A, +B, +XX, +Color) <-
      */

add_bar(Device, v_bar_graph, Size, (A, B, XX, Color)) :-
   AA is (A*Size)/18,
   BB is (B*Size)/18,
   X is ((10*Size)/18),
   Y is AA - BB,
   send(Device, display,
      new(Box, box(X, BB)), point(X*XX, Y)),
   send(Box, fill_pattern, colour(Color)).
add_bar(Device, h_bar_graph, Size, (A, B, XX, Color)) :-
   AA is (A*Size)/18,
   BB is (B*Size)/18,
   X is ((10*Size)/18),
   Y is AA - BB,
   send(Device, display,
      new(Box, box(BB*(-1), X)), point(Y*(-1), X*XX)),
   send(Box, fill_pattern, colour(Color)).


/******************************************************************/


