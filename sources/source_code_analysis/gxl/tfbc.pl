

/******************************************************************/
/***                                                            ***/
/***          XPCE Graphs:  DFS Coloring                        ***/
/***                                                            ***/
/******************************************************************/


:- module( gxl_tfbc, [ ] ).

:- dynamic dfs_variable/2.


/*** interface ****************************************************/


/* rnes_to_tfbcs(+Roots, +Nodes, +Edges,
      -Tree, -Forward, -Bachward, -Cross) <-
      */

rnes_to_tfbcs(Roots, Nodes, Edges, T_E, F_E, B_E, C_E) :-
   subtract(Nodes, Roots, Nodes_2),
   append(Roots, Nodes_2, Nodes_3),
   dfs(Nodes_3, Edges),
   specify_forward_cross_edges,
   ( foreach(Type, [tree, forward, back, cross]),
     foreach(V_W_Edges, Lists) do
        findall( V-W,
           gxl_tfbc:dfs_variable((type, V:W:_), Type),
           V_W_Edges ) ),
    maplist( list_to_ord_set,
       Lists, [T_E, F_E, B_E, C_E] ).


/*** implementation ***********************************************/


/* dfs(+Nodes, +Edges) <-
      */

dfs(Nodes, Edges) :-
   maplist( transform_edges,
      Edges, Matrix ),
   dfs_variable_set(time, 0),
   retractall( dfs_variable((d, _), _) ),
   retractall( dfs_variable((f, _), _) ),
   retractall( dfs_variable((color, _), _) ),
   retractall( dfs_variable((type, _), _) ),
   ( foreach(N, Nodes) do
        gxl_tfbc:dfs_variable_set((color, N), white) ),
   ( foreach(N , Nodes) do
        gxl_tfbc:dfs_variable((color, N), Color),
        ( Color = white ->
             gxl_tfbc:dfs_visit(Matrix, N)
        ; true ) ).

transform_edges(A-B, A:B:x).


/* dfs_visit(+Matrix, +Node) <-
      */

dfs_visit(Matrix, N) :-
   dfs_variable_set((color, N), grey),
   dfs_variable(time, Time),
   dfs_variable_set((d, N), Time),
   New_Time is Time + 1,
   dfs_variable_set(time, New_Time),
   forall( member(N:V:C, Matrix),
      ( dfs_variable((color, V), Color),
        Var = (type, N:V:C),
        ( Color = white ->
          ( dfs_variable_set(Var, tree),
            dfs_visit(Matrix, V) )
        ; ( Color = grey ->
            dfs_variable_set(Var, back)
          ; dfs_variable_set(Var, forward_cross_edge) ) ) ) ),
   dfs_variable_set((color, N), black),
   dfs_variable(time, Time_2),
   dfs_variable_set((f, N), Time_2),
   New_Time_2 is Time_2 + 1,
   dfs_variable_set(time, New_Time_2).


/* specify_forward_cross_edges <-
      */

specify_forward_cross_edges :-
   forall( dfs_variable((type, N:V:C), forward_cross_edge),
      ( dfs_variable((d, N), N_Time),
        dfs_variable((d, V), V_Time),
        Var = (type, N:V:C),
        ( N_Time < V_Time ->
          dfs_variable_set(Var, forward)
        ; dfs_variable_set(Var, cross) ) ) ).


/* dfs_variable_set(+Variable, +Value) <-
      */

dfs_variable_set(Variable, Value) :-
   retractall( dfs_variable(Variable, _) ),
   asserta( dfs_variable(Variable, Value) ).


/*** Tests ********************************************************/


test :-
   rnes_to_tfbcs([a], [b, c, d], [a-c, a-b, b-d, b-c, a-d, d-a],
         T_E, F_E, B_E, C_E),
   T_E = [a-b, a-c, b-d],
   F_E = [a-d],
   B_E = [d-a],
   C_E = [b-c].


/******************************************************************/


