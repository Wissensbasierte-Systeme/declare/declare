

/******************************************************************/
/***                                                            ***/
/***      GXL: Tests Basic Methods                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(source_code_analysis:gxl, check_gxl_methods) :-
   gxl_reset_gensym_variables,
   gxl(example_3, Gxl),
   check_atomic_ids_in_gxl(Gxl),
   gxl_to_node_types(Gxl,
      Fathers, Embedded, Not_Embedded,
      Deduced_Nodes, D_Node_Fathers),
   Fathers = [],
   Embedded = [],
   Not_Embedded = [example_id_1, example_id_2,
      example_id_3, example_id_4,
      example_id_5, example_id_6,
      example_id_7],
   Deduced_Nodes = [example_id_7, example_id_5,
      example_id_1, example_id_2, example_id_3,
      example_id_4, example_id_6],
   D_Node_Fathers = [],
   gxl_to_missing_nodes(Gxl, []),
   gxl_to_single_nodes(Gxl, []),
   gxl_to_tfbc_edges(v_w, Gxl, Tree, Forward, Back, Cross),
   Config = config:[]:[
      edge_colors:[]:[
         edge_color:[type:tree_edge, color:blue]:[],
         edge_color:[type:cross_edge, color:green]:[],
         edge_color:[type:back_edge, color:yellow]:[],
         edge_color:[type:forward_edge, color:red]:[]
   ]  ],
   gxl_to_gxl_highlighted_tfbc_edges(Config, Gxl, Gxl_dd),
   gxl_to_picture(Gxl_dd),
   Tree = [example_id_1-example_id_2,
      example_id_2-example_id_3,
      example_id_3-example_id_4,
      example_id_4-example_id_5,
      example_id_5-example_id_6,
      example_id_6-example_id_7],
   Forward = [],
   Back = [example_id_3-example_id_1,
      example_id_6-example_id_4,
      example_id_7-example_id_5],
   Cross = [],
   Vertices = [a, b, c, d],
   Edges = [a-b, a-c, b-d, a-d],
   vertices_and_edges_to_possible_roots(Vertices, Edges, Roots),
   tree_traversals:traverse(bfs, Roots,
      Edges, Tree_Edges),
   Roots = [a],
   Tree_Edges = [a-b, a-c, a-d].

test(source_code_analysis:gxl, empty_references) :-
   gxl_reset_gensym_variables,
   gxl(example_3, Gxl_1),
   Config_1 = config:[]:[
      action:[name:delete]:[
         node:[id:example_id_6]:[] ] ],
   gxl_to_gxl(Config_1, Gxl_1, Gxl_2),
   Config_2 = config:[]:[
      action:[name:delete_missing_references]:[] ],
   gxl_to_gxl(Config_2, Gxl_2, Edges, Gxl_3),
   Edges =
      [example_id_12, example_id_13, example_id_15],
   gxl_to_picture(Gxl_3).

test(source_code_analysis:gxl, delete_edges_weighted_colored) :-
   gxl(metty, Gxl_1),
   Config_1 = config:[]:[
      action:[name:delete_light_weighted_edges,
         limit:100]:[]],
   gxl_to_gxl(Config_1, Gxl_1, Gxl_2),
   gxl_to_picture(Gxl_2),
   Config_2 = config:[]:[
      action:[name:delete_colored_edges,
         color:blue]:[]],
   gxl_to_gxl(Config_2, Gxl_1, Gxl_3),
   gxl_to_picture(Gxl_3).

test(source_code_analysis:gxl, scc) :-
   gxl_reset_gensym_variables,
   gxl(example_3, Gxl_1),
   Config = config:[]:[
      action:[name:delete_prmtr_tags]:[],
      action:[name:scc]:[] ],
   gxl_to_gxl(Config, Gxl_1, Gxl_2),
   gxl_to_picture(Gxl_2).

test(source_code_analysis:gxl, highlight_scc) :-
   gxl_reset_gensym_variables,
   gxl(example_3, Gxl_1),
   test_sub_higlight_config(_, Config_2),
   gxl_to_gxl(Config_2, Gxl_1, Gxl_2),
   gxl_to_picture(Gxl_2).

test(source_code_analysis:gxl, highlight_tfbc) :-
   gxl_reset_gensym_variables,
   gxl(example_4, Gxl_1),
   Config = config:[]:[
      action:[name:highlight_tfbc_edges]:[
         edge_colors:[]:[
            edge_color:[type:tree_edge, color:black]:[],
            edge_color:[type:cross_edge, color:green]:[],
            edge_color:[type:back_edge, color:orange]:[],
            edge_color:[type:forward_edge, color:red]:[] ] ] ],
   gxl_to_gxl(Config, Gxl_1, Gxl_2),
   gxl_to_picture(Gxl_2).

test(source_code_analysis:gxl, without_loops) :-
   gxl(embedded_graph, Gxl_1),
   Config_1 = config:[]:[
      action:[name:delete_loops]:[] ],
   gxl_to_gxl(Config_1, Gxl_1, Gxl_2),
   gxl_to_picture(Gxl_2),
   Config_2 = config:[]:[
      action:[name:delete_embedded_graphs]:[] ],
   gxl_to_gxl(Config_2, Gxl_1, Gxl_3),
   fn_to_xml(Gxl_3).

test(source_code_analysis:gxl, gxl_to_gxl) :-
   gxl_reset_gensym_variables,
   gxl(example_3, Gxl_1),
   Config_1 = config:[]:[
      action:[name:change_ids]:[],
      action:[name:delete]:[
         node:[id:id_17]:[],
         edge:[id:id_3]:[] ],
      action:[name:delete_single_nodes]:[],
      action:[name:layout]:[
         gxl_layout:[mode:dfs, x_start:10, y_start:10,
            x_step:50, y_step:50, y_variance:10]:[
            roots:[]:[node:[id:id_1]:[] ] ] ] ],
   gxl_to_gxl(Config_1, Gxl_1, Gxl_2),
   Config_2 = config:[]:[
      action:[name:separate_gxl_and_layout]:[] ],
   gxl_to_gxl(Config_2, Gxl_2, Layout, Gxl_3),
   Config_3 = config:[]:[
      action:[name:merge_gxl_and_layout]:[]],
   gxl_to_gxl(Config_3, Gxl_3, Layout, Gxl_4),
   Config_4 = config:[]:[
      action:[name:set_color, color:red, mode:ids]:[
         node:[id:id_8]:[] ],
      action:[name:set_color, color:red, mode:outgoing_edges]:[
         node:[id:id_8]:[] ],
      action:[name:set_color, color:green, mode:incoming_edges]:[
         node:[id:id_8]:[] ] ],
   gxl_to_gxl(Config_4, Gxl_4, Gxl_5),
   gxl_to_picture(Gxl_5).


/*** implementation ***********************************************/


/* gxl_reset_gensym_variables <-
      */

gxl_reset_gensym_variables :-
   reset_gensym(id_),
   reset_gensym(example_id_),
   reset_gensym(id1_).


/* test_sub_higlight_config(-Config_1, -Config_2) <-
      */

test_sub_higlight_config(Config_1, Config_2) :-
   alias_to_default_fn_triple(gxl_config, config:Attr:Cont),
   Config_1 = config:Attr:Cont,
   memberchk(symbols:Symb_Attr:Symb_Cont, Cont),
   memberchk(sizes:Siz_Attr:Siz_Cont, Cont),
   memberchk(node_colors:Col_Attr:Col_Cont, Cont),
%  writeln(Col_Attr-Col_Cont),
   Config_2 = config:[]:[
      action:[name:delete_prmtr_tags]:[],
      action:[name:validate]:[],
      action:[name:highlight_scc]:[
         symbols:Symb_Attr:Symb_Cont,
         sizes:Siz_Attr:Siz_Cont,
         node_colors:Col_Attr:Col_Cont] ].


/******************************************************************/


