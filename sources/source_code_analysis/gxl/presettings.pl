

/******************************************************************/
/***                                                            ***/
/***     GXL: Default GXL Documents                             ***/
/***                                                            ***/
/******************************************************************/


:- dynamic gxl_presettings/1.


/*** interface ****************************************************/


/* load_gxl_presettings <-
      */

load_gxl_presettings :-
   alias_to_file(gxl_presettings, File),
   load_gxl_presettings(File).


/* load_gxl_presettings(+File) <-
      */

load_gxl_presettings(File) :-
   dread_(xml(gxl_graph), File, Gxl_Default),
   retractall(gxl_presettings(_)),
   assert( gxl_presettings(Gxl_Default) ).


:- load_gxl_presettings.


/* gxl_presetting(+Type, -Gxl) <-
      */

gxl_presetting(Type, Gxl) :-
   gxl_presetting(Type, _, Gxl).


/* gxl_presetting(+Type, ?Id, -Gxl) <-
      */

gxl_presetting(Type, Id, Gxl) :-
   var(Id),
   gensym(id1_, Id),
   gxl_presetting_ext(Type, Id, Gxl),
   !.
gxl_presetting(Type, Id, Gxl) :-
   gxl_presetting_ext(Type, Id,  Gxl).


/*** implementation ***********************************************/


/* gxl_presetting_ext(+Type, +Id, -Element) <-
      */

gxl_presetting_ext(gxl, Id, Gxl_2) :-
   gxl_presettings(Gxl_1),
   gensym(id2_, Id_Node),
   gensym(id3_, Id_Edge),
   Gxl_2 := Gxl_1*[^graph@id:Id,
      ^graph^node@id:Id_Node,
      ^graph^edge@id:Id_Edge,
      ^graph^edge@from:Id_Node,
      ^graph^edge@to:Id_Node].
gxl_presetting_ext(graph, Id, Graph) :-
   gxl_presettings(Gxl),
   gensym(id4_, Id_Node),
   gensym(id5_, Id_Edge),
   Graph := Gxl^graph*[@id:Id,
      ^node@id:Id_Node,
      ^edge@id:Id_Edge,
      ^edge@from:Id_Node,
      ^edge@to:Id_Node].
gxl_presetting_ext(head, Id, Head) :-
   gxl_presettings(Gxl_1),
   Gxl_2 := Gxl_1*[^graph@id:Id],
   Prmtr := Gxl_2^prmtr,
   graph:Attr:_ := Gxl_2^graph,
   Head = gxl:[
      'xmlns:xlink':'http://www.w3.org/1999/xlink']:[
      Prmtr, graph:Attr:[]].
gxl_presetting_ext(head_pure, _, Head) :-
   gxl_presettings(Gxl),
   Prmtr := Gxl^prmtr,
   Head = gxl:[
      'xmlns:xlink':'http://www.w3.org/1999/xlink']:[
      Prmtr].
gxl_presetting_ext(node, Id, Gxl_Node) :-
   random(0, 300, X),
   random(0, 300, Y),
   gxl_presettings(Gxl),
   Gxl_Node := Gxl^graph^node*[@id:Id,
      ^prmtr@x_pos:X, ^prmtr@y_pos:Y],
   !.
gxl_presetting_ext(edge, Id, Gxl_Edge) :-
   gxl_presettings(Gxl),
   Gxl_Edge := Gxl^graph^edge*[@id:Id].


/******************************************************************/


