

/******************************************************************/
/***                                                            ***/
/***      GXL: Labels                                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* multi_line_text_field(+Text, +Font, -MLTF) <-
      */

multi_line_text_field([], Font, D) :-
   new(D, device),
   text_field('', Font, TF),
   send(D, display, TF).
multi_line_text_field(Texts_1, Font, D) :-
   maplist( prolog_term_to_xpce_node_label,
      Texts_1, [Text|Texts_2] ),
   new(D, device),
   text_field(Text, Font, TF),
   send(D, display, TF),
   iterate_list(
      multi_line_text_field((D, Font)), (0, 0),
         Texts_2, (_, _) ).


/*** implementation ***********************************************/


/* multi_line_text_field((D, Font), (X, Y_1), Text, (X, Y_2)) <-
      */

multi_line_text_field((D, Font), (X, Y_1), Text, (X, Y_2)) :-
   text_field(Text, Font, TF_1),
   get(D, member, text, TF_2),
   Font = (_, _, Points),
   Y_2 is Y_1 + Points + 2,
   send(D, display, TF_1, point(X, Y_2)),
   new(_,
      constraint(TF_1, TF_2, identity(center_x))).


/* text_field(+Text, +Font, -TF) <-
      */

text_field(Text, Font, TF) :-
   Font = (Family, Style, Points),
   new(TF, text(Text)),
   send(TF, font, font(Family, Style, Points)).


/* prolog_term_to_xpce_node_label(+Term, -Atom) <-
      */

prolog_term_to_xpce_node_label((user:P)/unknown, Atom) :-
   term_to_atom_sure(P, Atom),
   !.
prolog_term_to_xpce_node_label((user:P)/A, Atom) :-
   term_to_atom_sure(P/A, Atom),
   !.
prolog_term_to_xpce_node_label((M:P)/unknown, Atom) :-
   term_to_atom_sure(M:P, Atom),
   !.
prolog_term_to_xpce_node_label(Term, Atom) :-
   var(Term),
   term_to_atom_sure(Term, Atom),
   !.
prolog_term_to_xpce_node_label(Term, Atom) :-
   number(Term),
   term_to_atom_sure(Term, Atom),
   !.
prolog_term_to_xpce_node_label(Term, Atom) :-
   compound(Term),
   term_to_atom_sure(Term, Atom),
   !.
/*
prolog_term_to_xpce_node_label(Atom, Term) :-
   name(Atom, Name),
   not(member(46, Name)),
   not(Atom == ''),
   term_to_atom_sure(Term, Atom),
   nonvar(Term).
*/
prolog_term_to_xpce_node_label(A, B) :-
   name(A, Name),
   member(39, Name),
   term_to_atom_sure(B, A),
   !.
prolog_term_to_xpce_node_label(Atom, Atom) :-
   atom(Atom),
   !.


/******************************************************************/


