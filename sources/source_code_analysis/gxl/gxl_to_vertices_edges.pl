

/******************************************************************/
/***                                                            ***/
/***     GXL: gxl_to_vertices_and_edges                             ***/
/***                                                            ***/
/******************************************************************/


:- module( gxl_to_vertices_and_edges, [
      gxl_to_vertices_and_edges/4 ]).


/*** interface ****************************************************/


/* gxl_to_vertices_and_edges(+Mode, +Gxl, -Vertices, -Edges) <-
      */

gxl_to_vertices_and_edges(V_Mode-E_Mode, Gxl, Vertices, Edges) :-
   get_gxl_nodes(V_Mode, Gxl, Gxl_Nodes),
   get_gxl_edges(E_Mode, Gxl, Gxl_Edges),
   maplist( gxl_node_to_vertex(V_Mode),
      Gxl_Nodes, Vertices ),
   maplist( gxl_edge_to_edge(E_Mode),
      Gxl_Edges, Edges ),
   !.


/*** implementation ***********************************************/


/* get_gxl_nodes(+Mode, +Gxl, -Gxl_Nodes) <-
      */

get_gxl_nodes(0, _, []) :-
   !.
get_gxl_nodes(_, Gxl, Gxl_Nodes) :-
   findall( Gxl_Node,
      Gxl_Node := Gxl^graph^node,
      Gxl_Nodes ),
   !.


/* get_gxl_edges(+Mode, +Gxl, -Gxl_Edges) <-
      */

get_gxl_edges(0, _, []) :-
   !.
get_gxl_edges(_, Gxl, Gxl_Edges) :-
   findall( Gxl_Edge,
      Gxl_Edge := Gxl^graph^edge,
      Gxl_Edges ),
   !.


/*   gxl_node_to_vertex(+Mode, +Gxl_Node, -Vertice) <-
      */

gxl_node_to_vertex(Mode, Gxl_Node_1, Vertice) :-
   member(Mode, [all, 6]),
   gxl_element_to_element_complete(Gxl_Node_1, Gxl_Node_2),
   Node_Id := Gxl_Node_2@id,
   [X_Pos, Y_Pos, Color, Symbol, Size] :=
       Gxl_Node_2^prmtr@[ x_pos, y_pos, color, symbol, size],
   string:_Attr:Label := Gxl_Node_2^prmtr^string,
%  Bubble := (string:Attr:Label)@bubble,
   Vertice = Node_Id-Label-Symbol-Color-Size-(X_Pos, Y_Pos),
   !.
gxl_node_to_vertex(Mode, Gxl_Node, Node_Id) :-
   member(Mode, [node_id, 1]),
   Node_Id := Gxl_Node@id,
   !.
gxl_node_to_vertex(0, _, nv) :-
   !.


/* gxl_edge_to_vertex(+Mode, +Gxl_Edge, -Edge) <-
      */

gxl_edge_to_edge(7, Gxl_Edge_1, Edge) :-
   gxl_element_to_element_complete(Gxl_Edge_1, Gxl_Edge_2),
   [Id, V, W] := Gxl_Edge_2@[id, from, to],
   [Color, Arrows, Pen, Weight] :=
       Gxl_Edge_2^prmtr@[color, arrows, pen, weight],
   Edge = Id-V-W-Color-Arrows-Pen-Weight,
   !.
gxl_edge_to_edge(Mode, Gxl_Edge_1, Edge) :-
   member(Mode, [all, 6]),
   gxl_element_to_element_complete(Gxl_Edge_1, Gxl_Edge_2),
   [Id, V, W] := Gxl_Edge_2@[id, from, to],
   [Color, Arrows, Pen] :=
       Gxl_Edge_2^prmtr@[color, arrows, pen],
   Edge = Id-V-W-Color-Arrows-Pen,
   !.
gxl_edge_to_edge(Mode, Gxl_Edge, V-W) :-
   member(Mode, [v_w, 2]),
   [V, W] := Gxl_Edge@[from, to],
   !.
gxl_edge_to_edge(Mode, Gxl_Edge, Edge_Id) :-
   member(Mode, [edge_id, 1]),
   Edge_Id := Gxl_Edge@id,
   !.
gxl_edge_to_edge(0, _, nv) :-
   !.


/******************************************************************/

