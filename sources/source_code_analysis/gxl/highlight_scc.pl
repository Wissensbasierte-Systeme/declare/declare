

/******************************************************************/
/***                                                            ***/
/***  GXL: Highligh Strongly Connected Components               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gxl_scc_to_gxl_highlighted_scc(+Config, +Gxl_1, -Gxl_2) <-
      */

gxl_scc_to_gxl_highlighted_scc(Config, Gxl_1, Gxl_3) :-
   config_to_scc_symbol_color_sizes(Config, SCSs),
   findall( SCC_Node,
      SCC_Node := Gxl_1^graph^node::[@scc=true],
      SCC_Nodes ),
   findall( SCC_Node,
      SCC_Node := Gxl_1^graph^node::[@scc=false],
      Normal_Nodes ),
   findall( Edge,
      Edge := Gxl_1^graph^edge,
      Inter_Edges ),
   highlight_scc(SCSs, SCC_Nodes, H_SCC_Nodes, H_SCC_Edges),
   gxl_element_to_element_complete(Gxl_1, Gxl_2),
   Gxl_2 = gxl:Attr_1:_,
   Prmtr := Gxl_2^prmtr,
   graph:Attr_2:_ := Gxl_2^graph,
   append([H_SCC_Nodes, Normal_Nodes, H_SCC_Edges, Inter_Edges],
      N_Es),
   Gxl_3 = gxl:Attr_1:[Prmtr, graph:Attr_2:N_Es].


/*** implementation ***********************************************/


/* highlight_scc(+SCSs, +SCCs, -SCC_Nodes, -SCC_Edges) <-
      */

highlight_scc([], _, _, _) :-
   writeln('Not enought symbols for all SCC!').
highlight_scc(_, [], [], []).
highlight_scc([[Symbol, Color, Size]|SCSs], [SCC|SCCs],
      SCC_Nodes_2, SCC_Edges_2) :-
   highlight_scc(SCSs, SCCs, SCC_Nodes_1, SCC_Edges_1),
   findall( Node_3,
      ( Node_1 := SCC^graph^node,
        gxl_element_to_element_complete(Node_1, Node_2),
        Node_3 := Node_2*[
         ^prmtr@symbol:Symbol,
         ^prmtr@color:Color,
         ^prmtr@size:Size] ),
      Nodes ),
   findall( Edge_3,
      ( Edge_1 := SCC^graph^edge,
        gxl_element_to_element_complete(Edge_1, Edge_2),
        Edge_3 := Edge_2*[^prmtr@color:Color] ),
      Edges ),
   append(SCC_Nodes_1, Nodes, SCC_Nodes_2),
   append(SCC_Edges_1, Edges, SCC_Edges_2),
   !.


/******************************************************************/


