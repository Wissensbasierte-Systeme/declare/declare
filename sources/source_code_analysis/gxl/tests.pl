

/******************************************************************/
/***                                                            ***/
/***      GXL: Tests                                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(source_code_analysis:gxl, id_with_capital_letters) :-
   gxl(id_with_capital_letters, Gxl),
   alias_to_default_fn_triple(gxl_config, Config),
   gxl_to_picture(Config, Gxl, _),
   !.

test(source_code_analysis:gxl, gxl_to_picture) :-
   test_sub_1(Picture_1),
   test_sub_2(Picture_1, Picture_2),
   test_sub_3(Picture_2),
   !.

test(source_code_analysis:gxl, arrow_types) :-
   alias_to_default_fn_triple(gxl_config, Config),
   gxl(arrow_types, Gxl_1),
   gxl_to_picture(Config, Gxl_1, _),
   !.

test(source_code_analysis:gxl, gxl_presetting) :-
   alias_to_default_fn_triple(gxl_config, Config),
   gxl_presetting(gxl, Gxl_D),
   gxl_to_picture(Config, Gxl_D, _),
   !.

test(source_code_analysis:gxl, nodes_and_edge_to_picture) :-
   new(Picture, picture(test)),
   send(Picture, open),
   alias_to_default_fn_triple(gxl_config, Config),
   Node_1 = node:[id:id_rrt]:[
      prmtr:[mouse_click:alias_db_tool_node_click,
         color:colour('#FF0000'),
         size:medium, symbol:text_in_box]:[
         string:[bubble:'']:['A'-'B'] ] ],
   gxl_element_to_element_complete(Node_1, Gxl_Node_1),
   gxl_node_to_picture(Config, Gxl_Node_1, Picture),
   Node_2 = node:[id:id_test_101]:[],
   gxl_element_to_element_complete(Node_2, Gxl_Node_2),
   gxl_node_to_picture(Config, Gxl_Node_2, Picture),
   gxl_element_to_element_complete(node:[id:_]:[], Gxl_Node_3),
   gxl_node_to_picture(Config, Gxl_Node_3, Picture, Node_Id),
   gxl_element_to_element_complete(edge:[id:_, from:id_test_101,
      to:Node_Id]:[], Gxl_Edge),
   gxl_edge_to_picture(Config, Gxl_Edge, Picture),
   picture_to_gxl(Picture, Gxl, _),
   Gxl = gxl:Attr:Cont,
   Attr = ['xmlns:xlink':'http://www.w3.org/1999/xlink'],
   Cont = [Graph, Prmtr],
   Prmtr = prmtr:[label:'Default GXL', width:450, height:400,
          background:white, mouse_click:picture]:[],
   Graph = graph:Attr_Graph:Cont_Graph,
   Attr_Graph = [added_graph:true, edgeids:true, edgemode:directed, hypergraph:false,
          id:_],
   Cont_Graph = [node:Attr_N_1:Cont_N_1,
      node:Attr_N_2:Cont_N_2,
      node:Attr_N_3:Cont_N_3,
      edge:Attr_E:Cont_E],
   Attr_N_1 = [id:id_rrt],
   Cont_N_1 = [prmtr:[
          color:grey, handles:default,
          mouse_click:node, size:small,
          symbol:circle, x_pos:_, y_pos:_]:[string:[
            bubble:'[\'\'\'A\'\'-\'\'B\'\'\']',
            font_style: (helvetica, roman, 12)]:['\'A\'-\'B\'']]],
   Attr_N_2 = [id:id_test_101],
   Cont_N_2 = [prmtr:[color:grey,
          handles:default, mouse_click:node, size:small,
          symbol:circle, x_pos:_, y_pos:_]:[string:[
          bubble:_,
          font_style: (helvetica, roman, 12)]:[_]]],
   Attr_N_3 = [id:Node_Id],
   Cont_N_3 = [prmtr:[color:grey, handles:default,
          mouse_click:node, size:small, symbol:circle, x_pos:_,
          y_pos:_]:[string:[bubble:_,
          font_style: (helvetica, roman, 12)]:[_]]],
   Attr_E = [id:_, from:id_test_101, to:Node_Id],
   Cont_E = [
          prmtr:[arrows:both, color:black, first_arrow:first_arrow,
          mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[
          string:[bubble:[], font_style: (helvetica, roman, 12)]:[]]],
   !.

test(source_code_analysis:gxl, gxl_to_vertices_and_edges_to_gxl) :-
   gxl(example_1, Gxl_1),
   gxl_to_vertices_and_edges(all-all, Gxl_1, Vertices, Edges),
   vertices_and_edges_to_gxl(Vertices, Edges, Gxl_2),
   alias_to_default_fn_triple(gxl_config, Config),
   gxl_to_picture(Config, Gxl_2, _Picture),
   !.

test(source_code_analysis:gxl, scc_1) :-
   gxl(example_3, Gxl_1),
   alias_to_default_fn_triple(gxl_config,
      Gxl_Config),
   gxl_to_picture(Gxl_Config, Gxl_1, _),
   Config_SCC = config:[]:[
      action:[name:scc]:[] ],
   gxl_to_gxl(Config_SCC, Gxl_1, Gxl_2),
   gxl_to_picture(Gxl_Config, Gxl_2, Picture_2),
   xpce_graph_layout(xpce, Picture_2),
   !.

test(source_code_analysis:gxl, scc_2) :-
   gxl(example_3, Gxl_1),
   test_sub_higlight_config(Config_1, Config_2),
   gxl_to_gxl(Config_2, Gxl_1, Gxl_2),
   gxl_to_picture(Config_1, Gxl_2, _),
   !.

test(source_code_analysis:gxl, scc_3) :-
   gxl(example_3_2, Gxl_1),
   test_sub_higlight_config(Config_1, Config_2),
   gxl_to_gxl(Config_2, Gxl_1, Gxl_2),
   gxl_to_picture(Config_1, Gxl_2, _),
   !.

test(source_code_analysis:gxl, embedded_graph) :-
   gxl(embedded_graph, Gxl),
   alias_to_default_fn_triple(gxl_config, Config),
   gxl_to_picture(Config, Gxl, _),
   !.

test(source_code_analysis:gxl, without_embedded_graph) :-
   gxl(embedded_graph, Gxl_1),
   gxl_to_gxl_without_embedded_graphs(Gxl_1, Gxl_2),
   Gxl_2 =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Graph Example 4 - Hypergraph', background:yellow, mouse_click:picture]:[], graph:[id:graph_4, edgeids:true, edgemode:directed, hypergraph:false]:[node:[id:n1]:[prmtr:[mouse_click:embedded_node, color:red, size:medium, symbol:circle, x_pos:70, y_pos:10]:[string:[bubble:'Embedded Graph', font_style: (helvetica, roman, 12)]:['Embedded Graph', 'click me with middle', 'mouse button']]], edge:[from:n1, id:edge_1, to:n1]:[prmtr:[arrows:second, color:green, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[font_style: (helvetica, roman, 12), bubble:'Default Edge with Arrow Configuration']:[]]]]],
   !.

test(source_code_analysis:gxl, without_loops) :-
   gxl(embedded_graph, Gxl_1),
   gxl_to_gxl_without_loops(Gxl_1, Gxl_2),
   gxl_to_picture(Gxl_2),
   Gxl_2 =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Graph Example 4 - Hypergraph', background:yellow, mouse_click:picture]:[], graph:[id:graph_4, edgeids:true, edgemode:directed, hypergraph:false]:[node:[id:n1]:[prmtr:[mouse_click:embedded_node, color:red, size:medium, symbol:circle, x_pos:70, y_pos:10]:[string:[bubble:'Embedded Graph', font_style: (helvetica, roman, 12)]:['Embedded Graph', 'click me with middle', 'mouse button']], graph:[id:graph_2, edgeids:true, edgemode:directed, hypergraph:false]:[prmtr:[label:'Content of Hypernode']:[], node:[id:n2]:[prmtr:[color:orange, handles:default, mouse_click:node, size:medium, symbol:box, x_pos:50, y_pos:10]:[string:[bubble:'[\'little Box 1\']', font_style: (helvetica, roman, 12)]:['little Box 1']]], node:[id:n3]:[prmtr:[color:orange, handles:default, mouse_click:node, size:medium, symbol:box, x_pos:150, y_pos:90]:[string:[bubble:'[little, \'Box\', 2]', font_style: (helvetica, roman, 12)]:[little, 'Box', 2]]]]]]],
   !.

test(source_code_analysis:gxl, without_loops_without_embedded_graphs) :-
   gxl(embedded_graph, Gxl_1),
   gxl_to_gxl_without_loops(Gxl_1, Gxl_2),
   gxl_to_gxl_without_embedded_graphs(Gxl_2, Gxl_3),
   Gxl_3 =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Graph Example 4 - Hypergraph', background:yellow, mouse_click:picture]:[], graph:[id:graph_4, edgeids:true, edgemode:directed, hypergraph:false]:[node:[id:n1]:[prmtr:[mouse_click:embedded_node, color:red, size:medium, symbol:circle, x_pos:70, y_pos:10]:[string:[bubble:'Embedded Graph', font_style: (helvetica, roman, 12)]:['Embedded Graph', 'click me with middle', 'mouse button']]]]],
   !.

test(source_code_analysis:gxl, simple) :-
   alias_to_default_fn_triple(gxl_config, Config),
   gxl(example_5, Gxl_1),
   gxl_to_picture(Config, Gxl_1, P),
   picture_to_gxl(P, _Gxl_2, _),
   !.

test(source_code_analysis:gxl, metty) :-
   gxl(metty, Gxl),
   alias_to_default_fn_triple(gxl_config, Config),
   gxl_to_picture(Config, Gxl, _),
   !.

test(source_code_analysis:gxl, even_simpler) :-
   alias_to_default_fn_triple(gxl_config, Config),
   gxl(example_6, Gxl_1),
   gxl_to_picture(Config, Gxl_1, _),
   !.

test(source_code_analysis:gxl, multi_line_text_field) :-
   Font = (helvetica, roman, 18),
   new(P, picture),
   send(P, open),
   multi_line_text_field([aaa, (user:bb)/6, c], Font, D_1),
   multi_line_text_field([a, (my_user:bb)/6, ccc], Font, D_2),
   send(P, display, D_1, point(50, 50)),
   send(P, display, D_2, point(150, 50)),
   !.

test(source_code_analysis:gxl, vertices_and_edges_to_picture_1) :-
   Vertices_1 = [
      1-[a]-rhombus-blue-medium,
      2-[b]-circle-green-small,
      3-[c]-triangle-red-large ],
   Edges_1 = [1-2, 1-3],
   vertices_and_edges_to_picture(
      xpce, Vertices_1, Edges_1, _),
   Vertices_2 = [a, b, c],
   Edges_2 = [a-b, a-c],
   vertices_and_edges_to_picture(
      xpce, Vertices_2, Edges_2, _),
   !.

test(source_code_analysis:gxl, vertices_and_edges_to_picture_2) :-
   Vertices_1 = [a, b, c, d, e, f, g, h],
   Edges_1 = [
      b-a, c-a, d-a,
      e-a, f-a, g-a, h-a ],
   vertices_and_edges_to_picture(
      xpce, Vertices_1, Edges_1, _),
   !.

test(source_code_analysis:gxl, gxl_id_to_xpce_address) :-
   gxl_id_to_xpce_address(@12345, n, @'n-12345'),
   gxl_id_to_xpce_address(@12345, 'N', @'\'N\'-12345'),
   gxl_id_to_xpce_address(@12345, (a:b)/7, @'(a:b)/7-12345'),
   gxl_id_to_xpce_address(@12345, '/path/file.pl',
      @'\'/path/file.pl\'-12345'),
   gxl_id_to_xpce_address(@12345, '(a:b)/7', @'(a:b)/7-12345'),
   !.

test(source_code_analysis:gxl, xpce_address_to_gxl_id) :-
   xpce_address_to_gxl_id(@'1234-3379298', 1234),
   xpce_address_to_gxl_id(@'(a:b)/3-3382561', '(a:b)/3'),
   xpce_address_to_gxl_id(@'\'N\'-3382561', 'N' ),
   xpce_address_to_gxl_id(@'(a:b)/7-12345', (a:b)/7 ),
   xpce_address_to_gxl_id(@'(a:b)/7-12345', '(a:b)/7' ),
   xpce_address_to_gxl_id(@'\'/path/file.pl\'-12345',
      '/path/file.pl' ),
   xpce_address_to_gxl_id(@'n2-12345', n2),
   !.

test(source_code_analysis:gxl, gxl_to_ugraph_to_gxl) :-
   gxl(example_5, Gxl_1),
   gxl_to_ugraph(Gxl_1, Ugraph),
   Ugraph = [1234-['N'], '(a:b)/3'-[], 'N'-['(a:b)/3']],
   ugraph_to_gxl(Ugraph, Gxl_2),
   Gxl_2 =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Default GXL', width:450, height:400, background:white, mouse_click:picture]:[],
         graph:[edgeids:true, edgemode:directed, hypergraph:false, id:_]:[
            node:[id:1234]:[prmtr:[color:grey, handles:default, mouse_click:node, size:small, symbol:no_symbol, x_pos:_, y_pos:_]:[string:[bubble:'1234', font_style: (helvetica, roman, 12)]:[1234]]],
            node:[id:'(a:b)/3']:[prmtr:[color:grey, handles:default, mouse_click:node, size:small, symbol:no_symbol, x_pos:_, y_pos:_]:[string:[bubble:'\'(a:b)/3\'', font_style: (helvetica, roman, 12)]:['(a:b)/3']]],
            node:[id:'N']:[prmtr:[color:grey, handles:default, mouse_click:node, size:small, symbol:no_symbol, x_pos:_, y_pos:_]:[string:[bubble:'\'N\'', font_style: (helvetica, roman, 12)]:['N']]],
            edge:[from:1234, id:_, to:'N']:[prmtr:[arrows:both, color:black, pen:1, mouse_click:edge, first_arrow:first_arrow, second_arrow:second_arrow, weight:1]:[string:[font_style: (helvetica, roman, 12), bubble:'Default Edge with Arrow Configuration']:[]]],
            edge:[from:'N', id:_, to:'(a:b)/3']:[prmtr:[arrows:both, color:black, pen:1, mouse_click:edge, first_arrow:first_arrow, second_arrow:second_arrow, weight:1]:[string:[font_style: (helvetica, roman, 12), bubble:'Default Edge with Arrow Configuration']:[]]]]],
   !.

test(source_code_analysis:gxl, roots) :-
   Nodes = [r1, r2, r3, r4,
      a, b, c, d, e, f, g],
   Edges = [r1-r1, r2-a, r3-r3, r3-b,
      r2-c, c-c, r2-d, d-e, r2-f, f-f, f-g],
   vertices_and_edges_to_possible_roots(Nodes, Edges, E),
   E = [r1, r2, r3, r4],
   !.

test(source_code_analysis:gxl, handles_1) :-
   alias_to_default_fn_triple(gxl_config, Config),
   gxl(handles, Gxl_1),
   gxl_to_picture(Config, Gxl_1, _),
   !.
test(source_code_analysis:gxl, handles_2) :-
   alias_to_default_fn_triple(gxl_config, Config),
   gxl(handles_2, Gxl_1),
   gxl_to_picture(Config, Gxl_1, _),
   !.

test(source_code_analysis:gxl, chart) :-
   alias_to_default_fn_triple(gxl_config, Config),
   gxl(example_bar_graph, Gxl_1),
   gxl_to_picture(Config, Gxl_1, _),
   !.

test(source_code_analysis:gxl, separate_and_merge_layout_1) :-
   dislog_variable_get(source_path, SP),
   !,
   concat(SP, 'source_code_analysis/gxl/tests/', SCA_Tests),
   concat(SCA_Tests, 'graph.gxl', File),
   concat(SCA_Tests, 'graph_gxl.gxl', Gxl_File),
   concat(SCA_Tests, 'graph_layout.xml', Layout_File),
   concat(SCA_Tests, 'graph_merged.gxl', Merged_File),
   dread_(xml(gxl_graph), File, Gxl_1),
   writeln('Gxl document read.'),
   separate_gxl_from_layout(Gxl_1, Gxl_2, Layout),
   writeln('Gxl document separated.'),
   merge_gxl_and_layout(Gxl_2, Layout, Gxl_3),
   writeln('Gxl document merged.'),
   dwrite(xml_2, Gxl_File, Gxl_2),
   dwrite(xml_2, Layout_File, Layout),
   dwrite(xml_2, Merged_File, Gxl_3),
   checklist( delete_file,
      [Gxl_File, Layout_File, Merged_File] ),
   !.

test(source_code_analysis:gxl, separate_and_merge_layout_2) :-
   dislog_variable_get(source_path, SP),
   !,
   concat(SP, 'source_code_analysis/gxl/tests/', SCA_Tests),
   concat(SCA_Tests, 'vertices_edges_to_gxl.gxllayout', L_File),
   concat(SCA_Tests, 'vertices_edges_to_gxl_org.gxl', File),
   dread_(xml(gxl_graph), L_File, Layout),
   dread_(xml(gxl_graph), File, Gxl_1),
   merge_gxl_and_layout(Gxl_1, Layout, Gxl_2),
   gxl_to_picture(config:[]:[], Gxl_2, _),
   !.

test(source_code_analysis:gxl, gxl_to_picture_to_gxl_1) :-
   new(Picture, picture(gxl_to_picture_to_gxl)),
   send(Picture, open),
   alias_to_default_fn_triple(gxl_config, Config),
   vertex_to_gxl_vertex_complete('A', Node_A),
   vertex_to_gxl_vertex_complete('B', Node_B),
   term_to_atom('A'-'B', Id),
   Edge_1 = edge:[from:'A', to:'B', id:Id]:[],
   edge_to_gxl_edge_complete(Edge_1, Edge_2),
   Gxl_1 = gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
      prmtr:[background:white, height:400,
         label:'Gxl to Picture to Gxl', mouse_click:picture,
         width:450]:[],
      graph:[edgeids:true, edgemode:directed, hypergraph:false,
          id:test_gxl_to_picture_to_gxl]:[
          Node_A, Node_B, Edge_2] ],
   gxl_to_picture(Config, Gxl_1, Picture),
   picture_to_gxl(Picture, Gxl_2, _),
   Gxl_2 =
      gxl:[]:[
         graph:[]:[
            node:[id:'A']:[
               prmtr:[color:grey, handles:default, mouse_click:node,
               size:small, symbol:no_symbol, x_pos:_, y_pos:_]:[
               string:[bubble:'\'A\'', font_style: (helvetica, roman, 12)]:[
               'A']]],
            node:[id:'B']:[prmtr:[color:grey, handles:default,
               mouse_click:node, size:small, symbol:no_symbol, x_pos:_,
               y_pos:_]:[string:[bubble:'\'B\'',
               font_style: (helvetica, roman, 12)]:['B']]],
            edge:[from:'A', id:'\'A\'-\'B\'', to:'B']:[
               prmtr:[arrows:both, color:black, first_arrow:first_arrow,
               mouse_click:edge, pen:1, second_arrow:second_arrow,
               weight:1]:[string:[
               bubble:'Default Edge with Arrow Configuration',
               font_style: (helvetica, roman, 12)]:[]]]]],
   send(Picture, destroy),
   !.

test(source_code_analysis:gxl, gxl_to_picture_to_gxl_2) :-
   alias_to_default_fn_triple(gxl_config, Config),
   vertex_to_gxl_vertex_complete('A', Node_A),
   vertex_to_gxl_vertex_complete('B', Node_B),
   term_to_atom('A'-'B', Id),
   Edge_1 = edge:[from:'A', to:'B', id:Id]:[],
   edge_to_gxl_edge_complete(Edge_1, Edge_2),
   Gxl_1 = gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
      prmtr:[background:white, height:400,
         label:'Gxl to Picture to Gxl', mouse_click:picture,
         width:450]:[],
      graph:[edgeids:true, edgemode:directed, hypergraph:false,
          id:test_gxl_to_picture_to_gxl]:[
          Node_A, Node_B, Edge_2] ],
   gxl_to_picture(Config, Gxl_1, Picture),
   picture_to_gxl(Picture, Gxl_2, _),
   Gxl_2 =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[background:white, height:400,
               label:'Gxl to Picture to Gxl',
               mouse_click:picture, width:450]:[],
         graph:[edgeids:true, edgemode:directed, hypergraph:false,
               id:test_gxl_to_picture_to_gxl]:[
            node:[id:'A']:[
               prmtr:[color:grey, handles:default, mouse_click:node,
               size:small, symbol:no_symbol, x_pos:_, y_pos:_]:[
               string:[bubble:'\'A\'', font_style: (helvetica, roman, 12)]:[
               'A']]],
            node:[id:'B']:[prmtr:[color:grey, handles:default,
               mouse_click:node, size:small, symbol:no_symbol, x_pos:_,
               y_pos:_]:[string:[bubble:'\'B\'',
               font_style: (helvetica, roman, 12)]:['B']]],
            edge:[from:'A', id:'\'A\'-\'B\'', to:'B']:[
               prmtr:[arrows:both, color:black, first_arrow:first_arrow,
               mouse_click:edge, pen:1, second_arrow:second_arrow,
               weight:1]:[string:[
               bubble:'Default Edge with Arrow Configuration',
               font_style: (helvetica, roman, 12)]:[]]]]],
   send(Picture, destroy),
   !.

test(source_code_analysis:gxl, gxl_to_tree) :-
   sca_variable_get(sca, H),
   concat(H, 'common/tests/squashml.dtd', File),
   visur:dtd_to_gxl(File, Gxl),
%  gxl_to_picture(Gxl),
   fn_to_xml(Gxl),
   gxl_to_tree(Gxl, Tree),
   alias_to_default_fn_triple(hierarchy_browser_config, Config),
   hierarchy_to_browser_ddk(Config, Tree, Browser),
   send(Browser, destroy),
   !.


test(source_code_analysis:gxl, gxl_contract_nodes) :-
   gxl(example_3, Gxl_1),
   gxl_contract_nodes(Gxl_1, Gxl_2),
   gxl_to_picture(Gxl_2),
   !.


test(source_code_analysis:gxl, rnes_to_tfbcs) :-
   gxl_tfbc:rnes_to_tfbcs([a], [b, c, d], [a-c, a-b, b-d, b-c, a-d, d-a],
         T_E, F_E, B_E, C_E),
   T_E = [a-b, a-c, b-d],
   F_E = [a-d],
   B_E = [d-a],
   C_E = [b-c],
   !.


/*** implementation ***********************************************/


test_sub_1(Picture) :-
   gxl(example_1, Gxl),
   alias_to_default_fn_triple(gxl_config, Config),
   gxl_to_picture(Config, Gxl, Picture),
   gxl_element_to_element_complete(node:[id:id_test_101]:[],
      Node_1),
   gxl_node_to_picture(Config,
      Node_1, Picture),
   gxl_element_to_element_complete(node:[id:_]:[], Node_2),
   gxl_node_to_picture(Config,
      Node_2, Picture, Node_Id),
   gxl_element_to_element_complete(edge:[id:_, from:id_test_101,
         to:Node_Id]:[prmtr:[color:green]:[]], Edge),
   gxl_edge_to_picture(Config, Edge,
      Picture).

test_sub_2(Picture_1, Picture_2) :-
   picture_to_gxl(Picture_1, Gxl, Config),
   gxl_to_picture(Config, Gxl, Picture_2).

test_sub_3(Picture) :-
   gxl(example_2, Gxl_1),
   alias_to_default_fn_triple(gxl_config, Config),
   gxl_to_picture(Config, Gxl_1, Picture),
   picture_to_gxl(Picture, Gxl_2, _),
   gxl_to_picture(Config, Gxl_2, _).


/******************************************************************/


