

/******************************************************************/
/***                                                            ***/
/***          GXL: Graph Examples                               ***/
/***                                                            ***/
/******************************************************************/

:- discontiguous gxl/2.


/*** interface ****************************************************/


gxl(example_1, Gxl) :-
   checklist( gensym(example_id_),
      [Node_Id_1, Node_Id_2] ),
   gensym(example_id_, Edge_Id_1),
   Vertices = [
      Node_Id_1-[no, proplems, 'With', 'Capital',
         letters]-no_symbol-light_green-medium-(100, 60),
      Node_Id_2-[text_in_box, 'line two', 'line three']-text_in_box-white-medium-(360, 60) ],
   maplist( vertex_to_gxl_vertex_complete,
      Vertices, Gxl_Nodes ),
   edge_to_gxl_edge_complete(
      Edge_Id_1-Node_Id_1-Node_Id_2-green-second-1, Gxl_Edge),
   Gxl =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Graph Example 1',
               width:500, height:350]:[],
         graph:[id:graph_3, edgeids:true, edgemode:directed,
               hypergraph:false]:[
            Gxl_Edge|Gxl_Nodes] ],
   !.

gxl(example_2, Gxl) :-
%   Node_Id_1 = id_1_1,
%   Node_Id_2 = id_2_1,
   dislog_variable_get(sca, SCA_Path),
   !,
   concat(SCA_Path, 'gxl/tests/trash.bm', Bitmap),
   gensym(example_id_, Node_Id_1),
   gensym(example_id_, Node_Id_2),
   term_to_atom(Node_Id_1-Node_Id_2, Edge_Id),
   Gxl =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Graph Example 2']:[],
         graph:[id:graph_2, edgeids:true, edgemode:directed,
               hypergraph:false]:[
            node:[id:Node_Id_1]:[
               prmtr:[color:green, size:1,
                     symbol:Bitmap,
                     x_pos:100,
                     y_pos:50]:[
                  string:[bubble:'My nice Trash']:[my, nice, 'Trash']]
            ],
            node:[id:Node_Id_2]:[
               prmtr:[color:orange, size:medium,
                     symbol:circle,
                     x_pos:200,
                     y_pos:150]:[
                  string:[bubble:'My nice node']:[
                     my, nice, node]]
            ],
            edge:[from:Node_Id_1, id:Edge_Id,
                  to:Node_Id_2]:[
               prmtr:[arrows:both, color:blue,
                  first_arrow:first_arrow,
                  second_arrow:second_arrow]:[]
            ]
         ]
      ],
   !.

gxl(example_3, Gxl) :-
   checklist( gensym(example_id_),
      [Node_Id_1, Node_Id_2, Node_Id_3,
       Node_Id_4, Node_Id_5, Node_Id_6, Node_Id_7] ),
   checklist( gensym(example_id_),
      [Edge_Id_1, Edge_Id_2, Edge_Id_3,
       Edge_Id_4, Edge_Id_5, Edge_Id_6,
       Edge_Id_7, Edge_Id_8] ),
   Vertices = [
      Node_Id_1-['little Box 1']-box-green-medium-(10, 10),
      Node_Id_2-[box_2]-box-dark_green-medium-(250, 10),
      Node_Id_3-['Box 3']-box-green-medium-(130, 150),
      Node_Id_4-['Circle 1']-circle-dark_blue-medium-(130, 250),
      Node_Id_5-['Circle 2']-circle-dark_blue-medium-(250, 350),
      Node_Id_6-['Circle 3']-circle-dark_blue-medium-(15, 350),
      Node_Id_7-['Circle 4']-circle-dark_blue-medium-(130, 450) ],
   maplist( vertex_to_gxl_vertex_complete,
      Vertices, Gxl_Nodes ),
   Edges_1 = [
      Edge_Id_1-Node_Id_1-Node_Id_2-dark_green-second-1-arrow_a,
      Edge_Id_2-Node_Id_2-Node_Id_3-dark_green-second-1-arrow_b,
      Edge_Id_3-Node_Id_3-Node_Id_1-dark_green-second-1-arrow_c,
      Edge_Id_4-Node_Id_4-Node_Id_5-yellow-second-1-arrow_d,
      Edge_Id_5-Node_Id_5-Node_Id_6-yellow-second-1-arrow_e,
      Edge_Id_6-Node_Id_6-Node_Id_4-yellow-second-1-arrow_f,
      Edge_Id_7-Node_Id_3-Node_Id_4-orange-second-3-arrow_g,
      Edge_Id_8-Node_Id_6-Node_Id_7-yellow-second-1-arrow_h ],
   maplist( edge_to_gxl_edge_complete,
      Edges_1, Gxl_Edges_1 ),
   append(Gxl_Nodes, Gxl_Edges_1, Gxl_Nodes_and_Edges_1),
   gxl_presetting(edge, Gxl_Edge_1),
   Gxl_Edge_2 := Gxl_Edge_1*[@from:Node_Id_7, @to:Node_Id_5],
   Gxl_Nodes_and_Edges_2 =[Gxl_Edge_2|Gxl_Nodes_and_Edges_1],
   Gxl =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Graph Example 3',
               width:500, height:550,
               background:light_blue,
               mouse_click:picture]:[],
         graph:[id:graph_3, edgeids:true, edgemode:directed,
               hypergraph:false]:Gxl_Nodes_and_Edges_2 ],
   !.

gxl(embedded_graph, Gxl) :-
   Node_Id_1 = n1,
   Node_Id_2 = n2,
   Node_Id_3 = n3,
   Vertices = [
      Node_Id_2-['little Box 1']-box-orange-medium-(50, 10),
      Node_Id_3-[little, 'Box', 2]-box-orange-medium-(150, 90) ],
   Edges = [
      edge_1-Node_Id_1-Node_Id_2-green-second-1,
      edge_2-Node_Id_2-Node_Id_3-yellow-second-2 ],
   maplist( vertex_to_gxl_vertex_complete,
      Vertices, Gxl_Nodes ),
   maplist( edge_to_gxl_edge_complete,
      Edges, [Gxl_Edge_1, Gxl_Edge_2] ),
   Gxl =
   gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
      prmtr:[label:'Graph Example 4 - Hypergraph',
            background:yellow, mouse_click:picture]:[],
      graph:[id:graph_4, edgeids:true, edgemode:directed,
            hypergraph:false]:[
         node:[id:Node_Id_1]:[
            prmtr:[
                  mouse_click:embedded_node,
                  color:red,
                  size:medium,
                  symbol:circle,
                  x_pos:70,
                  y_pos:10]:[
               string:[bubble:'Embedded Graph',
                       font_style:(helvetica, roman, 12)]:[
                         'Embedded Graph',
                  'click me with middle',
                  'mouse button']],
            graph:[id:graph_2, edgeids:true, edgemode:directed,
                  hypergraph:false]:[
               prmtr:[label:'Content of Hypernode']:[],
                Gxl_Edge_2|Gxl_Nodes]
         ],
         Gxl_Edge_1
      ]
   ],
   !.
gxl(example_5, Gxl) :-
   Node_Id_1 := 1234,
   Node_Id_2 := 'N',
   gxl_presetting(node, Node_Id_1, Gxl_Node_1),
   gxl_presetting(node, Node_Id_2, Gxl_Node_2),
   Gxl =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Simple Example 5',
               width:500, height:350,
               background:light_blue,
               mouse_click:picture]:[],
         graph:[id:graph_1, edgeids:true, edgemode:directed,
               hypergraph:false]:[
            Gxl_Node_1,
            Gxl_Node_2,
            node:[id:'(a:b)/3']:[
               prmtr:[
                  color:red,
                  size:medium,
                  symbol:rhombus,
                  x_pos:163,
                  y_pos:60]:[
               string:[font_style:(helvetica, roman, 12),
                  bubble:node_3]:[node_3]]],
            edge:[from:Node_Id_2, id:555,
                  to:'(a:b)/3']:[
               prmtr:[arrows:both, color:red, pen:1,
                     first_arrow:first_arrow,
                     second_arrow:second_arrow]:[
                  string:[font_style:(screen, roman, 12),
                     bubble:'my default edge 1']:[]
               ]
            ],
            edge:[from:Node_Id_1, id:i123456,
                  to:Node_Id_2]:[
               prmtr:[arrows:second, color:red, pen:1,
                     second_arrow:second_arrow]:[
                  string:[font_style:(times, roman, 12),
                      bubble:'my default edge 2']:[
                      my, default, 'edge 2']
               ]
            ]
         ]
      ],
   !.
gxl(example_6, Gxl) :-
   Gxl =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'One Node',
               width:500, height:350,
               background:beige,
               mouse_click:picture]:[],
         graph:[id:graph_1, edgeids:true, edgemode:directed,
               hypergraph:false]:[
            node:[id:12345]:[
               prmtr:[
                  color:red,
                  size:41,
                  symbol:honeycomb,
                  x_pos:160,
                  y_pos:60]:[
               string:[font_style:(helvetica, roman, 24),
                   bubble:'This is a Circle']:[
                  'Circle', name]]
            ]
         ]
      ].
gxl(arrow_types, Gxl) :-
   checklist( gensym(example_id_),
      [Node_Id_1, Node_Id_2, Node_Id_3,
       Node_Id_4, Node_Id_5, Node_Id_6,
       Node_Id_7, Node_Id_8] ),
   checklist( gensym(example_id_),
      [Edge_Id_1, Edge_Id_2, Edge_Id_3,
       Edge_Id_4] ),
   Vertices = [
      Node_Id_1-[]-circle-white-medium-(30, 10),
      Node_Id_2-['fill_pattern=nil', 'style=open']-circle-white-medium-(250, 10),
      Node_Id_3-[]-circle-white-medium-(30, 110),
      Node_Id_4-['fill_pattern=white_image', 'style=open']-circle-white-medium-(250,110),
      Node_Id_5-[]-circle-white-medium-(30, 210),
      Node_Id_6-['fill_pattern=white_image', 'style=closed']-circle-white-medium-(250, 210),
      Node_Id_7-[]-circle-white-medium-(30, 310),
      Node_Id_8-['fill_pattern=black_image', 'style=open']-circle-white-medium-(250, 310) ],
   maplist( vertex_to_gxl_vertex_complete,
      Vertices, Gxl_Nodes ),
   Edges_1 = [
      Edge_Id_1-Node_Id_1-Node_Id_2-black-second-1-arrow_1,
      Edge_Id_2-Node_Id_3-Node_Id_4-black-second-1-arrow_2,
      Edge_Id_3-Node_Id_5-Node_Id_6-black-second-1-arrow_3,
      Edge_Id_4-Node_Id_7-Node_Id_8-black-second-1-arrow_4],
   maplist( edge_to_gxl_edge_complete,
      Edges_1, Gxl_Edges_1 ),
   append(Gxl_Nodes, Gxl_Edges_1, Gxl_Nodes_and_Edges_1),
   Gxl =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Arrow Types',
               width:400, height:400,
               background:white,
               mouse_click:picture]:[],
         graph:[id:graph_3, edgeids:true, edgemode:directed,
               hypergraph:false]:Gxl_Nodes_and_Edges_1],
   !.

gxl(id_with_capital_letters, Gxl) :-
   Gxl =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Graph Example 2']:[],
         graph:[id:graph_2, edgeids:true, edgemode:directed,
               hypergraph:false]:[
            node:[id:'Node_Id_1']:[
               prmtr:[color:green, size:medium, symbol:box,
                     x_pos:30,
                     y_pos:130]:[
                  string:[bubble:'My nice Box']:[my, nice, 'Box']]
            ],
            node:[id:'Node_Id_2']:[
               prmtr:[color:orange, size:medium,
                     symbol:circle,
                     x_pos:160,
                     y_pos:40]:[
                  string:[bubble:'My nice node']:[
                     my, nice, folder]]
            ],
            edge:[from:'Node_Id_1', id:'Edge_Id',
                  to:'Node_Id_2']:[
               prmtr:[arrows:both, color:blue,
                  first_arrow:first_arrow,
                  second_arrow:second_arrow]:[
                  string:[bubble:'Edge']:[edge, test]
               ]
            ]
         ]
      ],
   !.
gxl(metty, Gxl) :-
   Gxl = gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
      graph:[id:default_graph, edgeids:true, edgemode:directed, hypergraph:false]:[
         node:[id:'TBLENZYMES']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:231, y_pos:24]:[string:[bubble:'']:['TBLENZYMES']]],
         node:[id:'TBLDICTPEPTIDES']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:279, y_pos:41]:[string:[bubble:'']:['TBLDICTPEPTIDES']]],
         node:[id:'TBLGERAET']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:80, y_pos:127]:[string:[bubble:'']:['TBLGERAET']]], node:[id:'TBLALGORITHM']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:77, y_pos:248]:[string:[bubble:'']:['TBLALGORITHM']]],
         node:[id:'TBLUSER']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:139, y_pos:125]:[string:[bubble:'']:['TBLUSER']]], node:[id:'TBLRESPROJEKT']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:195, y_pos:155]:[string:[bubble:'']:['TBLRESPROJEKT']]], node:[id:'TBLRESEXPERIMENT']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:289, y_pos:295]:[string:[bubble:'']:['TBLRESEXPERIMENT']]], node:[id:'TBLRESRUN']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:285, y_pos:74]:[string:[bubble:'']:['TBLRESRUN']]], node:[id:'TBLRESSPECLOCK']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:66, y_pos:230]:[string:[bubble:'']:['TBLRESSPECLOCK']]], node:[id:'TBLRESSPECTRA']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:215, y_pos:286]:[string:[bubble:'']:['TBLRESSPECTRA']]], node:[id:'TBLRESSEARCH']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:121, y_pos:74]:[string:[bubble:'']:['TBLRESSEARCH']]], node:[id:'TBLRESSEARCHRUN']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:102, y_pos:65]:[string:[bubble:'']:['TBLRESSEARCHRUN']]], node:[id:'TBLRESSEARCHEXPERIMENT']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:261, y_pos:78]:[string:[bubble:'']:['TBLRESSEARCHEXPERIMENT']]], node:[id:'TBLRESSEARCHPARAMETERS']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:36, y_pos:5]:[string:[bubble:'']:['TBLRESSEARCHPARAMETERS']]],
         node:[id:'TBLRESSEARCHRESULTS']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:8, y_pos:153]:[string:[bubble:'']:['TBLRESSEARCHRESULTS']]],
         node:[id:'TBLRESSEARCHPEPTIDES']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:182, y_pos:286]:[string:[bubble:'']:['TBLRESSEARCHPEPTIDES']]], node:[id:'TBLRESPEPTIDES2SEQUENCES']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:6, y_pos:252]:[string:[bubble:'']:['TBLRESPEPTIDES2SEQUENCES']]], node:[id:'TBLRESSEARCHFLAGS']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:129, y_pos:152]:[string:[bubble:'']:['TBLRESSEARCHFLAGS']]], node:[id:'TBLSEARCHPARAMETERSSEQUEST']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:0, y_pos:207]:[string:[bubble:'']:['TBLSEARCHPARAMETERSSEQUEST']]], node:[id:'TBLSEARCHPARAMETERSMASCOT']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:186, y_pos:74]:[string:[bubble:'']:['TBLSEARCHPARAMETERSMASCOT']]], node:[id:'TBLSEARCHRESULTSSEQUEST']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:70, y_pos:44]:[string:[bubble:'']:['TBLSEARCHRESULTSSEQUEST']]], node:[id:'TBLSEARCHRESULTSMASCOTQUERY']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:160, y_pos:39]:[string:[bubble:'']:['TBLSEARCHRESULTSMASCOTQUERY']]], node:[id:'TBLSEARCHRESULTSMASCOTPEPTIDE']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:107, y_pos:42]:[string:[bubble:'']:['TBLSEARCHRESULTSMASCOTPEPTIDE']]], node:[id:'TBLSEARCHRESULTSMASCOTPROTEIN']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:293, y_pos:108]:[string:[bubble:'']:['TBLSEARCHRESULTSMASCOTPROTEIN']]],
         node:[id:'TBLRESSEQUESTPRECALCKEYS']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:68, y_pos:67]:[string:[bubble:'']:['TBLRESSEQUESTPRECALCKEYS']]], node:[id:'TBLRESSEQUESTPRECALCCOUNTED']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:56, y_pos:228]:[string:[bubble:'']:['TBLRESSEQUESTPRECALCCOUNTED']]], node:[id:'TBLRESSEQUESTPRECALCPPOSCOUNT']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:82, y_pos:67]:[string:[bubble:'']:['TBLRESSEQUESTPRECALCPPOSCOUNT']]], node:[id:'TBLRESSEQUESTPRECALCSPECLIST']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:54, y_pos:138]:[string:[bubble:'']:['TBLRESSEQUESTPRECALCSPECLIST']]], node:[id:'TBLRESSEQUESTPRECALCACCIDLST']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:39, y_pos:212]:[string:[bubble:'']:['TBLRESSEQUESTPRECALCACCIDLST']]], node:[id:'TBLRESFALSEPOSITIVE']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:179, y_pos:244]:[string:[bubble:'']:['TBLRESFALSEPOSITIVE']]], node:[id:'TBLRESDSNTABLES']:[prmtr:[color:white, font_style: (helvetica, roman, 12), handles:default, mouse_click:alias_db_tool_node_click, size:medium, symbol:text_in_box, x_pos:96, y_pos:182]:[string:[bubble:'']:['TBLRESDSNTABLES']]],
         edge:[from:'TBLRESPROJEKT', id:id_1, to:'TBLUSER']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESPROJEKT\'-\'TBLUSER\'']:[]]],
         edge:[from:'TBLRESEXPERIMENT', id:id_2, to:'TBLGERAET']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESEXPERIMENT\'-\'TBLGERAET\'']:[]]],
         edge:[from:'TBLRESEXPERIMENT', id:id_3, to:'TBLRESPROJEKT']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESEXPERIMENT\'-\'TBLRESPROJEKT\'']:[]]],
         edge:[from:'TBLRESRUN', id:id_4, to:'TBLRESEXPERIMENT']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESRUN\'-\'TBLRESEXPERIMENT\'']:[]]],
         edge:[from:'TBLRESRUN', id:id_5, to:'TBLGERAET']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESRUN\'-\'TBLGERAET\'']:[]]],
         edge:[from:'TBLRESSPECTRA', id:id_6, to:'TBLRESRUN']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSPECTRA\'-\'TBLRESRUN\'']:[]]],
         edge:[from:'TBLRESSPECTRA', id:id_7, to:'TBLRESEXPERIMENT']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSPECTRA\'-\'TBLRESEXPERIMENT\'']:[]]],
         edge:[from:'TBLRESSEARCH', id:id_8, to:'TBLRESEXPERIMENT']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCH\'-\'TBLRESEXPERIMENT\'']:[]]],
         edge:[from:'TBLRESSEARCH', id:id_9, to:'TBLALGORITHM']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCH\'-\'TBLALGORITHM\'']:[]]],
         edge:[from:'TBLRESSEARCHRUN', id:id_10, to:'TBLRESSEARCH']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHRUN\'-\'TBLRESSEARCH\'']:[]]],
         edge:[from:'TBLRESSEARCHRUN', id:id_11, to:'TBLRESRUN']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHRUN\'-\'TBLRESRUN\'']:[]]],
         edge:[from:'TBLRESSEARCHEXPERIMENT', id:id_12, to:'TBLRESEXPERIMENT']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHEXPERIMENT\'-\'TBLRESEXPERIMENT\'']:[]]],
         edge:[from:'TBLRESSEARCHEXPERIMENT', id:id_13, to:'TBLRESSEARCH']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHEXPERIMENT\'-\'TBLRESSEARCH\'']:[]]],
         edge:[from:'TBLRESSEARCHEXPERIMENT', id:id_14, to:'TBLUSER']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHEXPERIMENT\'-\'TBLUSER\'']:[]]], edge:[from:'TBLRESSEARCHPARAMETERS', id:id_15, to:'TBLALGORITHM']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHPARAMETERS\'-\'TBLALGORITHM\'']:[]]], edge:[from:'TBLRESSEARCHRESULTS', id:id_16, to:'TBLALGORITHM']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHRESULTS\'-\'TBLALGORITHM\'']:[]]], edge:[from:'TBLRESSEARCHPEPTIDES', id:id_17, to:'TBLDICTPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHPEPTIDES\'-\'TBLDICTPEPTIDES\'']:[]]], edge:[from:'TBLRESSEARCHPEPTIDES', id:id_18, to:'TBLDICTPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHPEPTIDES\'-\'TBLDICTPEPTIDES\'']:[]]],
         edge:[from:'TBLRESSEARCHPEPTIDES', id:id_19, to:'TBLRESSEARCH']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[
            string:[bubble:'\'TBLRESSEARCHPEPTIDES\'-\'TBLRESSEARCH\'']:[]]], edge:[from:'TBLRESSEARCHPEPTIDES', id:id_20, to:'TBLRESSEARCHRESULTS']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHPEPTIDES\'-\'TBLRESSEARCHRESULTS\'']:[]]], edge:[from:'TBLRESPEPTIDES2SEQUENCES', id:id_21, to:'TBLRESSEARCHPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESPEPTIDES2SEQUENCES\'-\'TBLRESSEARCHPEPTIDES\'']:[]]], edge:[from:'TBLRESPEPTIDES2SEQUENCES', id:id_22, to:'TBLSEQSEQUENZEN']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESPEPTIDES2SEQUENCES\'-\'TBLSEQSEQUENZEN\'']:[]]], edge:[from:'TBLRESSEARCHFLAGS', id:id_23, to:'TBLRESSEARCH']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHFLAGS\'-\'TBLRESSEARCH\'']:[]]], edge:[from:'TBLRESSEARCHFLAGS', id:id_24, to:'TBLRESSEARCHRESULTS']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHFLAGS\'-\'TBLRESSEARCHRESULTS\'']:[]]], edge:[from:'TBLRESSEARCHFLAGS', id:id_25, to:'TBLUSER']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHFLAGS\'-\'TBLUSER\'']:[]]], edge:[from:'TBLRESSEARCHFLAGS', id:id_26, to:'TBLRESSEARCHPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEARCHFLAGS\'-\'TBLRESSEARCHPEPTIDES\'']:[]]], edge:[from:'TBLSEARCHPARAMETERSSEQUEST', id:id_27, to:'TBLRESSEARCH']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHPARAMETERSSEQUEST\'-\'TBLRESSEARCH\'']:[]]],
         edge:[from:'TBLSEARCHPARAMETERSMASCOT', id:id_28, to:'TBLRESSEARCH']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHPARAMETERSMASCOT\'-\'TBLRESSEARCH\'']:[]]], edge:[from:'TBLSEARCHPARAMETERSMASCOT', id:id_29, to:'TBLENZYMES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHPARAMETERSMASCOT\'-\'TBLENZYMES\'']:[]]], edge:[from:'TBLSEARCHRESULTSSEQUEST', id:id_30, to:'TBLDICTPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSSEQUEST\'-\'TBLDICTPEPTIDES\'']:[]]], edge:[from:'TBLSEARCHRESULTSSEQUEST', id:id_31, to:'TBLDICTPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSSEQUEST\'-\'TBLDICTPEPTIDES\'']:[]]], edge:[from:'TBLSEARCHRESULTSSEQUEST', id:id_32, to:'TBLRESSEARCHPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSSEQUEST\'-\'TBLRESSEARCHPEPTIDES\'']:[]]], edge:[from:'TBLSEARCHRESULTSSEQUEST', id:id_33, to:'TBLRESSPECTRA']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSSEQUEST\'-\'TBLRESSPECTRA\'']:[]]], edge:[from:'TBLSEARCHRESULTSSEQUEST', id:id_34, to:'TBLRESSEARCH']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSSEQUEST\'-\'TBLRESSEARCH\'']:[]]], edge:[from:'TBLSEARCHRESULTSMASCOTQUERY', id:id_35, to:'TBLRESSPECTRA']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSMASCOTQUERY\'-\'TBLRESSPECTRA\'']:[]]], edge:[from:'TBLSEARCHRESULTSMASCOTQUERY', id:id_36, to:'TBLRESSEARCH']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[
            string:[bubble:'\'TBLSEARCHRESULTSMASCOTQUERY\'-\'TBLRESSEARCH\'']:[]]], edge:[from:'TBLSEARCHRESULTSMASCOTPEPTIDE', id:id_37, to:'TBLSEARCHRESULTSMASCOTQUERY']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSMASCOTPEPTIDE\'-\'TBLSEARCHRESULTSMASCOTQUERY\'']:[]]], edge:[from:'TBLSEARCHRESULTSMASCOTPEPTIDE', id:id_38, to:'TBLDICTPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSMASCOTPEPTIDE\'-\'TBLDICTPEPTIDES\'']:[]]], edge:[from:'TBLSEARCHRESULTSMASCOTPEPTIDE', id:id_39, to:'TBLDICTPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSMASCOTPEPTIDE\'-\'TBLDICTPEPTIDES\'']:[]]], edge:[from:'TBLSEARCHRESULTSMASCOTPEPTIDE', id:id_40, to:'TBLRESSEARCHPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSMASCOTPEPTIDE\'-\'TBLRESSEARCHPEPTIDES\'']:[]]], edge:[from:'TBLSEARCHRESULTSMASCOTPEPTIDE', id:id_41, to:'TBLRESSPECTRA']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSMASCOTPEPTIDE\'-\'TBLRESSPECTRA\'']:[]]],
         edge:[from:'TBLSEARCHRESULTSMASCOTPEPTIDE', id:id_42, to:'TBLRESSEARCH']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSMASCOTPEPTIDE\'-\'TBLRESSEARCH\'']:[]]], edge:[from:'TBLSEARCHRESULTSMASCOTPROTEIN', id:id_43, to:'TBLSEARCHRESULTSMASCOTQUERY']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSMASCOTPROTEIN\'-\'TBLSEARCHRESULTSMASCOTQUERY\'']:[]]],
         edge:[from:'TBLSEARCHRESULTSMASCOTPROTEIN', id:id_44, to:'TBLRESSEARCHPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSMASCOTPROTEIN\'-\'TBLRESSEARCHPEPTIDES\'']:[]]], edge:[from:'TBLSEARCHRESULTSMASCOTPROTEIN', id:id_45, to:'TBLDICTPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSMASCOTPROTEIN\'-\'TBLDICTPEPTIDES\'']:[]]], edge:[from:'TBLSEARCHRESULTSMASCOTPROTEIN', id:id_46, to:'TBLDICTPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSMASCOTPROTEIN\'-\'TBLDICTPEPTIDES\'']:[]]], edge:[from:'TBLSEARCHRESULTSMASCOTPROTEIN', id:id_47, to:'TBLRESSEARCHPEPTIDES']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSMASCOTPROTEIN\'-\'TBLRESSEARCHPEPTIDES\'']:[]]], edge:[from:'TBLSEARCHRESULTSMASCOTPROTEIN', id:id_48, to:'TBLRESSPECTRA']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLSEARCHRESULTSMASCOTPROTEIN\'-\'TBLRESSPECTRA\'']:[]]], edge:[from:'TBLSEARCHRESULTSMASCOTPROTEIN', id:id_49, to:'TBLRESSEARCH']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[
            string:[bubble:'\'TBLSEARCHRESULTSMASCOTPROTEIN\'-\'TBLRESSEARCH\'']:[]]], edge:[from:'TBLRESSEQUESTPRECALCCOUNTED', id:id_50, to:'TBLRESSEQUESTPRECALCKEYS']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEQUESTPRECALCCOUNTED\'-\'TBLRESSEQUESTPRECALCKEYS\'']:[]]], edge:[from:'TBLRESSEQUESTPRECALCPPOSCOUNT', id:id_51, to:'TBLRESSEQUESTPRECALCCOUNTED']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEQUESTPRECALCPPOSCOUNT\'-\'TBLRESSEQUESTPRECALCCOUNTED\'']:[]]], edge:[from:'TBLRESSEQUESTPRECALCPPOSCOUNT', id:id_52, to:'TBLRESSEQUESTPRECALCKEYS']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEQUESTPRECALCPPOSCOUNT\'-\'TBLRESSEQUESTPRECALCKEYS\'']:[]]], edge:[from:'TBLRESSEQUESTPRECALCSPECLIST', id:id_53, to:'TBLRESSEQUESTPRECALCCOUNTED']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEQUESTPRECALCSPECLIST\'-\'TBLRESSEQUESTPRECALCCOUNTED\'']:[]]], edge:[from:'TBLRESSEQUESTPRECALCSPECLIST', id:id_54, to:'TBLRESSEQUESTPRECALCKEYS']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEQUESTPRECALCSPECLIST\'-\'TBLRESSEQUESTPRECALCKEYS\'']:[]]], edge:[from:'TBLRESSEQUESTPRECALCACCIDLST', id:id_55, to:'TBLRESSEQUESTPRECALCCOUNTED']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEQUESTPRECALCACCIDLST\'-\'TBLRESSEQUESTPRECALCCOUNTED\'']:[]]], edge:[from:'TBLRESSEQUESTPRECALCACCIDLST', id:id_56, to:'TBLRESSEQUESTPRECALCKEYS']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESSEQUESTPRECALCACCIDLST\'-\'TBLRESSEQUESTPRECALCKEYS\'']:[]]], edge:[from:'TBLRESFALSEPOSITIVE', id:id_57, to:'TBLSEQDESC']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESFALSEPOSITIVE\'-\'TBLSEQDESC\'']:[]]], edge:[from:'TBLRESFALSEPOSITIVE', id:id_58, to:'TBLUSER']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESFALSEPOSITIVE\'-\'TBLUSER\'']:[]]], edge:[from:'TBLRESFALSEPOSITIVE', id:id_59, to:'TBLRESPROJEKT']:[prmtr:[arrows:both, color:blue, first_arrow:first_arrow, mouse_click:edge, pen:1, second_arrow:second_arrow, weight:1]:[string:[bubble:'\'TBLRESFALSEPOSITIVE\'-\'TBLRESPROJEKT\'']:[]]]]].

gxl(example_3_2, Gxl) :-
   checklist( gensym(example_id_),
      [Node_Id_1,
       Node_Id_3,
       Node_Id_4] ),
   checklist( gensym(example_id_),
      [Edge_Id_3,
       Edge_Id_4,
       Edge_Id_5] ),
   Vertices = [
      Node_Id_1-['Box 1']-box-green-medium-(100, 10),
      Node_Id_3-['Circle 1']-box-green-medium-(130, 150),
      Node_Id_4-['Circle 2']-circle-dark_blue-medium-(130, 250) ],
   maplist( vertex_to_gxl_vertex_complete,
      Vertices, Gxl_Nodes ),
   Edges_1 = [
      Edge_Id_3-Node_Id_3-Node_Id_4-green-second-1-arrow_b,
      Edge_Id_4-Node_Id_4-Node_Id_3-blue-second-1-arrow_b,
      Edge_Id_5-Node_Id_1-Node_Id_3-blue-second-1-arrow_b ],
   maplist( edge_to_gxl_edge_complete,
      Edges_1, Gxl_Edges_1 ),
   append(Gxl_Nodes, Gxl_Edges_1, Gxl_Nodes_and_Edges_1),
%  gxl_presetting(edge, Gxl_Edge_1),
%  Gxl_Edge_2 := Gxl_Edge_1*[@from:Node_Id_7, @to:Node_Id_5],
%  Gxl_Nodes_and_Edges_2 =[Gxl_Edge_2|Gxl_Nodes_and_Edges_1],
   Gxl =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Graph Example 3',
               width:500, height:550,
               background:light_cyan,
               mouse_click:picture]:[],
         graph:[id:graph_3, edgeids:true, edgemode:directed,
               hypergraph:false]:Gxl_Nodes_and_Edges_1 ],
   !.

gxl(handles, Gxl) :-
   checklist( gensym(example_id_),
      [Node_Id_2, Node_Id_3,
       Node_Id_4, Node_Id_5] ),
   Node_Id_1 = box_node,
   checklist( gensym(example_id_),
      [Edge_Id_1, Edge_Id_2, Edge_Id_3,
       Edge_Id_4] ),
   Vertices = [
      Node_Id_1-['My white Box']-box-white-medium-(100, 100),
      Node_Id_2-[circle]-circle-white-medium-(100, 200),
      Node_Id_3-[circle]-circle-white-medium-(100, 0),
      Node_Id_4-[circle]-circle-white-medium-(0, 100),
      Node_Id_5-[circle]-circle-white-medium-(200, 100)],
   maplist( vertex_to_gxl_vertex_complete,
      Vertices, Gxl_Nodes ),
   Edges_1 = [
      Edge_Id_1-Node_Id_2-Node_Id_1-black-second-2-arrow_handles,
      Edge_Id_2-Node_Id_3-Node_Id_1-black-second-2-arrow_handles,
      Edge_Id_3-Node_Id_4-Node_Id_1-black-second-2-arrow_handles,
      Edge_Id_4-Node_Id_5-Node_Id_1-black-second-2-arrow_handles],
   maplist( edge_to_gxl_edge_complete,
      Edges_1, Gxl_Edges_1 ),
   append(Gxl_Nodes, Gxl_Edges_1, Gxl_Nodes_and_Edges_1),
   Gxl =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Graph Example 3',
               width:300, height:300,
               background:white,
               mouse_click:picture]:[],
         graph:[id:graph_3, edgeids:true, edgemode:directed,
               hypergraph:false]:Gxl_Nodes_and_Edges_1 ],
   !.


gxl(handles_2, Gxl_2) :-
   gxl(handles, Gxl_1),
   Gxl_2 := Gxl_1*[graph^node::[@id=box_node]^prmtr@handles:middle].

gxl(example_bar_graph, Gxl) :-
   Node_Id_1 := 1234,
   Node_Id_2 := 'N',
   gxl_presetting(node, Node_Id_1, Gxl_Node_1),
   gxl_presetting(node, Node_Id_2, Gxl_Node_2),
   Gxl =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Bar Graph',
               width:500, height:350,
               background:white,
               mouse_click:picture]:[],
         graph:[id:graph_1, edgeids:true, edgemode:directed,
               hypergraph:false]:[
            Gxl_Node_1,
            Gxl_Node_2,
            node:[id:bar]:[
               prmtr:[
                  color:red,
                  size:medium,
                  symbol:chart,
                  x_pos:163,
                  y_pos:60]:[
               chart:[type:h_bar_graph]:[
                  bar:[height:100, color:white, position:0]:[],
                  bar:[height:70, color:blue, position:0]:[],
                  bar:[height:23, color:red, position:1]:[],
                  bar:[height:15, color:green, position:2]:[]],
               string:[font_style:(helvetica, roman, 12),
                  bubble:'50/100']:[node_3]]],
            node:[id:bar_2]:[
               prmtr:[
                  color:red,
                  size:medium,
                  symbol:chart,
                  x_pos:350,
                  y_pos:100]:[
               chart:[type:v_bar_graph]:[
                  bar:[height:100, color:white, position:0]:[],
                  bar:[height:70, color:blue, position:0]:[],
                  bar:[height:23, color:red, position:1]:[],
                  bar:[height:15, color:green, position:2]:[]],
               string:[font_style:(helvetica, roman, 12),
                  bubble:'Rules:100, MPAs:70, Choosen:23, Necessary:15']:[node_3]]],
            edge:[from:Node_Id_2, id:edge_3,
                  to:bar_2]:[
               prmtr:[arrows:both, color:red, pen:1,
                     first_arrow:first_arrow,
                     second_arrow:second_arrow]:[
                  string:[font_style:(screen, roman, 12),
                     bubble:'']:[]
               ]
            ],
            edge:[from:Node_Id_2, id:555,
                  to:bar]:[
               prmtr:[arrows:both, color:red, pen:1,
                     first_arrow:first_arrow,
                     second_arrow:second_arrow]:[
                  string:[font_style:(screen, roman, 12),
                     bubble:'my default edge 1']:[]
               ]
            ],
            edge:[from:Node_Id_1, id:i123456,
                  to:Node_Id_2]:[
               prmtr:[arrows:second, color:red, pen:1,
                     second_arrow:second_arrow]:[
                  string:[font_style:(times, roman, 12),
                      bubble:'my default edge 2']:[
                      my, default, 'edge 2']
               ]
            ]
         ]
      ],
   !.

/*
im gxl dürfen die id's nicht variable sein!
nur, wenn man einzelne Knoten/edges ans picture sendet

arrow-attribute werden bei picture_to_gxl nicht gefunden und nicht
abgespeichert.

*/

/******************************************************************/


gxl(xxx, Gxl) :-
   Node_Id_1 = n1,
   Node_Id_2 = n2,
   Node_Id_3 = n3,
   Vertices = [
      Node_Id_2-['little Box 1']-box-orange-medium-(50, 10),
      Node_Id_3-[little, 'Box', 2]-box-orange-medium-(150, 90) ],
   Edges = [
      edge_1-Node_Id_1-n4-green-second-1,
      edge_2-Node_Id_2-Node_Id_3-yellow-second-2,
      edge_3-Node_Id_1-n6-green-second-1 ],
   maplist( vertex_to_gxl_vertex_complete,
      Vertices, Gxl_Nodes ),
   maplist( edge_to_gxl_edge_complete,
      Edges, [Gxl_Edge_1, Gxl_Edge_2, Gxl_Edge_3] ),
   Gxl =
   gxl:[]:[
      graph:[]:[
         node:[id:Node_Id_1]:[
            graph:[id:graph_2, edgeids:true, edgemode:directed,
                  hypergraph:false]:[
               Gxl_Edge_2|Gxl_Nodes] ],
         node:[id:n4]:[],
         node:[id:n5]:[],
         Gxl_Edge_1,
         Gxl_Edge_3
      ]
   ],
   !.



gxl(merged_nodes, Gxl) :-
   Gxl =
      gxl:['xmlns:xlink':'http://www.w3.org/1999/xlink']:[
         prmtr:[label:'Example: Merged Nodes',
            background:colour('#F05555')]:[],
         graph:[id:graph_1, edgeids:true, edgemode:directed,
               hypergraph:false]:[
            node:[id:'n1']:[
               prmtr:[color:orange, size:small, symbol:circle]:[
                  string:[]:[n1]] ],
            node:[id:'n2']:[
               prmtr:[color:orange, size:medium, symbol:circle]:[
                  string:[]:[n2]] ],
            node:[id:'n3']:[
               prmtr:[color:orange, size:large, symbol:circle]:[
                  string:[]:[n3]] ],
            node:[id:'n4']:[
               prmtr:[color:orange, size:medium, symbol:circle]:[
                  string:[]:[n4]] ],
            node:[id:'n5']:[
               prmtr:[color:colour('#FF0000'), size:medium, symbol:circle]:[
                  string:[]:[n5]] ],
            node:[id:'n6']:[
               prmtr:[color:orange, size:medium, symbol:circle]:[
                  string:[]:[n6]] ],
            edge:[from:n2, id:e1, to:n3]:[],
            edge:[from:n2, id:e2, to:n5]:[],
            edge:[from:n1, id:e3, to:n2]:[],
            edge:[from:n6, id:e4, to:n1]:[]] ],
   !.


gxl(example_4, Gxl) :-
   dread(xml(gxl_graph), 'tfbc.gxl', Gxl).

