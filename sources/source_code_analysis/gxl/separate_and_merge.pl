

/******************************************************************/
/***                                                            ***/
/***    GXL: Separating and Merging Parameters and Gxl          ***/
/***                                                            ***/
/******************************************************************/


:- module(separate_and_merge, [
      separate_gxl_from_layout/3,
      merge_gxl_and_layout/3,
      gxl_delete_prmtr_tag/2]).

:- dynamic gxl_element/2.


/*** interface ****************************************************/


/* separate_gxl_from_layout(+Gxl_1, +Gxl_2, -Layout) <-
      */

separate_gxl_from_layout(Gxl_1, Gxl_2, Layout) :-
   separate_layout(Gxl_1, Layout),
   gxl_delete_prmtr_tag(Gxl_1, Gxl_2).


/* merge_gxl_and_layout(+Gxl_1, +Layout, -Gxl_2) <-
      */

merge_gxl_and_layout(Gxl_1, gxl_layout:_:Layout, Gxl_2) :-
   retractall( separate_and_merge:gxl_element(_, _) ),
   forall(member(Element, Layout),
      separate_and_merge:assert_layout_element_prmtr(Element) ),
   merge_gxl_and_layout(Gxl_1, Gxl_2),
   retractall( separate_and_merge:gxl_element(_, _) ),
   !.
merge_gxl_and_layout(Gxl_1, gxllayout:_:Layout, Gxl_2) :-
   retractall( separate_and_merge:gxl_element(_, _) ),
   forall(member(Element, Layout),
      separate_and_merge:assert_layout_element_graphpad(Element) ),
   merge_gxl_and_layout(Gxl_1, Gxl_2),
   retractall( separate_and_merge:gxl_element(_, _) ),
   !.
merge_gxl_and_layout(Gxl_1, (svg:Attr:Layout), Gxl_2) :-
   retractall( separate_and_merge:gxl_element(_, _) ),
   forall( [[Id], [X, Y]] := (svg:Attr:Layout)^_^g::[@class=node]^[
            title^content::'*', ellipse@[cx, cy]],
      separate_and_merge:assert_layout_element_svg(Id, (X, Y)) ),
   merge_gxl_and_layout(Gxl_1, Gxl_2),
   retractall( separate_and_merge:gxl_element(_, _) ).


/*** implementation ***********************************************/


/* merge_gxl_and_layout(+Tree, -Tree) <-
      */

merge_gxl_and_layout(node:Attr:Cont_1, node:Attr:Cont_3) :-
   Id := (node:Attr:Cont_1)@id,
   separate_and_merge:gxl_element(Id, (X, Y)),
   Element := (node:Attr:Cont_1)*[^prmtr@x_pos:X, ^prmtr@y_pos:Y],
   Element = (node:Attr:Cont_2),
   maplist( merge_gxl_and_layout,
      Cont_2, Cont_3 ),
   !.
merge_gxl_and_layout(Tag:Attr:Cont_1, Tag:Attr:[prmtr:A:C|Cont_2]) :-
   Id := (Tag:Attr:Cont_1)@id,
   separate_and_merge:gxl_element(Id, prmtr:A:C),
   maplist( merge_gxl_and_layout,
      Cont_1, Cont_2 ),
   !.
merge_gxl_and_layout(gxl:Attr:Cont_1, gxl:Attr:[Prmtr|Cont_2]) :-
   separate_and_merge:gxl_element(gxl, Prmtr),
   maplist( merge_gxl_and_layout,
      Cont_1, Cont_2),
   !.
merge_gxl_and_layout(Tag:Attr:Cont_1, Tag:Attr:Cont_2) :-
   maplist( merge_gxl_and_layout,
      Cont_1, Cont_2 ),
   !.
merge_gxl_and_layout(Gxl, Gxl).


/* separate_layout(+Gxl, -Layout) <-
      */

separate_layout(Gxl, Layout) :-
   separate_lay(Gxl, Prmtrs_1),
   findall( gxl:[id:gxl]:[Prmtr],
      Prmtr := Gxl^prmtr,
      Prmtrs_2 ),
   append(Prmtrs_2, Prmtrs_1, Prmtrs_3),
   Layout = gxl_layout:[]:Prmtrs_3.

separate_lay(_Tag:Attr:Cont, [Layout|Layouts_2]) :-
   findall( prmtr:P_A:P_C,
      member(prmtr:P_A:P_C, Cont),
      Prmtrs ),
   not( Prmtrs = [] ),
   memberchk(id:Id, Attr),
   Layout = layout:[id:Id]:Prmtrs,
   maplist( separate_lay,
      Cont, Layouts_1 ),
   flatten(Layouts_1, Layouts_2),
   !.
separate_lay(_:_:Cont, L_2) :-
   maplist( separate_lay,
      Cont, L_1),
   flatten(L_1, L_2),
   !.
separate_lay(_, []).


/* gxl_delete_prmtr_tag(+Gxl_1, -Gxl_2) <-
      */

gxl_delete_prmtr_tag(Tag:Attr:C_1, Tag:Attr:C_3) :-
   sublist( gxl_filter_prmtr,
      C_1, C_2),
   maplist( gxl_delete_prmtr_tag,
      C_2, C_3 ),
   !.
gxl_delete_prmtr_tag(Gxl, Gxl).

gxl_filter_prmtr(prmtr:_:_) :-
   !,
   fail.
gxl_filter_prmtr(_).


/* assert_layout_element(+Element) <-
      */

assert_layout_element_prmtr(Element) :-
   Id := Element@id,
   Element = _:_:[Prmtr],
   assert( separate_and_merge:gxl_element(Id, Prmtr) ),
   !.
assert_layout_element_prmtr(_).


assert_layout_element_graphpad(Element) :-
   Id := Element@id,
   Pos := Element^a@val,
   term_to_atom((X, Y, _, _), Pos),
   assert( separate_and_merge:gxl_element(Id, (X, Y)) ),
   !.
assert_layout_element_graphpad(_).

assert_layout_element_svg(Id, (X, Y)) :-
   assert( separate_and_merge:gxl_element(Id, (X, Y)) ),
   !.
assert_layout_element_svg(_, _).


/******************************************************************/


