

/******************************************************************/
/***                                                            ***/
/***      GXL: Tests Layout                                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(source_code_analysis:gxl_layout, graphviz) :-
   test_graph(1, Vertices, Edges, _),
   vertices_and_edges_to_gxl(Vertices, Edges, Gxl_1),
   layout_graphviz(fdp, Gxl_1, _),
   layout_graphviz(dot, Gxl_1, _),
   layout_graphviz(neato, Gxl_1, _),
   layout_graphviz(twopi, Gxl_1, _),
   layout_graphviz(circo, Gxl_1, _),
   layout_graphviz(fdp, Gxl_1, _).


test(source_code_analysis:gxl_layout, layout_1) :-
   test_source_code_analysis(gxl, layout(1), dfs).
test(source_code_analysis:gxl_layout, layout_2) :-
   test_source_code_analysis(gxl, layout(2), dfs).
test(source_code_analysis:gxl_layout, layout_3) :-
   test_source_code_analysis(gxl, layout(3), dfs).
test(source_code_analysis:gxl_layout, layout_4) :-
   test_source_code_analysis(gxl, layout(4), dfs).
test(source_code_analysis:gxl_layout, layout_5) :-
   test_source_code_analysis(gxl, layout(1), dfs_inv).
test(source_code_analysis:gxl_layout, layout_6) :-
   test_source_code_analysis(gxl, layout(2), dfs_inv).
test(source_code_analysis:gxl_layout, layout_7) :-
   test_source_code_analysis(gxl, layout(3), dfs_inv).
test(source_code_analysis:gxl_layout, layout_8) :-
   test_source_code_analysis(gxl, layout(4), dfs_inv).
test(source_code_analysis:gxl_layout, layout_9) :-
   test_source_code_analysis(gxl, layout(1), bfs).
test(source_code_analysis:gxl_layout, layout_10) :-
   test_source_code_analysis(gxl, layout(2), bfs).
test(source_code_analysis:gxl_layout, layout_11) :-
   test_source_code_analysis(gxl, layout(3), bfs).
test(source_code_analysis:gxl_layout, layout_12) :-
   test_source_code_analysis(gxl, layout(4), bfs).
test(source_code_analysis:gxl_layout, layout_13) :-
   test_source_code_analysis(gxl, layout(1), bfs_inv).
test(source_code_analysis:gxl_layout, layout_14) :-
   test_source_code_analysis(gxl, layout(2), bfs_inv).
test(source_code_analysis:gxl_layout, layout_15) :-
   test_source_code_analysis(gxl, layout(3), bfs_inv).
test(source_code_analysis:gxl_layout, layout_16) :-
   test_source_code_analysis(gxl, layout(4), bfs_inv).

/*
test(source_code_analysis:gxl, layout) :-
   test(source_code_analysis:gxl, layout(_), _),
   fail.
test(source_code_analysis:gxl, layout).
*/

test_source_code_analysis(gxl, layout(X), Mode_1) :-
   member(X, [1, 2, 3, 4]),
   member(Mode_1, [dfs, dfs_inv, bfs, bfs_inv]),
   test_graph(X, Vertices, Edges, Roots),
   vertices_and_edges_to_gxl(Vertices, Edges, Gxl_1),
   alias_to_default_fn_triple(gxl_config, Config),
   gxl_graph_layout(Config, Mode_1, Roots, Gxl_1, Gxl_2),
   determine_mode(Mode_1, _, _, Mode_2, _, _, _, _),
   tree_traversals:traverse(Mode_2, Roots, Edges, Tree_Edges),
   v_w_to_gxl_edge_ids(Tree_Edges, Gxl_2, Tree_Edge_Ids),
   gxl_color_set(edges, red, Tree_Edge_Ids, Gxl_2, Gxl_3),
   Gxl_4 := Gxl_3*[^prmtr@width:700, ^prmtr@height:600],
   gxl_to_picture(Config, Gxl_4, _).


/*** implementation ***********************************************/


test_graph(1, Vertices, Edges, [a]) :-
   Vertices = [a, b, c, d, e, f, h, i, j, k, l, m],
   Edges = [a-b, a-c, a-d,
      b-h, b-i, j-l, j-m,
      c-d, c-f,
      d-e, d-f, d-j,
      f-i,
      i-d, j-k, k-a].
test_graph(2, Vertices, Edges, [a]) :-
   Vertices = [a, b, c, d, e, f, g, h, i],
   Edges = [a-b, a-c, a-d, a-e, a-f, a-g, a-h, a-i,
            b-c, c-d, d-e, e-f, f-g, g-h, h-i].

test_graph(3, Vertices, Edges, [a, x, y, z]) :-
   Vertices = [
      a, b, c, d, e, f, g, h, i, j, k, l,
      m, n, o, p, q, r, s, t, u, v, w, x, y, z,
      ganzLangesWortMitVielenBuchstaben ],
   Edges = [a-b, a-d, a-s, a-f,
      b-c, b-d, b-f,
      c-a, c-e, c-i, c-j,
      d-c, d-e, d-k,
      f-e, f-g, f-h,
      g-n, g-a, g-r, g-f,
      i-m, i-n, i-o, i-w, i-a,
      j-t, j-u, j-v, j-ganzLangesWortMitVielenBuchstaben,
      k-l, k-d, k-h, k-b,
      m-o, m-p, m-q, m-r, m-s, m-w ].

test_graph(4, Vertices, Edges, [a]) :-
   Vertices = [
      a, b, c, d, e, i, j],
   Edges = [a-b, a-d,
      b-c, b-d,
      c-e, c-i, c-j,
      d-e].


/******************************************************************/


