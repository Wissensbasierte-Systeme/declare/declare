

/******************************************************************/
/***                                                            ***/
/***          Visur:  Graphs                                    ***/
/***                                                            ***/
/******************************************************************/


:- module( gxl_to_tree, [gxl_to_tree/2] ).

:- dynamic edge_tmp/2.


/*** interface ****************************************************/


/* gxl_to_tree(+Gxl, -Tree) <-
      */

gxl_to_tree(Gxl, Tree) :-
   gxl_to_vertices_and_edges(1-2, Gxl, Vertices, Edges),
   vertices_and_edges_to_possible_roots(Vertices, Edges, Roots),
   edges_to_tree(Edges, Roots, Tree).


/*** implementation ***********************************************/


/* edges_to_tree(+Edges, +Roots, -Tree) <-
      */

edges_to_tree(Edges, Roots, Tree_2) :-
   retractall( gxl_to_tree:edge_tmp ),
   checklist( assert_edge_tmp,
      Edges ),
   maplist( node_to_xml_node,
      Roots, XML_Roots ),
   Tree_1 = tree:[]:XML_Roots,
   edges_to_tree(Tree_1, Tree_2),
   retractall( gxl_to_tree:edge_tmp ).

assert_edge_tmp(V-W) :-
   assert( gxl_to_tree:edge_tmp(V, W) ).


/* edges_to_tree(+Tree_1, -Tree_2) <-
      */

edges_to_tree(Tag:Attr:Cont_1, Tag:Attr:Cont_3) :-
   maplist( add_subnodes,
      Cont_1, Cont_2 ),
   maplist( edges_to_tree,
      Cont_2, Cont_3 ).

add_subnodes(Tag:Attr:[], Tag:Attr:Cont) :-
   findall( W,
      gxl_to_tree:edge_tmp(Tag, W),
      Ws ),
   retractall( gxl_to_tree:edge_tmp(Tag, _) ),
   maplist( node_to_xml_node,
      Ws, Cont ).


/* node_to_xml_node(+Node, -XML_Node) <-
      */

node_to_xml_node(Node, XML_Node) :-
   XML_Node = Node:[]:[].


/******************************************************************/


