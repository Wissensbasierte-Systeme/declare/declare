

/******************************************************************/
/***                                                            ***/
/***          GXL:  BFS Grouping                                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* group_nodes_vertically(Picture) <-
      */

group_nodes_vertically(Picture) :-
   get(Picture, xml, Gxl_1),
   get(Picture, config, Config),
   gxl_bfs_grouping(Gxl_1, Gxl_2),
   Gxl_3 := Gxl_2*[^prmtr@label:'BFS Grouped Graph'],
   gxl_to_picture(Config, Gxl_3, P),
   xpce_graph_layout(xpce, P).


/*gxl_bfs_grouping(Gxl_1, Gxl_3) <-
      */

gxl_bfs_grouping(Gxl_1, Gxl_3) :-
   gxl_to_vertices_and_edges(node_id-v_w, Gxl_1, Vertices, Edges),
   vertices_and_edges_to_possible_roots(Vertices, Edges, Roots),
   gxl_to_groups(Roots, Roots, Edges, Groups),
   generate_merge_configuration(list_2, Gxl_1, Groups,
      Config),
   gxl_to_gxl_with_contracted_nodes(Config, Gxl_1, Gxl_2),
   gxl_edge_weight(Gxl_2, Gxl_3).



/*** implementation ***********************************************/


/* gxl_to_groups([], _, _, []) <-
      */

gxl_to_groups([], _, _, []).
gxl_to_groups(Roots, Visited_1, Edges, [Roots|Groups]) :-
   maplist( tree_traversals:node_to_sons(Edges, Visited_1),
      Roots, Sons_1 ),
   union(Sons_1, Sons_2),
   union(Visited_1, Sons_2, Visited_2),
   gxl_to_groups(Sons_2, Visited_2, Edges, Groups).


/******************************************************************/


