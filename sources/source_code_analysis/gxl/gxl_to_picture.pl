

/******************************************************************/
/***                                                            ***/
/***    GXL: gxl_to_picture                                     ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library(help_message)).
:- use_module(library(pce_tagged_connection)).

:- dynamic gxl_node_1_tmp/2, glx_node_existing_id_tmp/1.


/*** interface ****************************************************/


/* gxl_to_pic(+Gxl) <-
      */

gxl_to_pic(Gxl) :-
   alias_to_default_fn_triple(gxl_config, Gxl_Config),
   gxl_to_picture(Gxl_Config, Gxl, _).


/* gxl_to_pic(+Gxl, +Picture) <-
      */

gxl_to_pic(Gxl, Picture) :-
   alias_to_default_fn_triple(gxl_config, Gxl_Config),
   gxl_to_picture(Gxl_Config, Gxl, Picture).


/* gxl_to_picture(+Config, +Gxl_Graph, ?Picture) <-
      */

gxl_to_picture(Config, Gxl_1, Picture) :-
   ground(Gxl_1),
   ground(Config),
   var(Picture),
   !,
   gxl_to_gxl_valid(Gxl_1, Gxl_2),
   [Label, W, H, Background, _Mouse_Click] :=
      Gxl_2^prmtr@[label, width, height,
         background, mouse_click],
   new(Picture, picture(Label, size(W, H))),
   send(Picture, attribute, xml, prolog(Gxl_2)),
   send(Picture, attribute, config, prolog(Config)),
   send(Picture, background, Background),
   add_click_gesture_to_graphical(Picture, Picture, Gxl_2),
   add_popup_to_gxl_graphical(Picture, Gxl_2, Picture),
   send(Picture, open_centered),
   gxl_to_picture_step(Config, Gxl_2, Picture),
   !.

gxl_to_picture(Config, Gxl_Append_1, Picture) :-
   ground(Gxl_Append_1),
   ground(Config),
   object(Picture),
   !,
   gxl_to_gxl_valid(Gxl_Append_1, gxl:_:Cont_A),
   memberchk(graph:_:Cont_G_A, Cont_A),
   catch( get(Picture, xml, gxl:Attr_B:Cont_B_1),
      _Error_Message,
      ( Attr_B = [], Cont_B_1 = [] ) ),
   ( memberchk(graph:Attr_G_B:Cont_G_B, Cont_B_1)
   ; ( Attr_G_B = [],
       Cont_G_B = [] ) ),
   append(Cont_G_A, Cont_G_B, Cont_G),
   delete(Cont_B_1, graph:Attr_G_B:Cont_G_B, Cont_B_2),
   Cont_B_3 = [graph:Attr_G_B:Cont_G|Cont_B_2],
   Gxl_3 = gxl:Attr_B:Cont_B_3,
   send(Picture, attribute, xml, prolog(Gxl_3)),
   send(Picture, attribute, config, prolog(Config)),
   add_click_gesture_to_graphical(Picture, Picture, Gxl_3),
   add_popup_to_gxl_graphical(Picture, Gxl_3, Picture),
   gxl_to_picture_step(Config, Gxl_3, Picture),
   !.

/*
gxl_to_picture(Config, Gxl_1, Picture) :-
   ground(Gxl_1),
   ground(Config),
   object(Picture),
   !,
   gxl_to_gxl_valid(Gxl_1, Gxl_2),
   catch( get(Picture, xml, gxl:A_1:B_1),
      _Error_Message,
      ( A_1 = [], B_1 = [] ) ),
writeln(B_1),
   Gxl_2 = gxl:A_2:B_2,
   ord_union_2(A_1, A_2, A),
   append(B_1, B_2, B),
   Gxl_3 = gxl:A:B,
   send(Picture, attribute, xml, prolog(Gxl_3)),
   send(Picture, attribute, config, prolog(Config)),
   add_click_gesture_to_graphical(Picture, Picture, Gxl_3),
   add_popup_to_gxl_graphical(Picture, Gxl_3, Picture),
   gxl_to_picture_step(Config, Gxl_2, Picture),
   !.
*/
gxl_to_picture(Config, Gxl, Picture) :-
   var(Config),
   ground(Gxl),
   object(Picture),
   catch( get(Picture, config, Config), _, fail ),
   gxl_to_picture(Config, Gxl, Picture),
   !.


/*** implementation ***********************************************/


/* gxl_to_picture_step(+Config, +Gxl, +Picture) <-
      */

gxl_to_picture_step(Config, Gxl, Picture) :-
   config_to_element(Config, handles, Handles),
   config_to_element(Config, sizes, Sizes),
   config_to_element(Config, mouse_clicks, Mouse_Clicks),
   config_to_element(Config, arrows, Arrows),
   retractall( gxl_node_1_tmp(_, _) ),
   retractall( glx_node_existing_id_tmp(_) ),
   forall( Gxl_Node := Gxl^graph^node,
      gxl_node_to_picture(
         config:[]:[Handles, Mouse_Clicks, Sizes],
         Gxl_Node, Picture) ),
   forall( Gxl_Edge := Gxl^graph^edge,
      gxl_edge_to_picture(
         config:[]:[Arrows], Gxl, Gxl_Edge, Picture, _) ),
   retractall( gxl_node_1_tmp(_, _) ),
   retractall( glx_node_existing_id_tmp(_) ),
   !.


/* gxl_node_to_picture(
      +Config, +Gxl_Node, +Picture, -Id) <-
      */

gxl_node_to_picture(Config, Gxl_Node, Picture) :-
   gxl_node_to_picture(Config, Gxl_Node, Picture, _).

gxl_node_to_picture(Config, Gxl_Node, Picture, Node_Id) :-
   [X_Pos, Y_Pos, Color, Symbol_1, Size, Handle_Alias] :=
       Gxl_Node^prmtr@[x_pos, y_pos, color, symbol, size,
          handles ],
   string:Attr:String := Gxl_Node^prmtr^string,
  [Font, Bubble] := (string:Attr:String)@[font_style, bubble],
   Handles := Config^handles^handle_group::[@alias=Handle_Alias],
   ( Symbol_1 = chart ->
     Symbol_2 := Gxl_Node^prmtr^chart
     ;
     Symbol_2 = Symbol_1 ),
   ( S_Size :=
        Config^sizes^size::[@alias=Size]@points,
     !
   ; S_Size = Size,
     ! ),
   check_and_get_id(Gxl_Node, Node_Id),
   Gxl_Node = _:_:Content,
   checklist( assert_embedded_nodes(Node_Id),
      Content ),
   gxl_id_to_xpce_address(Picture, Node_Id, Address),
   free(Address),
   new(Address, device),
   add_symbol_and_text_to_device(
      Address, [S_Size, Symbol_2, Color], Font, String, Image),
   add_click_gesture_to_graphical(Picture, Address, Gxl_Node),
   send(Address, recogniser, move_gesture(left)),
   add_popup_to_gxl_graphical(Picture, Gxl_Node, Address),
   ( not( Bubble = '') ->
     send(Address, help_message, tag, Bubble)
   ; true ),
   gxl_text_size(String, Address, (Text_Width, Text_Height) ),
   get(Image, size, size(Image_Width, Image_Height)),
   add_handles_to_node(Address, Handles,
      (Text_Width, Text_Height), (Image_Width, Image_Height)),
   R_X_Pos is X_Pos - Text_Width/2,
   send(Picture, display, Address, point(R_X_Pos, Y_Pos)),
   assert( glx_node_existing_id_tmp(Node_Id) ),
   !.


/* gxl_edge_to_picture(
      +Config, +Gxl, +Edge, +Picture, -Id) <-
      */

gxl_edge_to_picture(Config, Gxl_Edge, Picture) :-
   gxl_edge_to_picture(Config, _, Gxl_Edge, Picture, _).

gxl_edge_to_picture(Config, Gxl_Edge, Picture, Id) :-
   gxl_edge_to_picture(Config, _, Gxl_Edge, Picture, Id).

gxl_edge_to_picture(
      Config, Gxl, Gxl_Edge, Picture, Edge_Id) :-
   [V_Id, W_Id] := Gxl_Edge@[from, to],
   gxl_id_to_xpce_address(Picture, V_Id, V_Address_1),
   gxl_id_to_xpce_address(Picture, W_Id, W_Address_1),
   verify_node_address(Gxl, Picture, V_Address_1, V_Address_2),
   verify_node_address(Gxl, Picture, W_Address_1, W_Address_2),
   check_and_get_id(Gxl_Edge, Edge_Id),
   gxl_id_to_xpce_address(Picture, Edge_Id, Edge_Address),
   free(Edge_Address),
   [Color, Arrows, Pen] :=
       Gxl_Edge^prmtr@[color, arrows, pen],
   string:Attr:String := Gxl_Edge^prmtr^string,
   [Font, Bubble] := (string:Attr:String)@[font_style, bubble],
   new(Link, link(out, in, line(arrows := Arrows))),
   send(Link, colour(colour(Color))),
   send(Link, pen, Pen),
   new(Edge_Address, tagged_connection(
      V_Address_2, W_Address_2, Link)),
   multi_line_text_field(String, Font, TF),
   send(Edge_Address, tag, TF),
   add_arrow_to_link(Config, Gxl_Edge, Edge_Address, Arrows),
   ( not( Bubble = '') ->
     send(Edge_Address, help_message, tag, Bubble)
   ; true ),
   add_click_gesture_to_graphical(Picture, Edge_Address, Gxl_Edge),
   add_popup_to_gxl_graphical(Picture, Gxl_Edge, Edge_Address),
   !.
gxl_edge_to_picture(_, _, Gxl_Edge, _, _Edge_Id) :-
   [V_Id_1, W_Id_1] := Gxl_Edge@[from, to],
   term_to_atom_sure(V_Id_1, V_Id_2),
   term_to_atom_sure(W_Id_1, W_Id_2),
   concat(['Edge from ', V_Id_2, ' to ', W_Id_2, ' is skipped!'], Text),
   writeln(Text),
   nl.


/* add_popup_to_gxl_graphical(+Container, +Object, +Address) <-
      */

add_popup_to_gxl_graphical(Container, XML, Address) :-
   add_popup_to_graphical(Container, XML,
      [Container, XML, Address], Popup),
   send(Address, popup, Popup),
   !.
add_popup_to_gxl_graphical(_, _, _).


/* verify_node_address(+Gxl, +Picture, +Address_1, -Address_2) <-
      */

verify_node_address(_, _, Address, Address) :-
   xpce_address_to_gxl_id(Address, Id),
   glx_node_existing_id_tmp(Id),
   !.
verify_node_address(_Gxl, Picture, Address_1, Address_2) :-
   xpce_address_to_gxl_id(Address_1, Id_1),
   gxl_node_1_tmp(Id_1, Id_2),
   gxl_id_to_xpce_address(Picture, Id_2, Address_2),
   glx_node_existing_id_tmp(Id_2),
   !.
verify_node_address(_, _, Address, _) :-
   xpce_address_to_gxl_id(Address, Id),
   concat(['Node ', Id, ' does not exist. '], Text_1),
   Text_2 = 'Cannot connect an edge to a non existing node.',
   write_list([Text_1, Text_2]),
   nl,
   fail.


/* node_to_embedded_node_index(+Gxl_Node) <-
      */

node_to_embedded_node_index(Gxl_Node) :-
   Gxl_Node = _:Attr:Content,
   memberchk(id:Node_Id, Attr),
   checklist( assert_embedded_nodes(Node_Id),
      Content ).


/* assert_embedded_nodes(+Id, +Node) <-
      */

assert_embedded_nodes(Id_V, node:Attr:Content) :-
   memberchk(id:Id_W, Attr),
   assert( gxl_node_1_tmp(Id_W, Id_V) ),
   checklist( assert_embedded_nodes(Id_V),
      Content ),
   !.
assert_embedded_nodes(Id_V, _:_:Content) :-
   checklist( assert_embedded_nodes(Id_V),
      Content ),
   !.
assert_embedded_nodes(_, _).


/* gxl_text_size(+String, +Address, -(Width, Heigth) ) <-
      */

gxl_text_size([''], _, (0, 0) ) :-
   !.
gxl_text_size(_, Address, (Text_Width, Text_Height) ) :-
   get(Address, size, size(Text_Width, Text_Height)),
   !.


/******************************************************************/


