

/******************************************************************/
/***                                                            ***/
/***      GXL: GXL to Forward, Cross, Back, Tree Edge           ***/
/***                                                            ***/
/******************************************************************/


:- module( gxl_tfbc, [
      gxl_to_gxl_highlighted_tfbc_edges/3,
      gxl_to_tfbc_edges/6 ] ).

:- dynamic
      dfs_variable/2,
      gxl_edge_id_tmp/3.


/*** interface ****************************************************/


/* gxl_to_gxl_highlighted_tfbc_edges(+Config, +Gxl_1, -Gxl_2) <-
      */

gxl_to_gxl_highlighted_tfbc_edges(Config, Gxl_1, Gxl_5) :-
   Edge_Colors := Config^edge_colors,
   F_C := Edge_Colors^edge_color::[@type=forward_edge]@color,
   B_C := Edge_Colors^edge_color::[@type=back_edge]@color,
   T_C := Edge_Colors^edge_color::[@type=tree_edge]@color,
   C_C := Edge_Colors^edge_color::[@type=cross_edge]@color,
   gxl_to_tfbc_edges(1, Gxl_1, Tree, Forward, Back, Cross),
   gxl_color_set(ids, F_C, Forward, Gxl_1, Gxl_2),
   gxl_color_set(ids, B_C, Back, Gxl_2, Gxl_3),
   gxl_color_set(ids, T_C, Tree, Gxl_3, Gxl_4),
   gxl_color_set(ids, C_C, Cross, Gxl_4, Gxl_5).


/* gxl_to_tfbc_edges(+Gxl, -Tree, -Forward, -Back, -Cross) <-
      */

gxl_to_tfbc_edges(Mode, Gxl, Tree, Forward, Back, Cross) :-
   memberchk(Mode, [2, v_w]),
   gxl_to_vertices_and_edges(1-2, Gxl, Nodes, Edges),
   vertices_and_edges_to_possible_roots(Nodes, Edges, Roots),
   rnes_to_tfbcs(Roots, Nodes, Edges,
      Tree, Forward, Back, Cross).
gxl_to_tfbc_edges(Mode, Gxl, Tree_2, Forward_2, Back_2, Cross_2) :-
   memberchk(Mode, [1, id]),
   gxl_to_tfbc_edges(2, Gxl, Tree_1, Forward_1, Back_1, Cross_1),
   retractall( gxl_edge_id_tmp(_, _, _) ),
   assert_gxl_edges(Gxl),
   maplist( gxl_vws_to_ids,
      [Tree_1, Forward_1, Back_1, Cross_1],
      [Tree_2, Forward_2, Back_2, Cross_2] ),
   retractall( gxl_edge_id_tmp(_, _, _) ).


/*** implementation ***********************************************/


/* rnes_to_tfbcs(+Roots, +Nodes, +Edges,
      -Tree, -Forward, -Bachward, -Cross) <-
      */

rnes_to_tfbcs(Roots, Nodes, Edges, T_E, F_E, B_E, C_E) :-
   subtract(Nodes, Roots, Nodes_2),
   append(Roots, Nodes_2, Nodes_3),
   dfs(Nodes_3, Edges),
   specify_forward_cross_edges,
   ( foreach(Type, [tree, forward, back, cross]),
     foreach(V_W_Edges, Lists) do
        findall( V-W,
           gxl_tfbc:dfs_variable((type, V:W:_), Type),
           V_W_Edges ) ),
    maplist( list_to_ord_set,
       Lists, [T_E, F_E, B_E, C_E] ).


/* dfs(+Nodes, +Edges) <-
      */

dfs(Nodes, Edges) :-
   maplist( transform_edges,
      Edges, Matrix ),
   dfs_variable_set(time, 0),
   retractall( dfs_variable((d, _), _) ),
   retractall( dfs_variable((f, _), _) ),
   retractall( dfs_variable((color, _), _) ),
   retractall( dfs_variable((type, _), _) ),
   ( foreach(N, Nodes) do
        gxl_tfbc:dfs_variable_set((color, N), white) ),
   ( foreach(N , Nodes) do
        gxl_tfbc:dfs_variable((color, N), Color),
        ( Color = white ->
             gxl_tfbc:dfs_visit(Matrix, N)
        ; true ) ).

transform_edges(A-B, A:B:x).


/* dfs_visit(+Matrix, +Node) <-
      */

dfs_visit(Matrix, N) :-
   dfs_variable_set((color, N), grey),
   dfs_variable(time, Time),
   dfs_variable_set((d, N), Time),
   New_Time is Time + 1,
   dfs_variable_set(time, New_Time),
   forall( member(N:V:C, Matrix),
      ( dfs_variable((color, V), Color),
        Var = (type, N:V:C),
        ( Color = white ->
          ( dfs_variable_set(Var, tree),
            dfs_visit(Matrix, V) )
        ; ( Color = grey ->
            dfs_variable_set(Var, back)
          ; dfs_variable_set(Var, forward_cross_edge) ) ) ) ),
   dfs_variable_set((color, N), black),
   dfs_variable(time, Time_2),
   dfs_variable_set((f, N), Time_2),
   New_Time_2 is Time_2 + 1,
   dfs_variable_set(time, New_Time_2).


/* specify_forward_cross_edges <-
      */

specify_forward_cross_edges :-
   forall( dfs_variable((type, N:V:C), forward_cross_edge),
      ( dfs_variable((d, N), N_Time),
        dfs_variable((d, V), V_Time),
        Var = (type, N:V:C),
        ( N_Time < V_Time ->
          dfs_variable_set(Var, forward)
        ; dfs_variable_set(Var, cross) ) ) ).


/* dfs_variable_set(+Variable, +Value) <-
      */

dfs_variable_set(Variable, Value) :-
   retractall( dfs_variable(Variable, _) ),
   asserta( dfs_variable(Variable, Value) ).


/* assert_gxl_edges(+Gxl) <-
      */

assert_gxl_edges(edge:A:C) :-
   memberchk(from:From, A),
   memberchk(to:To, A),
   memberchk(id:Id, A),
   assert( gxl_edge_id_tmp(Id, From, To) ),
   checklist( assert_gxl_edges,
      C ),
   !.
assert_gxl_edges(prmtr:_:_) :-
   !.
assert_gxl_edges(_:_:C) :-
   checklist( assert_gxl_edges,
      C ),
   !.
assert_gxl_edges(_) :-
   !.

/* gxl_vws_to_ids(+VWs, -Ids) <-
      */

gxl_vws_to_ids(VWs, Ids_2) :-
   maplist( gxl_vw_to_ids,
      VWs, Ids_1),
   ord_union(Ids_1, Ids_2).


/* gxl_vw_to_ids(+VW, -Ids) <-
      */

gxl_vw_to_ids(V-W, Ids_2) :-
   findall( Id,
      gxl_edge_id_tmp(Id, V, W),
      Ids_1 ),
   list_to_ord_set(Ids_1, Ids_2).


/******************************************************************/


