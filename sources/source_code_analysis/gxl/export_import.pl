

/******************************************************************/
/***                                                            ***/
/***    GXL: Exporting and Importing GXL and Layout             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* export_gxl_and_layout(+Picture) <-
      */

export_gxl_and_layout(Picture) :-
   get(@finder, file, @off, [gxl], File),
   picture_to_gxl(Picture, Gxl_1, _Config),
   change_ids_in_gxl(Gxl_1, Gxl_2),
   separate_gxl_from_layout(Gxl_2, Gxl_3, Layout),
   dwrite(xml_2, File, Gxl_3),
   concat(File, '_layout', Layout_File),
   dwrite(xml_2, Layout_File, Layout).


/* import_gxl_and_layout(+Picture) <-
      */

import_gxl_and_layout(Picture) :-
   get(@finder, file, @on, [gxl], File),
   get(@finder, file, @on, ['*'], Layout_File),
   dread_(xml(gxl_graph), File, Gxl_1),
   dread_(xml(gxl_graph), Layout_File, Layout),
   merge_gxl_and_layout(Gxl_1, Layout, Gxl_2),
   xpce_picture_clear(Picture),
   gxl_to_picture(_, Gxl_2, Picture).


/*** implementation ***********************************************/


/******************************************************************/


