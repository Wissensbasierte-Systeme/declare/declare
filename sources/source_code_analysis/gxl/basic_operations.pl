

/******************************************************************/
/***                                                            ***/
/***             GXL: Basics Operations                         ***/
/***                                                            ***/
/******************************************************************/


:- module( basic_gxl_operations, [
      gxl_delete/4,
      gxl_color_set/5,
      gxl_delete_elements/3 ] ).


:- dynamic gxl_id_tmp/1.


/*** interface ****************************************************/


gxl_color_set(nodes, Color, all, Gxl_1, Gxl_2) :-
   gxl_color(node, Color, Gxl_1, Gxl_2).
gxl_color_set(edges, Color, all, Gxl_1, Gxl_2) :-
   gxl_color(edge, Color, Gxl_1, Gxl_2).
gxl_color_set(nodes, Color, Ids, Gxl_1, Gxl_2) :-
   gxl_color_elements(Color, Ids, Gxl_1, Gxl_2).
gxl_color_set(edges, Color, Ids, Gxl_1, Gxl_2) :-
   gxl_color_elements(Color, Ids, Gxl_1, Gxl_2).
gxl_color_set(ids, Color, Ids, Gxl_1, Gxl_2) :-
   gxl_color_elements(Color, Ids, Gxl_1, Gxl_2).
gxl_color_set(incoming_edges, Color, Ids_1, Gxl_1, Gxl_2) :-
   gxl_to_sm:delete_tmp_facts,
   gxl_to_sm:assert_gxl_node_id_and_deduced_node_id(Gxl_1),
   findall( Id_2,
      ( member(Id_1, Ids_1),
        gxl_to_sm:gxl_edge_id_tmp(Id_2, _, Id_1) ),
      Ids_2 ),
   gxl_color_elements(Color, Ids_2, Gxl_1, Gxl_2).
gxl_color_set(outgoing_edges, Color, Ids_Node, Gxl_1, Gxl_2) :-
   gxl_to_sm:delete_tmp_facts,
   gxl_to_sm:assert_gxl_node_id_and_deduced_node_id(Gxl_1),
   findall( Id_Edge,
      ( member(Id_Node, Ids_Node),
        gxl_to_sm:gxl_edge_id_tmp(Id_Edge, Id_Node, _) ),
      Ids_Edge ),
   gxl_color_elements(Color, Ids_Edge, Gxl_1, Gxl_2).
gxl_color_set(transitive_closure, _Color, _Ids, _Gxl_1, _Gxl_2) :-
   writeln('set_color transitive_closure comming soon.').


/* gxl_delete(+Mode, +Node_Ids, +Gxl_1, -Gxl_2) <-
      */

gxl_delete(nodes, Ids, Gxl_1, Gxl_2) :-
   gxl_delete_elements(Ids, Gxl_1, Gxl_2).
gxl_delete(edges, Ids, Gxl_1, Gxl_2) :-
   gxl_delete_elements(Ids, Gxl_1, Gxl_2).
gxl_delete(Mode, Prmtr, Gxl_1, Gxl_2) :-
   memberchk(Mode, [colored_edges, light_weighted_edges]),
   retractall( basic_gxl_operations:gxl_id_tmp(_) ),
   assert_edges_to_delete(Mode, Prmtr, Gxl_1),
   findall( Id,
      basic_gxl_operations:gxl_id_tmp(Id),
      Ids ),
   retractall( basic_gxl_operations:gxl_id_tmp(_) ),
   gxl_delete_elements(Ids, Gxl_1, Gxl_2).

assert_edges_to_delete(colored_edges, Color, Gxl) :-
   assert_colored_edge_ids(Color, Gxl),
   !.
assert_edges_to_delete(light_weighted_edges, Limit, Gxl) :-
   assert_light_weighted_edge_ids(Limit, Gxl),
   !.



/*** implementation ***********************************************/


/* gxl_delete_elements(+Ids, +Gxl_1, -Gxl_2) <-
      */

gxl_delete_elements(Ids, Gxl_1, Gxl_2) :-
   retractall( basic_gxl_operations:gxl_id_tmp(_) ),
   checklist( assert_gxl_id_tmp,
      Ids ),
   gxl_delete_elements(Gxl_1, Gxl_2),
   retractall( basic_gxl_operations:gxl_id_tmp(_) ).


/* gxl_color_nodes(+To_Change, +Color, +Gxl_1, -Gxl_2) <-
      */

gxl_color(_, _, prmtr:A:C, prmtr:A:C) :-
   !.
gxl_color(Tag, Color, Tag:A:C_1, Tag:A:C_3) :-
   (Tag:A:C_2) := (Tag:A:C_1)*[^prmtr@color:Color],
   maplist( gxl_color(Tag, Color),
      C_2, C_3 ),
   !.
gxl_color(To_Change, Color, Tag:A:C_1, Tag:A:C_2) :-
   maplist( gxl_color(To_Change, Color),
      C_1, C_2 ),
   !.
gxl_color(_, _, Element, Element).


/* gxl_color_elements(+Color, +Ids, +Gxl_1, -Gxl_2) <-
      */

gxl_color_elements(Color, Ids, Gxl_1, Gxl_2) :-
   retractall( basic_gxl_operations:gxl_id_tmp(_) ),
   checklist( assert_gxl_id_tmp,
      Ids ),
   gxl_color_elements(Color, Gxl_1, Gxl_2),
   retractall( basic_gxl_operations:gxl_id_tmp(_) ).


/* gxl_color_elements(+Color, +Gxl_1, -Gxl_2) <-
      */

gxl_color_elements(Color, T:A:C, Element) :-
   memberchk(id:Id, A),
   basic_gxl_operations:gxl_id_tmp(Id),
   Element := (T:A:C)*[^prmtr@color:Color],
   !.
gxl_color_elements(Color, T:A:C_1, T:A:C_2) :-
   maplist( gxl_color_elements(Color),
      C_1, C_2 ),
   !.
gxl_color_elements(_, Element, Element).


/* gxl_delete_elements(+Gxl_1, -Gxl_2) <-
      */

gxl_delete_elements(prmtr:A:C, prmtr:A:C) :-
   !.
gxl_delete_elements(T:A:C_1, T:A:C_3) :-
   gxl_id_filter(C_1, C_2),
   maplist( gxl_delete_elements,
      C_2, C_3 ),
   !.
gxl_delete_elements(A, A).

gxl_id_filter([_:A:_|Es_1], Es_2) :-
   memberchk(id:Id, A),
   basic_gxl_operations:gxl_id_tmp(Id),
   retract( basic_gxl_operations:gxl_id_tmp(Id) ),
   gxl_id_filter(Es_1, Es_2),
   !.
gxl_id_filter([E|Es_1], [E|Es_2]) :-
   gxl_id_filter(Es_1, Es_2),
   !.
gxl_id_filter([], []).


/* assert_light_weighted_edge_ids(+Limit, +Gxl) <-
      */

assert_light_weighted_edge_ids(Limit, edge:Attr:Cont) :-
   Weight := (edge:Attr:Cont)^prmtr@weight,
   Weight =< Limit,
   memberchk(id:Id, Attr),
   assert_gxl_id_tmp(Id),
   checklist( assert_light_weighted_edge_ids(Limit),
      Cont ),
   !.
assert_light_weighted_edge_ids(_, prmtr:_:_) :-
   !.
assert_light_weighted_edge_ids(Limit, _:_:Cont) :-
   checklist( assert_light_weighted_edge_ids(Limit),
      Cont ),
   !.
assert_light_weighted_edge_ids(_, _) :-
   !.


/* assert_colored_edge_ids(+Color, +Gxl) <-
      */

assert_colored_edge_ids(Color, edge:Attr:Cont) :-
   Color := (edge:Attr:Cont)^prmtr@color,
   memberchk(id:Id, Attr),
   assert_gxl_id_tmp(Id),
   checklist( assert_light_weighted_edge_ids(Color),
      Cont ),
   !.
assert_colored_edge_ids(_, prmtr:_:_) :-
   !.
assert_colored_edge_ids(Color, _:_:Cont) :-
   checklist( assert_colored_edge_ids(Color),
      Cont ),
   !.
assert_colored_edge_ids(_, _) :-
   !.


/* assert_gxl_id_tmp(+A) <-
      */

assert_gxl_id_tmp(A) :-
   assert( basic_gxl_operations:gxl_id_tmp(A) ).


/******************************************************************/


