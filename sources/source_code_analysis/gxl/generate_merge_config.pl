

/******************************************************************/
/***                                                            ***/
/***  GXL: Gerate Merge Configuration                           ***/
/***                                                            ***/
/******************************************************************/

:- module( generate_merge_conf, [
      generate_merge_configuration/4,
      assert_gxl_nodes_and_gxl_edges/1 ] ).

:- dynamic gxl_edge_2_tmp/3, gxl_node_2_tmp/2.


/*** interface ****************************************************/


/* generate_merge_configuration(+Mode, +Gxl, +Components,
      -XML_Config) <-
      */

generate_merge_configuration(Mode, Gxl, Components, XML) :-
   member(Mode, [list, bubble, name]),
   assert_gxl_nodes_and_gxl_edges(Gxl),
   maplist( generate_merge_configuration(Mode),
      Components, XML_Components ),
   XML = config:[]:[
      action:[name:merge_nodes]:[
         components:[
            mode:keep_uninvolved_edge_ids]:XML_Components] ],
   !.
generate_merge_configuration(list_2, Gxl, Components, XML) :-
   assert_gxl_nodes_and_gxl_edges(Gxl),
   maplist( generate_merge_configuration(list),
      Components, XML_Components ),
   XML = config:[]:[
      action:[name:merge_nodes]:[
         components:[
            mode:change_uninvolved_edge_ids]:XML_Components] ].


/*** implementation ***********************************************/


/* generate_merge_configuration(+Mode, +Components, -XML) <-
      */

generate_merge_configuration(name, Name:Components, XML) :-
   maplist( component_to_xml,
      Components, C_XML ),
   generate_merge_conf:gxl_node_2_tmp(Name, Gxl_Node),
   String := Gxl_Node^prmtr^string^content::'*',
   XML = merged_node:[]:[
      prmtr:[
          color:red, handles:default,
          mouse_click:embedded_node, size:large,
          symbol:honeycomb]:[string:[
          bubble:'',
          font_style: (helvetica, roman, 12)]:
          String ],
      nodes:[]:C_XML ].
generate_merge_configuration(list, Components, XML) :-
   maplist( component_to_xml,
      Components, C_XML ),
   component_strings(Components, String),
   XML = merged_node:[]:[
      prmtr:[
          color:red, handles:default,
          mouse_click:embedded_node, size:large,
          symbol:honeycomb]:[string:[
          bubble:'',
          font_style: (helvetica, roman, 12)]:
          String],
      nodes:[]:C_XML ].
generate_merge_configuration(bubble, Components, XML) :-
   maplist( component_to_xml,
      Components, C_XML ),
   component_strings(Components, String),
   term_to_atom(String, Bubble),
   XML = merged_node:[]:[
      prmtr:[
          color:red, handles:default,
          mouse_click:embedded_node, size:large,
          symbol:honeycomb]:[string:[
          bubble:Bubble,
          font_style: (helvetica, roman, 12)]:[]],
      nodes:[]:C_XML ].

component_to_xml(Id, node:[id:Id]:[]).

component_strings(Node_Ids, Inner_Node_Names_2) :-
   ( foreach(Node_Id, Node_Ids),
     foreach(Gxl_Node, Inner_Gxl_Nodes) do
       generate_merge_conf:gxl_node_2_tmp(Node_Id, Gxl_Node) ),
   ( foreach(Gxl_Node, Inner_Gxl_Nodes),
     foreach(Node_Name, Inner_Node_Names_1) do
       generate_merge_conf:gxl_get_string_name(Gxl_Node,
          Node_Name) ),
   list_to_ord_set(Inner_Node_Names_1, Inner_Node_Names_2).

gxl_get_string_name(Gxl_Node, Node_Name) :-
   [Node_Name] := Gxl_Node^prmtr^string^content::'*',
   !.
gxl_get_string_name(_, '').


/* assert_gxl_nodes_and_gxl_edges(+Gxl) <-
      */

assert_gxl_nodes_and_gxl_edges(Gxl) :-
   retractall( generate_merge_conf:gxl_edge_2_tmp(_, _, _) ),
   retractall( generate_merge_conf:gxl_node_2_tmp(_, _) ),
   traverse_gxl(Gxl),
   !.

traverse_gxl(edge:Attr:Cont) :-
   [V, W] := (edge:Attr:Cont)@[from, to],
   assert( generate_merge_conf:gxl_edge_2_tmp(V, W,
         (edge:Attr:Cont)) ),
   checklist( traverse_gxl,
      Cont ),
   !.
traverse_gxl(node:Attr:Cont) :-
   Id := (node:Attr:Cont)@id,
   assert( generate_merge_conf:gxl_node_2_tmp(Id,
         (node:Attr:Cont)) ),
   checklist( traverse_gxl,
      Cont ),
   !.
traverse_gxl(_:_:Cont) :-
   checklist( traverse_gxl,
      Cont ),
   !.
traverse_gxl(_).


/******************************************************************/


