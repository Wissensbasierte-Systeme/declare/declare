

/******************************************************************/
/***                                                            ***/
/***             GXL: Basics Methods                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* config_to_element(Config, Element, Value) <-
      */

config_to_element(Config, Element, Value) :-
   Value := Config^Element,
   !.
config_to_element(_, Element, Value) :-
   alias_to_default_file(gxl_config, C_File),
   dread_(xml(gxl_config), C_File, Config),
   Value := Config^Element,
   !.


/* config_to_scc_symobls_colors_sizes(Config, Symbol_Color_Sizes) <-
      */

config_to_scc_symbol_color_sizes(Config, Symbol_Color_Sizes) :-
   FN_Colors := Config^node_colors,
   FN_Symbols := Config^symbols,
   Size_Alias := FN_Symbols@scc_size,
   Size := Config^sizes^size::[@alias=Size_Alias]@points,
   !,
   findall( C,
      C := FN_Colors^color::[@scc=true]@name,
      Colors ),
   findall( S,
      S := FN_Symbols^symbol@name,
      Symbols ),
   findall( [Symbol, Color, Size],
      ( member(Symbol, Symbols),
        member(Color, Colors) ),
      Symbol_Color_Sizes ).


/* gxl_id_to_xpce_address(+@Picture, +Id, -@Address) <-
      */

gxl_id_to_xpce_address(@Picture, Id, @Address) :-
   atomic(Id),
   term_to_atom_(Id_Term, Id),
   ground(Id_Term),
   term_to_atom_(Id_Term-Picture, Address),
   !.
gxl_id_to_xpce_address(@Picture, Id, @Address) :-
   atomic(Id),
   term_to_atom_(Id-Picture, Address),
   !.
gxl_id_to_xpce_address(@Picture, Id, @Address) :-
   term_to_atom_(Id-Picture, Address),
   !.


/* xpce_address_to_gxl_id(+@Address, -Id_Atom) <-
      */

xpce_address_to_gxl_id(@Address, Id) :-
   term_to_atom_(Id-_, Address),
   number(Id),
   !.
xpce_address_to_gxl_id(@Address, Id_2) :-
   term_to_atom_(Id_1-_, Address),
   not(atom(Id_1)),
   term_to_atom_(Id_1, Id_2),
   !.
xpce_address_to_gxl_id(@Address, Id) :-
   term_to_atom_(Id-_, Address),
   ground(Id),
   !.


/* term_to_atom_(A, B) <-
      */

term_to_atom_(A, B) :-
   catch( term_to_atom(A, B), _, fail ).


/* check_and_get_id(+XML, -Id) <-
      */

check_and_get_id(XML, Id) :-
   Id_Temp := XML@id,
   var(Id_Temp),
   new(Temp_Address, device),
%  free(Temp_Address),
   Temp_Address = @Id,
   !.
check_and_get_id(XML, Id) :-
   Id := XML@id,
   !.


/* check_atomic_ids_in_gxl(+Gxl) <-
      */

check_atomic_ids_in_gxl(_:Attributes:Content) :-
   checklist( check_gxl_id,
      Attributes ),
   !,
   checklist( check_atomic_ids_in_gxl,
      Content ).
check_atomic_ids_in_gxl(_:_:Content) :-
   checklist(check_atomic_ids_in_gxl,
      Content ),
   !.
check_atomic_ids_in_gxl(_).


/*** implementation ***********************************************/


/* check_gxl_id(+Type:Id) <-
      */

check_gxl_id(id:Id) :-
   check_atomic_ids_in_gxl_sub(Id),
   !.
check_gxl_id(from:Id) :-
   check_atomic_ids_in_gxl_sub(Id),
   !.
check_gxl_id(to:Id) :-
   check_atomic_ids_in_gxl_sub(Id),
   !.
check_gxl_id(_).

check_atomic_ids_in_gxl_sub(Id) :-
   atomic(Id),
   !.
check_atomic_ids_in_gxl_sub(Id) :-
   write('Non-atomic Id detected in Gxl: '),
   writeq(Id), writeln('!'),
   writeln('In Gxl, all Ids have to be atomic!').


/******************************************************************/


