

/******************************************************************/
/***                                                            ***/
/***             GXL: Transformations                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gxl_to_gxl_without_loops(+Gxl_1, -Gxl_2) <-
      */

gxl_to_gxl_without_loops(Gxl_1, Gxl_2) :-
   retractall( gxl_node_1_tmp(_, _) ),
   forall( Gxl_Node := Gxl_1^graph^node,
      node_to_embedded_node_index(Gxl_Node) ),
   gxl_delete_loops(Gxl_1, Gxl_2),
   retractall( gxl_node_1_tmp(_, _) ).


/* gxl_to_gxl_without_embedded_graphs(+Gxl_1, -Gxl_2) <-
      */

gxl_to_gxl_without_embedded_graphs(Gxl_1, Gxl_2) :-
   retractall( gxl_node_1_tmp(_, _) ),
   forall( Gxl_Node := Gxl_1^graph^node,
      node_to_embedded_node_index(Gxl_Node) ),
   gxl_delete_embedded_graphs(Gxl_1, Gxl_2),
   retractall( gxl_node_1_tmp(_, _) ).


/*** implementation ***********************************************/


/* gxl_delete_embedded_graphs(+Tree_1, -Tree_2) <-
      */

gxl_delete_embedded_graphs(node:Attr:Cont_1, node:Attr:Cont_2) :-
   sublist( gxl_filter_embedded_graphs(node),
      Cont_1, Cont_2 ),
      !.
gxl_delete_embedded_graphs(edge:Attr_1:Cont_1, edge:Attr_2:Cont_2) :-
   memberchk(from:From_1, Attr_1),
   memberchk(to:To_1, Attr_1),
   gxl_node_3_tmp(From_1, From_2),
   gxl_node_3_tmp(To_1, To_2),
   (edge:Attr_2:[]) := (edge:Attr_1:[])*[@from:From_2, @to:To_2],
   sublist( gxl_filter_embedded_graphs(edge),
      Cont_1, Cont_2 ),
      !.
gxl_delete_embedded_graphs(Tag:Attr:Cont_1, Tag:Attr:Cont_2) :-
   maplist( gxl_delete_embedded_graphs,
      Cont_1, Cont_2 ),
   !.
gxl_delete_embedded_graphs(Tree, Tree).

gxl_filter_embedded_graphs(Tag, graph:_:_) :-
   memberchk(Tag, [node, edge]),
   !,
   fail.
gxl_filter_embedded_graphs(_, _).


/* gxl_delete_loops(+Gxl_1, -Gxl_2) <-
      */

gxl_delete_loops(Tag:Attr:Cont_1, Tag:Attr:Cont_3) :-
   sublist( gxl_filter_self_loops,
      Cont_1, Cont_2 ),
   maplist( gxl_delete_loops,
      Cont_2, Cont_3),
   !.
gxl_delete_loops(A, A) :-
   !.

gxl_filter_self_loops(edge:Attr:_) :-
   memberchk(to:To, Attr),
   memberchk(from:From, Attr),
   gxl_node_3_tmp(To, To_Father),
   gxl_node_3_tmp(From, From_Father),
   To_Father = From_Father,
   !,
   fail.
gxl_filter_self_loops(_) :-
   !.

gxl_node_3_tmp(Node, Node_Father) :-
   gxl_node_1_tmp(Node, Node_Father),
   !.
gxl_node_3_tmp(Node, Node) :-
   !.


/******************************************************************/


