

/******************************************************************/
/***                                                            ***/
/***  GXL: Contract Nodes                                       ***/
/***                                                            ***/
/******************************************************************/


:- module( gxl_contract, [
      gxl_contract_nodes/2 ] ).

:- dynamic gxl_node/3,
           gxl_node_embedded/2,
           gxl_edge/2.


/*** interface ****************************************************/


/* gxl_contract_nodes(+Gxl_1, -Gxl_2) <-
      */

gxl_contract_nodes(Gxl_1, Gxl_3) :-
   retractall( gxl_contract:gxl_node(_, _, _) ),
   retractall( gxl_contract:gxl_edge(_, _, _) ),
   retractall( gxl_contract:gxl_node_embedded(_, _) ),
   assert_gxl_main_nodes_and_edges(Gxl_1),
   collect_nodes_to_merge,
   retractall( gxl_contract:gxl_node_embedded(Id, Id) ),
   delete_nodes_to_merge(Gxl_1, Gxl_2),
   nodes_to_merge_to_embedded_nodes(Gxl_2, Gxl_3),
   retractall( gxl_contract:gxl_node(_, _, _) ),
   retractall( gxl_contract:gxl_edge(_, _, _) ),
   retractall( gxl_contract:gxl_node_embedded(_, _) ).


/*** implementation ***********************************************/


/* assert_gxl_main_nodes_and_edges(+Gxl) <-
      */

assert_gxl_main_nodes_and_edges(node:Attr:Cont) :-
   memberchk(id:Id, Attr),
   Name := (node:Attr:Cont)^prmtr^string^content::'*',
   assert( gxl_node(Name, Id, node:Attr:Cont) ),
   !.
assert_gxl_main_nodes_and_edges(edge:Attr:_) :-
   memberchk(from:From, Attr),
   memberchk(to:To, Attr),
   assert( gxl_edge(From, To) ),
   !.
assert_gxl_main_nodes_and_edges(_:_:Cont) :-
   checklist( assert_gxl_main_nodes_and_edges,
      Cont ),
   !.
assert_gxl_main_nodes_and_edges(_).


/* nodes_to_merge_to_embedded_nodes(+Gxl_1, -Gxl_2) <-
      */

nodes_to_merge_to_embedded_nodes(node:Attr:Cont_1, node:Attr:Cont_2) :-
   memberchk(id:Id_1, Attr),
   findall( Node,
      ( gxl_contract:gxl_node_embedded(Id_1, Id_2),
        gxl_contract:gxl_node(_, Id_2, Node) ),
      Nodes ),
   not( Nodes = [] ),
   Embedded = graph:[]:Nodes,
   Cont_2 = [Embedded|Cont_1].
nodes_to_merge_to_embedded_nodes(node:Attr:Cont, node:Attr:Cont) :-
   !.
nodes_to_merge_to_embedded_nodes(edge:Attr:Cont, edge:Attr:Cont) :-
   !.
nodes_to_merge_to_embedded_nodes(Tag:Attr:Cont_1, Tag:Attr:Cont_2) :-
   maplist( nodes_to_merge_to_embedded_nodes,
      Cont_1, Cont_2 ),
   !.
nodes_to_merge_to_embedded_nodes(A, A) :-
   !.


/* delete_nodes_to_merge(+Gxl_1, Gxl_2) <-
      */

delete_nodes_to_merge(node:Attr:Cont, node:Attr:Cont) :-
   !.
delete_nodes_to_merge(edge:Attr:Cont, edge:Attr:Cont) :-
   !.
delete_nodes_to_merge(Tag:Attr:Cont_1, Tag:Attr:Cont_3) :-
   sublist( filter_nodes_to_merge,
      Cont_1, Cont_2 ),
   maplist( delete_nodes_to_merge,
      Cont_2, Cont_3 ).
delete_nodes_to_merge(A, A) :-
   !.

filter_nodes_to_merge(node:Attr:_) :-
   memberchk(id:Id, Attr),
   gxl_contract:gxl_node_embedded(_, Id),
   !,
   fail.
filter_nodes_to_merge(_).


/* collect_nodes_to_merge <-
      */

collect_nodes_to_merge :-
   forall( gxl_contract:gxl_node(Name, Id, _),
      collect_nodes_to_merge(Name, Id, _) ).

/*
find_ggT_prefix([(M:P_1)/_A], Id_1) :-
   atom(P_1),
   findall( P_2,
      ( gxl_contract:gxl_node([(M:P_2)/_], Id_2, _),
        not( Id_1 = Id_2 ),
        atom(P_2),
        atom_prefix(P_1, P_2) ),
      Ids ),
   writelq(P_1-Ids),
   !.
find_ggT_prefix(_, _).
*/

collect_nodes_to_merge([(M:P)/_A], Id_2, _) :-
   sub_atom(P, Len, 4, 0, '_sub'),
   sub_atom(P, 0, Len, _, Prefix),
   gxl_contract:gxl_node([(M:Prefix)/_], Id_1, _),
   assert( gxl_node_embedded(Id_1, Id_2) ),
   !.
collect_nodes_to_merge([(M:P)/_A], Id_2, _) :-
   sub_atom(P, Len, 5, 0, '_step'),
   sub_atom(P, 0, Len, _, Prefix),
   gxl_contract:gxl_node([(M:Prefix)/_], Id_1, _),
   assert( gxl_node_embedded(Id_1, Id_2) ),
   !.
collect_nodes_to_merge([(M:P)/_], Id_2, _) :-
   sub_atom(P, Len, 5, 0, '_step'),
   sub_atom(P, 0, Len, _, Prefix),
   gxl_contract:gxl_node([(M:Prefix)/_], Id_1, _),
   assert( gxl_node_embedded(Id_1, Id_2) ),
   !.
collect_nodes_to_merge(['Box 3'], Id_2, _) :-
   gxl_contract:gxl_node([box_2], Id_1, _),
   assert( gxl_node_embedded(Id_1, Id_2) ),
   !.
collect_nodes_to_merge([Name], Id_2, _) :-
   atom(Name),
   atom_prefix(Name, 'Circle'),
   gxl_contract:gxl_node(['Circle 1'], Id_1, _),
   assert( gxl_node_embedded(Id_1, Id_2) ),
   !.
collect_nodes_to_merge(_, _, _).


/******************************************************************/


