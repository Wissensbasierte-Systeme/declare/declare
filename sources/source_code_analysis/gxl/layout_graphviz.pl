

/******************************************************************/
/***                                                            ***/
/***      GXL: Layout Graphviz                                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* layout_graphviz(+Layout, +Gxl_1, -Gxl_2) <-
      */

layout_graphviz(Layout, Gxl_1, Gxl_2) :-
   member(Layout, [dot, neato, twopi, circo, fdp]),
   tmp_file(graphviz, '', File),
   file_name_extension(File, dot, Dot_File),
   file_name_extension(File, svg, SVG_File),
   gxl_to_digraph(Gxl_1, Dot_File),
   concat_atoms([
      Layout, ' -Tsvg -o ', SVG_File, ' ', Dot_File],
      Command),
   unix(system(Command)),
   dread_(xml(dtd:[cx:int, cy:int]:[]), SVG_File, XML_Layout),
   merge_gxl_and_layout(Gxl_1, XML_Layout, Gxl_2),
   delete_file(Dot_File),
   delete_file(SVG_File).


/*** implementation ***********************************************/


/* gxl_to_digraph(+Gxl, +File) <-
      */

gxl_to_digraph(Gxl, File) :-
   retractall( graphviz_tmp(_) ),
   gxl_to_vertices_and_edges(node_id-v_w, Gxl, _, Edges),
   gxl_to_single_nodes(Gxl, Vertices),
   predicate_to_file(File,
      vertices_edges_to_digraph(Vertices, Edges) ),
   retractall( graphviz_tmp(_) ).


/* vertices_edges_to_digraph(+Vertices, +Edges) <-
      */

vertices_edges_to_digraph(Vertices, Edges) :-
   writeln('digraph export {'),
   checklist( node_to_digraph,
      Vertices ),
   checklist( edge_to_digraph,
      Edges ),
   writeln('}').


/* node_to_digraph(+Node) <-
      */

node_to_digraph(Node) :-
   write_list(['"', Node, '";']),
   nl.


/* edge_to_digraph(+Edge) <-
      */

edge_to_digraph(V-W) :-
   write_list(['"', V, '" -> "', W, '";']),
   nl.


/******************************************************************/


