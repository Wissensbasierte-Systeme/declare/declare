

/******************************************************************/
/***                                                            ***/
/***           GXL: Compound Methods                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* vertices_and_edges_to_picture(+Vertices, +Edges, -Picture) <-
      */

vertices_and_edges_to_picture(xpce, Vertices, Edges) :-
   vertices_and_edges_to_picture(xpce, Vertices, Edges, _).

vertices_and_edges_to_picture(xpce, Vertices, Edges, Picture) :-
   alias_to_file(gxl_config, C_File),
   dread_(xml(gxl_config), C_File, Config),
   vertices_and_edges_to_gxl(Vertices, Edges, Gxl),
   gxl_to_picture(Config, Gxl, Picture),
   xpce_graph_layout(xpce, Picture),
%  dwrite(xml, Gxl),
   !.
vertices_and_edges_to_picture(Mode, Vertices, Edges, Picture) :-
   vertices_and_edges_to_picture(_, Mode, Vertices, Edges, Picture).

vertices_and_edges_to_picture(Roots, Mode,
      Vertices, Edges, Picture) :-
   alias_to_file(gxl_config, C_File),
   dread_(xml(gxl_config), C_File, Config),
   vertices_and_edges_to_gxl(Vertices, Edges, Gxl_1),
   gxl_graph_layout(Config, Mode, Roots, Gxl_1, Gxl_2),
   !,
   gxl_to_picture(Config, Gxl_2, Picture).


/* picture_to_vertices_and_edges(+Picture, -Vertices, -Edges) <-
      */

picture_to_vertices_and_edges(Picture, Vertices, Edges) :-
   picture_to_gxl(Picture, Gxl, _),
   gxl_to_vertices_and_edges(node_id-v_w, Gxl, Vertices, Edges).


/* picture_to_ugraph(+Picture, -Ugraph) <-
      */

picture_to_ugraph(Picture, Ugraph) :-
   picture_to_gxl(Picture, Gxl, _),
   gxl_to_vertices_and_edges(node_id-v_w, Gxl, Vertices, Edges),
   vertices_edges_to_ugraph(Vertices, Edges, Ugraph).


/* gxl_to_ugraph(+Gxl, -Ugraph) <-
      */

gxl_to_ugraph(Gxl, Ugraph) :-
   gxl_to_vertices_and_edges(
      node_id-v_w, Gxl, Node_Ids, V_W_Edges),
   ugraphs:vertices_edges_to_ugraph(
      Node_Ids, V_W_Edges, Ugraph).


/* ugraph_to_gxl(+Ugraph, -Gxl) <-
      */

ugraph_to_gxl(Ugraph, Gxl) :-
   ugraph_to_vertices_edges(
      Ugraph, Vertices, Edges),
   vertices_and_edges_to_gxl(
      Vertices, Edges, Gxl).


/* ugraph_to_picture(+Mode, +Ugraph, -Picture) <-
      */

ugraph_to_picture(xpce, Ugraph, Picture) :-
   ugraph_to_gxl(Ugraph, Gxl),
   alias_to_file(gxl_config, C_File),
   dread_(xml(gxl_config), C_File, Config),
   gxl_to_picture(Config, Gxl, Picture),
   xpce_graph_layout(xpce, Picture).
ugraph_to_picture(Mode, Ugraph, Picture) :-
   ugraph_to_gxl(Ugraph, Gxl_1),
   alias_to_file(gxl_config, C_File),
   dread_(xml(gxl_config), C_File, Config),
   gxl_graph_layout(Mode, Config, Gxl_1, Gxl_2),
   gxl_to_picture(Config, Gxl_2, Picture).


/*** implementation ***********************************************/


/******************************************************************/

