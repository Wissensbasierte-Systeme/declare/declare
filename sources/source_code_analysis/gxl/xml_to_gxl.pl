

/******************************************************************/
/***                                                            ***/
/***                  XML to GXL                                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xml_tree_to_gxl(+XML, -Gxl, -Root_Id) <-
      */

xml_tree_to_gxl(XML, Gxl, Root_Id) :-
   gensym(xml_node, Root_Id),
   Label:Attr:_ = XML,
   Node = node:[id:Root_Id]:[
      prmtr:[mouse_click:xml_root,
         color:white, symbol:text_in_box, size:medium]:[
            string:[]:[Label] ] ],
   rar:update(gxl_node, Root_Id, Node),
   add_xml_attr(Root_Id, Attr),
   add_xml_content(out, Root_Id, XML),
   gxl_db_to_gxl(Gxl).


/*** implementation ***********************************************/


/* add_xml_content(in|out, +Id, +XML) <-
      */

add_xml_content(Direction, Id, XML) :-
   Content := XML^content::'*',
   forall( member(Element, Content),
      add_xml_element(Direction, Id, Element) ).


/* add_xml_element(in|out, +Id, +Element) <-
      */

add_xml_element(Direction, Id, 'swrl:AtomList':A:C) :-
   add_atom_list(Direction, Id, 'swrl:AtomList':A:C),
   !.
add_xml_element(Direction, Id_1, Element) :-
   gensym(xml_node, Id_2),
   Label:Attr:_ = Element,
   Node = node:[id:Id_2]:[
      prmtr:[mouse_click:xml_element,
         color:white, symbol:text_in_box, size:medium]:[
            string:[bubble:'SWRL-Rule']:[Label] ] ],
   rar:update(gxl_node, Id_2, Node),
   add_xml_attr(Id_2, Attr),
   add_xml_edge(Direction, Id_1, Id_2),
   add_xml_content(out, Id_2, Element).
add_xml_element(_Direction, _Id_1, Element) :-
   writeln('Not able to add element':Element).


/* add_atom_list(in|out, +Rule, +Atom_List) <-
      */

add_atom_list(Direction, Rule, Atom_List) :-
   First := Atom_List^'rdf:first',
   add_xml_content(Direction, Rule, First),
   Rest := Atom_List^'rdf:rest'@'rdf:resource',
   Rest = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#nil'.
add_atom_list(Direction, Rule, Atom_List) :-
   First := Atom_List^'rdf:first',
   add_xml_content(Direction, Rule, First),
   Rest := Atom_List^'rdf:rest'^'swrl:AtomList',
   add_atom_list(Direction, Rule, Rest).


/* add_xml_edge(in|out, +Id_1, +Id_2) <-
      */

add_xml_edge(_, root, _) :-
   !.
add_xml_edge(out, Id_1, Id_2) :-
   term_to_atom(Id_1-Id_2, Id),
   Edge = edge:[id:Id, from:Id_1, to:Id_2]:[
      prmtr:[mouse_click:mouse_click,
         arrows:second, color:black]:[
            string:[bubble:'']:[] ] ],
   rar:update(gxl_edge, Id_1, Id_2, Edge).
add_xml_edge(out2, Id_1, Id_2) :-
   term_to_atom(Id_1-Id_2, Id),
   Edge = edge:[id:Id, from:Id_1, to:Id_2]:[
      prmtr:[mouse_click:mouse_click,
         arrows:second, color:red]:[
            string:[bubble:'']:[] ] ],
   rar:update(gxl_edge, Id_1, Id_2, Edge).
add_xml_edge(out3, Id_1, Id_2) :-
   term_to_atom(Id_1-Id_2, Id),
   Edge = edge:[id:Id, from:Id_1, to:Id_2]:[
      prmtr:[mouse_click:mouse_click,
         arrows:second, color:blue]:[
            string:[bubble:'']:[] ] ],
   rar:update(gxl_edge, Id_1, Id_2, Edge).
add_xml_edge(out_attr, Id_1, Id_2) :-
   term_to_atom(Id_1-Id_2, Id),
   Edge = edge:[id:Id, from:Id_1, to:Id_2]:[
      prmtr:[mouse_click:mouse_click,
         arrows:none, color:green]:[
            string:[bubble:'']:[] ] ],
   rar:update(gxl_edge, Id_1, Id_2, Edge).
add_xml_edge(in, Id_1, Id_2) :-
   add_xml_edge(out, Id_2, Id_1).
add_xml_edge(none, Id_1, Id_2) :-
   term_to_atom(Id_1-Id_2, Id),
   Edge = edge:[id:Id, from:Id_1, to:Id_2]:[
      prmtr:[mouse_click:mouse_click,
         arrows:none, color:blue]:[
            string:[bubble:'']:[] ] ],
   rar:update(gxl_edge, Id_1, Id_2, Edge).
add_xml_edge(Arrow, Id_1, Id_2, Color) :-
   term_to_atom(Id_1-Id_2, Id),
   Edge = edge:[id:Id, from:Id_1, to:Id_2]:[
      prmtr:[mouse_click:mouse_click,
         arrows:Arrow, color:Color]:[
            string:[bubble:'']:[] ] ],
   rar:update(gxl_edge, Id_1, Id_2, Edge).


/* add_xml_attr(+Id, +Attr) <-
      */

add_xml_attr(Id_1, Attr) :-
   forall( member(A, Attr),
      ( gensym(xml_node, Id_2),
        Node = node:[id:Id_2]:[
           prmtr:[mouse_click:xml_rule,
           color:white, symbol:text_in_ellipse, size:medium]:[
            string:[]:[A] ] ],
        rar:update(gxl_node, Id_2, Node),
        Edge = edge:[id:Id_1-Id_2, from:Id_1, to:Id_2]:[
           prmtr:[mouse_click:mouse_click,
           arrows:none, color:black]:[string:[]:[] ] ],
        rar:update(gxl_edge, Id_1, Id_2, Edge) ) ).


/******************************************************************/


