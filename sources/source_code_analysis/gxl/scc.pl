

/******************************************************************/
/***                                                            ***/
/***  GXL: Strongly Connected Components                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gxl_to_gxl_scc(+Gxl_1, -Gxl_2) <-
      */

gxl_to_gxl_scc(Gxl_1, Gxl_2) :-
   gxl_to_vertices_and_edges(
      node_id-v_w, Gxl_1, Node_Ids, V_W_Edges),
   ugraphs:vertices_edges_to_ugraph(
      Node_Ids, V_W_Edges, Ugraph),
   ugraphs:strong_components(Ugraph, Components, _),
   generate_merge_configuration(list, Gxl_1, Components,
      Config),
   gxl_to_gxl_with_contracted_nodes(Config, Gxl_1, Gxl_2).


/*** implementation ***********************************************/


/******************************************************************/


