

/******************************************************************/
/***                                                            ***/
/***       DisLog:  Field Notation to XML                       ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      dread/3, dwrite/3.


/*** interface ****************************************************/


/* dread(Dialect, File, FN_Term)
   xml_file_to_fn_term(File, FN_Term) <-
      */

dread(_, File, _) :-
   \+ exists_file_with_tilde(File),
   !,
   write_list(
      ['DDK Error: file "', File, '" does not exist !\n']),
   fail.
dread(xml(Alias), File_1, FN_Triple_2) :-
   !,
   alias_to_dtd(Alias, DTD),
   Options = [dialect(xml), types(DTD)],
   tilde_to_home_path(File_1, File_2),
   xml_io:read_structure(File_2, [FN_Triple_1], Options),
   file_directory_name(File_2, Dir),
   xml_io:resolve_file_references_in_xml(
      Dir, Alias, FN_Triple_1, FN_Triple_2).
dread(xml, File, FN_Term) :-
   !,
   xml_file_to_fn_term(File, FN_Term).
dread(html, File, FN_Term) :-
%  Options = [dialect(sgml), max_errors(5000), space(remove)],
   Options = [dialect(sgml)],
   load_structure(File, Content, Options),
   xml_swi_to_fn(Content, FN_Term),
   !.
dread(Dialect, File, FN_Term) :-
   Options = [dialect(Dialect), max_errors(5000), space(remove)],
   sgml_file_to_fn_term(File, FN_Term, Options).

xml_file_to_fn_term(File, FN_Term, Options) :-
   sgml_file_to_fn_term(File, FN_Term, Options).

xml_file_to_fn_term(File, FN_Term) :-
%  Options = [dialect(xml), max_errors(5000), space(sgml)],
   Options = [dialect(xml), max_errors(5000), space(remove)],
   sgml_file_to_fn_term(File, FN_Term, Options).


/* sgml_file_to_fn_term(File, FN_Term, Options) <-
      */

sgml_file_to_fn_term(File, FN_Term, Options) :-
   load_structure(File, Content, Options),
   xml_swi_to_fn(Content, FN_Term),
   !.


/* dwrite(xml, File, FN_Term)
   fn_to_xml_file(FN_Term, File) <-
      */

dwrite(xml, File, FN_Term) :-
   fn_term_to_xml_file(FN_Term, File).
dwrite(xml_2, File, FN_Term) :-
   xml_io:fn_term_to_xml_file(FN_Term, File).

fn_to_xml_file(FN_Term, File) :-
   fn_term_to_xml_file(FN_Term, File).


/* dwrite(xml, FN_Term)
   fn_to_xml(FN_Term) <-
      */

dwrite(xml, FN_Term) :-
   field_notation_to_xml(FN_Term).

fn_to_xml(FN_Term) :-
   field_notation_to_xml(FN_Term).

dwrite(html, File, FN_Term) :-
   predicate_to_file( File,
      field_notation_to_xml(FN_Term) ).


/* pretty_print_xml_file(File_1, File_2) <-
      */

pretty_print_xml_file(File_1, File_2) :-
   xml_file_to_fn_term(File_1, [FN_Term]),
   fn_item_parse(FN_Term, Tag:_:_),
   ( member(Tag, [html, htm, 'HTML']),
     predicate_to_file( File_2,
        field_notation_to_xml(FN_Term) )
   ; fn_term_to_xml_file(FN_Term, File_2) ),
   !.


/* xml_swi_to_fn(Element_1, Element_2) <-
      */

xml_swi_to_fn(element(Name, As1, Es1), Name:As2:Es2) :-
   !,
   maplist( xml_swi_attribute_to_fn,
      As1, As2 ),
   maplist( xml_swi_to_fn,
      Es1, Es2 ).
xml_swi_to_fn(entity(X), Y) :-
   !,
   name_append(['&', X, ';'], Y).
xml_swi_to_fn(Xs, Ys) :-
   is_list(Xs),
   !,
   maplist( xml_swi_to_fn,
      Xs, Ys ).
xml_swi_to_fn(X, X).

xml_swi_attribute_to_fn(A=V, A:V).


/* swi_xml_entity_to_fn(entity(X), Y) <-
      */

xml_swi_entity_to_fn(entity(X), Y) :-
   name_append(['&', X, ';'], Y).


/*** implementation ***********************************************/


/* xml_files_to_fn_term(Files, FN_Term) <-
      */

xml_files_to_fn_term(Files, FN_Term) :-
   maplist( xml_file_to_fn_term,
      Files, FN_Terms ),
   append(FN_Terms, FN_Term).


/* fn_term_to_xml_file(FN_Term, File) <-
      */

fn_term_to_xml_file(FN_Term, File) :-
   predicate_to_file( File,
      ( writeln('<?xml version=''1.0'' encoding=''ISO-8859-1'' ?>'),
        nl,
        field_notation_to_xml(FN_Term) ) ).


/* field_notation_to_xml(Object) <-
      */

field_notation_to_xml(Object) :-
   is_list(Object),
   !,
   write('<>'), nl,
   checklist( field_notation_to_xml(3),
      Object ),
   write('</>'), nl,
   !.
field_notation_to_xml(Object) :-
   field_notation_to_xml(0, Object),
   !.

field_notation_to_xml(I, A:V:W) :-
   !,
   field_notation_to_xml_write(I, A:V:W).
field_notation_to_xml(I, A:V) :-
   !,
   field_notation_to_xml_write(I, A:V).
field_notation_to_xml(I, Object) :-
   is_list(Object),
   !,
   checklist( field_notation_to_xml(I),
      Object ).
field_notation_to_xml(I, comment(Xs)) :-
   !,
   name(Text, Xs),
   tab(I),
   write_list(['<!--', Text, '-->\n']).
field_notation_to_xml(I, declare(Xs)) :-
   !,
   name(Text, Xs),
   tab(I),
   write_list(['<!', Text, '>\n']).
field_notation_to_xml(I, V) :-
   tab(I),
   writeln(V).

field_notation_to_xml_write(I, A:V:[]) :-
   !,
   tab(I), write_list(['<', A]),
   checklist( field_notation_write_attribute,
      V ),
   writeln('/>').
field_notation_to_xml_write(I, A:V:X) :-
   \+ is_list(X),
   !,
   tab(I), write_list(['<', A]),
   checklist( field_notation_write_attribute,
      V ),
   write_list(['>', X, '</', A, '>']), nl.
field_notation_to_xml_write(I, A:V:[X]) :-
   atomic(X),
   !,
   tab(I), write_list(['<', A]),
   checklist( field_notation_write_attribute,
      V ),
   write_list(['>', X, '</', A, '>']), nl.
field_notation_to_xml_write(I, A:V:W) :-
   tab(I), write_list(['<', A]),
   checklist( field_notation_write_attribute,
      V ),
   writeln('>'),
   J is I + 3,
   field_notation_to_xml(J, W),
   tab(I), write_list(['</', A, '>']), nl.
field_notation_to_xml_write(I, A:X) :-
   \+ is_list(X),
   !,
   tab(I), write_list(['<', A, '>', X, '</', A, '>']), nl.
field_notation_to_xml_write(I, A:[X]) :-
   atomic(X),
   !,
   tab(I), write_list(['<', A, '>', X, '</', A, '>']), nl.
field_notation_to_xml_write(I, A:V) :-
   tab(I), write_list(['<', A, '>']), nl,
   J is I + 3,
   field_notation_to_xml(J, V),
   tab(I), write_list(['</', A, '>']), nl.

field_notation_write_attribute(Attribute:Value) :-
   ( atomic(Value),
     Value_2 = Value
   ; term_to_atom(Value, Value_2) ),
   name_exchange_sublist([["\"", "&quot;"]], Value_2, Value_3),
   write_list([' ', Attribute, '="', Value_3, '"']),
%  write_list([' ', Attribute, '="', Value, '"']),
   !.


/*** tests ********************************************************/


test(field_notation:field_notation_to_xml, 1) :-
   Object = [
      rented_movie:[
         a:1, b:2 ]: [
         movie:[
            title:'Something about Mary',
            price_code:new_release ],
         days_rented:4 ] ],
   field_notation_to_xml(Object).

test(field_notation:field_notation_to_xml, 2) :-
   Movie_Database = [
      (rental:[id:1]):[
         movie:[
            title:'Something about Mary',
            price_code:new_release ],
         days_rented:4 ],
      (rental:[id:2]):[
         movie:[
            title:'Bonanza',
            price_code:regular,
            length:2 ],
         days_rented:7 ] ],
   findall( Id-Title,
      ( Title := Movie_Database^Rental^movie^title,
        Id := [Rental]^rental^id ),
      Report ),
   writeln_list(Report).


/******************************************************************/


