

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Prolog to XML                           ***/
/***                                                            ***/
/******************************************************************/


:- op(1200, xfx, ':->'),
   op(1200, xfx, ':<-').


/*** implemantation ***********************************************/


/* dislog_files_to_xml(Xml) <-
      */

dislog_files_to_xml :-
   dislog_files_to_xml(Xml),
   dwrite(xml, 'dislog.xml', Xml).

dislog_files_to_xml(Xml) :-
   rar_variable_set(exported_predicates, []),
   dislog_sources(Files),
   dislog_variable_get(source_path, Sources),
   maplist( concat(Sources),
      Files, Paths ),
   maplist( program_file_to_xml,
      Paths, Xmls ),
   Xml = dislog:[]:Xmls.


/* directory_to_xml(Directory, Xml) <-
      */

directory_to_xml(Directory, Xml) :-
   directory_contains_files(Directory, Files_1),
   findall( File,
      ( member(File, Files_1),
        concat(_, '.pl', File) ),
      Files_2 ),
   concat(Directory, '/', Directory_2),
   maplist( concat(Directory_2),
      Files_2, Paths ),
   maplist( program_file_to_xml,
      Paths, Xmls ),
   Xml = module:[name:Directory]:Xmls,
   dislog_variable_get(ruleml_output, Path),
   fn_term_to_xml_file(Xml, Path),
   !.


/* program_module_to_xml(Unit, Module, Xml) <-
      */

program_module_to_xml(Unit, Module, Xml) :-
   dislog_module_to_tree(Module, [_|Xs]),
   list_of_elements_to_relation(Files, Xs),
   name_append([Unit, '/', Module], Unit_Module),
   maplist( module_and_file_to_path(Unit_Module),
      Files, Paths ),
   maplist( program_file_to_xml,
      Paths, Xmls ),
   Xml = module:[name:Unit_Module]:Xmls,
   dislog_variable_get(ruleml_output, Path),
   fn_term_to_xml_file(Xml, Path),
   !.

module_and_file_to_path(Module, File, Path) :-
   dislog_variable_get(source_path, Sources),
   name_append([Sources, Module, '/', File], Path).


/* program_to_xml(Program, Xml) <-
      */

program_to_xml(Program, Xml) :-
   maplist( rule_to_xml,
      Program, Xml ).

rule_to_xml(Rule, Xml) :-
   rule_to_variables_substitution(Rule, Substitution),
   parse_dislog_rule(Rule, Head, Body, _),
   ( Body = [],
     rule_and_variables_to_xml(Head-no_operator-[]-Substitution, Xml)
   ; rule_and_variables_to_xml(Head-(:-)-Body-Substitution, Xml) ).

rule_to_variables_substitution(Rule, Substitution) :-
   free_variables(Rule, Variables),
   length(Variables, N),
   n_free_variables(N, Xs),
   listvars(Xs, 1),
   ( foreach(X, Xs), foreach(V, Variables), foreach(S, Substitution) do
        S = (X = V) ).


/* program_file_to_xml(File, Xml) <-
      */

program_file_to_xml(File, Xml) :-
   read_term_loop_parse(File, Rs),
   program_file_to_xml_sub(File, Rs, Module),
   maplist( rule_and_variables_to_xml,
      Rs, Xmls ),
   Xml = file:[path:File, module:Module]:Xmls,
   !.

program_file_to_xml_sub(File, Rs, Module) :-
   first(Rs, []-(:-)-[module(Module, Exports)]-_),
   rar_variable_get(exported_predicates, Predicates),
   maplist( element_to_xml_element,
      Exports, Xml_Exports ),
   Xml = module:[name:Module, file:File]:Xml_Exports,
   rar_variable_set(exported_predicates, [Xml|Predicates]).
program_file_to_xml_sub(_, _, user).

element_to_xml_element(P/A, Xml) :-
   Xml = atom:[predicate:P, arity:A]:[].

read_term_loop_parse(File, Rs) :-
   read_term_loop(File, Rs_2),
   !,
   maplist( rule_and_variables_to_head_and_body,
      Rs_2, Rs ).

read_term_loop(File, Rs) :-
   ( ( file_exists(File),
       File_2 = File )
   ; concat(File, '.pl', File_2) ),
   see(File_2),
   read_term_loop(Rs),
   seen,
   write_list(['<--- ', File]), nl,
   !.

read_term_loop(Rs) :-
   read_term(Rule,
      [variable_names(Variables), syntax_errors(dec10)]),
   ( ( Rule = end_of_file,
       Rs = [] )
   ; ( R = Rule-Variables,
       read_term_loop(Rs_2),
       Rs = [R|Rs_2] ) ).


/* rule_and_variables_to_head_and_body(
         Rule-Variables, Head-Body-Variables) <-
      */

rule_and_variables_to_head_and_body(
      Rule-Variables, Head-Operator-Body-Variables) :-
   rule_to_head_and_body(Rule, Head-Operator-Body),
   !.


/* rule_to_head_and_body(Rule, Head-Body) <-
      */

rule_to_head_and_body(Rule, Head-Operator-Body) :-
   Rule =.. [Operator, X, Y],
   rule_operator(Operator),
   !,
   comma_structure_to_list(X, Head),
   comma_structure_to_list(Y, Body).
rule_to_head_and_body(Rule, []-Operator-Body) :-
   Rule =.. [Operator, Y],
   rule_operator(Operator),
   !,
   comma_structure_to_list(Y, Body).
rule_to_head_and_body(Rule, Head-no_operator-[]) :-
   comma_structure_to_list(Rule, Head).

rule_operator(Operator) :-
   member(Operator, ['?-', ':-', '--->', ':->', ':<-']).


/* rule_and_variables_to_xml(Head-Body-Variables, Xml) <-
      */

rule_and_variables_to_xml(Head-Operator-Body-Variables, Xml) :-
   maplist( atom_and_variables_to_xml(Variables),
      Head, Xml_Head ),
   maplist( atom_and_variables_to_xml(Variables),
      Body, Xml_Body ),
   ( ( Operator = no_operator,
       !,
       Xml = rule:[]:[
          head:[]:Xml_Head, body:[]:Xml_Body] )
   ; ( Xml = rule:[operator:Operator]:[
          head:[]:Xml_Head, body:[]:Xml_Body ] ) ).
%  Xml = rule:[]:[
%     head:[]:Xml_Head, operator:Operator, body:[]:Xml_Body ].


/* atom_and_variables_to_xml(Variables, Atom, Xml) <-
      */

atom_and_variables_to_xml(Variables, Atom, Xml) :-
   var(Atom),
   !,
   argument_and_variables_to_xml(Variables, Atom, X),
   Xml = atom:[predicate:(call)/1]:[X].
atom_and_variables_to_xml(Variables, Atom, Xml) :-
   dislog_variable_get(fn_query_result_type, xml),
   Atom =.. [':', Module, Atom_2],
   callable(Module),
   callable(Atom_2),
   Atom_2 =.. [Functor|Arguments],
   length(Arguments, Arity),
   maplist( argument_and_variables_to_xml(Variables),
      Arguments, Args ),
   Xml = atom:[predicate:(Module:Functor)/Arity]:Args.
atom_and_variables_to_xml(Variables, Atom, Xml) :-
   Atom =.. [Junctor|Atoms],
   formula_junctor(Junctor, Xml_Junctor),
   maplist( atom_and_variables_to_xml(Variables),
      Atoms, Xmls ),
   Xml = formula:[junctor:Xml_Junctor]:Xmls.
atom_and_variables_to_xml(Variables, Atom, Xml) :-
   Atom =.. [Functor|Arguments],
   length(Arguments, Arity),
   maplist( argument_and_variables_to_xml(Variables),
      Arguments, Args ),
   Predicate = Functor/Arity,
   Xml = atom:[predicate:Predicate]:Args.

formula_junctor(';', or).
formula_junctor('->', if_then).


/* argument_and_variables_to_xml(Variables, X, Xml) <-
      */

argument_and_variables_to_xml(Variables, X, Xml) :-
   var(X),
   !,
   variable_and_variables_to_xml(Variables, X, U),
   Xml = var:[name:U]:[].
% argument_and_variables_to_xml(Variables, X, Xml) :-
%    X =.. [':', New_Module, New_Atom],
%    callable(New_Module),
%    callable(New_Atom),
%    New_Atom =.. [Functor|Arguments],
%    maplist( argument_and_variables_to_xml(Variables),
%       Arguments, Args ),
%    Xml = term:[functor:(New_Module:Functor)]:Args.
argument_and_variables_to_xml(Variables, X, Xml) :-
   X =.. [Functor|Arguments],
   maplist( argument_and_variables_to_xml(Variables),
      Arguments, Args ),
   Xml = term:[functor:Functor]:Args.

variable_and_variables_to_xml(Variables, X, U) :-
   member(U=V, Variables),
   V == X,
   !.
variable_and_variables_to_xml(_, _, '_').


/*** tests ********************************************************/


test(program_analysis, program_module_to_xml) :-
   program_module_to_xml(basic_algebra, basics, _).


/******************************************************************/


