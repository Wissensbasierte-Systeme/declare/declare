

/******************************************************************/
/***                                                            ***/
/***         Statistics: Clone-Detection                        ***/
/***                                                            ***/
/******************************************************************/


:- module( spectra, [ ]).


/*** interface ****************************************************/


/* predicate_to_spectrum(?MPA, -Spectrum) <-
      */

predicate_to_spectrum(MPA, Spectrum) :-
   spectra_db:create_spectra_db_if_not_exists,
   spectra_db:spectrum(MPA, Spectrum).


/* spectrum_to_picture(?MPA) <-
      */

spectrum_to_picture(MPA) :-
   spectra_db:basic_mpa_amount(N),
   create_s_picture('Histogram', N, P),
   findall( M,
      spectra_db:spectrum(M, _),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   !,
   member(MPA, MPAs_2),
   spectrum_to_picture(P, N, MPA).


/*** implementation ***********************************************/


/* histogram_interface(+Object) <-
      */

histogram_interface(Object) :-
   get(Object, xml, XML_Node),
   Name_1 := XML_Node@name,
   writelq(Name_1),
   term_to_atom(Name_2, Name_1),
   spectrum_to_picture(Name_2).


/* create_s_picture(+Title, +W_1, -P) <-
      */

create_s_picture(Title, W_1, P) :-
   W_2 is W_1 + 20,
   new(P, picture(Title, size(W_2, 400))),
   send(P, open).


/* spectrum_to_picture(+Picture, +BP_Length, +MPA) <-
      */

spectrum_to_picture(P, Number, MPA) :-
   send(P, clear),
   term_to_atom(MPA, MPA_A),
   send(P, label, MPA_A),
   spectra_db:spectrum(MPA, Histogram),
   send(P, display,
      new(Line, line(0, 0, Number, 0))),
   send(Line, arrows, second),
   checklist( line_to_picture(P),
      Histogram ).

line_to_picture(P, Elt:Y_1) :-
   spectra_db:basic_mpa(Elt, X, _, _),
   Y_2 is (Y_1 * (-2)),
   send(P, display,
      new(_, line(X, 0, X, Y_2))),
   !.
line_to_picture(_, Elt:Y) :-
   writeln('failure':Elt:Y),
   !.


/******************************************************************/


