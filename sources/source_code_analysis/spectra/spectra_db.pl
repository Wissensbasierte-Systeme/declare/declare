

/******************************************************************/
/***                                                            ***/
/***         Statistics: Spectra                                ***/
/***                                                            ***/
/******************************************************************/


:- module( spectra_db, [ ]).


:- dynamic basic_mpa/4,
           spectrum/2.


/*** interface ****************************************************/


/* reset_spectra_db <-
      */

reset_spectra_db :-
   write('reset of spectra-facts ... '),
   retractall( spectrum(_, _) ),
   retractall( basic_mpa(_, _, _, _) ),
   writeln(' sec. ... done.').


/* create_spectra_db_if_not_exists <-
      */

create_spectra_db_if_not_exists :-
   spectrum(_, _),
   !.
create_spectra_db_if_not_exists :-
   create_spectra.


/* create_spectra <-
      */

create_spectra :-
   reset_spectra_db,
   topology_db:create_topology_db_if_not_exists,
   writeln('generating predicate-spectra ... '),
   determine_runtime( spectra_db:mpas_to_spectra ),
   writeln(' sec. ... done.'),
   basic_mpas_to_index_nr.


/* basic_mpa_amount(-N) <-
      */

basic_mpa_amount(N) :-
   findall( MPA,
      basic_mpa(MPA, _, _, _),
      MPAs ),
   length(MPAs, N).


/*** implementation ***********************************************/


/* mpas_to_spectra <-
      */

mpas_to_spectra :-
   findall( Level,
      topology_db:topology(_, Level, _),
      Levels_1 ),
   list_to_ord_set(Levels_1, Levels_2),
   max(Levels_2, Max),
   checklist( mpas_to_spectra(Max),
      Levels_2 ).


/* mpas_to_spectra(+Level_Max, +Current_Level) <-
      */

mpas_to_spectra(Max, Level) :-
   findall( MPA,
      topology_db:topology(MPA, Level, _),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   length(MPAs_2, L),
   write_list(['Determing Spectra of ', L, ' Predicates of Level ',
      Level, '/', Max, ';\n']),
   checklist( mpa_to_spectrum,
      MPAs_2 ).


/* mpa_to_spectrum(+MPA) <-
      */

mpa_to_spectrum(MPA) :-
   spectrum(MPA, _),
   !.
mpa_to_spectrum(MPA) :-
   topology_db:cyclic(MPA, _),
   !,
   topology_db:strong_component(Strong_Component),
   member(MPA, Strong_Component),
   !,
   mpa_to_spectrum_sub(Strong_Component).
mpa_to_spectrum(MPA) :-
   mpa_to_spectrum_sub([MPA]).

mpa_to_spectrum_sub(Strong_Component) :-
   findall( Rule,
      ( member(MPA_A, Strong_Component),
        rar:select(rule, MPA_A, _, Rule) ),
      Rules ),
   findall( MPA_B,
      ( member(Rule, Rules),
        rar:calls_pp(Rule, _, MPA_B),
        not( member(MPA_B, Strong_Component) ) ),
      MPA_Bs ),
   maplist( mpa_to_multiset,
      MPA_Bs, Multisets ),
   append(Multisets, Multiset_1),
   multiset_normalize(Multiset_1, Multiset_2),
   checklist( assert_multiset(Multiset_2),
      Strong_Component ).

assert_multiset(Multiset, MPA) :-
   assert( spectrum(MPA, Multiset) ).


/* mpa_to_multiset(+MPA, -Multiset) <-
      */

mpa_to_multiset(MPA, Multiset) :-
   spectrum(MPA, Multiset),
   !.
mpa_to_multiset((user:(','))/2, []) :-
   !.
mpa_to_multiset(MPA, [MPA:1]) :-
   retract( basic_mpa(MPA, x, y, DC_1) ),
   DC_2 is DC_1 + 1,
   assert( basic_mpa(MPA, x, y, DC_2) ),
   !.
mpa_to_multiset(MPA, [MPA:1]) :-
   assert( basic_mpa(MPA, x, y, 1) ).


/* basic_mpas_to_index_nr <-
      */

basic_mpas_to_index_nr :-
   write('indexing basic predicates ... '),
   findall( (MPA, Calls, Direct_Calls),
      basic_mpa(MPA, _, Calls, Direct_Calls),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   retractall( basic_mpa(_, _, _, _) ),
   iterate_list( spectra_db:assert_basic_mpa,
      0, MPAs_2, _ ),
   writeln(' ... done.').

assert_basic_mpa(N_1, (MPA, Calls, Direct_Calls), N_2) :-
   N_2 is N_1 + 1,
   assert( basic_mpa(MPA, N_1, Calls, Direct_Calls) ).


/******************************************************************/


parse_dislog_sources :-
   rar:dislog_sources_to_tree(Tree_1),
   rar:filter_redundant_paths(Tree_1, Tree_2),
   rar:parse_sources_to_rar_database(Tree_2, _).

create_spectra_db :-
   parse_dislog_sources,
   create_spectra.



