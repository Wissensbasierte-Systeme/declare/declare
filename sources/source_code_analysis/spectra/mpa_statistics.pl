

/******************************************************************/
/***                                                            ***/
/***         Statistics: MPA Histogram Call Statistics          ***/
/***                                                            ***/
/******************************************************************/


:- module( mpa_statistics, [ ]).


/*** interface ****************************************************/


/* histogram_basic_mpa_call_statistics(+alphabetical|calls) <-
      */

histogram_basic_mpa_call_statistics(Mode) :-
   spectra_db:create_spectra_db_if_not_exists,
   spectra_db:basic_mpa_amount(N),
   spectra:create_s_picture('Histogram of Basic Predicate Calls',
      N, P),
   find_basic_mpas(Mode, 0, BPs),
   iterate_list(
      mpa_statistics:add_line_to_picture(P),
      0, BPs, _ ).


/* table_basic_mpa_call_statistics(+alphabetical|calls, +Min) <-
      */

table_basic_mpa_call_statistics(Mode, Min) :-
   spectra_db:create_spectra_db_if_not_exists,
   find_basic_mpas(Mode, Min, BPs),
   length(BPs, L),
   concat_atom(['Number of Basic Predicates having at least ', Min,
      ' Calls: ', L], Label),
   topology:statistic_table(Label,
      ['Basic Predicate', 'Calls'], BPs).


/* table_basic_mpa_call_statistics_ext(+alphabetical|calls, +Min) <-
      */

table_basic_mpa_call_statistics_ext(Mode, Min) :-
   spectra_db:create_spectra_db_if_not_exists,
   find_basic_mpas(Mode, Min, BPs),
   length(BPs, L),
   findall( [BMPA, Calls, Package, Number_3],
      ( member([BMPA, Calls], BPs),
        spectra_db:basic_mpa(BMPA, _, Spectrum, _),
        member(Package:Number_1, Spectrum),
        round(Number_1, 1, Number_2),
        concat_atom([Number_2, ' %'], Number_3) ),
      Lists ),
   list_to_dag:list_of_lists_to_dag(Lists, Tree),
   concat_atom(['Number of Basic Predicates having at least ', Min,
      ' Calls: ', L], Label),
   list_to_dag:tree_to_table(Label,
      ['Basic Predicate', 'Calls', 'Package', 'Percentage'], Tree).


/* mpa_to_package(+unit|module, +MPA, -Package) <-
      */

mpa_to_package(Package, MPA, Packages_4) :-
   spectra_db:create_spectra_db_if_not_exists,
   assign_bmpas_to_package(Package),
   spectra:predicate_to_spectrum(MPA, Spectrum),
   multiset:multiset_sum(Spectrum, Sum),
   maplist( bmpa_to_package,
      Spectrum, Packages_1 ),
   flatten(Packages_1, Packages_2),
   multiset_normalize(Packages_2, Packages_3),
   maplist( divide(Sum),
      Packages_3, Packages_4 ),
   !.
mpa_to_package(_, MPA, []) :-
   spectra_db:basic_mpa(MPA, _, _, _),
   write_list([MPA, ' is a basic predicate!\n']),
   fail.


/* polar_diagram_mpa_to_package(+Package, +Border, +MPA) <-
      */

polar_diagram_mpa_to_package(Package, Border, MPA) :-
   ground(Package),
   ground(Border),
   packages_to_multiset(Package, All_Names),
   mpa_to_package(Package, MPA, Multiset),
   ord_union(All_Names, Multiset, Packages_1),
   multiset_normalize(Packages_1, Packages_2),
   maplist( remove_prefix('sources/'),
      Packages_2, Packages_3 ),
   sublist( is_lager_than(Border),
      Packages_3, Packages_4 ),
   maplist( add_percentage_as_atom,
      Packages_4, Packages_5 ),
   term_to_atom(MPA, Atom),
   concat_atom(['Associations of Predicate "', Atom,'" to ',
      Package, 's with more than ', Border, ' \\%'], Label),
   polar_diagram:multiset_to_polar_diagram(
      Label, red, Packages_5).


/*** implementation ***********************************************/


/* packages_to_multiset(+Scale, -Multiset) <-
      */

packages_to_multiset(Scale, Multiset) :-
   rar:package_names(Scale, Names),
   maplist( transform_1(0),
      Names, Multiset).

transform_1(A, B, B:A).


/* assign_bmpas_to_package(+unit|module) <-
      */

assign_bmpas_to_package(Package) :-
   sca_variable_get(bmpas_package, Package).
assign_bmpas_to_package(Package) :-
   find_basic_mpas(alphabetical, 0, BMPAs),
   checklist( assign_bmpa_to_packages(Package),
      BMPAs ),
   sca_variable_set(bmpas_package, Package).


/* assign_bmpa_to_packages(+Package, +[BMPA, Calls]) <-
      */

assign_bmpa_to_packages(_, [(user:(','))/2, _]) :-
   !.
assign_bmpa_to_packages(Package, [BMPA, Calls]) :-
   retract( spectra_db:basic_mpa(BMPA, No, _, Calls) ),
   findall( Name,
      ( rar:calls_pp_inv(BMPA, File, Calls_BMPA),
        not( BMPA = Calls_BMPA ),
        rar:file_to_package(File, Package:Name) ),
      Names ),
   list_to_multiset(Names, Multiset),
   multiset:multiset_sum(Multiset, Sum),
   maplist( normalize(Sum),
     Multiset, Package_Belonging ),
   assert(
      spectra_db:basic_mpa(BMPA, No, Package_Belonging, Calls) ),
   !.
assign_bmpa_to_packages(_Package, (BMPA, _Calls)) :-
   writeln(BMPA).

normalize(Sum, A:B, A:C) :-
   C is (B/Sum)*100.


/* find_basic_mpas(+alphabetical|calls, -BPs) <-
      */

find_basic_mpas(alphabetical, Min, BPs_3) :-
   integer(Min),
   findall( (MPA, MPA, DC),
      ( spectra_db:basic_mpa(MPA, _, _, DC),
        DC >= Min ),
      BPs_1 ),
   list_to_ord_set(BPs_1, BPs_2),
   maplist( transform_2,
      BPs_2, BPs_3 ),
   !.
find_basic_mpas(calls, Min, BPs_4) :-
   integer(Min),
   findall( (DC, MPA, DC),
      ( spectra_db:basic_mpa(MPA, _, _, DC),
        DC >= Min ),
      BPs_1 ),
   list_to_ord_set(BPs_1, BPs_2),
   reverse(BPs_2, BPs_3),
   maplist( transform_2,
      BPs_3, BPs_4 ).

transform_2((_, B, C), [B, C]).


/* add_line_to_picture(+Pic, +X_1, +(_, MPA, Calls), -X_2) <-
      */

add_line_to_picture(Pic, X_1, [_, Y], X_2) :-
   add_line_to_picture(Pic, (X_1, Y)),
   X_2 is X_1 + 1.

add_line_to_picture(P, (X, Y_0)) :-
   Y_1 is (Y_0 * (-1)),
   send(P, display,
      new(_, line(X, 0, X, Y_1))),
   !.
add_line_to_picture(_, (X, _)) :-
   writeln('failure':X).


/* bmpa_to_package(+BMPA:Number, -Packages) <-
      */

bmpa_to_package(BMPA:Number, Packages_2) :-
   spectra_db:basic_mpa(BMPA, _, Packages_1, _),
   maplist( multiply(Number),
      Packages_1, Packages_2 ).

multiply(A, P:B, P:C) :-
   C is A*B.

divide(A, P:B, P:C) :-
   C is B/A.


/* is_lager_than(+Border, +_:B) <-
      */

is_lager_than(Border, _:B) :-
   B >= Border.


/* add_percentage_as_atom(+A_1:B_1, -A_5:B_1) <-
      */

add_percentage_as_atom(A_1:B_1, A_5:B_1) :-
   round(B_1, 1, B_2),
   name(A_1, A_2),
   name(B_2, B_3),
   append(A_2, [58, 32|B_3], A_3),
   append(A_3, [37], A_4),
   name(A_5, A_4).


/******************************************************************/


