

/******************************************************************/
/***                                                            ***/
/***         Statistics: Spectrum Save and Load                 ***/
/***                                                            ***/
/******************************************************************/


:- module( spectra_save_and_load, []).


/*** interface ****************************************************/


/* load_spectra_facts(+File) <-
      */

load_spectra_facts(File_1) :-
   tilde_to_home_path(File_1, File_2),
   reset_spectra_db,
   open(File_2, read, Stream),
   read(Stream, T0),
   asserted_stream_content(T0, Stream),
   close(Stream).


/* save_spectra_facts(+File) <-
      */

save_spectra_facts(File_1) :-
   tilde_to_home_path(File_1, File_2),
   predicate_to_file( File_2,
      spectra_save_and_load:write_spectra_facts ),
   !.


/*** implementation ***********************************************/


/* asserted_stream_content(+Term, +Stream) <-
      */

asserted_stream_content(end_of_file, _) :-
   !.
asserted_stream_content(basic_mpa(A, C, D, E), Stream) :-
   !,
   assert( spectra_db:basic_mpa(A, C, D, E) ),
   read(Stream, T2),
   rar:asserted_stream_content(T2, Stream).
asserted_stream_content(spectrum(A, B), Stream) :-
   !,
   assert( spectra_db:spectrum(A, B) ),
   read(Stream, T2),
   rar:asserted_stream_content(T2, Stream).


/* write_spectra_molecules <-
      */

write_spectra_facts :-
   forall( spectra_db:basic_mpa(A, C, D, E),
      rar:write_term_with_dot(
         basic_mpa(A, C, D, E)) ),
   forall( spectra_db:spectrum(A, B),
      rar:write_term_with_dot(
         spectrum(A, B)) ).


/******************************************************************/


