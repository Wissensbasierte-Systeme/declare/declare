

/******************************************************************/
/***                                                            ***/
/***        01.06.2008                                          ***/
/***                                                            ***/
/******************************************************************/


:- module( further, [
      ]).


/*** interface ****************************************************/


/* call_spreading(+Level, -Call_Spreadings) <-
      */

call_spreading(Level, Call_Spreadings) :-
   rar:package_names(Level, Paths),
   findall( Path:Call_Spreading,
      ( member(Path, Paths),
        outgoing_calls(Level:Path, Call_Spreading) ),
      Call_Spreadings ).


/* outgoing_calls(+Package, -Multiset) <-
      */

outgoing_calls(Level:Path_1, Multiset) :-
   rar:package_to_mpas(Level:Path_1, Ps),
   findall( Path_2,
      ( member(P_1, Ps),
        rar:calls_pp(P_1, P_2),
        rar:contains_lp(Level:Path_2, P_2),
        not( Path_1 = Path_2 ) ),
      Calls ),
   list_to_multiset(Calls, Multiset).


/******************************************************************/


