

/******************************************************************/
/***                                                            ***/
/***          Slicing:  Tests                                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(source_code_analysis:slice, necessary_files) :-
   rar:init(sca_tests_1),
   sca_variable_get(rar_test_files, [Cross, Desc, Necess, _]),
   slice:necessary_files(
      module:'sources/source_code_analysis/rar/test_2',
      Files, MPAs, Iterations),
   slice_test_output_2(Files, MPAs, Iterations),
   Result_1 = [ (rar:rar_search)/3,
      (rar:transitive_closure)/3,
      (user: (:=))/2,
      (user:append)/2,
      (user:in_same_module)/2,
      (rar:rar_filename)/2,
      (user:rar_predicate_to_descendant)/2,
      (user:rar_predicate_to_descendants)/2,
      (user:rar_predicate_to_files)/2,
      (user:rar_predicate_to_necessary_files)/2,
      (user:rar_search)/3],
   subtract(MPAs, Result_1, A),
   subtract(Result_1, MPAs, B),
   slice:slice_to_tests(MPAs, Test_Calls, Called_MPAs),
   nl,
   length(Test_Calls, Test_Amount),
   write(Test_Amount),
   writeln(' Tests, which call predicates in this slice:'),
   writeln_list([''|Test_Calls]),
   nl,
   writeln('Called predicates of the tests:'),
   writeln_list([''|Called_MPAs]),
   A = [],
   B = [],
   Files = [Cross, Desc, Necess],
   Iterations = 2.


/*** implementation ***********************************************/


slice_test_output_2(Files, Predicates, Iterations) :-
   length(Files, F),
   length(Predicates, P),
   writeln_list([
      'Iterations':Iterations,
      'Files':F, Files,
      'Predicates':P, Predicates ]).


/******************************************************************/


