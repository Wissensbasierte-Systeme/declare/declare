

/******************************************************************/
/***                                                            ***/
/***          RAR:  Global Variables                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* find_possible_meta_predicates(-MPAs) <-
      */

find_possible_meta_predicates(MPAs_4) :-
   alias_to_file(predicate_groups, SP_File),
   rar:assert_predicate_groups(SP_File),
   findall( MPA,
      find_possible_meta_predicate_1(MPA),
      MPAs_1 ),
   findall( MPA,
      find_possible_meta_predicate_2(MPA),
      MPAs_2 ),
   intersection(MPAs_1, MPAs_2, MPAs_3),
   list_to_ord_set(MPAs_3, MPAs_4),
   length(MPAs_4, L),
   writeln(L).


/* find_possible_meta_predicate_1(-MPA) <-
      */

find_possible_meta_predicate_1((M:P)/A) :-
   rar:select(rule, _MPA, _File, Rule),
   Functor := Rule^body^atom::[
      @module=M, @predicate=P, @arity=A]^term@functor,
   not( rar:special_predicate((M:P)/A, _, _, _, _) ),
   rar:select(rule, (_:Functor)/_, _, _).


/* find_possible_meta_predicate_2(-MPA) <-
      */

find_possible_meta_predicate_2(MPA) :-
   rar:select(rule, MPA, _File, Rule),
   [user, call, 1] := Rule^body^atom@[module, predicate, arity],
   not( rar:special_predicate(MPA, _, _, _, _) ).


/* twin_predicates_to_files_s(+Predicates, +Twin_Predicates,
      -Files) <-
      */

twin_predicates_to_files_s(Predicates, Twin_Predicates,
      Files) :-
   findall( Fs,
      ( member(Twin_Predicate, Twin_Predicates),
        twin_predicate_to_files(Predicates, Twin_Predicate, Fs) ),
      Fs_T ),
   ord_union_2(Fs_T, Files).


/* files_defining_mpa_property_in_slice(
      +N_Files, +(Bool, dynamic|multifile|discontiguous),
      -Files) <-
      */

files_defining_mpa_property_in_slice(
      N_Files, (Bool, Mode, Flag),  Files) :-
   ( Bool = true ->
     files_defining_mpa_property(Mode, Flag, N_Files, Files)
   ; Files = [] ).


/* find_files_containing_current_num(+Predicates, -Files) <-
      */
find_files_containing_current_num(false, _, []) :-
   !.
find_files_containing_current_num(true, Predicates, Files) :-
   find_files_containing_current_num(Predicates, Files),
   !.

find_files_containing_current_num(Predicates, Files) :-
   mpa_to_variables(Predicates, (user:current_num)/2, Variables),
   findall( MPA_1,
      ( member(P, [assert, asserta, assertz]),
        rar:calls_pp_with_variables(
           _, MPA_1, _, (user:P)/1, [current_num]) ),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   findall( Fs,
      ( member(MPA_2, MPAs_2),
        mpa_with_variables_to_files(MPA_2, Variables, Fs) ),
      Files_1 ),
   append(Files_1, Files_2),
   sort(Files_2, Files).


/* directive_mpas(+true|false, +Necessary_MPAs, +Necessary_Files,
      -MPAs, -Files) <-
      */

directive_mpas(false, _, _, [], []) :-
   !.
directive_mpas(true, Necessary_MPAs, Necessary_Files,
      MPAs, Files) :-
   directive_mpas_s(Necessary_MPAs, Necessary_Files,
      Necessary_Files, MPAs, Files),
   !.


/*** implementation ***********************************************/


/* files_defining_mpa_property(
      +dynamic|multifile|discontiguous, +Slice_Files, -Files) <-
      */

files_defining_mpa_property(b, Flag, Slice_Files, Files_2) :-
   statistics:find_mpa_property(Flag, Slice_Files, DM_MPAs),
   findall( File,
      ( member(MPA, DM_MPAs),
        rar:select(rule, MPA, File, _) ),
      Files_1 ),
   list_to_ord_set(Files_1, Files_2),
   !.
files_defining_mpa_property(a, Flag, Slice_Files, Files_2) :-
   statistics:find_mpa_property(Flag, MPA_Files),
   findall( File,
      ( member(MPA-File, MPA_Files),
        is_mpa_called_in_files(MPA, Slice_Files) ),
      Files_1 ),
   list_to_ord_set(Files_1, Files_2),
   !.


/* is_mpa_called_in_files(+MPA, +Files) <-
      */

is_mpa_called_in_files(MPA_2, Files) :-
   rar:calls_pp_inv(MPA_2, MPA_1),
   rar:contains_lp(file:File, MPA_1),
   not( rar:ignore_calls_pp(MPA_1) ),
   member(File, Files).


/* twin_predicate_to_files(+Predicates, +Twin_Pair, -Files) <-
      */

twin_predicate_to_files(Predicates, Get-Set, Files) :-
   twin_predicate_definition_to_files(Get-Set, Twin_Files),
   mpa_to_variables(Predicates, Get, Get_Variables_in_Slice),
   mpa_to_variables(Predicates, Set, Set_Variables_in_Slice),
   subtract(
      Get_Variables_in_Slice, Set_Variables_in_Slice, Variables),
   mpa_with_variables_to_files(
      Set, Variables, Twin_Variable_Files),
   ord_union_2(Twin_Files, Twin_Variable_Files, Files_2),
   list_to_ord_set(Files_2, Files).


/* twin_predicate_definition_to_files(+Get-Set, -Files) <-
      */

twin_predicate_definition_to_files(Get-Set, Files) :-
   twin_predicate_definition_to_files_step(Get, Files_Get),
   twin_predicate_definition_to_files_step(Set, Files_Set),
   ord_union_2(Files_Get, Files_Set, Files).

twin_predicate_definition_to_files_step(MPA, Files) :-
   findall( File,
      rar:select(rule, MPA, File, _),
      Files_2 ),
   list_to_ord_set(Files_2, Files).


/* mpa_with_variables_to_files(+MPA, +Variables, -Files) <-
      */

mpa_with_variables_to_files(MPA, Variables, Files) :-
   findall( File,
      ( member(Variable, Variables),
        mpa_with_variable_to_file(MPA, Variable, File) ),
      Files_1 ),
   list_to_ord_set(Files_1, Files).


/* mpa_with_variable_to_file(+MPA, ?Variable, ?File) <-
      */

mpa_with_variable_to_file((M:P)/A, Variable, File) :-
   rar:calls_pp_inv((M:P)/A, File, _),
   rar:select(rule, _, File, Rule),
   Atom := Rule^body^atom::[
      @module=M, @predicate=P, @arity=A],
   Variable := Atom-nth(1)^term@functor.


/* mpa_to_variables(+Predicates, +MPA, -Variables) <-
      */

mpa_to_variables(MPAs, MPA_2, Variables) :-
   findall( Variable,
      ( member(MPA_1, MPAs),
        rar:calls_pp_with_variables(_, MPA_1,
           _, MPA_2, [Variable, _]) ),
      Variables_1 ),
   list_to_ord_set(Variables_1, Variables).


/* directive_mpas_s(+Necessary_MPAs, +Necessary_Files,
      +Added_Files, -MPAs, -Files) <-
      */

directive_mpas_s(Necessary_MPAs, Necessary_Files,
      Added_Files, MPAs, Files) :-
   findall( MPA,
      ( member(File, Added_Files),
        rar:select(rule, Directive_MPA, File, Rule),
        rar:is_directive(Directive_MPA),
        rar:calls_pp(Rule, Directive_MPA, MPA) ),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   subtract(MPAs_2, Necessary_MPAs, MPAs_3),
   directive_mpas_sub(Necessary_MPAs, Necessary_Files,
      MPAs_3, MPAs, Files).

directive_mpas_sub(_, _, [], [], []) :-
   !.
directive_mpas_sub(Necessary_MPAs, Necessary_Files,
      Added_MPAs, MPAs, Files) :-
   mpas_to_necessary_mpas_and_files(
      Added_MPAs, MPAs_1, Files_1),
   ord_union_2(MPAs_1, Necessary_MPAs, MPAs_2),
   ord_union_2(Files_1, Necessary_Files, Files_2),
   directive_mpas_s(MPAs_2, Files_2, Files_1,
      MPAs_New, Files_New),
   ord_union_2(MPAs_1, MPAs_New, MPAs),
   ord_union_2(Files_1, Files_New, Files).


/******************************************************************/


