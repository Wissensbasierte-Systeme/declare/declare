

/******************************************************************/
/***                                                            ***/
/***          Slicing:  System Methods                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* determine_consulting_order(+Files_1, -Files_2) <-
      */

determine_consulting_order(Files_1, Files_4) :-
   sca_variable_get(source_tree, Source_Tree),
   rar:tree_to_files(Source_Tree, Ordered_Files),
   intersection(Ordered_Files, Files_1, Files_2),
   subtract(Files_1, Ordered_Files, Missing_Files),
   append(Files_2, Missing_Files, Files_3),
   nl,
   length(Missing_Files, ML),
   write(ML),
   writeln(' files could not be found in the pattern of the files, which are ordered by consulting!'),
   writeln('They are appended at the end of the consulting list.'),
   list_to_set(Files_3, Files_4).


/* slice_package(
      +Dynamics, +Multifiles, +Discontigious,
      +Slice_Files, +MPAs, +Libraries) <-
      */

slice_package(Dynamic, Multifile, Discontigious,
      Slice_Files_01, _MPAs, Libraries) :-
   slice_package_prepare(Slice_Dir, Library_Dir),
   dislog_variable_get(home, DisLog),
   !,
   add_slash_suffix(DisLog, DisLog_Path),
   sca_variable_get(source_tree, Tree),
   Root := Tree@root,
   subtract(Slice_Files_01, Libraries, Slice_Files_1),
   maplist( concat(Root),
      Slice_Files_1, Slice_Files_2 ),
   maplist( copy_file_to_file(DisLog_Path, Slice_Dir),
      Slice_Files_2, Target_Files ),
%  tar_files(Slice_Dir, Slice_Files),
   ( foreach(Library, Libraries) do
        copy_file_to_file(Library, Library_Dir) ),
   sca_variable_get(sca, SCA),
   concat(SCA, 'slice/slice_additional.pl', Slice_Add_1),
   concat(Slice_Dir, '/slice_additional.pl', Slice_Add_2),
   copy_file_to_file(Slice_Add_1, Slice_Add_2),
   concat(Slice_Dir, '/slice_package.pl', Slice_File),
   predicate_to_file( Slice_File,
      ( slice:slice_header(Dynamic, Multifile, Discontigious),
        slice:write_consult_line_for_slicing(
           'slice_additional.pl'),
        checklist( slice:write_consult_line_for_slicing,
           Target_Files ) ) ),
   write_browser_hierarchy_1(Target_Files, Slice_Dir),
   write_browser_hierarchy_2(Slice_Dir, Target_Files),
   nl,
   writeln('Slice written to directory:'),
   writeln(Slice_Dir).
%  concat(Slice_Dir, '/all_rules.pl', All_Rules_File),
%  write_all_rules_to_one_file(Dynamic, Multifile, Discontigious,
%     Slice_Files, MPAs, All_Rules_File).


/* write_all_rules_to_one_file(+Dynamic, +Multifile, +Discontigious,
      +Files, +MPAs, +File) <-
      */

write_all_rules_to_one_file(Dynamic, Multifile, Discontigious,
      Files, MPAs, File) :-
   files_and_mpas_to_necessary_rule(Files, MPAs, Rules),
   predicate_to_file( File,
      ( slice:slice_header(Dynamic, Multifile, Discontigious),
        rar:xml_rules_to_prolog(Rules) ) ).



files_and_mpas_to_necessary_rule(Files, MPAs, Rules) :-
   ( foreach(File, Files),
     foreach(Rules_1, Rules_2) do
        findall( Rule,
           ( rar:select(rule, MPA, File, Rule),
             member(MPA, MPAs) ),
           Rules_1 ) ),
    append(Rules_2, Rules).


/*** implementation ***********************************************/


/* slice_package_prepare(Slice_Dir, Library_Dir) <-
      */

slice_package_prepare(Slice_Dir, Library_Dir) :-
   sca_variable_get(slice, Directory),
   make_directory_recursive(Directory),
   tmp_file(slice, '', File),
   concat_atom([Directory, '/', File], Slice_Dir),
   make_directory_recursive(Slice_Dir),
   concat(Slice_Dir, '/library', Library_Dir),
   make_directory_recursive(Library_Dir).


/* copy_file_to_file(Directory, Source_File, Target_File) <-
      */

copy_file_to_file(DisLog_Source_Path,
      Directory, Source_File_1, Target_File) :-
   tilde_to_home_path(Source_File_1, Source_File_2),
   concat(DisLog_Source_Path, Target_File, Source_File_2),
   concat([Directory, '/', Target_File], Target_Path),
   copy_file_to_file(Source_File_2, Target_Path).

copy_file_to_file(File_1, File_2) :-
   file_directory_name(File_2, Directory),
   make_directory_recursive(Directory),
   concat(['cp ', File_1, ' ', File_2], Command),
   shell(Command).


/* slice_header(+Dynamic, +Multifile, +Discontigious) <-
      */

slice_header(Dynamic, Multifile, Discontigious) :-
   slice_write(dynamic, Dynamic),
   slice_write(multifile, Multifile),
   slice_write(discontiguous, Discontigious).

slice_write(_, []).
slice_write(Type, Elts) :-
   write(':- '),
   write(Type),
   write(' '),
   slice_write(Elts).

slice_write([(M:P)/A]) :-
   write(M:P/A),
   writeln('.').
slice_write([(M:P)/A|Es]) :-
   write(M:P/A),
   write(', '),
   slice_write(Es).
slice_write([]).


/* write_browser_hierarchy_1(Target_Files, Slice_Dir) <-
      */

write_browser_hierarchy_1(Target_Files, Slice_Dir) :-
   ( foreach(Target_File, Target_Files),
     foreach(XML_File, XML_Files) do
        concat_atom([Slice_Dir, '/', Target_File], File_Path),
        XML_File = file:[name:File_Path]:[] ),
   concat(Slice_Dir, '/slice_hierarchy_1.xml', XML_Hierarchy),
   dwrite(xml_2, XML_Hierarchy, module:[name:'slice']:XML_Files).


/* write_consult_line_for_slicing(File) <-
      */

write_consult_line_for_slicing(File) :-
   write(':- load_files('),
   writeq([File]),
   writeln(', [silent(true)]).').
%  write(':- consult('), writeq(File), writeln(').').
%  rar_saving:write_term_with_dot(
%     :-  consult(File) ).


/* write_browser_hierarchy_2(Slice_Dir, Slice_Files) <-
      */

write_browser_hierarchy_2(Slice_Dir, Slice_Files) :-
   slice_to_source_hierarchy(Slice_Files, Tree),
   concat(Slice_Dir, '/slice_hierarchy_2.xml', XML_Hierarchy),
   !,
   dwrite(xml_2, XML_Hierarchy, Tree).


/* slice_to_source_hierarchy(Slice_Files, Tree) <-
      */

slice_to_source_hierarchy(Slice_Files, Tree) :-
   findall( unit:[name:Unit]:Terms,
      ( dislog_unit(Unit, Modules),
        maplist( rar_module_to_fn_term(Slice_Files),
           Modules, Terms ) ),
      Units ),
   Tree = system:[name:dislog]:Units.

rar_module_to_fn_term(Slice_Files, Module, Term) :-
   dislog_module(Module, _, Files),
   maplist( correct_path,
      Slice_Files, S_Files ),
   intersection(Files, S_Files, H_Files),
   maplist( rar_file_to_fn_term,
      H_Files, Terms ),
   Term = module:[name:Module]:Terms.

rar_file_to_fn_term(File, file:[name:Path, path:Path]:[]) :-
   dislog_variable_get(source_path, Sources),
   !,
   concat(Sources, File, Path_2),
   ( ( exists_file_with_tilde(Path_2),
       Path = Path_2,
       ! )
   ; file_name_extension(Path_2, pl, Path) ),
   !.

correct_path(A, B) :-
   concat('sources/', B, A),
   !.
correct_path(A, A).


/* tar_files(Source, Source_Files) <-
      */

tar_files(Source, Source_Files) :-
   file_name_extension(Source, tgz, Tgz_File),
   maplist( add_space,
      Source_Files, Files_with_Space ),
   concat(Files_with_Space, Files),
   concat(['tar czf ', Tgz_File, ' ', Files], Command),
   writeq(Command).
%  shell(Command).

add_space(X, Y) :-
   concat_atom([X, ' '], Y).


/******************************************************************/


