

/******************************************************************/
/***                                                            ***/
/***          Slicing:  Examples                                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


example(slicing, init) :-
   rar:init(dislog).

example(slicing, tsv) :-
   slice:necessary_files(
      predicate:(user:generate_tsv_website)/0,
      Files, Preds, Iterations),
   slice_test_output_2(Files, Preds, Iterations).

example(slicing, module_slicing) :-
   slice:necessary_files(
      module:slicing, Files, Preds, Iterations),
   slice_test_output_2(Files, Preds, Iterations).

example(slicing, predicate_slice_7) :-
   slice:necessary_files(
      predicate:(slice:slice)/11, Files, Preds, Iterations),
   slice_test_output_2(Files, Preds, Iterations).

example(slicing, module_rar) :-
   slice:necessary_files(
      module:rar, Files, Preds, Iterations),
   slice_test_output_2(Files, Preds, Iterations).

example(slicing, file_fn_query) :-
   slice:necessary_files(
      file:fn_query, Files, Preds, Iterations),
   slice_test_output_2(Files, Preds, Iterations).

example(slicing, file_mine_sweeper_board) :-
   slice:necessary_files(
      file:mine_sweeper_board, Files, Preds, Iterations),
   slice_test_output_2(Files, Preds, Iterations).

example(slicing, predicate_user_create_board_0) :-
   slice:necessary_files(
      predicate:(user:create_board)/0, Files, Preds, Iterations),
   slice_test_output_2(Files, Preds, Iterations).
example(slicing, predicate_user_fnquerry_2) :-
   slice:necessary_files(
      predicate:(user:(:=))/2, Files, Preds, Iterations),
   slice_test_output_2(Files, Preds, Iterations).


example(slicing, generate_tsv_website) :-
   Exclude = [file:[path:'sources/development/jaml/jaml_files.pl']:[]],
   Include = [file:[path:'library/loops.pl']:[]],
   Libraries = [file:[path:'library/static_swi.pl']:[],
                file:[path:'library/lists_swi.pl']:[]],
   Slice = slice:[multi:false, dyn:false, discont:false,
         twins:false, current_num:false, directives:false,
         tests:false,
         multi_2:false, discont_2:false, consults:false]:[
      predicates:[type:include]:[
         atom:[module:user,
               predicate:generate_tsv_website, arity:0]:[]],
      predicates:[type:exclude]:[],
      predicates:[type:dynamic]:[],
      predicates:[type:multifile]:[],
      predicates:[type:discontiguous]:[],
      files:[type:include]:Include,
      files:[type:libraries]:Libraries,
      files:[type:exclude]:Exclude ],
   determine_runtime( slice:slice(Slice, Files, Preds),
      RT ),
   nl,
   writeln('Runtime of determining the slice':RT),
   slice_test_output_1(Files, Preds).

example(slicing, dread) :-
%  Exclude = [file:[path:'library/sgml.pl']:[]],
   Exclude = [],
   Include = [file:[path:'library/loops.pl']:[]],
   Libraries = [file:[path:'library/static_swi.pl']:[],
                file:[path:'library/lists_swi.pl']:[]],
   Slice = slice:[multi:false, dyn:false, discont:false,
         twins:false, current_num:false, directives:false,
         tests:false,
         multi_2:false, discont_2:false, consults:false]:[
      predicates:[type:include]:[
         atom:[module:user,
               predicate:dread, arity:3]:[],
         atom:[module:user,
               predicate:dwrite, arity:3]:[],
         atom:[module:user,
               predicate:'<=', arity:2]:[]],
      predicates:[type:exclude]:[],
      predicates:[type:dynamic]:[
         atom:[module:user,
               predicate:test, arity:2]:[]],
      predicates:[type:multifile]:[
         atom:[module:user,
               predicate:test, arity:2]:[]],
      predicates:[type:discontiguous]:[],
      files:[type:include]:Include,
      files:[type:libraries]:Libraries,
      files:[type:exclude]:Exclude ],
   determine_runtime( slice:slice(Slice, Files, Preds),
      RT ),
   nl,
   writeln('Runtime of determining the slice':RT),
   slice_test_output_1(Files, Preds).

example(slicing, example) :-
   Slice = slice:[multi:false, dyn:false, discont:false,
         twins:false, current_num:false, directives:false,
         tests:true]:[
      predicates:[type:include]:[
         atom:[module:user,
               predicate:writelq, arity:1]:[] ],
      predicates:[type:exclude]:[],
      predicates:[type:dynamic]:[],
      predicates:[type:multifile]:[],
      predicates:[type:discontiguous]:[],
      files:[type:include]:[],
      files:[type:libraries]:[],
      files:[type:exclude]:[] ],
   determine_runtime( slice:slice(Slice, Files, Preds),
      RT ),
   nl,
   writeln('Runtime of determining the slice':RT),
   slice_test_output_1(Files, Preds).


example(slicing, d3_diagnostic_reasoning) :-
   dislog_variable_get(home, DisLog_Home),
   !,
   Source_Files = [
      '/library/loops.pl',                         % because of 'foreach'
      '/sources/basic_algebra/basics/operators'    % because of '~'
   ],
   maplist( concat(DisLog_Home),
      Source_Files, [S_File_1, S_File_2] ),
   concat(DisLog_Home, '/library/static_swi.pl', File_3),
   Exclude_1 = [
      '/library/sgml.pl',
      '/library/lists_swi.pl',
      '/sources/databases/squash/squash_parser',
      '/sources/databases/squash/squash_and_sql',
      '/sources/xml/fn_case_studies/stock',
      '/sources/stock_tool/stock_gui/gui'],
   findall( file:[path:File_2]:[],
      ( member(File_1, Exclude_1),
        concat(DisLog_Home, File_1, File_2) ),
      Exclude_2 ),
   Slice = slice:[multi:false, dyn:false, discont:false,
         twins:false, current_num:false, directives:false,
         tests:true]:[
      predicates:[type:include]:[
         atom:[module:user,
                  predicate:d3_diagnostic_reasoning, arity:0]:[],
         atom:[module:user, % file: '/sources/projects/d3/d3_dialogs' -> because of 'Goal = d3_condition(Type, Id, Value).' in projects/d3/d3_knowledge_base_to_rules
                  predicate:d3_condition, arity:3]:[] ],
      predicates:[type:exclude]:[
         atom:[module:user,
                  predicate:dread_, arity:1]:[],
         atom:[module:user,
                  predicate:dread_, arity:2]:[],
         atom:[module:user,
                  predicate:dread_, arity:3]:[] ],
      predicates:[type:dynamic]:[
         atom:[module:user,
                  predicate:test, arity:2]:[] ],
      predicates:[type:multifile]:[
         atom:[module:user,
                  predicate:test, arity:2]:[] ],
      predicates:[type:discontiguous]:[],
      files:[type:include]:[
         file:[path:S_File_1]:[],
         file:[path:S_File_2]:[] ],
      files:[type:libraries]:[
         file:[path:File_3]:[] ],
      files:[type:exclude]:Exclude_2 ],
   measure_runtime(slice:d3_diagnostic_reasoning,
      slice:slice(Slice, Files, Preds) ),
   slice_test_output_1(Files, Preds).
/*
example(slicing, mine_sweeper) :-
   Predicates = [ (user:create_board)/0,
%     Buttons:
      (user:new_game)/0, (user:clear_board)/0,
      (user:show_board)/0,
      (user:mine_sweeper_decision_support)/1,
      (user:pp_ratio_information)/0 ],
   concat('/sources/reasoning',
      '/disjunctive_consequence_operators/tps_delta', Tps_Delta),
   Fs = [
      '/library/loops.pl',
      '/library/ordsets.pl',
      '/library/lists_sicstus.pl',
      '/sources/basic_algebra/basics/specials_swi',
      '/sources/basic_algebra/basics/operators', Tps_Delta,
      '/sources/reasoning/nmr_interfaces/smodels_interface',
      '/sources/basic_algebra/utilities/test_predicates' ],
   dislog_variable_get(home, DisLog),
   !,
   maplist( concat(DisLog),
      Fs, Extra_Files ),
   concat(DisLog, '/library/static_swi.pl', Library),
   Dynamic = [(user:test)/2],
   Multifile = [(user:test)/2],
   measure_runtime(slice:mine_sweeper,
      slice:slice(Predicates, Dynamic, Multifile,
         Extra_Files, [Library], [], Files, Preds) ),
   slice_test_output_1(Files, Preds).

example(slicing:case_study, field_notation) :-
   Predicates = [
      (user:(:=))/2, (user:add)/2, (user:add)/3,
      (user:average)/2, (user:dwrite)/2 ],
   Fs = ['/library/lists_sicstus.pl', '/library/loops.pl'],
   dislog_variable_get(home, DisLog),
   !,
   maplist( concat(DisLog),
      Fs, Extra_Files ),
   Dynamic = [(user:test)/2],
   Multifile = [(user:test)/2],
   measure_runtime(slice:field_notation, slice:slice(
      Predicates, Dynamic, Multifile,
      Extra_Files, [], [], Files, Preds) ),
   slice_test_output_1(Files, Preds).
example(slicing:case_study, example) :-
   Predicates = [ (user:test)/2-[field_notation:_, _] ],
   Fs = ['/library/lists_sicstus.pl', '/library/loops.pl'],
   dislog_variable_get(home, DisLog),
   !,
   maplist( concat(DisLog),
      Fs, Extra_Files ),
   Dynamic = [(user:test)/2],
   Multifile = [(user:test)/2],
   measure_runtime(slice:test, slice:slice(
      Predicates, Dynamic, Multifile,
      Extra_Files, [], [], Files, Preds) ),
   slice_test_output_1(Files, Preds).
example(slicing:case_study, slice) :-
   Predicates = [(slice:slice)/8],
%     (slice:rar_transitive_closure)/2
%     (slice:slice_package)/4,
%     (slice:necessary_files)/2,
%     (slice:rar_transitive_closure)/2,
%     (user:rgb_to_colour)/2
   slice:slice(
      Predicates, [], [],
      [], [], [], Files, Preds),
   slice_test_output_1(Files, Preds).
*/


example(slicing, bonengel_1) :-
%    alias_to_file(db_files, DB_File),
%    rar:load_rar_database(DB_File),
   Slice =
      slice:[multi:false, dyn:false, discont:false,
         twins:false, current_num:false, directives:true]:[
      predicates:[type:include]:[
         atom:[module:user,
               predicate:fng_transformation_for_gui_new, arity:2]:[] ],
      predicates:[type:exclude]:[],
      predicates:[type:dynamic]:[],
      predicates:[type:multifile]:[],
      predicates:[type:discontiguous]:[],
      files:[type:include]:[],
      files:[type:libraries]:[],
      files:[type:exclude]:[] ],
   measure_runtime(slice:bonengel,
      slice:slice(Slice, Files, Preds) ),
%  writeln_list(Preds),
   slice_test_output_1(Files, Preds).

example(slicing, bonengel_2) :-
%    alias_to_file(db_files, DB_File),
%    rar:load_rar_database(DB_File),
   Slice =
      slice:[multi:true, dyn:true, discont:true,
         twins:true, current_num:true, directives:true]:[
      predicates:[type:include]:[
         atom:[module:user,
               predicate:fn_select_for_gui_new, arity:2]:[] ],
      predicates:[type:exclude]:[],
      predicates:[type:dynamic]:[],
      predicates:[type:multifile]:[],
      predicates:[type:discontiguous]:[],
      files:[type:include]:[],
      files:[type:libraries]:[],
      files:[type:exclude]:[] ],
   measure_runtime(slice:bonengel,
      slice:slice(Slice, Files, Preds) ),
%  writeln_list(Preds),
   slice_test_output_1(Files, Preds).


example(slicing, bonengel_3) :-
%    alias_to_file(db_files, DB_File),
%    rar:load_rar_database(DB_File),
   Slice =
      slice:[multi:true, dyn:true, discont:true,
         twins:true, current_num:true, directives:true]:[
      predicates:[type:include]:[
         atom:[module:user,
               predicate:dread_, arity:3]:[] ],
      predicates:[type:exclude]:[],
      predicates:[type:dynamic]:[],
      predicates:[type:multifile]:[],
      predicates:[type:discontiguous]:[],
      files:[type:include]:[],
      files:[type:libraries]:[],
      files:[type:exclude]:[] ],
   measure_runtime(slice:bonengel,
      slice:slice(Slice, Files, Preds) ),
%  writeln_list(Preds),
   slice_test_output_1(Files, Preds).


example_stat :-
   slice:necessary_files(
      predicate:(slice:slice)/3, Files, Preds, _),
   sca_variable_get(source_tree, Tree),
   tree_to_statistics_tree:tree_to_statistics_tree(Files, Preds, Tree, Ext_Tree),
   xml_tree_to_table:slice_statistics_table(Ext_Tree,
   [(system, 'System'),
  (sources, 'Sources'),
  (unit, 'Unit'),
  (module, 'Module')], 0),
  !.



/*** implementation ***********************************************/


/******************************************************************/


