
:- dynamic current_num/2.
:- multifile current_num/2.


home_directory(Home) :-
   current_prolog_flag(unix, true),
   getenv('HOME', Home).
home_directory('C:/Programme') :-
   current_prolog_flag(windows, true).


current_num(sql_trace_mode, oracle).
current_num(tps_mode, 2).
current_num(trace_level, 1).

%current_num(mm_satchmo, 1).
%current_num(evidential_stable_mode, a).
%current_num(pm_algorithm2_generate, atoms).
%current_num(connected_atoms_mode, atoms).
%current_num(edge_mode, long).
%current_num(join_mode, regular).
%current_num(egcwa_mode, 0).
%current_num(sd_mode, 2).

