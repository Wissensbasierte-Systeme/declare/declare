

/******************************************************************/
/***                                                            ***/
/***          Slicing:  Necessary Files                         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* slice(+XML, -Necessary_Files, -Necessary_Predicates) <-
      */

slice(XML, Necessary_Files, Necessary_Predicates) :-
   xml_to_content(XML, mpas, include, MPAs),
   xml_to_content(XML, mpas, exclude, Exclude_MPAs),
   xml_to_content(XML, mpas, dynamic, Dynamic),
   xml_to_content(XML, mpas, multifile, Multifile),
   xml_to_content(XML, mpas, discontiguous, Discontigious),
   xml_to_content(XML, files, include, Included_Files),
   xml_to_content(XML, files, libraries, Libraries),
   xml_to_content(XML, files, exclude, Excluded_Files),
   slice(XML, MPAs, Exclude_MPAs,
      Dynamic, Multifile, Discontigious,
      Included_Files, Libraries,
      Excluded_Files,
      Necessary_Files, Necessary_Predicates).


/* tc(+Predicates, -TC_Predicates, -TC_Files) <-
      */

tc(Predicates, TC_Predicates, TC_Files) :-
   mpas_to_necessary_mpas_and_files(
      Predicates, TC_Predicates, TC_Files).


/* mpas_to_necessary_mpas_and_files(
      +Predicates, -Necessary_Predicates, -Necessary_Files) <-
      */

mpas_to_necessary_mpas_and_files(
      Predicates, Necessary_Predicates, Necessary_Files) :-
   mpas_to_necessary_mpas_and_files(
      [], [], Predicates, Necessary_Predicates, Necessary_Files).

mpas_to_necessary_mpas_and_files(
      Excluded_Files, Excluded_MPAs, Predicates,
      Necessary_Predicates, Necessary_Files) :-
   ( foreach(Predicate, Predicates),
     foreach(Files, List_of_Files),
     foreach(Preds, List_of_Preds) do
        slice:necessary_files(Excluded_Files, Excluded_MPAs,
           predicate:Predicate, Files, Preds, _) ),
   union(List_of_Preds, Necessary_Predicates),
   union(List_of_Files, Necessary_Files).


/*** implementation ***********************************************/


/* slice(+XML, +MPAs, +Exclude_MPAs,
         +Dynamic, +Multifile, +Discontigious,
         +Files, +Libraries, +Excluded_Files,
         -Necessary_Files, -Necessary_Predicates) <-
      */

slice(XML, Predicates, Excluded_MPAs, Dynamic,
      Multifile, Discontigious, Files, Libraries,
      Excluded_Files, Necessary_Files, MPAs_inc_Tests) :-
   mpas_to_necessary_mpas_and_files(
      Excluded_Files, Excluded_MPAs, Predicates,
      Necessary_Predicates, N_Files_0),
   subtract(N_Files_0, Excluded_Files, N_Files_1),
   union(Files, N_Files_1, N_Files_2),
   precision_of_slice(
      XML, N_Files_2, Necessary_Predicates, Fs_6, MPAs_inc_Tests),
   slice:determine_consulting_order(Fs_6, Fs_7),
   append(Files, Fs_7, Slice_Files_1),
   list_to_set(Slice_Files_1, Slice_Files_2),
   append(Libraries, Slice_Files_2, Nec_Files),
   subtract(Nec_Files, Excluded_Files, Necessary_Files),
   subtract(Slice_Files_2, Excluded_Files, Slice_Files_3),
   slice_package(Dynamic, Multifile, Discontigious,
      Slice_Files_3, MPAs_inc_Tests, Libraries).


/* precision_of_slice(+XML, +N_Files, +N_MPAs, -Files) <-
      */

precision_of_slice(XML, N_Files, N_MPAs, Files, MPAs_inc_Tests) :-
   maplist( precision_of_slice_parameter(XML),
      [discont, discont_2, multi, multi_2, dyn, twins,
         current_num, directives, tests, consults],
      [Dis, Dis_2, Multi, Multi_2, Dyn, Twin,
         Current, Directives, Tests, Consults] ),
   add_tests_to_slice(Tests, N_MPAs, Test_Files, Test_MPAs),
   ord_union(N_MPAs, Test_MPAs, MPAs_inc_Tests),
   maplist( slice:files_defining_mpa_property_in_slice(N_Files),
      [(Dyn, a, dynamic), (Multi, a, multifile),
       (Dis, a, discontiguous),
       (Dis_2, b, dynamic), (Multi_2, b, multifile)],
      [Fs_1, Fs_2, Fs_3, Fs_4, Fs_5] ),
   twin_predicates_to_files(Twin, N_MPAs, Fs_6),
   find_files_containing_current_num(Current, N_MPAs, Fs_7),
   consulted_files(Consults, N_Files, Fs_8),
   ord_union_2([Test_Files, N_Files, Fs_1, Fs_2, Fs_3, Fs_4, Fs_5,
      Fs_6, Fs_7, Fs_8], Fs_9),
   directive_mpas(Directives, N_MPAs, Fs_9, _, Fs_10),
   ord_union_2([Fs_9, Fs_10], Files).

precision_of_slice_parameter(XML, Parameter, Value) :-
   Value := XML@Parameter,
   !.
precision_of_slice_parameter(_, _, false) :-
   !.


/* add_tests_to_slice(+MPAs, -Test_Files, -Test_MPAs) <-
      */

add_tests_to_slice(false, _, [], []) :-
   !.
add_tests_to_slice(true, MPAs, Test_Files, Test_MPAs) :-
   add_tests_to_slice(MPAs, Test_Files, Test_MPAs),
   !.

add_tests_to_slice(MPAs, Test_Files_4, Test_MPAs) :-
   slice_to_tests(MPAs, Tests_Files_1, Called_MPAs),
   maplist( slice:extract_files,
      Tests_Files_1, Tests_Files_2 ),
   mpas_to_necessary_mpas_and_files(Called_MPAs,
      Test_MPAs, Test_Files_3),
   ord_union_2(Tests_Files_2, Test_Files_3, Test_Files_4),
   nl,
   length(Tests_Files_1, Test_Amount),
   write(Test_Amount),
   writeln(' Tests, which call predicates in this slice:'),
   writeln_list([''|Tests_Files_1]).

extract_files((F, _), F).


/* twin_predicates_to_files(+Necessary_Predicates, -Files) :-
      */

twin_predicates_to_files(false, _, []) :-
   !.
twin_predicates_to_files(true, Necessary_Preds, Files) :-
   twin_predicates_to_files(Necessary_Preds, Files),
   !.

twin_predicates_to_files(Necessary_Preds, Files) :-
   Twins = [
      (user:get_num)/2-(user:set_num)/2,
      (user:dislog_flag_get)/2-(user:dislog_flag_set)/2,
      (user:rar_variable_get)/2-(user:rar_variable_set)/2,
      (user:sca_variable_get)/2-(user:sca_variable_set)/2,
      (user:dislog_variable_get)/2-(user:dislog_variable_set)/2 ],
   twin_predicates_to_files_s(
      Necessary_Preds, Twins, Files).


/* necessary_predicates(MPA, Iterations-MPAs) <-
      */

necessary_predicates(predicate:MPA, Iterations-MPAs) :-
   !,
   rar_transitive_closure([MPA], Iterations-Preds_Temp),
   ord_union_2([MPA], Preds_Temp, MPAs).


/* necessary_files(+Level:Name,
      -Files, -Predicates, -Iterations) <-
      */

necessary_files(A, B, C, D) :-
   necessary_files([], [], A, B, C, D).

necessary_files(Exc_Fs, Exc_Ps, predicate:Name-Parameters,
      Files, Preds_2, Iterations) :-
   !,
   findall( File,
      rar:mpa_to_file(Name, Parameters, File),
      Files_T ),
   rar_transitive_closure(Exc_Fs, Exc_Ps, [Name-Parameters],
      Iterations-Preds),
   ord_union_2([Name], Preds, Preds_2),
   findall( F,
      ( member(P_1, Preds),
        rar:contains_lp(file:F, P_1) ),
      Files_Temp_1 ),
   list_to_ord_set(Files_Temp_1, Files_Temp_2),
   ord_union_2(Files_T, Files_Temp_2, Files),
   !.
necessary_files(Exc_Fs, Exc_Ps, predicate:MPA,
      Files_2, MPAs, Iterations) :-
   !,
   rar_transitive_closure(Exc_Fs, Exc_Ps, [MPA],
      Iterations-Preds_Temp),
   ord_union_2([MPA], Preds_Temp, MPAs),
   findall( F,
      ( member(P_1, MPAs),
        rar:select(rule, P_1, F, _) ),
      Files_1 ),
   list_to_ord_set(Files_1, Files_2),
   !.
necessary_files(Exc_Fs, Exc_Ps, Level:Name,
      Files, Preds, Iterations) :-
   called_predicates(Level:Name, Called_Predicates),
   rar_transitive_closure(Exc_Fs, Exc_Ps,
      Called_Predicates, Iterations-Preds_Temp),
   ord_union_2(Called_Predicates, Preds_Temp, Preds),
   findall( F,
      ( member(P_1, Preds),
        rar:contains_lp(file:F, P_1) ),
      Files_Temp ),
   list_to_ord_set(Files_Temp, Files),
   !.


/* rar_transitive_closure(Called_Predicates, N-Predicates) <-
      */

rar_transitive_closure(MPAs, N-Called_MPAs) :-
   rar_transitive_closure([], [], MPAs, N-Called_MPAs).

rar_transitive_closure(Excluded_Files, Excluded_MPAs,
      MPAs, N-Called_MPAs) :-
   rar_tc_step(Excluded_Files, Excluded_MPAs,
      MPAs, 0-[], N-Called_MPAs).

rar_tc_step(_, _, [], N-Predicates, N-Predicates) :-
   !.
rar_tc_step(Excluded_Files, Excluded_MPAs, MPAs,
      N1-Predicates_1, N2-Predicates_2) :-
   maplist( rar_tc_step_sub(Excluded_Files, Excluded_MPAs),
      MPAs, Ps_1 ),
   ord_union_2(Ps_1, Predicates),
   N3 is N1 + 1,
   subtract(Predicates, Predicates_1, Called_Predicates_New),
   ord_union_2(
      Predicates_1, Called_Predicates_New, Predicates_3),
   rar_tc_step(Excluded_Files, Excluded_MPAs,
      Called_Predicates_New, N3-Predicates_3, N2-Predicates_2),
   !.

rar_tc_step_sub(_, _, Predicate-Parameter, Called_Predicates) :-
   !,
   findall( Predicate_2,
      rar:calls_pp_with_variables(
         Predicate, Parameter, Predicate_2),
      Predicates_2 ),
   list_to_ord_set(Predicates_2, Called_Predicates).
rar_tc_step_sub(Excluded_Files, Excluded_MPAs, MPA_1, MPAs_2) :-
   findall( MPA_2,
      ( rar:select(rule, MPA_1, File, Rule),
        not( member(File, Excluded_Files) ),
        rar:calls_pp(Rule, MPA_1, MPA_2),
        not( member(MPA_2, Excluded_MPAs) ),
        not( built_in:mpa(MPA_2) )
      ),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   !.


/* called_predicates(Type:Name, Called_Predicates) <-
      */

called_predicates(file:Name, Called_Predicates) :-
   !,
   file_to_predicates(Name, Called_Predicates).
called_predicates(Level_1:Name_1, Called_Predicates) :-
   level_to_predicates(Level_1:Name_1, Called_Predicates).

file_to_predicates(Name, Called_Predicates) :-
   filename:file_part_to_relative_filename(Name, Name_1),
   findall( P_1,
      rar:defines_fp(Name_1, P_1),
      Call_List_Temp ),
   list_to_ord_set(Call_List_Temp, Called_Predicates).

level_to_predicates(Level_1:Name_1, Called_Predicates) :-
   findall( P_1,
      rar:contains_lp(Level_1:Name_1, P_1),
      Call_List_Temp ),
   list_to_ord_set(Call_List_Temp, Called_Predicates).


/* xml_to_content(+XML, mpas|files, +Type, -Values) <-
      */

xml_to_content(XML, files, Type, Files) :-
   findall( File,
      File :=
         XML^files::[@type=Type]^file@path,
      Files ).
xml_to_content(XML, mpas, Type, MPAs) :-
   findall( (M:P)/A,
      [M, P, A] :=
         XML^predicates::[@type=Type]^atom@[
            module, predicate, arity],
      MPAs ).


/* slice_to_tests(+MPAs, +Tests, +Called_MPAs) <-
      */

slice_to_tests(MPAs, Test_Calls, Called_MPAs_2) :-
   findall( (_, MPA),
      member(MPA, MPAs),
      FMPAs_1 ),
   list_to_ord_set(FMPAs_1, FMPAs_2),
   rar:transitive_closure_inv(FMPAs_2, Inv_Calls),
   sublist( rar:constraint_1,
      Inv_Calls, Test_Calls ),
   ( foreach(X, Test_Calls),
     foreach(Called_MPAs, MPA_List) do
        slice:calls_spec(X, Called_MPAs) ),
   union(MPA_List, Called_MPAs_1),
   list_to_ord_set(Called_MPAs_1, Called_MPAs_2).

calls_spec((File_1, MPA_1), MPAs_2) :-
   findall( MPA_2,
      rar:calls_ff(File_1, MPA_1, _, MPA_2),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2).


/******************************************************************/


