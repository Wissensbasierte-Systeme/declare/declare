

/******************************************************************/
/***                                                            ***/
/***          Slice: Find consulted Files                       ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* consulted_files(+Files, -Consulted_Files) <-
      */

consulted_files(false, _, []) :-
   !.
consulted_files(true, Files, Consulted_Files) :-
   consulted_files(Files, Consulted_Files),
   !.

consulted_files(Files, Consulted_Files) :-
   dislog_variable_get(home, DisLog_1),
   !,
   add_slash_suffix(DisLog_1, DisLog_2),
   findall( C_Files,
      ( member(File, Files),
        rar:select(rule, _, File, Rule),
        slice:rule_consults_files(Rule, DisLog_2, File, C_Files) ),
      Fs ),
   ord_union(Fs, Consulted_Files).


/*** implementation ***********************************************/


/* rule_consults_files(+Rule, +DisLog, +File, -Files) <-
      */

rule_consults_files(Rule, DisLog, File, Files_2) :-
   Atom := Rule^body^atom::[@predicate='.', @arity=2],
   rar:term_to_list(Atom, Files_1),
   findall( F_2,
      ( member(F_1, Files_1),
        slice:check_and_complete_file(DisLog, File, F_1, F_2) ),
      Files_2 ).


/* check_and_complete_file(+DisLog, +File, +File_1, -File_2) <-
      */

check_and_complete_file(DisLog_2, File, File_1, File_2) :-
   file_directory_name(File, Directory),
   concat([Directory, '/', File_1], File_2),
   concat([DisLog_2, File_2], File_3),
   exists_file(File_3),
   !.
check_and_complete_file(DisLog_2, File, File_1, File_2) :-
   file_directory_name(File, Directory),
   concat([Directory, '/', File_1, '.pl'], File_2),
   concat([DisLog_2, File_2], File_3),
   exists_file(File_3),
   !.


/******************************************************************/


