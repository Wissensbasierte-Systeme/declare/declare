
writeln_list_with_announcement(Xs) :-
   writeln('beginning of list'),
   writeln_list(Xs),
   writeln('end of list').
	   
write_list(As) :-
      maplist( write,
         As ).

writeln_list(Xs) :-
     maplist( writeln,
         Xs ).

writeln(X) :-
	write(X),
	nl.

/*
delete(Xs, E, Ys) :-
   findall( X,
      ( member(X, Xs),
        not( E = X ) ),
      Ys ).

list_info(Ys) :-
   Ys = [],
   writeln('Empty List.')
   ;
   writeln('Full List.').

xxx(A) :-
	A = [a,b,c].

yyy(A) :-
	A = (a,b,c).
*/




