

/******************************************************************/
/***                                                            ***/
/***          DisLog Unit:  Source Code Analysis                ***/
/***                                                            ***/
/******************************************************************/



/*** DisLog Units *************************************************/


dislog_unit( source_code_analysis, [
   common,
   runtime_measurement,
   diagrams,
   gxl,
   xpce_tools,
   statistics,
   spectra,
   rar,
   visur,
   slice,
   swrl,
   jaml_sca,
   php_sca ] ).


/*** DisLog Modules ***********************************************/


dislog_module( common, [
   'sca_variables.pl',
   'exists_file_with_tilde.pl',
   'aliases.pl',
   'alias_to_dtd.pl',
   'alias_browser.pl',
   'basics.pl',
   'xml_io.pl', 'tests_xml_io.pl',
   'fn_patches.pl', 'tests_basics.pl',
   'word_count.pl',
   'file_system_changes.pl',
   'missing_predicates.pl',
   'multiset.pl' ] ).

dislog_module( runtime_measurement, [
   'random_integers.pl',
   'benchmark.pl' ]).

dislog_module( diagrams, [
   'polar_diagram.pl',
   'chart.pl' ] ).

dislog_module( gxl, [
   'presettings.pl',
   'basics.pl',
   'basic_transformations.pl',
   'basic_operations.pl',
   'examples.pl',
   'gxl_to_vertices_edges.pl',
   'gxl_to_single_or_missing_nodes.pl',
   'gxl_to_tree_forward_back_cross_edges.pl',
   'transformations.pl',
   'vertices_edges_to_gxl.pl',
   'complete_gxl.pl',
   'gxl_to_picture.pl',
   'picture_to_gxl.pl',
   'layout.pl',
   'layout_graphviz.pl',
   'tests_layout.pl',
   'scc.pl',
   'highlight_scc.pl',
   'merge_nodes.pl',
   'generate_merge_config.pl',
   'embedded_graphs.pl',
   'bfs_grouping.pl',
   'metrics.pl',
   'symbols.pl',
   'chart_symbols.pl',
   'arrows_handles.pl',
   'labels.pl',
   'separate_and_merge.pl',
   'xml_to_gxl.pl',
   'compound_methods.pl',
   'export_import.pl',
   'gxl_to_tree.pl',
   'gxl_contract.pl',
   'gxl_to_gxl.pl',
   'tests_basics.pl',
   'tests.pl' ] ).

dislog_module( xpce_tools, [
   'xpce_picture.pl',
   'hierarchy_browser_ddk_class.pl',
   'hierarchy_browser_ddk.pl',
   'tests_hierarchy_browser_ddk.pl',
   'xml_click_gesture_and_popups.pl' ] ).

dislog_module( statistics, [
   'statistics.pl',
   'tree_to_statistics_tree.pl',
   'xml_tree_to_table.pl',
   'dead_code.pl',
   'undefined_code.pl',
   'dead_and_undefined_code_excluded.pl',
   'file_statistics.pl',
   'mpas_rules_calls.pl',
   'bar_graph.pl',
   'statistics_2004.pl',
   'topology_db.pl',
   'topology.pl',
   'polar_diagrams.pl',
   'polar_diagrams_save_and_load.pl',
   'polar_diagrams_db.pl',
%  'tests_mpa_spectrum.pl',
   'tests.pl' ] ).

dislog_module( spectra, [
   'spectra_db.pl',
   'spectra.pl',
   'spectra_save_and_load.pl',
   'further.pl',
   'mpa_statistics.pl' ]).

dislog_module( rar, [
   'rar.pl',
   'filename.pl',
   'file_dependency.pl',
   'module_dependency.pl',
   'list_to_tree.pl',
   'built_in_predicates.pl',
   'tests_calls_pp.pl',
   'tests_calls_ll.pl',
   'tests_contains.pl',
   'tests.pl',
   'tests_xml_to_prolog.pl' ] ).

%  'parser_prolog.pl',
%  'predicate_groups.pl',
%  'dml.pl',
%  'database.pl',

dislog_module( visur, [
   'visur.pl',
   'tests_visur.pl' ] ).

%  'graphs.pl',
%  'rule_goal_graphs.pl',
%  'rule_goal_graphs_with_topological_filter.pl',

dislog_module( slice, [
   'slice.pl',
   'tests.pl' ] ).

dislog_module( swrl, [
   'swrl.pl',
   'owl.pl',
   'owl_instance.pl',
   'owl_class.pl',
   'compact.pl',
   'merge_single_neighbours.pl',
   'tests.pl' ] ).

dislog_module( jaml_sca, [
   'sources_to_rar_db.pl',
   'common_methods.pl' ] ).

dislog_module( php_sca, [
   'sources_to_rar_db.pl' ] ).


/******************************************************************/


