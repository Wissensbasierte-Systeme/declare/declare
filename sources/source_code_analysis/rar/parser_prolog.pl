

/******************************************************************/
/***                                                            ***/
/***          RAR:  Parser                                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* parse_sources_to_rar_database(+Tree_1, -Tree_2) <-
      */

parse_sources_to_rar_database(Tree_1, Tree_5) :-
   Type := Tree_1@source_code,
   memberchk(Type, [prolog, dislog]),
   rar:reset_rar_database,
   rar:reset_gxl_database,
   rar:reset_temporary_dbs,
   alias_to_file(predicate_groups, SP),
   assert_predicate_groups(SP),
   writeln('generating RaR database ... '),
   determine_runtime(
      rar:parse_sources_to_rar_database_sub(Tree_1, Tree_5) ),
   writeln(' ... done.'),
   set_rar_db_time_variable(Type),
   !.
parse_sources_to_rar_database(Tree_1, Tree_2) :-
   Type:= Tree_1@source_code,
   parse_sources_to_rar_database(Type, Tree_1, Tree_2).


/* parse_file_to_rar_database(+File) <-
      */

parse_file_to_rar_database(File) :-
   rar:reset_rar_database,
   rar:reset_gxl_database,
   alias_to_fn_triple(predicate_groups, PG),
   assert_predicate_groups(PG),
   writeln('generating RaR database ... '),
   Tree_1 =
      tree:[root:'', name:tree]:[
         file:[name:File, path:File]:[] ],
   determine_runtime(
      rar:parse_sources_to_rar_database_sub(Tree_1, _) ),
   writeln(' ... done.').


/*** implementation ***********************************************/


/* parse_sources_to_rar_database_sub(+Tree_1, -Tree_2) <-
      */

parse_sources_to_rar_database_sub(Tree_1, Tree_5) :-
   clear_tree(Tree_1, Tree_2),
   sca_variable_set(tmp_tree, Tree_2),
   tree_to_files(Tree_2, Relative_File_Names),
   Root_1 := Tree_2@root,
   tilde_to_home_path(Root_1, Root_2),
   maplist( concat(Root_2),
      Relative_File_Names, Absolute_File_Names),
   sublist( exists_file,
      Absolute_File_Names, Existing_Files ),
   maplist( program_file_to_xml,
      Existing_Files, Xmls ),
   sca_variable_get(exported_predicates, Exp_Preds),
   checklist( xml_file_to_db(Root_2, Exp_Preds),
      Xmls ),
   sca_variable_get(tmp_tree, Tree_3),
   rar:add_date_to_files(Tree_3, Tree_4),
   rar:add_mpas_to_file_content(Tree_4, Tree_5),
   reverse_tree(Tree_5),
   sca_variable_set(source_tree, Tree_5).


/* xml_file_to_db(+Root, +Exp_Preds, +Xml) <-
      */

xml_file_to_db(Root, Exp_Preds, Xml) :-
   Abs_File := Xml@path,
   Module := Xml@module,
   xml_consults_files(Abs_File, Xml, Abs_Files),
   add_consulted_files_to_tree(Root, Abs_File, Abs_Files),
   maplist( program_file_to_xml,
      Abs_Files, Xmls_1 ),
   Xmls_2 = [Xml|Xmls_1],
   findall( Head,
      ( member(X, Xmls_2),
        Head := X^rule^head^atom@predicate ),
      Heads_1 ),
   findall( P2/A,
      ( member(X, Xmls_2),
        Atom := X^rule^body^atom::[@predicate=(dynamic)/1],
        ( Term := Atom^_^term::[@functor='/']
        ; Term := Atom^term::[@functor='/'] ),
        P2 := Term-nth(1)^term@functor,
        A := Term-nth(2)^term@functor ),
      Dynamics ),
   append(Heads_1, Dynamics, Heads_2),
   list_to_ord_set(Heads_2, Heads_3),
   checklist( xml_file_to_db(Root, Exp_Preds, Module, Heads_3),
      Xmls_2 ).

xml_file_to_db(Root, Exp_Preds, Module_1, Heads, Xml) :-
   Module_2 := Xml@module,
   ( Module_2 = user ->
     Module = Module_1
   ; Module = Module_2 ),
   Abs_File := Xml@path,
   write('... inserting rules to db of '),
   writeln(Abs_File),
   concat(Root, Rel_File, Abs_File),
   forall( Rule := Xml^rule^content::'*',
      rule_to_db(Exp_Preds,
         Rel_File, Module, Heads, Rule) ).


/* rule_to_db(+Exp_Preds, +File, +Module,
         +Heads_in_Files, +Rule) <-
      */

rule_to_db(Exp_Preds, File, Module, Heads_in_Files, Rule) :-
   rule_to_head(Rule, P/A, C),
   Body := Rule^body,
   complete_body(Exp_Preds, Module,
      Heads_in_Files, Body, Completed_Body),
   R1 := Rule*[^body:[]:Completed_Body],
   Head_l = [atom:[module:Module, predicate:P, arity:A]:C],
   R2 := R1*[^head:[]:Head_l],
   insert_rule(File, (Module:P)/A, rule:[module:Module]:R2),
   !.

rule_to_head(Rule, P/0, []) :-
   Head/_ := Rule^head@atom^predicate,
   rule_operator(Head),
   gensym(no_head_, P),
   !.
rule_to_head(Rule, Head, C) :-
   Head := Rule^head@atom^predicate,
   C := Rule^head^atom,
   !.
rule_to_head(Rule, P/0, []) :-
   not( _ := Rule^head@atom^predicate ),
   gensym(no_head_, P),
   !.


/* complete_body(+Exp_Preds, +Module, +Heads_in_Files,
      +Atoms, -Completed_Atoms) <-
      */

complete_body(Exp_Preds, Module, Heads_in_Files,
      [Atom|Atoms], [Completed_Atom|Completed_Atoms]) :-
   Pred := [Atom]@atom^predicate,
   pa_to_mpa(Exp_Preds, Module, Heads_in_Files, Pred, (M:P)/A),
   complete_atom(Exp_Preds, Module, Heads_in_Files,
      (M:P)/A, Atom, Atom_1),
   [Completed_Atom] := [Atom_1]*[
      @atom^module:M, @atom^predicate:P, @atom^arity:A],
   complete_body(Exp_Preds, Module, Heads_in_Files,
      Atoms, Completed_Atoms),
   !.
complete_body(_, _, _, [], []) :-
   !.
complete_body(Exp_Preds, Module, Heads_in_Files,
      [Atom|Atoms], [Completed_Atom|Completed_Atoms]) :-
   Atom = formula:[junctor: Junctor]:C,
   formula_junctor(_, Junctor),
   complete_body(Exp_Preds, Module, Heads_in_Files,
      C, C_1),
   Completed_Atom = formula:[junctor: (Junctor)]:C_1,
   complete_body(Exp_Preds, Module, Heads_in_Files,
      Atoms, Completed_Atoms),
   !.
complete_body(_, _, _, A, A) :-
%  dwrite(xml, A),
%  writeln(A),
   writeln('Error while parsing the construct above!').

complete_atom(Exp_Preds, Module, Heads, (M:P)/A,
      Atom_In, Atom_Out) :-
   special_predicate_attributes((M:P)/A, meta, _, _, Attr),
   List := Attr@calls,
   Atom_In = Label:X_1:Term_In,
   X_2 := X_1*[module:M, predicate:P, arity:A],
   maplist( patch_1,
      Term_In, T_Out),
   !,
   complete_term(Exp_Preds, Module, Heads, List,
      T_Out, Term_Out),
   Atom_Out = Label:X_2:Term_Out.
complete_atom(Exp_Preds, Module, Heads, (M:P)/A,
      Atom_In, Atom_Out) :-
   special_predicate_attributes((M:P)/A, meta, _, _, Attr),
   List := Attr@calls,
   !,
   Atom_In = Label:X_1:Term_In,
   ( ( _ := X_1@functor,
       X_2 := X_1*[module:M, functor:P, arity:A] )
   ; ( _ := X_1@predicate,
       X_2 := X_1*[module:M, predicate:P, arity:A] ) ),
   xml_to_mpa_structure(Term_In, Term_In_2),
   complete_term(Exp_Preds, Module, Heads, List,
      Term_In_2, Term_Out),
   Atom_Out = Label:X_2:Term_Out,
   !.
complete_atom(_, _, _, (_:P)/A, Term_In, Term_Out) :-
   [Module, _] := Term_In@[module, functor],
   Term_Out := Term_In*[@module:Module, @functor:P, @arity:A],
   !.
complete_atom(_, _, _, (M:P)/A, Term_In, Term_Out) :-
   _ := Term_In@functor,
   Term_Out := Term_In*[@module:M, @functor:P, @arity:A],
   !.
complete_atom(_, _, _, (M:P)/A, Term_In, Term_Out) :-
   _ := Term_In@predicate,
   Term_Out := Term_In*[@module:M, @predicate:P, @arity:A],
   !.
complete_atom(_, _, _, _, Term, Term).


complete_term(Exp_Preds, Module, Heads,
      [n|List], [Term_In|Terms_In], [Term_In|Terms_Out]) :-
   !,
   complete_term(Exp_Preds, Module, Heads,
      List, Terms_In, Terms_Out),
   !.
complete_term(Exp_Preds, Module, Heads,
      [D_Arity|List], [Term_In|Terms_In], [Term_Out|Terms_Out]) :-
   ctia_1(Exp_Preds, Module, Heads,
      D_Arity, Term_In, Term_Out),
   complete_term(Exp_Preds, Module, Heads,
      List, Terms_In, Terms_Out).
complete_term(_, _, _, _, [], []).

ctia_1(Exp_Preds, Module,
      Heads, D_Arity, Term_In, Term_Out) :-
   ( ',' := [Term_In]@term^functor ->
     comma_structure_to_db(Exp_Preds, Module, Heads,
        Term_In, Term_Out)
   ; ctia_2(Exp_Preds, Module, Heads, D_Arity,
        Term_In, Term_Out) ).

ctia_2(_, _, _, D_Arity, Term_In, Term_Out) :-
   [_, Arity, _] := Term_In@[functor, arity, module],
   !,
   New_Arity is Arity + D_Arity,
   Term_Out := Term_In*[@arity:New_Arity],
   !.
ctia_2(Exp_Preds, Module, Heads,
      D_Arity, Term_In, Term_Out) :-
   Term := [Term_In]^term,
   !,
   length(Term, Arity),
   New_Arity is Arity + D_Arity,
   Functor := [Term_In]@term^functor,
   This_Module := [Term_In]@term@functor,
   ( This_Module == [] ->
     pa_to_mpa(Exp_Preds, Module, Heads,
        Functor/New_Arity, (M:P)/A)
   ; (M:P)/A = (This_Module:Functor)/New_Arity ),
  complete_atom(Exp_Preds, Module, Heads,
     (M:P)/A, Term_In, T_Out),
   [Term_Out] := [T_Out]*[
      @term^module:M, @term^arity:A, @term^functor:P],
   !.
ctia_2(_, _, _, _, Argument, Argument) :-
   _ := [Argument]^var,
   !.


/* comma_structure_to_db(
         Exp_Preds, Module, Heads, Term, Result) <-
      */

comma_structure_to_db(
      Exp_Preds, Module, Heads, Term_In, Term_Out) :-
   Term_In = term:X_1:T_In,
   X_2 := X_1*[module:user, arity:2],
   maplist( patch_1,
      T_In, T_In_2 ),
   maplist( ctia_1(Exp_Preds, Module, Heads, 0),
      T_In_2, T_Out ),
   length(T_Out, Arity),
   Term_O = term:X_2:T_Out,
   [Term_Out] :=
      [Term_O]*[@term^module:user, @term^arity:Arity].


/* pa_to_mpa(Exp_Preds, M, Heads, Pred, (M:P)/A) <-
      */

pa_to_mpa(_, _, _, (M:P)/A, (M:P)/A) :-
   !.
pa_to_mpa(_, M, Heads, P/A, (M:P)/A) :-
   member(P/A, Heads),
   !.
pa_to_mpa(Exp_Preds, _, _, P/A, (M:P)/A) :-
   member(module:[name:M, file:_]:Atoms, Exp_Preds),
   member(atom:[predicate:P, arity:A]:[], Atoms),
   !.
pa_to_mpa(_, _, _, P/A, (user:P)/A) :-
   !.
pa_to_mpa(_, _, _, A, F) :-
   gensym(error, F),
   writeln(error:F-A).


xml_to_mpa_structure([C|Cs], [C_Out|Cs_Out]) :-
   C = term:[functor: :]:[ term:[functor:Module]:[],
          term:[functor:Predicate]:X ],
   length(X, Arity),
   xml_to_mpa_structure(X, X_Out),
   C_Out = term:[
      module:Module, functor:Predicate, arity:Arity]:X_Out,
   xml_to_mpa_structure(Cs, Cs_Out).
xml_to_mpa_structure([C|Cs], [C_Out|Cs_Out]) :-
   C = T:C_2:X,
   xml_to_mpa_structure(X, X_Out),
   C_Out = T:C_2:X_Out,
   xml_to_mpa_structure(Cs, Cs_Out).
xml_to_mpa_structure([], []).


/* add_consulted_files_to_tree(+Root, +File, +Files) <-
      */

add_consulted_files_to_tree(_, _, []) :-
   !.
add_consulted_files_to_tree(Root, Abs_File, Abs_Files) :-
   concat(Root, Rel_File, Abs_File),
   maplist( concat(Root),
      Rel_Files, Abs_Files ),
   maplist( file_name_to_xml_term(Rel_File),
      Rel_Files, Terms ),
   sca_variable_get(tmp_tree, Tree_1),
   delete_files_from_tree(Rel_Files, Tree_1, Tree_2),
   add_term_to_tree(Rel_File, Terms, Tree_2, Tree_3),
   sca_variable_set(tmp_tree, Tree_3),
   !.


/* add_term_to_tree(+File, +Term, +Tree_1, -Tree_2) <-
      */

add_term_to_tree(File, Term, Tag:Attr_1:C_1, Tag:Attr_1:C_2) :-
%  File := (Tag:Attr_1:C_1)^file@path,
   member(file:Attr_2:_, C_1),
   memberchk(path:File, Attr_2),
   union(C_1, Term, C_2),
   !.
add_term_to_tree(File, Term, Tag:Attr:C_1, Tag:Attr:C_2) :-
   maplist( add_term_to_tree(File, Term),
      C_1, C_2 ),
   !.
add_term_to_tree(_, _, '', '').


/* file_name_to_xml_term(+Cons_File, +File_1, -Term) <-
      */

file_name_to_xml_term(Cons_File, File_1, Term) :-
   file_base_name(File_1, File_2),
   Term = file:[name:File_2, path:File_1,
      consulted_from:Cons_File,
      mouse_click:consulted_file, alias:consulted_file]:[].


/* xml_consults_files(+Xml, -Files) <-
      */

xml_consults_files(File, Xml, Files) :-
   write('... searching for files consulted in '),
   writeln(File),
%    Xml = _:_:X,
   findall( Consulted_Files,
      ( Rule := Xml^rule,
        %member(Rule, X),
        rule_consults_files(Rule, File, Consulted_Files) ),
      Fs ),
   ord_union_2(Fs, Files).

rule_consults_files(Rule, File, Files_2) :-
   Rule = rule:_:Content,
   member(head:[]:[], Content),
/*   Rule = _:_:X1,
   member(body:_:X2, X1),
   member(atom:X3:X4, X2),
   member(predicate:'.'/2, X3),
   Atom = atom:X3:X4,*/
   Atom := Rule^body^atom::[@predicate='.'/2],
   term_to_list(Atom, Files_1),
   maplist( filename:check_and_complete_file(File),
      Files_1, Files_2 ).


/* term_to_list(_:_:[A, B], [F|List]) <-
      */

term_to_list(_:_:[_:A1:_, Tag:A2:Cont], [F|List]) :-
   member(functor:F, A1),
   memberchk(functor:'.', A2),
   term_to_list(Tag:A2:Cont, List),
   !.
term_to_list(_:_:[_:A:_, B], [F]) :-
   member(functor:F, A),
   B = term:[functor:[]]:[].


/* patch_1(+Term_1, -Term_2) <-
      */

patch_1(term:[functor:':']:C_1, T_1) :-
   C_1 = [term:[functor:C_1_Module]:[], term:[functor:Pred]:C_2],
   maplist( patch_1,
      C_2, C_3 ),
   T_1 = term:[module:C_1_Module, functor:Pred]:C_3,
   !.
patch_1(T, T).


/* the same in fast, but not flexible to xml changes
      */
/*
term_to_list(_:_:[_:[functor:F]:_, _:[functor:'.']:Cont], [F|List]) :-
   term_to_list(_:_:Cont, List),
   !.
term_to_list(_:_:[_:[functor:F]:_, term:[functor:[]]:[]], [F]) :-
   !.
*/

/******************************************************************/


