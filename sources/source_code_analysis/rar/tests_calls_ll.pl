

/******************************************************************/
/***                                                            ***/
/***          RAR:  calls_ll                                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(source_code_analysis:rar_new, calls_ff_1) :-
   rar:init(sca_tests_2),
   P = 'sources/source_code_analysis/rar/tests/',
   concat(P, 'calls_pp_test_1.pl', File),
   rar:calls_ff(File, P_1_a, F_2, P_2_a),
%  writelq(F_2),
%  writelq(Calls_1),
   concat(P, 'calls_pp_test_2.pl', F_2),
   P_1_a = (rar_test_1:a)/2,
   P_2_a = (rar_test_2:tam_1)/1,
   rar:calls_ff(F_1, P_1_b, F_2, P_2_b),
   concat(P, 'calls_pp_test_1.pl', F_1),
   P_1_b = (rar_test_1:b)/2,
   P_2_b = (rar_test_2:tam_2)/1,
   !.


test(source_code_analysis:rar_new, who_calls) :-
   rar:init(sca_tests_1),
   rar:who_calls(
      module:'sources/source_code_analysis/rar/test_1',
      module:'sources/source_code_analysis/rar/test_2', Calls),
   Calls = [
      (user:rar_predicate_to_cross_calls)/2-
         (user:in_same_module)/2,
      (user:rar_predicate_to_cross_calls)/2-
         (user:rar_predicate_to_descendants)/2 ],
   mpas_rules_calls:multilevel_mpa(
      module:Module_1, module:Module_2, Predicate ),
   Predicate = (user:in_same_module)/2,
   Module_1 = 'sources/source_code_analysis/rar/test_1',
   Module_2 = 'sources/source_code_analysis/rar/test_2'.


test(source_code_analysis:rar_new, calls_ll) :-
   rar:init(sca_tests_1),
   findall( Module,
      rar:calls_ll(module:'sources/source_code_analysis/rar/test_1',
      module:Module),
      Modules_1_a ),
   list_to_ord_set(Modules_1_a, Modules_1_b),
   Modules_1_b = ['sources/source_code_analysis/rar/test_1',
      'sources/source_code_analysis/rar/test_2'],
   findall( Module,
      rar:calls_ll(module:Module,
      module:'sources/source_code_analysis/rar/test_2'),
      Modules_2_a ),
   list_to_ord_set(Modules_2_a, Modules_2_b),
   Modules_2_b = ['sources/source_code_analysis/rar/test'|Modules_1_b].


/*** implementation ***********************************************/


/******************************************************************/


