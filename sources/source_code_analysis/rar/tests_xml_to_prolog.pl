

/******************************************************************/
/***                                                            ***/
/***          RAR: xml_to_prolog                                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(source_code_analysis:xml, xml_to_prolog) :-
   dislog_variable_get(sca, SCA_Path),
   !,
   concat(SCA_Path, 'rar/tests/', Dir),
   concat(Dir, 'xml_to_prolog_test.pl', File_1),
   concat(Dir, 'xml_to_prolog_test_out.xml', File_2),
   concat(Dir, 'xml_to_prolog_test_out.pl', File_3),
   program_file_to_xml(File_1, Xml),
   dwrite(xml_2, File_2, Xml),
   xml_file_to_prolog(File_2, File_3),
   checklist( delete_file,
      [File_2, File_3] ).


/*** implementation ***********************************************/


/******************************************************************/
