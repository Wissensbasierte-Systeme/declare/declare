

/******************************************************************/
/***                                                            ***/
/***          RAR:  Built In Predicates                         ***/
/***                                                            ***/
/******************************************************************/


:- module( built_in, []).

:- dynamic mpa/1.


/*** interface ****************************************************/


assert_all_built_in :-
   retractall( mpa(_) ),
   findall( MPA_T,
      predicate_prolog(MPA_T),
      MPAs_1 ),
   findall( MPA_T,
      predicate_xpce(MPA_T),
      MPAs_2 ),
   manual_built_in_list(Built_In_List),
   append([Built_In_List, MPAs_1, MPAs_2], MPAs_3),
   list_to_ord_set(MPAs_3, MPAs_4),
   forall( member(MPA, MPAs_4),
      assert( mpa(MPA) ) ).


/*** implementation ***********************************************/


/* predicate_prolog(-Built_In) <-
      */

predicate_prolog((user:P)/A) :-
   catch( user:predicate_property(Atom, built_in), _, fail ),
   functor(Atom, P, A).
predicate_prolog((user:(?-))/1).


/* predicate_xpce(-Built_In) <-
      */

predicate_xpce((user:P)/A) :-
%  xref_common:built_in(Atom),
%  catch( xref_common:built_in(Atom), _, fail ),
   clause( xref_common:built_in(Atom), true ),
   functor(Atom, P, A).


manual_built_in_list(List) :-
   List = [
      (user: (or))/2,
      (user:current_num)/2,
      (pce_host:pce_home)/1,
      (user:send_super)/3,
      (user:pce_begin_class)/3,
      (user:send_super)/7,
      (user:pce_begin_class)/2,
      (user:pce_group)/1,
      (user:pce_group)/1,
      (user:pce_end_class)/1,
      (user:pce_begin_class)/2,
      (user:pce_end_class)/1,
      (user:if_then)/2,
      (lists:delete)/3,
      (user: (==>))/2,
      (user:union)/3,
      (xref_common:built_in)/1,
      (user:subtract)/3,
      (user:subset)/2,
      (ascii_text:compose)/3,
      (ascii_text:split)/3,
      (d3_module:consult)/1,
      (font: :)/2,
      (hash_table:get)/3,
      (prolog_in_xml_layout_transformation:term_to_xml)/3,
      (rar_database:calls_uu)/1,
      (substitution_rules_application:convert_layout_to_modified_term)/4,
      (substitution_rules_application:term_with_chars_to_xml)/4,
      (system:statistics)/2,
      (triple_parser:parse)/3,
      (user:apply_substitution_rules_to_textbuffer)/1,
      (user:atom_concat)/3,
      (user:atom_prefix)/2,
      (user:b_getval)/2,
      (user:b_setval)/2,
      (user:size)/2,
      (user:sublist)/3,
      (user:link)/2,
      (user:scale)/3,
      (user:backslash)/1,
      (user:chain_list)/2,
      (user:char_code)/2,
      (user:chart)/2,
      (user:chart)/4,
      (user:code_type)/2,
      (user:convert_layout_to_modified_rule)/3,
      (user:convert_time)/2,
      (user:convert_time)/8,
      (user:ddk_version)/1,
      (user:dialog)/2,
      (user:dislog_module)/3,
      (user:dislog_module_description)/3,
      (user:dislog_modules)/1,
      (user:dislog_sources)/1,
      (user:dtd)/2,
      (user:emacs)/1,
      (user:fetch_url)/3,
      (user:flatten)/2,
      (user:fn_get_optional_attribute)/4,
      (user:free_variables)/2,
      (user:gensym)/2,
      (user:get_url_to_file)/2,
      (user:global_variable)/3,
      (user:hash_term)/2,
      (user:help)/1,
      (user:home_directory)/1,
      (user:hoppenstedt_table_rows)/3,
      (user:html2terms)/2,
      (user:intersection)/3,
      (user:is_absolute_file_name)/1,
      (user:last)/2,
      (user:list_to_set)/2,
      (user:load_html_file)/2,
      (user:load_structure)/3,
      (user:make_dialog)/2,
      (user:manpce)/0,
      (user:max)/2,
      (user:min)/2,
      (user:nth1)/3,
      (user:numlist)/3,
      (user:odbc_connect)/3,
      (user:odbc_query)/2,
      (user:odbc_query)/4,
      (user:open)/4,
      (user:parse_stable_models)/3,
      (user:parse_xml_to_prolog)/2,
      (user:postscript)/2,
      (user:prolog_file_to_xml)/2,
      (user:put_char)/1,
      (user:qsave_program)/1,
      (user:random)/3,
      (user:rdf)/3,
      (user:rdf_has)/3,
      (user:rdf_load)/1,
      (user:rdf_split_url)/3,
      (user:rdf_subject)/1,
      (user:rdf_unload)/1,
      (user:read_file_to_codes)/3,
      (user:read_line_to_codes)/2,
      (user:readln)/1,
      (user:recorda)/2,
      (user:recorded)/2,
      (user:redefine_system_predicate)/1,
      (user:require)/1,
      (user:select)/3,
      (user:send_list)/2,
      (user:send_list)/3,
      (user:setenv)/2,
      (user:single_data)/2,
      (user:static_models_operator)/2,
      (user:sumlist)/2,
      (user:table_rows)/3,
      (user:term_variables)/2,
      (user:textbuffer_to_xml)/2,
      (user:time_file)/2,
      (user:url_info)/2,
      (user:wildcard_match)/2,
      (user:writeln_log)/2,
      (user:www_open_url)/1,
      (user:xml2terms)/2,
      (user:xml_rule_to_atom)/2,
      (user:xml_rule_to_chars)/2,
      (prolog:_)/_,
      (user:variable)/3,
      (user:initialise)/6,
      (user:expand_node)/2,
      (user:drop)/3,
      (user:event)/2,
      (user:make_dragdevice_recogniser)/1,
      (xml_pillow_mavigate:any_element)/2 ].


/*** directives ***************************************************/


:- built_in:assert_all_built_in.


/******************************************************************/


