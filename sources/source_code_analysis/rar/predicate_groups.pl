

/******************************************************************/
/***                                                            ***/
/***          RAR:  Special Predicates                          ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library('xref/common.pl')).


/*** interface ****************************************************/


/* assert_special_predicates(+File) <-
      */

assert_special_predicates(F) :-
   assert_predicate_groups(F).


/* assert_predicate_groups(+XML|File) <-
      */

assert_predicate_groups(predicate_groups:Attr:Cont) :-
   xml_to_special_predicate_facts(predicate_groups:Attr:Cont).
assert_predicate_groups(File) :-
   exists_file_with_tilde(File),
   dread_(xml(predicate_groups), File, XML),
   xml_to_special_predicate_facts(XML),
   add_file_to_alias(predicate_groups, File),
   !.


/* special_predicate_attributes(?MPA, ?Group, ?Class, ?Type, ?Attr) <-
      */

special_predicate_attributes(MPA, Group, Class, Type, Attr) :-
   special_predicate(MPA, Group, Class, Type, Attr),
   !.
special_predicate_attributes((_:_)/_, Group, Class, Type, Attr) :-
   special_predicate(predicate, Group, Class, Type, Attr),
   !.
special_predicate_attributes(_, Group, Class, Type, Attr) :-
   special_predicate(default, Group, Class, Type, Attr),
   !.


/*** implementation ***********************************************/


/* xml_to_special_predicate_facts(+XML) <-
      */

xml_to_special_predicate_facts(XML) :-
   reset_predicate_groups,
   write('asserting special predicates ... '),
   rar:xml_to_special_predicate_facts_sub(XML),
   writeln(' ... done.').

xml_to_special_predicate_facts_sub(XML) :-
   forall(
      xml_sp_to_facts_sp(XML, MPA, Group, Class, Type_1, Attr),
      asserta( rar:special_predicate(MPA, Group, Class, Type_1, Attr) )
      ),
   assert_prolog_built_in_predicates(XML).


/* xml_sp_to_facts_sp(+XML, -Predicate, -Group) <-
      */

xml_sp_to_facts_sp(_:_:XML, MPA, Group, Class, Type, Attr) :-
   member(P, XML),
   Group := P@group,
   Class := P@class,
   Type := P@type,
   xml_sp_to_facts_sp(P, MPA, Attr).

xml_sp_to_facts_sp(predicates:Attr:[], MPA, attr:[]:[]) :-
   MPA := (predicates:Attr:[])@group,
   !.
xml_sp_to_facts_sp(P, (Module:Name)/Arity, attr:Attr_2:[]) :-
   atom:Attr_1:_ := P^atom::[
      @predicate=Name, @module=Module, @arity=Arity],
   subtract(Attr_1, [arity:Arity, predicate:Name, module:Module],
      Attr_2).


/* assert_prolog_built_in_predicates(+XML) <-
      */

assert_prolog_built_in_predicates(XML) :-
   _ := XML^predicates::[@group=built_in, @class=prolog, @type=Type_2],
   forall( built_in:predicate_prolog(Built_In),
      assertz( rar:special_predicate(Built_In,
         built_in, prolog, Type_2, attr:[]:[] ) ) ),
   !.
assert_prolog_built_in_predicates(_).


/******************************************************************/


