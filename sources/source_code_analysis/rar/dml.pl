

/******************************************************************/
/***                                                            ***/
/***          RAR:  DML                                         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* reset_rar_database <-
      */

reset_rar_database :-
   write('reset of RaR database ... '),
   sca_namespace_get(DB),
   retractall( rar:rule(_, _, _, _, _, DB) ),
   retractall( rar:inverse_rule(_, _, _, _, _, DB) ),
   retractall( rar:leaf_to_path(_, _, _, DB) ),
   retractall( visur:leaf_to_path_tmp(_, _, _) ),
   retractall( visur:calls_pp_tmp(_, _, _, _) ),
   sca_variable_set(rar_db_created, ''),
   sca_variable_set(source_tree, nv:[name:'']:[]),
   sca_variable_set(exported_predicates, []),
   reset_gensym(no_head_),
   writeln(' ... done.').


/* reset_predicate_groups <-
      */

reset_predicate_groups :-
   write('reset of special predicates ... '),
   retractall( rar:special_predicate(_, _, _, _, _) ),
   writeln(' ... done.').


/* reset_gxl_database <-
      */

reset_gxl_database :-
   sca_namespace_get(DB),
   write('reset of gxl database ... '),
   reset_gensym(meta),
   reset_gensym(body),
   retractall( rar:gxl_node(_, _, _, DB) ),
   retractall( rar:gxl_edge(_, _, _, _, _, DB) ),
   writeln(' ... done.').


/* reset_dtd_database <-
      */

reset_dtd_database :-
   sca_namespace_get(DB),
   retractall( rar:dtd_element(_, _, _, DB) ),
   retractall( rar:dtd_attribute(_, _, _, DB) ),
   retractall( rar:dtd_edge(_, _, _, _, _, DB) ).


/* reset_temporary_dbs <-
      */

reset_temporary_dbs :-
   file_statistics:reset_file_statistics_db,
   topology_db:reset_topology_db,
   topology_db:reset_scc_db,
   spectra_db:reset_spectra_db,
   reset_polar_db,
   reset_dtd_database,
   reset_gxl_database,
   nl,
   !.


/* set_rar_db_time_variable(+prolog|dislog|php|java|sca_tests) <-
      */

set_rar_db_time_variable(Type) :-
   current_date_and_time_to_string(Date),
   current_prolog_flag(xpce_version, XPCE_Version),
   sca_variable_set(rar_db_created, (Type, Date, XPCE_Version)).


/* insert_rule(+File, +Rule) <-
      */

insert_rule(File, Rule) :-
   [M, P, A] := Rule^head^atom@[module, predicate, arity],
   insert_rule(File, (M:P)/A, Rule).


/* insert_rule(+File, +MPA, +Rule) <-
      */

insert_rule(File_A, MPA_A, Rule) :-
   insert(rule, MPA_A, File_A, Rule),
   ( forall( rar:calls_pp(Rule, MPA_A, MPA_B),
        rar:insert(inverse_rule, MPA_B, File_A, MPA_A) )
   ),
   !.


/* delete_rules(+File) <-
      */

delete_rules(File) :-
   hash_term(File, Hash),
   sca_namespace_get(DB),
   retractall( rule(_, Hash, _, File, _, DB) ),
   retractall( inverse_rule(_, Hash, _, File, _, DB) ).


/* select(Type, V, Object) <-
      */

select(Type, V, Object) :-
   sca_namespace_get(DB),
   hash_term(V, H),
   Structure =.. [Type, H, V, Object, DB],
   call(rar:Structure).


/* select(Type, V1, V2, Object) <-
      */

select(Type, V1, V2, Object) :-
   sca_namespace_get(DB),
   hash_term(V1, H1),
   hash_term(V2, H2),
   Structure =.. [Type, H1, H2, V1, V2, Object, DB],
   call(rar:Structure).


/*** implementation ***********************************************/


/* insert(+Type, +Index, +Object) <-
      */

insert(Type, V, Object) :-
   sca_namespace_get(DB),
   hash_term(V, H),
   Structure =.. [Type, H, V, Object, DB],
   assertz(rar:Structure),
   !.


/* insert(+Type, +Index_1, +Index_2, +Object) <-
      */

insert(Type, V1, V2, Object) :-
   sca_namespace_get(DB),
   hash_term(V1, H1),
   hash_term(V2, H2),
   Structure =.. [Type, H1, H2, V1, V2, Object, DB],
   assertz(rar:Structure),
   !.


/* update(+Type, +Index, +Object) <-
      */

update(Type, V, _Object) :-
   select(Type, V, _),
   !.
update(Type, V, Object) :-
   insert(Type, V, Object),
   !.


/* update(+Type, +Index_1, +Index_2, +Object) <-
      */

update(Type, V1, V2, _Object) :-
   select(Type, V1, V2, _),
   !.
update(Type, V1, V2, Object) :-
   insert(Type, V1, V2, Object),
   !.


/* replace_or_insert(+Type, +V, +Object) <-
      */

replace_or_insert(Type, V, Object) :-
   sca_namespace_get(DB),
   hash_term(V, H),
   Structure_1 =.. [Type, H, V, _, DB],
   Structure_2 =.. [Type, H, V, Object, DB],
   retractall( rar:Structure_1 ),
   assertz(rar:Structure_2).


delete(Type, V, Object) :-
   hash_term(V, H),
   sca_namespace_get(DB),
   Structure =.. [Type, H, V, Object, DB],
   retractall( Structure ).

delete(Type, V, W, Object) :-
   hash_term(V, H_1),
   hash_term(W, H_2),
   sca_namespace_get(DB),
   Structure =.. [Type, H_1, H_2, V, W, Object, DB],
   retractall( Structure ).


/******************************************************************/


