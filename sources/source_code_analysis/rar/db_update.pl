

/******************************************************************/
/***                                                            ***/
/***  RAR:  Update                                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* update_rar_database <-
      */

update_rar_database :-
   determine_runtime( (
      sca_variable_get(source_tree, Tree_1),
      rar:reset_temporary_dbs,
      rar:get_changed_files(Tree_1, U_Files, A_Files, D_Files),
      rar:message('changed files', U_Files),
      rar:message('new added files', A_Files),
      rar:message('deleted files', D_Files),
      writeln('beginning incremental update now ... '),
      rar:delete_files_from_tree(D_Files, Tree_1, Tree_2),
      rar:delete_files_from_db(D_Files),
      ord_union(U_Files, A_Files, UA_Files),
      rar:update_files_of_db(Tree_2, UA_Files, Tree_3),
      rar:add_date_to_files(Tree_3, Tree_4),
      sca_namespace_get(DB),
      retractall( leaf_to_path(_, _, _, DB) ),
      rar:reverse_tree(Tree_4),
      sca_variable_set(source_tree, Tree_4) ) ),
   writeln('... done.').


/*** implementation ***********************************************/


/* get_changed_files(+Tree, -Update_Files,
      -Add_Files, -Delete_Files, -Consulted_Files) <-
      */

get_changed_files(Tree, Update_Files, Add_Files, Delete_Files) :-
   Root_1 := Tree@root,
   tilde_to_home_path(Root_1, Root_2),
   tree_to_files_xml(Tree, Hierarchy_Files),
   Hierarchy_Files = files:_:File_List,
   findall( Path,
      ( member(File, File_List),
        Path := File@path,
        not( _ := File@date ) ),
      Add_Files ),
   findall( Rel_Path,
      ( Rel_Path := Hierarchy_Files^file@path,
        filename:absolute_filename(Root_2, Rel_Path, Abs_Path),
        not( exists_file_with_tilde(Abs_Path)) ),
      Delete_Files ),
   findall( CF,
      ( File := Hierarchy_Files^file,
        [Rel_Path, Date] := File@[path, date],
        filename:absolute_filename(Root_2, Rel_Path, Abs_Path),
        time_file_to_atom(Abs_Path, Actual_Date),
        Date \= Actual_Date,
        ( X := File@consulted_from ->
          CF = X
        ; CF = Rel_Path )
      ),
      CFs ),
   list_to_ord_set(CFs, Update_Files).


/* delete_files_from_db(+Files) <-
      */

delete_files_from_db(Files) :-
   forall( member(File, Files),
      rar:delete_rules(File) ).


/* update_files_of_db(+Tree_1, +Files, -Tree_3) <-
      */

update_files_of_db(Tree_1, U_Files_1, Tree_3) :-
   sca_variable_set(tmp_tree, Tree_1),
   filename:absolute_filenames(U_Files_1, U_Files_2),
   maplist( program_file_to_xml,
      U_Files_2, Xmls ),
   maplist( xml_consults_files_2,
      Xmls, CFiles_1),
   union(CFiles_1, CFiles_2),
   append(U_Files_1, CFiles_2, Concerned_Files_1),
   list_to_ord_set(Concerned_Files_1, Concerned_Files_2),
   delete_files_from_db(Concerned_Files_2),
   sca_variable_get(exported_predicates, Exp_Preds),
   Root_1 := Tree_1@root,
   tilde_to_home_path(Root_1, Root_2),
   checklist( xml_file_to_db(Root_2, Exp_Preds),
      Xmls ),
   sca_variable_get(tmp_tree, Tree_2),
   iterate_list(
      rar:add_mpas_to_file_content_p,
         Tree_2, Concerned_Files_2, Tree_3).


/* xml_consults_files_2(+Xml, -Files) <-
      */

xml_consults_files_2(Xml, Files) :-
   File := Xml@path,
   xml_consults_files(File, Xml, Files).


/* message(+Headline, +List) <-
      */

message(Headline, List_1) :-
   write_list(['--- ', Headline, ' ---', '\n']),
   maplist( concat('  '),
      List_1, List_2 ),
   writeln_list(List_2),
   nl.


/******************************************************************/


