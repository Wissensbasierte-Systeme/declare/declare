

/******************************************************************/
/***                                                            ***/
/***       DisLog:  XML to Prolog                               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xml_to_prolog(Xml, File_Prolog) <-
      */

xml_file_to_prolog(File_Xml, File_Prolog) :-
   dread_(xml, File_Xml, Xml),
   xml_to_prolog_2(Xml, File_Prolog).

xml_to_prolog_2(Xml, File_Prolog) :-
   Rules := Xml^file,
   write_list(['---> ', File_Prolog]), nl,
   predicate_to_file( File_Prolog,
      xml_to_prolog:xml_rules_to_prolog(Rules) ).


/* xml_get_prefix_symbols(Type, Symbols) <-
      */

xml_get_prefix_symbols(without_space, ['@', '^', ':']).
xml_get_prefix_symbols(with_space, [
   'or', '-', '+', ':=', '^',
   '==', '\\=', '=', '>', '<', '=<', '>=', 'is' ] ).


/*** implementation ***********************************************/


/* xml_rules_to_prolog(Rules) <-
      */

xml_rules_to_prolog([Rule_1, Rule_2|Rules]) :-
   xml_rule_to_prolog(Rule_1),
   xml_insert_newlines(Rule_1, Rule_2),
   xml_rules_to_prolog([Rule_2|Rules]).
xml_rules_to_prolog([Rule]) :-
   xml_rule_to_prolog(Rule).
xml_rules_to_prolog([]).


/* xml_rule_to_prolog(Rule) <-
      */

xml_rule_to_prolog(Rule) :-
   head:[]:Head := Rule^head,
   body:[]:[] := Rule^body,
   !,
   xml_atoms_to_prolog(0, 1, 0, ';', Head),
   writeln('.').
xml_rule_to_prolog(Rule) :-
   head:[]:Head := Rule^head,
   body:[]:Body := Rule^body,
   xml_atoms_to_prolog(0, 1, 0, ';', Head),
   xml_rule_operator_to_prolog(Rule),
   xml_atoms_to_prolog(3, 0, 1, ',', Body),
   writeln('.').


/* xml_atoms_to_prolog(Tab, Space, NL, Junctor, Xmls) <-
      */

xml_atoms_to_prolog(Tab, _, _, _, [Xml]) :-
   tab(Tab),
   xml_atom_to_prolog(Xml).
xml_atoms_to_prolog(Tab, Space, NL, Junctor, [Xml|Xmls]) :-
   tab(Tab),
   xml_atom_to_prolog(Xml),
   write(Junctor),
   tab(Space),
   xml_write_new_line(NL),
   xml_atoms_to_prolog(Tab, Space, NL, Junctor, Xmls).
xml_atoms_to_prolog(_, _, _, _, []).

xml_write_new_line(1) :-
  nl.
xml_write_new_line(0).


/* xml_rule_operator_to_prolog(Rule) <-
      */

xml_rule_operator_to_prolog(Rule) :-
   head:[]:[] := Rule^head,
   Operator := Rule@operator,
   write(Operator).
xml_rule_operator_to_prolog(Rule) :-
   Operator := Rule@operator,
   write_list([' ', Operator]),
   nl.
xml_rule_operator_to_prolog(_) :-
   writeln(' :-').


/* xml_atom_to_prolog_atom(Xml, Atom) <-
      */

xml_atom_to_prolog_atom(Xml, Atom) :-
   dislog_variable_get(output_path, Results),
   concat(Results, 'prolog_atom.tmp', Path),
   predicate_to_file( Path,
      ( xml_to_prolog:xml_atom_to_prolog(Xml),
        write('.') ) ),
   predicate_from_file( Path,
      read(Atom) ).


/* xml_atom_to_prolog(Xml) <-
      */

xml_atom_to_prolog(Xml) :-
   [not, 1] := Xml@[predicate, arity],
   Xml = atom:_:[C],
   ( F := C@functor,
     member(F, [',', ';']),
     write('not'),
     xml_tuple_to_prolog(C)
   ; write('not( '),
     xml_tuple_to_prolog(C),
     write(' )') ).
xml_atom_to_prolog(Xml) :-
   P := Xml@predicate,
   Xml = atom:_:Content,
   xml_meta_to_prolog(P, Content).
xml_atom_to_prolog(Xml) :-
   ['.', 2] := Xml@[predicate, arity],
   Xml = atom:_:Content,
   xml_list_to_prolog(term:[functor:'.']:Content).
xml_atom_to_prolog(Xml) :-
   P := Xml@predicate,
   Xml = atom:_:Content,
   xml_get_prefix_symbols(Space, Prefix),
   member(P, Prefix),
   xml_prefix_to_infix(Space, term:[functor:P]:Content).
xml_atom_to_prolog(Xml) :-
   P := Xml@predicate,
   Xml = atom:_:[],
   writeq(P).
xml_atom_to_prolog(Xml) :-
   [',', 2] := Xml@[predicate, arity],
   Xml = atom:_:Content,
   xml_tuple_to_prolog(term:[functor:',']:Content).
xml_atom_to_prolog(Xml) :-
   [';', 2] := Xml@[predicate, arity],
   Xml = atom:_:Content,
   xml_tuple_to_prolog(term:[functor:';']:Content).
xml_atom_to_prolog(Xml) :-
   P := Xml@predicate,
   Xml = atom:_:Content,
   xml_functor_and_arguments_to_prolog(P, Content).

xml_atom_to_prolog(Xml) :-
   Functor := Xml@functor,
%  Xml = term:[functor:Functor]:_,
   xml_get_prefix_symbols(Space, Prefix),
   member(Functor, Prefix),
   xml_prefix_to_infix(Space, Xml).
xml_atom_to_prolog(Xml) :-
   Functor := Xml@functor,
   Xml = term:_:[],
   xml_write_functor(Functor).
xml_atom_to_prolog(Xml) :-
   Functor := Xml@functor,
   Functor = '.',
%  Xml = term:[functor:'.']:_,
   xml_list_to_prolog(Xml).
xml_atom_to_prolog(Xml) :-
   Functor := Xml@functor,
   Functor = ',',
%  Xml = term:[functor:',']:_,
   xml_tuple_to_prolog(Xml).
xml_atom_to_prolog(Xml) :-
   Functor := Xml@functor,
   Xml = term:_:Content,
   xml_functor_and_arguments_to_prolog(Functor, Content).

xml_atom_to_prolog(Xml) :-
   _ := Xml@junctor,
%    Xml = formula:[junctor:_]:_,
   xml_formula_to_prolog(Xml).

xml_atom_to_prolog(Xml) :-
   Xml = var:[name:P]:[],
   write(P).

xml_atom_to_prolog(Xml) :-
   writelq('not identified construct 2':Xml).


/* xml_functor_and_arguments_to_prolog(Functor, Content) <-
      */

xml_functor_and_arguments_to_prolog(Functor, Content) :-
   writeq(Functor),
   write('('),
   xml_atoms_to_prolog(0, 1, 0, ',', Content),
   write(')').


/* xml_meta_to_prolog(Meta, List) <-
      */

xml_meta_to_prolog(Meta, [A, B]) :-
   member(Meta, [checklist, forall]),
   xml_meta_to_prolog_1(Meta, A, B),
   write(' )').
xml_meta_to_prolog(maplist, [A, B, C]) :-
   xml_meta_to_prolog_1(maplist, A, B),
   write(', '),
   xml_atom_to_prolog(C),
   write(' )').
xml_meta_to_prolog(findall, [A, B, C]) :-
   xml_meta_to_prolog_1(findall, A, B),
   writeln(', '),
   tab(6),
   xml_atom_to_prolog(C),
   write(' )').

xml_meta_to_prolog_1(Meta, A, B) :-
   write_list([Meta, '( ']),
   xml_atom_to_prolog(A),
   writeln(','),
   tab(6),
   xml_atom_to_prolog(B).


/* xml_formula_to_prolog(Formula) <-
      */

xml_formula_to_prolog(Formula) :-
   Formula =
      formula:[junctor: (or)]:[
         formula:[junctor:if_then]:[A, B], C],
   write('( '),
   xml_atom_to_prolog(A),
   write(' ->'),
   nl,
   tab(5),
   xml_atom_to_prolog(B),
   nl,
   tab(3),
   write('; '),
   xml_atom_to_prolog(C),
   write(' )').

xml_formula_to_prolog(Body) :-
   Body = formula:[junctor: (or)]:[A, B],
   write('( '),
   xml_atom_to_prolog(A),
   nl,
   tab(3),
   write('; '),
   xml_atom_to_prolog(B),
   write(' )').


/* xml_tuple_to_prolog(Xml) <-
      */

xml_tuple_to_prolog(Xml) :-
   Xml = term:[functor:';']:[A, B],
   write('( '),
   xml_tuple_to_prolog(A),
   write(' ; '),
   xml_tuple_to_prolog(B),
   write(' )').
xml_tuple_to_prolog(Xml) :-
   Xml = term:[functor:',']:B,
   write('( '),
   xml_tuple_to_prolog_2(B),
   write(' )').
xml_tuple_to_prolog(Xml) :-
   xml_atom_to_prolog(Xml).

xml_tuple_to_prolog_2([A, term:[functor:',']:D]) :-
   xml_atom_to_prolog(A),
   write(', '),
   xml_tuple_to_prolog_2(D).
xml_tuple_to_prolog_2([A, B]) :-
   xml_atom_to_prolog(A),
   write(', '),
   xml_atom_to_prolog(B).


/* xml_list_to_prolog(Xml) <-
      */

xml_list_to_prolog(Xml) :-
   Xml = term:[functor:'.']:B,
   write('['),
   xml_list_to_prolog_1(B),
   write(']').

xml_list_to_prolog_1([A, term:[functor:'.']:D]) :-
   xml_atom_to_prolog(A),
   write(', '),
   xml_list_to_prolog_1(D).
xml_list_to_prolog_1([A, term:[functor:'[]']:[]]) :-
   xml_atom_to_prolog(A).
xml_list_to_prolog_1([A, B]) :-
   xml_atom_to_prolog(A),
   write('|'),
   xml_atom_to_prolog(B).


/* xml_prefix_to_infix(Space, List) <-
      */

xml_prefix_to_infix(Space, List) :-
   List = term:[functor:Op]:A,
   A = [B, C],
   xml_prefix_to_infix_1(B),
   xml_write_operator(Space, Op),
   xml_prefix_to_infix_1(C).
/*
xml_prefix_to_infix(Space, List) :-
   List = [ formula:[junctor:Op]:A ],
   A = [B, C],
   tab(Tab),
   xml_prefix_to_infix_1(Tab, B),
   xml_write_operator(Space, Op),
   xml_prefix_to_infix_1(Tab, C).
*/
xml_prefix_to_infix_1(_, X) :-
   X = _Label_1:[_Label_2:F]:[],
   write(F).
/*
xml_prefix_to_infix_1(_, X) :-
   X = atom:[_Label_2:F]:_,
   xml_atom_to_prolog(X).
%  write(F).
*/
xml_prefix_to_infix_1(Label_1:[Label_2:Op]:C) :-
   xml_atom_to_prolog(Label_1:[Label_2:Op]:C).

xml_write_operator(with_space, Op) :-
   write_list([' ', Op, ' ']).
xml_write_operator(without_space, Op) :-
   writeq(Op).


/* xml_insert_newlines(Rule_1, Rule_2) <-
      */

xml_insert_newlines(Rule_1, Rule_2) :-
   Pred_1 := Rule_1^head^atom@predicate,
   Pred_2 := Rule_2^head^atom@predicate,
   ( Pred_1 = Pred_2 ->
     true
   ; nl ).
xml_insert_newlines(_, _) :-
   nl, nl.


/* xml_write_functor(Functor) <-
      */

xml_write_functor(Functor) :-
   name(Functor, Ns),
   forall( member(N, Ns),
      ( 48 =< N, N =< 57 ) ),
   !,
   writeq(Functor).
xml_write_functor(Functor) :-
%  writeq(Functor).
   write_list(['''', Functor, '''']).


/******************************************************************/


