

/******************************************************************/
/***                                                            ***/
/***          RAR:  Tests                                       ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(source_code_analysis:rar_new, contains_1) :-
   rar:init(sca_tests_1),
   sca_variable_get(rar_test_files, [Cross, Desc, Necess, Test]),
   findall( File,
      rar:contains_ll(unit:rar, file:File),
      Files ),
   Files = [Test, Cross, Desc, Necess],
   findall( Predicate,
      rar:defines_fp(Cross, Predicate),
      Predicates_1 ),
   list_to_ord_set(Predicates_1, Predicates_2),
   Predicates_2 = [
      (user:in_same_module)/2,
      (user:rar_predicate_to_cross_calls)/2 ],
   rar:contains_lp(
      module:Module, (user:rar_predicate_to_cross_calls)/2),
   Module = 'sources/source_code_analysis/rar/test_1'.

test(source_code_analysis:rar_new, defines) :-
   rar:init(sca_tests_1),
   sca_variable_get(rar_test_files, [Cross, _, _, _]),
   findall( Predicate,
      rar:defines_fp(Cross, Predicate),
      Predicates_1 ),
   list_to_ord_set(Predicates_1, Predicates_2),
   Predicates_2 = [ (user:in_same_module)/2,
      (user:rar_predicate_to_cross_calls)/2 ].

test(source_code_analysis:rar_new, contains_2):-
   retractall( rar:leaf_to_path(_, _, _, _) ),
   rar:insert(leaf_to_path, my_file,
      leaf_to_path:[name:my_file, level:file]:[
        leaf_to_path:[name:rar, level:module]:[
          leaf_to_path:[name:source_code_analysis, level:unit]:[
            leaf_to_path:[name:dislog, level:system]:[] ] ] ] ),
   test_1,
   test_2,
   test_3,
   test_4,
   test_5.

test_1 :-
   test_1_sub(system, System),
   System = [],
   test_1_sub(unit, Unit),
   Unit = [source_code_analysis-system-dislog],
   test_1_sub(module, Module),
   Module = [rar-unit-source_code_analysis, rar-system-dislog],
   test_1_sub(file, File),
   File = [my_file-module-rar,
      my_file-unit-source_code_analysis,
      my_file-system-dislog].

test_2 :-
   test_2_sub(my_file, File),
   File = [file-module-rar,
           file-unit-source_code_analysis,
           file-system-dislog],
   test_2_sub(rar, RAR),
   RAR = [module-unit-source_code_analysis,
          module-system-dislog],
   test_2_sub(source_code_analysis, SCA),
   SCA = [unit-system-dislog],
   test_2_sub(dislog, Dislog),
   Dislog = [].

test_3 :-
   test_sub_3(system, System),
   System = [dislog-unit-source_code_analysis,
      dislog-module-rar,
      dislog-file-my_file],
   test_sub_3(unit, Unit),
   Unit = [source_code_analysis-module-rar,
           source_code_analysis-file-my_file],
   test_sub_3(module, Module),
   Module = [rar-file-my_file],
   test_sub_3(file, File),
   File = [].

test_4 :-
   test_sub_4(dislog, Dislog),
   Dislog =[system-unit-source_code_analysis,
            system-module-rar,
            system-file-my_file],
   test_sub_4(source_code_analysis, SCA),
   SCA = [unit-module-rar, unit-file-my_file],
   test_sub_4(rar, RAR),
   RAR = [module-file-my_file],
   test_sub_4(my_file, File),
   File = [].

test_5 :-
   findall( Level_1:Name_1-Level_2:Name_2,
      rar:contains_ll(Level_1:Name_1, Level_2:Name_2),
      Rs ),
   Rs = [module:rar-file:my_file,
         unit:source_code_analysis-file:my_file,
         system:dislog-file:my_file,
         unit:source_code_analysis-module:rar,
         system:dislog-module:rar,
         system:dislog-unit:source_code_analysis].


/*** implementation ***********************************************/


test_1_sub(Level, Result) :-
   findall( X-Level_T-Name_T,
      rar:contains_ll(Level_T:Name_T, Level:X),
      Result ).


test_2_sub(Name, Result) :-
   findall( X-Level_T-Name_T,
      rar:contains_ll(Level_T:Name_T, X:Name),
      Result ).


test_sub_3(Level_1, Rs) :-
   findall( Name_1-Level_2-Name_2,
      rar:contains_ll(Level_1:Name_1, Level_2:Name_2),
      Rs ).


test_sub_4(Name_1, Rs) :-
   findall( Level_1-Level_2-Name_2,
      rar:contains_ll(Level_1:Name_1, Level_2:Name_2),
      Rs ).


/******************************************************************/


