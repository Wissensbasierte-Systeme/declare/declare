

/******************************************************************/
/***                                                            ***/
/***          RAR:  Retrieval                                   ***/
/***                                                            ***/
/******************************************************************/


:- module( filename, []).


/*** interface ****************************************************/


/* relative_filename(+File, -Rel_File) <-
      */

relative_filename(File_1, Relative_Filename) :-
   ground(File_1),
   sca_variable_get(source_tree, Tree),
   Root_1 := Tree@root,
   tilde_to_home_path(Root_1, Root_2),
   tilde_to_home_path(File_1, File_2),
   atom_concat(Root_2, Relative_Filename, File_2),
   !.
relative_filename(File, File).


/* absolute_filenames(+Files_1, -Files_2) :-
      */

absolute_filenames(Files_1, Files_2) :-
   is_list(Files_1),
   sca_variable_get(source_tree, Tree),
   Root_1 := Tree@root,
   tilde_to_home_path(Root_1, Root_2),
   absolute_filenames(Root_2, Files_1, Files_2),
   !.

absolute_filenames(Root, Files_1, Files_3) :-
   maplist( tilde_to_home_path,
      Files_1, Files_2 ),
   maplist( absolute_filename(Root),
      Files_2, Files_3 ),
   !.

absolute_filename(_, File, File) :-
   is_absolute_file_name(File),
   !.
absolute_filename(Root_1, File, Absolute_File) :-
   tilde_to_home_path(Root_1, Root_2),
   atom_concat(Root_2, File, Absolute_File).


/* file_part_to_relative_filename(+File_Part_Name, -Relative_File_Name) <-
      */

file_part_to_relative_filename(Base_Name, File) :-
   sca_variable_get(source_tree, Tree),
   file_part_to_relative_filename(Tree, Base_Name, File).


/* check_and_complete_file(+File, +File_1, -File_2) <-
      */

check_and_complete_file(_, File, File) :-
   exists_file_with_tilde(File),
   !.
check_and_complete_file(_, File_1, File_2) :-
   file_name_extension(File_1, 'pl', File_2),
   exists_file_with_tilde(File_2),
   !.
check_and_complete_file(File, File_1, File_3) :-
   file_directory_name(File, Directory),
   file_base_name(File_1, File_2),
   concat([Directory, '/', File_2], File_3),
   exists_file_with_tilde(File_3),
   !.
check_and_complete_file(File, File_1, File_4) :-
   file_directory_name(File, Directory),
   file_base_name(File_1, File_2),
   concat([Directory, '/', File_2], File_3),
   file_name_extension(File_3, 'pl', File_4),
   exists_file_with_tilde(File_4),
   !.


/*** implementation ***********************************************/


/* file_part_to_relative_filename(+Tree, +Base_Name, -File) <-
      */

file_part_to_relative_filename(_:_:Content, File, File) :-
   member(file:Attrs:_, Content),
   File := Attrs^path,
   !.
file_part_to_relative_filename(_:_:Content, Base_Name, File) :-
   member(file:Attrs:_, Content),
   File := Attrs^path,
   concat(_, Base_Name, File),
   !.
file_part_to_relative_filename(_:_:Content, Base_Name, File) :-
   member(Element, Content),
   file_part_to_relative_filename(Element, Base_Name, File).


/******************************************************************/


