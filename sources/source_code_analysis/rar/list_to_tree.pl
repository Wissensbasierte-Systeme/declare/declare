

/******************************************************************/
/***                                                            ***/
/***                                                            ***/
/***                                                            ***/
/******************************************************************/


:- module( list_to_dag, []).


/*** interface ****************************************************/


/* list_of_lists_to_dag(+List_of_Lists, -Tree) <-
      */

list_of_lists_to_dag(List_1, Tree) :-
   list_to_set(List_1, List_2),
   list_to_dag(List_2, tree:[name:root]:[], Tree).


/* list_of_lists_to_ord_dag(+List_of_Lists, -Ord_Tree) <-
      */

list_of_lists_to_ord_dag(List_1, Tree) :-
   list_to_ord_set(List_1, List_2),
   list_to_dag(List_2, tree:[name:root]:[], Tree).


/* tree_to_table(+Title, +Columns, +Tree) <-
      */

tree_to_table(Title, Columns, Tree) :-
   new(Picture, auto_sized_picture(Title)),
   send(Picture, display, new(Table, tabular)),
   send(Table, border, 1),
   send(Table, cell_spacing, -1),
   send(Table, rules, all),
   checklist( topology:append_column(Table),
      Columns ),
   send(Table, next_row),
   tree_to_table(Table, Tree),
   send(Picture, open),
   !.


/*** implementation ***********************************************/


/* list_to_dag(+List_of_Lists, +Tree_1, -Tree_2) <-
      */

list_to_dag([], Tree, Tree).
list_to_dag([[]|Ls], tree:A_1:C_1, tree:A_3:C_3) :-
   list_to_dag(Ls, tree:A_1:C_1, tree:A_3:C_3).
list_to_dag([[E|Es]|Ls], tree:A_1:C_1, tree:A_4:C_4) :-
   member(tree:A_2:C_2, C_1),
   memberchk(name:E, A_2),
   delete(C_1, tree:A_2:C_2, C_5),
   list_to_dag([Es], tree:A_2:C_2, tree:A_3:C_3),
   append(C_5, [tree:A_3:C_3], C),
   list_to_dag(Ls, tree:A_1:C, tree:A_4:C_4),
   !.
list_to_dag([L|Ls], tree:A_1:C_1, tree:A_3:C_3) :-
   l_to_d(L, Tree),
   append(C_1, [Tree], C_2),
   list_to_dag(Ls, tree:A_1:C_2, tree:A_3:C_3),
   !.

l_to_d([], tree:[]:[]).
l_to_d([E], tree:[name:E]:[]).
l_to_d([E|Es], tree:[name:E]:[Tree]) :-
   l_to_d(Es, Tree).


/* convert_structure(+A, -B) <-
      */

convert_structure(A, B) :-
   maplist( convert_structure_s,
      A, B ).

convert_structure_s(A-B:C, [A, B, C]).
convert_structure_s(A:B-C, [A, B, C]).
convert_structure_s(A:B, [A, B]).


/* tree_to_table(+Table, +Tree) <-
      */

tree_to_table(Table, tree:_:[C|Cs]) :-
   tree_to_table(0, 0, Table, C),
   checklist( tree_to_table(1, 0, Table),
      Cs ),
   !.
tree_to_table(_, tree:_:[]).

tree_to_table(Ident, Column_No, Table, tree:A:[]) :-
   add_empty_columns(Ident, Column_No, Table),
   memberchk(name:Name, A),
   term_to_atom_sure_extended(Name, Name_Atom),
   send_list(Table, [
      append(Name_Atom),
      next_row] ),
   !.
tree_to_table(Ident, Column_No_1, Table, tree:A:[C|Cs]) :-
   add_empty_columns(Ident, Column_No_1, Table),
   memberchk(name:Name, A),
   term_to_atom_sure_extended(Name, Name_Atom),
   send(Table, append(Name_Atom)),
   Column_No_2 is Column_No_1 + 1,
   tree_to_table(0, Column_No_2, Table, C),
   checklist( tree_to_table(1, Column_No_2, Table),
      Cs ),
   !.

add_empty_columns(0, _, _) :-
   !.
add_empty_columns(_, 0, _) :-
   !.
add_empty_columns(Ident, No_Column_1, Table) :-
   send(Table, append('')),
   No_Column_2 is No_Column_1 - 1,
   add_empty_columns(Ident, No_Column_2, Table).




/******************************************************************/


