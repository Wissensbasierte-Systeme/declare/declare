

/******************************************************************/
/***                                                            ***/
/***          RAR:  calls_ll                                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* calls_ff(+File_1, -File_2) <-
      */

calls_ff(File_1, File_2) :-
   calls_ff(File_1, _, File_2, _).


/* calls_ff(?File_1, ?P_1, ?File_2, ?P_2) <-
      */

calls_ff(File_1, P_1, File_2, P_2) :-
   select(rule, P_1, File_1, Rule),
   calls_pp(Rule, P_1, P_2),
   select(rule, P_2, File_2, _).


/* calls_ff_inv(+File_2, -File_1) <-
      */

calls_ff_inv(File_2, File_1) :-
   calls_ff_inv(File_2, _, File_1, _).


/* calls_ff_inv(?File_2, ?P_2, -File_1, -P_1) <-
      */

calls_ff_inv(File_2, P_2, File_1, P_1) :-
   ( ground(File_2)
   ; ground(P_2) ),
   var(File_1),
   select(rule, P_2, File_2, _),
   rar:calls_pp_inv(P_2, File_1, P_1).


/* calls_ll(?Level_1:Name_1, ?Level_2:Name_2) <-
      */

calls_ll(Level_Name_1, Level_Name_2) :-
   ground(Level_Name_1),
   !,
   rar:contains_lp(Level_Name_1, P_1),
   rar:calls_pp(P_1, P_2),
   rar:contains_lp(Level_Name_2, P_2).
calls_ll(Level_Name_1, Level_Name_2) :-
   ground(Level_Name_2),
   !,
   rar:contains_lp(Level_Name_2, P_2),
   rar:calls_pp_inv(P_2, P_1),
   rar:contains_lp(Level_Name_1, P_1).


/* who_calls_ff(?File_1, ?File_2, -Call_List) <-
      */

who_calls_ff(File_1, File_3, Call_List) :-
   ground(File_1),
   findall( File_2-(P_1-P_2),
      calls_ff(File_1, P_1, File_2, P_2),
      Files_1 ),
   make_set_of_ordered_calls(Files_1, Call_List_2),
   member(File_3-Call_List, Call_List_2).
who_calls_ff(File_1, File_2, Call_List) :-
   ground(File_2),
   findall( File-(P_1-P_2),
      calls_ff_inv(File_2, P_2, File, P_1),
      Files_1 ),
   make_set_of_ordered_calls(Files_1, Call_List_1),
   member(File_1-Call_List, Call_List_1).


/* who_calls(+Level_1:Name_1, ?Level_2:Name_2, -Call_List) <-
      */

who_calls(Level_1:File_1, Level_2:File_2, Call_List) :-
   ground(Level_1),
   ground(Level_2),
   Level_1 = file,
   Level_2 = file,
   !,
   who_calls_ff(File_1, File_2, Call_List).
who_calls(Level_1:Name_1, Level_2:Name_2, Call_List) :-
   findall( Name_2-(P_1-P_2),
      ( contains_lp(Level_1:Name_1, P_1),
        calls_pp(P_1, P_2),
        contains_lp(Level_2:Name_2, P_2) ),
      Call_List_Temp ),
   make_set_of_ordered_calls(Call_List_Temp, Ordered_Calls),
   member(Name_2-Call_List, Ordered_Calls).


/*** implementation ***********************************************/


/* make_set_of_ordered_calls(+List_I, -List_O) <-
      */

make_set_of_ordered_calls(List_I, List_O) :-
   findall( Name_2,
      member(Name_2-_, List_I),
      L_1),
   list_to_ord_set(L_1, L_2),
   maplist( make_set_of_ordered_calls_step(List_I),
      L_2, List_O).

make_set_of_ordered_calls_step(List_I, L_2, L_2-Calls_O) :-
   findall( P,
      member(L_2-P, List_I),
      Calls ),
   list_to_ord_set(Calls, Calls_O).


/******************************************************************/


