

/******************************************************************/
/***                                                            ***/
/***      correct jaml file                                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* correct_jaml_files <-
      */

correct_jaml_files(Jaml_Files) :-
   length(Jaml_Files, L),
   sca_variable_set(counter, 0),
   sca_variable_set(defect_files, []),
   checklist( correct_jaml_file(L),
      Jaml_Files ),
   sca_variable_get(defect_files, Files),
   writeln_list(Files).


/*** implementation ***********************************************/


/* correct_jaml_file(+L, +File) <-
      */

correct_jaml_file(L, File) :-
   dread_(xml, File, Jaml),
   sca_variable_get(counter, C),
   C_2 is C +1,
   sca_variable_set(counter, C_2),
   writeln(C_2/L),
   correct_jaml(File, Jaml).

correct_jaml(File, [XML, jaml:[]:[]]) :-
   dwrite(xml, File, XML),
   !.
correct_jaml(File, Jaml) :-
   length(Jaml, L),
   sca_variable_get(defect_files, Files_1),
   Files_2 = [(File, L)|Files_1],
   sca_variable_set(defect_files, Files_2).


/******************************************************************/


