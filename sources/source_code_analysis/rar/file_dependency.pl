

/******************************************************************/
/***                                                            ***/
/***                                                            ***/
/***                                                            ***/
/******************************************************************/


:- module( file_dependency, []).


/*** interface ****************************************************/


/* file_calls_external_mpas(+File, -MPA_F_MPAs) <-
      */

file_calls_external_mpas(File, MPA_F_MPAs_2) :-
   findall( File_MPA,
      file_calls_external_mpa(File, File_MPA),
      MPA_F_MPAs_1 ),
   list_to_ord_set(MPA_F_MPAs_1, MPA_F_MPAs_2).


/* directive_of_file_calls_external_mpas(+File, -MPAs) <-
      */

directive_of_file_calls_external_mpas(File, MPAs_2) :-
   findall( MPA,
      directive_of_file_calls_external_mpa(File, MPA),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2).


/* file_called_from_external_mpas(+File, -File_MPAs) <-
      */

file_called_from_external_mpas(File_A, File_MPAs_2) :-
   findall( File_B:MPA_B-MPA_A,
      mpas_rules_calls:file_mpa_is_external_called(
         File_A:MPA_A, File_B:MPA_B),
      File_MPAs_1 ),
   list_to_ord_set(File_MPAs_1, File_MPAs_2).


/*** implementation ***********************************************/


/* file_calls_external_mpa(+File, -MPA_A_File_B_MPA_B) <-
      */

file_calls_external_mpa(File_A, MPA_A-File_B:MPA_B) :-
   mpas_rules_calls:file_mpa_to_external_calls(
      File_A:MPA_A, File_B:MPA_B),
   not( rar:is_directive(MPA_A) ).


/* directive_of_file_calls_external_mpa(
      +File, -MPA_A_File_B_MPA_B) <-
      */

directive_of_file_calls_external_mpa(File_A, MPA_A-File_B:MPA_B) :-
   rar:select(rule, MPA_A, File_A, Rule),
   rar:is_directive(MPA_A),
   rar:calls_pp(Rule, MPA_A, MPA_B),
   rar:select(rule, MPA_B, File_B, _),
   not( File_A = File_B ).


/* is_file_containing_directive_with_external_call(?File) <-
      */

is_file_containing_directive_with_external_call(File_1) :-
   rar:select(rule, MPA_1, File_1, Rule),
   rar:is_directive(MPA_1),
   rar:calls_pp(Rule, _, MPA_2),
   rar:select(rule, MPA_2, File_2, _),
   not( File_1 = File_2 ).


/******************************************************************/


