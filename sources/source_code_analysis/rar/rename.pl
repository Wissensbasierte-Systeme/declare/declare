

/******************************************************************/
/***                                                            ***/
/***  Renaming DisLog Files                                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


add_pl_extension_to_dislog_files :-
   dislog_sources(Rel_Files),
   dislog_variable_get(source_path, Source_Path),
   !,
   maplist( add_source_path(Source_Path),
      Rel_Files, Absolute_Files),
   sublist( has_no_pl_ext,
      Absolute_Files, Has_no_pl_ext ),
   sublist( pl_file_does_not_yet_exist,
      Has_no_pl_ext, To_Rename ),
   checklist( add_pl_extension_to_dislog_file,
      To_Rename ).


/*** implementation ***********************************************/


/* add_source_path(+Source_Path, +Rel_File, -Abs_File) <-
      */

add_source_path(Source_Path, Rel_File, Abs_File) :-
   concat(Source_Path, Rel_File, Abs_File).


/* has_no_pl_ext(+File) <-
      */

has_no_pl_ext(File) :-
   file_name_extension(_, '', File).


/* pl_file_does_not_yet_exist(+File) <-
      */

pl_file_does_not_yet_exist(File) :-
   file_name_extension(File, 'pl', File_PL),
   not( exists_file_with_tilde(File_PL) ).


/* add_pl_extension_to_dislog_file(+File) <-
      */

add_pl_extension_to_dislog_file(File) :-
   file_name_extension(File, pl, File_PL),
   not( exists_file_with_tilde(File_PL) ),
   rename_file(File, File_PL),
   !.
add_pl_extension_to_dislog_file(File) :-
   writeln('Error during processing':File).


/******************************************************************/


