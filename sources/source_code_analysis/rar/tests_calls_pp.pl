

/******************************************************************/
/***                                                            ***/
/***  calls_pp: Test                                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(source_code_analysis:rar_new, calls_pp_1) :-
   rar:init(sca_tests_2),
   findall( MPA,
      rar:calls_pp((rar_test_1:a)/2, MPA),
      MPAs ),
%  writelq(MPAs),
   MPAs = [ (user:forall)/2,
      (user:member)/2,
      (user:writelq)/1,
      (rar_test_2:tam_1)/1,
      (user: (=))/2],
   setof( MPA_2,
      rar:calls_pp((rar_test_1:b)/2, MPA_2),
      MPAs_2 ),
%  writelq(MPAs_2),
   MPAs_2 = [ (rar_test_2:tam_2)/1,
      (user:(','))/2,
      (user:findall)/3,
      (user:member)/2,
      (user:member_x)/2,
      (user:writeln)/1,
      (user:writelq)/1].

test(source_code_analysis:rar_new, calls_pp_2) :-
   rar:init(sca_tests_2),
   rar:calls_pp(Head_1, (rar_test_2:tam_2)/1),
%  writelq(Head_1),
   Head_1 = (rar_test_1:b)/2,
   rar:calls_pp(Head_2, (user:member_x)/2),
%  writelq(Head_2),
   Head_2 = (rar_test_1:b)/2.

test(source_code_analysis:rar_new, calls_pp_3) :-
   rar:init(sca_tests_2),
   findall( MPA,
      ( rar:select(rule, _, _, Rule),
        rar:calls_pp(Rule, (rar_test_1:a)/2, MPA) ),
      MPAs ),
%  writelq(MPAs),
   MPAs = [ (user:forall)/2,
      (user:member)/2,
      (user:writelq)/1,
      (rar_test_2:tam_1)/1,
      (user: (=))/2],
   setof( MPA_2,
      ( rar:select(rule, _, _, Rule),
        rar:calls_pp(Rule, (rar_test_1:b)/2, MPA_2) ),
      MPAs_2 ),
%  writelq(MPAs_2),
   MPAs_2 = [ (rar_test_2:tam_2)/1,
      (user:(','))/2,
      (user:findall)/3,
      (user:member)/2,
      (user:member_x)/2,
      (user:writeln)/1,
      (user:writelq)/1].

test(source_code_analysis:rar_new, calls_pp_4) :-
   rar:init(sca_tests_2),
   rar:select(rule, _, _, Rule_1),
   rar:calls_pp(Rule_1, Head_1, (rar_test_2:tam_2)/1),
%  writelq(Head_1),
   Head_1 = (rar_test_1:b)/2,
   rar:select(rule, _, _, Rule_2),
   rar:calls_pp(Rule_2, Head_2, (user:member_x)/2),
%  writelq(Head_2),
   Head_2 = (rar_test_1:b)/2.


test(source_code_analysis:rar_new, rule_calls_pp) :-
   rar:init(sca_tests_1),
   rar:select(rule, (user:rar_predicate_to_cross_calls)/2, _, Rule),
   Rule =
      rule:[module:user]:[
      body:[]:[
            atom:[arity:2, module:user,
               predicate:rar_predicate_to_descendants]:[
               var:[name:'Predicate']:[], var:[name:'Descendants']:[]],
            atom:[arity:3, module:user, predicate:findall]:[
            var:[name:'P']:[],
            term:[arity:2, functor: (','), module:user]:[
               term:[arity:2, functor:member, module:user]:[
                  var:[name:'P']:[],
                  var:[name:'Descendants']:[]],
               term:[arity:1, functor:not, module:user, predicate:not]:[
                  term:[arity:2, functor:in_same_module, module:user]:[
                     var:[name:'Predicate']:[],
                     var:[name:'P']:[]]]],
               var:[name:'Predicates']:[]]],
         head:[]:[atom:[module:user,
            predicate:rar_predicate_to_cross_calls, arity:2]:[
               var:[name:'Predicate']:[],
               var:[name:'Predicates']:[]]]],
   findall( Predicate,
      rar:calls_pp(Rule, _, Predicate),
      Predicates_1 ),
   Predicates_1 = [ (user:rar_predicate_to_descendants)/2,
      (user:findall)/3,
      (user: (','))/2,
      (user:member)/2,
      (user:not)/1,
      (user:in_same_module)/2],
   findall( Predicate,
      rar:calls_pp(
         (user:rar_predicate_to_cross_calls)/2,
         Predicate),
      Predicates_2 ),
   Predicates_2 = Predicates_1,
   !.


test(source_code_analysis:rar_new, calls_pp) :-
   rar:init(sca_tests_2),
   determine_runtime((
      not( rar:calls_pp((user:test)/2, _) ),
      not( rar:calls_pp(_, (user:test)/2) ),
      rar:calls_pp(_, (rar_test_1:e)/1),
      rar:calls_pp((rar_test_1:jat)/1, _) ), Time ),
   writeln(runtime:Time).


/*** implementation ***********************************************/


init_sca_tests_2 :-
   alias_to_default_file(calls_pp_test, File),
   dread_(xml(dtd:[]:[]), File, _),
   dislog_variable_get(home, Home),
   !,
   add_slash_suffix(Home, Dislog_Home),
   P = 'sources/source_code_analysis/rar/tests/',
   concat(P, 'calls_pp_test_1.pl', File_1),
   concat(P, 'calls_pp_test_2.pl', File_2),
   Tree_2 =
      system:[source_code:prolog, root:Dislog_Home, path:Home,
            name:'Test']:[file:[name:'calls_pp_test_1.pl',
         path:File_1]:[],
      file:[source_code:prolog, name:'calls_pp_test_2.pl',
         path:File_2]:[]],
   rar:reset_rar_database,
   rar:parse_sources_to_rar_database(Tree_2, _Tree_3),
   rar:set_rar_db_time_variable(sca_tests_2).


/******************************************************************/


