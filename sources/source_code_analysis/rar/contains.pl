

/******************************************************************/
/***                                                            ***/
/***          RAR:  Retrieval                                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* contains_ll(?Level_1:Name_1, ?Level_2:Name_2) <-
      */

contains_ll(Level_1:Name_1, Level_2:Name_2) :-
   ground(Level_2),
   Level_2 = file,
   !,
   rar:select(leaf_to_path, Name_2, Element_1),
   rar:parse_element(Element_1, Element_2, file, Name_2),
   rar:parse_element(Element_2, _, Level_1, Name_1),
   not( Level_1:Name_1 = file:Name_2 ).
contains_ll(Level_1:_, _:_) :-
   ground(Level_1),
   Level_1 = file,
   !,
   fail.
contains_ll(Level_1:Name_1, Level_2:Name_2) :-
   ( ground(Name_2)
   ; ground(Level_2) ),
   !,
   rar:select(leaf_to_path, _, Element_1),
   rar:parse_element(Element_1, Element_2, Level_2, Name_2),
   rar:parse_element(Element_2, _, Level_1, Name_1),
   not( Level_1:Name_1 = Level_2:Name_2 ).
contains_ll(Level_1:Name_1, Level_2:Name_2) :-
   ( ground(Name_1)
   ; ground(Level_1) ),
   !,
   rar:select(leaf_to_path, _, Element_1),
   parse_element_reverse(Element_1, [], Element_2, Level_1, Name_1),
   rar:parse_element(Element_2, _, Level_2, Name_2).
contains_ll(Level_1:Name_1, Level_2:Name_2) :-
   !,
   rar:select(leaf_to_path, _, Element_1),
   rar:parse_element(Element_1, Element_2, Level_2, Name_2),
   rar:parse_element(Element_2, _, Level_1, Name_1),
   not( Level_1:Name_1 = Level_2:Name_2 ).


/* contains_ll_ext(?Package_1:Name_1, ?Package_2:Name_2) <-
      */

contains_ll_ext(Level_1:Name_1, Level_2:Name_2) :-
   ( ground(Level_1)
   ; ground(Name_1) ),
   Level_1 = file,
   Level_2 = file,
   Name_1 = Name_2,
   !.
contains_ll_ext(Package_1:Name_1, Package_2:Name_2) :-
   contains_ll(Package_1:Name_1, Package_2:Name_2).


/* package_to_mpas(+Level:Name, -MPAs) <-
      */

package_to_mpas(Level:Name, MPAs_2) :-
   findall( MPAs,
      rar:contains_lp(Level:Name, MPAs),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   !.


/* contains_lp(?Level:Name, ?Predicate) <-
      */

contains_lp(Level:Name, MPA) :-
   ground(MPA),
   !,
   rar:defines_fp(File, MPA),
   rar:select(leaf_to_path, File, Element),
   parse_element(Element, _, Level, Name).
contains_lp(Level:Name, Predicate) :-
   ( ground(Level)
   ; ground(Name) ),
   !,
   contains_ll_ext(Level:Name, file:File),
   defines_fp(File, Predicate).


/* file_to_rules(+File, -MPAs) <-
      */

file_to_rules(File, MPAs) :-
   findall( MPA,
      defines_fp(File, MPA),
      MPAs ).


/* file_to_directives_and_mpas(+File, -DMs) <-
      */

file_to_directives_and_mpas(File, DMs) :-
   package_to_mpas(file:File, MPAs),
   file_to_directives(File, Directives),
   append(Directives, MPAs, DMs).


/* file_to_directives(File, Directives) <-
      */

file_to_directives(File, Directives) :-
   findall( (M:P)/A,
      ( select(rule, (M:P)/A, File, _),
        is_directive((M:P)/A) ),
      Directives ).


/* defines_fp(?File, ?MPA) <-
      */

defines_fp(File, (M:P)/A) :-
   rar:select(rule, (M:P)/A, File, _),
   atomic(P),
   not( is_directive((M:P)/A) ).


/* is_directive(+MPA) <-
      */

is_directive((_:P)/0) :-
   atom(P),
   atom_prefix(P, no_head),
   !.
is_directive((_:(?-))/1).


/* package_names(+system|sources|unit|module|file, -Names) <-
      */

package_names(file, Filenames_2) :-
   !,
   findall( File,
      rar:select(leaf_to_path, File, _),
      Filenames_1 ),
   list_to_ord_set(Filenames_1, Filenames_2),
   !.
package_names(Package, Names_2) :-
   sca_variable_get(source_tree, Tree),
   package_names(Package, Tree, Names_1),
   list_to_ord_set(Names_1, Names_2).

package_names(Package, Package:Attr:Content, [Name|Names_2]) :-
   memberchk(path:Name, Attr),
   !,
   maplist( package_names(Package),
      Content, Names_1 ),
   ord_union(Names_1, Names_2),
   !.
package_names(Package, _:_:Content, Names_2) :-
   !,
   maplist( package_names(Package),
      Content, Names_1 ),
   ord_union(Names_1, Names_2),
   !.


/* file_to_package(+File, ?Package:Name) <-
      */

file_to_package(File, Package:Name) :-
   rar:select(leaf_to_path, File, Tree),
   parse_tree(Tree, Package, Name),
   !.


/*** implementation ***********************************************/


/* parse_tree(+Tree, +Level, -Name) <-
      */

parse_tree(_:Attr:_, Level, Name) :-
   member(level:Level, Attr),
   member(name:Name, Attr),
   !.
parse_tree(_:_:Cont, R, Name) :-
   member(Sub_Tree, Cont),
   parse_tree(Sub_Tree, R, Name).


/* parse_element(+Element_1, -Element_2, +Level, +Name) <-
      */

parse_element(Tag:Attr:Content, Tag:Attr:Content, Level, Name) :-
   member(level:Level, Attr),
   member(name:Name, Attr).
parse_element(_:_:Cont, Element_2, Level, Name) :-
   member(Element_1, Cont),
   parse_element(Element_1, Element_2, Level, Name).


/* parse_element_reverse(+Element, +P_1, -P_2, +Level, +Name) <-
      */

parse_element_reverse(_:Attr:_, [P], P, Level, Name) :-
   member(level:Level, Attr),
   member(name:Name, Attr),
   !.
parse_element_reverse(Tag:Attr:Cont, P_1, P_2, Level, Name) :-
   member(Element, Cont),
   parse_element_reverse(Element, [Tag:Attr:P_1], P_2, Level, Name).


/******************************************************************/


