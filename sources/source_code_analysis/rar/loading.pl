

/******************************************************************/
/***                                                            ***/
/***          RAR:  Loading                                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* load_rar_database(+File) <-
      */

load_rar_database(File) :-
   exists_file_with_tilde(File),
   reset_rar_database,
   reset_temporary_dbs,
   alias_to_file(predicate_groups, SP),
   assert_predicate_groups(SP),
   write_list(['<--- ', File, ' ... ']),
   file_name_extension(_, Type, File),
   determine_runtime(
      rar:load_rar_database(Type, File) ),
   add_file_to_alias(db_files, File),
   writeln(' ... done.').


/*** implementation ***********************************************/


/* load_rar_database(+Type, +File) <-
      */

load_rar_database(rar, File_1) :-
   tilde_to_home_path(File_1, File_2),
   open(File_2, read, Stream),
   read(Stream, T0),
   asserted_stream_content(T0, Stream),
   close(Stream).
load_rar_database(xml, File_1) :-
   dread_(xml(rar_database), File_1, XML_RAR_Database),
   [Source_Tree] := XML_RAR_Database^source_tree^content::'*',
   sca_variable_set(source_tree, Source_Tree),
   reverse_tree(Source_Tree),
   Exported_Predicates :=
      XML_RAR_Database^exported_predicates^content::'*',
   sca_variable_set(exported_predicates, Exported_Predicates),
   forall( File_2 := XML_RAR_Database^program^file,
      rar:xml_file_to_rar_database(File_2) ).


/* asserted_stream_content(+Term, +Stream) <-
      */

asserted_stream_content(end_of_file, _) :-
   !.
asserted_stream_content(rule(MPA, File, E), Stream) :-
   !,
   insert(rule, MPA, File, E),
   read(Stream, T2),
   asserted_stream_content(T2, Stream).
asserted_stream_content(inverse_rule(MPA_1, File, MPA_2), Stream) :-
   !,
   insert(inverse_rule, MPA_1, File, MPA_2),
   read(Stream, T2),
   asserted_stream_content(T2, Stream).
asserted_stream_content(leaf_to_path(B, C), Stream) :-
   !,
   insert(leaf_to_path, B, C),
   read(Stream, T2),
   asserted_stream_content(T2, Stream).
asserted_stream_content(sca_variable(A, B), Stream) :-
   !,
   sca_variable_set(A, B),
   read(Stream, T2),
   asserted_stream_content(T2, Stream).
asserted_stream_content(Term, Stream) :-
   spectra_save_and_load:asserted_stream_content(Term, Stream),
   !.
asserted_stream_content(Term, Stream) :-
   topology_db:asserted_stream_content(Term, Stream),
   !.
asserted_stream_content(Term, Stream) :-
   format(user_error, 'Bad term: ~p~n', [Term]),
   read(Stream, T2),
   asserted_stream_content(T2, Stream).


/* xml_file_to_rar_database(+File) <-
      */

xml_file_to_rar_database(File) :-
   Path := File@path,
   forall( Rule := File^rule,
      insert_rule(Path, Rule) ).


/******************************************************************/


