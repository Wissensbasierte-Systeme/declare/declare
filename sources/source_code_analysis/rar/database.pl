

/******************************************************************/
/***                                                            ***/
/***          RAR:  Database                                    ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      special_predicate/5,
      rule/6,
      inverse_rule/6,
      leaf_to_path/4,
      dtd_element/4,
      dtd_attribute/4,
      dtd_edge/6,
      gxl_node/4,
      gxl_edge/6.



/*** interface ****************************************************/


/* reverse_tree(+Tree) <-
      */

reverse_tree(Tree) :-
   reverse_tree(Tree, file, []).


/* init
      */

init :-
   alias_to_file(db_files, DB_File),
   load_rar_database(DB_File),
   !.
init :-
   dislog_sources_to_tree(Tree),
   parse_sources_to_rar_database(Tree, _),
   !.


/* init(dislog|php|prolog|java|sca_tests_1, sca_test_2) <-
      */

init(Type) :-
   sca_variable_get(rar_db_created, (Type, _, _)),
   !.
init(sca_tests_1) :-
   init_sca_tests_1,
   !.
init(sca_tests_2) :-
   init_sca_tests_2,
   !.
init(_) :-
   init.


/*** implementation ***********************************************/


/* reverse_tree(+Tree, +Level, +[]) <-
      */

reverse_tree(Tag:Attr:_, Tag, Path) :-
   memberchk(path:Name, Attr),
   rar:insert(leaf_to_path, Name,
      leaf_to_path:[level:Tag, name:Name]:Path),
   !.
reverse_tree(Tag_1:Attr:Cont, Tag_2, Path) :-
   forall( member(Tree, Cont),
      ( memberchk(path:Name, Attr),
        reverse_tree(Tree, Tag_2,
           [leaf_to_path:[level:Tag_1, name:Name]:Path]) ) ),
   !.
reverse_tree(_, _, _).


/******************************************************************/


