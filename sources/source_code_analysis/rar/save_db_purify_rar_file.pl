

/******************************************************************/
/***                                                            ***/
/***          RAR:  Parsing to Internal Database                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* save_db_purify_rar_file(File_1, File_2) <-
      */

save_db_purify_rar_file(File_1, File_2) :-
   read_file_to_line_facts(File_1),
   predicate_to_file( File_2,
      forall( retract(its_analysis:file_line_fact(Line)),
         ( rar:save_db_purify_line(Line, Line_2),
           writeln(Line_2) ) ) ).

save_db_purify_line(Line_1, Line_2) :-
   Substitutions = [
      ["functor: **", "functor: '**"], ["**]", "**']"],
      ["functor: /*]", "functor: '/*']"],
      ["functor: */]", "functor: '*/']"],
      ["functor: /*", "functor: '/*"], ["*/]", "*/']"] ],
   name_exchange_sublist(Substitutions, Line_1, Line),
   name_exchange_sublist(Substitutions, Line, Line_2),
   !.

read_file_to_line_facts(File_1) :-
   tilde_to_home_path(File_1, File_2),
   open(File_2, read, Stream),
   its_analysis:read_stream_to_line_facts(Stream, []),
   close(Stream).


/*** implementation ***********************************************/



ask_for_purify(File) :-
   new(D, dialog('Purify the Rar File')),
   send(D, append,
      new(_, text('You use an old Version of XPCE Prolog!'))),
   send(D, append,
      new(_, text('This causes, that you are not able to reload the saved DB.'))),
   send(D, append,
      new(_, text('In order to reload the DB again, you have to purify the DB.'))),
   send(D, append,
      new(_, text('Purifiing a file can take about 15 minutes.'))),
   send(D, append,
      new(_, text('The purified file will be written to:'))),
   send(D, append,
      new(_, text(File))),
   send(D, append,
      button('Purify', message(D, return,
      true))),
   send(D, append,
      button(cancel, message(D, return, @nil))),
   send(D, default_button, 'Purify'),
   get(D, confirm, Answer),
   send(D, destroy),
   Answer \== @nil.


/******************************************************************/


