

/******************************************************************/
/***                                                            ***/
/***          RAR:  Configuration File                          ***/
/***                                                            ***/
/******************************************************************/


:- maplist( concat('sources/source_code_analysis/rar/tests/'),
      ['cross.pl', 'desc.pl', 'necess.pl', 'test.pl'],
      Files ),
   sca_variable_set(rar_test_files, Files).


:- dislog_variable_get(home, Sources),
   !,
   concat(Sources, '/results/slicing', Directory),
   sca_variable_set(slice, Directory).


/******************************************************************/


