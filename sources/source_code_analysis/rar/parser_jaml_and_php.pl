

/******************************************************************/
/***                                                            ***/
/***   PHP and JAML source code to RaR database                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* parse_sources_to_rar_database(+java|php, +Tree_1, -Tree_2) <-
      */

parse_sources_to_rar_database(Mode, Tree_1, Tree_2) :-
   rar:tree_to_files(Tree_1, Files),
   sca_variable_set(counter, 0),
   length(Files, Amount_of_all_Files),
   [Root, XML_Root] := Tree_1@[root, xml_root],
   determine_runtime(
      checklist( rar:add_code_to_rar_db(
            Mode, Amount_of_all_Files, Root, XML_Root),
         Files ),
      RT ),
   rar:add_date_to_files(Tree_1, Tree_2),
   rar:reverse_tree(Tree_2),
   sca_variable_set(source_tree, Tree_2),
   rar:set_rar_db_time_variable(Mode),
   writeln(runtime:RT).


/*** implementation ***********************************************/


/* add_code_to_rar_db(+java|php,
      +Amount_of_Files, +Root, +XML_Root, +File) <-
      */

add_code_to_rar_db(Mode, Amount_of_Files, Root, XML_Root, File_1) :-
   file_name_extension(File_2, _, File_1),
   file_name_extension(File_2, xml, File_3),
   concat(XML_Root, File_3, File_4),
   exists_file_with_tilde(File_4),
   concat(Root, File_1, File_5),
   exists_file_with_tilde(File_5),
   dread_(xml, File_4, [XML_Code|_]),
   !,
   sca_variable_get(counter, C),
   C_2 is C +1,
   sca_variable_set(counter, C_2),
   writeln(C_2/Amount_of_Files),
   xml_representation_to_rar_db(Mode, File_1, XML_Code),
   !.
add_code_to_rar_db(_, _, _, _, File) :-
   writeln('failure during parsing file':File).


/* xml_representation_to_rar_db(+java|php, +File, +Code) <-
      */

xml_representation_to_rar_db(Mode, File, Code) :-
   xml_representation_to_rar_db_sub(Mode, Code, Calls),
   length(Calls, L),
   concat(['Calls in file ', File, ': ', L], Message),
   writeln(Message),
   nl,
   checklist( add_rar_db_fact(File),
      Calls ).

xml_representation_to_rar_db_sub(java, Code, Calls) :-
   calls_cmcm(Code, Calls),
   !.
xml_representation_to_rar_db_sub(php, Code, Calls) :-
   php_method_calls(Code, Calls),
%  php_class_calls(Code, Calls),
   !.

add_rar_db_fact(File, Calls_cmcm) :-
   S_Class := Calls_cmcm^source@class,
   S_Method := Calls_cmcm^source@method,
   T_Class := Calls_cmcm^target@class,
   T_Method := Calls_cmcm^target@method,
   Rule =
      rule:[module:S_Class]:[
         body:[]:[atom:[arity:unknown,
            module:T_Class, predicate:T_Method]:[]],
         head:[]:[atom:[
            module:S_Class, predicate:S_Method, arity:unknown]:[]]
      ],
   !,
   rar:insert_rule(File, (S_Class:S_Method)/unknown, Rule),
   !.


/******************************************************************/


