

/******************************************************************/
/***                                                            ***/
/***          RAR:  calls_pp_with_variables                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* calls_pp_with_variables(
         ?File, ?MPA_1, -Parameters_1, ?MPA_2, -Parameters_2) <-
      */

calls_pp_with_variables(File, MPA_1, Prmtrs_1, MPA_2, Prmtrs_2) :-
   ground(MPA_2),
   !,
   rar:select(inverse_rule, MPA_2, File, MPA_1),
   calls_pp_with_variables_sub(File, MPA_1, Prmtrs_1,
      MPA_2, Prmtrs_2).
calls_pp_with_variables(File, MPA_1, Prmtrs_1, MPA_2, Prmtrs_2) :-
   !,
   calls_pp_with_variables_sub(File, MPA_1, Prmtrs_1,
      MPA_2, Prmtrs_2).


/* mpa_to_file(+MPA, -Parameters, -File) <-
      */

mpa_to_file(MPA, Parameters, File) :-
   rar:select(rule, MPA, File, Rule),
   calls_pp_with_variables_step(head, Rule, MPA, Parameters).


/* calls_pp(+MPA_1, -Parameters_1, -MPA_2) <-
      */

calls_pp_with_variables(MPA_1, Parameters_1, MPA_2) :-
   rar:select(rule, MPA_1, _, Rule),
   calls_pp_with_variables_step(head, Rule, MPA_1, Parameters_1),
   calls_pp(Rule, MPA_1, MPA_2).


/*** implementation ***********************************************/


/* calls_pp_with_variables_sub(
      File, MPA_1, Prmtrs_1, MPA_2, Prmtrs_2) <-
      */

calls_pp_with_variables_sub(File, MPA_1, Prmtrs_1, MPA_2, Prmtrs_2) :-
   rar:select(rule, MPA_1, File, Rule),
   calls_pp_with_variables_step(head, Rule, MPA_1, Prmtrs_1),
   calls_pp_with_variables_step(body, Rule, MPA_2, Prmtrs_2).

calls_pp_with_variables_step(
      Choice, Rule, (M:P)/A, Parameters) :-
   Atom := Rule^Choice^atom::[@module=M, @predicate=P, @arity=A],
   Atom = _:_:Para,
   maplist( xml_to_prolog,
      Para, Parameters ).


/* xml_to_prolog(Xml, Term) <-
      */

xml_to_prolog(Xml, A:B) :-
   ':' := Xml@functor,
   A := Xml-nth(1)^_@_,
   B := Xml-nth(2)^_@_,
   !.
xml_to_prolog(Xml, [A|As]) :-
   '.' := Xml@functor,
   A := Xml-nth(1)^_@_,
   As := Xml-nth(2)^_@_,
   !.
xml_to_prolog(Xml, Term) :-
   Term := Xml@functor,
   !.
xml_to_prolog(Xml, Term) :-
   Term := Xml@name,
   !.


/******************************************************************/


