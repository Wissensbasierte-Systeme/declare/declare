

/******************************************************************/
/***                                                            ***/
/***          RAR:  Tree Management                             ***/
/***                                                            ***/
/******************************************************************/


:- dynamic file_to_delete/1.


/*** interface ****************************************************/


/* add_mpas_to_file_content(+Tree_1, -Tree_2) <-
      */

add_mpas_to_file_content(file:Attr:C, file:Attr:MPAs_2) :-
   add_mpas_to_file_content(_, file:Attr:C, file:Attr:MPAs_2).
add_mpas_to_file_content(Tag:Attr:C_1, Tag:Attr:C_2) :-
   maplist( add_mpas_to_file_content,
      C_1, C_2 ),
   !.
add_mpas_to_file_content('', '').


/* add_mpas_to_file_content(+File, +Tree_1, -Tree_2) <-
      */

add_mpas_to_file_content(File, file:Attr:_, file:Attr:MPAs_2) :-
   memberchk(path:File, Attr),
   findall( predicate:[name:Atom]:[],
      ( rar:select(rule, MPA, File, _),
        term_to_atom(MPA, Atom) ),
      MPAs_1 ),
   list_to_ord_set(MPAs_1, MPAs_2),
   !.
add_mpas_to_file_content(File, Tag:Attr:C_1, Tag:Attr:C_2) :-
   maplist( add_mpas_to_file_content(File),
      C_1, C_2 ),
   !.
add_mpas_to_file_content(_, '', '').


/* add_date_to_files(+Tree_1, -Tree_2) <-
      */

add_date_to_files(Tree_1, Tree_2) :-
   Root_1 := Tree_1@root,
   tilde_to_home_path(Root_1, Root_2),
   add_date_to_files(Root_2, Tree_1, Tree_2),
   !.
add_date_to_files(Tree_1, Tree_2) :-
   add_date_to_files('', Tree_1, Tree_2),
   !.

add_date_to_files(Root, file:Attr_1:Cont, Tree_2) :-
   memberchk(path:Rel_Path, Attr_1),
   concat(Root, Rel_Path, Abs_Path),
   time_file_to_atom(Abs_Path, Time_Atom),
   Tree_2 := (file:Attr_1:Cont)*[@date:Time_Atom],
   !.
add_date_to_files(Root, Tag:Attr:C_1, Tag:Attr:C_2) :-
   maplist( add_date_to_files(Root),
      C_1, C_2 ),
   !.
add_date_to_files(_, '', '') :-
   !.


/* time_file_to_atom(+File, -Time) <-
      */

time_file_to_atom(File, Time_Atom) :-
   time_file(File, Time),
   convert_time(Time, Time_String),
   string_to_atom(Time_String, Time_Atom).


/* tree_to_files(+Tree, -Files) <-
      */

tree_to_files(Tree, Files) :-
   tree_to_files_xml(Tree, XML_Files),
   findall( File,
      File := XML_Files^file@path,
      Files ).


/* tree_to_files_xml(+Tree, -files:[]:Files) <-
      */

tree_to_files_xml(Tree, files:[]:Files) :-
   tree_to_files_sub(Tree, Files),
   !.

tree_to_files_sub(file:Attr:_, [file:Attr:[]]) :-
   !.
tree_to_files_sub(_:_:Cont, Files_2) :-
   findall( File,
      ( member(M, Cont),
        tree_to_files_sub(M, File) ),
      Files_1 ),
   append(Files_1, Files_2),
   !.
tree_to_files_sub('', []).


/* clear_tree(Tree_1, Tree_3) <-
      */

clear_tree(Tree_1, Tree_3) :-
   delete_file_content(Tree_1, Tree_2),
   delete_consulted_files_from_tree(Tree_2, Tree_3).


/* delete_files_from_tree(+Files, +Tree_1, +Tree_2) <-
   */

delete_files_from_tree(Files, Tree_1, Tree_2) :-
   retractall( file_to_delete(_) ),
   checklist( assert_file_to_delete,
      Files ),
   delete_files_from_tree(Tree_1, Tree_2),
   retractall( file_to_delete(_) ).


/* filter_redundant_paths(+Tree_1, -Tree_2) <-
      */

filter_redundant_paths(Tree_1, Tree_3) :-
   Directory := Tree_1@path,
   add_slash_suffix(Directory, Root),
   change_fn_value(Tree_1, root:Root, Tree_2),
   filter_redundant_paths(Root, Tree_2, Tree_3),
   !.


/*** implementation ***********************************************/


/* filter_redundant_paths(+Root, +Tree_1, -Tree_2) <-
      */

filter_redundant_paths(Root, Tag:Attr_1:Cont_1,
      Tag:Attr_4:Cont_2) :-
   memberchk(path:Abs_Path, Attr_1),
   sub_atom(Abs_Path, 0, B, C, Root),
   sub_atom(Abs_Path, B, C, 0, Rel_Path),
%  (Tag:Attr_4:[]) := (Tag:Attr_1:[])*[@path:Rel_Path],
   change_fn_value_fast(Attr_1, path:Rel_Path, Attr_4),
   maplist( filter_redundant_paths(Root),
      Cont_1, Cont_2 ),
   !.
filter_redundant_paths(Root, Tag:Attr:Cont_1, Tag:Attr:Cont_2) :-
   maplist( filter_redundant_paths(Root),
      Cont_1, Cont_2 ),
   !.
filter_redundant_paths(_, Tag:Attr:Cont, Tag:Attr:Cont) :-
   writeln('failure').


/* assert_file_to_delete(+File) <-
      */

assert_file_to_delete(File) :-
   assert( file_to_delete(File) ).


/* delete_files_from_tree(+Tree_1, -Tree_2) <-
      */

delete_files_from_tree(Tag:Attr:C_1, Tag:Attr:C_3) :-
   sublist( filter_file_to_delete,
      C_1, C_2 ),
   maplist( delete_files_from_tree,
      C_2, C_3 ),
   !.
delete_files_from_tree('', '').

filter_file_to_delete(file:Attr:_) :-
   memberchk(path:Rel_Path, Attr),
   file_to_delete(Rel_Path),
   !,
   fail.
filter_file_to_delete(_).


/* add_mpas_to_file_content_p(+Tree_1, +File, -Tree_2) <-
      */

add_mpas_to_file_content_p(Tree_1, File, Tree_2) :-
   add_mpas_to_file_content(File, Tree_1, Tree_2).


/* delete_consulted_files_from_tree(+Tree_1, -Tree_2) <-
      */

delete_consulted_files_from_tree(T_1:A_1:C_1, T_1:A_1:C_3) :-
   sublist( filter_consulted_file,
      C_1, C_2 ),
   maplist( delete_consulted_files_from_tree,
      C_2, C_3 ),
   !.
delete_consulted_files_from_tree('', '').


/* filter_consulted_file(+Element) <-
      */

filter_consulted_file(file:Attr:_) :-
   memberchk(alias:consulted_file, Attr),
   !,
   fail.
filter_consulted_file(_).


/* delete_file_content(+Tree_1, -Tree_2) <-
      */

delete_file_content(file:Attr:_, file:Attr:[]) :-
   !.
delete_file_content(Tag:Attr:C_1, Tag:Attr:C_2) :-
   maplist( delete_file_content,
      C_1, C_2 ),
   !.
delete_file_content('', '').


/*** Tests ********************************************************/


test(source_code_analysis, rar, add_file_date) :-
   rar:dislog_sources_to_tree(Tree_1),
   add_date_to_files(Tree_1, Tree_2),
   writelq(Tree_2).


/******************************************************************/


