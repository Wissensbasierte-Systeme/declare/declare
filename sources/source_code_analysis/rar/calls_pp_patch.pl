

/******************************************************************/
/***                                                            ***/
/***          RAR:  calls_pp                                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* ignore_calls_pp(List) <-
      */

ignore_calls_pp(MPA) :-
   ground(MPA),
   List = [
      (_:_:_)/_,
      (_:test)/2,
      (_:test)/3,
      (rar:init)/1,
      (_:test_find_successful_tests)/2,
      (_:dportray)/_,
      (_:tf1)/0,
      % (user:',')/2,
      % (user:'.')/2,
      (_:':')/2,
      (_:[])/0 ],
   memberchk(MPA, List).
%  writeln('Call omitted':P).


/* calls_pp(Predicate_1, Predicate_2) <-
      */

calls_pp(P, _) :-
   ignore_calls_pp(P),
   !,
   fail.
calls_pp(_, P) :-
   ignore_calls_pp(P),
   !,
   fail.
calls_pp(MPA_1, MPA_2) :-
   ground(MPA_2),
   not( ground(MPA_1) ),
   !,
   calls_pp_inv(MPA_2, MPA_1).
calls_pp(MPA, (M:P)/A) :-
   select(rule, MPA, _, Rule),
   calls_pp(Rule, MPA, (M:P)/A).


/* calls_pp(+Rule, ?P_1, ?P_2) <-
      */

calls_pp(Rule, MPA_1, (M:P)/A) :-
   ground(Rule),
   Head := Rule^head^atom,
   term_to_mpa(Head, MPA_1),
   not( ignore_calls_pp(MPA_1) ),
   Body := Rule^body,
   term_to_subterm(Body, Term),
   term_to_mpa(Term, (M:P)/A),
   not( ignore_calls_pp((M:P)/A) ).
calls_pp(Rule, MPA_1, (M:P)/A) :-
   calls_pp_extended(Rule, MPA_1, (M:P)/A).


/* calls_pp_inv(+P_2, -P_1) <-
      */

calls_pp_inv(P_2, P_1) :-
   calls_pp_inv(P_2, _, P_1).


/* calls_pp_inv(+P_2, -File_1, -P_1) <-
      */

calls_pp_inv(P_2, File_1, P_1) :-
   not( ignore_calls_pp(P_2) ),
   rar:select(inverse_rule, P_2, File_1, P_1),
   not( ignore_calls_pp(P_1) ).


/*** implementation ***********************************************/


/* term_to_subterm(+Term_1, -Term_2) <-
      */

term_to_subterm(Term_1, Term_3) :-
   ground(Term_1),
   ( Term_2 := Term_1^atom
   ; Term_2 := Term_1^term
   ; Term_2 := Term_1^formula ),
   ( Term_3 = Term_2
   ;
   ( term_to_mpa(Term_2, MPA),
     special_predicate_attributes(MPA, meta, _, _, _),
     term_to_subterm(Term_2, Term_3) ) ).


/* term_to_mpa(MAT, (M:P)/A) <-
      */

term_to_mpa(MAT, (M:P)/A) :-
   [M, P, A] := MAT@[module, predicate, arity],
   !.
term_to_mpa(MAT, (M:P)/A) :-
   [M, P, A] := MAT@[module, functor, arity],
   !.
term_to_mpa(MAT, (M:P)/A) :-
   [M, P, A] := MAT@[module, junctor, arity],
   !.
term_to_mpa(MAT, (user:P)/2) :-
   P := MAT@junctor.


/* calls_pp_extended(+Rule, +MPA_1, -MPA_2) <-
      */

calls_pp_extended(Rule, MPA_1, MPA_2) :-
   not( ignore_calls_pp(MPA_1) ),
   Atom := Rule^body^atom::[
      @module=user, @predicate=call, @arity=1],
   ( ( Module := Rule@module,
       Var := Atom^var@name )
   ; ( Module := Atom^term^term@functor,
       Var := Atom^term^var@name ) ),
   calls_pp_sub(Rule, Var, Module, MPA_2),
   not( ignore_calls_pp(MPA_2) ).

calls_pp_sub(Rule, Var, Module, (Module:P)/A) :-
   variable_to_binding(Rule, Var, [Var], P/A),
   P \== _:_.
calls_pp_sub(Rule, Var, _, (M:P)/A) :-
   variable_to_binding(Rule, Var, [Var], (M:P)/A).

variable_to_binding(Rule, Var, _, Binding) :-
   variable_to_binding_step(Rule, Var, Atom),
   Term := Atom^term,
   add_arity_to_functor(Term, Term_1),
   colon_to_module(Term_1, Term_Out),
   variable_to_binding(Term_Out, Binding).
variable_to_binding(Rule, Var, Vars, Binding) :-
   variable_to_binding_step(Rule, Var, atom:_:Arguments),
   member(var:[name:V]:[], Arguments),
   V \= Var,
   not(member(V, Vars)),
   variable_to_binding(Rule, V, [V|Vars], Binding).

variable_to_binding(Term, (M:P)/A) :-
   [M, P, A] := Term@[module, functor, arity],
   !.
variable_to_binding(Term, P/A) :-
   [P, A] := Term@[functor, arity],
   !.

variable_to_binding_step(Rule, Var, Atom) :-
   variable_to_binding_sub(Rule, Var, [user, =, 2], Atom).
variable_to_binding_step(Rule, Var, Atom) :-
   variable_to_binding_sub(Rule, Var, [user, =.., 2], Atom_2),
   Term := Atom_2^term,
   list_to_predicate(Term, Atom).

variable_to_binding_sub(Rule, Var, Bind, Atom) :-
   Body := Rule^body,
   get_atom(Body, Atom),
   Bind := Atom@[module, predicate, arity],
   Var := Atom^var@name.

get_atom(Body, Atom) :-
   Atom := Body^atom.
get_atom(Body, Atom) :-
   Atom := Body^_^atom.

list_to_predicate(Term, Atom) :-
   P := Term^term@functor,
   P \= '.',
   get_term_arity(Term, A),
   Atom = atom:[]:[term:[functor:P, arity:A]:[]].

get_term_arity(Term, Arity) :-
   get_term_arity(Term, -1, Arity).

get_term_arity(Term, A, Arity) :-
   T := Term^term,
   B is A + 1,
   get_term_arity(T, B, Arity).
get_term_arity(Term, Arity, Arity) :-
   [] := Term@functor.

colon_to_module(Term_1, Term_2) :-
   ':' := Term_1@functor,
   2 := Term_1@arity,
%  user := Term_1@module,
   Term_1 = term:_:[term:[functor:M]:[], _:[_:P]:C_2],
   length(C_2, A),
   Term_2 = term:[module:M, functor:P, arity:A]:C_2,
   !.
colon_to_module(Term, Term).

add_arity_to_functor(Term_1, Term_2) :-
   not(_ := Term_1@arity),
   Term_1 = term:_:Terms,
   length(Terms, A),
   Term_2 := Term_1*[@arity:A],
   !.
add_arity_to_functor(Term, Term).


/******************************************************************/


