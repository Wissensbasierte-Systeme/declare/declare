

/******************************************************************/
/***                                                            ***/
/***          RAR:  Prolog Sources to XML Tree                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* sources_to_tree(+Type, +Root, +XML_Root, -Tree) <-
      */

sources_to_tree(dislog, Root, _, Tree_2) :-
   dislog_sources_to_tree(Root, Tree_1),
   filter_redundant_paths(Tree_1, Tree_2).
sources_to_tree(prolog, Root, XML_Root, Tree_2) :-
   prolog_sources_to_tree(Root, XML_Root, Tree_1),
   filter_redundant_paths(Tree_1, Tree_2).
sources_to_tree(java, Root, XML_Root, Tree_2) :-
   jaml_sources_to_tree(Root, XML_Root, Tree_1),
   filter_redundant_paths(Tree_1, Tree_2).
sources_to_tree(php, Root, XML_Root, Tree_2) :-
   phpml_sources_to_tree(Root, XML_Root, Tree_1),
   filter_redundant_paths(Tree_1, Tree_2).


/*** implementation ***********************************************/


prolog_sources_to_tree(Directory, XML_Directory_1, Tree_3) :-
   file_system_to_xml(Directory, Tree_1),
   filter_files(pl, Tree_1, Tree_2),
   set_mouse_click_attributes([dir:module, file:file],
      Tree_2, Tag:Attr_1:Cont),
   add_slash_suffix(XML_Directory_1, XML_Directory_2),
   Attr_2 = [xml_root:XML_Directory_2,
      source_code:prolog|Attr_1],
   list_to_ord_set(Attr_2, Attr_3),
   Tree_3 = Tag:Attr_3:Cont.


/******************************************************************/


