

/******************************************************************/
/***                                                            ***/
/***      File System contains_ll                               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* fs_contains_ll(+Tree, +Depth,
      +Tag_A:Path_A, -Tag_B:Path_B, -XML_Files) <-
      */

fs_contains_ll(Tag:Attr_A:Cont, Depth,
      Tag:Path_A, Type_B:Path_B, XML_Files) :-
   memberchk(path:Path_A, Attr_A),
   !,
   fs_contains_ll_sub(Tag:Attr_A:Cont, Depth,
      Type_B:Path_B, XML_Files).
fs_contains_ll(_:_:Cont, Depth,
      Type_A:Path_A, Type_B:Path_B, XML_Files) :-
   member(Sub_Tree, Cont),
   fs_contains_ll(Sub_Tree, Depth, Type_A:Path_A,
      Type_B:Path_B, XML_Files).


/*** implementation ***********************************************/


/* fs_contains_ll_sub(+Tree, +Depth, +Type_B:Path_B, -XML_Files) <-
      */

fs_contains_ll_sub(_:_:Cont, 0, Type_B:Path_B, XML_Files) :-
   member(Type_B:Attr_B:Cont_B, Cont),
   rar:tree_to_files_xml((Type_B:Attr_B:Cont_B), XML_Files),
   memberchk(path:Path_B, Attr_B).
fs_contains_ll_sub(_:_:Cont, Depth, Type_B:Path_B, XML_Files) :-
   Depth > 0,
   member(Type_B:Attr_B:[], Cont),
   rar:tree_to_files_xml((Type_B:Attr_B:[]), XML_Files),
   memberchk(path:Path_B, Attr_B).
fs_contains_ll_sub(_:_:Cont, Depth_1, Type_B:Path_B, XML_Files) :-
   Depth_1 > 0,
   Depth_2 is Depth_1 - 1,
   member(X, Cont),
   fs_contains_ll_sub(X, Depth_2, Type_B:Path_B, XML_Files).

contains_dir_files(Tree, Dir, Files) :-
   Dir := Tree@path,
   !,
   rar:tree_to_files_xml(Tree, files:_:XML_Files),
   maplist( extract_pure_file_path,
      XML_Files, Files ).
contains_dir_files(_:_:Cont, Dir, XML_Files) :-
   member(Sub_Tree, Cont),
   contains_dir_files(Sub_Tree, Dir, XML_Files).

extract_pure_file_path(XML_File, File) :-
   File := XML_File@path.


/******************************************************************/


