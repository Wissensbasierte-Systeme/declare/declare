

/******************************************************************/
/***                                                            ***/
/***          RAR:  DisLog Sources to XML Tree                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dislog_sources_to_tree(+Directory, -Tree) <-
      */

dislog_sources_to_tree(Tree) :-
   dislog_variable_get(home, Directory),
   !,
   dislog_sources_to_tree(Directory, Tree).

dislog_sources_to_tree(Dir, Tree) :-
   delete_slash_suffix(Dir, Directory),
   concat(Directory, '/sources', Source_Path),
   Library_1 = [
      'loops.pl',
      'ordsets.pl',
      'assoc.pl',
      'lists_sicstus.pl',
      'ugraphs.pl',
      'arrays.pl'
%     'sgml.pl',
%     'module_test.pl', 'sldmagic_swi.pl', 'heaps.pl',
%     'sockets.pl', 'html.pl',
%     'static_swi.pl',
%     'pillow.pl', 'pillow_sicstus.pl',
%     'lists_swi.pl', 'random.pl'
       ],
   concat(Directory, '/library', Path_Library),
   maplist( file_to_fn_term(Path_Library),
      Library_1, Library_3 ),
   findall( unit:[name:Unit, path:Unit_Path]:Terms,
      ( dislog_unit(Unit, Modules),
        concat([Directory, '/sources/', Unit], Unit_Path),
        maplist( module_to_fn_term(Source_Path, Unit_Path),
           Modules, Terms ) ),
      Units_1 ),
   concat(Directory,
      '/sources/system/dislog_units.pl', File_DisLog_Units),
   DisLog_Units = file:[name:dislog_units,
      path:File_DisLog_Units]:[],
   append(Units_1, [DisLog_Units], Units_2),
   Tree = system:[source_code:dislog, name:'DisLog',
      path:Directory]:[
             sources:[name:sources, path:Source_Path]:Units_2,
             sources:[name:libraries, path:Path_Library
             ]:[unit:[name:library, path:Path_Library]:Library_3]].


/* dislog_to_xml_files <-
      */

dislog_to_xml_files :-
   rar:init(dislog),
   dislog_variable_get(home, DisLog),
   !,
   add_slash_suffix(DisLog, Root_1),
   concat(Root_1, 'XML/', Root_2),
   sca_variable_get(source_tree, Tree),
   tree_to_files_xml(Tree, files:_:Files),
   checklist( rar:facts_to_xml_file(Root_2),
      Files ).


/*** implementation ***********************************************/


/* facts_to_xml_file(+Root, +File) <-
      */

facts_to_xml_file(Root, file:Attr:_) :-
   memberchk(path:Path, Attr),
   findall( XML,
      rar:select(rule, _, Path, XML),
      XMLs ),
   File_and_Rules = file:Attr:XMLs,
   concat(Root, Path, Abs_File_1),
   file_directory_name(Abs_File_1, Dir),
   make_directory_recursive(Dir),
   file_name_extension(Abs_File_2, _, Abs_File_1),
   file_name_extension(Abs_File_2, xml, Abs_File_3),
   dwrite(xml, Abs_File_3, File_and_Rules).


/* module_to_fn_term(+Path, +Module, -Term) <-
      */

module_to_fn_term(Source_Path, Unit_Path, Module, Term) :-
   dislog_module_2(Module, _, Files),
   maplist( file_to_fn_term(Source_Path),
      Files, Terms ),
   concat([Unit_Path, '/', Module], Module_Path),
   Term = module:[name:Module, path:Module_Path]:Terms.

dislog_module_2(basics, _, Files_2) :-
   dislog_module(basics, _, Files_1),
   Files_2 = ['basic_algebra/basics/specials_swi',
      'basic_algebra/basics/date'|Files_1],
   !.
dislog_module_2(Module, _, Files) :-
   dislog_module(Module, _, Files).


/* file_to_fn_term(+Path_1, +File_1, -Term) <-
      */

file_to_fn_term(Path_1, File_1, Term) :-
   concat([Path_1, '/', File_1], Path_File_1),
   ( ( exists_file_with_tilde(Path_File_1),
       Path_File_2 = Path_File_1,
       ! )
   ; file_name_extension(Path_File_1, pl, Path_File_2) ),
   !,
   ( exists_file_with_tilde(Path_File_2)
   ; writeln('file not found':Path_File_2) ),
   !,
   file_base_name(Path_File_2, File_2),
   Term = file:[name:File_2, path:Path_File_2]:[].


/******************************************************************/


