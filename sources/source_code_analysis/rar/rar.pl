

/******************************************************************/
/***                                                            ***/
/***          RAR:  Reasoning about Rules                       ***/
/***                                                            ***/
/******************************************************************/


:- module( rar, []).


/*** interface ****************************************************/


:- [ 'sources_to_tree_dislog.pl',
     'sources_to_tree_prolog.pl',
     'save_db_purify_rar_file.pl',
     'tree_management.pl',
     'calls_pp_patch.pl',
     'calls_pp_with_variables.pl',
     'calls_ll.pl',
     'contains.pl',
     'fs_contains_ll.pl',
     'settings.pl',
     'loading.pl',
     'saving.pl',
     'parser_prolog.pl',
     'database.pl',
     'db_update.pl',
     'predicate_groups.pl',
     'dml.pl',
     'transitive_closure.pl',
     'parser_jaml_and_php.pl',
     'rename.pl',
     'xml_to_prolog.pl',
     'correct_xml_jaml_files.pl'].


/*** implementation ***********************************************/



/******************************************************************/


