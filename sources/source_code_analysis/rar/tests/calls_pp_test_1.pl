
/******************************************************************/
/***                                                            ***/
/***          RAR:  Reasoning about Rules                       ***/
/***                                                            ***/
/******************************************************************/


:- module( rar_test_1, [a/2]).


/*** interface ****************************************************/

my_meta(A, B, C) :-
   writeln(A),
   Call =.. [B, C],
   call(Call),
   writelq(C).


a(A, B) :-
   forall( member(C, A),
      writelq(C) ),
   rar_test_2:tam_1(B),
   A = gxl:[d:1, e:2]:[
      b:[]:[],
      c:[]:[]].
%   writeln(A),
%   member_x(A, B).

b(A, Bs) :-
   findall( C,
      ( findall( D,
           member_x(D, Bs),
           Ds ),
        member(C, Ds),
        rar_test_2:tam_2(B) ),
      Cs 
   ),
   writelq(A),
   writeln(B).



b2(A, Bs) :-
   ( writelx(A)
   ; writeln(A) ),
   writelq(A),
   writely(Bs).


c(A) :-
   my_meta(A, writelq_d, A), 
   d(A),
   e(A).


d(A) :-
   writelq_d(A).

e(A) :-
   writelq_e(A).


jat(A) :-
   writeln(A),
   e(A).


/*** implementation ***********************************************/


/******************************************************************/


