

rar_predicate_to_cross_calls(Predicate, Predicates) :-
   rar_predicate_to_descendants(Predicate, Descendants),
   findall( P,
      ( member(P, Descendants),
        not( in_same_module(Predicate, P) ) ),
      Predicates ).


in_same_module(M:_, M:_).

in_same_module(M:_, N:_) :-
   M == N.


