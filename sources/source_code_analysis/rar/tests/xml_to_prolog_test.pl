rule_with_foreach_1(As) :-
   ( foreach(A, As) do
     writeln(A) ).

rule_with_foreach_2(As, Xs) :-
   ( foreach(A, [a,b]), 
     foreach(X, [x, y]) do 
       user:write(A-X) ).

:- rule_witout_head,
   directive_a,
   directive_b(b).


rule_without_body.

simple_predicate :-
   a(U, v, W),
   b(V),
   c(V, W),
   d(u, v, w),
   e.


multi_head_1, multi_head_2.

multi_head_1(U), multi_head_2(G) :-
   a(U, G).
multi_head_1(_), multi_head_2(_).


prefix_to_infix(['A', B, C]) :-
   A is B + 1,
   A > B,
   C < D,
   A \= B,
   D := E@f^g.


listen :-
   [a, b, c],
   x_1([]),
   x_2([a(f), b, c]),
   x_3([a, b|[c, d]]).

listen:-
   x_1([a, c], b),
   x_2([a|B]),
   x_3([a, b]),
   x_4([A, B]),
   x_5([A|B]).

listen(A) :-
   x_4([a, [b, c]]),
   x_5([[a(d, e, k), b, h],[c([u, i, o]), d]]),
   x_6([[a, b], [c, d]]).


klammern :-
   X = A:B:C:D,
   Y = (A:B):(C:D),
   Z = (A:(B:C)):D,
   U = A:B:(C:D),
   [a, b, c, d, e],
   (a, b, c, d, e),
   z.
klammern :-
   X = A-B-C-D,
   Y = (A-B)-(C-D),
   Z = (A-(B-C))-D,
   U = A-B-(C-D),
   V = [a, [b, c]],
   W = a-(b:c),
   R = (a-b):c,
   S = a:(b-c).


tupel :-
   x(a, (b, c), d).


fn :-
   A := X^fn,
   B := X@fn,
   C := X@fn^b.


terms :-
   x_1(a-b+c).


metapredicates(A) :-
   checklist( writeln,
      A ),
   maplist( tamtam(a),
      A, B),
   findall( E,
      x(E),
      A ),
   findall( E,
      ( X := E@w^r,
        member(X, A) ),
      A ),
   checklist( maplist(a, b),
      C ),
   forall( member(X, A),
      ( x(X),
        y(X) ) ).


oder(X) :-
   ( a(X)
   ; b(X) ),
   c(X).
oder(X) :-
   ( a(X),
     c(X),
     d(X) )
   ; b(X).


wenn_dann(X, Y) :-
   ( member(X, Y) ->
     ( writeln(a),
       writeln(b) )
   ; writeln(c) ).
