

rar_predicate_to_descendants(Predicate, Descendants) :-
   rar:transitive_closure( user:rar_predicate_to_descendant,
      Predicate, Descendants ).


rar_predicate_to_descendant(Predicate, Descendant) :-
   rar:rar_search(rule, [head:Predicate], Rules),
   member(Rule, Rules),
   Descendants := Rule^body,
   member(Descendant, Descendants).


in_same_module(M:_, M:_).

