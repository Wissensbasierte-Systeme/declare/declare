%a:-
%   rar_predicate_to_necessary_files(_Predicate, _Files),
%   rar_predicate_to_cross_calls(_Predicate, _Predicates).

rar_predicate_to_necessary_files(Predicate, Files) :-
   rar_predicate_to_descendants(Predicate, Descendants),
   maplist( rar_predicate_to_files, Descendants, Fs ),
   rar_predicate_to_files(Predicate, File),
   append([File|Fs], F),
   list_to_set(F, Files).


rar_predicate_to_files(Predicate, Files) :-
   rar_search(rule, [head:Predicate], Rules),
   maplist( rar:rar_filename,
      Rules, Files ).

