

/******************************************************************/
/***                                                            ***/
/***                                                            ***/
/***                                                            ***/
/******************************************************************/


:- module( module_dependency, []).


/*** interface ****************************************************/


/* table_prolog_module_to_files <-
      */

table_prolog_module_to_files :-
   visur:ask_for_integer(
      'Choose number of Files a Prolog Modules is split at least',
      'Please select an Integer: ', Number),
   table_prolog_module_to_files(Number).


/* table_prolog_module_to_files(+Number) <-
      */

table_prolog_module_to_files(Number) :-
   get_prolog_modules(Modules),
   findall( [Module, File],
      ( member(Module, Modules),
        rar:select(rule, (Module:_)/_, File, _) ),
      Module_Files_1 ),
   list_to_ord_set(Module_Files_1, Module_Files_2),
   sublist( composed_of_at_least_files(Number, Module_Files_2),
      Module_Files_2, Module_Files_3 ),
   list_to_dag:list_of_lists_to_ord_dag(Module_Files_3, Tree),
   Tree = _:_:Cont,
   length(Cont, Number_of_Modules),
   concat_atom(['Prolog Modules split into at least ',
      Number, ' Files (module "user" is excluded): ',
      Number_of_Modules], Label),
   list_to_dag:tree_to_table(Label,
      ['Module', 'File'], Tree).


/*** implementation ***********************************************/


/* composed_of_at_least_files(
      +Number, +Module_Files, +[Module, _]) <-
      */

composed_of_at_least_files(Number, Module_Files, [Module, _]) :-
   findall( Module,
      member([Module, _], Module_Files),
      Ms ),
   length(Ms, L),
   L >= Number.


/* number_of_prolog_modules(-PM) <-
      */

number_of_prolog_modules(PM) :-
   sca_variable_get(exported_predicates, Exp_Preds),
   length(Exp_Preds, PM).


/* get_prolog_modules(-Modules) <-
      */

get_prolog_modules(Modules_2) :-
   sca_variable_get(exported_predicates, Exp_Preds),
   findall( Module,
      ( member(module:M:_, Exp_Preds),
        member(name:Module, M) ),
      Modules_1 ),
   list_to_ord_set(Modules_1, Modules_2).


/******************************************************************/


