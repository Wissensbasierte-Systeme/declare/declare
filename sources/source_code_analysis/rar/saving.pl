

/******************************************************************/
/***                                                            ***/
/***          RAR:  Saving                                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* save_rar_database(+File) <-
      */

save_rar_database(File_1) :-
   File_1 \= '',
   write_list(['---> ', File_1, ' ... ']),
   file_name_extension(_, Extension, File_1),
   save_rar_database(Extension, File_1, File_2),
   add_file_to_alias(db_files, File_2),
   writeln(' done.').


/*** implementation ***********************************************/


/* save_rar_database(+Target_Type, +File_1, -File_2) <-
      */

save_rar_database(rar, File_1, File_4) :-
   current_prolog_flag(version, Version),
   predicate_to_file( File_1,
      rar:write_rar_database_with_dot ),
   ( ( Version < 50500,
       file_name_extension(Pref, Extension, File_1),
       concat(Pref, '_purify', File_2),
       file_name_extension(File_2, Extension, File_3),
       ask_for_purify(File_3),
       rar:save_db_purify_rar_file(File_1, File_3),
       File_4 = File_3,
       writeln('The purified file is written to':File_3)  )
   ; File_4 = File_1 ),
   !.
save_rar_database(xml, File, File) :-
   rar:rar_database_to_xml(Xml),
   dwrite(xml_2, File, Xml),
   !.
save_rar_database(Ext, _, _) :-
   writeln('File has unknown extension':Ext).


/* write_rar_database_with_dot <-
      */

write_rar_database_with_dot :-
   write_sca_variable_with_dot(rar_db_created),
   write_sca_variable_with_dot(source_tree),
   write_sca_variable_with_dot(exported_predicates),
   write_rar_facts_with_dot,
   topology_db:write_topology_facts,
   spectra_save_and_load:write_spectra_facts.


/* write_rar_facts_with_dot <-
      */

write_rar_facts_with_dot :-
   forall( rar:select(rule, C, D, E),
      rar:write_term_with_dot(
         rule(C, D, E)) ),
   forall( rar:select(inverse_rule, C, D, E),
      rar:write_term_with_dot(
         inverse_rule(C, D, E)) ),
   forall( rar:select(leaf_to_path, B, C),
      rar:write_term_with_dot(
         leaf_to_path(B, C)) ).


/* write_sca_variable_with_dot(+Variable) <-
      */

write_sca_variable_with_dot(Variable) :-
   sca_variable_get(Variable, Value),
   rar:write_term_with_dot(
      sca_variable(Variable, Value)),
   nl.


/* write_term_with_dot(Term) <-
      */

write_term_with_dot(Term) :-
%  current_prolog_flag(character_escapes, B),
%  set_prolog_flag(character_escapes, false),
%  term_to_atom_sure(Term, Atom),
%  write(Atom),
   writeq(Term),
   writeln('.').
%  set_prolog_flag(character_escapes, B),
%    nl.


/* rar_database_to_xml(-XML) <-
      */

rar_database_to_xml(XML) :-
   sca_variable_get(exported_predicates, Exported_Predicates),
   sca_variable_get(source_tree, Tree),
   facts_to_list(Tree, Rules),
   XML = rar:[]:[
      source_tree:[]:[Tree],
      program:[]:Rules,
      exported_predicates:[]:Exported_Predicates ].


/* facts_to_list(+Tree, -XML) <-
      */

facts_to_list(Tree, Files_and_Rules) :-
   tree_to_files_xml(Tree, files:_:Files),
   maplist( facts_to_list_sub,
      Files, Files_and_Rules ).

facts_to_list_sub(file:Attr:_, File_and_Rules) :-
   memberchk(path:Path, Attr),
   findall( XML,
      rar:select(rule, _, Path, XML),
      XMLs ),
   File_and_Rules = file:Attr:XMLs.


/******************************************************************/


