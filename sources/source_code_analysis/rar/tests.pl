

/******************************************************************/
/***                                                            ***/
/***          RAR: Tests                                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(source_code_analysis:rar_new, file_part_to_relative_filename) :-
   rar:init(sca_tests_1),
   sca_variable_get(rar_test_files, [Cross, _, _, _]),
   filename:file_part_to_relative_filename('cross.pl', Cross),
   filename:file_part_to_relative_filename(Cross, Cross),
   filename:absolute_filenames([Cross], [Abs_Cross]),
   filename:relative_filename(Abs_Cross, Relative_Cross),
   !,
   Relative_Cross = Cross.

test(source_code_analysis:rar_new, term_to_atom) :-
   term_to_atom('/*', T),
   T = '\'/*\''.

/**/

test(source_code_analysis:rar_new, maplist_4) :-
   maplist( pattern_for_maplist_4,
      [A, 1, 1], [2, B, 2], [3, 3, C] ),
   A = 1,
   B = 2,
   C = 3.

pattern_for_maplist_4(1, 2, 3).


/*** implementation ***********************************************/


init_sca_tests_1 :-
   dislog_variable_get(home, Home),
   !,
   add_slash_suffix(Home, DisLog),
   sca_variable_get(rar_test_files, [Cross, Desc, Necess, Test]),
   Tree_1 =
      unit:[source_code:prolog, name:rar, path:'rar', root:DisLog]:[
         module:[name:test,
               path:'sources/source_code_analysis/rar/test']:[
            file:[name:Test, path:Test]:[]],
         module:[name:test_1,
               path:'sources/source_code_analysis/rar/test_1']:[
            file:[name:Cross, path:Cross]:[]],
         module:[name:test_2,
               path:'sources/source_code_analysis/rar/test_2']:[
            file:[name:Desc, path:Desc]:[],
            file:[name:Necess, path:Necess]:[] ] ],
   rar:parse_sources_to_rar_database(Tree_1, _),
   rar:set_rar_db_time_variable(sca_tests_1).


/******************************************************************/


