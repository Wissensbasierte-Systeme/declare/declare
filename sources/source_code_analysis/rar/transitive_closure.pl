

/******************************************************************/
/***                                                            ***/
/***          RAR:  Transitive Closure                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/*** implementation ***********************************************/


/* transitive_closure_inv(+FMPAs_1, -FMPAs_2) <-
      */

transitive_closure_inv(FMPAs_1, FMPAs_2) :-
   transitive_closure_inv(FMPAs_1, FMPAs_1, FMPAs_2).

transitive_closure_inv(Current_FMPAs, [], Current_FMPAs) :-
   !.
transitive_closure_inv(Current_FMPAs_1, New_FMPAs_1, Result) :-
   maplist( is_called_by,
      New_FMPAs_1, New_FMPAs_2 ),
   union(New_FMPAs_2, New_FMPAs_3),
   subtract(New_FMPAs_3, Current_FMPAs_1, New_FMPAs_4),
   union(Current_FMPAs_1, New_FMPAs_4, Current_FMPAs_2),
   transitive_closure_inv(Current_FMPAs_2, New_FMPAs_4, Result).

is_called_by((File_2, MPA_2), FMPAs_2) :-
   findall((File_1, MPA_1),
      calls_ff_inv(File_2, MPA_2, File_1, MPA_1),
      FMPAs_1 ),
   list_to_ord_set(FMPAs_1, FMPAs_2).


/* transitive_closure_tree_inv(+FMPA, -Tree) <-
      */

transitive_closure_tree_inv(FMPA, Tree_2) :-
   fmpas_to_xml(FMPA, Tree_1),
   transitive_closure_tree_inv([FMPA], Tree_1, Tree_2).


transitive_closure_tree_inv(Current_1, Tree_1, Tree_2) :-
   xml_to_fmpas(Tree_1, (File, (M:P)/A)),
   is_called_by((File, (M:P)/A), FMPAs_1),
   subtract(FMPAs_1, Current_1, FMPAs_2),
   union(Current_1, FMPAs_2, Current_2),
   not( FMPAs_2 = [] ),
   maplist( fmpas_to_xml,
      FMPAs_2, FMPAs_XML_1 ),
   maplist( transitive_closure_tree_inv(Current_2),
      FMPAs_XML_1, FMPAs_XML_2 ),
   term_to_atom((M:P)/A, Name),
   Tree_2 = atom:[name:Name, file:File, module:M,
      predicate:P, arity:A]:FMPAs_XML_2,
   !.
transitive_closure_tree_inv(_, T, T).


fmpas_to_xml(FMPA, FMPA_XML) :-
   FMPA = (File, (M:P)/A),
   term_to_atom((M:P)/A, Name),
   FMPA_XML =
      atom:[name:Name,
      file:File, module:M, predicate:P, arity:A]:[].
xml_to_fmpas(FMPA_XML, FMPA) :-
   [File, M, P, A] := FMPA_XML@[file, module, predicate, arity],
   FMPA = (File, (M:P)/A).


/*** Tests ********************************************************/


test(transitive_closure, 1) :-
   transitive_closure_inv([(_, (rar:calls_ff_inv)/4)], X),
   sublist(rar:constraint_1,
      X, Y ),
   writeln_list(Y).

test(transitive_closure, 2) :-
   transitive_closure_tree_inv(
      (_, (rar:calls_ff_inv)/4),
      Tree),
   hierarchy_to_browser_ddk(Tree, _).

constraint_1((_, (_:test)/_)).


/******************************************************************/


