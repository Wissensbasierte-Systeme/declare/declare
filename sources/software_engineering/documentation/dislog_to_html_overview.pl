

/******************************************************************/
/***                                                            ***/
/***          DisLog: Documentation                             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dislog_to_html_overview(Path) <-
      */

dislog_to_html_overview(Path) :-
   findall( [File, Pred],
      ( dislog_predicate_and_file(Pred, File),
        write(user, '.'), ttyflush ),
      Pairs_2 ),
   sort(Pairs_2, Pairs),
   group_tuples(Pairs, [1], Groups),
   length(Groups, N),
   write_list(user, [N, ' files \n']),
   maplist( predicate_group_to_html_paragraph,
      Groups, Lists ),
   predicate_to_file( Path,
      field_notation_to_xml(html:Lists) ).


/* dislog_predicate_and_file(Predicate, File) <-
      */

dislog_predicate_and_file(Predicate, File) :-
   dislog_sources_with_absolute_path(Sources),
   member(File, Sources),
   nl(user), writeln(user, File),
   dislog_file_to_predicate(File, Predicate).

dislog_file_to_predicate(File, P/A) :-
   source_file(Atom, File),
   functor(Atom, P, A).
/*
dislog_predicate_and_file(Predicate, File) :-
   rar:rule(_, _, Predicate, File, _, _).
*/

dislog_sources_with_absolute_path(Sources) :-
   dislog_sources(Sources_1),
   dislog_variable_get(source_path, Path),
   maplist( concat(Path),
      Sources_1, Sources_2 ),
   maplist( dislog_source_adapt_ending,
      Sources_2, Sources ).

dislog_source_adapt_ending(Path_1, Path_2) :-
   concat(Path_1, '.pl', Path_3),
   ( file_exists(Path_3) ->
     Path_2 = Path_3
   ; Path_2 = Path_1 ).


/*** implementation ***********************************************/


/* predicate_group_to_html_paragraph(Group, Html) <-
      */

predicate_group_to_html_paragraph(Group, Html) :-
   first(Group, [File, _]),
   ( dislog_variable_get(home, '/', Path),
     concat(Path, F, File)
   ; F = File ),
   findall( li:[Predicate],
      member([File, Predicate], Group),
      Items ),
   P = a:[href:F]:[F],
   Html = p:[h2:[P], ol:Items].


/* group_tuples(Tuples, Ns, Groups) <-
      */

group_tuples(Tuples, Ns, Groups) :-
   ( foreach(Tuple, Tuples), foreach(Xs-Tuple, Tuples_2) do
        nth_multiple(Ns, Tuple, Xs) ),
   sort(Tuples_2, Tuples_3),
   group_tuples_sub(Tuples_3, Groups).

group_tuples_sub([Xs-T1, Xs-T2|Ts], [[T1|G]|Gs]) :-
   !,
   group_tuples_sub([Xs-T2|Ts], [G|Gs]).
group_tuples_sub([Xs_1-T1, Xs_2-T2|Ts], [[T1]|Gs]) :-
   Xs_1 \= Xs_2,
   !,
   group_tuples_sub([Xs_2-T2|Ts], Gs).
group_tuples_sub([_-T], [[T]]).
group_tuples_sub([], []).


/******************************************************************/


