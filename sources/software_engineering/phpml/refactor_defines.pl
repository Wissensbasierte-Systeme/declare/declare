

/* transform_defines(Fng, File_1, File_2) <-
      */

transform_defines___(Fng, File_1, File_2) :-
   dislog_variable_get(example_path, Examples),
   concat([Examples, 'phpml/', File_1], Path_1),
   concat([Examples, 'phpml/', File_2], Path_2),
   dislog_variable_get(source_path, Sources),
   concat([Sources, 'development/phpml/', Fng], Path),
   writeln(user, transform_defines(Path, Path_1, Path_2)),
   transform_defines(Path, Path_1, Path_2).

transform_defines(Fng, File_1, File_2) :-
   dread(xml, File_1 , [Item_1]),
   make_ref_class(Item_1),
   find_define_statements(Item_1),
   fn_transform_fn_item_fng(Fng, Item_1, Item_2),
   retract_all(user:constclass(_)),
   retract_all(user:constnames(_)),
   dwrite(xml, File_2, Item_2).


/* make_ref_class(Item) <-
      */

make_ref_class(Item) :-
   M = modifiers:[]:[modifier:[]:[const]],
   O = operator:[ type:'assignment',
      associativity:'left-to-right', precedence:'5' ]:[ '=' ],
   findall( Member_Decl,
      ( C := Item^_^'constant-declaration'^'constant-expression',
        N := C^constant@name,
        V := C^literal^content::'*',
	T := C^literal@type,
        Member_Decl = 'member-declaration':[name:N]:[
           M, constant:[name:N, kind:'class-constant']:[N],
           O, literal:[type:T]:V, symbol:[]:[';'] ] ),
      Member_Decls ),
   Class_Decl = 'class-declaration':[
      name:'Constants', 'is-interface':true,
      'is-abstract':false, 'is-final':false ]:[
      superclass:[]:[], interfaces:[]:[],
      keyword:[]:[interface], identifier:[]:['Constants'],
      block:[]:[
         symbol:[]:['{'], Member_Decls, symbol:[]:['}'] ] ],
   assert(user:constclass(Class_Decl)).	  


/* find_define_statements(Item) <-
      */

find_define_statements(Item) :-
   findall( Name,
      Name := Item^_^'constant-declaration'
         ^'constant-expression'^constant@name,
      Names ),
   assert(user:constnames(Names)).


