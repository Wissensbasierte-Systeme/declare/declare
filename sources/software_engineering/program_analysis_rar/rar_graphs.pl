

/******************************************************************/
/***                                                            ***/
/***           RAR:  Defines and Calls                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


defines_fp(Program, File, Predicate) :-
   defines_or_calls_fp(Program, File, head, Predicate).

calls_fp(Program, File, Predicate) :-
   defines_or_calls_fp(Program, File, body, Predicate).

defines_or_calls_fp(Program, File, Selector, Predicate) :-
   Rule := Program^definition^rule,
   File := Rule@file,
   Predicate := Rule^Selector^atom@predicate.


calls_uu(Program, (Unit_1-Unit_2):N) :- 
   unit(Unit_1), unit(Unit_2),
   Unit_1 =\= Unit_2,
   findall( Predicate,
      calls_uu_pred(Program, (Unit_1-Unit_2):Predicate),
      Predicates ),
   length(Predicates, N).

calls_uu_pred(Program, (Unit_1-Unit_2):Predicate) :-
   calls_fp(Program, File_1, Predicate),
   defines_fp(Program, File_2, Predicate),
   path_extract(unit, File_1, Unit_1),
   path_extract(unit, File_2, Unit_2).


calls_uu_reduce(Program, K, Reduced_Graph) :-
   findall( Unit_1-Unit_2,
      ( calls_uu(Program, (Unit_1-Unit_2):N),
        N > K ),
      Edges ),
   edges_to_reduced_graph(Edges, Reduced_Graph).


/******************************************************************/


