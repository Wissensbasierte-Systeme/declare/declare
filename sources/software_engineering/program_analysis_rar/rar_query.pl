

/******************************************************************/
/***                                                            ***/
/***           RAR:  Query                                      ***/
/***                                                            ***/
/******************************************************************/


:- module( rar_query, [
      calls_ff/2 ] ).


/*** interface ****************************************************/


/* calls_pp(?Predicate_1, ?Predicate_2) <-
      */

calls_pp(MPA_1, MPA_2) :-
   rar:calls_pp(MPA_1, MPA_2).


/* calls_packages_ff_pp(L1:X1, L2:X2,
         File_1, File_2, Predicate_1, Predicate_2) <-
      */

calls_packages_ff_pp(L1:X1, L2:X2,
      File_1, File_2, Predicate_1, Predicate_2) :-
   dislog_package_to_files(L1:X1, Files_1),
   dislog_package_to_files(L2:X2, Files_2),
   calls_ff_pp(Files_1, Files_2,
      File_1, File_2, Predicate_1, Predicate_2).


/* calls_ff(File_1, File_2) <-
      */

calls_ff(File_1, File_2) :-
   rar:calls_ff(File_1, File_2).


/* calls_ff(Files_1, Files_2, File_1, File_2) <-
      */

calls_ff(Files_1, Files_2, File_1, File_2) :-
   calls_ff_pp(Files_1, Files_2, File_1, File_2, _, _).


/* calls_ff_pp(Files_1, Files_2,
         File_1, File_2, Predicate_1, Predicate_2) <-
      */

calls_ff_pp(Files_1, Files_2,
      File_1, File_2, Predicate_1, Predicate_2) :-
   rar:select(rule, Predicate_1, File_1, Rule),
   member(File_1, Files_1),
   rar:calls_pp(Rule, _, Predicate_2),
   rar:select(rule, Predicate_2, File_2, _),
   Predicate_2 \= (user:test)/2,
   Predicate_2 \= (user:dislog_variable)/2,
   Predicate_2 \= (user:dportray)/2,
   Predicate_2 \= (user:dportray)/3,
   member(File_2, Files_2).


/******************************************************************/


