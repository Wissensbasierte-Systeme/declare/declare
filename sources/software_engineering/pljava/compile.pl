

/******************************************************************/
/***                                                            ***/
/***       DisLog:  PLcafe Compiler                             ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(
      source_path_plcafe, '/home/seipel/DisLog/sources_plcafe/').


pljava_files('basic_algebra/basics', [
   dummies_plcafe, specials_plcafe, loops,
   arithmetic, lists, elementary, input_output,
   meta_predicates, prolog_database,
   names, current_num, global_variables,
   fn_query,
   pillow_plcafe_listing, xml_pillow_to_fn ]).
   

pljava_compile(Module) :-
   pljava_files(Module, Files),
   dislog_variable_get(source_path_plcafe, Sources),
   concat(Sources, Module, Directory),
   ( foreach(File, Files) do
        pljava_compile(Directory, File) ).

pljava_compile(Directory, File) :-
   name_append([Directory, '/', File], Path),
   writeln(user, Path),
   name_append([
      'cd ', Directory, ';',
      'pljava ', File, '.pl;',
      'plcomp -cp . -clean ', File, '.pl'],
      Command),
   us(Command).


/******************************************************************/


