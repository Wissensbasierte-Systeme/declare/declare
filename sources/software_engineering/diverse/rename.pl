

/******************************************************************/
/***                                                            ***/
/***         DisLog:  Renaming Files                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* add_pl_extension_to_dislog_files <-
      */

add_pl_extension_to_dislog_files :-
   dislog_sources(Sources),
   dislog_variable_get(source_path, Source_Path),
   checklist( add_pl_extension_to_dislog_file(Source_Path),
      Sources ).


/*** implementation ***********************************************/


/* add_pl_extension_to_dislog_file(Source_Path, File) <-
      */

add_pl_extension_to_dislog_file(Source_Path, File) :-
   concat(Source_Path, File, Path),
   file_name_extension(_, '', Path),
   file_name_extension(Path, pl, Path_pl),
   \+ exists_file(Path_pl),
   rename_file(Path, Path_pl),
   !.
add_pl_extension_to_dislog_file(_, File) :-
   write_list([File, ' not renamed.\n']).


/******************************************************************/


