

/******************************************************************/
/***                                                            ***/
/***          Refactoring:  Dialogs                             ***/
/***                                                            ***/
/******************************************************************/


/* qm_dialog_name_to_refactored_dialog(
         Dialog_Name, Refactored_Dialog ) <-
      */

qm_dialog_name_to_refactored_dialog(
      Dialog_Name, Refactored_Dialog ) :-
   get_parsed_dialog( Dialog_Name,
      dialog(Dialog_Name,Dialog_Description) ),
   qm_join_attribute_lists( parts, modifications,
      Dialog_Description, Dialog_Description_2 ),
   replace_patterns([
      [ menu_item(A,@default,B,@off,@nil,C),
        menu_item(A,B,C) ] ],
      Dialog_Description_2, Dialog_Description_3 ),
   qm_collect_atoms(layout,[below,right,area],
      Dialog_Description_3,Dialog_Description_4),
   Refactored_Dialog =..
      [dialog,Dialog_Name,Dialog_Description_4].
       
qm_join_attribute_lists(Attribute1,Attribute2,Term1,Term2) :-
   member(Attribute1:=List1,Term1),
   member(Attribute2:=List2,Term1),
   join_attribute_lists(List1,List2,List),
   substitute(
      Attribute1:=List1,Term1,Attribute1:=List,Term3),
   delete(Term3,Attribute2:=List2,Term2).

qm_collect_atoms(Attribute,Predicates,Term1,Term2) :-
   member(Attribute:=Description1,Term1),
   collect_atoms(Predicates,Description1,Description2),
   substitute(Attribute:=Description1,Term1,
      Attribute:=Description2,Term2).


/* replace_patterns_in_program(Substitutions,
         Program_1,Program_2,Additional_Rules) <-
      */

replace_patterns_in_program(Substitutions,
      Program_1, Program_2, Additional_Rules ) :-
   writeln(Substitutions),
   maplist( substitution_to_rule,
      Substitutions, Additional_Rules ),
   writeln(Additional_Rules),
   maplist( replace_patterns(Substitutions),
      Program_1, Program_2 ).

% substitution_to_rule([X,Y],Y':-'X).
substitution_to_rule([X,Y], ':-'(Y, X)).


/* replace_patterns(Substitution,Term_1,Term_2) <-
      Successively replace subterms of Term_1 according to
      Substitution, thus yielding the new term Term_2. */

replace_patterns([[X,Y]|Substitution],Term_1,Term_2) :-
   replace_pattern([X,Y],Term_1,Term_3),
   replace_patterns(Substitution,Term_3,Term_2).
replace_patterns([],Term,Term).


/* replace_pattern([X,Y],Term_1,Term_2) <-
      If Term_1 contains the subterm X,
      then substitute X with Y,
      thus yielding the new term Term_2. */

replace_pattern([X,Y],X,Y) :-
   !.
replace_pattern([X,Y],List_1,List_2) :-
   is_list(List_1),
   !,
   replace_pattern_loop([X,Y],List_1,List_2).
replace_pattern([X,Y],U_1:=List_1,U_2:=List_2) :-
   !,
   replace_pattern([X,Y],U_1,U_2),
   replace_pattern([X,Y],List_1,List_2).
replace_pattern([X,Y],Term_1,Term_2) :-
   Term_1 =.. [P|Args_1],
   !,
   replace_pattern_loop([X,Y],Args_1,Args_2),
   Term_2 =.. [P|Args_2].
replace_pattern(_,Term,Term).

replace_pattern_loop([X,Y],Args_1,Args_2) :-
   findall( Arg_2,
      ( member(Arg_1,Args_1),
        replace_pattern([X,Y],Arg_1,Arg_2) ),
      Args_2 ).
/*
replace_pattern_loop([X,Y],List_1,List_2) :-
   maplist( replace_pattern([X,Y]),
      List_1, List_2 ).
*/


/* join_attribute_lists(List_1,List_2,List_3) <-
      */

join_attribute_lists(List_1,List_2,List_3) :-
   join_attribute_lists(List_1,List_2,[],List_3).

join_attribute_lists(
      [Attr:=Desc|Elems1],Elems2,List1,List3) :-
   member(Attr:=Mod,Elems2),
   L = [Desc,Mod],
   append(List1,[Attr:=L],List2),
   join_attribute_lists(Elems1,Elems2,List2,List3).
join_attribute_lists(
      [Attr:=Desc|Elems1],Elems2,List1,List3) :-
   append(List1,[Attr:=[Desc]],List2),
   join_attribute_lists(Elems1,Elems2,List2,List3).
join_attribute_lists([],_,List,List).


/* collect_atoms(Predicates,Term_List,New_Term_List) <-
      */

collect_atoms(Predicates,Term_List,New_Term_List) :-
   collect_atoms(Predicates,Term_List,[],New_Term_List).

collect_atoms([Predicate|Predicates],Term_List,
     Term_List1,Term_List2) :-
   collect_predicate_args(Predicate,Term_List,[]),
   collect_atoms(Predicates,Term_List,Term_List1,
     Term_List2).
collect_atoms([Predicate|Predicates],Term_List,
     Term_List1,Term_List2) :-
   collect_predicate_args(Predicate,Term_List,Term),
   append(Term_List1,[Term],Term_List3),
   collect_atoms(Predicates,Term_List,Term_List3,
     Term_List2).
collect_atoms([],_,Term_List,Term_List).

collect_predicate_args(Predicate,Term_List,Term) :-
   findall( Args,
      ( member(Y,Term_List),
        Y =.. [Predicate|Args]),
      L ),
   L \= [],
   !,
   Term =.. [Predicate|L].
collect_predicate_args(_,_,[]).


/* qm_open_dialog(Dialog_Name) <-
     */

qm_open_dialog(Dialog_Name) :-
   make_dialog_chain(Dialog_Name,C),
   make_dialog(D1,Dialog_Name),
   fill_dialog(D1,C),
   make_dialog(D2,buttons),
   send(D2,below,D1),
   send(D1,open).


/* qm_diags(List) <-
     show list of dialog names */

qm_diags(List) :-
   read_dialog_list(L),
   writeln(L),
   maplist( get_dialog_name,
      L, List ).


/* qdb_store_diag_descriptions <-
     */

qdb_store_diag_descriptions :-
   qm_diags(L1),
   maplist( simplify_dialog,
      L1, L2 ),
   maplist( dialog_list_to_dislog_clause,
      L2, L3 ),
   qdb_store(L3,'qualimed/orthopaedie/qm_simp_dialogs').

dialog_list_to_dislog_clause(DL,[Atom]) :-
   Atom =.. [dialog|DL].


/* simplify_dialog_layout(Dialog,Layout) <-
      */

simplify_dialog_layout(Dialog,Layout) :-
   get_dialog_layout_predicates(Dialog,[area]),
   get_dialog_layout(Dialog,L),
   simplify_area_list(L,Layout).
simplify_dialog_layout(Dialog,Layout) :-
   get_dialog_layout(Dialog,Layout).

simplify_layout(Layout1,Layout2) :-
   get_layout_predicates(Layout1,[area]),
   simplify_area_list(Layout1,Layout2).
simplify_layout(Layout1,Layout2) :-
   get_layout_predicates(Layout1,Pred_List),
   maplist( simplify_layout(Layout1),
      Pred_List, Layout2 ).

simplify_area_list(List1,Area_Atom) :-
  maplist( simplify_area_atom,
     List1, List2 ),
  Area_Atom =.. [areas|List2].

simplify_area_atom(area(Item,X),[Item,Coordinates]) :-
   X =.. [area|Coordinates].

simplify_layout(Layout,Pred,CC_Atom) :-
   Atom =.. [Pred,X,Y],
   findall( X-Y ,
      member(Atom,Layout),
      Edges ),
   findall( V,
      ( member(V-_,Edges)
      ; member(_-V,Edges) ),
      Vertices ),
   vertices_edges_to_ugraph(Vertices,Edges,Graph),
%  writeln(Graph),
   symmetric_closure(Graph,Graph2),
   reduce(Graph2,Reduced_Graph),
   vertices(Reduced_Graph,CCs),
   graph_to_conn_component_subgraphs(Graph,CCs,Subgraphs),
   maplist( top_sort,
      Subgraphs, CC_Rev ),
   maplist( reverse,
      CC_Rev, CC ),
   CC_Atom =.. [Pred,CC].
   

/* graph_to_conn_component_subgraphs(Graph,CCs,Subgraphs) <-
      */

graph_to_conn_component_subgraphs(Graph,CCs,Subgraphs) :-
   maplist( qm_get_subgraph(Graph),
      CCs, Subgraphs ).

qm_get_subgraph(Graph,CC,Subgraph) :-
   qm_get_subgraph(Graph,CC,[],Subgraph).

qm_get_subgraph(Graph,[Edge|Edges],List1,List3) :-
   member(Edge - X,Graph),
   append(List1,[Edge - X],List2),
   qm_get_subgraph(Graph,Edges,List2,List3).
qm_get_subgraph(Graph,[_|Edges],List1,List2) :-
   qm_get_subgraph(Graph,Edges,List1,List2).
qm_get_subgraph(_,[],List,List).


/* connected_components_layout(Pred,Layout1,Layout2) <-
      */

connected_components_layout(Pred,Layout1,Layout2) :-
   Atom =.. [Pred,X,Y],
   findall( Y-X ,
      member(Atom,Layout1),
      Edges ),
   findall( V,
      ( member(V-_,Edges)
      ; member(_-V,Edges) ),
      Vertices ),
   vertices_edges_to_ugraph(Vertices,Edges,Graph),
%  write(Graph),nl,
   top_sort(Graph,List),
%  write(List),
   Layout2 =.. [below|[List]].


/******************************************************************/


