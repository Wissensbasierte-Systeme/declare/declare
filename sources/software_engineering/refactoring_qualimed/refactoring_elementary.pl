

/******************************************************************/
/***                                                            ***/
/***   Refactoring:  Elementary Predicates                      ***/
/***                                                            ***/
/******************************************************************/


character_is_in(Character,Characters) :-
   member(Character,Characters).


atom_character(C) :-
   letter(C);
   digit(C);
   character_is_in(C,"_").
atom_character(47).


letter(C) :-
   lowercase(C);
   uppercase(C).

lowercase(C) :-
   C >= 97,
   C =< 122.
uppercase(C) :-
   C >= 65,
   C =< 90.


eof(-1).
eol(10).
eol(13).

tabulator(9).


whitespace(X) :-
   character_is_in(X," ");
   tabulator(X).


to_uppercase(C,C) :-
   uppercase(C),
   !.
to_uppercase(C1,C2) :-
   lowercase(C1),
   C2 is C1 - 32.


/*
open_parenthesis(40).
close_parenthesis(41).
open_bracket(91).
close_bracket(93).
underscore(95).
komma(44).
hyphen(45).
point(46).
slash(47).
double_point(58).
doublepoint(58).
apostroph(39).
blank(32).
equal(61).
*/


/*****************************************************************/


