

/******************************************************************/
/***                                                            ***/
/***          Refactoring:  QualiMed Dialog Parser              ***/
/***                                                            ***/
/******************************************************************/


refactoring_read_from_file(File) :-
   dislog_variable_get(home, DisLog),
   concat(DisLog,
      '/examples/refactoring/qm_parser_input',File).

refactoring_write_to_file(File) :-
   dislog_variable_get(home, DisLog),
   concat(DisLog,
      '/examples/refactoring/qm_parser_output',File).


/*** interface ****************************************************/


/* parse_dialog(Dialog,X) <-
      */

parse_dialog(Dialog,X) :-
   dialog(Dialog,Description),
   member(parts:=Parts,Description),
   member(modifications:=Modifications,Description),
   member(layout:=Layout,Description),
   refactoring_read_from_file(InFile),
   predicate_to_file( InFile,
      ( writeln(Parts),
        writeln(Modifications),
        write(Layout) ) ),
   refactoring_write_to_file(Outfile),
   xpce_dialog_parse_file_to_file(InFile,Outfile),
   predicate_from_file( Outfile,
      read(X) ).


/* get_parsed_dialog(Dialog_Name,Dialog_Term) <-
      */

get_parsed_dialog(Dialog_Name,Dialog_Term) :-
   refactoring_write_to_file(File),
   get_parsed_dialog(File,Dialog_Name,Dialog_Term).

get_parsed_dialog(File,Dialog_Name,Dialog_Term) :-
   dconsult(File,P),
   !,
   member([dialog(Dialog_Name,Description)],P),
   remove_subatom_from_term(
      'variable_',Description,Description_2),
   Dialog_Term = dialog(Dialog_Name,Description_2).

/*
get_parsed_dialog(Dialog_Name,Dialog_Term) :-
   parsed_dialog(Dialog_Name,dialog(_,X)),
   !,
   restore_xpce_variables(X,Y),
   Dialog_Term = dialog(Dialog_Name,Y).
*/


/* get_parsed_dialog_items(Dialog_Name,Item,Description) <-
      */

get_parsed_dialog_items(Dialog_Name,Item,Description) :-
   parsed_dialog(Dialog_Name,dialog(Dialog_Name,X)),
   member(Item:=Description,X).
get_parsed_dialog_items(Dialog_Name,Parts,Modifications,Layout) :-
   parsed_dialog(Dialog_Name,dialog(Dialog_Name,X)),
   member(parts:=Parts,X),
   member(modifications:=Modifications,X),
   member(layout:=Layout,X). 


/* parsed_dialogs(Dialogs) <-
     */

parsed_dialogs(Dialogs) :-
   qm_entity_types(QM_Entity_Types),
   maplist( parsed_dialog,
      QM_Entity_Types, Dialogs ).


/* parsed_dialog(
         Dialog_Name,dialog(Dialog_Name,Dialog_Desc)) <-
      */

parsed_dialog(Dialog_Name,dialog(Dialog_Name,Dialog_Desc)) :-
   get_parsed_qm_dialog_description(Dialog_Name,Dialog_Desc).


/* get_parsed_qm_dialog_description(Dialog_Name,New_Dialog) <-
      */

get_parsed_qm_dialog_description(Dialog_Name,New_Dialog) :-
   dialog(Dialog_Name,Description),
   member(parts:=Parts_1,Description),
   member(modifications:=Modif_1,Description),
   parse_dialog( Dialog_Name,
      dialog(Parts_2,Modif_2,Layout_2) ),
   !,
   qm_eliminate_variables(Parts_1,Parts_2,Parts),
   qm_eliminate_variables(Modif_1,Modif_2,Modif),
   first(Parts_2,Object_2),
   New_Dialog = [
      object:=Object_2, parts:=Parts,
      modifications:=Modif, layout:=Layout_2 ].


/* get_parsed_qm_dialog_object_description(
         Dialog_Name,Desc_List) <-
      */

get_parsed_qm_dialog_object_description(Dialog_Name,Desc_List) :-
   get_parsed_qm_dialog_item_description(Dialog_Name,object,
      Desc_List).
get_parsed_qm_dialog_parts_description(Dialog_Name,Desc_List) :-
   get_parsed_qm_dialog_item_description(Dialog_Name,parts,
      Desc_List).
get_parsed_qm_dialog_modifications_description(Dialog_Name,
     Desc_List) :-
   get_parsed_qm_dialog_item_description(Dialog_Name,modifications,
      Desc_List).
get_parsed_qm_dialog_layout_description(Dialog_Name,Desc_List) :-
   get_parsed_qm_dialog_item_description(Dialog_Name,layout,
      Desc_List).

get_parsed_qm_dialog_item_description(
      Dialog_Name,Attr,Desc_List) :-
   get_parsed_qm_dialog_description(Dialog_Name,New_Dialog),
%  writeln(New_Dialog),
   member(Attr:=Desc_List,New_Dialog).


/* qm_eliminate_variables(Items,Atoms,New_Items) <-
     */

qm_eliminate_variables(Items,Atoms,New_Items) :-
   pair_lists(Items,Atoms,Pairs),
   maplist( qm_eliminate_variable_from_item,
      Pairs, New_Items ).

qm_eliminate_variable_from_item(
   [ _ := Desc, Atom ], Atom := Desc ).


/* restore_xpce_variables(
         Dialog_Description1,Dialog_Description2) <-
      */

restore_xpce_variables(Description_1,Description_2) :-
   member(parts:=Parts,Description_1),
   findall( X,
      member(X:=_,Parts),
      Variable_Atoms_1 ),
   findall( Item_Name,
      ( member(_:=Atom,Parts),
        Atom =.. [_|[Item_Name|_]] ),
      Item_Names_1 ),
   maplist( atom_to_xpce_var_atom,
      Item_Names_1, Item_Names_2 ),
   pair_lists(Variable_Atoms_1,Item_Names_2,Substitutions),
   replace_patterns(Substitutions,Description_1,Description_2).

atom_to_xpce_var_atom(Atom1,Atom2) :-
   name(Atom1,List1),
   maplist( eliminate_non_letter_chars,
      List1, List2 ),
   construct_xpce_variable_character_list(List2,List3),
   name(Atom2,List3).

eliminate_non_letter_chars(C,C) :-
   letter(C).
eliminate_non_letter_chars(_,95).

construct_xpce_variable_character_list([C1|Chars1],Chars2) :-
   to_uppercase(C1,C2),
   construct_xpce_variable_character_list(Chars1,[C2],Chars2).

construct_xpce_variable_character_list([95,C2|Chars],
      List1, List3 ) :- 
   to_uppercase(C2,C),
   append(List1,[95,C],List2),
   construct_xpce_variable_character_list(Chars,List2,List3).
construct_xpce_variable_character_list([C|Chars],List1,List3) :-
   append(List1,[C],List2), 
   construct_xpce_variable_character_list(Chars,List2,List3).
construct_xpce_variable_character_list([],List,List).


/* enumerate_double_list_elements(List_1,List_2) <-
      */

enumerate_double_list_elements(List,List) :-
   no_doubles(List).
enumerate_double_list_elements(List1,List2) :-
   remove_duplicates(List1,List_Elements),
   enumerate_double_list_elements(List_Elements,List1,List2).

enumerate_double_list_elements([Elem|Elems],List1,List3) :-
   findall( I,
      nth(I,List1,Elem),
      Position_List ),
   length(Position_List,L),
   L > 1,
   !,
   enumerate_elem(Position_List,Elem,List1,List2),
   enumerate_double_list_elements(Elems,List2,List3).
enumerate_double_list_elements([_|Elems],List1,List2) :-
   enumerate_double_list_elements(Elems,List1,List2).
enumerate_double_list_elements([],List,List).


/* enumerate_elem(Position_List,Elem,List1,List2) <-
      */

enumerate_elem(Position_List,Elem,List1,List2) :-
   enumerate_elem(Position_List,1,Elem,List1,List2).

enumerate_elem([Position|Positions],Count,Elem,List1,List3) :-
   int_to_atom(Count,Count_Atom),
   concat_atom([Elem,Count_Atom],New_Elem),
   substitute_list_element_at_position(
      Position, New_Elem, List1, List2 ),
   New_Count is Count + 1,
   enumerate_elem(Positions,New_Count,Elem,List2,List3).
enumerate_elem([],_,_,List,List).


/* substitute_list_element_at_position(
         Position,New_Elem,List1, List2 ) <-
      Substitute the element at position Position in List1
      with the element New_Elem,
      thus yielding as result the list List2. */

substitute_list_element_at_position(
      Position,New_Elem,List1,List2) :-
   length(List1,L),
   L1 is Position - 1,
   L2 is L - Position,
   prefix(Pref,List1),
   length(Pref,L1),
   suffix(Suff,List1),
   length(Suff,L2),
   append([Pref,[New_Elem],Suff],List2).


/* write_dialogs_without_variables_to_file(File_1,File_2) <-
      */

write_dialogs_without_variables_to_file(File_1,File_2) :-
   see(File_1),
   refactoring_write_to_file(File_2),
   tell(File_2),
   writeln(':- disjunctive.'),
   get0(Char),
   refactoring_put_and_get_character(Char),
   write(':- prolog.'),
   told,
   seen.

refactoring_put_and_get_character(Char) :-
   uppercase(Char),
   !,
%  name(X,[Char]),
%  concat_atom(['variable_',X],S),
   write('variable_'),
   put(Char),
   get0(Char_2),
   refactoring_put_and_get_character(Char_2).
refactoring_put_and_get_character(Char) :-
   character_is_in(Char," (,"),
   !,
   put(Char),
   get0(Char_2),
   underscore_test(Char_2),
   refactoring_put_and_get_character(Char_2).   
refactoring_put_and_get_character(Char) :-
   eof(Char),
   !.
refactoring_put_and_get_character(Char) :-
   put(Char),
   get0(Char_2),
   refactoring_put_and_get_character(Char_2).

underscore_test(Char) :-
   character_is_in(Char,"_"),
   write('variable_').
underscore_test(_) :-
   !.


/* remove_subatom_from_term_in_dialog(
         Sub_Atom,[dialog(X,Y)],Z) <-
      */

remove_subatom_from_term_in_dialog(Sub_Atom,[dialog(X,Y)],Z) :-
   writeln(X),
   remove_subatom_from_term(Sub_Atom,Y,Z).
remove_subatom_from_term_in_dialog(_,[dialog(X,_)],X).

remove_subatom_from_term(Sub_Atom,Term_1,Term_2) :-
   atomic(Term_1),
   !,
   remove_subatom(Sub_Atom,Term_1,Term_2).
remove_subatom_from_term(Sub_Atom,Term_1,Term_2) :-
   is_list(Term_1),
   !,
   maplist( remove_subatom_from_term(Sub_Atom),
      Term_1, Term_2 ). 
remove_subatom_from_term(Sub_Atom,Term_1,Term_2) :-
   Term_1 =.. [Functor_1|Args_1],
   !,
%  write_list([Functor_1,' ',Args_1]),nl,
   remove_subatom_from_term(Sub_Atom,Functor_1,Functor_2),
   remove_subatom_from_term(Sub_Atom,Args_1,Args_2),
   Term_2 =.. [Functor_2|Args_2].

remove_subatom(Subatom,Atom_1,Atom_3) :-
   sub_atom(Atom_1,Before,_,After,Subatom),
   !,
   atom_prefix_with_length(New_Atom_1,Before,Atom_1),
   atom_suffix_with_length(New_Atom_2,After,Atom_1),
   concat_atom([New_Atom_1,New_Atom_2],Atom_2),
   remove_subatom(Subatom,Atom_2,Atom_3).
remove_subatom(_,Atom,Atom) :-
   !.

atom_suffix_with_length(Suffix,Length,Atom) :-
   name(Atom,List),
   suffix_with_length(Sublist,Length,List),
   name(Suffix,Sublist).
atom_prefix_with_length(Prefix,Length,Atom) :-
   name(Atom,List),
   prefix_with_length(Sublist,Length,List),
   name(Prefix,Sublist).

suffix_with_length(Sublist,Length,List) :-
   suffix(Sublist,List),
   length(Sublist,Length).

prefix_with_length(Sublist,Length,List) :-
   prefix(Sublist,List),
   length(Sublist,Length).


/******************************************************************/


