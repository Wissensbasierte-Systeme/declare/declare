

/******************************************************************/
/***                                                            ***/
/***          Refactoring:  Demos                               ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      refactoring_demo/1,
      refactoring_demo/2,
      refactoring_demo/3,
      refactoring_demo/4.


refactoring_demo(1,Dialog,Description_1,Description_2) :-
   get_parsed_dialog(Dialog,Description_1),
   replace_patterns(
      [ [ menu_item(A,@default,B,@off,@nil,C),
          short_menu_item(A,B,C) ],
        [ [ label:=Geb_Datum, length:=Length ],
          label_length(Geb_Datum,Length) ] ],
      Description_1, Description_2 ).

refactoring_demo(2,Dialog,Dialog_Term) :-
   get_parsed_dialog(Dialog,dialog(Dialog,D)),
   member(object:=Object,D),
   member(parts:=Parts,D),
   member(modifications:=Modifications,D),
   member(layout:=Layout,D),
   join_attribute_lists(
      Parts,Modifications,Attribute_Descriptions),
   Dialog_Term =
      dialog(Dialog,[
         object:=Object,
         parts:=Attribute_Descriptions,
         layout:=Layout ]).

refactoring_demo(3,Dialog,Dialog_Term) :-
   qm_dialog_name_to_refactored_dialog(Dialog,Dialog_Term).

refactoring_demo(4) :-
   dislog_variable_get(home, DisLog),
   concat(DisLog,
      '/examples/refactoring/dialogs',File_1),
   concat(DisLog,
      '/examples/refactoring/refactored_dialogs',File_2),
   write_qualimed_dialogs_to_output_file(File_1),
   write_refactored_dialogs_to_output_file(File_2).

refactoring_demo(5,Dialog,X) :-
   get_parsed_dialog(Dialog,dialog(Dialog,Y)),
   member(modifications:=M,Y),
   replace_pattern(
      [ menu_item(A, @default, B, @off, @nil, C),
        menu_item(A,B,C) ],
      M, X ).

refactoring_demo(6,R) :-
   replace_patterns(
      [ [a(X,y),c(X)],
        [b(X),c(X)] ],
      [ a(x,y),b(l),e(k),a(c,p) ], R ).

refactoring_demo(7,R) :-
   replace_patterns(
      [ [a(X,y),c(X)],
        [b(Y),c(Y)] ],
      [ a(x,y),b(l),[a(p,y)],e(k),a(c,p)], R ).

refactoring_demo(8,R) :-
   replace_patterns(
      [ [a(X,y), c(X)] ],
      [ [a(p,y)] ], R ).

refactoring_demo(9,Dialog_Name) :-
   get_parsed_dialog(Dialog_Name,X),
   dislog_variable_get(home, DisLog),
   concat(DisLog,
      '/examples/refactoring/qm_pp_simplified_dialogs',File),
   pretty_println_dialog(File,X).

refactoring_demo(10,Dialog_Name) :-
   get_parsed_dialog(Dialog_Name,X),
   replace_patterns([
      [ menu_item(A,@default,B,@off,@nil,C),
        menu_item(A,B,C) ] ],
      X, Y),
   dislog_variable_get(home, DisLog),
   concat(DisLog,
      '/examples/refactoring/qm_pp_simplified_dialogs',File),
   pretty_println_dialog(File,Y).

refactoring_demo(11,X) :-
   tps_fixpoint_iteration([
      [p(a,b)], [p(b,c)], [p(c,d)],
      [p([X,Y,Z])] - [p(X,Y),p(Y,Z)] ],
      X ).

refactoring_demo(12,Dialog) :-
   get_parsed_dialog(Dialog,dialog(_,D)),
   member(layout:=L,D),
   collect_atoms([below],L,M),
   writeln_list([L,M]).

refactoring_demo(13,Dialog,Description_1,Description_2) :-
   get_parsed_dialog(Dialog,Description_1),
   replace_patterns([
      [ menu_item(A,@default,B,@off,@nil,C),
        short_menu_item(A,B,C) ],
      [ [ label:=Geb_Datum, length:=Length ],
        label_length(Geb_Datum,Length) ] ],
      Description_1, Description_2 ).

refactoring_demo(14,Dialog,dialog(Dialog,D),Layout_2) :-
   get_parsed_dialog(Dialog,dialog(Dialog,D)),
   member(layout:=Layout_1,D),
   connected_components_layout(below,Layout_1,Layout_2).

refactoring_demo(15,Dialogs,Ref_Program,Rules) :-
   maplist( get_parsed_dialog,
      Dialogs, Program ),
   !,
   writeln(Program),
   replace_patterns_in_program([
      [ menu_item(A,@default,B,@off,@nil,C),
        short_menu_item(A,B,C) ],
      [ [ label:=Geb_Datum, length:=Length ],
        label_length(Geb_Datum,Length) ] ],
      Program, Ref_Program, Rules ).

refactoring_demo(16,P) :-
   refactoring_write_to_file(X),
   dconsult(X,Y),
   !,
   maplist( remove_subatom_from_term_in_dialog('variable_'),
      Y, P ).

refactoring_demo(17,X) :-
   remove_subatom_from_term('variable_',[
      object := variable_Patient,
      parts := [
         variable_Patient := dialog(variable_Patient),
         variable_Name := text_item(name),
         variable_Vorname := text_item(vorname),
         variable_Geb_variable_Datum := text_item(geb_datum),
         variable_Geschlecht := menu(geschlecht, choice),
         variable_Code := text_item(code) ],
      modifications:= [
         variable_Name := [length:=30],
         variable_Vorname := [length:=30],
         variable_Geb_variable_Datum := [
            label := 'variable_Geb. variable_Datum:',
            length:=10 ],
         variable_Geschlecht := [
            reference := point(0, 13),
            append := [
               menu_item(mnlich, @default,
                  variable_Mnlich, @off, @nil, m),
               menu_item(weiblich, @default,
                  variable_Weiblich, @off, @nil, w) ] ],
         variable_Code := [length:=30] ],
      layout := [
         below(variable_Vorname, variable_Name),
         below(variable_Geb_variable_Datum, variable_Vorname),
         below(variable_Geschlecht, variable_Geb_variable_Datum),
         below(variable_Code, variable_Geschlecht) ] ],
   X ).

refactoring_demo(18,Res) :-
   refactoring_write_to_file(X),
   dconsult(X,Y),
   !,
   member([dialog(buttons,Desc)],Y),
   member(layout:=Z,Desc),
   writeln(Z),
   remove_subatom_from_term('variable_',Z,Res).

refactoring_demo(18,Dialog_Name,Res) :-
   refactoring_write_to_file(X),
   dconsult(X,Y),
   !,
   writeln(Dialog_Name),
   member([dialog(Dialog_Name,Desc)],Y),
   remove_subatom_from_term('variable_',Desc,Res).

refactoring_demo(19,X) :-
   findall( A,
      ( qm_entity_type(E),
        refactoring_demo(18,E,A) ),
      X ).


/******************************************************************/


