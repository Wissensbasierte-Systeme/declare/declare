

/******************************************************************/
/***                                                            ***/
/***          Refactoring:  XPCE Dialog Parser                  ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      xpce_dialog_parse/3,
      xpce_dialog_parse/4,
      xpce_dialog_parse/5.


/*** interface ****************************************************/


/* xpce_dialog_parse_file_to_file(File_1,File_2) <-
      */

xpce_dialog_parse_file_to_file(File_1,File_2) :-
   see(File_1),
   tell(File_2),
   get0(Char),
   xpce_dialog_spy(
      xpce_dialog_parse(start,Char,[])),
   xpce_dialog_parse(start,Char,[]),
   told,
   seen.


/*** implementation ***********************************************/


/* xpce_dialog_parse(Mode,Char,Tokens) <-
      */

xpce_dialog_parse(start,Char,_) :-
   eof(Char),
   !.
xpce_dialog_parse(start,Char,Tokens) :-
   ( whitespace(Char)
   ; eol(Char) ),
   !,
   get0(Char_2),
   xpce_dialog_parse(start,Char_2,Tokens).
xpce_dialog_parse(start,Char,Tokens) :-
   character_is_in(Char,"["),
   get0(Char_2),
   xpce_dialog_spy(
      xpce_dialog_parse(part_name_start,Char_2,Tokens)),
   xpce_dialog_parse(part_name_start,Char_2,Tokens).
xpce_dialog_parse(start,_,Tokens) :-
   get0(Char_2),
   xpce_dialog_parse(start,Char_2,Tokens).


/*** parts ********************************************************/


xpce_dialog_parse(part_name_start,Char,Tokens) :-
   ( character_is_in(Char," ,")
   ; eol(Char) ),
   get0(Char_2),
   xpce_dialog_parse(part_name_start,Char_2,Tokens).
xpce_dialog_parse(part_name_start,Char,Tokens) :-
   ( letter(Char)
   ; character_is_in(Char,"_")
   ; digit(Char) ),
   get0(Char_2),
   xpce_dialog_spy(
      xpce_dialog_parse(part_name,Char_2,[Char],[],Tokens)),
   xpce_dialog_parse(part_name,Char_2,[Char],[],Tokens).
xpce_dialog_parse(part_name_start,Char,Tokens) :-
   character_is_in(Char,"]"),
   get0(Char_2),
   xpce_dialog_spy(
      xpce_dialog_parse(modifications,Char_2,Tokens)),
   xpce_dialog_parse(modifications,Char_2,Tokens).

xpce_dialog_parse(part_name,Char,Part,Item,Tokens) :- 
   atom_character(Char),
   append(Part,[Char],Part_2),
   get0(Char_2),
   xpce_dialog_parse(part_name,Char_2,Part_2,Item,Tokens).
xpce_dialog_parse(part_name,Char,Part,Item,Tokens) :-
   character_is_in(Char,":"),
   get0(Char_1),
   character_is_in(Char_1,"="),
   append(["'",Part,"'"],Part_2),
   name(Part_Name,Part_2),
   append(Item,[Part_Name],Item_2),
   get0(Char_2),
   xpce_dialog_spy(
      xpce_dialog_parse(part_type_start,Char_2,Item_2,Tokens)),
   xpce_dialog_parse(part_type_start,Char_2,Item_2,Tokens).
xpce_dialog_parse(part_name,Char,Part,Item,Tokens) :-
   whitespace(Char),
   append(["'",Part,"'"],Part_2),
   name(Part_Name,Part_2),
   append(Item,[Part_Name],Item_2),
   get0(Char_2),
   xpce_dialog_spy(
      xpce_dialog_parse(part_type_start,Char_2,Item_2,Tokens)),
   xpce_dialog_parse(part_type_start,Char_2,Item_2,Tokens).

xpce_dialog_parse(part_type_start,Char,Item,Tokens) :-
   ( whitespace(Char)
   ; eol(Char) ),
   get0(Char_2),
   xpce_dialog_parse(part_type_start,Char_2,Item,Tokens).
xpce_dialog_parse(part_type_start,Char,Item,Tokens) :-
   character_is_in(Char,":"),
   get0(Char_1),
   character_is_in(Char_1,"="),
   get0(Char_2),
   xpce_dialog_parse(part_type_start,Char_2,Item,Tokens). 
xpce_dialog_parse(part_type_start,Char,Item,Tokens) :-
   letter(Char),
   get0(Char_2),
   xpce_dialog_spy(
      xpce_dialog_parse(part_type,Char_2,Item,Tokens)),
   xpce_dialog_parse(part_type,Char_2,Item,Tokens).

xpce_dialog_parse(part_type,Char,Item,Tokens) :-
   atom_character(Char),
   get0(Char_2),
   xpce_dialog_parse(part_type,Char_2,Item,Tokens).
xpce_dialog_parse(part_type,Char,Item,Tokens) :-
   character_is_in(Char,"("),
   get0(Char_2),
   xpce_dialog_spy(
      xpce_dialog_parse(part_type_info,Char_2,Item,Tokens)),
   xpce_dialog_parse(part_type_info,Char_2,Item,Tokens).

xpce_dialog_parse(part_type_info,Char,Item,Tokens) :-
   character_is_in(Char,")"),
   append(Tokens,[Item],Tokens_2),
   get0(Char_2),
   xpce_dialog_spy(
      xpce_dialog_parse(part_name_start,Char_2,Tokens_2)),
   xpce_dialog_parse(part_name_start,Char_2,Tokens_2).
xpce_dialog_parse(part_type_info,Char,Item,Tokens) :-
   character_is_in(Char,"("),
   get0(Char_2),
   xpce_dialog_spy(
      xpce_dialog_parse(part_type_info2,Char_2,Item,Tokens)),
   xpce_dialog_parse(part_type_info2,Char_2,Item,Tokens).
xpce_dialog_parse(part_type_info,_,Item,Tokens) :-
   get0(Char_2),
   xpce_dialog_parse(part_type_info,Char_2,Item,Tokens).

xpce_dialog_parse(part_type_info2,Char,Item,Tokens) :-
   character_is_in(Char,")"),
   get0(Char_2),
   xpce_dialog_parse(part_type_info,Char_2,Item,Tokens).
xpce_dialog_parse(part_type_info2,_,Item,Tokens) :-
   get0(Char_2),
   xpce_dialog_parse(part_type_info2,Char_2,Item,Tokens).


/*** modifications ************************************************/


xpce_dialog_parse(modifications,Char,Tokens) :-
   character_is_in(Char,"["),
   get0(Char_2),
   xpce_dialog_parse(mod_name_start,Char_2,Tokens).
xpce_dialog_parse(modifications,_,Tokens) :-
   get0(Char_2),
   xpce_dialog_parse(modifications,Char_2,Tokens).

xpce_dialog_parse(mod_name_start,Char,Tokens) :-
   ( character_is_in(Char," ,")
   ; eol(Char) ),
   get0(Char_2),
   xpce_dialog_parse(mod_name_start,Char_2,Tokens).
xpce_dialog_parse(mod_name_start,Char,Tokens) :-
   ( letter(Char)
   ; character_is_in(Char,"_") ),
   get0(Char_2),
   xpce_dialog_parse(mod_name,Char_2,[Char],[],Tokens).
xpce_dialog_parse(mod_name_start,Char,Tokens) :-
   character_is_in(Char,"]"),
   Tokens = [Parts|Modifications],
   Tokens_2 = [Parts,Modifications],
   get0(Char_2),
   xpce_dialog_spy(
      xpce_dialog_parse(layout,Char_2,Tokens_2)),
   xpce_dialog_parse(layout,Char_2,Tokens_2).

xpce_dialog_parse(mod_name,Char,Name,Mod,Tokens) :-
   atom_character(Char),
   append(Name,[Char],Name_2),
   get0(Char_2),
   xpce_dialog_parse(mod_name,Char_2,Name_2,Mod,Tokens).
xpce_dialog_parse(mod_name,Char,Name,Mod,Tokens) :-
   character_is_in(Char,":"),
   get0(Char_1),
   character_is_in(Char_1,"="),
   append(["'",Name,"'"],Name_2),
   name(Mod_Name,Name_2),
   append(Mod,[Mod_Name],Mod_2),
   get0(Char_2),
   xpce_dialog_parse(mod_type_start,Char_2,Mod_2,Tokens).
xpce_dialog_parse(mod_name,Char,Name,Mod,Tokens) :-
   whitespace(Char),
   append(["'",Name,"'"],Name_2),
   name(Mod_Name,Name_2),
   append(Mod,[Mod_Name],Mod_2),
   get0(Char_2),
   xpce_dialog_parse(mod_type_start,Char_2,Mod_2,Tokens).

xpce_dialog_parse(mod_type_start,Char,Mod,Tokens) :-
   ( whitespace(Char)
   ; eol(Char) ),
   get0(Char_2),
   xpce_dialog_parse(mod_type_start,Char_2,Mod,Tokens).
xpce_dialog_parse(mod_type_start,Char,Mod,Tokens) :-
   character_is_in(Char,":"),
   get0(Char_1),
   character_is_in(Char_1,"="),
   get0(Char_2),
   xpce_dialog_parse(mod_type_start,Char_2,Mod,Tokens).
xpce_dialog_parse(mod_type_start,Char,Mod,Tokens) :-
   character_is_in(Char,"["),
   get0(Char_2),
   xpce_dialog_parse(mod_type,Char_2,Mod,Tokens).

xpce_dialog_parse(mod_type,Char,Mod,Tokens) :-
   character_is_in(Char,"]"),
   append(Tokens,Mod,Tokens_2),
   get0(Char_2),
   xpce_dialog_parse(mod_name_start,Char_2,Tokens_2).
xpce_dialog_parse(mod_type,Char,Mod,Tokens) :-
   character_is_in(Char,"["),
   get0(Char_2),
   xpce_dialog_parse(mod_type_2,Char_2,Mod,Tokens).
xpce_dialog_parse(mod_type,_,Mod,Tokens) :-
   get0(Char_2),
   xpce_dialog_parse(mod_type,Char_2,Mod,Tokens).

xpce_dialog_parse(mod_type_2,Char,Mod,Tokens) :-
   character_is_in(Char,"]"),
   get0(Char_2),
   xpce_dialog_parse(mod_type,Char_2,Mod,Tokens).
xpce_dialog_parse(mod_type_2,_,Mod,Tokens) :-
   get0(Char_2),
   xpce_dialog_parse(mod_type_2,Char_2,Mod,Tokens).


/*** layout *******************************************************/


xpce_dialog_parse(layout,Char,Tokens) :-
   character_is_in(Char,"["),
   get0(Char_2),
   xpce_dialog_parse(layout_atom_start,Char_2,Tokens).
xpce_dialog_parse(layout,_,Tokens) :-
   get0(Char_2),
   xpce_dialog_parse(layout,Char_2,Tokens).

xpce_dialog_parse(layout_atom_start,Char,Tokens) :-
   ( character_is_in(Char," ,")
   ; eol(Char) ),
   get0(Char_2),
   xpce_dialog_parse(layout_atom_start,Char_2,Tokens).
xpce_dialog_parse(layout_atom_start,Char,Tokens) :-
   letter(Char),
   get0(Char_2),
   xpce_dialog_parse(layout_atom,Char_2,[Char],Tokens).
xpce_dialog_parse(layout_atom_start,Char,Tokens) :-
   character_is_in(Char,"]"),
   Tokens = [Parts,Modifications|Layout],
   write(dialog(Parts,Modifications,Layout)),
   writeln('.').

xpce_dialog_parse(layout_atom,Char,List,Tokens) :-
   lowercase(Char),
   append(List,[Char],List_2),
   get0(Char_2),
   xpce_dialog_parse(layout_atom,Char_2,List_2,Tokens).
xpce_dialog_parse(layout_atom,Char,"area",Tokens) :-
   character_is_in(Char,"("),
   get0(Char_2),
   xpce_dialog_parse(layout_area,Char_2,[],"area(",Tokens).
xpce_dialog_parse(layout_atom,Char,List,Tokens) :-
   character_is_in(Char,"("),
   append(List,"(",List_2),
   get0(Char_2),
   xpce_dialog_parse(layout_atom_arg,Char_2,[],List_2,Tokens).

xpce_dialog_parse(layout_atom_arg,Char,Arg,List,Tokens) :-
   atom_character(Char),
   append(Arg,[Char],Arg_2),
   get0(Char_2),
   xpce_dialog_parse(layout_atom_arg,Char_2,Arg_2,List,Tokens).
xpce_dialog_parse(layout_atom_arg,Char,Arg,List,Tokens) :-
   character_is_in(Char,","),
   whitespace(Y),
   delete(Arg,Y,Arg_2),
   append([List,"'",Arg_2,"',"],List_2),
   get0(Char_2),
   xpce_dialog_parse(layout_atom_arg,Char_2,[],List_2,Tokens).
xpce_dialog_parse(layout_atom_arg,Char,Arg,List,Tokens) :-
   whitespace(Char),
   append(Arg,[Char],Arg_2),
   get0(Char_2),
   xpce_dialog_parse(layout_atom_arg,Char_2,Arg_2,List,Tokens).
xpce_dialog_parse(layout_atom_arg,Char,Arg,List,Tokens) :-
   character_is_in(Char,")"),
   whitespace(Y),
   delete(Arg,Y,Arg_2),
   append([List,"'",Arg_2,"')"],List_2),
   name(Layout_Atom,List_2),
   append(Tokens,[Layout_Atom],Tokens_2),
   get0(Char_2),
   xpce_dialog_parse(layout_atom_start,Char_2,Tokens_2).

xpce_dialog_parse(layout_area,Char,Arg,List,Tokens) :-
   atom_character(Char),
   append(Arg,[Char],Arg_2),
   get0(Char_2),
   xpce_dialog_parse(layout_area,Char_2,Arg_2,List,Tokens).
xpce_dialog_parse(layout_area,Char,Arg,List,Tokens) :-
   character_is_in(Char,","),
   whitespace(Y),
   delete(Arg,Y,Arg_2),
   append([List,"'",Arg_2,"',"],List_2),
   get0(Char_2),
   xpce_dialog_parse(layout_area_atom,Char_2,[],List_2,Tokens).
xpce_dialog_parse(layout_area,Char,Arg,List,Tokens) :-
   whitespace(Char),
   append(Arg,[Char],Arg_2),
   get0(Char_2),
   xpce_dialog_parse(layout_area,Char_2,Arg_2,List,Tokens).

xpce_dialog_parse(layout_area_atom,Char,Arg,List,Tokens) :-
   lowercase(Char),
   append(Arg,[Char],Arg_2),
   get0(Char_2),
   xpce_dialog_parse(layout_area_atom,Char_2,Arg_2,List,Tokens).
xpce_dialog_parse(layout_area_atom,Char,Arg,List,Tokens) :-
   ( whitespace(Char)
   ; eol(Char) ),
   get0(Char_2),
   xpce_dialog_parse(layout_area_atom,Char_2,Arg,List,Tokens).
xpce_dialog_parse(layout_area_atom,Char,Arg,List,Tokens) :-
   character_is_in(Char,"("),
   append([List,Arg,"("],List_2),
   get0(Char_2),
   xpce_dialog_parse(layout_area_args,Char_2,[],List_2,Tokens).

xpce_dialog_parse(layout_area_args,Char,Arg,List,Tokens) :-
   character_is_in(Char,")"),
   append(Arg,")",Arg_2),
   get0(Char_2),
   xpce_dialog_parse(layout_area_end,Char_2,Arg_2,List,Tokens).
xpce_dialog_parse(layout_area_args,Char,Arg,List,Tokens) :-
   append(Arg,[Char],Arg_2),
   get0(Char_2),
   xpce_dialog_parse(layout_area_args,Char_2,Arg_2,List,Tokens).

xpce_dialog_parse(layout_area_end,Char,Arg,List,Tokens) :-
   ( whitespace(Char)
   ; eol(Char) ),
   get0(Char_2),
   xpce_dialog_parse(layout_area_end,Char_2,Arg,List,Tokens).
xpce_dialog_parse(layout_area_end,Char,Arg,List,Tokens) :-
   character_is_in(Char,")"),
   append([List,Arg,")"],List_2),
   name(Layout,List_2),
   append(Tokens,[Layout],Tokens_2),
   get0(Char_2),
   xpce_dialog_parse(layout_atom_start,Char_2,Tokens_2).


/*** errors *******************************************************/


xpce_dialog_parse(State,_,Tokens) :-
   xpce_dialog_parse_error_message(State,Tokens).

xpce_dialog_parse(State,_,_,Tokens) :-
   xpce_dialog_parse_error_message(State,Tokens).

xpce_dialog_parse_error_message(State,Tokens) :-
   write(State),
   writeln('Error !'),
   writeln(Tokens).


/*** debugging ****************************************************/


/* xpce_dialog_spy(Goal) <-
      debugging predicate */

xpce_dialog_spy(_) :-
   !.
xpce_dialog_spy(X) :-
   writeln(user,X).


/******************************************************************/


