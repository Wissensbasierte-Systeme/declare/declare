

/******************************************************************/
/***                                                            ***/
/***          Refactoring:  Pretty Print                        ***/
/***                                                            ***/
/******************************************************************/


write_refactored_dialogs_to_output_file(File) :-
   findall(E,
      qm_entity_type(E),
      Dialogs),
   maplist( qm_dialog_name_to_refactored_dialog,
      Dialogs, Dialogs2 ),
   tell(File),
   checklist( pretty_println_dialog,
      Dialogs2 ),
   told.


write_qualimed_dialogs_to_output_file(File) :-
   findall(X,
      qm_entity_type(X),
      Dialogs),
   tell(File),
   checklist( pretty_println_qualimed_dialog,
      Dialogs ),
   told.

pretty_print_qualimed_dialog(Dialog_Name) :-
%  get_parsed_qm_dialog_description(Dialog_Name,Dialog_Description),
   get_parsed_dialog(Dialog_Name,
      dialog(Dialog_Name,Dialog_Description)),
   write('dialog('),
   write(Dialog_Name),
   write(','),
   nl,
   print_description_list(3,Dialog_Description),
   write(').').


refactoring_print_dialog(Dialog) :-
   Dialog =.. [dialog,Dialog_Name,Dialog_Description],
   write('dialog('),
   write(Dialog_Name),
   write(','),
   nl,
   print_description_list(3,Dialog_Description),
   write(').').


/******************************************************************/


pretty_println_qualimed_dialog(Dialog_Name) :-
   pretty_print_qualimed_dialog(Dialog_Name),
   nl.
pretty_println_qualimed_dialog(Dialog_Name) :-
   nl,
   write('Fehler: '),
   write_ln(Dialog_Name).


pretty_println_dialog(File,Dialog) :-
   tell(File),
   pretty_println_dialog(Dialog),
   told.

pretty_println_dialog(Dialog) :-
   pretty_print_dialog(Dialog),
   nl.
   
pretty_print_dialog(Dialog) :-
   write(Dialog).


/*
print_description_list(Indent,List) :-
   print_description_list(indent,Indent,List).
*/

print_description_list(Indent,List) :-
%  qm_indent(Ind,Indent),
   empty_space(Indent),
   write('[ '),
   Indent2 is Indent + 2,
   qm_pp_list_elems(Indent2,List),
   nl,
   empty_space(Indent),
   write(']').

qm_pp_list_elems(Indent,[Elem]) :-
   qm_pp_list_elem(noindent,Indent,Elem),
   !.
qm_pp_list_elems(Indent,[Elem|Elems]) :-
   qm_pp_list_elem(noindent,Indent,Elem),
   write(','),
   qm_pp_list_elems2(Indent,Elems).
%qm_pp_list_elems(_,[]).
   
qm_pp_list_elems2(Indent,[Elem]) :-
   nl,
   qm_pp_list_elem(indent,Indent,Elem),
   !.
qm_pp_list_elems2(Indent,[Elem|Elems]) :-
   nl,
   qm_pp_list_elem(indent,Indent,Elem),
   write(','),
   qm_pp_list_elems2(Indent,Elems).
%qm_pp_list_elems2(_,[]).


qm_pp_list_elem(Ind,Indent,Elem) :-
   atomic(Elem),
   !,
   qm_indent(Ind,Indent),
   print(Elem).
qm_pp_list_elem(_,Indent,Elem) :-
   is_list(Elem),
   !,
   print_description_list(Indent,Elem).
qm_pp_list_elem(Ind,Indent,A:=B) :-
   atomic(B),
   !,
   qm_indent(Ind,Indent),
   write(A),
   write(' := '),
   print(B).
qm_pp_list_elem(Ind,Indent,A:=B) :-
   is_list(B),
   !,
%  write(Ind),
   qm_indent(Ind,Indent),
   write(A),
   write_ln(' := '),
   Indent2 is Indent + 3,
   print_description_list(Indent2,B).
qm_pp_list_elem(Ind,Indent,A:=B) :-
   compound(B),
   !,
   qm_indent(Ind,Indent),
   write(A),
   write(' := '),
   write_compound(Indent,B).
%  print(B).
qm_pp_list_elem(Ind,Indent,Elem) :-
   compound(Elem),
   !,
   qm_indent(Ind,Indent),
   write_compound(Indent,Elem).
%  print(Elem).

qm_indent(noindent,_).
qm_indent(indent,Indent) :- 
   empty_space(Indent).

empty_space(I) :-
   I > 0,
   write(' '),
   J is I - 1,
   empty_space(J).
empty_space(_) :- !.


write_compound(Indent,Term) :-
   Term =.. [Functor|Args],
   length(Args,L),
   L > 4,
%  empty_space(Indent),
   atom_length(Functor,Ind),
   Indent2 is Indent + Ind + 1,
   write(Functor),
   write('('),
   print_term_args(Indent2,Args).
write_compound(_,Term) :-
%  Term =.. [Functor|Args],
%  write(Functor),
   write('('),
%  empty_space(Indent),
   print(Term).
%  write_term_args(Args).

print_term_args(Indent,[Arg|Args]) :-
   is_list(Arg),
   write(Arg),
   write(','),
   print_term_args2(Indent,Args).
print_term_args(Indent,[Arg|Args]) :-
   print(Arg),
   write(','),
   print_term_args2(Indent,Args).

print_term_args2(Indent,[Arg]) :-
   nl,
   empty_space(Indent),
   print(Arg),
   write(')').
print_term_args2(Indent,[Arg|Args]) :-
   nl,
   empty_space(Indent),
   print(Arg),
   write(','),
   print_term_args2(Indent,Args).
% print_term_args2(_,[]) :-
%   write(')').


write_term_args([Arg]) :-
   write_arg(Arg),
   write(')').
write_term_args([Arg|Args]) :-
   write_arg(Arg),
   write(','),
   write_term_args(Args).
write_term_args([Arg|Args]) :-
   write_arg(Arg),
   write(','),
   write_term_args(Args).

   
write_arg(Arg) :-   
   is_list(Arg),
   write(Arg).
write_arg(Arg) :-   
   print(Arg).


/*****************************************************************/


