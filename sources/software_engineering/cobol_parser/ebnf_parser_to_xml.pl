

/******************************************************************/
/***                                                            ***/
/***           EBNF:  Parser                                    ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(ebnf_rules, 1) :-
   dislog_variable_get(home, '/projects/Cobol/', Cobol),
   concat(Cobol, 'cobol_parser.ebnf', File_1),
   read_file_to_name(File_1, Name_1),
   Substitution = [["\n\n", "."]],
   name_exchange_sublist(Substitution, Name_1, Name_2),
   atom_chars(Name_2, Cs),
   ebnf_rules([Xml], Cs, []),
   concat(Cobol, 'cobol_parser.xml', File_2),
   dwrite(xml, File_2, Xml).


/*** interface ****************************************************/


ebnf_rules ==>
   sequence(+, ebnf_rule).

ebnf_rule([ebnf_rule:[token:T]:F]) -->
   spaces(_), ebnf_token([T]), spaces(_),
   ['='], { writeln(user, T) }, spaces(_),
   ebnf_formula(F), spaces(_), ['.'], spaces(_),
%  { dwrite(xml, ebnf_rule:[token:T]:F) },
   !.


/*** implementation ***********************************************/


ebnf_formula ==>
   ( ebnf_formula_
   ; ebnf_conjunction ).

ebnf_formula_(Xs) -->
   ( ebnf_token([T]), { Xs = [ebnf_token:[value:T]:[]] }
   ; ebnf_terminal(Xs)
   ; ebnf_optional(Xs)
   ; ebnf_brackets(Xs)
   ; ebnf_sequence(Xs) ),
   !.


ebnf_terminal([ebnf_terminal:[value:T]:[]]) -->
   ['"'],
   ebnf_token(["AZ", "az", "-.", "19", "<>", "(/", "::"], Cs),
   ['"'],
   { concat(Cs, T) }.


ebnf_optional([ebnf_optional:Xs]) -->
   ['['], spaces(_), ebnf_formula(Xs), spaces(_), [']'].


ebnf_brackets(Xs) -->
   ['('], spaces(_),
   ( ebnf_disjunction(Xs)
   ; ebnf_permutation(Xs) ),
   spaces(_), [')'].

% ebnf_disjunction ==>
%    spaces, ebnf_formula, spaces,
%    ['|', '|'], spaces, ebnf_formula, spaces.

ebnf_disjunction ==>
   spaces, ebnf_formula, spaces,
   ebnf_or, spaces,
   ( ebnf_disjunction
   ; ebnf_formula ), spaces.

ebnf_permutation ==>
   spaces, ebnf_formula, spaces,
   ebnf_perm, spaces,
   ( ebnf_permutation
   ; ebnf_formula ), spaces.

ebnf_or([]) -->
   ['|'].

ebnf_perm([]) -->
   ['|', '|'].


ebnf_sequence([ebnf_sequence:[type:T]:F]) -->
   ['{'], spaces(_), ebnf_formula(F), spaces(_), ['}', T],
   { member(T, ['*', '+', '?']) },
   !.


ebnf_conjunction ==>
   ebnf_formula_, spaces,
   ebnf_conjunction.
ebnf_conjunction(Xs) -->
   ebnf_formula_(Xs), spaces(_).


ebnf_token([T]) -->
   ebnf_token(["AZ", "az", "--", "19"], Cs),
   { concat(Cs, T) }.

ebnf_token(Ranges, [X|Xs]) -->
   ebnf_character(Ranges, X),
   !,
   ebnf_token_(Ranges, Xs).

ebnf_token_(Ranges, [X|Xs]) -->
   ebnf_character(Ranges, X),
   !,
   ebnf_token_(Ranges, Xs).
ebnf_token_(_, []) -->
   [].

% ebnf_token(Xs) -->
%    sequence(+, ebnf_character, Xs).


ebnf_character(Ranges, X) -->
   [X],
   { atom_codes(X, [C]),
     member([I,J], Ranges),
     between(I, J, C) }.


spaces([]) -->
   ( [' '], spaces(_)
   ; ['\n'], spaces(_)
   ; [] ).

 
/******************************************************************/


