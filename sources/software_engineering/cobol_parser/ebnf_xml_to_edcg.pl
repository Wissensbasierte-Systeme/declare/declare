

/******************************************************************/
/***                                                            ***/
/***           EBNF:  Transformations                           ***/
/***                                                            ***/
/******************************************************************/


:- op(1100, xfy, '||').


/*** tests ********************************************************/


test(ebnf_parser_xml_to_edcg, 1) :-
   dislog_variable_get(home, '/projects/Cobol/', Cobol),
   concat(Cobol, 'cobol_parser.xml', File_1),
   concat(Cobol, 'cobol_parser.edcg', File_2),
   ebnf_parser_xml_to_edcg(File_1, File_2).

test(ebnf_tokenizer_xml_to_edcg, 1) :-
   dislog_variable_get(home, '/projects/Cobol/', Cobol),
   concat(Cobol, 'cobol_tokenizer.xml', File_1),
   concat(Cobol, 'cobol_tokenizer.edcg', File_2),
   ebnf_tokenizer_xml_to_edcg(File_1, File_2).

test(ebnf_tokenizer_xml_to_bridges, 1) :-
   dislog_variable_get(home, '/projects/Cobol/', Cobol),
   concat(Cobol, 'cobol_tokenizer.xml', File_1),
   concat(Cobol, 'cobol_parser_tokenizer_bridges.pl', File_2),
   ebnf_tokenizer_xml_to_bridges(File_1, File_2).

test(create_cobol_parser, 1) :-
   dislog_variable_get(home, '/projects/Cobol/', Cobol),
   concat(Cobol, 'cobol_parser.edcg', File_1),
   concat(Cobol, 'cobol_tokenizer.edcg', File_2),
   concat(Cobol, 'cobol_parser_tokenizer_bridges.pl', File_3),
   concat(Cobol, 'cobol_tokenizer_default_word.edcg', File_4),
   concat(Cobol, 'parser.pl', File_5),
   concat_atom([cat, File_1, File_2, File_3, File_4,
      '>', File_5], ' ', Command),
   writeln(user, Command),
   us(Command).


test(ebnf_simplify, 1) :-
   dislog_variable_get(home, '/projects/Cobol/', Cobol),
   concat(Cobol, 'cobol_parser.xml', File_1),
   concat(Cobol, 'cobol_parser.short.xml', File_2),
   ebnf_parser_xml_simplify(File_1, File_2).


test(ebnf_xml_analyze, 1) :-
   dislog_variable_get(home, '/projects/Cobol/', Cobol),
   concat(Cobol, 'cobol_parser.xml', File_1),
   dread(xml, File_1, [Xml_1]),
   concat(Cobol, 'cobol_tokenizer.xml', File_2),
   dread(xml, File_2, [Xml_2]),
   let( Xs_A := Xml_1/descendant::ebnf_rule@token ),
   let( Xs_B := Xml_1/descendant::ebnf_token@value ),
   let( Ys_A := Xml_2/descendant::ebnf_rule@token ),
   let( Ys_B := Xml_2/descendant::ebnf_token@value ),
   Zs <= ord_subtract(
      ord_union(sort(Xs_B), sort(Ys_B)),
      ord_union(sort(Xs_A), sort(Ys_A)) ),
   writeq(user, Zs).


/*** interface ****************************************************/


/* ebnf_parser_xml_to_edcg(File_1, File_2) <-
      */

ebnf_parser_xml_to_edcg(File_1, File_2) :-
   ebnf_input_to_output(File_1, File_2),
   dislog_variable_get(source_path,
      'development/cobol_parser/ebnf_parser_xml_to_edcg.fng',
      Fng),
   dread(xml, File_1, [Item_1]),
   fn_item_transform_fng(Fng, Item_1, Item_2),
   !,
   Rules := Item_2/content::'*',
   pretty_print_edcgs(File_2, Rules).

%  fn_transform_xml_file_fng(Fng, File_1, File_2).
%  fn_transform_fn_item_fng(Fng, Item_1, Item_2),
%  fn_transform([item, fng(Fng)], Item_1, Item_2),


/* ebnf_tokenizer_xml_to_edcg(File_1, File_2) <-
      */

ebnf_tokenizer_xml_to_edcg(File_1, File_2) :-
   ebnf_input_to_output(File_1, File_2),
   dislog_variable_get(source_path,
      'development/cobol_parser/ebnf_tokenizer_xml_to_edcg.fng',
      Fng),
   dread(xml, File_1, [Item_1]),
   fn_item_transform_fng(Fng, Item_1, Item_2),
   !,
   Rules := Item_2/content::'*',
   pretty_print_edcgs(File_2, Rules).


/* ebnf_tokenizer_xml_to_bridges(File_1, File_2) <-
      */

ebnf_tokenizer_xml_to_bridges(File_1, File_2) :-
   ebnf_input_to_output(File_1, File_2),
   dislog_variable_get(source_path,
      'development/cobol_parser/ebnf_tokenizer_xml_to_bridges.fng',
      Fng),
   dread(xml, File_1, [Item_1]),
   fn_item_transform_fng(Fng, Item_1, Item_2),
   Rules := Item_2/content::'*',
   pretty_print_bridges(File_2, Rules).


/* ebnf_parser_xml_simplify(File_1, File_2) <-
      */

ebnf_parser_xml_simplify(File_1, File_2) :-
   ebnf_input_to_output(File_1, File_2),
   dislog_variable_get(source_path,
      'development/cobol_parser/ebnf_parser_xml_simplify_1.fng',
      Fng_1),
   dislog_variable_get(source_path,
      'development/cobol_parser/ebnf_parser_xml_simplify_2.fng',
      Fng_2),
   fn_transform_xml_file_fng(Fng_1, File_1, File_2),
   fn_transform_xml_file_fng(Fng_2, File_2, File_2).


/*** implementation ***********************************************/


pretty_print_edcgs(File, Rules) :-
   program_to_xml(Rules, Xml),
   Xml_2 := (rules:Xml) * [rule@operator:'==>'],
   Rules_2 := Xml_2/content::'*',
   predicate_to_file( File,
      xml_to_prolog:xml_rules_to_prolog(Rules_2) ).

pretty_print_bridges(File, Rules) :-
   program_to_xml(Rules, Xml),
   predicate_to_file( File,
      xml_to_prolog:xml_rules_to_prolog(Xml) ).


ebnf_input_to_output(File_1, File_2) :-
   write_list(user, [
      '<--- ', File_1, '\n', '---> ', File_2, '\n']).


/******************************************************************/


