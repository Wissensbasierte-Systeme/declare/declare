

/******************************************************************/
/***                                                            ***/
/***           EBNF:  Tokenizer                                 ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(ebnf_lexical_rules, 1) :-
   dislog_variable_get(home, '/projects/Cobol/', Cobol),
   concat(Cobol, 'cobol_tokenizer.ebnf', File_1),
%  concat(Cobol, 'test.ebnf', File_1),
   read_file_to_name(File_1, Name_1),
   concat(Name_1, '/*  */', Name_2),
   atom_chars(Name_2, Cs),
   ebnf_lexical_rules([Xml], Cs, []),
   concat(Cobol, 'cobol_tokenizer.xml', File_2),
   dwrite(xml, File_2, Xml).


/*** interface ****************************************************/


ebnf_lexical_rules ==>
   spaces,
   ebnf_lexical_comment,
   sequence(+, ebnf_lexical_rule).

ebnf_lexical_rule([ebnf_rule:[token:T]:F]) -->
   spaces(_), ebnf_token([T]), spaces(_),
   ['='], { writeln(user, T) }, spaces(_),
   ebnf_lexical_formula(F),
   spaces(_),
   ( ebnf_lexical_comment(_)
   ; ['\n', '\n'] ),
   spaces(_),
   !.


/*** implementation ***********************************************/


ebnf_lexical_formula(Xs) -->
   ( ebnf_lexical_undefined(Xs)
   ; ebnf_lexical_formula_(Xs)
   ; ebnf_lexical_conjunction(Xs) ).

ebnf_lexical_formula_(Xs) -->
   ( ebnf_token([T]), { Xs = [ebnf_token:[value:T]:[]] }
   ; ebnf_terminal(Xs)
   ; ebnf_lexical_sequence(Xs)
   ; ebnf_lexical_list(Xs)
   ; ebnf_lexical_brackets(Xs) ).


ebnf_lexical_brackets(Xs) -->
   ['('], spaces(_),
   ( ebnf_lexical_disjunction(Xs)
   ; ebnf_lexical_conjunction(Xs) ),
   spaces(_), [')'].


ebnf_lexical_conjunction ==>
   spaces, ebnf_lexical_formula_, spaces,
   ( ebnf_lexical_conjunction
   ; [] ).

ebnf_lexical_disjunction ==>
   spaces, ebnf_lexical_formula, spaces,
   ebnf_or, spaces,
   !,
   ( ebnf_lexical_disjunction
   ; ebnf_lexical_formula ), spaces.


ebnf_lexical_sequence([ebnf_sequence:[type:T]:F]) -->
   ( ebnf_lexical_brackets(F)
   ; ebnf_lexical_list(F)
   ; ebnf_token(F)
   ; ebnf_terminal(F) ),
   [T],
   { member(T, ['*', '+', '?']) }.


/* ebnf_lexical_list(Xs) -->
      */

ebnf_lexical_list([not:Xs]) -->
   ['~'], ebnf_lexical_list(Xs).
ebnf_lexical_list([ebnf_list:Xs]) -->
   ['['], spaces(_),
   sequence(+, ebnf_lexical_range, Xs),
   spaces(_), [']'].

ebnf_lexical_range([ebnf_range:[from:X, to:Y]:[]]) -->
   [X, '-', Y],
   { X \= '\\' },
   !.
ebnf_lexical_range([ebnf_range:[value:X]:[]]) -->
   ( ['\\', n], { X = '\\n' }
   ; ['\\', X]
   ; [X], { X \= ']' } ).


ebnf_lexical_comment([], Xs, Ys) :-
   append(['/', '*'|_], ['*', '/'|Ys], Xs).

ebnf_lexical_undefined([]) -->
   spaces(_), ['.', '.', '.'], !.


ebnf_in_range(X, Ys) :-
   atom_codes(X, [K]),
   member(Y, Ys),
   ( Y = A-B ->
     atom_codes(A, [I]),
     atom_codes(B, [J]),
     between(I, J, K)
   ; Y = X ).

ebnf_range_pairs_to_ebnf_list(Pairs, T-{C}) :-
   pair_lists('-', Ts, Cs, Pairs),
   maplist( unify(T),
      Ts ),
   ( foreach({B}, Cs), foreach(B, Bs) do
        true ),
   list_to_semicolon_structure(Bs, C).

unify(X, X).


/******************************************************************/


