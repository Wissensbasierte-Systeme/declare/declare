

/******************************************************************/
/***                                                            ***/
/***           EBNF:  Cobol Parser                              ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_get(home, '/projects/Cobol/', Cobol),
   concat(Cobol, 'parser.pl', Parser),
   consult_if_exists(Parser).


/*** interface ****************************************************/


/* cobol_parser(Cobol, Syntax_Tree) <-
      */

cobol_parser(Cobol, Syntax_Tree) :-
   read_file_to_name(Cobol, Name),
   Substitution = [["\r", ""], ["\n", " "], ["  ", " "]],
   name_exchange_sublist(Substitution, Name, Name_2),
   name_split_at_position([" "], Name_2, Tokens_2),
   list_exchange_sublist([[[''], []]], Tokens_2, Tokens),
   'parser-cobol-source-program'(Syntax_Tree_2, Tokens, []),
%  writeq(user, Syntax_Tree_2), nl(user), wait,
   dislog_variable_get(source_path,
      'development/cobol_parser/cobol_syntax_tree_simplify.fng',
      Fng),
   fn_transform([item, fng(Fng)], Syntax_Tree_2, Syntax_Tree).


/*** tests ********************************************************/


test(cobol_parser, 1) :-
   dislog_variable_get(home, '/projects/Cobol/', Cobol),
   concat(Cobol, 'Programs/K.cob', File),
   cobol_parser(File, Syntax_Tree),
   dwrite(xml, Syntax_Tree).

test(cobol_parser, 2) :-
   Tokens = ['PROGRAM-ID', '.', 'KUNDEN-AUSGABE', '.',
      'AUTHOR', '.', 'CHRISTOPHER', '.'],
   'parser-program-id-cobol-source-program'(X, Tokens, Y),
   dwrite(xml, X),
   writeln(user, Y).

test(cobol_parser, 3) :-
   Tokens = ['AUTHOR', '.', 'CHRISTOPHER', '.'],
   'parser-identification-division-content'(X, Tokens, Y),
   dwrite(xml, X),
   writeln(user, Y).


/******************************************************************/


