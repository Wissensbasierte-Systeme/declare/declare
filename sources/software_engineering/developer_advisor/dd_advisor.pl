

/******************************************************************/
/***                                                            ***/
/***       Program : DisLog Developer Advisor                   ***/
/***    SourceFile : dd_advisor                                 ***/
/***       Version : 0.0.0                                      ***/
/***        Author : Thomas Schilling                           ***/
/***      Language : SWI-Prolog 2.9.5                           ***/
/***       Started : 7. August 1997                             ***/
/***  Last Changed : 4. April 1998                              ***/
/***       Purpose :                                            ***/
/***                                                            ***/
/******************************************************************/


dislog_variable(
   dd_advisor_path,
   'sources/software_engineering/developer_advisor/').
dislog_variable(
   dda_path,
   'sources/software_engineering/developer_advisor/').
dislog_variable(
   dislog_developer_advisor_path,
   'sources/software_engineering/developer_advisor/').


open_stream(File, Mode, Stream, Options) :-
   open(File, Mode, Stream_2, Options),
   set_stream(Stream_2, alias(Stream)).


use_module_dda(_) :-
   !.
use_module_dda(Files) :-
   maplist( dda_append_dislog_path(dd_advisor_path),
      Files, Files_2 ),
   use_module(Files_2).

dda_append_dislog_path(Variable, File, Path) :-
   dislog_variable_get(Variable, Path_1),
   name_append([Path_1, File], Path).

dda_absolute_file_name(Variable, File, Path) :-
   dislog_variable_get(Variable, Path_1),
   name_append([Path_1, File], Path_2),
   absolute_file_name(Path_2, Path).


dd_advisor(help) :-
   dhelp.
dd_advisor(help, predicate, Predicate) :-
   dislog_help_pred(Predicate).
dd_advisor(help, substring, String) :-
   dislog_help_substring(String).
dd_advisor(help, pattern, Pattern) :-
   dislog_help_pattern(Pattern).

dd_advisor(help, init, Value) :-
   init_help(Value).
%  Values can be "default", "check", "no_check".

dd_advisor(signature, insert, Signature) :-
   signature(Signature).
dd_advisor(signature, types, Signature_Types) :-
   signature_types(Signature_Types).
dd_advisor(signature, type_to_predicates, Data_Type) :-
   signature_for_type(Data_Type).
dd_advisor(signature, predicate_to_types, Predicate) :-
   signature_for_pred(Predicate).

dd_advisor(heuristic, Program, Operator) :-
   heuristic(Program, Operator).

dd_advisor(software_metrics, create_metrics) :-
   create_metrics_db.
dd_advisor(software_metrics, save_metrics) :-
   save_metrics_db.
dd_advisor(software_metrics, load_metrics) :-
   load_metrics_db.
dd_advisor(software_metrics, analyse_metrics) :-
   analyse_metrics_db.
dd_advisor(software_metrics, save_analysis) :-
   save_metrics_db_analysis.
dd_advisor(software_metrics, load_analysis) :-
   load_metrics_db_analysis.


/******************************************************************/


