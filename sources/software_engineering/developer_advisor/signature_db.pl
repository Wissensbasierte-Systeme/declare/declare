
/******************************************************************/
/***                                                            ***/
/***       Program : DisLog Developer Advisor                   ***/
/***    SourceFile : signature_db                               ***/
/***       Version : 0.0.0                                      ***/
/***        Author : Thomas Schilling                           ***/
/***      Language : SWI-Prolog 2.9.5                           ***/
/***       Started : 22. January 1998                           ***/
/***  Last Changed : 2. April 1998                              ***/
/***       Purpose : Database with the signature of some        ***/
/***                 important predicates.                      ***/
/***                                                            ***/
/******************************************************************/


:- module(dda_signature_db,[
	]).


:- use_module_dda([
	'signature']).


/*** Interface ****************************************************/


/*** Implementation ***********************************************/


/*** Initialization ***********************************************/


/* Description of data-types: */

/* program :
        Signature-DataType: disjunctive (negative) logic program. */

/* coin :
        Signature-DataType: collection of interpretations. */


/* Herbrand States and Coins */

:- signature(state_prune(state, state)).
:- signature(state_subsumes(state, state)).
:- signature(state_subtract(state, state, state)).
:- signature(state_can(state, state)).
:- signature(state_disjunction(state, state, state)).
:- signature(state_resolve(state, literal, state)).
:- signature(state_reduce(state, literal, state)).
:- signature(state_to_coin(state, coin)).
:- signature(coin_to_state(coin, state)).

/* The Clause Tree - A Data Structure */

:- signature(state_to_tree(state, tree)).
:- signature(tree_to_state(tree, state)).
:- signature(tree_subsumes_clause(tree, clause)).
:- signature(tree_subsumes_state(tree, state)).
:- signature(state_tree_subtract(state, tree, state)).
:- signature(tree_state_subtract(tree, state, tree)).
:- signature(tree_resolve(tree, literal, tree)).
:- signature(tree_reduce(tree, literal, tree)).

/* Disjunctive Consequence Operators */

:- signature(tps_operator(program, state, state)).
:- signature(tps_operator_dc(program, state, state)).
:- signature(tps_delta_operator(program, state, state, state)).
:- signature(tps_delta_operator_dc(program, state, state, state)).
:- signature(tree_tps_operator(program, tree, tree)).
:- signature(tree_tps_operator_dc(program, tree, tree)).
:- signature(tree_tps_delta_operator(program, tree, tree, tree)).
:- signature(tree_tps_delta_operator_dc(program, tree, tree, tree)).
:- signature(tps_fixpoint(program, state, state)).
:- signature(tps_fixpoint_cc(program, state, state)).
:- signature(tree_tps_fixpoint(program, tree, tree)).
:- signature(tps_fixpoint_iteration(program, state)).
:- signature(tree_tpi_operator(program, tree, tree)).
:- signature(tree_tpi_operator_ground(program, tree, tree)).
:- signature(tree_tpi_delta_operator(program, tree, delta, tree, delta)).
:- signature(tree_tpi_delta_operator(program, tree, tree, tree, tree)). % ADDED
:- signature(tree_tpi_fixpoint(program, tree, tree)).
:- signature(tpi_fixpoint_iteration(program, models)).

/* Program Transformations */

:- signature(partial_evaluation(program, program)).
:- signature(gl_transformation_non_ground(program, interpretation, program)).
:- signature(gl_transformation(program, interpretation, program)).
:- signature(evidential_transformation(program, program)).
:- signature(stratify_program(program, programs)).
:- signature(tu_transformation(program, program)).

/* Model-Based Semantics */

:- signature(minimal_models(program, minimal_models)).
:- signature(minimal_models(program, models)). % ADDED
:- signature(minimal_model_state(program, minimal_model_state)).
:- signature(minimal_model_state(program, state)). % ADDED
:- signature(minimal_model_semantics_s(program, minimal_state)).
:- signature(minimal_model_semantics_s(program, state)). % ADDED
:- signature(minimal_model_semantics_i(program, minimal_state)).
:- signature(minimal_model_semantics_i(program, state)). % ADDED
:- signature(perfect_models(program, perfect_models)).
:- signature(perfect_models(program, models)). % ADDED
:- signature(perfect_model_state(program, perfect_model_state)).
:- signature(perfect_model_state(program, state)). % ADDED
:- signature(perfect_model_semantics(program, perfect_state)).
:- signature(perfect_model_semantics(program, state)). % ADDED
:- signature(stable_models(program, stable_models)).
:- signature(stable_models(program, models)). % ADDED
:- signature(stable_models_normal(program, stable_models)).
:- signature(stable_models_normal(program, models)). % ADDED
:- signature(stable_models_normal_e(program, e, stable_models)).
:- signature(stable_models_normal_e(program, e, models)). % ADDED
:- signature(stable_model_state(program, stable_model_state)).
:- signature(stable_model_state(program, state)). % ADDED
:- signature(stable_model_semantics(program, stable_state)).
:- signature(stable_model_semantics(program, state)). % ADDED
:- signature(partial_stable_models(program, partial_stable_models)).
:- signature(partial_stable_models(program, models)). % ADDED
:- signature(evidential_stable_models(program, models)).
:- signature(partial_evidential_stable_models(program, models)).

/* Closed-World-Assumptions */

:- signature(state_to_egcwa(state, egcwa)).
:- signature(state_to_gcwa(state, gcwa)).
:- signature(state_to_sn_sets(state, supports)).
:- signature(snd_snd_product(state, supports, supports, supports, egcwa)).
:- signature(egcwa_operator(program, con_egcwa)).
:- signature(egcwa_operator(program, egcwa)). % ADDED
:- signature(gcwa_operator(program, gcwa)).

/* Well-Founded Semantics */

:- signature(sd_operator(program, state_pair, state_pair)).
:- signature(se_operator(program, state_pair, state_pair)).
:- signature(sed_operator(program, state_pair, state_pair)).
:- signature(sd_fixpoint(program, state_pair, state_pair)).
:- signature(se_fixpoint(program, state_pair, state_pair)).
:- signature(sed_fixpoint(program, state_pair, state_pair)).
:- signature(dwfs_operator(program, state_pair)).
:- signature(gdwfs_operator(program, state_pair)).
:- signature(sb_operator(program, state, state)).
:- signature(sb_fixpoint(program, state, state)).
:- signature(sb_fixpoint_tree(program, tree, tree)).
:- signature(bdwfs_operator(program, state)).
:- signature(tsb_fixpoint(program, state, state)).
:- signature(fsb_fixpoint(program, state, state)).

/* Pretty-Printers for DisLog-Objects */

:- signature(dportray(atom, state)).
:- signature(dportray(atom, program)).
:- signature(dportray(atom, state_pair)).
:- signature(dportray(atom, state_triple)).
:- signature(dportray(atom, tree)).
:- signature(dportray(atom, edges)).


/*** Testing / Debugging ******************************************/


/******************************************************************/


/* Emacs-Configuration: */

%%% Local Variables:
%%% mode: prolog
%%% End:
