
/******************************************************************/
/***                                                            ***/
/***       Program : DisLog Developer Advisor                   ***/
/***    SourceFile : heuristic                                  ***/
/***       Version : 0.0.0                                      ***/
/***        Author : Thomas Schilling                           ***/
/***      Language : SWI-Prolog 2.9.5                           ***/
/***       Started : 10. January 1998                           ***/
/***  Last Changed : 2. April 1998                              ***/
/***       Purpose : Try to give a usefull rating of logic      ***/
/***                 programs to decide which operator would    ***/
/***                 be more efficient to use.                  ***/
/***                                                            ***/
/******************************************************************/


:- module(dda_heuristic,[
	heuristic/2,
	heuristic/3,
        test_heuristics_with_prog/3]).

	
:- use_module_dda([
	'tools',
	'config']).


:- ddiscontiguous([
	heuristic/3]).
%:- discontiguous
%	heuristic/3.


/*** Interface ****************************************************/


/* heuristic(+Prog, -Result) <-
        Rate a disjunctive logic program Prog. Use the default
        heuristic. See also heuristic/3. Prog can be in the internal
        list-form or a filename. */


/* heuristic(+Nr, +Prog, -Result) <-
        Rate a disjunctive logic program Prog. Use Heuristic with
        number Nr. Result will be unified with 'tpi' or 'tps',
        indicating the suggested use of the Tpi or Tps respectively.
        If Result is unified with 'UNKNOWN', it is suggested to use
        the operator depending on the desired result-type. */


/* test_heuristics_with_prog(+ProgFile, +TimesToRun, +ResultFile) <-
        Test the heuristics with a given logic program. ProgFile is the name
        of the file of that logic program. The test-results are written
        to a log-file with the name ResultFile. TimesToRun specifies how
        often each operator is executed to measure it's time. */


/*** Implementation ***********************************************/


/* count_disjunctions(+Program, -NrFactsList, -NrRulesList) <-
        Count the disjunctions in facts or rule-heads. The results
        are unified with NrFactsList and NrRulesList respectivly.
        The lists contain the number of x-valued disjunctions in
        natural order (1, 2, 3, ...). So the first value is
        actually the number of facts/rules without disjunction.
        The lists at least contain [0, 0] so that later processing
        can be sure to unify with at least one number for
        disjunctions. */

count_disjunctions(NProg, Facts, Rules) :-
	dnlp_to_dlp(NProg, Prog),
	count_disjunctions_acc(Prog, [0, 0], [0, 0], Facts, Rules).

count_disjunctions_acc([Rule | Prog], FactsAcc, RulesAcc, Facts, Rules) :-
	!,
	add_rule(Rule, FactsAcc, RulesAcc, FactsAccNew, RulesAccNew),
	!,
	count_disjunctions_acc(Prog, FactsAccNew, RulesAccNew, Facts, Rules).
count_disjunctions_acc([], FactsAcc, RulesAcc, FactsAcc, RulesAcc).


add_rule(Head - Pos - [], FactsAcc, RulesAcc, FactsAccNew, RulesAccNew) :-
	add_rule(Head - Pos , FactsAcc, RulesAcc, FactsAccNew, RulesAccNew).
add_rule(Head - _Pos - _Neg, FactsAcc, RulesAcc, FactsAcc, RulesAccNew) :-
	writef('WARNING: count_disjunctions(+Program, -NrFacts, -NrRules):\n'),
	writef('         Negative rule-body found in program!\n'),
	length(Head, NrAtoms),
	inc_nth_value_in_list(NrAtoms, RulesAcc, RulesAccNew).
add_rule(Head - [], FactsAcc, RulesAcc, FactsAccNew, RulesAccNew) :-
	add_rule(Head, FactsAcc, RulesAcc, FactsAccNew, RulesAccNew).
add_rule(Head - _Pos, FactsAcc, RulesAcc, FactsAcc, RulesAccNew) :-
	length(Head, NrAtoms),
	inc_nth_value_in_list(NrAtoms, RulesAcc, RulesAccNew).
add_rule(Head, FactsAcc, RulesAcc, FactsAccNew, RulesAcc) :-
	length(Head, NrAtoms),
	inc_nth_value_in_list(NrAtoms, FactsAcc, FactsAccNew).


/* inc_nth_value_in_list(+N, +List, -ListNew) <-
        Increase N'th value in List by one. Result is ListNew.
        First list-element has number 1. */

inc_nth_value_in_list(N, List, List) :-
	N < 1.
inc_nth_value_in_list(1, [Nr | List], [NrNew | List]) :-
	NrNew is Nr + 1.
inc_nth_value_in_list(1, [], [1]).
inc_nth_value_in_list(N, [Nr | List], [Nr | ListNew]) :-
	N_New is N - 1,
	inc_nth_value_in_list(N_New, List, ListNew).
inc_nth_value_in_list(N, [], [0 | ListNew]) :-
	N_New is N - 1,
	inc_nth_value_in_list(N_New, [], ListNew).


/* heuristic/2: Description see interface. */

heuristic(Prog, Result) :-
	is_list(Prog),
	!,
	heuristic_default(Nr),
	heuristic(Nr, Prog, Result).
heuristic(ProgName, Result) :-
	atom(ProgName),
	!,
	dconsult(ProgName, Prog),
	heuristic_default(Nr),
	heuristic(Nr, Prog, Result).


/* heuristic/3: Description see interface. */

heuristic(Number, ProgName, Result) :-
	atom(ProgName),
	!,
	dconsult(ProgName, Prog),
	heuristic(Number, Prog, Result).

heuristic(1, Prog, Result) :-
	!,
	count_disjunctions(Prog, [NrNormalFacts | Facts], [NrNormalRules | Rules]),
	NrNormals is NrNormalFacts + NrNormalRules,
	sum_list(Facts, NrDisFacts),
	sum_list(Rules, NrDisRules),
	NrDis is NrDisFacts + NrDisRules,
	!,
	(NrDis + NrNormals) > 0,
	Factor is (NrDis / (NrDis + NrNormals)),
	heuristic_1_result(Factor, Result),
	heuristic_1_print(NrNormals, NrDis, Factor, Result).

heuristic_1_result(Factor, tps) :-
	Factor > 0.6.
heuristic_1_result(Factor, tpi) :-
	Factor < 0.3.
heuristic_1_result(_Factor, Result) :-
	unknown(Result).

heuristic_1_print(NrNormals, NrDis, Factor, Op) :-
	heuristic_verbose(true),
	writef('Heuristic 1 for disjunctive logic programs:\n'),
	writef('   Weight of normal      rules+facts: %w\n', [NrNormals]),
	writef('   Weight of disjunctive rules+facts: %w\n', [NrDis]),
	writef('   Factor (portion of disjunct. r+f): %w\n', [Factor]),
	writef('   Suggested operator               : %w\n', [Op]).
heuristic_1_print(_NrNormals, _NrDis, _Factor, _Op).


heuristic(2, Prog, Result) :-
	!,
	count_disjunctions(Prog, [NrNormalFacts | Facts], [NrNormalRules | Rules]),
	NrNormals is NrNormalFacts + NrNormalRules,
	heuristic_2_sum_list(2, Facts, WeightDisFacts),
	heuristic_2_sum_list(2, Rules, WeightDisRules),
	WeightDis is WeightDisFacts + WeightDisRules,
	heuristic_2_result(NrNormals, WeightDis, Result),
	heuristic_2_print(NrNormals, WeightDis, Result).

heuristic_2_sum_list(Factor, [FirstNr | TailList], Sum) :-
	NewFactor is Factor + 1,
	heuristic_2_sum_list(NewFactor, TailList, TailSum),
	Sum is (Factor * FirstNr) + TailSum.
heuristic_2_sum_list(_Factor, [], 0).

heuristic_2_result(NrNormals, WeightDis, tps) :-
	WeightDis > (2 * NrNormals).
heuristic_2_result(NrNormals, WeightDis, tpi) :-
	NrNormals > (2 * WeightDis).
heuristic_2_result(_NrNormals, _WeightDis, Result) :-
	unknown(Result).

heuristic_2_print(NrNormals, WeightDis, Op) :-
	heuristic_verbose(true),
	writef('Heuristic 2 for disjunctive logic programs:\n'),
	writef('   Weight of normal      rules+facts: %w\n', [NrNormals]),
	writef('   Weight of disjunctive rules+facts: %w\n', [WeightDis]),
	writef('   Suggested operator               : %w\n', [Op]).
heuristic_2_print(_NrNormals, _WeightDis, _Op).


/* sum_list(+List, -Sum) <-
        Add the elements of List into Sum. */

/* The same is already defined in the library lists.pl. */

%sum_list([FirstNr | TailList], Sum) :-
%	sum_list(TailList, TailSum),
%	Sum is FirstNr + TailSum.
%sum_list([], 0).


/* test_heuristics_with_prog/3: Description see interface. */

test_heuristics_with_prog(ProgFile, TimesToRun, ResultFile) :-
	clear_current_program,
	dconsult(ProgFile, NProg),
	dnlp_to_dlp(NProg, Prog),
	count_disjunctions(Prog, Facts, Rules),
	writef('\nCalculate minimal-model-state ...\n', []), !,
	call_with_timer(tree_tps_fixpoint(Prog, [], _),
	                TimesToRun, StateTime), !,
	tree_tps_fixpoint(Prog, [], ClauseTree),
	writef('\nCalculate minmal-models from minimal-model-state ...\n', []), !,
	call_with_timer(model_tree_to_clause_tree(ClauseTree, _),
	                TimesToRun, StateToModelsTime), !,
	model_tree_to_clause_tree(ClauseTree, ModelTreeFromClause),
	writef('\nCalculate minimal-models ...\n', []), !,
	call_with_timer(tree_tpi_fixpoint(Prog, [[]], _),
	                TimesToRun, ModelsTime), !,
	tree_tpi_fixpoint(Prog, [[]], ModelTree),
	writef('\nCalculate minimal-model-state from minimal-models ...\n', []), !,
	call_with_timer(model_tree_to_clause_tree(ModelTree, _),
	                TimesToRun, ModelsToStateTime), !,
	model_tree_to_clause_tree(ModelTree, ClauseTreeFromModels),
	tree_to_state(ClauseTree,           State),
	tree_to_state(ClauseTreeFromModels, StateFromModels), 
	tree_to_state(ModelTree,            Models),
	tree_to_state(ModelTreeFromClause,  ModelsFromState),
	length(State, StateLen),
	length(Models, ModelsLen),
	StateAndDualTime is StateTime + StateToModelsTime,
	ModelsAndDualTime is ModelsTime + ModelsToStateTime,
	tell(ResultFile),
	writef('\nProgram-filename                 : %w\n', [ProgFile]),
	writef('Number of times to run operators : %w\n', [TimesToRun]),
	writef('Number of facts by disj.-size    : %w\n', [Facts]),
	writef('Number of rules by disj.-size    : %w\n', [Rules]),
	writef('Number of disjunctions         n : %w\n', [StateLen]),
	writef('Number of models               m : %w\n', [ModelsLen]),
	writef('Time for Tps-fixpoint          H : %w seconds\n', [StateTime]),
	writef('Time for Tpi-fixpoint          M : %w seconds\n', [ModelsTime]),
	writef('Time for state to models       D : %w seconds\n', [StateToModelsTime]),
	writef('Time for models to state       D : %w seconds\n', [ModelsToStateTime]),
	writef('Time for Tps and dualization  HD : %w seconds\n', [StateAndDualTime]),
	writef('Time for Tpi and dualization  MD : %w seconds\n\n', [ModelsAndDualTime]),
	heuristic(1, Prog, _Result_1), nl,
	heuristic(2, Prog, _Result_2),
	ignore( (
	    State \== StateFromModels,
	    writef('\nWARNING: minimal-model-state not identical !!!\n', [])
	) ),
	ignore( (
	    Models \== ModelsFromState,
	    writef('\nWARNING: minimal-models not identical !!!\n', [])
	) ),
	writef('\nMinimal-model-state direct     : %w\n', [State]),
	writef('\nMinimal-model-state from models: %w\n', [StateFromModels]),
	writef('\nMinimal-models direct    : %w\n', [Models]),
	writef('\nMinimal-models from state: %w\n', [ModelsFromState]),
	writef('\nThe original logic program:\n', []),
	dportray(lp, NProg),
	writef('\nThe positive logic program:\n', []),
	dportray(lp, Prog),
	told.


/*** Initialization ***********************************************/


/*** Testing / Debugging ******************************************/


thwp(ProgFile, TimesToRun, ResultFile) :-
	test_heuristics_with_prog(ProgFile, TimesToRun, ResultFile).


test_heuristics :-
	%thwp('seipel/example_1', 10, 'results/heur_habil_1'),
	%thwp('seipel/example_2', 10, 'results/heur_habil_2'),
	%thwp('seipel/example_3', 10, 'results/heur_habil_3'),
	%thwp('classical_examples/same_gen', 10, 'results/heur_same_gen'),
	%thwp('classical_examples/same_gen_magic', 10, 'results/heur_same_gen_magic'),
	%thwp('dix/dix_142', 10, 'results/heur_dix_142'),
	%thwp('eiter/strategic_company', 10, 'results/heur_strategic_company'),
	%thwp('elementary/abcd1', 10, 'results/heur_abcd1'),
	%thwp('elementary/abcde1', 10, 'results/heur_abcde1'),
	%thwp('elementary/abcdef1', 10, 'results/heur_abcdef1'),
	%thwp('elementary/program_1', 10, 'results/heur_elem_1'),
	%thwp('games/win_move_2', 10, 'results/heur_win_move_2'),
	%thwp('graph_problems/graph_coloring_4', 10, 'results/heur_graph_color_4'),
	%thwp('medicine/blood_example', 10, 'results/heur_blood_example'),
	%thwp('medicine/blood_rules', 10, 'results/heur_blood_rules'),
	%thwp('minker/blocks_world', 10, 'results/heur_blocks_world'),
	%thwp('minker/minker2', 10, 'results/heur_minker2'),
	%thwp('path/path_3', 10, 'results/heur_path_3'),
	%thwp('path/path_abcd', 10, 'results/heur_path_abcd'),
	%thwp('path/path_abcdef', 10, 'results/heur_path_abcdef'),
	%thwp('path_blocked/pb1_bdwfs_state', 10, 'results/heur_pb1_bdwfs'),
	%thwp('path_blocked/pb2', 10, 'results/heur_pb2'),
	%thwp('path_blocked/pb3', 10, 'results/heur_pb3'),
	%thwp('possible_models/powerplant_example', 10, 'results/heur_powerplant_ex'),
	%thwp('possible_models/weekdays', 10, 'results/heur_weekdays'),
	%thwp('projects/dentalog/dentalog_kb', 10, 'results/heur_dentalog'),
	%thwp('przymusinski/przymusinski_1', 10, 'results/heur_przy_1'),
	%thwp('spatial_databases/spatial', 10, 'results/heur_spatial'),
	%thwp('spatial_databases/spatial1', 10, 'results/heur_spatial1'),
	%thwp('states/state_11', 10, 'results/heur_state_11'),
	%thwp('states/state_12', 10, 'results/heur_state_12'),
	%thwp('states/state_13', 10, 'results/heur_state_13'),
	%thwp('states/state_14', 10, 'results/heur_state_14'),
	%thwp('states/state_2', 10, 'results/heur_state_2'),
	%thwp('states/state_6', 10, 'results/heur_state_6'),
	%thwp('states/state_raja_3', 10, 'results/heur_state_raja'),
	%thwp('states/binary_states/branch2', 10, 'results/heur_branch2'),
	%thwp('states/binary_states/chain7', 10, 'results/heur_chain7'),
	%thwp('states/binary_states/cycle9', 10, 'results/heur_cycle9'),
	%thwp('wastl/ralf_3', 10, 'results/heur_ralf_3'),
	%thwp('wastl/train', 10, 'results/heur_train'),
	writef('\nHeuristics-test successful finished.\n', []).


/******************************************************************/


/* Emacs-Configuration: */

%%% Local Variables:
%%% mode: prolog
%%% End:
