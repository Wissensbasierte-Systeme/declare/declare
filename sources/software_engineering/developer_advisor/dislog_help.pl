

/******************************************************************/
/***                                                            ***/
/***       Program : DisLog Developer Advisor                   ***/
/***    SourceFile : dislog_help                                ***/
/***       Version : 0.0.0                                      ***/
/***        Author : Thomas Schilling                           ***/
/***      Language : SWI-Prolog 2.9.5                           ***/
/***       Started : 6. August 1997                             ***/
/***  Last Changed : 4. April 1998                              ***/
/***       Purpose : Help-system for DisLog. Using information  ***/
/***                 from the sourcefiles-comments.             ***/
/***                                                            ***/
/******************************************************************/


:- module( dda_dislog_help, [
      init_help/0,
      init_help/1,
      dhelp/0,
      dhelp/1,
      dhelp/2,
      dislog_help_pred/1,
      dislog_help_substring/1,
      dislog_help_pattern/1 ] ).

   
:- use_module_dda( [
      'signature',
      'config',
      'create_info' ] ).


:- multifile
      dislog_help_index/5.


:- dynamic
      dislog_help_index/5,
      compare_line/1.


/* flags:
        sourcefilename */


/*** Interface ****************************************************/


/* init_help <-
      The same as init_help(default). */


/* init_help(?Check) <-
      Check can be check, no_check or default. If Check is a
      variable it is unified with the default-value from
      init_help_default/1. The same value is used if Check
      is default. If Check is check check_sources is called
      first, otherwise not. Then the help-index is created if
      necessary, that is the file containing the comments-
      information for the help is newer than the help-index. */


/* dhelp <-
      The same as dhelp(dhelp). */


/* dhelp(+Pred) <-
      The same as dislog_help_pred(Pred). */


/* dhelp(+Type, +Pattern) <-
      Depending on value of Type:
         pred or predicate:
             The same as dislog_help_pred(Pattern).
         sub or substring:
             The same as dislog_help_substring(Pattern).
           pat or pattern:
             The same as dislog_help_pattern(Pattern). */


/* dislog_help_pred(+Pred) <-
      Shows the comments that look like describing the predicate
      Pred. */


/* dislog_help_substring(+SubString) <-
      Shows the comments that contain a line that contains
      SubString. Case is ignored. */


/* dislog_help_pattern(+Pattern) <-
      Shows the comments that contain a line that matches
      "*Pattern*" using SWI-Prolog's wildcard_match/2. Case is
        ignored. Current patterns from SWI-Prolog manual 2.8.4:
        ?     - matches one arbitrary character
        *     - matches any number of arbitrary characters
          [...] - matches one of the characers between the braces
                char1-char2 indicates a range
        {...} - matches any of the patterns of the comma-separated
                list between the braces */


/*** Implementation ***********************************************/


/* init_help/0: Description see interface. */

:- signature(init_help).

init_help :-
   init_help(default).


/* init_help/1: Description see interface. */

:- signature(init_help(atom)).
:- signature(init_help(var)).

init_help(Check) :-
   var(Check),
   !,
   init_help_default(Check),
   init_help(Check).
init_help(default) :-
   !,
   init_help_default(Check),
   init_help(Check).
init_help(check) :-
   !,
   check_sources,
   !,
   init_help(no_check).
init_help(no_check) :-
   !,
   comments_file(CommentsFile),
   ih_checkexist_comments(CommentsFile),
   time_file(CommentsFile, CommentsFileTime),
   index_file(IndexFile),
   ih_checkexist_index(IndexFile),
   time_file(IndexFile, IndexFileTime),
   ih_checktime_comments_index(CommentsFileTime, IndexFileTime).
init_help(_Check) :-
   !,
   writef('ERROR: init_help(?Check):\n'),
   writef('       Check must be one of: default, check, no_check\n').

ih_checkexist_comments(File) :-
   exists_file(File),
   !.
ih_checkexist_comments(_File) :-
   !,
   check_sources.

ih_checkexist_index(File) :-
   exists_file(File),
   !.
ih_checkexist_index(_File) :-
   !,
   create_help_index.

ih_checktime_comments_index(CommentsFileTime, IndexFileTime) :-
   CommentsFileTime > IndexFileTime,
   !,
   create_help_index.
ih_checktime_comments_index(_CommentsFileTime, _IndexFileTime) :-
   writef('\nConsulting DisLog help-index ...'),
   retractall(dislog_help_index(_File, _Line, _Pred, _First, _Last)),
   index_file(IndexFile),
   consult(IndexFile).


/* dislog_help_index(?FileName, ?LineNumber, ?Predicate, ?FirstByte, ?LastByte) <-
      This predicate works like a database-index. It has a clause for every
      comment that is recorded in the comments-file. It is created during
      runtime by create_help_index/0. The arguments will be unified with the
      following contents:
        FileName   - name of the sourcefile which contains the comment
        LineNumber - number of the first line of the comment in the
                     sourcefile
        Predicate  - if the comment looks like the description of a
                     predicate, this is it's name
        FirstByte  - number of the first byte of the comment-text in the
                     comment-file
        LastByte   - number of the last byte of the comment-text in the
                     comment-file */


/* create_help_index <-
      Create the help-index. */

:- signature(create_help_index).

create_help_index :-
   writef('Creating DisLog help-index ...\n'),
   % Clear help-index.
   retractall(dislog_help_index(_, _, _, _, _)),
   % Create help-index from comments-file.
   comments_file(CommentsFile),
   open_stream(CommentsFile, read, commentsfile_stream,
        [type(text), eof_action(eof_code)]),
   get0(commentsfile_stream, Char),
   !,
   read_comments_entry(Char),
   close(commentsfile_stream),
   % Set arguments that should be indexed for faster access.
   index(dislog_help_index(1, 1, 1, 0, 0)),
   % Create sourcefile that will define help-index.
   index_file(IndexFile),
   open_stream(IndexFile, write, indexfile_stream,
        [type(text), eof_action(eof_code)]),
   write(indexfile_stream, '/* Automatically generated file. */'),
   nl(indexfile_stream),
   write(indexfile_stream, '/* Do NOT edit. */'),
   nl(indexfile_stream),
   nl(indexfile_stream),
   write_index_entry(dislog_help_index(
      _File, _Line, _Pred, _First, _Last)),
   close(indexfile_stream).


/* write_index_entry(+IndexEntry) <-
      Write one entry of the help-index for the index-file and
      continue automatically with the next.
      IndexEntry must be of the form:
          dislog_help_index(_File, _Line, _Pred, _First, _Last) */

write_index_entry(IndexEntry) :-
   call(IndexEntry),
   swritef(String, '%q.\n', [IndexEntry]),
   write(indexfile_stream, String),
   fail.
write_index_entry(_IndexEntry).


/* read_comments_entry(+Char) <-
      Read one comment from the comments-file.
      Also the appropriate index-entry is made.
      Used by create_help_index/0.
      Char is the next character in the comments-file.
      Automatically continues with the next entry. */

read_comments_entry(-1).   % end of file
read_comments_entry(Char) :-
   read_comments_line(Char, Line, Char1),
   read_comments_head(Line, Char1,
      [_LineNumber, _Pred, _FirstByte, _LastByte]).
read_comments_head([Sep, 0'F, 0'i, 0'l, 0'e, 58, 32 | LineRest],
      Char, [LineNumber, Pred, _FirstByte, LastByte]) :-   % File:
   comments_file_separator(Sep),
   !,
   name(FileName, LineRest),
   flag(sourcefilename, _Old, FileName),
   character_count(commentsfile_stream, Byte),
   FirstByte is Byte - 1,
   read_comments_line(Char, Line, Char1),
   read_comments_head(Line, Char1,
                      [LineNumber, Pred, FirstByte, LastByte]).
read_comments_head([Sep, 0'L, 0'i, 0'n, 0'e, 58, 32 | LineRest],
      Char, [LineNumber, Pred, _FirstByte, LastByte]) :-   % Line:
   comments_file_separator(Sep),
   !,
   name(LineNumber, LineRest),
   character_count(commentsfile_stream, Byte),
   FirstByte is Byte - 1,
   read_comments_line(Char, Line, Char1),
   read_comments_head(Line, Char1,
      [LineNumber, Pred, FirstByte, LastByte]).
read_comments_head([Sep, 0'P, 0'r, 0'e, 0'd, 58, 32 | LineRest],
      Char, [LineNumber, Pred, _FirstByte, LastByte]) :-   % Pred:
   comments_file_separator(Sep),
   !,
   name(Pred, LineRest),
   character_count(commentsfile_stream, Byte),
   FirstByte is Byte - 1,
   read_comments_line(Char, Line, Char1),
   read_comments_head(Line, Char1,
      [LineNumber, Pred, FirstByte, LastByte]).
read_comments_head(_Line, Char,
      [LineNumber, Pred, FirstByte, LastByte]) :-
   !,
   read_comments_body(Char, Char1),
   character_count(commentsfile_stream, Byte),
   LastByte is Byte - 1,
   ignore(unknown(Pred)),
   flag(sourcefilename, FileName, FileName),
   assertz(dislog_help_index(
      FileName, LineNumber, Pred, FirstByte, LastByte)),
   read_comments_entry(Char1).
read_comments_body(Char, Char) :-
   comments_file_separator(Char),
   !.
read_comments_body(-1, -1) :-   % end of file
   !.
read_comments_body(Char, Char1) :-
   !,
   read_comments_line(Char, _Line, Char2),
   read_comments_body(Char2, Char1).


/* read_comments_line(+Char, -Chars, -Char1) <-
      Read one line from the comments-containing file.
      Char is the next character to read,
      Chars will be the line and Char1 will be
      the first character of the next line. */

read_comments_line(-1, [], -1) :-   % end of file
   !.
read_comments_line(10, [], Char1) :-   % line feed
   !,
   get0(commentsfile_stream, Char2),
   read_comments_line_cr(Char2, Char1).
read_comments_line(13, [], Char1) :-   % carriage return
   !,
   get0(commentsfile_stream, Char2),
   read_comments_line_lf(Char2, Char1).
read_comments_line(Char, [Char | Chars], Char1) :-
   get0(commentsfile_stream, Char2),
   read_comments_line(Char2, Chars, Char1).

read_comments_line_cr(13, Char1) :-   % carriage return
   !,
   get0(commentsfile_stream, Char1).
read_comments_line_cr(Char, Char).

read_comments_line_lf(10, Char1) :-   % line feed
   !,
   get0(commentsfile_stream, Char1).
read_comments_line_lf(Char, Char).


/* dislog_help_pred/1: Description see interface. */

:- signature(dislog_help_pred(atom)).

dislog_help_pred(Pred) :-
   var(Pred),
   unknown(Pred),
   writef('ERROR: dislog_help_pred(+Predicate):\n'),
   writef('       Predicate must be instantiated\n'),
   !.
dislog_help_pred(Pred) :-
   pager_file(Pager),
   open_stream(Pager, write, pager_stream, [type(text)]),
   comments_file(CommentsFile),
   open_stream(CommentsFile, read, commentsfile_stream,
      [type(text), eof_action(eof_code)]),
   swritef(String, '\nDisLog-Help for Predicate : %w\n\n', [Pred]),
   write(pager_stream, String),
   current_output(OldOutput),
   set_output(pager_stream),
   signature_for_pred(Pred),
   nl,
   set_output(OldOutput),
   findall( [FileName, Line],
      dislog_help_index(FileName, Line, Pred, _First, _Last),
      PredList ),
   dislog_help_pred_loop(PredList, [], Comments),
   write_comment_list(Comments),
   close(commentsfile_stream),
   close(pager_stream),
   pager_command(Command),
   shell(Command, _Status),
   delete_file(Pager).
dislog_help_pred_loop([[File, Line] | Preds],
      OldComments, NewComments) :-
   add_comment_interval(File, Line, OldComments, NewComments1),
   dislog_help_pred_loop(Preds, NewComments1, NewComments).
dislog_help_pred_loop([], Comments, Comments).


/* write_filename(+FileName) <-
      Write the name of the sourcefile to the pager-stream. */

write_filename(FileName) :-
   swritef(String, 'SourceFile : %w\n\n', [FileName]),
   write(pager_stream, String).


/* write_empty <-
      Write an empty line to the pager-stream. */

write_empty :-
   nl(pager_stream).


/* write_comment(+LineNumber, +FirstByte) <-
      Write the comment-entry starting at FirstByte in the
      comment-file to the pager stream.
      The number of the first line is LineNumber. */

write_comment(LineNumber, FirstByte) :-
   stream_position(commentsfile_stream, _OldPos,
      '$stream_position'(FirstByte, 0, 0)),
   get0(commentsfile_stream, Char),
   write_comment_loop(LineNumber, Char).
write_comment_loop(_LineNumber, Char) :-
   comments_file_separator(Char),
   !.
write_comment_loop(_LineNumber, -1) :-   % end of file
   !.
write_comment_loop(LineNumber, Char) :-
   !,
   read_comments_line(Char, Line, Char1),
   swritef(String, '%5r: %s\n', [LineNumber, Line]),
   write(pager_stream, String),
   LineNumber1 is LineNumber + 1,
   write_comment_loop(LineNumber1, Char1).


/* write_comment_interval(+FileName, +LineNumber) <-
      Write the comment-entry from the comment-file to the pager-stream,
      which have the sourcefile FileName and whose linenumbers are
      between LineNumber minus and plus the value of
      help_linenumber_interval/1. */

write_comment_interval(FileName, LineNumber) :-
   help_linenumber_interval(Interval),
   From is LineNumber - Interval,
   To   is LineNumber + Interval,
   write_comment_interval_loop(FileName, From, To).
write_comment_interval_loop(_FileName, LineNumber, To) :-
   LineNumber >= To,
   !.
write_comment_interval_loop(FileName, LineNumber, To) :-
   dislog_help_index(FileName, LineNumber, _Pred, FirstByte, _LastByte),
   !,
   write_comment(LineNumber, FirstByte),
   LineNumber1 is LineNumber + 1,
   write_comment_interval_loop(FileName, LineNumber1, To).
write_comment_interval_loop(FileName, LineNumber, To) :-
   !,
   LineNumber1 is LineNumber + 1,
   write_comment_interval_loop(FileName, LineNumber1, To).


/* write_comment_list(+CommentList) <-
      Write the comment-entry from CommentList to the pager-stream.
      CommentList can be created with add_comment_interval/4. */

write_comment_list([]) :- 
   !,
   swritef(String, 'Found nothing.\n'),
   write(pager_stream, String).
write_comment_list(Comments) :- 
   write_comment_list_loop(Comments).

write_comment_list_loop([[FileName | Lines] | Files]) :-
   write_empty,
   write_filename(FileName),
   write_comment_list_lines(FileName, Lines),
   write_comment_list_loop(Files).
write_comment_list_loop([]).

write_comment_list_lines(FileName, [Line | Lines]) :-
   dislog_help_index(FileName, Line, _Pred, FirstByte, _LastByte),
   !,
   write_comment(Line, FirstByte),
   write_comment_list_lines(FileName, Lines).
write_comment_list_lines(FileName, [_Line | Lines]) :-
   write_comment_list_lines(FileName, Lines).
write_comment_list_lines(_FileName, []).


/* add_comment_interval(+FileName, +LineNumber, +OldList, -NewList) <-
      Collect entries of the comment-file for displaying later.
      FileName is the sourcefile.
      LineNumber is the position of the comment in the sourcefile.
      The linenumbers to collect are between LineNumber minus and
      plus the value of help_linenumber_interval/1.
      The List is a list of sublists.
      Each sublist has the filename as the first element and the
      rest are the linenumbers to display.
      NewList will contain OldList and the new entries. */

add_comment_interval(File, Line, [[File | Lines] | OldList],
      [[File | NewLines] | OldList]) :-
   !,
   add_comment_interval_lines(Line, Lines, NewLines).
add_comment_interval(File, Line, [OldSub | OldList], [OldSub | NewList]) :-
   !,
   add_comment_interval(File, Line, OldList, NewList).
add_comment_interval(File, Line, [], [[File | NewLines]]) :-
   !,
   add_comment_interval_lines(Line, [], NewLines).

add_comment_interval_lines(Line, Lines, NewLines) :-
      help_linenumber_interval(Interval),
   From is Line - Interval,
   To   is Line + Interval,
   findall(Num, between(From, To, Num), NewLines1),
   append(Lines, NewLines1, NewLines2),
   sort(NewLines2, NewLines).


/* dislog_help_substring/1: Description see interface. */

:- signature(dislog_help_substring(atom)).

dislog_help_substring(SubString) :-
   var(SubString),
   unknown(SubString),
   writef('ERROR: dislog_help_substring(+SubString):\n'),
   writef('       SubString must be instantiated\n'),
   !.
dislog_help_substring(SubString) :-
   atom_chars(SubString, String1),
   to_lower_list(String1, String2),
   writef('Searching for substring %s ...\n', [String2]),
   retractall(compare_line(_Line)),
   assert(
      (compare_line(Line) :-
         compare_line_substring(Line, String2))),
   pager_file(Pager),
   open_stream(Pager, write, pager_stream, [type(text)]),
   comments_file(CommentsFile),
   open_stream(CommentsFile, read, commentsfile_stream,
      [type(text), eof_action(eof_code)]),
   swritef(String,
      '\nDisLog-Help for SubString : %s\n\n', [String2]),
   write(pager_stream, String),
   get0(commentsfile_stream, Char),
   search_comments_entry(Char, [], CommentList),
   write_comment_list(CommentList),
   close(commentsfile_stream),
   close(pager_stream),
   pager_command(Command),
   shell(Command, _Status),
   delete_file(Pager).


/* compare_line_substring(+Line, +SubString) <-
      Test if Line contains SubString.
      Both are lists of ASCII characters.
      Case is ignored. */

compare_line_substring(
      [LineChar | Line], [SubStringChar | SubString]) :-
   to_lower_char(LineChar, Char1),
   Char1 = SubStringChar,
   compare_line_substring_rest(Line, SubString).
compare_line_substring([_LineChar | Line], SubString) :-
   compare_line_substring(Line, SubString).

compare_line_substring_rest(
      [LineChar | Line], [SubStringChar | SubString]) :-
   to_lower_char(LineChar, Char1),
   Char1 = SubStringChar,
   !,
   compare_line_substring_rest(Line, SubString).
compare_line_substring_rest(_Line, []).


/* dislog_help_pattern/1: Description see interface. */

:- signature(dislog_help_pattern).

dislog_help_pattern(Pattern) :-
   var(Pattern),
   unknown(Pattern),
   writef('ERROR: dislog_help_pattern(+Pattern):\n'),
   writef('       Pattern must be instantiated\n'),
   !.
dislog_help_pattern(Pattern) :-
   atom_chars(Pattern, PatternList1),
   to_lower_list(PatternList1, PatternList2),
   flatten([0'*, PatternList2, 0'*], PatternList3),
   atom_chars(Pattern1, PatternList3),
   writef('Searching for pattern %w ...\n', [Pattern1]),
   retractall(compare_line(_Line)),
   assert((compare_line(Line) :-
               compare_line_pattern(Line, Pattern1))),
   pager_file(Pager),
   open_stream(Pager, write, pager_stream, [type(text)]),
   comments_file(CommentsFile),
   open_stream(CommentsFile, read, commentsfile_stream,
      [type(text), eof_action(eof_code)]),
   swritef(String, '\nDisLog-Help for Pattern : %w\n\n', [Pattern1]),
   write(pager_stream, String),
   get0(commentsfile_stream, Char),
   search_comments_entry(Char, [], CommentList),
   write_comment_list(CommentList),
   close(commentsfile_stream),
   close(pager_stream),
   pager_command(Command),
   shell(Command, _Status),
   delete_file(Pager).


/* compare_line_pattern(+Line, +Pattern) <-
      Test if Line matches Pattern.
      Line is a list of ASCII characters.
      Pattern is an atom.
      Case is ignored.
      Uses SWI-Prolog's wildcard_match/2. */

compare_line_pattern(Line, Pattern) :-
   to_lower_list(Line, Line1),
   string_to_list(String, Line1),
   wildcard_match(Pattern, String).


/* search_comments_entry(+Char, +OldList, -NewList) <-
      Search one comment from the comments-file. Used by
      dislog_help_pattern/1. Char is the next character in the
      comments-file. Automatically continues with the next entry.
      OldList and NewList are used to create the list of comments
      to display with add_comment_interval/4. */

search_comments_entry(-1, Comments, Comments).   % end of file
search_comments_entry(Char, OldComments, NewComments) :-
   read_comments_line(Char, Line, Char1),
   search_comments_head(Line, Char1, _LineNumber,
      OldComments, NewComments).
search_comments_head([Sep, 0'F, 0'i, 0'l, 0'e, 58, 32 | LineRest], Char,
      LineNumber, OldComments, NewComments) :-   % File:
   comments_file_separator(Sep),
   !,
   name(FileName, LineRest),
   flag(sourcefilename, _Old, FileName),
   read_comments_line(Char, Line, Char1),
   search_comments_head(Line, Char1, LineNumber,
      OldComments, NewComments).
search_comments_head([Sep, 0'L, 0'i, 0'n, 0'e, 58, 32 | LineRest],
      Char, LineNumber, OldComments, NewComments) :-   % Line:
   comments_file_separator(Sep),
   !,
   name(LineNumber, LineRest),
   read_comments_line(Char, Line, Char1),
   search_comments_head(Line, Char1, LineNumber,
      OldComments, NewComments).
search_comments_head([Sep | _LineRest], Char, LineNumber,
      OldComments, NewComments) :-
   comments_file_separator(Sep),
   !,
   read_comments_line(Char, Line, Char1),
   search_comments_head(Line, Char1, LineNumber,
      OldComments, NewComments).
search_comments_head(Line, Char, LineNumber,
      OldComments, NewComments) :-
   !,
   check_line_with_compare(Line, LineNumber,
      OldComments, NewComments1),
   search_comments_body(Char, Char1, LineNumber,
      NewComments1, NewComments2),
   search_comments_entry(Char1, NewComments2, NewComments).
search_comments_body(Char, Char, _LineNumber,
      Comments, Comments) :-
   comments_file_separator(Char),
   !.
search_comments_body(-1, -1, _LineNumber,
      Comments, Comments) :-   % end of file
   !.
search_comments_body(Char, Char1, LineNumber,
      OldComments, NewComments) :-
   !,
   read_comments_line(Char, Line, Char2),
   check_line_with_compare(Line, LineNumber,
      OldComments, NewComments1),
   search_comments_body(Char2, Char1, LineNumber,
      NewComments1, NewComments).
check_line_with_compare(Line, LineNumber, OldComments, NewComments) :-
   compare_line(Line),
   flag(sourcefilename, FileName, FileName),
   add_comment_interval(FileName, LineNumber,
      OldComments, NewComments).
check_line_with_compare(_Line, _LineNumber, Comments, Comments).


/* to_lower_list(+Chars1, ?Chars2) <-
      Convert character-list Chars1 to lower case into Chars2. */

to_lower_list([Char1 | Chars1], [Char2 | Chars2]) :-
   to_lower_char(Char1, Char2),
   to_lower_list(Chars1, Chars2).
to_lower_list([], []).


/* to_lower_char(+Char1, ?Char2) <-
      Convert character Char1 to lower case into Char2. */

to_lower_char(Char1, Char2) :-
   between(0'A, 0'Z, Char1),
   !,
   Char2 is Char1 + 32.
to_lower_char(Char, Char).


/* dhelp/0: Description see interface. */

:- signature(dhelp).

dhelp :-
   dhelp(dhelp).


/* dhelp/1: Description see interface. */

:- signature(dhelp(atom)).

dhelp(Pred) :-
   dislog_help_pred(Pred).


/* dhelp/2: Description see interface. */

:- signature(dhelp(atom, atom)).

dhelp(Type, Pattern) :-
   var(Type),
   !,
   Type = 'ERROR',
   dhelp(Type, Pattern).
dhelp(pred, Pattern) :-
   !,
   dislog_help_pred(Pattern).
dhelp(predicate, Pattern) :-
   !,
   dislog_help_pred(Pattern).
dhelp(pat, Pattern) :-
   !,
   dislog_help_pattern(Pattern).
dhelp(pattern, Pattern) :-
   !,
   dislog_help_pattern(Pattern).
dhelp(sub, Pattern) :-
   !,
   dislog_help_substring(Pattern).
dhelp(substring, Pattern) :-
   !,
   dislog_help_substring(Pattern).
dhelp(_Type, _Pattern) :-
   !,
   writef('ERROR: dhelp(+Type, +Pattern):\n'),
   writef('       Type must be one of: pred, predicate,\n'),
   writef('                            sub, substring,\n'),
   writef('                            pat, pattern\n').


/*** Initialization ***********************************************/


:- init_help(default).


/*** Testing / Debugging ******************************************/


/******************************************************************/


/* Emacs-Configuration: */

%%% Local Variables:
%%% mode: prolog
%%% End:


