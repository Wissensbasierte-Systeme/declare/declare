

/******************************************************************/
/***                                                            ***/
/***       Program : DisLog Developer Advisor                   ***/
/***    SourceFile : dda                                        ***/
/***       Version : 0.0.0                                      ***/
/***        Author : Thomas Schilling                           ***/
/***      Language : SWI-Prolog 2.9.5                           ***/
/***       Started : 20. March 1998                             ***/
/***  Last Changed : 2. April 1998                              ***/
/***       Purpose : System to support Developers in DisLog.    ***/
/***                                                            ***/
/******************************************************************/


:- module( dda, [
      lmdb/0,
      smdb/0,
      cmdb/0,
      amdb/0,
      lmdba/0,
      smdba/0,
      pmdb/0,
      cmdb_all/0 ] ).

/*
:- module( dda, [
%     config
%     create_info
      create_metrics_db/0,
%     dda   (see abbreviations at the end)
%     dislog_help
      init_help/0,
      init_help/1,
      dhelp/0,
      dhelp/1,
      dhelp/2,
      dislog_help_pred/1,
      dislog_help_substring/1,
      dislog_help_pattern/1,
%     heuristic
      heuristic/2,
      heuristic/3,
      test_heuristics_with_prog/3,
%     metrics_db
      save_metrics_db/0,
      save_metrics_db/1,
      load_metrics_db/0,
      load_metrics_db/1,
      analyse_metrics_db/0,
      save_metrics_db_analysis/0,
      save_metrics_db_analysis/1,
      load_metrics_db_analysis/0,
      load_metrics_db_analysis/1,
      portray_metrics_db/0,
      init_metrics_db/0,
      clear_metrics_db/0,
      portray_metrics_db_interface/1,
      get_metrics_db_relation/2,
%     parse_source
%     read_source
%     signature
      signature/1,
      signature_for_pred/1,
      signature_for_pred/2,
      signature_for_type/1,
      signature_types/1,
      clear_signature/0,
%     signature_db
%     tools
%     abbreviations   (implemented in this sourcefile)
      lmdb/0,
      smdb/0,
      cmdb/0,
      amdb/0,
      lmdba/0,
      smdba/0,
      pmdb/0,
      cmdb_all/0 ] ).
*/


:- use_module_dda([
      'signature',
      'signature_db',
      'config',
      'dislog_help',
      'heuristic',
      'create_info',
      'metrics_db' ]).


/*** Interface ****************************************************/


/* For the description of the abbreviations see the
        respective predicate. */

lmdb  :- load_metrics_db.
smdb  :- save_metrics_db.
cmdb  :- create_metrics_db.
amdb  :- analyse_metrics_db.
lmdba :- load_metrics_db_analysis.
smdba :- save_metrics_db_analysis.
pmdb  :- portray_metrics_db.


/* cmdb_all <-
        Create metrics-database and do all other steps of analysing
        and saving the data. */

cmdb_all :-
   create_metrics_db,
   save_metrics_db,
   analyse_metrics_db,
   save_metrics_db_analysis,
   portray_metrics_db.


/*** Implementation ***********************************************/


/*** Initialization ***********************************************/


/*** Testing / Debugging ******************************************/


/******************************************************************/


/* Emacs-Configuration: */

%%% Local Variables:
%%% mode: prolog
%%% End:


