
/******************************************************************/
/***                                                            ***/
/***       Program : DisLog Developer Advisor                   ***/
/***    SourceFile : signature                                  ***/
/***       Version : 0.0.0                                      ***/
/***      Author : Thomas Schilling                             ***/
/***      Language : SWI-Prolog 2.9.5                           ***/
/***       Started : 4. September 1997                          ***/
/***  Last Changed : 21. March 1998                             ***/
/***       Purpose : Extend the help-system with signature of   ***/
/***                 predicates.                                ***/
/***                                                            ***/
/******************************************************************/


:- module( dda_signature, [
      signature/1,
      signature_for_pred/1,
      signature_for_pred/2,
      signature_for_type/1,
      signature_types/1,
      clear_signature/0 ] ).


:- dynamic
      signature_pred_index/3,
      signature_type_index/3.


/*** Interface ****************************************************/


/* signature(+PredTerm) <-
      Define the signature of a predicate. PredTerm is of the form
      predicate_name(type1, type2, ...) */


/* signature_for_pred(?Pred) <-
      Show signature for all arities of predicate Pred. If Pred
      is a variable, all signature are shown. */

/* signature_for_pred(?Pred/?Arity) <-
      Show signature for predicate Pred with Arity. If Pred
      is a variable, all signature with Arity are shown. If
      Arity is a variable all arities of Pred are shown. */

/* signature_for_pred(?Pred, ?Arity) <-
      Show signature for predicate Pred with Arity. If Pred
      is a variable, all signature with Arity are shown. If
      Arity is a variable all arities of Pred are shown. */


/* signature_for_type(?Type) <-
      Shows predicate/arity for predicates that have Type in
      their signature. If Type is a variable, this is done for
      all types. */


/* signature_types(-TypeList) <-
      Returns a list of known types from signature in TypeList. */


/* clear_signature <-
      Clear the signature-database. */


/*** Implementation ***********************************************/


/* signature_pred_index(?Pred, ?Arity, ?Arguments) <-
      Defined as facts: predicate Pred with Arity has
      the type list Arguments as arguments. */


/* signature_type_index(?Type, ?Pred, ?Arity) <-
      Defined as facts:
      Type is used in predicate Pred with Arity. */


/* signature/1: Description see interface. */

signature(PredTerm) :-
   \+ ground(PredTerm),
   !,
   writef('ERROR: signature(+PredTerm):\n'),
   writef('       Pred must not hold free variables.\n').
signature(PredTerm) :-
   PredTerm =.. [Pred | Arguments],
   length(Arguments, Arity),
   assert_if(signature_pred_index(Pred, Arity, Arguments)),
   signature_loop(Pred, Arity, Arguments).

signature_loop(Pred, Arity, [FirstArg | Arguments]) :-
   assert_if(signature_type_index(FirstArg, Pred, Arity)),
   signature_loop(Pred, Arity, Arguments).
signature_loop(_Pred, _Arity, []).


/* assert_if(Fact) <-
      Fact will only be asserted if it is not yet. */

assert_if(Fact) :-
%  don't give warnings if Fact is not defined yet
   unknown(OldUnknown, fail),
   assert_if_2(Fact),
   unknown(_LocalUnknown, OldUnknown).

assert_if_2(Fact) :-
   \+ Fact,
   !,
   assert(Fact).
assert_if_2(_Fact).


/* signature_for_pred/1: Description see interface. */

signature_for_pred(Pred) :-
   var(Pred),
   !,
   signature_for_pred(Pred, _Arity).
signature_for_pred(Pred/Arity) :-
   signature_for_pred(Pred, Arity).
signature_for_pred(Pred) :-
   signature_for_pred(Pred, _Arity).


/* signature_for_pred/2: Description see interface. */

signature_for_pred(Pred, Arity) :-
   \+ signature_pred_index(Pred, Arity, _Arguments),
   !,
   writef('No Signature-Information.\n').
signature_for_pred(Pred, Arity) :-
   signature_for_pred_loop(Pred, Arity).

signature_for_pred_loop(Pred, Arity) :-
   signature_pred_index(Pred, Arity, Arguments),
   %writef('Signature: %w/%w (%w)\n', [Pred, Arity, Arguments]),
   writef('Signature : %w/%w (', [Pred, Arity]),
   write_argument_list(Arguments),
   writef(')\n'),
   fail.
signature_for_pred_loop(_Pred, _Arity).

write_argument_list([LastArg]) :-
   !,
   writef('%w', [LastArg]).
write_argument_list([FirstArg | Arguments]) :-
   writef('%w, ', [FirstArg]),
   write_argument_list(Arguments).
write_argument_list([]).


/* signature_for_type/1: Description see interface. */

signature_for_type(Type) :-
   \+ signature_type_index(Type, _Pred, _Arity),
   !,
   writef('No Signature-Type-Information.\n').
signature_for_type(Type) :-
   signature_for_type_loop(Type).

signature_for_type_loop(Type) :-
   signature_type_index(Type, Pred, Arity),
   writef('Signature-Type : %w used in %w/%w\n', [Type, Pred, Arity]),
   fail.
signature_for_type_loop(_Type).


/* signature_types/1: Description see interface. */

signature_types(TypeList) :-
   findall(Type, signature_type_index(Type, _Pred, _Arity), TypeList1),
   % sort/2 also removes duplicates in SWI-Prolog
   sort(TypeList1, TypeList).


/* clear_signature/0: Description see interface. */

clear_signature :-
   retractall(signature_pred_index(Pred, Arity, _Arguments)),
   retractall(signature_type_index(_Type, Pred, Arity)).


/*** Initialization ***********************************************/


/* The definition of the signature in this sourcefile. They must
   be at the end, so that all necessary predicates are compiled. */

% :- clear_signature.
:- signature(signature(term)).
:- signature(signature_for_pred(term)).
:- signature(signature_for_pred(predicate)).
:- signature(signature_for_pred(predicate, number)).
:- signature(signature_for_type(type)).
:- signature(signature_types(list)).
:- signature(clear_signature).


/*** Testing / Debugging ******************************************/


/******************************************************************/


/* Emacs-Configuration: */

%%% Local Variables:
%%% mode: prolog
%%% End:


