

/******************************************************************/
/***                                                            ***/
/***           DDA:  DCG Trees                                  ***/
/***                                                            ***/
/******************************************************************/


/* scan_dcg_tree(Tree) <-
      */

scan_dcg_tree(Tree) :-
   scan_dcg_tree_block(Tree).
scan_dcg_tree(_Tree).

scan_dcg_tree_block(rule(Head, Body)) :-
   !,
   scan_dcg_tree_head(Head),
   scan_dcg_tree_body(Body).
scan_dcg_tree_block(fact(Head)) :-
   !,
   scan_dcg_tree_head(Head),
   flag(current_functor, Functor, Functor),
   flag(current_arity, Arity, Arity),
   record_pred_def(Functor, Arity).
scan_dcg_tree_block(directive(Body)) :-
   !,
   current_sourcefile(SourceFile),
   concat('DIRECTIVE_', SourceFile, Functor),
   flag(current_functor, _OldFunctor, Functor),
   flag(current_arity, _OldArity, 0),
   scan_dcg_tree_body(Body).
scan_dcg_tree_block(grammar_rule(Head, Body)) :-
   !,
   scan_dcg_tree_grammar_head(Head),
   scan_dcg_tree_grammar_body(Body).
scan_dcg_tree_block(Tree) :-
   !,
   scan_dcg_warning(scan_dcg_tree_block, Tree),
   fail.


scan_dcg_tree_head(predicate(atom(Functor), Args)) :-
   !,
   length(Args, Arity),
   flag(current_functor, _OldFunctor, Functor),
   flag(current_arity, _OldArity, Arity),
   record_pred_def(Functor, Arity).
scan_dcg_tree_head(predicate(atom(Functor))) :-
   !,
   flag(current_functor, _OldFunctor, Functor),
   flag(current_arity, _OldArity, 0),
   record_pred_def(Functor, 0).
scan_dcg_tree_head(Tree) :-
   !,
   scan_dcg_warning(scan_dcg_tree_head, Tree),
   fail.


scan_dcg_tree_body([HeadTree | TailTreeList]) :-
   !,
   scan_dcg_tree_body(HeadTree),
   scan_dcg_tree_body(TailTreeList).
scan_dcg_tree_body([]) :-
   !.
scan_dcg_tree_body(predicate(atom(call), [Goal])) :-
   !,
   scan_dcg_tree_body(Goal).
scan_dcg_tree_body(
      predicate(atom(call),
         [predicate(Functor, Args) | ExtraArgs])) :-
   !,
   append(Args, ExtraArgs, AllArgs),
   scan_dcg_tree_body(predicate(Functor, AllArgs)).
scan_dcg_tree_body(
      predicate(atom(call),
         [predicate(Functor) | ExtraArgs])) :-
   !,
   scan_dcg_tree_body(predicate(Functor, ExtraArgs)).
scan_dcg_tree_body(
      predicate(atom(apply),
         [predicate(Functor, Args), list(ExtraArgs)])) :-
   !,
   append(Args, ExtraArgs, AllArgs),
   scan_dcg_tree_body(predicate(Functor, AllArgs)).
scan_dcg_tree_body(
      predicate(atom(apply),
         [predicate(Functor), list(ExtraArgs)])) :-
   !,
   scan_dcg_tree_body(predicate(Functor, ExtraArgs)).
scan_dcg_tree_body(
      predicate(atom(ignore), [Goal])) :-
   !,
   scan_dcg_tree_body(Goal).
scan_dcg_tree_body(
      predicate(atom(once), [Goal])) :-
   !,
   scan_dcg_tree_body(Goal).
scan_dcg_tree_body(
      predicate(atom(call_with_depth_limit),
         [Goal, _Limit, _Result])) :-
   !,
   scan_dcg_tree_body(Goal).
scan_dcg_tree_body(
      predicate(atom(catch),
         [Goal, _Catcher, Recover])) :-
   !,
   scan_dcg_tree_body(Goal),
   scan_dcg_tree_body(Recover).
scan_dcg_tree_body(predicate(atom(block), [_Label, Goal, _Exit])) :-
   !,
   scan_dcg_tree_body(Goal).
scan_dcg_tree_body(predicate(atom(not), [Goal])) :-
   !,
   scan_dcg_tree_body(Goal).
scan_dcg_tree_body(predicate(atom(forall), [Cond, Action])) :-
   !,
   scan_dcg_tree_body(Cond),
   scan_dcg_tree_body(Action).
scan_dcg_tree_body(predicate(atom(findall), [_Var, Goal, _Bag])) :-
   !,
   scan_dcg_tree_body(Goal).
scan_dcg_tree_body(predicate(atom(bagof), [_Var, Goal, _Bag])) :-
   !,
   scan_dcg_tree_body(Goal).
scan_dcg_tree_body(predicate(atom(setof), [_Var, Goal, _Bag])) :-
   !,
   scan_dcg_tree_body(Goal).
scan_dcg_tree_body(
      predicate(atom(checklist), [predicate(Functor, Args), _List])) :-
   !,
   append(Args, ['_'], AllArgs),
   scan_dcg_tree_body(predicate(Functor, AllArgs)).
scan_dcg_tree_body(
      predicate(atom(checklist), [predicate(Functor), _List])) :-
   !,
   scan_dcg_tree_body(predicate(Functor, ['_'])).
scan_dcg_tree_body(
      predicate(atom(maplist),
         [predicate(Functor, Args), _List1, _List2])) :-
   !,
   append(Args, ['_', '_'], AllArgs),
   scan_dcg_tree_body(predicate(Functor, AllArgs)).
scan_dcg_tree_body(
      predicate(atom(maplist),
         [predicate(Functor), _List1, _List2])) :-
   !,
   scan_dcg_tree_body(predicate(Functor, ['_', '_'])).
scan_dcg_tree_body(
      predicate(atom(sublist),
         [predicate(Functor, Args), _List1, _List2])) :-
   !,
   append(Args, ['_'], AllArgs),
   scan_dcg_tree_body(predicate(Functor, AllArgs)).
scan_dcg_tree_body(
      predicate(atom(sublist),
         [predicate(Functor), _List1, _List2])) :-
   !,
   scan_dcg_tree_body(predicate(Functor, ['_'])).
scan_dcg_tree_body(predicate(atom(Functor), Args)) :-
   !,
   length(Args, Arity),
   record_pred_use(Functor, Arity).
scan_dcg_tree_body(predicate(atom(Functor))) :-
   !,
   record_pred_use(Functor, 0).
scan_dcg_tree_body(atom(Functor)) :-
   !,
   record_pred_use(Functor, 0).
scan_dcg_tree_body(variable(_Var)) :-
   !.
scan_dcg_tree_body(disjunction(A, B)) :-
   !,
   scan_dcg_tree_body(A),
   scan_dcg_tree_body(B).
scan_dcg_tree_body(if(Condition, Action)) :-
   !,
   scan_dcg_tree_body(Condition),
   scan_dcg_tree_body(Action).
scan_dcg_tree_body(list(List)) :-
   !,
   scan_dcg_tree_body(predicate(atom(consult), [list(List)])).
scan_dcg_tree_body(xfx_operator(:, _Module, Pred)) :-
   !,
   scan_dcg_tree_body(Pred).
scan_dcg_tree_body(xfx_operator(is, _ArgA, _ArgB)) :-
   !.   % may be some operators need special treatment
scan_dcg_tree_body(xfx_operator(_Op, _ArgA, _ArgB)).
scan_dcg_tree_body(fx_operator(\+, Arg)) :-
   !,
   scan_dcg_tree_body(Arg).
scan_dcg_tree_body(fx_operator(not, Arg)) :-
   !,
   scan_dcg_tree_body(Arg).
scan_dcg_tree_body(fx_operator(multifile, _Arg)) :-
   !.
scan_dcg_tree_body(fx_operator(meta_predicate, _Arg)) :-
   !.
scan_dcg_tree_body(fx_operator(module_transparent, _Arg)) :-
   !.
scan_dcg_tree_body(fx_operator(discontiguous, _Arg)) :-
   !.
scan_dcg_tree_body(fx_operator(dynamic, _Arg)) :-
   !.  % might use this to find undefined predicates
scan_dcg_tree_body(fx_operator(_Op, _Arg)).
scan_dcg_tree_body(xf_operator(_Op, _Arg)).
scan_dcg_tree_body(Tree) :-
   !,
   scan_dcg_warning(scan_dcg_tree_body, Tree),
   fail.


scan_dcg_tree_grammar_head(predicate(atom(Functor), Args)) :-
   !,
   length(Args, Length),
   Arity is Length + 2,
   flag(current_functor, _OldFunctor, Functor),
   flag(current_arity, _OldArity, Arity),
   record_pred_def(Functor, Arity).
scan_dcg_tree_grammar_head(predicate(atom(Functor))) :-
   !,
   flag(current_functor, _OldFunctor, Functor),
   flag(current_arity, _OldArity, 2),
   record_pred_def(Functor, 2).
scan_dcg_tree_grammar_head(Tree) :-
   !,
   scan_dcg_warning(scan_dcg_tree_grammar_head, Tree),
   fail.


scan_dcg_tree_grammar_body([HeadTree | TailTreeList]) :-
   !,
   scan_dcg_tree_grammar_body(HeadTree),
   scan_dcg_tree_grammar_body(TailTreeList).
scan_dcg_tree_grammar_body([]) :-
   !.
scan_dcg_tree_grammar_body(disjunction(A, B)) :-
   !,
   scan_dcg_tree_grammar_body(A),
   scan_dcg_tree_grammar_body(B).
scan_dcg_tree_grammar_body(if(Condition, Action)) :-
   !,
   scan_dcg_tree_grammar_body(Condition),
   scan_dcg_tree_grammar_body(Action).
scan_dcg_tree_grammar_body(predicate(atom(Functor), Args)) :-
   !,
   length(Args, Length),
   Arity is Length + 2,
   record_pred_use(Functor, Arity).
scan_dcg_tree_grammar_body(predicate(atom(!))) :-
   !.
scan_dcg_tree_grammar_body(predicate(atom(Functor))) :-
   !,
   record_pred_use(Functor, 2).
scan_dcg_tree_grammar_body(atom(!)) :-
   !.
scan_dcg_tree_grammar_body(atom(Functor)) :-
   !,
   record_pred_use(Functor, 2).
scan_dcg_tree_grammar_body(list(_List)) :-
   !.
scan_dcg_tree_grammar_body(no_grammar(Goals)) :-
   !,
   scan_dcg_tree_body(Goals).
scan_dcg_tree_grammar_body(Tree) :-
   !,
   scan_dcg_warning(scan_dcg_tree_grammar_body, Tree),
   fail.


/* record_pred_def(Functor, Arity) <-
      */

record_pred_def(Functor, Arity) :-
   do_record_pred(Functor, Arity),
   current_sourcefile(SourceFile),
   add_pred_def(SourceFile, Functor, Arity).
record_pred_def(_Functor, _Arity).


/* record_pred_use(Functor, Arity) <-
      */

record_pred_use(Functor, Arity) :-
   do_record_pred(Functor, Arity),
   current_sourcefile(SourceFile),
   flag(current_functor, CurFunctor, CurFunctor),
   flag(current_arity, CurArity, CurArity),
   add_pred_use(SourceFile, CurFunctor, CurArity, Functor, Arity).
record_pred_use(_Functor, _Arity).


/* do_record_pred(Functor, Arity) <-
      */

do_record_pred(Functor, Arity) :-
   \+ ignore_pred(Functor, Arity),
   \+ pred_is_system(Functor, Arity).


/* pred_is_system(Functor, Arity) <-
      */

pred_is_system(Functor, Arity) :-
   functor(Term, Functor, Arity),
   predicate_property(Term, built_in),
   !.
pred_is_system(Functor, Arity) :-
   functor(Term, Functor, Arity),
   predicate_property(Term, imported_from(system)),
   !.


/* scan_dcg_warning(Type, Tree) <-
      */

scan_dcg_warning(Type, Tree) :-
   name_append([
      'WARNING: ', Type, '(+Tree): unknown parse-tree\n'],
      Warning),
   swritef(String1, Warning, []),
   write(user_output, String1),
   swritef(String2, '         %w\n', [Tree]),
   write(user_output, String2).


/* portray_dcg_tree(Tree) <-
      */

portray_dcg_tree(Tree) :-
   writef('DCG-Parse-Tree:\n'),
   writef('===============\n'),
   portray_dcg_tree(Tree, 0).

portray_dcg_tree(atom(Atom), Indent) :-
   !,
   tab(Indent),
   writef('atom : %w\n', [Atom]).
portray_dcg_tree(variable(Var), Indent) :-
   !,
   tab(Indent),
   writef('variable : %w\n', [Var]).
portray_dcg_tree(number(Term), Indent) :-
   !,
   tab(Indent),
   writef('number : %w\n', [Term]).
portray_dcg_tree(string(String), Indent) :-
   !,
   tab(Indent),
   writef('string : %w\n', [String]).
portray_dcg_tree(rule(Head, Body), Indent) :-
   !,
   tab(Indent),
   writef('rule\n'),
   NewIndent is Indent + 2,
   portray_dcg_tree(Head, NewIndent),
   tab(Indent),
   writef(':-\n'),
   portray_dcg_tree(Body, NewIndent).
portray_dcg_tree(grammar_rule(Head, Body), Indent) :-
   !,
   tab(Indent),
   writef('grammar-rule\n'),
   NewIndent is Indent + 2,
   portray_dcg_tree(Head, NewIndent),
   tab(Indent),
   writef(':-\n'),
   portray_dcg_tree(Body, NewIndent).
portray_dcg_tree(fact(Head), Indent) :-
   !,
   tab(Indent),
   writef('fact\n'),
   NewIndent is Indent + 2,
   portray_dcg_tree(Head, NewIndent).
portray_dcg_tree(directive(Body), Indent) :-
   !,
   tab(Indent),
   writef('directive\n'),
   NewIndent is Indent + 2,
   portray_dcg_tree(Body, NewIndent).
portray_dcg_tree(rule_head(Head), Indent) :-
   !,
   tab(Indent),
   writef('rule_head\n'),
   NewIndent is Indent + 2,
   portray_dcg_tree(Head, NewIndent).
portray_dcg_tree(rule_body(Body), Indent) :-
   !,
   tab(Indent),
   writef('rule_body\n'),
   NewIndent is Indent + 2,
   portray_dcg_tree(Body, NewIndent).
portray_dcg_tree(predicate(atom(Functor), Args), Indent) :-
   !,
   tab(Indent),
   length(Args, Arity),
   writef('predicate : %w/%w\n', [Functor, Arity]),
   NewIndent is Indent + 2,
   portray_dcg_tree(Args, NewIndent).
portray_dcg_tree(predicate(atom(Functor)), Indent) :-
   !,
   tab(Indent),
   writef('predicate : %w/0\n', [Functor]).
portray_dcg_tree(list(List), Indent) :-
   !,
   tab(Indent),
   writef('[\n', []),
   NewIndent is Indent + 2,
   portray_dcg_tree_list(List, NewIndent),
   tab(Indent),
   writef(']\n', []).
portray_dcg_tree(list(Head, Tail), Indent) :-
   !,
   tab(Indent),
   writef('[\n', []),
   NewIndent is Indent + 2,
   portray_dcg_tree_list(Head, NewIndent),
   tab(Indent),
   writef('|\n', []),
   portray_dcg_tree(Tail, NewIndent),
   tab(Indent),
   writef(']\n', []).
portray_dcg_tree(xfx_operator(Op, Arg1, Arg2), Indent) :-
   !,
   tab(Indent),
   writef('xfx_operator : %w\n', [Op]),
   NewIndent is Indent + 2,
   portray_dcg_tree(Arg1, NewIndent),
   portray_dcg_tree(Arg2, NewIndent).
portray_dcg_tree(fx_operator(Op, Arg), Indent) :-
   !,
   tab(Indent),
   writef('fx_operator : %w\n', [Op]),
   NewIndent is Indent + 2,
   portray_dcg_tree(Arg, NewIndent).
portray_dcg_tree(xf_operator(Op, Arg), Indent) :-
   !,
   tab(Indent),
   writef('xf_operator : %w\n', [Op]),
   NewIndent is Indent + 2,
   portray_dcg_tree(Arg, NewIndent).
portray_dcg_tree(disjunction(A, B), Indent) :-
   !,
   tab(Indent),
   writef('or\n'),
   NewIndent is Indent + 2,
   portray_dcg_tree(A, NewIndent),
   tab(Indent),
   writef(';\n'),
   portray_dcg_tree(B, NewIndent).
portray_dcg_tree(if(Condition, disjunction(Then, Else)), Indent) :-
   !,
   tab(Indent),
   writef('if\n'),
   NewIndent is Indent + 2,
   portray_dcg_tree(Condition, NewIndent),
   tab(Indent),
   writef('then\n'),
   portray_dcg_tree(Then, NewIndent),
   tab(Indent),
   writef('else\n'),
   portray_dcg_tree(Else, NewIndent).
portray_dcg_tree(if(Condition, Then), Indent) :-
   !,
   tab(Indent),
   writef('if\n'),
   NewIndent is Indent + 2,
   portray_dcg_tree(Condition, NewIndent),
   tab(Indent),
   writef('then\n'),
   portray_dcg_tree(Then, NewIndent).
portray_dcg_tree(List, Indent) :-
   is_list(List),
   !,
   tab(Indent),
   writef('(\n'),
   NewIndent is Indent + 2,
   portray_dcg_tree_list(List, NewIndent),
   tab(Indent),
   writef(')\n').
portray_dcg_tree(no_grammar(List), Indent) :-
   !,
   tab(Indent),
   writef('{\n'),
   NewIndent is Indent + 2,
   portray_dcg_tree_list(List, NewIndent),
   tab(Indent),
   writef('}\n').
portray_dcg_tree(unknown(List), Indent) :-
   !,
   tab(Indent),
   writef('unknown : %w\n', [List]).
portray_dcg_tree(Tree, Indent) :-
   tab(Indent),
   writef('unknown_by_portray_dcg_tree : %w\n', [Tree]).

portray_dcg_tree_list([Head | Tail], Indent) :-
   portray_dcg_tree(Head, Indent),
   portray_dcg_tree_list(Tail, Indent).
portray_dcg_tree_list([], _Indent).


/******************************************************************/


