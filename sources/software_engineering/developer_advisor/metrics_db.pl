
/******************************************************************/
/***                                                            ***/
/***       Program : DisLog Developer Advisor                   ***/
/***    SourceFile : metrics_db                                 ***/
/***       Version : 0.0.0                                      ***/
/***        Author : Thomas Schilling                           ***/
/***      Language : SWI-Prolog 2.9.5                           ***/
/***       Started : 25. October 1997                           ***/
/***  Last Changed : 7. May 1998                                ***/
/***       Purpose : Managing the data for the software         ***/
/***                 metrics.                                   ***/
/***                                                            ***/
/******************************************************************/


:- module( dda_metrics_db, [
      add_pred_def/3,
      add_pred_use/5,
      save_metrics_db/0,
      save_metrics_db/1,
      load_metrics_db/0,
      load_metrics_db/1,
      analyse_metrics_db/0,
      save_metrics_db_analysis/0,
      save_metrics_db_analysis/1,
      load_metrics_db_analysis/0,
      load_metrics_db_analysis/1,
      portray_metrics_db/0,
      init_metrics_db/0,
      clear_metrics_db/0,
      portray_metrics_db_interface/1,
      get_metrics_db_relation/2 ] ).

   
:- use_module_dda( [
      'signature',
      'config',
      'tools',
      'create_info' ] ).


:- dynamic
      pred_def/4,
      pred_use/7,
      sourcefile_in_module/2,
      pred_undefined/4,
      pred_def_multiple/4,
      pred_use_over_module/5,
      pred_use_over_module_sum/3,
      module_interface_pred/4,
      portray/1.


:- multifile
      pred_def/4,
      pred_use/7,
      sourcefile_in_module/2,
      pred_undefined/4,
      pred_def_multiple/4,
      pred_use_over_module/5,
      pred_use_over_module_sum/3,
      portray/1.


:- ddiscontiguous([
      get_metrics_db_relation/2]).


/* flags:
      sum_counter */


/*** Interface ****************************************************/


/* add_pred_def(+SourceFile, +Functor, +Arity) <-
      Add a predicate-definition to the database. */


/* add_pred_use(+UsingSourceFile, +UsingFunctor, +UsingArity,
         +UsedFunctor, +UsedArity) <-
      Add a predicate-usage-definition to the database.
      The predicate defined by UsingFunctor/Arity in SourceFile
      which is part of Module uses the predicate defined by
      UsedFunctor/Arity. */


/* save_metrics_db <-
      Save the metrics-database in a file and get the FileName
      from the module defs. */


/* save_metrics_db(+FileName) <-
      Save the metrics-database in a file called FileName. */


/* load_metrics_db <-
      Load the metrics-database from a file and get the FileName
      from the module defs. */


/* load_metrics_db(+FileName) <-
      Load the metrics-database from a file called FileName. */


/* analyse_metrics_db <-
      Analyse the data about the sourcefiles to get
      the software-metrics-information. */


/* save_metrics_db_analysis <-
      Save the analysis of the metrics-database in a file and
      get the FileName from the module defs. */


/* save_metrics_db_analysis(+FileName) <-
      Save the analysis of the metrics-database in a file
      called FileName. */


/* load_metrics_db_analysis <-
      Load the analysis of the metrics-database from a file and
      get the FileName from the module defs. */


/* load_metrics_db_analysis(+FileName) <-
      Load the analysis of the metrics-database from a file
      called FileName. */


/* init_metrics_db <-
      Empty the software-metrics-database and initialise it with
      some easy-to-calculate date. */


/* clear_metrics_db <-
      Empty the software-metrics-database. */


/* portray_metrics_db_interface(+Module) <-
      Output the interface of module Module in a nice way to a
      text-file. The interface are all predicates, that are used
      by other modules. */


/* get_metrics_db_relation(+Relation, -Result) <-
      Get one of the relations in the metrics-database in the form
      of a list. Relation can be one of the atoms/terms:
        sourcefile_in_module
        pred_def
        pred_use
        pred_undefined
        pred_def_multiple
        pred_use_over_module
        pred_use_over_module_sum
        dynamic_pred
          (from DDA config, format:
              [SourceFile, Module, Functor/Arity])
        sourcefile
          (only a sorted list of all sourcefiles)
        module
          (only a sorted list of all modules)
        module_interface(+Module)
          (list of predicates, that are used by other modules,
           format: [Funcor/Arity, NumberOfUses])
      Result is a list with the content of the respective fact
      collection.
      Each fact is represented by a list of its arguments.
      Unlike in the the facts each functor-arity-combination is
      a term like functor/arity and not two separate elements.
      See also description of the respective fact. */


/*** Implementation ***********************************************/


/* sourcefile_in_module(?SourceFile, ?Module) <-
      This predicate is a fact database. Each fact defines that
      SourceFile is part of Module. */


/* pred_def(?SourceFile, ?Module, ?Functor, ?Arity) <-
      This predicate is a fact database.
      Each fact defines that a predicate with Functor and Arity
      is defined in SourceFile, which is a part of Module. */


/* pred_use(?UsingSourceFile, ?UsingModule, ?UsingFunctor,
         ?UsingArity, ?UsedFunctor, ?UsedArity, ?NumberOfUses) <-
      This predicate is a fact database.
      Each fact defines that a predicate with UsingFunctor and
      UsingArity which resides in SourceFile which is part of
      Module is using a predicate with UsedFunctor and UsedArity.
      It is used NumberOfUses times. */


/* pred_undefined(?UsingSourceFile, ?UsingModule,
         ?UsedFunctor, ?UsedArity) <-
      This predicate is a fact database.
      Each fact defines that a predicate with UsedFunctor and
      UsedArity is used in UsingSourceFile which is part
      of UsingModule. But no definition is found anywhere. */


/* pred_def_multiple(?SourceFiles, ?Modules, ?Functor, ?Arity) <-
      This predicate is a fact database.
      Each fact defines that a predicate with Functor and Arity
      is defined in all of the list SourceFiles which are part
      of Modules. */


/* pred_use_over_module(?UsingModule,
         ?UsedModules, ?UsedFunctor, ?UsedArity, ?NumberOfUses) <-
      This predicate is a fact database.
      Each fact defines that a predicate with UsedFunctor and
      UsedArity is used in UsingModule and defined in UsedModules
      (which is a list). It is used NumberOfUses times. */


/* pred_use_over_module_sum(
         ?UsingModule, ?UsedModule, ?NumberOfUses) <-
      This predicate is a fact database.
      Each fact defines that UsingModule used a (any) predicate
      of UsedModule NumberOf Uses times. */


/* module_interface_pred(
         ?Module, ?Functor, ?Arity, ?NumberOfUses) <-
      This predicate is a fact database.
      Each fact defines that a predicate with Functor and Arity
      is a interface-predicate of Module.
      That is it is used from another module.
      It is used NumberOfUses times. */


/* clear_metrics_db/0: Description see interface. */

clear_metrics_db :-
   clear_metrics_db_analysis,
   retractall(pred_def(_, _, _, _)),
   retractall(pred_use(_, _, _, _, _, _, _)),
   retractall(sourcefile_in_module(_, _)),
   index(pred_def(1, 1, 1, 1)),
   index(pred_use(1, 1, 1, 1, 1, 1, 0)),
   index(sourcefile_in_module(1, 1)).


/* clear_metrics_db_analysis <-
      Empty the analysis of the software-metrics-database. */

clear_metrics_db_analysis :-
   retractall(pred_undefined(_, _, _, _)),
   retractall(pred_def_multiple(_, _, _, _)),
   retractall(pred_use_over_module(_, _, _, _, _)),
   retractall(pred_use_over_module_sum(_, _, _)),
   retractall(module_interface_pred(_, _, _, _)),
   index(pred_undefined(1, 1, 1, 1)),
   index(pred_def_multiple(1, 1, 1, 1)),
   index(pred_use_over_module(1, 1, 1, 1, 0)),
   index(pred_use_over_module_sum(1, 1, 0)),
   index(module_interface_pred(1, 1, 1, 0)).


/* init_metrics_db/0: Description see interface. */

init_metrics_db :-
   clear_metrics_db,
   add_module(dislog_module(Module, SourceFileList)),
   add_module(dislog_module2(Module, SourceFileList)),
   sourcedirs(Dirs),
   sourcefiles(Dirs, Files),
   check_sourcefiles_for_module(Files),
   add_dynamic_pred.


/* add_module(+ModuleDef) <-
      Add the module <-> sourcefile definitions for one module
      to the database and continue automatically with the next.
      ModuleDef must be of the form:
          dislog_module(Module, SourceFileList)
          dislog_module2(Module, SourceFileList) */

add_module(ModuleDef) :-
   unknown(_Old1, fail),
   call(ModuleDef),
   unknown(_Old2, trace),
   add_sourcefile_in_module(ModuleDef),
   fail.
add_module(_ModuleDef).


/* add_sourcefile_in_module(+ModuleDef) <-
      Add the module <-> sourcefile definitions for one module
      to the database. ModuleDef must be of the form:
          dislog_module(_Module, _SourceFileList)
          dislog_module2(_Module, _SourceFileList) */

add_sourcefile_in_module(
      dislog_module(Module, [SourceFile | SourceFiles])) :-
   !,
   concat('sources/', SourceFile, SourceFile2),
   absolute_file_name(SourceFile2, SourceFile3),
   add_predicate(sourcefile_in_module(SourceFile3, Module)),
   add_sourcefile_in_module(dislog_module(Module, SourceFiles)).
add_sourcefile_in_module(dislog_module(_Module, [])).
add_sourcefile_in_module(dislog_module2(Module, SourceFiles)) :-
   !,
   add_sourcefile_in_module(dislog_module(Module, SourceFiles)).


/* check_sourcefiles_for_module(+Files) <-
      Check the sourcefiles in the list Files, if they are in the
      database sourcefile_in_module. For every file that is not,
      an entry is made. */

check_sourcefiles_for_module([File | Files]) :-
   sourcefile_in_module(File, _Module),
   !,
   check_sourcefiles_for_module(Files).
check_sourcefiles_for_module([File | Files]) :-
   dont_create_info(File),
   !,
   check_sourcefiles_for_module(Files).
check_sourcefiles_for_module([File | Files]) :-
   extra_sourcefile_in_module(File, Module),
   !,
   add_predicate(sourcefile_in_module(File, Module)),
   check_sourcefiles_for_module(Files).
check_sourcefiles_for_module([File | Files]) :-
   !,
   unknown(Module),
   add_predicate(sourcefile_in_module(File, Module)),
   check_sourcefiles_for_module(Files).
check_sourcefiles_for_module([]).


/* add_dynamic_pred <-
      Add the pred_def-facts for the dynamically defined
      predicates. */

add_dynamic_pred :-
   dynamic_pred(SourceFile1, Functor, Arity),
   absolute_file_name(SourceFile1, SourceFile2),
   sourcefile_in_module(SourceFile2, Module),
   add_predicate(pred_def(SourceFile2, Module, Functor, Arity)),
   fail.
add_dynamic_pred.


/* add_predicate(+Predicate) <-
      Add the fact Predicate to the database.
      That is assertz it if it was not yet defined. */

add_predicate(Predicate) :-
   unknown(OldUnknown, fail),
   \+ call(Predicate),
   unknown(_Old, OldUnknown),
   assertz(Predicate).


/* add_predicate_counter(+Predicate) <-
      Add the fact Predicate to the database. That is assertz it.
      This is for predicates with counters.
      The Counter has to be the last argument of the predicate.
      It will be increased by the value of the last argument
      in Predicate.
      If the fact wasn't yet defined NumberToAdd will be the
      counter-value (like adding to 0). */

add_predicate_counter(Predicate) :-
   functor(Predicate, _Functor, Arity),
   arg(Arity, Predicate, NumberToAdd),
   setarg(Arity, Predicate, OldNumber),
   add_predicate_counter_test(Predicate),
   NewNumber is OldNumber + NumberToAdd,
   setarg(Arity, Predicate, NewNumber),
   assertz(Predicate).

add_predicate_counter_test(Predicate) :-
   unknown(OldUnknown, fail),
   call(Predicate),
   unknown(_Old, OldUnknown),
   retract(Predicate).
add_predicate_counter_test(Predicate) :-
   functor(Predicate, _Functor, Arity),
   setarg(Arity, Predicate, 0).


/* add_pred_def/3: Description see interface. */

add_pred_def(SourceFile, Functor, Arity) :-
   \+ ignore_pred(Functor, Arity),
   sourcefile_in_module(SourceFile, Module),
   add_predicate(pred_def(SourceFile, Module, Functor, Arity)).
add_pred_def(_SourceFile, _Functor, _Arity).


/* add_pred_use/4: Description see interface. */

add_pred_use(SourceFile, UsingFunctor, UsingArity,
      UsedFunctor, UsedArity) :-
   \+ ignore_pred(UsingFunctor, UsingArity),
   \+ ignore_pred(UsedFunctor, UsedArity),
   sourcefile_in_module(SourceFile, Module),
   add_predicate_counter(pred_use(
      SourceFile, Module, UsingFunctor, UsingArity,
      UsedFunctor, UsedArity, 1)).
add_pred_use(_SourceFile, _UsingFunctor, _UsingArity,
      _UsedFunctor, _UsedArity).


/* save_metrics_db/0: Description see interface. */

save_metrics_db :-
   metrics_db_file(MetricsFile),
   save_metrics_db(MetricsFile).


/* save_metrics_db/1: Description see interface. */

save_metrics_db(FileName) :-
   writef('Saving DisLog software-metrics-database ...\n'),
   open_stream(FileName, write, metricsfile_stream,
      [type(text), eof_action(eof_code)]),
   stream_writef(
      metricsfile_stream, '/* Automatically generated file. */\n'),
   stream_writef(metricsfile_stream, '/* Do NOT edit. */\n\n'),
   stream_writef(metricsfile_stream, '/*\n   Number of facts:\n'),
   count_goal(sourcefile_in_module(SourceFile, Module), Number1),
   stream_writef(metricsfile_stream,
      '     sourcefile_in_module : %w\n', [Number1]),
   count_goal(pred_def(
      SourceFile, Module, Functor, Aritiy), Number2),
   stream_writef(metricsfile_stream,
      '     pred_def             : %w\n', [Number2]),
   count_goal(pred_use(
      _UsingSourceFile, _UsingModule, UsingFunctor, UsingArity,
      UsedFunctor, UsedArity, NumberOfUses), Number3),
   stream_writef(metricsfile_stream,
      '     pred_use             : %w\n*/\n\n', [Number3]),
   write_predicate(sourcefile_in_module(SourceFile, Module)),
   nl(metricsfile_stream),
   write_predicate(pred_def(
      SourceFile, Module, Functor, Aritiy)),
   nl(metricsfile_stream),
   write_predicate(pred_use(
      SourceFile, Module, UsingFunctor, UsingArity,
      UsedFunctor, UsedArity, NumberOfUses)),
   nl(metricsfile_stream),
   close(metricsfile_stream).


/* save_metrics_db_analysis/0: Description see interface. */

save_metrics_db_analysis :-
   metrics_db_analysis_file(MetricsFile),
   save_metrics_db_analysis(MetricsFile).


/* save_metrics_db_analysis/1: Description see interface. */

save_metrics_db_analysis(FileName) :-
   writef('Saving DisLog software-metrics-analysis ...\n'),
   open_stream(FileName, write, metricsfile_stream,
      [type(text), eof_action(eof_code)]),
   stream_writef(metricsfile_stream,
      '/* Automatically generated file. */\n'),
   stream_writef(metricsfile_stream, '/* Do NOT edit. */\n\n'),
   stream_writef(metricsfile_stream, '/*\n   Number of facts:\n'),
   count_goal(pred_undefined(
      UsingSourceFile, UsingModule, UsedFunctor, UsedArity),
      Number1),
   stream_writef(metricsfile_stream,
      '     pred_undefined           : %w\n', [Number1]),
   count_goal(pred_def_multiple(
      SourceFiles, Modules, Functor, Arity), Number2),
   stream_writef(metricsfile_stream,
      '     pred_def_multiple        : %w\n', [Number2]),
   count_goal(pred_use_over_module(
      UsingModule, UsedModules, UsedFunctor, UsedArity,
      NumberOfUses), Number3),
   stream_writef(metricsfile_stream,
      '     pred_use_over_module     : %w\n', [Number3]),
   count_goal(pred_use_over_module_sum(
      UsingModule, UsedModule, NumberOfUses), Number4),
   stream_writef(metricsfile_stream,
      '     pred_use_over_module_sum : %w\n*/\n\n', [Number4]),
   write_predicate(pred_undefined(
      UsingSourceFile, UsingModule, UsedFunctor, UsedArity)),
   nl(metricsfile_stream),
   write_predicate(pred_def_multiple(
      SourceFiles, Modules, Functor, Arity)),
   nl(metricsfile_stream),
   write_predicate(pred_use_over_module(
      UsingModule, UsedModules, UsedFunctor, UsedArity,
      NumberOfUses)),
   nl(metricsfile_stream),
   write_predicate(pred_use_over_module_sum(
      UsingModule, UsedModule, NumberOfUses)),
   nl(metricsfile_stream),
   close(metricsfile_stream).


/* write_predicate(+Predicate) <-
   Write one predicate of the metrics-database for the save-file.
        (All facts of the predicate). Predicate must be of the form:
            functor(_Arg1, _Arg2, ...) */

write_predicate(Predicate) :-
   call(Predicate),
   stream_writef(metricsfile_stream, '%q.\n', [Predicate]),
   fail.
write_predicate(_Predicate).


/* load_metrics_db/0: Description see interface. */

load_metrics_db :-
   metrics_db_file(MetricsFile),
   load_metrics_db(MetricsFile).


/* load_metrics_db/1: Description see interface. */

load_metrics_db(FileName) :-
   writef('Consulting DisLog software-metrics-database ...\n'),
   clear_metrics_db,
%  load_files/2 is redefined in module compile_dislog
%  system:load_files(FileName, silent(true)).
   consult(FileName).


/* load_metrics_db_analysis/0: Description see interface. */

load_metrics_db_analysis :-
   metrics_db_analysis_file(MetricsFile),
   load_metrics_db_analysis(MetricsFile).


/* load_metrics_db_analysis/1: Description see interface. */

load_metrics_db_analysis(FileName) :-
   writef('Consulting DisLog software-metrics-analysis ...\n'),
   clear_metrics_db_analysis,
   % load_files/2 is redefined in module compile_dislog
   %system:load_files(FileName, silent(true)).
   consult(FileName).


/* analyse_metrics_db/1: Description see interface. */

analyse_metrics_db :-
   writef('Analysing DisLog software-metrics ...\n'),
   clear_metrics_db_analysis,
   scan_pred_defs,
   scan_pred_uses,
   scan_pred_use_over_modules.


/* scan_pred_defs <-
        Go through the definitions of the predicate pred_def with a
        failure-driven loop to analyse them. */

scan_pred_defs :-
   pred_def(SourceFile, Module, Functor, Arity),
   analyse_pred_def(SourceFile, Module, Functor, Arity),
   fail.
scan_pred_defs.


/* scan_pred_uses <-
        Go through the definitions of the predicate pred_use with a
        failure-driven loop to analyse them. */

scan_pred_uses :-
   pred_use(SourceFile, Module, UsingFunctor, UsingArity,
            UsedFunctor, UsedArity, NumberOfUses),
   analyse_pred_use(SourceFile, Module, UsingFunctor, UsingArity,
                    UsedFunctor, UsedArity, NumberOfUses),
   fail.
scan_pred_uses.


/* scan_pred_use_over_modules <-
        Go through the definitions of the predicate pred_use_over_module
        with a failure-driven loop to analyse them. */

scan_pred_use_over_modules :-
   pred_use_over_module(UsingModule, UsedModules,
                        UsedFunctor, UsedArity, NumberOfUses),
   analyse_pred_use_over_module(UsingModule, UsedModules,
                                UsedFunctor, UsedArity, NumberOfUses),
   fail.
scan_pred_use_over_modules.


/* analyse_pred_def(+SourceFile, +Module, +Functor, +Arity) <-
        Collect the software-metrics-relevant data in one fact of
        pred_def. The arguments are as in pred_def. */

analyse_pred_def(_SourceFile, _Module, Functor, Arity) :-
   findall( SourceFile2,
      pred_def(SourceFile2, _Module2, Functor, Arity),
      SourceFiles2 ),
   sort(SourceFiles2, SourceFiles),
   findall( Module3,
      pred_def(_SourceFile3, Module3, Functor, Arity),
      Modules3 ),
   sort(Modules3, Modules),
   length(SourceFiles, Length),
   Length > 1,
   add_predicate(pred_def_multiple(
      SourceFiles, Modules, Functor, Arity)),
   !.
analyse_pred_def(_SourceFile, _Module, _Functor, _Arity).


/* analyse_pred_use(+UsingSourceFile, +UsingModule,
         +UsingFunctor, +UsingArity, +UsedFunctor, +UsedArity,
         +NumberOfUses) <-
      Collect the software-metrics-relevant data in one fact of
      pred_use. The arguments are as in pred_use. */

analyse_pred_use(UsingSourceFile, UsingModule,
       _UsingFunctor, _UsingArity,
       UsedFunctor, UsedArity, NumberOfUses) :-
   findall( UsedModule,
      pred_def(_UsedSourceFile, UsedModule, UsedFunctor, UsedArity),
      UsedModules2 ),
   sort(UsedModules2, UsedModules),
   add_pred_undefined_if(
      UsingSourceFile, UsingModule, UsedFunctor, UsedArity),
   add_pred_use_over_module_if(UsingModule,
      UsedModules, UsedFunctor, UsedArity, NumberOfUses),
   !.
analyse_pred_use(
      _UsingSourceFile, _UsingModule, _UsingFunctor, _UsingArity,
      _UsedFunctor, _UsedArity, _NumberOfUses).


/* analyse_pred_use_over_module(+UsingModule, +UsedModules,
         +UsedFunctor, +UsedArity, +NumberOfUses) <-
      Collect the software-metrics-relevant data in one fact of
      pred_use_over_module.
      The arguments are as in pred_use_over_module. */

analyse_pred_use_over_module(UsingModule, UsedModules,
      _UsedFunctor, _UsedArity, NumberOfUses) :-
   add_pred_use_over_module_sum(
      UsingModule, UsedModules, NumberOfUses),
   !.
analyse_pred_use_over_module(_UsingModule, _UsedModules,
      _UsedFunctor, _UsedArity, _NumberOfUses).


/* add_pred_undefined_if(
         +UsingSourceFile, +UsingModule, +UsedFunctor, +UsedArity) <-
      Test if the predicate with UsedFunctor and UsedArity is defined.
      If not a pred_undefined-fact will be added. */

add_pred_undefined_if(
      UsingSourceFile, UsingModule, UsedFunctor, UsedArity) :-
   \+ pred_def(_UsedSourceFile, _UsedModule, UsedFunctor, UsedArity),
   add_predicate(pred_undefined(
      UsingSourceFile, UsingModule, UsedFunctor, UsedArity)),
   !.
add_pred_undefined_if(
      _UsingSourceFile, _UsingModule, _UsedFunctor, _UsedArity).


/* add_pred_use_over_module_if(+UsingModule, +UsedModules,
         +UsedFunctor, +UsedArity, +NumberOfUses) <-
      A definition of the predicate UsedFunctor/UsedArity is in all
      of the modules in the list UsedModules. Test if one of them
      is UsingModule. If not a pred_use_over_module-fact is added.
      UsedModules must not be empty (otherwise it is recorded under
      pred_undefined). */

add_pred_use_over_module_if(UsingModule,
      [FirstUsedModule | UsedModules],
      UsedFunctor, UsedArity, NumberOfUses) :-
   \+ member(UsingModule, [FirstUsedModule | UsedModules]),
   add_predicate_counter(pred_use_over_module(
      UsingModule, [FirstUsedModule | UsedModules],
      UsedFunctor, UsedArity, NumberOfUses)),
   !.
add_pred_use_over_module_if(_UsingModule, _UsedModules,
      _UsedFunctor, _UsedArity, _NumberOfUses).


/* add_pred_use_over_module_sum(
         +UsingModule, +UsedModules, +NumberOfUses) <-
      A fact of pred_use_over_module_sum is added to the database
      for each module in the list UsedModules. */

add_pred_use_over_module_sum(UsingModule,
      [UsedModule | UsedModules], NumberOfUses) :-
   add_predicate_counter(pred_use_over_module_sum(
      UsingModule, UsedModule, NumberOfUses)),
   add_pred_use_over_module_sum(
      UsingModule, UsedModules, NumberOfUses),
   !.
add_pred_use_over_module_sum(_UsingModule, [], _NumberOfUses).


/* create_interface(+Module) <-
      Create the relation module_interface_pred/4 for Module. */

create_interface(Module) :-
   retractall(module_interface_pred(_, _, _, _)),
   index(module_interface_pred(1, 1, 1, 0)),
   create_interface_loop(Module).

create_interface_loop(Module) :-
   pred_use_over_module(
      _UsingModule, UsedModules, Functor, Arity, Number),
   create_interface_add_if(
      Module, UsedModules, Functor, Arity, Number),
   fail.
create_interface_loop(_Module).
   
create_interface_add_if(Module, UsedModules, Functor, Arity, Number) :-
   !,
   member(Module, UsedModules),
   add_predicate_counter(module_interface_pred(
      Module, Functor, Arity, Number)),
   !.


/* portray_metrics_interface/1: Description see interface. */

portray_metrics_db_interface(Module) :-
   create_interface(Module),
   tmp_text_file(TmpFile),
   portray_pred(module_interface_pred(_, _, _, _), predicate),
   rename_file(TmpFile, 'results/module_interface.txt').


/* portray_metrics_db <-
      Output the data from the analysis of the software-metrics
      in a nice way. */

portray_metrics_db :-
   writef('Portray DisLog software-metrics to files ...\n'),
   tmp_text_file(TmpFile),
   portray_pred(pred_undefined(_, _, _, _), module),
   rename_file(TmpFile, 'results/pred_undefined.module.txt'),
   portray_pred(pred_def_multiple(_, _, _, _), module),
   rename_file(TmpFile, 'results/pred_def_multiple.module.txt'),
   portray_pred(pred_def_multiple(_, _, _, _), sourcefile),
   rename_file(TmpFile, 'results/pred_def_multiple.sourcefile.txt'),
   portray_pred(pred_def_multiple(_, _, _, _), predicate),
   rename_file(TmpFile, 'results/pred_def_multiple.predicate.txt'),
   portray_pred(dynamic_pred(_, _, _), module),
   rename_file(TmpFile, 'results/dynamic_pred.module.txt'),
   portray_pred(dynamic_pred(_, _, _), predicate),
   rename_file(TmpFile, 'results/dynamic_pred.predicate.txt'),
   portray_pred(pred_use_over_module_sum(_, _, _), using_module),
   rename_file(TmpFile, 'results/pred_use_over_module_sum.using_module.txt'),
   portray_pred(pred_use_over_module_sum(_, _, _), used_module),
   rename_file(TmpFile, 'results/pred_use_over_module_sum.used_module.txt').


/* portray_pred(+Predicate) <-
      Output all Predicate-facts in a nice way. */

/* portray_pred(+Predicate, +SortKey) <-
      Output all Predicate-facts in a nice way.
      SortKey see portray. */

portray_pred(Pred) :-
   telling(OldOutput),
   tmp_text_file(File),
   tell(File),
   portray_pred_loop(Pred),
   told,
   append(OldOutput),
   sort_file(File).

portray_pred_loop(Pred) :-
   call(Pred),
   portray(Pred),
   fail.
portray_pred_loop(_Pred).

portray_pred(Pred, SortKey) :-
   telling(OldOutput),
   tmp_text_file(File),
   tell(File),
   portray_pred_loop(Pred, SortKey),
   told,
   append(OldOutput),
   sort_file(File).

portray_pred_loop(Pred, SortKey) :-
   call(Pred),
   portray(Pred, SortKey),
   fail.
portray_pred_loop(_Pred, _SortKey).


/* portray(+Predicate) <-
      Output Predicate in a nice way.
      (portray/1 is a dynamic system-predicate.) */

portray(pred_undefined(AbsFile, Module, Functor, Arity)) :-
   portray(pred_undefined(
      AbsFile, Module, Functor, Arity), module).
portray(pred_def_multiple(AbsFiles, Modules, Functor, Arity)) :-
   portray(pred_def_multiple(
      AbsFiles, Modules, Functor, Arity), predicate).
portray(dynamic_pred(File, Functor, Arity)) :-
   portray(dynamic_pred(
      File, Functor, Arity), module).
portray(pred_use_over_module_sum(UsingModule, UsedModule, NumberOfUses)) :-
   portray(pred_use_over_module_sum(
      UsingModule, UsedModule, NumberOfUses), used_module).
portray(module_interface_pred(Module, Functor, Arity, NumberOfUses)) :-
   portray(module_interface_pred(
      Module, Functor, Arity, NumberOfUses), predicate).


/* portray(+Predicate, +SortKey) <-
      Output Predicate in a nice way.
      SortKey defines which column should be the first.
      This implies that a table made from those lines will
      be sorted after that. */

portray(pred_undefined(AbsFile, Module, Functor, Arity), module) :-
   !,
   de_absolute_file_name(AbsFile, File),
   atom_length(Module, ModuleLen),
   column_width(module, ModuleWidth),
   write(Module),
   tab(ModuleWidth - ModuleLen),
   atom_length(File, FileLen),
   column_width(sourcefile, FileWidth),
   write(File),
   tab(FileWidth - FileLen),
   writef('%w/%w\n', [Functor, Arity]).

portray(pred_def_multiple(
      [AbsFile | AbsFiles], Modules, Functor, Arity), sourcefile) :-
   !,
   de_absolute_file_name(AbsFile, File),
   atom_length(File, FileLen),
   column_width(sourcefile, FileWidth),
   write(File),
   tab(FileWidth - FileLen),
   writef('%w/%w\n', [Functor, Arity]),
   portray(pred_def_multiple(
      AbsFiles, Modules, Functor, Arity), sourcefile).
portray(pred_def_multiple(
      [AbsFile | AbsFiles], Modules, Functor, Arity), module) :-
   !,
   sourcefile_in_module(AbsFile, Module),
   atom_length(Module, ModuleLen),
   column_width(module, ModuleWidth),
   write(Module),
   tab(ModuleWidth - ModuleLen),
   de_absolute_file_name(AbsFile, File),
   atom_length(File, FileLen),
   column_width(sourcefile, FileWidth),
   write(File),
   tab(FileWidth - FileLen),
   writef('%w/%w\n', [Functor, Arity]),
   portray(pred_def_multiple(
      AbsFiles, Modules, Functor, Arity), module).
portray(pred_def_multiple(
      [AbsFile | AbsFiles], Modules, Functor, Arity), predicate) :-
   !,
   atom_length(Functor, FunctorLen),
   atom_length(Arity, ArityLen),
   PredLen is FunctorLen + 1 + ArityLen,
   column_width(predicate, PredWidth),
   writef('%w/%w', [Functor, Arity]),
   tab(PredWidth - PredLen),
   sourcefile_in_module(AbsFile, Module),
   atom_length(Module, ModuleLen),
   column_width(module, ModuleWidth),
   write(Module),
   tab(ModuleWidth - ModuleLen),
   de_absolute_file_name(AbsFile, File),
   write(File), nl,
   portray(pred_def_multiple(
      AbsFiles, Modules, Functor, Arity), predicate).
portray(pred_def_multiple([], _Modules, _Functor, _Arity), _SortKey).

portray(dynamic_pred(File, Functor, Arity), module) :-
   !,
   absolute_file_name(File, AbsFile),
   de_absolute_file_name(AbsFile, DeAbsFile),
   sourcefile_in_module(AbsFile, Module),
   atom_length(Module, ModuleLen),
   column_width(module, ModuleWidth),
   write(Module),
   tab(ModuleWidth - ModuleLen),
   atom_length(DeAbsFile, FileLen),
   column_width(sourcefile, FileWidth),
   write(DeAbsFile),
   tab(FileWidth - FileLen),
   writef('%w/%w\n', [Functor, Arity]).
portray(dynamic_pred(File, Functor, Arity), predicate) :-
   !,
   atom_length(Functor, FunctorLen),
   atom_length(Arity, ArityLen),
   PredLen is FunctorLen + 1 + ArityLen,
   column_width(predicate, PredWidth),
   writef('%w/%w', [Functor, Arity]),
   tab(PredWidth - PredLen),
   absolute_file_name(File, AbsFile),
   sourcefile_in_module(AbsFile, Module),
   atom_length(Module, ModuleLen),
   column_width(module, ModuleWidth),
   write(Module),
   tab(ModuleWidth - ModuleLen),
   de_absolute_file_name(AbsFile, DeAbsFile),
   write(DeAbsFile), nl.
portray(pred_use_over_module_sum(
      UsingModule, UsedModule, NumberOfUses), using_module) :-
   !,
   atom_length(UsingModule, UsingModuleLen),
   column_width(module, UsingModuleWidth),
   write(UsingModule),
   tab(UsingModuleWidth - UsingModuleLen),
   atom_length(UsedModule, UsedModuleLen),
   column_width(module, UsedModuleWidth),
   write(UsedModule),
   tab(UsedModuleWidth - UsedModuleLen),
   writef('%w\n', [NumberOfUses]).
portray(pred_use_over_module_sum(
      UsingModule, UsedModule, NumberOfUses), used_module) :-
   !,
   atom_length(UsedModule, UsedModuleLen),
   column_width(module, UsedModuleWidth),
   write(UsedModule),
   tab(UsedModuleWidth - UsedModuleLen),
   atom_length(UsingModule, UsingModuleLen),
   column_width(module, UsingModuleWidth),
   write(UsingModule),
   tab(UsingModuleWidth - UsingModuleLen),
   writef('%w\n', [NumberOfUses]).
portray(module_interface_pred(
      _Module, Functor, Arity, NumberOfUses), predicate) :-
   !,
   atom_length(Functor, FunctorLen),
   atom_length(Arity, ArityLen),
   PredLen is FunctorLen + 1 + ArityLen,
   column_width(predicate, PredWidth),
   writef('%w/%w', [Functor, Arity]),
   tab(PredWidth - PredLen),
   writef('%w\n', [NumberOfUses]).


/* column_width(+ColumnType, ?Width) <-
      Given the ColumnType (or content), Width is unified with the
      width in characters for that column. */

column_width(module, 35).
column_width(sourcefile, 34).
column_width(predicate, 34).


/* get_metrics_db_relation/2: Description see interface. */

get_metrics_db_relation(sourcefile_in_module, Result) :-
   findall( Tuple,
      gmdbr_sourcefile_in_module(Tuple),
      Result ).
gmdbr_sourcefile_in_module([SourceFile, Module]) :-
   sourcefile_in_module(SourceFile, Module).

get_metrics_db_relation(pred_def, Result) :-
   findall( Tuple,
      gmdbr_pred_def(Tuple),
      Result ).
gmdbr_pred_def([SourceFile, Module, Functor/Arity]) :-
   pred_def(SourceFile, Module, Functor, Arity).

get_metrics_db_relation(pred_use, Result) :-
   findall( Tuple,
      gmdbr_pred_use(Tuple),
      Result ).
gmdbr_pred_use([
      UsingSourceFile, UsingModule, UsingFunctor/UsingArity,
      UsedFunctor/UsedArity, NumberOfUses]) :-
   pred_use(UsingSourceFile, UsingModule, UsingFunctor, UsingArity,
      UsedFunctor, UsedArity, NumberOfUses).

get_metrics_db_relation(pred_undefined, Result) :-
   findall( Tuple,
      gmdbr_pred_undefined(Tuple),
      Result ).
gmdbr_pred_undefined([UsingSourceFile, UsingModule, UsedFunctor/UsedArity]) :-
   pred_undefined(UsingSourceFile, UsingModule, UsedFunctor, UsedArity).

get_metrics_db_relation(pred_def_multiple, Result) :-
   findall( Tuple,
      gmdbr_pred_def_multiple(Tuple),
      Result ).
gmdbr_pred_def_multiple([SourceFiles, Modules, Functor/Arity]) :-
   pred_def_multiple(SourceFiles, Modules, Functor, Arity).

get_metrics_db_relation(pred_use_over_module, Result) :-
   findall( Tuple,
      gmdbr_pred_use_over_module(Tuple),
      Result ).
gmdbr_pred_use_over_module([
      UsingModule, UsedModules, UsedFunctor/UsedArity,
      NumberOfUses]) :-
   pred_use_over_module(
      UsingModule, UsedModules, UsedFunctor, UsedArity,
      NumberOfUses).

get_metrics_db_relation(pred_use_over_module_sum, Result) :-
   findall( Tuple,
      gmdbr_pred_use_over_module_sum(Tuple),
      Result ).
gmdbr_pred_use_over_module_sum([UsingModule, UsedModule, NumberOfUses]) :-
   pred_use_over_module_sum(UsingModule, UsedModule, NumberOfUses).

get_metrics_db_relation(dynamic_pred, Result) :-
   findall(Tuple,
      gmdbr_dynamic_pred(Tuple),
      Result ).
gmdbr_dynamic_pred([SourceFile2, Module, Functor/Arity]) :-
   dynamic_pred(SourceFile1, Functor, Arity),
   absolute_file_name(SourceFile1, SourceFile2),
   sourcefile_in_module(SourceFile2, Module).

get_metrics_db_relation(sourcefile, Result) :-
   findall( SourceFile,
      sourcefile_in_module(SourceFile, _Module),
      SourceFiles ),
   sort(SourceFiles, Result).

get_metrics_db_relation(module, Result) :-
   findall( Module,
      sourcefile_in_module(_SourceFile, Module),
      Modules ),
   sort(Modules, Result).

get_metrics_db_relation(module_interface(Module), Result) :-
   create_interface(Module),
   findall( Tuple,
      gmdbr_module_interface(Module, Tuple),
      Result ).
gmdbr_module_interface(Module, [Functor/Arity, Number]) :-
   module_interface_pred(Module, Functor, Arity, Number).



/*** Initialization ***********************************************/


% :- clear_metrics_db.


/*** Testing / Debugging ******************************************/


sum_pred_use :-
   flag(sum_counter, _Old, 0),
   pred_use(_,_,_,_,_,_, Number),
   flag(sum_counter, OldSum, OldSum + Number),
   fail.
sum_pred_use :-
   flag(sum_counter, Sum, Sum),
   writef('Sum of pred_use: %w', [Sum]).


sum_pred_use_over_module :-
   flag(sum_counter, _Old, 0),
   pred_use_over_module(_,_,_,_, Number),
   flag(sum_counter, OldSum, OldSum + Number),
   fail.
sum_pred_use_over_module :-
   flag(sum_counter, Sum, Sum),
   writef('Sum of pred_use_over_module: %w', [Sum]).


sum_pred_use_over_module_sum :-
   flag(sum_counter, _Old, 0),
   pred_use_over_module_sum(_,_, Number),
   flag(sum_counter, OldSum, OldSum + Number),
   fail.
sum_pred_use_over_module_sum :-
   flag(sum_counter, Sum, Sum),
   writef('Sum of pred_use_over_module_sum: %w', [Sum]).


/******************************************************************/


/* Emacs-Configuration: */

%%% Local Variables:
%%% mode: prolog
%%% End:


