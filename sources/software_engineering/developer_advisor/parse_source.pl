

/******************************************************************/
/***                                                            ***/
/***       Program : DisLog Developer Advisor                   ***/
/***    SourceFile : parse_source                               ***/
/***       Version : 0.0.0                                      ***/
/***        Author : Thomas Schilling                           ***/
/***      Language : SWI-Prolog 2.9.5                           ***/
/***       Started : 4. August 1997                             ***/
/***  Last Changed : 5. April 1998                              ***/
/***       Purpose : Create information about a source file     ***/
/***                 of the package using DCGs.                 ***/
/***                                                            ***/
/******************************************************************/


:- module(dda_parse_source,[
      parse_source/1,
      parse_source/2 ]).


:- use_module_dda([
      config,
      create_info,
%     read_source,
      metrics_db ]).


/* flags:
      current_functor
      current_predicate */


/*** Interface ****************************************************/


/* parse_source(TokenList) <-
      Parse the list of tokens in TokenList; the tokens have
      to be strings (not character lists or atoms).
      The predicate allways succeeds. */

parse_source(StringTokenList) :-
   parse_source(StringTokenList, _).

parse_source(StringTokenList, Tree) :-
   maplist( string_to_atom,
      StringTokenList, AtomTokenList ),
   parse_source_update_precedences,
   parse_blocks(AtomTokenList, Tree).
parse_source(_, _).


/*** Implementation ***********************************************/


/* parse_source_update_precedences <-
      */

parse_source_update_precedences :-
   findall( Prec,
      current_op(Prec, _Type, _Name),
      Precs ),
   sort(Precs, Sorted),
   nth1(1, Sorted, Lowest),
   last_element(Sorted, Highest),
   flag(lowest_precedence, _OldLow, Lowest),
   flag(highest_precedence, _OldHigh, Highest).


/* parse_blocks(TokenList, Tree) <-
      Parse the list of tokens in TokenList.
      The tokens have to be atoms.
      Automatically recurses through the blocks in TokenList. */

parse_blocks(TokenList) :-
   parse_blocks(TokenList, _).

parse_blocks(TokenList, Tree) :-
   writeln(parse_blocks(TokenList)),
%  inspect_list(TokenList),
%  writeln(user, dcg_block(Tree, TokenList, Rest)),
   dcg_block(Tree, TokenList, _),
%  writeln(user, dcg_block(Tree, TokenList, Rest)),
   scan_dcg_tree(Tree).

inspect_list(List) :-
   maplist( name,
      List, List_2 ),
   writeln_list(List_2).

/*
parse_blocks([]).
parse_blocks(TokenList) :-
   phrase(dcg_block(Tree), TokenList, Rest),
%  for testing and debugging:
%  append('results/parse_source_test'),
%  nl, writeln(Tree),
%  portray_dcg_tree(Tree),
%  told,
   scan_dcg_tree(Tree),
   parse_blocks(Rest).
*/


/* dcg_block(Tree) -->
      */

dcg_block(Tree) -->
   dcg_block_sub(Tree),
   { writeln_trace(user, dcg_block(Tree)) }.

dcg_block_sub(Tree) -->
   dcg_directive(Tree).
dcg_block_sub(Tree) -->
   dcg_rule(Tree).
dcg_block_sub(Tree) -->
   dcg_fact(Tree).
dcg_block_sub(Tree) -->
   dcg_grammar_rule(Tree).
dcg_block_sub(unknown_by_dcg(Tree)) -->
   dcg_unknown(Tree).


dcg_unknown([Token | Rest]) -->
   [Token],
   { Token \= '.' },
   dcg_unknown(Rest),
   ['.'].
dcg_unknown([Token]) -->
   [Token],
   { Token \= '.' },
   ['.'].


dcg_directive(directive(Term)) -->
   dcg_rule_operator,
   !,
   dcg_rule_body(Term),
   ['.'].

dcg_rule(rule(Head, Body)) -->
   dcg_rule_head(Head),
   dcg_rule_operator,
   { ! },
   dcg_rule_body(Body),
   ['.'].

dcg_rule_operator -->
   [':-'].
dcg_rule_operator -->
   ['?-'].

dcg_fact(fact(Term)) -->
   dcg_rule_head(Term),
   ['.'].

dcg_grammar_rule(grammar_rule(Head, Body)) -->
   dcg_rule_head(Head),
   ['-->'],
   { ! },
   dcg_rule_body(Body),
   ['.'].


dcg_rule_head(Term) -->
   dcg_predicate(Term).
dcg_rule_head(Term) -->
   dcg_op_term(Term).

dcg_rule_body(Term) -->
   dcg_term_list(Term).


dcg_predicate(Predicate) -->
   dcg_functor(Functor),
   dcg_arguments(Functor, Predicate).

dcg_arguments(Functor, predicate(Functor, Arguments)) -->
   ['('],
   { ! },
   dcg_term_list(Arguments),
   [')'],
   { ! },
   { test_for_op(Functor, Arguments) }.
dcg_arguments(Functor, predicate(Functor)) -->
   [].


dcg_term_list(Terms) -->
   dcg_term_list_conj(Ts),
   dcg_term_list_disj_rest(Ts, Terms).

dcg_term_list_conj([T|Ts]) -->
   dcg_op_term(T),
   dcg_term_list_conj_rest(Ts).

dcg_term_list_conj_rest(Terms) -->
   [','],
   dcg_term_list_conj(Terms).
dcg_term_list_conj_rest([]) -->
   [].

dcg_term_list_disj_rest(Ts, if(Ts, Terms)) -->
   ['->'],
   dcg_term_list(Terms).
dcg_term_list_disj_rest(Ts, disjunction(Ts, Terms)) -->
   [';'],
   dcg_term_list(Terms).
dcg_term_list_disj_rest(Ts, Ts) -->
   [].


dcg_op_term(fx_operator(Op, A)) -->
   [Op],
   { is_fx_operator(Op, _) },
   dcg_op_term(A).
dcg_op_term(Term) -->
   dcg_term(TermA),
   dcg_op_term_rest(TermA, Term).

dcg_op_term_rest(TermA, xfx_operator(Op, TermA, TermB)) -->
   [Op],
   { is_xfx_operator(Op, _) },
   dcg_op_term(TermB).
dcg_op_term_rest(TermA, xf_operator(Op, TermA)) -->
   [Op],
   { is_xf_operator(Op, _) }.
dcg_op_term_rest(TermA, TermA) -->
   [].

/*
dcg_op_term(Term) -->
   dcg_term(TermA),
   { flag(highest_precedence, MaxPrec, MaxPrec) },
   dcg_op_term_rest_lower(TermA, Term, MaxPrec).

dcg_op_term_rest(TermA, Term, MaxPrec) -->
   dcg_op_term_rest_lower(TermA, TermB, MaxPrec),
   dcg_op_term_rest(TermB, Term, MaxPrec).
dcg_op_term_rest(TermA, Term, _MaxPrec) -->
   dcg_op_term_rest_higher(TermA, TermB, Prec),
   dcg_op_term_rest(TermB, Term, Prec).

dcg_op_term_rest_lower(TermA,
      xfx_operator(Op, TermA, TermC), MaxPrec) -->
   [Op], { is_xfx_operator(Op, Prec) },
   { Prec < MaxPrec },
   dcg_term(TermB),
   dcg_op_term_rest_lower(TermB, TermC, Prec).
dcg_op_term_rest_lower(Term, Term, _MaxPrec) -->
   [].

dcg_op_term_rest_higher(TermA, Term, Prec) -->
   [Op], { is_xfx_operator(Op, Prec) },
   dcg_term(TermB),
   dcg_op_term_rest(
      xfx_operator(Op, TermA, TermB), Term, Prec).
*/


dcg_term(Term) -->
   ['('],
   { ! },
   dcg_term_list(Term),
   [')'],
   !.
dcg_term(no_grammar('{}')) -->
   ['{'],
   ['}'],
   { ! }.
dcg_term(no_grammar(Term)) -->
   ['{'],
   { ! },
   dcg_term_list(Term),
   ['}'],
   { ! }.
dcg_term(Term) -->
   dcg_list(Term).
dcg_term(Term) -->
   dcg_string(Term).
dcg_term(Term) -->
   dcg_constant(Term).
dcg_term(Term) -->
   dcg_variable(Term).
dcg_term(Term) -->
   dcg_predicate(Term).


dcg_list(list([])) -->
   ['[', ']'],
   { ! }.
dcg_list(List) -->
   ['['],
   dcg_term_list(Head),
   dcg_list_rest(Head, List).

dcg_list_rest(Head, list(Head, Tail)) -->
   ['|'],
   { ! },
   dcg_term(Tail),
   [']'],
   { ! }.
dcg_list_rest(Head, list(Head)) -->
   [']'],
   { ! }.

dcg_functor(Term) -->
   dcg_atom(Term).

dcg_constant(Term) -->
   dcg_number(Term).
dcg_constant(Term) -->
   dcg_atom(Term).

dcg_string(string(String)) -->
   [Token],
   { atom_chars(Token, Cs),
     append(['"'|Cs2], ['"'], Cs),
     atom_chars(String, Cs2),
     ! }.

dcg_variable(variable(Token)) -->
   [Token],
   { is_variable(Token),
     ! }.

dcg_atom(atom(Atom)) -->
   [Token],
   { token_to_atom(Token, Atom),
     ! }.

dcg_number(number(Number)) -->
   [Token_1],
   ['.'],
   [Token_2],
   { atom_chars(Token_1, CharList_1),
     number_chars_save(Number_1, CharList_1),
     atom_chars(Token_2, CharList_2),
     number_chars_save(Number_2, CharList_2),
     name_append([Number_1, '.', Number_2], Number),
     ! }.
dcg_number(number(Number)) -->
   [Token],
   { atom_chars(Token, CharList),
     number_chars_save(Number, CharList),
     ! }.
%  { atom_chars(Token, CharList) },
%  { number_chars_save(Number, CharList) },
%  !.


/******************************************************************/


