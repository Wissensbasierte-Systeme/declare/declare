

/******************************************************************/
/***                                                            ***/
/***           DDA:  Tests                                      ***/
/***                                                            ***/
/******************************************************************/


test(dda, 1) :-
   Lists = ["b", "(", "'1'", ")", "."],
   ( foreach(X, Lists), foreach(Y, Tokenlist) do
        name(Y, X) ),
   writeln(Tokenlist),
   dda_parse_source:dcg_block(Tree, Tokenlist, Rest),
   writeln(Tree),
   writeln(Rest).

test(dda, 2) :-
%  Tokenlist = ['\'1\'', ')', '.'],
   Tokenlist = ['1', ')', '.'],
   dda_parse_source:dcg_op_term(Term, Tokenlist, Rest),
   writeln(dcg_op_term(Term, Rest)).

/*
test(dda, 3) :-
   dislog_variable(dd_advisor_path, DDA_Path),
   concat(DDA_Path, source_for_testing, Path),
   read_source(Path, Tokens, Comments),
   predicate_to_file( 'results/parse_source_test',
      ( nl, dda_read_source:portray_tokens(Tokens),
        nl, dda_read_source:portray_comments(Comments),
        nl, parse_source(Tokens), nl ) ),
   !,
   star_line, parse_source_all(Tokens, Trees),
   star_line, writeln_list(Trees).
*/

test(dda, 4) :-
   Tokens = [
      ":-", "g", ".",
      "a", "(", "X", ")", ":-",
         "b", "(", "X", ")", ",", "c", "(", "1", ")", ".",
      "d", "(", "X", ")", ":-", "e", "(", "X", ")", "." ],
   parse_source_all(Tokens, Trees),
   Trees = [
      directive([atom(g)]),
      rule(
         predicate(atom(a), [atom('X')]), [
            predicate(atom(b), [atom('X')]),
            predicate(atom(c), [number(1)]) ] ),
      rule(
         predicate(atom(d), [atom('X')]), [
            predicate(atom(e), [atom('X')])]) | _ ].


/******************************************************************/


