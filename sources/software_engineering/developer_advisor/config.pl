
/******************************************************************/
/***                                                            ***/
/***       Program : DisLog Developer Advisor                   ***/
/***    SourceFile : config                                     ***/
/***       Version : 0.0.0                                      ***/
/***        Author : Thomas Schilling                           ***/
/***      Language : SWI-Prolog 2.9.5                           ***/
/***       Started : 7. August 1997                             ***/
/***  Last Changed : 4. April 1998                              ***/
/***       Purpose : Some basic configuration-definitions for   ***/
/***                 the help and software engineering system.  ***/
/***                                                            ***/
/******************************************************************/


:- module(dda_config,[
	sourcedirs/1,
	init_help_default/1,
	comments_file/1,
	index_file/1,
	metrics_db_file/1,
	metrics_db_analysis_file/1,
	extra_sourcefile_in_module/2,
	dont_create_info/1,
	op_for_dcg/3,
	ignore_pred/2,
	dynamic_pred/3,
	comments_file_separator/1,
	pager_file/1,
	pager_command/1,
	help_linenumber_interval/1,
	unknown/1,
	tmp_text_file/1,
	heuristic_verbose/1,
	heuristic_default/1]).
        %%% for testing and debugging:
	%test_assert/1


/*** Interface ****************************************************/


/* sourcedirs(?Directories) <-
        Directories is unified with a list of of dirs that contain
        the sourcefiles to examine. */

sourcedirs([
   'sources',
   'library' ]).
% for testing and debugging
% sourcedirs([
%    'sources/dd_advisor' ]).


/* init_help_default(?Check) <-
        Check is unified with the default value for init_help/1.
        Can be check or no_check. */

init_help_default(no_check).
% init_help_default(check).


/* comments_file(?FileName) <-
        FileName is unified with the file that is supposed to hold
        the collected information from the comments in the source
        files. This is for a help function. */

comments_file(File) :-
	absolute_file_name('results/dda_comments.hlp', File).


/* index_file(?FileName) <-
        FileName is unified with the file that is supposed to hold
        the help-index. */

index_file(File) :-
	absolute_file_name('results/dda_comments.idx', File).


/* metrics_db_file(?FileName) <-
        FileName is unified with the file that is supposed to hold
        the metrics-database. */

metrics_db_file(File) :-
	absolute_file_name('results/metrics_db.sav', File).


/* metrics_db_analysis_file(?FileName) <-
        FileName is unified with the file that is supposed to hold
        the analysis of the metrics-database. */

metrics_db_analysis_file(File) :-
	absolute_file_name('results/metrics_db_analysis.sav', File).


/* extra_sourcefile_in_module(?FileName, ?Module) <-
        FileName is unified with a file that is part of Module.
        These are files not covered by dislog_module/2. */

extra_sourcefile_in_module(File, dislog_global) :-
	absolute_file_name('sources/dislog', File).
extra_sourcefile_in_module(File, dislog_global) :-
	absolute_file_name('sources/specials_if_prolog', File).
extra_sourcefile_in_module(File, dislog_global) :-
	absolute_file_name('sources/specials_swi', File).
extra_sourcefile_in_module(File, dislog_global) :-
	absolute_file_name('sources/specials_sicstus', File).
extra_sourcefile_in_module(File, dislog_global) :-
	absolute_file_name('sources/cdislog', File).
extra_sourcefile_in_module(File, dislog_global) :-
	absolute_file_name('sources/mdislog', File).
extra_sourcefile_in_module(File, dislog_global) :-
	absolute_file_name('sources/sdislog', File).

extra_sourcefile_in_module(File, graphs) :-
	absolute_file_name('sources/hypergraphs', File).
extra_sourcefile_in_module(File, graphs) :-
	absolute_file_name('sources/hypergraphs_rehang', File).

extra_sourcefile_in_module(File, interfaces) :-
	absolute_file_name('sources/demo_dislog', File).
extra_sourcefile_in_module(File, interfaces) :-
	absolute_file_name('sources/tcl_tk_interface', File).
extra_sourcefile_in_module(File, interfaces) :-
	absolute_file_name('sources/tcl_tk_ptlink', File).

extra_sourcefile_in_module(File, tools) :-
	absolute_file_name('sources/time_table', File).

extra_sourcefile_in_module(File, possible_model_semantics) :-
	absolute_file_name('sources/possible_models', File).
extra_sourcefile_in_module(File, possible_model_semantics) :-
	absolute_file_name('sources/possible_models_l1', File).

extra_sourcefile_in_module(File, model_based_semantics) :-
	absolute_file_name('sources/mm_satchmo_1', File).
extra_sourcefile_in_module(File, model_based_semantics) :-
	absolute_file_name('sources/mm_satchmo_2', File).
extra_sourcefile_in_module(File, model_based_semantics) :-
	absolute_file_name('sources/mm_satchmo_2_a', File).
extra_sourcefile_in_module(File, model_based_semantics) :-
	absolute_file_name('sources/mm_satchmo_d', File).
extra_sourcefile_in_module(File, model_based_semantics) :-
	absolute_file_name('sources/mm_satchmo_elem', File).
extra_sourcefile_in_module(File, model_based_semantics) :-
	absolute_file_name('sources/mm_satchmo_es', File).
extra_sourcefile_in_module(File, model_based_semantics) :-
	absolute_file_name('sources/mm_satchmo_examples', File).
extra_sourcefile_in_module(File, model_based_semantics) :-
	absolute_file_name('sources/stable_opti', File).

extra_sourcefile_in_module(File, well_founded_semantics) :-
	absolute_file_name('sources/adwfs_tu', File).

extra_sourcefile_in_module(File, library) :-
	absolute_file_name('library/assoc.pl', File).
extra_sourcefile_in_module(File, library) :-
	absolute_file_name('library/heaps.pl', File).
extra_sourcefile_in_module(File, library) :-
	absolute_file_name('library/lists.pl', File).
extra_sourcefile_in_module(File, library) :-
	absolute_file_name('library/ordsets.pl', File).
extra_sourcefile_in_module(File, library) :-
	absolute_file_name('library/random.pl', File).
extra_sourcefile_in_module(File, library) :-
	absolute_file_name('library/sldmagic_swi.pl', File).
extra_sourcefile_in_module(File, library) :-
	absolute_file_name('library/static_swi.pl', File).
extra_sourcefile_in_module(File, library) :-
	absolute_file_name('library/ugraphs.pl', File).



/* dont_create_info(?FileName) <-
        FileName is unified with files that should not be considered
        during creation of the information about the sourcefiles.
        For example these can be backup-sourcefiles. */

dont_create_info(Path) :-
   dont_create_info_files(Variable,Files),
   maplist( dda_absolute_file_name(Variable), Files, Paths ),
   member(Path,Paths).

dont_create_info_files(dd_advisor_path,[
   'source_for_testing', 'source_for_testing~',
   'config~',
   'create_info~',
   'dda~',
   'dislog_help~',
   'heuristic~',
   'metrics_db~',
   'parse_source~',
   'read_source~',
   'signature~', 'signature_db~',
   'tools~' ]).

dont_create_info_files(source_path,[
   'cc_semantics_test',
   'denial_tests',
   'fold_tests',
   'gnaf_tests',
   'mm_satchmo_tests',
   'mm_satchmo_d',
   'tpi_tps_tests',
%  --------------------
   'abbreviations~',
   'clause_trees~',
   'coins~',
   'dislog~',
   'edge_display~',
   'egcwa~',
   'elementary~',
   'epistemic_transformation~',
   'er_diagrams~',
   'fd_algorithms_3nf~',
   'ground_transformation~',
   'herbrand_base~',
   'magic_sets~',
   'mm_satchmo_2~',
   'mm_satchmo_elem~',
   'probabilistic_1~',
   'probabilistic_2~',
   'specials_swi~',
   'sql_connect~',
   'sql_elementary~',
   'sql_fixpoint~',
   'sql_interface~',
   'stable_disjunctive~',
   'stable_models~',
   'stock~',
   'stratification~',
   'supp_for_min~',
   'testing~',
   'tps_fixpoint~',
   'tps_fixpoint_cc~',
   'tps_delta~',
   'tracing~',
   'transformations~',
   'type_check~' ]).


/* op_for_dcg(?Precedence, ?Type, ?Name) <-
        Like the system-predicate op it defines operators. This ones
        are to be recognised by the definite-clauses-grammar (DCG). */

/* Sourcefile: load */
op_for_dcg(900, fy, '~').

/* Sourcefile: mm_satchmo_* */
op_for_dcg(1200, xfx, '--->').

/* Sourcefile: stratification */
op_for_dcg(30, xfy, \).
op_for_dcg(30, xfy, eq).
op_for_dcg(30, xfy, ge).
op_for_dcg(30, xfy, gt).

/* Sourcefile: transformations */
op_for_dcg(400, fx, 'E').
%op_for_dcg(900, fy, '~').

/* Sourcefile: library/static_swi.pl */
op_for_dcg(1200, xfx, '<-').
op_for_dcg(990, xfy, v).
op_for_dcg(995, xfy, '&').
%op_for_dcg(900, fy, not).
%op_for_dcg(1000, xfy, ',').

/* Sourcefile: library/sldmagic_swi.pl */
op_for_dcg(900, fy, '&').
op_for_dcg(1000, fy, db).
op_for_dcg(1000, fy, use_query_const).
op_for_dcg(1000, fy, derive_idb_literals).


/* ignore_pred(?Functor, ?Arity) <-
        Don't record the predicate with Functor and Arity in the
        software metrics. */

ignore_pred(!, 0).
ignore_pred(module, 2).
ignore_pred(statistics, 2).
ignore_pred(load_foreign_library, 2).
ignore_pred(unix, 1).   % in SWI-Prolog this is in the Quintus-Compatibility
ignore_pred(qsave_program, 1).
ignore_pred(qsave_program, 2).
ignore_pred(numbervars, 3).   % in SWI-Prolog see numbervars/4
ignore_pred(freeze, 1).   % only in SICStus-Prolog (maybe freeze/2 ???)
ignore_pred(save, 1).   % only in SICStus-Prolog
ignore_pred(error, 0).   % used in epistemic_transformation
ignore_pred(atom_codes, 2).   % only in IF/Prolog
ignore_pred(exception_handler, 3).   % only in IF/Prolog

% see sources/compile_dislog, sources/cdislog and sources/mdislog:
ignore_pred(load, 1).   % only in SICStus-Prolog
ignore_pred(compile, 1).   % only in SICStus-Prolog
ignore_pred(fcompile, 1).   % only in SICStus-Prolog

% see library/static_swi.pl:
ignore_pred(get_prompt, 3).   % only in ECLiPSe-Prolog
ignore_pred(set_prompt, 3).   % only in ECLiPSe-Prolog

% loaded from foreign libraries in libraray/random.pl:
ignore_pred(cgetrand, 3).
ignore_pred(cputrand, 3).
ignore_pred(crandom, 1).
ignore_pred(load_foreign_files, 2).   % only in SICStus-Prolog

% in sourcefile sources/interface predicate start_xdislog/0 consulted
% from sourcefiles, that are not in the public package
ignore_pred(start, 0).
ignore_pred(end, 0).
ignore_pred(xdislog, 0).

% in sourcefiles sources/tcl_tk_interface and sources/tcl_tk_ptlink
% consulted from sourcefiles, that are not in the public package
ignore_pred(tcl, 1).
ignore_pred(tcl, 3).
ignore_pred(tix_init, 0).
ignore_pred(tk_do_one_event, 0).


/* dynamic_pred(?SourceFile, ?Functor, ?Arity) <-
        The predicate with Functor and Arity is dynamically defined in
        SourceFile. */

dynamic_pred('sources/dd_advisor/dislog_help', dislog_help_index, 5).
dynamic_pred('sources/dd_advisor/dislog_help', compare_line, 1).
dynamic_pred('sources/dd_advisor/metrics_db', sourcefile_in_module, 2).
dynamic_pred('sources/dd_advisor/metrics_db', pred_def, 4).
dynamic_pred('sources/dd_advisor/metrics_db', pred_use, 7).
dynamic_pred('sources/dd_advisor/metrics_db', pred_undefined, 4).
dynamic_pred('sources/dd_advisor/metrics_db', pred_def_multiple, 4).
dynamic_pred('sources/dd_advisor/metrics_db', pred_use_over_module, 5).
dynamic_pred('sources/dd_advisor/metrics_db', pred_use_over_module_sum, 3).
dynamic_pred('sources/dd_advisor/metrics_db', module_interface_pred, 4).
dynamic_pred('sources/dd_advisor/signature', signature_pred_index, 3).
dynamic_pred('sources/dd_advisor/signature', signature_type_index, 3).

dynamic_pred('sources/cc_semantics', cc_semantics, 4).
dynamic_pred('sources/clause_trees', d_fact_tree, 1). % facts_clauses_db
dynamic_pred('sources/clause_trees', delta_fact_tree, 1). % facts_clauses_db
dynamic_pred('sources/clause_trees', delta_d_fact_tree, 1). % facts_clauses_db
dynamic_pred('sources/clause_trees', tree_edge, 2).
dynamic_pred('sources/clause_trees', tree_atom, 1).
dynamic_pred('sources/clause_trees', good_edge, 2).
dynamic_pred('sources/clause_trees', greedy_root, 1).
dynamic_pred('sources/clause_trees', join_tree, 1).
dynamic_pred('sources/clause_trees', join_tree_fact, 1).
dynamic_pred('sources/clause_trees', old_d_fact_tree, 1).
dynamic_pred('sources/clause_trees', meta_subsumes, 2).
dynamic_pred('sources/ddwfs', minimal_model_state, 1).
dynamic_pred('sources/depth_first_search', node, 1).
dynamic_pred('sources/depth_first_search', hyperedge, 1).
dynamic_pred('sources/depth_first_search', node_2cc, 1).
dynamic_pred('sources/depth_first_search', edge, 2).
dynamic_pred('sources/depth_first_search', edge_2cc, 2).
dynamic_pred('sources/edge_display', i_fact, 1).
dynamic_pred('sources/edge_display', hasse_node, 1).
dynamic_pred('sources/edge_display', cardinality_with_hasse_nodes, 2).
dynamic_pred('sources/edge_display', card_node, 1).
dynamic_pred('sources/egcwa', egcwa, 1).
dynamic_pred('sources/egcwa', gcwa, 1).
dynamic_pred('sources/egcwa', minimal_model_state, 1).
dynamic_pred('sources/egcwa_bin_graph', graph_ge, 1).
dynamic_pred('sources/er_diagrams', current_er_diagram, 1).
%dynamic_pred('sources/facts_clauses_db', d_fact, 1).
%dynamic_pred('sources/facts_clauses_db', delta_d_fact, 1).
%dynamic_pred('sources/facts_clauses_db', d_clause, 2).
%dynamic_pred('sources/facts_clauses_db', dn_clause, 3).
%dynamic_pred('sources/facts_clauses_db', d_fact_tree, 1).
%dynamic_pred('sources/facts_clauses_db', delta_fact_tree, 1).
%dynamic_pred('sources/facts_clauses_db', delta_d_fact_tree, 1).
dynamic_pred('sources/gdwfs_sp', d_fact, 1). % facts_clauses_db
dynamic_pred('sources/gdwfs_sp', d_clause, 2). % facts_clauses_db
dynamic_pred('sources/gdwfs_sp', dn_clause, 3). % facts_clauses_db
dynamic_pred('sources/gdwfs_sp', c_fact, 1).
dynamic_pred('sources/gdwfs_sp', state_pair, 2).
dynamic_pred('sources/gdwfs_st', u_fact, 1).
dynamic_pred('sources/gdwfs_st', n_fact, 1).
dynamic_pred('sources/gdwfs_st', state_triple, 3).
dynamic_pred('sources/gdwfs_st', current_state_triple, 3).
dynamic_pred('sources/gdwfs_st', tsd_omega_facts, 1).
dynamic_pred('sources/gdwfs_st', fsd_omega_facts, 1).
dynamic_pred('sources/gnaf', supersumed_clause, 1).
dynamic_pred('sources/gnaf', support_pair, 2).
dynamic_pred('sources/gnaf', delta_support_set, 2).
dynamic_pred('sources/gnaf', support_set, 2).
dynamic_pred('sources/gnaf', d_fact_tree_S, 1).
dynamic_pred('sources/gnaf', d_fact_tree_s, 1).
dynamic_pred('sources/gnaf', delta_d_fact_tree_s, 1).
dynamic_pred('sources/gnaf', global_extenders, 1).
dynamic_pred('sources/gnaf', actual_state, 1).
dynamic_pred('sources/gnaf', accumulated_state, 1).
dynamic_pred('sources/ground_transformation', controller, 1).
dynamic_pred('sources/heaps', heap_size, 1).
dynamic_pred('sources/heaps', a, 3).
dynamic_pred('sources/herbrand_base', herbrand_base, 1).
dynamic_pred('sources/herbrand_base', herbrand_tuples, 2).
dynamic_pred('sources/herbrand_base', program_atom, 1).
dynamic_pred('sources/input_output', data_dictionary, 3).
dynamic_pred('sources/input_output', i_files, 2).
dynamic_pred('sources/input_output', set, 3).
dynamic_pred('sources/interface', special_predicates, 1).
dynamic_pred('sources/interface', dislog_variable, 2).
dynamic_pred('sources/load', 'dis$read', 0).
dynamic_pred('sources/load', dis_status, 1).
dynamic_pred('sources/load', dis_trace, 0).
dynamic_pred('sources/load', dis_use_prolog_builtins, 0).
dynamic_pred('sources/load', dis_use_interpreted, 0).
dynamic_pred('sources/lolita', extend_flag, 0).
dynamic_pred('sources/lolita', rule, 2).
dynamic_pred('sources/lolita', fact, 1).
dynamic_pred('sources/lolita', str_anc, 4).
dynamic_pred('sources/lolita', new_delta, 4).
dynamic_pred('sources/lolita', delta, 4).
dynamic_pred('sources/lolita', neg_Lit, 4).
dynamic_pred('sources/lolita', pos_Lit, 4).
dynamic_pred('sources/lolita', rank, 3).
dynamic_pred('sources/lolita', str_rule, 2).
dynamic_pred('sources/lolita', delta, 1).
dynamic_pred('sources/lolita', first_Lit, 0).
dynamic_pred('sources/lolita', ground_flag, 0).
dynamic_pred('sources/lolita', indiv, 1).
dynamic_pred('sources/lolita', g_fact, 1).
dynamic_pred('sources/lolita', new_delta, 1).
dynamic_pred('sources/lolita', v_fact, 1).
dynamic_pred('sources/lolita', vq_fact, 1).
dynamic_pred('sources/min_span_tree', graph_node, 1).
dynamic_pred('sources/min_span_tree', graph_edge, 3).
dynamic_pred('sources/min_span_tree', graph_root, 1).
dynamic_pred('sources/min_span_tree', greedy_root, 1).
dynamic_pred('sources/min_span_tree', tree_edge, 2).
dynamic_pred('sources/min_span_tree', good_edge, 2).
dynamic_pred('sources/min_span_tree', cardinality_with_hasse_nodes, 2).
% ... dynamic_pred('sources/mm_satchmo_1', X, X).
dynamic_pred('sources/mm_satchmo_2', false, 0).
dynamic_pred('sources/mm_satchmo_2', counter, 1).
dynamic_pred('sources/mm_satchmo_2', ob, 1).
dynamic_pred('sources/mm_satchmo_2', ob_clause, 2).
dynamic_pred('sources/mm_satchmo_2', s_clause, 2).
dynamic_pred('sources/mm_satchmo_2_a', interpretation_to_be_tested, 1).
dynamic_pred('sources/mm_satchmo_es', at, 1).
dynamic_pred('sources/mm_satchmo_es', ev, 1).
dynamic_pred('sources/model_trees', c_fact_tree, 1).
dynamic_pred('sources/specials_sicstus', current_num, 2).
dynamic_pred('sources/states', dn_fact, 1).
dynamic_pred('sources/steiner_trees', cardinality_with_hasse_nodes, 2).
dynamic_pred('sources/steiner_trees', required_node, 2).
dynamic_pred('sources/steiner_trees', steiner_node, 2).
dynamic_pred('sources/stock', dax_100, 1).
dynamic_pred('sources/supp_for_neg', minimal_model_state, 1).
dynamic_pred('sources/testing', manual_file, 1).
dynamic_pred('sources/testing', result_file, 1).
dynamic_pred('sources/testing', perf_test, 0).
dynamic_pred('sources/testing', perf_test_file, 1).
dynamic_pred('sources/testing', perf_result_file, 1).
dynamic_pred('sources/testing', dont_test, 1).
dynamic_pred('sources/testing', output_stream, 1).
dynamic_pred('sources/testing', performance_test, 4).
dynamic_pred('sources/time_statistics', start_time, 1).
dynamic_pred('sources/time_statistics', start_time, 2).
dynamic_pred('sources/time_statistics', accumulated_time, 2).
dynamic_pred('sources/tps_delta', delta_d_fact, 1). % facts_clauses_db
dynamic_pred('sources/tps_delta', length_d_fact_pair, 2).
dynamic_pred('sources/tps_delta', base_literal, 1).
dynamic_pred('sources/tps_fixpoint_cc', tps_iteration_ps, 2).
dynamic_pred('sources/ufi_interface', semantics, 4).

dynamic_pred('library/sldmagic_swi.pl', option, 2).
dynamic_pred('library/sldmagic_swi.pl', edb_pred, 2).
dynamic_pred('library/sldmagic_swi.pl', idb_pred, 2).
dynamic_pred('library/sldmagic_swi.pl', rule, 2).
dynamic_pred('library/sldmagic_swi.pl', query, 1).
dynamic_pred('library/sldmagic_swi.pl', node, 2).
dynamic_pred('library/sldmagic_swi.pl', answer, 1).
dynamic_pred('library/sldmagic_swi.pl', sld, 2).


/* comments_file_separator(?Char) <-
        Char is unified with the character that separates different
        entries in the comments-file. ISO-8859-1-Code 182 is the
        pilcrow sign, which text-processors usually use to separate
        paragraphs. */

comments_file_separator(182).


/* pager_file(?FileName) <- 
        FileName is unified with the filename that should be used to
        output data for reading with a pager like "more" or "less". */

pager_file('results/pager.tmp').


/* pager_command(-Command) <- 
        Command is unified with the command that should be invoked
        to read text with a pager like "more" or "less". The
        filename from pager_file/1 has to be part of the command. */

pager_command('less results/pager.tmp').


/* help_linenumber_interval(?Lines) <-
        Lines is unified with the number of lines that the help
        system should check around a found help-topic. Comments,
        that are at the most Lines lines away are also shown. */

%help_linenumber_interval(0).
help_linenumber_interval(10).


/* unknown(?X) <-
        X is unified with an atom to signal that it is unknown. */

unknown('UNKNOWN').


/* tmp_text_file(?File) <-
        File will be unified with the name of a file to hold
        temporary texts. */

tmp_text_file(File) :-
	absolute_file_name('results/temp.txt', File).


/* heuristic_verbose(?Value) <-
        If Value unifies with true the heuristics to rate disjuncive
        programs print some detailed information. */

heuristic_verbose(true).
%heuristic_verbose(false).


/* heuristic_default(?Number) <-
        If Number is unified with the number of the heuristic
        function that will be used as a default. */

heuristic_default(1).


/*** Implementation ***********************************************/


/*** Initialization ***********************************************/


/*** Testing / Debugging ******************************************/


/*
% :- module_transparent test_assert/1.
:- meta_predicate test_assert(_).

test_assert(Pred) :-
	context_module(ContextModule),
	findall(Module,
	        default_module(ContextModule, Module),
	        DefaultModules),
	writef('Asserting      : %w\n', [Pred]),
	writef('Context-Module : %w\n', [ContextModule]),
	writef('Default-Modules: %w\n', [DefaultModules]),
	assert(Pred).
*/


/******************************************************************/


/* Emacs-Configuration: */

%%% Local Variables:
%%% mode: prolog
%%% End:
