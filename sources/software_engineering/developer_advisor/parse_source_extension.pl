

/******************************************************************/
/***                                                            ***/
/***           DDA:  Parse Sources - Extension                  ***/
/***                                                            ***/
/******************************************************************/


/* dda_parsing_fails(Path) <-
      */

dda_parsing_fails(Path) :-
   dislog_variable_get(source_path, Sources),
   dda_parsing_fails_files(Files),
   member(File, Files),
   concat(Sources, File, Path).

dda_parsing_fails_files([
   'interfaces/oracle_interface/sql_fixpoint',
   'projects/conferences/reviews',
   'stock_tool/stock_hierarchy/stock_hierarchy_window',
   'software_engineering/refactoring/refactoring_dialog',
   'software_engineering/visur_rar/rar_special_predicates',
   'software_engineering/visur_rar/visur',
   'software_engineering/developer_advisor/create_info',
   'software_engineering/developer_advisor/read_source',
   'software_engineering/developer_advisor/parse_source_elementary',
   'software_engineering/developer_advisor/dislog_help' ]).


/*** interface ****************************************************/


/* parse_source_files(Trees_and_Comments) <-
      */

parse_source_files(Trees_and_Comments) :-
   dislog_sources(Sources),
   dislog_variable_get(source_path, Path),
   maplist( concat(Path),
      Sources, Files ),
   maplist( parse_source_file,
      Files, Trees_and_Comments ).


/* parse_source_file(Path, Trees-Comments) <-
      */

parse_source_file(Path, []-[]) :-
   dda_parsing_fails(Path),
   !.
parse_source_file(Path, Trees-Comments) :-
%  star_line,
   writeln(user, Path),
   parse_source_file(Path, Trees, Comments).


/* parse_source_file(Path, Trees, Comments) <-
      */

parse_source_file(Path, Trees, Comments) :-
   ( ( exists_file(Path),
       read_source(Path, Tokens, Comments) )
   ; ( concat(Path, '.pl', Path_2),
       exists_file(Path_2),
       read_source(Path_2, Tokens, Comments) ) ),
   !,
   parse_source_all(Tokens, Trees).


/* parse_source_all(StringTokenList, Trees) <-
      */

parse_source_all(StringTokenList, Trees) :-
   maplist( string_to_atom,
      StringTokenList, AtomTokenList ),
   dda_parse_source:parse_source_update_precedences,
   parse_blocks_all(AtomTokenList, Trees).


/* parse_blocks_all(TokenList, Trees) <-
      */

parse_blocks_all([], []) :-
   !.
parse_blocks_all(TokenList, [Tree|Trees]) :-
   dda_parse_source:dcg_block(Tree, TokenList, Rest),
   ( Rest = []
   ; parse_blocks_all(Rest, Trees) ).


/******************************************************************/


