
/******************************************************************/
/***                                                            ***/
/***       Program : DisLog Developer Advisor                   ***/
/***    SourceFile : tools                                      ***/
/***       Version : 0.0.0                                      ***/
/***        Author : Thomas Schilling                           ***/
/***      Language : SWI-Prolog 2.9.5                           ***/
/***       Started : 26. November 1997                          ***/
/***  Last Changed : 9. April 1998                              ***/
/***       Purpose : Miscellaneous tools.                       ***/
/***                                                            ***/
/******************************************************************/


:- module(dda_tools,[
      count_goal/2,
      stream_writef/3,
      stream_writef/2,
      de_absolute_file_name/2,
      sort_file/1,
      press_key/0,
      press_key_quiet/0,
      press_key_verbose/0,
      start_cpu_timer/0,
      get_cpu_timer/1,
      call_with_timer/3,
      call_with_timer_loop/2,
      call_with_timer_call_fail/1]).

   
:- use_module_dda([
      signature,
      config ]).


/* flags:
      count_goal_counter
      cpu_timer_start_time */


/*** Interface ****************************************************/


/* count_goal(+Goal, -Number) <-
      Count the solutions for Goal.
      The result is unified with Number. */


/* stream_writef(+Stream, +Format, +Arguments) <-
      Like writef, but output is on Stream. */


/* stream_writef(+Stream, +Format) <-
      Like writef, but output is on Stream. */


/* de_absolute_file_name(+Absolute, ?File) <-
      Opposite of absolute_file_name/2.
      Absolute should be the name of a file including
      the full path.
      File will be unified with the current path cut from
      that. */


/* sort_file(+File) <-
      The file given by the filename File will be sorted
      line by line alphabetically. */

/* press_key :-
      The same as press_key_verbose or press_key_quiet
      or "do nothing".
      (Supposed to be configured for use in debugging.) */


/* press_key_quiet :-
      Wait for any key to be pressed. */


/* press_key_verbose :-
      Writes a message to press any key to standard output
      and waits for that to happen.
      Afterwards this message is deleted. */


/* start_cpu_timer :-
      Start a timer to stop some operations.
      See also get_timer/1. */


/* get_cpu_timer(-Time) :-
      Get the time between the call of get_timer/1
      and start_timer/1.
      It is the CPU-time in seconds as a float. */


/* call_with_timer(+Goal, +N, -Time) :-
      The goal Goal is called N times.
      The CPU time used by one call is calculated as
      the average time and returned in Time. */


/*** Implementation ***********************************************/


/* count_goal/2: Description see interface. */

% :- meta_predicate
%       count_goal(_, _),
%       count_goal_2(_).

:- module_transparent
      count_goal/2,
      count_goal_2/1.

count_goal(Goal, Number) :-
   flag(count_goal_counter, _Old, 0),
   count_goal_2(Goal),
   flag(count_goal_counter, Number, Number).

count_goal_2(Goal) :-
   call(Goal),
   flag(count_goal_counter, Old, Old + 1),
   fail.
count_goal_2(_Goal).


/* stream_writef/3: Description see interface. */

stream_writef(Stream, Format, Arguments) :-
   swritef(String, Format, Arguments),
   write(Stream, String).


/* stream_writef/2: Description see interface. */

stream_writef(Stream, Format) :-
   swritef(String, Format),
   write(Stream, String).


/* de_absolute_file_name/2: Description see interface. */

de_absolute_file_name(Absolute, File) :-
   absolute_file_name(Absolute, Absolute2),
   absolute_file_name('.', CurrentPath),
   concat(CurrentPath, File, Absolute2).


/* sort_file/1: Description see interface. */

sort_file(File) :-
   absolute_file_name(File, AbsFile),
   file_directory_name(AbsFile, Dir),
   concat(Dir, 'sort_file.tmp', TmpFile),
   rename_file(AbsFile, TmpFile),
   concat_atom(['sort <', TmpFile, ' >', AbsFile], Command),
   shell(Command, _Status),
   delete_file(TmpFile).


/* ddiscontiguous(+PredList) <-
      Declares the predicates in PredList discontiguous.
      That is they might not be together in the source file.
      The predicates have to be in the form name/arity.
      Instead of a list a single predicate is also allowed. */

% This should reside in specials_swi and specials_sicstus.

/*
ddiscontiguous([Pred | Preds]) :-
   discontiguous(Pred),
   ddiscontiguous(Preds).
ddiscontiguous([]).
ddiscontiguous(Pred) :-
   discontiguous(Pred).
*/


/* press_key/0: Description see interface. */

press_key :-
%  press_key_verbose,
%  press_key_quiet,
   true.


/* press_key_quiet/0: Description see interface. */

press_key_quiet :-
   get_single_char(_Char).


/* press_key_verbose/0: Description see interface. */

press_key_verbose :-
   write(user_output, 'Please press any key ...'),
   press_key_quiet,
   nl(user_output),
   tty_get_capability('up', string, CsrUp),
   tty_put(CsrUp, 1),
   tty_get_capability('ce', string, ClrEoL),
   tty_put(ClrEoL, 1).


/* start_cpu_timer/0: Description see interface. */

start_cpu_timer :-
   system:statistics(cputime, Time),
   flag(cpu_timer_start_time, _Old, Time).
%  start_cpu_timer.
%  In case the system-predicate causes errors.


/* get_timer/1: Description see interface. */

get_cpu_timer(Time) :-
   system:statistics(cputime, CurrentTime),
   flag(cpu_timer_start_time, StartTime, StartTime),
   Time is CurrentTime - StartTime.
%  get_cpu_timer(0).
%  In case the system-predicate causes errors.


/* call_with_timer/3: Description see interface. */

% :- meta_predicate
%       call_with_timer(_, _, _),
%       call_with_timer_loop(_, _),
%       call_with_timer_call_fail(_).

:- module_transparent
      call_with_timer/3,
      call_with_timer_loop/2,
      call_with_timer_call_fail/1.

call_with_timer(Goal, N, Time) :-
   start_cpu_timer,
   call_with_timer_loop(Goal, N),
   get_cpu_timer(TimeSum),
   Time is TimeSum / N.

call_with_timer_loop(Goal, N) :-
   N > 0,
   ignore(call_with_timer_call_fail(Goal)),
   N2 is N - 1,
   !,
   call_with_timer_loop(Goal, N2).
call_with_timer_loop(_Goal, _N).

call_with_timer_call_fail(Goal) :-
   % This predicate fails to assure, that there are no choice-points
   % left for backtracking. So the stack won't overrun.
   !, call(Goal), !,
   fail.


/*** Initialization ***********************************************/


%:- start_cpu_timer.
:- flag(cpu_timer_start_time, _Old, 0).
   % In case the system-predicate causes errors.


/******************************************************************/


