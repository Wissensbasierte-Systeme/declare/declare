
/******************************************************************/
/***                                                            ***/
/***       Program : DisLog Developer Advisor                   ***/
/***    SourceFile : read_source                                ***/
/***       Version : 0.0.0                                      ***/
/***        Author : Thomas Schilling                           ***/
/***      Language : SWI-Prolog 2.9.5                           ***/
/***       Started : 1. August 1997                             ***/
/***  Last Changed : 21. March 1998                             ***/
/***       Purpose : Reading and analysing a sourcefile. (Like  ***/
/***                 a scanner.)                                ***/
/***                                                            ***/
/******************************************************************/


:- module(dda_read_source,[
      read_source/3]).


:- use_module_dda([
      config]).


/* flags:
      sourcelinenumber */


/*** interface ****************************************************/


/* read_source(+File, -Tokens, -Comments) <-
      Read a source file with the name File.
      Tokens will be the list of tokens of File,
      and Comments will be the list of comments.
      Each comment is a list containing the
      LineNumber and the CommentString */

read_source(File, Tokens, Comments) :-
   absolute_file_name(File, Path),
   open_stream(Path, read, sourcefile,
      [type(text), eof_action(eof_code)]),
   flag(sourcelinenumber, _Old, 1),
   get0_trace(sourcefile, Char),
   read_token_trace(Char, Token, Comment, Char1),
   read_source_rest(Token, Comment, Char1, Tokens1, Comments),
   subtract([Token|Tokens1], [c_comment_token,
       percent_comment_token, end_of_file_token], Tokens),
   writeln_trace(user, read_source(File, Tokens, Comments)).


/*** implementation ***********************************************/


/* read_source_rest(+Token, +Comment, +Char, -Tokens, -Comments) <-
      Read the rest of a sourcefile.
      Token was the last token read and Char is the next character.
      Tokens will contain the rest of the tokens of the file.
      If Token is a comment Comment is the data for that comment.
      Comments will contain the rest of comments of that file. */

read_source_rest(end_of_file_token, _, _, [], []) :-
   close(sourcefile),
   !.
read_source_rest(Token, C, Char, [T|Ts], [C|Cs]) :-
   member(Token, [c_comment_token, percent_comment_token]),
   !,
   read_token_trace(Char, T, Comment1, Char1),
   read_source_rest(T, Comment1, Char1, Ts, Cs).
read_source_rest(_, _, Char, [T|Ts], Cs) :-
   read_token_trace(Char, T, Comment1, Char1),
   read_source_rest(T, Comment1, Char1, Ts, Cs).


read_token_trace(A, B, C, D) :-
   !,
   read_token(A, B, C, D).
read_token_trace(A, B, C, D) :-
   writeln(user, read_token(A, B, C, D)),
   read_token(A, B, C, D),
   writeln(user, read_token(A, B, C, D)).


/* read_token(+Char, -Token, -Comment, -Char1) <-
      Read one token.
      Char is the next character and Char1 will be the next
      character after this token.
      If the token is a comment, Comment will be the small
      list describing the comment. (See read_source/3.) */

read_token(-1, end_of_file_token, _, _) :-
   !.
read_token(Char, Token, _, Char1) :-
   single_char_token(Char),
   !,
   string_to_list(Token, [Char]),
   get0_trace(sourcefile, Char1).
read_token(48, Token, _, NextChar) :-               % 0'
   dda_peek_byte(sourcefile, 39),
%  writeln(user, dda_peek_byte(sourcefile, 39)),
   !,
   get0_trace(sourcefile, _),
   get0_trace(sourcefile, Char1),
   number_chars_save(Char1, CharList),
   string_to_list(Token, CharList),
   get0_trace(sourcefile, NextChar).
read_token(61, Token, _Comment, NextChar) :-        % =..
   dda_peek_byte(sourcefile, 46),
%  writeln(user, dda_peek_byte(sourcefile, 46)),
   !,
   get0_trace(sourcefile, _),
   get0_trace(sourcefile, 46),
   string_to_list(Token, "=.."),
   get0_trace(sourcefile, NextChar).
read_token(39, Token, _Comment, Char2) :-           % '
   !,
   get0_trace(sourcefile, Char1),
   read_atom_string(Char1, Chars, Char2),
   string_to_list(Token, [39|Chars]).
read_token(34, Token, _Comment, Char2) :-           % "
   !,
   get0_trace(sourcefile, Char1),
   read_string(Char1, Chars, Char2),
   string_to_list(Token, [34|Chars]).
read_token(47, c_comment_token, [LineNumber, String], Char2) :-
   dda_peek_byte(sourcefile, 42),                       % /*
   !,
   flag(sourcelinenumber, LineNumber, LineNumber),
   get0_trace(sourcefile, Char1),
   read_c_comment(Char1, Chars, Char2),
   string_to_list(String, [47|Chars]).
read_token(37, percent_comment_token, [LineNumber, String], Char2) :-
   !,                                               % %
   flag(sourcelinenumber, LineNumber, LineNumber),
   get0_trace(sourcefile, Char1),
   read_percent_comment(Char1, Chars, Char2),
   string_to_list(String, [37|Chars]).
read_token(Char, Token, _Comment, Char2) :-
   start_of_atom_or_variable(Char),
%  writeln(user, start_of_atom_or_variable(Char)),
   !,
   get0_trace(sourcefile, Char1),
   read_atom_var(Char1, Chars, Char2),
   string_to_list(Token, [Char|Chars]).
read_token(Char, Token, _Comment, Char2) :-
   start_of_number(Char),
   !,
   get0_trace(sourcefile, Char1),
   read_number(Char1, Chars, Char2),
   string_to_list(Token, [Char|Chars]).
read_token(Char, Token, _Comment, Char2) :-
   in_token(Char),
%  writeln(user, in_token(Char)),
   !,
   get0_trace(sourcefile, Char1),
   read_token_rest(Char1, Chars, Char2),
   string_to_list(Token, [Char|Chars]).
read_token(10, Token, Comment, Char2) :-            % line feed
   flag(sourcelinenumber, Line, Line+1),
   get0_trace(sourcefile, Char1),
   read_token(Char1, Token, Comment, Char2).
read_token(_, Token, Comment, Char2) :-
   get0_trace(sourcefile, Char1),
   read_token(Char1, Token, Comment, Char2).


/* read_token_rest(+Char, -Chars, -Char1) <-
      Read the rest of a token.
      Char is the next character, Chars will contain the
      characters of this token and Char1 will be the next
      character after this token. */

read_token_rest(Char, [Char|Chars], Char2) :-
   in_token(Char),
   \+ in_atom_or_variable(Char),
   !,
   get0_trace(sourcefile, Char1),
   read_token_rest(Char1, Chars, Char2).
read_token_rest(Char, [], Char).


/* read_atom_var(+Char, -Chars, -Char1) <-
      Read the rest of an atom- or variable-token.
      Char is the next character, Chars will contain the
      characters of this token and Char1 will be the next
      character after this token. */

read_atom_var(Char, [Char|Chars], Char2) :-
   in_atom_or_variable(Char),
   !,
   get0_trace(sourcefile, Char1),
   read_atom_var(Char1, Chars, Char2).
read_atom_var(Char, [], Char).


/* read_number(+Char, -Chars, -NextChar) <-
      Read the rest of a number-token. Char is the next character,
      Chars will contain the characters of this token and
      NextChar will be the next character after this token. */

read_number(Char, [Char|Chars], NextChar) :-
   in_number(Char),
   !,
   get0_trace(sourcefile, NextCharSub),
   read_number(NextCharSub, Chars, NextChar).
read_number(46, [46|Chars], NextChar) :-
   dda_peek_byte(sourcefile, AfterPointChar),
   between_character(0'0, 0'9, AfterPointChar),
   !,
   get0_trace(sourcefile, NextCharSub),
   read_number(NextCharSub, Chars, NextChar).
read_number(Char, [], Char).


/* read_atom_string(+Char, -Chars, -NextChar) <-
      Read the rest of a '...'-string.
      Char is the next character, Chars will contain the
      characters of this string (including the ') and
      NextChar will be the next character after this string. */

read_atom_string(-1, [], NextChar) :-   % -1 = end of file
   !,
   get0_trace(sourcefile, NextChar).
read_atom_string(92, [92,Char|Chars], NextChar) :-   % 92 = backslash
   !,
   get0_trace(sourcefile, Char),
   get0_trace(sourcefile, NextCharSub),
   read_atom_string(NextCharSub, Chars, NextChar).
read_atom_string(39, [39|Chars], NextChar) :-        % 39 = '
   dda_peek_byte(sourcefile, 39),
   !,
   get0_trace(sourcefile, _Char),
   get0_trace(sourcefile, NextCharSub),
   read_atom_string(NextCharSub, Chars, NextChar).
read_atom_string(39, [39], NextChar) :-              % 39 = '
   !,
   get0_trace(sourcefile, NextChar).
read_atom_string(Char, [Char|Chars], NextChar) :-
   !,
   get0_trace(sourcefile, NextCharSub),
   read_atom_string(NextCharSub, Chars, NextChar).
read_atom_string(Char, [], Char).


/* read_string(+Char, -Chars, -Char1) <-
      Read the rest of a "..."-string. Char is the next character,
      Chars will contain the characters of this string (including
      the ") and Char1 will be the next character after this string. */

read_string(-1, [], Char1) :-   % -1 = end of file
   !,
   get0_trace(sourcefile, Char1).
read_string(34, [34], Char1) :-   % 34 = "
   !,
   get0_trace(sourcefile, Char1).
read_string(Char, [Char | Chars], Char2) :-
   !,
   get0_trace(sourcefile, Char1),
   read_string(Char1, Chars, Char2).
read_string(Char, [], Char).


/* read_c_comment(+Char, -Chars, -Char1) <-
      Read the rest of a / *...* /-comment. Char is the next
      character, Chars will contain the characters of this comment
      (including the / *  * / ) and Char1 will be the next character
      after this comment. */

read_c_comment(Char, Chars, Char1) :-
   read_c_comment(0, Char, Chars, Char1).

read_c_comment(_, -1, [], Char1) :-                  % end of file
   !,
   get0_trace(sourcefile, Char1).
read_c_comment(Level, 47, [47|Chars], Char1) :-      % /*
   dda_peek_byte(sourcefile, 42),
   !,
   get0_trace(sourcefile, _),
   Level_1 is Level + 1,
   read_c_comment(Level_1, 42, Chars, Char1).
read_c_comment(0, 42, [42, 47], Char1) :-            % */
   dda_peek_byte(sourcefile, 47),
   !,
   get0_trace(sourcefile, _),
   get0_trace(sourcefile, Char1).
read_c_comment(Level, 42, [42|Chars], Char1) :-      % */
   dda_peek_byte(sourcefile, 47),
   !,
   get0_trace(sourcefile, _),
   Level_1 is Level - 1,
   read_c_comment(Level_1, 47, Chars, Char1).
read_c_comment(Level, 10, [10|Chars], Char2) :-      % line feed
   !,
   flag(sourcelinenumber, Line, Line+1),
   get0_trace(sourcefile, Char1),
   read_c_comment(Level, Char1, Chars, Char2).
read_c_comment(Level, Char, [Char|Chars], Char2) :-
   !,
   get0_trace(sourcefile, Char1),
   read_c_comment(Level, Char1, Chars, Char2).
read_c_comment(0, Char, [], Char).


/* read_percent_comment(+Char, -Chars, -Char1) <-
      Read the rest of a comment starting with a %. Char is the next
      character, Chars will contain the characters of this comment
      (including the %) and Char1 will be the next character after
      this comment. */

read_percent_comment(-1, [], Char1) :-   % -1 = end of file
   !,
   get0_trace(sourcefile, Char1).
read_percent_comment(10, [], Char1) :-   % 10 = line feed
   !,
   flag(sourcelinenumber, Line, Line+1),
   get0_trace(sourcefile, Char1).
read_percent_comment(13, [], Char1) :-   % 13 = carriage return
   !,
   get0_trace(sourcefile, Char1).
read_percent_comment(Char, [Char | Chars], Char2) :-
   !,
   get0_trace(sourcefile, Char1),
   read_percent_comment(Char1, Chars, Char2).
read_percent_comment(Char, [], Char).


/* get0_trace(X, Y) <-
      */

get0_trace(X, Y) :-
   !,
   get0(X, Y).
get0_trace(X, Y) :-
   get0(X, Y),
   writeln(user, get0(X, Y)).


/* single_char_token(?Char) <-
      True if Char is a character that forms a token by itself. */

single_char_token(Char) :-
   member(Char, "()[]{},.;|").


/* in_token(?Char) <-
      True if Char is legal as part of a token. */

in_token(Char) :-
   ( between(0'0, 0'9, Char)
   ; between(0'A, 0'Z, Char)
   ; between(0'a, 0'z, Char)
   ; international_char(Char)
   ; member(Char, "!#$&*+-/:<=>?@\\^_`~") ).


/* start_of_atom_or_variable(?Char) <-
      True if Char is legal as the first character of an atom or a
      variable. */

start_of_atom_or_variable(Char) :-
   ( between(0'A, 0'Z, Char)
   ; between(0'a, 0'z, Char)
   ; international_char(Char)
   ; member(Char, "$_") ).


/* international_char(Char) <-
      international chars (ISO 8859-1) */

international_char(Char) :-
   ( between(192, 214, Char)
   ; between(216, 246, Char)
   ; between(248, 255, Char) ).



/* in_atom_or_variable(?Char) <-
        True if Char is legal as part of an atom or a variable. */

in_atom_or_variable(Char) :-
   ( start_of_atom_or_variable(Char)
   ; between(0'0, 0'9, Char) ).


/* start_of_number(?Char) <-
        True if Char is legal as the first character of a number. */

start_of_number(Char) :-
   between(0'0, 0'9, Char).


/* in_number(?Char) <-
        True if Char is legal as part of a number. */

in_number(Char) :-
   start_of_number(Char).
in_number(Char) :-
   member(Char, "'bho").
%  member(Char, "'bho.").


/* whitespace(?Char) <-
        True if Char is a whitespace character. */

whitespace(Char) :-
   line_ending_char(Char).
whitespace(9).               % tab
whitespace(32).              % space


/* line_ending_char(?Char) <-
        True if Char is a character that marks the end of a line. */

line_ending_char(  4).   % control D
line_ending_char( 10).   % line feed
line_ending_char( 12).   % form feed
line_ending_char( 13).   % carriage return
line_ending_char( 26).   % control Z


/*** testing and debugging ****************************************/


/* portray_tokens(+Tokens) <-
        Pretty-print a list of tokens. (A Token after a dot is
        indented less. */

portray_tokens(Tokens) :-
   write('Token-List:'), nl,
   write('==========='), nl,
   portray_tokens_rec(Tokens, was_dot).
portray_tokens_rec([Token | Tokens], Was_Dot) :-
   string_to_list(Token, [46]),   % .
   !,
   indent_token(Was_Dot),
   write(Token), nl,
   portray_tokens_rec(Tokens, was_dot).
portray_tokens_rec([Token | Tokens], Was_Dot) :-
   !,
   indent_token(Was_Dot),
   write(Token), nl,
   portray_tokens_rec(Tokens, was_no_dot).
portray_tokens_rec([], _Was_Dot).
indent_token(was_dot) :-
   tab(4).
indent_token(was_no_dot) :-
   tab(8).
   

/* portray_comments(+Comments) <-
        Pretty-print a list of comments. */

portray_comments(Comments) :-
   write('Comment-List:'), nl,
   write('============='), nl,
   portray_comments_rec(Comments).
portray_comments_rec([[LineNumber, String] | Comments]) :-
   !,
   writef('Line: %w\n%w\n', [LineNumber, String]),
   portray_comments_rec(Comments).
portray_comments_rec([_Comment | Comments]) :-
   write('ERROR: Comment has wrong format.'), nl,
   portray_comments_rec(Comments).
portray_comments_rec([]).
portray_comments(Stream, Comments) :-
   current_output(OldStream),
   set_output(Stream),
   portray_comments(Comments),
   set_output(OldStream).


/* test_read_source <-
        Test this implementation. */

test_read_source :-
   read_source(
      'sources/software_engineering/developer_advisor/source_for_testing',
      Tokens, Comments),
   tell('results/read_source_test'),
   nl,
   portray_tokens(Tokens),
   nl,
   portray_comments(Comments),
   told.


/* dda_peek_byte(File, Char) <-
      */

dda_peek_byte(File, Char) :-
   current_prolog_flag(version, Version),
   ( Version < 60000 ->
     catch( peek_byte(File, Char), _, fail )
   ; get0_trace(File, Char) ).


/******************************************************************/


