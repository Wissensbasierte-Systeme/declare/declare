
/******************************************************************/
/***                                                            ***/
/***       Program : DisLog Developer Advisor                   ***/
/***    SourceFile : create_info                                ***/
/***       Version : 0.0.0                                      ***/
/***        Author : Thomas Schilling                           ***/
/***      Language : SWI-Prolog 2.9.5                           ***/
/***       Started : 4. August 1997                             ***/
/***  Last Changed : 2. April 1998                              ***/
/***       Purpose : Create files with information about        ***/
/***                 the sourcefiles of the package.            ***/
/***                                                            ***/
/******************************************************************/


:- module(dda_create_info,[
      check_sources/0,
      create_metrics_db/0,
      current_sourcefile/1,
      sourcefiles/2 ]).


:- use_module_dda([
      config,
      signature,
      dislog_help,
      read_source,
      parse_source,
      metrics_db ]).


/* flags:
      current_sourcefile_flag
      create_metrics_db_flag */


/*** Interface ****************************************************/


/* check_sources <-
        Checks if the comments-file need to be updated by comparing
        it's time-stamp with the sourcefiles, and if so it does
        this. */


/* create_metrics_db <-
        The information-files about the sourcefiles will be created.
        (This includes the help-files.) */


/* current_sourcefile(?File) <-
        File is unified with the name of the sourcefile that
        currently is examined. */


/* sourcefiles(+TopDirs, -Files) <-
        Create a list of files with their absolute file names into
        Files. Starting in the directories in the list TopDir,
        subdirectories are scanned recursively. */


/*** Implementation ***********************************************/


/* check_sources/0: Description see interface. */

:- signature(check_sources).

check_sources :-
   flag(create_metrics_db_flag, _Old, no),
   sourcedirs(Dirs),
   sourcefiles(Dirs, Files),
   create_info_if(Files).


/* dislog_sources_dda(Sources) <-
      */

dislog_sources_dda(Sources) :-
   dislog_sources(DisLog_Sources),
   dislog_variable(home, DisLog),
   concat(DisLog, '/sources/', Path),
   maplist( concat(Path),
      DisLog_Sources, Sources ).


/* create_metrics_db/0: Description see interface. */

:- signature(create_metrics_db).

create_metrics_db :-
   flag(create_metrics_db_flag, _Old1, yes),
   init_metrics_db,
%  sourcedirs(Dirs),
%  sourcefiles(Dirs, Files),
%  dislog_sources_dda(Files),
   Files = [sss],
   create_info(Files),
   flag(create_metrics_db_flag, _Old2, no),
   init_help(check).


/* sourcefiles/2: Description see interface. */

sourcefiles(TopDirs, Files) :-
   sourcefiles_unflat(TopDirs, Files1),
   flatten(Files1, Files).
sourcefiles_unflat([TopDir | TopDirs], [Files | FilesTail]) :-
   sourcefiles_single(TopDir, Files),
   sourcefiles_unflat(TopDirs, FilesTail).
sourcefiles_unflat([], []).
sourcefiles_single(TopDir, Files) :-
   exists_directory(TopDir),
   !,
   absolute_file_name(x, Absolute),
   file_directory_name(Absolute, CurrentDir),
   chdir(TopDir),
   expand_file_name('*', Files1),
   maplist( absolute_file_name,
      Files1, Files2 ),
   maplist( sourcefiles_single,
      Files2, Files ),
   chdir(CurrentDir).
sourcefiles_single(TopDir, TopDir).


/* latest_file(+Files, -Time) <-
        Time will contain the modification time of the youngest
        file in the list of filenames Files. */

latest_file(Files, Time) :-
   latest_file_rec(Files, 0, Time).
latest_file_rec([File | Files], CurrentTime, Time) :-
   time_file(File, FileTime),
   FileTime > CurrentTime,
   !,
   latest_file_rec(Files, FileTime, Time).
latest_file_rec([_File | Files], CurrentTime, Time) :-
   !,
   latest_file_rec(Files, CurrentTime, Time).
latest_file_rec([], Time, Time).


/* create_info_if(+Files) <-
        Collect the informations from the sourcefiles in the list
        Files. But ONLY IF it is necessary, that is the files to hold those
        informations are older than the youngest sourcefile or they don't
        exist. Uses create_info/1. */

create_info_if(Files) :-
   comments_file(CommentsFile),
   exists_file(CommentsFile),
   time_file(CommentsFile, CommentsTime),
   latest_file(Files, SourceTime),
   float(SourceTime) < float(CommentsTime),
   !.
create_info_if(Files) :-
   create_info(Files).


/* create_info(+Files) <-
        Collect the informations from the sourcefiles in the list
        Files. They are stored in result-files. Uses
        create_info_fail/1. */

create_info(Files) :-
   comments_file(CommentsFile),
   open_stream(CommentsFile, write, commentsfile_stream, [type(text)]),
   close(commentsfile_stream),
   create_info_rec(Files).

create_info_rec([File | Files]) :-
   !,
   ignore(create_info_fail(File)),
   garbage_collect,
   create_info_rec(Files).
create_info_rec([]).


/* create_info_fail(+File) <-
        Collect the informations from the sourcefile File. This predicate
        always fails. This is to prevent the global stack to overrun
        because of the large lists produced by read_source/3. (They are
        discarded from memory after the predicate has failed.) */

create_info_fail(File) :-
   \+ dont_create_info(File),
   writef('Collecting info from %w ...\n', [File]),
   flag(current_sourcefile_flag, _Old, File),
   read_source(File, Tokens, Comments),
%       writeln(user, read_source(File, Tokens, Comments)),
   !,
   create_info_fail_sm(Tokens),
   comments_file(CommentsFile),
   open_stream(CommentsFile, append, commentsfile_stream,
        [type(text), eof_action(eof_code)]),
   comments_file_separator(Sep),
   absolute_file_name(File, FileName),
   swritef(String, '%nFile: %w\n', [Sep, FileName]),
   write(commentsfile_stream, String),
   write_comments(Comments),
   close(commentsfile_stream),
   !,
   fail.

create_info_fail_sm(Tokens) :-
   flag(create_metrics_db_flag, yes, yes),
   parse_source(Tokens).
create_info_fail_sm(_Tokens).


/* write_comments(+Comments) <-
        Write a list of comments in the help-file-format. */

write_comments(Comments) :-
   current_output(OldStream),
   set_output(commentsfile_stream),
   write_comments_rec(Comments),
   set_output(OldStream).
write_comments_rec([[LineNumber, String] | Comments]) :-
   !,
   comments_file_separator(Sep),
   writef('%nLine: %w\n', [Sep, LineNumber]),
   string_to_list(String, Chars),
   ignore(write_comments_check_pred(Chars)),
   writef('%w\n', [String]),
   write_comments_rec(Comments).
write_comments_rec([_Comment | Comments]) :-
   write(user_output, 'ERROR: Comment has wrong format.'), nl,
   write_comments_rec(Comments).
write_comments_rec([]).


/* write_comments_check_pred(Chars) <-
        Tries to check if the character-list Chars contains a comment
        that describes the definition of a predicate. If so a mark
        is written into the help-file. Currently it recognises the
        following patterns:
          pred(args) <-
          pred(args) :-
          pred(args) :
          pred(args) (end of line) */

write_comments_check_pred([47, 42 | Chars]) :- % /*
   !,
   write_comments_check_pred_1(Chars, _PredChars).
write_comments_check_pred([37 | Chars]) :-   % %
   !,
   write_comments_check_pred_1(Chars, _PredChars).
write_comments_check_pred_1([32 | Chars], PredChars) :-   % space
   !,
   write_comments_check_pred_1(Chars, PredChars).
write_comments_check_pred_1([Char | Chars], [Char | PredChars]) :-
   in_pred(Char),
   !,
   write_comments_check_pred_2(Chars, PredChars),
   string_to_list(String, [Char | PredChars]),
   comments_file_separator(Sep),
   writef('%nPred: %w\n', [Sep, String]).
write_comments_check_pred_2([40 | Chars], []) :-   % (
   !,
   write_comments_check_pred_3(Chars).
write_comments_check_pred_2([32 | Chars], []) :-   % space
   !,
   write_comments_check_pred_4(Chars).
write_comments_check_pred_2([Char | Chars], [Char | PredChars]) :-
   in_pred(Char),
   !,
   write_comments_check_pred_2(Chars, PredChars).
write_comments_check_pred_3([41 | Chars]) :-   % )
   !,
   write_comments_check_pred_4(Chars).
write_comments_check_pred_3([_Char | Chars]) :-
   !,
   write_comments_check_pred_3(Chars).
write_comments_check_pred_4([60, 45 | _Chars]) :- !.   % <-
%write_comments_check_pred_4([58, 45 | _Chars]) :- !.  % :-   (see next clause)
write_comments_check_pred_4([58     | _Chars]) :- !.   % :
write_comments_check_pred_4([10     | _Chars]) :- !.   % line feed
write_comments_check_pred_4([13     | _Chars]) :- !.   % carriage return
write_comments_check_pred_4([               ]) :- !.
write_comments_check_pred_4([32 | Chars]) :-   % space
   !,
   write_comments_check_pred_4(Chars).

in_pred(Char) :- between_character(0'0, 0'9, Char).   % 0 ... 9
in_pred(Char) :- between_character(0'a, 0'z, Char).   % a ... z
in_pred(  95).   % _


/* current_sourcefile/1: Description see interface. */

:- signature(current_sourcefile(var)).

current_sourcefile(SourceFile) :-
   flag(current_sourcefile_flag, SourceFile, SourceFile).


/*** Initialization ***********************************************/


% :- check_sources.


:- flag(current_sourcefile_flag, _Old, unknown_file).
:- flag(create_metrics_db_flag, _Old, no).


/*** Testing / Debugging ******************************************/


/* write_time <-
        Write a time in the system-format as:
        Day.Month.Year Hour:Minute:Second */

write_time(Time) :-
   convert_time(Time, Year, Month, Day, Hour, Minute, Second, _Milli),
   writef('%w.%w.%w %w:%w:%w', [Day, Month, Year, Hour, Minute, Second]).


/* test_create_info <-
        Test this implementation. */

test_create_info :-
   sourcefiles(['sources', 'library'], Files),
   latest_file(Files, SourceTime),
   write('Date of youngest sourcefile: '),
   write_time(SourceTime),
   nl,
   comments_file(CommentsFile),
   time_file(CommentsFile, CommentsTime),
   write('Date of comments-file: '),
   write_time(CommentsTime),
   nl,
   %create_info_if(Files),
   create_info(['sources/dd_advisor/read_source']),
   nl.


/******************************************************************/


/* Emacs-Configuration: */

%%% Local Variables:
%%% mode: prolog
%%% End:
