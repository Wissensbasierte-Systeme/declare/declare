

/******************************************************************/
/***                                                            ***/
/***           DDA:  Parse Sources - Elementary                 ***/
/***                                                            ***/
/******************************************************************/


/* token_to_atom(Token, Atom) <-
      */

token_to_atom(Token, _) :-
   name(Token, [C|_]),
   member(C, "(),.;"),
   !,
   fail.
token_to_atom(Token, _) :-
   name(Token, Cs),
   append("->", _, Cs),
   !,
   fail.
token_to_atom(Token_1, Token_2) :-
   name(Token_1, Cs_1),
   append("'", Cs_2, Cs_1),
   !,
   append(Cs_3, "'", Cs_2),
   name(Token_2, Cs_3),
   ( atom(Token_2)
   ; number(Token_2) ).
token_to_atom(Token, Token) :-
   atom(Token).


/* test_for_op(Functor, Arguments) <-
      */

test_for_op(atom(op), [Prec, Type, Name]) :-
   \+ test_for_op_exist(Prec, Type, Name),
   swritef(String1,
      'WARNING: Possible definition of unknown operator\n', []),
   write(user_output, String1),
   swritef(String2, '         Precedence : %w\n', [Prec]),
   write(user_output, String2),
   swritef(String3, '         Type       : %w\n', [Type]),
   write(user_output, String3),
   swritef(String4, '         Name       : %w\n', [Name]),
   write(user_output, String4).
test_for_op(_Functor, _Arguments) :-
   !.

test_for_op_exist(number(Prec), atom(Type), atom(Name)) :-
   is_operator(Prec, Type, Name).


is_xfx_operator(Op, _) :-
   member(Op, [',', ';', '|', '->']),
   !,
   fail.
is_xfx_operator(Op, Prec) :-
   member(X, [xfx, xfy, yfx, yfy]),
   is_operator(Prec, X, Op).

is_fx_operator(Op, Prec) :-
   member(X, [fx, fy]),
   is_operator(Prec, X, Op).

is_xf_operator(Op, Prec) :-
   member(X, [xf, yf]),
   is_operator(Prec, X, Op).

is_operator(Prec, Type, Name) :-
   catch(
      ( current_op(Prec, Type, Name)
      ; op_for_dcg(Prec, Type, Name) ), _, fail ).


is_variable(Token) :-
   atom_chars(Token, [C|Cs]),
   ( C = 95                                 % 95 = _
   ; between_character(0'A, 0'Z, C) ),
   !,
   checklist( in_variable,
      Cs ).

in_variable(C) :-
   between_character(0'0, 0'9, C).
in_variable(C) :-
   between_character(0'A, 0'Z, C).
in_variable(C) :-
   between_character(0'a, 0'z, C).
in_variable(95).                            % _


between_character(N, M, Char) :-
   name(Char, [K]),
   between(N, M, K).

number_chars_save(Number, Chars) :-
   checklist( char_is_number,
      Chars ),
   number_chars(Number, Chars),
   !.

char_is_number(Char) :-
   name(Char, [N]),
   between(0'0, 0'9, N).


/******************************************************************/


