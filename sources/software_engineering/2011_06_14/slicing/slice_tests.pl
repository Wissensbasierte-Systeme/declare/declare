

/******************************************************************/
/***                                                            ***/
/***          Slicing:  Tests                                   ***/
/***                                                            ***/
/******************************************************************/


test(slicing:initalization, start_visur) :-
   visur.

test(slicing:rar_retrieval, 1) :-
   slice:necessary_files(
      module:slicing, N-Files),
   slice_test_output(N, Files).
test(slicing:rar_retrieval, 2) :-
   slice:necessary_files(
      predicate:(slice:slice)/6, N-Files),
   slice_test_output(N, Files).
test(slicing:rar_retrieval, 3) :-
   slice:necessary_files(
      module:visur_rar, N-Files),
   slice_test_output(N, Files).
test(slicing:rar_retrieval, 4) :-
   slice:necessary_files(
      file:fn_query, N-Files),
   slice_test_output(N, Files).
test(slicing:rar_retrieval, 5) :-
   slice:necessary_files(
      file:mine_sweeper_board, N-Files),
   slice_test_output(N, Files).
test(slicing:rar_retrieval, 6) :-
   slice:necessary_files(
      predicate:(user:create_board)/0, N-Files),
   slice_test_output(N, Files).
test(slicing:rar_retrieval, 7) :-
   slice:necessary_files(
      predicate:(user:(:=))/2, N-Files),
   slice_test_output(N, Files).

test(slicing:case_study, mine_sweeper) :-
   Predicates = [ (user:create_board)/0,
%     Buttons:
      (user:new_game)/0, (user:clear_board)/0,
      (user:show_board)/0,
      (user:mine_sweeper_decision_support)/1,
      (user:pp_ratio_information)/0 ],
   concat('/sources/nm_reasoning',
      '/disjunctive_consequence_operators/tps_delta', Tps_Delta),
   Fs = [
      '/library/loops.pl', '/library/ordsets.pl',
      '/library/lists_sicstus.pl',
      '/sources/basic_algebra/basics/specials_swi',
      '/sources/basic_algebra/basics/operators', Tps_Delta,
      '/sources/nm_reasoning/nmr_interfaces/smodels_interface',
      '/sources/basic_algebra/utilities/test_predicates' ],
   dislog_variable_get(home, DisLog),
   maplist( concat(DisLog),
      Fs, Extra_Files ),
   concat(DisLog, '/library/static_swi.pl', Static),
   Dynamic = [(user:test)/2],
   Multifile = [(user:test)/2],
   measure_runtime(slice:mine_sweeper, slice:slice(
      Predicates, Dynamic, Multifile,
      Extra_Files, [Static], Files, Preds) ),
   slice_test_output(Files-Preds).

test(slicing:case_study, field_notation) :-
   Predicates = [
      (user:(:=))/2, (user:add)/2, (user:add)/3,
      (user:average)/2, (user:dwrite)/2 ],
   Fs = ['/library/lists_sicstus.pl', '/library/loops.pl'],
   dislog_variable_get(home, DisLog),
   maplist( concat(DisLog),
      Fs, Extra_Files ),
   Dynamic = [(user:test)/2],
   Multifile = [(user:test)/2],
   measure_runtime(slice:field_notation, slice:slice(
      Predicates, Dynamic, Multifile,
      Extra_Files, [], Files, Preds) ),
   slice_test_output(Files-Preds).

test(slicing:case_study, test) :-
   Predicates = [ (user:test)/2-[field_notation:_, _] ],
   Fs = ['/library/lists_sicstus.pl', '/library/loops.pl'],
   dislog_variable_get(home, DisLog),
   maplist( concat(DisLog),
      Fs, Extra_Files ),
   Dynamic = [(user:test)/2],
   Multifile = [(user:test)/2],
   measure_runtime(slice:test, slice:slice(
      Predicates, Dynamic, Multifile,
      Extra_Files, [], Files, Preds) ),
   slice_test_output(Files-Preds).

test(slicing:case_study, slice) :-
   Predicates = [(slice:slice)/6],
%     (slice:rar_transitive_closure)/2
%     (slice:slice_package)/4,
%     (slice:necessary_files)/2,
%     (slice:rar_transitive_closure)/2,
%     (user:rgb_to_colour)/2
   slice:slice(
      Predicates, [], [],
      [], [], Files, Preds),
   slice_test_output(Files-Preds).


slice_test_output(Files-Preds) :-
   length(Preds, N),
   slice:get_amount_of_predicates_in_files(Files, Amount),
   writeln_list(Files),
   length(Files, M),
   write('Files':M),
   write(', Predicates':Amount),
   writeln(', Necessary Predicates':N).

slice_test_output(N, Files) :-
   writeln_list(Files),
   length(Files, M),
   writeln('Iterations':N),
   writeln('Files':M).


/******************************************************************/


