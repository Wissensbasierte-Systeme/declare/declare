

/******************************************************************/
/***                                                            ***/
/***          Slicing:  Necessary Files                         ***/
/***                                                            ***/
/******************************************************************/


:- module(slice, [
      copy_file_to_file/2,
      copy_file_to_file/4 ]).


/*** interface ****************************************************/


/* slice(+Predicates, +Dynamic, +Multifile, +Files, +Libraries,
         -Necessary_Files, -Necessary_Predicates) <-
      */

slice(Predicates, Dynamic, Multifile, Files, Libraries,
      Necessary_Files, Necessary_Predicates) :-
   writeln(user, 1),
   slice_predicates_to_necessary_predicates_and_files(
      Predicates, Necessary_Predicates, Necessary_Files_A),
   union(Files, Necessary_Files_A, Necessary_Files_B),
   writeln(user, 2),
   rar_global_variables:necessary_predicates_to_files(
      dynamic, Necessary_Files_B, Fs_1),
   writeln(user, 3),
   rar_global_variables:necessary_predicates_to_files(
      multifile, Necessary_Files_B, Fs_2),
   writeln(user, 4),
   necessary_predicates_to_envelope_pairs_and_files(
      Necessary_Predicates, Fs_3),
   writeln(user, 5),
   rar_global_variables:current_num_to_files(
      Necessary_Predicates, Fs_4),
   union([Necessary_Files_B, Fs_1, Fs_2, Fs_3, Fs_4], Fs_5),
   order_necessary_files(Fs_5, Fs_6),
   union(Files, Fs_6, Slice_Files),
   append(Libraries, Slice_Files, Necessary_Files),
   writeln(user, 6),
   slice_package(Dynamic, Multifile, Slice_Files, Libraries).

slice_predicates_to_necessary_predicates_and_files(
      Predicates, Necessary_Predicates, Necessary_Files) :-
   rar_variable_set(preds, []),
   ( foreach(Predicate, Predicates),
     foreach(Fs, List_of_Fs) do
        ( slice:necessary_files(predicate:Predicate, _-Fs-Ps),
          rar_variable_get(preds, Ps_1),
          union(Ps_1, Ps, Ps_2),
          rar_variable_set(preds, Ps_2) ) ),
   rar_variable_get(preds, Necessary_Predicates),
   union(List_of_Fs, Necessary_Files).

necessary_predicates_to_envelope_pairs_and_files(
      Necessary_Predicates, Variable_Files) :-
   Envelope_Pairs = [
      (user:get_num)/2-(user:set_num)/2,
      (user:dislog_flag_get)/2-(user:dislog_flag_set)/2,
      (user:rar_variable_get)/2-(user:rar_variable_set)/2,
      (user:dislog_variable_get)/2-(user:dislog_variable_set)/2 ],
   rar_global_variables:envelope_pairs_to_files_2(
      Necessary_Predicates, Envelope_Pairs, Variable_Files).


/*** implementation ***********************************************/


/* order_necessary_files(Files_1, Files_2) <-
      */

order_necessary_files(Files_1, Files_2) :-
   dislog_variable_get(source_path, Sources),
   dislog_sources(Files),
   ( foreach(File, Files), foreach(Path, Paths) do
        concat(Sources, File, Path) ),
   intersection(Paths, Files_1, List),
   list_to_set(List, Files_2).


/* slice_package(Dynamic, Multifile, Slice_Files, Libraries) <-
      */

slice_package(Dynamic, Multifile, Slice_Files, Libraries) :-
   slice_package_prepare(Slice_Dir, Library_Dir),
   dislog_variable_get(home, DisLog),
   concat_atom([DisLog, '/'], DisLog_Path),
   maplist( copy_file_to_file(DisLog_Path, Slice_Dir),
      Slice_Files, Target_Files ),
%  tar_files(Slice_Dir, Slice_Files),
   ( foreach(Library, Libraries) do
        copy_file_to_file(Library, Library_Dir) ),
   concat(Slice_Dir, '/slice_package.pl', Slice_File),
   predicate_to_file( Slice_File,
      ( slice:slice_header(Dynamic, Multifile),
        checklist( slice:write_consult_line_for_slicing,
           Target_Files ) ) ),
   write_browser_hierarchy_1(Target_Files, Slice_Dir),
   write_browser_hierarchy_2(Slice_Dir, Target_Files).

write_browser_hierarchy_1(Target_Files, Slice_Dir) :-
   ( foreach(Target_File, Target_Files),
     foreach(XML_File, XML_Files) do
        concat_atom([Slice_Dir, '/', Target_File], File_Path),
        XML_File = file:[name:File_Path]:[] ),
   concat(Slice_Dir, '/slice_hierarchy_1.xml', XML_Hierarchy),
   write_structure(XML_Hierarchy,
      module:[name:'slice']:XML_Files, [dialect(xml)]).

write_browser_hierarchy_2(Slice_Dir, Slice_Files) :-
   slice_to_source_hierarchy(Slice_Files, Tree),
   concat(Slice_Dir, '/slice_hierarchy_2.xml', XML_Hierarchy),
   !,
   write_structure(XML_Hierarchy, Tree, [dialect(xml)]).


/* slice_to_source_hierarchy(Slice_Files, Tree) <-
      */

slice_to_source_hierarchy(Slice_Files, Tree) :-
   findall( unit:[name:Unit]:Terms,
      ( dislog_unit(Unit, Modules),
        maplist( rar_module_to_fn_term(Slice_Files),
           Modules, Terms ) ),
      Units ),
   Tree = system:[name:dislog]:Units.

rar_module_to_fn_term(Slice_Files, Module, Term) :-
   dislog_module(Module, _, Files),
   maplist( correct_path,
      Slice_Files, S_Files ),
   intersection(Files, S_Files, H_Files),
   maplist( rar_file_to_fn_term,
      H_Files, Terms ),
   Term = module:[name:Module]:Terms.

rar_file_to_fn_term(File, file:[name:Path]:[]) :-
   dislog_variable_get(source_path, Sources),
   concat(Sources, File, Path_2),
   ( ( exists_file(Path_2),
       Path = Path_2,
       ! )
   ; file_name_extension(Path_2, pl, Path) ),
   !.

correct_path(A, B) :-
   concat('sources/', B, A),
   !.
correct_path(A, A).


/* slice_package_prepare(Slice_Dir, Library_Dir) <-
      */

slice_package_prepare(Slice_Dir, Library_Dir) :-
   rar_variable_get(slice, Directory),
   make_directory_recursive(Directory),
   tmp_file(slice, File_1),
   file_base_name(File_1, File_2),
   concat_atom([Directory, '/', File_2], Slice_Dir),
   make_directory_recursive(Slice_Dir),
   concat(Slice_Dir, '/library', Library_Dir),
   make_directory_recursive(Library_Dir).


/* slice_header(Dynamic, Multifile) <-
      */

slice_header(Dynamic, Multifile) :-
   slice_write(dynamic, Dynamic),
   slice_write(multifile, Multifile).

slice_write(_, []).
slice_write(Type, Elts) :-
   write(':- '),
   write(Type),
   write(' '),
   slice_write(Elts).

slice_write([(M:P)/A]) :-
   write(M:P/A),
   writeln('.').
slice_write([(M:P)/A|Es]) :-
   write(M:P/A),
   write(', '),
   slice_write(Es).
slice_write([]).


/* write_consult_line_for_slicing(File) <-
      */

write_consult_line_for_slicing(File) :-
   write(':- consult('), writeq(File), writeln(').').
%  rar_saving:write_term_with_dot(
%     :-  consult(File) ).


/* necessary_predicates(MPA, Iteration-MPAs) <-
      */

necessary_predicates(predicate:MPA, Iteration-MPAs) :-
   !,
   rar_transitive_closure([MPA], Iteration-Preds_Temp),
   ord_union([MPA], Preds_Temp, MPAs).


/* necessary_files(Level:Name, Iteration-Files) <-
      */

necessary_files(
      predicate:Name-Parameters, Iteration-Files-Preds_3) :-
   !,
   findall( File,
      rar_retrieval:predicate_to_file(Name, Parameters, File),
      Files_T ),
   rar_transitive_closure([Name-Parameters], Iteration-Preds),
   union([Name], Preds, Preds_2),
   list_to_ord_set(Preds_2, Preds_3),
   findall( F,
      ( member(P_1, Preds),
        rar_retrieval:contains_lp(file:F, P_1) ),
      Files_Temp ),
   union(Files_T, Files_Temp, Files_Temp_1),
   list_to_ord_set(Files_Temp_1, Files).
necessary_files(predicate:Name, Iteration-Files-Preds) :-
   !,
   rar_transitive_closure([Name], Iteration-Preds_Temp),
   ord_union([Name], Preds_Temp, Preds),
   findall( F,
      ( member(P_1, Preds),
        rar_retrieval:contains_lp(file:F, P_1) ),
      Files_Temp ),
   list_to_ord_set(Files_Temp, Files).
necessary_files(Level:Name, Iteration-Files-Preds) :-
   called_predicates(Level:Name, Called_Predicates),
   rar_transitive_closure(
      Called_Predicates, Iteration-Preds_Temp),
   ord_union(Called_Predicates, Preds_Temp, Preds_0),
   list_to_set(Preds_0, Preds),
   findall( F,
      ( member(P_1, Preds),
        rar_retrieval:contains_lp(file:F, P_1) ),
      Files_Temp ),
   list_to_ord_set(Files_Temp, Files).


/* rar_transitive_closure(Called_Predicates, N-Predicates) <-
      */

rar_transitive_closure(Called_Predicates, N-Predicates) :-
   rar_tc_step(Called_Predicates, 0-[], N-Predicates).

rar_tc_step([], N-Predicates, N-Predicates) :-
   !.
rar_tc_step(Called_Predicates,
      N1-Predicates_1, N2-Predicates_2) :-
   maplist( rar_tc_step_sub,
      Called_Predicates, Ps_1 ),
   ord_union(Ps_1, Predicates_0),
   list_to_set(Predicates_0, Predicates),
   N3 is N1 + 1,
   subtract(Predicates, Predicates_1, Called_Predicates_New),
   ord_union(
      Predicates_1, Called_Predicates_New, Predicates_3),
   list_to_set(Predicates_3, Predicates_4),
   rar_tc_step(Called_Predicates_New,
      N3-Predicates_4, N2-Predicates_2).

rar_tc_step_sub(Predicate-Parameter, Called_Predicates) :-
   !,
   findall( Predicate_2,
      rar_retrieval:calls_pp(Predicate, Parameter, Predicate_2),
      Predicates_2 ),
   list_to_ord_set(Predicates_2, Called_Predicates).
rar_tc_step_sub(Predicate, Called_Predicates) :-
   findall( Predicate_2,
      rar_retrieval:calls_pp(Predicate, Predicate_2),
      Predicates_2 ),
   list_to_ord_set(Predicates_2, Called_Predicates).


/* called_predicates(Type:Name, Called_Predicates) <-
      */

called_predicates(file:Name, Called_Predicates) :-
   !,
   file_to_predicates(Name, Called_Predicates).
called_predicates(Level_1:Name_1, Called_Predicates) :-
   level_to_predicates(Level_1:Name_1, Called_Predicates).

file_to_predicates(Name, Called_Predicates) :-
   rar_retrieval:get_full_file_name(Name, Name_1),
   findall( P_1,
      rar_retrieval:contains_fp(Name_1, P_1),
      Call_List_Temp ),
   list_to_ord_set(Call_List_Temp, Called_Predicates).

level_to_predicates(Level_1:Name_1, Called_Predicates) :-
   findall( P_1,
      rar_retrieval:contains_lp(Level_1:Name_1, P_1),
      Call_List_Temp ),
   list_to_ord_set(Call_List_Temp, Called_Predicates).


/* copy_file_to_file(Directory, Source_File, Target_File) <-
      */

copy_file_to_file(DisLog_Source_Path,
      Directory, Source_File, Target_File) :-
   concat(DisLog_Source_Path, Target_File, Source_File),
   concat([Directory, '/', Target_File], Target_Path),
   copy_file_to_file(Source_File, Target_Path).

copy_file_to_file(File_1, File_2) :-
   file_directory_name(File_2, Directory),
   make_directory_recursive(Directory),
   concat(['cp ', File_1, ' ', File_2], Command),
   shell(Command).


/* tar_files(Source, Source_Files) <-
      */

tar_files(Source, Source_Files) :-
   file_name_extension(Source, tgz, Tgz_File),
   maplist( add_space,
      Source_Files, Files_with_Space ),
   concat(Files_with_Space, Files),
   concat(['tar czf ', Tgz_File, ' ', Files], Command),
   writeq(Command).
%  shell(Command).

add_space(X, Y) :-
   concat_atom([X, ' '], Y).


/* get_amount_of_predicates_in_files(Files, Amount) <-
      */

get_amount_of_predicates_in_files(Files, Amount) :-
   maplist( get_amount_of_predicates_in_file,
      Files, Amounts ),
   add(Amounts, Amount).

get_amount_of_predicates_in_file(File, Amount) :-
   findall( P,
      ( rar_dml:select(rule, (_M:P)/_A, File, _) ),
%       atomic(P),
%       not(atom_prefix(P, no_head)) ),
      Predicates ),
   length(Predicates, Amount).


/******************************************************************/


