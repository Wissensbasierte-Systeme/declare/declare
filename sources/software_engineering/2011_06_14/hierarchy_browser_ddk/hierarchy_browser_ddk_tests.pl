

/******************************************************************/
/***                                                            ***/
/***      Hierarchy Browser: Tests                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(hierarchy_browser_ddk, 0) :-
   dislog_variable_get(source_path, Home),
   concat([Home, 'development/gxl/tests/hierarchy_gxl.xml'],
      File),
   dread(xml, File, [XML]),
   hierarchy_to_browser_ddk(XML, _).
test(hierarchy_browser_ddk, 1) :-
   file_system_to_xml('~/DisLog/', XML),
   hierarchy_to_browser_ddk(XML, _).
test(hierarchy_browser_ddk, 2) :-
   rar:dislog_sources_to_hierarchy(Tree_1),
   hierarchy_to_browser_ddk(Tree_1, Explorer),
   load_rar_gxl_visur_config(Config),
   Tree_2 =
   system:[name:'System']:[
      unit:[name:unit_1]:[
         module:[name:module_1]:[
            file:[name:file_1]:[],
            file:[name:file_2,
                  open:'16x16/ghost.xpm',
                  close:'16x16/ghost.xpm',
                  path:'~/DisLog/sources/dislog']:[],
            file:[name:file_3,
                  open:'16x16/binocular.xpm',
                  close:'16x16/binocular.xpm',
                  mouse_click:'individual']:[],
            file:[name:file_4]:[]
         ],
         module:[name:module_2,
               open:'16x16/new.xpm',
               close:'16x16/font.xpm']:[],
         module:[name:module_3]:[]
      ],
      unit:[name:unit_2,
            open:'16x16/ghost.xpm',
            close:'16x16/foot.xpm']:[
         module:[name:module_4]:[]
      ]
   ],
   hierarchy_to_browser_ddk(Config, Tree_2, Explorer).
test(hierarchy_browser_ddk, 3) :-
   load_rar_gxl_visur_config(Config),
   rar:dislog_to_source_hierarchy([Tree]),
   hierarchy_to_browser_ddk(Config, Tree, _Explorer).


/*** implementation ***********************************************/


/******************************************************************/


