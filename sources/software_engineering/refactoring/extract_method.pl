

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Refactoring Tool                         ***/
/***                                                            ***/
/******************************************************************/


dislog_variable(refactoring_path, Refactoring) :-
   dislog_variable_get(home, DisLog),
   concat(DisLog, '/projects/Refactoring/', Refactoring).


/*** interface ****************************************************/


/* extract_method_from_file(
         Pattern_File, Prolog_File, Xml_File) <-
      */

extract_method_from_file(
      Pattern_File, Prolog_File, Xml_File) :-
   file_to_refactoring_rule(Pattern_File, Rule),
   A := Rule^head,
   fn_item_parse(A, head:[]:Head),
   B := Rule^body,
   fn_item_parse(B, body:[]:Body),
   program_file_to_xml(Prolog_File, Program),
   fn_item_parse(Program, file:_:Rules_1),
%  maplist( extract_method_from_rule(Head-Body),
%     Rules_1, Rules_2 ),
   findall( Rule_2,
      ( member(Rule_1, Rules_1),
        extract_method_from_rule(Head-Body, Rule_1, Rule_2) ),
      Rules_2 ),
   dwrite(xml, Xml_File, file:Rules_2).


/*** implementation ***********************************************/


/* extract_method_from_rule(Head-Body, Rule_1, Rule_2) <-
      */

extract_method_from_rule(Head-Body, Rule_1, Rule_2) :-
   Head_1 := Rule_1^head,
   Body_1 := Rule_1^body,
   fn_item_parse(Body_1, body:[]:Atoms_1),
   replace_sublist(Head-Body, Atoms_1, Atoms_2),
   Body_2 = body:[]:Atoms_2,
   Rule_2 = rule:[operator:':-']:[Head_1, Body_2],
   !.
extract_method_from_rule(_, Rule, Rule).


/* file_to_refactoring_rule(File, Xml) <-
      */

file_to_refactoring_rule(File, Xml) :-
   predicate_from_file( File,
      read_term(Rule, [
         variable_names(Variables_1),
         syntax_errors(dec10) ]) ),
   maplist( variable_assignment_transform,
      Variables_1, Variables_2 ),
   rule_and_variables_to_head_and_body(
      Rule-Variables_2, Head_and_Body),
   rule_and_variables_to_xml(Head_and_Body, Xml_2),
   dwrite(xml, Xml_2),
   substitute_recursively(
      [[var:[name:V]:[], V]], Xml_2, Xml).

variable_assignment_transform(_=X, X=X).


/* replace_sublist(Us-Xs, Ys, Zs) <-
      */

replace_sublist(Us-Xs, Ys, Zs) :-
   sublist(Xs, Ys),
   append(As, Bs, Ys),
   append(Xs, Cs, Bs),
   append([As, Us, Cs], Zs).


/*** tests ********************************************************/


test(refactoring, extract_method_from_file) :-
   dislog_variable_get(refactoring_path, Refactoring),
   concat(Refactoring, 'Example/', Dir),
   concat(Dir, 'pattern.pl', Pattern_File),
   concat(Dir, 'test.pl', Prolog_File),
   concat(Dir, 'test_refac.pl', Prolog_File_Refac),
   concat(Dir, 'test_refac.xml', Xml_File),
   extract_method_from_file(
      Pattern_File, Prolog_File, Xml_File),
   xml_file_to_prolog(Xml_File, Prolog_File_Refac).


/******************************************************************/


