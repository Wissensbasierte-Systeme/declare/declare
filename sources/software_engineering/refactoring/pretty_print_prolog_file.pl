

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Pretty Print Prolog File                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* pretty_print_prolog_file(File_1, File_2) <-
      nested comments are not allowed in File_1. */

pretty_print_prolog_file(File_1, File_2) :-
   Tmp_1 = 'results/pretty_print_tmp_1.pl',
   Tmp_2 = 'results/pretty_print_tmp_2.pl',
   prolog_file_wrapper(ddk_wrap_string, File_1, Tmp_1),
   program_file_to_xml(Tmp_1, Xml),
   xml_to_prolog([Xml], Tmp_2),
   prolog_file_wrapper(ddk_unwrap_string, Tmp_2, File_2).


/*** implementation ***********************************************/


/* prolog_file_wrapper(Predicate, File_1, File_2) <-
      */

prolog_file_wrapper(Predicate, File_1, File_2) :-
   write(user, '<--- '), writeln(user, File_1),
   read_file_to_string(File_1, String_1),
   apply(Predicate, [String_1, String_2]),
   name(Name, String_2),
   write(user, '---> '), writeln(user, File_2),
   predicate_to_file( File_2,
      write(Name) ).

ddk_wrap_string(String_1, String_2) :-
   list_split_at_position(["\n"], String_1, Strings_1),
   list_exchange_sublist([[[""], ["ddk_empty_line."]]],
      Strings_1, Strings_a),
   list_split_at_position(["\n"], String_a, Strings_a),
   Substitution = [
      ["/*", "ddk_comment('COMMENT"],
      ["*/", "COMMENT')."] ],
   list_exchange_sublist(Substitution, String_a, String_b),
   list_exchange_sublist(Substitution, String_b, String_2).

ddk_unwrap_string(String_1, String_2) :-
   list_split_at_position(["\n"], String_1, Strings_1),
   list_exchange_sublist([[[""], []]],
      Strings_1, Strings_a),
   list_split_at_position(["\n"], String_a, Strings_a),
   Substitution_1 = [["\\n", "\n"]],
   list_exchange_sublist(Substitution_1, String_a, String_b),
   Substitution_2 = [
      ["ddk_empty_line.", ""], 
      ["ddk_comment('COMMENT", "/*"],
      ["COMMENT').", "*/"] ],
   list_exchange_sublist(Substitution_2, String_b, String_2).


/*** tests ********************************************************/


test(refactoring, pretty_print_prolog_file) :-
   pretty_print_prolog_file(
      'projects/Refactoring/Pretty_Print/t1',
      'projects/Refactoring/Pretty_Print/t2' ).


/******************************************************************/


