:- module(pa_clause, [
   clause_head/2,
   clause_body/2,
   clause_file/2
]).

/**
 * Module for handling and analyzing clauses.
 */

/*** tests ********************************************************/
test(clause_head, 1) :-
   clause_head(trace/0, Head),
   Head == trace.
test(clause_head, 2) :-
   clause_head(user:trace/0, Module:Head),
   Module:Head == system:trace.
test(clause_head, 3) :-
   clause_head(PredInd, trace),
   PredInd == system:trace/0.
test(clause_head, 4) :-
   clause_head(PredInd, user:trace),
   PredInd == system:trace/0.
test(clause_head, 5) :-
   clause_head(Ref, findall(_, _, _)),
   !,
   clause_head(Ref, _:H),
   H = findall(_, _, _).

test(clause_file, 1) :-
   clause_head(ClauseRef, test(clause_file, 1)),
   clause_file(ClauseRef, _).

/*** interface ****************************************************/

/* clause_head(?ClauseRef, :Head) <-
   Unifies Head with the head of the clause belonging to ClauseRef. */
:- meta_predicate clause_head(?, 0).
clause_head(ClauseRef, Head) :-
   compound(ClauseRef),
   ground(ClauseRef),
   !,
   predicate_indicator(ClauseRef, Head).
clause_head(ClauseRef, Module:Head) :-
   nonvar(Head),
   predicate_private(Module:Head),
   !,
   predicate_indicator(ClauseRef, Module:Head).
clause_head(ClauseRef, Head) :-
   nth_clause(Head, _, ClauseRef).

/* clause_body(+ClauseRef, -Body) <-
   Unifies Body with the body belonging to ClauseRef. */
clause_body(ClauseRef, Body) :-
   catch( clause(_, Body, ClauseRef),
      _,
      Body = _ ).

/* clause_file(+ClauseRef, -File) <-
   Unifies File with the filename ClauseRef is defined in.
   */
clause_file(ClauseRef, File) :-
   ground(ClauseRef),
   !,
   catch( clause_property(ClauseRef, file(File)),
      _,
      fail ).
clause_file(ClauseRef, File) :-
   source_file(Head, File),
   nth_clause(Head, _, ClauseRef),
   clause_property(ClauseRef, file(File)).