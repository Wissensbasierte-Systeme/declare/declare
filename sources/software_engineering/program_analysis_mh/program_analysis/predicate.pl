:- module(pa_predicate, [
   predicate_indicator/2,
   predicate_private/1,
   current_predicate_filter/2,
   filter_predicate_by_source/2,
   called_with_same_arg/2,
   clear_arguments/2
]).

/**
 * Module for handling predicates.
 */

/*** tests ********************************************************/
test(predicate_indicator, 1) :-
   predicate_indicator((:)/2, _:_).
test(predicate_indicator, 2) :-
   predicate_indicator(findall/3, Head),
   Head = findall(_, _, _).
test(predicate_indicator, 3) :-
   predicate_indicator(user:findall/3, M:Head),
   M:Head = '$bags':findall(_, _, _).
test(predicate_indicator, 4) :-
   predicate_indicator(PredInd, findall(_, _, _)),
   PredInd = '$bags':findall/3.
test(predicate_indicator, 5) :-
   predicate_indicator(PredInd, user:findall(_, _, _)),
   PredInd = '$bags':findall/3.

test(predicate_private, 1) :-
   predicate_private(;).
test(predicate_private, 2) :-
   \+ predicate_private(predicate_private(_)).

/*** interface ****************************************************/

/* predicate_indicator(?PredIndicator, -Head) <-
   True, if PredIndicator is a predicate indicator.
   Head will be unified with the corresponding head. */
:- meta_predicate predicate_indicator(?, 0).
predicate_indicator(ModuleF:Functor/Arity, ModuleH:Head) :-
   !,
   module_functor(ModuleH:Head, ModuleF:Functor, Arity).
predicate_indicator(Functor/Arity, Head) :-
   module_functor(Head, Functor, Arity).

/* predicate_private(+Head) <-
   True, if Head is bound to a private predicate.
   Using the existance of a source file is faster than checking
   predicate_property(_, foreign) */
:- meta_predicate predicate_private(0).
predicate_private(user:A:B) :-
   var(A),
   var(B),
   !.
predicate_private(Head) :-
   catch( \+ source_file(Head, _),
      _,
      true ).

/* current_predicate_filter(+FilterPred, -Pred) <-
   Backtrack over all predicates matching a filter. */
:- meta_predicate current_predicate_filter(1, 0).
current_predicate_filter(_:true, Pred) :-
   !,
   current_predicate(_, Pred).
current_predicate_filter(FilterPred, Pred) :-
   var(FilterPred),
   !,
   current_predicate_filter(true, Pred).
current_predicate_filter(FilterPred, Pred) :-
   current_predicate(_, Pred),
   call(FilterPred, Pred).

/* filter_predicate_by_source(+Path, -Pred) <-
   Filters predicates based on the path of their source file. */
:- meta_predicate filter_predicate_by_source(+, 0).
filter_predicate_by_source(Path, Pred) :-
   catch( source_file(Pred, File),
      _,
      false ),
   name_append(Path, _, File),
   \+ predicate_property(Pred, imported_from(_)).

/* called_with_same_arg(Head1, Head2) <-
   True, if Head1 and Head2 have at least one argument in common.
   */
:- meta_predicate called_with_same_arg(0, 0).
called_with_same_arg(_:Head1, _:Head2) :-
   arg(_, Head1, Arg1),
   arg(_, Head2, Arg2),
   Arg1 == Arg2,
   !.

/* clear_arguments(+Head, -NewHead) <-
   Replace all arguments of Head with variables.
   */
:- meta_predicate clear_arguments(0, 0).
clear_arguments(Module:Head, Module:NewHead) :-
   functor(Head, Functor, Arity),
   length(Args, Arity),
   NewHead =.. [Functor|Args].

/*** implementation ***********************************************/
