:- module(pa_ddk, [
   ddk_unit_interface/2,
   ddk_module_interface/3,
   ddk_unit_dependencies/2,
   ddk_module_dependencies/3,
   ddk_unit_calls_unit/2,
   ddk_module_calls_module/2
]).

/**
 * Module for analyzing structures of the DDK.
 */

/*** tests ********************************************************/
   

/*** interface ****************************************************/

/* ddk_unit_interface(?Unit, -Interface) <-
   Get the Interface of Unit of the DDK.
   Allows backtracking over all units.
   */
ddk_unit_interface(Unit, Interface) :-
   ddk_unit_classifier(M:Classifier),
   pa_structure:interface(M:Classifier, Unit, Interface).

/* ddk_module_interface(?Unit, ?Module, -Interface) <-
   Get the Interface of Module in Unit of the DDK.
   Allows backtracking over all units/modules.
   */
ddk_module_interface(Unit, Module, Interface) :-
   ddk_module_classifier(M:Classifier),
   interface(M:Classifier, Unit:Module, Interface).

/* ddk_unit_dependencies(?Unit, -Dependencies) <-
   Unifies Dependencies with the list of dislog units Unit directly depends on.
   Allows backtracking over all units.
   */ 
ddk_unit_dependencies(Unit, Dependencies) :-
   ddk_unit_classifier(M:Classifier),
   pa_structure:dependencies(M:Classifier, Unit, Dependencies).

/* ddk_module_dependencies(?Unit, ?Module, -Dependencies) <-
   Unifies Dependencies with a list of DepUnit:DepModule that Module in Unit
   directly depends on.
   Allows backtracking over all units/modules.
   */
ddk_module_dependencies(Unit, Module, Dependencies) :-
   ddk_module_classifier(M:Classifier),
   dependencies(M:Classifier, Unit:Module, Dependencies).

/* ddk_unit_calls_unit(?Caller, -Callee) <-
   Unifies Callee with the unit that is called from unit Caller.
   Returns all called units by backtracking.
   */
ddk_unit_calls_unit(Caller, Callee) :-
   ddk_unit_classifier(M:Classifier),
   calls(M:Classifier, Caller, Callee).

/* ddk_module_calls_module(?Caller, -Callee) <-
   Unifies Callee with the Unit:Module that is called from module Caller.
   Returns all called modules by backtracking.
   */
ddk_module_calls_module(Caller, Callee) :-
   ddk_module_classifier(M:Classifier),
   calls(M:Classifier, Caller, Callee).   

/*** implementation ***********************************************/

/* ddk_module_classifier(-Classifier) <-
   Generates a module classifier for the clauses of the DDK.
   See: classify_clause_to_ddk_module/3
   */
:- meta_predicate ddk_module_classifier(2).
ddk_module_classifier(Classifier) :-
   ddk_file_module_map(Mapping),
   Classifier = pa_ddk:classify_clause_to_ddk_module(Mapping).

/* classify_clause_to_ddk_module(+Mapping, +ClauseRef, -Unit:Module) <-
   Unifies Unit:Module with the unit and module where the clause ClauseRef is defined
   in the DDK.
   Don't use this predciate directly but generate it using ddk_module_classifier/1.
   */
classify_clause_to_ddk_module(Mapping, ClauseRef, Unit:Module) :-
   ground(ClauseRef),
   !,
   catch( clause_property(ClauseRef, file(File)),
      _,
      fail ),
   get_assoc(File, Mapping, Unit:Module).

/* ddk_unit_classifier(-Classifier) <-
   Generates a unit classifier for the clauses of the DDK.
   See: classify_clause_to_ddk_unit/3
   */
:- meta_predicate ddk_unit_classifier(2).
ddk_unit_classifier(Classifier) :-
   ddk_file_module_map(Mapping),
   Classifier = pa_ddk:classify_clause_to_ddk_unit(Mapping).

/* classify_clause_to_ddk_unit(+Mapping, +ClauseRef, -Unit) <-
   Unifies Unit with the unit where the clause ClauseRef is defined in the DDK.
   Don't use this predciate directly but generate it using ddk_unit_classifier/1.
   */
classify_clause_to_ddk_unit(Mapping, ClauseRef, Unit) :-
   ground(ClauseRef),
   !,
   catch( clause_property(ClauseRef, file(File)),
      _,
      fail ),
   get_assoc(File, Mapping, Unit:_).

/* ddk_file_module_map(-Mapping) <-
   Get Mapping of dislog File and the Unit:Module it belongs to.
   */
ddk_file_module_map(Mapping) :-
   dislog_variable(source_path, SourcePath),
   findall( ModuleFileAbs-(Unit:Module),
      ( dislog_module(Module, _, ModuleFiles),
        dislog_module_to_unit(Module, Unit),
        member(File, ModuleFiles),
        source_file_extension(File, ModuleFile),
        name_append(SourcePath, ModuleFile, ModuleFileAbs),
        source_file(ModuleFileAbs) ),
      Pairs ),
   list_to_assoc(Pairs, Mapping).