:- module(pa_source, [
   source_file_extension/2
]).

/**
 * Module for handling source files.
 */

/*** interface ****************************************************/

/* source_file_extension(Base, Name) <-
   Unifies Name with Base extended by all defined prolog file extensions. */
source_file_extension(Base, Base).
source_file_extension(Base, Name) :-
   prolog_file_type(Extension, prolog),
   file_name_extension(Base, Extension, Name).