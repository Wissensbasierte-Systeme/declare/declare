:- module(pa_module_safe, [
   module_functor/3,
   module_current_predicate/2,
   module_arg/3,
   module_callable/1,
   original_head/2
]).

/**
 * Module containing predicates for safe handling of qualified predicates.
 */

/*** tests ********************************************************/

test(module_functor, 1) :-
   module_functor(help, F, A),
   F = online_help:help,
   A = 0.
test(module_functor, 2) :-
   module_functor(user:help, F, A),
   F = online_help:help,
   A = 0.
test(module_functor, 3) :-
   module_functor(H, help, 0),
   H = help.
test(module_functor, 4) :-
   module_functor(M:H, user:help, 0),
   H = help,
   M = online_help.

test(module_current_predicate, 1) :-
   once(module_current_predicate(_, _)).
test(module_current_predicate, 2) :-
   module_current_predicate(module_current_predicate, _).
test(module_current_predicate, 3) :-
   module_current_predicate(pa_module_safe:module_current_predicate, _:_).
test(module_current_predicate, 4) :-
   module_current_predicate(_, module_current_predicate(_, _)).

/*** interface ****************************************************/

/* module_functor(?Head, ?Functor, ?Arity) <-
   Module safe version of predicate functor/3.
   */
:- meta_predicate module_functor(0, ?, ?).
module_functor(_:Head, system:':', 2) :-
   var(Head),
   !.
module_functor(Module:Head, SrcModule:Functor, Arity) :-
   ground(SrcModule),
   functor(Head, Functor, Arity),
   original_module(SrcModule:Head, Module),
   !.
module_functor(Module:Head, SrcModule:Functor, Arity) :-
   functor(Head, Functor, Arity),
   original_module(Module:Head, SrcModule),
   !.
module_functor(Module:Head, Module:Functor, Arity) :-
   !,
   functor(Head, Functor, Arity).
module_functor(_:Head, Functor, Arity) :-
   functor(Head, Functor, Arity).

/* module_current_predicate(?Name, ?Head) <-
   Module safe version of predicate current_predicate/2.
   */
:- meta_predicate module_current_predicate(?, 0).
module_current_predicate(Module:Functor, Module:Head) :-
   !,
   current_predicate(Functor, Module:Head).
module_current_predicate(Functor, Head) :-
   current_predicate(Functor, Head).

/* meta_predicate module_arg(?Arg, +Head, ?Value) <-
   Module save version of arg/3.
   */
:- meta_predicate module_arg(?, 0, ?).
module_arg(Arg, _:Head, Value) :-
   arg(Arg, Head, Value).

/* module_callable(+Term) <-
   Module save version of callable/1.
   */
:- meta_predicate module_callable(0).
module_callable(_:Term) :-
   callable(Term).

/* original_head(:CurrentHead, -OriginalHead) <-
   Unifies OriginalHead with the predicate adressed by CurrentHead but
   prefixed with its original module.
   */
:- meta_predicate original_head(0, 0).
original_head(Current:Head, Original:Head) :-
   original_module(Current:Head, Original).

/*** implementation ***********************************************/

/* original_module(:Head, -OrgModule) <-
   Unify OrgModule with the module Head was defined in originally.
   This is usefull, since imported predicates are prefixed with the module
   they were imported into.
   */
:- meta_predicate original_module(0, -).
original_module(Module:Head, OrgModule) :-
   predicate_property(Module:Head, imported_from(OrgModule)),
   !.
original_module(Module:_, Module).