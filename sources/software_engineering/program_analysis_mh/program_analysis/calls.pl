:- module(pa_calls, [
   calls/3,
   calls/5,
   predicate_calls_predicate/2,
   clause_calls_clause/2
]).

/**
 * Module for analyzing references between clauses and predicates.
 */

/*** tests ********************************************************/
test(clause_calls_clause, 1) :-
   clause(mh_test(3) ,_ , CallerRef),
   findall( CalleeRef,
      clause_calls_clause(CallerRef, CalleeRef),
      Calles ),
   length(Calles, 3),
   memberchk(system:integer/1, Calles),
   clause_head(CalleeRef2, (_,_)),
   memberchk(CalleeRef2, Calles),
   clause_head(CalleeRef3, mh_test_aux(_)),
   memberchk(CalleeRef3, Calles).

test(predicate_calls_predicate, 1) :-
   findall( Called,
      predicate_calls_predicate(mh_test(_), Called),
      Calleds),
   length(Calleds, 15).

test(calls_2, 1) :-
   source_file(mh_test(_), File),
   !,
   setof( Called,
      calls(pa_clause:clause_file, File, Called),
      Files ),
   length(Files, 2).

/*** interface ****************************************************/

/* calls(+Classifier, ?CallerClass, -CalleeClass) <-
   Unifies CalleeClass with the class of the clauses called by clauses classified
   as CallerClass.
   */
calls(Classifier, CallerClass, CalleeClass) :-
   calls(Classifier, _, CallerClass, _, CalleeClass).

calls(Classifier, CallerRef, CallerClass, CalleeRef, CalleeClass) :-
   clause_calls_clause(CallerRef, CalleeRef),
   call(Classifier, CallerRef, CallerClass),
   call(Classifier, CalleeRef, CalleeClass).
   
/* predicate_calls_predicate(+Predicate, -Callee) <-
   */
:- meta_predicate predicate_calls_predicate(0, 0).
predicate_calls_predicate(Module:Head, Callee) :-
   var(Head),
   !,
   current_predicate(_, Module:Head),
   predicate_calls_predicate_aux(Module:Head, Callee).
predicate_calls_predicate(Predicate, Callee) :-
   % Predicate is bound
   predicate_calls_predicate_aux(Predicate, Callee).

/* clause_calls_clause(+CallerRef, -CalleeRef) <-
   Unifies CalleeRef with the references to the clauses called by CallerRef. */
clause_calls_clause(CallerRef, CalleeRef) :-
   ground(CallerRef),
   !,
   clause_body(CallerRef, Body),
   term_calls_term(Body, Called),
   clause_head(CalleeRef, Called).
clause_calls_clause(CallerRef, CalleeRef) :-
   % CallerRef is unbound
   current_predicate(Module:Functor/Arity),
   functor(Head, Functor, Arity),
   \+ predicate_private(Module:Head),
   clause(Module:Head, Body, CallerRef),
   term_calls_term(Body, Called),
   clause_head(CalleeRef, Called).

/*** implementation ***********************************************/

/* predicate_calls_predicate_aux(:Predicate, -Callee) <-
   */
:- meta_predicate predicate_calls_predicate_aux(0, 0).
predicate_calls_predicate_aux(Predicate, _) :-
   predicate_private(Predicate),
   !,
   fail.
predicate_calls_predicate_aux(Predicate, Callee) :-
   clause(Predicate, Body),
   term_calls_term(Body, Term),
   bind_caller_arguments(Predicate, Term, Module:Head),
   original_head(Module:Head, Callee).

/* term_calls_term(:Term, -SubTerm) <-
   Unifies SubTerm with the terms called from within Term.
   */
:- meta_predicate term_calls_term(0, 0).
term_calls_term([], _) :-
   !,
   false.
term_calls_term(Term, Callee) :-
   genuine_meta_predicate(Term, MetaHead),
   arg(Arg, MetaHead, MetaSpec),
   metaspec_arity(MetaSpec),
   module_arg(Arg, Term, OriginalSubTerm),
   module_callable(OriginalSubTerm),
   term_calls_term_meta(OriginalSubTerm, MetaSpec, SubTerm),
   term_calls_term(SubTerm, Callee).
term_calls_term(Term, Term) :-
   module_callable(Term).

/* term_calls_term_meta(:Term, +AdditionalArity, -Callee) <-
   Unifies Callee with Term extended by AdditionalArity arguments.
   */
:- meta_predicate term_calls_term_meta(0, +, 0).
term_calls_term_meta(Module:Var^Term, AdditionalArity, Module:Callee) :-
   var(Var),
   !,
   term_calls_term_meta(Module:Term, AdditionalArity, Callee).
term_calls_term_meta(Module:Term, ':', Module:Callee) :-
   !,
   module_functor(Term, Functor, OriginalArity),
   module_current_predicate(Functor, Callee),
   module_functor(Callee, Functor, CalledArity),
   CalledArity >= OriginalArity.
term_calls_term_meta(Module:Term, AdditionalArity, Module:Callee) :-
   integer(AdditionalArity),
   Term =.. [Functor|OriginalArgs],
   length(AdditionalArgs, AdditionalArity),
   append(OriginalArgs, AdditionalArgs, CalleeArgs),
   Callee =.. [Functor|CalleeArgs].

/* bind_caller_arguments(:Caller, +Callee, -Head) <-
   Calculate Head by replacing all arguments of Callee with variables, if they
   are no arguments of Caller.
   */
:- meta_predicate bind_caller_arguments(0, +, 0).
bind_caller_arguments(Caller, Module:Callee, Module:Head) :-
   !,
   bind_caller_arguments_aux(Caller, Callee, Head).
bind_caller_arguments(Module:Caller, Callee, Module:Head) :-
   bind_caller_arguments_aux(Caller, Callee, Head).

/* bind_caller_arguments_aux(:Caller, +Callee, -Callee) <-
   */
:- meta_predicate bind_caller_arguments_aux(0, +, -).
bind_caller_arguments_aux(_, Callee, Callee) :-
   functor(Callee, _, 0),
   !.
bind_caller_arguments_aux(Caller, Callee, Head) :-
   functor(Callee, CalleeName, _),
   bagof( Arg,
      Pos^CalleeArg^( arg(Pos, Callee, CalleeArg),
         bind_caller_argument(Caller, CalleeArg, Arg) ),
      Args ),
   Head =.. [CalleeName|Args].
   
/* bind_caller_argument(:Caller, ?Arg, -NewArg) <-
   If Arg is a bound argument of Caller return it as NewArg.
   Otherwise return a variable.
   */
:- meta_predicate bind_caller_argument(0, ?, -).
bind_caller_argument(_, Arg, Arg) :-
   var(Arg),
   !.
bind_caller_argument(_:Caller, Arg, Arg) :-
   % Arg is bound
   arg(_, Caller, CallerArg),
   CallerArg == Arg,
   !.
bind_caller_argument(_, _, _) :-
   !.