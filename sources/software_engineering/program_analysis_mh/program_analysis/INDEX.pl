/*  Creator: make/0

    Purpose: Provide index for autoload
*/

index((calls), 3, pa_calls, calls).
index((calls), 5, pa_calls, calls).
index((predicate_calls_predicate), 2, pa_calls, calls).
index((clause_calls_clause), 2, pa_calls, calls).
index((clause_head), 2, pa_clause, clause).
index((clause_body), 2, pa_clause, clause).
index((clause_file), 2, pa_clause, clause).
index((ddk_unit_interface), 2, pa_ddk, ddk).
index((ddk_module_interface), 3, pa_ddk, ddk).
index((ddk_unit_dependencies), 2, pa_ddk, ddk).
index((ddk_module_dependencies), 3, pa_ddk, ddk).
index((ddk_unit_calls_unit), 2, pa_ddk, ddk).
index((ddk_module_calls_module), 2, pa_ddk, ddk).
index((genuine_meta_predicate), 1, pa_meta_predicates, meta_predicates).
index((genuine_meta_predicate), 2, pa_meta_predicates, meta_predicates).
index((scan_for_disguised_meta_predicates), 2, pa_meta_predicates, meta_predicates).
index((metaspec_arity), 1, pa_meta_predicates, meta_predicates).
index((module_functor), 3, pa_module_safe, module_safe).
index((module_current_predicate), 2, pa_module_safe, module_safe).
index((module_arg), 3, pa_module_safe, module_safe).
index((module_callable), 1, pa_module_safe, module_safe).
index((original_head), 2, pa_module_safe, module_safe).
index((predicate_indicator), 2, pa_predicate, predicate).
index((predicate_private), 1, pa_predicate, predicate).
index((current_predicate_filter), 2, pa_predicate, predicate).
index((filter_predicate_by_source), 2, pa_predicate, predicate).
index((called_with_same_arg), 2, pa_predicate, predicate).
index((clear_arguments), 2, pa_predicate, predicate).
index((source_file_extension), 2, pa_source, source).
index((dependencies), 3, pa_structure, structure).
index((interface), 3, pa_structure, structure).
index((file_interface), 2, pa_structure, structure).
index((file_dependencies), 2, pa_structure, structure).
index((unused_predicate), 1, pa_structure, structure).
