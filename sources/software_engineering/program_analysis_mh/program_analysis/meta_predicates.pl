:- module(pa_meta_predicates, [
   genuine_meta_predicate/1,
   genuine_meta_predicate/2,
   scan_for_disguised_meta_predicates/2,
   metaspec_arity/1
]).

/**
 * Module for detecting meta-predicates.
 */

/*** tests ********************************************************/
test(normalize_metaspec, 1) :-
   normalize_metaspec(:, S),
   S == (:).
test(normalize_metaspec, 2) :-
   normalize_metaspec(1, S),
   S == 1.
test(normalize_metaspec, 3) :-
   normalize_metaspec(+, S),
   S == (+).
test(normalize_metaspec, 4) :-
   normalize_metaspec(_, S),
   S == (?).

test(predicate_metahead, 1) :-
   predicate_metahead(call(_), MH),
   MH == call(0).
test(predicate_metahead, 2) :-
   predicate_metahead(scan_for_disguised_meta_predicates(_, _), MH),
   MH = scan_for_disguised_meta_predicates(_, _),
   arg(1, MH, A1),
   var(A1),
   arg(2, MH, A2),
   var(A2).

test(normalize_metahead, 1) :-
   normalize_metahead(a(_, 0, +, -, ?, :), MH),
   MH == a(?, 0, +, -, ?, :).
   
test(metahead_contains_arity, 1) :-
   metahead_contains_arity(a(0)).
test(metahead_contains_arity, 2) :-
   \+ metahead_contains_arity(a(?)).

test(metaspec_arity, 1) :-
   metaspec_arity(:).
test(metaspec_arity, 2) :-
   metaspec_arity(0).
test(metaspec_arity, 3) :-
   \+ metaspec_arity(+).
test(metaspec_arity, 4) :-
   \+ metaspec_arity(-).
test(metaspec_arity, 5) :-
   \+ metaspec_arity(?).
test(metaspec_arity, 6) :-
   \+ metaspec_arity(_).

test(merge_metaspec, 1) :-
   merge_metaspec([_, +], S),
   S == (+).
test(merge_metaspec, 2) :-
   merge_metaspec(-, +, S),
   S == (?).
test(merge_metaspec, 3) :-
   merge_metaspec(-, 0, S),
   S == (?).
test(merge_metaspec, 4) :-
   merge_metaspec(0, :, S),
   S == (:).

test(merge_metaheads, 1) :-
   merge_metaheads([a(_), a(+)], MH),
   MH == a(+).

test(predicate_metahead_from_callee, 1) :-
   predicate_metahead_from_callee(call(A), apply(A, _), MetaHead),
   MetaHead == call(:).

test(predicate_metahead_from_closure, 1) :-
   predicate_metahead_from_closure([(user:call(A))-[user:apply(A, _)]], _:Head, MetaHead),
   Head == call(A),
   MetaHead == call(:).

/*** interface ****************************************************/

/* genuine_meta_predicate(:Head) <-
   True, if Head is a meta predicated defined using meta_predicate/1.
   */
:- meta_predicate genuine_meta_predicate(0).
genuine_meta_predicate(Head) :-
   genuine_meta_predicate(Head, _).

/* genuine_meta_predicate(:Head, -MetaHead) <-
   Same as genuine_meta_predicate/1 but also unifies MetHead with the defined MetHead.
   */
:- meta_predicate genuine_meta_predicate(0, -).
genuine_meta_predicate(Head, MetaHead) :-
   predicate_property(Head, meta_predicate(MetaHead)),
   !.

/* scan_for_disguised_meta_predicates(+Path, -MetaHeads) <-
   Scan Path for predicates that are meta predicates by their structure.
   Depending on the number of predicates in Path this may take some time.
   */
scan_for_disguised_meta_predicates(Path, MetaHeads) :-
   FilterPred = filter_predicate_by_source(Path),
   findall( MetaHead,
      ( predicate_metahead(FilterPred, Head, MetaHead),
        \+ genuine_meta_predicate(Head),
        metahead_contains_arity(MetaHead) ),
      MetaHeads ).

/* metaspec_arity(+Spec) <-
   True if Spec defines the arity the arg is called with.
   */
metaspec_arity(Spec) :-
   var(Spec),
   !,
   fail.   
metaspec_arity(:).
metaspec_arity(Spec) :-
   integer(Spec).

/*** implementation ***********************************************/

/* predicate_metahead(+FilterPred, -Head, -MetaHead) <-
   Get the MetaHead definition of al Head matching FilterPred.
   The metahead is retrieve by calculating the transitive closure over all matching predicates. This takes some time!
   */
:- meta_predicate predicate_metahead(1, -, -).
predicate_metahead(FilterPred, Head, MetaHead) :-
   predicate_closure_for_metahead(FilterPred, Closure),
   predicate_metahead_from_closure(Closure, Head, MetaHead),
   current_predicate_filter(FilterPred, Head).

/* predicate_closure_for_metahead(+FilterPred, -Closure) <-
   Get the transitive closure Closure for all predicates matching FilterPred.
   ATTENTION: This may take some while. 
   */
:- meta_predicate predicate_closure_for_metahead(1, -).
predicate_closure_for_metahead(FilterPred, Closure) :-
   scan_for_predicates(FilterPred, Predicates),
   get_edges_for_closure(Predicates, Edges),
   get_standalone_vertices(Predicates, Edges, Vertices),
   vertices_edges_to_ugraph(Vertices, Edges, GraphWithDups),
   list_to_set(GraphWithDups, Graph),
   transitive_closure(Graph, Closure).

/* scan_for_predicates(+FilterPred, -Predicates) <-
   Return all Predicates matching FilterPred.
   */
scan_for_predicates(FilterPred, Predicates) :-
   setof( Module:Head,
      FilterPred^M^H^( current_predicate_filter(FilterPred, M:H),
         clear_arguments(M:H, Module:Head) ),
      PredicatesWithDups ),
   list_to_set(PredicatesWithDups, Predicates).

/* get_edges_for_closure(+Predicates, -Edges) <-
   Get all edges, where one of Predicates calls a predicate with the one of its own arguments.
   */
get_edges_for_closure(Predicates, Edges) :-
   setof( Caller-Callee,
      M^H^Predicates^( Callee = M:H,
         member(Caller, Predicates),
         \+ genuine_meta_predicate(Caller),
         predicate_calls_predicate(Caller, Callee),
         called_with_same_arg(Caller, Callee) ),
      EdgesWithDups ),
   list_to_set(EdgesWithDups, Edges).

/* get_standalone_vertices(Vertices, Edges, NewVertices) <-
   Get all elements of Vertices, that are not contained in any edge in Edges.
   */
get_standalone_vertices(Vertices, Edges, NewVertices) :-
   exclude( get_standalone_vertices_filter(Edges),
      Vertices,
      NewVertices ).

/* clean_vertices_filter(+Edges, +Vertice) <-
   Check if Vertice is contained in Edges.
   */
get_standalone_vertices_filter(Edges, Vertice) :-
   memberchk(Vertice-_, Edges);
   memberchk(_-Vertice, Edges).

/* predicate_metahead_from_closure(+Closure, -Head, -MetaHead) <-
   Calculate the MetaHead of Head unsing Closure.
   */
:- meta_predicate predicate_metahead_from_closure(+, 0, -).
predicate_metahead_from_closure(Closure, Head, MetaHead) :-
   setof( H,
      B^member(H-B, Closure),
      Heads ),
   member(Head, Heads),
   bagof( MH,
      Callees^( member(Head-Callees, Closure),
         predicate_metahead_from_closure_aux(Head, Callees, MH) ),
      MHs ),
   merge_metaheads(MHs, MetaHead2),
   normalize_metahead(MetaHead2, MetaHead).

/* predicate_metahead_from_closure_aux(+Caller, +Callees, -MetaHead) <-
   */
:- meta_predicate predicate_metahead_from_closure_aux(0, +, -).
predicate_metahead_from_closure_aux(Caller, [], MetaHead) :-
   !,
   predicate_metahead(Caller, MetaHead).
predicate_metahead_from_closure_aux(Caller, Callees, MetaHead) :-
   setof( MH,
      Callee^( member(Callee, Callees),
         predicate_metahead_from_callee(Caller, Callee, MH) ),
      MetaHeads ),
   merge_metaheads(MetaHeads, MetaHead).

/* predicate_metahead_from_callee(+Caller, +Callee, -MetaHead) <-
   Unifies MetaHead with the meta head of Caller, based on the arguments shared with the predicate Callee.
   */
:- meta_predicate predicate_metahead_from_callee(0, 0, -).
predicate_metahead_from_callee(_:Caller, Callee, MetaHead) :-
   genuine_meta_predicate(Callee, CalleeMetaHead),
   !,
   bagof( Spec,
      CallerArg^Pos^( arg(Pos, Caller, CallerArg),
         predicate_metaspec_from_callee(CallerArg, Callee, CalleeMetaHead, Spec) ),
      Specs ),
   functor(Caller, Functor, _),
   MetaHead =.. [Functor|Specs].
predicate_metahead_from_callee(Caller, _, MetaHead) :-
   predicate_metahead(Caller, MetaHead).

/* predicate_metaspec_from_callee(+CallerArg, +Callee, +CalleeMetaHead, -Spec) <-
   Unifies Spec with the spec of CalleeMetaHead corresponding to the usage of CallerArg in Callee.
   */
:- meta_predicate predicate_metaspec_from_callee(+, 0, +, -).
predicate_metaspec_from_callee(CallerArg, _:Callee, CalleeMetaHead, Spec) :-
   findall( S,
      ( arg(ArgPos, Callee, CalleeArg),
        CalleeArg == CallerArg,
        arg(ArgPos, CalleeMetaHead, S) ),
      Specs ),
   merge_metaspec(Specs, Spec).

/* merge_metaheads(+MetaHeads, -MetaHead) <-
   Combine MetaHeads to their generalized MetaHead.
   */
merge_metaheads([MetaHead], MetaHead) :-
   !.
merge_metaheads([MetaHead1, MetaHead2], MetaHead) :-
   !,
   functor(MetaHead1, Functor, Arity),
   functor(MetaHead2, Functor, Arity),
   findall( Spec,
      ( between(1, Arity, ArgPos),
        arg(ArgPos, MetaHead1, Spec1),
        arg(ArgPos, MetaHead2, Spec2),
        merge_metaspec(Spec1, Spec2, Spec) ),
      Specs ),
   MetaHead =.. [Functor|Specs].
merge_metaheads([MetaHead1|MetaHeads], MetaHead) :-
   merge_metaheads(MetaHeads, MetaHead2),
   merge_metaheads([MetaHead1,MetaHead2], MetaHead).

/* merge_metaspec(+Spec1, +Spec2, -Spec) <-
   Combine Spec1 and Spec2 to their generalized Spec. */
merge_metaspec(Spec, Spec, Spec) :-
   !.
merge_metaspec(Spec1, Spec2, :) :-
   metaspec_arity(Spec1),
   metaspec_arity(Spec2),
   !.
merge_metaspec(_, _, ?).

/* merge_metaspec(+Specs, -Spec) <-
   Combine Specs to their generalized Spec.
   */
merge_metaspec([], _) :-
   !.
merge_metaspec([Spec], Spec) :-
   !.
merge_metaspec([Spec1|Specs], Spec) :-
   merge_metaspec(Specs, Spec2),
   merge_metaspec(Spec1, Spec2, Spec).

/* metahead_contains_arity(+MetaHead) <-
   True, if metahead contains an arity spec. */
metahead_contains_arity(MetaHead) :-
   arg(_, MetaHead, Spec),
   metaspec_arity(Spec),
   !.

/* normalize_metahead(+MetaHead, -Normalized) <-
   Bind all unbound specs of MetaHead to  ?.
   */
normalize_metahead(MetaHead, Normalized) :-
   functor(MetaHead, Functor, _),
   bagof( Spec,
      Pos^S^( arg(Pos, MetaHead, S),
         normalize_metaspec(S, Spec) ),
      Specs ),
   Normalized =.. [Functor|Specs].

/* normalize_metaspec(?Spec, -Normalized) <-
   Normalize Spec to ? if it is unbound.
   */
normalize_metaspec(Spec, ?) :-
   var(Spec),
   !.
normalize_metaspec(Spec, Spec).

/* predicate_metahead(:Pred, -MetaHead) <-
   Unifies MetaHead with the meta head of Pred if Pred is a genuine meta predicate.
   Otherwise an empty MetaHead is generated.
   */
:- meta_predicate predicate_metahead(0, -).
predicate_metahead(Pred, MetaHead) :-
   predicate_property(Pred, meta_predicate(MetaHead)),
   !.
predicate_metahead(_:Head, MetaHead) :-
   functor(Head, Functor, Arity),
   length(Specs, Arity),
   MetaHead =.. [Functor|Specs].