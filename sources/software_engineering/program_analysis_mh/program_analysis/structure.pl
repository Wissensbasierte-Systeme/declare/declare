:- module(pa_structure, [
   dependencies/3,
   interface/3,
   file_interface/2,
   file_dependencies/2,
   unused_predicate/1
]).

/**
 * Module for analysing the structure of programs.
 */

/*** interface ****************************************************/

/* file_interface(?File, -Interface) <-
   Unifies Interface with the list of predicates of File that are called from
   other files.
   See: interface/3
   */
file_interface(File, Interface) :-
   interface(pa_clause:clause_file, File, Interface).

/* file_dependencies(?File, -Dependencies) <-
   Unifies Dependencies with the list of tiles that File depends on.
   See: dependencies/3
   */
file_dependencies(File, Dependencies) :-
   dependencies(pa_clause:clause_file, File, Dependencies).

/* dependencies(+Classifier, ?CallerClass, -Dependencies) <-
   Unifies Dependencies with the list of classes that clauses of class
   CallerClass depend on.
   Classification is done using provided Classifier(+ClauseRef, -ClauseRef).
   */
:- meta_predicate dependencies(2, ?, -).
dependencies(Classifier, CallerClass, Dependencies) :-
   setof( CalleeClass,
      ( pa_calls:calls(Classifier, CallerClass, CalleeClass),
        CalleeClass \= CallerClass ),
      DepsWithDups ),
   list_to_set(DepsWithDups, Dependencies).

/* interface(+Classifier, ?Class, -Interface) <-
   Unifies Interface with the list of predicates with class Class, that are called
   by a clause belonging to another class.
   Classification is done using provided Classifier(+ClauseRef, -ClauseRef).
   */
:- meta_predicate interface(2, ?, -).
interface(Classifier, Class, Interface) :-
   setof( PredIndicator,
      CallerRef^CallerClass^CalleeRef^M^CalleeHead^(
         pa_calls:calls(Classifier, CallerRef, CallerClass, CalleeRef, Class),
         CallerClass \= Class,
         clause_head(CalleeRef, M:CalleeHead),
         predicate_indicator(PredIndicator, M:CalleeHead) ),
      Interface ).

/* unused_predicate(?Pred) <-
   Unifies Pred with a predicate that is not called by another predicate. 
   */
unused_predicate(Pred) :-
   all_predicates(AllPreds),
   called_predicates(CalledPreds),
   subtract(AllPreds, CalledPreds, UncalledPreds),
   member(Pred, UncalledPreds).

/*** implementation ***********************************************/

/* called_predicates(-Predicates) <-
   Unifies Predicates with a set of all called predicates.
   */
called_predicates(Predicates) :-
   setof( Pred,
      FM^FH^TM^TH^( predicate_calls_predicate(FM:FH, TM:TH),
         predicate_indicator(Pred, TM:TH) ),
      Predicates ).

/* all_predicates(-Predicates) <-
   Unifies Predicates with a set of all available predicates.
   */
all_predicates(Predicates) :-
   setof( Pred,
      Functor^Module^Head^( current_predicate(Functor, Module:Head),
         predicate_indicator(Pred, Module:Head) ),
      Predicates ).