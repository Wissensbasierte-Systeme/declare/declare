
%  This file contains a fixed set of predicates for testing puropses.

%  mh_test(+Count) <-
%     Calls Count subgoals.

mh_test(1) :-
   integer(1).
mh_test(2) :-
   \+ C is C + C.
mh_test(3) :-
   integer(3),
   mh_test_aux(3).
mh_test(4) :-
   A is A/A,
   \+ mh_test_aux(4).
mh_test(5) :-
   call(mh_test_aux, A, A),
   call(mh_test_aux(A), A).

mh_test_aux(_).

mh_test_aux(_, _).

