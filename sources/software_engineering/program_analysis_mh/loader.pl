

:- dislog_variable_get(source_path, Sources),
   assert(file_search_path(ddk, Sources)).

:- dislog_variable_get(source_path,
      'software_engineering/program_analysis_mh/program_analysis',
      Path),
   assert(library_directory(Path)),
   reload_library_index.

?- ['test_set/test.pl'].
?- ['elementary.pl'].


