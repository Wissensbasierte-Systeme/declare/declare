

%  Collection of elementary predicates.

%  name_append_benchmark(+Pred) <-
%     Benchmark for predicate name_append/3.
%     Will profile performance of multiple executions of predicate Pred.   

name_append_benchmark(Pred) :-
   findall( Word,
      ( between(1, 95, Length),
        word(Length, Word) ),
      Words ),
   profile(
      foreach(
         ( member(Word1, Words),
           member(Word2, Words),
           member(Word3, Words) ),
         ( call(Pred, Word1, Word2, Word12),
           call(Pred, Word12, Word3, _) ) ) ).

%  mh_name_append(+Name_A, +Name_B, -Name_C) <-
%     Alternative implementation of name_append/3 of the DDK.
%     Benchmark results (using name_append_benchmark/1):
%       Old Implementation (name_append/3): 25.66 sec 
%     - New Implementation (mh_name_append/3): 3.82 sec

mh_name_append(Name_A, Name_B, Name_C) :-
   atom_concat(Name_A, Name_B, Name_C).

%  mh_name_append(+Names, -Name) <-
%     Alternative implementation of name_append/2 of the DDK.

mh_name_append([], '').
mh_name_append([N|Ns], Name) :-
   mh_name_append(Ns, Name_2),
   mh_name_append(N, Name_2, Name).

%  concat_names_benchmark(+Pred) <-
%     Benchmark for predicate concat_names/3.
%     Will profile performance of multiple executions of predicate Pred.

concat_names_benchmark(Pred) :-
   findall( Word,
      ( between(1, 95, Length),
        word(Length, Word) ),
      Words ),
   profile(
      foreach( between(1, 500, _),
         call(Pred, Words, '', _) ) ).
         
%  mh_concat_names(+Names, +Old_Name, -New_Name) <-
%     Alternative implementation of name_append/3 of the DDK.
%     Benchmark results (using name_append_benchmark/1):
%     - Old Implementation (concat_names): 7.51 sec 
%     - New Implementation (mh_concat): 0.39 sec

mh_concat_names(Names, Old_Name, New_Name) :-
   name_append([Old_Name|Names], New_Name).

%  word(+Length, -Word) <-
%     Generates a string Word with Length characters.

word(Length, Word) :-
   Max is 31+Length,
   findall( LetterCode,
      between(32, Max, LetterCode),
      Letters ),
   string_to_atom(Letters, Word).


