

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser Library                          ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_library, [
       alphabet_name/4,
       apostrophize_value/2,
       area/2,
       condition_name/4,
       consume_until_rule/5,
       consume_until_rule/6,
       data_name/4,
       file_name/4,
       file_name/5,
       float_literal/4,
       float_literal/5,
       implementor_name/4,
       instruction_delimiter/5,
       integer_literal/4,
       integer_literal/5,
       literal/4,
       mnemonic_name/4,
       next_rule/5,
       next_rule_with_input_update/5,
       non_numeric_literal/4,
       number_literal/4,
       number_literal/5,
       optional_rule/5,
       optional_word/5,
       paragraph_name/4,
       paragraph_name/5,
       procedure_name/4,
       procedure_name/5,
       point/4,
       point/5,
       programmer_name/5,
       keyword/5,
       keyword/6,
       section_name/4,
       section_name/5,
       separator_left/4,
       separator_right/4,
       sequenced/6,
       sign/5,
       switch_name/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_parser_tedcg_help).
:- use_module('../cobolml_general/cobolml_general_reserved_words').
:- use_module(
   '../cobolml_tokenizer/cobolml_tokenizer_phase_1_cobol_symbols').

:- discontiguous
      area/2.

:- module_transparent
      consume_until_rule/5,
      consume_until_rule/6,
      next_rule/5,
      next_rule_with_input_update/5,
      optional_rule/5,
      optional_word/5,
      sequenced/6,
      sequenced_/6.


/*** interface ****************************************************/


/* alphabet_name -++>
      */

alphabet_name -++>
   sequenced(separator_left, **),
   --->(_^[X], [alphabet_name:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     area(X, 'B'),
     upcase_value(X, Value),
     \+(cobol_reserved_word(Value)) },
   sequenced(separator_right, ++).


/* apostrophize_value(+As1, -As2) <-
      */

apostrophize_value(As1, As2) :-
   member(value:Value, As1),
   findall( W,
      ( member(W, As1),
        W \= value:_ ),
      Ws1 ),
   concat_atom(['\'', Value, '\''], Value2),
   append([value:Value2], Ws1, As2).


/* area(+X, +Area) <-
      Checks if the token X starts in Area */

area(X, Area) :-
   Start_column := X/start@column,
   !,
   area(Start_column, Area).


/* condition_name -++>
      */

condition_name -++>
   sequenced(separator_left, **),
   --->(_^[X], [condition_name:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     area(X, 'B'),
     upcase_value(X, Value),
     \+(cobol_reserved_word(Value)) },
   sequenced(separator_right, ++).


/* consume_until_rule(+Rule, ++) -++>
      Equal to consume_until_rule(Rule)-++> except that at least
      one element must be consumed until Rule is applicable */

consume_until_rule(Rule, ++) -++>
   Xs^consume_until_rule(Rule),
   { Xs \= [] }.


/* consume_until_rule(+Rule, **) -++>
      Equal to consume_until_rule(Rule)-++> */

consume_until_rule(Rule, **) -++>
   consume_until_rule(Rule).


/* consume_until_rule(+Rule) -++>
      Consumes every pretoken from the input until Rule is
      applicable. Returns all the consumed pretokens and
      whose values as well as the remaining output.
      If Rule is not applicable, consume_until_rule fails */

consume_until_rule(Rule) -++>
   ( next_rule(Rule),
     !
   ; [_],
     consume_until_rule(Rule) ).


/* data_name -++>
      */

data_name -++>
   sequenced(separator_left, **),
   --->(_^[X], [data_name:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     area(X, 'B'),
     upcase_value(X, Value),
     \+(cobol_reserved_word(Value)) },
   sequenced(separator_right, ++).


/* file_name -++>
      */

file_name -++>
   file_name('B').


/* file_name(+Area) -++>
      */

file_name(Area) -++>
   sequenced(separator_left, **),
   --->(_^[X], [file_name:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     area(X, Area),
     upcase_value(X, Value),
     \+(cobol_reserved_word(Value)) },
   sequenced(separator_right, ++).


/* float_literal -++>
      Equal to float_literal(_)-++> which means that
      the sign of the float literal is negligible */

float_literal -++>
   float_literal(_),
   !.


/* float_literal(+Sign) -++>
      Sign defines the algebraic sign of the float literal */

float_literal(Sign) -++>
   sequenced(separator_left, **),
   [X],
   { float_literal := X/tag::'*',
     Value := X@value,
     atom_chars(Value, As),
     ( foreach(A, As), foreach(B, Bs) do
       ( A = ',' -> B = '.'
       ; B = A ) ),
     atom_chars(Value_2, Bs),
     atom_to_int(Value_2, Int),
     ( Sign == +,
       Int > 0
     ; Sign == -,
       Int < 0
     ; Sign == 0,
       Int = 0
     ; var(Sign) ),
     area(X, 'B') },
   sequenced(separator_right, ++).


/* implementor_name -++>
      Implementors of cobol compilers can define a name for
      a certain behaviour. For example in the cobol/2 work-
      bench compiler the implementor name C01 defines a feed
      in a write instruction. CobolML is not specialized in
      a certain compiler and therefore the word tokens
      are just transformed in implementor_name tokens without
      checking the value */

implementor_name -++>
   sequenced(separator_left, **),
   --->(_^[X], [implementor_name:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     area(X, 'B'),
     upcase_value(X, Value),
     \+(cobol_reserved_word(Value)) },
   sequenced(separator_right, ++).


/* index_name -++>
      */

index_name -++>
   sequenced(separator_left, **),
   --->(_^[X], [index_name:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     area(X, 'B'),
     upcase_value(X, Value),
     \+(cobol_reserved_word(Value)) },
   sequenced(separator_right, ++).


/* instruction_delimiter(+Value) --+>
      */

instruction_delimiter(Value) --+>
   sequenced(separator_left, **),
   --->(_^[X], [instruction_delimiter:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     upcase_value(X, Value),
     area(X, 'B') },
   sequenced(separator_right, ++).


/* integer_literal -++>
      Equal to integer_literal(_)-++> which means that
      the sign of the integer literal is negligible */

integer_literal -++>
   integer_literal(_),
   !.


/* integer_literal(+Sign) -++>
      Sign defines the algebraic sign of the integer literal */

integer_literal(Sign) -++>
   sequenced(separator_left, **),
   [X],
   { integer_literal := X/tag::'*',
     Value := X@value,
     atom_to_int(Value, Int),
     ( Sign == +,
       Int > 0
     ; Sign == -,
       Int < 0
     ; Sign == 0,
       Int = 0
     ; var(Sign) ),
     area(X, 'B') },
   sequenced(separator_right, ++).


/* keyword(+Value) -++>
      */

keyword(Value) -++>
   keyword(Value, 'B').


/* keyword(+Value, +Area) -++>
      */

keyword(Value, Area) -++>
   sequenced(separator_left, **),
   --->(_^[X], [keyword:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     upcase_value(X, Value),
     area(X, Area) },
   sequenced(separator_right, ++).


/* literal -++>
      */

literal -++>
   ( non_numeric_literal
   ; word
   ; integer_literal
   ; float_literal ).


/* mnemonic_name -++>
      */

mnemonic_name -++>
   sequenced(separator_left, **),
   --->(_^[X], [mnemonic_name:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     area(X, 'B'),
     upcase_value(X, Value),
     \+(cobol_reserved_word(Value)) },
   sequenced(separator_right, ++).


/* next_rule(+Rule, -[], -[], +Ys, -Ys) <-
      Checks if Rule is applicable to input Ys.
      The output stays unaffected independently of the success
      of Rule and is therefore just the input. Of course there
      is no need to give any content related to As or Xs back */

next_rule(Rule, [], [], Ys, Ys) :-
   call(Rule, _, _, Ys, _).


/* next_rule_with_input_update(+Rule, [], [], +Ys, -Zs) <-
      Checks if Rule is applicable to the input Ys, giving [] as
      the value and [] as the consumed part of a cobol dcg rule
      back.
      If next_rule_with_input_update succeeds the output Zs is
      updated through Rule */

next_rule_with_input_update(Rule, [], [], Ys, Zs) :-
   call(Rule, _, Xs, Ys, Ys_1),
   append(Xs, Ys_1, Zs).


/* non_numeric_literal -++>
      */

non_numeric_literal -++>
   sequenced(separator_left, **),
   --->(As1@[X], As2),
   { non_numeric_literal := X/tag::'*',
     area(X, 'B'),
     apostrophize_value(As1, As2) },
   sequenced(separator_right, ++).


/* number_literal -++>
      Equal to number_literal(_)-++> which means that
      the sign of the integer respectively float literal
      is negligible */

number_literal -++>
   number_literal(_).


/* number_literal(+Sign) -++>
      Sign defines the algebraic sign of the
      integer respectively float literal */

number_literal(Sign) -++>
   ( integer_literal(Sign)
   ; float_literal(Sign) ).


/* optional_rule(+Rule) -++>
      Applies Rule to the input if possible.
      Attention: Do not use optional_rule in a "if-then-else"-
                 construct, because optional_rule always succeeds */

optional_rule(Rule) -++>
   call(Rule).
optional_rule(_) -++>
   !.


/* optional_word(+Value) -++>
      Transformes a word X into an optional word iff
      X has the value Value and stands in area B */

optional_word(Value) -++>
   sequenced(separator_left, **),
   --->(_^[X], [optional_word:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     upcase_value(X, Value),
     area(X, 'B') },
   sequenced(separator_right, ++).
optional_word(_) --+>
   !.


/* paragraph_name -++>
      */

paragraph_name -++>
   paragraph_name('B').


/* paragraph_name(+Area) -++>
      */

paragraph_name(Area) -++>
   sequenced(separator_left, **),
   --->(_^[X], [paragraph_name:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     upcase_value(X, Value),
     \+(cobol_reserved_word(Value)),
     area(X, Area) },
   sequenced(separator_right, ++).


/* point --+>
      */

point --+>
   point('B').


/* point(+Area) --+>
      */

point(_) --+>
   { dislog_variable_get( cobolml_instruction_mode,
        no_delimiting_points_allowed ) },
   !.
point(Area) --+>
   sequenced(separator_left, **),
   [X],
   { point := X/tag::'*',
     area(X, Area) },
   sequenced(separator_right, ++).


/* procedure_name -++>
      */

procedure_name -++>
   procedure_name('B').


/* procedure_name(+Area) -++>
      */

procedure_name(Area) -++>
   sequenced(separator_left, **),
   --->(_^[X], [procedure_name:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     upcase_value(X, Value),
     \+(cobol_reserved_word(Value)),
     area(X, Area) },
   sequenced(separator_right, ++).


/* programmer_name(+Tag) -++>
      Transformes a word X into a token with the tag Tag.
      The Value of X must not match a reserved cobol word */

programmer_name(Tag) -++>
   sequenced(separator_left, **),
   --->(_^[X], [Tag:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     upcase_value(X, Value),
     \+(cobol_reserved_word(Value)),
     area(X, 'B') },
   sequenced(separator_right, ++).


/* section_name -++>
      */

section_name -++>
   section_name('B').


/* section_name(+Area) -++>
      */

section_name(Area) -++>
   sequenced(separator_left, **),
   --->(_^[X], [section_name:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     upcase_value(X, Value),
     \+(cobol_reserved_word(Value)),
     area(X, Area) },
   sequenced(separator_right, ++).


/* separator_left --+>
      Consumes all separator tokens which
      can stand on the left side of a token */

separator_left --+>
   space.
separator_left --+>
   ( comma
   ; semicolon ),
   ( space
   ; optional_rule(program_id_area),
     newline ).
separator_left --+>
   sequence_number_area.
separator_left --+>
   regular_line_tag.
separator_left --+>
   ( comment_line
   ; comment_line_with_page_feed
   ; debugging_line
   ; empty_line ),
   newline.
separator_left --+>
   { dislog_variable_get( cobolml_parser_mode,
        parentheses_are_separators ) },
   next_rule(left_parenthesis).


/* separator_right --+>
      Consumes all separator tokens which
      can stand on the right side of a token */

separator_right --+>
   ( space
   ; newline
   ; program_id_area
   ; '$eof' ).
separator_right --+>
   ( comma
   ; semicolon ),
   ( space
   ; optional_rule(program_id_area),
     ( newline
     ; '$eof' ) ).
separator_right --+>
   empty_line,
   optional_rule(newline).
separator_right --+>
   sequenced(comment_lines_, ++),
   '$eof'.
separator_right --+>
   ( next_rule(point_separator)
   ; next_rule(point_eof) ).
separator_right --+>
   { dislog_variable_get( cobolml_parser_mode,
        parentheses_are_separators ) },
   next_rule(right_parenthesis).


/* sequenced(+Rule, +) -++>
      Apply Rule at least one time */

sequenced(Rule, +) -++>
   call(Rule),
   sequenced(Rule, *),
   !.


/* sequenced(+Rule, *) --+> oder -++>
      Apply Rule at backtracking */

sequenced(_, *) --+>
   [].
sequenced(Rule, *) -++>
   call(Rule),
   sequenced(Rule, *),
   !.


/* sequenced(+Rule, ++) -++>
      Apply Rule as often as possible and at least one time */

sequenced(Rule, ++) -++>
   call(Rule),
   sequenced_(Rule, **),
   !.


/* sequenced(+Rule, **) --+> oder -++>
      Apply Rule as often as possible */

sequenced(Rule, **) -++>
   sequenced_(Rule, **),
   !.


/* sign(+Tag) -++>
      Consumes a token with tag Tag. Left and right
      parenthesis do not need to be concluded with separators */

sign(Tag) -++>
   sequenced(separator_left, **),
   [X],
   { Tag := X/tag::'*' },
   ( { member(Tag, [left_parenthesis, right_parenthesis]) },
     sequenced(separator_right, **)
   ; sequenced(separator_right, ++) ),
   !.


/* sign(+Symbol) -++>
      Consumes a token represented by Symbol. Left and right
      parenthesis do not need to be concluded with separators */

sign(Symbol) -++>
   sequenced(separator_left, **),
   [X],
   { Tag := X/tag::'*',
     cobolml_tokenizer_phase_1_cobol_symbols:
        cobol_symbol(Symbol, Tag) },
   ( { member(Tag, [left_parenthesis, right_parenthesis]) },
     sequenced(separator_right, **)
   ; sequenced(separator_right, ++) ),
   !.


/* switch_name -++>
      */

switch_name -++>
   sequenced(separator_left, **),
   --->(_^[X], [switch_name:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     area(X, 'B'),
     upcase_value(X, Value),
     \+(cobol_reserved_word(Value)) },
   sequenced(separator_right, ++).


/*** implementation ***********************************************/


/* area(+Column, ?Area) <-
      Checks if Column is consistent with Area */

area(Column, Area) :-
   ( between(1, 6, Column) -> Area = 'line_number'
   ; ( Column =:= 7 -> Area = 'indicator_area'
     ; ( between(8, 11, Column) -> Area = 'A'
       ; ( between(12, 72, Column) -> Area = 'B'
         ; ( between(73, 80, Column) ->
             Area = 'program_id' ) ) ) ) ).


/* comma -++>
      */

comma -++>
   [X],
   { comma := X/tag::'*' }.


/* comment_line -++>
      */

comment_line -++>
   [X],
   { comment_line := X/tag::'*' }.


/* comment_line_with_page_feed -++>
      */

comment_line_with_page_feed -++>
   [X],
   { comment_line_with_page_feed := X/tag::'*' }.


/* comment_lines_ -++>
      */

comment_lines_ -++>
   ( comment_line
   ; comment_line_with_page_feed ),
   optional_rule(newline).


/* debugging_line -++>
      */

debugging_line -++>
   { ( dislog_variable_get(cobolml_debugging_mode, yes)
     ; dislog_variable_get(cobolml_debugging_mode, null) ) },
   !,
   debugging_line_tag.
debugging_line -++>
   { dislog_variable_get(cobolml_debugging_mode, no) },
   debugging_line_without_debugging_mode.


/* debugging_line_tag -++>
      */

debugging_line_tag -++>
   [X],
   { debugging_line_tag := X/tag::'*' }.


/* debugging_line_without_debugging_mode -++>
      */

debugging_line_without_debugging_mode -++>
   debugging_line_tag,
   debugging_line_without_debugging_mode_remaining.


/* debugging_line_without_debugging_mode_remaining -++>
      */

debugging_line_without_debugging_mode_remaining -++>
   '$eof',
   !.
debugging_line_without_debugging_mode_remaining -++>
   ( new_line
   ; [_],
     debugging_line_without_debugging_mode_remaining ).


/* empty_line -++>
      */

empty_line -++>
   [X],
   { empty_line := X/tag::'*' }.


/* left_parenthesis -++>
      */

left_parenthesis -++>
   [X],
   { left_parenthesis := X/tag::'*' }.


/* newline -++>
      */

newline -++>
   [X],
   { newline := X/tag::'*' }.


/* point_eof --+>
      */

point_eof --+>
   [X1],
   { point := X1/tag::'*' },
   '$eof'.


/* point_separator --+>
      */

point_separator --+>
   [X1, X2],
   { point := X1/tag::'*',
     T2 := X2/tag::'*',
     member(T2, [space, program_id_area, newline]) }.


/* program_id_area -++>
      */

program_id_area -++>
   [X],
   { program_id_area := X/tag::'*' }.


/* regular_line_tag -++>
      */

regular_line_tag -++>
   [X],
   { regular_line_tag := X/tag::'*' }.


/* right_parenthesis -++>
      */

right_parenthesis -++>
   [X],
   { right_parenthesis := X/tag::'*' }.


/* semicolon -++>
      */

semicolon -++>
   [X],
   { semicolon := X/tag::'*' }.


/* sequenced_(+Rule, **) --+> oder -++>
      Apply Rule as often as possible */

sequenced_(_, **) --+>
   '$eof',
   !.
sequenced_(Rule, **) -++>
   ( Cs^call(Rule),
     { Cs \= [] },
     sequenced_(Rule, **)
   ; ! ).


/* sequence_number_area -++>
      */

sequence_number_area -++>
   [X],
   { sequence_number_area := X/tag::'*' }.


/* space -++>
      */

space -++>
   [X],
   { space := X/tag::'*' }.


/* upcase_value(+X, -Value) <-
      Returns the value of X in upper case letters */

upcase_value(X, Value) :-
   V := X@value,
   upcase_atom(V, Value).


/* word -++>
      */

word -++>
   [X],
   { word := X/tag::'*' }.


/* '$eof'(-[], -[], +[], -[]) <-
      Indicates the end of the cobol source file */

'$eof'([], [], [], []).


/******************************************************************/


