

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser                                  ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser, [
      cobolml_parser/2 ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_parser_tedcg_help).
:- use_module(cobolml_parser_identification_division).
:- use_module(cobolml_parser_data_division).
:- use_module(cobolml_parser_environment_division).
:- use_module(cobolml_parser_procedure_division).


/*** interface ****************************************************/


/* cobolml_parser(Tokens, CobolML) <-
      */

cobolml_parser(Tokens, CobolML) :-
   dislog_variable_set( tedcg_help_module,
      cobolml_parser_tedcg_help ),
   cobolml(_, [Cobolml_], Tokens, []),
   fn_item_parse(Cobolml_, cobolml:_:Cs),
   CobolML = cobolml:[version:'1.0']:Cs,
   !,
   nl, writeln('Parsing successful'), nl, nl.


/* cobolml +-+>
      */

cobolml +-+>
  identification_division,
  environment_division,
  data_division,
  procedure_division.


/******************************************************************/


