

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: DISPLAY                     ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_display, [
       display_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions_help).


/*** interface ****************************************************/


/* display_instruction +++>
      */

display_instruction +++>
   keyword('DISPLAY'),
   sequenced(display_assignment, ++),
   point.


/*** implementation ***********************************************/


/* display_assignment -++>
      */

display_assignment -++>
   ( data_name
   ; literal ).

 
/******************************************************************/


