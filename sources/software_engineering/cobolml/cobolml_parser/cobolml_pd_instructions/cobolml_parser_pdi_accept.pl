

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: ACCEPT                      ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_add, [
       add_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').


/*** interface ****************************************************/


/* accept_instruction +++>
      */

accept_instruction +++> % HIER weitermachen
   keyword('ADD'),
   sequenced(add_assignment, ++),
   add_connector,
   add_receiver,
   optional_rule(on_size_error_clause),
   optional_rule(not_on_size_error_clause),
   optional_word('END-ADD'),
   point. % HIER accept_delimiter


/*** implementation ***********************************************/


/* add_assignment +++>
      */

add_assignment +++>
   ( data_name
   ; number_literal ).


/* add_connector +++>
      */

add_connector +++>
   ( keyword('TO')
   ; keyword('GIVING') ).


/* add_receiver +++>
      */

add_receiver +++>
   data_name,
   optional_word('ROUNDED').


/******************************************************************/


