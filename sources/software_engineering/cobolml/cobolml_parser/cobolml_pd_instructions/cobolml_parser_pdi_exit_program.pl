

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: EXIT PROGRAM                ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_exit_program, [
       exit_program_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions_help).


/*** interface ****************************************************/


/* exit_program_instruction +++>
      */

exit_program_instruction +++>
   keyword('EXIT'),
   keyword('PROGRAM'),
   point.


/******************************************************************/



