

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: OPEN                        ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_open, [
       open_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions_help).


/*** interface ****************************************************/


/* open_instruction +++>
      */

open_instruction +++>
   keyword('OPEN'),
   sequenced(open_mode_file, ++),
   point.
open_instruction +++>
   keyword('OPEN'),
   ( open_mode_input_file
   ; open_mode_output_file
   ; open_mode_i_o_file
   ; open_mode_extend_file ),
   point.


/*** implementation ***********************************************/


/* file_reversed_rewind -++>
      */

file_reversed_rewind -++>
   file_name,
   optional_rule(reversed_rewind).


/* file_rewind -++>
      */

file_rewind -++>
   file_name,
   optional_rule(no_rewind_clause).


/* open_mode_extend_file -++>
      */

open_mode_extend_file -++>
   keyword('EXTEND'),
   sequenced(file_name, ++).


/* open_mode_input_file -++>
      */

open_mode_input_file -++>
   keyword('INPUT'),
   sequenced(file_reversed_rewind, ++).


/* open_mode_i_o_file -++>
      */

open_mode_i_o_file -++>
   keyword('I-O'),
   sequenced(file_name, ++).


/* open_mode_file -++>
      */

open_mode_file -++>
   ( keyword('INPUT')
   ; keyword('OUTPUT')
   ; keyword('I-O')
   ; keyword('EXTEND') ),
   sequenced(file_name, ++).


/* open_mode_output_file -++>
      */

open_mode_output_file -++>
   keyword('OUTPUT'),
   sequenced(file_rewind, ++).

   
/* reversed_rewind -++>
      */

reversed_rewind -++>
   ( reversed_clause
   ; no_rewind_clause ).


/******************************************************************/


