

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: READ                        ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_read, [
       read_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions_help).


/*** interface ****************************************************/


/* read_instruction +-+>
      */

read_instruction +-+>
   keyword('READ'),
   file_name,
   optional_rule( keyword('NEXT') ),
   optional_word('RECORD'),
   optional_rule(into_clause),
   optional_rule(at_end_clause),
   optional_rule(not_at_end_clause),
   ( point
   ; instruction_delimiter('END-READ') ).
read_instruction +-+>
   keyword('READ'),
   file_name,
   optional_word('RECORD'),
   optional_rule(into_clause),
   optional_rule(key_clause),
   optional_rule(invalid_key_clause),
   optional_rule(not_invalid_key_clause),
   ( point
   ; instruction_delimiter('END-READ') ).
read_instruction +-+>
   keyword('READ'),
   file_name,
   optional_rule( keyword('NEXT') ),
   optional_word('RECORD'),
   optional_rule(into_clause),
   optional_rule(lock_mode_clause),
   optional_rule(key_clause),
   optional_rule(invalid_key_clause),
   optional_rule(at_end_clause),
   ( point
   ; instruction_delimiter('END-READ') ).


/*** implementation **********************************************/


/* into_clause +++>
      */

into_clause +++>
   keyword('INTO'),
   data_name.


/* lock_mode_clause +++>
      */

lock_mode_clause +++>
   ( optional_word('WITH'),
     ( optional_word('KEPT')
     ; keyword('NO') ),
     keyword('LOCK')
   ; optional_rule( keyword('LOCK') ),
     optional_rule( keyword('WAIT') ) ).


/*****************************************************************/


