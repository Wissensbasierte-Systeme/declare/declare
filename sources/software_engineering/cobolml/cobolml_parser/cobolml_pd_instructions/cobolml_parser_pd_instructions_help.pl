

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PD Instructions Help             ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pd_instructions_help, [
       at_end_clause/4,
       condition/4,
       invalid_key_clause/4,
       key_clause/4,
       no_rewind_clause/4,
       not_at_end_clause/4,
       not_invalid_key_clause/4,
       not_on_size_error_clause/4,
       on_size_error_clause/4,
       reversed_clause/4,
       rounded_clause/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions).
:- use_module(cobolml_parser_pd_instructions_help_arithmetic_expr).


/*** interface ****************************************************/


/* at_end_clause +++>
      */

at_end_clause +++>
   optional_word('AT'),
   keyword('END'),
   instruction_without_delimiting_point.


/* condition -++>
      */

condition -++>
   condition_.


/* invalid_key_clause +++>
      */

invalid_key_clause +++>
   keyword('INVALID'),
   optional_word('KEY'),
   instruction_without_delimiting_point.


/* key_clause +++>
      */

key_clause +++>
   keyword('KEY'),
   optional_word('IS'),
   data_name.


/* no_rewind_clause +++>
      */

no_rewind_clause +++>
   optional_word('WITH'),
   keyword('NO'),
   keyword('REWIND').


/* not_at_end_clause +++>
      */

not_at_end_clause +++>
   keyword('NOT'),
   optional_word('AT'),
   keyword('END'),
   instruction_without_delimiting_point.


/* not_invalid_key_clause +++>
      */

not_invalid_key_clause +++>
   keyword('NOT'),
   keyword('INVALID'),
   optional_word('KEY'),
   instruction_without_delimiting_point.


/* not_on_size_error_clause +++>
      */

not_on_size_error_clause +++>
   keyword('NOT'),
   optional_word('ON'),
   keyword('SIZE'),
   keyword('ERROR'),
   instruction_without_delimiting_point.


/* on_size_error_clause +++>
      */

on_size_error_clause +++>
   optional_word('ON'),
   keyword('SIZE'),
   keyword('ERROR'),
   instruction_without_delimiting_point.


/* reversed_clause +++>
      */

reversed_clause +++>
   keyword('REVERSED').


/* rounded_clause +++>
      */

rounded_clause +++>
   keyword('ROUNDED').


/*** implementation ***********************************************/


/* bracketed_condition(As, Xs) -->
      */

bracketed_condition(As, Xs) -->
   sign(left_parenthesis, As1, Xs1),
   consume_until_closing_right_parenthesis(1, Xs2),
   sign(right_parenthesis, As3, Xs3),
   { condition_(As2, Condition_, Xs2, []),
     append([Xs1, Condition_, Xs3], Xs),
     flatten([As1, As2, As3], As123),
     cobolml_parser_tedcg_help:
        process_interim_attribute_list(As123, As) }.


/* class_condition +++>
      */

class_condition +++>
   data_name,
   optional_word('IS'),
   optional_rule( keyword('NOT') ),
   ( keyword('NUMERIC')
   ; keyword('ALPHABETIC') ).


/* combinable_condition -++>
      */

combinable_condition -++>
   optional_word('NOT'),
   simple_condition.


/* combined_condition_term -++>
      */

combined_condition_term -++>
   ( keyword('OR')
   ; keyword('AND') ),
   condition_.


/* condition_ -++>
      */

condition_ -++>
   ( condition_name
   ; bracketed_condition
   ; combinable_condition ),
   sequenced(combined_condition_term, **).


/* condition_operand -++>
      */

condition_operand -++>
   ( data_name
   ; literal
   ; arithmetic_expression ).


/* relational_condition +++>
      */

relational_condition +++>
   condition_operand,
   relational_operator,
   condition_operand.


/* relational_operator -++>
      */

relational_operator -++>
   optional_word('IS'),
   optional_rule( keyword('NOT') ),
   ( ( keyword('GREATER')
     ; keyword('LESS') ),
     optional_word('THAN')
   ; sign(greater)
   ; sign(lower)
   ; ( keyword('EQUAL'),
       optional_word('TO')
     ; sign(equal) ) ).
relational_operator -++>
   optional_word('IS'),
   ( ( keyword('GREATER')
     ; keyword('LESS') ),
     optional_word('THAN'),
     keyword('OR'),
     keyword('EQUAL'),
     optional_word('TO')
   ; sign(greater_equal)
   ; sign(lower_equal) ).


/* sign_condition +++>
      */

sign_condition +++>
   data_name,
   optional_word('IS'),
   optional_rule( keyword('NOT') ),
   ( keyword('POSITIVE')
   ; keyword('NEGATIVE')
   ; keyword('ZERO') ).
   

/* simple_condition -++>
      */

simple_condition -++>
   ( relational_condition
   ; class_condition
   ; sign_condition ).


/******************************************************************/


