

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: PERFORM                     ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_perform, [
       perform_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions).
:- use_module(cobolml_parser_pd_instructions_help).


/*** interface ****************************************************/


/* perform_instruction +-+>
      */

perform_instruction +-+>
   keyword('PERFORM'),
   procedure_name,
   optional_rule(through_clause),
   optional_rule(times_clause),
   point.
perform_instruction +-+>
   keyword('PERFORM'),
   procedure_name,
   optional_rule(through_clause),
   optional_rule(test_clause),
   until_clause,
   point.
perform_instruction +-+>
   keyword('PERFORM'),
   procedure_name,
   optional_rule(through_clause),
   optional_rule(test_clause),
   varying_clause,
   from_clause,
   by_clause,
   until_clause,
   sequenced(after_clause, **),
   point.
perform_instruction +-+>
   keyword('PERFORM'),
   optional_rule(times_clause),
   instruction_clause,
   ( instruction_delimiter('END-PERFORM')
   ; point ).
perform_instruction +-+>
   keyword('PERFORM'),
   optional_rule(test_clause),
   until_clause,
   instruction_clause,
   ( instruction_delimiter('END-PERFORM')
   ; point ).
perform_instruction +-+>
   keyword('PERFORM'),
   optional_rule(test_clause),
   varying_clause,
   from_clause,
   by_clause,
   until_clause,
   sequenced(after_clause, **),
   instruction_clause,
   ( instruction_delimiter('END-PERFORM')
   ; point ).


/*** implementation **********************************************/


/* after_clause +++>
      */

after_clause +++>
   keyword('AFTER'),
   ( index_name
   ; data_name ),
   from_clause,
   by_clause,
   until_clause.


/* by_clause +++>
      */

by_clause +++>
   keyword('BY'),
   ( data_name
   ; number_literal ).


/* from_clause +++>
      */

from_clause +++>
   keyword('FROM'),
   ( index_name
   ; data_name
   ; number_literal ).


/* instruction_clause +-+>
      */

instruction_clause +-+>
   sequenced(after_clause, **),
   sequenced(instruction_without_delimiting_point, ++).


/* test_clause +++>
      */

test_clause +++>
   optional_word('WITH'),
   keyword('TEST'),
   ( keyword('BEFORE')
   ; keyword('AFTER') ).


/* through_clause +++>
      */

through_clause +++>
   ( keyword('THROUGH')
   ; keyword('THRU') ),
   procedure_name.


/* times_clause +++>
      */

times_clause +++>
   ( data_name
   ; integer_literal(+) ),
   keyword('TIMES').


/* varying_clause +++>
      */

varying_clause +++>
   keyword('VARYING'),
   ( index_name
   ; data_name).


/* until_clause +++>
      */

until_clause +++>
   keyword('UNTIL'),
   condition.


/*****************************************************************/


