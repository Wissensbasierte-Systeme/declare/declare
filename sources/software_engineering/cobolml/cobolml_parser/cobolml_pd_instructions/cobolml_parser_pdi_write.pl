

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: WRITE                       ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_write, [
       write_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions).
:- use_module(cobolml_parser_pd_instructions_help).


/*** interface ****************************************************/


/* write_instruction +++>
      */

write_instruction +++>
   keyword('WRITE'),
   data_name,
   optional_rule(from_clause),
   optional_rule(advancing_clause),
   optional_rule(end_of_page_clause),
   optional_rule(not_end_of_page_clause),
   ( point
   ; instruction_delimiter('END-WRITE') ).
write_instruction +++>
   keyword('WRITE'),
   data_name,
   optional_rule(from_clause),
   optional_rule(invalid_key),
   optional_rule(not_invalid_key),
   ( point
   ; instruction_delimiter('END-WRITE') ).


/*** implementation **********************************************/


/* advancing_clause +++>
      */

advancing_clause +++>
   ( keyword('AFTER')
   ; keyword('BEFORE') ),
   optional_word('ADVANCING'),
   ( ( data_name
     ; integer_literal(+) ),
     ( Xs^optional_word('LINE'),
       { Xs \= [] }
     ; optional_word('LINES') )
   ; keyword('PAGE') ).


/* end_of_page_clause +++>
      */

end_of_page_clause +++>
   keyword('AT'),
   ( keyword('END-OF-PAGE')
   ; keyword('EOP') ),
   instruction_without_delimiting_point.


/* from_clause +++>
      */

from_clause +++>
   keyword('FROM'),
   data_name.


/* not_end_of_page_clause +++>
      */

not_end_of_page_clause +++>
   keyword('NOT'),
   keyword('AT'),
   ( keyword('END-OF-PAGE')
   ; keyword('EOP') ),
   instruction_without_delimiting_point.


/*****************************************************************/



