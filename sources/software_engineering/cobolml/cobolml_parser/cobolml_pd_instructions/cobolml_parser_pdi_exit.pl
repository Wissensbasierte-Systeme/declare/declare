

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: EXIT                        ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_exit, [
       exit_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions_help).


/*** interface ****************************************************/


/* exit_instruction +++>
      */

exit_instruction +++>
   keyword('EXIT'),
   point.


/******************************************************************/



