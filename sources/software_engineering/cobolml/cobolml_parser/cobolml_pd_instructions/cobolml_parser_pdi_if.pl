

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: IF                          ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_if, [
       if_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions).
:- use_module(cobolml_parser_pd_instructions_help).


/*** interface ****************************************************/


/* if_instruction +++>
      */

if_instruction +++>
   keyword('IF'),
   condition,
   if_instruction_then,
   optional_rule(if_instruction_else),
   ( point
   ; instruction_delimiter('END-IF') ).


/*** implementation ***********************************************/


/* if_instruction_else +-+>
      */

if_instruction_else +-+>
   keyword('ELSE'),
   if_instruction_subsequent_action.


/* if_instruction_subsequent_action --+>
      */

if_instruction_subsequent_action --+>
   ( sequenced(instruction_without_delimiting_point, ++)
   ; keyword('NEXT'),
     keyword('SENTENCE') ).


/* if_instruction_then +-+>
      */

if_instruction_then +-+>
   optional_word('THEN'),
   if_instruction_subsequent_action.


/******************************************************************/


