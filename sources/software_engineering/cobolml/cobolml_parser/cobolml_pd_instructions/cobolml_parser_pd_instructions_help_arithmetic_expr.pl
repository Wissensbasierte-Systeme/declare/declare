

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PD Instructions Help             ***/
/***                              Arithmetic Expression         ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pd_instructions_help_arithmetic_expr, [
       arithmetic_expression/4,
       consume_until_closing_right_parenthesis/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').


/*** interface ****************************************************/


/* arithmetic_expression +++>
      Builds an arithmetic expression out of additions,
      subtractions, multiplications, divisions, exponentiations,
      and compound statements after the ordinary mathematical
      rules */

arithmetic_expression +++>
   { dislog_variable_set( cobolml_parser_mode,
          parentheses_are_separators ) },
   ( additive_term
   ; multiplicative_term
   ; unary_operation ),
   { dislog_variable_set( cobolml_parser_mode, null ) }.
arithmetic_expression --+>
   { dislog_variable_set( cobolml_parser_mode, null ) },
   !,
   fail.


/*** implementation ***********************************************/


/* additive_term -++>
      */

additive_term -++>
   sequenced(additive_operation, ++).


/* multiplicative_term -++>
      */

multiplicative_term -++>
   ( sequenced(multiplicative_operation, ++)
   ; sequenced(exponentiative_operation, ++)
   ; factor ).


/* unary_operation -++>
      */

unary_operation -++>
   ( unary_plus
   ; unary_minus
   ; sequence_unary_operation ).


/* additive_operation -++>
      */

additive_operation -++>
   ( ( next_rule_with_input_update(addition)
     ; next_rule_with_input_update(subtraction) )
   ; [X],
     { fn_item_parse(X, Tag:_:_),
       member(Tag, [addition, subtraction]) } ).


/* exponentiative_operation -++>
      */

exponentiative_operation -++>
   ( next_rule_with_input_update(exponentiation)
   ; [X],
     { fn_item_parse(X, exponentiation:_:_) } ).


/* multiplicative_operation -++>
      */

multiplicative_operation -++>
   ( ( next_rule_with_input_update(division)
     ; next_rule_with_input_update(multiplication) )
   ; [X],
     { fn_item_parse(X, Tag:_:_),
       member(Tag, [division, multiplication]) } ).


/* addition +++>
      */

addition +++>
   [X],
   { fn_item_parse(X, Tag:_:_),
     member(Tag, [addition, subtraction]) },
   sign(plus),
   ( multiplicative_term
   ; unary_operation ).
addition +++>
   multiplicative_term,
   sign(plus),
   ( multiplicative_term
   ; unary_operation ).


/* subtraction +++>
      */

subtraction +++>
   [X],
   { Tag := X/tag::'*',
     member(Tag, [addition, subtraction]) },
   sign_(hyphen),
   ( multiplicative_term
   ; unary_operation ).
subtraction +++>
   multiplicative_term,
   sign_(hyphen),
   ( multiplicative_term
   ; unary_operation ).


/* exponentiation +++>
      */

exponentiation +++>
   [X],
   { fn_item_parse(X, exponentiation:_:_) },
   sign(power),
   ( factor
   ; unary_operation ).
exponentiation +++>
   factor,
   sign(power),
   ( factor
   ; unary_operation ).


/* division +++>
      */

division +++>
   [X],
   { fn_item_parse(X, Tag:_:_),
     member(Tag, [division, multiplication]) },
   sign(divide),
   ( sequenced(exponentiative_operation, ++)
   ; factor
   ; unary_operation ).
division +++>
   ( sequenced(exponentiative_operation, ++)
   ; factor ),
   sign(divide),
   ( sequenced(exponentiative_operation, ++)
   ; factor
   ; unary_operation ).


multiplication +++>
   [X],
   { fn_item_parse(X, Tag:_:_),
     member(Tag, [division, multiplication]) },
   sign(multiply),
   ( sequenced(exponentiative_operation, ++)
   ; factor
   ; unary_operation ).
multiplication +++>
   ( sequenced(exponentiative_operation, ++)
   ; factor ),
   sign(multiply),
   ( sequenced(exponentiative_operation, ++)
   ; factor
   ; unary_operation ).


/* factor -++>
      */

factor -++>
   ( bracketed_arithmetic_expression
   ; number_literal
   ; data_name ).


/* bracketed_arithmetic_expression([Value], Xs) --> <-
      */

bracketed_arithmetic_expression(As, Xs) -->
   sign(left_parenthesis, As1, Xs1),
   consume_until_closing_right_parenthesis(1, Xs2),
   sign(right_parenthesis, As3, Xs3),
   { arithmetic_expression(As2, Ae, Xs2, []),
     append([Xs1, Ae, Xs3], Xs123),
     flatten([As1, As2, As3], As123),
     cobolml_parser_tedcg_help:
        process_interim_attribute_list(As123, As),
     list_to_first_and_last(Xs123, First, Last),
     Start := First/start,
     End := Last/end,
     Xs = [bracketed_arithmetic_expression:As:[Start, End|Xs123]] }.


/* consume_until_closing_right_parenthesis(Quantity, Xs) --> <-
      */

consume_until_closing_right_parenthesis(Quantity, Xs) -->
   consume_until_rule(sign(right_parenthesis), **, _, Xs_1),
   { findall( L,
        ( member(L, Xs_1),
          left_parenthesis := L/tag::'*' ),
        Ls ),
     length(Ls, Quantity_left_parentheses),
     Quantity_2 is Quantity + Quantity_left_parentheses - 1 },
   ( { Quantity_2 = 0 } ->
       { Xs = Xs_1 }
     ; right_parenthesis_sign(Xs_2),
       consume_until_closing_right_parenthesis(Quantity_2, Xs_3),
       { append([Xs_1, Xs_2, Xs_3], Xs) } ).


/* unary_plus +++>
      */

unary_plus +++>
   sign(plus),
   ( multiplicative_term
   ; [X],
     { fn_item_parse(X, Tag:_:_),
       member(Tag, [unary_plus, unary_minus]) } ).


/* unary_minus +++>
      */

unary_minus +++>
   sign_(hyphen),
   ( multiplicative_term
   ; [X],
     { fn_item_parse(X, Tag:_:_),
       member(Tag, [unary_plus, unary_minus]) } ).


/* sequence_unary_operation([Value], Xs) <-
      Gives a sequence of unary operations back */

sequence_unary_operation([Value], Xs) -->
   get_all_successive_unary_operators(Ops),
   { Ops \= [],
     reverse(Ops, Ops_2) },
   sequence_unary_plus_minus(Ops_2, [Value], Xs).


/* get_all_successive_unary_operators(Xs) --> <-
      Gives all successive unary operators through Xs back */

get_all_successive_unary_operators([], [], []) :-
   !.
get_all_successive_unary_operators(Xs) -->
   ( sign_plus_minus(_, Xs_1),
     !,
     get_all_successive_unary_operators(Xs_2),
     { ( Xs_2 = [] ->
         Xs = [Xs_1]
       ; append([Xs_1], Xs_2, Xs) ) }
   ; [],
     { Xs = [] } ).


/* sign_(hyphen) -++>
      */

sign_(hyphen) -++>
   sequenced(separator_left, **),
   --->(_^[X], [minus:As:Cs]),
   { fn_item_parse(X, hyphen:As:Cs),
     area(X, 'B') },
   sequenced(separator_right, ++).


/* sign_plus_minus --+>
      */

sign_plus_minus --+>
   ( sign(plus)
   ; sign_(hyphen) ).


/* sequence_unary_plus_minus(Ops, Vs, Xs) --> <-
      Builds a sequence of unary operations out of the reversed
      list Ops of lists of a unary operator Op with its precending
      left separators and successive right separators.
      Finally the return Xs holds the unary operators in the
      correct position */

sequence_unary_plus_minus([], [Value], [X]) -->
   [X],
   { fn_item_parse(X, Tag:_:_),
     member(Tag, [unary_minus, unary_plus]),
     Value := X@value },
   !.
sequence_unary_plus_minus([Op|Ops], Vs, Xs, Ys, Zs) :-
   append(Op, Ys, Ys_1),
   ( unary_plus(_, Xs_1, Ys_1, Ys_2)
   ; unary_minus(_, Xs_1, Ys_1, Ys_2) ),
   append(Xs_1, Ys_2, Ys_3),
   sequence_unary_plus_minus(Ops, Vs, Xs, Ys_3, Zs).


/******************************************************************/


