

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: STOP                        ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_stop, [
       stop_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').


/*** interface ****************************************************/


/* stop_instruction +++>
      */

stop_instruction +++>
   keyword('STOP'),
   ( keyword('RUN')
   ; literal ),
   point.


/******************************************************************/


