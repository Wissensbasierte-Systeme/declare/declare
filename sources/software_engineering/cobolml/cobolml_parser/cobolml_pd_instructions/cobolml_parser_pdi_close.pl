

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: CLOSE                       ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_close, [
       close_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions_help).


/*** interface ****************************************************/


/* close_instruction +++>
      */

close_instruction +++>
   keyword('CLOSE'),
   sequenced(close_file_mode, ++),
   point.


/*** implementation ***********************************************/


/* close_file_mode -++>
      */

close_file_mode -++>
   file_name,
   optional_rule(close_file_mode_12).


/* close_file_mode_1 -++>
      */

close_file_mode_1 -++>
   ( keyword('REEL')
   ; keyword('UNIT') ),
   optional_rule(close_file_mode_1_).


/* close_file_mode_1_ -++>
      */

close_file_mode_1_ -++>
   ( no_rewind_clause
   ; optional_word('FOR'),
     keyword('REMOVEL') ).


/* close_file_mode_12 -++>
      */

close_file_mode_12 -++>
   ( close_file_mode_1
   ; close_file_mode_2 ).


/* close_file_mode_2 -++>
      */

close_file_mode_2 -++>
   ( no_rewind_clause
   ; with_lock_clause ).


/* with_lock_clause +++>
      */

with_lock_clause +++>
   optional_word('WITH'),
   keyword('LOCK').


/******************************************************************/



