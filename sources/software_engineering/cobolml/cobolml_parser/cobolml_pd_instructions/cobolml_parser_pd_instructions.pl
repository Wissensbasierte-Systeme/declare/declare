

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PD Instructions                  ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pd_instructions, [
       instruction/4,
       instruction_without_delimiting_point/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions_catcher).


/*** interface ****************************************************/


/* instruction -++>
      */

instruction -++>
   detect_next_instruction(Instruction),
   { export_list(cobolml_parser_pd_instructions_catcher, Is),
     member(Instruction/_, Is),
     ! },
   call(Instruction),
   !.
instruction -++>
   call(exit_program_instruction).
 /*
instruction -++>
   { export_list(cobolml_parser_pd_instructions_catcher, Is),
     !,
     member(I, Is),
     Functor/_ = I },
   call(Functor).
 */


/* instruction_without_delimiting_point -++>
      */

instruction_without_delimiting_point -++>
   { dislog_variable_set( cobolml_instruction_mode,
        no_delimiting_points_allowed ) },
   instruction,
   { dislog_variable_set( cobolml_instruction_mode, null ) }.
instruction_without_delimiting_point --+>
   { dislog_variable_set( cobolml_instruction_mode, null ) }.


/*** implementation **********************************************/


/* detect_next_instruction(-Instruction) --+>
      */

detect_next_instruction(Instruction) --+>
   next_rule( detect_next_instruction_(Instruction) ).


/* detect_next_instruction_(-Instruction) --+>
      */

detect_next_instruction_(Instruction) --+>
   sequenced(separator_left, **),
   keyword(Value),
   { downcase_atom(Value, Value2),
     atom_concat(Value2, '_instruction', Instruction) }.


/******************************************************************/


