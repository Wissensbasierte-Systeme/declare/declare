

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: COMPUTE                     ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_compute, [
       compute_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions_help_arithmetic_expr).
:- use_module(cobolml_parser_pd_instructions_help).


/*** interface ****************************************************/


/* compute_instruction +++>
      */

compute_instruction +++>
   keyword('COMPUTE'),
   sequenced(compute_receiver, ++),
   ( keyword('EQUAL')
   ; sign('=') ),
   ( data_name
   ; arithmetic_expression ),
   optional_rule(on_size_error_clause),
   optional_rule(not_on_size_error_clause),
   ( instruction_delimiter('END-COMPUTE')
   ; point ).


/*** implementation ***********************************************/


/* compute_receiver -++>
      */

compute_receiver -++>
   data_name,
   optional_rule(rounded_clause).


/******************************************************************/


