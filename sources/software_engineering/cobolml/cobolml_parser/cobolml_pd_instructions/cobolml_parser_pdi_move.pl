

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: MOVE                        ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_move, [
       move_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions_help).


/*** interface ****************************************************/


/* move_instruction +++>
      */

move_instruction +++>
   keyword('MOVE'),
   ( data_name
   ; literal ),
   keyword('TO'),
   sequenced(data_name, ++),
   point.
move_instruction +++>
   keyword('MOVE'),
   ( keyword('CORRESPONDING')
   ; keyword('CORR') ),
   data_name,
   keyword('TO'),
   data_name,
   point.


/******************************************************************/


