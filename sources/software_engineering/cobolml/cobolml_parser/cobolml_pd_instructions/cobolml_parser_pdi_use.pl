

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: USE                         ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_use, [
       use_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions_help).


/*** interface ****************************************************/


/* use_instruction +++>
      */

use_instruction +++>
   keyword('USE'),
   keyword('AFTER'),
   optional_word('STANDARD'),
   ( keyword('EXCEPTION')
   ; keyword('ERROR') ),
   keyword('PROCEDURE'),
   optional_word('ON'),
   ( sequenced(file_name, ++)
   ; keyword('INPUT')
   ; keyword('OUTPUT')
   ; keyword('I-O')
   ; keyword('EXTEND') ),
   point.
use_instruction +++>
   keyword('USE'),
   optional_word('FOR'),
   keyword('DEBUGGING'),
   sequenced(use_instruction_debugging, **),
   point.


/* use_instruction_debugging -++>
      */

use_instruction_debugging -++>
   ( paragraph_name
   ; file_name
   ; keyword('ALL'),
     keyword('PROCEDURES') ).


/*****************************************************************/


