

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PDI: ADD                         ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pdi_add, [
       add_instruction/4
       ] ).

:- use_module('../../cobolml_general/tedcg').
:- use_module('../cobolml_parser_tedcg_help').
:- use_module('../cobolml_parser_library').
:- use_module(cobolml_parser_pd_instructions_help).


/*** interface ****************************************************/


/* add_instruction +++>
      */

add_instruction +++>
   keyword('ADD'),
   sequenced(add_assignment, ++),
   ( keyword('TO')
   ; keyword('GIVING') ),
   data_name,
   optional_rule(rounded_clause),
   optional_rule(on_size_error_clause),
   optional_rule(not_on_size_error_clause),
   ( point
   ; instruction_delimiter('END-ADD') ).
add_instruction +++>
   keyword('ADD'),
   ( keyword('CORRESPONDING')
   ; keyword('CORR') ),
   data_name,
   keyword('TO'),
   add_assignment,
   optional_rule(rounded_clause),
   optional_rule(on_size_error_clause),
   optional_rule(not_on_size_error_clause),
   ( point
   ; instruction_delimiter('END-ADD') ).


/*** implementation ***********************************************/


/* add_assignment -++>
      */

add_assignment -++>
   ( data_name
   ; number_literal ).


/******************************************************************/


