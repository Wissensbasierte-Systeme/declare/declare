

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser PD Instructions Catcher          ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_pd_instructions_catcher,
       [] ).


/*** interface ****************************************************/


:- reexport([
%      cobolml_parser_pdi_accept,
       cobolml_parser_pdi_add,
       cobolml_parser_pdi_close,
%      cobolml_parser_pdi_compute,
       cobolml_parser_pdi_display,
       cobolml_parser_pdi_exit,
       cobolml_parser_pdi_exit_program,
       cobolml_parser_pdi_if,
       cobolml_parser_pdi_move,
       cobolml_parser_pdi_open,
       cobolml_parser_pdi_perform,
       cobolml_parser_pdi_read,
       cobolml_parser_pdi_stop,
       cobolml_parser_pdi_use,
       cobolml_parser_pdi_write
       ]).


/******************************************************************/


