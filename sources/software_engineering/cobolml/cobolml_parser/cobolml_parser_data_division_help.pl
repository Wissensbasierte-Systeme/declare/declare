

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser Data Division Help               ***/
/***                                                            ***/
/******************************************************************/


:- module( cobol_parser_data_division_help, [
       data_description_entry/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_parser_tedcg_help).
:- use_module(cobolml_parser_library).


/*** interface ****************************************************/


/* data_description_entry +-+>
      Responsible for level_number 66 */

data_description_entry +-+>
   level_number('66'),
   data_name,
   renames_clause.


/* data_description_entry +-+>
      Responsible for level_number 88 */

data_description_entry +-+>
   level_number('88'),
   condition_name,
   ( value_is_clause
   ; values_are_clause ),
   point.


/* data_description_entry +-+>
      Responsible for level_numbers from 1 to 49 and 77 */

data_description_entry +-+>
   level_number,
   ( data_name
   ; filler ),
   optional_rule(redefines_clause),
   optional_rule(picture_clause),
   optional_rule(usage_clause),
   optional_rule(sign_is_clause),
   optional_rule(occurs_clause),
   optional_rule(synchronized_clause),
   optional_rule(justified_clause),
   optional_rule(blank_when_zero_clause),
   optional_rule(value_is_clause),
   point.


/*** implementation ***********************************************/


/* blank_when_zero_clause +++>
      */

blank_when_zero_clause +++>
   keyword('BLANK'),
   optional_word('WHEN'),
   keyword('ZERO').


/* check_level_number_arity(+Integer_literal) <-
      Level-numbers have to be at least two-digit
      and without a sign */

check_level_number_arity(Integer_literal) :-
   Value := Integer_literal@value,
   atom_chars(Value, Ls),
   length(Ls, L),
   L =< 2,
   [F|_] = Ls,
   atom_to_int(F, I),
   between(0, 9, I).


/* check_level_number_position(+Integer_literal) <-
      */

check_level_number_position(Integer_literal) :-
   Value := Integer_literal@value,
   atom_to_int(Value, N),
   ( member(N, [1, 77]),
     area(Integer_literal, 'A')
   ; between(2, 49, N),
     area(Integer_literal, 'B')
   ; member(N, [66, 88]) ).


/* data_field -++>
      */

%/*
data_field -++>
   sequenced(separator_left, **),
   [X],
   { data_field := X/tag::'*',
     area(X, 'B') },
   sequenced(separator_right, ++).
%*/
 /*
data_field -++>
   sequenced(separator_left, **),
   --->(Ds^consume_until_rule(separator_right, ++), [Data_field]),
   { list_to_first_and_last(Ds, First, Last),
     area(First, 'B'),
     Start := First/start,
     End := Last/end,
     slenderize(Ds, Ds2),
     Data_field = data_field:[]:[Start, End|Ds2] },
   sequenced(separator_right, ++).
 */


/* filler -++>
      */

filler -++>
   sequenced(separator_left, **),
   keyword('FILLER'),
   sequenced(separator_right, ++).


/* index_name -++>
      */

index_name -++>
   sequenced(separator_left, **),
   --->(_^[X], [index_name:As:Cs]),
   { fn_item_parse(X, word:As:Cs),
     area(X, 'B') },
   sequenced(separator_right, ++).


/* justified_clause +++>
      */

justified_clause +++>
   ( keyword('JUSTIFIED')
   ; keyword('JUST') ),
   optional_word('RIGHT').


/* level_number -++>
      */

level_number -++>
   sequenced(separator_left, **),
   --->(_^[I], [level_number:As:Cs]),
   { integer_literal := I/tag::'*',
     check_level_number_arity(I),
     check_level_number_position(I),
     slenderize(I, I2),
     fn_item_parse(I2, integer_literal:As:Cs) },
   sequenced(separator_right, ++).


/* level_number(+Value) -++>
      */

level_number(Value) -++>
   As@level_number,
   { member(value:Value, As) }.


/* occurs_clause +++>
      */

occurs_clause +++>
   keyword('OCCURS'),
   optional_rule(occurs_clause_integer_to),
   integer_literal(+),
   optional_word('TIMES'),
   optional_rule(occurs_clause_depending_on),
   sequenced(occurs_clause_ascending_descending_key, **),
   optional_rule(occurs_clause_indexed).


/* occurs_clause_integer_to -++>
      */

occurs_clause_integer_to -++>
   integer_literal(+),
   optional_word('IS').


/* occurs_clause_depending_on -++>
      */

occurs_clause_depending_on -++>
   keyword('DEPENDING'),
   optional_word('ON'),
   data_name.


/* occurs_clause_ascending_descending_key -++>
      */

occurs_clause_ascending_descending_key -++>
   ( keyword('ASCENDING')
   ; keyword('DESCENDING') ),
   optional_word('KEY'),
   optional_word('IS'),
   sequenced(data_name, ++).


/* occurs_clause_indexed -++>
      */

occurs_clause_indexed -++>
   keyword('INDEXED'),
   optional_word('BY'),
   sequenced(index_name, ++).


/* picture_clause +++>
      */

picture_clause +++>
   ( keyword('PICTURE')
   ; keyword('PIC') ),
   optional_word('IS'),
   data_field.


/* redefines_clause +++>
      */

redefines_clause +++>
   keyword('REDEFINES'),
   data_name.


/* renames_clause +++>
      */

renames_clause +++>
   keyword('RENAMES'),
   data_name,
   optional_rule(renames_clause_).


/* renames_clause_ -++>
      */

renames_clause_ -++>
   ( keyword('THROUGH')
   ; keyword('THRU') ),
   data_name.


/* sign_is_clause +++>
      */

sign_is_clause +++>
   keyword('SIGN'),
   optional_word('IS'),
   ( keyword('LEADING')
   ; keyword('TRAILING') ),
   optional_rule(sign_is_).


/* sign_is_ -++>
      */

sign_is_ -++>
   keyword('SEPARATE'),
   optional_word('CHARACTER').


/* synchronized_clause +++>
      */

synchronized_clause +++>
   ( keyword('SYNCHRONIZED')
   ; keyword('SYNC') ),
   optional_rule(synchronized_clause_).


/* synchronized_clause_ -++>
      */

synchronized_clause_ -++>
   ( keyword('LEFT')
   ; keyword('RIGHT') ).


/* usage_clause +++>
      */

usage_clause +++>
   keyword('USAGE'),
   optional_word('IS'),
   usage_clause_.


/* usage_clause_ -++>
      */

usage_clause_ -++>
   ( keyword('DISPLAY')
   ; keyword('COMPUTATIONAL')
   ; keyword('COMP')
   ; keyword('COMPUTATIONAL-0')
   ; keyword('COMP-0')
   ; keyword('COMPUTATIONAL-1')
   ; keyword('COMP-1')
   ; keyword('COMPUTATIONAL-2')
   ; keyword('COMP-2')
   ; keyword('COMPUTATIONAL-3')
   ; keyword('COMP-3')
   ; keyword('COMPUTATIONAL-4')
   ; keyword('COMP-4')
   ; keyword('COMPUTATIONAL-5')
   ; keyword('COMP-5')
   ; keyword('COMPUTATIONAL-6')
   ; keyword('COMP-6')
   ; keyword('PACKED-DECIMAL')
   ; keyword('BINARY')
   ; keyword('INDEX') ).


/* value_is_clause +++>
      */

value_is_clause +++>
   keyword('VALUE'),
   optional_word('IS'),
   literal.


/* values_are_clause +++>
      */

values_are_clause +++>
   keyword('VALUES'),
   optional_word('ARE'),
   literal,
   optional_rule(values_are_clause_1).


/* values_are_clause_1 -++>
      */

values_are_clause_1 -++>
   ( keyword('THROUGH')
   ; keyword('THRU') ),
   literal,
   sequenced(values_are_clause_2, **).


/* values_are_clause_2 -++>
      */

values_are_clause_2 -++>
   literal,
   ( keyword('THROUGH')
   ; keyword('THRU') ),
   literal.


/*****************************************************************/


