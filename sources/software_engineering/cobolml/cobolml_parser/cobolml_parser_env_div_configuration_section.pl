

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser Environment Division             ***/
/***                           Configuration Section            ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_env_div_configuration_section, [
       configuration_section/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_parser_tedcg_help).
:- use_module(cobolml_parser_library).


/*** interface ****************************************************/


/* configuration_section +-+>
      */

configuration_section +-+>
   configuration_section_header,
   source_computer_paragraph,
   object_computer_paragraph.
   special_names_paragraph.


/*** implementation ***********************************************/


/* alphabet_name_clause +++>
      */

alphabet_name_clause +++>
   optional_word('ALPHABET'),
   alphabet_name,
   optional_word('IS'),
   alphabet_type.


/* alphabet_type -++>
      */

alphabet_type -++>
   ( keyword('STANDARD-1')
   ; keyword('NATIVE')
   ; non_numeric_literal,
     optional_rule(alphabet_type_) ).


/* alphabet_type_ -++>
      */

alphabet_type_ -++>
   ( keyword('THROUGH')
   ; keyword('THRU') ),
   non_numeric_literal,
   sequenced(alphabet_type_also_clause, **).


/* alphabet_type_also_clause +++>
      */

alphabet_type_also_clause +++>
   keyword('ALSO'),
   non_numeric_literal.


/* collating_sequence_clause +++>
      */

collating_sequence_clause +++>
   optional_word('PROGRAM'),
   optional_word('COLLATING'),
   keyword('SEQUENCE'),
   optional_word('IS'),
   ( keyword('ASCII')
   ; alphabet_name ).


/* configuration_section_header +-+>
      */

configuration_section_header +-+>
   keyword('CONFIGURATION', 'A'),
   keyword('SECTION'),
   point.


/* currency_sign_clause +++>
      */

currency_sign_clause +++>
   keyword('CURRENCY'),
   optional_word('SIGN'),
   optional_word('IS'),
   Ns^non_numeric_literal,
   { Currency_sign := (_:Ns)/non_numeric_literal@value,
     atom_length(Currency_sign, 1),
     ( Currency_sign = '$'
     ; cobolml_tokenizer_phase_1_separators:letter(Currency_sign) ),
     --->(_@currency_sign_clause, [currency_sign:Currency_sign]),
     ! }.


/* decimal_point_clause +++>
      */

decimal_point_clause +++>
   keyword('DECIMAL-POINT'),
   optional_word('IS'),
   keyword('COMMA'),
   { --->(_@decimal_point_clause, [decimal_point:comma]),
     ! }.


/* implementor_name_clause +++>
      */

implementor_name_clause +++>
   implementor_name,
   optional_word('IS'),
   mnemonic_name,
   ( on_status_clause,
     optional_rule(off_status_clause)
   ; off_status_clause,
     optional_rule(on_status_clause) ).


/* memory_clause +++>
      */

memory_clause +++>
   keyword('MEMORY'),
   optional_word('SIZE'),
   integer_literal(+),
   ( keyword('WORDS')
   ; keyword('CHARACTERS')
   ; keyword('MODULES') ).


/* object_computer_paragraph +-+>
      */

object_computer_paragraph +-+>
   keyword('OBJECT-COMPUTER', 'A'),
   point,
   programmer_name(object_computer_name),
   optional_rule(memory_clause),
   optional_rule(collating_sequence_clause),
   optional_rule(segment_limit_clause),
   point.


/* off_status_clause +++>
      */

off_status_clause +++>
   keyword('OFF'),
   optional_word('STATUS'),
   optional_word('IS'),
   condition_name.


/* on_status_clause +++>
      */

on_status_clause +++>
   keyword('ON'),
   optional_word('STATUS'),
   optional_word('IS'),
   condition_name.


/* segment_limit_clause +++>
      */

segment_limit_clause +++>
   keyword('SEGMENT-LIMIT'),
   optional_word('IS'),
   integer_literal(+).


/* source_computer_paragraph +-+>
      */

source_computer_paragraph +-+>
   keyword('SOURCE-COMPUTER', 'A'),
   point,
   programmer_name(source_computer_name),
   Dmc^optional_rule(with_debugging_mode_clause),
   { ( Dmc \= [] ->
       dislog_variable_set(cobolml_debugging_mode, yes)
     ; dislog_variable_set(cobolml_debugging_mode, no) ) },
   point.


/* special_names_paragraph +-+>
      */

special_names_paragraph +-+>
   keyword('SPECIAL-NAMES', 'A'),
   point,
   Acs^special_names_paragraph_additional_clauses,
   ( { Acs \= [] } ->
     point
   ; [] ).


/* special_names_paragraph_additional_clauses -++>
      */ 

special_names_paragraph_additional_clauses -++>
   ( sequenced(implementor_name_clause, **)
   ; sequenced(switch_name_clause, **) ),
   sequenced(alphabet_name_clause, **),
   Csc_as@optional_rule(currency_sign_clause),
   Dpc_as@optional_rule(decimal_point_clause),
   { ( member(currency_sign:Currency_sign, Csc_as),
       dislog_variable_set(cobolml_currency_sign, Currency_sign)
     ; dislog_variable_set(cobolml_currency_sign, '$') ),
     ( Dpc_as \= [] ->
       dislog_variable_set(cobolml_decimal_point, comma)
     ; dislog_variable_set(cobolml_decimal_point, point) ) }.


/* switch_name_clause +++>
      */

switch_name_clause +++>
   switch_name,
   ( on_status_clause,
     optional_rule(off_status_clause)
   ; off_status_clause,
     optional_rule(on_status_clause) ).


/* with_debugging_mode_clause +++>
      */

with_debugging_mode_clause +++>
   optional_word('WITH'),
   keyword('DEBUGGING'),
   keyword('MODE').


/******************************************************************/


