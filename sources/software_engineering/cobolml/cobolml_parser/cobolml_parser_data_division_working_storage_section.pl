

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser Data Division                    ***/
/***                           Working-Storage Section          ***/
/***                                                            ***/
/******************************************************************/


:- module( cobol_parser_data_division_working_storage_section, [
       working_storage_section/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_parser_tedcg_help).
:- use_module(cobolml_parser_library).
:- use_module(cobolml_parser_data_division_help).


/*** interface ****************************************************/


/* working_storage_section +-+>
      */

working_storage_section +-+>
   working_storage_section_header,
   sequenced(data_description_entry, **).


/*** implementation ***********************************************/


/* working_storage_section_header +++>
      */

working_storage_section_header +++>
   keyword('WORKING-STORAGE', 'A'),
   keyword('SECTION'),
   point.


/******************************************************************/


