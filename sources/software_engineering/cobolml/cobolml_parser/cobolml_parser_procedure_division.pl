

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser Procedure Division               ***/
/***                                                            ***/
/******************************************************************/


:- module( cobol_parser_procedure_division, [
       procedure_division/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_parser_tedcg_help).
:- use_module(cobolml_parser_library).
:- use_module(
      'cobolml_pd_instructions/cobolml_parser_pd_instructions').
:- use_module(
  'cobolml_pd_instructions/cobolml_parser_pd_instructions_catcher').

/*** interface ****************************************************/


procedure_division +-+>
  procedure_division_header,
  optional_rule(using_clause),
  optional_rule(declaratives_clause),
  sequenced(procedure_division_content, ++).
 

/*** implementation ***********************************************/


/* declaratives_clause +++>
      */

declaratives_clause +++>
   keyword('DECLARATIVES'),
   point,
   sequenced(declaratives_clause_section, ++),
   keyword('END'),
   keyword('DECLARATIVES'),
   point.


/* declaratives_clause_section -++>
      */

declaratives_clause_section -++>
   section_name,
   keyword('SECTION'),
   point,
   use_instruction,
   sequenced(paragraph, **).

   
/* paragraph +-+>
      */

paragraph +-+>
   paragraph_name('A'),
   point,
   sequenced(instruction, **),
   optional_rule(paragraph_delimiter).


/* paragraph_delimiter +-+>
      */

paragraph_delimiter +-+>
   paragraph_name('A'),
   call(exit_instruction),
   point.


/* procedure_division_content -++>
      */

procedure_division_content -++>
  ( section
  ; paragraph
  ; sequenced(instruction, ++) ).


/* procedure_division_header +-+>
      */

procedure_division_header +-+>
  keyword('PROCEDURE', 'A'),
  keyword('DIVISION'), point.


/* section +-+>
      */

section +-+>
   section_name('A'),
   keyword('SECTION'),
   optional_rule(segment_number),
   point,
   sequenced(section_content, **).


/* section_content --+>
      */

section_content --+>
   ( paragraph
   ; sequenced(instruction, ++) ).


/* segment_number -++>
      */

segment_number -++>
   sequenced(separator_left, **),
   --->(_^[X], [segment_number:As:Cs]),
   { fn_item_parse(X, inter_literal:As:Cs),
     area(X, 'B'),
     Value := X@value,
     atom_to_int(Value, Int),
     Int > 0 },
   sequenced(separator_right, ++).


/* using_clause +++>
      */

using_clause +++>
   keyword('USING'),
   sequenced(data_name, ++).


/******************************************************************/


