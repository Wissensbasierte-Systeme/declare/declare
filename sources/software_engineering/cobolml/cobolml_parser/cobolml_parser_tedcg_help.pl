

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser FN-Triple-EDCG Help              ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_tedcg_help, [
       check_attribute_list_transform/2,
       check_attribute_value_pair_transform/2,
       check_fn_triples_transform/2,
       dedicated_tedcg_transform/3,
       dedicated_tedcg_transform/5,
       slenderize/2
       ] ).


/*** interface ****************************************************/


/* dedicated_tedcg_transform(??+>, ?, ?) <-
      The predicate is called after flattening both the forwarded
      attribute lists and the result lists giving As1 and Xs1. The
      lists for As1 and Xs1 come from the called predicates within
      the predicate T.
      A leading T resulting from a leading plus on an fn-triple
      edcg-operator leaves unchanged. The leading plus causes
      the union of all equal and adjacent subelements of Xs1 and
      the derivation of a start and an end subelement giving all
      together the new list of subelements Xs2.
      A middle attribute list As1 resulting from a middle plus
      on an fn-triple edcg-operator is transformed by concatenating
      all values of value attributes and replacing them through the
      concatenated and upcased value for the new value attribute.
      All other attribute/value pairs are assumed.
      The obligatory plus at the end of fn-triple edcg-operators
      causes nothing without a leading plus */

 /* % HIER
dedicated_tedcg_transform(+++>, T:As1:Xs1, instruction:As2:Xs2) :-
   atom(T),
   sub_atom(T, _, 11, 0, instruction),
   !,
   sub_atom(T, 0, _, 12, Identifier),
   term_variables(As1, []),
   term_variables(Xs1, []),
   process_interim_attribute_list(As1, As2),
   list_to_first_and_last(Xs1, First, Last),
   Start := First/start,
   End := Last/end,
   Xs2 = [Start, End, identifier:[]:[Identifier]|Xs1]. % oder als attribut ?!
 */
dedicated_tedcg_transform(+++>, T:As1:Xs1, T:As2:Xs2) :-
   atom(T),
   term_variables(As1, []),
   term_variables(Xs1, []),
   !,
   process_interim_attribute_list(As1, As2),
   list_to_first_and_last(Xs1, First, Last),
   Start := First/start,
   End := Last/end,
   Xs2 = [Start, End|Xs1].
dedicated_tedcg_transform(+-+>, T:[]:Xs1, T:[]:Xs2) :-
   atom(T),
   term_variables(Xs1, []),
   !,
   list_to_first_and_last(Xs1, First, Last),
   Start := First/start,
   End := Last/end,
   Xs2 = [Start, End|Xs1].
dedicated_tedcg_transform(--+>, Xs1, Xs1) :-
   term_variables(Xs1, []),
   !.
dedicated_tedcg_transform(-++>, As1, Xs1, As2, Xs1) :-
   term_variables(As1, []),
   term_variables(Xs1, []),
   !,
   process_interim_attribute_list(As1, As2).


/* check_attribute_list_transform(As0, As1) <-
      Checks the transformation of a list of attribute:value pairs.
      All assumed attributes with new values are tested */

check_attribute_list_transform(As0, As1) :-
   % Treat only attribute/value pairs here
   is_attribute_value_list(As0),
   is_attribute_value_list(As1),
   check_attributes(As1),
   ( foreach(A0:V0, As0) do
     ( member(A0:V1, As1),
       % Attributes are unique, hence V0 has been changed into V1
       R =.. [check_attribute_value_pair_transform, A0:V0, A0:V1],
       M = cobolml_parser_tedcg_help,
       !,
       call(M:R)
     ; true ) ),
   !.


/* check_fn_triples_transform(Xs0, Xs1) <-
      Checks the transformation of a list of fn triples. Certain
      fn triples are fix and not changeable. All other fn-triples
      are checked in various ways */

check_fn_triples_transform(
      [source_text:[]:[Source_text]],
      [source_text:[]:[Source_text]]) :-
   !.
check_fn_triples_transform(
      [newline:As:Xs],
      [newline:As:Xs]) :-
   !.
check_fn_triples_transform(
      [continuation_break:As:Xs],
      [continuation_break:As:Xs]) :-
   !.
check_fn_triples_transform(
      [_:As0:Xs0],
      [T:As:Xs]) :-
   % Check parts of the new fn-triple
   check_attribute_list_transform(As0, As),
   check_subelements(Xs),
   % The new fn triple has to start and end at the same positions
   [Start, End|_] = Xs0,
   [Start, End|_] = Xs,
   % Check consistency related to start/end subelements
   check_fn_triple_consistency(T:As:Xs),
   !,
   % Check if there are forbidden changes of certain elements
   get_fix_elements(Xs0, Source_text, Cbs, Newlines),
   get_fix_elements(Xs, Source_text, Cbs, Newlines),
   !.
check_fn_triples_transform(Xs0, Xs) :-
   is_list(Xs0),
   is_list(Xs),
   list_to_first_and_last(Xs0, F0, L0),
   list_to_first_and_last(Xs, F1, L1),
   % The fn triple lists have to be in the same range
   Start := F0/start,
   Start := F1/start,
   End := L0/end,
   End := L1/end,
   !,
   % Xs has to be consistent related to start/end subelements
   [Sl, Sc] := Start@[line, column],
   [El, Ec] := End@[line, column],
   check_fn_triples_consistencies(Sl, Sc, El, Ec, Xs),
   !,
   % Check if there are forbidden changes of certain elements
   get_fix_elements(Xs0, Source_text, Cbs, Newlines),
   get_fix_elements(Xs, Source_text, Cbs, Newlines),
   !.


/* slenderize(T:As:Xs0, T:As:Xs) <-
      Transforms all elements from Xs0 unequal to start, end,
      newline, continuation_break or source_text elements by
      assuming whose continuation_break and source_text elements.
      Finally all equal and adjacent elements are united */

slenderize(T:As:Xs0, T:As:Xs) :-
   % There has to be at least one element to slenderize
   Xs0 \= [],
   !,
   ( foreach(X0, Xs0),
     foreach(X1, Xs1) do
        Tag := X0/tag::'*',
        ( \+(member(Tag,
                    [start, end, newline,
                     continuation_break, source_text])),
          !,
          cobolml_parser_tedcg_help:
             slenderize_([], [X0], X1)
        ; X1 = X0 ) ),
   flatten(Xs1, Xs2),
   unite_equal_adjacent_subelements(Xs2, Xs),
   !.
slenderize(T:As:[], T:As:[]) :-
   !.


/* slenderize(Xs0s, Xss) <-
      Slenderizes all fn triple from Xs0s giving Xss */

slenderize([Xs0], [Xs]) :-
   !,
   slenderize(Xs0, Xs).
slenderize([Xs0|Xs0s], [Xs|Xss]) :-
   slenderize(Xs0, Xs),
   slenderize(Xs0s, Xss),
   !.


/*** implementation ***********************************************/


/* check_attribute_value_pair_transform(A0:V0, A0:V1) <-
      All transformation of attribute:value pairs are allowed
      iff the attribute stays unaffected and V1 is instantiated */

check_attribute_value_pair_transform(A0:V0, A0:V1) :-
   atom(A0),
   atom(V0),
   nonvar(V1),
   !.


/* check_attributes(As) <-
      */

check_attributes(As) :-
   % No uninstantiated variables allowed
   term_variables(As, []),
   % Due to well-formed xml
   unique_attributes(As),
   % The value attribute has to be at first place iff it exists
   ( [value:_|_] = As
   ; \+(member(value:_, As)) ),
   !.


/* check_fn_triple_consistency([Start, End|Xs]) <-
      Checks the consistency of a fn-triple by checking
      the consistencies of the fn-triples of Xs related
      to start/end subelements */

check_fn_triple_consistency(_:_:[Start, End|Xs]) :-
   [Sl, Sc] := Start@[line, column],
   [El, Ec] := End@[line, column],
   check_fn_triples_consistencies(Sl, Sc, El, Ec, Xs),
   !.


/* check_fn_triples_consistencies(Fsl, Fsc, Fel, Fec, Xs) <-
      The fn triples in Xs has to fit in the range of the forced
      start line Fsl and the forced start column Fsc as well as the
      forced end line Fel and the forced end column Fec.
      Once a fn triple is recognized as a fitting one and it is
      no source_text element, its consistency is also checked */

check_fn_triples_consistencies(Fsl, Fsc, Fsl, Fec, []) :-
   !,
   Fec is Fsc - 1.
check_fn_triples_consistencies(Fsl, Fsc, Fsl, Fec, [X1]) :-
   [source_text, [Source_text]] := X1/[tag::'*', content::'*'],
   atom_length(Source_text, L),
   Fec is Fsc + L - 1,
   !.
check_fn_triples_consistencies(Fsl, Fsc, Fel, Fec, [X1]) :-
   [Fsl, Fsc] := X1/start@[line, column],
   [Fel, Fec] := X1/end@[line, column],
   check_fn_triple_consistency(X1),
   !.
check_fn_triples_consistencies(Fsl, Fsc, Fel, Fec, [X1, X2|Xs]) :-
   [source_text, [Source_text]] := X1/[tag::'*', content::'*'],
   !,
   atom_length(Source_text, L),
   Fsc2 is Fsc + L,
   check_fn_triples_consistencies(Fsl, Fsc2, Fel, Fec, [X2|Xs]).
check_fn_triples_consistencies(Fsl, Fsc, Fel, Fec, [X1, X2|Xs]) :-
   [source_text, [Source_text]] := X2/[tag::'*', content::'*'],
   !,
   [Fsl, Fsc] := X1/start@[line, column],
   [El_X1, Ec_X1] := X1/end@[line, column],
   check_fn_triple_consistency(X1),
   atom_length(Source_text, L),
   Fsc2 is Ec_X1 + L + 1,
   check_fn_triples_consistencies(El_X1, Fsc2, Fel, Fec, Xs).
check_fn_triples_consistencies(Fsl, Fsc, Fel, Fec, [X1, X2|Xs]) :-
   newline := X1/tag::'*',
   [source_text, [Source_text]] := X2/[tag::'*', content::'*'],
   !,
   [Fsl, Fsc] := X1/start@[line, column],
   Fsl2 is Fsl + 1,
   atom_length(Source_text, L),
   Fsc2 is L + 1,
   check_fn_triples_consistencies(Fsl2, Fsc2, Fel, Fec, Xs).
check_fn_triples_consistencies(Fsl, Fsc, Fel, Fec, [X1, X2|Xs]) :-
   newline := X1/tag::'*',
   !,
   [Fsl, Fsc] := X1/start@[line, column],
   [Sl_X2, 1] := X2/start@[line, column],
   Sl_X2 is Fsl + 1,
   [El_X2, Ec_X2] := X2/end@[line, column],
   check_fn_triple_consistency(X2),
   Fsc2 is Ec_X2 + 1,
   check_fn_triples_consistencies(El_X2, Fsc2, Fel, Fec, Xs).
check_fn_triples_consistencies(Fsl, Fsc, Fel, Fec, [X1, X2|Xs]) :-
   newline := X2/tag::'*',
   !,
   [Fsl, Fsc] := X1/start@[line, column],
   [El_X1, Ec_X1] := X1/end@[line, column],
   check_fn_triple_consistency(X1),
   [El_X1, Sc_X2] := X2/start@[line, column],
   Sc_X2 is Ec_X1 + 1,
   check_fn_triple_consistency(X2),
   Fsl2 is El_X1 + 1,
   check_fn_triples_consistencies(Fsl2, 1, Fel, Fec, Xs).
check_fn_triples_consistencies(Fsl, Fsc, Fel, Fec, [X1, X2|Xs]) :-
   [Fsl, Fsc] := X1/start@[line, column],
   [El_X1, Ec_X1] := X1/end@[line, column],
   check_fn_triple_consistency(X1),
   [El_X1, Sc_X2] := X2/start@[line, column],
   Sc_X2 is Ec_X1 + 1,
   [El_X2, Ec_X2] := X2/end@[line, column],
   check_fn_triple_consistency(X2),
   Fsc2 is Ec_X2 + 1,
   check_fn_triples_consistencies(El_X2, Fsc2, Fel, Fec, Xs).


/* check_subelements(Xs) <-
      */

check_subelements(Xs) :-
   % No uninstantiated variables allowed
   term_variables(Xs, []),
   % There has to be exactly one start/end subelement
   Start = start:_:_,
   End = end:_:_,
   findall( Start,
      member(Start, Xs),
      [Start] ),
   findall( End,
      member(End, Xs),
      [End] ),
   % The start/end subelements come at first/second place
   % There has to be at least one more element
   [Start, End|_] = Xs,
   !.


/* format_value(Vs, Value) <-
      Transforms every value atom excepted points in Vs into
      an atom with an appended space for separation reasons.
      Afterwards the atoms are concatenated and given back
      in upcase letters through Value */

format_value(Vs, Value) :-
   format_value_([], Vs, Value),
   !.

format_value_(Accs, [V], Value) :-
   reverse([V|Accs], Accs2),
   concat_atom(Accs2, Value0),
   upcase_atom(Value0, Value),
   !.
format_value_(Accs, [V1, '.'|Vs], Value) :-
   !,
   format_value_([V1|Accs], ['.'|Vs], Value).
format_value_(Accs, [V|Vs], Value) :-
   atom_concat(V, ' ', V2),
   format_value_([V2|Accs], Vs, Value).


/* get_fix_elements(Xs, Source_text, Conti_breaks, Newlines) <-
      Returns the Source_text of (pre-)tokens and checks that
      there is no structure created where a source text element
      has any subelements but the code source text itself.
      It follows that there are not any superordinate source text
      elements having source text elements as a subelement.
      In addition both all continuation break and all newline
      elements are returned through Conti_breaks and Newlines */

get_fix_elements(Xs, Source, Cbs, Ns) :-
   get_fix_elements([], [], [], Xs, Source, Cbs, Ns),
   !.

get_fix_elements(Ac1, Ac2, Ac3, [], Source, Cbs, Ns) :-
   reverse(Ac1, Ss),
   concat_atom(Ss, Source),
   reverse(Ac2, Ts),
   flatten(Ts, Cbs),
   reverse(Ac3, Us),
   flatten(Us, Ns),
   !.
get_fix_elements(Ac1, Ac2, Ac3, [X|Xs], Source, Cbs, Ns) :-
   fn_item_parse(X, source_text:[]:[Text]),
   !,
   ( atom(Text)
   ; fail ),
   get_fix_elements([Text|Ac1], Ac2, Ac3, Xs, Source, Cbs, Ns).
get_fix_elements(Ac1, Ac2, Ac3, [X|Xs], Source, Cbs, Ns) :-
   continuation_break := X/tag::'*',
   !,
   get_fix_elements(Ac1, [X|Ac2], Ac3, Xs, Source, Cbs, Ns).
get_fix_elements(Ac1, Ac2, Ac3, [X|Xs], Source, Cbs, Ns) :-
   newline := X/tag::'*',
   !,
   get_fix_elements(Ac1, Ac2, [X|Ac3], Xs, Source, Cbs, Ns).
get_fix_elements(Ac1, Ac2, Ac3, [X|Xs], Source, Cbs, Ns) :-
   fn_item_parse(X, _:_:Cs),
   append(Cs, Xs, Xs2),
   !,
   get_fix_elements(Ac1, Ac2, Ac3, Xs2, Source, Cbs, Ns).


/* is_attribute_value_list(As) <-
      */

is_attribute_value_list(As) :-
    is_list(As),
    findall( A:V,
       ( member(A:V, As),
         V = _:_ ),
       [] ).


/* process_interim_attribute_list(As1, As2) <-
      Builds the attribut list As2 out of As1 by concatenating
      all values of value attributes and replacing them through
      the concatenated and upcased value for the new value
      attribute. All other attribute/value pairs are assumed */

process_interim_attribute_list(As1, As2) :-
   findall( V,
      member(value:V, As1),
      Vs ),
   length(Vs, L),
   ( L > 1 ->
     % Concat all value attributes to one Value
     format_value(Vs, Value),
     findall( W,
        ( member(W, As1),
          W \= value:_ ),
        Ws1 ),
     append([value:Value], Ws1, As2)
   ; As2 = As1 ).


/* slenderize_(Accs, Xs0, Xs) <-
      Cuts all start and end elements while all fn triple equal
      to continuation_break or source_text are assumed. All other
      elements are slenderized again */

slenderize_(Accs, [], Xs) :-
   !,
   reverse(Accs, Accs2),
   flatten(Accs2, Xs).
slenderize_(Accs, [T:_:_|Xs0], Xs) :-
   member(T, [start, end]),
   !,
   slenderize_(Accs, Xs0, Xs).
slenderize_(Accs, [T:As:Cs|Xs0], Xs) :-
   member(T, [continuation_break, source_text]),
   !,
   slenderize_([T:As:Cs|Accs], Xs0, Xs).
slenderize_(Accs, [_:_:Cs|Xs0], Xs) :-
   slenderize_([], Cs, Cs2),
   slenderize_([Cs2|Accs], Xs0, Xs),
   !.


/* unique_attributes(As) <-
      Checks if all attributes are unique due to well-formed xml */

unique_attributes([]) :-
   !.
unique_attributes([_:_]) :-
   !.
unique_attributes([A:V|As]) :-
   findall( A:V,
      member(A:V, As),
      [] ),
   !,
   unique_attributes(As).


/* unite_equal_adjacent_subelements(Ys, Zs) <-
      Builds a subelement Y out of two equal
      adjacent subelements Y_1 and Y_2 iff
      Y_1 and Y_2 have the same tag */

unite_equal_adjacent_subelements([], []) :-
   !.
unite_equal_adjacent_subelements([Y_1, Y_2|Ys], Zs) :-
   fn_item_parse(Y_1, source_text:[]:[Value_1]),
   fn_item_parse(Y_2, source_text:[]:[Value_2]),
   atom_concat(Value_1, Value_2, Value),
   Y = source_text:[]:[Value],
   unite_equal_adjacent_subelements([Y|Ys], Zs).
unite_equal_adjacent_subelements([Y_1, Y_2|Ys], Zs) :-
   fn_item_parse(Y_1, Tag:As_1:_),
   fn_item_parse(Y_2, Tag:As_2:_),
   [Value_1, Start] := Y_1-[@value, /start],
   [Value_2, End] := Y_2-[@value, /end],
   atom_concat(Value_1, Value_2, Value),
   !,
   % Assume all equal attributes excepted the value-attribute
   subtract(As_1, [value:_], As_1_new),
   subtract(As_2, [value:_], As_2_new),
   intersection(As_1_new, As_2_new, As_12),
   Y = Tag:[value:Value|As_12]:[Start, End, source_text:[]:[Value]],
   unite_equal_adjacent_subelements([Y|Ys], Zs).
unite_equal_adjacent_subelements([Y|Ys], [Y|Zs]) :-
   unite_equal_adjacent_subelements(Ys, Zs).


/******************************************************************/


