

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser Environment Division             ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_environment_division, [
       environment_division/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_parser_tedcg_help).
:- use_module(cobolml_parser_env_div_configuration_section).
:- use_module(cobolml_parser_env_div_input_output_section).
:- use_module(cobolml_parser_library).


/*** interface ****************************************************/


/* environment_division +-+>
      */

environment_division +-+>
   environment_division_header,
   configuration_section,
   optional_rule(input_output_section),
   !.


/*** implementation ***********************************************/


/* environment_division_header +-+>
      */

environment_division_header +-+>
   keyword('ENVIRONMENT', 'A'),
   keyword('DIVISION'), point.


/******************************************************************/


