

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser Data Division                    ***/
/***                           File Section                     ***/
/***                                                            ***/
/******************************************************************/


:- module( cobol_parser_data_division_file_section, [
       file_section/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_parser_tedcg_help).
:- use_module(cobolml_parser_library).
:- use_module(cobolml_parser_data_division_help).


/*** interface ****************************************************/


/* file_section +-+>
      */

file_section +-+>
   file_section_header,
   sequenced(file_description_entry, **).


/*** implementation ***********************************************/


/* block_contains_clause +++>
      */

block_contains_clause +++>
   keyword('BLOCK'),
   optional_word('CONTAINS'),
   integer_literal(+),
   optional_rule(block_contains_clause_),
   ( keyword('RECORDS')
   ; keyword('CHARACTERS') ).


/* block_contains_clause_ -++>
      */

block_contains_clause_ -++>
   keyword('TO'),
   integer_literal(+).


/* code_set_clause +++>
      */

code_set_clause +++>
   keyword('CODE-SET'),
   optional_word('IS'),
   alphabet_name.


/* data_record_clause +++>
      */

data_record_clause +++>
   keyword('DATA'),
   ( keyword('RECORD'),
     optional_word('IS')
   ; keyword('RECORDS'),
     optional_word('ARE') ),
   sequenced(data_name_1, ++).


/* file_section_header +-+>
      */

file_section_header +-+>
   keyword('FILE', 'A'),
   keyword('SECTION'),
   point.


/* file_description_entry -++>
      */

file_description_entry -++>
   ( file_description_entry_fd
   ; file_description_entry_sd),
   sequenced(data_description_entry, **).


/* file_description_entry_fd +-+>
      */

file_description_entry_fd +-+>
   keyword('FD', 'A'),
   file_name,
   optional_rule(block_contains_clause),
   optional_rule(record_contains_clause),
   optional_rule(label_records_clause),
   optional_rule(value_of_clause),
   optional_rule(data_record_clause),
   optional_rule(code_set_clause),
   optional_rule(linage_clause),
   point.


/* file_description_entry_sd +-+>
      */

file_description_entry_sd +-+>
   keyword('SD', 'A'),
   file_name,
   optional_rule(record_contains_clause),
   optional_rule(data_records_clause),
   point.


/* label_records_clause +++>
      */

label_records_clause +++>
   keyword('LABEL'),
   ( keyword('RECORD'),
     optional_word('IS')
   ; keyword('RECORDS'),
     optional_word('ARE') ),
   ( keyword('STANDARD')
   ; keyword('OMITTED') ).


/* linage_clause +++>
      */

linage_clause +++>
   keyword('LINAGE'),
   optional_word('IS'),
   ( data_name_1
   ; integer_literal(+) ),
   optional_word('LINES'),
   optional_rule(linage_clause_footing),
   optional_rule(linage_clause_top),
   optional_rule(linage_clause_bottom).


/* linage_clause_footing +-+>
      */

linage_clause_footing +-+>
   optional_word('WITH'),
   keyword('FOOTING'),
   optional_word('AT'),
   ( data_name_1
   ; integer_literal(+) ).



/* linage_clause_top +-+>
      */

linage_clause_top +-+>
   optional_word('LINES'),
   optional_word('AT'),
   keyword('TOP'),
   ( data_name_1
   ; integer_literal(+) ).


/* linage_clause_bottom +-+>
      */

linage_clause_bottom +-+>
   optional_word('LINES'),
   optional_word('AT'),
   keyword('BOTTOM'),
   ( data_name_1
   ; integer_literal(+) ).


/* record_contains_clause +++>
      */

record_contains_clause +++>
   keyword('RECORD'),
   optional_word('CONTAINS'),
   optional_rule(record_contains_clause_),
   integer_literal(+),
   optional_word('CHARACTERS').



/* record_contains_clause_ --+>
      */

record_contains_clause_ -++>
   integer_literal(+),
   keyword('TO').


/* value_of_clause +++>
      */

value_of_clause +++>
   keyword('VALUE'),
   keyword('OF'),
   sequenced(value_of_clause_, ++).


/* value_of_clause_ -++>
      */

value_of_clause_ -++>
   data_name_1,
   optional_word('IS'),
   ( data_name_1
   ; literal ).


/******************************************************************/


