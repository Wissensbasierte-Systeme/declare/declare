

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser Environment Division             ***/
/***                           Input-Output Section             ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_env_div_input_output_section, [
       input_output_section/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_parser_tedcg_help).
:- use_module(cobolml_parser_library).


/*** interface ****************************************************/


/* input_output_section +-+>
      */

input_output_section +-+>
   input_output_section_header,
   file_control_paragraph,
   optional_rule(i_o_control_paragraph).


/*** implementation ***********************************************/


/* access_clause +++>
      */

access_clause +++>
   keyword('ACCESS'),
   optional_word('MODE'),
   optional_word('IS'),
   ( keyword('SEQUENTIAL')
   ; keyword('RANDOM')
   ; keyword('DYNAMIC') ),
   optional_rule(relative_key_clause).


/* alternate_record_key_clause +++>
      */

alternate_record_key_clause +++>
   keyword('ALTERNATE'),
   keyword('RECORD'),
   optional_word('KEY'),
   optional_word('IS'),
   data_name,
   optional_rule(alternate_record_key_clause_).

/* alternate_record_key_clause_ -++>
      */

alternate_record_key_clause_ -++>
   optional_word('WITH'),
   keyword('DUPLICATES').


/* assign_clause +++>
      */

assign_clause +++>
   keyword('ASSIGN'),
   optional_word('TO'),
   sequenced(assign_clause_, ++).


/* assign_clause_ -++>
      */

assign_clause_ -++>
   ( file_name
   ; external_file_name ).


/* external_file_name -++>
      */

external_file_name -++>
   sequenced(separator_left, **),
   --->(_^[X], [external_file_name:As2:Cs]),
   { fn_item_parse(X, non_numeric_literal:As1:Cs),
     apostrophize_value(As1, As2) },
   sequenced(separator_right, ++),
   { --->(_@external_file_name, As2) }.


/* file_control_entry +-+>
      */

file_control_entry +-+>
   select_clause,
   assign_clause,
   optional_rule(reserve_clause),
   organization_clause,
   optional_rule(access_clause),
   optional_rule(file_status_clause),
   point.
file_control_entry +-+>
   select_clause,
   assign_clause,
   optional_rule(reserve_clause),
   organization_clause,
   optional_rule(access_clause),
   record_key_clause,
   optional_rule(alternate_record_key_clause),
   optional_rule(file_status_clause),
   point.
file_control_entry +-+>
   select_clause,
   assign_clause,
   optional_rule(reserve_clause),
   optional_rule(file_status_clause),
   optional_rule(access_clause),
   organization_clause,
   optional_rule(record_key_clause),
   optional_rule(relative_key_clause),
   point.
file_control_entry +-+>
   select_clause,
   assign_clause,
   point.
file_control_entry +-+>
   select_clause,
   assign_clause,
   optional_rule(reserve_clause),
   optional_rule(organization_clause),
   optional_rule(access_clause),
   optional_rule(password_clause),
   optional_rule(file_status_clause),
   point.
file_control_entry +-+>
   select_clause,
   assign_clause,
   optional_rule(reserve_clause),
   organization_clause,
   optional_rule(access_clause),
   optional_rule(password_clause),
   optional_rule(file_status_clause),
   point.
file_control_entry +-+>
   select_clause,
   assign_clause,
   optional_rule(reserve_clause),
   organization_clause,
   optional_rule(access_clause),
   record_key_clause,
   optional_rule(password_clause),
   optional_rule(alternate_record_key_clause),
   optional_rule(password_clause),
   optional_rule(file_status_clause),
   point.


/* file_control_paragraph +-+>
      */

file_control_paragraph +-+>
   keyword('FILE-CONTROL', 'A'),
   point,
   sequenced(file_control_entry, **).


/* file_status_clause +++>
      */

file_status_clause +++>
   keyword('FILE'),
   keyword('STATUS'),
   optional_word('IS'),
   data_name,
   optional_rule(data_name).


/* input_output_section_header +++>
      */

input_output_section_header +++>
   keyword('INPUT-OUTPUT', 'A'),
   keyword('SECTION'),
   point.


/* i_o_control_paragraph +-+>
      */

i_o_control_paragraph +-+>
   keyword('I-O-CONTROL', 'A'),
   point,
   sequenced(input_output_control_entry, **).


/* input_output_control_entry -++>
      */

input_output_control_entry -++>
   keyword('SAME'),
   optional_rule(input_output_control_entry_same),
   optional_word('AREA'),
   optional_word('FOR'),
   sequenced(data_name, ++).
input_output_control_entry -++>
   keyword('RERUN'),
   optional_rule(input_output_control_entry_rerun_1),
   optional_word('EVERY'),
   ( input_output_control_entry_rerun_2
   ; integer_literal(+),
     keyword('CLOCK-UNITS')
   ; condition ).
input_output_control_entry -++>
   keyword('MULTIPLE'),
   keyword('FILE'),
   optional_word('TAPE'),
   optional_word('CONTAINS'),
   sequenced(input_output_control_entry_multiple, ++).
 

/* input_output_control_entry_multiple -++>
      */

input_output_control_entry_multiple -++>
   file_name,
   optional_rule(input_output_control_entry_multiple_).


/* input_output_control_entry_multiple_ -++>
      */

input_output_control_entry_multiple_ -++>
   keyword('POSITION'),
   integer_literal(+).


/* input_output_control_entry_rerun_1 -++>
      */

input_output_control_entry_rerun_1 -++>
   keyword('ON'),
   file_name.


/* input_output_control_entry_rerun_2 -++>
      */

input_output_control_entry_rerun_2 -++>
   ( keyword('END'),
     optional_word('OF'),
     ( keyword('REEL')
     ; keyword('UNIT') )
   ; integer_literal(+),
     keyword('RECORDS') ),
   optional_word('OF'),
   file_name.


/* input_output_control_entry_same -++>
      */

input_output_control_entry_same -++>
   ( keyword('RECORD')
   ; keyword('SORT')
   ; keyword('SORT-MERGE') ).


/* organization_clause +++>
      */

organization_clause +++>
   keyword('ORGANIZATION'),
   optional_word('IS'),
   ( keyword('SEQUENTIAL')
   ; keyword('RELATIVE')
   ; keyword('INDEXED') ).


/* password_clause +++>
      */

password_clause +++>
   keyword('PASSWORD'),
   optional_word('IS'),
   data_name.


/* relative_key_clause +++>
      */

relative_key_clause +++>
   keyword('RELATIVE'),
   optional_word('KEY'),
   optional_word('IS'),
   data_name.


/* record_key_clause +++>
      */

record_key_clause +++>
   keyword('RECORD'),
   optional_word('KEY'),
   optional_word('IS'),
   data_name.


/* reserve_clause +++>
      */
      
reserve_clause +++>
   keyword('RESERVE'),
   integer_literal(+),
   optional_rule(reserve_clause_).


/* reserve_clause_ -++>
      */

reserve_clause_ -++>
   ( Xs^optional_word('AREA'),
     { Xs \= [] }
   ; optional_word('AREAS') ).


/* select_clause +++>
      */

select_clause +++>
   keyword('SELECT'),
   optional_rule( keyword('OPTIONAL') ),
   file_name.


/*****************************************************************/


