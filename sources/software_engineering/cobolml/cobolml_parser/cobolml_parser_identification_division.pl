

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser Identification Division          ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_parser_identification_division, [
       identification_division/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_parser_tedcg_help).
:- use_module(cobolml_parser_environment_division).
:- use_module(cobolml_parser_library).


/*** interface ****************************************************/


/* identification_division +-+>
      */

identification_division +-+>
   identification_division_header,
   program_id_paragraph,
   optional_rule(author_paragraph),
   optional_rule(installation_paragraph),
   optional_rule(date_written_paragraph),
   optional_rule(date_compiled_paragraph),
   optional_rule(security_paragraph),
   !.


/*** implementation ***********************************************/


/* identification_division_header +-+>
      */

identification_division_header +-+>
   ( keyword('IDENTIFICATION', 'A')
   ; keyword('ID', 'A') ),
   keyword('DIVISION'), point.


/* program_id_paragraph +++>
      */

program_id_paragraph +++>
   keyword('PROGRAM-ID', 'A'), point,
   Ps^programmer_name(program_name),
   !,
   { % The program name has to start with a letter
     [Name] := Ps/program_name/source_text,
     sub_atom(Name, 0, 1, _, A),
     cobolml_tokenizer_phase_1_cobol_symbols:letter(A) },
   point.


/* author_paragraph +-+>
      */

author_paragraph +-+>
   keyword('AUTHOR', 'A'), point,
   paragraph_comment.


/* installation_paragraph +-+>
      */

installation_paragraph +-+>
   keyword('INSTALLATION', 'A'), point,
   paragraph_comment.


/* date_written_paragraph +-+>
      */

date_written_paragraph +-+>
   keyword('DATE-WRITTEN', 'A'), point,
   paragraph_comment.


/* date_compiled_paragraph +-+>
      */

date_compiled_paragraph +-+>
   keyword('DATE-COMPILED', 'A'), point,
   paragraph_comment.


/* security_paragraph +-+>
      */

security_paragraph +-+>
   keyword('SECURITY', 'A'), point,
   paragraph_comment.


/* paragraph_comment +-+> % HIER evtl als argument den aufrufparagraph % HIER evtl als argument den aufrufparagraph
      */

paragraph_comment +-+>
   paragraph_comment_.
%  paragraph_comment_,
%  { --->([P]^paragraph_comment, [P2]),
%    slenderize(P, P2) }.

 /*
paragraph_comment_ --+>
   consume_until_rule(separator_right),
   sequenced(separator_right, ++),
   !.
 */
paragraph_comment_ --+>
   ( { member(P, [ author_paragraph,
          installation_paragraph,
          date_written_paragraph,
          date_compiled_paragraph,
          security_paragraph ]) },
     next_rule(
        cobolml_parser_identification_division:P)
   ; next_rule(
        cobolml_parser_environment_division:
           environment_division_header) ).
paragraph_comment_ -++>
   [_],
   paragraph_comment_.


/******************************************************************/


