

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser Data Division                    ***/
/***                           Linkage Section                  ***/
/***                                                            ***/
/******************************************************************/


:- module( cobol_parser_data_division_linkage_section, [
       linkage_section/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_parser_tedcg_help).
:- use_module(cobolml_parser_library).
:- use_module(cobolml_parser_data_division_help).


/*** interface ****************************************************/


/* linkage_section +-+>
      */

linkage_section +-+>
   linkage_section_header,
   sequenced(data_description_entry, **).


/*** implementation ***********************************************/


/* linkage_section_header +++>
      */

linkage_section_header +++>
   keyword('LINKAGE', 'A'),
   keyword('SECTION'),
   point.


/******************************************************************/


