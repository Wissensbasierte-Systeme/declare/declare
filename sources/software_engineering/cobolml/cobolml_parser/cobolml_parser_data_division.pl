

/******************************************************************/
/***                                                            ***/
/***           CobolML: Parser Data Division                    ***/
/***                                                            ***/
/******************************************************************/


:- module( cobol_parser_data_division, [
       data_division/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_parser_tedcg_help).
:- use_module(cobolml_parser_library).
:- use_module(cobolml_parser_data_division_file_section).
:- use_module(cobolml_parser_data_division_working_storage_section).
:- use_module(cobolml_parser_data_division_linkage_section).


/*** interface ****************************************************/


/* data_division +-+>
      */

data_division +-+>
   data_division_header,
   optional_rule(file_section),
   optional_rule(working_storage_section),
   optional_rule(linkage_section),
   !.


/*** implementation ***********************************************/


/* data_division_header +-+>
      */

data_division_header +-+>
   keyword('DATA', 'A'),
   keyword('DIVISION'),
   point.


/******************************************************************/


