

/******************************************************************/
/***                                                            ***/
/***           Triple-EDCG                                      ***/
/***                                                            ***/
/******************************************************************/


:- module( tedcg, [
       op(1200, xfx, +++>),
       op(1200, xfx, +-+>),
       op(1200, xfx, -++>),
       op(1200, xfx, --+>),
       term_expansion/2,
       '$tedcg_append'/4,
       tedcg_transform/3,
       tedcg_transform/5,
       assign_interim_as_and_xs/4,
       assign_final_as_and_xs/8
       ] ).


/*** interface ****************************************************/


/* term_expansion(H1+++>B1, [Rule] <-
      */

term_expansion(H1+++>B1, [Rule]) :-
   tedcg_rule_to_dcg_rule(H1+++>B1, H2-->B2),
   expand_term(H2-->B2, Rule).


/* term_expansion(H1+-+>B1, [Rule]) <-
      */

term_expansion(H1+-+>B1, [Rule]) :-
   tedcg_rule_to_dcg_rule(H1+-+>B1, H2-->B2),
   expand_term(H2-->B2, Rule).


/* term_expansion(H1-++>B1, [Rule]) <-
      */

term_expansion(H1-++>B1, [Rule]) :-
   tedcg_rule_to_dcg_rule(H1-++>B1, H2-->B2),
   expand_term(H2-->B2, Rule).


/* term_expansion(H1--+>B1, [Rule]) <-
      */

term_expansion(H1--+>B1, [Rule]) :-
   tedcg_rule_to_dcg_rule(H1--+>B1, H2-->B2),
   expand_term(H2-->B2, Rule).


/* '$tedcg_append'(As, Xs, Ys, Zs) <-
      */

'$tedcg_append'(As, Xs, Ys, Zs) :-
   append(Xs, Zs, Ys),
   findall( Asi,
      ( member(X, Xs),
        fn_item_parse(X, _:Asi:_) ),
      Ass ),
   flatten(Ass, As).


/*** implementation ***********************************************/


/* tedcg_rule_to_dcg_rule(H1+++>B1, H2-->B2) <-
      */

tedcg_rule_to_dcg_rule(H1+++>B1, H2-->B2) :-
   functor(H1, T, _),
   account_for_meta_operators(T, B1, B1_1, B1_meta,
      Vs_as, Vs_xs, As3, Imtr_as, Xs3, Imtr_xs),
   tedcg_formula_to_dcg_formula(B1_1, B1_dcg, As1, Xs1),
   B2 = ( B1_dcg,
          { Xs1 \= [],
            tedcg_transform(+++>, T:As1:Xs1, T:As2:Xs2),
            assign_interim_as_and_xs(Vs_as, Vs_xs,
               As2, [T:As2:Xs2]),
            B1_meta,
            assign_final_as_and_xs(As, Xs,
               As2, As3, Imtr_as, [T:As2:Xs2], Xs3, Imtr_xs) } ),
   H1 =.. Hs1,
   append(Hs1, [As, Xs], Hs2),
   H2 =.. Hs2.


/* tedcg_rule_to_dcg_rule(H1+-+>B1, H2-->B2) <-
      */

tedcg_rule_to_dcg_rule(H1+-+>B1, H2-->B2) :-
   functor(H1, T, _),
   account_for_meta_operators(T, B1, B1_1, B1_meta,
      Vs_as, Vs_xs, _, fail, Xs3, Imtr_xs),
   tedcg_formula_to_dcg_formula(B1_1, B1_dcg, _, Xs1),
   B2 = ( B1_dcg,
          { Xs1 \= [],
            tedcg_transform(+-+>, T:[]:Xs1, T:[]:Xs2),
            assign_interim_as_and_xs(Vs_as, Vs_xs, [], [T:[]:Xs2]),
            B1_meta,
            assign_final_as_and_xs(_, Xs,
               _, _, fail, [T:[]:Xs2], Xs3, Imtr_xs) } ),
   H1 =.. Hs1,
   append(Hs1, [[], Xs], Hs2),
   H2 =.. Hs2.


/* tedcg_rule_to_dcg_rule(H1-++>B1, H2-->B2) <-
      */

tedcg_rule_to_dcg_rule(H1-++>B1, H2-->B2) :-
   functor(H1, T, _),
   account_for_meta_operators(T, B1, B1_1, B1_meta,
      Vs_as, Vs_xs, As3, Imtr_as, Xs3, Imtr_xs),
   tedcg_formula_to_dcg_formula(B1_1, B1_dcg, As1, Xs1),
   B2 = ( B1_dcg,
          { tedcg_transform(-++>, As1, Xs1, As2, Xs2),
            assign_interim_as_and_xs(Vs_as, Vs_xs, As2, Xs2),
            B1_meta,
            assign_final_as_and_xs(As, Xs,
               As2, As3, Imtr_as, Xs2, Xs3, Imtr_xs) } ),
   H1 =.. Hs1,
   append(Hs1, [As, Xs], Hs2),
   H2 =.. Hs2.


/* tedcg_rule_to_dcg_rule(H1--+>B1, H2-->B2) <-
      */

tedcg_rule_to_dcg_rule(H1--+>B1, H2-->B2) :-
   functor(H1, T, _),
   account_for_meta_operators(T, B1, B1_1, B1_meta,
      Vs_as, Vs_xs, _, fail, Xs3, Imtr_xs),
   tedcg_formula_to_dcg_formula(B1_1, B1_dcg, _, Xs1),
   B2 = ( B1_dcg,
          { tedcg_transform(--+>, Xs1, Xs2),
            assign_interim_as_and_xs(Vs_as, Vs_xs, [], Xs2),
            B1_meta,
            assign_final_as_and_xs(_, Xs,
               _, _, fail, Xs2, Xs3, Imtr_xs) } ),
   H1 =.. Hs1,
   append(Hs1, [[], Xs], Hs2),
   H2 =.. Hs2.


/* account_for_meta_operators(T, B1, B1_regular, B1_meta,
         Vs_as, Vs_xs, As3, Imtr_as, Xs3, Imtr_xs) <-
      Accounts for both inner access/transformation meta operators
      and outer transformation meta operators. The functor T of the
      fn-triple-edcg-rule R and the body B1 of R are inputted.
      B1_regular are the formulas in the body B1 which can simply
      be transformed later with the mechanism for dcg-rules.
      B1_meta is either the term "true" in the case that there
      aren�t any transformation meta operations or an area of
      predicates which check the correctness of the transformation
      meta operations among other things.
      Vs_as and Vs_xs contain the variables for the interim
      attribute and the interim result list of T which are
      requested from inner meta operations and assumed later from 
      the body of B1.
      As3 and Xs3 represent either the empty list in the case that
      there aren�t any transformation meta operations of the attri-
      bute respectively the result list in B1 or they represent
      the new attribute/result list(s) after the corresponding
      transformations.
      The switches Imtr_as and Imtr_xs have the values "fail" or
      "true" in dependency of the existency of inner meta-transfor-
      mations for transforming the interim attribute and result
      lists to As3 respectively Xs3 */

account_for_meta_operators(T, B1, B1_regular, B1_meta,
      Vs_as, Vs_xs, As3, Imtr_as, Xs3, Imtr_xs) :-
   account_for_inner_mops(T, B1, B1_regular, B1_meta02s,
      Vs_as, Vs_xs, As3, Imtr_as, Xs3, Imtr_xs),
   account_for_outer_transformation_mops(B1_regular, B1_meta01s),
   !,
   append(B1_meta01s, B1_meta02s, B1_metas),
   ( B1_metas = [] ->
     B1_meta = ( true )
   ; list_to_comma_structure(B1_metas, B1_meta) ).
 

/* account_for_inner_mops(T, B1, B1_1, B1_meta02s,
         Vs_as, Vs_xs, As3, Imtr_as, Xs3, Imtr_xs) <-
      Accounts for both inner access and inner transformation meta
      operators. The functor T of the fn-triple-edcg-rule R and the
      body B1 of R are inputted.
        In the case that there are inner meta operations, they have
      to be placed in an area F2 which is clamped with brackets and
      concludes B1. Vs_as and  Vs_xs contain the variables for the
      attribute and the result list of T which are requested from
      inner meta operations and assumed later from the body of B1.
      As3 and Xs3 represent the new attribute/result list(s) after
      possibly contained transformation meta operations. B1_meta is
      built of the transformed area F2 and B1_1 represents the
      formulas in the body B1 which can simply be transformed later
      with the mechanism for dcg-rules. It is also checked, that
      there isn�t more than one transformation per attribute/result
      list by the predicate check_inner_meta_transformations. If
      there are more than one, the cut related to the failing pred
      check_inner_meta_transformations leads to a failing
      tedcg_rule_to_dcg_rule-predicate.
        In the case that there aren�t any inner meta operations,
      B1_meta is just the term "true" and B1_1 is represented by
      the original body B1. Of course all variables related to an
      inner transformation meta operation are the empty list */

account_for_inner_mops(T, B1, B1_1, B1_meta02s,
      Vs_as, Vs_xs, As3, Imtr_as, Xs3, Imtr_xs) :-
%  get_single_formulas(B1, Fs), % HIER 03.09.2009
   comma_structure_to_list(B1, Fs),
   ( delete_last_element(Fs, ({F2}), Fs1),
     comma_structure_to_list(F2, Fs2),
     account_for_inner_access_mops(T, Fs2, Fs3,
        Vs_as01, Vs_xs01),
     account_for_inner_transformation_mops(T, Fs3, B1_meta02s,
        Vs_as02, Vs_xs02, As3, Xs3, Imtrs),
     ( Imtrs \= [] ->
       !
     ; true ),
     check_inner_meta_transformations(Imtrs, Imtr_as, Imtr_xs),
     append(Vs_as01, Vs_as02, Vs_as),
     append(Vs_xs01, Vs_xs02, Vs_xs),
     % There has to be at least one T meta predicate
     ( Vs_as \= []
     ; Vs_xs \= [] ),
     list_to_comma_structure(Fs1, B1_1),
     !
   ; B1_1 = B1,
     B1_meta02s = [],
     Imtr_as = fail,
     Imtr_xs = fail,
     Vs_as = [],
     Vs_xs = [],
     As3 = [],
     Xs3 = [] ).


/* account_for_inner_access_mops(T, Fs1, Fs2, Vs_as2, Vs_xs2) <-
      Accounts for inner access meta operators. The functor T of
      the fn-triple-edcg-rule R and the formulas Fs1 are inputted.
        Every meta operation to gain access to an attribute list is
      replaced by an assignment with an placeholder V_as which is
      later filled with the interim attribute list of T. All V_as
      are collected, flattened and given back through Vs_as2.
        Every meta operation to gain access to a result list is
      replaced by an assignment with an placeholder V_xs which is
      later filled with the interim result list of T. All V_xs are
      collected, flattened and given back through Vs_xs2.
      The transformed formulas of Fs1 are given back through Fs2 */

account_for_inner_access_mops(T, Fs1, Fs2, Vs_as2, Vs_xs2) :-
   ( foreach(F1, Fs1),
     foreach(F2, Fs2_),
     foreach(V_as, Vs_as1),
     foreach(V_xs, Vs_xs1) do
        ( F1 = Xs1^T,
%         F2 = ( Xs1 = V_xs ),
          F2 = [ Xs1 = V_xs ],
%         F2 = [],
%         Xs1 = V_xs,
          V_as = []
        ; F1 = As1@T,
%         F2 = ( As1 = V_as ),
          F2 = [ As1 = V_as ],
%         F2 = [],
%         As1 = V_as,
          V_xs = []
        ; F2 = F1,
          V_xs = [],
          V_as = [] ) ),
   flatten(Fs2_, Fs2),
   flatten(Vs_as1, Vs_as2),
   flatten(Vs_xs1, Vs_xs2),
   !.


/* account_for_inner_transformation_mops(T, Fs1, B1_meta02s,
         Vs_as2, Vs_xs2, As3, Imtr_as, Xs3, Imtr_xs) <-
      Accounts for inner transformation meta operators.
      The functor T of the fn-triple-edcg-rule R and the
      formulas Fs1 are inputted.
        Every meta operation to transform an attribute list is
      replaced by two assignments. The first one with an
      placeholder V_as which is later filled with the interim
      attribute list of T representing the original attribute list
      before the transformation. The second one assigns the new
      attribute list. In addition for every transformation meta
      operation a predicate for a later legality-check is inserted.
      This predicates have to be defined by the program which uses
      the corresponding transformation meta operation. The call of
      the predicates is at the end of a fn-triple-operator because
      of the occured instantiation of all involved variables.
        Transformation meta operations for result lists are treated
      analog as the operations for the attribute lists */

account_for_inner_transformation_mops(T, Fs1, B1_meta02s,
      Vs_as2, Vs_xs2, As3, Xs3, Imtrs) :-
   ( foreach(F1, Fs1), foreach(F2, Fs2_), foreach(F3, Fs3_),
     foreach(V_as, Vs_as1), foreach(V_xs, Vs_xs1),
     foreach(As3_, Ass), foreach(Xs3_, Xss), foreach(Imtr, Its_) do
        ( F1 =.. [--->, Access_rule, Ls],
          ( Access_rule = As1@T,
            !,
%           F2 = ( As1 = V_as,
%                  As2 = Ls ),
            As3_ = Ls, % HIER
%           F2 = ( As1 = V_as ),
            F2 = [ As1 = V_as ],
%           As1 = V_as,
%           F2 = [],
            F3 = ( dislog_variable_get(tedcg_help_module, M),
                   M:check_attribute_list_transform(V_as, Ls) ),
            Imtr = [as],
            V_xs = [],
            Xs3_ = []
          ; Access_rule = Xs1^T,
            !,
%           F2 = ( Xs1 = V_xs,
%                  Xs2 = Ls ),
            Xs3_ = Ls,
%           F2 = ( Xs1 = V_xs ), % HIER
            F2 = [ Xs1 = V_xs ], % HIER
%           Xs1 = V_xs,
%           F2 = [],
            F3 = ( dislog_variable_get(tedcg_help_module, M),
%                  M:check_fn_triples_transform(Xs1, Ls) ),
                   M:check_fn_triples_transform(V_xs, Ls) ),
            Imtr = [xs],
            V_as = [],
            As3_ = [] )
        ; F2 = F1,
          F3 = [],
          V_as = [],
          V_xs = [],
          Imtr = [],
          As3_ = [],
          Xs3_ = [] ) ),
   flatten(Fs2_, Fs2),
   flatten(Fs3_, Fs3),
   flatten(Vs_as1, Vs_as2),
   flatten(Vs_xs1, Vs_xs2),
   flatten(Ass, As3),
   flatten(Xss, Xs3),
   flatten(Its_, Imtrs),
   append(Fs2, Fs3, B1_meta02s),
   !.


/* check_inner_meta_transformations(Imtrs, Imtr_as, Imtr_xs) <-
      Checks if there are at most one inner transformation of the
      interim attribute list and at most one inner transformation
      of the interim result list at a time */
      
check_inner_meta_transformations(Imtrs, Imtr_as, Imtr_xs) :-
   is_set(Imtrs),
   ( member(as, Imtrs) ->
     Imtr_as = true
   ; Imtr_as = fail ),
   ( member(xs, Imtrs) ->
     Imtr_xs = true
   ; Imtr_xs = fail ).


/* account_for_outer_transformation_mops(B1_regular, B1_meta01s) <-
      Accounts for outer transformation meta operations. 
      For every transformation meta operation a predicate for a
      later legality-check is inserted. This predicates have to
      be defined by the program which uses the corresponding
      transformation meta operation. The call of the predicates
      is in the body area of B1_meta because of the occured
      instantiation of all involved variables */

account_for_outer_transformation_mops(B1_regular, B1_meta01s) :-
   get_single_formulas(B1_regular, Fs1),
   ( foreach(F1, Fs1),
     foreach(F2, Fs2_) do
        ( F1 =.. [--->, Access_rule, Ls],
          ( Access_rule = As1@P,
            !,
            F2 = ( dislog_variable_get(tedcg_help_module, M),
                   M:check_attribute_list_transform(As1, Ls) )
          ; Access_rule = Xs1^P,
            !,
            F2 = ( dislog_variable_get(tedcg_help_module, M),
                   M:check_fn_triples_transform(Xs1, Ls) ) )
        ; F2 = [] ) ),
   flatten(Fs2_, B1_meta01s).


/* get_single_formulas(Body, Fs) <-
      Divides the Body of a rule in single formulas Fs */

get_single_formulas(Body, Fs) :-
   comma_structure_to_list(Body, Ls),
   ( foreach(L, Ls),
     foreach(List, Lists) do
%       ( L = (_,_), % HIER
        ( L = (_,_),
          tedcg:get_single_formulas(L, List)
        ; L = (_;_),
          semicolon_structure_to_list(L, Ls_),
          ( foreach(L_, Ls_),
            foreach(List_, List) do
               tedcg:get_single_formulas(L_, List_) )
        ; List = [L] ) ),
   append(Lists, Fs).
%  flatten(Lists, Fs), writeln(Fs).


/* tedcg_formula_to_dcg_formula(B1, B2, As, Xs) <-
      */

tedcg_formula_to_dcg_formula(B1, B2, As, Xs) :-
   B1 =.. [--->, Access_rule, Ls],
   callable(Access_rule),
   tedcg_formula_to_dcg_formula(Access_rule, R1, As1, Xs1),
   ( Access_rule = Xs1^P,
     As = As1,
     R2 = ( Xs = Ls )
   ; Access_rule = As1@P,
     Xs = Xs1,
     R2 = ( As = Ls ) ),
   B2 = ( R1, { R2 } ).
tedcg_formula_to_dcg_formula(B1, B2, As, Xs) :-
   B1 = As@B0,
   !,
   tedcg_formula_to_dcg_formula(B0, B2, As, Xs).
tedcg_formula_to_dcg_formula(B1, B2, As, Xs) :-
   B1 = Xs^B0,
   !,
   tedcg_formula_to_dcg_formula(B0, B2, As, Xs).
tedcg_formula_to_dcg_formula(B1, B2, As, Xs) :-
   B1 = M:P1,
   !,
   add_variables_to_predicate(P1, P2, As, Xs),
   B2 = M:P2.
tedcg_formula_to_dcg_formula(B1, B2, As, Xs) :-
   functor(B1, (\+), _),
   !,
   B1 =.. [\+|[Goal]],
   callable(Goal),
   tedcg_formula_to_dcg_formula(Goal, G0, _, _),
   comma_structure_to_list(G0, [Goal2|_]),
   B0 =.. [\+|[Goal2]],
   B2 = (B0, { As = [], Xs = [] }).
tedcg_formula_to_dcg_formula(B1, B2, As, Xs) :-
   B1 = {X},
   !,
   B2 = {X, (As = [], Xs = [])}.
tedcg_formula_to_dcg_formula(B1, B2, As, Xs) :-
   B1 = !,
   !,
   B2 = (!, {(As = [], Xs = [])}).
tedcg_formula_to_dcg_formula(B1, B2, As, Xs) :-
   B1 = (_,_),
   !,
   comma_structure_to_list(B1, Bs1),
   !,
   ( foreach(A, Bs1), foreach(B, Bs2),
     foreach(C, As1), foreach(D, Xs1) do
        tedcg:tedcg_formula_to_dcg_formula(A, B, C, D) ),
   list_to_comma_structure(Bs2, B0),
   B2 = ( B0, { flatten(As1, As),
                flatten(Xs1, Xs) } ),
   !.
tedcg_formula_to_dcg_formula(B1, B2, As, Xs) :-
   B1 = (_;_),
   !,
   semicolon_structure_to_list(B1, Bs1),
   maplist( tedcg_formula_to_dcg_formula_(As, Xs),
      Bs1, Bs2 ),
   list_to_semicolon_structure(Bs2, B2),
   !.
tedcg_formula_to_dcg_formula(B1, B2, As, Xs) :-
   add_variables_to_list(B1, B2, As, Xs).
tedcg_formula_to_dcg_formula(B1, B2, As, Xs) :-
   add_variables_to_predicate(B1, B2, As, Xs).

tedcg_formula_to_dcg_formula_(As, Xs, B1, B2) :-
   tedcg_formula_to_dcg_formula(B1, B2, As, Xs).


/* add_variables_to_list(X1, X2, As, Xs) <-
      */

add_variables_to_list(X1, X2, As, Xs) :-
   is_list(X1),
   !,
   X2 =.. ['$tedcg_append', As, X1],
   Xs = X1.


/* add_variables_to_predicate(X1, X2, As, Xs) <-
      */

add_variables_to_predicate(X1, X2, As, Xs) :-
   X1 =.. Xs1,
   append([Xs1, [As], [Xs]], Xs2),
   X2 =.. Xs2.


/* tedcg_transform(Op, Xs1, Xs2) <-
      */

tedcg_transform(+++>, T:As1:Xs1, T:As2:Xs2) :-
   dislog_variable_get(tedcg_help_module, M),
   M:dedicated_tedcg_transform(+++>, T:As1:Xs1, T:As2:Xs2).
tedcg_transform(+-+>, T:[]:Xs1, T:[]:Xs2) :-
   dislog_variable_get(tedcg_help_module, M),
   M:dedicated_tedcg_transform(+-+>, T:[]:Xs1, T:[]:Xs2).
tedcg_transform(--+>, Xs1, Xs2) :-
   dislog_variable_get(tedcg_help_module, M),
   M:dedicated_tedcg_transform(--+>, Xs1, Xs2).
tedcg_transform(-++>, As1, Xs1, As2, Xs2) :-
   dislog_variable_get(tedcg_help_module, M),
   M:dedicated_tedcg_transform(-++>, As1, Xs1, As2, Xs2).


/* assign_interim_as_and_xs(Vs_as, Vs_xs, As, Xs) <-
      */

assign_interim_as_and_xs(Vs_as, Vs_xs, As, Xs) :-
   ( foreach(V_as, Vs_as) do
        V_as = As ),
   ( foreach(V_xs, Vs_xs) do
        V_xs = Xs ).


/* assign_final_as_and_xs(As, Xs,
         As2, As3, Imtr_as, Xs2, Xs3, Imtr_xs) <- */

assign_final_as_and_xs(As, Xs,
      As2, As3, Imtr_as, Xs2, Xs3, Imtr_xs) :-
   ( Imtr_as = true ->
     As = As3
   ; As = As2 ),
   ( Imtr_xs = true ->
     Xs = Xs3
   ; Xs = Xs2 ).


/******************************************************************/


