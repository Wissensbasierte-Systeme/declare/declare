

/******************************************************************/
/***                                                            ***/
/***           CobolML: General Reserved words                  ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_general_reserved_words, [
       cobol_reserved_word/1
       ] ).       


/*** interface ****************************************************/


cobol_reserved_word(Word) :-
   ansi_85(Word).


/*** implementation ***********************************************/


ansi_85('ACCEPT').
ansi_85('ACCESS').
ansi_85('ADD').
ansi_85('ADVANCING').
ansi_85('AFTER').
ansi_85('ALL').
ansi_85('ALPHABET').
ansi_85('ALPHABETIC').
ansi_85('ALPHABETIC-LOWER').
ansi_85('ALPHABETIC-UPPER').
ansi_85('ALPHANUMERIC').
ansi_85('ALPHANUMERIC-EDITED').
ansi_85('ALSO').
ansi_85('ALTER').
ansi_85('ALTERNATE').
ansi_85('AND').
ansi_85('ANY').
ansi_85('ARE').
ansi_85('AREA').
ansi_85('AREAS').
ansi_85('ASCENDING').
ansi_85('ASSIGN').
ansi_85('AT').
ansi_85('AUTHOR').
ansi_85('BEFORE').
ansi_85('BINARY').
ansi_85('BLANK').
ansi_85('BLOCK').
ansi_85('BOTTOM').
ansi_85('BUFFER').
ansi_85('BY').
ansi_85('CALL').
ansi_85('CANCEL').
ansi_85('CD').
ansi_85('CF').
ansi_85('CH').
ansi_85('CHARACTER').
ansi_85('CHARACTERS').
ansi_85('CLASS').
ansi_85('CLOCK-UNITS').
ansi_85('CLOSE').
ansi_85('COBOL').
ansi_85('CODE').
ansi_85('CODE-SET').
ansi_85('COLLATING').
ansi_85('COLUMN').
ansi_85('COMMA').
ansi_85('COMMON').
ansi_85('COMMUNICATION').
ansi_85('COMP').
ansi_85('COMPUTATIONAL').
ansi_85('COMPUTE').
ansi_85('CONFIGURATION').
ansi_85('CONTAINS').
ansi_85('CONTENT').
ansi_85('CONTINUE').
ansi_85('CONTROL').
ansi_85('CONTROLS').
ansi_85('CONVERTING').
ansi_85('COPY').
ansi_85('CORR').
ansi_85('CORRESPONDING').
ansi_85('COUNT').
ansi_85('CURRENCY').
ansi_85('DATA').
ansi_85('DATE').
ansi_85('DATE-COMPILED').
ansi_85('DATE-WRITTEN').
ansi_85('DAY').
ansi_85('DAY-OF-WEEK').
ansi_85('DE').
ansi_85('DEBUG-CONTENTS').
ansi_85('DEBUG-ITEM').
ansi_85('DEBUG-LINE').
ansi_85('DEBUG-NAME').
ansi_85('DEBUG-SUB-1').
ansi_85('DEBUG-SUB-2').
ansi_85('DEBUG-SUB-3').
ansi_85('DEBUGGING').
ansi_85('DECIMAL-POINT').
ansi_85('DELARATIVES').
ansi_85('DELETE').
ansi_85('DELIMITED').
ansi_85('DELIMITER').
ansi_85('DEPENDING').
ansi_85('DESCENDING').
ansi_85('DESTINATION').
ansi_85('DETAIL').
ansi_85('DISABLE').
ansi_85('DISPLAY').
ansi_85('DIVIDE').
ansi_85('DIVISION').
ansi_85('DOWN').
ansi_85('DUPLICATES').
ansi_85('DYNAMIC').
ansi_85('EGI').
ansi_85('ELSE').
ansi_85('EMI').
ansi_85('ENABLE').
ansi_85('END').
ansi_85('END-ADD').
ansi_85('END-CALL').
ansi_85('END-COMPUTE').
ansi_85('END-DELETE').
ansi_85('END-DIVIDE').
ansi_85('END-EVALUATE').
ansi_85('END-IF').
ansi_85('END-MULTIPLY').
ansi_85('END-OF-PAGE').
ansi_85('END-PERFORM').
ansi_85('END-READ').
ansi_85('END-RECEIVE').
ansi_85('END-RETURN').
ansi_85('END-REWRITE').
ansi_85('END-SEARCH').
ansi_85('END-START').
ansi_85('END-STRING').
ansi_85('END-SUBTRACT').
ansi_85('END-UNSTRING').
ansi_85('END-WRITE').
ansi_85('ENTER').
ansi_85('ENTRY'). % HIER arzdorf hat es
ansi_85('ENVIRONMENT').
ansi_85('EOP').
ansi_85('EQUAL').
ansi_85('ERASE'). % HIER arzdorf hat es
ansi_85('ERROR').
ansi_85('ESI').
ansi_85('EVALUATE').
ansi_85('EVERY').
ansi_85('EXAMINE').
ansi_85('EXCEPTION').
ansi_85('EXIT').
ansi_85('EXTEND').
ansi_85('EXTERNAL').
ansi_85('FALSE'). % HIER eipper nicht
ansi_85('FD').
ansi_85('FILE').
ansi_85('FILE-CONTROL').
ansi_85('FILLER').
ansi_85('FINAL').
ansi_85('FIRST').
ansi_85('FOOTING').
ansi_85('FOR').
ansi_85('FREE'). % HIER arzdorf hat es
ansi_85('FROM').
ansi_85('FUNCTION'). % HIER eipper nicht
ansi_85('GENERATE').
ansi_85('GIVING').
ansi_85('GLOBAL').
ansi_85('GO').
ansi_85('GREATER').
ansi_85('GROUP').
ansi_85('HEADING').
ansi_85('HIGH-VALUE').
ansi_85('HIGH-VALUES').
ansi_85('I-O').
ansi_85('I-O-CONTROL').
ansi_85('IDENTIFICATION').
%HIER ?! ansi_85('ID').
ansi_85('IF').
ansi_85('IN').
ansi_85('INDEX').
ansi_85('INDEXED').
ansi_85('INDICATE').
ansi_85('INITIAL').
ansi_85('INITIALIZE').
ansi_85('INITIATE').
ansi_85('INPUT').
ansi_85('INPUT-OUTPUT').
ansi_85('INSPECT').
ansi_85('INSTALLATION').
ansi_85('INTO').
ansi_85('INVALID').
ansi_85('IS').
ansi_85('JUST').
ansi_85('JUSTIFIED').
ansi_85('KEY').
ansi_85('KEYS'). % HIER arzdorf hat es
ansi_85('LABEL').
ansi_85('LAST').
ansi_85('LEADING').
ansi_85('LEFT').
ansi_85('LENGTH').
ansi_85('LESS').
ansi_85('LIMIT').
ansi_85('LIMITS').
ansi_85('LINAGE').
ansi_85('LINAGE-COUNTER').
ansi_85('LINE').
ansi_85('LINE-COUNTER').
ansi_85('LINES').
ansi_85('LINKAGE').
ansi_85('LOCK').
ansi_85('LOW-VALUE').
ansi_85('LOW-VALUES').
ansi_85('MEMORY').
ansi_85('MERGE').
ansi_85('MESSAGE').
ansi_85('MODE').
ansi_85('MODULES').
ansi_85('MOVE').
ansi_85('MULTIPLE').
ansi_85('MULTIPLY').
ansi_85('NATIVE').
ansi_85('NEGATIVE').
ansi_85('NEXT').
ansi_85('NO').
ansi_85('NOT').
ansi_85('NUMBER').
ansi_85('NUMERIC').
ansi_85('NUMERIC-EDITED').
ansi_85('OBJECT'). % HIER arzdorf hat es
ansi_85('OBJECT-COMPUTER').
ansi_85('OCCURS').
ansi_85('OF').
ansi_85('OFF').
ansi_85('OMITTED').
ansi_85('ON').
ansi_85('ONLY'). % HIER arzdorf hat es
ansi_85('OPEN').
ansi_85('OPTIONAL').
ansi_85('OR').
ansi_85('ORDER'). % HIER arzdorf hat es nicht
ansi_85('ORGANIZATION').
ansi_85('OTHER').
ansi_85('OUTPUT').
ansi_85('OVERFLOW').
ansi_85('PACKED-DECIMAL').
ansi_85('PADDING').
ansi_85('PAGE').
ansi_85('PAGE-COUNTER').
ansi_85('PERFORM').
ansi_85('PF').
ansi_85('PH').
ansi_85('PIC').
ansi_85('PICTURE').
ansi_85('PLUS').
ansi_85('POINTER').
ansi_85('POSITION').
ansi_85('POSITIVE').
ansi_85('PRINTING').
ansi_85('PROCEDURE').
ansi_85('PROCEDURES').
ansi_85('PROCEED').
ansi_85('PROGRAM').
ansi_85('PROGRAM-ID').
ansi_85('PURGE').
ansi_85('QUEUE').
ansi_85('QUOTE').
ansi_85('QUOTES').
ansi_85('RANDOM').
ansi_85('RD').
ansi_85('READ').
ansi_85('RECEIVE').
ansi_85('RECORD').
ansi_85('RECORDS').
ansi_85('REDEFINES').
ansi_85('REEL').
ansi_85('REFERENCE').
ansi_85('REFERENCES').
ansi_85('RELATIVE').
ansi_85('RELEASE').
ansi_85('REMAINDER').
ansi_85('REMOVAL').
ansi_85('RENAMES').
ansi_85('REPLACE').
ansi_85('REPLACING').
ansi_85('REPORT').
ansi_85('REPORTING').
ansi_85('REPORTS').
ansi_85('RERUN').
ansi_85('RESERVE').
ansi_85('RESET'). % HIER habib nicht
ansi_85('RESTART'). % HIER arzdorf hat es
ansi_85('RETURN').
ansi_85('REVERSED').
ansi_85('REWIND').
ansi_85('REWRITE').
ansi_85('RF').
ansi_85('RH').
ansi_85('RIGHT').
ansi_85('ROUNDED').
ansi_85('RUN').
ansi_85('SAME').
ansi_85('SD').
ansi_85('SEARCH').
ansi_85('SECOND'). % HIER arzdorf hat es
ansi_85('SECONDS'). % HIER arzdorf hat es
ansi_85('SECTION').
ansi_85('SECURITY').
ansi_85('SEGMENT').
ansi_85('SEGMENT-LIMIT').
ansi_85('SELECT').
ansi_85('SEND').
ansi_85('SENTENCE').
ansi_85('SEPARATE').
ansi_85('SEQUENCE').
ansi_85('SEQUENTIAL').
ansi_85('SET').
ansi_85('SIGN').
ansi_85('SIZE').
ansi_85('SORT').
ansi_85('SORT-MERGE').
ansi_85('SOURCE').
ansi_85('SOURCE-COMPUTER').
ansi_85('SPACE').
ansi_85('SPACES').
ansi_85('SPECIAL-NAMES').
ansi_85('STANDARD').
ansi_85('STANDARD-1').
ansi_85('STANDARD-2').
ansi_85('START').
ansi_85('STATUS').
ansi_85('STOP').
ansi_85('STRING').
ansi_85('SUB-QUEUE-1').
ansi_85('SUB-QUEUE-2').
ansi_85('SUB-QUEUE-3').
ansi_85('SUBTRACT').
ansi_85('SUM').
ansi_85('SUPPRESS').
ansi_85('SYMBOLIC').
ansi_85('SYNC').
ansi_85('SYNCHRONIZED').
ansi_85('TABLE').
ansi_85('TALLY'). % HIER Arzdorf hat es, siehe aber Habib
ansi_85('TALLYING').
ansi_85('TAPE').
ansi_85('TERMINAL').
ansi_85('TERMINATE').
ansi_85('TEST').
ansi_85('TEXT').
ansi_85('THAN').
ansi_85('THEN').
ansi_85('THROUGH').
ansi_85('THRU').
ansi_85('TIME').
ansi_85('TIMES').
ansi_85('TO').
ansi_85('TOP').
ansi_85('TRAILING').
ansi_85('TRUE').
ansi_85('TYPE').
ansi_85('UNIT').
ansi_85('UNSTRING').
ansi_85('UNTIL').
ansi_85('UP').
ansi_85('UPON').
ansi_85('USAGE').
ansi_85('USE').
ansi_85('USING').
ansi_85('VALUE').
ansi_85('VALUES').
ansi_85('VARYING').
ansi_85('WHEN').
ansi_85('WITH').
ansi_85('WORDS').
ansi_85('WORKING-STORAGE').
ansi_85('WRITE').
ansi_85('ZERO').
ansi_85('ZEROES').
ansi_85('ZEROS').


/******************************************************************/


