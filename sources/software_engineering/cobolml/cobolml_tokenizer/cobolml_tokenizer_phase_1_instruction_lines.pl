

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Phase 1 Instruction lines     ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer_phase_1_instruction_lines, [
       comment_line/4,
       comment_line_with_page_feed/4,
       continuation_line/4,
       debugging_line/4,
       empty_line/4,
       regular_line/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_tokenizer_tedcg_help).
:- use_module(cobolml_tokenizer_library).


/*** interface ****************************************************/


/* comment_line +++>
      Builds a comment line out of cobol symbols */

comment_line +++>
   comment_line_1,
   optional_rule(comment),
   comment_line_2,
   !.
comment_line_1 --+>
   sequence_number_area,
   indicator_area(comment_line_tag).
comment_line_2 --+>
   optional_rule(program_id_area).


/* comment_line_with_page_feed +++>
      Builds a comment line with page feed out of cobol symbols */

comment_line_with_page_feed +++>
   comment_line_with_page_feed_1,
   optional_rule(comment),
   comment_line_with_page_feed_2,
   !.
comment_line_with_page_feed_1 --+>
   sequence_number_area,
   indicator_area(comment_line_with_page_feed_tag).
comment_line_with_page_feed_2 --+>
   optional_rule(program_id_area).


/* continuation_line --+>
      Builds certain elements out of cobol symbols */

continuation_line --+>
   sequence_number_area,
   indicator_area(continuation_line_tag),
   program_text_area,
   optional_rule(program_id_area),
   optional_rule(newline).


/* debugging_line --+>
      Builds certain elements out of cobol symbols */

debugging_line --+>
   sequence_number_area,
   indicator_area(debugging_line_tag),
   program_text_area,
   optional_rule(program_id_area),
   optional_rule(newline).


/* empty_line +-+>
      Builds an empty line out of cobol symbols */

empty_line --+>
   empty_line_only_newline,
   !.
empty_line +-+>
   ( empty_line_ordinary
   ; empty_line_eof ),
   !.


/* regular_line --+>
      Builds elements out of cobol symbols */

regular_line --+>
   sequence_number_area,
   indicator_area(regular_line_tag),
   program_text_area,
   optional_rule(program_id_area),
   optional_rule(newline).


/*** implementation ***********************************************/


/* comment +++>
      Subsumes all cobol symbols within the program text area
      in one comment token */

comment +++>
   comment_(65),
   { --->([C]^comment, [C2]),
     slenderize(C, C2) }.

comment_(0) -++>
   !.
comment_(_) -++>
   next_rule(newline),
   !.
comment_(N) -++>
   [_],
   { M is N - 1 },
   !,
   comment_(M).
comment_(_) -++>
   !.

 
/* empty_line_ --+>
      */

empty_line_ --+>
   optional_rule(sequence_number_area),
   optional_rule(indicator_area(regular_line_tag)),
   sequenced(space, **),
   optional_rule(program_id_area).


/* empty_line_ordinary --+>
      */

empty_line_ordinary --+>
   Es^empty_line_,
   { Es \= [] },
   next_rule(newline),
   !.


/* empty_line_only_newline([], [X], [Y], [Y]) <-
      */

empty_line_only_newline([], [X], [Y], [Y]) :-
   [newline, Line] := Y/[tag::'*', start@line],
   X = empty_line:[]:[
          start:[line:Line, column:0]:[], 
          end:[line:Line, column:0]:[],
          source_text:[]:[] ],
   !.


/* empty_line_eof --+>
      */

empty_line_eof --+>
   empty_line_,
   '$eof',
   !.


/* indicator_area(Line_tag) -++>
      Subsumes the indicator area in one token
      tagged subject to the indicator area */

indicator_area(comment_line_tag) -++>
   --->(_^[X], [comment_line_tag:As:Cs]),
   { fn_item_parse(X, multiply:As:Cs) }.
indicator_area(comment_line_with_page_feed_tag) -++>
   --->(_^[X], [comment_line_with_page_feed_tag:As:Cs]),
   { fn_item_parse(X, divide:As:Cs) }.
indicator_area(continuation_line_tag) -++>
   --->(_^[X], [continuation_line_tag:As:Cs]),
   { fn_item_parse(X, hyphen:As:Cs) }.
indicator_area(debugging_line_tag) -++>
   --->(_^[X], [debugging_line_tag:[value:'D']:Cs]),
   { fn_item_parse(X, letter:_:Cs),
     Value := X@value,
     upcase_atom(Value, 'D') }.
indicator_area(regular_line_tag) -++>
   --->(_^[X], [regular_line_tag:As:Cs]),
   { fn_item_parse(X, space:As:Cs) }.


/* letter(N) +++>
      Subsumes up to N leading letter cobol symbols
      in one letter pretoken */

letter(N) +++>
   letter_(N).

letter_(0) -++>
   !.
letter_(_) -++>
   next_rule(newline),
   !.
letter_(N) -++>
   [X],
   { letter := X/tag::'*',
     M is N - 1 },
   !,
   letter_(M).
letter_(_) -++>
   !.


/* numeric(N) +++>
      Subsumes up to N leading numeric cobol symbols
      in one numeric pretoken */

numeric(N) +++>
   numeric_(N).

numeric_(0) -++>
   !.
numeric_(_) -++>
   next_rule(newline),
   !.
numeric_(N) -++>
   [X],
   { numeric := X/tag::'*',
     M is N - 1 },
   !,
   numeric_(M).
numeric_(_) -++>
   !.


/* program_id_area +++>
      Subsumes the program id area in one token
      by cosuming the next 8 cobol symbols
      iff they are unequal to a newline token */

program_id_area +++>
   program_id_area_(8),
   { --->([X1]^program_id_area, [X2]),
     slenderize(X1, X2) }.

program_id_area_(0) -++>
   !,
   ( next_rule(newline)
   ; '$eof' ).
program_id_area_(_) -++>
   next_rule(newline),
   !.
program_id_area_(N) -++>
   [_],
   { M is N - 1 },
   !,
   program_id_area_(M).
program_id_area_(_) -++>
   !.


/* program_text_area --+>
      Subsumes at a time adjacent letter, space and numeric
      pretokens within the program text area in one pretoken
      with the respective tag for effectiveness reasons */

program_text_area --+>
   Xs^program_text_area_(65),
   !,
   { Xs \= [],
     Xs \= [space:_:_] }.

program_text_area_(0) --+>
   !.
program_text_area_(_) --+>
   next_rule(newline),
   !.
program_text_area_(N) --+>
   { member(R, [letter, space, numeric]),
     Rule =.. [R, N] },
   --->([X]^call(Rule), [X2]),
   { slenderize(X, X2),
     [S_column, E_column] := X/[start@column, end@column],
     L is N - (E_column - S_column + 1) },
   program_text_area_(L).
program_text_area_(N) --+>
   [_],
   { M is N - 1 },
   !,
   program_text_area_(M).
program_text_area_(_) --+>
   !.


/* sequence_number_area +++>
      Subsumes the sequence number area in one token
      by consuming the next 6 cobol symbols
      iff they are unequal to a newline token */

sequence_number_area +++>
   !,
   sequence_number_area_(6),
   { --->([X1]^sequence_number_area, [X2]),
     slenderize(X1, X2) }.

sequence_number_area_(0) -++>
   !.
sequence_number_area_(_) -++>
   next_rule(newline),
   !.
sequence_number_area_(N) -++>
   [_],
   { M is N - 1 },
   !,
   sequence_number_area_(M).
sequence_number_area_(_) -++>
   !.


/* space(N) +++>
      Subsumes up to N leading space cobol symbols
      in one space pretoken */

space(N) +++>
   space_(N).

space_(0) -++>
   !.
space_(_) -++>
   next_rule(newline),
   !.
space_(N) -++>
   [X],
   { space := X/tag::'*',
     M is N - 1 },
   !,
   space_(M).
space_(_) -++>
   !.


/******************************************************************/


