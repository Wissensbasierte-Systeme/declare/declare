

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Phase 2 Integer-Literals      ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer_phase_2_integer_literals, [
       pretokens_to_integer_literals/2
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_tokenizer_tedcg_help).
:- use_module(cobolml_tokenizer_library).
:- use_module(cobolml_tokenizer_phase_1_cobol_symbols).


/*** interface ****************************************************/


/* pretokens_to_integer_literals(Pretokens_1, Pretokens_2) <-
      Builds tokens of the type integer_literal on pretokens
      (plus-/minus- and numeric-pretokens) in
      every list of pretokens in Pretokens_1 giving Pretokens_2 */

pretokens_to_integer_literals(Pretokens_1, Pretokens_2) :-
   pretokens_to_integer_literals(_, Pretokens_2, Pretokens_1, []).


/*** implementation ***********************************************/


/* pretokens_to_integer_literals --+>
      */

pretokens_to_integer_literals --+>
   '$eof',
   !.
pretokens_to_integer_literals --+>
   integer_literal,
   pretokens_to_integer_literals.
pretokens_to_integer_literals --+>
   consume_until_rule(separator),
   sequenced(separator, ++),
   pretokens_to_integer_literals.
 /*
pretokens_to_integer_literals --+>
   consume_until_rule(separator_left),
   sequence(++, separator_left),
   !,
   pretokens_to_integer_literals.
pretokens_to_integer_literals --+>
   consume_until_rule(separator_right),
   sequence(++, separator_right),
   !.
 */


/* integer_literal +++>
      */

integer_literal +++>
   optional_rule(integer_literal_sign),
   integer_literal_number,
   { % Integer literals consist of at most 18 digits
     [I]^integer_literal,
     Value := I@value,
     atom_chars(Value, Vs),
     findall( D,
        ( member(D, Vs),
          cobolml_tokenizer_phase_1_cobol_symbols:digit(D) ),
        Ds ),
     length(Ds, Length),
     Length =< 18,
     ! }.


/* integer_literal_sign -++>
      */

integer_literal_sign -++>
   algebraic_sign,
   optional_rule(continuation_break),
   !.


/* integer_literal_number +++>
      */

integer_literal_number +++>
   integer_literal_number_,
   { --->([I]^integer_literal_number, [I2]),
     slenderize(I, I2) }.

integer_literal_number_ -++>
   numeric,
   next_rule(separator).
integer_literal_number_ -++>
   numeric,
   continuation_break,
   integer_literal_number_.


/******************************************************************/


