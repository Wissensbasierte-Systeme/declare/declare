

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Phase 2 Word-Literals         ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer_phase_2_words, [
       pretokens_to_words/2
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_tokenizer_tedcg_help).
:- use_module(cobolml_tokenizer_library).


/*** interface ****************************************************/


/* pretokens_to_words(Pretokens_1, Pretokens_2) <-
      Builds tokens of the type word on pretokens
      (numeric-, word- and hyphen-pretokens) in
      every list of pretokens in Pretokens_1 giving Pretokens_2 */

pretokens_to_words(Pretokens_1, Pretokens_2) :-
   pretokens_to_words(_, Pretokens_2, Pretokens_1, []).


/*** implementation ***********************************************/


/* pretokens_to_words --+>
      */

pretokens_to_words --+>
   '$eof',
   !.
pretokens_to_words --+>
   --->([W]^word, [W2]),
   { % Words consist of at most 30 characters
     Value := W@value,
     atom_length(Value, L),
     L =< 30,
     slenderize(W, W2) },
   !,
   pretokens_to_words.
pretokens_to_words --+>
   consume_until_rule(separator),
   sequenced(separator, ++),
   pretokens_to_words.
 /*
pretokens_to_words --+>
   consume_until_rule(separator_left),
   sequence(++, separator_left),
   pretokens_to_words.
pretokens_to_words --+>
   consume_until_rule(separator_right),
   sequence(++, separator_right),
   !.
 */


/* word +++>
      */

word +++>
   numeric,
   optional_rule(continuation_break),
   Xs^word_remaining,
   { % A word must contain at least one letter pretoken
     member(letter:_, Xs),
     ! }.
word +++>
   letter,
%  ( next_rule(separator_right),
   ( next_rule(separator),
     !
   ; optional_rule(continuation_break),
     word_remaining ).


/* word_remaining -++>
      */

word_remaining -++>
   ( numeric
   ; letter ),
%  ( next_rule(separator_right)
   ( next_rule(separator)
   ; optional_rule(continuation_break),
     word_remaining ).
word_remaining -++>
   hyphen,
   optional_rule(continuation_break),
   % Word must not end with a hyphen
   word_remaining.


/******************************************************************/


