

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer                               ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer, [
       cobolml_tokenizer/2 ] ).

:- use_module(cobolml_tokenizer_phase_1).
:- use_module(cobolml_tokenizer_phase_2).
:- use_module(cobolml_tokenizer_phase_3).
:- use_module(cobolml_tokenizer_phase_4).


/*** interface ****************************************************/


/* cobolml_tokenizer(Lines, Tokens) <-
      */

cobolml_tokenizer(Lines, Tokens) :-
   dislog_variable_set( tedcg_help_module,
      cobolml_tokenizer_tedcg_help ),
   cobolml_tokenizer_phase_1(Lines, Pretokens1),
   !,
   cobolml_tokenizer_phase_2(Pretokens1, Pretokens2),
   !,
   cobolml_tokenizer_phase_3(Pretokens2),
   !,
   cobolml_tokenizer_phase_4(Pretokens2, Tokens),
   !.


/******************************************************************/


