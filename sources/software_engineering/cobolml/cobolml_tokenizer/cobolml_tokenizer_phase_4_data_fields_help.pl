

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Phase 4 Data Fields Help      ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer_phase_4_data_fields_help, [
       pretokens_to_cobol_symbols/2,
       cobol_symbols_to_field_content_symbols/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_tokenizer_tedcg_help).
:- use_module(cobolml_tokenizer_library).
:- use_module(
      cobolml_tokenizer_phase_4_data_fields_field_content_symbols).


/*** interface ****************************************************/


% DA: 'CR' �ber Zeile gebrochen 'C' continuation_break 'R' erkl�ren
pretokens_to_cobol_symbols([P|Ps], Css) :-
   [L, C] := P/start@[line, column],
   pretokens_to_cobol_symbols(L, C, [], [P|Ps], Css).


cobol_symbols_to_field_content_symbols -++>
   continuation_break,
   cobol_symbols_to_field_content_symbols.
cobol_symbols_to_field_content_symbols -++> % DA: evtl schoen zum zeigen einmal ohne attribute, zweimal mit Funktioniert wenn uebergeordnetes nicht ?-+>
   '$cobol_field_content_symbol',
   cobol_symbols_to_field_content_symbols.
cobol_symbols_to_field_content_symbols --+>
   !.

 
/*** implementation ***********************************************/


pretokens_to_cobol_symbols(_, _, Accs, [], Css) :-
   reverse(Accs, Scca),
   flatten(Scca, Css),
   !.
pretokens_to_cobol_symbols(_, _, Accs, [P|Ps], Css) :-
   [continuation_break, End] := P/[tag::'*', end],
   [L2, E_column] := End@[line, column],
   C2 is E_column + 1,
   !,
   pretokens_to_cobol_symbols(L2, C2, [P|Accs], Ps, Css).
pretokens_to_cobol_symbols(L, C, Accs, [P|Ps], Css) :- % DA: Trennung eines field_content_pretokens ueber zwei pretokens kann nicht sein,
   fn_item_parse(P, source_text:[]:[Source_text]), %  da fcp nur einzelne cobol_symbols sind
   atom_chars(Source_text, Chars),
   atom_length(Source_text, Length),
   End_column is C + Length - 1,
   numlist(C, End_column, Ns),
   ( foreach(Value, Chars),
     foreach(Column, Ns),
     foreach(Csymbol, Csymbols) do
        cobolml_tokenizer_phase_1_cobol_symbols:
           cobol_symbol(Value, Tag),
        Csymbol = Tag:[value:Value]:[
                    start:[line:L, column:Column]:[],
                    end:[line:L, column:Column]:[],
                    source_text:[]:[Value]] ),
   C2 is End_column + 1  ,
   !,
   pretokens_to_cobol_symbols(L, C2, [Csymbols|Accs], Ps, Css).
pretokens_to_cobol_symbols(L, C, Accs, [P|Ps], Css) :-
   fn_item_parse(P, _:_:Cs),
   subtract(Cs, [start:_, end:_], Cs2),
   pretokens_to_cobol_symbols(L, C, _, Cs2, Css0),
   last(Css0, Lsymbol),
   [L2, E_column] := Lsymbol/end@[line, column],
   C2 is E_column + 1,
   !,
   pretokens_to_cobol_symbols(L2, C2, [Css0|Accs], Ps, Css).


'$cobol_field_content_symbol' +++>
   [X1],
   optional_rule(continuation_break),
   [X2],
   { [V1] := X1/source_text/content::'*',
     [V2] := X2/source_text/content::'*',
     atom_concat(V1, V2, Value),
     % Field content symbols with arity 2 are not parenthesisable
     cobol_field_content_symbol(Value, Tag, fail),
     As0 = [value:Value] },
   Pes^optional_rule(parentheses_extension),
   { ( Pes = [Pe],
       1 := Pe/integer@value,
       As1 := Pe/attributes::'*'
     ; As1 = [] ),
     append(As0, As1, As01),
     cobolml_tokenizer_tedcg_help:
%       process_attribute_list(As01, As),
        process_interim_attribute_list(As01, As),
     --->(_@'$cobol_field_content_symbol', As),
     --->([S0]^'$cobol_field_content_symbol', [Tag:As:Xs0]),
     fn_item_parse(S0, _:_:Xs0) }.
'$cobol_field_content_symbol' +++>
   [X],
   { [Value] := X/source_text/content::'*',
     cobol_field_content_symbol(Value, Tag, Parenthesisable),
     As0 = [value:Value] },
   Pes^optional_rule(parentheses_extension),
   { ( Pes = [Pe] ->
       As1 := Pe/attributes::'*'
     ; As1 = [] ),
     ( Parenthesisable ->
       true
     ; ( Pes = []
       ; Pes = [Pe],
         1 := Pe/integer@value ) ),
     append(As0, As1, As01),
     cobolml_tokenizer_tedcg_help:
%       process_attribute_list(As01, As),
        process_interim_attribute_list(As01, As),
     --->(_@'$cobol_field_content_symbol', As),
     --->([S0]^'$cobol_field_content_symbol', [Tag:As:Xs0]),
     fn_item_parse(S0, _:_:Xs0) }.


/* parentheses_extension +++>
      Accounts for the optional parentheses of
      certain parenthesiable field content symbols */

parentheses_extension +++>
   Lps^sequenced(left_parenthesis_fcs, ++),
   integer_literal,
   Rps^sequenced(right_parenthesis_fcs, ++),
   { % Quantities of left and right parentheses must be equal
     Lp = left_parenthesis:_,
     Rp = right_parenthesis:_,
     findall( Lp,
        member(Lp, Lps),
        Ns ),
     findall( Rp,
        member(Rp, Rps),
        Ms ),
     length(Ns, L),
     length(Ms, L) }. 


/* left_parenthesis_fcs -++>
      */

left_parenthesis_fcs -++>
   optional_rule(continuation_break),
   [Lp],
   { left_parenthesis := Lp/tag::'*' }.


/* integer_literal +++>
      Returns the integer which is built of all pretokens
      until the next right parenthesis */

integer_literal +++>
   optional_rule(continuation_break),
   optional_rule(plus),
   optional_rule(continuation_break),
   --->(Ns0^integer_numerics, Ns),
   { slenderize(Ns0, Ns) }.

integer_numerics -++>
   numeric,
   optional_rule(continuation_break),
   ( next_rule([right_parenthesis:_]),
     !
   ; integer_numerics ).
   

/* right_parenthesis_fcs -++>
      */

right_parenthesis_fcs -++>
   optional_rule(continuation_break),
   [Rp],
   { right_parenthesis := Rp/tag::'*' }.


/******************************************************************/


