

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Phase 2                       ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer_phase_2, [
       cobolml_tokenizer_phase_2/2
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_tokenizer_library).
:- use_module(cobolml_tokenizer_phase_2_string_literals).
:- use_module(cobolml_tokenizer_phase_2_words).
:- use_module(cobolml_tokenizer_phase_2_integer_literals).


/*** interface ****************************************************/


/* cobolml_tokenizer_phase_2(Pretokens_1, Pretokens_2) <-
      Join consecutive continued and continuation lines
      represented by lists of pretokens from Pretokens_1 and
      build successively tokens of the types non_numeric_literal,
      word and integer in every list of pretokens in Pretokens_1
      giving the list of pretoken-lists Pretokens_2 */

cobolml_tokenizer_phase_2(Pretokens_1, Pretokens_2) :-
   continued_and_continuation_line_to_line(Pretokens_1, Ps2),
   S = cobolml_tokenizer_phase_2_string_literals,
   W = cobolml_tokenizer_phase_2_words,
   I = cobolml_tokenizer_phase_2_integer_literals,
   ( foreach(Pretokens_line_1, Ps2),
     foreach(Pretokens_line_2, Pretokens_2) do
        S:pretokens_to_string_literals(Pretokens_line_1, Pl1),
        W:pretokens_to_words(Pl1, Pl2),
        I:pretokens_to_integer_literals(Pl2, Pretokens_line_2) ).


/*** implementation ***********************************************/


/* continued_and_continuation_line_to_line(Ps1, Ps2) <-
      Join consecutive continued and continuation lines
      represented by lists of pretokens from Ps1 giving Ps2
      using an accumulator Accs */

continued_and_continuation_line_to_line(Ps1, Ps2) :-
   continued_and_continuation_line_to_line([], Ps1, Ps2).

continued_and_continuation_line_to_line(
      Accs, [Line1, Line2|Ls], Lines) :-
   ( n_th_element(Line2, 2, continuation_line_tag:_) ->
     !,
     % Between a continued and a continuation line
     % is no other line allowed
     n_th_element(Line1, 2, regular_line_tag:_),
     append([Line1, Line2], Line12),
     % Build a continuation_break-pretoken at the fusion point
     pretokens_to_continuation_break_pretoken(Line12, Line),
     Accs2 = Accs
   ; Line = Line2,
     Accs2 = [Line1|Accs] ),
   Ls2 = [Line|Ls],
   continued_and_continuation_line_to_line(Accs2, Ls2, Lines).
continued_and_continuation_line_to_line(Accs, [Line], Lines) :-
   reverse([Line|Accs], Lines).


/* pretokens_to_continuation_break_pretoken(Ps1, Ps2) <-
      Build a continuation_break-pretoken at the fusion point of
      the former list which represented the continued line and
      the former list which represented the continuation line
      in the pretoken-list Ps1 giving the pretoken-list Ps2 */

pretokens_to_continuation_break_pretoken(Ps1, Ps2) :-
   pretokens_to_continuation_break_pretoken(_, Ps2, Ps1, []),
   % Exactly one additional continuation_break-pretoken
   % must be found to be aware of a potentially correct
   % splitting of the separated element
   X = continuation_break:_,
   findall( X, member(X, Ps1), Xs),
   findall( X, member(X, Ps2), Ys),
   length(Xs, L1),
   length(Ys, L2),
   !,
   L2 is L1 + 1.

pretokens_to_continuation_break_pretoken --+>
   continuation_break,
   pretokens_to_continuation_break_pretoken.
pretokens_to_continuation_break_pretoken --+>
   [_],
   pretokens_to_continuation_break_pretoken.
pretokens_to_continuation_break_pretoken --+>
   !.


/******************************************************************/


