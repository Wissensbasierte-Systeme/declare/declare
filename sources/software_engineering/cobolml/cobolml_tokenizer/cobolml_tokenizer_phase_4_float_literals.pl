

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Phase 4 Float-Literals        ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer_phase_4_float_literals, [
       pretokens_to_float_literals/2 ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_tokenizer_tedcg_help).
:- use_module(cobolml_tokenizer_library).
:- use_module(cobolml_tokenizer_phase_1_cobol_symbols).


/*** interface ****************************************************/


/* pretokens_to_float_literals(Pretokens_1, Pretokens_2) <-
      Builds tokens of the type float_literal on pretokens
      (plus-/minus-, numeric- and decimal_point-pretokens) in
      every list of pretokens in Pretokens_1 giving Pretokens_2 */

pretokens_to_float_literals(Pretokens_1, Pretokens_2) :-
   pretokens_to_float_literals(_, Pretokens_2, Pretokens_1, []).


/*** implementation ***********************************************/


/* pretokens_to_float_literals --+>
      */

pretokens_to_float_literals --+>
   '$eof',
   !.
pretokens_to_float_literals --+>
   float_literal,
   pretokens_to_float_literals.
pretokens_to_float_literals --+>
   consume_until_rule(separator),
   sequenced(separator, ++),
   !,
   pretokens_to_float_literals.
 /*
pretokens_to_float_literals --+>
   consume_until_rule(separator_left),
   sequence(++, separator_left),
   !,
   pretokens_to_float_literals.
pretokens_to_float_literals --+>
   consume_until_rule(separator_right),
   sequence(++, separator_right),
   !.
 */


/* float_literal +++>
      */

float_literal +++>
   optional_rule(float_literal_sign),
   optional_rule(float_literal_integer_places),
   float_literal_decimal_point,
   float_literal_decimal_places,
   { % Float literals consist of at most 18 digits
     [F]^float_literal,
     Value := F@value, % DA: Wichtig, sich value aus F zu holen und nicht aus As
     atom_chars(Value, Vs),
     findall( D,
        ( member(D, Vs),
          cobolml_tokenizer_phase_1_cobol_symbols:digit(D) ),
        Ds ),
     length(Ds, Length),
     Length =< 18,
     ! }.


/* float_literal_sign -++>
      */

float_literal_sign -++>
   algebraic_sign,
   optional_rule(continuation_break).


/* float_literal_integer_places -++>
      */

float_literal_integer_places -++>
   float_literal_integer_places_,
   { --->([I]^float_literal_integer_places, [integer_places:As:Xs]),
     slenderize(I, I0),
     fn_item_parse(I0, _:As:Xs) }.

float_literal_integer_places_ -++>
   numeric,
   next_rule(decimal_point).
float_literal_integer_places_ -++>
   numeric,
   continuation_break,
   float_literal_integer_places_.


/* float_literal_decimal_point -++>
      */

float_literal_decimal_point -++>
   decimal_point,
   optional_rule(continuation_break).


/* float_literal_decimal_places +++>
      */

float_literal_decimal_places +++>
   float_literal_decimal_places_,
   { --->([D]^float_literal_decimal_places, [decimal_places:As:Xs]),
     slenderize(D, D0),
     fn_item_parse(D0, _:As:Xs) }.

float_literal_decimal_places_ -++>
   numeric,
%  next_rule(separator_right).
   next_rule(separator).
float_literal_decimal_places_ -++>
   numeric,
   continuation_break,
   float_literal_decimal_places_.


/******************************************************************/


