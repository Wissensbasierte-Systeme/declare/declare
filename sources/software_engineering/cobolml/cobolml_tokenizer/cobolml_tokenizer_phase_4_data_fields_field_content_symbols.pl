

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Phase 4                       ***/
/***                    Data Fields - Field Content Symbols     ***/
/***                                                            ***/
/******************************************************************/


:- module(
     cobolml_tokenizer_phase_4_data_fields_field_content_symbols, [
       cobol_field_content_symbol/3,
       cobol_field_content_symbol/7,
       cobol_ordinary_field_content_symbols/1,
       cobol_non_floating_field_content_symbols/1,
       cobol_floating_field_content_symbols/1
       ] ).


/*** interface ****************************************************/


/* cobol_field_content_symbol(Symbol, Tag, Parenthesisable) <-
      Returns the Tag of Symbol and provides information about
      the possibility of bracketing the Symbol */

cobol_field_content_symbol(Symbol, Tag, Parenthesisable) :-
   cobol_field_content_symbol(
      Symbol, Tag, _, Parenthesisable, _, _, _, _).


/* cobol_field_content_symbol(Symbol, Tag, Type,
         Sides,
         Ordinary_symbols,
         Non_floating_symbols,
         Floating_symbols) <-
      Returns the attributes Tag, Type and Sides of Symbol.
      The Type simply shows the type ('ordinary', 'non_floating',
      'floating') of the field content Symbol.
      The Sides state the possible positions of Symbol.
      Ordinary_symbols, Non_floating_symbols and Floating_symbols
      are the matching symbols (they can still appear later in the
      data field) to Symbol and are returned as well */

cobol_field_content_symbol(Symbol, Tag, Type, Sides,
      Ordinary_symbols, Non_floating_symbols, Floating_symbols) :-
   cobol_field_content_symbol(
      Symbol, Tag, Type, _, Sides,
      Ordinary_symbols, Non_floating_symbols, Floating_symbols).


/* cobol_ordinary_field_content_symbols(Symbols) <-
      Returns all ordinary field content symbols */

cobol_ordinary_field_content_symbols(
   ['9', 'A', 'X', 'S', 'V', 'P':l, 'P':r]).


/* cobol_non_floating_field_content_symbols(Symbols) <-
      Returns all non floating field content symbols */

cobol_non_floating_field_content_symbols(Symbols) :-
   get_cobol_fc_currency(Fc_currency),
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter),
   Symbols = ['B', '0', '/',
              Fc_decimal_point, Fc_delimiter,
              '-':l, '-':r, '+':l, '+':r,
              'CR', 'DB', Fc_currency].


/* cobol_floating_field_content_symbols(Symbols) <-
      Returns all floating field content symbols */

cobol_floating_field_content_symbols(Symbols) :-
   get_cobol_fc_currency(Fc_currency),
   Symbols = ['Z':l, 'Z':r, '*':l, '*':r, '-':l, '+':l, '-':r, '+':r,
              Fc_currency:l, Fc_currency:r].


/*** implementation ***********************************************/


/* cobol_field_content_symbol( Symbol, Tag,
         Type,
         Parenthesisable,
         Sides,
         Ordinary_symbols,
         Non_floating_symbols,
         Floating_symbols ) <-
      The field content symbol is represented by Symbol and Tag.
      The Type simply shows the type ('ordinary', 'non_floating',
      'floating') of the field content Symbol.
      The possibility of bracketing is given by Parenthesisable.
      The Sides state the possible positions of Symbol, where 'l'
      stands for 'before' and 'r' stands for 'after' the (assumed)
      decimal point.
      Ordinary_symbols, Non_floating_symbols and Floating_symbols
      are the matching symbols (they can still appear later in the
      data field) to Symbol and are returned as well */

cobol_field_content_symbol( '9', fc_numeric,
      ordinary,
      true,
      [l, r],
      ['9', 'A', 'X', 'V', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter,
       '-':r, '+':r, 'CR', 'DB'],
      [] ) :-
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( 'A', fc_letter,
      ordinary,
      true,
      [l],
      ['9', 'A', 'X'],
      ['B', '0', '/'],
      [] ).
cobol_field_content_symbol( 'X', fc_alpha,
      ordinary,
      true,
      [l],
      ['9', 'A', 'X'],
      ['B', '0', '/'],
      [] ).
cobol_field_content_symbol( 'S', fc_sign,
      ordinary,
      fail,
      [l],
      ['9', 'V', 'P':r],
      [],
      [] ).
cobol_field_content_symbol( 'V', fc_assumed_decimal_point,
      ordinary,
      fail,
      [l],
      ['9', 'P':l],
      ['B', '0', '/', Fc_delimiter, '-':r, '+':r, 'CR', 'DB'],
      ['Z':r, '*':r, '-':r, '+':r, Fc_currency:r] ) :-
   get_cobol_fc_currency(Fc_currency),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( 'P', fc_decimal_power,
      ordinary,
      true,
      [l],
      ['9', 'P':l],
      ['B', '0', '/', '-':r, '+':r, 'CR', 'DB'],
      ['Z':r, '*':r, '-':r, '+':r, Fc_currency:r] ) :-
   get_cobol_fc_currency(Fc_currency).
cobol_field_content_symbol( 'P', fc_decimal_power,
      ordinary,
      true,
      [r],
      ['V', 'P':r],
      [],
      [] ).
cobol_field_content_symbol( 'B', fc_space,
      non_floating,
      true,
      [l, r],
      ['9', 'A', 'X', 'V', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter,
       '-':r, '+':r, 'CR', 'DB'],
      ['Z':l, 'Z':r, '*':l, '*':r, '-':l, '+':l, '-':r, '+':r,
       Fc_currency:l, Fc_currency:r] ) :-
   get_cobol_fc_currency(Fc_currency),
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( '0', fc_zero,
      non_floating,
      true,
      [l, r],
      ['9', 'A', 'X', 'V', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter,
       '-':r, '+':r, 'CR', 'DB'],
      ['Z':l, 'Z':r, '*':l, '*':r, '-':l, '+':l, '-':r, '+':r,
       Fc_currency:l, Fc_currency:r] ) :-
   get_cobol_fc_currency(Fc_currency),
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( '/', fc_slash,
      non_floating,
      true,
      [l, r],
      ['9', 'A', 'X', 'V', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter,
       '-':r, '+':r, 'CR', 'DB'],
      ['Z':l, 'Z':r, '*':l, '*':r, '-':l, '+':l, '-':r, '+':r,
       Fc_currency:l, Fc_currency:r] ) :-
   get_cobol_fc_currency(Fc_currency),
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( Fc_decimal_point, fc_decimal_point,
      non_floating,
      fail,
      [r],
      ['9', 'V', 'P':r],
      ['B', '0', '/', Fc_delimiter, '-':r, '+':r, 'CR', 'DB'],
      ['Z':l, 'Z':r, '*':l, '*':r, '-':l, '-':l, '+':l, '+':r,
       Fc_currency:l, Fc_currency:r] ) :-
   get_cobol_fc_currency(Fc_currency),
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( Fc_delimiter, fc_delimiter,
      non_floating,
      fail,
      [l, r],
      ['9', 'V', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter,
       '-':r, '+':r, 'CR', 'DB'],
      ['Z':l, 'Z':r, '*':l, '*':r, '-':l, '-':l, '+':l, '+':r,
       Fc_currency:l, Fc_currency:r] ) :-
   get_cobol_fc_currency(Fc_currency),
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( '-', fc_minus,
      non_floating,
      fail,
      [l],
      ['9', 'V', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter],
      ['Z':l, 'Z':r, '*':l, '*':r, Fc_currency:l, Fc_currency:r]) :-
   get_cobol_fc_currency(Fc_currency),
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( '-', fc_minus,
      non_floating,
      fail,
      [r],
      [],
      [],
      []).
cobol_field_content_symbol( '+', fc_plus,
      non_floating,
      fail,
      [l],
      ['9', 'V', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter],
      ['Z':l, 'Z':r, '*':l, '*':r, Fc_currency:l, Fc_currency:r]) :-
   get_cobol_fc_currency(Fc_currency),
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( '+', fc_plus,
      non_floating,
      fail,
      [r],
      [],
      [],
      []).
cobol_field_content_symbol( 'CR', fc_creditor,
      non_floating,
      fail,
      [l, r],
      [],
      [],
      []).
cobol_field_content_symbol( 'DB', fc_debitor,
      non_floating,
      fail,
      [l, r],
      [],
      [],
      [] ).
cobol_field_content_symbol( Fc_currency, fc_currency,
      non_floating,
      fail,
      [l],
      ['9', 'V', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter,
       '-':r, '+':r, 'CR', 'DB'],
      ['Z':l, 'Z':r, '*':l, '*':r, '-':l, '+':l, '-':r, '+':r] ) :-
   get_cobol_fc_currency(Fc_currency),
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( 'Z', fc_z,
      floating,
      true,
      [l],
      ['9', 'V', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter,
       '-':r, '+':r, 'CR', 'DB'],
      ['Z':l, 'Z':r] ) :-
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( 'Z', fc_z,
      floating,
      true,
      [r],
      [],
      ['B', '0', '/', Fc_delimiter, '-':r, '+':r, 'CR', 'DB'],
      ['Z':r] ) :-
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( '*', fc_star,
      floating,
      true,
      [l],
      ['9', 'V', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter,
       '-':r, '+':r, 'CR', 'DB'],
      ['*':l, '*':r] ) :-
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( '*', fc_star,
      floating,
      true,
      [r],
      [],
      ['B', '0', '/', Fc_delimiter, '-':r, '+':r, 'CR', 'DB'],
      ['*':r] ) :-
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( '-', fc_minus,
      floating,
      true,
      [l],
      ['9', 'V', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter],
      ['-':l, '-':r] ) :-
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( '-', fc_minus,
      floating,
      true,
      [r],
      [],
      ['B', '0', '/', Fc_delimiter],
      ['-':r] ) :-
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( '+', fc_plus,
      floating,
      true,
      [l],
      ['9', 'V', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter],
      ['+':l, '+':r] ) :-
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( '+', fc_plus,
      floating,
      true,
      [r],
      [],
      ['B', '0', '/', Fc_delimiter],
      ['+':r] ) :-
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( Fc_currency, fc_currency,
      floating,
      true,
      [l],
      ['9', 'A', 'X', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter,
       '-':r, '+':r, 'CR', 'DB', Fc_currency],
      [Fc_currency:l, Fc_currency:r] ) :-
   get_cobol_fc_currency(Fc_currency),
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).
cobol_field_content_symbol( Fc_currency, fc_currency,
      floating,
      true,
      [r],
      ['9', 'P':r],
      ['B', '0', '/', Fc_decimal_point, Fc_delimiter,
       '-':r, '+':r, 'CR', 'DB', Fc_currency],
      [Fc_currency:r] ) :-
   get_cobol_fc_currency(Fc_currency),
   get_cobol_fc_decimal_point(Fc_decimal_point),
   get_cobol_fc_delimiter(Fc_delimiter).


/* get_cobol_fc_decimal_point(Fc_decimal_point) <-
      */

get_cobol_fc_decimal_point(Fc_decimal_point) :-
   ( dislog_variable_get(cobolml_decimal_point, point) ->
     Fc_decimal_point = '.'
   ; Fc_decimal_point = ',' ).


/* get_cobol_fc_delimiter(Fc_delimiter) <-
      */

get_cobol_fc_delimiter(Fc_delimiter) :-
   ( dislog_variable_get(cobolml_decimal_point, point) ->
     Fc_delimiter = ','
   ; Fc_delimiter = '.' ).


/* get_cobol_fc_currency(Fc_currency) <-
      */

get_cobol_fc_currency(Fc_currency) :-
   dislog_variable_get(cobolml_currency_sign, Fc_currency).


/******************************************************************/


