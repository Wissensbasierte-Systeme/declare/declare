

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Phase 2 String-Literals       ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer_phase_2_string_literals, [
       pretokens_to_string_literals/2 ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_tokenizer_tedcg_help).
:- use_module(cobolml_tokenizer_library).


/*** interface ****************************************************/


/* pretokens_to_string_literals(Pretokens_1, Pretokens_2) <-
      Builds tokens of the type non_numeric_literal on pretokens
      (quote-/apostrophe-pretokens and the enclosed pretokens) in
      every list of pretokens in Pretokens_1 giving Pretokens_2 */

pretokens_to_string_literals(Pretokens_1, Pretokens_2) :-
   pretokens_to_string_literals(_, Pretokens_2, Pretokens_1, []).


/*** implementation ***********************************************/


/* pretokens_to_string_literals --+>
      */

pretokens_to_string_literals --+>
   '$eof',
   !.
pretokens_to_string_literals --+> % HIER in DA: String gaengiger, non_numeric_literal Cobol, alles gleich
   non_numeric_literal,
   pretokens_to_string_literals.
pretokens_to_string_literals --+>
   consume_until_rule(separator),
   sequenced(separator, ++),
   pretokens_to_string_literals.
 /*
pretokens_to_string_literals --+>
   consume_until_rule(separator_left),
   sequence(++, separator_left),
   pretokens_to_string_literals.
pretokens_to_string_literals --+>
   consume_until_rule(separator_right),
   sequence(++, separator_right),
   !.
 */


/* non_numeric_literal +++>
      */

non_numeric_literal +++>
   ( quote_bordered_string
   ; apostrophe_bordered_string ),
   { --->([S]^non_numeric_literal, [S2]),
     Value := S@value,
     atom_length(Value, L),
     L =< 120,
     slenderize(S, S2) }.


/* quote_bordered_string -++>
      */

quote_bordered_string -++>
%  --->(As01@[X01], [X1]), % DA: evtl zeigen, was ware, wenn
%  { quote := X01/tag::'*', %    quote nicht in library definiert
%    As01 = [] }, %              waere
   --->(_@quote, []), % DA: zeige, wie einfach es ist Da: Zeige schlechte alternative mit quote --+> in library
   string_content(quote),
   ( % String would be complete with a closing quote
     --->(_@quote, []),
     !,
     next_rule(separator)
   ; % Continuation break, string is continued
     continuation_break('>='),
     quote_bordered_string ).


/* apostrophe_bordered_string -++>
      */

apostrophe_bordered_string -++>
   --->(_@apostrophe, []),
   string_content(apostrophe),
   ( % String would be complete with a closing apostrophe
     --->(_@apostrophe, []),
     !,
     next_rule(separator)
   ; % Continuation break, string is continued
     continuation_break('>='),
     apostrophe_bordered_string ).


/* string_content(Enclosure_tag) -++>
      */

string_content(Enclosure_tag) -++>
   ( next_rule(Enclosure_tag)
   ; next_rule(continuation_break('>=')) ).
string_content(Enclosure_tag) -++>
   [_],
   string_content(Enclosure_tag).


/******************************************************************/


