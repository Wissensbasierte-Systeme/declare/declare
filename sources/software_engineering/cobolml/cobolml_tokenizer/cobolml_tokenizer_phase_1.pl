

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Phase 1                       ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer_phase_1, [
       cobolml_tokenizer_phase_1/2 ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_tokenizer_library).
:- use_module(cobolml_tokenizer_phase_1_cobol_symbols).
:- use_module(cobolml_tokenizer_phase_1_instruction_lines).


/*** interface ****************************************************/


/* cobolml_tokenizer_phase_1(Lines, Pretokens) <-
      Build pretokens in each line of the list of Lines
      and give the list Pretokens of pretoken-lists back */

cobolml_tokenizer_phase_1(Lines, Pretokens) :-
   length(Lines, Length),
   % Build all line numbers Ns
   numlist(1, Length, Ns),
   maplist( line_to_pretokens,
      Ns, Lines, Pretokens ).


/*** implementation ***********************************************/


/* line_to_pretokens(L, Line, Pretokens) <-
      Converts the atom Line to a list of Pretokens where each
      pretoken has subelements like start and end position as well
      as the source text and in addition an attribute value */

line_to_pretokens(L, Line, Pretokens) :-
   line_to_cobol_symbols(L, Line, Symbols),
   cobol_symbols_to_pretokens(_, Pretokens, Symbols, []),
   !.


/* line_to_cobol_symbols(L, Line, Symbols) <-
      Converts the atom Line to the list Chars of chars and
      builds a list of Symbols out of Chars and both the line
      position L and the column position C (C is 1 at the call) */

line_to_cobol_symbols(L, Line, Symbols) :-
   atom_chars(Line, Chars),
   atom_chars_to_cobol_symbols(L, 1, Symbols, Chars, []),
   !.


/* atom_chars_to_cobol_symbols(L, C, Xs, Ys, Zs) <-
      Builds cobol symbols X out of all input atom chars Ys.
      Firstly it is tried to build a cobol symbol of two atom chars.
      Secondly cobol symbols are built out of one atom char */

atom_chars_to_cobol_symbols(L, C, [X|Xs], [Y1, Y2|Ys], Zs) :-
   % Continuation line with * in column 8 must not be a power sign (**)
   C > 7,
   atom_concat(Y1, Y2, Value),
   % Two-place symbols are only '<=', '>=', '**'
   member(Y1, ['<', '>', '*']),
   cobol_symbol(Value, Tag),
   !,
   D is C + 1,
   X = Tag:[value:Value]:[
          start:[line:L, column:C]:[],
          end:[line:L, column:D]:[],
          source_text:[]:[Value]],
   E is C + 2,
   atom_chars_to_cobol_symbols(L, E, Xs, Ys, Zs).
atom_chars_to_cobol_symbols(L, C, [X|Xs], [Value|Ys], Zs) :-
   cobol_symbol(Value, Tag),
   ( Tag = newline ->
     X = newline:[value:'\\n']:[
           start:[line:L, column:C]:[],
           end:[line:L, column:C]:[],
           source_text:[]:['\n']]
   ; X = Tag:[value:Value]:[
           start:[line:L, column:C]:[],
           end:[line:L, column:C]:[],
           source_text:[]:[Value]] ),
   D is C + 1,
   atom_chars_to_cobol_symbols(L, D, Xs, Ys, Zs).
atom_chars_to_cobol_symbols(_, _, [], [], []).


/* cobol_symbols_to_pretokens --+>
      Builds a list of pretokens out of a list of cobol symbols */

cobol_symbols_to_pretokens --+>
   ( regular_line
   ; continuation_line
   ; debugging_line ).
cobol_symbols_to_pretokens --+>
   ( comment_line
   ; comment_line_with_page_feed
   ; empty_line ),
   optional_rule(newline),
   !.


/******************************************************************/


