

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Phase 4                       ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer_phase_4, [
       cobolml_tokenizer_phase_4/2 ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_tokenizer_phase_4_float_literals).
:- use_module(cobolml_tokenizer_phase_4_data_fields).


/*** interface ****************************************************/


/* cobolml_tokenizer_phase_4(Pretokens, Tokens) <-
      Builds tokens of the type float literal in every list
      of pretoken-lists in Pretokens. Thereafter Pretokens_2
      is permanently flattened and used to build tokens of the
      type data_field. Finally all elements are returned through
      Tokens */

cobolml_tokenizer_phase_4 -->
   maplist( pretokens_to_float_literals ), % DA: In ausarbeitung erlaeutern
   flatten,
   pretokens_to_data_fields.


/******************************************************************/


