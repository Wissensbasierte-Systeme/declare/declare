

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Phase 1 Cobol Symbols         ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer_phase_1_cobol_symbols, [
       cobol_symbol/2
       ] ).


/*** interface ****************************************************/


/* cobol_symbol(Symbol, Tag) <-
      */

cobol_symbol('\n', newline). % HIER noch tab implementieren
cobol_symbol('+', plus).
cobol_symbol('*', multiply).
cobol_symbol('/', divide).
cobol_symbol('**', power).
cobol_symbol('>', greater).
cobol_symbol('<', lower).
cobol_symbol('=', equal).
cobol_symbol('>=', greater_equal).
cobol_symbol('<=', lower_equal).
cobol_symbol(' ', space).
cobol_symbol('"', quote).
cobol_symbol('\'', apostrophe).
cobol_symbol('.', point).
cobol_symbol('-', hyphen).
cobol_symbol(',', comma).
cobol_symbol(';', semicolon).
cobol_symbol('(', left_parenthesis).
cobol_symbol(')', right_parenthesis).
cobol_symbol(X, numeric) :-
   digit(X). % HIER
%  integer(X).
cobol_symbol('$', dollar).
cobol_symbol(X, letter) :-
   letter(X).
cobol_symbol(X, special_character) :-
   atom_length(X, 1).
   

/*** implementation ***********************************************/


/* digit(X) <- % HIER
      Tests "atom-digits" */

digit(X) :-
   nonvar(X),
   member(X, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']).
/* digit(X) <- % HIER

digit(X) :-
   ( nonvar(X) ->
     member(X, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])
   ; findall(D, (between(48, 57, C), char_code(D, C)), Ds),
     member(X, Ds) ).
      Tests or constructs "atom-digits" */


/* letter(X) <-
      Checks if X is a letter */

letter(X) :-
   nonvar(X),
   atom_length(X, 1),
   char_code(X, C),
   ( between(65, 90, C)
   ; between(97, 122, C) ).
/* letter(X) <- % HIER noch rausnehmen

letter(X) :-
   ( nonvar(X) ->
     atom_length(X, 1),
     char_code(X, C),
     ( between(65, 90, C)
     ; between(97, 122, C) )
   ; findall(L,
        ( (between(65, 90, C);between(97, 122, C)),
          char_code(L, C) ),
        Ls),
     member(X, Ls) ).
      Tests or constructs upper-case and lower-case letters */


/* tabulator_length(X) <-
      Adjusted length of the horizontal tabulator */

tabulator_length(2).


/******************************************************************/


