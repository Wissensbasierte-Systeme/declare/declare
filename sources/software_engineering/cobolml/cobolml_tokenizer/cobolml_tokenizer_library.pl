

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Library                       ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer_library, [
       algebraic_sign/4,
       apostrophe/4,
       consume_until_rule/5,
       consume_until_rule/6,
       continuation_break/4,
       continuation_break/5,
       decimal_point/4,
       hyphen/4,
       letter/4,
       minus/4,
       next_rule/5,
       newline/4,
       numeric/4,
       optional_rule/5,
       plus/4,
       quote/4,
       separator/4,
       separator_left/4,
       separator_right/4,
       sequenced/6,
       space/4,
       '$eof'/4
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_tokenizer_tedcg_help).

:- module_transparent
      consume_until_rule/5,
      consume_until_rule/6,
      next_rule/5,
      optional_rule/5,
      sequenced/6,
      sequenced_/6.


/*** interface ****************************************************/


/* algebraic_sign -++>
      */

algebraic_sign -++>
   ( plus
   ; minus ).


/* apostrophe -++>
      */

apostrophe -++>
   [X],
   { apostrophe := X/tag::'*' }.


/* consume_until_rule(+Rule, ++) -++>
      Equal to consume_until_rule(+Rule)-++> except that at least
      one element must be consumed until Rule is applicable */

consume_until_rule(Rule, ++) -++>
   Xs^consume_until_rule(Rule),
   { Xs \= [] }.


/* consume_until_rule(+Rule, **) -++>
      Equal to consume_until_rule(+Rule)-++> */

consume_until_rule(Rule, **) -++>
   consume_until_rule(Rule).


/* consume_until_rule(+Rule) -++>
      Consumes every pretoken from the input until Rule is
      applicable. Returns all the consumed pretokens and
      whose values as well as the remaining output */

consume_until_rule(Rule) -++>
   ( next_rule(Rule),
     !
   ; [_],
     consume_until_rule(Rule) ).


/* continuation_break(+Operator) --+>
      Operator can be '=' or '>=' and defines the column until the
      last space token within the continuation break must reach */

continuation_break(Operator) --+>
   [X],
   { continuation_break := X/tag::'*',
     Column := X/end@column,
     ( Operator == '=',
       Column = 11
     ; Operator == '>=',
       Column >= 11 ) }.


/* continuation_break --+>
      Equal to continuation_break('=')--+> */

continuation_break --+>
   continuation_break('=').


/* continuation_break +-+>
      Builds a continuation break token, 
      the last Space pretoken has to end in column 12 or later */

continuation_break +-+>
   optional_rule(program_id_area),
   newline,
   [Sna, Clt, Space],
   { sequence_number_area := Sna/tag::'*',
     continuation_line_tag := Clt/tag::'*',
     [space, Column] := Space/[tag::'*', end@column],
     !,
     between(11, 71, Column) }.


/* decimal_point +++>
      Consumes and returns the determined decimal point */

decimal_point +++>
   { dislog_variable_get(cobolml_decimal_point, point) },
   !,
   point,
   { --->([D]^decimal_point, [D2]),
     slenderize(D, D2) }.
decimal_point +++>
   { dislog_variable_get(cobolml_decimal_point, comma) },
   !,
   comma,
   { --->([D]^decimal_point, [D2]),
     slenderize(D, D2) }.


/* hyphen -++>
      */

hyphen -++>
   [X],
   { hyphen := X/tag::'*' }.


/* letter -++>
      */

letter -++>
   [X],
   { letter := X/tag::'*' }.


/* minus -++>
      */

minus -++>
   --->(_^[X], [minus:As:Cs]),
   { fn_item_parse(X, hyphen:As:Cs) }.


/* next_rule(+Rule_Ls, -[], -[], +Ys, -Ys) <-
      Checks if Rule_Ls is applicable to the input Ys in terms of
      Rule_Ls would consume a particular leading part of Ys.
      The output stays unaffected independently of the success
      of Rule_Ls and is therefore just the input. Of course there
      is no need to give any content related to As or Xs back */

next_rule(Ls, [], [], Ys, Ys) :-
   is_list(Ls),
   !,
   append(Ls, _, Ys).
next_rule(Rule, [], [], Ys, Ys) :-
   call(Rule, _, _, Ys, _).


/* newline --+>
      */

newline --+>
   [X],
   { newline := X/tag::'*' }.


/* numeric -++>
      */

numeric -++>
   [X],
   { numeric := X/tag::'*' }.


/* optional_rule(+Rule) -++>
      Applies Rule to the input if possible.
      Attention: Do not use optional_rule in a "if-then-else"-
                 construct, because optional_rule always succeeds */

optional_rule(Rule) -++>
   call(Rule).
optional_rule(_) -++>
   !.


/* plus -++>
      */

plus -++>
   [X],
   { plus := X/tag::'*' }.


/* quote -++>
      */

quote -++>
   [X],
   { quote := X/tag::'*' }.


/* separator --+>
      Consumes all separator tokens which can stand
      on the left or on the right side of a token */

separator --+>
   ( separator_left
   ; separator_right ).


/* separator_left --+>
      Consumes all separator tokens which
      can stand on the left side of a token */

separator_left --+>
   space.
separator_left --+>
   ( comma
   ; semicolon ),
   ( space
   ; optional_rule(program_id_area),
     newline ).
separator_left --+>
   sequence_number_area.
separator_left --+>
   ( regular_line_tag
   ; comment_line
   ; comment_line_with_page_feed
   ; debugging_line ).
separator_left --+>
   empty_line.
separator_left --+>
   { \+(dislog_variable_get( cobolml_tokenizer_mode,
           parenthesis_are_not_separators )) },
   left_parenthesis.


/* separator_right --+>
      Consumes all separator tokens which
      can stand on the right side of a token */

separator_right --+>
   ( point
   ; comma
   ; semicolon ),
   ( space
   ; optional_rule(program_id_area),
     ( newline
     ; '$eof' ) ).
separator_right --+>
   space.
separator_right --+>
   optional_rule(program_id_area),
   newline.
separator_right --+>
   empty_line.
separator_right --+>
   '$eof'.
separator_right --+>
   { \+(dislog_variable_get( cobolml_tokenizer_mode,
           parenthesis_are_not_separators )) },
   right_parenthesis.


/* sequenced(+Rule, +) -++>
      Apply Rule at least one time */

sequenced(Rule, +) -++>
   call(Rule),
   sequenced(Rule, *),
   !.


/* sequenced(+Rule, *) --+> oder -++>
      Apply Rule at backtracking */

sequenced(_, *) --+>
   [].
sequenced(Rule, *) -++>
   call(Rule),
   sequenced(Rule, *),
   !.


/* sequenced(+Rule, ++) -++>
      Apply Rule as often as possible and at least one time */

sequenced(Rule, ++) -++>
   call(Rule),
   sequenced_(Rule, **),
   !.


/* sequenced(+Rule, **) -++>
      Apply Rule as often as possible */

sequenced(Rule, **) -++>
   sequenced_(Rule, **),
   !.


/* space -++>
      */

space -++>
   [X],
   { space := X/tag::'*' }.


/* '$eof'(-[], -[], +[], -[])
      Indicates the end of the cobol source file */

'$eof'([], [], [], []).


/*** implementation ***********************************************/


/* comma -++>
      */

comma -++>
   [X],
   { comma := X/tag::'*' }.


/* comment_line -++>
      */

comment_line -++>
   [X],
   { comment_line := X/tag::'*' }.


/* comment_line_with_page_feed -++>
      */

comment_line_with_page_feed -++>
   [X],
   { comment_line_with_page_feed := X/tag::'*' }.


/* debugging_line -++>
      */

debugging_line -++>
   { ( dislog_variable_get(cobolml_debugging_mode, yes)
     ; dislog_variable_get(cobolml_debugging_mode, null) ) },
   !,
   debugging_line_tag.
debugging_line +++>
   { dislog_variable_get(cobolml_debugging_mode, no) },
   debugging_line_without_debugging_mode.


/* debugging_line_tag -++>
      */

debugging_line_tag -++>
   [X],
   { debugging_line_tag := X/tag::'*' }.


/* debugging_line_without_debugging_mode -++>
      */

debugging_line_without_debugging_mode -++>
   debugging_line_tag,
   debugging_line_without_debugging_mode_remaining.


/* debugging_line_without_debugging_mode_remaining -++>
      */

debugging_line_without_debugging_mode_remaining -++>
   '$eof',
   !.
debugging_line_without_debugging_mode_remaining -++>
   ( next_rule(newline) ->
     newline
   ; [_],
     debugging_line_without_debugging_mode_remaining ).


/* empty_line -++>
      */

empty_line -++>
   [X],
   { empty_line := X/tag::'*' }.


/* left_parenthesis -++>
      */

left_parenthesis -++>
   [X],
   { left_parenthesis := X/tag::'*' }.


/* point -++>
      */

point -++>
   [X],
   { point := X/tag::'*' }.


/* program_id_area -++>
      */

program_id_area -++>
   [X],
   { program_id_area := X/tag::'*' }.


/* regular_line_tag -++>
      */

regular_line_tag -++>
   [X],
   { regular_line_tag := X/tag::'*' }.


/* right_parenthesis -++>
      */

right_parenthesis -++>
   [X],
   { right_parenthesis := X/tag::'*' }.


/* semicolon -++>
      */

semicolon -++>
   [X],
   { semicolon := X/tag::'*' }.


/* sequenced_(+Rule, **) --+> oder -++>
      Apply Rule as often as possible */

sequenced_(_, **) --+>
   '$eof',
   !.
sequenced_(Rule, **) -++>
   ( Cs^call(Rule),
     { Cs \= [] },
     sequenced_(Rule, **)
   ; ! ).


/* sequence_number_area -++>
      */

sequence_number_area -++>
   [X],
   { sequence_number_area := X/tag::'*' }.


/******************************************************************/


