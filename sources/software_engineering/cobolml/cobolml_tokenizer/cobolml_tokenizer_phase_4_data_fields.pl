

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Phase 4 Data Fields           ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer_phase_4_data_fields, [
       pretokens_to_data_fields/2
       ] ).

:- use_module('../cobolml_general/tedcg').
:- use_module(cobolml_tokenizer_tedcg_help).
:- use_module(cobolml_tokenizer_library).
:- use_module(cobolml_tokenizer_phase_4_data_fields_help).
:- use_module(
      cobolml_tokenizer_phase_4_data_fields_field_content_symbols).


/*** interface ****************************************************/


/* pretokens_to_data_fields(Pretokens, Tokens) <-
      Builds tokens of the type data_field on pretokens
      of the single list Pretokens giving Tokens */

pretokens_to_data_fields(Pretokens, Tokens) :-
   pretokens_to_data_fields(_, Tokens, Pretokens, []).


/*** implementation ***********************************************/


/* pretokens_to_data_fields --+ <-
      */

pretokens_to_data_fields --+>
   '$eof',
   !.
pretokens_to_data_fields --+>
   [X],
   { [word, Value] := X-[/tag::'*', @value],
     member(Value, ['PIC', 'PICTURE']) },
   sequenced(separator, ++),
   !,
   data_field,
   sequenced(separator, ++),
   pretokens_to_data_fields.
pretokens_to_data_fields --+>
   consume_until_rule(separator),
   sequenced(separator, ++),
   pretokens_to_data_fields.


/* data_field([Data_field], Pretokens_1, Pretokens_2) <-
      Consumes the pretokens from Pretokens_1 which are part
      of a data field. Specific symbols in a data field can be
      extended with bracketed integers, so you have to consider
      parentheses */

data_field +++>
   { dislog_variable_set( cobolml_tokenizer_mode,
        parenthesis_are_not_separators ) },
   Xs^consume_until_rule(separator_right, ++),
   !,
   { pretokens_to_cobol_symbols(Xs, Css),
     cobol_symbols_to_field_content_symbols(As1, Fcss, Css, []),
     !,
     validate_and_type_field_content_symbols(Fcss, Fcss_ext),
     --->(_@data_field, As1),
     --->([D]^data_field, [D2]),
     append(As1, [type:Type, length:Length], As2),
     fn_item_parse(D, data_field:_:[Start, End|_]),
     D2 = data_field:As2:[Start, End|Fcss_ext],
     identify_type(D2, Type),
     token_length(D2, Type),
     data_field_length(D2, Length),
     dislog_variable_set( cobolml_tokenizer_mode, null ),
     ! }.


/* identify_type(+Data_field, -Type) <-
      Identifies the Type of a Data_field */

identify_type(Data_field, Type) :-
   fn_item_parse(Data_field, _:_:Xs),
   findall( T,
      ( member(X, Xs),
        T := X/tag::'*',
        \+(member(T, [start, end, continuation_break])) ),
      Ts1 ),
   list_to_set(Ts1, Ts2),
   identify_type_(Ts2, Type),
   !.


/* identify_type_(+Tags, -Type) <-
      */

identify_type_(Tags, alphabetic) :-
   subtract(Tags, [fc_letter, fc_space], []).
identify_type_(Tags, numeric) :-
   subtract(Tags, [fc_sign, fc_assumed_decimal_point,
      fc_decimal_power, fc_numeric], []),
   member(fc_numeric, Tags).
identify_type_(Tags, alphanumeric) :-
   subtract(Tags, [fc_numeric, fc_letter, fc_alpha], []).
identify_type_(Tags, alphanumeric_edited) :-
   subtract(Tags, [fc_letter, fc_alpha, fc_numeric, fc_space,
      fc_zero, fc_slash], []),
   intersection(Tags, [fc_letter, fc_alpha], Ls1),
   Ls1 \= [],
   intersection(Tags, [fc_space, fc_zero, fc_slash], Ls2),
   Ls2 \= [].
identify_type_(Tags, numeric_edited) :-
   subtract( Tags,
      [fc_numeric, fc_decimal_power, fc_assumed_decimal_point,
       fc_decimal_point, fc_delimiter, fc_creditor, fc_debitor,
       fc_minus, fc_plus, fc_star, fc_zero, fc_z, fc_slash,
       fc_space, fc_currency],
      [] ).


/* data_field_length(+Data_field, -Length) <-
      */

data_field_length(Data_field, Length) :-
   fn_item_parse(Data_field, _:_:Cs),
   ( foreach(C, Cs),
     foreach(N, Ns1) do
        T := C/tag::'*',
        ( \+(member(T, [start, end, continuation_break,
                        fc_assumed_decimal_point, fc_decimal_power,
                        fc_sign])),
          ( Value := C/parentheses_extension/integer_literal@value,
            atom_to_int(Value, N)
          ; Value := C@value,
            atom_length(Value, N) )
        ; N = [] ) ),
   flatten(Ns1, Ns2),
   sumlist(Ns2, Length),
   !.


/* token_length(+Data_field, +Type) <-
      The maximum length of the description of a data field is 30
      characters. In addition there are only 18 digits allowed.
      ( Digits only appear in numeric(-edited) data fields) */

token_length(Data_field, Type) :-
   Value := Data_field@value,
   atom_length(Value, L),
   L =< 30,
   ( member(Type, [numeric, numeric_edited]),
     Cs := Data_field/content::'*',
     ( foreach(C, Cs),
       foreach(N, Ns1) do
          ( fc_numeric := C/tag::'*',
            ( N := C@extension
            ; N = 1 )
          ; N = [] ) ),
     flatten(Ns1, Ns2),
     sumlist(Ns2, S),
     !,
     S =< 18
   ; true ).


/* validate_and_type_field_content_symbols(Fcss, Fcss_extended) <-
      Validates that Fcss comply with the rules of building data
      fields in Cobol. Every field content symbol is therefore
      checked and extended with its Type as a attribute */

validate_and_type_field_content_symbols -->
   { cobol_ordinary_field_content_symbols(Ordinary),
     cobol_non_floating_field_content_symbols(Non_floating),
     cobol_floating_field_content_symbols(Floating) },
   validate_and_type_field_content_symbols(
      [], [l, r], Ordinary, Non_floating, Floating).

/* validate_and_type_field_content_symbols(
         Accs, Allowed_positions, Allowed_ordinarys,
         Allowed_non_floatings, Allowed_floatings,
         Fcss, Fcss_extended ) <-
      Accs is the accumulator for the already validated and
      extended field content symbols. The Allowed_positions define
      the possible positions for the current field content symbol.
      The lists Allowed_ordinarys, Allowed_non_floatings and
      Allowed_floatings define the possible ordinary, non_floating
      and floating field content symbols for the upcoming field
      content symbols from Fcss */

validate_and_type_field_content_symbols(
      Accs, _, _, _, _, [], Fcss_extended):-
   reverse(Accs, Scca),
   flatten(Scca, Fcss_extended),
   !.
validate_and_type_field_content_symbols(
      Accs,
      Allowed_positions,
      Allowed_ordinarys,
      Allowed_non_floatings,
      Allowed_floatings) -->
   continuation_break(_, Cb),
   validate_and_type_field_content_symbols(
      [Cb|Accs], Allowed_positions,
      Allowed_ordinarys, Allowed_non_floatings, Allowed_floatings).
validate_and_type_field_content_symbols(
      Accs,
      Allowed_positions,
      Allowed_ordinarys,
      Allowed_non_floatings,
      Allowed_floatings) -->
   [X],
   { % Get values from current field content symbol
     Tag := X/tag::'*',
     cobol_field_content_symbol(Symbol, Tag, Type, Symbol_positions,
        Symbol_ordinarys, Symbol_non_floatings, Symbol_floatings),
     % Validate current field content symbol X
     intersection(Allowed_positions, Symbol_positions, Positions),
     Positions \= [],
     ( Symbol_positions = [l],
       Allowed_positions = [l, r] -> New_positions = [l, r]
     ; New_positions = Positions ),
     % Test if Symbol is still possible in at least one Allowed-list
     member(Set,
        [Allowed_ordinarys, Allowed_non_floatings, Allowed_floatings]),
     member_symbol(Symbol, Symbol_positions, Set),
     % Build new allowed lists
     intersection(Symbol_ordinarys, Allowed_ordinarys,
        New_ordinarys),
     intersection(Symbol_non_floatings, Allowed_non_floatings,
        New_non_floatings),
     intersection(Symbol_floatings, Allowed_floatings,
        New_floatings),
     % Extend the attributes of X with its Type
     fn_item_parse(X, Tag:As:Cs),
     append(As, [type:Type], As_2) },
   validate_and_type_field_content_symbols(
      [[Tag:As_2:Cs]|Accs], New_positions,
      New_ordinarys, New_non_floatings, New_floatings).


/* member_symbol(Symbol, Symbol_positions, Allowed_symbols) <-
      Checks if Symbol or respectively Symbol with its possible
      Symbol_positions are a member of Allowed_symbols and hence
      Symbol is a valid field content symbol in the analyzed
      data field */

member_symbol(Symbol, Symbol_positions, Allowed_symbols) :-
   ( member(Symbol, Allowed_symbols)
   ; member(Side, Symbol_positions),
     member(Symbol:Side, Allowed_symbols) ),
   !.


/******************************************************************/


