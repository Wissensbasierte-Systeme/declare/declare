

/******************************************************************/
/***                                                            ***/
/***           CobolML: Tokenizer Phase 3                       ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_tokenizer_phase_3, [
       cobolml_tokenizer_phase_3/1 ] ).


/*** interface ****************************************************/


/* cobolml_tokenizer_phase_3(Pretokens) <-
      Pretokens is still a list of pretoken-lists and
      the list is only temporary flattened for parsing reasons.
      After identifying the used decimal point and currency sign
      and setting them to global variables by the use of
      cobolml_parser_environment_division:environment_division
      the tokenizer continues to work in phase 4.
      Phase 4 is responsible for building float literals and
      data fields with the acquired knowledge of the decimal point
      and the currency sign */

cobolml_tokenizer_phase_3(Pretokens) :-
   dislog_variable_set( tedcg_help_module, 
      cobolml_parser_tedcg_help ),
   flatten(Pretokens, Pretokens2),
   cobolml_parser_identification_division:
      identification_division(_, _, Pretokens2, Zs),
   cobolml_parser_environment_division:
      environment_division(_, _, Zs, _),
   dislog_variable_set( tedcg_help_module, 
      cobolml_tokenizer_tedcg_help ).


/******************************************************************/


