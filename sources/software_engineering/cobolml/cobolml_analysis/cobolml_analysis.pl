

/******************************************************************/
/***                                                            ***/
/***           CobolML: Analysis                                ***/
/***                                                            ***/
/******************************************************************/


:- module( cobolml_analysis, [
       average_data_field_size/2,
       check_defindness/2,
       check_read_instruction/1,
       cobolml_write_elements/2,
       count_different_instructions/2,
       count_instruction/3,
       count_instructions/2,
       count_procedure_calls/2,
       display_all_data_groups/1,
       get_all_elements/3,
       get_all_values/3,
       get_all_source_text/2,
       get_tags/2,
       get_values/2
       ] ).


/*** interface ****************************************************/


/* average_data_field_size(+CobolML, -Average) <-
      */

average_data_field_size(CobolML, Average) :-
   Dd := CobolML/data_division,
   get_all_elements(data_description_entry, [Dd], Dde),
   findall( L,
      ( member(D, Dde),
        L := D/picture_clause@length ),
      Ls ),
   length(Ls, N),
   sum_list(Ls, Sum),
   Average is Sum / N.

   
/* check_defindness(+Tag, +CobolML) <-
      */

check_defindness(Tag, CobolML) :-
   Pds := CobolML/procedure_division/content::'*',
   get_all_elements(Tag, Pds, P_elements),
   get_values(P_elements, P_values_),
   list_to_set(P_values_, P_values),
   Dds := CobolML/data_division/content::'*',
   get_all_elements(Tag, Dds, D_elements),
   get_values(D_elements, D_values_),
   list_to_set(D_values_, D_values),
   !,
   subset(P_values, D_values).


/* check_read_instruction(+CobolML) <-
      */

check_read_instruction(CobolML) :-
   Pds := CobolML/procedure_division/content::'*',
   get_all_elements(read_instruction, Pds, Reads),
   get_all_values(file_name, Reads, Vs1),
   get_all_elements(open_instruction, Pds, Opens),
   get_all_values(file_name, Opens, Vs2),
   get_all_elements(close_instruction, Pds, Closes),
   get_all_values(file_name, Closes, Vs3),
   !,
   subset(Vs1, Vs2),
   subset(Vs1, Vs3),
   check_defindness_file_name(environment, Vs1, CobolML),
   check_defindness_file_name(data, Vs1, CobolML).


/* cobolml_write_elements(+Mode, +Elements) <-
      */

cobolml_write_elements(xml, Elements) :-
   Stars = '********************',
   concat_atom([Stars, Stars, Stars], Starline),
   writeln(Starline), nl,
   ( foreach(Element, Elements) do
        dwrite(xml, Element),
        writeln(Starline), nl ).
cobolml_write_elements(cobol, Elements) :-
   Stars = '********************',
   concat_atom([Stars, Stars, Stars], Starline),
   writeln(Starline), nl,
   M = cobolml_analysis,
   ( foreach(Element, Elements) do
        M:get_all_source_text([Element], Source_Text),
        writeln(Source_Text),
        writeln(Starline), nl ).

   
/* count_different_instructions(+CobolML, -Quantity) <-
      */

count_different_instructions(CobolML, Quantity) :-
   Pds := CobolML/procedure_division/content::'*',
   get_all_elements(instruction, Pds, Instructions),
   get_tags(Instructions, Tags),
   list_to_set(Tags, Tags_),
   length(Tags_, Quantity),
   !.


/* count_instruction(+Instruction_tag, +CobolML, -Quantity) <-
      */

count_instruction(Instruction_tag, CobolML, Quantity) :-
   Pds := CobolML/procedure_division/content::'*',
   get_all_elements(Instruction_tag, Pds, Instructions),
   length(Instructions, Quantity),
   !.


/* count_instructions(+CobolML, -Quantity) <-
      */

count_instructions(CobolML, Quantity) :-
   Pds := CobolML/procedure_division/content::'*',
   get_all_elements(instruction, Pds, Instructions),
   length(Instructions, Quantity),
   !.


/* count_procedure_calls(+CobolML, -Quantity) <-
      */

count_procedure_calls(CobolML, Quantity) :-
   Pds := CobolML/procedure_division/content::'*',
   get_all_elements(perform_instruction, Pds, P_instructions),
   get_all_elements(procedure_name, P_instructions, P_names),
   list_to_set(P_names, P_names2),
   length(P_names2, Quantity).
 

/* display_all_data_groups(+CobolML) <- 
      */

display_all_data_groups(CobolML) :- 
   Dd := CobolML/data_division,
   get_all_elements(data_description_entry, [Dd], Dde),
   M = cobolml_analysis,
   ( foreach(D, Dde) do
        ( \+(_ := D/picture_clause) ->
          M:cobolml_write_elements(cobol, [D])
        ; true ) ).


/* get_all_elements(+Tag, +Elements1, -Elements2) <-
      */

get_all_elements(Tag, Elements1, Elements2) :-
   M = cobolml_analysis,
   ( foreach(E, Elements1),
     foreach(Es, Ess) do
        ( M:check_element(Tag, E) ->
          Xs1 = [E]
        ; Xs1 = [] ),
        ( Cs := E/content::'*' ->
          M:get_all_elements(Tag, Cs, Xs2)
        ; Xs2 = [] ),
        append(Xs1, Xs2, Es) ),
   flatten(Ess, Elements2).


/* get_all_source_text(+Elements, -Source_text) <-
      */

get_all_source_text(Elements, Source_text) :-
   M = cobolml_analysis,
   ( foreach(E, Elements),
     foreach(S, Ss) do
        ( Cs := E/content::'*' ->
          ( [Atom] = Cs,
            atom(Atom),
            S = Cs
          ; Cs = [],
            S = []
          ; M:get_all_source_text(Cs, S) )
        ; S = [] ) ),
   flatten(Ss, Ss2),
   concat_atom(Ss2, Source_text).


/* get_all_values(+Tag, +Elements, -Values) <-
      */

get_all_values(Tag, Elements, Values) :-
   get_all_elements(Tag, Elements, Xs),
   get_values(Xs, Values).


/* get_tags(+Elements, -Tags) <-
      */

get_tags(Elements, Tags) :-
   foreach(E, Elements), foreach(T, Tags) do
      T := E/tag::'*'.


/* get_values(+Elements, -Values) <-
      */

get_values(Elements, Values) :-
   foreach(E, Elements), foreach(V, Values) do
      V := E@value.



/*** implementation **********************************************/


/* check_defindness_file_name(+data, +Values, +CobolML) <-
      */

check_defindness_file_name(data, Values, CobolML) :-
   File_section := CobolML/data_division/file_section,
   get_all_values(file_name, [File_section], Values2),
   subset(Values, Values2).


/* check_defindness_file_name(+environment, +Values, +CobolML) <-
      */

check_defindness_file_name(environment, Values, CobolML) :-
   Ios := CobolML/environment_division/input_output_section,
   File_control_paragraph := Ios/file_control_paragraph,
   get_all_values(file_name, [File_control_paragraph], Values2),
   subset(Values, Values2).


/* check_element(+Tag, +Element) <-
      */

check_element(instruction, Element) :-
   Tag := Element/tag::'*',
   sub_atom(Tag, _, 11, 0, instruction),
   !.
check_element(clause, Element) :-
   Tag := Element/tag::'*',
   sub_atom(Tag, _, 6, 0, clause),
   !.
check_element(Tag, Element) :-
   Tag := Element/tag::'*',
   !.
   

/*****************************************************************/


