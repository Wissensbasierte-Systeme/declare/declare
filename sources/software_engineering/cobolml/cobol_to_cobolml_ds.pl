

/******************************************************************/
/***                                                            ***/
/***         CobolML: Transforms Cobol into CobolML             ***/
/***                                                            ***/
/******************************************************************/


:- module( cobol_to_cobolml, [
      cobol_to_cobolml/0,
      cobol_to_cobolml/1,
      cobol_to_cobolml/2,
      cobol_to_cobolml/3 ] ).

:- use_module(cobolml_analysis/cobolml_analysis).
:- use_module(cobolml_tokenizer/cobolml_tokenizer).
:- use_module(cobolml_parser/cobolml_parser).


/*** interface ****************************************************/


cobol_to_cobolml :-
   cobol_to_cobolml(_).

cobol_to_cobolml(CobolML) :-
   dislog_variable_get(example_path, Examples),
   File = 'cobolml/programs/Kunden-Ausgabe.cob',
   concat(Examples, File, Path_In), 
   cobol_file_to_cobolml(Path_In, CobolML),
   Selection := CobolML,
   concat(Examples, '/cobolml/CobolML.txt', Path_Out),
   dwrite(xml, Path_Out, Selection).

cobol_file_to_cobolml(File, CobolML) :-
   set_cobolml_variables,
   read_file_to_name(File, Program),
   writeln(Program), nl,
   program_to_lines(Program, Lines),
   !,
   cobolml_tokenizer(Lines, Tokens),
   !,
   cobolml_parser(Tokens, CobolML),
   !.


/* cobol_to_cobolml(Cobol_file, CobolML_file) <-
      Reads the Cobol_file and writes the CobolML_file */

cobol_to_cobolml(Cobol_file, CobolML_file) :-
   cobol_to_cobolml(Cobol_file, CobolML_file, _).


/* cobol_to_cobolml(Cobol_file, CobolML_file, CobolML) <-
      Reads the Cobol_file, writes the CobolML_file
      and gives CobolML for further analyses back */

cobol_to_cobolml(Cobol_file, CobolML_file, CobolML) :-
   set_cobolml_variables,
   read_file_to_name(Cobol_file, Program),
   program_to_lines(Program, Lines),
   !,
   cobolml_tokenizer(Lines, Tokens),
   !,
   cobolml_parser(Tokens, CobolML),
   !,
   ( nonvar(CobolML_file) ->
     dwrite(xml, CobolML_file, CobolML)
   ; true ).


/*** implementation ***********************************************/


/* program_to_lines(Program, Lines) <-
      Separates the cobol-program Program in the list
      Lines of all lines of the cobol-program.
      Each line is also a list. */

program_to_lines(Program, Lines) :-
   name(Program, List),
   list_split_at_position(["\n"], List, Ls),
   delete_last_element(Ls, Last, Ls_2),
   % Give the newline-signs back
   ( foreach(L_1, Ls_2),
     foreach(L_2, Ls_3) do
        append(L_1, [10], L_2) ),
   append(Ls_3, [Last], Ls_4),
   maplist( string_to_atom,
      Ls_4, Lines ).

 
/* set_cobolml_variables <-
      */

set_cobolml_variables :-
   dislog_variable_set(cobolml_debugging_mode, null),
   dislog_variable_set(cobolml_decimal_point, null),
   dislog_variable_set(cobolml_currency_sign, null),
   dislog_variable_set(cobolml_instruction_mode, null),
   dislog_variable_set(cobolml_parser_mode, null),
   dislog_variable_set(cobolml_tokenizer_mode, null).


/******************************************************************/


