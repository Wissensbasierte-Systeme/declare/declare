

/******************************************************************/
/***                                                            ***/
/***         DisLog:  Package Dependency Analysis               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* calls_pp_ff_p_to_table(Level) <-
      */

calls_pp_ff_p_to_table :-
   calls_pp_ff_p_to_table(
      unit, [], [basic_algebra, reasoning, xml]).

calls_pp_ff_p_to_table(Level) :-
   calls_pp_ff_p_to_table(Level, [], []).


/* calls_pp_ff_p_to_table(Level, Exclusions_1, Exclusions_2) <-
      */

calls_pp_ff_p_to_table(Level, Exclusions_1, Exclusions_2) :-
   findall( [X1, X2, File_1, File_2, Pred_2],
      ( rar_query:calls_packages_ff_pp(Level:X1, Level:X2,
           File_1, File_2, _, Predicate_2),
        X1 \= X2,
        \+ member(X1, Exclusions_1),
        \+ member(X2, Exclusions_2),
        term_to_atom(Predicate_2, Pred_2) ),
      Rows_2 ),
   sort(Rows_2, Rows_3),
   dislog_variable_get(source_path, Sources),
   maplist( calls_pp_ff_p_to_table_shorten_row(Sources),
      Rows_3, Rows ),
   xpce_display_table(_, _, Level,
      ['X1', 'X2', 'File_1', 'File_2', 'Predicate_2'], Rows).

calls_pp_ff_p_to_table_shorten_row(Path, Row_1, Row_2) :-
   Row_1 = [X1, X2, File_1, File_2, Pred_2],
   concat(Path, F_1, File_1),
   name_start_after_position(["/"], F_1, F1),
   concat(Path, F_2, File_2),
   name_start_after_position(["/"], F_2, F2),
   Row_2 = [X1, X2, F1, F2, Pred_2].


/* package_to_calls_ff_edges(L:X, Files, Edges) <-
      */

package_to_calls_ff_edges(L:X, Files, Edges) :-
   member(L, [unit, module, file]),
   dislog_package_to_files(L:X, Files),
   files_to_calls_ff_edges(Files, Edges).
package_to_calls_ff_edges(L:X, Files, Edges) :-
   L:X = system:dislog,
   dislog_package_to_files(L:X, Files),
   calls_ff_edges(Edges).


/* files_to_calls_ff_edges(Files, Edges) <-
      */

files_to_calls_ff_edges(Files, Edges) :-
   findall( File_1-File_2,
      rar_query:calls_ff(Files, Files, File_1, File_2),
      Edges_2 ),
   sort(Edges_2, Edges).


/* calls_ff_edges(Edges) <-
      */

calls_ff_edges(Edges) :-
   findall( File_1-File_2,
      rar_query:calls_ff(File_1, File_2),
      Edges_2 ),
   sort(Edges_2, Edges).


/******************************************************************/


