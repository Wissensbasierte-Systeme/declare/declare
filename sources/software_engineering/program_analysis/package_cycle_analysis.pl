

/******************************************************************/
/***                                                            ***/
/***         DisLog:  Package Dependency Analysis               ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(calls_pp_ff_pp_to_table, d3) :-
   dislog_variable_get(source_path, Sources),
   concat(Sources, 'projects/d3/', Directory),
   Files_1 = [
      d3_diagnosis_implication,
      d3_abduction_multiple, d3_abduction,
      d3_diagnostic_reasoning,
      d3,
      d3_knowledge_base_slicing,
      d3_knowledge_base_to_rules,
      d3_knowledge_base_to_edges,
      d3_knowledge_base_to_rule_goal_graph,
      d3_rule_goal_graphs,
      d3_knowledge_base ],
   maplist( concat(Directory),
      Files_1, Files_2 ),
   elements_to_lists(Files_2, Layers),
   calls_pp_ff_pp_to_table(module:d3, Layers).


/*** interface ****************************************************/


/* calls_pp_ff_pp_to_table(Level:X, Layers) <-
      */

calls_pp_ff_pp_to_table(Level:X, Layers) :-
   layers_to_precedences(Layers, Precedences),
   findall( [File_1, File_2, Pred_1, Pred_2],
      ( rar_query:calls_packages_ff_pp(Level:X, Level:X,
           File_1, File_2, Predicate_1, Predicate_2),
        file_preceeds_file(Precedences, File_2, File_1),
        term_to_atom(Predicate_1, Pred_1),
        term_to_atom(Predicate_2, Pred_2) ),
      Rows_2 ),
   sort(Rows_2, Rows_3),
   dislog_variable_get(source_path, Sources),
   maplist( calls_pp_ff_pp_to_table_shorten_row(Sources),
      Rows_3, Rows ),
   xpce_display_table(_, _, Level,
      ['File_1', 'File_2', 'Predicate_1', 'Predicate_2'],
      Rows).

layers_to_precedences(Layers, Precedences) :-
   findall( X1-X2,
      ( append(_, [L1|Ls], Layers),
        member(L2, Ls),
        member(X1, L1),
        member(X2, L2) ),
      Precedences ).

file_preceeds_file(Precedences, File_1, File_2) :-
   member(File_1-File_2, Precedences).

calls_pp_ff_pp_to_table_shorten_row(Path, Row_1, Row_2) :-
   Row_1 = [File_1, File_2, Pred_1, Pred_2],
   concat(Path, F_1, File_1),
   name_start_after_position(["/"], F_1, F1),
   concat(Path, F_2, File_2),
   name_start_after_position(["/"], F_2, F2),
   Row_2 = [F1, F2, Pred_1, Pred_2].


/******************************************************************/


