

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Predicate Dependency Analysis            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dislog_to_maximal_predicate_support(Predicates) <-
      */

dislog_to_maximal_predicate_support(Predicates) :-
   dislog_to_calls_pp_edges(Edges),
   edges_to_ugraph(Edges, Graph),
   ugraph_to_maximal_vertex(Graph, Predicate),
   reachable_with_writelns(Predicate, Graph, Predicates).


/* ugraph_to_maximal_vertex(Graph, Vertex) <-
      */

ugraph_to_maximal_vertex(Graph, Vertex) :-
   reduce(Graph, Reduced_Graph),
   reduced_ugraph_to_levels(Reduced_Graph, Levels),
   last(Levels, Level),
   !,
   member(Sc, Level),
   member(Vertex, Sc).


/* reachable_with_writelns(Vertex, Graph, Vertices) <-
      */

reachable_with_writelns(Vertex, Graph, Vertices) :-
   write(Vertex),
   reachable(Vertex, Graph, Vertices),
   length(Vertices, N),
   write_list([' reaches ', N, ' vertices.\n']).


/* dislog_to_levels_html_table <-
      */

dislog_to_levels_html_table :-
   dislog_to_calls_pp_edges_reduce(Graph),
   reduced_ugraph_to_levels(Graph, Levels),
   levels_to_html_table(Levels).


/*** implementation ***********************************************/


/* levels_to_html_table(Levels) <-
      */

levels_to_html_table(Levels) :-
   findall( [M|Measures],
      ( nth(N, Levels, Level),
        level_to_measures(Level, Measures),
        M is N - 1 ),
      Rows ),
   html_display_table('Levels',
      ['Level', 'SCs', 'Preds', 'Min', 'Avg', 'Max'],
      Rows, size(300, 500)).
   
levels_to_measures(Levels, Measures) :-
   maplist( level_to_measures,
      Levels, Measures ).

level_to_measures(Level, [A, B, Min, C, Max]) :-
   length(Level, A),
   maplist( length,
      Level, Xs ),
   add(Xs, B),
   maximum(Xs, Max),
   minimum(Xs, Min),
   average(Xs, C).


/* reduced_ugraph_to_levels(Graph, Levels) <-
      */

reduced_ugraph_to_levels(Graph, Levels) :-
   reduced_ugraph_to_levels(Graph, Levels, []).

reduced_ugraph_to_levels([], [], []).
reduced_ugraph_to_levels(
      Graph_1, [Level|Levels], Graph_2) :-
   ugraph_to_bottom_level_and_rest(Graph_1, Level, Graph_3),
   reduced_ugraph_to_levels(Graph_3, Levels, Graph_2).


/* ugraph_to_bottom_level_and_rest(Graph_1, Level, Graph_2) <-
      */

ugraph_to_bottom_level_and_rest(Graph_1, Level, Graph_2) :-
   ugraph_to_bottom_level(Graph_1, Level),
   ugraph_delete_vertices(Graph_1, Level, Graph_2).

ugraph_to_bottom_level(Graph, Level) :-
   findall( Vertex,
      member(Vertex-[], Graph),
      Level ).

ugraph_delete_vertices(Graph_1, Vertices, Graph_2) :-
   findall( V-Vs,
      ( member(V-Ws, Graph_1),
        \+ ord_element(V, Vertices),
        ord_subtract(Ws, Vertices, Vs) ),
      Graph_2 ).


/* dislog_to_calls_pp_edges_reduce(Reduced_Graph) <-
      */

dislog_to_calls_pp_edges_reduce(Reduced_Graph) :-
   dislog_to_calls_pp_edges(Edges),
   edges_to_ugraph(Edges, Graph), 
   reduce(Graph, Reduced_Graph).

dislog_to_calls_pp_edges(Edges) :-
   dislog_files_to_xml(Program),
   findall( Predicate_1 - Predicate_2,
      calls_pp(Program, Predicate_1, Predicate_2),
      Edges_2 ),
   sort(Edges_2, Edges).


/*** tests ********************************************************/


test(program_analysis, maximal_vertex_support) :-
   Edges = [a-b, b-c, c-d, e-f, f-b],
   writeln(Edges),
   edges_to_ugraph(Edges, Graph),
   ugraph_to_maximal_vertex(Graph, Vertex),
   reachable_with_writelns(Vertex, Graph, Vertices),
   writeln(Vertices).


/******************************************************************/


