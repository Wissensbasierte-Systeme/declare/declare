

/******************************************************************/
/***                                                            ***/
/***        DisLog:  Predicate Dependency Analysis              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dislog_predicates_measure(File) <-
      */

dislog_predicates_measure(File) :-
   diconsult('magic_sets/tc_right', Program),
   dislog_predicates_collect(Program, Predicates),
   select_every_nth_element(100, Predicates, Predicates_2),
   maplist( dislog_dependencies_dlv(Program),
      Predicates_2, Pairs ),
   predicate_to_file(File, writeln(Pairs)),
   sort(Pairs, Pairs_2),
   graph_plot(Pairs_2).


/* dislog_predicates_collect(Program, Predicates) <-
      */

dislog_predicates_collect(Program, Predicates) :-
   findall( Predicate,
      ( member([arc(Predicate, _)], Program)
      ; member([arc(_, Predicate)], Program) ),
      Predicates_2 ),
   sort(Predicates_2, Predicates),
   !.


/* dislog_dependencies_dlv(Program, Predicate, [M, N]) <-
      */

dislog_dependencies_dlv(Program, Predicate, [M, N]) :-
   sort(Program, Program_1),
   append([[tc_magic_bf(Predicate)]], Program_1, Program_2),
   program_to_stable_models_dlv(Program_2, [Model]),
   findall( Y,
      member(tc_bf(Predicate, Y), Model),
      Answers ),
   findall( Atom,
      ( member(Atom, Model),
        Atom =.. [P|_],
        member(P, [tc_bf, tc_magic_bf]) ),
      Atoms ),
%  length(Model, K),
   length(Answers, M),
   length(Atoms, N),
   !.


/* dislog_dependencies(File) <-
      */

dislog_dependencies(File) :-
   diconsult(File, Edge_Facts),
   Tc_Program = [
      [arc(1, 2)],
      [tc(X, Y)]-[arc(X, Y)],
      [tc(X, Y)]-[arc(X, Z), tc(Z, Y)] ],
   magic_sets_evaluation(Tc_Program, Edge_Facts,
      tc(user_mine_sweeper_analysis_norm_0, _), State),
   dportray(dhs, State).


/* dislog_calls_pp_to_file(File) <-
      */

dislog_calls_pp_to_file(File) :-
   findall( [arc(Predicate_1, Predicate_2)],
      rar_query:calls_pp(Predicate_1, Predicate_2),
      State_2 ),
   sort(State_2, State),
   distore(State, File).


/* defines_fp(Program, File, Predicate) <-
      */

defines_fp(Program, File, Predicate) :-
   defines_or_calls_fp(Program, File, head, Predicate).


/* calls_fp(Program, File, Predicate) <-
      */

calls_fp(Program, File, Predicate) :- 
   defines_or_calls_fp(Program, File, body, Predicate).


/* defines_or_calls_fp(Program, File, Selector, Predicate) <-
      */

defines_or_calls_fp(Program, File, Selector, Predicate) :-
   Predicate := Program^file::[@path=File]
      ^rule^Selector^atom@predicate.


/* calls_pp(Program, Predicate_1, Predicate_2) <-
      */

calls_pp(Program, Predicate_1, Predicate_2) :-
   Rule := Program^file^rule,
   Predicate_1 := Rule^head^atom@predicate,
   Predicate_2 := Rule^body^atom@predicate.


/******************************************************************/


