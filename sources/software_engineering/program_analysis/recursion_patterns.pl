

/******************************************************************/
/***                                                            ***/
/***           DDK:  Recursion Patterns                         ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_get(output_path, Results),
   concat(Results, structural_recursion_patterns, Path_1),
   concat(Results, ruleml_output, Path_2),
   dislog_variable_set(
      structural_recursion_patterns, Path_1 ),
   dislog_variable_set(
      ruleml_output, Path_2 ).


/*** interface ****************************************************/


/* directory_to_patterns(Type, Directory, Rules) <-
      */

directory_to_patterns(Type, Directory, Patterns) :-
   directory_prolog_files_to_xml(Directory, Xml),
   findall( Pattern,
      structural_recursion_pattern(Type, Xml, Pattern),
      Patterns ),
   structural_recursion_patterns_to_table_file_and_view(
      Type, Directory, Xml, Patterns).


/* structural_recursion_patterns_to_table_file_and_view(
         Type, Directory, Xml, Patterns) <-
      */

structural_recursion_patterns_to_table_file_and_view(
      Type, Directory, Xml, Patterns) :-
   Xml_2 = directory:[path:Directory]:Patterns,
   dislog_variable_get(structural_recursion_patterns, Path),
   fn_term_to_xml_file(Xml_2, Path),
   concat(Path, '.pl', Prolog_File),
   structural_recursion_patterns_to_files_and_predicates(
      Xml_2, Fs_and_Ps),
   tuples_to_table_rows(Fs_and_Ps, Rows),
   xpce_display_table(_, _, Type, ['File', 'Predicate'], Rows),
   files_and_predicates_to_rules(Xml, Fs_and_Ps, Rules),
   predicate_to_file( Prolog_File,
      xml_to_prolog:xml_rules_to_prolog(Rules) ),
   file_view(Prolog_File).

structural_recursion_patterns_to_files_and_predicates(
      Xml, Fs_and_Ps) :-
   findall( [File, Predicate],
      Predicate :=
         Xml^file::[@path=File]^rule^head^atom@predicate,
      Fs_and_Ps ).

files_and_predicates_to_rules(Xml, Fs_and_Ps, Rules) :-
   findall( Rule,
      ( member([File, Predicate], Fs_and_Ps),
        Rule := Xml^file::[@path=File]
           ^rule::[^head^atom@predicate=Predicate] ),
      Rules ).


/* structural_recursion_patterns(Type) <-
      */

structural_recursion_patterns(Type) :-
%  Units = [basic_algebra, reasoning, interfaces, xml],
   Units = [databases, development, projects, stock_tool],
   findall( [Unit, N],
      ( dislog_unit(Unit, _),
        member(Unit, Units),
        star_line,
        writeln(user, Unit),
        star_line,
        structural_recursion_patterns(Type, Unit, N) ),
      Rows ),
   xpce_display_table(_, _, Type, ['Unit', 'Patterns'], Rows).

structural_recursion_patterns(Type, Unit, N) :-
   dislog_unit(Unit, Modules),
   maplist( structural_recursion_patterns(Type, Unit),
      Modules, Xs ),
   maplist( length,
      Xs, Ns ),
   add(Ns, N),
   pair_lists(Modules, Xs, Pairs),
   findall( module:[name:Module]:Patterns,
      member([Module, Patterns], Pairs),
      Ys ),
   Xml = unit:[name:Unit]:Ys,
   dislog_variable_get(structural_recursion_patterns, Path),
   fn_term_to_xml_file(Xml, Path),
   !.


/* structural_recursion_patterns(Type, Unit) <-
      */

structural_recursion_patterns(Type, Unit) :-
   findall( [Module, N],
      ( dislog_unit(Unit, Modules),
        member(Module, Modules),
        star_line,
        writeln(user, Module),
        star_line,
        structural_recursion_patterns(
           Type, Unit, Module, Patterns),
        length(Patterns, N) ),
      Rows ),
   concat([Unit, ' - ', Type], Title),
   xpce_display_table(_, _, Title, ['Module', 'Patterns'], Rows).


/* structural_recursion_patterns(Type, Unit, Module, Patterns) <-
      */

structural_recursion_patterns(Type, Unit, Module, Patterns) :-
   writeln(user, Module),
   findall( Pattern,
      structural_recursion_pattern(
         Type, Unit, Module, Pattern),
      Patterns ),
   length(Patterns, N),
   writeln(user, N).


/* structural_recursion_pattern(Type, Unit, Module, Pattern) <-
      */

structural_recursion_pattern(Type, Unit, Module, Pattern) :-
   program_module_to_xml(Unit, Module, Xml),
   structural_recursion_pattern(Type, Xml, Pattern).


/* structural_recursion_pattern(Type, Xml, Pattern) <-
      */

structural_recursion_pattern(maplist, Xml, Pattern) :-
   Rule := Xml^file::[@path=File]^rule,
   Head := Rule^head,
   Atom_1 := Head^atom,
   Body := Rule^body,
   Atom_2 := Body-nth(K)^atom,
   list_argument(Atom_1, Predicate, N, X:Xs),
   list_argument(Atom_1, Predicate, M, Y:Ys),
   N < M,
   rest_positions(Atom_1, N, M, Arguments),
   variable_argument(Atom_2, Predicate, N, Xs),
   variable_argument(Atom_2, Predicate, M, Ys),
   rest_positions(Atom_2, N, M, Arguments),
   head_arguments_do_not_intersect(X:Xs, Y:Ys, Arguments),
   rest_body_arguments_do_not_intersect(
      [Xs, Ys], Body, Predicate, K),
   writeln(user, [Predicate, N=X:Xs, M=Y:Ys, rest=Arguments]),
   Pattern = file:[path:File]:[
      rule:[]:[maplist:[n:N, m:M, k:K]:[], Head, Body]].

structural_recursion_pattern(checklist, Xml, Pattern) :-
   Rule := Xml^file::[@path=File]^rule,
   Head := Rule^head,
   Atom_1 := Head^atom,
   Body := Rule^body,
   Atom_2 := Body-nth(K)^atom,
   list_argument(Atom_1, Predicate, N, X:Xs),
   variable_argument(Atom_2, Predicate, N, Xs),
   writeln(user, [Predicate, N=X:Xs]),
   Pattern = file:[path:File]:[
      rule:[]:[checklist:[n:N, k:K]:[], Head, Body]].


/*** implementation ***********************************************/


/* list_argument(Atom, Predicate, N, X:Xs) <-
      */

list_argument(Atom, Predicate, N, X:Xs) :-
   Predicate := Atom@predicate,
   Argument := Atom-nth(N)^_,
   '.' := Argument@functor,
   X := Argument-nth(1)^var@name,
   Xs := Argument-nth(2)^var@name.


/* variable_argument(Atom, Predicate, N, Variable) <-
      */

variable_argument(Atom, Predicate, N, Variable) :-
   Predicate := Atom@predicate,
   Argument := Atom-nth(N)^_,
   Variable := Argument@name.


/* head_arguments_do_not_intersect(X:Xs, Y:Ys, Terms) <-
      */

head_arguments_do_not_intersect(X:Xs, Y:Ys, Terms) :-
   terms_to_variables(Terms, Variables),
   sort([X, Xs, Y, Ys], Variables_2),
   ord_disjoint(Variables, Variables_2).


/* rest_body_arguments_do_not_intersect(
         [Xs, Ys], Body, Predicate, K) <-
      */

rest_body_arguments_do_not_intersect(
      [Xs, Ys], Body, Predicate, K) :-
   findall( Atom,
      ( Atom := Body-nth(L)^atom,
        L \= K ),
      Atoms ),
   atoms_to_variables(Atoms, Variables),
   sort([Xs, Ys], Variables_2),
   ord_disjoint(Variables, Variables_2),
   findall( Predicate,
      Predicate := (atoms:Atoms)@predicate, 
      [] ).


/* atoms_to_variables(Atoms, Variables) <-
      */

atoms_to_variables(Atoms, Variables) :-
   findall( Variable,
      Variable := (atoms:Atoms)^_^var@name,
      Variables_2 ),
   sort(Variables_2, Variables).


/* terms_to_variables(Terms, Variables) <-
      */

terms_to_variables(Terms, Variables) :-
   findall( Variable,
      ( Variable := (terms:Terms)^var@name
      ; Variable := (terms:Terms)^_^var@name ),
      Variables_2 ),
   sort(Variables_2, Variables).


/* rest_positions(atom:_:Arguments_1, N, M, Arguments_2) <-
      */

rest_positions(atom:_:Arguments_1, N, M, Arguments_2) :-
   findall( Argument,
      ( nth(L, Arguments_1, Argument),
        \+ member(L, [N, M]) ),
      Arguments_2 ),
   !.


/* tuples_to_table_rows(Tuples, Rows) <-
      */

tuples_to_table_rows(Tuples, Rows) :-
   maplist( tuple_to_table_row,
      Tuples, Rows ).

tuple_to_table_row(Tuple, Row) :-
   maplist( tuple_component_to_table_row_component,
      Tuple, Row ).

tuple_component_to_table_row_component(X, X) :-
   atomic(X),
   !.
tuple_component_to_table_row_component(X, Y) :-
   term_to_atom(X, Y),
   !.


/*** tests ********************************************************/


test(structural_recursion, dir(maplist)) :-
   prolog_flag(home, Prolog),
   concat(Prolog, '/library', Dir),
   directory_to_patterns(maplist, Dir, _).
test(structural_recursion, dir(checklist)) :-
   prolog_flag(home, Prolog),
   concat(Prolog, '/library', Dir),
   directory_to_patterns(checklist, Dir, _).
test(structural_recursion, unit(maplist_and_checklist)) :-
   structural_recursion_patterns(maplist, xml),
   structural_recursion_patterns(checklist, xml).


/******************************************************************/


