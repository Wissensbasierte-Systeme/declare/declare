

/******************************************************************/
/***                                                            ***/
/***        DisLog:  Analysis of Meta Predicates                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* higher_level_predicates_to_frequency_bar_chart <-
      */

higher_level_predicates_to_frequency_bar_chart :-
   Predicates = [
%     (meta:findall)/3, (meta:forall)/2,
%     (meta:checklist)/2,
      (meta:maplist)/3 ],
%  directly_recursive_predicate(Modules_1, _),
   Modules_1 = [],
   definition_calls_predicate(
      Predicates, Modules_2, _Distribution),
   writeln(user, Modules_2),
   multiset_union(Modules_1, Modules_2, Modules_3),
   definition_calls_predicate([_], Modules_4, _),
   dislog_frequency_bar_chart_common(
      'Higher--Level', Modules_3, Modules_4).


/* directly_recursive_predicates_to_frequency_bar_chart <-
      */

directly_recursive_predicates_to_frequency_bar_chart :-
   directly_recursive_predicate(Modules_1, _),
   definition_calls_predicate([_], Modules_2, _),
   dislog_frequency_bar_chart_common(
      'Directly Recursive', Modules_1, Modules_2).


/* dislog_predicates_to_frequency_bar_chart(Predicates) <-
      */

dislog_predicates_to_frequency_bar_chart(Predicates) :-
   definition_calls_predicate(Predicates, Modules_1, _),
   definition_calls_predicate([_], Modules_2, _),
   term_to_atom(Predicates, Title),
   dislog_frequency_bar_chart_common(
      Title, Modules_1, Modules_2).


/* dislog_frequency_bar_chart_common(
         Title, Modules_1, Modules_2) <-
      */

dislog_frequency_bar_chart_common(
      Title, Modules_1, Modules_2) :-
   dislog_variable_get(dislog_unit_colours, Cs),
   length(Modules_2, N),
   first_n_elements(N, Cs, Colours),
   triple_lists(Modules_1, Modules_2, Colours, Triples),
   maplist( triple_to_distribution,
      Triples, Distribution ),
   dislog_bar_chart(Title, Distribution, 20),
   findall( [A, Amount],
      ( member([A, _]-Amount_2, Distribution),
        round(Amount_2, 2, Amount) ),
      Rows ),
   xpce_display_table(_, _, Title, ['Unit', 'Calls'], Rows).         

triple_to_distribution(
      [Module:N1, Module:N2, Colour], [Module,Colour]-N ) :-
    N is 100 * N1 / N2.


/*** implemantation ***********************************************/


/* directly_recursive_predicate(Modules, N) <-
      */

directly_recursive_predicate(Modules, N) :-
   findall( Module,
      ( rar_database:db_get_next(fact, definition, X, _, Y),
        Definition = definition:Y,
        Rule := Definition^rules^rule,
        X := Rule^body^atom@name,
        Path := Rule@name,
        path_to_unit_name(Path, Module) ),
      Modules_2 ),
%  field_notation_to_xml(definitions:Definitions),
   list_to_multiset(Modules_2, Modules),
   length(Modules_2, N).


/* definition_calls_predicate(Predicates, Modules, N) <-
      */

definition_calls_predicate(Predicates, Modules, N) :-
   findall( Module,
      ( rar_database:db_get_next(fact, definition, _, _, Y),
        Definition = definition:Y, 
        Rule := Definition^rules^rule,
        Predicate := Rule^body^atom@name,
        member(Predicate, Predicates),
        Path := Rule@name,
        path_to_unit_name(Path, Module) ),
      Modules_2 ),
%  field_notation_to_xml(definitions:Definitions),
   list_to_multiset(Modules_2, Modules),
   length(Modules_2, N).


/******************************************************************/


