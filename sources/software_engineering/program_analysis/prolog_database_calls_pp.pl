

/******************************************************************/
/***                                                            ***/
/***           DDK:  Prolog Database Calls                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* predicates_rules_and_files(Triples) <-
      */

predicates_rules_and_files(Triples) :-
   setof( [Predicate, Rule, File],
      ( predicate_to_rule_and_file(Predicate, Rule, File),
        write(user, '.') ),
      Triples ).


/* predicates_rules_and_files_in_modules(Triples) <-
      */

predicates_rules_and_files_in_modules(Triples) :-
   setof( [Module:Predicate, Rule, File],
      ( current_module(Module),
        module(Module),
        writeln(user, Module),
        predicate_to_rule_and_file(Predicate, Rule, File) ),
      Triples ),
   module(user).


/* predicate_to_rule_and_file(Predicate, Rule, File) <-
      */

predicate_to_rule_and_file(Predicate, Rule, File) :-
   current_predicate(Predicate, Head),
   catch_fail(clause(Head, Body, Reference)),
   clause_property(Reference, file(File)),
   Rule = (Head :- Body).


/* predicate_and_arity_to_rule_and_file(
         Predicate/Arity, Rule, File) <-
      */

predicate_and_arity_to_rule_and_file(
      Predicate/Arity, Rule, File) :-
   predicate_to_clause(Predicate/Arity, Rule, Reference),
   clause_property(Reference, file(File)).


/* predicate_to_clause(
         Predicate/Arity, Head :- Body, Reference) <-
      */

predicate_to_clause(Predicate/Arity, Head :- Body) :-
   current_predicate(Predicate, Head),
   functor_(Head, Predicate, Arity),
   catch_fail(clause(Head, Body)).

predicate_to_clause(Predicate/Arity, Head :- Body, Reference) :-
   current_predicate(Predicate, Head),
   functor_(Head, Predicate, Arity),
   catch_fail(clause(Head, Body, Reference)).


/* functor_(Atom, Predicate, Arity) <-
      */

functor_(Atom, Predicate, Arity) :-
   catch_fail(functor_(Atom, Predicate, Arity)).


/* catch_fail(Goal) <-
      */

catch_fail(Goal) :-
   catch(Goal, _, fail).

catch_test(Goal) :-
   catch( (Goal, write(yes)),
      _, write(no) ).


/******************************************************************/


