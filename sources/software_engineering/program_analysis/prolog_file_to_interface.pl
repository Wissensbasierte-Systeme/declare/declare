

/******************************************************************/
/***                                                            ***/
/***           DDK:  Prolog Database Calls                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* predicate_calls_predicate(Edges) <-
      */

predicate_calls_predicate :-
   forall( predicate_calls_predicate(X1, X2),
      writeln(X1-X2) ).

predicate_calls_predicate(Edges) :-
   findall( X1-X2,
      ( predicate_calls_predicate(X1, X2),
        writeln(X1-X2) ),
      Edges ).


/* predicate_calls_predicate(X1, X2) <-
      */

predicate_calls_predicate(X1, X2) :-
   predicate_calls_predicate(P1/A1, File_1, P2/A2, File_2),
   concat([File_1, '/', P1, '/', A1], X1),
   concat([File_2, '/', P2, '/', A2], X2).

/*
predicate_calls_predicate(P1/A1, File_1, P2/A2, File_2) :-
   catch(
      predicate_calls_predicate_(P1/A1, File_1, P2/A2, File_2),
      _, fail ).
*/

predicate_calls_predicate(P1/A1, File_1, P2/A2, File_2) :-
   predicate_and_arity_to_rule_and_file(P1/A1, Rule_1, File_1),
   concat('/home/seipel/DisLog/sources', _, File_1),
   Rule_1 = (_ :- Body),
   atom_calls_predicate(Body, P2/A2),
   predicate_and_arity_to_rule_and_file(P2/A2, _, File_2).


/* atom_calls_predicate(Atom, Predicate/Arity) <-
      */

atom_calls_predicate(Atom, _) :-
   var(Atom),
   !,
   fail.
atom_calls_predicate(Atom, Predicate/Arity) :-
   Atom =.. [Junctor|Atoms],
   member(Junctor, [',', ';', not, \+, ->]),
   !,
   member(A, Atoms),
   atom_calls_predicate(A, Predicate/Arity).
atom_calls_predicate(Atom, Predicate/Arity) :-
   Atom =.. [Meta_Predicate, _, Goal, _],
   member(Meta_Predicate, [findall, setof, bagof]),
   !,
   atom_calls_predicate(Goal, Predicate/Arity).
atom_calls_predicate(Atom, Predicate/Arity) :-
   Atom =.. [Meta_Predicate, A|Atoms],
   member(Meta_Predicate, [maplist, checklist]),
   !,
   length(Atoms, N),
   functor_(A, Predicate, M),
   Arity is N + M.
atom_calls_predicate(Atom, Predicate/Arity) :-
   functor_(Atom, Predicate, Arity).


/******************************************************************/


