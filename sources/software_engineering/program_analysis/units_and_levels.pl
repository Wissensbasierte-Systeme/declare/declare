

/******************************************************************/
/***                                                            ***/
/***        DisLog:  Analysis of Units and Levels               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* calls_ll_problematic(Calls) <-
      */

calls_ll_problematic(Calls) :-
   Level_1 = [
      basic_algebra, xml, reasoning, development ],
   Level_2 = [
      stock_tool, databases, projects ],
   calls_uu_cached(Cs),
   findall( V-W-Ps_2,
      ( member(_-V-W-Ps, Cs),
        member(V, Level_1),
        member(W, Level_2),
        predicates_prune(Ps, Ps_2) ),
      Calls ).

predicates_prune(Predicates_1, Predicates_2) :-
   ord_subtract(Predicates_1, [
      (user:dportray)/2, (user:test)/2, (user: :)/2 ],
      Predicates_2),
   Predicates_2 \= [].


/* maximal_preceedes_uu(Stable_Models) <-
      */

maximal_preceedes_uu(Stable_Models_2) :-
   calls_uu_cached(Cs),
   findall( [calls_uu(V, W)],
      ( member(N-V-W-_, Cs),
        N > 20 ),
      State ),
   dconsult('rar/rar_stable', Rules),
   append(Rules, State, Program),
   program_to_stable_models_dlv(Program, Stable_Models_1),
   maplist( stable_model_reduce_to_ignored,
      Stable_Models_1, Stable_Models_2 ),
   dportray(chs, Stable_Models_2),
   tree_based_boolean_dualization(Stable_Models_2, Stable_State),
   dportray(dhs, Stable_State).

% stable_model_reduce_to_ignored(Model, Model).
stable_model_reduce_to_ignored(Model_1, Model_2) :-
   findall( A,
      ( member(A, Model_1),
        ( A = ignored_uu(V, W)
        ; A = preceedes_uu(V, W) ),
        Units = [basic_algebra, reasoning],
        member(V, Units),
        member(W, Units) ),
      Model_2 ).


/* calls_uu_non_problematic(Calls) <-
      */

calls_uu_non_problematic(Calls) :-
   calls_uu_cached(Cs),
   calls_uu_reduce(Reduced_Graph),
   findall( N-V-W,
      ( member(N-V-W-_, Cs),
        \+ calls_uu_problematic(Reduced_Graph, V, W) ),
      Calls ).


/* calls_uu_problematic(Calls) <-
      */

calls_uu_problematic(Calls) :-
   calls_uu_cached(Cs),
   calls_uu_reduce(Reduced_Graph),
   findall( N-V-W-Ps,
      ( member(N-V-W-Ps, Cs),
        calls_uu_problematic(Reduced_Graph, V, W) ),
      Calls ).

calls_uu_problematic(Reduced_Graph, V, W) :-
   member(Ws-Vss, Reduced_Graph),
   member(W, Ws),
   member(Vs, Vss),
   member(V, Vs).


/* calls_uu_reduce(Reduced_Graph) <-
      */

calls_uu_reduce(Reduced_Graph) :-
   calls_uu_reduce(25, Reduced_Graph).

calls_uu_reduce(K, Reduced_Graph) :-
   calls_uu_cached(Cs),
   findall( V-W,
      ( member(N-V-W-_, Cs),
        ( N > K
        ; W = xml ) ),
      Edges ),
   findall( V,
      ( member(_-V-_-_, Cs)
      ; member(_-_-V-_, Cs) ),
      Vs ),
   list_to_ord_set(Vs, Vertices),
   vertices_edges_to_ugraph(Vertices, Edges, Graph),
   vertices_edges_to_picture(Vertices, Edges),
   reduce(Graph, Reduced_Graph).


calls_uu_cached_short(Cs) :-
   calls_uu_cached(Cs_2),
   findall( (V-W):N,
      member(N-V-W-_, Cs_2),
      Cs ).


/* calls_uu_relevant_to_picture <-
      */

calls_uu_relevant_to_picture :-
   calls_uu_relevant(Cs),
   findall( V-W,
      ( member(N-_-V-W-_, Cs),
        N > 0 ),
      Edges ),
   edges_to_vertices(Edges, Vertices),
   vertices_edges_to_gxl_graph(Vertices, Edges, Graph),
   gxl_graph_to_picture(Graph),
   findall( [N, M, V, W],
      ( member(N-M-V-W-_, Cs),
        N > 0 ),
      Rows ),
   xpce_display_table(_, _, 'Units',
      ['N', 'M', 'V', 'W'], Rows ).

calls_uu_relevant(Cs) :-
   Predicates = [
      'base_literal/1',
      'dislog_variable/2',
      'dportray/2',
      'test/2',
      '(:)/2',
      'trim/2',
      'dsa_dialog/1',
      'dislog_files_to_xml/1',
      'ghostview_charts/1', 'preview_latex_file/0',
      'rar_variable_get/2', 'vertices_edges_to_gxl_graph/3' ],
   sort(Predicates, Predicates_2),
   calls_uu_cached(Cs_1),
   findall( N-M-V-W-Ps,
      ( member(M-V-W-Qs, Cs_1),
        ord_subtract(Qs, Predicates_2, Ps),
        length(Ps, N),
        N > 6 ),
      Cs_2 ),
   sort(Cs_2, Cs).


/* calls_uu_cached(Cs) <-
      */

calls_uu_cached :-
   retract_all(rar_database:calls_uu(_)),
   file_defines_predicate,
   calls_uu_cached(_).

calls_uu_cached(Cs) :-
   rar_database:calls_uu(Cs).
calls_uu_cached(Cs) :-
   calls_uu(Cs),
   assert(rar_database:calls_uu(Cs)).


/* calls_uu(Calls) <-
      */

calls_uu(Calls) :-
   findall( Unit_1-Unit_2-Predicate,
      ( calls_uu(unit:Unit_1, unit:Unit_2, predicate:Predicate),
        writeln(user, Unit_1-Unit_2-Predicate) ),
      Calls_2 ),
   list_to_ord_set(Calls_2, Calls_3),
   findall( Unit_1-Unit_2,
      member(Unit_1-Unit_2-_, Calls_3),
      Calls_4 ),
   list_to_multiset(Calls_4, Calls_5),
   findall( Multiplicity-Unit_1-Unit_2-Predicates,
      ( member(Unit_1-Unit_2:Multiplicity, Calls_5),
        findall( Predicate,
           member(Unit_1-Unit_2-Predicate, Calls_3),
           Predicates ) ),
      Calls_6 ),
   list_to_ord_set(Calls_6, Calls).

calls_uu(unit:Unit_1, unit:Unit_2, predicate:Predicate) :-
   dread(xml, 'dislog.xml', [DisLog]),
   !,
   calls_fp(DisLog, File_1, Predicate),
   file_defines_predicate(File_2, Predicate),
%  defines_fp(DisLog, File_2, Predicate),
%  calls_fp(file:File_1, predicate:Predicate),
%  defines_fp(file:File_2, predicate:Predicate),
   File_1 \= File_2,
   path_to_unit_name(File_1, Unit_1),
   path_to_unit_name(File_2, Unit_2),
   Unit_1 \= Unit_2.


calls_fp(file:File, predicate:Predicate) :-
   rar_database:db_get_next(fact, definition, _, _, Y),
   Definition = definition:Y,
   Rule := (Definition)^rules^rule,
   File := Rule@name,
   Predicate := Rule^body^atom@name.

defines_fp(file:File, predicate:Predicate) :-
   rar_database:db_get_next(
      fact, definition, Predicate, _, Definition),
   File := Definition^rules@rule^name.

file_defines_predicate :-
   dread(xml, 'dislog.xml', [DisLog]),
   forall( defines_fp(DisLog, File, Predicate),
      assert(file_defines_predicate(File, Predicate)) ).


/* rar_calls_unit_unit(Calls) <-
      */

rar_calls_unit_unit(Calls) :-
   findall( [X1-Predicate_1, X2-Predicate_2],
      ( rar_calls_unit_unit(X1-Predicate_1, X2-Predicate_2),
        writeln(user, [X1-Predicate_1, X2-Predicate_2]) ),
      Calls ).

rar_calls_unit_unit(X1-Predicate_1, X2-Predicate_2) :-
   rar_retrieval:calls(unit:X1, unit:X2,
      predicate:Predicate_1, predicate:Predicate_2),
   X1 \= X2.


/* path_to_unit_name(Path, Unit) <-
      */

path_to_unit_name(Path, Unit) :-
   name(Path, List_1),
   reverse(List_1, List_2),
   list_start_after_position(["/"], List_2, List_3),
   list_start_after_position(["/"], List_3, List_4),
   list_cut_at_position(["/"], List_4, List_5),
   reverse(List_5, List_6),
   name(Unit, List_6),
   !.


/******************************************************************/


