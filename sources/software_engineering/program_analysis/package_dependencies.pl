

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Package Dependencies                    ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(package_dependencies, predicate_to_rule_and_file) :-
   predicate_to_rule_and_file(Predicate, Rule, File),
   writeln(Predicate), writeln(File),
   dportray(prolog, [Rule]).

test(package_dependencies, store_predicates_to_files) :-
   List =[
      dislog_unit_calls_unit_asserted/3,
      dislog_unit_defines_predicate_asserted/2,
      prolog_predicate_calls_predicate_asserted/2 ],
   ( foreach(Predicate/Arity, List) do
        store_facts_to_file(Predicate/Arity, Predicate) ).


/*** interface ****************************************************/


/* dislog_units(Units) <-
      */

dislog_units(Units) :-
   Units = [
      basic_algebra, databases, xml, reasoning, 
      artificial_intelligence, 
      software_engineering, source_code_analysis,
      stock_tool, biology_and_medicine, linguistics,
      projects ].


/* dislog_unit_calls_unit <-
      */

dislog_unit_calls_unit :-
   dislog_units(Units),
   ( foreach(Unit, Units), foreach(Row, Rows) do
        ( foreach(Unit_2, Units), foreach(N, Ns) do
             ( dislog_unit_calls_unit_asserted(Unit, Unit_2, N)
             ; N = 0 ) ),
        Row = [Unit|Ns] ),
   writeln_list(Rows),
   Attributes = [''|Units],
   xpce_display_table(dislog_unit_calls_unit,
      Attributes, Rows).


/* dislog_unit_calls_unit(Triples) <-
      */

dislog_unit_calls_unit(Triples) :-
   ddbase_aggregate( [U_1, U_2, length(Pair)],
      ( dislog_unit(U_1, _),
        writeln(U_1), ttyflush,
        dislog_unit_defines_predicate_asserted(U_1, P_1),
        prolog_predicate_calls_predicate_asserted(P_1, P_2),
        dislog_unit_defines_predicate_asserted(U_2, P_2),
        Pair = [U_1, U_2] ),
      Triples ).

dislog_unit_calls_unit_2(Triples) :-
   Unit_1 = stock_tool,
   ddbase_aggregate( [Unit_1, Unit_2, length(Pair)],
      ( dislog_unit_calls_unit(Unit_1, Unit_2),
        Pair = [Unit_1, Unit_2] ),
      Triples ).


/* dislog_unit_calls_unit_2(Unit_1, Unit_2) <-
      */

dislog_unit_calls_unit_2(Unit_1, Unit_2) :-
   dislog_unit_calls_predicate(Unit_1, Predicate/N),
   dislog_unit_defines_predicate(Unit_2, Predicate/N).


/* dislog_unit_calls_predicate(Unit, Predicate/N) <-
      */

dislog_unit_calls_predicate(Unit, Predicate/N) :-
   prolog_file_calls_predicate(File, Predicate/N),
   name_split_at_position(["/"], File, Xs),
   last_n_elements(3, Xs, [Unit|_]).

dislog_unit_defines_predicate(Pairs) :-
   findall( [Unit, Predicate/N],
      dislog_unit_defines_predicate(Unit, Predicate/N),
      Pairs_2 ),
   sort(Pairs_2, Pairs).

dislog_unit_defines_predicate(Unit, Predicate/N) :-
   prolog_file_defines_predicate(File, Predicate/N),
   name_split_at_position(["/"], File, Xs),
   last_n_elements(3, Xs, [Unit|_]).


/* prolog_unit_module_file_calls_predicate(Ys, Predicate) <-
      */

prolog_unit_module_file_calls_predicate(Ys, Predicate) :-
   predicate_property(Atom, file(File)),
   Atom =.. [Predicate|_],
   name_split_at_position(["/"], File, Xs),
   last_n_elements(3, Xs, Ys).


/* predicate_calls_predicate(Predicate_A/N, Predicate_B/M) <-
      */

prolog_predicate_calls_predicate(Pairs) :-
   findall( [Predicate_A/N, Predicate_B/M],
      prolog_predicate_calls_predicate(
         Predicate_A/N, Predicate_B/M),
      Pairs_2 ),
   sort(Pairs_2, Pairs).

prolog_predicate_calls_predicate(Predicate_A/N, Predicate_B/M) :-
   current_predicate(_, Head),
   catch_fail(clause(Head, Body, _)),
   functor(Head, Predicate_A, N),
   prolog_formula_to_predicate(Body, Predicate_B/M).


/* prolog_file_calls_predicate(File, Predicate/N) <-
      */

prolog_file_calls_predicate(File, Predicate/N) :-
   current_predicate(_, Head),
   catch_fail(clause(Head, Body, Reference)),
   clause_property(Reference, file(File)),
   prolog_formula_to_predicate(Body, Predicate/N).


/* prolog_file_defines_predicate(File, Predicate/N) <-
      */

prolog_file_defines_predicate(File, Predicate/N) :-
   predicate_property(Atom, file(File)),
   functor(Atom, Predicate, N).


/* prolog_formula_to_predicate(Formula, Predicate) <-
      */

prolog_formula_to_predicates(Formula, Predicates) :-
   findall( Predicate,
      prolog_formula_to_predicate(Formula, Predicate),
      Predicates ).

prolog_formula_to_predicate(Formula, Predicate) :-
   comma_structure_to_list(Formula, Atoms),
   member(call(X), Atoms),
   member(X=F, Atoms),
   prolog_formula_to_predicate(F, Predicate).

prolog_formula_to_predicate(Formula, Predicate) :-
   nonvar(Formula),
   Formula =.. [Junktor, A, B],
   member(Junktor, [',', ';']),
   !,
   ( prolog_formula_to_predicate(A, Predicate)
   ; prolog_formula_to_predicate(B, Predicate) ).
prolog_formula_to_predicate(Formula, Predicate) :-
   nonvar(Formula),
   Formula =.. [Junktor, _, B, _],
   member(Junktor, [findall, setof, bagof, ddbase_aggregate, maplist]),
   !,
   prolog_formula_to_predicate(B, Predicate).
prolog_formula_to_predicate(Formula, Predicate) :-
   nonvar(Formula),
   Formula =.. [Junktor, B],
   member(Junktor, [not, \+]),
   !,
   prolog_formula_to_predicate(B, Predicate).
prolog_formula_to_predicate(Formula, Predicate/Arity) :-
   nonvar(Formula),
   functor(Formula, Predicate, Arity).


/* prolog_formula_to_fuction(Formula, Function) <-
      */

prolog_formula_to_functions(Formula, Functions) :-
   findall( Function,
      prolog_formula_to_function(Formula, Function),
      Functions ).

prolog_formula_to_function(Formula, Function) :-
   nonvar(Formula),
   Formula =.. [Junktor, A, B],
   member(Junktor, [',', ';']),
   !,
   ( prolog_formula_to_function(A, Function)
   ; prolog_formula_to_function(B, Function) ).
prolog_formula_to_function(Formula, Function) :-
   nonvar(Formula),
   Formula =.. [Junktor, _, B, _],
   member(Junktor, [findall, setof, bagof, ddbase_aggregate]),
   !,
   prolog_formula_to_function(B, Function).
prolog_formula_to_function(Formula, Function) :-
   nonvar(Formula),
   Formula =.. [Junktor, B],
   member(Junktor, [not, \+]),
   !,
   prolog_formula_to_function(B, Function).
prolog_formula_to_function(Formula, Function/Arity) :-
   nonvar(Formula),
   \+ atomic(Formula),
   arg(_, Formula, Term),
   prolog_term_to_function(Term, Function/Arity).


/* prolog_term_to_function(Term, Function/Arity) <-
      */

prolog_term_to_functions(Term, Functions) :-
   findall( Function,
      prolog_term_to_function(Term, Function),
      Functions ).

prolog_term_to_function(Term, Function/Arity) :-
   nonvar(Term),
   ( functor(Term, Function, Arity)
   ; \+ atomic(Term),
     arg(_, Term, Subterm),
     prolog_term_to_function(Subterm, Function/Arity) ).


/******************************************************************/


