

/******************************************************************/
/***                                                            ***/
/***          DisLog: Management                                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tgz_sources_notebook <-
      */

tgz :-
   archive_date(Date),
   concat(['cd ~/Declare; ',
      'tar czvf Archive/notebook/sources_',
         Date, '.tgz sources projects/XUL ',
         'projects/Trinodis/sources_1 ',
         'projects/Trinodis/sources_2 ',
         'projects/Trinodis/data' ],
      Command),
   writeln(user, Command), wait,
   unix(system(Command)).

tgz_sources_notebook :-
   archive_date(Date),
   concat(['cd ~/Declare; ',
      'tar czvf Archive/notebook/sources_',
         Date, '.tgz sources'],
      Command),
   writeln(user, Command), wait,
   unix(system(Command)).

archive_date(Date) :-
   date(date(Year, Month, Day)),
   date_to_natural_date(Day-Month-Year, Date_N),
   name(Date_N, [D1,D2, _, M1,M2, _, Y1,Y2,Y3,Y4]),
   name('_', [Underscore]),
   atom_codes(Date,
      [Y1,Y2,Y3,Y4, Underscore, M1,M2, Underscore, D1,D2]).


/******************************************************************/


