

/******************************************************************/
/***                                                            ***/
/***          DisLog: Management                                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* directory_to_xml(Directory, Xml) <-
      Directory has to be an absolute path name. */

directory_to_xml(Directory, Xml) :-
   directory_contains_files(Directory, Files),
   findall( X,
      ( member(File, Files),
        concat([Directory, '/', File], Path),
        system:exists_file(Path),
        size_file(Path, Size),
        X = file:[name:File, size:Size]:[] ),
      Xs_1 ),
   length(Xs_1, N_1),
   directory_contains_directories(Directory, Dirs),
   findall( X,
      ( member(Dir, Dirs),
        system:exists_directory(Dir),
        directory_to_xml(Dir, X) ),
      Xs_2 ),
   length(Xs_2, N_2),
   append(Xs_1, Xs_2, Xs),
   Xml = directory:[path:Directory, files:N_1, directories:N_2]:Xs,
   !.
   

/******************************************************************/


