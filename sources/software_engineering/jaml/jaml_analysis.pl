

/******************************************************************/
/***                                                            ***/
/***       Jaml:  Analysis                                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* jaml_directory_to_picture(Call_Method, Directory) <-
      */

jaml_directory_to_picture(Call_Method, Directory) :-
   directory_contains_files(Directory, Files),
   concat(Directory, '/', Dir),
   maplist( concat(Dir),
      Files, Paths ),
   jaml_files_to_picture(Call_Method, Paths),
   !.


/* jaml_files_to_picture(Call_Method, Files) <-
      */

jaml_file_to_picture(Call_Method, File) :-
   jaml_files_to_picture(Call_Method, [File]).

jaml_files_to_picture(Call_Method, Files) :-
   jaml_files_to_graph(Call_Method, Files, Vs, Es, _, _),
   vertices_edges_to_picture(Vs, Es).


/* jaml_files_to_graph(Call_Method, Files,
         Vertices, Edges, Graph, Reduced_Graph) <-
      */

jaml_files_to_graph(Call_Method, Files,
      Vertices, Edges, Graph, Reduced_Graph) :-
   findall( C1-C2,
      ( member(File, Files),
        xml_file_to_fn_term(File, Jaml),
        apply(Call_Method, [(_:Jaml), C1, C2]) ),
%       \+ is_a_get_method(C1),
%       \+ is_a_get_method(C2) ),
      Edges ),
   edges_to_vertices(Edges, Vertices),
   vertices_edges_to_ugraph(Vertices, Edges, Graph),
   reduce(Graph, Reduced_Graph).

is_a_get_method(Method) :-
   name(Method, Name),
   append("get", _, Name).
is_a_get_method(Method) :-
   name(Method, Name),
   ( append("toString", _, Name)
   ; append("println", _, Name)
   ; append("size", _, Name)
   ; append("equals", _, Name) ).


/* jaml_files_to_patterns_file(Type, Base, File) <-
      */

jaml_files_to_patterns_file(Type, Base, File) :-
   jaml_files(Base, Files),
   jaml_files_to_patterns(Type, Files, Xml),
   fn_term_to_xml_file(Xml, File).


/* jaml_files_to_patterns(Type, Files, Xml) <-
      */

jaml_files_to_patterns(Type, Files, Xml) :-
   maplist( jaml_file_to_patterns(Type),
      Files, Xmls ),
   Xml = patterns:Xmls.


/* jaml_file_to_patterns(Type, File, Xml) <-
      */

jaml_file_to_patterns(Type, File, Xml) :-
   xml_file_to_fn_term(File, Jaml),
   !,
   jaml_to_patterns(Type, (_:Jaml), Patterns),
   Xml = patterns:[file:File]:Patterns,
   star_line,
   field_notation_to_xml(Xml),
   star_line.

jaml_to_patterns(Type, Jaml, Patterns) :-
   findall( Pattern,
      jaml_to_pattern(Type, Jaml, Pattern),
      Patterns ).


/*** tests ********************************************************/


test(jaml, abstract_class(1)) :-
   dislog_variable_get(jaml_home, Directory),
   concat(Directory, '/java/awt/Toolkit.xml', Path),
   jaml_file_to_patterns(abstract_class, Path, _).

test(jaml, jaml_file_to_picture) :-
   dislog_variable_get(jaml_home, Directory),
   concat(Directory, '/java/awt/Toolkit.xml', Path),
   jaml_file_to_picture(calls_mm, Path).

test(jaml, composite_patterns(1)) :-
   dislog_variable_get(jaml_home, Directory),
   concat(Directory, '/java/awt/Container.xml', Path),
   jaml_file_to_patterns(composite, Path, _).

test(jaml, composite_patterns(2)) :-
   dislog_variable_get(output_path, Results),
   concat(Results, 'jaml_output.xml', Path),
   jaml_files_to_patterns_file(
      composite, 'java/awt/event', Path).

test(jaml, jaml_directory_to_picture) :-
   home_directory(Home),
   concat( [ Home, '/teaching/diplomarbeiten/Duensser_Masato/',
      '2003_09_17/105_Final_SingleQuery_jaml' ], Directory ),
   jaml_directory_to_picture(calls_mm, Directory).

test(jaml, jaml_file_to_picture_aaa) :-
   home_directory(Home),
   concat( [ Home, '/teaching/diplomarbeiten/Duensser_Masato/',
      '2003_09_17/My_jaml/BLASTupdater.java.jaml' ], File ),
   jaml_file_to_picture(calls_mm, File).


/******************************************************************/


