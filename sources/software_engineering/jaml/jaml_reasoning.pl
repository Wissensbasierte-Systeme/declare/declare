

/******************************************************************/
/***                                                            ***/
/***       Jaml:  Reasoning                                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* jaml_directory_to_edges(Method, Directory, Edges) <-
      */

jaml_directory_to_edges(Method, Directory, Edges) :-
   directory_contains_files_absolute(Directory, Paths),
   jaml_files_to_edges(Method, Paths, Edges).


/* jaml_files_to_edges(Method, Files, Edges) <-
      */

jaml_files_to_edges(calls_cc, Files, Edges) :-
   jaml_files_apply_method(calls_cc, Files, Xml),
   findall( C1-C2,
      ( Calls_cc := Xml^calls_cc,
        C1 := Calls_cc@source,
        C2 := Calls_cc@target ),
      Edges_2 ),
   list_to_ord_set(Edges_2, Edges).


/* jaml_directory_apply_method(Method, Directory, Xml) <-
      */

jaml_directory_apply_method(Method, Directory, Xml) :-
   directory_contains_files_absolute(Directory, Paths),
   jaml_files_apply_method(Method, Paths, Xml).


/* jaml_files_apply_method(Method, Files, Xml) <-
      */

jaml_files_apply_method(Method, Files, Xml) :-
   xml_files_to_fn_term(Files, Jaml),
   jaml_apply_method(Method, [], (_:Jaml), Xml).


/* jaml_file_apply_method(Method, File, Xml) <-
      */

jaml_file_apply_method(Method, File, Xml) :-
   xml_file_to_fn_term(File, Jaml),
   jaml_apply_method(Method, [file:File], (_:Jaml), Xml).


/* jaml_apply_method(Method, Attributes, Jaml, Xml) <-
      */

jaml_apply_method(Method, Attributes, Jaml, Xml) :-
   member(Method, [calls_cc, calls_cmcm, calls_mm, owns_cm]),
   Goal =.. [Method, Jaml, Results],
   call(Goal),
   Xml = relations:Attributes:Results,
   star_line,
   field_notation_to_xml(Xml),
   star_line.


/*** tests ********************************************************/


test(jaml, calls_mm) :-
   test_jaml_sub(jaml, jaml_file_apply_method(calls_mm)).

test(jaml, owns_cm) :-
   test_jaml_sub(jaml, jaml_file_apply_method(owns_cm)).

test(jaml, calls_cc) :-
   test_jaml_sub(jaml, jaml_file_apply_method(calls_cc)).

test(jaml, calls_cmcm) :-
   test_jaml_sub(jaml, jaml_file_apply_method(calls_cmcm)).


test_jaml_sub(jaml, jaml_file_apply_method(Method)) :-
   dislog_variable_get(jaml_home, Directory),
   concat(Directory, '/java/awt/Toolkit.xml', Path_1),
   test_jaml_sub(jaml, jaml_file_apply_method(Method), Path_1).

test_jaml_sub(jaml, jaml_file_apply_method(Method), Path_1) :-
   jaml_file_apply_method(Method, Path_1, Xml),
   dislog_variable_get(output_path, Results),
   concat(Results, 'jaml_output.xml', Path_2),
   fn_term_to_xml_file(Xml, Path_2).


test(jaml, jaml_directory_to_picture_improved) :-
   home_directory(Home),
   concat( [ Home, '/teaching/diplomarbeiten',
      '/Duensser_Masato/2003_09_17/My_jaml' ],
      Directory ),
   jaml_directory_to_edges(calls_cc, Directory, Edges_1),
   Prefixes = [ 'BLAST', 'COG', 'SMART', 'JESS',
      'WebProgrammStarter', 'MyBSMLstudio' ],
   graph_edges_coarsen(Prefixes, Edges_1, Edges),
   edges_to_vertices(Edges, Vertices),
%  vertices_edges_reduce(Vertices, Edges, Vertices_2, Edges_2),
%  writeln(user, Vertices_2),
   vertices_edges_to_picture(Vertices, Edges).
%  vertices_edges_to_picture(Vertices_2, Edges_2).

test(jaml, my_bsml_studio) :-
   Edges = [
      'MyBSMLstudio'-'COG',
      'MyBSMLstudio'-'BLAST',
      'MyBSMLstudio'-'SMART',
      'MyBSMLstudio'-'JESS' ],
   edges_to_vertices(Edges, Vertices),
   vertices_edges_to_picture(Vertices, Edges).


/******************************************************************/


