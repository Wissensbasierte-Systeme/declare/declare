

/******************************************************************/
/***                                                            ***/
/***       Jaml:  Files                                         ***/
/***                                                            ***/
/******************************************************************/


dislog_variable(jaml_home, Dir) :-
   home_directory(Home),
   concat(
      Home, '/research/projects/Jaml/Javasrc_Jaml/xml', Dir).


/*** interface ****************************************************/


/* jaml_files(Base, Files) <-
      */

jaml_files(Base, Files) :-
   dislog_variable(jaml_home, Jaml_Home),
   concat([Jaml_Home, '/', Base], Path),
   jaml_path_to_files(xml, Path, Files).


/* jaml_path_to_files(Extension, Path, Files) <-
      */

jaml_path_to_files(Extension, Path, Files) :-
   unix_directory_contains_files(Path, Files_1),
   concat(Path, '/', Path_2),
   maplist( concat(Path_2),
      Files_1, Files_2 ),
   sublist( file_name_extension(Extension),
      Files_2, Files_3 ),
   sublist( exists_directory,
      Files_2, Directories ),
   maplist( jaml_path_to_files(Extension),
      Directories, List_of_Files_4 ),
   flatten(List_of_Files_4, Files_4),
   append(Files_3, Files_4, Files).

file_name_extension(Extension, File) :-
   file_name_extension(_, Extension, File).


/*** tests ********************************************************/


test(jaml, jaml_files) :-
   jaml_files('java/awt', Files),
   length(Files, N),
   writeln(user, N).


/******************************************************************/


