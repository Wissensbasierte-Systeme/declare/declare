

/******************************************************************/
/***                                                            ***/
/***       Jaml:  Elementary                                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* jaml_to_java(Jaml, Java) <-
      */
   
jaml_to_java(Jaml) :-
   jaml_to_java(Jaml, Java),
   bar_line,
   writeln(Java),
   bar_line.

jaml_to_java(Jaml, Java) :-
   jaml_to_java_sub(Jaml, Java_1),
   flatten(Java_1, Java_2),
   concat(Java_2, Java).

jaml_to_java_sub(Jaml, '\n') :-
   fn_item_parse(Jaml, newline:_:_),
   !.
jaml_to_java_sub(Jaml, Java) :-
   fn_item_parse(Jaml, _:_:Es),
   !,
   ( ( is_list(Es),
       maplist( jaml_to_java_sub,
          Es, Terms ),
       Java = Terms )
   ; Java = Es ),
   !.
jaml_to_java_sub(Jaml, [' '|Jaml]) :-
   !.


/******************************************************************/


