

/******************************************************************/
/***                                                            ***/
/***       Jaml:  Basic Relations                               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* calls_cc(Jaml, Calls_cc) <-
      */

calls_cc(Jaml, Calls_cc) :-
   findall( Call_cc,
      ( calls_cc(Jaml, C1, C2),
        Call_cc = calls_cc:[source:C1, target:C2]:[] ),
      Calls_cc_2 ),
   list_to_ord_set(Calls_cc_2, Calls_cc).

calls_cc(Jaml, C1, C2) :-
   calls_cmcm(Jaml, C1:_, C2:_).


/* calls_cmcm(Jaml, C1:M1, C2:M2) <-
      */

calls_cmcm(Jaml, Calls_cmcm) :-
   findall( Call_cmcm,
      ( calls_cmcm(Jaml, C1:M1, C2:M2),
        Call_cmcm = calls_cmcm:[]:[
           source:[class:C1, method:M1]:[], 
           target:[class:C2, method:M2]:[] ] ),
      Calls_cmcm_2 ),
   list_to_ord_set(Calls_cmcm_2, Calls_cmcm).

calls_cmcm(Jaml, C1:M1, C2:M2) :-
   owns_cm(Jaml, Owns_cm),
   !,
   calls_cmm(Jaml, C1, M1, M2),
   member(owns_cm:[class:C2, method:M2]:[], Owns_cm),
   \+ member(owns_cm:[class:C1, method:M2]:[], Owns_cm).

/*
calls_cmcm(Jaml, C1:M1, C2:M2) :-
   calls_mm(Jaml, Calls_mm),
   owns_cm(Jaml, Owns_cm),
   !,
   findall( C1:M1-C2:M2,
      ( member(calls_mm:[source:M1, target:M2]:[], Calls_mm),
        member(owns_cm:[class:C1, method:M1]:[], Owns_cm),
        member(owns_cm:[class:C2, method:M2]:[], Owns_cm) ),
      Calls_cmcm ),
   member(C1:M1-C2:M2, Calls_cmcm).
*/


/* calls_mm(Jaml, Method_Name_1, Method_Name_2) <-
      */

calls_mm(Jaml, Calls_mm) :-
   findall( calls_mm:[source:M1, target:M2]:[],
      calls_mm(Jaml, M1, M2),
      Calls_mm ).

calls_mm(Jaml, Method_Name_1, Method_Name_2) :-
   Method_Declaration :=
      Jaml^_^'method-declaration',
   Method_Name_1 :=
      Method_Declaration@name,
   Method_Name_2 :=
      Method_Declaration^_^'method-invocation'@name.


/* owns_cm(Jaml, Class_Name, Method_Name) <-
      */

owns_cm(Jaml, Owns_cm) :-
   findall( owns_cm:[class:Class, method:Method]:[],
      owns_cm(Jaml, Class, Method),
      Owns_cm ).

owns_cm(Jaml, Class_Name, Method_Name) :-
   Class_Definition :=
      Jaml^_^'class-definition',
   Class_Name :=
      Class_Definition@'fully-qualified-name',
   Method_Name :=
      Class_Definition^_^'method-declaration'@name.


/* calls_cmm(Jaml, Class_Name, Method_Name_1, Method_Name_2) <-
      */

calls_cmm(Jaml, Class_Name, Method_Name_1, Method_Name_2) :-
   Class_Definition :=
      Jaml^_^'class-definition',
   Class_Name :=
      Class_Definition@'fully-qualified-name',
   Method_Declaration :=
      Class_Definition^_^'method-declaration',
   Method_Name_1 :=
      Method_Declaration@name,
   Method_Name_2 :=
      Method_Declaration^_^'method-invocation'@name.


/* jaml_to_pattern(Type, Jaml, Pattern) <-
      */

jaml_to_pattern(abstract_class, Jaml, Pattern) :-
   Class_Definition :=
      Jaml^_^'class-definition',
   Modifier :=
      Class_Definition^modifier,
   fn_item_parse(Modifier, _:_:[abstract]),
   Class_Name :=
      Class_Definition@'fully-qualified-name',
   Pattern = abstract_class:[
      name:Class_Name]:[].

jaml_to_pattern(composite, Jaml, Pattern) :-
   D := Jaml^_^'method-declaration',
   I := D^_^'for-statement'^block^_^'method-invocation',
   Method_Name := D@name,
   Method_Name := I@name,
   Type_Ref :=
      I^qualifier^'array-access-expression'@'type-ref',
   jaml_to_java(D),
   Pattern = composite:[
      method:Method_Name, type_ref:Type_Ref]:[].


/*** tests ********************************************************/


test(jaml, calls_cc) :-
   dislog_variable_get(jaml_home, Directory),
   concat(Directory, '/java/awt/Toolkit.xml', Path),
   dread(xml, Path, [Jaml]),
   calls_cc(Jaml, Calls_cc),
   findall( V-W,
      ( member(Call_cc, Calls_cc),
        [V, W] := Call_cc@[source, target] ),
      Edges ),
   edges_to_picture(Edges, _).


/******************************************************************/


