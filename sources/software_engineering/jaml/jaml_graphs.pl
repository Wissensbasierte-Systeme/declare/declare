

/******************************************************************/
/***                                                            ***/
/***       Jaml:  Graphs                                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* graph_edges_coarsen(Prefixes, Edges_1, Edges_2) <-
      */

graph_edges_coarsen(Prefixes, Edges_1, Edges_2) :-
   maplist( graph_edge_coarsen(Prefixes),
      Edges_1, Edges ),
   list_to_ord_set(Edges, Edges_2).

graph_edge_coarsen(Prefixes, V1-W1, V2-W2) :-
   graph_vertex_coarsen(Prefixes, V1, V2),
   graph_vertex_coarsen(Prefixes, W1, W2).

graph_vertex_coarsen(Prefixes, V1, V2) :-
   member(V2, Prefixes),
   concat(V2, _, V1),
   !.
graph_vertex_coarsen(_, V, V).


/******************************************************************/


