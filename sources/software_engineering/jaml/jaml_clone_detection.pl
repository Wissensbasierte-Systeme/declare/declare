

/******************************************************************/
/***                                                            ***/
/***       Jaml:  Clone Detection                               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* jaml_clone_miner <-
      */

jaml_clone_miner :-
   home_directory(Home),
   concat(Home, '/soft/Jaml/Jar/', Directory),
   concat(Directory, 'soaplinux.xml', Xml_File),
   concat(Directory, 'cloneDetection.jar', Jar_File),
%  concat('CloneMiner -prolog -nogui ', Xml_File, Command),
   concat(['java -Xmx1000M -jar ', Jar_File, ' ',
      '-prolog -nogui ', Xml_File], Command),
   writeln(user, Command),
   us(Command).


/******************************************************************/


