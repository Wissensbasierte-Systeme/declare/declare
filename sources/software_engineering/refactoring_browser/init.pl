

/******************************************************************/
/***                                                            ***/
/***           DDK:  Refactoring Browser - Init                 ***/
/***                                                            ***/
/******************************************************************/


/*
:- [ 'prolog_to_xml.pl',
     'prolog_in_xml_layout_transformation.pl',
     'refactoring_browser_classes.pl',
     'substitution_rules_application.pl',
     'xml_to_prolog.pl' ].
*/


/*** interface ****************************************************/


/* refactoring_browser_demo <-
      */

refactoring_browser_demo :-
   refactoring_browser,
   dislog_variable_get(home,
      '/projects/Text_Parser/Refactoring/', Dir),
   concat(Dir, 'Refactoring/example_DA.pl', F_1),
   concat(Dir, 'substitution_rules_DA.pl', F_2),
   send(@rb, load_file(view_left, F_1, 0)),
   send(@rb, load_file(view_code, F_2, 0)).


refactoring_browser_apply_substitution_rules(L) :-
   apply_substitution_rules_to_textbuffer(L).


/* prolog_to_xml_transformation_demo <-
      */

prolog_to_xml_transformation_demo :-
   dislog_variable_get(home, Home),
   concat([Home, '/projects/Text_Parser/',
      'prolog_in_xml_layout_conversion_demo.pl'], File),
   prolog_file_to_xml(File, Xml),
   R_Orig := Xml^rule,
   R_Mod := Xml^rule <->
      [^body^atom^formula^atom::[@predicate = writeln/_]],
   convert_layout_to_modified_rule(R_Orig, R_Mod, R_Conv),
   dwrite(xml, R_Orig),
   dwrite(xml, R_Mod),
   dwrite(xml, R_Conv),
   xml_rule_to_atom(R_Orig, A_1),
   writeln(A_1),
   nl,
   writeln('After transformation:'),
   xml_rule_to_atom(R_Conv, A_2),
   writeln(A_2).


meta(findall/3, [n, 1, n]).
meta(forall/2, [n, 1]).
meta(call/1, [1]).


/******************************************************************/


