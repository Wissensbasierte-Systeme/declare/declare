

/******************************************************************/
/***                                                            ***/
/***      Refactoring Browser:  Navigation Tree                ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Mueller

:- use_module(library('trace/browse')).

% prolog_source_structure class definition


/*** class definition *********************************************/


:- pce_begin_class(navigation_tree, prolog_source_structure,
      "Navigation tree of the refactoring browser").

variable(file_menu, popup, get, "File menu").
variable(predicate_menu, popup, get, "Predicate menu").
variable(xpce_class_menu, popup, get, "XPCE class menu").
variable(xpce_entity_menu, popup, get, "XPCE entity menu").

initialise(FB, Args:any ...) :->
   Msg =.. [initialise | Args],
   send_super(FB, Msg),
   RegEx = '.*',
   send(FB, slot, file_pattern, RegEx),
   Menus = [file_menu, predicate_menu,
      xpce_class_menu, xpce_entity_menu],
   forall( member(M, Menus),
      ( create_popup(M, P),
        send(FB, slot, M, P) ) ).


:- pce_group(popup).

popup(FB, Id:any, Popup:popup) :<-
   "Return popup from current node"::
   get(FB, node, Id, Node),
   get_popup(FB, Node, Popup).


get_popup(FB, Node, P) :-
   send(Node, instance_of, sb_prolog_file),
   get(FB, slot, file_menu, P).
get_popup(FB, Node, P) :-
   send(Node, instance_of, sb_predicate),
   get(FB, slot, predicate_menu, P).
get_popup(FB, Node, P) :-
   send(Node, instance_of, toc_xpce_class),
   get(FB, slot, xpce_class_menu, P).
get_popup(FB, Node, P) :-
   send(Node, instance_of, toc_xpce_entity),
   get(FB, slot, xpce_entity_menu, P).
get_popup(_FB, Node, P) :-
   send(Node, has_get_method, popup),
   get(Node, popup, P).


create_popup(file_menu, P) :-
   new(P, popup(file_menu)),
   send_list(P, append, [
      menu_item(open_in_left_editor,
         message(@rb, load_file,
            view_left, @arg1?identifier)),
      menu_item(open_in_right_editor,
         message(@rb, load_file,
            view_right, @arg1?identifier)),
      menu_item(open_in_code_editor,
         message(@rb, load_file,
            view_code, @arg1?identifier),
         end_group := @on),
      menu_item(consult, message(@arg1, consult)) ]).

create_popup(predicate_menu, P) :-
   new(P, popup(predicate_menu)),
   send_list(P, append, [
      menu_item(open_in_left_editor,
         message(@rb, load_file,
            view_left, @arg1?file_id, @arg1?line),
         condition := message(@arg1, has_source)),
      menu_item(open_in_right_editor,
         message(@rb, load_file,
            view_right, @arg1?file_id, @arg1?line),
         condition := message(@arg1, has_source)),
      menu_item(open_in_code_editor,
         message(@rb, load_file,
            view_code, @arg1?file_id, @arg1?line),
         condition := message(@arg1, has_source),
         end_group := @on),
      menu_item(spy, message(@arg1, spy),
         condition := message(@arg1, loaded)),
      menu_item(trace, message(@arg1, trace),
         condition := message(@arg1, loaded)),
      menu_item(manual, message(@arg1, manual),
         condition := message(@arg1, has_manual)) ]).

create_popup(xpce_class_menu, P) :-
   new(P, popup(xpce_class_menu)),
   send_list(P, append, [
      menu_item(open_in_left_editor,
         message(@rb, load_file,
            view_left, @arg1?file_id, @arg1?line),
         condition := message(@arg1, has_source)),
      menu_item(open_in_right_editor,
         message(@rb, load_file,
            view_right, @arg1?file_id, @arg1?line),
         condition := message(@arg1, has_source)),
      menu_item(open_in_code_editor,
         message(@rb, load_file,
            view_code, @arg1?file_id, @arg1?line),
         condition := message(@arg1, has_source),
         end_group := @on),
      menu_item(class_details,
         message(@arg1, class_details),
         condition := message(@arg1, loaded)),
      menu_item(class_hierarchy,
         message(@arg1, class_hierarchy),
         condition := message(@arg1, loaded)) ]).

create_popup(xpce_entity_menu, P) :-
   new(P, popup(xpce_entity_menu)),
   send_list(P, append, [
      menu_item(open_in_left_editor,
         message(@rb, load_file,
            view_left, @arg1?file_id, @arg1?line),
         condition := message(@arg1, has_source)),
      menu_item(open_in_right_editor,
         message(@rb, load_file,
            view_right, @arg1?file_id, @arg1?line),
         condition := message(@arg1, has_source)),
      menu_item(open_in_code_editor,
         message(@rb, load_file,
            view_code, @arg1?file_id, @arg1?line),
         condition := message(@arg1, has_source),
         end_group := @on),
      menu_item(spy, message(@arg1, spy),
         condition := message(@arg1, loaded)),
      menu_item(trace, message(@arg1, trace),
         condition := message(@arg1, loaded)) ]).

:- pce_end_class(navigation_tree).


/******************************************************************/


