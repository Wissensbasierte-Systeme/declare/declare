

/******************************************************************/
/***                                                            ***/
/***        Transform Term with Subterm Positions to Xml        ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Mueller

:- module( term_to_xml_transformation, [
      term_to_xml_structure/6 ] ).

:- use_module(basic_predicates).


/*** interface ****************************************************/


/* term_to_xml_structure(
         +Term, +Subterm_Positions, +Variable_Names,
         +Whitespaces, +Chars, -Xml) <-
      */
      
term_to_xml_structure(Term, Subterm_Positions,
      Variable_Names, Whitespaces, Chars, Xml) :-
   parse_subterm_positions(
      [Term], [Subterm_Positions], Variable_Names, Result),
   rule_to_head_and_body(Result, Result_Mod),
   extract_from_to_from_list(Result_Mod, From_To_List_Filtered),
   msort(From_To_List_Filtered, Sorted),
   number_list_elements(0, [0-0|Sorted], Numbered),
   extract_offset_positions(Numbered, Offset_Positions),
   generate_xml_structure(Result_Mod, Numbered, [Xml_temp]),
   generate_offsets(Offset_Positions, Whitespaces, Chars, Offsets),
   Xml := Xml_temp*[Offsets].


/* term_to_xml_structure(
         +Term, +Subterm_Positions, +Variable_Names, -Xml) <-
      Xml representation without offsets. */

term_to_xml_structure(
      Term, Subterm_Positions, Variable_Names, Xml) :-
   parse_subterm_positions(
      [Term], [Subterm_Positions], Variable_Names, Result),
   rule_to_head_and_body(Result, Result_Mod),
   generate_xml_structure(Result_Mod, [], [Xml]).
%  No Position_Numbers wanted? -> Numbered = []


/*** implementation ***********************************************/


/* parse_subterm_positions(
         +Terms, +Subterm_Positions, +Variable_Names, -Result) <-
      */

parse_subterm_positions([], [], _, []) :-
   !.
parse_subterm_positions([T|Ts], [P|Ps], Vs,
      [FFrom-FTo-term:[functor:Functor/Arity]:SubRest_Result|
       SubTermPos_Rest_Result]) :-
   P = term_position(_F, _T, FFrom, FTo, SubRest),
   functor(T, Functor, Arity),
   T =.. [Functor|SubTerm],
   parse_subterm_positions(SubTerm, SubRest, Vs, SubRest_Result),
   parse_subterm_positions(Ts, Ps, Vs, SubTermPos_Rest_Result).
parse_subterm_positions([T|Ts], [P|Ps], Vs,
      [FFrom-FTo-var:[name:Element]:[]|
       SubTermPos_Rest_Result]) :-
   var(T),
   P = FFrom-FTo,
   ( member( (Element=Temp), Vs),
     Temp == T
%    Temp is needed,
%    because member( (Element=T), Vs) would unify Term with Element
   ; Element = '_' ),
   parse_subterm_positions(Ts, Ps, Vs, SubTermPos_Rest_Result).
%  filter {} because of SWI Prolog bug
parse_subterm_positions([T|Ts], [_-_|Ps], Vs,
      [from-to-term:[functor:{}/0]:[]|SubTermPos_Rest_Result]) :-
   functor(T, {}, 0),
   parse_subterm_positions(Ts, Ps, Vs, SubTermPos_Rest_Result).
parse_subterm_positions([T|Ts], [P|Ps], Vs,
      [FFrom-FTo-term:[functor:Functor/Arity]:[]|
       SubTermPos_Rest_Result]) :-
   P = FFrom-FTo,
   functor(T, Functor, Arity),
   parse_subterm_positions(Ts, Ps, Vs, SubTermPos_Rest_Result).
parse_subterm_positions([T|Ts], [P|Ps], Vs,
      [from-to-term:[functor:'.'/2]:List_Structure|
       SubTermPos_Rest_Result]) :-
   P =.. [list_position, _FFrom, _FTo, Elms, Tail],
   parse_list_subterm_positions(T, Elms, Tail, Vs, List_Structure),
   parse_subterm_positions(Ts, Ps, Vs, SubTermPos_Rest_Result).
parse_subterm_positions([T|Ts], [P|Ps], Vs,
      [from-to-term:[functor:Functor/Arity]:Arg_Result|
       SubTermPos_Rest_Result]) :-
   P = brace_term_position(_FFrom, _FTo, Arg),    % Arg is no list!
   T =.. [Functor|SubTerm],
   functor(T, _, Arity),
   parse_subterm_positions(SubTerm, [Arg], Vs, Arg_Result),
   parse_subterm_positions(Ts, Ps, Vs, SubTermPos_Rest_Result).
parse_subterm_positions([T|Ts], [P|Ps], Vs,
      [FFrom_2-FTo_2-term:[string:String]:[]|
       SubTermPos_Rest_Result]) :-
   P = string_position(FFrom, FTo),
   FFrom_2 is FFrom + 1,
   FTo_2 is FTo - 1,
   name(String, T),
   parse_subterm_positions(Ts, Ps, Vs, SubTermPos_Rest_Result).


/* parse_list_subterm_positions(
         +Term, +Elements_Subterm_Positions, +Tail_Subterm_Position,
         +Vars, -Xml_Structure) <-
      Term: prolog-term containing a list
      Xml_Structure: List! */

parse_list_subterm_positions(Term, [], Tail, Vars, Tail_Structure) :-
   % Term is Xs in a list like [...|Xs]
   parse_subterm_positions([Term], [Tail], Vars, Tail_Structure).
parse_list_subterm_positions(Term,
      [Element|Elements], Tail, Vars, Result) :-
   Term =.. [., A, B], % if B == [] reached end of list
   parse_subterm_positions([A], [Element], Vars, A_Result),
   parse_list_subterm_positions(B, Elements, Tail, Vars, B_Result),
   append(A_Result, B_Result, Result).
parse_list_subterm_positions(Term, _Elements, _Tail, _Vars,
      [from-to-term:[functor:'[]'/0]:[]]) :-
%  if Term was an empty list (or reached end of list)
   nonvar(Term),
   Term =.. [[]].   % empty list
parse_list_subterm_positions(Term, Elements, Tail, Vars, _) :-
   write('An unexpected error occured '),
   writeln('in parse_list_subterm_positions'),
   writeln(Term-Elements-Tail-Vars),
   abort.


/* rule_to_head_and_body(+List_In, -List_Out) <-
      */

rule_to_head_and_body([From-To-_X:[functor:Functor/2]:Y],
      [From-To-rule:[operator:Functor]:Y_2]) :-
   xml_representation_rule_operator(Functor),
   [Rule_Head_1|Rule_Body_1] = Y,
   parse_head_and_body_root(atom, [Rule_Head_1], Rule_Head_2_New),
   parse_head_and_body_root(atom, Rule_Body_1, Rule_Body_2_New),
   Y_2 = [
      from-to-head:[]:Rule_Head_2_New,
      from-to-body:[]:Rule_Body_2_New].
rule_to_head_and_body([From-To-_X:[functor:Functor/1]:Y],
      [From-To-rule:[operator:Functor]:Y_2]) :-
   xml_representation_rule_operator(Functor),
   parse_head_and_body_root(atom, Y, Rule_Body),
   Y_2 = [from-to-head:[]:[], from-to-body:[]:Rule_Body].
rule_to_head_and_body([From-To-X:A:Y], [from-to-rule:[]:Z_2]) :-
%  in this case, rule shouldn't have a position,
%  because there's no operator
   parse_head_and_body_root(atom, [From-To-X:A:Y], Rule_Head),
   Z_2 = [from-to-head:[]:Rule_Head, from-to-body:[]:[]].
rule_to_head_and_body([], []).


/* parse_head_and_body(+Type, +List_In, -List_Out) <-
      */

parse_head_and_body_root(Type,
      [_From-_To-_X:[functor:(,)/_]:[X|Xs]], [X_2|Xs_2]) :-
   parse_head_and_body(Type, X, X_2),
   parse_head_and_body_root(Type, Xs, Xs_2).
parse_head_and_body_root(Type, [X], [X_2]) :-
   !,
   parse_head_and_body(Type, X, X_2).
parse_head_and_body_root(_, [], []).


parse_head_and_body(atom, Term, Result) :-
   replace_if_then_else(Term, Result).
parse_head_and_body(Type, From-To-term:[functor:(,)/2]:X,
      from-to-formula:[functor:and]:Y) :-
   parse_head_and_body_root(Type,
      [From-To-term:[functor:(,)/2]:X], Y).
%  evtl wieder Komma-Struktur
parse_head_and_body(Type, _From-_To-term:[functor:(;)/2]:[A, B],
      from-to-formula:[functor:or]:[A_2, B_2]) :-
   parse_head_and_body(Type, A, A_2),
   parse_head_and_body(Type, B, B_2).
parse_head_and_body(atom, From-To-term:[_:Functor/Arity]:Y,
      From_2-To_2-atom:[predicate:Functor_2]:Y_2) :-
   check_special_cases(
      atom, From-To, Functor/Arity, From_2-To_2, Functor_2),
   !,
   (  meta(Functor/Arity, Pos)
   -> parse_head_and_body_args(Pos, Y, Y_2)
   ;  maplist(parse_head_and_body(term), Y, Y_2) ).  % Y ist Liste
parse_head_and_body(term,
      From-To-term:[Type:Functor/Arity]:Y,
      From_2-To_2-term:[Type:Functor_2]:Y_2) :-
   check_special_cases(
      term, From-To, Functor/Arity, From_2-To_2, Functor_2),
   maplist(parse_head_and_body(term), Y, Y_2).
parse_head_and_body(_Type, From-To-X:A:[], From-To-X:A:[]).
%  variables: Evtl an Anfang und X durch var ersetzen ->
%  Geschwindigkeitstests durchführen
parse_head_and_body(Type, L, L) :-
%  If nothing else matches
   writeln('ERROR!!'),
   write('Type = '),writeln(Type),
   writeq(L), nl, nl,
   abort.


/* parse_head_and_body_args(+Ps, +Args, -Args_Out) <-
      */
      
parse_head_and_body_args([n|Pos], [Arg|Args], [Arg_1|Args_1]) :-
   parse_head_and_body(term, Arg, Arg_1),
   !,
   parse_head_and_body_args(Pos, Args, Args_1).
parse_head_and_body_args([_|Pos], [Arg|Args], [Arg_1|Args_1]) :-
   parse_head_and_body(atom, Arg, Arg_1),
   !,
   parse_head_and_body_args(Pos, Args, Args_1).
parse_head_and_body_args(_, [], []) :-
   !.


/* replace_if_then_else(+L_In, -L_Out) <-
      */

replace_if_then_else(_:As:Cs, T) :-
   member(functor:(;)/2, As),
   [_:As_2:[Cond, Then], Else] = Cs,
   member(functor:(->)/2, As_2),
   !,
   parse_head_and_body(atom, Cond, Cond_2),
   parse_head_and_body(atom, Then, Then_2),
   parse_head_and_body(atom, Else, Else_2),
   T = from-to-if:[]:[
      from-to-cond:[]:[Cond_2],
      from-to-then:[]:[Then_2],
      from-to-else:[]:[Else_2] ].


/* check_special_cases(
         +Type, +From-To, +Functor/Arity,
         -From_2-To_2, -Functor_2) <-
      */

check_special_cases(_, from-to, '.'/_Arity, from-to, '.') :-
   !.
check_special_cases(_, from-to, Functor/Arity,
      from-to, Functor/Arity) :-
   !.
check_special_cases(_Type, From-To, '.'/_, From_2-To_2, '.') :-
   (  1 is To - From
   -> From_2 = From,
      To_2 = To
   ;  From_2 is From + 1,
      To_2 is To - 1 ),
   !.
check_special_cases(Type, From-To, Functor/Arity,
      From_2-To_2, Functor_Out_2) :-
   term_to_atom(Functor, Functor_Atom),
%  Because we need ' for some atoms
   Len is To - From,
   atom_length(Functor_Atom, Len_2),
   ( Len == Len_2 ->
     From_2 = From,
     To_2 = To,
     Functor_Out = Functor_Atom
   ; ( Len > Len_2 ->
       From_2 is From + 1,
%      we have a case like this: write('atom')
       To_2 is To - 1,
%      ' aren't needed, but we don't want to lose them!
       Functor_Out = Functor
     ; From_2 is From - 1,
       To_2 is To + 1,
       Functor_Out = Functor ) ),
   ( Type == atom ->
     Functor_Out_2 = Functor/Arity
   ; Functor_Out_2 = Functor_Out ).


/* extract_from_to_from_list(List_In, From_To_Out) <-
      */
      
extract_from_to_from_list([from-to-_:_:Y|Z], R) :-
   !,
   extract_from_to_from_list(Y, Y_1),
   extract_from_to_from_list(Z, Z_1),
   append(Y_1, Z_1, R).
extract_from_to_from_list([From-To-_:_:Y|Z], [From-To|R]) :-
   extract_from_to_from_list(Y, Y_1),
   extract_from_to_from_list(Z, Z_1),
   append(Y_1, Z_1, R).
extract_from_to_from_list([], []).


/* number_list_elements(+Start_Nr, +List, -Numbered) <-
      */
      
number_list_elements(Nr, [X|Xs], [Nr:X|R]) :-
   Next_Nr is Nr + 1,
   number_list_elements(Next_Nr, Xs, R).
number_list_elements(_, [], []).


/* extract_offset_positions(+Numbered, -Offset_Positions) :-
      */
      
extract_offset_positions([_Nr_1:_From_1-To_1, Nr_2:From_2-To_2|R],
      [Nr_2:To_1-From_2_New|S]) :-
   From_2_New is From_2 - 1,
   To_1 =< From_2_New,
   extract_offset_positions([Nr_2:From_2-To_2|R], S).
extract_offset_positions(
      [_Nr_1:_From_1-_To_1, Nr_2:From_2-To_2|R], S) :-
   extract_offset_positions([Nr_2:From_2-To_2|R], S).
extract_offset_positions([Nr:_From-To], [Nr_New:To-end]) :-
   Nr_New is Nr + 1.


/* generate_xml_structure(Termlist, Numbered, Xml) <-
      */

generate_xml_structure([], _Numbered, []).
generate_xml_structure([From-To-X:A:Y|Z], Numbered,
      [X:A_New:Xml_SubTerm | Xml_RestTerm]) :-
   ( select(Nr:From-To, Numbered, Numbered_2) ->
     append(A, [position:Nr], A_New)
   ; A_New = A,
     Numbered_2 = Numbered ),
   generate_xml_structure(Y, Numbered_2, Xml_SubTerm),
   generate_xml_structure(Z, Numbered_2, Xml_RestTerm).


/* generate_offsets(
         Offset_Positions, Whitespaces, Chars, Offsets) <-
      */

generate_offsets(
      Offset_Positions, Whitespaces, Chars, Offsets) :-
   mask_newline(Whitespaces, WSs_Masked),
   name(WSs_without_nl, WSs_Masked),
   append(L, [Nr_last:From_last-end], Offset_Positions),
%  delete last element
   findall( O,
      ( member(Nr:From-To, L),
        get_elements_from_list(Chars, From, To, Result),
        mask_newline(Result, Result_Masked),
        name(Offset, Result_Masked),
        O = offset:[position:Nr, text:Offset]:[]),
      Os ),
   length(Chars, Length),
   get_elements_from_list(Chars, From_last, Length, Result_2),
   mask_newline(Result_2, Result_2_Masked),
   name(Offset_2, Result_2_Masked),
   append([ [offset:[position:0, text:WSs_without_nl]:[]], Os,
      [offset:[position:Nr_last, text:Offset_2]:[]] ], Temp),
   Offsets = offsets:[]:Temp.


/* xml_representation_rule_operator(+Op) <-
      */
      
xml_representation_rule_operator(Op) :-
   member(Op, [':-', '-->', ':->', ':<-', '--->']).


/******************************************************************/


