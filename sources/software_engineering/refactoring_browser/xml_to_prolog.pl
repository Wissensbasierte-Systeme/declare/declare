

/******************************************************************/
/***                                                            ***/
/***          Convert Xml to Prolog Files                       ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Mueller

:- module( xml_to_prolog_extended, [
      xml_file_to_prolog_file/2,
      xml_rule_to_atom/2,
      xml_rules_to_chars/2,
      xml_rules_to_prolog_file/2 ] ).

:- use_module(xml_to_prolog_implementation).


/*** interface ****************************************************/


/* xml_file_to_prolog_file(+Xml_File, -Prolog_File) <-
      */

xml_file_to_prolog_file(Xml_File, Prolog_File) :-
   xml_file_to_chars(Xml_File, Cs),
   write_chars_to_file(Prolog_File, Cs),
   !.


/* xml_file_to_chars(+Xml_File, -Prolog_Chars) <-
      */

xml_file_to_chars(Xml_File, Prolog_Chars) :-
   dread(xml, Xml_File, [Xml_to_FN]),
   Xml_Content := Xml_to_FN^content::'*',
   parse_xml_to_prolog(Xml_Content, Prolog_Chars).


/* xml_rule_to_atom(+Xml_Rule, +Atom) <-
      */

xml_rule_to_atom(Xml, A) :-
   xml_rule_to_chars(Xml, Cs),
   name(A, Cs),
   !.


/* xml_rules_to_prolog_file(+Xml, +File) <-
      */

xml_rules_to_prolog_file(Xml, F) :-
   Rs := Xml^content::'*',
   xml_rules_to_chars(Rs, Cs),
   write_chars_to_file(F, Cs).


/* xml_rules_to_chars(+Xml_Rules, -Chars) <-
      */
      
xml_rules_to_chars(Rs, Cs) :-
   parse_xml_to_prolog(Rs, Cs).


/*** implementation ***********************************************/


/* write_chars_to_file(+File, +Chars) <-
      */

write_chars_to_file(File, Cs) :-
   open(File, write, Stream, []),
   write_chars_to_stream(Stream, Cs),
   close(Stream).


/* write_chars_to_stream(+Stream, +Chars) <-
      */
      
write_chars_to_stream(Stream, [C|Cs]) :-
   put(Stream, C),
   !,
   write_chars_to_stream(Stream, Cs).
write_chars_to_stream(_, []).


/******************************************************************/


