

/******************************************************************/
/***                                                            ***/
/***       Convert Xml to Prolog                                ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Mueller

:- module( xml_to_prolog_implementation, [
      parse_xml_to_prolog/2,
      xml_rule_to_chars/2 ] ).

:- use_module(basic_predicates).
:- use_module(xml_to_prolog_without_offsets).


/*** interface ****************************************************/


/* parse_xml_to_prolog(+Xml, -Chars) <-
      */

parse_xml_to_prolog([X|Xs], Cs) :-
   xml_rule_to_chars(X, Cs_1),
   parse_xml_to_prolog(Xs, Cs_2),
   append(Cs_1, Cs_2, Cs).
parse_xml_to_prolog([], []).


/* xml_rule_to_chars(+Xml, -Chars) <-
      Converts Xml containing a rule or comment
      in Xml-representation to Chars containing the element,
      i.e. the Prolog term or the comment. */

xml_rule_to_chars(Rule, Element_Chars) :-
   rule := Rule^tag::'*',
   Offsets := Rule^offsets,
   !,
   Rule_2 := Rule<->[^offsets],
   create_predicate_list([Rule_2], Combined),
   keysort(Combined, Sorted),
   (  Sorted == []
   -> Point_Offset = 1
   ;  last(Sorted, Nr-_),
      Point_Offset is Nr + 1 ),
   append([[0-''], Sorted, [Point_Offset-'']], Sorted_2),
   parse_preds(Sorted_2, Offsets, Pred_Chars),
   mask_newline(Element_Chars, Pred_Chars).
xml_rule_to_chars(Comment, Element_Chars_nl) :-
   comment := Comment^tag::'*',
   Offset := Comment^offset@text,
   Content := Comment^content@text,
   atom_chars(Offset, Offset_Chars),
   atom_chars(Content, Content_Chars),
   append(Offset_Chars, Content_Chars, Element_Chars),
   mask_newline(Element_Chars_nl, Element_Chars).
xml_rule_to_chars(Rule, Element_Chars) :-
   rule := Rule^tag::'*',
   \+ (_ := Rule^offsets),
%  missing offsets
   !,
   xml_rule_without_offsets_to_chars(Rule, Element_Chars).


/*** implementation ***********************************************/


/* create_predicate_list(+List, -Flat_List) <-
      */
      
create_predicate_list([X|Y], [T-Pred|R]) :-
   member(Type, [functor, predicate, name, operator, string]),
   Pred := X@Type,
   Nr := X@position,
   Cs := X^content::'*',
   !,
   create_predicate_list(Cs, Cs_1),
   create_predicate_list(Y, Y_1),
   append(Cs_1, Y_1, R),
   term_to_atom_catch(T, Nr).
create_predicate_list([X|Y], R) :-
   Cs := X^content::'*',
%  ignore elements like head, body or unknown
   create_predicate_list(Cs, Cs_1),
   create_predicate_list(Y, Y_1),
   append(Cs_1, Y_1, R).
create_predicate_list([], []).


/* parse_preds(+Sorted, +Offsets, -Pred_Chars) <-
      */

parse_preds([Nr-Pred|Preds], Offsets, Cs) :-
   extract_pred_name(Pred, Pred_Name),
   !,
   atom_chars(Pred_Name, Cs_2),
   get_offset(Nr, Offsets, Cs_1),
   parse_preds(Preds, Offsets, Cs_3),
   append([Cs_1, Cs_2, Cs_3], Cs).
parse_preds([], _, []).



/* extract_pred_name(+Pred_Atom, -Pred_Name_Atom) <-
      */
extract_pred_name('', '') :-
   !.
extract_pred_name('.', '.') :-
   !.
extract_pred_name('\',\'', '\',\'') :-
   !.
%  (Pred_Name)/Arity or Pred_Name/Arity
extract_pred_name(Pred_Atom, Pred_Name_Atom) :-
   atom(Pred_Atom),
   term_to_atom_catch(Pred_Term, Pred_Atom),
   nonvar(Pred_Term),
   Pred_Name/_Arity = Pred_Term,
   term_to_atom_catch(Pred_Name, Pred_Name_Atom).
% (Pred_Name) or Pred_Name
extract_pred_name(Pred_Atom, Pred_Name_Atom) :-
   atom(Pred_Atom),
   term_to_atom_catch(Pred_Term, Pred_Atom),
   nonvar(Pred_Term),
   ( member(Pred_Term, [(,)]) ->
     Pred_Name_Atom = Pred_Term
   ; term_to_atom_catch(Pred_Term, Pred_Name_Atom) ).
extract_pred_name(Pred_Term, Pred_Name_Atom) :-
   compound(Pred_Term),
   Pred_Name/_Arity = Pred_Term,
   term_to_atom_catch(Pred_Name, Pred_Name_Atom).
extract_pred_name(Pred_Atom, Pred_Atom).


/* get_offset(+Nr, +Offsets, -Offset_Chars) <-
      */
      
get_offset(Nr, Offsets, Offset_Chars) :-
   term_to_atom_catch(Nr, Nr_Atom),
   ( Offset_Text :=
        Offsets^offset::[@position == Nr_Atom]@text ->
     true
   ; Offset_Text :=
        Offsets^offset::[@position == Nr]@text ),
   atom_chars(Offset_Text, Offset_Chars).
get_offset(_, _, []).


/* term_to_atom_catch(Term, Atom) <-
      */

term_to_atom_catch(Term, Atom) :-
   catch( term_to_atom(Term, Atom),
      _, fail ).


/******************************************************************/


