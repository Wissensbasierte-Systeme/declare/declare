

/******************************************************************/
/***                                                            ***/
/***        Refactoring Browser:  Basic predicates              ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Mueller

:- module( basic_predicates, [
      delete_pattern_from_list/3,
      insert_element_at_position/4,
      get_elements_from_list/4,
      mask_newline/2 ] ).


/*** interface ****************************************************/


/* delete_pattern_from_list(+L_In, +Pattern, -L_Out) :-
      Delete all Elements from L_In
      which can be unified with Pattern.
      If Pattern is an atom, then delete_pattern_from_list
      returns the same result as the built_in call
      delete(L_In, Pattern, L_Out).
      Example:
      delete_pattern_from_list(
        [a, a(b), b, a(c), c], a(_), L_Out)
      returns L_Out = [a, b, c]
      The built_in predicate delete couldn't do that. */

delete_pattern_from_list(L_In, Pattern, L_Out) :-
   sublist( \=(Pattern), L_In, L_Out).


/* insert_element_at_position(
         +Position, +Element, +L_In, -L_Out) <-
      Insert Element at Position into list L_In.
      If Position is greater than the length of L_In,
      then the Element will be added at the tail of L_In.
      Position count starts with 0. */

insert_element_at_position(P, E, L_In, L_Out) :-
   split(P, L_In, L_1, L_2),
   append(L_1, [E|L_2], L_Out).


/* get_elements_from_list(+List, +From, +To, -Result) <-
      Result is a list, containing the elements
     from From to To from List.
      Element count starts with 0. */

get_elements_from_list(List, From, To, Result) :-
   From =< To,
   !,
   split(From, List, _List_1, List_2),
   Len is To - From + 1,
   split(Len, List_2, Result, _List_3).
get_elements_from_list(_List, _From, _To, []).


/* mask_newline(?Chars, ?Chars_Masked) <-
      Mask all newlines ('\n') in a list of one-character atoms
      to avoid new lines in the Xml output. */

mask_newline(['\n'|Xs], ['\\', 'n'|Xs_2]) :-
   !,
   mask_newline(Xs, Xs_2).
mask_newline(['\\', 'n'|Xs], ['\\', '\\', 'n'|Xs_2]) :-
   !,
   mask_newline(Xs, Xs_2).
mask_newline([X|Xs], [X|Xs_2]) :-
   !,
   mask_newline(Xs, Xs_2).
mask_newline([], []).


/******************************************************************/


