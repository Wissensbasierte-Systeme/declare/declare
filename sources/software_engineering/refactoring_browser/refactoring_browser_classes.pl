

/******************************************************************/
/***                                                            ***/
/***           DDK:  Refactoring Browser                        ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Müller

:- use_module(xml_to_prolog_implementation).

:- consult(navigation_tree).


/*** interface ****************************************************/


/* refactoring_browser <-
      (Re-)start refactoring browser */

refactoring_browser :-
   free(@rb),
   new(@rb, refactoring_browser('.')),
   send(@rb, open).


/*** class definition *********************************************/


:- pce_begin_class(
      refactoring_browser, frame, "Prolog refactoring browser").

variable(editor_size, size := size(50, 10),
   both, "Size of left and right text-field").
variable(view_left, emacs_view, get, "Left emacs view").
variable(view_right, emacs_view, get, "Right emacs view").
variable(view_code, view, get, "Code view").

initialise(RB, D:name) :->
   send_super(RB, initialise, 'Prolog refactoring browser'),
   send(RB, icon, '16x16/handpoint.xpm'),
   send(RB, append(new(Dialog, dialog))),
   send(Dialog, append, new(menu_bar)),
   new(TB, tool_bar),
   send(Dialog, pen(0)),
   send(Dialog, gap(size(0, 5))),
   send(Dialog, append, TB),
   send_list(RB, [fill_tool_bar(TB), fill_menu]),
   get(RB, slot, editor_size, size(W, H)),
   create_empty_emacs_view(prolog, W, H, View_left),
   create_empty_emacs_view(prolog, W, H, View_right),
   create_empty_emacs_view(prolog, 5, 5, View_Code),
   send_list([View_left, View_right, View_Code],
      font, font(screen, roman, 10)),
   send(RB, slot, view_left, View_left),
   send(RB, slot, view_right, View_right),
   send(RB, slot, view_code, View_Code),
   send(View_right?editor, wrap, none),
   send(View_left?editor, wrap, none),
   new(Dir, directory(D)),
   new(Tree, navigation_tree(Dir)),
   send(View_left, left, View_right),
   send(View_Code, below, View_left),
   send(View_left, right, Tree),
   send(View_left, below, Dialog),
   send(new(report_dialog), below, View_Code),
   forall( member(V, [View_left, View_right, View_Code]),
      send_styles(V, [
         back_1-style(background := darkseagreen),
         back_2-style(background := burlywood) ] ) ).


menu_bar(RB, Menu_Bar:menu_bar) :<-
   "Get the menu_bar"::
   get(RB, member, dialog, D),
   get(D, member, menu_bar, Menu_Bar).


fill_menu(RB) :->
   "Fill the menu"::
   get(RB, menu_bar, Menu_Bar),
   send_list(Menu_Bar, append, [
      new(MB_File, popup(file)),
      new(MB_Edit, popup(mode)),
      new(MB_Refac, popup(refactoring)),
      new(MB_Help, popup(help)) ]),
   new(File_Left, popup('Left Editor')),
   send_file_actions(RB, File_Left, view_left),
   new(File_Right, popup('Right Editor')),
   send_file_actions(RB, File_Right, view_right),
   new(File_Code, popup('Code Editor')),
   send_file_actions(RB, File_Code, view_code),
   send_list(MB_File, append, [
      menu_item('Change Root Directory',
         message(RB, change_root_directory_dialog)),
      File_Left, File_Right, File_Code,
      menu_item('Exit', message(RB, destroy)) ]),
   new(Mode_Left, popup('Left Editor')),
   send_mode_actions(RB, Mode_Left, view_left),
   new(Mode_Right, popup('Right Editor')),
   send_mode_actions(RB, Mode_Right, view_right),
   new(Mode_Code, popup('Code Editor')),
   send_mode_actions(RB, Mode_Code, view_code),
   send_list(MB_Edit, append, [
      Mode_Left, Mode_Right, Mode_Code ]),
   send_list(MB_Refac, append, [
      menu_item('Start refactoring',
         message(RB, start_refactoring)) ]),
   send(MB_Help, append,
      menu_item('About', message(RB, about_box))).


send_file_actions(RB, Menu, View) :-
   send_list(Menu, append, [
      menu_item('New File', message(RB, new_file, View)),
      menu_item('Open File', message(RB, open_file_dialog, View)),
      menu_item('Save File', message(RB, save_file, View)),
      menu_item('Save File As ...',
         message(RB, save_file_as_dialog, View)) ]).


send_mode_actions(RB, Menu, View) :-
   send_list(Menu, append, [
      menu_item('Text Mode', message(RB, mode, View, fundamental)),
      menu_item('Prolog Mode', message(RB, mode, View, prolog)),
      menu_item('Xml Mode', message(RB, mode, View, xml)) ]).


tool_bar(RB, TB:tool_bar) :<-
   "Get the toolbar"::
   get(RB, member, dialog, D),
   get(D, member, tool_bar, TB).


fill_tool_bar(RB, TB:tool_bar) :->
   "Fill the toolbar"::
   new(I, image('16x16/fatleft_arrow.xpm')),
   get(I, rotate, 270, Up),
   get(I, rotate, 90, Down),
   send_list(TB, append, [
      tool_button(message(RB, change_root_directory_dialog),
         '16x16/copy.xpm', 'Change Root Directory (Source Tree)'),
      gap,
      tool_button(message(RB, new_file, view_left),
         '16x16/doc.xpm', 'New File (Left Editor)'),
      tool_button(message(RB, open_file_dialog, view_left),
         '16x16/open.xpm', 'Open File (Left Editor)'),
      tool_button(message(RB, save_file, view_left),
         '16x16/save.xpm', 'Save File (Left Editor)'),
      tool_button(message(RB, save_file_as_dialog, view_left),
         '16x16/saveall.xpm', 'Save File As ... (Left Editor)'),
      gap,
      tool_button(message(RB, new_file, view_code),
         '16x16/doc.xpm', 'New File (Code Editor)'),
      tool_button(message(RB, open_file_dialog, view_code),
         '16x16/open.xpm', 'Open File (Code Editor)'),
      tool_button(message(RB, save_file, view_code),
         '16x16/save.xpm', 'Save File (Code Editor)'),
      tool_button(message(RB, save_file_as_dialog, view_code),
         '16x16/saveall.xpm', 'Save File As ... (Code Editor)'),
      gap,
      tool_button(message(RB, new_file, view_right),
         '16x16/doc.xpm', 'New File (Right Editor)'),
      tool_button(message(RB, open_file_dialog, view_right),
         '16x16/open.xpm', 'Open File (Right Editor)'),
      tool_button(message(RB, save_file, view_right),
         '16x16/save.xpm', 'Save File (Right Editor)'),
      tool_button(message(RB, save_file_as_dialog, view_right),
         '16x16/saveall.xpm', 'Save File As ... (Right Editor)'),
      gap,
      tool_button(message(RB, scroll_editors, scroll_one_line_down),
         Up, 'Scroll Editors up'),
      tool_button(message(RB, scroll_editors, scroll_one_line_up),
         Down, 'Scroll Editors down'),
      gap,
      tool_button(message(RB, left_editor_to_xml),
         '16x16/ok.xpm', 'Show Xml representation (L->R)'),
      gap,
      tool_button(message(RB, start_refactoring),
         '16x16/handpoint.xpm', 'Start refactoring (left)') ]).


send_styles(V, [Name-Style|Ys]) :-
   send(V, style, Name, Style),
   send_styles(V, Ys).
send_styles(_, []).


/* scroll_editors(Direction) <-
      Direction: scroll_one_line_down, scroll_one_line_up */

scroll_editors(RB, D) :->
   get(RB, slot, view_left, V_1),
   get(RB, slot, view_right, V_2),
   send_list([V_1?editor, V_2?editor], D).


about_box(_) :->
   new(D, dialog('About Refactoring Browser')),
   prolog_flag(encoding, Encoding),
   set_prolog_flag(encoding, utf8),
   concat([
      'Dieses Programm ist Teil der Diplomarbeit\n',
      '"Refactoring von Source Code ',
      'mittels logischer Programmierung"\n',
      'Lehrstuhl für Informationsstrukturen ',
      'und wissensbasierte Systeme (Informatik I)\n',
      'Julius-Maximilians-Universität Würzburg\n',
      'Autor: Michael Müller, 2005/2006\n'], A),
   set_prolog_flag(encoding, Encoding),
   send_list(D, append, [
      new(_, label(about, A)),
      button(ok, and(
         message(D, destroy) )) ]),
   send(D, open).


change_root_directory_dialog(RB) :->
   new(D, dialog('Enter root directory')),
   send_list(D, append, [
      new(T, text_item(root_directory)),
      button(ok, and(
         message(RB, change_root_directory, T?selection),
         message(D, destroy) )),
      button(cancel, message(D, destroy)) ]),
   send(D, open).


change_root_directory(_RB, Path:name) :->
   get(@rb, member, navigation_tree, FB),
   new(Dir, directory(Path)),
   new(R, toc_directory(Dir, path)),
   send(FB, root, R, @on),
   send(R, update).


unlink(RB) :->
   get(RB, get_emacs_buffer(view_left), EB_1),
   get(RB, get_emacs_buffer(view_right), EB_2),
   get(RB, get_emacs_buffer(view_code), EB_3),
   send_super(RB, unlink),
   send_list([EB_1, EB_2, EB_3], free).


create_empty_emacs_view(Mode, Width, Height, View) :-
   new(F, file),
   new(EB, emacs_buffer(F)),
   send(EB, auto_save_mode, @off),
   ( Mode == @default ->
     send(EB, mode(fundamental))
   ; send(EB, mode(Mode)) ),
   new(E, emacs_editor(EB)),
   new(View, emacs_view(E, Width, Height)).
%  mit Editor statt Buffer wird Frame-Label nicht aktualisiert


/* mode(RB, Side:name, Mode:name) <-
      Mode: fundamental, prolog, xml */

mode(RB, Side:name, Mode:name) :->
   get(RB, slot, Side, V),
   send(V?editor?text_buffer, mode(Mode)).


replace_emacs_buffer(RB, EB:emacs_buffer, Side:name) :->
   get(RB, slot, Side, V),
   get(V?editor, text_buffer, Old),
   send(V?editor, text_buffer(EB)),
   (
%    EB \== Old,
     get(Old, editors, C),
     get(C, size, 0) ->
%    if no further editors are attached to the buffer ->
%    delete (save???)
     send(Old, free)
   ; true ).


new_file(RB, Side:name) :->
   new(F, file),
   new(EB, emacs_buffer(F)),
   send(RB, replace_emacs_buffer(EB, Side)).


get_emacs_buffer(RB, Side:name, EB:emacs_buffer) :<-
   get(RB, slot, Side, V),
   get(V?editor, text_buffer, EB).


open_file_dialog(RB, Side:name) :->
   get(@finder, file(open, *), File),
   send(RB, load_file(Side, File, 0)).


save_file_as_dialog(RB, Side:name) :->
   get(@finder, file(save, *), File),
   send(RB, save_file_as(Side, File)).


load_file(RB, Side:name, File:name, Line:[int]) :->
   writeln(load:File),
   new(EB, emacs_buffer(File)),
   send(EB, auto_save_mode, @off),
   get(RB, get_emacs_buffer(Side), EB_Old),
   get(EB_Old, mode, M),
   send(EB, mode(M)),
   send(RB, replace_emacs_buffer(EB, Side)),
   ( integer(Line) ->
     get(RB, slot, Side, View),
     send(View?editor, line_number(Line))
   ; true ).


save_file(RB, Side:name) :->
   writeln(save_file),
   get(RB, slot, Side, View),
   ( send(View?editor?text_buffer, save) ->
     true
   ; send(RB, save_file_as_dialog(Side)) ).


save_file_as(RB, Side:name, File:name) :->
   writeln(save_file_as:File),
   get(RB, slot, Side, View),
   send(View?editor?text_buffer, save(File)).


start_refactoring(RB) :->
   dislog_variable_get(output_path,
      'refactoring_browser_substitution_rules.pl', File),
   get(RB, slot, view_code, View),
   send(View, save(File)),
   consult(File).


left_editor_to_xml(RB) :->
   dislog_variable_get(output_path,
      'refactoring_browser_tmp_xml.xml', Xml_File),
   get(RB, slot, view_left, View_1),
   get(View_1, text_buffer, TB),
   textbuffer_to_xml(TB, Xml),
   dwrite(xml, Xml_File, file:[path:'left_editor']:Xml),
   send(RB, load_file, view_right, Xml_File).

:- pce_end_class(refactoring_browser).


/******************************************************************/


