

/******************************************************************/
/***                                                            ***/
/***      DCGs for the prolog_in_xml layout transformation      ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Mueller

:- module( prolog_in_xml_layout_transformation_dcgs, [
      split_offset_characters/3 ] ).


/*** interface ****************************************************/


/* split_offset_characters([Type:A|As], +Cs_1, -Cs_2) <-
      Split the list of one character atoms in Cs_1
      into a list of code:Atom and space:Atom pairs.
      space: the corresponding Atom can stay between
         source code elements (i.e. whitespaces and comments
      code: the corresponding Atom affects
         the syntax of the rule */
      
split_offset_characters([Type:A|As]) -->
   code_or_space(Type:Cs),
   { atom_chars(A, Cs) },
   split_offset_characters(As),
   !.
split_offset_characters([]) -->
   [].


/*** implementation ***********************************************/


code_or_space(X) -->
   ( ( spaces(Cs),
       { X = space:Cs } )
   ; ( [C],
       { Cs = [C],
         X = code:Cs } ) ),
   { Cs \== [] }.


spaces([C|Cs]) -->
   [C],
   { char_type(C, space) },
   spaces(Cs).
spaces(Cs) -->
   comment(Cs_1),
   spaces(Cs_2),
   { append(Cs_1, Cs_2, Cs) }.
spaces([]) -->
   [].


comment(Cs) -->
   ( line_comment(Cs)
   ; block_comment(Cs) ).


line_comment(['%'|Cs]) -->
   ['%'],
   chars_until_end_of_line(Cs).
   

block_comment(['/', '*'|Cs]) -->
   ['/', '*'],
   block_comment_until_end(Cs).


block_comment_until_end(['*', '/']) -->
   ['*', '/'].
block_comment_until_end(Cs) -->
   block_comment(Cs_1),
%  embedded comments!
   block_comment_until_end(Cs_2),
   { append(Cs_1, Cs_2, Cs) }.
block_comment_until_end([C|Cs]) -->
   [C],
   block_comment_until_end(Cs).


chars_until_end_of_line(['\n']) -->
   ['\n'].
chars_until_end_of_line([C|Cs]) -->
   [C],
   chars_until_end_of_line(Cs).


/******************************************************************/


