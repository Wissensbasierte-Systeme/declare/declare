

/******************************************************************/
/***                                                            ***/
/***            Convert Rule Layout to modified Rule            ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Mueller

:- module(prolog_in_xml_layout_transformation, [
      convert_layout_to_modified_rule/3,
      convert_layout_to_modified_term/4 ]).

:- use_module(basic_predicates).
:- use_module(prolog_in_xml_layout_transformation_dcgs).
:- use_module(xml_to_prolog_without_offsets).


/*** interface ****************************************************/


/* convert_layout_to_modified_rule(
         +Rule_Orig, +Rule_Mod, -Rule_Conv) <-
      Transform the offset layout info from Rule_Orig
      into the offsets of Rule_Mod.
      All arguments contain the Xml representation
      of a Prolog rule. */

convert_layout_to_modified_rule(R_Orig, R_Mod, R_Conv) :-
   xml_rule_to_term(R_Mod, T, VNs),
   term_to_xml(T, R_Mod_2, VNs),
   transform_layout_to_modified_rule_with_correct_offsets(
      R_Orig, R_Mod_2, R_Conv).


/* convert_layout_to_modified_term(
         R_Orig, T_Mod, Variable_Names, R_Conv) <-
      */

convert_layout_to_modified_term(R_Orig, T_Mod, VNs, R_Conv) :-
   term_to_xml(T_Mod, R_Mod, VNs),
   transform_layout_to_modified_rule_with_correct_offsets(
      R_Orig, R_Mod, R_Conv).


/*** implementation ***********************************************/


/* transform_layout_to_modified_rule_with_correct_offsets(
         R_Orig, R_Mod, R_Conv) <-
      Transform the layout of R_Orig into the offsets of R_Mod
      and return the result in R_Conv.
      R_Orig and R_Mod both must contain consistent offsets. */
      
transform_layout_to_modified_rule_with_correct_offsets(
      R_Orig, R_Mod, R_Conv) :-
   extract_tags_with_positions(R_Orig, L_Orig),
   extract_tags_with_positions(R_Mod, L_Mod),
   Offsets_Orig := R_Orig^offsets,
   Offsets_Mod := R_Mod^offsets,
   combine_and_split_offsets(Offsets_Orig, Offsets_Orig_2),
   combine_and_split_offsets(Offsets_Mod, Offsets_Mod_2),
   last(Offsets_Orig_2, position:N_1),
   last(Offsets_Mod_2, position:N_2),
   append(L_Orig, [point:[position:N_1]], L_Orig_2),
   append(L_Mod, [point:[position:N_2]], L_Mod_2),
   convert_offsets(L_Mod_2, L_Orig_2, Offsets_Orig_2,
      Offsets_Mod_2, Offsets_Conv),
   offset_list_to_offsets(Offsets_Conv, '', [], Offsets_Conv_2),
   R_Mod_2 := R_Mod <-> [^offsets],
   R_Offset := R_Orig^offsets^offset::[@position = 0],
%  rule offset
   R_Conv := R_Mod_2 <+> [^offsets:[R_Offset | Offsets_Conv_2]],
   !.


/* extract_tags_with_positions(+Rule, -Tags_with_Positions) <-
      */

extract_tags_with_positions(Rule, L) :-
   H := Rule^head^content::'*',
   B := Rule^body^content::'*',
   extract_tags_with_positions_2(H, H_1),
   extract_tags_with_positions_2(B, B_1),
   (  P := Rule@position
   -> X = [rule_operator:[position:P]]
   ;  X = [] ),
   append([H_1, B_1, X], L).


extract_tags_with_positions_2([X|Xs], Zs) :-
   (  _ := X@position
   -> T := X^tag::'*',
      As := X^attributes::'*',
      Zs = [T:As|Ys]
   ;  Zs = Ys ),
   Cs := X^content::'*',
   extract_tags_with_positions_2(Cs, Ys_1),
   extract_tags_with_positions_2(Xs, Ys_2),
   append(Ys_1, Ys_2, Ys).
extract_tags_with_positions_2([], []).


/* combine_and_split_offsets(+Offsets_In, -Offsets_Split_List) <-
      */

combine_and_split_offsets(Offsets, Xs) :-
   findall(P,
      P := Offsets^offset@position,
      Ps),
   sort(Ps, Ps_2),
   last(Ps_2, N),
   numlist(1, N, Ns),
   combine_and_split_offsets_2(Ns, Offsets, Xs).


combine_and_split_offsets_2([N|Ns], Offsets, Xs) :-
   (  T := Offsets^offset::[@position == N]@text
   -> ( atom_chars(T, Cs),
        mask_newline(Cs_2, Cs),
        split_offset_characters(Ys, Cs_2, []),
        append(Ys, [position:N], Xs_1) )
   ;  Xs_1 = [position:N] ),
   combine_and_split_offsets_2(Ns, Offsets, Xs_2),
   append(Xs_1, Xs_2, Xs).
combine_and_split_offsets_2([], _, []).


/* convert_offsets(+L_Mod, +L_Orig,
         +Offsets_Orig, +Offsets_Mod, -Offsets_Conv) <-
      */

convert_offsets([T:As|Xs], L_1, Offs_Orig, Offs_Mod, Offs_Conv) :-
   select(position:P_Mod, As, As_2),
   append(As_2, [position:P_Orig], As_3),
   select(T:As_3, L_1, L_2),
   convert_offsets_2(
      P_Mod, P_Orig, Offs_Orig, Offs_Mod, Offs_Mod_2),
   convert_offsets(Xs, L_2, Offs_Orig, Offs_Mod_2, Offs_Conv).
convert_offsets([_|Xs], L_1, Offs_Orig, Offs_Mod, Offs_Conv) :-
   convert_offsets(Xs, L_1, Offs_Orig, Offs_Mod, Offs_Conv).
convert_offsets([], _, _, Offs_Mod, Offs_Mod).


convert_offsets_2(P_Mod, P_Orig, Offs_Orig, Offs_Mod, L_Conv_2) :-
   append(L_1, [position:P_Orig|_], Offs_Orig),
   append(L_2, [position:P_Mod|L_Mod_2], Offs_Mod),
   reverse(L_1, L_1_reverse),
   reverse(L_2, L_2_reverse),
   convert_offsets_3(L_1_reverse, L_2_reverse, L_Conv),
   reverse(L_Conv, L_Conv_reverse),
   append(L_Conv_reverse, [position:P_Mod|L_Mod_2], L_Conv_2).


convert_offsets_3([code:X|Xs], [space:_Y|Ys], Z) :-
   convert_offsets_3([code:X|Xs], Ys, Z).
convert_offsets_3([code:X|Xs], [code:X|Ys], [code:X|Z]) :-
   convert_offsets_3(Xs, Ys, Z).
convert_offsets_3([space:X|Xs], [code:Y|Ys], [space:X|Z]) :-
   convert_offsets_3(Xs, [code:Y|Ys], Z).
convert_offsets_3([space:X|Xs], [space:_Y|Ys], [space:X|Z]) :-
   convert_offsets_3(Xs, Ys, Z).
convert_offsets_3([space:X|_Xs],
      [position:Y|Ys], [space:X, position:Y|Ys]).
convert_offsets_3(_Xs, Ys, Ys).


/* offset_list_to_offsets(
         +Offset_List, +Atom, +Offsets_In, -Offsets_Out) <-
      */

offset_list_to_offsets(
      [position:N|Xs], A, Offsets_In, Offsets_Out) :-
   atom_chars(A, Cs),
   mask_newline(Cs, Cs_2),
   atom_chars(A_2, Cs_2),
   !,
   ( A_2 \== '' ->
     append(Offsets_In,
        [offset:[position:N, text:A_2]:[]], Offsets_2)
   ; Offsets_2 = Offsets_In ),
   offset_list_to_offsets(Xs, '', Offsets_2, Offsets_Out).
offset_list_to_offsets(
      [_:A_2|Xs], A_1, Offsets_In, Offsets_Out) :-
   concat_atom([A_1, A_2], A_3),
   offset_list_to_offsets(Xs, A_3, Offsets_In, Offsets_Out).
offset_list_to_offsets([], _, Offsets, Offsets).


/******************************************************************/


