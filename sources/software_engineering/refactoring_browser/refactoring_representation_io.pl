

/******************************************************************/
/***                                                            ***/
/***   Input and output tools for refactoring representation    ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Mueller

:- module( refactoring_representation_io, [
      read_file_to_comments_and_terms/2,
      textbuffer_to_comments_and_terms/2,
      create_diff_view/3 ] ).

:- use_module(read_source_code_from_stream).


/*** interface ****************************************************/


/* read_file_to_comments_and_terms(+File, -CommentsAndTerms) <-
      */
      
read_file_to_comments_and_terms(F, Ts) :-
   open(F, read, S, []),
   read_stream_to_comments_and_terms(S, Ts),
   close(S).


/* textbuffer_to_comments_and_terms(
         +Text_Buffer, -CommentsAndTerms) <-
      */

textbuffer_to_comments_and_terms(TB, Ts) :-
   pce_open(TB, read, S),
   read_stream_to_comments_and_terms(S, Ts),
   close(S).


/* create_diff_view(
         +Text_Buffer_1, +Text_Buffer_2, +Refac_Result) <-
      Create a difference view of the result
      of the application of some substitution rules
      given in the list Refac_Result. */
      
create_diff_view(TB_1, TB_2, [term_with_chars(_T, Cs, _Vs)|Ts]) :-
   new(S, string(Cs)),
   send_list([TB_1, TB_2], append(S)),
   create_diff_view(TB_1, TB_2, Ts).
create_diff_view(TB_1, TB_2, [comment(Cs)|Ts]) :-
   new(S, string(Cs)),
   send_list([TB_1, TB_2], append(S)),
   create_diff_view(TB_1, TB_2, Ts).
create_diff_view(TB_1, TB_2,
      [term_with_chars_refac(_, Cs_1, _, Cs_2, _Vs)|Ts]) :-
   get(TB_1, length, Len_1a),
   get(TB_2, length, Len_2a),
   new(S_1, string(Cs_1)),
   new(S_2, string(Cs_2)),
   send(TB_1, append(S_1)),
   send(TB_2, append(S_2)),
   append_newlines_to_textbuffer(TB_1, TB_2),
   get(TB_1, length, Len_1b),
   get(TB_2, length, Len_2b),
   X_1 is Len_1b - Len_1a,
   X_2 is Len_2b - Len_2a,
   new(_, fragment(TB_1, Len_1a, X_1, back_1)),
   new(_, fragment(TB_2, Len_2a, X_2, back_2)),
   create_diff_view(TB_1, TB_2, Ts).
create_diff_view(_, _, []).


/*** implementation ***********************************************/


/* read_stream_to_comments_and_terms(
         +Stream, -CommentsAndTerms) <-
      Read comments and terms from stream into CommentsAndTerms. */
      
read_stream_to_comments_and_terms(Stream, Ys) :-
   read_whitespaces(Stream, Ws),
   ( read_comment(Stream, Cs_Comment) ->
     Cs_Comment \== [],
     T = comment(Cs_Comment)
   ; read_term_extended(Stream, Term, Cs_Term, _, Var_Names),
     T = term_with_chars(Term, Cs_Term, Var_Names) ),
   !,
   read_stream_to_comments_and_terms(Stream, Ts),
   ( Ws == [] ->
     Ys = [T|Ts]
   ; Ys = [comment(Ws), T|Ts] ).
read_stream_to_comments_and_terms(_, []) :-
   !.


/* append_newlines_to_textbuffer(+Text_Buffer_1, +Text_Buffer_2) <-
      Assimilate the number of lines of both text buffers
      by appending newlines at the end of the shorter one. */
      
append_newlines_to_textbuffer(TB_1, TB_2) :-
   get(TB_1, line_number, L_1),
   get(TB_2, line_number, L_2),
   ( L_1 < L_2 ->
     X is L_2 - L_1,
     send(TB_1, append('\n', X))
   ; X is L_1 - L_2,
     send(TB_2, append('\n', X)) ).
append_newlines_to_textbuffer(_, _).


/******************************************************************/


