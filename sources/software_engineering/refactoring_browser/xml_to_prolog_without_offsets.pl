

/******************************************************************/
/***                                                            ***/
/***           Convert Xml to Prolog without offsets            ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Mueller

:- module(xml_to_prolog_without_offsets, [
      xml_rule_to_term/3,
      xml_rule_without_offsets_to_chars/2 ]).

      
/*** interface ****************************************************/


/* xml_rule_to_term(+Rule, -Term, -Variable_Names) <-
      */
      
xml_rule_to_term(Rule, T, Var_Names) :-
   Head := Rule^head^content::'*',
   Body := Rule^body^content::'*',
   !,
   insert_commas(Head, Head_with_Commas),
   insert_commas(Body, Body_with_Commas),
   !,
%  war vorher nicht
   xml_term_to_prolog_term(
      Head_with_Commas, Head_Struct, [], Var_Names_1),
   xml_term_to_prolog_term(
      Body_with_Commas, Body_Struct, Var_Names_1, Var_Names),
   ( Operator := Rule@operator ->
     ( Head_Struct \== [] ->
%      we have an operator
       Head_Term =.. Head_Struct,
       Body_Term =.. Body_Struct,
%      if we have an rule-operator, body can't be empty
       T =.. [Operator, Head_Term, Body_Term]
     ; Body_Term =.. Body_Struct,
       T =.. [Operator, Body_Term] )
   ; ( Head_Struct \== [] ->
%      we have no operator
       T =.. Head_Struct ) ).



/* xml_rule_without_offsets_to_chars(+Rule, -Chars) <-
      */

xml_rule_without_offsets_to_chars(Rule, Element_Chars) :-
   xml_rule_to_term(Rule, Term, Var_Names),
   maplist(assign_variable_names, Var_Names),
   swritef(S, '%t', [Term]),
   writeln(S),
   string_to_list(S, Element_Chars).


/*** implementation ***********************************************/


/* xml_term_to_prolog_term(
         +Xml, +Terms, +Variable_Names_In, -Variable_Names_Out) <-
      */
      
xml_term_to_prolog_term([X|Xs],
      [Prolog_Term | Prolog_Term_Rest],
      Var_Names_In, Var_Names_Out) :-
   member(Type, [predicate, functor, name, operator]),
   Pred := X@Type,
   T := X^tag::'*',
   Cs := X^content::'*',
   ( T == var ->
     ( member( (Pred=Temp), Var_Names_In) ->
       Pred_Term = Temp
     ; V_Names_1 = [Pred=Pred_Term] )
   ; delete_arity(Pred, Pred_Term),
     V_Names_1 = [] ),
   append(Var_Names_In, V_Names_1, V_Names_2),
   xml_term_to_prolog_term(
      Cs, Prolog_Term_Nested, V_Names_2, V_Names_3),
   Prolog_Term =.. [Pred_Term | Prolog_Term_Nested],
   xml_term_to_prolog_term(
      Xs, Prolog_Term_Rest, V_Names_3, Var_Names_Out),
   !.
xml_term_to_prolog_term([], [], Vs, Vs).


/* insert_commas(+Xml_In, -Xml_Out) <-
      */

insert_commas([X], X_2) :-
   !,
   replace_structs([X], X_2).
insert_commas([X|Xs], [term:[functor:'(,)/2']:R]) :-
   replace_structs([X], X_2),
   insert_commas(Xs, Xs_2),
   append(X_2, Xs_2, R).
insert_commas([], []).


/* insert_points(+Xml_In, -Xml_Out) <-
      */
      
insert_points([X], X_2) :-
   !,
   replace_structs([X], X_2).
insert_points([X|Xs], [term:[functor:'.']:R]) :-
   replace_structs([X], X_2),
   insert_points(Xs, Xs_2),
   append(X_2, Xs_2, R).
insert_points([], []).


/* replace_structs(+Xml_In, -Xml_Out) <-
      Xml_In contains constructs like if-then-else, and, or. */
      
replace_structs([], []).
replace_structs([X|Xs], [atom:[predicate:'(;)/2']:Cs_2|Xs_2]) :-
   formula := X^tag::'*',              % or-formulas
   or := X@functor,
   Cs := X^content::'*',
   replace_structs(Cs, Cs_2),
   replace_structs(Xs, Xs_2).
replace_structs([X|Xs], Result) :-
   formula := X^tag::'*',              % and-formulas
   and := X@functor,
   Cs := X^content::'*',
   insert_commas(Cs, Cs_2),
   replace_structs(Xs, Xs_2),
   append(Cs_2, Xs_2, Result).
replace_structs([X|Xs], Result) :-
   if := X^tag::'*',                   % if-then-else constructs
   Cond := X^cond^content::'*',
   Then := X^then^content::'*',
   Else := X^else^content::'*',
   replace_structs(Cond, Cond_2),
   replace_structs(Then, Then_2),
   replace_structs(Else, Else_2),
   append(Cond_2, Then_2, Cond_Then),
   append([term:[functor:'(->)/2']:Cond_Then],
      Else_2, Cond_Then_Else),
   If := term:[functor:'(;)/2']:Cond_Then_Else,
   replace_structs(Xs, Xs_2),
   append([If], Xs_2, Result).
replace_structs([X|Xs], [T:[N:'.']:[C|Cs_2] | Xs_2]) :-
   member(N, [predicate, functor]),
   P := X@N,
   ( atom(P) ->
     atom_prefix(P, '.')
   ; P = '.'/_ ),
   T = X^tag::'*',
   [C|Cs] := X^content::'*',
   insert_points(Cs, Cs_2),
   replace_structs(Xs, Xs_2).
replace_structs([X|Xs], [Tag:Atts:Content_2 | Xs_2]) :-
   Tag := X^tag::'*',
   Atts := X^attributes::'*',
   Content := X^content::'*',
   replace_structs(Content, Content_2),
   replace_structs(Xs, Xs_2).
replace_structs(X, X).


/* delete_arity(+Pred, -Functor) <-
      If Pred is a variable, Functor must remain unbound!!!*/

delete_arity('.', '.').
delete_arity(Pred, Functor) :-
   atomic(Pred),
   term_to_atom(Functor/_, Pred),
   !.
delete_arity(Pred, Functor) :-
   atomic(Pred),
   term_to_atom(Functor, Pred),
   !.
delete_arity(Functor/_, Functor) :-
%  because of ',' that are already terms
   !.
delete_arity(Functor, Functor) :-
%  because of ',' that are already terms
   !.


/* assign_variable_names(+Name=Variable) <-
      */
      
assign_variable_names(N=V) :-
   V = '$VAR'(N).


/******************************************************************/


