

/******************************************************************/
/***                                                            ***/
/***   Read Source Code (i.e. Terms and Comments) from Stream   ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Mueller

:- module( read_source_code_from_stream, [
      read_comment/2,
      read_term_extended/5,
      read_whitespaces/2 ] ).


/*** interface ****************************************************/


/* read_term_extended(+Stream, -Term, -Term_Chars,
         -Subterm_Positions, -Variable_Names) <-
      Read a term from Stream and unify the term with Term.
      Chars contains the corresponding characters
      as read from the stream (including whitespaces and comments).
      Subterm_Positions describes the detailed layout of the term
      as defined in the SWI-Prolog read_term documentation.
      Variable_Names is a list of 'Name'=Var pairs,
      where Name is an atom describing the variable name
      and Var is a variable that shares with the corresponding
      variable in Term. */

read_term_extended(S, T, Cs_2, Ps, VNs) :-
   read_term_characters(S, Cs),
   check_and_expand_term_characters(S, Cs, Cs_2, T, [
      syntax_errors(error), variable_names(VNs),
      subterm_positions(Ps)]).


/* read_whitespaces(+Stream, -Whitespaces) <-
      Read Whitespaces from Stream. */

read_whitespaces(S, []) :-
   at_end_of_stream(S),
   !.
read_whitespaces(S, [C|Cs]) :-
   peek_char(S, C),
   char_type(C, space),
   get_char(S, C),
   !,
   read_whitespaces(S, Cs).
read_whitespaces(_, []).


/* read_comment(+Stream, -Chars) :-
      Read comment characters from Stream. */
      
read_comment(S, []) :-
   at_end_of_stream(S),
   !.
read_comment(S, [C|Cs]) :-
   peek_char(S, '%'),
   get_char(S, C),
   !,
   read_to_end_of_comment(line_comment, S, Cs).
read_comment(S, [C_1, C_2|Cs]) :-
   peek_char(S, '/'),
   get_char(S, C_1),
   peek_char(S, '*'),
   get_char(S, C_2),
   !,
   read_to_end_of_comment(block_comment, S, Cs).


/*** implementation ***********************************************/


/* check_and_expand_term_characters(
         +Stream, +Chars, -Chars_Expanded, -Term, +Options) <-
      Test, if Chars contains a legal Term. If not, expand it. */
      
check_and_expand_term_characters(_, Cs, Cs, T, Os) :-
   name(A, Cs),
   pce_open(A, read, S),    % use A as stream
   catch(read_term(S, T, Os), _Catcher, (close(S), fail) ),
   close(S),
   !.
check_and_expand_term_characters(S, Cs, Cs_3, T, Os) :-
   read_term_characters(S, Cs_Exp),  % expand chars to next
   append(Cs, Cs_Exp, Cs_2),         % end of term symbol ('.')
   check_and_expand_term_characters(S, Cs_2, Cs_3, T, Os).


/* read_term_characters(+Stream, -Chars) <-
      */
      
read_term_characters(S, [C|Cs]) :-
   \+ at_end_of_stream(S),
   get_char(S, C),
   !,
   ( C == '.' ->
     Cs = []
   ; read_term_characters(S, Cs) ).


/* read_to_end_of_comment(+Type, +Stream, -Chars) :-
      Read characters from Stream until the end of the comment
      is reached.  Type defines the type of comment.
      This predicate can also deal with embedded comments,
      i.e. one comment within another comment. */

read_to_end_of_comment(line_comment, S, [C|Cs]) :-
   get_char(S, C),
   ( char_type(C, newline) ->
     Cs = []
   ; read_to_end_of_comment(line_comment, S, Cs) ).
read_to_end_of_comment(block_comment, S, Cs) :-
%  embedded comments
   peek_char(S, '/'),
   get_char(S, C_1),
   (  peek_char(S, '*')
   -> get_char(S, C_2),
      read_to_end_of_comment(block_comment, S, Cs_1),
      Cs_2 = [C_1, C_2|Cs_1]
   ;  Cs_2 = [C_1] ),
   read_to_end_of_comment(block_comment, S, Cs_3),
   append(Cs_2, Cs_3, Cs).
read_to_end_of_comment(block_comment, S, [C_1|Cs]) :-
   get_char(S, C_1),
   ( C_1 == '*' ->
     peek_char(S, C_2),
     ( C_2 == '/' ->
       get_char(S, _),
       Cs = [C_2]
     ; read_to_end_of_comment(block_comment, S, Cs) )
   ; read_to_end_of_comment(block_comment, S, Cs) ).


/******************************************************************/


