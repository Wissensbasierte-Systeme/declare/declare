

/******************************************************************/
/***                                                            ***/
/***    Transformation: Rule <-> Refactoring Representation     ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Mueller

:- module( rule_transformation, [
      rule_to_refactoring_representation/4,
      root_to_refactoring_representation/4,
      refactoring_representation_to_rule/2,
      refactoring_representation_to_term/2 ] ).


/*** interface ****************************************************/


/* rule_to_refactoring_representation(
         +Type, +Rule, +Options, -Refac_Rep) <-
      Type: rule, pattern
      If Type == pattern, then variables will remain variable.
      Thus they can be unified with other
      refactoring_representation terms.
      If Type == rule, then variables will be replaced
      by var(...) terms. */

rule_to_refactoring_representation(
      Type, Rule, Options, Refac_Rep) :-
   ( member(variable_names(Variable_Names), Options) ->
     true
   ; Variable_Names = [] ),
   ( member(predicate_variables(Predvar_Names), Options) ->
     true
   ; Predvar_Names = [] ),
   rule_to_refactoring_representation_2(
      Type, Rule, Predvar_Names, Variable_Names, Refac_Rep).


/* root_to_refactoring_representation(
         +Type, +Term, +Options, -Refac_Rep) <-
      */

root_to_refactoring_representation(Type, Term, Options, L) :-
   ( member(variable_names(Variable_Names), Options) ->
     true
   ; Variable_Names = [] ),
   ( member(predicate_variables(Predvar_Names), Options) ->
     true
   ; Predvar_Names = [] ),
   root_to_refactoring_representation_2(
      Type, Term, Predvar_Names, Variable_Names, L).


/* refactoring_representation_to_rule(+Refac_Rep, -Rule) <-
      */

refactoring_representation_to_rule(refac_rep(none, H, []), R) :-
   !,
   refactoring_representation_list_to_term(H, H_1),
   R =.. [H_1].
refactoring_representation_to_rule(refac_rep(Op, [], B), R) :-
   !,
   refactoring_representation_list_to_term(B, B_1),
   R =.. [Op, B_1].
refactoring_representation_to_rule(refac_rep(Op, H, B), R) :-
   refactoring_representation_list_to_term(H, H_1),
   refactoring_representation_list_to_term(B, B_1),
   R =.. [Op, H_1, B_1].


/* refactoring_representation_to_term(
         +Refac_Rep, +Variable_Names, -Term) <-
      */

refactoring_representation_to_term(V, V) :-
   var(V),
   !.
refactoring_representation_to_term(var(V, _), V).
refactoring_representation_to_term(atom(F, _, Args), T) :-
   maplist(refactoring_representation_to_term, Args, Args_1),
   T =.. [F|Args_1].
refactoring_representation_to_term(term(F, _, Args), T) :-
   maplist(refactoring_representation_to_term, Args, Args_1),
   T =.. [F|Args_1].


/*** implementation ***********************************************/


/* rule_to_refactoring_representation_2(
         +Type, +Term, +Predvar_Names, +Variable_Names,
         -Refac_Rep) <-
      */

rule_to_refactoring_representation_2(_, R, _, _, _) :-
   var(R),
   !,
   fail.
rule_to_refactoring_representation_2(
      Type, R, Ps, Vs, refac_rep(Op, H_1, B_1)) :-
   R =.. [Op, H, B],                                % rules
   member(Op, [':-', '-->', ':->', ':<-']),
   !,
   root_to_refactoring_representation_2(Type, H, Ps, Vs, H_1),
   root_to_refactoring_representation_2(Type, B, Ps, Vs, B_1).
rule_to_refactoring_representation_2(
      Type, R, Ps, Vs, refac_rep(':-', [], B_1)) :-
   R =.. [':-', B],                                 % directives
   !,
   root_to_refactoring_representation_2(Type, B, Ps, Vs, B_1).
rule_to_refactoring_representation_2(
      Type, R, Ps, Vs, refac_rep(none, H, [])) :-
%  facts
   root_to_refactoring_representation_2(Type, R, Ps, Vs, H).


/* root_to_refactoring_representation_2(
         +Type, +Term, +Predvar_Names, +Variable_Names,
         -Refac_Rep) <-
      */

root_to_refactoring_representation_2(Type, Term, _, Vs, [X]) :-
   var(Term),
   variable_to_refactoring_representation(Type, Term, Vs, X),
   !.
root_to_refactoring_representation_2(
      Type, Term, Ps, Vs, [X_1|Xs_1]) :-
   Term =.. [',', X, Xs],
   !,
   atom_to_refactoring_representation(Type, X, Ps, Vs, X_1),
   root_to_refactoring_representation_2(Type, Xs, Ps, Vs, Xs_1).
root_to_refactoring_representation_2(Type, Term, Ps, Vs, [X]) :-
   atom_to_refactoring_representation(Type, Term, Ps, Vs, X),
   !.


/* atom_to_refactoring_representation(
         +Type, +Term, +Predicate_Variables, +Variable_Names,
         -Refac_Rep) <-
      */
      
atom_to_refactoring_representation(Type, Term, _, Vs, X) :-
   var(Term),
   variable_to_refactoring_representation(Type, Term, Vs, X),
   !.
atom_to_refactoring_representation(
      Type, Term, Ps, Vs, atom(F_2, A, Args_1)) :-
   functor(Term, F, A),
   Term =.. [F|Args],
   (  meta(F/A, Pos)
   -> arguments_to_refactoring_representation(
         Type, Args, Ps, Vs, Pos, Args_1)
   ;  maplist( term_to_refactoring_representation(Type, Ps, Vs),
         Args, Args_1 ) ),
   (  member(F=F_2, Ps)
   -> true
   ;  F_2 = F ).


/* term_to_refactoring_representation(
         +Type, +Predicate_Variables, +Variable_Names, +Term,
         -Refac_Rep) <-
      */
      
term_to_refactoring_representation(Type, _Ps, Vs, Term, X) :-
   var(Term),
   variable_to_refactoring_representation(Type, Term, Vs, X),
   !.
term_to_refactoring_representation(
      Type, Ps, Vs, Term, term(F_2, A, Args_1)) :-
   functor(Term, F, A),
   Term =.. [_|Args],
   !,
   maplist( term_to_refactoring_representation(Type, Ps, Vs),
      Args, Args_1 ),
   (  member(F=F_2, Ps)
   -> true
   ;  F_2 = F ).


/* variable_to_refactoring_representation(
         +Type, +Variable, +Names, -Refac_Rep) <-
      */
      
variable_to_refactoring_representation(
      rule, V, Names, var(V, Name)) :-
   ( member( (Name=Temp), Names),
      Temp == V ->  % Temp is needed,
                    % because member( (Name=V), Names)
                    % would unify V with Name
     true
   ; term_to_atom(V, Name) ).
variable_to_refactoring_representation(pattern, V, _, V).



/* arguments_to_refactoring_representation(
         +Type, +Args, +Predicate_Variables,
         +Variable_Names, +Pos_Infos, -Refac_Rep) <-
      */
      
arguments_to_refactoring_representation(
      Type, [Arg|Args], Ps, Vs, [n|Pos], [Arg_1|Args_1]) :-
   term_to_refactoring_representation(Type, Ps, Vs, Arg, Arg_1),
   !,
   arguments_to_refactoring_representation(
      Type, Args, Ps, Vs, Pos, Args_1).
arguments_to_refactoring_representation(
      Type, [Arg|Args], Ps, Vs, [_|Pos], [Arg_1|Args_1]) :-
   atom_to_refactoring_representation(Type, Arg, Ps, Vs, Arg_1),
   !,
   arguments_to_refactoring_representation(
      Type, Args, Ps, Vs, Pos, Args_1).
arguments_to_refactoring_representation(
      _Type, [], _, _, [], []) :-
   !.


/* refactoring_representation_list_to_term(
         +Refactoring_Represenation, -Term) <-
      */
      
refactoring_representation_list_to_term(Xs, T) :-
   maplist(refactoring_representation_to_term, Xs, Ts),
   list_to_comma_structure(Ts, T).


/******************************************************************/


