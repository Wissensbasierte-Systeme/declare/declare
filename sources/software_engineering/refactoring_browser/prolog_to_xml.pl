

/******************************************************************/
/***                                                            ***/
/***       Prolog to XML: Interface                             ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Mueller

:- module( prolog_to_xml, [
      prolog_file_to_xml/2,
      term_to_xml/3,
      term_with_chars_to_xml/4,
      textbuffer_to_xml/2 ] ).

:- use_module(basic_predicates).
:- use_module(read_source_code_from_stream).
:- use_module(term_to_xml_transformation).


/*** interface ****************************************************/


/* prolog_file_to_xml(+Prolog_File, -Xml) <-
      */
      
prolog_file_to_xml(File, file:[path:File]:Xml) :-
   open(File, read, Stream, [eof_action(eof_code)]),
   read_sourcecode_to_xml_with_offsets(Stream, Xml),
   close(Stream),
   !.


/* textbuffer_to_xml(+Text_Buffer, -Xml) <-
      */

textbuffer_to_xml(TB, Xml) :-
   pce_open(TB, read, Stream),
   read_sourcecode_to_xml_with_offsets(Stream, Xml),
   close(Stream),
   !.


/* term_to_xml(+Term, -Xml, +Variable_Names) <-
      Variable_Names: List of 'Name'=Var pairs,
      where Name is an atom describing the name of the variable
      and Var is a variable that shares with the corresponding
      variable in Term. */
         
term_to_xml(T, Xml, VNs) :-
   term_to_atom(T, A),
   term_variables(T, Vs),       % important to unify the variables!
   concat(A, '.', A_2),         % Term misses point '.'
   pce_open(A_2, read, Stream), % open atom as stream
   read_term(Stream, _, [subterm_positions(Ps),
      variables(Vs), variable_names(VNs_2)]),
   close(Stream),
   name(A_2, Chars),
   append(VNs, VNs_2, VNs_3),
   term_to_xml_structure(T, Ps, VNs_3, [], Chars, Xml),
   !.


/* term_with_chars_to_xml(Term, Cs, Xml, Variable_Names) <-
      */

term_with_chars_to_xml(Term, Cs, Xml, Variable_Names) :-
   term_variables(Term, Vars),
   atom_chars(Atom, Cs),
   pce_open(Atom, read, Stream),   % use Atom as stream
   read_term(Stream, _,
      [subterm_positions(Ps), variables(Variables)]),
   close(Stream),
   maplist(=, Variables, Vars),
   name(Atom, Chars),
   term_to_xml_structure(Term, Ps, Variable_Names, [], Chars, Xml),
   !.


/*** implementation ***********************************************/


/* read_sourcecode_to_xml_with_offsets(+Stream, -Xml) <-
      */
      
read_sourcecode_to_xml_with_offsets(Stream, [Xml|Xmls]) :-
   read_sourcecode_element_to_xml_with_offsets(Stream, Xml),
   read_sourcecode_to_xml_with_offsets(Stream, Xmls).
read_sourcecode_to_xml_with_offsets(_, []).


/* read_sourcecode_element_to_xml_with_offsets(+Stream, -Xml) <-
      */
      
read_sourcecode_element_to_xml_with_offsets(Stream, Xml) :-
   read_whitespaces(Stream, WSs),      % also true if end_of_file
   ( read_comment(Stream, Cs) ->       % also true if end_of_file
     WSs-Cs \== []-[],                 % Thus each Xml representation ends with a comment
     mask_newline(WSs, WSs_2),         % that holds the whitespaces to the end of the file
     name(A_1, WSs_2),
     mask_newline(Cs, Cs_2),
     name(A_2, Cs_2),
     Xml = comment:[]:[offset:[text:A_1]:[], content:[text:A_2]:[]]
   ; read_term_extended(Stream, T, Cs, Ps, VNs),
     term_to_xml_structure(T, Ps, VNs, WSs, Cs, Xml) ).


/******************************************************************/


