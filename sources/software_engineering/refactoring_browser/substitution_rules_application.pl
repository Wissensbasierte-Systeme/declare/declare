

/******************************************************************/
/***                                                            ***/
/***                  Apply Substitution Rules                  ***/
/***                                                            ***/
/******************************************************************/


% Author: Michael Mueller

:- module(substitution_rules_application, [
      apply_substitution_rules_to_textbuffer/1,
      substitute_pattern/6 ]).

:- use_module(refactoring_representation_io).
:- use_module(rule_transformation).


/*** interface ****************************************************/


/* apply_substitution_rules_to_textbuffer(+Substitution_Rules) <-
      Substitution_Rules: Names of the substitution rules. */
      
apply_substitution_rules_to_textbuffer(SRs) :-
   get(@rb, get_emacs_buffer, view_left, EB),
   textbuffer_to_comments_and_terms(EB, Ts),
   maplist(apply_substitution_rules(SRs), Ts, Ts_2),
   send_list(@rb, new_file, [view_left, view_right]),
   get(@rb, get_emacs_buffer, view_left, EB_1),
   get(@rb, get_emacs_buffer, view_right, EB_2),
   send_list([EB_1, EB_2], mode(prolog)),
   create_diff_view(EB_1, EB_2, Ts_2).


/* substitute_pattern(+Search_Term, +Subst_Term,
         +Condition, +Options, +Rule_In, -Rule_Out) <-
      Search for Search_Term in Rule_In and replace it
      with Subst_Term if it fulfills the Condition.
      Options:
         ignore_rule_operator
            Ignore the rule operator of the Search_Term.
         predicate_variables(L):
            L is a list of pred=Var pairs.
            Such a pair will replace the predicate pred
            with the variable Var in the refactoring
            representation.
         variable_names(L):
            L is a list of 'Name'=Var pairs,
            defining the name 'Name' for the variable Var. */
      
substitute_pattern(Search_Term, Subst_Term,
      Condition, Options, Rules_In, Rules_Out) :-
   parse_term(search, Search_Term, Options, Search_Term_2),
%  nach Refac_Rep wandeln
   substitute_pattern_2(init,
      substitution_terms(
         Search_Term_2, Subst_Term, Condition, Options),
      Rules_In, Rules_Out).


/*** implementation ***********************************************/


/* apply_substitution_rules(
         +Substitution_Rules, +Rule_In, -Rule_Out) <-
      Substitution_Rules: Names of the substitution rules. */
      
apply_substitution_rules([SR|SRs], Rule_In, Rule_Out_2) :-
   findall_substitution_rules(SR, Bodies),
   apply_substitution_rule(Bodies, Rule_In, Rule_Out),
   !,
   apply_substitution_rules(SRs, Rule_Out, Rule_Out_2).
apply_substitution_rules([], Rule, Rule) :-
   !.



/* findall_substitution_rules(+Substitution_Rules, -Bodies) <-
      Bodies contains the bodies of the substitution rules
      together with the variables for the difference list. */
      
findall_substitution_rules(SR, Bodies) :-
   findall(R_In-R_Out-Body,
      clause(substitution_rule(SR, R_In, R_Out), Body),
      Bodies).



/* apply_substitution_rule(
         +Substitution_Rules, +Rule_In, -Rule_Out) <-
      Substitution_Rules:
         List of substitution rule bodies with the same name. */

apply_substitution_rule([], R, R) :-
   !.
apply_substitution_rule(_, comment(Cs), comment(Cs)) :-
   !.
apply_substitution_rule([Rule_In-Rule_Out-Body|Bs],
      term_with_chars(T, Cs, Vs), R_Out) :-
   rule_to_refactoring_representation(rule, T,
      [variable_names(Vs)], Refac_Rep),
   Rule_In = term_with_refac_rep(T, Cs, Refac_Rep, Vs),
   call(Body),
   apply_substitution_rule(Bs, Rule_Out, R_Out).
apply_substitution_rule([Rule_In-Rule_Out-Body|Bs],
      term_with_chars_refac(
         T_Orig, Cs_Orig, T_Refac, Cs_Refac, Vs),
      R_Out_2) :-
   rule_to_refactoring_representation(
      rule, T_Refac, [variable_names(Vs)], Refac_Rep),
   Rule_In = term_with_refac_rep(T_Refac, Cs_Refac, Refac_Rep, Vs),
   call(Body),
   apply_substitution_rule(Bs, Rule_Out, R_Out),
   R_Out = term_with_chars_refac(_, _, T_Refac_2, Cs_Refac_2, Vs_2),
   R_Out_2 = term_with_chars_refac(
      T_Orig, Cs_Orig, T_Refac_2, Cs_Refac_2, Vs_2).
apply_substitution_rule(_SR, R, R).



/* substitute_pattern_2(
         +Mode, +Substitution_Terms, +Rule_In, -Rule_Out) <-
      */
      
substitute_pattern_2(Mode,
      substitution_terms(
         Search_Term, Subst_Term, Condition, Options),
      Rule_In, Rule_Out) :-
   copy_term(
      substitution_terms(Search_Term,
         Subst_Term, Condition, Options),
      substitution_terms(Search_Term_Copy,
         Subst_Term_Copy, Condition_Copy, Options_Copy)),
   copy_term(Search_Term_Copy, Search_Term_Copy_2),
%  Vs_1 contains variables that will be bound with Prolog-Terms
   term_variables(Search_Term_Copy, Vs_1),
%  Vs_2 contains the corresponding refactoring representations
   term_variables(Search_Term_Copy_2, Vs_2),
   maplist(assign_terms, Vs_1, Vs_2, L),
   copy_term(
      substitution_terms(Search_Term, Subst_Term, Condition, Options),
      Copies),
   substitute_pattern_3(Mode, Search_Term_Copy_2,
      substitution_terms(
         Search_Term_Copy, Subst_Term_Copy, Condition_Copy),
      L, Copies, Options_Copy, Rule_In, Rule_Out).


substitute_pattern_3(init, Search_Places,
      Substitution_Terms, L, Copies, Options,
      term_with_chars_refac(T, Cs, T_Refac, Cs_Refac, Vs),
      Rule_Out_2) :-
   rule_to_refactoring_representation(
      rule, T_Refac, [variable_names(Vs)], Refac_Rep),
   !,
   substitute_pattern_3(init, Search_Places, Substitution_Terms,
      L, Copies, Options,
      term_with_refac_rep(T_Refac, Cs_Refac, Refac_Rep, Vs),
      Rule_Out),
   term_with_chars_refac(_, _, T_Refac_2, Cs_Refac_2, _) = Rule_Out,
   Rule_Out_2 = term_with_chars_refac(
      T, Cs, T_Refac_2, Cs_Refac_2, Vs).
substitute_pattern_3(init, Search_Places,
      substitution_terms(_Search_Term, Subst_Term, Cond),
      L, Copies, Options, Rule_In, Rule_Out) :-
   Rule_In = term_with_refac_rep(T, Cs, Refac_Rep_In, Vs),
   replace_sublist_with_condition_check(
      init, Search_Places, Cond, Subst_Term,
      L, Copies, Options, Refac_Rep_In, Refac_Rep_Out),
   refactoring_representation_to_rule(Refac_Rep_Out, T_Refac),
   ( member(variable_names(VNs), Options) ->
     append(Vs, VNs, Vs_2)
   ; Vs_2 = Vs ),
   layout_conversion(T, Cs, T_Refac, Vs_2, Cs_Refac),
   Rule_Out = term_with_chars_refac(T, Cs, T_Refac, Cs_Refac, Vs_2).
substitute_pattern_3(recursive, Search_Places,
      substitution_terms(_Search_Term, Subst_Term, Cond),
      L, Copies, Options, Rule_In, Rule_Out) :-
   replace_sublist_with_condition_check(recursive, Search_Places, Cond,
      Subst_Term, L, Copies, Options, Rule_In, Rule_Out).


/* replace_sublist_with_condition_check(
      +Mode, +Search, +Cond, +Subst_Term, +L, +Copies,
      +Options, +Rule_In (in Refac_Rep),
      +Rule_Out (in Refac_Rep)) <- */
      
replace_sublist_with_condition_check(_,
      search_term(Op_S, do_nothing, Bs_S), Cond, T_Subst, L,
      Copies, Options,
      refac_rep(Op, Hs, Bs), refac_rep(Op, Hs_Out, Bs_Out)) :-
   ( Op_S == ignore ->
     true
     ; Op_S == Op ),
   split_list_at_sublist(Bs, Bs_S, Xs, Zs),
   check_condition(Cond, L),
   parse_term(subst, T_Subst, Options,
      subst_term(_Op_Sub, Hs_Sub, Bs_Sub)),
   no_singleton_variable(Op_2),
   ( Hs_Sub == do_nothing ->
     substitute_pattern_2(recursive, Copies,
        refac_rep(Op, Hs, Zs), refac_rep(Op_2, Hs_2, Zs_2))
   ; replace_sublist_with_condition_check(recursive,
     search_term(Op_S, do_nothing, Bs_S),
     Cond, T_Subst, L, Copies, Options,
     refac_rep(Op, Hs, Zs), refac_rep(Op_2, Hs_2, Zs_2)) ),
   replace_search_pattern_by_substitution(
      Hs_Sub, [], Hs_2, [], Hs_Out),
   replace_search_pattern_by_substitution(
      Bs_Sub, Xs, Bs_S, Zs_2, Bs_Out).
replace_sublist_with_condition_check(
      _, search_term(Op_S, Hs_S, do_nothing), Cond, T_Subst, L,
      _, Options, refac_rep(Op, Hs, Bs),
      refac_rep(Op, Hs_Out, Bs_Out)) :-
   ( Op_S == ignore ->
     true
   ; ( Op_S == Op ->
       true
     ; Op == none,
       Op_S == (:-) ) ),     % facts
   split_list_at_sublist(Hs, Hs_S, Xs, Zs),
   check_condition(Cond, L),
   parse_term(subst, T_Subst, Options,
      subst_term(_Op_Sub, Hs_Sub, Bs_Sub)),
   replace_search_pattern_by_substitution(
      Hs_Sub, Xs, Hs_S, Zs, Hs_Out),
   ( Op == none ->
     Bs_Out = Bs
   ; replace_search_pattern_by_substitution(
        Bs_Sub, [], Bs, [], Bs_Out) ).
replace_sublist_with_condition_check(
      _, search_term(Op_S, Hs_S, Bs_S), Cond, T_Subst, L,
      Copies, Options,
      refac_rep(Op, Hs, Bs), refac_rep(Op, Hs_Out, Bs_Out)) :-
   no_singleton_variable(Op),
   Hs_S \== do_nothing,
   Bs_S \== do_nothing,
   ( Op_S == ignore ->
     true
   ; Op_S == Op ),
   split_list_at_sublist(Hs, Hs_S, H_Xs, H_Zs),
   split_list_at_sublist(Bs, Bs_S, Xs, Zs),
   check_condition(Cond, L),
   parse_term(subst, T_Subst, Options,
      subst_term(_Op_Sub, Hs_Sub, Bs_Sub)),
   no_singleton_variable(Op_2),
   ( Hs_Sub == do_nothing ->
     substitute_pattern_2(recursive, Copies,
       refac_rep(Op, H_Zs, Zs), refac_rep(Op_2, Hs_2, Zs_2))
   ; replace_sublist_with_condition_check(
        recursive, search_term(Op_S, do_nothing, Bs_S),
        Cond, T_Subst, L, Copies, Options,
        refac_rep(Op, H_Zs, Zs),
        refac_rep(Op_2, Hs_2, Zs_2)) ),
   replace_search_pattern_by_substitution(
      Hs_Sub, H_Xs, Hs_S, Hs_2, Hs_Out),
   replace_search_pattern_by_substitution(
      Bs_Sub, Xs, Bs_S, Zs_2, Bs_Out).
replace_sublist_with_condition_check(
     recursive, _, _, _, _, _, _, Refac_Rep, Refac_Rep).


/* replace_search_pattern_by_substitution(
         +Subst, +Before, +Search_Pattern, +After, -L) <-
      */
      
replace_search_pattern_by_substitution(do_nothing, Xs, Ys, Zs, L) :-
   append([Xs, Ys, Zs], L).
replace_search_pattern_by_substitution(delete_pattern, Xs, _, Zs, L) :-
   append([Xs, Zs], L).
replace_search_pattern_by_substitution(Subst, Xs, _, Zs, L) :-
   append([Xs, Subst, Zs], L).


/* split_list_at_sublist(+List, +Sublist, -Before, -After) <-
      */
      
split_list_at_sublist(L, Xs, As, Bs) :-
   copy_term(Xs, Xs_Copy),
   sublist(Xs_Copy, L),
   !,
   append(As, Zs, L),
   append(Xs, Bs, Zs).


/* parse_term(+Mode, +Term, +Options, -Out) <-
      Mode: search, subst
      Out: search_term(Op, H, B), subst_term(Op, H, B) */
      
parse_term(_M, T, _Options, var) :-
   var(T),
   !,
   fail.
parse_term(M, T, Os, Out) :-
   T =.. [Op, H, B],                                  % rules
   member(Op, [':-', '-->', ':->', ':<-']),
   !,
   parse_term_2(M, Op-H-B, Os, Out).
parse_term(M, T, Os, Out) :-
   T =.. [(:-), B],                                   % directives
   !,
   parse_term_2(M, (:-)-do_nothing-B, Os, Out).
parse_term(M, T, Os, Out) :-
   !,
   parse_term_2(M, (:-)-do_nothing-T, Os, Out).


parse_term_2(search, Op-H-B, Options, search_term(Op_2, H_2, B_2)) :-
   ( member(ignore_rule_operator, Options) ->
     Op_2 = ignore
   ; Op_2 = Op ),
   ( H == do_nothing ->
     H_2 = do_nothing
   ; root_to_refactoring_representation(pattern, H, Options, H_2) ),
   ( B == do_nothing ->
     B_2 = do_nothing
   ; root_to_refactoring_representation(pattern, B, Options, B_2) ).
parse_term_2(subst, Op-H-B, Options, subst_term(Op_2, H_2, B_2)) :-
   ( member(ignore_rule_operator, Options) ->
     Op_2 = ignore
   ; Op_2 = Op ),
   ( member(H, [do_nothing, delete_pattern, delete_rule]) ->
     H_2 = H
   ; root_to_refactoring_representation(pattern, H, Options, H_2) ),
   ( member(B, [do_nothing, delete_pattern, delete_rule]) ->
     B_2 = B
   ; root_to_refactoring_representation(pattern, B, Options, B_2) ).


/* layout_conversion(Term, Chars, Term_Refac, Variables, Chars_Out) <-
      */

layout_conversion(T, Cs, T_Refac, Vs, Cs_Out) :-
   term_with_chars_to_xml(T, Cs, Xml_1, Vs),
   convert_layout_to_modified_term(Xml_1, T_Refac, Vs, Xml_3),
   xml_rule_to_chars(Xml_3, Cs_Out),
   !.


/* check_condition(Condition, L) <-
      */

check_condition(Cond, L) :-
   bound_representation_terms_to_prolog(L),
   !,
   call(Cond).


/* bound_representation_terms_to_prolog([X:Y|Zs]) :-
      */
      
bound_representation_terms_to_prolog([X:Y|Zs]) :-
   nonvar(Y),
   refactoring_representation_to_term(Y, X),
   bound_representation_terms_to_prolog(Zs).
bound_representation_terms_to_prolog([X:Y|Zs]) :-
   atom(Y),
   X=Y,
   bound_representation_terms_to_prolog(Zs).
bound_representation_terms_to_prolog([_X:_Y|Zs]) :-
   bound_representation_terms_to_prolog(Zs).
bound_representation_terms_to_prolog([]).


/* assign_terms(+X, +Y, -X:Y) <-
      */

assign_terms(X, Y, X:Y).


/******************************************************************/


