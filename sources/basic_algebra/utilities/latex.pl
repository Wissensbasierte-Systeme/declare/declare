

/******************************************************************/
/***                                                            ***/
/***           DDK:  LaTeX                                      ***/
/***                                                            ***/
/******************************************************************/


/* ps_cut_to_pdfs(Type) <-
      */

ps_cut_to_pdfs('DDB') :-
   Pages = [41, 110, 380, 620, 789, 953, 1144, 1332, 1349],
   ps_cut_to_pdfs('DDB', 11, Pages).

ps_cut_to_pdfs('DB_und_DB_2') :-
   Pages = [1, 64, 188, 426, 540, 733, 822, 854, 995, 1031],
   ps_cut_to_pdfs('DB_und_DB_2', 21, Pages).
ps_cut_to_pdfs('DB_und_DB_2_deutsch') :-
   Pages = [1, 58, 167, 312, 408, 482, 512, 577],
   ps_cut_to_pdfs('DB_und_DB_2', 18, Pages).

ps_cut_to_pdfs('Info_HaF') :-
   Pages = [1, 60, 163, 214, 223, 232],
   ps_cut_to_pdfs('Info_HaF', 15, Pages).

ps_cut_to_pdfs('Logik') :-
   Pages = [1, 145, 298, 408, 409],
   ps_cut_to_pdfs('Logik', 23, Pages).


/* script_type_to_path(Type, Path) <-
      */

script_type_to_path('Logik', Path) :-
   !,
   home_directory(Home),
   concat(Home,
      '/teaching/Logik/logik_1415/Folien/', Path).
script_type_to_path('DDB', Path) :-
   !,
   home_directory(Home),
   concat(Home,
      '/teaching/Deductive_Databases/dd_2015/Folien/', Path).
script_type_to_path(_, Path) :-
   home_directory(Home),
   concat(Home,
      '/teaching/Databases/db_1415/Folien/', Path).


/* script_type_to_file(Type, File) <-
      */

script_type_to_file('Logik', 'Logik') :-
   !.
script_type_to_file('DDB', 'Folien') :-
   !.
script_type_to_file('Info_HaF', 'Datenbanken_IH') :-
   !.
script_type_to_file(_, 'Datenbanken').


/*** interface ****************************************************/


/* ps_cut_to_pdfs(Type, N, Pages) <-
      */

ps_cut_to_pdfs(Type, N, Pages) :-
   page_list_reorganize(Pages, Pages_1),
   maplist( add(N),
      Pages_1, Pages_2 ),
   script_type_to_path(Type, Path),
   script_type_to_file(Type, File),
   Pages_3 = [1|Pages_2],
   ps_cut_to_pdfs(Path, File, 0, Pages_3),
   write(user, Pages_1),
   !.


/* ps_cut_to_pdfs(Path, File, N, Ns) <-
      */

ps_cut_to_pdfs(Path, File, N, [N1,N2|Ns]) :-
   !,
   N3 is N2 - 1,
   ps_cut_to_pdf(Path, File, N, N1-N3),
   M is N + 1,
   ps_cut_to_pdfs(Path, File, M, [N2|Ns]).
ps_cut_to_pdfs(_, _, _, _).

ps_cut_to_pdf(Path, File, N, N1-N2) :-
   writeln(user, ps_cut_to_pdf(Path, File, N, N1-N2)),
   concat([Path, File, '.ps'], File_ps),
   concat(Path, 'Handout/Kapitel_', Kapitel_),
   concat([Kapitel_, N, '.ps'], Kapitel_ps),
   concat([Kapitel_, N, '.pdf'], Kapitel_pdf),
   psselect(N1-N2, File_ps, Kapitel_ps),
   write_list(['---> ', Kapitel_pdf, '\n']),
   unix_command(['ps2pdf ', Kapitel_ps, ' ', Kapitel_pdf]),
   concat([Kapitel_, N, '.ps.4'], Kapitel_ps4),
   write_list(['---> ', Kapitel_ps4, '\n']),
   unix_command(['$HOME/bin/scripts/ps4f ', Kapitel_, N]).

psselect(N-M, File_1, File_2) :-
   write_list(['---> ', File_2, '\n']),
   unix_command([
      'psselect -p', N, '-', M, ' ', File_1, ' > ', File_2]).


/* page_list_reorganize(Xs, Ys) <-
      */

page_list_reorganize(Xs, Ys) :-
   page_list_reorganize(0, Xs, Ys).

page_list_reorganize(_, [], []).
page_list_reorganize(Offset, [X+N|Xs], [Y|Ys]) :-
   !,
   Offset_2 is Offset + N,
   Y is Offset_2 + X,
   page_list_reorganize(Offset_2, Xs, Ys).
page_list_reorganize(Offset, [X|Xs], [Y|Ys]) :-
   Y is Offset + X,
   page_list_reorganize(Offset, Xs, Ys).


/*** tests ********************************************************/


test(ps_cut_to_pdfs, 1) :-
   home_directory(Home),
   concat([Home, '/teaching/Skripten/db_seipel/',
      '2007_2008/DB_und_DB_2/'], Path),
   script_type_to_file('DB_und_DB_2', File),
   ps_cut_to_pdfs(Path, File, 0, [1,16,70,153]).


/******************************************************************/


