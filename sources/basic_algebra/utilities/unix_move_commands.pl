

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Unix File Handling                       ***/
/***                                                            ***/
/******************************************************************/


/* directory_to_move_commands(Directory, Prefix) <-
      */

directory_to_move_commands(Directory, Prefix) :-
   directory_to_move_commands(Directory, Prefix, Move_Commands),
   forall( member(Move_Command, Move_Commands),
      us(Move_Command) ).

directory_to_move_commands(Directory, Prefix, Move_Commands) :-
   directory_contains_files(Directory, Files),
   findall( Move_Command,
      ( member(File_1, Files),
        file_name_transform(Prefix, File_1, File_2),
        name_append([ 'mv ',
           Directory, '/', File_1, ' ',
           Directory, '/', File_2 ], Move_Command ) ),
      Move_Commands ).

file_name_transform(Prefix, File_1, File_2) :-
   name(Prefix, Pref),
   name(File_1, Fi_1),
   append(Pref, Num, Fi_1),
   length(Num, N),
   N < 3,
   !,
   append([Pref, "0", Num], Fi_2),
   name(File_2, Fi_2).


/******************************************************************/


