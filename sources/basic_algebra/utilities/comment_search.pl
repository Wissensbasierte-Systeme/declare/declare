

/******************************************************************/
/***                                                            ***/
/***        DisLog :  Comment Search                            ***/
/***                                                            ***/
/******************************************************************/


/* This module allows the user to search for keywords in the
   comments of the source code from the dislog system.
   For this purpose a search file is compiled from all source
   files by the predicat "compile_search_file".
   "compile_search_file" compiles all source files mentioned in
   "dislog_sources" with the c-program 'compile_search_file'
   executable in the active directory.
   The name of the search file is given by the atom
   "dislog_comment_search_file('results/comment_search.com')"

   There are several commands given to search for keywords in
   conjunctive or disjunctive combinition.
   These predicats use the unix command 'agrep' which also has
   to be executable in the active directory.

   After the successful search the command
   "vi(Sourcefile:Linenumber)"
   starts the vi with Sourcefile at Linenumber. */
 

/*** files ********************************************************/


dislog_comment_search_file('results/comment_search').


/*** interface ****************************************************/


/* compile_comment_search_file <-
      Takes all comments from all source files of DisLog placed 
      in dislog_sources and puts them in the comment search file.
      Compile_search_file must be executed after changing the 
      source code comments or bevor the first search command. 
      The predicat compile_search_file uses the c-program
      compile_search_file.c in the comiled version 
      compile_search_file by calling it with the command line:
         "compile_search_file source_file search_file"
      Thus compile_search_file must be executable from the 
      active directory.
      The c-program extracts all comments from the source_file
      and appends them to the search_file in the needed format 
      to search the search_file with agrep.*/
     
compile_comment_search_file :-
    dislog_comment_search_file(S_file),
    remove_file(S_file),    
    dislog_sources(Sources),
    compile_file_list(Sources).


/* conjunctive_comment_search(Keywords) <-
      Keywords is a list of keywords. con_search searches for 
      comments in the search file with all keywords in it, 
      by calling the unix-command agrep. 
      The unix command line looks like:
         "agrep -d '\^$' 'Pattern' 'Search_file'"
      Thus agrep must be executable from the active directory.
      If the keywords are not atomic in prolog, they have to be 
      enclosed with '...'; for example 
          conjunctive_comment_search([tree_,'_tree']).  */ 

conjunctive_comment_search(Keywords) :-
%  comment
%  conjunctive_convert_keywords(['<\<->'|Keywords],Pattern),
   conjunctive_convert_keywords(Keywords,Pattern),
   comment_search_pattern(Pattern).


/* disjunctive_comment_search(Keywords]) <-
      Like con_search but searches for comments with only one 
      of the keywords in it. */

disjunctive_comment_search(Keywords) :-
   disjunctive_convert_keywords(Keywords,Pattern),
   comment_search_pattern(Pattern).


/* conjunctive_comment_search(Number_of_Errors,Keywords) <-
      Like conjunctive_comment_search, but allows that in each 
      appearence of a keyword 'Number_of_Errors' errors may 
      appear.  A maximum of 8 errors is allowed.
         "agrep -d '\^$' -Number_of_Errors 'Pattern' 'Search_file'"
      One error appears if
         - a character is missing
         - a character is to much
         - a character is wrong.
      You can prohibit errors in special keywords or even special
      parts of keywords by enclosing them with <...>. 
      When using <...>, don't forget to enclose the keywords with 
      '...'. For example:
         conjunctive_error_search(3,['<basic_><_operator>']).
      finds 'basic_sd/se/sed_operator'. To combine disjunctiv search 
      with conjunctive search you could search for
         disjunctive_comment_search(8,
            ['<tree_>_<ratio>','<tree><fixpoint>']).
      and get 'tree_ts_fixpoint', 'tree_state_overlap_ratio' 
      and 'tree_overlap_ratio'.
      But this method is limited by the maximum number of errors 
      which is 8 and normally too small. */

conjunctive_comment_search(Number_of_Errors,Keywords) :-
   number(Number_of_Errors),
%  comment
%  conjunctive_convert_keywords(['<\<->'|Keywords],Pattern),
   conjunctive_convert_keywords([' '|Keywords],Pattern),
   comment_search_pattern(Number_of_Errors,Pattern).


/* disjunctive_comment_search(Number_of_Errors,Keywords) <-
      Same as disjunctive_comment_search with the posibility of 
      'Number_of_Errors' errors. */

disjunctive_comment_search(Number_of_Errors,Keywords) :-
   number(Number_of_Errors),
   disjunctive_convert_keywords(Keywords,Pattern),
   comment_search_pattern(Number_of_Errors,Pattern).


comment_search_pattern(Pattern) :-
   dislog_comment_search_file(S_file),
   !,
%  comment
%  unix_command(['tools/agrep -d ''\^$'' ''',
%     Pattern,''' ',S_file]).
   unix_command([' ',
      Pattern,''' ',S_file]).

comment_search_pattern(Number_of_Errors,Pattern) :-
   dislog_comment_search_file(S_file),
   !,
%  comment
%  unix_command(['tools/agrep -d ''\^$'' -',
%     Number_of_Errors, ' ''',Pattern,''' ',S_file]).
   unix_command([' ',
      Number_of_Errors, ' ''',Pattern,''' ',S_file]).


/* jump_into_source_file(Source:Linenumber) <-
      opens a vi window with the source file Source at line 
      Linenumber. */

jump_into_source_file(Source:Linenumber) :-
   number(Linenumber),
   source_file_name(Source,Sourcename),
   unix_command(['xterm -title ''vi:',Source,''' -n vi -e vi -c',
      Linenumber,' ',Sourcename,' &']).


/*** implementation ***********************************************/


remove_file(File) :-
   file_exists(File),
   unix_command(['rm ',File]).
remove_file(_).


compile_file_list([]).
compile_file_list([File|List]) :-
   dislog_comment_search_file(S_file),
   source_file_name(File,Filename),
   unix_command(['tools/compile_search_file ',
      Filename,' ',S_file]),
   compile_file_list(List).


conjunctive_convert_keywords(Keywords,Pattern) :- 
   generic_convert_keywords(';',Keywords,Pattern).

disjunctive_convert_keywords(Keywords,Pattern) :- 
   generic_convert_keywords(',',Keywords,Pattern).

generic_convert_keywords(_,[K],K) :- 
   !.
generic_convert_keywords(Separator,[K|Ks],Pattern) :-
   generic_convert_keywords(Separator,Ks,Pattern_Ks),
   concat_names([K,Separator,Pattern_Ks],Pattern).
generic_convert_keywords(_,[],'').


/******************************************************************/


