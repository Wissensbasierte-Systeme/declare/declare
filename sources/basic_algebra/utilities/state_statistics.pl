

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Overlap- and Tree-Ratios                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


mst_ratio :-
   old_init_d_fact_trees.

hr :-
   d_fact_tree(_), !,
   pp_ratio_information.
hr :-
   init_d_fact_trees.

pp_ratio_information :-
   d_fact_tree(Tree),
   pp_ratio_information(Tree).

pp_ratio_information(Tree) :-
   tree_overlap_ratio(Tree,N), 
   tree_state_overlap_ratio(Tree,M), K is M / N,
   write('                       '),
   write('  o_s = '), format("~2f",M),
   write('  o_t = '), format("~2f",N),
   write('  c_r = '), format("~2f",K), writeln(' ').

pp_join_tree_ratio_information(Facts) :-
   join_trees_ratio(Facts,N), state_overlap_ratio(Facts,M), K is M / N,
   write('                       '),
   write('  o_s = '), format("~2f",M),
   write('  j_r = '), format("~2f",N),
   write('  c_r = '), format("~2f",K), writeln(' ').


/* average_clause_length(State,Average) <-
      average length of the clauses in the d-state State. */
 
average_clause_length(State,Average) :-
   length(State,Length),
   state_volume(State,Volume),
   Average is Volume / Length.
 

/* tree_state_overlap_ratio(Tree,Ratio) <-
      Ratio is the ratio of the total number of different atoms and
      the number of all atoms in the state State represented by the 
      clause tree Tree. */

tree_state_overlap_ratio(Tree,Ratio) :-
   tree_to_state(Tree,State),
   state_overlap_ratio(State,Ratio).


/* tree_overlap_ratio(Tree,Ratio) <-
      Ratio is the ratio of the total number of different atoms and 
      the number of all atoms in the clause tree Tree. */

tree_overlap_ratio([],Max) :- 
   not_a_number(Max),
   !.
tree_overlap_ratio(Tree,Ratio) :-
   tree_volume(Tree,Volume),
   tree_weight(Tree,Weight),
   Ratio is Volume / Weight,
   !.
 
join_trees_ratio([],Max) :- 
   not_a_number(Max),
   !.
join_trees_ratio(State,Ratio) :-
   collect_arguments(join_tree,L_L_Trees),
   append(L_L_Trees,L_Trees),
   tree_volume([[]|L_Trees],Volume),
   state_weight(State,Weight),
   Ratio is Volume / Weight,
   !.


/*** implementation ***********************************************/


/* Add weight of fact and sons of node of tree */

tree_volume([Head|Trees],W) :-
   length(Head,W1),
   tree_volume_loop(Trees,W2),
   W is W1 + W2.
tree_volume([],0).


/* Adds weight of list of trees using tree_volume */

tree_volume_loop([Tree|Trees],TW) :-
   tree_volume(Tree,TW_Tree),
   tree_volume_loop(Trees,TW_Trees),
   TW is TW_Trees + TW_Tree.
tree_volume_loop([],0).


/* Number of different atoms */

state_volume_weight(State,Volume,Weight) :-
   state_volume(State,Volume),
   state_weight(State,Weight).

d_facts_volume_weight(Volume,Weight) :-
   collect_arguments(d_fact,State),
   state_volume_weight(State,Volume,Weight).

d_facts_volume(Volume) :-
   collect_arguments(d_fact,State),
   state_volume(State,Volume).


tree_weight(Tree,Weight) :-
   tree_to_state(Tree,State),
   state_weight(State,Weight).

tree_volume_weight(Tree,Volume,Weight) :-
   tree_to_state(Tree,State),
   state_volume_weight(State,Volume,Weight).


/* state_overlap_ratio(State,Ratio) <-
      the state overlap ratio of the Herbrand state State 
      is given by Ratio. */

state_overlap_ratio([],Max) :-
   not_a_number(Max), 
   !.
state_overlap_ratio(State,Ratio) :-
   state_volume(State,Volume),
   state_weight(State,Weight),
   Ratio is Volume / Weight,
   !.


/* state_volume(State,Volume) <-
      The volume Volume of a state State is the sum of the lenghts
      of the clauses in State. */

state_volume([C|Cs],Volume) :-
   length(C,V1),
   state_volume(Cs,V2),
   Volume is V1 + V2.
state_volume([],0).


/* state_weight(State,Weight) <-
      The weight Weight of a state State is the number 
      of different atoms State. */

state_weight(State,Weight) :-
   ord_union(State,Atoms),
   length(Atoms,Weight).

 
/******************************************************************/


