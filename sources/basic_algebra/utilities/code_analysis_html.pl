

/******************************************************************/
/***                                                            ***/
/***             DDK:  Source Analysis                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dislog_code_tree_to_html(File) <-
      */

dislog_code_tree_to_html(File) :-
   dislog_to_trees(Trees),
   maplist( dislog_code_tree_to_html,
      Trees, HTML ),
   fn_term_to_xml_file(
      body:[font:[color:'#ff0000']:[
         'The DisLog System'], ol:HTML], File).

dislog_code_tree_to_html([File],[li:[File]]).
dislog_code_tree_to_html(
      [[Root_1]|Trees_1], li:[b:[Root_2], ol:Trees_2]) :-
   ( ( Root_1 = 'Unit':Name,
       Root_2 = font:[color:'#0000ff']:['Unit:', Name],
       ! )
   ; term_to_atom(Root_1, Root_2) ),
   maplist( dislog_code_tree_to_html,
      Trees_1, Trees_2 ).


/******************************************************************/


