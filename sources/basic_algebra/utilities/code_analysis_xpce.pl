

/******************************************************************/
/***                                                            ***/
/***           Declare:  Code Analysis - XPCE                   ***/
/***                                                            ***/
/******************************************************************/


/*
   rgb_to_colour([11,11,15.99], Colour_1),
   rgb_to_colour([12,12,15.99], Colour_2),
   rgb_to_colour([13,13,15.99], Colour_3),
   rgb_to_colour([14,14,15.99], Colour_4),
   rgb_to_colour([15.99,15,3], Colour_5),
   rgb_to_colour([15.99,14,3], Colour_6),
   rgb_to_colour([15.99,13,3], Colour_7),
   rgb_to_colour([15.99,12,3], Colour_8),
   Colours = [ Colour_1, Colour_2, Colour_3, Colour_4,
      Colour_5, Colour_6, Colour_7, Colour_8 ],
*/

:- N = 15,
   ( for(I, 1, N), foreach(Colour, Colours) do
        R is 15.99 - (N - I)/3,
        G is 15.99 - (I - 1)/3,
        B is 15.99,
        rgb_to_colour([R,G,B], Colour) ),
   dislog_variables_set([
      dislog_unit_colours - Colours,
      dislog_unit_name - '' ]).


/*** interface ****************************************************/


/* dislog_code_analysis <-
      */

dislog_code_analysis :-
   dislog_frame_generic([
      header: 'Code Analysis',
      above: [ ],
      interface: [
         'Unit' - dislog_unit_name ],
      below: [
         'All Units' -
            dislog_unit_sizes_to_bar_chart,
         'Selected Unit' -
            dislog_unit_module_sizes_to_bar_chart ],
      close_button: [
         'Close' - right ] ]).


/* dislog_unit_sizes_to_bar_chart <-
      */

dislog_unit_sizes_to_bar_chart :-
   dislog_unit_sizes(Sizes),
   dislog_unit_sizes_to_distribution(Sizes, Distribution),
   Title = 'DisLog Units',
   dislog_bar_chart(Title, Distribution),
   findall( [Files, Lines, Words, Bytes, Unit],
      ( member(Lines-Words-Bytes-Unit, Sizes),
        dislog_unit_to_files(Unit, Fs),
        length(Fs, Files) ),
      Rows ),
   maplist( sum_all_nth(Rows),
      [1, 2, 3, 4], Sums ),
   append(Sums, ['- Total -'], Row),
   append(Rows, [Row], Rows_2),
   xpce_display_table(_,_,Title,
      ['Files', 'Lines', 'Words', 'Bytes', 'Unit'], Rows_2),
   maplist( nth_multiple([5, 2]),
      Rows_2, Rows_3 ),
   dislog_variable_get(output_path, Results),
   concat(Results, 'dislog_unit_sizes.csv', File),
   dwrite(xls, File, Rows_3).

sum_all_nth(Tuples, N, Sum) :-
   maplist( nth(N),
      Tuples, Xs ),
   sum(Xs, Sum).


/* dislog_unit_module_sizes_to_bar_chart(Unit) <-
      */

dislog_unit_module_sizes_to_bar_chart :-
   dislog_dialog_variables( get,
      [ 'Unit' - U ] ),
   dislog_unit(Unit, _),
   concat(U, _, Unit),
   dislog_dialog_variables( send,
      [ 'Unit' - Unit ] ),
   dislog_unit_module_sizes_to_bar_chart(Unit).

dislog_unit_module_sizes_to_bar_chart(Unit) :-
   dislog_unit_to_module_sizes(Unit, Sizes),
   dislog_unit_sizes_to_distribution(Sizes, Distribution),
   Title = Unit,
   dislog_bar_chart(Title, Distribution),
   findall( [Files, Lines, Words, Bytes, Module],
      ( member(Lines-Words-Bytes-Module, Sizes),
        dislog_module_to_files(Module, Fs),
        length(Fs, Files) ),
      Rows ),
   xpce_display_table(_, _, Title,
      ['Files', 'Lines', 'Words', 'Bytes', 'Module'], Rows).


/* dislog_unit_sizes_to_distribution(Ss,Ds) <-
      */

dislog_unit_sizes_to_distribution(Ss,Ds) :-
   dislog_unit_sizes_to_distribution([],Ss,Ds).

dislog_unit_sizes_to_distribution([C|Cs],[S|Ss],[D|Ds]) :-
   S = Lines-_-_-Unit,
   D = [Unit,C]-Lines,
   dislog_unit_sizes_to_distribution(Cs,Ss,Ds).
dislog_unit_sizes_to_distribution([],Ss,Ds) :-
   dislog_variable_get(dislog_unit_colours,Cs),
   dislog_unit_sizes_to_distribution(Cs,Ss,Ds).
dislog_unit_sizes_to_distribution(_,[],[]).


/* dislog_unit_to_module_sizes(Unit, Modules_Sizes) <-
      */

dislog_unit_to_module_sizes(Unit, Modules_Sizes) :-
   dislog_source_sizes(Sizes),
   dislog_unit(Unit, Modules),
   findall( Ls-Ws-Bs-M,
      ( member(M, Modules),
        concat([Unit, '/', M, '/'], Module),
        dislog_module_size(Sizes, Module, Ls-Ws-Bs-_) ),
      Modules_Sizes ).


/******************************************************************/


