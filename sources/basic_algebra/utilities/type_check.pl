

/******************************************************************/
/***                                                            ***/
/***             DisLog :  Type-Checking                        ***/
/***                                                            ***/
/******************************************************************/
 

type_check(dnlp,[R|Rs]) :-
   type_check(rule,R),
   type_check(dnlp,Rs).
type_check(dnlp,[]).

type_check(rule,R) :-
   ( type_check(dn_clause,R)
   ; type_check(d_clause,R)
   ; type_check(p_clause,R) ).


type_check(empty_set,[]).

type_check(non_empty_clause,Element) :-
   type_check(atoms,Element).


type_check(atoms,[A|As]) :-
   type_check(atom,A),
   type_check(atoms,As).
type_check(atoms,[]).

type_check(atom,A) :-
   atom(A),
   \+ type_check(empty_set,A).


type_check(literal,L) :-
   type_check(atom,L).
type_check(literal,~L) :-
   type_check(atom,L).


type_check(clause,[L|Ls]) :-
   type_check(literal,L),
   type_check(clause,Ls).
type_check(clause,[]).

type_check(p_clause,As) :- 
   type_check(atoms,As).
type_check(p_clause,[]).


type_check(d_clause,Rule) :-
   parse_dislog_rule(Rule,Head,Body1,Body2),
   !,
   type_check(p_clause,Head),
   type_check(p_clause,Body1),
   type_check(empty_set,Body2).
 
type_check(dn_clause,Rule) :-
   parse_dislog_rule(Rule,Head,Body1,Body2),
   !,
   type_check(p_clause,Head),
   type_check(p_clause,Body1),
   type_check(p_clause,Body2).


type_check(dhs,[C|Cs]) :-
   type_check(p_clause,C),
   type_check(dhs,Cs).
type_check(dhs,[]).


type_check(tree,[Root|Trees]) :-
   type_check(clause,Root),
   type_check(trees,Trees).
type_check(tree,[]).

type_check(trees,[T|Ts]) :-
   type_check(tree,T),
   type_check(trees,Ts).
type_check(trees,[]).


type_check(state_triple,[Cs1,Cs2,Cs3]) :-
   type_check(dhs,Cs1),
   type_check(atoms,Cs2),
   type_check(chs,Cs3).


type_check(sn_set,[[Atom]|State]) :-
   atom(Atom),
   type_check(dhs,State).

type_check(snd_set,[Clause|State]) :-
   type_check(non_empty_clause,Clause),
   type_check(dhs,State).


/******************************************************************/


