

/******************************************************************/
/***                                                            ***/
/***           DisLog:  LaTeX Index                             ***/
/***                                                            ***/
/******************************************************************/
   
   
   
/*** interface ****************************************************/
   

/* transform_latex_index_entries(Mode) <-
      */

transform_latex_index_entries('DDB_Buch') :-
   home_directory(Home),
   P = '/teaching/Deductive_Databases/dd_2020/Folien/',
   concat(Home, P, Path),
   transform_latex_index_entries(
      Path, 'Buch.idx', 'Buch.ind').
transform_latex_index_entries('DDB') :-
   home_directory(Home),
   P = '/teaching/Deductive_Databases/dd_2020/Folien',
   concat(Home, P, Path),
   transform_latex_index_entries(
      Path, 'Folien.idx', 'Index.tex').
transform_latex_index_entries('DB') :-
   home_directory(Home),
   P = '/teaching/Databases/db_1415/Folien/',
   concat(Home, P, Path),
   transform_latex_index_entries(
      Path, 'Datenbanken.idx', 'Datenbanken.idy').
transform_latex_index_entries('Info_HaF') :-
   home_directory(Home),
   P = '/teaching/Databases/db_1415/Folien/',
   concat(Home, P, Path),
   transform_latex_index_entries(
      Path, 'Datenbanken_IH.idx', 'Datenbanken_IH.idy').
transform_latex_index_entries('Logik') :-
   home_directory(Home),
   P = '/teaching/Logik/logik_1415/Folien/',
   concat(Home, P, Path),
   transform_latex_index_entries(
      Path, 'Logik.idx', 'Logik.idy').

transform_latex_index_entries(Path, File_Idx_1, File_Idx_2) :-
   concat(Path, File_Idx_1, File_1),
   read_file_to_name(File_1, Name),
   name_exchange_sublist([["\\@mathit ", "\\mathit"]], Name, Name_1),
   name_split_at_position(["\n"], Name_1, Is_1),
   ( foreach(I, Is_1), foreach(J, Is_2) do
        concat(K1, '}', I),
        name_split_at_position(["entry{","}{"], K1, K2),
        K2 = [_|J] ),
   index_entries_sort(Is_2, Is_3),
   index_entries_group(Is_3, Is_4),
   extract_index_entries_for_types(
      ['Operator', '\\ddk', '\\prolog'], Is_4, Is_s, Is_5),
%  writeq(user, Is_s),
   ( foreach(I, Is_5), foreach(J, Is_6) do
        I = [X, Ns],
        concat(['\\item ', X, ', ', Ns], J) ),
   index_items_group_subitems(Is_6, Is_7),
   append(Is_7, Is_s, Is_8),
   index_entries_add_spaces(Is_8, Is_9),
   concat(Path, File_Idx_2, File_2),
   predicate_to_file( File_2,
      writeln_list(Is_9) ).


/* extract_index_entries_for_types(Types, Es_1, Es_s, Es_2) <-
      */

extract_index_entries_for_types(Types, Es_1, Es_s, Es_2) :-
   extract_index_entries_for_types_(Types, Es_1, Es_List, Es_2),
   append(Es_List, Es_s).

extract_index_entries_for_types_([T|Ts], Es_1, Es, Es_2) :-
   extract_index_entries_for_type(T, Es_1, Es_s, Es_3),
   ( Es_s = [_] -> Es = Es_List
   ; Es = [Es_s|Es_List] ),
   extract_index_entries_for_types_(Ts, Es_3, Es_List, Es_2).
extract_index_entries_for_types_([], Es, [], Es).

extract_index_entries_for_type(
      Type, Entries_1, Entries_s, Entries_2) :-
   findall( Entry_2,
      ( member(Entry_1, Entries_1),
        transform_index_entry_for_type(Type, Entry_1, Entry_2) ),
      Entries_s_2 ),
   findall( Entry_1,
      ( member(Entry_1, Entries_1),
        \+ transform_index_entry_for_type(Type, Entry_1, _) ),
      Entries_2 ),
   length(Entries_1, N1),
   length(Entries_s_2, Ns),
   length(Entries_2, N2),
   writeln(user, N1-Ns=N2),
   concat('\\item ', Type, Entry),
   Entries_s = [Entry|Entries_s_2].

transform_index_entry_for_type(Type, [X, N], Item) :-
   concat(Type, '!', Prefix),
   concat(Prefix, Y, X),
   concat(['\\subitem ', Y, ', ', N], Item).


/* index_entries_group(Es_1, Es_2) <-
      */

index_entries_group([[X, N]|Es_1], Es_2) :-
   index_entries_group([X, [N]], Es_1, Es_2).
index_entries_group([], []).

index_entries_group([X, Ns], [[X, N]|Es_1], Es_2) :-
   !,
   index_entries_group([X, [N|Ns]], Es_1, Es_2).
index_entries_group([X, Ns], [[Y, M]|Es_1], [[X, N]|Es_2]) :-
   sort_name_numbers(Ns, Ms),
   names_append_with_separator(Ms, ', ', N),
   index_entries_group([Y, [M]], Es_1, Es_2).
index_entries_group([X, Ns], [], [[X, N]]) :-
   sort_name_numbers(Ns, Ms),
   names_append_with_separator(Ms, ', ', N).

sort_name_numbers(Xs, Ys) :-
   maplist( atom_number,
      Xs, Ns ),
   sort(Ns, Ms),
   maplist( atom_number,
      Ys, Ms ).


/* index_entries_add_spaces(Es_1, Es_2) <-
      */

index_entries_add_spaces([E|Es_1], Es_2) :-
   concat('\\item ', X, E),
   strip_slash_and_downcase(X, Y),
   name(Y, [C|_]),
   index_entries_add_spaces(C, [E|Es_1], Es_2).

index_entries_add_spaces(C, [E|Es_1], [Space, E|Es_2]) :-
   concat('\\item ', X, E),
   strip_slash_and_downcase(X, Y),
   name(Y, [D|_]),
   C \= D,
   !,
   Space = '\\indexspace',
   index_entries_add_spaces(D, Es_1, Es_2).
index_entries_add_spaces(C, [E|Es_1], [E|Es_2]) :-
   index_entries_add_spaces(C, Es_1, Es_2).
index_entries_add_spaces(_, [], []).


/* index_items_group_subitems(Es_1, Es_2) <-
      */

index_items_group_subitems([], []) :-
   !.
index_items_group_subitems([E1, E2|Es_1], [E|Es_2]) :-
   concat('\\item ', X1, E1),
   concat('\\item ', X2, E2),
   name_contains_name(X1, '!'),
   name_contains_name(X2, '!'),
   name_split_at_position(["!"], X1, [N|_]), 
   name_split_at_position(["!"], X2, [N|_]),
   !,
   concat('\\item ', N, E),
   index_items_group_subitems(N, [E1, E2|Es_1], Es_2).
index_items_group_subitems([E1, E2|Es_1], [E1|Es_2]) :-
   concat('\\item ', X1, E1),
   concat('\\item ', X2, E2),
   name_contains_name(X2, '!'),
   name_split_at_position([","], X1, [N|_]),
   name_split_at_position(["!"], X2, [N|_]),
   !,
   index_items_group_subitems(N, [E2|Es_1], Es_2).
index_items_group_subitems([E|Es_1], [E1, E2|Es_2]) :-
   concat('\\item ', X, E),
   name_contains_name(X, '!'),
   !,
   name_split_at_position(["!"], X, [N|Names]),
   names_append_with_separator(Names, '!', Y),
   concat('\\item ', N, E1),
   concat('\\subitem ', Y, E2),
   index_items_group_subitems(Es_1, Es_2).
index_items_group_subitems([E|Es_1], [E|Es_2]) :-
   index_items_group_subitems(Es_1, Es_2).

index_items_group_subitems(_, [], []) :-
   !.
index_items_group_subitems(N, [E1|Es_1], [E2|Es_2]) :-
   concat('\\item ', X, E1),
   name_contains_name(X, '!'),
   name_split_at_position(["!"], X, [N|Names]),
   !,
   names_append_with_separator(Names, '!', Y),
   concat('\\subitem ', Y, E2),
   index_items_group_subitems(N, Es_1, Es_2).
index_items_group_subitems(_, Es_1, Es_2) :-
   index_items_group_subitems(Es_1, Es_2).


/* index_entries_sort(Es_1, Es_2) <-
      */

index_entries_sort(Es_1, Es_2) :-
   predsort(index_entries_compare, Es_1, Es_2).

index_entries_compare(C, [E1, _], [E2, _]) :-
   strip_slash_and_downcase(E1, X1),
   strip_slash_and_downcase(E2, X2),
   ( X1 @< X2, C = <
   ; X1  = X2, C = <
   ; X1 @> X2, C = > ).


/* strip_slash_and_downcase(X, Y) <-
      */

strip_slash_and_downcase(X, Y) :-
   ( concat('\\', A, X)
   ; A = X ),
   downcase_atom(A, Y),
   !.


/******************************************************************/


