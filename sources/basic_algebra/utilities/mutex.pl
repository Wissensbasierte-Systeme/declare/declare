

/******************************************************************/
/***                                                            ***/
/***          DDK:  Mutexes                                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* with_mutex_(Mutex, Goal) <-
      */

with_mutex_(Mutex, Goal) :-
   mutex_create_(Mutex),
   with_mutex(Mutex, Goal).


/* mutex_create_(Mutex) <-
      */

mutex_create_(Mutex) :-
   current_mutex(Mutex, _, _),
   !.
mutex_create_(Mutex) :-
   mutex_create(Mutex).


/******************************************************************/


