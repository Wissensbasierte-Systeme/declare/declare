

/******************************************************************/
/***                                                            ***/
/***             DDK:  Source Analysis                          ***/
/***                                                            ***/
/******************************************************************/


dislog_variable(
   word_count_file, 'results/words').
dislog_variable(
   word_count_file_2, 'results/words_2').


/*** interface ****************************************************/


/* dislog_to_files(Files) <-
      */

dislog_to_files(Files) :-
   findall( File,
      ( dislog_unit(Unit, _),
        dislog_unit_to_files(Unit, Fs),
        member(File, Fs) ),
      Files ).


/* dislog_unit_to_files(Unit, Files) <-
      */

dislog_unit_to_files(Unit, Files) :-
   dislog_unit(Unit, Modules),
   maplist( dislog_module_to_files,
      Modules, List ),
   append(List, Fs),
   ( foreach(F, Fs), foreach(File, Files) do
        concat([Unit, '/', F], File) ).


/* dislog_module_to_files(Module, Files) <-
      */

dislog_module_to_files(Module, Files) :-
   findall( File,
      ( dislog_module(Module, Files_in_Module),
        member(File, Files_in_Module)
      ; dislog_module_description(Module, Files_in_Module),
        member(File, Files_in_Module) ),
      Fs ),
   ( foreach(F, Fs), foreach(File, Files) do
        concat([Module, '/', F], File) ).


/* dislog_package_to_files(Level:X, Files) <-
      */

dislog_package_to_files(system:dislog, Files) :-
   dislog_variable_get(source_path, Sources),
   dislog_to_files(Fs),
   dislog_package_to_files_2(Sources, Fs, Files).
dislog_package_to_files(unit:Unit, Files) :-
   dislog_variable_get(source_path, Sources),
   dislog_unit_to_files(Unit, Fs),
   dislog_package_to_files_2(Sources, Fs, Files).
dislog_package_to_files(module:Module, Files) :-
   dislog_variable_get(source_path, Sources),
   dislog_unit(Unit, Modules),
   member(Module, Modules),
   dislog_module(Module, Fs),
   concat([Sources, Unit, '/', Module, '/'], Path),
   dislog_package_to_files_2(Path, Fs, Files).

dislog_package_to_files_2(Path, Fs, Files) :-
   ( foreach(F, Fs), foreach(File_2, Files_2) do
        concat(Path, F, File_2) ),
   findall( File,
      ( member(File_2, Files_2),
        file_to_real_file(File_2, File) ),
      Files ).


/* file_to_real_file(File_1, File_2) <-
      */

file_to_real_file(File, File) :-
   concat(_, '.pl', File),
   !,
   file_exists(File).
file_to_real_file(File_1, File_2) :-
   concat(File_1, '.pl', File_2),
   file_exists(File_2),
   !.
file_to_real_file(File, File) :-
   file_exists(File).


/* dislog_to_trees <-
      */

dislog_to_trees :-
   dislog_to_trees(Trees), 
   dportray(tree,[['System':'DisLog']|Trees]).
 
dislog_to_trees_edge :-
   dislog_to_trees(Trees), 
   dportray(edge(trees),Trees).
 
dislog_to_trees(Trees) :-
   findall( Unit, 
      dislog_unit(Unit,_),
      Units ), 
   maplist( dislog_unit_to_tree,
      Units, Trees ).  
 

/* dislog_unit_to_tree(Unit,Tree) <-
      */

dislog_unit_to_tree(Unit) :-
   dislog_unit_to_tree(Unit,Tree),
   dportray(tree,Tree).    
 
dislog_unit_to_tree_edge(Unit) :-
   dislog_unit_to_tree(Unit,Tree),
   dportray(edge(tree),Tree).    
 
dislog_unit_to_tree(Unit,[['Unit':Unit]|Subtrees]) :-
   dislog_unit(Unit,Modules),  
   maplist( dislog_module_to_tree,
      Modules, Subtrees ). 
 

/* dislog_module_to_tree(Module,Tree) <-
      */

dislog_module_to_tree(Module,[['Module':Module]|Subtrees]) :-
   ( ( dislog_module(Module,Files),
       ! )
   ; Files = [] ),
   sort(Files,Sorted_Files), 
%  elements_to_lists(Sorted_Files,List_of_Lists),
%  elements_to_lists(List_of_Lists,Subtrees).
   elements_to_lists(Sorted_Files,Subtrees).


/* dislog_total_number_of_lines(Total_Number_of_Lines) <-
      */

dislog_total_number_of_lines(Total_Number_of_Lines) :-
   dislog_module_sizes(Modules_Sizes),
   maplist( project_on_first_column_out_of_four,
      Modules_Sizes, Lines_List ),
   add(Lines_List,Total_Number_of_Lines).
   
project_on_first_column_out_of_four(X-_-_-_,X).


/* dislog_unit_sizes(Unit_Sizes) <-
      */

dislog_unit_sizes(Unit_Sizes) :-
   dislog_module_sizes(Modules_Sizes),
   findall( Size,
      ( dislog_unit(Unit, _),
        dislog_unit_size(Modules_Sizes, Unit, Size) ),
      Unit_Sizes ).


/* dislog_module_sizes <-
      */

dislog_module_sizes(Modules_Sizes) :-
   dislog_modules(DisLog_Modules),
   dislog_module_sizes(DisLog_Modules, Modules_Sizes).

dislog_module_sizes(Modules, Modules_Sizes) :-
   dislog_source_sizes(Sizes),
   maplist_with_status_bar( dislog_module_size(Sizes),
      Modules, Modules_Sizes ).


/* dislog_module_size(Module, Module_Size) <-
      */

dislog_module_size(Module, Module_Size) :-
   dislog_source_sizes(Sizes),
   dislog_module_size(Sizes, Module, Module_Size).


/* dislog_source_sizes(Sizes) <-
      */

dislog_source_sizes(Sizes) :-
   current_prolog_flag(unix, true),
   dislog_variable(word_count_file, File),
   dislog_variable(word_count_file_2, File_2),
   concat_atoms([
      'cd sources;',
      'wc */*/* > ../',File_2,';',
      'cd ..;',
      'echo " 0 0 0 total " > results/eof;',
      'cat ',File_2,' results/eof > ',File],Command),
   unix(system(Command)),
   read_word_count_lines(File, Sizes),
   !.
dislog_source_sizes(Sizes) :-
   current_prolog_flag(windows, true),
   dislog_sources(Sources),
   dislog_variable_get(source_path, Path),
   maplist( dislog_source_sizes_wc(Path),
      Sources, Sizes ).

dislog_source_sizes_wc(Path, File, Lines-Words-Bytes-File) :-
   concat(Path, File, Path_1),
   dislog_file_adjust_ending(Path_1, Path_2),
   wc(Path_2, Lines, Words, Bytes),
   writeln(user, Lines-Words-Bytes-File).


/* dislog_total_number_of_rules(N) <-
      */

dislog_total_number_of_rules(N) :-
   dislog_files_to_xml(Xml),
   findall( Rule,
      Rule := Xml^file^rule,
      Rules ),
   length(Rules, N).

dislog_total_number_of_rules_2(N) :-
   dislog_sources(Sources),
   dislog_variable_get(source_path, Path),
   maplist( concat(Path),
      Sources, Files ),
   maplist( dislog_file_adjust_ending,
      Files, Files_2 ),
   maplist( dislog_consult,
      Files_2, Programs ),
   maplist( length,
      Programs, Ns ),
   add(Ns,N).


dislog_file_adjust_ending_2(File_1, File_2) :-
%  writeln(user, File_1),
   concat('sources/', File_1, File_A),
   dislog_file_adjust_ending(File_A, File_B),
   concat('sources/', File_2, File_B),
%  writeln(user, File_2),
   !.

dislog_file_adjust_ending(File, File) :-
   file_exists(File).
dislog_file_adjust_ending(File_1, File_2) :-
   concat(File_1, '.pl', File_2).


/*** implementation ***********************************************/


/* dislog_unit_size(Sizes, Unit, Lines-Words-Bytes-Unit) <-
      */

dislog_unit_size(Sizes, Unit, Lines-Words-Bytes-Unit) :-
   dislog_unit(Unit, Sources),
   ddbase_aggregate( [sum(L), sum(W), sum(B)],
      ( member(Source, Sources),
        concat([Unit, '/', Source, '/'], Source_Path),
        member(L-W-B-Source_Path, Sizes) ),
      [[Lines, Words, Bytes]] ).


/* dislog_module_size(
         Sizes, Module_Path, Lines-Words-Bytes-Module_Path) <-
      */

dislog_module_size(
      Sizes, Module_Path, Lines-Words-Bytes-Module_Path) :-
   write_list(user, [Module_Path, ' ']),
   dislog_module_description(_, Module_Path, Source_Paths),
   findall( L-W-B-Source_Path_2,
      ( member(Source_Path, Source_Paths),
        dislog_file_adjust_ending_2(Source_Path, Source_Path_2),
        once(member(L-W-B-Source_Path_2, Sizes)),
        write(user,'.'), ttyflush ),
      Sizes_2 ),
   nl(user),
   ddbase_aggregate( [sum(L), sum(W), sum(B)],
      ( member(Source_Path, Source_Paths),
        dislog_file_adjust_ending_2(Source_Path, Source_Path_2),
        once(member(L-W-B-Source_Path_2, Sizes_2)) ),
      [[Lines, Words, Bytes]] ).


/* read_word_count_lines(File, Wc_Lines) <-
      */

read_word_count_lines(File, Wc_Lines) :-
%  open(File, read, Stream),
%  set_input(Stream),
   see(File),
   read_word_count_lines(Wc_Lines),
   seen,
%  close(Stream),
   !.

read_word_count_lines(Wc_Lines) :-
   read_word_count_line(L-W-B-Name),
   !,
   ( ( Name = 'total' ->
       Wc_Lines = [L-W-B-Name] )
   ; ( read_word_count_lines(Lines),
       Wc_Lines = [L-W-B-Name|Lines] ) ).

read_word_count_line(L-W-B-Name) :-
   read_entry(L),
   read_entry(W),
   read_entry(B),
   read_entry(Name),
   !.

read_entry(Entry) :-
   get0(C),
   read_entry(C, Entry, _).

read_entry(26, _, _) :-
   !,
   fail.
read_entry(C, Entry, C2) :-
   in_entry(C),
   !,
   get0(C1),
   rest_entry(C1, Cs, C2),
   name(Entry, [C|Cs]).
read_entry(_, Entry, C2) :-
   get0(C1),
   read_entry(C1, Entry, C2).

rest_entry(C, [C|Cs], C2) :-
   in_entry(C),
   !,
   get0(C1),
   rest_entry(C1, Cs, C2).
rest_entry(C, [], C).

in_entry(C) :-
   in_word(C, _).
in_entry(C) :-
   member(C, "./_-").


/******************************************************************/


