

/******************************************************************/
/***                                                            ***/
/***          DDK:  Time Statistics                             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* ddk_measure <-
      */

ddk_measure :-
   Max is 10^7,
   ddk_measure(Max).

ddk_measure(Max) :-
   measure( (for(_, 1, Max) do write('')) ).


/* ddk_time(T) <-
      */

ddk_time(T) :-
%  statistics(runtime, [T, _]).
%  statistics(cputime, T).
   statistics(real_time, [T, _]).


/* measure_runtime(Text, Goal) <-
      */

measure_runtime(Text, Goal) :-
   write_list([Text, ': ']),
   measure_runtime(Goal).

measure_runtime(Goal) :-
   statistics(runtime, [T1|_]),
   call(Goal),
   statistics(runtime, [T2|_]),
   round((T2 - T1) / 1000, 2, Time),
   write_list([Time, ' sec.\n']).


/* measure(Timer, Goal, Time) <-
      */

measure(Goal) :-
   measure(Goal, Time),
   pp_time(Time).

measure(Goal, Time) :-
   measure(timer, Goal, Time).

measure(Timer, Goal, Time) :-
   get_num(Timer, N),
   start_timer(Timer-N),
   call(Goal),
   stop_timer(Timer-N, Time),
   M is N - 1,
   set_num(Timer, M).


/* measure_and_add(Goal,Timer) <-
      */

measure_and_add(Goal, Timer) :-
   measure(Goal, Time),
   time_add(Timer, Time).


/* measure_hidden(I, Goal, Time) <-
      */

measure_hidden(I, Goal) :-
   measure_hidden(I, Goal, Time), nl,
   write('total time   '),
   pp_time(Time),
   writeln(' sec. ').

measure_hidden(I, Goal, Time) :-
   get_num(timer,N),
   start_timer(timer-N),
   hidden( I, Goal ),
   stop_timer(timer-N, Time),
   M is N - 1,
   set_num(timer, M).


/* measure_until_measurable(Goal,Time) <-
      */

measure_until_measurable(Goal) :-
   measure_until_measurable(Goal, Time),
%  pp_time(Time).
   write(Time), write(' sec').

measure_until_measurable(Goal, Time) :-
   measure_until_measurable(1, Goal, Time).

measure_until_measurable(M, Goal, Time) :-
   measure_multiple(M, Goal, T),
   ( ( T \= 0,
       !,
       Time = T )
   ; ( N is 10 * M,
       measure_until_measurable(N, Goal, Time) ) ).

measure_multiple(M, Goal, Time) :-
   get_num(timer, N),
   start_timer(timer-N),
   measure_multiple(M, Goal),
   stop_timer(timer-N, T),
   retract(current_num(timer, N)),
   Time is T / M,
   !.

measure_multiple(0, _) :-
   !.
measure_multiple(M, Goal) :-
   call(Goal),
   K is M - 1,
   !,
   measure_multiple(K, Goal).


/* time_add(Time_Predicate, Time) <-
      */

time_add(Time_Predicate, Time) :-
   Time_Atom_1 =.. [Time_Predicate,Time_1],
   retract(Time_Atom_1),
   Time_2 is Time + Time_1,
   Time_Atom_2 =.. [Time_Predicate,Time_2],
   assert(Time_Atom_2).


/* prepare_analysis <-
      */

prepare_analysis :-
   ddk_time(Time),
   asserta(start_time(Time)).


/* perform_analysis <-
      */

perform_analysis :-
   retract(start_time(Time_1)),
   ddk_time(Time_2),
%  Time is ( Time_2 - Time_1 ) / 1000,
   Time is Time_2 - Time_1,
   write('total time   '),
   pp_time(Time),
   writeln(' sec. ').


/* pp_time(T) <-
      */

pp_time(T) :-
%  dislog_format(user,"~2f",T).
   dislog_format("~2f",T).

pp_times([T]) :-
   dislog_format("~4f",T),
   nl.
pp_times([T|Ts]) :-
   dislog_format("~4f",T),
   write(' - '),
   pp_times(Ts).

pp_times(File,Times) :-
   switch(File2,File),
   pp_times(Times),
   switch(File2).


/* pp_averages(Times) <-
      */

pp_averages([T]) :-
   dislog_format("~2f",T),
   nl.
pp_averages([T|Ts]) :-
   dislog_format("~2f",T),
   write(' - '),
   pp_averages(Ts).
   

/* start_timer(I) <-
      */

start_timer(I) :-
    ddk_time(Time), 
    retractall(start_time(I, _)),
    assert(start_time(I, Time)).


/* stop_timer(I) <-
      */

stop_timer_write(I) :-
   stop_timer(I, Time),
   pp_time(Time), write(' sec.   ').
  
stop_timer(I) :-
   stop_timer(I, Time),
   pp_time(Time), writeln(' sec. ').

stop_timer(I, Time) :-
   retract(start_time(I, Time_1)),
   ddk_time(Time_2),
%  Time is ( Time_2 - Time_1 ) / 1000.
   Time is Time_2 - Time_1.


/* init_timer(I) <-
      */

init_timer(I) :-
   assert(accumulated_time(I,0)).


/* add_timer(I) <-
      */

add_timer(I) :-
   retract(accumulated_time(I, Time_0)),
   !,
   start_time(I, Time_1),
   ddk_time(Time_2),
   Time is ( Time_0 + ( Time_2 - Time_1 ) ),
   assert(accumulated_time(I, Time)).
add_timer(I) :-
   start_time(I, Time_1),
   ddk_time(Time_2),
   Time is ( Time_2 - Time_1 ),
   assert(accumulated_time(I, Time)).


/* close_timer(I) <-
      */

close_timer(I) :-
   retract(accumulated_time(I, Time_1)),
%  Time is Time_1 / 1000,
   Time is Time_1,
   pp_time(Time), writeln(' sec. ').


/* ddk_date_time(Time) <-
      */

ddk_date_time(Time) :-
   get_time(U), convert_time(U,X2,X3,X4,X5,X6,X7,X8),
   maplist( number_to_two_digit_name,
      [X2,X3,X4,X5,X6,X7,X8], [Y2,Y3,Y4,Y5,Y6,Y7,Y8] ),
   concat(
      [Y2,'-',Y3,'-',Y4,' ',Y5,':',Y6,':',Y7,' ',Y8],
      Time).


/* dislog_date_and_time(Date, Time) <-
      */

dislog_date_and_time(Date, Time) :-
   ddk_timestamp(Date_and_Time),
%  name_split_at_position([" "], Date_and_Time, [Date, Time]).
   name_split_at_position([[32]], Date_and_Time, [Date, Time]).


/* ddk_timestamp(Time) <-
      */

ddk_timestamp(Time) :-
   get_time(U), convert_time(U,X2,X3,X4,X5,X6,X7,_X8),
   maplist( number_to_two_digit_name,
      [X2,X3,X4,X5,X6,X7], [Y2,Y3,Y4,Y5,Y6,Y7] ),
   concat(
      [Y4,'.',Y3,'.',Y2,' ',Y5,':',Y6,':',Y7,'-'],
      T),
%  name_split_at_position(["-"], T, [Time]).
   name_split_at_position([[45]], T, [Time]).

number_to_two_digit_name(N, S) :-
   N < 10,
   !,
   concat('0', N, S).
number_to_two_digit_name(N, N).


/* dislog_time(Hour:Minute:Second, Day:Month:Year) <-
      */

dislog_time(Hour:Minute:Second, Day:Month:Year) :-
   get_time(Time),
   convert_time(
      Time, Year, Month, Day, Hour, Minute, Second, _).


/*** tests ********************************************************/


test(meta_predicates:measure, 1) :-
   generate_interval(1, 100000, I),
   measure(
      measure( add(I, _), T1 ),
      T2 ),
%  Time_1 is 1000 * T1,
%  Time_2 is 1000 * T2,
   writeln(time(T1, T2)).
 

/******************************************************************/


