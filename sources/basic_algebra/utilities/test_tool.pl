

/******************************************************************/
/***                                                            ***/
/***         DDK:  Test - Tool                                  ***/
/***                                                            ***/
/******************************************************************/
 

:- module( test_tool,[
      manual_file/1,
      test_manual_file/1,
      result_file/1,
      perf_test_file/1,
      perf_result_file/1,
      perf_result_file2/1,
      dont_test/1,
      manual_test/0,
      manual_test/1,
      chapter_test/1,
      chapter_test/2,
      performance_test/0,
      performance_test/1,
      performance_query/4 ] ).

:- dynamic
      manual_file/1,
      result_file/1,
      perf_test/0,
      perf_test_file/1,
      perf_result_file/1,
      performance_test/4,
      dont_test/1.


/*** files ********************************************************/


manual_file('manuals/nm_reasoning/library.tex').
test_manual_file('testing/test_library.tex').
string_to_term_tmp_file('results/string_to_term.tmp').
result_file('results/test_result').
perf_test_file('results/performance/performance.in').
perf_result_file('results/performance/performance.out').
perf_result_file2('results/performance/performance.acc').
date_tmp_file('results/date_file.tmp').


/*** interface ****************************************************/


/* dont_test(Predicate) <-
      predicate Predicate will not be tested */

dont_test(use_module).
dont_test(tps_fixpoint_cc).


/* manual_test <-
      test all examples in the texinfo file which is specified by
      the manual_file predicate */

manual_test :-
   manual_file(File),
   manual_test(File).


/* manual_test(File) <-
      test all examples in the texinfo file File */

manual_test(File) :-
   result_file(Resultfile),
   prepare_test(File,Resultfile,Old,Stream),
   process_manual,
   conclude_test(Old,Stream).


/* chapter_test(Chapter) <-
      test all examples in chapter Chapter of the texinfo file
      which is specified by the manual_file predicate */

chapter_test(Chapter) :-
   manual_file(File),
   chapter_test(File,Chapter).


/* chapter_test(File,Chapter) <-
      test all examples in chapter Chapter of the texinfo file
      File */

chapter_test(File,Chapter) :-
   result_file(Resultfile),
   prepare_test(File,Resultfile,Old,Stream),
   goto_chapter(Chapter),
   process_chapter,
   conclude_test(Old,Stream).


/* performance_test <-
      compute the performance of all the queries in the texinfo
      file specified by perf_test_file */

performance_test :-
   perf_test_file(File),
   performance_test(File).


/* performance_test(File) <-
      same as performance_test/0, but for the file File */

performance_test(File) :-
   perf_result_file(Outputfile),
   perf_result_file2(Accfile),
   prepare_test(File,Outputfile,Oldflag,Stream),
   assert(perf_test),
   process_manual,
   conclude_test(Oldflag,Stream),
   name(Outputfile,OF),
   name(Accfile,AF),
   append(OF," >> ",X1),
   append(X1,AF,X2),
   name(Arg,X2),
   user:us(cat,Arg),
   retract(perf_test).


/* performance_query(Goal,Time,Date,Host) <-
      Make sure the performance_test database (which is produced by
      performance_test) is loaded and deliver Goal, Time, Date and
      Host. */

performance_query(Goal,Time,Date,Host) :-
   perf_result_file2(File),
   ensure_loaded(File),
   performance_test(Goal,Time,Date,Host).


/*** implementation ***********************************************/


/* prepare_test(File,Outputfile,OldFlag,Stream) <-
      Initializes I/O and Prolog flags before the test.
      File is the file which is to be tested.
      Outputfile is the file where program output is written.
      OldFlag saves the state of the unknown-flag.
      Stream is the stream used for output. */

prepare_test(File,Outputfile,Oldflag,Stream) :-
   seen,
   prolog_flag(unknown,Oldflag,fail),
   see(File),
   open(Outputfile, write, Stream),
   assert(output_stream(Stream)),
   user:set_num(tt_errors,0).


/* conclude_test(Oldflag,Stream) <-
      Resets I/O and Prolog flags after the test. Variables as in
      prepare_test. */

conclude_test(Oldflag,Stream) :-
   seen,
   retract(user:current_num(tt_errors,Errors)),
   nl(Stream),
   ( perf_test -> write(Stream,'/* ')
   ; true ),
   write(Stream,Errors),
   write(Stream,' errors found.'), nl(Stream),
   ( perf_test -> write(Stream,'*/'), nl(Stream)
   ; true ),
   star_line,
%  nl,
   write(Errors),
   write(' errors found.'), nl,
   close(Stream),
   retract(output_stream(Stream)),
   prolog_flag(unknown,_,Oldflag).


/* process_manual <-
      Runs in a loop, getting examples and testing them. */

process_manual :-
   get_next_example(Q,R),
   qr_to_term(Q,R,T),
   process_example(Q,R,T),
   process_manual.
process_manual.


/* process_chapter <-
      Same as process_manual, but only reads one chapter. */

process_chapter :-
   get_next_example_in_chapter(Q,R),
   qr_to_term(Q,R,T),
   process_example(Q,R,T),
   process_chapter.
process_chapter.


/******************************************************************/


/* process_example(Query,Result,Term) <-
      Tests the example given by
         Query: a Prolog query (character list)
         Result: a Prolog result (character list)
         Term: Query and Result combined (Prolog term)
      The first rule applies in case we are running
      a performance test, which requires different handling. */

process_example(Q,_,T) :-
   perf_test,
   !,
   calc_perf(Q,T).

process_example(Q,R,T) :-
   Q \== -1,
   R \== -1,
   T = not_a_term,
   errors_inc,
   output_stream(S),
   star_line(S),
   writelist(S,Q), nl(S),
   writelist(S,R), nl(S), nl(S),
   write(S,'Error: not a valid term'),
   nl(S),
   user:star_line,
   writelist(Q), nl,
   writelist(R), nl, nl,
   write('Error: not a valid term'),
   nl.

process_example(Q,R,T) :-
   Q \== -1,
   R \== -1,
   T \== not_a_term,
   user:star_line,
   writelist(Q), nl,
   writelist(R), nl, nl,
   test_example(T,N),
   test_output(Q,R,T,N).

process_example(Q,R,_) :-
   (Q = -1; R = -1).


/* test_example(T,N) <-
      Tests whether the term T holds.
      N returns the result of the test as
         N = 0: no errors
         N = 1: result fails
         N = 2: query fails
         N = 3: not tested */

test_example(T,3) :-
   T = (T1,_),
   dont_test2(T1),
   !.

test_example(T,N) :-
   T = (T1,T2),
   assert(user:manual_test_example(T1,T2)),
   call(T1),
   !,
   test_example2(T1,T2,N).

test_example(T,2) :-
   T = (_,_).

test_example(T,3) :-
   T \== (_,_),
   dont_test2(T),
   !.

test_example(T,0) :-
   T \== (_,_),
   call(T),
   !.

test_example(T,2) :-
   T \== (_,_).


test_example2(_,T2,0) :-
   call(T2).

test_example2(_,T2,1) :-
   \+ call(T2).


/* test_output(Q,R,T,N) <-
      outputs the result of the test with query Q, result R,
      term T and error code N */

test_output(_,_,(T1,T2),0) :- 
   write(T1), nl,
   write(T2), nl, nl,
   write('... Success'), nl.

test_output(_,_,T,0) :-
   T \== (_,_),
   write(T), nl, nl,
   write('... Success'), nl.

test_output(Q,R,(T1,T2),1) :-
   errors_inc,
   output_stream(S),
   star_line(S),
   writelist(S,Q), nl(S),
   writelist(S,R), nl(S), nl(S),
   write(S,T1), nl(S),
   write(S,T2), nl(S), nl(S),
   write(S,'... Failure'), nl(S),
   write(T1), nl,
   write(T2), nl, nl,
   write('... Failure'), nl.

test_output(Q,R,(T1,_),2) :-
   errors_inc,
   output_stream(S),
   star_line(S),
   writelist(S,Q), nl(S),
   writelist(S,R), nl(S), nl(S),
   write(S,T1), nl(S), nl(S),
   write(S,'... Failure'), nl(S),
   write(T1), nl, nl,
   write('... Failure'), nl.

test_output(Q,R,T,2) :-
   T \== (_,_),
   errors_inc,
   output_stream(S),
   star_line(S),
   writelist(S,Q), nl(S),
   writelist(S,R), nl(S), nl(S),
   write(S,T), nl(S), nl(S),
   write(S,'... Failure'), nl(S),
   write(T), nl, nl,
   write('... Failure'), nl.

test_output(_,_,(T1,T2),3) :-
   write(T1), nl,
   write(T2), nl, nl,
   write('... not tested'), nl.

test_output(_,_,T,3) :-
   T \== (_,_),
   write(T), nl, nl,
   write('... not tested'), nl.


/* dont_test2(Term) <-
      Succeeds if Term should not be tested.
      Either the whole functor or just a certain pattern
      can be excluded via dont_test facts. */

dont_test2(Term) :-
   functor(Term,Functor,_),
   dont_test(Functor),
   !.

dont_test2(Term) :-
   dont_test(Term).


/******************************************************************/


/* goto_next_example <-
      reads from input until it sees the line '@example'
      or EOF */

goto_next_example :-
   repeat,
      readline(L),
   ( L = "@example"
   ; L = end_of_file ),
   !,
   L = "@example".


/* get_next_example(Query,Result) <-
      reads the next example consisting of Query and optionally
      Result */

get_next_example(Q,R) :-
   goto_next_example,
   get_query(Q),
   bugfix(Q,R).

bugfix(Q,R) :-         /* If the Query is not to be tested,  */
   append(Q,".",Q1),   /* don't even try to read the result! */
   string_to_term(Q1,T1),
   dont_test2(T1),
   !,
   R = [].

bugfix(Q,R) :-
   get_next_example2(Q,R).


get_next_example2(Q,R) :-
   ( Q = []
   ; Q = -1 ),
   !,
   R = Q.

get_next_example2(Q,R) :-
   append(Q,".",Q1),
   string_to_term(Q1,T),
   no_free_vars(T),
   !,
   R = [].

get_next_example2(_,Result) :-
   get_result(Result).

sort_result(R1,R2) :-
   writeln(user,sort_result(R1,R2)),
   wait,
   functor(R1,'.',_),
   list_to_ord_set(R1,R2),
   !.
sort_result(R,R).


/* goto_chapter(Chapter) <-
      reads from input until the line '@chapter Chapter'
      or EOF is encountered */

goto_chapter(C) :-
   repeat,
      readline(L),
   ( append("@chapter ",C,L)
   ; L = end_of_file ),
   !,
   L \== end_of_file.


/* goto_next_example_in_chapter <-
      like goto_next_example, but only reads until the beginning
      of the next chapter */

goto_next_example_in_chapter :-
   repeat,
      readline(L),
   ( L = "@example"
   ; prefix("@chapter",L)
   ; L = end_of_file ),
   !,
   L = "@example".


/* get_next_example_in_chapter(Query,Result) <-
      like get_next_example, but uses goto_next_example_in_chapter
      instead of goto_next_example */

get_next_example_in_chapter(Q,R) :-
   goto_next_example_in_chapter,
   get_query(Q),
   get_next_example2(Q,R).


/******************************************************************/


/* get_query(Query) <-
      Reads a query Query from input.
      If an error occurs, then Query = -1.
      A query starts with the characters '| ?- ' at the beginning
      of a line and it ends with a '.' at the end of a line.
      A query can consist of several lines. */

get_query(Q) :-
   repeat,
      readline(L),
   ( append("| ?- ", _, L)
   ; L = "@end example"
   ; L = end_of_file ),
   !,
   get_query2(L,Q).


get_query2(Q1, Q) :-
   append("| ?- ", Q, Q2),
   append(Q2, ".", Q1),
   !.

get_query2("@end example",[]) :-
   !.

get_query2(end_of_file,-1) :-
   errors_inc,
   output_stream(S),
   star_line(S),
   write(S,'Error: Unexpected EOF in example block'),
   nl(S),
   flush_output(S),
   user:star_line,
   write('Error: Unexpected EOF in example block'),
   nl,
   !.

get_query2(Q1,Q) :-
   readline(L),
   get_query3(Q1,L,Q).


get_query3(Q1,"@end example",-1) :-
   errors_inc,
   output_stream(S),
   star_line(S),
   writelist(S,Q1),
   nl(S),
   write(S,'Error: Unexpected @end example in query'),
   nl(S),
   flush_output(S),
   user:star_line,
   writelist(Q1),
   nl,
   write('Error: Unexpected @end example in query'),
   nl,
   !.

get_query3(Q1,end_of_file,-1) :-
   errors_inc,
   output_stream(S),
   star_line(S),
   writelist(S,Q1),
   nl(S),
   write(S,'Error: Unexpected EOF in query'),
   nl(S),
   flush_output(S),
   user:star_line,
   writelist(Q1),
   nl,
   write('Error: Unexpected EOF in query'),
   nl,
   !.

get_query3(Q1,L,Q) :-
   string_trim(Q1,Q2),
   string_trim(L,L2),
   append(Q2,L2,Q3),
   get_query2(Q3,Q).


/******************************************************************/


/* get_result(Result) <-
      Reads a result Result from input.
      If an error occurs, then Result = -1.
      A result starts with the first non-empty line
      and it ends with a '?' at the end of a line.
      A result can consist of several lines. */

get_result(R) :-
   repeat,
      readline(L),
   L \== [],
   !,
   get_result2(L,R).


get_result2(R1,R) :-
   append(R,"?",R1),
   !.

get_result2("yes", []) :-
   !.
get_result2("@end example", []) :-
   !.

get_result2("@comment tt_ignore",R) :-
   repeat,
      readline(L),
   L = "@comment tt_end_ignore",
   !,
   get_result(R).

get_result2(end_of_file,-1) :-
   errors_inc,
   output_stream(S),
   star_line(S),
   write(S,'Error: Unexpected EOF in example block'),
   nl(S),
   flush_output(S),
   user:star_line,
   write('Error: Unexpected EOF in example block'),
   nl,
   !.

get_result2(R1,R) :-
   readline(L),
   get_result3(R1,L,R).


get_result3(R1,"@end example",-1) :-
   errors_inc,
   output_stream(S),
   star_line(S),
   writelist(S,R1),
   nl(S),
   write(S,'Error: Unexpected @end example in result'),
   nl(S),
   flush_output(S),
   user:star_line,
   writelist(R1),
   nl,
   write('Error: Unexpected @end example in result'),
   nl,
   !.

get_result3(R1,end_of_file,-1) :-
   errors_inc,
   output_stream(S),
   star_line(S),
   writelist(S,R1),
   nl(S),
   write(S,'Error: Unexpected EOF in result'),
   nl(S),
   flush_output(S),
   user:star_line,
   writelist(R1),
   nl,
   write('Error: Unexpected EOF in result'),
   nl,
   !.

get_result3(R1,L,R) :-
   string_trim(R1,R2),
   string_trim(L,L2),
   append(R2,L2,R3),
   get_result2(R3,R).


/******************************************************************/


/* qr_to_term(Query,Result,Term) <-
      takes two character strings Query and Result as input,
      concatenates them and converts them to a term Term */

qr_to_term(Q, R, T) :-
   Q \== [],
   R \== [],
   append(["(", Q, "),(", R, ")."], X),
   string_to_term(X, T).

qr_to_term(Q, R, T) :-
   Q \== [],
   R == [],
   append(Q, ".", X1),
   string_to_term(X1, T).

qr_to_term([], [], true).


/* string_to_term(String,Term) <-
      takes a string String and converts it to the corresponding
      term Term */

string_to_term(".",not_a_term) :-
   !.
string_to_term(String, Term) :-
   name(Name,String),
   term_to_atom(Term,Name).

/*
string_to_term(String, Term) :-
   prolog_flag(syntax_errors,Old_Flag,quiet),
   string_to_term_tmp_file(Tmp_File),
   telling(Old),
   tell(Tmp_File),
   writelist(String),
   nl,
   told,
   tell(Old),
   seeing(Old2),
   see(Tmp_File),
   ( ( read(Term),
       ! )
   ; Term = not_a_term ),
   seen,
   see(Old2),
   prolog_flag(syntax_errors,_,Old_Flag).
*/


/******************************************************************/


/* no_free_vars(Term) <-
      succeeds if Term contains no free variables */

no_free_vars(T) :-
   integer(T),
   !.

no_free_vars(T) :-
   nonvar(T),
   functor(T,_,0).

no_free_vars(T) :-
   nonvar(T),
   functor(T,_,N),
   N > 0,
   no_free_vars(T,1,N).

no_free_vars(T,N,N) :-
   arg(N,T,A),
   no_free_vars(A).

no_free_vars(T,I,N) :-
   I < N,
   arg(I,T,A),
   no_free_vars(A),
   J is I + 1,
   no_free_vars(T,J,N).


/******************************************************************/

/* readline(L) <-
      reads one line from input */

readline(L) :-
   get0(C),
   readline(C,L).

readline(10,[]) :-            /* LF */
   !.
readline(-1,end_of_file) :-   /* EOF */
   !.

readline(C,[C|Cs]) :-
   readline(Cs).


/* writelist(String) <-
      the character list String is written on standard output */

writelist([C|Cs]) :-
   put(C),
   writelist(Cs).
writelist([]).

writelist(X) :-
   \+ is_list(X),
   print(not_a_list).


/* writelist(Stream,String) <-
      the character list String is written on output stream
      Stream */

writelist(Stream,[C|Cs]) :-
   put(Stream,C),
   writelist(Stream,Cs).
writelist(_,[]).

writelist(Stream,X) :-
   \+ is_list(X),
   print(Stream,not_a_list).


/* string_trim(String,Trimmed) <-
      Trimmed is calculated from String by removing leading and
      trailing blanks */

string_trim([32|T],Y) :-
   string_trim(T,Y).

string_trim(X,Y) :-
   X = [H|_],
   H \== 32,
   last_element(X,32),
   append(Z,[32],X),
   string_trim(Z,Y).

string_trim([],[]).

string_trim(X,X) :-
   X = [H|_],
   H \== 32,
   last_element(X,L),
   L \== 32.


star_line(Stream) :-
   nl(Stream),
   write(Stream,'/*********************************'),
   write(Stream,'*********************************/'),
   nl(Stream),
   nl(Stream).
 

/* cat(File) <-
      copies the file File to standard output */

cat(File) :-
   see(File),
   repeat,
      readline(L),
      writelist(L),
      nl,
   L = end_of_file,
   seen.


/* errors_inc <-
      increments the error counter */

errors_inc :-
   user:current_num(tt_errors,Errors),
   E is Errors + 1,
   user:set_num(tt_errors,E).


/* get_date(Time) <-
      Use the UNIX command 'date' to get system time
      and date as a term. */

get_date(Time) :-
   date_tmp_file(File),
   name(File,FN),
   append("date > ",FN,Cmd),
   name(Command,Cmd),
   unix(system(Command)),
   seeing(Old),
   see(File),
   readline(T),
   seen,
   see(Old),
   name(Time,T).


/******************************************************************/


/* process_perf_file <-
      runs in a loop,
      getting queries and computing their runtime */

process_perf_file :-
   get_query(Q),
   append(Q,".",Q2),
   string_to_term(Q2,T),
   calc_perf(Q2,T),
   process_perf_file.
process_perf_file.


/* calc_perf(Query,Term) <-
      calcuates the runtime of Term */

calc_perf(Query,Term) :-
   Term == not_a_term,
   errors_inc,
   output_stream(S),
   write(S,'/*'),
   nl(S),
   writelist(S,Query), nl(S), nl(S),
   write(S,'Syntax Error'), nl,
   write(S,'*/'),
   nl(S), nl(S),
   user:star_line,
   writelist(Query), nl, nl,
   write('Syntax Error'), nl.

calc_perf(Query,Term) :-
   Term \== not_a_term,
   output_stream(S),
   user:start_timer(perf),
   user:Term,
   !,
   user:stop_timer(perf,Time),

   current_host(Host),
   get_date(Date),
   writeq(S,performance_test(Term,Time,Date,Host)),
   write(S,'.'),
   nl(S), nl(S),

   user:star_line,
   writelist(Query), nl,
   write(Term), nl, nl,
   write(Time),
   write(' seconds.'), nl.

calc_perf(Query,Term) :-
   user:stop_timer(perf),
   errors_inc,
   output_stream(S),
   write(S,'/*'),
   nl(S),
   writelist(S,Query), nl(S), nl(S),
   write(S,Term), nl(S), nl(S),
   write(S,'Error: failure.'), nl(S),
   write(S,'*/'),
   nl(S), nl(S),
   user:star_line,
   writelist(Query), nl, nl,
   write('Error: failure.'), nl.


/******************************************************************/


