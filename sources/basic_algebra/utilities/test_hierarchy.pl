

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Test Hierarchy                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* test_hierarchy(Vertices, Edges) <-
      */

test_hierarchy :-
   test_hierarchy(Vertices, Edges),
   vertices_edges_to_picture_bfs(Vertices, Edges).

test_hierarchy(Vertices, Edges) :-
   findall( X-Y,
      ( test_unit_to_domains(Unit, Domains),
        concat('U_', Unit, X),
        member(Domain, Domains),
        concat('D_', Domain, Y) ),
      Edges_1 ),
   findall( Y-Z,
      ( clause(test(Domain:Z, _), _),
        concat('D_', Domain, Y) ),
      Edges_2 ),
   append(Edges_1, Edges_2, Edges_3),
   sort(Edges_3, Edges),
   edges_to_vertices(Edges, Vertices_2),
   maplist( test_vertex_prepare,
      Vertices_2, Vertices ).

test_vertex_prepare(Vertex, Vertex-X-circle-red-medium) :-
   concat('U_', X, Vertex).
test_vertex_prepare(Vertex, Vertex-X-circle-blue-medium) :-
   concat('D_', X, Vertex).
test_vertex_prepare(Vertex, Vertex).


/* test_unit_to_domains(Unit, Domains) <-
      */

test_unit_to_domains(basic_algebra, [
   meta_predicates,
   set_trees ]).
   
test_unit_to_domains(xml, [
   ascii_text,
   hash_table,
   field_notation,
   graph_notation,
   xml,
   xml_pillow ]).

test_unit_to_domains(databases, [
   database_design,
   deductive_databases,
   mysql_odbc ]).

test_unit_to_domains(projects, [
   cardinality_constraints,
   evu,
   genetics,
   ontologies,
   selli,
   teaching ]).

test_unit_to_domains(development, [
   dda,
   graphs_to_xpce,
   jaml,
   program_analysis,
   rar,
   refactoring,
   slicing,
   structural_recursion,
   visur ]).

test_unit_to_domains(stock_tool, [
   energy,
   pattern_search,
   stock_tool ]).

test_unit_to_domains(nm_reasoning, [
   nmr ]).


/******************************************************************/


