

/******************************************************************/
/***                                                            ***/
/***          Declare:  Test Predicates                         ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      test/1, test/2, test/3.

:- dislog_variable_set(dislog_domain_name, '').


/*** interface ****************************************************/


/* dislog_test_tool <-
      */

dislog_test_tool :-
   dislog_flag_set(xpce_mode, yes),
   dislog_frame_generic([
      header: 'Declare - Test Tool',
      above: [ ],
      interface: [
         'Domain' - dislog_domain_name ],
      below: [
         'All Domains' -
            test_all,
         'Selected Domain' -
            test_selected_domain,
         'Test Overview' -
            test_overview - below ],
      close_button: [
         'Close' - right ] ]).

test_selected_domain :-
   dislog_dialog_variables( get,
      [ 'Domain' - D ] ),
   test_domains(Test_Domains),
   findall( Domain,
      ( member(Domain, Test_Domains),
        concat(D, _, Domain) ),
      Domains ),
%  dislog_dialog_variables( send,
%     [ 'Domain' - Domain ] ),
   test_all_domains(Domains).


/* test_all <-
      */

test_all :-
   findall( Domain,
      ( clause(test(Item, _), _),
        test_item_to_test_domain(Item, Domain) ),
      Domains_2 ),
   list_to_ord_set(Domains_2, Domains),
   test_all_domains(Domains).


/* test_all_domains(Domains) <-
      */

test_all_domains(Domains) :-
%  maplist( 'Testing all Domains', test_all,
   maplist( test_all,
      Domains, Results_Lists ),
   append(Results_Lists, Results), 
   html_display_table_for_tests_and_results(Results).


/* test_overview <-
      */

test_overview :-
   test_hierarchy,
   test_domains(Domains),
   findall( [Domain_2, N],
      ( member(Domain, Domains),
        test_domain_to_tests(Domain, Tests),
        length(Tests, N),
        term_to_atom(Domain, Domain_2) ),
      Rows ),
   !,
   xpce_display_table(_, _, 'Declare Tests',
      ['Domain', 'Tests'], Rows ).
%  html_display_table('Declare Tests', ['Domain', 'Tests'], Rows).

test_domains(Domains) :-
   findall( Domain,
      ( clause(test(Item, _), _),
        test_item_to_test_domain(Item, Domain) ),
      Domains_2 ),
   list_to_ord_set(Domains_2, Domains),
   !.

test_domain_to_tests(Domain, Tests) :-
   findall( test(Item, Test),
      ( clause(test(Item, Test), _),
        test_item_to_test_domain(Item, Domain) ),
      Tests ),
   !.

test_item_to_test_domain(Domain:_, Domain) :-
   !.
test_item_to_test_domain(Domain, Domain).


/* test_all(Domain) <-
      */

test_all_2(Domain) :-
   findall( test(Domain, Test),
      clause(test(Domain, Test), _),
      Tests ),
   test_all_common(Tests, Domain).

test_all(Domain) :-
   test_all(Domain, Results),
   html_display_table_for_tests_and_results(Results),
   !.

test_all(Domain, Results) :-
   write(user, '% testing ... '),
   writeln(user, Domain),
   test_domain_to_tests(Domain, Tests),
   test_all_common(Tests, Domain, Results).

test_all_common(Tests, Domain) :-
   test_all_common(Tests, Domain, _).

test_all_common(Tests, Domain, Results) :-
   concat(['Testing ', Domain, ' ...'], Label),
   ( dislog_flag_get(xpce_mode, yes) ->
     maplist_with_status_bar(
        maplist_with_status_bar_for_test, Label, writeln_and_call,
        Tests, Results )
   ; maplist( writeln_and_call,
        Tests, Results ) ),
   sublist( test_has_result(yes),
      Results, Tests_1 ),
   maplist( test_and_result_to_test,
      Tests_1, Tests_Yes ),
   sublist( test_has_result(no),
      Results, Tests_2 ),
   maplist( test_and_result_to_test,
      Tests_2, Tests_No ),
   test_report(Tests_Yes, succeeded),
   test_report(Tests_No, failed).

test_report(Tests, Result_Type) :-
   length(Tests, N),
   write_list(['*** ', N, ' tests ', Result_Type, ' ***']), nl,
   writeln_list(Tests).

writeln_and_call(Test, Test-yes) :-
%  writeln(user, Test),
%  call(Test),
   catch(Test, _, fail),
   !.
writeln_and_call(Test, Test-no).

test_has_result(Boolean, _-Boolean).

test_and_result_to_test(Test-_, Test).


/* manual_test <-
      */

mt :-
   manual_test.
mt2 :-
   manual_test_2.

manual_test_2 :-
   test_manual_file(File),
   manual_test(File).

tt :-
   declare_unit_tests.

declare_unit_tests :-
   dislog_variable_get(output_path, test_tool_results, File),
   dislog_variable_set(ddbase_show_tables_xpce, no),
   predicate_to_file( File,
      ( dislog_variable_switch(fn_mode, Mode, fn),
        test_selected_domains(2),
        dislog_variable_set(fn_mode, Mode) ) ),
   dislog_variable_set(ddbase_show_tables_xpce, yes),
   write_list(user, ['% ---> ', File]), nl(user),
   !.

tt2 :-
   test_all(mysql_odbc),
   test_all(rule_analysis),
%  dislog_variable_get(output_path, test_tool_results, File),
%  predicate_to_file( File,
%     ( test_all(mysql_odbc),
%       test_all(rule_analysis) ) ),
%  write_list(user, ['% ---> ', File]), nl(user),
   !.

ttt :-
   tt_no_xpce.

tt_no_xpce :-
   dislog_flag_switch(xpce_mode, Mode, no),
   tt,
   dislog_flag_set(xpce_mode, Mode).


/* test_selected_domains(N) <-
      */

test_selected_domains(1) :-
   selected_test_domains(Selected_Domains),
   ( foreach(Domain, Selected_Domains), foreach(Row, Rows) do
        Goal = test_all(Domain, Results),
        measure(tt_timer, Goal, Time),
        test_results_to_one_row(Domain, Results, Time, Row) ),
   xpce_display_table_for_tests(_, _,
      'Tests', ['Domain', 'Time', 'Yes', 'No'], Rows).

test_selected_domains(2) :-
   dislog_flag_get(xpce_mode, yes),
   !,
   selected_test_domains(Selected_Domains),
   maplist( test_domain_to_empty_row,
      Selected_Domains, Empty_Rows ),
   xpce_display_table(_, Table, 'Declare - Unit Tests',
      ['Domain', 'Tests', 'Time', 'Yes', 'No'], Empty_Rows),
   ( foreach(Domain, Selected_Domains), foreach(Row, Rows) do
        Goal = test_all(Domain, Results),
        measure(tt_timer, Goal, Time),
        test_results_to_one_row(Domain, Results, Time, Row),
        xpce_test_table_update(Table, Domain, Row) ),
   xpce_display_table_for_tests(_, _,
      'Declare - Unit Tests', ['Domain', 'Time', 'Yes', 'No'], Rows),
   xml_display_table_for_tests(Rows).
test_selected_domains(2) :-
   selected_test_domains(Selected_Domains),
   ( foreach(Domain, Selected_Domains), foreach(Row, Rows) do
        Goal = test_all(Domain, Results),
        measure(tt_timer, Goal, Time),
        test_results_to_one_row(Domain, Results, Time, Row) ),
   xml_display_table_for_tests(user, Rows).

xml_display_table_for_tests(File, Rows) :-
   predicate_to_file( File,
      xml_display_table_for_tests(Rows) ).

xml_display_table_for_tests(Rows) :-
   ( foreach(Row, Rows), foreach(Item, Items) do
        Row = [Domain|Xs],
        pair_lists(:, ['Time', 'Yes', 'No'], Xs, Ys),
        Item = Domain:Ys:[] ),
   nl, dwrite(xml, tests:Items), nl.

test_domain_to_empty_row(Domain, Row) :-
   test_domain_to_tests(Domain, Tests),
   length(Tests, N),
   Row = [Domain, N, '', '', ''].

xpce_test_table_update(Table, Domain, Row) :-
   Conditions = [1=Domain],
   Row = [_|Values],
   substitute_tuple(Values, [[0, '-']], Values_2),
   pair_lists(=, [3, 4, 5], Values_2, Updates),
   xpce_table_update_row(Table, 5, Conditions, Updates).


/* selected_test_domains(Selected_Domains) <-
      */

selected_test_domains(Selected_Domains) :-
   Selected_Domains = [
      ascii_text, cardinality_constraints,
      mysql_odbc,
      database_design, dda, evu,
%     xml,
      fn_query, gn_query,
      hash_table,
      meta_predicates,
      nmr_manual,
%     program_analysis,
      test_test ].
%     xml_pillow


/* test_results_to_one_row(Domain, Results, Time, Row) <-
      */

test_results_to_one_row(Domain, Results, Time, Row) :-
   findall( Test,
      member(Test-yes, Results),
      Tests_Yes ),
   length(Tests_Yes, N_Yes),
   findall( Test,
      member(Test-no, Results),
      Tests_No ),
   length(Tests_No, N_No),
   Row = [Domain, Time, N_Yes, N_No].


test(test_test, 1) :-
   fail.
test(test_test, 2) :-
   wait_seconds(3).


/*** implementation ***********************************************/


/* html_display_table_for_tests_and_results(Test_Results) <-
      */

html_display_table_for_tests_and_results(Test_Results) :-
   Title = 'Test Results',
   tests_and_results_to_table(Test_Results, Table),
   Document = html:[]:[
%     head:[]:[title:Title],
      body:[]:[Table]],
   dislog_variable_get(output_path, Results),
   concat(Results, 'table.html', File),
   predicate_to_file( File,
      field_notation_to_xml(0, Document) ),
   html_file_to_display(Title, File, size(350, 350)).

tests_and_results_to_table(Test_Results, Table) :-
   html_create_table_row(th, '#dfdfff',
      ['Domain', 'Group', 'Name'], Header),
   maplist( test_and_result_to_table_row,
      Test_Results, Rows),
   Table = table:[
%     'BORDER':0, cellspacing:1, 'BGCOLOR':'#e9e9e9']:[
      'BORDER':0, cellspacing:1, 'BGCOLOR':'#ffffff']:[
      Header|Rows].

test_and_result_to_table_row(test(X:Y, N)-B, Row) :-
   test_and_result_to_table_row_colour(B, Background_Color),
   html_create_table_row(td, Background_Color, [X, Y, N], Row ).
test_and_result_to_table_row(test(X, N)-B, Row) :-
   test_and_result_to_table_row_colour(B, Background_Color),
   html_create_table_row(td, Background_Color, [X, '', N], Row ).

% test_and_result_to_table_row_colour(yes, '#e9e9e9').
test_and_result_to_table_row_colour(yes, '#e9e9ff').
test_and_result_to_table_row_colour(no, '#ffd9d9').


/* test_find_successful_tests(Domain, Successful_Tests) <-
      */

test_find_successful_tests(Domain, Successful_Tests) :-
   findall( test(Domain, Y),
      test(Domain, Y),
      Successful_Tests ).


/* test_calls(Goals) <-
      */

test_calls(Goals) :-
   checklist( test_call,
      Goals ).

test_call(Goal) :-
   writeln('--- begin of test ---'),
   writeln(Goal),
   ( ( call(Goal),
       ! )
   ; ( writeln('--- end of test ---'),
       fail ) ),
   writeln(Goal),
   writeln('--- end of test ---').


manual_test_examples_show :-
   findall( [test(nm_reasoning, Name)]-[X, Y],
      ( manual_test_example(X, Y),
        X =.. [Name|_] ),
      Tests ),
   dislog_variable_get(home, Declare),
   dislog_variable_get(output_path, Path),
   concat([Declare, '/', Path, nm_reasoning_tests], File),
   dsave(Tests, File).


/******************************************************************/


