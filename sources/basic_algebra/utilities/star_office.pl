

/******************************************************************/
/***                                                            ***/
/***       DisLog:  Star Office Interface                       ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* csv_file_projection(Ns, File_1, File_2) <-
      */

csv_file_projection(Ns, Separator, File_1, File_2) :-
   dread(csv(Separator), File_1, Rows_1),
   table_projection(Ns, Rows_1, Rows_2),
   dwrite(csv(Separator), File_2, Rows_2).


/* csv_file_to_display(Separator, File) <-
      */

csv_file_to_display(Separator, File) :-
   dread(csv(Separator), File, [Attributes|Rows]),
   openoffice_display_table(Attributes, Rows).


/* openoffice_display_table(Attributes, Rows) <-
      */

openoffice_display_table(Attributes, Rows) :-
   dislog_variable_get(output_path, Path),
   concat(Path, 'openoffice_tmp.csv', File),
   openoffice_display_table(File, Attributes, Rows).


/* openoffice_display_table(File, Attributes, Rows) <-
      */

openoffice_display_table(File, Attributes, Rows) :-
   write_star_office_file([Attributes|Rows], File),
   concat(['ooffice ', File, ' &'], Command),
   us(Command).


/* write_star_office_file(Rows, File) <-
      */

write_star_office_file(Rows, File) :-
   write_star_office_file(':', Rows, File).

write_star_office_file(Separator, Rows, File) :-
   predicate_to_file( File,
      write_star_office_table(Separator, Rows) ).

write_star_office_table(Separator, Rows) :-
   checklist(
      write_list_with_separators(
         write_with_separator_for_csv(Separator), writeln ),
      Rows ).

write_with_separator_for_csv(Separator, X) :-
   write(X),
   write(Separator).


/* read_star_office_file(File_Name, File_Contents) <-
      */

dread(xls, File_Name, File_Contents) :-
   dread(csv, File_Name, File_Contents).

dread(csv, File_Name, File_Contents) :-
   read_star_office_file(File_Name, File_Contents).

dread(csv(Separator, Delimiter), File_Name, File_Contents) :-
   dread(csv(Separator), File_Name, Tuples),
   csv_tuples_strip_at_both_ends(Delimiter, Tuples, File_Contents).

dread(csv(Separator), File_Name, File_Contents) :-
   name(Separator, Xs),
   append(Xs, [10, -1], Separators),
   read_star_office_file(
      File_Name, Separators, File_Contents).

read_star_office_file(File_Name, File_Contents) :-
   read_star_office_file(
      File_Name, [58, 10, -1], File_Contents).

read_star_office_file(File_Name, Separators, File_Contents) :-
   see(File_Name),
   read_star_office_file_loop(Separators, [], Inverse_File),
   reverse(Inverse_File, File_Contents),
   seen.

read_star_office_file_loop(Separators, Sofar, File) :-
   read_star_office_line(Separators, Line, Stop),
%  write(user, '.'), ttyflush,
   ( ( Stop = -1,
       File = Sofar )
   ; read_star_office_file_loop(
        Separators, [Line|Sofar], File) ).
   

read_star_office_line(Separators, Line, Stop) :-
   read_star_office_line(Separators, [], Inverse_Line, Stop),
   reverse(Inverse_Line, Line).

read_star_office_line(Separators, Sofar, Line, Stop) :-
   read_star_office_item(Separators, Item, Stop_Item),
   New_Sofar = [Item|Sofar],
   ( ( ( Stop_Item = 10
       ; Stop_Item = -1 ),
       Line = New_Sofar,
       Stop = Stop_Item )
   ; read_star_office_line(
        Separators, New_Sofar, Line, Stop) ).
   

read_star_office_item(Separators, Item, Stop) :-
   read_star_office_item(Separators, [], Item_1, Stop),
   reverse(Item_1, Item_2),
   list_exchange_elements([",."], Item_2, Item_3),
%  Item_2 = Item_3,
   atom_codes(Item, Item_3).

read_star_office_item(Separators, Sofar, Item, Stop) :-
   get0(C),
   ( ( member(C, Separators), !,
       Item = Sofar,
       Stop = C )
   ; read_star_office_item(
        Separators, [C|Sofar], Item, Stop) ).


/* csv_tuples_strip_at_both_ends(C, Tuples_1, Tuples_2) <-
      */

csv_tuples_strip_at_both_ends(C, Tuples_1, Tuples_2) :-
   maplist( csv_names_strip_at_both_ends(C),
      Tuples_1, Tuples_2 ).
   
csv_names_strip_at_both_ends(C, Names_1, Names_2) :-
   maplist( csv_name_strip_at_both_ends(C),
      Names_1, Names_2 ).


/* csv_name_strip_at_both_ends(C, Name_1, Name_2) <-
      */

csv_name_strip_at_both_ends(C, Name_1, Name_2) :-
   concat(C, Name, Name_1),
   concat(Name_2, C, Name),
   !.
csv_name_strip_at_both_ends(_, Name, Name).
 

/******************************************************************/


