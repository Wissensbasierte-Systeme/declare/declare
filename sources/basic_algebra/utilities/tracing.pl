

/******************************************************************/
/***                                                            ***/
/***             DisLog :  Tracing - Tool                       ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(ddk_spy_level, 1).


/*** interface ****************************************************/


/* hidden(Goal) <-
      the goal Goal is executed and the output is send to the
      DisLog variable control_data_file. */

hidden(Goal) :-
   dislog_variable(control_data_file,File1),
   switch(File,File1),
   call(Goal),
   switch(File).


/* hidden(Trace_Level,Goal) <-
      if the current trace level I is at most Trace_Level,
      then the goal Goal is executed and the output is send 
      to the DisLog variable control_data_file. */

hidden(1,Goal) :-
   current_num(trace_level,1),
   !,
   set_num(trace_level,a1),
   nl, hidden(Goal),
   set_num(trace_level,1).
hidden(1,Goal) :-
   !,
   call(Goal).
hidden(Trace_Level,Goal) :-
   ( ( current_num(trace_level,a1),
       I = 1 )
   ; current_num(trace_level,I) ), 
   I =< Trace_Level,
   !,
   nl, hidden(Goal).
hidden(Trace_Level,Goal) :-
   ( ( current_num(trace_level,a1),
       I = 1 )
   ; current_num(trace_level,I) ), 
   I > Trace_Level,
   !,
   call(Goal).
hidden(_,Goal) :-
   call(Goal).


/* dnotrace, dtrace, dextrace <-
      Sets the current value of the DisLog flag trace_level to
      1,4,6, which yields no trace, trace, extensive trace of
      DisLog operators. */

dnotrace :-
   set_num(trace_level,1),
   write_list([
      '{The DisLog trace is switched off}', '\n',
      'trace_level = 1' ]).
dtrace :-
   set_num(trace_level,4),
   write_list([
      '{DisLog will trace computations}', '\n',
      'trace_level = 4' ]).
dextrace :-
   set_num(trace_level,6),
   write_list([
      '{DisLog will trace computations extensively}', '\n',
      'trace_level = 6' ]).


/* begin_trace(File) <-
      the current standard output is remembered as File,
      if the current DisLog trace level is 1, then tracing is 
      activated, i.e. standard output is redirected to the
      DisLog variable control_data_file 
      and the trace level is set to a1. */

begin_trace(File) :-
   current_num(trace_level,1),
   set_num(trace_level,a1),
   !,
   nl, 
   dislog_variable(control_data_file,File1), 
   switch(File,File1).
begin_trace(File) :-
   telling(File).


/* end_trace(File) <-
      if the current DisLog trace level is a1, i.e. tracing is 
      activated, then standard output is set back to File and
      the trace level is set back to 1. */

end_trace(File) :-
   current_num(trace_level,a1),
   !,
   switch(File), nl,
   set_num(trace_level,1).
end_trace(_) :-
   nl.


/* begin_hiding(1, B, File) <-
      the current standard output is remembered as File,
      if the current DisLog trace level is 1, then hiding is
      activated, i.e. standard output is redirected to the DisLog
      variable control_data_file, the trace level is set to a1,
      and B is set to y, otherwise B is set to n. */

begin_hiding(1, y, File) :-
   dislog_flag_switch(trace_level, 1, a1), nl, 
   dislog_variable(control_data_file, File1), 
   switch(File, File1),
   !.
begin_hiding(1, n, _).


/* end_hiding(1, B, File) <-
      if the current DisLog trace level is a1 and B is y,
      i.e. hiding is activated, then standard output is set 
      back to File and the trace level is set back to 1. */

end_hiding(1,y,File) :-
   dislog_flag_switch(trace_level, a1, 1),
   switch(File), nl,
   !.
end_hiding(1, n, _).


/* ddk_spy(Goal) <-
      */

ddk_spy(_) :-
   dislog_variable_get(ddk_spy_level, N),
   N < 4,
   !.
ddk_spy(Goal) :-
   call(Goal).


/* dspy(Goal) <-
      nothing is done if the current DisLog trace level is
      I = 1 or I = a1 or I = 2,
      otherwise Goal is executed,
      if I = 6, DisLog additionally waits for a <return>. */

dspy(_) :-
   !.
dspy(_) :-
   current_num(trace_level,I),
   ( I = a1
   ; I = 1
   ; I = 2),
   !.
dspy(Goal) :-
   current_num(trace_level,I),
   2 < I, I < 6,
   call(Goal).
dspy(Goal) :-
   current_num(trace_level,6),
   call(Goal),
   wait.

 
/* dspy_tree(Tree) <-
      for debugging reasons. */
 
dspy_tree(_).
% dspy_tree(Tree) :-
%    dportray(tree,Tree), wait.


/* wait <-
      forces the system to wait for an input,
      e.g. <Return>. */

% wait :-
%    true.
wait :-
   get0(_).


/* wait_seconds(Seconds) <-
      */

wait_seconds(Seconds) :-
   statistics(real_time, [Start, _]),
   wait_seconds(Seconds, Start).

wait_seconds(Seconds, Start) :-
   statistics(real_time, [Sec, _]),
   Sec - Start >= Seconds,
   !.
wait_seconds(Seconds, Start) :-
   wait_seconds(Seconds, Start).


/* wait_milli_seconds(Seconds) <-
      */

wait_milli_seconds(Seconds) :-
   statistics(system_time, [Start, _]),
   Sec is 5 * Seconds / 17,
   wait_milli_seconds(Sec, Start).

wait_milli_seconds(Seconds, Start) :-
   statistics(system_time, [Sec, _]),
   Sec - Start >= Seconds,
   !.
wait_milli_seconds(Seconds, Start) :-
   wait_milli_seconds(Seconds, Start).


/* silent, loud <-
      sets the current standard output to the file 
      'results/consulting' or to 'user', respectively. */

silent :-
   tell('results/consulting').
loud :-
   tell('user').


/* begin_protocol(Filename) <-
      begins a protocolled execution of a DisLog operator,
      the standard output is switched to the file Filename in the
      directory Path given by the Dislog variable data_path. */

begin_protocol(Filename) :-
   dislog_variable(data_path,Path),
   concat_atoms([Path,Filename],Pathname),
   tell(Pathname).


/* end_protocol <-
      ends a protocolled execution of a DisLog operator,
      the standard output is switched back to user. */

end_protocol :-
   told.


lispred :-
   listing(special_predicates),
   special_predicates(Predicates),
   listing_all(Predicates).
  

/******************************************************************/


