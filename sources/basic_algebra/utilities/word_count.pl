

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Word Count                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* wc(File, Lines, Words, Bytes) <-
      */

wc(File, Lines, Words, Bytes) :-
   size_file(File, Bytes),
   open(File, read, Stream),
   wc_sub(Stream, 0, Lines, 0, Words),
   close(Stream),
   !.


/* word_separator(Separator) <-
      */

word_separator(Separator) :-
   member(Separator, [0, 9, 10, 13, 32]).


/*** implementation ***********************************************/


/* wc_sub(Stream, Lines_1, Lines_2, Words_1, Words_2) <-
      */

wc_sub(Stream, Lines_1, Lines_2, Words_1, Words_2) :-
   read_line_to_codes(Stream, Codes),
   lines_and_words(
      Codes, Stream, Lines_1, Lines_2, Words_1, Words_2).

lines_and_words(end_of_file, _, Lines, Lines, Words, Words) :-
   !.
lines_and_words(
      Codes, Stream, Lines_1, Lines_3, Words_1, Words_3) :-
   Lines_2 is Lines_1 + 1,
   count_words(Codes, 0, Words),
   Words_2 is Words_1 + Words,
   wc_sub(Stream, Lines_2, Lines_3, Words_2, Words_3).


/* count_words(Codes, Words_1, Words_2) <-
      */

count_words([Code|Codes], Words_1, Words_2) :-
   word_separator(Code),
   count_words(Codes, Words_1, Words_2).
count_words([_|Codes_1], Words_1, Words_3) :-
   Words_2 is Words_1 + 1,
   skip_characters(Codes_1, Codes_2),
   count_words(Codes_2, Words_2, Words_3).
count_words([], W, W).

skip_characters([Code|Codes], Codes) :-
   word_separator(Code),
   !.
skip_characters([_|Codes_1], Codes_2) :-
   skip_characters(Codes_1, Codes_2),
   !.
skip_characters([], []) :-
   !.


/******************************************************************/


