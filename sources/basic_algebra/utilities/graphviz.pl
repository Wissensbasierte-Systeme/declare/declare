

/******************************************************************/
/***                                                            ***/
/***           DDK:  Graphviz                                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* graphviz(Call) <-
      */

graphviz(term(Term)) :-
   term(Term),
   us('mv term.dot results/term.dot'),
   us('dot -Tjpg -oresults/term.jpg results/term.dot'),
   us('gimp results/term.jpg').
graphviz(sld Goal) :-
   sld(Goal),
%  sld(Goal, 6),
   us('mv sld.dot results/sld.dot'),
   us('dot -Tjpg -oresults/sld.jpg results/sld.dot'),
   us('gimp results/sld.jpg').

 
/******************************************************************/


