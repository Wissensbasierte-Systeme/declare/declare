

/******************************************************************/
/***                                                            ***/
/***           DisLog:  LaTeX Pictures                          ***/
/***                                                            ***/
/******************************************************************/


:- module( latex_pictures, [
      latex_picture_self_loop/5 ] ).


/*** interface ****************************************************/


/* latex_picture_self_loop(Base, Factor, Power, Arrow, Loop) <-
      */

latex_picture_self_loop(Base, Factor, Power, Arrow, Loop) :-
   Vectors_1 = [
      [+0.3,+0.3], [-0.2,-0.1], [-0.7,-0.2],
      [-1.2, 0.0],
      [-0.7,+0.2], [-0.2,+0.1], [+0.3,-0.3] ],
   Vector_1 = [-0.18,-0.1],
   sqrt(0.5, S),
   Matrix_1 = [[S,S],[-S,S]],
   matrix_power(Power, Matrix_1, Matrix_2),
   matrix_multiply(Factor, Matrix_2, Matrix),
   writeln(user, Matrix),
   affine_transformations(Matrix, Base, Vectors_1, Vectors_2),
   affine_transformation(Matrix, Base, Vector_1, Vector_2),
   Loop = Vectors_2-Vector_2,
   writeln(user, Arrow-Loop),
   write_self_loop(Vectors_2, Vector_2, Arrow).

affine_transformations(Matrix, Offset, Vectors_1, Vectors_2) :-
   maplist( affine_transformation(Matrix, Offset),
      Vectors_1, Vectors_2 ).


/* write_self_loop(Vectors, Vector, Arrow) <-
      */

write_self_loop(Vectors, Vector, Arrow) :-
   M = latex_pictures,
   write('\\tagcurve('),
   write_list_with_separators(
      M:write_point_comma, M:write_point, Vectors ),
   writeln(')'),
   write('\\put ('), write_point(Vector),
   write('){\\vector('), write_point(Arrow), writeln('){0}}').

write_point([X, Y]) :-
   round(X, 2, A),
   round(Y, 2, B),
   write_list([A,',',B]).
write_point_comma([X, Y]) :-
   write_point([X, Y]), write(', ').


/******************************************************************/


