

/******************************************************************/
/***                                                            ***/
/***          DDK:  Tests for Threads                           ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(threads, 1) :-
   Goal = catch( mysql_database_schema_to_xml(company, _Xml),
      Error, writeln(Error) ),
   thread_create(Goal, _, [detached(true)]),
   thread_create(Goal, _, [detached(true)]).

test(threads, 2) :-
   for(_, 1, 6) do
      test(threads, 3).

test(threads, 3) :-
   star_line,
   Mutex = mysql_information_schema_mutex,
   mutex_create_(Mutex),
   G = ( mysql_database_schema_to_xml(company, Xml),
      ':='(Tag, Xml/'::'(tag,'*')),
      writeln(user, tag(Tag)), star_line ),
   Goal = with_mutex(Mutex, catch(G, Error, writeln(Error))),
   thread_create(Goal, _, [detached(true)]),
   thread_create(Goal, _, [detached(true)]).

test(mutex, 1) :-
   Mutex = mysql_information_schema_mutex,
   mutex_create_(Mutex),
   Goal = (
      mutex_lock(Mutex),
      wait_seconds(10), writeln(finished),
      mutex_unlock(Mutex) ),
   thread_create(Goal, _, []),
   current_mutex(Mutex, Id, Count),
   writeln(user, current_mutex(Mutex, Id, Count)),
%  wait,
   with_mutex(Mutex, mysql_database_schema_to_xml(company, Xml)),
   writeln(user, Xml).


/******************************************************************/


