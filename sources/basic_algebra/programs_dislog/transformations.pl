

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Program Transformations               ***/
/***                                                            ***/
/******************************************************************/


:- op(400,fx,'E').


/*** interface ****************************************************/


/* partial_evaluation(Program1,Program2) <-
      A range-restricted, disjunctive-normal logic program 
      Program1 is partially evaluated.  
      The result is a ground, disjunctive-normal logic program
      Program2, where all positive bodies are empty,
      a so-called pbe-program. */

partial_evaluation(Database_Id) :-
   dconsult(Database_Id,Program),
   partial_evaluation_cc(Program,Pbe_Program),
   dportray(lp,Pbe_Program).

partial_evaluation(Program1,Program2) :-
   hidden(
      partial_evaluation_verbose(Program1,Program2) ).

partial_evaluation_verbose(Program1,Program2) :-
   announcement('partial_evaluation'), nl,
   evidential_transformation(Program1,E_Program),
   tree_tps_fixpoint_iteration(E_Program,E_Tree),
   tree_to_state(E_Tree,E_State),
%  minimal_model_state(E_Program,E_State),
   evidential_transformation_reverse(E_State,Program2).
 
partial_evaluation_cc(Program1,Program2) :-
   announcement('partial_evaluation_cc'), nl,
   evidential_transformation(Program1,E_Program),
   evidential_program_to_cc(E_Program,E_Program_CC),
   tps_fixpoint_cc(E_Program_CC,E_State_CC),
   evidential_program_to_cc_reverse(E_State_CC,E_State),
   evidential_transformation_reverse(E_State,Program2).


/* gl_transformation(Program1,I,Program2) <-
      The Gelfond-Lifschitz transformation of the ground DNLP
      Program1 w.r.t. the Herbrand interpretation I yields the
      ground DLP Program2. */

gl_transformation_non_ground(Program1,I,Program2) :-
   ground_transformation(Program1,Program),
   gl_transformation(Program,I,Program2).

gl_transformation(Program1,I,Program2) :-
   gl_transformation(Program1,I,[],Program3),
   list_to_ord_set(Program3,Program2).

gl_transformation([Rule|Rules],I,Acc,Program) :-
   parse_dislog_rule(Rule,Head,PosBody,NegBody),
   list_to_ord_set(NegBody,OrdNegBody),
   ( ( ord_disjoint(OrdNegBody,I),
       simplify_rule(Head-PosBody,New_Rule),
       NewAcc = [New_Rule|Acc] )
   ; NewAcc = Acc ),
   gl_transformation(Rules,I,NewAcc,Program).
gl_transformation([],_,Program,Program).


evidential_gl_transformation(Program1,I,Program2) :-
   evidential_gl_transformation(Program1,I,[],Program2).

evidential_gl_transformation([Rule|Rules],I,Acc,Program) :-
   parse_dislog_rule(Rule,Head,PosBody,NegBody),
   list_to_ord_set(NegBody,OrdNegBody),
   ord_intersection(OrdNegBody,I,Atoms),
   clause_to_evidential_clause(Atoms,E_Atoms),
   list_to_ord_set(E_Atoms,E_Clause),
   ord_union(Head,E_Clause,New_Head),
   simplify_rule(New_Head-PosBody,New_Rule),
   NewAcc = [New_Rule|Acc],
   evidential_gl_transformation(Rules,I,NewAcc,Program).
evidential_gl_transformation([],_,Program,Program).


/* ground_transformation(Program1,Program2) <-
      A given DNLP Program1 is transformed to its corresponding 
      ground version Program2. */
 
ground_transformation_rr(Program1,Program2) :-
   dnlp_to_herbrand_base(Program1,HB),
   tree_tpi_join_2(Program1,[HB],Program2).

ground_transformation(Program1,Program2) :-
   dnlp_to_herbrand_base(Program1,HB),
   ground_transformation(HB,Program1,Program2).

ground_transformation(HB,Program1,Program2) :-
   findall( Rule,
      ( member(Rule,Program1),
        parse_dislog_rule(Rule,Head,PosBody,NegBody),
        ord_union([Head,PosBody,NegBody],Atoms),
        tpi_resolve([HB],Atoms,_) ),
      Program2 ).


/* normal_transformation(Program1,Program2) <-
      A given DNLP Program1 is transformed to its corresponding 
      NLP Program2. */

normal_transformation(Program,Normal_Program) :-
   normal_transformation(Program,[],Normal_Program1),
   list_to_ord_set(Normal_Program1,Normal_Program).

normal_transformation([R|Rs],Acc,Normal_Program) :-
   parse_dislog_rule(R,Head,PosBody,NegBody),
   ( ( Head \== [],
       Rule = Head-PosBody-NegBody,
       generate_normal_rules(Head,Rule,[],Normal_Rules),
       append(Normal_Rules,Acc,NewAcc) )
   ; NewAcc = [R|Acc] ),
   normal_transformation(Rs,NewAcc,Normal_Program).
normal_transformation([],Normal_Program,Normal_Program).

generate_normal_rules([A|As],Rule,Acc,Normal_Rules) :-
   Rule = Head-PosBody-NegBody,
   ord_del_element(Head,A,NegAtoms),
   ord_union(NegBody,NegAtoms,NewNegBody),
   Normal_Rule = [A]-PosBody-NewNegBody,
   NewAcc = [Normal_Rule|Acc],
   generate_normal_rules(As,Rule,NewAcc,Normal_Rules).
generate_normal_rules([],_,Normal_Rules,Normal_Rules).



/* dnlp_simplify(Program_1, Program_2) <-
      */

dnlp_simplify([Rule_1|Rules_1], [Rule_2|Rules_2]) :-
   parse_dislog_rule(Rule_1, H, P, N),
   ( P = [], N = [],
     Rule_2 = H
   ; N = [],
     Rule_2 = H-P
   ; Rule_2 = Rule_1 ),
   dnlp_simplify(Rules_1, Rules_2).
dnlp_simplify([], []).


/* disjunctive_transformation(Program1,Program2) <-
      A given DNLP Program1 is transformed to its corresponding 
      DLP Program2. */
 
disjunctive_transformation(Program1,Program2) :-
   dnlp_to_dlp(Program1,Program2).

disjunctive_transformation :-
   announcement(12,'disjunctive transformation'),
   retractall(d_clause(_,_)),
   fail.
disjunctive_transformation :-
   assertz(dn_clause([[]],[],[])),
   dn_clause(Head,Body_1,Body_2),
   Head \== [[]],
   append(Head,Body_2,New_Head),
   list_to_ord_set(New_Head,Ordered_Head),
   list_to_ord_set(Body_1,Ordered_Body_1),
   \+ d_clause(Ordered_Head,Ordered_Body_1),
   assert(d_clause(Ordered_Head,Ordered_Body_1)),
   fail.
disjunctive_transformation :-
   retract(dn_clause([[]],[],[])),
   ldp.


/* horn_transformation(Program1,Program2) <-
      A given DNLP Program1 is transformed to its corresponding 
      horn program Program2. */

horn_transformation(Program1,Program2) :-
   horn_transformation(Program1,[],List_of_Programs),
   append(List_of_Programs,Program),
   list_to_ord_set(Program,Program2).

horn_transformation([Rule1|Rules1],Acc,Rules2) :-
   parse_dislog_rule(Rule1,Head,PosBody,NegBody),
   findall( [Atom]-PosBody-NegBody,
      member(Atom,Head),
      Rules ),
   horn_transformation(Rules1,[Rules|Acc],Rules2).
horn_transformation([],Rules,Rules).


/* evidential_truth_annotate_transformation(V,Program,EA_Program) <-
      Given a DNLP Program, and the number V of truth values.
      For V = 2, the t-transformation A_Program is computed,
      for V = 3, the tu-transformation A_Program is computed.
      Then A_Program is evidentially transformed to EA_Program. */

evidential_truth_annotate_transformation(2,Program,EA_Program) :-
   evidential_t_transformation(Program,EA_Program).
evidential_truth_annotate_transformation(3,Program,EA_Program) :-
   evidential_tu_transformation(Program,EA_Program).

evidential_tu_transformation(Program1,Program2) :-
   tu_transformation(Program1,Program1_tu),
   evidential_transformation(Program1_tu,Program1a),
   program_to_pure_evidential_program(Program1_tu,Program1b),
   Program1c = [['E'(t(A))]-[t(A)],['E'(u(A))]-[u(A)]],
   append([Program1a,Program1b,Program1c],Program2).

evidential_t_transformation(Program1,Program2) :-
   t_transformation(Program1,Program1_t),
   evidential_transformation(Program1_t,Program1a),
   program_to_pure_evidential_program(Program1_t,Program1b),
   Program1c = [['E'(t(A))]-[t(A)]],
   append([Program1a,Program1b,Program1c],Program2).


extended_evidential_transformation(Program1,Program2) :-
   evidential_transformation(Program1,Program1a),
   program_to_pure_evidential_program(Program1,Program1b),
   program_to_atoms(Program1,Atoms),
   atoms_to_evidential_rules(Atoms,Program1c),
   append([Program1a,Program1b,Program1c],Program2).

program_to_pure_evidential_program([R1|Rs1],Rs2) :-
   parse_dislog_rule_save(R1,_H,[],_BN),
   !,
   program_to_pure_evidential_program(Rs1,Rs2).
program_to_pure_evidential_program([R1|Rs1],[R2|Rs2]) :-
   parse_dislog_rule(R1,H,BP,BN),
   ord_union(H,BN,HX),
   clause_to_evidential_clause(HX,EH),
   clause_to_evidential_clause(BP,EBP),
   R2 = EH-EBP,
   program_to_pure_evidential_program(Rs1,Rs2).
program_to_pure_evidential_program([],[]).


/* program_to_atoms(Program,Atoms) <-
      */

program_to_atoms(Program,Atoms) :-
   maplist( rule_to_atoms,
      Program, Sets_of_Atoms ),
   ord_union(Sets_of_Atoms,Atoms).

rule_to_atoms(Rule,Atoms) :-
   parse_dislog_rule(Rule,H,BP,BN),
   ord_union([H,BP,BN],Atoms).
   

/* atoms_to_evidential_rules(Atoms,Program) <-
      */

atoms_to_evidential_rules(Atoms,Program) :-
   maplist( atom_to_evidential_rule, Atoms, Program ).

atom_to_evidential_rule(A,['E'(A)]-[A]).


/* evidential_transformation(Program1,Program2) <-
      A given DNLP Program1 is evidentially transformed to its
      corresponding DLP Program2. */
 
evidential_transformation(Program1,Program2) :-
   maplist( rule_to_evidential_rule, Program1, Program2 ).
%  Program2 = [
%     ['E'(A)]-[A]-[evidential_atom(A)],
%     [evidential_atom('E'(_))]|Program].

rule_to_evidential_rule(H-B1-B2,Rule) :-
   !,
   clause_to_evidential_clause(B2,H2),
   append(H,H2,New_H),
   ( ( B1 == [], Rule = New_H )
   ; ( B1 \== [], Rule = New_H-B1 ) ).
rule_to_evidential_rule(Rule,Rule).

 
/* clause_to_evidential_clause(Clause1,Clause2) <-
      A given clause Clause1 = A1 v ... v Ak is transformed
      to the evidential clause Clause2 = E A1 v ... v E Ak. */
 
clause_to_evidential_clause([A|As],['E'(A)|New_As]) :-
   !,
   clause_to_evidential_clause(As,New_As).
clause_to_evidential_clause([],[]).


/* evidential_transformation(Program1,Program2) <-
      A given DNLP Program1 is evidentially transformed to its
      corresponding DLP Program2. */
 
es_evidential_transformation(Program1,Program2) :-
   maplist( rule_to_xyz_rule(at-at-ev),
      Program1, Program_aae ),
   maplist( rule_to_xyz_rule(ev-ev-ev),
      Program1, Program_eee ),
   append(Program_aae,Program_eee,Program2).
 
rule_to_xyz_rule(X-Y-Z,Rule1,Rule2) :-
   parse_dislog_rule(Rule1,H,P,N),
   !,
   clause_to_x_clause(X,H,H1),
   clause_to_x_clause(Y,P,P1),
   clause_to_x_clause(Z,N,N1),
   append(H1,N1,New_H),
   ( ( P1 == [], Rule2 = New_H )
   ; ( P1 \== [], Rule2 = New_H-P1 ) ).
 
 
/* clause_to_x_clause(X,Clause1,Clause2) <-
      A given clause Clause1 = A1 v ... v Ak is transformed
      to the evidential clause Clause2 = X(A1) v ... v X(Ak). */
 
clause_to_x_clause(X,[A|As],[New_A|New_As]) :-
   !,
   New_A =.. [X,A],
   clause_to_x_clause(X,As,New_As).
clause_to_x_clause(_,[],[]).


/* evidential_transformation_reverse(Program1,Program2) <-
      A given evidential DLP Program1 is transformed to its
      corresponding DNLP Program2. */
 
evidential_transformation_reverse(Program1,Program2) :-
   maplist( evidential_rule_to_rule, Program1, Program2 ).

evidential_rule_to_rule(H1-P1,H2-P1-P2) :-
   !,
   split_evidential_clause(H1,H2-P2).
evidential_rule_to_rule(H1,H2-[]-P2) :-
   split_evidential_clause(H1,H2-P2).

 
/* split_evidential_clause(C1,C2-C3) <-
      A given evidential disjunctive clause C1 is split into its
      objective part C2 and its evidential part C3. */
 
split_evidential_clause([],[]-[]).
split_evidential_clause(['E'(A)|As1],As2-[A|As3]) :-
   !,
   split_evidential_clause(As1,As2-As3).
split_evidential_clause([A|As1],[A|As2]-As3) :-
   !,
   split_evidential_clause(As1,As2-As3).


/* evidential_program_to_cc(Program1,Program2) <-
      A given evidential DLP Program1 is transformed to its
      another evidential DLP Program2 by replacing evidential
      atoms 'E'(A) by evidential atoms e(A). */
    
evidential_program_to_cc([H1-B|Rs1],[H2-B|Rs2]) :-
   !,
   evidential_clause_to_cc(H1,H2),
   evidential_program_to_cc(Rs1,Rs2).
evidential_program_to_cc([H1|Rs1],[H2|Rs2]) :-
   evidential_clause_to_cc(H1,H2),
   evidential_program_to_cc(Rs1,Rs2).
evidential_program_to_cc([],[]).
 
evidential_clause_to_cc(['E'(A)|As1],[A2|As2]) :-
   !,
   A2 =.. [e,A],
   evidential_clause_to_cc(As1,As2).
evidential_clause_to_cc([A1|As1],[A1|As2]) :-
   !,
   evidential_clause_to_cc(As1,As2).
evidential_clause_to_cc([],[]).


/* evidential_program_to_cc_reverse(Program1,Program2) <-
      A given evidential DLP Program1 is transformed to its
      another evidential DLP Program2 by replacing evidential
      atoms e(A) by evidential atoms 'E'(A). */

evidential_program_to_cc_reverse([H1-B|Rs1],[H2-B|Rs2]) :-
   !,
   evidential_clause_to_cc_reverse(H1,H2),
   evidential_program_to_cc_reverse(Rs1,Rs2).
evidential_program_to_cc_reverse([H1|Rs1],[H2|Rs2]) :-
   evidential_clause_to_cc_reverse(H1,H2),
   evidential_program_to_cc_reverse(Rs1,Rs2).
evidential_program_to_cc_reverse([],[]).

evidential_clause_to_cc_reverse([A1|As1],['E'(A)|As2]) :-
   A1 =.. [e,A],
   !,
   evidential_clause_to_cc_reverse(As1,As2).
evidential_clause_to_cc_reverse([A1|As1],[A1|As2]) :-
   !,
   evidential_clause_to_cc_reverse(As1,As2).
evidential_clause_to_cc_reverse([],[]).


/* ground_and_standardize_program(Program1,Program2) <-
      The disjunctive-normal logic program Program1 is transformed
      to a standard representation Program2 of its ground version,
      such that all rules are of the form H-B1-B2. */

ground_and_standardize_program(Program1,Program3) :-
   ground_transformation(Program1,Program2),
   standardize_program(Program2,Program3).


/* standardize_program(Program1,Program2) <-
      The ground disjunctive-normal logic program Program1 is 
      transformed to a standard representation Program2,
      such that all rules are of the form H-B1-B2. */

standardize_program(Program1,Program2) :-
   standardize_program(Program1,[],Program2).

standardize_program([Rule|Rules],Acc,Program) :-
   parse_dislog_rule(Rule,As,Bs,Cs),
   NewAcc = [As-Bs-Cs|Acc],
   standardize_program(Rules,NewAcc,Program).
standardize_program([],Acc,Program) :-
   list_to_ord_set(Acc,Program).


/* parse_dislog_rule(Rule_1, Rule_2) <-
      */

parse_dislog_rule(Rule_1, Rule_2) :-
   parse_dislog_rule(Rule_1, As, Bs, Cs),
   Rule_2 = As-Bs-Cs.


/* parse_dislog_rule(Rule, As, Bs, Cs) <-
      A DisLog rule is parsed to obtain its
       - head As,
       - positive body Bs, and
       - negative body Cs. */

parse_dislog_rule(As-Bs-Cs,As,Bs,Cs) :-
   !.
parse_dislog_rule(As-Bs,As,Bs,[]) :-
   !.
parse_dislog_rule(As,As,[],[]).

parse_dislog_rule_save(Rule,Head,Pos_Body,Neg_Body) :-
   parse_dislog_rule(Rule,Head,P_Body,N_Body),
   !,
   Pos_Body = P_Body,
   Neg_Body = N_Body.

 
/* simplify_program(Program1,Program2) <-
      simplifies triples Head-Body1-[] to pairs Head-Body1
      triples Head-[]-[] and pairs Head-[] to Head. */

simplify_program(Program1,Program2) :-
   maplist( simplify_rule, Program1, Program2 ).

simplify_rule(Head-[]-[],Head) :-
   !.
simplify_rule(Head-Body1-[],Head-Body1) :-
   !.
simplify_rule(Head-Body1-Body2,Head-Body1-Body2) :-
   !.
simplify_rule(Head-[],Head) :-
   !.
simplify_rule(Head-Body,Head-Body) :-
   !.
simplify_rule(Head,Head) :-
   !.


current_program(Program) :-
   d_facts_to_program(Program1),
   d_clauses_to_program(Program2),
   dn_clauses_to_program(Program3),
   ord_union([Program1,Program2,Program3],Program4),
   simplify_program(Program4,Program), !.

d_facts_to_program(Program) :-
   collect_arguments(d_fact,Program1),
   list_to_ord_set(Program1,Program).

d_clauses_to_program(Program) :-
   assert(d_clause(dummy,dummy)),
   findall( Head-Body,
      ( d_clause(Head,Body),
        Head \== dummy, Body \== dummy ),
      Program1 ),
   list_to_ord_set(Program1,Program),
   retract(d_clause(dummy,dummy)).

dn_clauses_to_program(Program) :-
   assert(dn_clause(dummy,dummy,dummy)),
   findall( Head-Body1-Body2,
      ( dn_clause(Head,Body1,Body2),
        Head \== dummy, Body1 \== dummy, Body2 \== dummy ),
      Program1 ),
   list_to_ord_set(Program1,Program),
   retract(dn_clause(dummy,dummy,dummy)).


/* dnlp_to_dn_clauses(Program) <-
      asserts all rules in the DNLP Program as dn_clause atoms. */

dnlp_to_dn_clauses(Program) :-
   announcement(12,dnlp_to_dn_clauses),
   retractall(dn_clause(_,_,_)),
   dnlp_to_dn_clauses_loop(Program).

dnlp_to_dn_clauses_loop([Rule|Rules]) :-
   parse_dislog_rule(Rule,Head,P_Body,N_Body),
   assert(dn_clause(Head,P_Body,N_Body)),
   dnlp_to_dn_clauses_loop(Rules).
dnlp_to_dn_clauses_loop([]).


dnlp_to_d_clauses(Program) :-
   announcement(12,dnlp_to_d_clauses),
   retractall(d_clause(_,_)),
   dnlp_to_dlp(Program,Program2),
   dlp_to_d_clauses_loop(Program2).

dnlp_to_dlp([Head-Body1-Body2|Rules], [New_Head-Body1|New_Rules]) :-
   !,
   ord_union(Head, Body2, New_Head),
   dnlp_to_dlp(Rules, New_Rules).
dnlp_to_dlp([Rule|Rules],[Rule|New_Rules]) :-
   dnlp_to_dlp(Rules,New_Rules).
dnlp_to_dlp([],[]).


/* dlp_to_d_clauses(Program) <-
      asserts all rules in the DLP Program as d_clause atoms. */

dlp_to_d_clauses(Program) :-
   announcement(12,dlp_to_d_clauses),
   retractall(d_clause(_,_)),
   dlp_to_d_clauses_loop(Program).

dlp_to_d_clauses_loop([Head-Body|Rules]) :-
   !,
   assert(d_clause(Head,Body)),
   dlp_to_d_clauses_loop(Rules).
dlp_to_d_clauses_loop([Head|Rules]) :-
   !,
   assert(d_clause(Head,[])),
   dlp_to_d_clauses_loop(Rules).
dlp_to_d_clauses_loop([]).


dnlp_to_edlp(Program1,Program2) :-
   maplist( dn_rule_to_ed_rule, Program1, Program2 ).

dn_rule_to_ed_rule(Rule,New_Rule) :-
   parse_dislog_rule(Rule,Head,P_Body,N_Body),
   negate_atoms(N_Body,N_Body_Lit),
   ord_union(P_Body,N_Body_Lit,Body),
   simplify_rule(Head-Body,New_Rule).


dn_clauses_to_ed_clauses :-
   announcement(12,dn_clauses_to_ed_clauses),
   retractall(d_clause(_,_)),
   fail.
dn_clauses_to_ed_clauses :-
   dn_clause(Head,Body1,Body2), 
   negate_atoms(Body2,Negated_Body2),
   ord_union(Body1,Negated_Body2,Body),
   assert(d_clause(Head,Body)),
   fail.
dn_clauses_to_ed_clauses.


dlp_to_dnlp :-
   announcement(12,
      'disjunctive program to disjunctive normal program'),
   fail.
dlp_to_dnlp :-
   assertz(d_clause([],[])),
   d_clause(Head,Body),
   Head \== [],
   assert(dn_clause(Head,Body,[])),
   fail.
dlp_to_dnlp :-
   assertz(d_fact([])),
   d_fact(D_Fact),
   D_Fact \== [],
   assert(dn_clause(D_Fact,[],[])),
   fail.
dlp_to_dnlp :-
   retract(d_fact([])),
   retract(d_clause([],[])),
   listing(dn_clause/3).


dhs_to_d_facts(State) :-
   announcement(12,dhs_to_d_facts),
   dabolish(d_fact/1),
   dhs_to_d_facts_loop(State).

dhs_to_d_facts_loop([C|Cs]) :-
   assert(d_fact(C)),
   dhs_to_d_facts_loop(Cs).
dhs_to_d_facts_loop([]).


dhs_to_delta_d_facts(State) :-
   announcement(12,dhs_to_delta_d_facts),
   dabolish(delta_d_fact/1),
   dhs_to_delta_d_facts_loop(State).

dhs_to_delta_d_facts_loop([C|Cs]) :-
   assert(delta_d_fact(C)),
   dhs_to_delta_d_facts_loop(Cs).
dhs_to_delta_d_facts_loop([]).

 
dhs_to_d_clauses(State) :-
   announcement(12,dhs_to_d_clauses),
   dabolish(d_clause/2),
   dhs_to_d_clauses_loop(State).
 
dhs_to_d_clauses_loop([C|Cs]) :-
   assert(d_clause(C,[])),
   dhs_to_d_clauses_loop(Cs).
dhs_to_d_clauses_loop([]).

d_facts_to_d_clauses :-
   d_fact(Fact),
   assert(d_clause(Fact,[])),
   fail.
d_facts_to_d_clauses.


g_state_to_dlp(State,Program) :-
   g_state_to_dlp_loop(State,Program2),
   list_to_ord_set(Program2,Program).

g_state_to_dlp_loop([C|Cs],[R|Rs]) :-
   g_fact_to_d_clause(C,R),
   g_state_to_dlp_loop(Cs,Rs).
g_state_to_dlp_loop([],[]).

g_fact_to_d_clause([~A|Ls],Positive-[A|Negative]) :-
   g_fact_to_d_clause(Ls,Positive-Negative).
g_fact_to_d_clause([A|Ls],[A|Positive]-Negative) :-
   g_fact_to_d_clause(Ls,Positive-Negative).
g_fact_to_d_clause([],[]-[]).


/* fact_filter(Program,Facts) <-
      Selects the set Facts of all facts from the DNLP Program. */

fact_filter([Rule|Rules],[Fact|Facts]) :-
   parse_dislog_rule_save(Rule,Fact,[],[]),
   !,
   fact_filter(Rules,Facts).
fact_filter([_|Rules],Facts) :-
   fact_filter(Rules,Facts).
fact_filter([],[]).


/* connection to mm-satchmo */

dlp_to_satchmo_program([R1|Rs1],[(R2)|Rs2]) :-
   parse_dislog_rule(R1,H1,B1,_),
   ( ( H1 = [], H2 = false )
   ; list_to_semicolon_structure(H1,H2) ),
   ( ( B1 = [], B2 = true )
   ; list_to_comma_structure(B1,B2) ),
   R2 =.. ['--->',B2,H2],
   dlp_to_satchmo_program(Rs1,Rs2).
dlp_to_satchmo_program([],[]).
 
satchmo_to_dlp_program([(R2)|Rs2],[R1|Rs1]) :-
   R2 =.. ['--->',B2,H2],
   ( ( H2 = false, H1 = [] )
   ; list_to_semicolon_structure(H1,H2) ),
   ( ( B2 = true, B1 = [] )
   ; list_to_comma_structure(B1,B2) ),
   R1 = H1-B1,
   satchmo_to_dlp_program(Rs2,Rs1).
satchmo_to_dlp_program([],[]).


/* dlp_to_dual_program(Program,Dual_Program) <-
      */

dlp_to_dual_program(Program,Dual_Program) :-
   maplist( dlp_rule_to_dual_rule,
      Program, Dual_Program ).

dlp_rule_to_dual_rule(H-B,B-H) :-
   !.
dlp_rule_to_dual_rule(H,[]-H).


/******************************************************************/

 
