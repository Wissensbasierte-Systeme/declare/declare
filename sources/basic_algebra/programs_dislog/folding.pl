

/******************************************************************/
/***                                                            ***/
/***            DisLog: Folding                                 ***/
/***                                                            ***/
/******************************************************************/
 


/*** interface ****************************************************/


stable_generate_disjunctive(Program,Stable_Models) :-
      measure(
   partial_evaluation_cc(Program,Program2), Time1 ),
      write_length(Program2),
   normal_transformation(Program2,Program3),
      write_length(Program3),
      measure( (
   select_facts_dnlp(Program2,State2),
   simplify_program_3(Program3,State2,Program3a) ), Time1a ),
      write_length(Program3a),
%     dportray(lp,Program3a),
      measure(
   simplify_program_gamma_2_pbe(Program3a,Program4), Time2 ),
%     dportray(lp,Program4),
   simplify_program_pbe_can(Program4,Program4a),
      write_length(Program4a),
      measure(
   stable_generate(Program4a,Stable_Models), Time3 ),
      write_length(Stable_Models),
   pp_times(user,[Time1,Time1a,Time2,Time3]).

   
stable_generate(Program,Models) :-
   dportray(lp,Program),
   list_to_ord_set(Program,Program2),
   negative_bodies_to_tree(Program2,Tree),
   dportray(tree,Tree),
   stable_generate([[],Tree],Program2,[]-[],Models), 
   nl.

stable_generate(Tree,Program,I_plus-I_minus,Models) :-
   stable_generate(Tree,Program,I_plus-I_minus),
   setof2( I, retract(stable_model_stabalg_branch(I)), Models ).

stable_generate([Node|Trees],Program,I_plus-I_minus) :-
%  writeln(user,
%     stable_generate([Node|Trees],Program,I_plus-I_minus)),
   writeln(user,stable_generate(I_plus-I_minus)),
   writeln(user,assume_not(Node)),
   ord_union(Node,I_minus,I_minus_2),
   simplify_program_1_b(Program,I_plus,I_minus_2,Program_1),
%  writeln(user,simplify_program_1_b(
%     Program,I_plus,I_minus_2,Program_1)),
   list_to_ord_set(Program_1,Program_1a),
   transitive_eval_of_def_rules(
      Program_1a,I_plus,I_new,Program_2),
   writeln(user,i_new(I_new)),
%  writeln(user,trans_eval_of_def_rules(
%     Program_1a,I_plus,I_new,Program_2)),
   ord_union(I_plus,I_new,I_plus_2),
   ord_disjoint(I_plus_2,I_minus_2),
   extension_of_interpretation_possible_with(
      Program_2,I_plus_2,Rules),
   ( ( Rules = [],
       assert(stable_model_stabalg_branch(I_plus_2)) )
   ; stable_generate_loop(Trees,Program_2,I_plus_2-I_minus_2) ).
stable_generate([_|_],_,_-_).
stable_generate([],_,I_plus-_) :-
   assert(stable_model_stabalg_branch(I_plus)).
       
stable_generate_loop([Tree|Trees],Program,I_plus-I_minus) :-
   stable_generate(Tree,Program,I_plus-I_minus),
   stable_generate_loop(Trees,Program,I_plus-I_minus).
stable_generate_loop([],_,_-_).
% stable_generate_loop([],[],_-_) :-
%    !.
% stable_generate_loop([],Program,I_plus-I_minus) :-
%    negative_bodies_to_tree(Program,Tree),
%    stable_generate(Tree,Program,I_plus-I_minus).


negative_bodies_to_tree(Program,Tree) :-
   program_to_negative_bodies(Program,Neg_Bodies),
   state_to_tree_non_can(Neg_Bodies,Tree).

state_to_tree_non_can(State,Tree) :-
   extend_clauses_by_ids(State,State_with_Ids),
   state_to_tree(State_with_Ids,Tree_with_Ids),
   tree_remove_ids(Tree_with_Ids,Tree).
%  dportray(tree,Tree2), dportray(tree,Tree).


tree_remove_ids([N1|Trees1],[N2|Trees2]) :-
   clause_remove_ids(N1,N2),
   trees_remove_ids(Trees1,Trees2).
tree_remove_ids([],[]).
   
trees_remove_ids([T1|Ts1],[T2|Ts2]) :-
   tree_remove_ids(T1,T2),
   trees_remove_ids(Ts1,Ts2).
trees_remove_ids([],[]).


program_to_negative_bodies(Program,Neg_Bodies) :-
   program_to_negative_bodies(Program,[],List),
   list_to_ord_set(List,Neg_Bodies).
   
program_to_negative_bodies([R|Rs],Acc,Ns) :-
   parse_dislog_rule(R,_,_,N),
   program_to_negative_bodies(Rs,[N|Acc],Ns).
program_to_negative_bodies([],Ns,Ns).


clause_remove_ids([L1|Ls1],Ls2) :-
   is_clause_id(L1),
   !,
   clause_remove_ids(Ls1,Ls2).
clause_remove_ids([L1|Ls1],[L1|Ls2]) :-
   clause_remove_ids(Ls1,Ls2).
clause_remove_ids([],[]).


extend_clauses_by_ids([C1|Cs1],[C2|Cs2]) :-
   extend_clause_by_id(C1,C2),
   extend_clauses_by_ids(Cs1,Cs2).
extend_clauses_by_ids([],[]). 
 
extend_clause_by_id(Clause,[Clause_Id|Clause]) :-
   assign_id_to_clause(Clause,Clause_Id).


assign_id_to_clause(Clause,Clause).

is_clause_id(Clause_Id) :-
   is_list(Clause_Id).


/******************************************************************/


