

/******************************************************************/
/***                                                            ***/
/***         Declare:  Range Restriction                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* program_range_restrict(Program_1, Program_2) <-
      */

program_range_restrict(Program_1, Program_2) :-
   maplist( rule_range_restrict, Program_1, Program ),
   ( Program == Program_1 -> Program_2 = Program
   ; program_to_herbrand_universe(Program_1, HU),
     ( foreach(T, HU), foreach(F, Fs) do F = [domain(T)] ),
     append(Fs, Program, Program_2) ).
%    star_line, dwrite(lp, user, Program_2), star_line

rule_range_restrict(Rule_1, Rule_2) :-
    parse_rule(Rule_1, Head-Body_1),
    term_to_variables(Head, Vs_1),
    sort(Vs_1, Ws_1),
    term_to_variables(Body_1, Vs_2),
    sort(Vs_2, Ws_2),
    ord_subtract(Ws_1, Ws_2, Vs),
    ( foreach(V, Vs), foreach(A, As) do A = domain(V) ),
    append(Body_1, As, Body_2),
    ( Body_1 = [], Body_2 = [] ->
      Rule_2 = Head
    ; Rule_2 = Head-Body_2 ).


/* dislog_rule_is_range_restricted(Rule) <-
      */

dislog_rule_is_range_restricted(Rule) :-
   copy_term(Rule, Rule_2),
   parse_dislog_rule(Rule_2, As, Bs, Cs),
   append(As, Cs, Ds),
   free_variables(Ds, Vs_Ds),
   free_variables(Bs, Vs_Bs),
   listvars(Rule_2, 1),
   subset(Vs_Ds, Vs_Bs).


/******************************************************************/


