

/******************************************************************/
/***                                                            ***/
/***          DisLog:  CD-Transformations                       ***/
/***                                                            ***/
/******************************************************************/


/* cd_transformation(Mode,Program_1,Program_2) <-
      - Mode = 2: CD-Transformation,
      - Mode = 3: TU-CD-Transformation. */

cd_transformation(2,Program_1,Program_2) :-
   findall( H-P-N2,
      ( member(Rule,Program_1),
        parse_dislog_rule(Rule,H1,P1,N1),
        H = [dis(Alpha)],
        positive_body_expand(Alpha,P1,P2),
        negative_body_expand(N1,N2),
        P = [subsumes(H1,Alpha)|P2] ),
      Program_2 ).
cd_transformation(3,Program_1,Program_2) :-
   findall( H-P-N2,
      ( member(Rule,Program_1),
        parse_dislog_rule(Rule,H1,P1,N1),
        ( ( H = [t(dis(Alpha))],
            positive_body_expand(t,Alpha,P1,P2),
            negative_body_expand(u,N1,N2) )
        ; ( H = [u(dis(Alpha))],
            positive_body_expand(u,Alpha,P1,P2),
            negative_body_expand(t,N1,N2) ) ),
        P = [subsumes(H1,Alpha)|P2] ),
      Program_2 ).

positive_body_expand(Alpha,Clause_1,Clause_2) :-
   findall( [dis(Alpha_1),conjunction([B],Alpha,Alpha_1)],
      member(B,Clause_1),
      Pairs ),
   append(Pairs,Clause_2).
positive_body_expand(Annotation,Alpha,Clause_1,Clause_2) :-
   findall( [Dis_Atom,conjunction([B],Alpha,Alpha_1)],
      ( Dis_Atom =.. [Annotation,dis(Alpha_1)],
        member(B,Clause_1) ),
      Pairs ),
   append(Pairs,Clause_2).

negative_body_expand(Clause_1,Clause_2) :-
   findall( con([C]),
      member(C,Clause_1),
      Clause_2 ).
negative_body_expand(Annotation,Clause_1,Clause_2) :-
   Con_Atom =.. [Annotation,con([C])],
   findall( Con_Atom,
      member(C,Clause_1),
      Clause_2 ).


/******************************************************************/


