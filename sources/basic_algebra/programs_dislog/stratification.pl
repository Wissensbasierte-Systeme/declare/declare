

/******************************************************************/
/***                                                            ***/
/***         DisLog:  Stratification                            ***/
/***                                                            ***/
/******************************************************************/


:- op(30, xfy, \).
:- op(30, xfy, eq).
:- op(30, xfy, ge).
:- op(30, xfy, gt).


/*** interface ****************************************************/


stratify(Dnlp) :-
   dconsult(Dnlp,Program),
   stratify_program(Program,Partition,Groups),
   dportray(sets,Partition),
   dportray(lps,Groups).

min_stratify(Dnlp) :-
   dconsult(Dnlp,Program),
   min_stratify_program(Program,Strata,Groups),
   dportray(sets,Strata),
   dportray(lps,Groups).

locally_stratify(Dnlp) :-
   dconsult(Dnlp,Program),
   locally_stratify_program(Program,Partition,Groups),
   dportray(sets,Partition),
   dportray(lps,Groups).

stratify_test(Program) :-
   stratify_program(Program,_,_).

locally_stratify_test(Program) :-
   locally_stratify_program(Program,_,_).

min_stratify_ground(Dnlp) :-
   dconsult(Dnlp,Program),
   min_stratify_ground_program(Program,Strata,Groups),
   dportray(sets,Strata),
   dportray(lps,Groups).


/* stratify_program(Dnlp,Partition,Groups) <-
      for a disjunctive normal logic program Dnlp a stratification
      Partition of the predicate symbols is generated and Groups
      is the partition of the rules w.r.t. Partition. */

stratify_program(Program,Programs) :-
   stratify_program(Program,_,Programs).

stratify_program(Dnlp,Partition,Groups) :-
   dnlp_to_fdnlp(Dnlp,FDnlp), 
   create_graph_lists(FDnlp,PosList,NegList),
   append(PosList,NegList,PosNegList),
   vertices_edges_to_ugraph([],PosNegList,PosGraph),
   reduce(PosGraph,Graph),
   top_sort(Graph,Partition),
   !,
   stratification_check(NegList,Partition),
   group_rules(Partition,Dnlp,Groups),
   !.


/* min_stratify_program(Dnlp,Partition,Groups) <-
      for a disjunctive normal logic program Dnlp a minimal 
      stratification Partition of the predicate symbols is generated
      and Groups is the partition of the rules w.r.t. Partition. */

min_stratify_program(Program,Strata,Groups) :-
   dnlp_to_fdnlp(Program,FProgram), 
   create_expression_lists(FProgram,[],[],[],Equals,Greqs,Grths),
   reduce_greater_equal_chains(Equals,Greqs,Equals_2,Greqs_2),
   append(Grths,Greqs_2,Exprs),
   relsyms(FProgram,[],Relsyms),
   develop_strati(Exprs,[Relsyms],HelpStrata),
   check_equalities(Equals_2,HelpStrata,Strata),
   !,
   min_stratification_check(Exprs,Strata),
   group_rules(Strata,Program,Groups),
   !.


locally_stratify_program(Program,Programs) :-
   locally_stratify_program(Program,_,Programs).

locally_stratify_program(Dnlp,Partition,Groups) :-
   dnlp_to_herbrand_base(Dnlp,HB),
   tree_tpi_join_2(Dnlp,[HB],DnlpGround),
   create_graph_lists(DnlpGround,PosList,NegList),
   append(PosList,NegList,PosNegList),
   vertices_edges_to_ugraph([],PosNegList,PosGraph),
   reduce(PosGraph,Graph),
   top_sort(Graph,Partition),
   !,
   stratification_check(NegList,Partition),
   group_ground_rules(Partition,DnlpGround,Groups),
   !.

min_stratify_ground_program(Dnlp,Strata,Groups) :-
   dnlp_to_herbrand_base(Dnlp,HB),
   tree_tpi_join_2(Dnlp,[HB],DnlpGround),
   create_expression_lists(DnlpGround,[],[],[],
      EqualList,GreqList,GrthList),
   reduce_greater_equal_chains(EqualList,GreqList,
      NewEqualList,NewGreqList),
   append(GrthList,NewGreqList,ExprList),
   relsyms(DnlpGround,[],Relsyms),
   develop_strati(ExprList,[Relsyms],HelpStrata),
   check_equalities(NewEqualList,HelpStrata,Strata),
   !,
   min_stratification_check(ExprList,Strata),
   group_ground_rules(Strata,DnlpGround,Groups),
   !.


/*** implementation ***********************************************/


/* create_graph_lists(Dnlp,PosList,NegList) <-
      For the disjunctive normal logic program Dnlp the set of 
      positive edges PosList and the set of negative edges NegList 
      of the dependency graph are created. */

create_graph_lists(Dnlp,PosList,NegList) :-
   create_graph_lists(Dnlp,[],[],PosList,NegList).

create_graph_lists([Term|Terms],L1,L2,PosList,NegList) :-
   parse_dislog_rule(Term,As,Bs,Ds),
   create_head_cycle(As,CycleList),
   ( Bs \== []  ->  
        ( positive_pairs(As,Bs,PosPairs),
          append(CycleList,L1,NewL1),
	  append(NewL1,PosPairs,NewerL1) )
        ; append(CycleList,L1,NewerL1) ),
   ( Ds \== []  ->  
        ( negative_pairs(As,Ds,NegPairs),
          append(NegPairs,L2,NewerL2) )
        ; NewerL2 = L2 ),
   create_graph_lists(Terms,NewerL1,NewerL2,PosList,NegList).
create_graph_lists([],L1,L2,L1,L2).


/* create_head_cycle(Head,CycleList) <-
      Head are the predicate symbols of a disjunctive normal rule and
      CycleList is the List of edges connecting all symbols in a wrap 
      around fashion.                                    */

create_head_cycle([A|As],CycleList) :-
   append([A|As],[A],NewAs),
   create_cyclelist(NewAs,CycleList\[]).

create_cyclelist([A,B|Rest],Xs\Ys) :- 
   create_cyclelist([B|Rest],Xs\[A-B|Ys]).
create_cyclelist([A,B],Xs\Ys) :- 
   Xs = [A-B|Ys].


/* positive_pairs(Head,PosBody,PosPairs) <-
      Head and PosBody are lists of predicate symbols of a disjunctive
      normal rule and PosPairs is the List of all edges from a Head
      symbol to a PosBody symbol. */

positive_pairs(As,Bs,PosPairs) :-
   setof( Y-X, 
      ( member(X,As), 
        member(Y,Bs) ), 
      PosPairs ).


/* negative_pairs(Head,NegBody,NegPairs) <-
      Head and NegBody are lists of predicate symbols of a disjunctive
      normal rule and NegPairs is the List of all edges from a Head
      symbol to a NegBody symbol. */

negative_pairs(As,Ds,NegPairs) :-
   setof( Y-X,
      ( member(X,As),
        member(Y,Ds) ),
      NegPairs ).


/* stratification_check(NegList,Partition) <-
      verifies whether the vertices of all negative edges of the
      dependency graph are in different partition. */

stratification_check([A-B|Edges],Partition) :-
   locate_in_set_of_sets(A,Partition,Si),
   locate_in_set_of_sets(B,Partition,Sj),
   Si \== Sj,
   stratification_check(Edges,Partition).
stratification_check([],_).


/* locate_in_set_of_sets(Element,SetofSets,Set) <-
      finds the Set in SetofSets where X is member of. */

locate_in_set_of_sets(X,[Partition|_],Partition) :- 
   member(X,Partition),
   !.
locate_in_set_of_sets(X,[_|Partitions],Part) :- 
   locate_in_set_of_sets(X,Partitions,Part).


/* create_expression_lists(
      Dnlp,L1,L2,L3,EqualList,GreqList,GrthList) <-
      for the disjunctive normal logic program Dnlp the following
      lists are created:
      - EqualList contains all Expressions with form 'A = B'
      - GreqList contains all Expressions with form 'A >= B'
      - GrthList contains all Expressions with form 'A > B' */

create_expression_lists(
      [Term|Terms],L1,L2,L3,EqualList,GreqList,GrthList) :-
   parse_dislog_rule(Term,As,Bs,Ds),
   create_head_equations(As,EqList),
   append(L1,EqList,NewL1),
   ( Bs \== [] -> 
     ( create_greater_equals(As,Bs,GeList),
       append(L2,GeList,NewL2) )
     ; NewL2 = L2 ),
   ( Ds \== [] -> 
     ( create_greater_then(As,Ds,GtList),
       append(L3,GtList,NewL3) )
     ; NewL3 = L3 ),
   create_expression_lists(Terms,NewL1,NewL2,NewL3,
      EqualList,GreqList,GrthList).
create_expression_lists([],L1,L2,L3,L1,L2,L3).


/* create_head_equations(Head,EqList) <-
      Head is the list of predicate symbols of a disjunctive normal
      rule and EqList is the List of Expression of the form 'A = B'
      saying that the Head symbol A and B must be members of the
      same stratum. */

create_head_equations([A|As],EqList) :-
   append([A|As],[A],NewAs),
   create_equations(NewAs,EqList\[]).

create_equations([A,B|Rest],Xs\Ys) :- 
   create_equations([B|Rest],Xs\[A eq B|Ys]).
create_equations([A,B],Xs\Ys) :- 
   Xs = [A eq B|Ys].


/* create_greater_equals(Head,PosBody,GeList) <-
      GeList is the list of Expressions of the form 'A >= B' where
      A is a Head symbol and B is a PosBody symbol. */

create_greater_equals(As,Bs,GeList) :-
   setof( X ge Y, 
      ( member(X, As), 
        member(Y, Bs) ), 
      GeList).


/* create_greater_then(Head,NegBody,GtList) <-
      GtList is the list of Expressions of the form 'A > B' where
      A is a Head symbol and B is a NegBody symbol. */

create_greater_then(As,Ds,GtList) :-
   setof( X gt Y, 
      ( member(X, As), 
        member(Y, Ds) ),
      GtList).


/* reduce_greater_equal_chains(EqualList,GreqList,
         NewEqualList,NewGreqList) <-
      Chains like A <= B <= C <= A in GreqList are removed from 
      GreqList and transformed in A = B, B = C. 
      The =-expressions are appended to EqualList yielding 
      NewEqualList. */

reduce_greater_equal_chains(EqualList,GreqList,
      NewEqualList,NewGreqList) :-
   build_chains(GreqList,Chains),
   build_equations(Chains,Equations),
   append(EqualList,Equations,NewEqualList),
   remove_chains(GreqList,Chains,NewGreqList).


/* build_chains(GreqList,Chains) <-
      Chains is the list of all <= - chains in GreqList. */

build_chains(GreqList,Chains) :-
   setof( Chain, 
      ( sublist(Chain,GreqList),
        greater_equal_chain_check(Chain),
	length(Chain,N),
	N > 1,
	Chain = [E|_],
	parse_expr(E,_,B,_),
	last_element(Chain,F),
	parse_expr(F,C,_,_),
	C == B ), 
      Chains).
build_chains(_,[]).


/*
build_chains(GreqList,Chains) :-
   build_chains(GreqList,GreqList,[],Chains).
build_chains([E|Es],GreqList,L,Chains) :-
   build_chain(E,GreqList,Chain),
   length(Chain,N),
   ( ( N > 1,
       Chain = [_ ge B|_],
       last_element(Chain,C ge _),
       B == C,
       NewL = [Chain|L] )
   ; NewL = L ),
   build_chains(Es,GreqList,NewL,Chains).
build_chains([],_,L,L).
build_chains([],[]).

build_chain(Expr,GreqList,Chain) :-
   build_chain(Expr,GreqList,[Expr],Chain).
build_chain(Expr,[E|Es],L,Chain) :-
   delete([E|Es],Expr,NewGreqList),
   ( ( find_succ(Expr,NewGreqList,Succ),
       NewerGreqList = NewGreqList,
       NewL = [Succ|L] )
   ; ( NewerGreqList = [],
       NewL = L ) ),
   build_chain(Succ,NewerGreqList,NewL,Chain).
build_chain(_,[],L,L).
build_chain(_,[],[]).
*/



find_succ(E,[F|_],F) :-
   parse_expr(E,A,B,_),
   A \== B,
   parse_expr(F,C,D,_),
   C \== D,
   A == D,
   !.
find_succ(E,[_|Rest],Expr) :-
   find_succ(E,Rest,Expr).


/* greater_equal_chain_check(Chain) <-
      verifies whether Chain is a list of expressions of the form
      A1 <= A2 <= ... <= An. */

greater_equal_chain_check([E,F|Rest]) :- 
   parse_expr(E,A,B,Op), Op == ge,
   A \== B,
   parse_expr(F,C,D,Op), Op == ge,
   C \== D, A == D,
   greater_equal_chain_check([F|Rest]).
greater_equal_chain_check([_]).

parse_expr(A eq B,A,B,Op) :- 
   !, Op = eq.
parse_expr(A ge B,A,B,Op) :- 
   !, Op = ge.
parse_expr(A gt B,A,B,Op) :- 
   Op = gt.


/* build_equations(Chains,Equations) <-
      transforms a chain A1 <= A2 <= ... <= An in a list of 
      expressions A1 = A2, A2 = A3, ... An-1 = An. */

build_equations(Chains,Equations) :-
   setof( A eq B, 
      ( member(Chain,Chains),
        member(A ge B,Chain) ), 
      Equations).
build_equations(_,[]).


/* remove_chains(GreqList,Chains,NewGreqList) <-
      the <= - chains in Chains are removed from GreqList yielding
      NewGreqList. */

remove_chains(GreqList,Chains,NewGreqList) :-
   setof( A ge B, 
      ( member(A ge B,GreqList),
	member(Chain,Chains),
	non_member(A ge B,Chain) ),
      NewGreqList).
remove_chains(X,_,X).


/* relsyms(Dnlp,L,Relsyms) <-
      Relsyms is the list of all predicate symbols of the
      disjunctive normal logic program Dnlp. */

relsyms([R|Rs],L,Relsyms) :-
   parse_dislog_rule(R,As,Bs,Ds),
   append([L,As,Bs,Ds],L2),
   relsyms(Rs,L2,Relsyms).
relsyms([],L,Relsyms) :- 
   remove_duplicates(L,Relsyms).


/* develop_strati(ExprList,Partition,HelpStrata) <-
      from the expression list ExprList and the singelton partition
      [Relsyms] the partition HelpStrata is generated. */

develop_strati(ExprList,Partition,HelpStrata) :-
   ( find_minimals(ExprList,MinList) -> 
        ( move_elements(MinList,Partition,NewPartition),
          remove_minimals(ExprList,MinList,NewExprList),
          develop_strati(NewExprList,NewPartition,HelpStrata) )
        ; HelpStrata = Partition ).


/* find_minimals(ExprList,MinList) <-
      Minlist is a sublist of ExprList containing all expressions 
      with minimal elememts. */

find_minimals(ExprList,MinList) :-
   setof( Expr, 
      ( member(Expr,ExprList),
        \+( bagof( Ex, 
	       ( member(Ex,ExprList),
	         Ex \== Expr,
		 parse_expr(Expr,_,B,_),
		 parse_expr(Ex,C,_,_),
		 C == B ), _ ) ) ),
      MinList).



/* move_elements(MinList,Partition,NewPartition) <-
      The partition Partition of the predicate symbols of a 
      disjunctive normal logic program is updated w.r.t. the 
      expressions in MinList. */ 

move_elements([Min|Mins],Partition,NewPartition) :-
   parse_expr(Min,A,B,Op),
   locate_in_set_of_sets(A,Partition,Ma),
   locate_in_set_of_sets(B,Partition,Mb),
   nth(I,Partition,Ma),
   nth(J,Partition,Mb),
   move_elements_1(Op,I,J,A,Ma,Mb,Partition,HelpPartition),
   move_elements_2(Op,I,J,A,Ma,HelpPartition,NewerPartition),
   move_elements(Mins,NewerPartition,NewPartition).
move_elements([],P,P).

move_elements_1(Op,I,J,A,Ma,Mb,Partition,NewerPartition) :-
   ( ( Op == ge, I < J ) -> 
       ( delete(Ma,A,NewMa),
         substitute(Ma,Partition,NewMa,HelpPartition),
	 append(Mb,[A],NewMb),
	 substitute(Mb,HelpPartition,NewMb,NewerPartition))
     ; NewerPartition = Partition ).

move_elements_2(Op,I,J,A,Ma,Partition,NewerPartition) :-
   ( ( Op == gt, I =< J ) -> 
       ( K is J + 1,
	 move_elements_put_a(A,K,Ma,Partition,NewerPartition) )
     ; NewerPartition=Partition).

move_elements_put_a(A,K,Ma,Partition,NewerPartition) :-
   ( nth(K,Partition,M) -> 
       ( delete(Ma,A,NewMa),
         substitute(Ma,Partition,NewMa,HelpPartition),
         append(M,[A],NewM),
         substitute(M,HelpPartition,NewM,NewerPartition) )
     ; ( delete(Ma,A,NewMa),
	 substitute(Ma,Partition,NewMa,HelpPartition),
	 append(HelpPartition,[[A]],NewerPartition) ) ).


/* check_equalities(EqualList,Partition,Strata) <-
      verifies whether all equality expressions in EqualList are
      satisfied by Partition yielding Strata. */

check_equalities([Expr|Exprs],HelpStrata,Strata) :-
   parse_expr(Expr,A,B,Op), 
   Op == eq,
   locate_in_set_of_sets(A,HelpStrata,Ma),
   locate_in_set_of_sets(B,HelpStrata,Mb),
   ( Ma \== Mb -> 
        ( nth(I,HelpStrata,Ma),
	  nth(J,HelpStrata,Mb),
	  ( I < J -> 
              ( delete(Ma,A,NewMa),
	        substitute(Ma,HelpStrata,NewMa,Help1Strata),
	        append(Mb,[A],NewMb),
		substitute(Mb,Help1Strata,NewMb,Help2Strata) )
            ; ( delete(Mb,B,NewMb),
	        substitute(Mb,HelpStrata,NewMb,Help1Strata),
		append(Ma,[B],NewMa),
		substitute(Ma,Help1Strata,NewMa,Help2Strata) ) ) )
     ; Help2Strata = HelpStrata ),
   check_equalities(Exprs,Help2Strata,Strata).
check_equalities([],X,X).


/* min_stratification_check(ExprList,Strata) <-
      verifies whether all expressions in ExprList are satisfied
      by Strata. */

min_stratification_check([Expr|Exprs],Strata) :-
   parse_expr(Expr,A,B,Op),
   locate_in_set_of_sets(A,Strata,Ma),
   locate_in_set_of_sets(B,Strata,Mb),
   nth(I,Strata,Ma),
   nth(J,Strata,Mb),
   ( ( Op == ge, I < J ) -> 
     fail
   ; true ), 
   ( ( Op == gt, I =< J ) -> 
     fail
   ; true), 
   min_stratification_check(Exprs,Strata).
min_stratification_check([],_).


/* remove_minimals(ExprList,MinList,NewExprList) <-
      NewExprList = ExprList - MinList. */

remove_minimals(ExprList,[Min|Mins],NewExprList) :-
   delete(ExprList,Min,HelpExprList),
   remove_minimals(HelpExprList,Mins,NewExprList).
remove_minimals(X,[],X).


termlist_to_functorlist([Term|Terms],[Func|Funcs]) :-
   functor(Term,Func,_),
   termlist_to_functorlist(Terms,Funcs).
termlist_to_functorlist([],[]).


rule_to_frule(Rule,Fs11-Fs22-Fs33) :-
   parse_dislog_rule(Rule,Head,PosBody,NegBody),
   termlist_to_functorlist(Head,Fs1),
   remove_duplicates(Fs1,Fs11),
   termlist_to_functorlist(PosBody,Fs2),
   remove_duplicates(Fs2,Fs22),
   termlist_to_functorlist(NegBody,Fs3),
   remove_duplicates(Fs3,Fs33).


dnlp_to_fdnlp([Rule|Rules],[FRule|FRules]) :-
   rule_to_frule(Rule,FRule),
   dnlp_to_fdnlp(Rules,FRules).
dnlp_to_fdnlp([],[]).


/* group_rules(Strata,Program,Groups) <-
      for the disjunctive normal logic program Program and the
      stratification Strata the rules of Program will be grouped
      w.r.t. Strata. */

group_rules(Strata,Program,Groups) :-
   group_rules(Strata,Program,[],Group),
   delete(Group,[],Groups).

group_rules([Stratum|Strata],Program,Lgroup,Group) :-
   create_group(Stratum,Program,[],HL),
   append(Lgroup,HL,NewLgroup),
   group_rules(Strata,Program,NewLgroup,Group).
group_rules([],_,Lgroup,Lgroup).


create_group(Stratum,[Prog|Progs],L,LL) :-
   parse_dislog_rule(Prog,As,_,_),
   termlist_to_functorlist(As,FAs1),
   remove_duplicates(FAs1,FAs),
   ( sublist(FAs,Stratum) ->
     append(L,[Prog],NewL)
   ; NewL = L ),
   !,
   create_group(Stratum,Progs,NewL,LL).
create_group(_,[],L,[L]).


group_ground_rules(Strata,Program,Groups) :-
   group_ground_rules(Strata,Program,[],Group),
   delete(Group,[],Groups).

group_ground_rules([Stratum|Strata],Program,Lgroup,Group) :-
   create_ground_group(Stratum,Program,[],HL),
   append(Lgroup,HL,NewLgroup),
   group_ground_rules(Strata,Program,NewLgroup,Group).
group_ground_rules([],_,Lgroup,Lgroup).


create_ground_group(Stratum,[Prog|Progs],L,LL) :-
   parse_dislog_rule(Prog,As,_,_),
   ( ( sublist(As,Stratum),
       append(L,[Prog],NewL) )
   ; NewL = L ),
   create_ground_group(Stratum,Progs,NewL,LL).
create_ground_group(_,[],L,[L]).


/******************************************************************/


