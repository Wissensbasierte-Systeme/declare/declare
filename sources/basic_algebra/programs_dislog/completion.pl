

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Completion                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gnaf_fs_completion(Program, Completion) <-
      */

gnaf_fs_completion(Program, Completion) :- 
   disjunctive_transformation(Program, Program2),
   simplify_program(Program2, State),
   state_prune(State, State2),
   state_can(State2, State3),
   gnaf_fs_operator(Program, State3, [], N_State),
   negate_state(N_State, Pos_Bodies),
   positive_bodies_to_integrity_constraints(
      Pos_Bodies, Completion).
    

/* clark_complete_pbe_program(Program1, Completion) <-
      */

clark_complete_pbe_program(Program1, Completion) :-
   hidden( (
      normal_transformation(Program1, Program2),
      ground_program_to_head_atoms(Program2, Atoms),
      complete_atoms_wrt_pbe_program(
         Atoms, Program2, Completion) ) ).


/*** implementation ***********************************************/


/* ground_program_to_head_atoms(Program, Atoms) <-
      */

ground_program_to_head_atoms(Program, Atoms) :-
   ground_program_to_head_atoms(Program, [], Lists),
   ord_union(Lists, Atoms).

ground_program_to_head_atoms([R|Rs], Sofar, Atoms) :-
   parse_dislog_rule(R,Head, _, _),
   New_Sofar = [Head|Sofar],
   ground_program_to_head_atoms(Rs,New_Sofar, Atoms).
ground_program_to_head_atoms([], Atoms, Atoms).


/* complete_atoms_wrt_pbe_program(Atoms, Program, Completion) <-
      */

complete_atoms_wrt_pbe_program([A|As], Program, Completion) :-
   complete_atoms_wrt_pbe_program([A|As], Program, [], Lists),
   ord_union(Lists, Completion).

complete_atoms_wrt_pbe_program(
      [A|As], Program, Sofar, Completion) :-
   complete_atom_wrt_pbe_program(A, Program, Completion1),
   New_Sofar = [Completion1|Sofar],
   complete_atoms_wrt_pbe_program(
      As, Program,New_Sofar, Completion).
complete_atoms_wrt_pbe_program([], _, Completion, Completion).

complete_atom_wrt_pbe_program(Atom, Program, Completion) :-
   compute_definition(Atom, Program, Definition),
   definition_to_negative_bodies(Definition, State),
   tree_based_state_to_coin(State, Coin),
%  state_to_coin(State, Coin),
   state_disjunction([[Atom]], Coin, Pos_Bodies),
   positive_bodies_to_integrity_constraints(
      Pos_Bodies, Completion).


/* compute_definition(Atom, Rs1, Rs2) <-
      */

compute_definition(Atom, [R1|Rs1], [R1|Rs2]) :-
   parse_dislog_rule(R1, [Atom], _, _),
   !,
   compute_definition(Atom, Rs1, Rs2).
compute_definition(Atom, [_|Rs1], Rs2) :-
   compute_definition(Atom, Rs1, Rs2).
compute_definition(_, [], []).


/* definition_to_negative_bodies(Definition, State) <-
      */

definition_to_negative_bodies([R|Rs], [Neg_Body|Cs]) :-
   parse_dislog_rule(R, _, _,Neg_Body),
   definition_to_negative_bodies(Rs, Cs).
definition_to_negative_bodies([], []).


/* positive_bodies_to_integrity_constraints(
         Pos_Bodies, Constraints) <-
      */

positive_bodies_to_integrity_constraints([B|Bs], [[]-B|Rs]) :-
   positive_bodies_to_integrity_constraints(Bs, Rs).
positive_bodies_to_integrity_constraints([], []).


/******************************************************************/


