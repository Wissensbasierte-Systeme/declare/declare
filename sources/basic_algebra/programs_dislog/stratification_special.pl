

/******************************************************************/
/***                                                            ***/
/***         DisLog:  Stratification                            ***/
/***                                                            ***/
/******************************************************************/


stratify_program_special_1(File) :-
   dconsult(File,Program),
   stratify_program_special_1(Program,Strata),
   pp_programs(Strata). 

stratify_program_special_1(Program,Strata) :-
   extract_integrity_constraints(
      Program,Constraints,Program2),
   stratify_program(Program2,Programs2),
   append(Ps,[P2],Programs2),
   ord_union(Constraints,P2,P),
   append(Ps,[P],Strata).

extract_integrity_constraints([],[],[]).
extract_integrity_constraints(
      [[]-B|Rs],[[]-B|Constraints],Program) :-
   !,
   extract_integrity_constraints(Rs,Constraints,Program).
extract_integrity_constraints(
      [[]-B1-B2|Rs],[[]-B1-B2|Constraints],Program) :-
   !,
   extract_integrity_constraints(Rs,Constraints,Program).
extract_integrity_constraints(
      [R|Rs],Constraints,[R|Program]) :-
   extract_integrity_constraints(Rs,Constraints,Program).


stratify_program_special_2(File) :-
   dconsult(File,Program),
   stratify_program_special_2(Program,Strata),
   pp_programs(Strata).

stratify_program_special_2(Program,Strata) :-
   encode_program(Program,Program2),
   min_stratify_program(Program2,_,Strata2),
   decode_programs(Strata2,Strata).

encode_program([],[]).
encode_program([[]-B|Rs1],[[false]-B|Rs2]) :-
   !,
   encode_program(Rs1,Rs2).
encode_program([[]-B1-B2|Rs1],[[false]-B1-B2|Rs2]) :-
   !,
   encode_program(Rs1,Rs2).
encode_program([R|Rs1],[R|Rs2]) :-
   encode_program(Rs1,Rs2).

decode_program(Program1,Program2) :-
   encode_program(Program2,Program1).

decode_programs([],[]).
decode_programs([P1|Ps1],[P2|Ps2]) :-
   decode_program(P1,P2),
   decode_programs(Ps1,Ps2).


/******************************************************************/


