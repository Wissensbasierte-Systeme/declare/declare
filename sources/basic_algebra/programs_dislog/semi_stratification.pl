

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Semi-Stratification                      ***/
/***                                                            ***/
/******************************************************************/


/* semi_stratify_program(Program,Programs) <-
      */

semi_stratify_program(Program,Programs) :-
   dlp_reduce(Program,_,Predicates_Multiple),
   maplist( program_to_definition_of_predicates(Program),
      Predicates_Multiple, Programs_2 ),
   reverse(Programs_2,Programs).

dlp_reduce(Program,U_Graph_Reduced,Sorting) :-
   program_to_vertices_and_edges(Program,Vertices,Edges),
   maplist( edge_without_label,
      Edges, Edges_2 ),
   vertices_edges_to_ugraph(Vertices,Edges_2,U_Graph),
%  writeln(vertices_edges_to_ugraph(Vertices,Edges_2,U_Graph)),
   reduce(U_Graph,U_Graph_Reduced),
   top_sort(U_Graph_Reduced,Sorting).

edge_without_label(P1-P2-_,P1-P2).


/* semi_stratify_normal_program(Program,Semi_Strata) <-
      The normal logic program Program1 is transformed to its
      sem-stratified version Semi_Strata. 
      Semi_Strata is a set of normal logic programs. */
 
semi_stratify_normal_program(Program,Semi_Strata) :-
   generate_edges_for_dependency_graph(Program,Edges),
   vertices_edges_to_ugraph([],Edges,Graph),
   reduce(Graph,ReducedGraph),
   top_sort(ReducedGraph,SCCs),
   find_sscs_with_negedges(Program,SCCs,NegSCCs),
   group_ground_rules(SCCs,Program,NegSCCs,[],Semi_Strata).

generate_edges_for_dependency_graph(Program,Edges) :-
   generate_edges_for_dependency_graph(Program,[],Edges).

generate_edges_for_dependency_graph([R|Rs],Acc,Edges) :-
   parse_dislog_rule(R,[Head],Bs,Ds),
   findall( Head-B,
      member(B,Bs),
      PosEdges ),
   findall( Head-D,
      member(D,Ds),
      NegEdges ),
   append([Acc,PosEdges,NegEdges,[Head-Head]],NewAcc),
   generate_edges_for_dependency_graph(Rs,NewAcc,Edges).
generate_edges_for_dependency_graph([],Edges,Edges).

find_sscs_with_negedges(Program,SCCs,NegSCCs) :-
   findall( SCC, 
      ( member(R,Program),
        parse_dislog_rule_save(R,[Head],_,Ds),
        member(SCC,SCCs),
        member(D,Ds), member(Head,SCC), member(D,SCC) ),
      NegSCCs1 ),
   list_to_ord_set(NegSCCs1,NegSCCs).

group_ground_rules([SCC|SCCs],Program,NegSCCs,Acc,Groups) :-
%  setof2( As-Bs-Ds,
   findall( As-Bs-Ds,
      ( member(R,Program),
        parse_dislog_rule_save(R,As,Bs,Ds),
        ord_subset(As,SCC) ),
      Rules_2 ),
   list_to_ord_set(Rules_2,Rules),
   ( ( Rules \== [],
       ( ( member(SCC,NegSCCs),
           Group = [nonstratified|Rules] )
       ; Group = [stratified|Rules] ),
     NewAcc = [Group|Acc],
     list_to_ord_set(Program,OrdP),
     ord_subtract(OrdP,Rules,NewP) )
   ; ( NewAcc = Acc, NewP = Program ) ),
   group_ground_rules(SCCs,NewP,NegSCCs,NewAcc,Groups).
group_ground_rules([],_,_,Acc,Acc).


/******************************************************************/


