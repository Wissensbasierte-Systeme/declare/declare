

/******************************************************************/
/***                                                            ***/
/***         DisLog:  All Stratifications                       ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* program_to_all_stratifications <-
      */

program_to_all_stratifications :-
%  dc('teaching/railway_for_stratifications.dl', Program),
%  dc('teaching/sink_for_stratifications.dl', Program),
%  dc('teaching/arc_node_tc.dl', Program),
   dc('nmr/perfect_models/program.dl', Program),
   program_to_all_stratifications(Program, Stratifications),
   writeln_list(Stratifications),
%  checklist( dportray(chs),
%     Stratifications ),
   length(Stratifications, N),
   writeln(user, N),
   ( foreach(S1, Stratifications),
     foreach(S2, Stratifications_2) do
        findall( Set,
           ( member(Set, S1), Set \= [] ),
           S2 ) ),
   sort(Stratifications_2, Stratifications_3),
   writeln_list(Stratifications_3).


/* program_to_all_stratifications(Program, Stratifications) <-
      */

program_to_all_stratifications(Program, Stratifications) :-
   stratify_generate_relationships(Program, Relationships),
   writeln(user, Relationships),
   dc('nmr/perfect_models/stratifications.dl', Stratifier),
   append(Relationships, Stratifier, Dnlp),
   program_to_stable_models_dlv(Dnlp, Models),
%  maplist( interpretation_to_stratification,
%     Models, Stratifications_2 ),
%  findall( Stratification,
%     ( member(Model, Models),
%       interpretation_to_stratification(Model, Stratification) ),
%     Stratifications_2 ),
   ( foreach(Model, Models),
     foreach(Stratification, Stratifications_2) do
        interpretation_to_stratification(Model, Stratification) ),
   Stratifications = Stratifications_2.
%  sort(Stratifications_2, Stratifications_3),
%  length_order(Stratifications_3, Stratifications).


/*** implementation ***********************************************/


/* stratify_generate_relationships(Program, Relationships) <-
      */

stratify_generate_relationships(Program, Relationships) :-
   stratify_generate_relationships(Program, Eq, Geq, G),
   append([Eq, Geq, G], Relationships_2),
   sort(Relationships_2, Relationships).

stratify_generate_relationships(Program, Eq, Geq, G) :-
   dnlp_to_fdnlp(Program, FProgram),
   create_expression_lists(FProgram, [], [], [],
      EqualList, GreqList, GrthList),
   findall( [equal(X, Y)],
      member(X eq Y, EqualList),
      Eq ),
   findall( [greater_equal(X, Y)],
      member(X ge Y, GreqList),
      Geq ),
   findall( [greater(X, Y)],
      member(X gt Y, GrthList),
      G ).


/* interpretation_to_stratification(I, Stratification) <-
      */

interpretation_to_stratification(I, Stratification) :-
   findall( Predicate,
      ( member(Atom, I),
        Atom =.. [_|Ps],
        member(Predicate, Ps) ),
      Predicates_2 ),
   sort(Predicates_2, Predicates),
   maplist( predicate_to_stratum(I),
      Predicates, Stratification_2 ),
   sort(Stratification_2, Stratification_3),
   !,
   topological_sort( smaller_stratum(I),
      Stratification_3, Stratification ).

predicate_to_stratum(I, Predicate, Predicates) :-
   findall( P,
      member(equal(Predicate, P), I),
      Predicates_2 ),
   sort(Predicates_2, Predicates).


/* topological_sort(Compare, Xs, Ys) <-
      */

topological_sort(_, [], []) :-
   !.
topological_sort(_, [X], [X]) :-
   !.
topological_sort(Compare, Xs, Ys) :-
   topological_sort_divide(Xs, Xs1, Xs2),
   topological_sort(Compare, Xs1, Ys1),
   topological_sort(Compare, Xs2, Ys2),
   topological_sort_merge(Compare, Ys1, Ys2, Ys).

topological_sort_divide(Xs, Xs1, Xs2) :-
   length(Xs, N),
   M is round(N/2),
   first_n_elements(M, Xs, Xs1),
   append(Xs1, Xs2, Xs),
   !.

topological_sort_merge(_, Ys1, [], Ys1) :-
   !.
topological_sort_merge(_, [], Ys2, Ys2) :-
   !.
topological_sort_merge(Compare, [Y1|Ys1], [Y2|Ys2], [Y1|Ys]) :-
   apply(Compare, [Y1, Y2]),
   !,
   topological_sort_merge(Compare, Ys1, [Y2|Ys2], Ys).
topological_sort_merge(Compare, [Y1|Ys1], [Y2|Ys2], [Y2|Ys]) :-
   apply(Compare, [Y2, Y1]),
   !,
   topological_sort_merge(Compare, [Y1|Ys1], Ys2, Ys).
topological_sort_merge(Compare, [Y1|Ys1], [Y2|Ys2], [Y1|Ys]) :-
   topological_sort_merge(Compare, Ys1, [Y2|Ys2], Ys).
topological_sort_merge(Compare, [Y1|Ys1], [Y2|Ys2], [Y2|Ys]) :-
   topological_sort_merge(Compare, [Y1|Ys1], Ys2, Ys).


/* smaller_stratum(I, Xs, Ys) <-
      */

smaller_stratum(I, Xs, Ys) :-
   member(X, Xs),
   member(Y, Ys),
   ( member(greater(Y, X), I)
   ; member(greater_equal(Y, X), I) ),
   !.


/******************************************************************/


