

/******************************************************************/
/***                                                            ***/
/***         DisLog:  TU-Annotation                             ***/
/***                                                            ***/
/******************************************************************/

 

/*** interface ****************************************************/


/* coin_tu_annotated_to_partial(HB,Coin1,Coin2) <-
      Given a partial coin Coin1 in tu-representation,
      and the Herbrand Base HB.  Coin1 is transformed 
      to its literal representation Coin2. */

coin_tu_annotated_to_partial(HB,Coin1,Coin2) :-
   maplist(
      interpretation_tu_annotated_to_partial(HB),
      Coin1, Coin2 ).

interpretation_tu_annotated_to_partial(HB,Int_1,Int_2) :-
   findall( A, member(t(A),Int_1), T ),
   findall( A, member(u(A),Int_1), U ),
   ord_subtract(HB,U,F),
   negate_atoms(F,FN),
   ord_union(T,FN,Int_2).
 

/* truth_annotate_transformation(V,Program,A_Program) <-
      Given a DNLP Program, and the number V of truth values.
      For V = 2, the t-transformation A_Program is computed.
      For V = 3, the tu-transformation A_Program is computed. */

truth_annotate_transformation(2,Program,A_Program) :-
   t_transformation(Program,A_Program).
truth_annotate_transformation(3,Program,A_Program) :-
   tu_transformation(Program,A_Program).


/* tu_transformation(Program1,Program2) <-
      The DNLP Program1 is tu-annotated,
      which yields the annotated DNLP Program2. */

tu_transformation(Database_Id) :-
   dconsult(Database_Id,Program1),
   tu_transformation(Program1,Program2),
   dportray(lp,Program2).

tu_transformation(Program1,[[u(A)]-[t(A)]|Program2]) :-
   annotate_program_tu(Program1,Program2).

t_transformation(Program1,Program2) :-
   maplist( annotate_rule(t-t-t),
      Program1, Program2 ).
 

/* annotate_program_tu(Rules,Annotated_Rules) <-
      The rules in Rules are annotated by t-t-u and u-u-t,
      which yields the set Annotated_Rules. */
 
annotate_program_tu([R|Rs1],[R_t,R_u|Rs2]) :-
   annotate_rule_t(R,R_t),
   annotate_rule_u(R,R_u),
   annotate_program_tu(Rs1,Rs2).
annotate_program_tu([],[]).
 
annotate_program_t(Rules,Rules_t) :-
   maplist( annotate_rule_t,
      Rules, Rules_t ).
annotate_program_u(Rules,Rules_u) :-
   maplist( annotate_rule_u,
      Rules, Rules_u ).
 
annotate_rule_t(R,R_t) :-
   annotate_rule(t-t-u,R,R_t).
annotate_rule_u(R,R_u) :-
   annotate_rule(u-u-t,R,R_u).
 

/* annotate_rule(VH-VP-VN,Rule1,Rule2) <-
      Rule1 is annotated, the result is Rule2.
      The head of Rule1 is annotated by VH, the postive body
      by VP, and the negative body by VN. */

annotate_rule(VH-VP-VN,R1,R2) :-
   parse_dislog_rule(R1,H1,P1,N1),
   annotate_clause(VH,H1,H2),
   annotate_clause(VP,P1,P2),
   annotate_clause(VN,N1,N2),
   R2 = H2-P2-N2.
 
annotate_clause(Annotation,[L1|Ls1],[L2|Ls2]) :-
   L2 =.. [Annotation,L1],
   annotate_clause(Annotation,Ls1,Ls2).
annotate_clause(_,[],[]).
 

/* tu_knowledge_smaller(Int_1,Int_2) <-
      Given two partial Herbrand interpretations Int_1 and Int_2
      in tu-representation, it is tested whether Int_1 is smaller
      than Int_2 in the knowledge ordering. */

tu_knowledge_smaller(Int_1,Int_2) :-
   \+ tu_knowledge_violation(Int_1,Int_2).
 
tu_knowledge_violation(Int_1,Int_2) :-
   tu_knowledge_violation(_,_,Int_1,Int_2).
 
tu_knowledge_violation(t,Atom,Int_1,Int_2) :-
   tu_truth_value(Int_1,Atom,t),
   tu_truth_value(Int_2,Atom,u).
tu_knowledge_violation(f,Atom,Int_1,Int_2) :-
   tu_truth_value(Int_1,Atom,f),
   tu_truth_value(Int_2,Atom,u).


/* tu_truth_value(Int,Atom,V) <-
      Given a partial Herbrand interpretation Int in tu-repre-
      sentation and an atom Atom, the truth value V of Atom 
      under Int is computed. */

tu_truth_value(Int,Atom,t) :-
   tu_member(t,Atom,Int),
   !.
tu_truth_value(Int,Atom,u) :-
   tu_member(u,Atom,Int),
   !.
tu_truth_value(_Int,_Atom,f).
 
tu_member(V,Atom,Int) :-
   A =.. [V,Atom],
   member(A,Int).

 
/******************************************************************/


