

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Program Operations                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dislog_program_is_for_tp_iteration_prolog(Rules) <-
      */

dislog_program_is_for_tp_iteration_prolog(Rules) :-
   \+ ( member(Rule, Rules),
        parse_dislog_rule(Rule, Head, _, _),
        length(Head, N),
        N > 1 ).


/* dislog_program_is_disjunctive(Rules) <-
      */

dislog_program_is_disjunctive(Rules) :-
   member(Rule, Rules),
   parse_dislog_rule(Rule, Head, _, Neg),
   ( Head \= [_]
   ; Neg \= [] ).


/* dislog_program_is_definite(Rules) <-
      */

dislog_program_is_definite(Rules) :-
   \+ dislog_program_is_disjunctive(Rules).


/* rule_to_atom(Rule, Atom) <-
      */

rule_to_atom(Rule, Atom) :-
   no_singleton_variable(Atom),
   parse_dislog_rule(Rule, Head, Pos, Neg),
   negate_literals(Neg, Neg_2),
   append(Pos, Neg_2, Body),
   ( list_to_semicolon_structure(Head, Head_2),
     term_to_atom(Head_2, Head_3)
   ; Head_3 = '' ),
   no_singleton_variable(Body_3),
   ( list_to_comma_structure(Body, Body_2),
     term_to_atom(Body_2, Body_3),
     concat([Head_3, ' :- ', Body_3], Atom)
   ; Body_3 = '',
     Atom = Head_3 ),
   !.


/* program_to_definition_of_predicates(
         Program_1, Predicates, Program_2) <-
      */

program_to_definition_of_predicates(
      Program_1, Predicates, Program_2) :-
   findall( Rule,
      ( member(Rule, Program_1),
        parse_dislog_rule(Rule, Head, _, _),
        atoms_to_predicate_symbols(Head, Ps),
        ord_intersect(Ps, Predicates) ),
      Program_2 ),
   dportray(lp, Program_2).


/* ground_program_to_occuring_atoms(Program, Atoms) <-
      */

ground_program_to_occuring_atoms(Program, Atoms) :-
   findall( Atom,
      ( member(Rule, Program),
        parse_dislog_rule(Rule, Head, Pos_Body, Neg_Body),
        ( member(Atom, Head)
        ; member(Atom, Pos_Body)
        ; member(Atom, Neg_Body) ) ),
      List_of_Atoms ),
   list_to_ord_set(List_of_Atoms, Atoms).


/******************************************************************/


