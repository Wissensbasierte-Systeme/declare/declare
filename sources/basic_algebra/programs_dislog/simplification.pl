

/******************************************************************/
/***                                                            ***/
/***         DisLog:  Simplify Programs                         ***/
/***                                                            ***/
/******************************************************************/


simplify_program_gamma_pbe_dis(Program1,Program2) :- 
   gamma_operator_ground_pbe(Program1,[],I_Upper), 
   simplify_program_2(Program1,[],I_Upper,Program), 
   list_to_ord_set(Program,Program2).


/* simplify_program_gamma_2_pbe(Program1,Program2) <-
      The normal pbe-program Program1 is simplified using the
      operator gamma_2. */

simplify_program_gamma_2_pbe(Program1,Program2) :-
   simplify_program_gamma_2_pbe(Program1,_,Program2).

simplify_program_gamma_2_pbe(Program1,GrdP,Program2) :-
%  standardize_program(Program1,GrdP),
   GrdP = Program1,
   gamma_2_operator_ground_pbe(GrdP,I_Upper,I_Lower),
%  writeln(user,I_Lower), writeln(user,I_Upper),
   simplify_program_2(GrdP,I_Lower,I_Upper,Program),
   list_to_ord_set(Program,Program2).

gamma_2_operator_ground_pbe(Program,I1,I2) :-
   gamma_operator_ground_pbe(Program,[],I1),
   gamma_operator_ground_pbe(Program,I1,I2).

gamma_operator_ground_pbe(Program,I1,I2) :-
   gl_transformation(Program,I1,State),
   ord_union(State,I2).


simplify_program_pbe_can(Program1,Program2) :-
   evidential_transformation(Program1,State1),
   state_can(State1,State2),
   evidential_transformation_reverse(State2,Program2).
   

simplify_program_3_mgf(Program1,Program3) :-
   select_facts_dnlp(Program1,State),   
   simplify_program_3_a(Program1,State,[],Program2),
   modified_ground_folding_program(Program2,Program3).

simplify_program_3(Program1,State,Program2) :-
   simplify_program_3_a(Program1,State,[],Program2).
 
simplify_program_3(Program1,Program2) :-
   select_facts_dnlp(Program1,State),   
   simplify_program_3_a(Program1,State,[],Program2).
 
simplify_program_3_a([Rule|Rules],State,Acc,P_sim) :-
   parse_dislog_rule(Rule,_,_,NegBody),
   ( ( state_subsumes(State,[NegBody]),
       NewAcc = Acc )
   ; NewAcc = [Rule|Acc] ),
   simplify_program_3_a(Rules,State,NewAcc,P_sim).
simplify_program_3_a([],_,P_sim,P_sim).


select_facts_dnlp(Program,State) :-
   select_facts_dnlp(Program,[],State).

select_facts_dnlp([Rule|Rules],Acc,State) :-
   parse_dislog_rule(Rule,Head,Pos_Body,Neg_Body),
   ( ( Pos_Body = [],
       Neg_Body = [],
       NewAcc = [Head|Acc] )
   ; NewAcc = Acc ),
   select_facts_dnlp(Rules,NewAcc,State).
select_facts_dnlp([],State,State).


/******************************************************************/

 
