

/******************************************************************/
/***                                                            ***/
/***           CMS:  Doc Browser Loader                         ***/
/***                                                            ***/
/******************************************************************/


:- retractall(file_search_path(cms, _)),
   dislog_variable_get(source_path,
      'basic_algebra/utilities_xpce/', Path),
   assert(file_search_path(cms, Path)).

:- use_module(cms(cms_doc_browser)).


/******************************************************************/


