

google_search_for_first_appearance(Name_1, Name_2) :-
   for(I, 0, 10) do
      ( N is 10 * I,
        google_search_to_file(Name_1, N, File),
        read_file_to_name(File, Text),
        ( name_contains_name(Text, Name_2) ->
          write_list(user, [N, ' - yes \n']),
          wait
        ; write_list(user, [N, ' - no  \n']) ) ).

google_search_to_file(Name, N, File) :-
   concat([
      'http://www.google.de/search?',
      'hl=de&client=firefox-a&',
      'rls=com.ubuntu:en-GB:unofficial&hs=4Ex&',
      'q=', Name, '&start=', N, '&sa=N'], Url),
   get_url_to_file(Url, File).


