

/******************************************************************/
/***                                                            ***/
/***           Utilities:  Terms to Picture                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* read_term_to_picture <-
      */

read_term_to_picture :-
   read_term(Term, [variable_names(V_Names)]),
   term_to_picture(Term, V_Names, _).

   
/* term_to_picture(Term, V_Names, Picture) <-
      */

term_to_picture(Term) :-
   term_to_picture(Term, [], _).

term_to_picture(Term, V_Names, Picture) :-
   dislog_variable_switch(fn_mode, Mode, fn),
   term_to_vertices_and_edges(Term, V_Names, Vertices, Edges),
   vertices_edges_to_picture_bfs(Vertices, Edges, Picture),
   dislog_variable_set(fn_mode, Mode),
   !.


/* term_to_vertices_and_edges(
         Term, V_Names, Vertices, Edges) <-
      */

term_to_vertices_and_edges(Term, V_Names, Vertices, Edges) :-
   gensym('#_node_', Id),
   term_to_vertices_and_edges(
      Id, Term, V_Names, Vertices, Edges).


/* substitution_to_picture(Substitution) <-
      */

substitution_to_picture(Substitution) :-
   substitution_to_picture([], Substitution).

substitution_to_picture(Variables, Substitution) :-
   edges_to_vertices(Substitution, Vs),
   ( foreach(V, Vs), foreach(Vertex, Vertices) do
        ( member(V, Variables),
          Vertex = V-V-rhombus-red-small
        ; Vertex = V-V-rhombus-grey-small ) ),
   vertices_edges_to_picture(Vertices, Substitution).


/* substitution_multiply(S1, S2, S3) <-
      */

substitution_multiply(S1, S2, S3) :-
   maplist( substitution_multiply_(S2),
      S1, S4),
   findall( X-Y,
      ( member(X-Y, S2),
        \+ ( member(X2-_, S1), X == X2 ) ),
      S5 ),
   append(S4, S5, S6),
   findall( X-Y,
      ( member(X-Y, S6),
        X \== Y ),
      S3 ).

substitution_multiply_(S, X-Y1, X-Y2) :-
   substitution_apply_(S, Y1, Y2).

substitution_apply_(S, Y1, Y2) :-
   ( atomic(Y1)
   ; var(Y1) ),
   !,
   ( member(X-Y2, S),
     X == Y1
   ; Y2 = Y1 ).
substitution_apply_(S, Y1, Y2) :-
   Y1 =.. [F|Ys1],
   maplist( substitution_apply_(S),
      Ys1, Ys2 ),
   Y2 =.. [F|Ys2].


/*** implementation ***********************************************/


/* term_to_vertices_and_edges(
         Id, Term, V_Names, Vertices, Edges) <-
      */

term_to_vertices_and_edges(Id, Term, V_Names, [V], []) :-
   var(Term),
   !,
   variable_and_variables_to_xml(V_Names, Term, Name),
   V = Id-Name-circle-red-small.
term_to_vertices_and_edges(Id, Term, V_Names, Vertices, Edges) :-
   Term =.. [F|As],
   maplist( term_to_vertices_and_edges_sub(Id, V_Names),
      As, Pairs ),
   pair_lists_2(Vss, Ess, Pairs),
   ( As = [],
     Colour = blue
   ; As \= [],
     Colour = white ),
   ( F = [],
     Label = '[ ]'
   ; F \= [],
     Label = F ),
   V = Id-Label-circle-Colour-small,
   append([[V]|Vss], Vertices),
   append(Ess, Edges).

term_to_vertices_and_edges_sub(Id, V_Names, A, Vs-Es) :-
   gensym('#_node_', Id_2),
   E = Id-Id_2,
   term_to_vertices_and_edges(Id_2, A, V_Names, Vs, Es_2),
   Es = [E|Es_2].

/*
term_to_vertices_and_edges(Id, Term, V_Names, Vertices, Edges) :-
   Term =.. [F|As],
   ( foreach(A, As), foreach(Es, Ess), foreach(Vs, Vss) do
        gensym('#_node_', Id_2),
        E = Id-Id_2,
        term_to_vertices_and_edges(Id_2, A, V_Names, Vs, Es_2),
        Es = [E|Es_2] ),
   ( As = [],
     Colour = blue
   ; As \= [],
     Colour = white ),
   V = Id-F-circle-Colour-small,
   append([[V]|Vss], Vertices),
   append(Ess, Edges).
*/


/*** tests ********************************************************/


test(term_to_picture, 1) :-
   Term = t(a(X, Y), f(b, X)),
   V_Names = ['X'=X, 'Y'=Y],
   term_to_picture(Term, V_Names).

test(term_to_picture, 2) :-
   Term =
   ( foreach(A, As), foreach(B, Bs), foreach(C, Cs) do
        add(A, B, C) ),
   V_Names = ['A'=A, 'B'=B, 'C'=C, 'As'=As, 'Bs'=Bs, 'Cs'=Cs ],
   term_to_picture(Term, V_Names).


/******************************************************************/


