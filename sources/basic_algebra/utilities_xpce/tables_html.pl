

/******************************************************************/
/***                                                            ***/
/***          Declare:  HTML Tables in XPCE                     ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(html_display_table, 1) :-
   html_display_table('Table', [a,b,c],
      [[1,2,3],[4,5,6],[7,8,9]],
      size(200, 150)).


/*** interface ****************************************************/


/* html_display_table(Title, Attributes, Tuples) <-
      */

html_display_table(Title, Attributes, Tuples) :-
   html_display_table(Title, Attributes, Tuples, _).

html_display_table(Title, Attributes, Tuples, Size) :-
   html_create_table(Attributes, Tuples, Table),
   concat(['<title>', Title, '</title>'], T),
   Document = html:[]:[
%     head:[]:[title:Title],
      head:[]:[T],
      body:[]:[Table]],
   html_to_display(Document, Size),
   !.


/* html_create_table(Attributes, Tuples, Table) <-
      */

html_create_table(Attributes, Tuples, Table) :-
%  html_create_table_row(th, '#d9d9d9', Attributes, Header),
   html_create_table_row(th, '#dfdfff', Attributes, Header),
%  html_create_table_rows(['#ffffff', '#eeeeee'], Tuples, Rows),
   html_create_table_rows(['#ffffff', '#e9e9ff'], Tuples, Rows),
   Table = table:[
      'BORDER':1, cellspacing:1, 'BGCOLOR':'#eeeeee']:[
      Header|Rows].


/* html_create_table_rows(Colors, Tuples, Rows) <-
      */

html_create_table_rows(Colors, Tuples, Rows) :-
   html_create_table_rows(Colors, Colors, Tuples, Rows).

html_create_table_rows(Colors, [C|Cs], [T|Ts], [R|Rs]) :-
   html_create_table_row(td, C, T, R),
   html_create_table_rows(Colors, Cs, Ts, Rs).
html_create_table_rows(Colors, [], [T|Ts], [R|Rs]) :-
   html_create_table_rows(Colors, Colors, [T|Ts], [R|Rs]).
html_create_table_rows(_, _, [], []).


/* html_create_table_row(Tag, Background_Color, Values, Row) <-
      */

html_create_table_row(Tag, Background_Color, Values, Row) :-
   maplist( html_create_table_row_entry(Tag, Background_Color),
      Values, Entries ),
   Row = tr:[]:Entries.

html_create_table_row_entry(
      Tag, Background_Color, Value, Entry) :-
   Entry = Tag:['BGCOLOR':Background_Color]:[Value].


/******************************************************************/


