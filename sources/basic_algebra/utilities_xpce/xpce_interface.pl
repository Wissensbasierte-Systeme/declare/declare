

/******************************************************************/
/***                                                            ***/
/***        XPCE-Dislog:  Basic Interface Predicates            ***/
/***                                                            ***/
/******************************************************************/



/* rgb_to_colour([R,G,B],colour(@default,Red,Green,Blue)) <-
      */

rgb_to_colour([R,G,B],colour(@default,Red,Green,Blue)) :-
   Max is 65535,
   vector_multiply(Max/16,[R,G,B],Xs),
   maplist( round,
      Xs, [Red,Green,Blue] ).


:- rgb_to_colour([14,14,14],Colour),
   dislog_variable_set(
      dislog_frame_interface_color, Colour ).


/* rgb_to_colour_hex([R,G,B], Hex) <-
      */

rgb_to_colour_hex([R,G,B], Hex) :-
   vector_multiply(16, [R,G,B], [R1,G1,B1]),
   maplist( truncate,
      [R1,G1,B1], [R2,G2,B2] ),
   maplist( number_to_hex,
      [R2,G2,B2], [R3,G3,B3] ),
   concat(['#',R3,G3,B3], Hex).


/* colour_hex_to_rgb(Hex, [R,G,B]) <-
      */

colour_hex_to_rgb(Hex, [R,G,B]) :-
   concat('#', Xs, Hex),
   name(Xs, Ns),
   maplist( hex_to_number_1,
      Ns, [R1,R2, G1,G2, B1,B2] ),
   maplist( hex_to_number_2,
      [[R1,R2], [G1,G2], [B1,B2]], [R,G,B] ).


/*** part a *******************************************************/


/* dislog_frame_generic(Description) <-
      */

dislog_frame_generic(Description) :-
   dislog_frame_generic(_,Description).

dislog_frame_generic(Frame,Description) :-
   [ Header, Buttons_Above, Interface, Buttons_2, CB ] :=
   Description^[ header, above, interface, below,
     close_button ],
   new(Frame,frame(Header)),
   ( CB = [Name - Where],
     Close_Button = [Name - send(Frame,destroy) - Where]
   ; Close_Button = [] ),
   ( ( Buttons_Above = [],
       !,
       send(Frame,append,new(Dialog_I,dialog)),
       append(Buttons_2,Close_Button,Buttons_B) )
   ; ( send(Frame,append,new(Dialog_A,dialog)),
       send(new(Dialog_I,dialog),below,Dialog_A),
       append(Buttons_Above,Close_Button,Buttons_A),
       checklist( send_append(Dialog_A),
          Buttons_A ),
       Buttons_B = Buttons_2 ) ),
   send(new(Dialog_B,dialog),below,Dialog_I),
   dislog_variable_get(dislog_frame_interface_color,Colour),
   send(Dialog_I,background,Colour),
   dislog_frame_generic_fill_i_and_b(
      Dialog_I,Interface, Dialog_B,Buttons_B ),
   send(Frame,open).

dislog_frame_generic_fill_i_and_b(
      Dialog_I, Interface, Dialog_B, Buttons_B ) :-
   maplist( pair_to_triple,
      Interface, Triples ),
   checklist( send_text_menu(Dialog_I),
      Triples ),
   checklist( triple_to_dislog_variable,
      Triples ),
   checklist( send_append(Dialog_B),
      Buttons_B ).


/* dislog_dialog_generic(Dialog,Header,Interface,Buttons) <-
      */

dislog_dialog_generic(Dialog,Header,Interface,Buttons) :-
   new(Dialog,dialog(Header)),
   maplist( pair_to_triple,
      Interface, Triples ),
   checklist( send_text_menu(Dialog),
      Triples ),
   checklist( triple_to_dislog_variable,
      Triples ),
   checklist( send_append(Dialog),
      Buttons ).


/* dislog_dialog_generic(Header,Interface,Buttons) <-
      */

dislog_dialog_generic(Header,Interface,Buttons) :-
   dislog_dialog_generic(Dialog,Header,Interface,Buttons),
   send(Dialog,append,button('Close',
      message(Dialog,destroy))),
   send(Dialog,open).


/* triple_to_dislog_variable(Label-_-Address) <-
      */

triple_to_dislog_variable(Label-_-Address) :-
   dislog_variable_set(Label,Address).


/* pair_to_triple(X-Y,X-Y-_) <-
      */

pair_to_triple(X-Y,X-Y-_).


/* dislog_dialog_variables(Operation,Pairs) <-
      */

dislog_dialog_variables(Operation,Pairs) :-
   checklist( dislog_dialog_variable(Operation),
      Pairs ).

dislog_dialog_variable(Operation,Label:Value) :-
   dislog_dialog_variable(Operation,Label-Value).

dislog_dialog_variable(Operation,Label-Value) :-
   dislog_variable_get(Label,Address),
   Goal =.. [Operation,Address,selection,Value],
   call(Goal).


/* xpce_formular_get(Label-Value)
   xpce_formular_get(Label,Value) <-
      */

xpce_formular_get(Label-Value) :-
   dislog_dialog_variable(get,Label-Value).

xpce_formular_get(Label,Value) :-
   dislog_dialog_variable(get,Label-Value).


/* xpce_formular_send(Label-Value),
   xpce_formular_send(Label,Value) <-
      */

xpce_formular_send(Label-Value) :-
   dislog_dialog_variable(send,Label-Value).

xpce_formular_send(Label,Value) :-
   dislog_dialog_variable(send,Label-Value).


/*** part b *******************************************************/


/* send_pulldown_menu(Window_Address,
         Button_Label,Labels_and_Operations) <-
      */
       
send_pulldown_menu(Window_Address,
      Button_Label,Labels_and_Operations) :-
   send_pulldown_menu(Window_Address,
      Button_Label,Labels_and_Operations,_).

send_pulldown_menu(Window_Address,
      Button_Label,Labels_and_Operations,
      Button_Address) :-
   send(Window_Address,append,
      new(Button_Address,popup(Button_Label))),
   maplist(label_and_operation_to_menu_item,
      Labels_and_Operations,Menu_Items),
%  writeln(send_list(Button_Address,append,Menu_Items)),
   send_list(Button_Address,append,Menu_Items).

label_and_operation_to_menu_item(
      Label-Operation,Menu_Item) :-
   Operation =.. Operation_List,
   Message =.. [message,@prolog|Operation_List],
   Menu_Item = menu_item(Label,Message).


/* send_text_menu(
         Target, Menu_Name - DisLog_Variable - Reference ) <-
      */

send_text_menu(Target,
      Menu_Name - DisLog_Variable - Reference ) :-
   dislog_variable_get(DisLog_Variable,Value),
   send(Target,append,
      new(Reference,text_item(Menu_Name,Value))).


/* send_choice_menu_gen(Target,Xs) <-
      */

send_choice_menu_gen(Target,Xs) :-
   minus_separated_list_to_list(Xs,
      [Menu_Name,DisLog_Variable|Values]),
   send_choice_menu_gen(Target,Menu_Name,
      DisLog_Variable,Values).

send_choice_menu_gen(Target,Menu_Name,
      DisLog_Variable,Values) :-
   send(Target,append,
      new(Menu_Reference,menu(Menu_Name,choice)),below),
   dislog_variable_get(DisLog_Variable,Value),
   maplist( determine_on_off_value(Value),
      Values, Selected_List ),
   maplist( determine_append_menu_item(DisLog_Variable),
      Values, Append_Menu_Items ),
   append([
      [ gap:size(5,5),
%       alignment:right,
        layout:horizontal ],
      Append_Menu_Items, Selected_List ], List ),
   send_multiple(Menu_Reference,List).


determine_on_off_value(Value,Value,selected:Value:'@on') :-
   !.
determine_on_off_value(_,Value,selected:Value:'@off').


determine_append_menu_item(DisLog_Variable,Value,
      append:menu_item(Value,message(@prolog,
         dislog_variable_set,DisLog_Variable,Value)) ).


/* send_choice_menu <-
      */

send_choice_menu(Target,
      Menu_Name - DisLog_Variable - Value_1 - Value_2) :-
   send_choice_menu(Target,Menu_Name,
      DisLog_Variable,Value_1,Value_2).

send_choice_menu(Target,Menu_Name,
      DisLog_Variable,Value_1,Value_2) :-
   send(Target,append,
      new(Menu_Reference,menu(Menu_Name,choice)),below),
   dislog_variable_get(DisLog_Variable,Value),
   determine_on_off_values(Value,
      Value_1:On_Off_1,Value_2:On_Off_2),
   send_multiple(Menu_Reference,[
      gap:size(5,5),
      layout:horizontal,
%     alignment:right,
      append:menu_item(Value_1,message(@prolog,
         dislog_variable_set,DisLog_Variable,Value_1)),
      append:menu_item(Value_2,message(@prolog,
         dislog_variable_set,DisLog_Variable,Value_2)),
      selected:Value_1:On_Off_1,
      selected:Value_2:On_Off_2]).


/* send_multiple(Object_Reference,Xs) <-
      */

send_multiple(Object_Reference,[X|Xs]) :-
   colon_structure_to_list(X,Y),
   Goal =.. [send,Object_Reference|Y],
   call(Goal),
   send_multiple(Object_Reference,Xs).
send_multiple(_,[]).

   
/* determine_on_off_values <-
      */

determine_on_off_values(X, X:'@on', _:'@off') :-
   !.
determine_on_off_values(X, _:'@off', X:'@on').


/* send_append(Dialog,Label-Function-Location),
   send_append(Dialog,Label-Function) <-
      */

send_append(Dialog,Label-Function-Location) :-
   !,
   Function =.. Args,
   Message =.. [message,@prolog|Args],
   send(Dialog,append,button(Label,Message),Location).
send_append(Dialog,Label-Function) :-
   !,
   Function =.. Args,
   Message =.. [message,@prolog|Args],
   send(Dialog,append,button(Label,Message)).


/*** part c *******************************************************/


/* ask_name(Label,Name,Result) <-
      */

ask_name(Label,Name,Result) :-
   new(Dialog,dialog(Label)),
   send(Dialog,append,
      new(Text_Item,text_item(Name,''))),
   send(Dialog,append,button('Accept',
      message(Dialog,return,Text_Item?selection))),
   send(Dialog,append,button('Cancel',
      message(Dialog,return,@nil))),
%  send(Dialog,default_button,ok),
   get(Dialog,confirm,Answer),
   send(Dialog,destroy),
   Answer \== @nil,
   Result = Answer.


/******************************************************************/


