

/******************************************************************/
/***                                                            ***/
/***          DisLog:   XPCE Tables                             ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library(tabular)).
:- use_module(library(autowin)).


/*** examples *****************************************************/


test(xpce_display_table_for_tests, 1) :-
   xpce_display_table_for_tests(
      _, _, [a,b,c], [[1,2,3], [4,5,6], [1,2,3], [4,5,6]]).


/*** interface ****************************************************/


/* xpce_display_table_for_tests(Attributes, Rows) <-
      */

xpce_display_table_for_tests(Attributes, Rows) :-
   xpce_display_table_for_tests(_, _, Attributes, Rows).


/* xpce_display_table_for_tests(
         Table_Window, Tabular, Attributes, Rows) <-
      */

xpce_display_table_for_tests(
      Table_Window, Tabular, Attributes, Rows) :-
   xpce_display_table_for_tests(
      Table_Window, Tabular, 'Table', Attributes, Rows).

xpce_display_table_for_tests(
      Table_Window, Tabular, Title, Attributes, Rows) :-
   new(Table_Window, auto_sized_picture(Title)),
   send(Table_Window, display, new(Tabular, tabular)),
%  send(Tabular, border, 1),
   send(Tabular, border, 0),
%  send(Tabular, cell_padding, size(8, 4)),
%  send(Tabular, cell_spacing, -1),
   send(Tabular, cell_spacing, 1),
   send(Tabular, rules, all),
   rgb_to_colour([7, 7, 7], Colour),
   send(Tabular, colour, Colour),
%  rgb_to_colour([10, 14, 10], Colour_A),
%  rgb_to_colour([15.85, 15.85, 0], Colour_A),
%  rgb_to_colour([15.85, 14.85, 4], Colour_A),
%  rgb_to_colour([13, 15.99, 13], Colour_A),
   rgb_to_colour([11, 13, 11], Colour_A),
   send_attributes(Tabular, Colour_A, Attributes),
%  send_attributes(Tabular, Attributes),
   send_rows_for_tests(Tabular, Rows),
   send(Table_Window, open).


/* send_rows_for_tests(Tabular, Rows) <-
      */

send_rows_for_tests(Tabular, Rows) :-
%  rgb_to_colour([15, 15.99, 15], Colour_1),
   rgb_to_colour([12, 14, 12], Colour_2),
%  rgb_to_colour([14, 15.99, 14], Colour_2),
   rgb_to_colour([13, 15, 13], Colour_1),
%  rgb_to_colour([15.2, 15.2, 15.99], Colour_1),
%  rgb_to_colour([14.5, 14.5, 15.99], Colour_2),
   send_rows_for_tests(Tabular, [Colour_1, Colour_2], Rows).

send_rows_for_tests(Tabular, [C1, C2], [Row_1, Row_2|Rows]) :-
   send_row_for_tests(Tabular, C1, Row_1),
   send(Tabular, next_row),
   send_rows_for_tests(Tabular, [C2, C1], [Row_2|Rows]).
send_rows_for_tests(Tabular, [Colour, _], [Row]) :-
   send_row_for_tests(Tabular, Colour, Row).
send_rows_for_tests(_, _, []).

 
/* send_row_for_tests(Tabular, Colour, Row) <-
      */

send_row_for_tests(Tabular, Row) :-
   rgb_to_colour([14.5, 14.5, 15.99], Colour),
   send_row_for_tests(Tabular, Colour, Row).

send_row_for_tests(Tabular, Colour, Row) :-
   send_attribute_values_for_tests(Tabular, Colour, Row).

send_attribute_values_for_tests(Tabular, _, [Value]) :-
   Value > 0,
   !,
   rgb_to_colour([15, 8, 8], Red),
   send(Tabular, append(Value, font(helvetica, roman, 12),
      center, center, 1, 1, Red, black)).
send_attribute_values_for_tests(Tabular, Colour, [Value|Values]) :-
   send(Tabular, append(Value, font(helvetica, roman, 12),
      center, center, 1, 1, Colour, black)),
   send_attribute_values_for_tests(Tabular, Colour, Values).
send_attribute_values_for_tests(_, _, []).


/******************************************************************/


