

/******************************************************************/
/***                                                            ***/
/***       Editor:  Basic Operations                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* editor_start(Editor) <-
      */

editor_start(Editor) :-
   new(Dialog, dialog('Editor')),
   new(Editor, editor),
   send(Editor, style, underline, style(underline := @on)),
   send(Editor, size, size(90, 20)),
   send(Dialog, append, Editor),
   send(Dialog, open).


/* editor_insert(Editor, X) <-
      */

editor_insert(Editor, X) :-
   ( atomic(X) ->
     A = X
   ; term_to_atom(X, A) ),
   send(Editor, insert, A).


/* editor_insert_with_new_line(Editor, Text) <-
      */

editor_insert_with_new_line(Editor, Text) :-
   editor_insert(Editor, Text),
   send(Editor, insert, '.\n').


/* editor_insert_with_underline(Editor, Text) <-
      */

editor_insert_with_underline(Editor, Text) :-
   get(Editor, caret, N),
   editor_insert(Editor, Text),
   get(Editor, caret, M),
   editor_underline(Editor, N, M).


/* editor_underline(Editor, From, To) <-
      */

editor_underline(Editor, From, To) :-
   Length is To - From,
   new(_, fragment(Editor, From, Length, underline)).


/* editor_eat_line(Editor, Text) <-
      */

editor_eat_line(Editor, Text) :-
   send(Editor, beginning_of_line, 1),
   get(Editor, caret, N),
   get(Editor, read_line, S),
   get(S, value, Text),
   send(Editor, caret, N).


/* editor_kill_line(Editor) <-
      */

editor_kill_line(Editor) :-
   send(Editor, beginning_of_line, 1),
   send(Editor, kill_line, 0).


/******************************************************************/


