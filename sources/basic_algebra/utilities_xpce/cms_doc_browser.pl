

/******************************************************************/
/***                                                            ***/
/***           CMS:  CMS Show Stock Charts                      ***/
/***                                                            ***/
/******************************************************************/



/*** files ********************************************************/


:- module(cms_doc_browser, []).

:- use_module(library(pce)).
:- use_module(library(toolbar)).
:- use_module(library(pce_report)).
:- use_module(library(url)).
:- use_module(cms(cms_doc_window)).


:- pce_image_directory(icons).


resource(backward, image, image('back.xpm')).
resource(forward,  image, image('forward.xpm')).
resource(reload,   image, image('reload.xpm')).
resource(source,   image, image('source.xpm')).


/*** interface  ***************************************************/


:- pce_begin_class(cms_doc_browser, frame, "CMS Document browser").

initialise(TB, URL:name, Prolog_Link_Handler:[name]) :->
   send_super(TB, initialise),
   send(TB, set(width := 640, height := 480)),
   send(TB, append, new(D, dialog)),
   send(D, name, top_dialog),
   send(D, pen, 0),
   send(D, gap, size(0, 5)),
   send(D, append, tool_bar(TB)),
   send(D, append, graphical(0,0,20,1), right),
   send(D, append,
      new(TI, text_item(url, '', message(TB, url, @arg1))), right),
   send(TI, hor_stretch, 100),
   send(TI, editable(@off)),
   send(TI, selection(URL)), 
   send(D, resize_message, message(D, layout, @arg2)),
   get(TI, height, H),
   send(TI, reference, point(0, H+5)),
   send(TI, value_set, new(chain)),
   send(TB, fill_toolbar),
      default(Prolog_Link_Handler, handle_prolog_link, Link_Handler),
   send(new(DW, cms_doc_window), below, D),
   send(DW, size(size(740, 400))),
   send(DW, url(URL)),
   send(DW, link_handler, Link_Handler),
   send(new(report_dialog), below, DW).


:- pce_group(parts).

document_window(TB, DW:cms_doc_window) :<-
   "Window representing current document"::
      get(TB, member, cms_doc_window, DW).

tool_bar(TB, Toolbar:tool_bar) :<-
   "Find the toolbar"::
      get(TB, member, top_dialog, D),
      get(D, member, tool_bar, Toolbar).

:- pce_group(menu).

fill_toolbar(F) :->
   get(F, tool_bar, TB),
   send_list(TB, append, [
      tool_button(backward, resource(backward), backward),
      tool_button(forward, resource(forward), forward), gap, 
      tool_button(reload, resource(reload), reload), gap,
      tool_button(view_source, resource(source), view_source) ]).

reload(TB) :->
   "Reload current page"::
      get(TB, document_window, DW),
      get(DW, url, URL),
      send(DW, url, @nil),
      send(DW, url, URL).


backward(TB) :->
   "Go back to previous location"::
      get(TB, document_window, DW),
      send(DW, backward).


forward(TB) :->
   "Go forwards in history"::
      get(TB, document_window, DW),
      send(DW, forward).


view_source(TB) :->
   "Go forwards in history"::
      get(TB, document_window, DW),
      get(DW, page_source, File),
      start_emacs,
      send(@emacs, open_file, File).


add_history(TB, URL:name) :->
   "Add URL to history"::
      get(TB, member, top_dialog, D),
      get(D, member, url, TI),
      send(TI, selection, URL),
      get(TI, value_set, Chain),
      send(Chain, delete_all, URL),
      send(Chain, prepend, URL),
      ( get(Chain, size, Size), Size > 20
        -> send(Chain, delete_tail)
      ; true ).

:- pce_group(navigate).


url(TB, URL:name*) :->
   "Jump to indicated URL"::
      get(TB, document_window, DW),
      cms_check_url(URL, URL2),
      send(DW, url, URL2).

cms_check_url(URL, URL).

:- pce_end_class.


/******************************************************************/


