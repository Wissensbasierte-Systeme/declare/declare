

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Status Bar                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* checklist_with_status_bar(Label, Predicate, Xs) <-
      */

checklist_with_status_bar(Predicate, Xs) :-
   functor(Predicate, Label, _),
   checklist_with_status_bar( checklist_with_status_bar,
      Label, Predicate, Xs ).

checklist_with_status_bar(Label, Predicate, Xs) :-
   checklist_with_status_bar( checklist_with_status_bar,
      Label, Predicate, Xs ).

checklist_with_status_bar(Bar, Label, Predicate, Xs) :-
   length(Xs, N),
   ( N =< 50 ->
     start_status_bar(Bar, Label, N),
     checklist_with_status_bar_loop(Bar, Predicate, Xs)
   ; ( N =< 500,
       M = 10
     ; M = 100 ),
     K is ceil(N/M),
     start_status_bar(Bar, Label, K),
     checklist_with_status_bar_loop(Bar, M, Predicate, 1, Xs) ),
   status_bar_disappear(Bar).
 
checklist_with_status_bar_loop(Bar, Predicate, [X|Xs]) :-
   !,
   Predicate =.. List_1,
   append(List_1, [X], List_2),
   Predicate_2 =.. List_2,
   call(Predicate_2),
   status_bar_increase(Bar),
   checklist_with_status_bar_loop(Bar, Predicate, Xs).
checklist_with_status_bar_loop(_, _, []).

checklist_with_status_bar_loop(Bar, M, Predicate, N, [X|Xs]) :-
   !,
   Predicate =.. List_1,
   append(List_1, [X], List_2),
   Predicate_2 =.. List_2,
   call(Predicate_2),
   status_bar_increase(Bar, M, N),
   K is N + 1,
   checklist_with_status_bar_loop(Bar, M, Predicate, K, Xs).
checklist_with_status_bar_loop(_, _, _, _, []).


/* checklist_with_status_bar_dsa(Predicate, Xs) <-
      */

checklist_with_status_bar_dsa(Predicate, Xs) :-
   length(Xs, N),
   start_status_bar_dsa(N),
   checklist_with_status_bar_loop_dsa(Predicate, Xs),
   status_bar_disappear_dsa.

checklist_with_status_bar_loop_dsa(Predicate, [X|Xs]) :-
   Predicate =.. List_1,
   append(List_1, [X], List_2),
   Predicate_2 =.. List_2,
%  writeln(call(Predicate_2)),
   call(Predicate_2),
   status_bar_increase_dsa,
   checklist_with_status_bar_loop_dsa(Predicate, Xs).
checklist_with_status_bar_loop_dsa(_, []).


/* maplist_with_status_bar(Predicate, Xs, Ys) <-
      */

maplist_with_status_bar(Predicate, Xs, Ys) :-
   functor(Predicate, Label, _),
   maplist_with_status_bar( maplist_with_status_bar,
      Label, Predicate, Xs, Ys ).

maplist_with_status_bar(Label, Predicate, Xs, Ys) :-
   maplist_with_status_bar(maplist_with_status_bar,
      Label, Predicate, Xs, Ys).

maplist_with_status_bar(Bar, Label, Predicate, Xs, Ys) :-
   length(Xs, N),
   ( N =< 50 ->
     start_status_bar(Bar, Label, N),
     maplist_with_status_bar_loop(Bar, Predicate, Xs, Ys)
   ; M = 10,
     K is ceil(N/M),
     start_status_bar(Bar, Label, K),
     maplist_with_status_bar_loop(Bar, M, Predicate, 1, Xs, Ys) ),
   status_bar_disappear(Bar).

maplist_with_status_bar_loop(Bar, Predicate, [X|Xs], [Y|Ys]) :-
   Predicate =.. List_1,
   append(List_1, [X, Y], List_2), 
   Predicate_2 =.. List_2,
   call(Predicate_2),
   status_bar_increase(Bar),
   maplist_with_status_bar_loop(Bar, Predicate, Xs, Ys).
maplist_with_status_bar_loop(_, _, [], []).

maplist_with_status_bar_loop(Bar, M, Predicate, N, [X|Xs], [Y|Ys]) :-
   Predicate =.. List_1,
   append(List_1, [X, Y], List_2),
   Predicate_2 =.. List_2,
   call(Predicate_2),
   status_bar_increase(Bar, M, N),
   K is N + 1,
   maplist_with_status_bar_loop(Bar, M, Predicate, K, Xs, Ys).
maplist_with_status_bar_loop(_, _, _, _, [], []).


/* maplist_with_status_bar_dsa(Predicate, Xs, Ys) <-
      */

maplist_with_status_bar_dsa(Predicate, Xs, Ys) :-
   length(Xs,N),
   start_status_bar_dsa(N),
   maplist_with_status_bar_loop_dsa(Predicate, Xs, Ys),
   status_bar_disappear_dsa.

maplist_with_status_bar_loop_dsa(Predicate, [X|Xs], [Y|Ys]) :-
   Predicate =.. List_1,
   append(List_1, [X,Y], List_2),
   Predicate_2 =.. List_2,
   call(Predicate_2),
   status_bar_increase_dsa,
   maplist_with_status_bar_loop_dsa(Predicate, Xs, Ys).
maplist_with_status_bar_loop_dsa(_, [], []).


/* start_status_bar(Name, Label, N) <-
      */

start_status_bar(Name, Label, N) :-
   concat_atom([Name, pic], Namepic),
   concat_atom([Name, dev], Namedev),
   free(@Name),
   free(@Namepic),
   free(@Namedev),
   rgb_to_colour([14,14,14], Colour),
   dislog_dialog_create([
      name:Name,
      title:'Working Status',
      label:[text:Label]:below,
%     button:[cancel, cancel_status_bar, Name]:right,
      picture:[
         name:Namepic, width:251, height:21,
         background:Colour ]:below ]),
   send(@Namepic, display, new(@Namedev, device)),
   send(@Namedev, display, new(_L1, line(250,0)),point(0,0)),
   send(@Namedev, display, new(_L2, line(0,20)),point(0,0)),
   send(@Namedev, display, new(L1, line(250,0)),point(0,19)),
   send(@Namedev, display, new(L2, line(0,20)),point(249,0)),
   send(L1, colour, colour(white)),
   send(L2, colour, colour(white)),
   send(@Name, open),
   send(@Name, synchronise),
   dislog_variable_set(status_bar-Name, N-0).


/* start_status_bar(Name, N) <-
      */

start_status_bar(Name, N) :-
   concat_atom([Name, pic], Namepic),
   concat_atom([Name, dev], Namedev),
   rgb_to_colour([14,14,14], Colour),
   dislog_dialog_create([
      name:Name,
      title:'Working Status',
%     label:[text:Label]:below,
%     button:[cancel, cancel_status_bar, Name]:right,
      picture:[
         name:Namepic, width:251, height:21,
         background:Colour ]:below ]),
   send(@Namepic, display, new(@Namedev, device)),
   send(@Namedev, display, new(_L1, line(250,0)),point(0,0)),
   send(@Namedev, display, new(_L2, line(0,20)),point(0,0)),
   send(@Namedev, display, new(L1, line(250,0)),point(0,19)),
   send(@Namedev, display, new(L2, line(0,20)),point(249,0)),
   send(L1, colour, colour(white)),
   send(L2, colour, colour(white)),
   send(@Name, open),
   send(@Name, synchronise),
   dislog_variable_set(status_bar-Name, N-0).


/* start_status_bar_dsa(Name, Label, N) <-
      */

start_status_bar_dsa(_, _, N) :-
   start_status_bar_dsa(N).

start_status_bar_dsa(N) :-
   Name = dsa_status_bar,
   concat_atom([Name, pic], Namepic),
   concat_atom([Name, dev], Namedev),
   send(@Namepic, display, new(@Namedev, device)),
   send(@Namepic, synchronise),
   dislog_variable_set(status_bar-Name, N-0).


/* status_bar_increase(Name, M, N) <-
      */

status_bar_increase(Name, M, N) :-
   ( 0 is N mod M ->
     status_bar_increase(Name)
   ; true ).


/* status_bar_increase(Name) <-
      */

status_bar_increase(Name) :-
   concat_atom([Name, dev], Namedev),
   dislog_variable_get(status_bar-Name, N-Status),
   !,
   Status < N,
   Status_1 is Status + 1,
   Gap is 50 / ( N + 1 ),
   Width_box is 200 / N,
   X_box = Gap + Status * ( Gap + Width_box ),
   send(@Namedev, display,
      new(B, box(Width_box, 10)), point(X_box,5)),
   rgb_to_colour([10,10,13], Colour),
   send(B, colour, Colour),
   send(B, fill_pattern, Colour),
   send(@Name, synchronise),
%  send(@Namedev, flush),
   dislog_variable_set(status_bar-Name, N-Status_1).


/* status_bar_increase_dsa(Name) <-
      */

status_bar_increase_dsa(_) :-
   status_bar_increase_dsa.

status_bar_increase_dsa :-
   Name = dsa_status_bar,
   concat_atom([Name, pic], Namepic),
   concat_atom([Name, dev], Namedev),
   dislog_variable_get(status_bar-Name, N-Status),
   !,
   Status < N,
   Status_1 is Status + 1,
   Gap is 25 / ( N + 1 ),
   Width_box is 125 / N,
   X_box = Gap + Status * ( Gap + Width_box ),
   send(@Namedev, display,
      new(B, box(Width_box, 10)), point(X_box,5)),
%  rgb_to_colour([10,10,13],Colour),
   rgb_to_colour([13,13,13],Colour),
   send(B, colour, Colour),
   send(B, fill_pattern, Colour),
   send(@Namepic, synchronise),
   dislog_variable_set(status_bar-Name, N-Status_1).


/* status_bar_disappear(Name) <-
      */

status_bar_disappear(Name) :-
   wait_seconds(0.5),
   concat_atom([Name, pic], Namepic),
   concat_atom([Name, dev], Namedev),
   concat_atom([Name, box], Namebox),
   send(@Name, destroy),
   checklist( free, [
      @Namedev, @Namepic, @Namebox, @Name ] ).


/* status_bar_disappear_dsa(Name) <-
      */

status_bar_disappear_dsa(_) :-
   status_bar_disappear_dsa.

status_bar_disappear_dsa :-
   Name = dsa_status_bar,
   concat_atom([Name, dev], Namedev),
   free(@Namedev).
 

/* cancel_status_bar(get_tv_bar) <-
      */

cancel_status_bar(get_tv_bar) :-
   status_bar_disappear(get_tv_bar),
   dislog_variables_set([
      get_tv_bar - off,
      old_number_name_list_cl - dummy ]).


/* cancel_status_bar(compare_bar) <-
      */

cancel_status_bar(compare_bar) :-
   status_bar_disappear(compare_bar),
   dislog_variable_set(
      compare_bar, off ).


/******************************************************************/


