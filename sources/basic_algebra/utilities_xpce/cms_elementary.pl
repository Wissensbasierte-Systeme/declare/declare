

/******************************************************************/
/***                                                            ***/
/***           CMS:  Elementary                                 ***/
/***                                                            ***/
/******************************************************************/



/*** files ********************************************************/


/* cms_data_directory(Directory) <-
      */

cms_base_directory(Directory) :-
   dislog_variable(home, DisLog),
   concat(DisLog, '/CMS', Directory),
   dislog_variable_set(
      cms_base_directory, Directory ).

cms_data_directory(Directory) :-
   cms_base_directory(Cms_Base_Directory),
   concat(Cms_Base_Directory, '/Messages', Directory),
   dislog_variable_set(
      cms_data_directory, Directory ).

cms_chart_directory(Directory) :-
   cms_base_directory(Cms_Base_Directory),
   concat(Cms_Base_Directory, '/Charts', Directory),
   dislog_variable_set(
      cms_chart_directory, Directory ).


/*** interface ****************************************************/


/* exists_directory_or_file(Dir_or_File) <-
      */

exists_directory_or_file(Dir_or_File) :-
   ( exists_directory(Dir_or_File)
   ; exists_file(Dir_or_File) ),
   !.
exists_directory_or_file(Dir_or_File) :-
   writeq(Dir_or_File),
   writeln(' does not exist. Program aborted.'),
   !,
   fail.


/* date_to_atom([D, M, Y], Atom) <-
      */

date_to_atom([D, M, Y], Atom) :-
   integer(D),
   !,
   ( ( M < 10, !,
       concat_atom([Y, '0', M], D1) )
   ; concat_atom([Y, M], D1) ),
   ( ( D < 10, !,
       concat_atom([D1, '0', D], Atom) )
   ; concat_atom([D1, D], Atom) ).
date_to_atom(Date, Atom) :-
   maplist( term_to_atom,
      Date_2, Date ),
   date_to_atom(Date_2, Atom).


/* get_file_name(Wildcard_Name, File_Name) <-
      */
   
get_file_name(Wildcard_Name, File_Name) :-
   expand_file_name(Wildcard_Name, [File_Name]),
   !.
% get_file_name(Wildcard_Name, '') :-
%    expand_file_name(Wildcard_Name, []),
%    !.
get_file_name(Wildcard_Name, File_Name) :-
   expand_file_name(Wildcard_Name, Files_Unsorted),
   sort(Files_Unsorted, Files_Sorted),
   reverse(Files_Sorted, [File_Name | _]).


/******************************************************************/


