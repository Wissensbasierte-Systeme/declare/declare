

/******************************************************************/
/***                                                            ***/
/***          DDK:  Database Tables in XPCE                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* database_table_xpce_create(Window, Tabular, Title) <-
      */

database_table_xpce_create(
      Window, Tabular, Title, Attributes, Rows) :-
   database_table_xpce_create(Window, Tabular, Title),
   send_attributes(Tabular, Attributes),
   database_table_xpce_insert_multiple(Tabular, Rows).

database_table_xpce_create(Window, Tabular, Title) :-
   new(Window, auto_sized_picture(Title)),
   send(Window, display, new(Tabular, tabular)),
   send(Tabular, border, 1),
   send(Tabular, cell_spacing, -1),
   send(Tabular, rules, all),
   send(Tabular, colour, white),
   xpce_table_add_scroll_bar(Window),
   send(Tabular, next_row),
   send(Window, open).


/* database_table_xpce_insert(Tabular, Row) <-
      */

database_table_xpce_insert_multiple(Tabular, Rows) :-
   checklist( database_table_xpce_insert(Tabular),
      Rows ).

database_table_xpce_insert(Tabular, Row) :-
   rgb_to_colour([15.2, 15.2, 15.99], Colour),
   send_row(Tabular, Colour, Row),
   send(Tabular, next_row),
   get(Tabular, device, Window),
   xpce_window_scroll_vertical(Window, 1).


/* database_table_xpce_update(Tabular, N, Conditions, Updates) <-
      */

database_table_xpce_update(Tabular, N, Conditions, Updates) :-
   xpce_table_update_row(Tabular, N, Conditions, Updates).


/* database_table_xpce_delete(T_1, N, Conditions, Window, T_2) <-
      */

database_table_xpce_delete(T_1, N, Conditions, Window, T_2) :-
   xpce_table_to_references(T_1, N, [As|Rs]),
   xpce_references_to_values(As, Attributes),
   sublist( xpce_table_row_falsifies_some_condition(Conditions),
      Rs, Rs_2 ),
   maplist( xpce_references_to_values,
      Rs_2, Rows ),
   database_table_xpce_create(
      Window, T_2, 'Table', Attributes, Rows).

xpce_table_row_falsifies_some_condition(Conditions, R) :-
   \+ checklist( xpce_table_row_fullfils_condition(R),
         Conditions ).


/*** tests ********************************************************/


test(database_tables_xpce, 1) :-
   As = [a, b],
   Rs = [[1, 2], [3, 4]],
   database_table_xpce_create(W, T, 'Table', As, Rs),
   database_table_xpce_insert(T, [5, 6]),
%  send(T, flush),
%  wait_seconds(2),
   database_table_xpce_update(T, 2, [1='3'], [2='-']),
   send(T, table_width, 100),
   send(W, width, 100).

test(database_tables_xpce, 2) :-
   Attributes = [
      'FNAME', 'MINIT', 'LNAME', 'SSN', 'BDATE',
      'ADDRESS', 'SEX', 'SALARY', 'SUPERSSN', 'DNO'],
   Rows = [
      ['John','B','Smith','123456789','1955-01-09',
       '731 Fondren, Houston, TX','M','30000.00','333445555',5],
      ['Franklin','T','Wong','333445555','1945-12-08',
       '638 Voss, Houston, TX','M','40000.00','888665555',5],
      ['Alicia','J','Zelaya','999887777','1958-07-19',
       '3321 Castle, Spring, TX','F','25000.00','987654321',4],
      ['Jennifer','S','Wallace','987654321','1931-06-20',
       '291 Berry, Bellaire, TX','F','43000.00','888665555',4],
      ['Ramesh','K','Narayan','666884444','1952-09-15',
       '975 Fire Oak, Humble, TX','M','38000.00','333445555',5],
      ['Joyce','A','English','453453453','1962-07-31',
       '5631 Rice, Houston, TX','F','25000.00','333445555',5],
      ['Ahmad','V','Jabbar','987987987','1959-03-29',
       '980 Dallas, Houston, TX','M','25000.00','987654321',4],
      ['James','E','Borg','888665555','1927-11-10',
       '450 Stone, Houston, TX','M','55000.00','NULL',1] ],
   database_table_xpce_create(W, T, 'Table', Attributes, Rows),
   send(T, table_width, 900),
   send(W, width, 1500),
   writeln(user, T).


/******************************************************************/


