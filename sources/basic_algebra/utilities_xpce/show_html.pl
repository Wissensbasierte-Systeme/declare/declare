

/******************************************************************/
/***                                                            ***/
/***          DisLog:  HTML Ouput                               ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library('doc/load')).
:- use_module(doc(emit)).
:- use_module(doc(url_fetch)).
:- use_module(doc(html)).
:- use_module(library(sgml)).
:- use_module(doc(browser)).


/*** interface ****************************************************/


/* doc_browser <-
      */

doc_browser :-
   send(new(_, doc_browser), open).


/* html_to_display(Document, Size) <-
      */

html_to_display(Html) :-
   html_to_display(Html, _).

html_to_display(Html, Size) :-
   html_to_display('', Html, Size).

html_to_display(Title, Html, Size) :-
   dislog_variable_get(output_path, Results),
   concat(Results, 'tmp.html', Html_File),
   dwrite(html, Html_File, Html),
   html_file_to_display(Title, Html_File, Size).


/* html_file_to_display(File) <-
      */

html_file_to_display(File) :-
   html_file_to_display(File, _).

html_file_to_display(File, Size) :-
   html_file_to_display('', File, Size).

html_file_to_display(Title, File, Size) :-
   ( var(Size),
     Size = size(700, 500)
   ; true ),
   ( % current_prolog_flag(windows, true),
     www_open_url(File)
   ; current_prolog_flag(unix, true),
     show_html_file_in_frame(Title, File, Size) ).


/* show_html_file(Title, File) <-
      */

show_html_file(File) :-
   show_html_file('', File).

show_html_file(Title, File) :-
   new(Picture, auto_sized_picture(Title)),
   send_html_file_to_picture(File, Picture),
   send(Picture, open).

send_html_file_to_picture(File, Picture) :-
   send(Picture, display,
      new(Pbox, pbox), point(10, 10)),
   send(Picture, resize_message,
      message(Pbox, width, @arg2?width - 20)),
   load_html_file(File, Tokens),
   send(Pbox, show, Tokens).


/* show_html_file_in_frame(File) <-
      */

show_html_file_in_frame(File) :-
   show_html_file_in_frame(File, size(600, 400)).

show_html_file_in_frame(File, Size) :-
   show_html_file_in_frame('', File, Size).

show_html_file_in_frame(Title, File, Size) :-
   load_html_file_robust(File, Tokens),
   show_tokens_in_frame(Title, Tokens, Size).

load_html_file_robust(File, Tokens) :-
   dtd(html, DTD),
   load_structure(File, Tokens,
      [max_errors(5000), dtd(DTD), dialect(sgml)]).

show_tokens_in_frame(Title, Tokens, Size) :-
   new(Frame, frame(Title)),
   send(Frame, size, Size),
   send(Frame, append, new(P, doc_window)),
   send(P, display, new(PB, pbox), point(10, 10)),
   send(P, size, Size),
   send(P, resize_message,
      message(PB, width, @arg2?width - 20)),
   send(PB, show, Tokens),
   send(Frame, open).


/* show_html_in_dsa_window(File) <-
      */

show_html_in_dsa_window(File) :-
   dislog_variable_get(dsa_chart_picture, Picture),
   free(Picture),
   dislog_variable_get(
      dsa_navigation_hierarchy, Navigation_Hierarchy ),
   dislog_stock_advisor(
      create_picture_sub, Navigation_Hierarchy/window ),
   show_html_in_doc_window(File, Picture).


/* show_html_in_doc_window(File, DW) <-
      */

show_html_in_doc_window(File, DW) :-
   send(DW, display, new(PB, pbox), point(10, 10)),
   send(DW, resize_message,
      message(PB, width, @arg2?width - 20)),
   load_html_file(File, Tokens),
   send(PB, show, Tokens).


/* trees_to_html_file(Title, Trees, File) <-
      */

trees_to_html_file(Title, Trees, File) :-
   predicate_to_file( File,
      ( write_list([
           '<html>\n',
           '<hr>\n',
           '<h2>', Title, '</h2>\n',
           '<hr><br><p>\n',
           '<blockquote>\n']),
        trees_to_html_file(Trees),
        write_list([
           '</blockquote>\n',
           '</html>\n']) ) ).

trees_to_html_file(Trees) :-
   writeln('<ul>'),
   checklist( tree_to_html_file(3),
      Trees ),
   writeln('</ul>').

tree_to_html_file(I, [Vs]) :-
   !,
   tab(I), write('<li> '),
   checklist( write_a_href,
      Vs ),
   nl.
tree_to_html_file(I, [Vs|Ts]) :-
   tab(I), write('<li> '),
   checklist( write_a_href,
      Vs ),
   nl,
   J is I + 3, tab(J), writeln('<ul>'),
   K is I + 6,
   checklist( tree_to_html_file(K),
      Ts ),
   tab(J), writeln('</ul>').

write_a_href(V) :-
   write_url(V),
   write(' &nbsp;&nbsp; ').


/* write_url(URL) <-
      */

write_url(URL) :-
   write_list(['<A HREF="', URL, '">', URL, '</A>']).


/*** tests ********************************************************/


/*
test(show_html, show_html_in_dsa_window) :-
   show_html_in_dsa_window(
      'projects/Selli/Reports/t.html' ).

test(show_html, show_html_file_in_frame) :-
   show_html_file_in_frame(
      'projects/Selli/Reports/t.html', size(800, 500) ).
*/


/******************************************************************/


