

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Generic Dialogs                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dislog_dialog_create(Items) <-
      */

dislog_dialog_create(Items) :-
   member(name:D, Items),
   ( member(title:Title, Items) ->
     new(@D, dialog(Title))
   ; new(@D, dialog) ),
   list_remove_elements([name:_, title:_], Items, Items_2),
   checklist( dislog_dialog_create(D),
      Items_2 ).

dislog_dialog_create(D, Item:List:Pos) :-
   dislog_dialog_create(Item, D, List, Pos).


/*** implementation ***********************************************/


/* dislog_dialog_create(Item, D, List, Pos) <-
      */

dislog_dialog_create(label, D, List, Pos) :-
   member(text:Text, List),
   send(@D, append, label(help, Text), Pos).

dislog_dialog_create(menu_bar, D, List, Pos) :-
   member(title:T, List),
   member(items:Items, List),
   send(@D, append, new(Men, menu_bar), Pos),
   send_pulldown_menu(Men, T, Items).

dislog_dialog_create(picture, D, List, Pos) :-
   member(name:P, List),
   send(@D, append, new(@P, picture), Pos),
   ( member(width:Width, List) ->
     send(@P, width,Width) ),
   ( member(height:Height, List) ->
     send(@P, height, Height) ),
   forall( member(background:Background, List),
      send(@P, background, Background) ).

dislog_dialog_create(text_item, D, List, Pos) :-
   member(name:T, List),
   send(@D, append, new(@T, text_item), Pos),
   ( member(label:Label, List) ->
     send(@T, label, Label) ),
   ( member(selection:Sel, List) ->
     send(@T, selection, Sel) ).

dislog_dialog_create(slider, D, List, Pos) :-
   member(title:Title, List),
   member(begin:Begin, List),
   member(end:End, List),
   member(default:Default, List),
   member(function:Function, List),
   send(@D, append,
      new(slider(Title, Begin, End, Default,
         message(@prolog, Function, @arg1))), Pos).

dislog_dialog_create(list_browser, D, List, Pos) :-
   member(name:B, List),
   send(@D, append, new(@B, list_browser), Pos),
   forall( member(label:Label, List),
      send(@B, label(Label)) ),
   forall( member(width:Width, List),
      send(@B, width, Width) ),
   forall( member(members:Members, List),
      send(@B, members, Members) ).

dislog_dialog_create(menu, D, List, Pos) :-
   member(title:Title, List),
   member(type:Type, List),
   member(function:Function, List),
   send(@D, append, new(Menu, menu(Title, Type,
      message(@prolog, Function, @arg1))), Pos),
   forall(
      ( member(items:Members, List),
        member(Member, Members) ),
      send(Menu, append, menu_item(Member)) ),
   forall( member(default:Default, List),
      send(Menu, default, Default) ),
   forall( member(layout:Layout, List),
      send(Menu, layout, Layout) ),
   forall( member(columns:Columns, List),
      send(Menu, columns, Columns) ).
   	
dislog_dialog_create(button,
      D, [Title, Function], Pos) :-
   send(@D, append,
      button(Title, message(@prolog, Function)), Pos).
dislog_dialog_create(button,
      D, [Title, Function, Param], Pos) :-
   send(@D, append, button(Title,
      message(@prolog, Function, Param)), Pos).


/******************************************************************/


