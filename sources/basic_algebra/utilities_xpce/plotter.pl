

/******************************************************************/
/***                                                            ***/
/***           Declare:  Graph Plotter                          ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library('plot/plotter')).
:- use_module(library(autowin)).


/*** tests ********************************************************/


test(graph_plot, entropy_epsilon) :-
   generate_interval(1, 100, Interval),
   ( foreach(I, Interval), foreach([I/100, J], Pairs) do
        entropy_epsilon(I/100, J) ),
   graph_plot(Pairs).

test(graph_plot, entropy) :-
   generate_interval(1, 99, Interval),
   ( foreach(I, Interval), foreach([I/100, J], Pairs) do
        entropy([I/100, 1-I/100], J) ),
   graph_plot(Pairs).


/*** interface ****************************************************/


/* graph_plot(Pairs) <-
      */

graph_plot(Pairs) :-
   Title = 'Graph Plotter',
   graph_plot(Title, Pairs).

graph_plot(Title, Pairs) :-
   ( foreach([X,Y], Pairs), foreach(X, Xs), foreach(Y, Ys) do
        true ),
   minimum(Xs, X1), maximum(Xs, X2),
   minimum(Ys, Y1), maximum(Ys, Y2),
   graph_plot(Title, Pairs, [X1, X2], [Y1, Y2]).


/* graph_plot(Pairs, [X1, X2], [Y1, Y2]) <-
      */

graph_plot(Pairs, [X1, X2], [Y1, Y2]) :-
   Title = 'Graph Plotter',
   graph_plot(Title, Pairs, [X1, X2], [Y1, Y2]).

graph_plot(Title, Pairs, [X1, X2], [Y1, Y2]) :-
   new(W, auto_sized_picture(Title)),
   send(W, display, new(Plotter, plotter)),
%  X_Step is (X2 - X1) / 10,
%  Y_Step is (Y2 - Y1) / 10,
   X_Step is 10,
   Y_Step is 1,
   send(Plotter, axis,
      new(X_axis, plot_axis(x, X1, X2, X_Step, 300))),
   send(Plotter, axis,
      new(Y_axis, plot_axis(y, Y1, Y2, Y_Step, 200))),
%  send(X_axis, format, '%.1f'),
%  send(Y_axis, format, '%.1f'),
   send(X_axis, format, '%1.f'),
   send(Y_axis, format, '%1.f'),
   send(Plotter, graph, new(Graph, plot_graph)),
   ( foreach([X, Y], Pairs) do
        send(Graph, append, X, Y) ),
   send(W, open).


/******************************************************************/


