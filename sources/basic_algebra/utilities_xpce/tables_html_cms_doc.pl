

/******************************************************************/
/***                                                            ***/
/***        DisLog:  HTML Tables in CMS Doc Window              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* html_display_table_cms_doc(Title, Header_Tuple, Tuples) <-
      */

html_display_table_cms_doc(Title, Header_Tuple, Tuples) :-
   tuples_to_html(Title, Header_Tuple, Tuples, Html),
   html_to_cms_doc_window(Title, Html).

html_to_cms_doc_window(Title, Html) :-
   html_to_cms_doc_window(Title, Html, size(800, 400)).


/* html_to_cms_doc_window(Title, Html, Size) <-
      */

html_to_cms_doc_window_2(Dialog, Html, Size) :-
   html_to_file_for_cms_doc_window(Html, File),
   file_to_cms_doc_window_2(Dialog, File, Size,
      cms_doc_extended_link_handler).

html_to_cms_doc_window(Title, Html, Size) :-
   html_to_file_for_cms_doc_window(Html, File),
   file_to_cms_doc_window(Title, File, Size,
      cms_doc_extended_link_handler).

cms_doc_extended_link_handler(Atom) :-
   term_to_atom(Goal, Atom),
   call(Goal).


/* tuples_to_html(Title, Header_Tuple, Tuples, Html) <-
      */

tuples_to_html(Title, Header_Tuple, Tuples, Html) :-
   html_create_table(Header_Tuple, Tuples, Table),
   name_append(['<title>', Title, '</title>'], T),
   Html = html:[]:[
      head:[]:[T], body:[]:[Table]].


/******************************************************************/


