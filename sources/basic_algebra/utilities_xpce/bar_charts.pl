

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Bar Charts                            ***/
/***                                                            ***/
/******************************************************************/


/* dislog_bar_chart(Title, Distribution) <-
      */

dislog_bar_chart(Title, Distribution) :-
   dislog_bar_chart(Title, Distribution, 50).

dislog_bar_chart(Title, Distribution, Limit) :-
   name_append(Title, ' Distribution', Title_Distribution),
   new(W, auto_sized_picture(Title_Distribution)),
%  send(W, size, size(700, 600)),
   length(Distribution, N),
   dislog_bar_chart_required_scale(
      Distribution, Limit, Scale),
   !,
   send(W, display,
      new(BC, bar_chart(vertical, 0, Scale, 300, N))),
   forall( member([A, Colour]-Amount, Distribution),
      send(BC, append,
         bar_group(A,
            bar('', Amount, Colour))) ),
   send(W, open).

dislog_bar_chart_required_scale(
      Distribution, Limit, Scale) :-
   findall( Amount,
      member(_-Amount, Distribution),
      Amounts ),
   maximum(Amounts, X),
   ( X < Limit,
     Scale is Limit
   ; round(X/100+0.5, 0, Y),
     Scale is Y * 100 ).


/*** tests ********************************************************/


test(bar_chart, 1) :-
   Distribution = [
      [a, red]-10, [b, yellow]-20, [c, green]-30 ],
   dislog_bar_chart('Test', Distribution).


/******************************************************************/


