

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Elementary XPCE Utilities                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* ddk_message(Messages) <-
      */

ddk_message(Messages) :-
   name_append(Messages,Message),
   send(@display,inform,Message).


/* ddk_tone(success) <-
      */

ddk_tone(success) :-
   current_prolog_flag(windows, true),
   !.
ddk_tone(success) :-
   current_prolog_flag(unix, true),
   home_directory(Home),
   concat(['cat ', Home, '/diverse/Sound/sounds/drip.au ',
      '> /dev/audio'], Command),
   us(Command),
   !.
ddk_tone(success).


/******************************************************************/


