

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Graphical Desktop                     ***/
/***                                                            ***/
/******************************************************************/


/* dislog_desktop <-
      */

dislog_desktop :-
   new(Dialog,dialog('DisLog Desktop')),
%  send(Dialog,append,button(quit,
%     message(Dialog,destroy))),
%  send(Dialog,append,button(make,
%     message(@prolog,make))),
   send(Dialog,append,
      new(Picture,picture),below),
   send(Picture,size,size(500,400)),
   send_dislog_tools(Picture),
%  send(Dialog,_compute_desired_size),
   send(Dialog,open).
   
send_dislog_tools(Picture) :-
   findall( Label-Call,
      dislog_tool(Label,Call),
      Tools ),
   length(Tools,N),
   generate_interval(1,N,I),
   pair_lists(I,Tools,Pairs),
   checklist( send_dislog_tool(Picture),
      Pairs ).

send_dislog_tool(Picture,[N,Label-Call]) :-
   Column_Length = 4,
   K is N mod Column_Length,
   ( K = 0, M1 = Column_Length
   ; M1 = K ),
   M2 is ( N - 1 ) // Column_Length,
   X is 20 + M2 * 150,
   Y is M1 * 50,
   U is X,
   V is Y + 20,
   B is 10,
   H is 10,
   send(Picture,display,
      new(Device,device)),
   send(Device,display,
      new(Box,box(B,H)),point(X,Y)),
%  send(Box,colour,Colour),
%  send(Box,fill_pattern,@grey25_image),
   send(Box,fill_pattern,colour(blue)),
   send(Device,display,
      new(Text,text(Label)),point(U,V)),
   send(Text,font,font(times,bold,12)),
   send(Device,recogniser,new(move_gesture)),
   send(Device,recogniser,
      click_gesture(left, '', double,
         message(@prolog,Call))).
  

/* dislog_tool(Name, Call) <-
      */

dislog_tool('Stock Advisor', dsa).
dislog_tool('Energy Tool', energy_tool).
dislog_tool('Mine Sweeper', create_board).
dislog_tool('Selli', selli).
dislog_tool('Tennis Tool', tennis_court).
dislog_tool('Qualimed', qualimed).
dislog_tool('Sorting Algorithms', sb).
dislog_tool('Visur / RAR', visur).
dislog_tool('Code Analysis', dislog_code_analysis).
dislog_tool('Test Tool', dislog_test_tool).


/* dislog_desktop_html <-
      */

dislog_desktop_html :-
   dislog_variable_get(home, DisLog),
   Desktop = 'desktop.html',
   concat(['file://', DisLog,
      '/sources/basic_algebra/utilities_xpce/', Desktop], Path),
   ddk_version(Version_DisLog),
%  concat(['Declare Desktop (', Version_DisLog, ')'], Title),
   concat(['Declare (', Version_DisLog, ')'], Title),
   current_prolog_flag(version, Version_Prolog),
   ( Version_Prolog < 60000 -> Size = size(550, 400)
   ; gethostname(daniel)    -> Size = size(600, 420)
   ;                           Size = size(600, 540) ),
   file_to_cms_doc_window_no_prune(
      Title, Path, Size, desktop_link_handler).

file_to_cms_doc_window_no_prune(Title, Path, Size, Link_Handler) :-
   new(Frame, frame(Title)),
   file_to_cms_doc_window_2(CDW, Path, Size, Link_Handler),
   send(Frame, append, CDW),
   send(Frame, open).


/* file_to_cms_doc_window(Title, Path, Size, Link_Handler) <-
      */

file_to_cms_doc_window(Title, Path, Size, Link_Handler) :-
   new(Frame, frame(Title)),
   html_file_prune_lists(Path, Path_2),
   file_to_cms_doc_window_2(CDW, Path_2, Size, Link_Handler),
   send(Frame, append, CDW),
   send(Frame, open).

file_to_cms_doc_window_2(CDW, Path, Size, Link_Handler) :-
   new(CDW, cms_doc_window(Path)),
   send(CDW, size, Size),
%  vector_multiply(1/16, [255, 255, 255], RGB),
%  vector_multiply(1/16, [243, 243, 222], RGB),
%  vector_multiply(1/16, [243, 222, 243], RGB),
%  vector_multiply(1/16, [222, 243, 243], RGB),
%  vector_multiply(1/16, [200, 233, 233], RGB),
%  vector_multiply(1/16, [200, 233, 100], RGB),
   vector_multiply(1/16, [245, 245, 220], RGB),
   rgb_to_colour(RGB, Colour),
   send(CDW, background, Colour),
   send(CDW, link_handler, Link_Handler).

desktop_link_handler(Link) :-
   term_to_atom(Goal, Link),
   call(Goal).


/******************************************************************/


