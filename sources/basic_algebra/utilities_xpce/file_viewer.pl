

/******************************************************************/
/***                                                            ***/
/***          DisLog:  File Viewer                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* file_viewer(Directory) <-
      */

file_viewer(Directory) :-
   new(Frame,frame('File Viewer')),
   send(Frame,append,new(Browser,browser)),
   send(new(Dialog,dialog),below,Browser),
   send(Dialog,append,button(view,
         message(@prolog,file_view,Browser?selection?key))),
   send(Dialog,append,button(quit,
         message(Frame,destroy))),
   send(Browser,members,directory(Directory)?files),
   send(Frame,open).


/* file_view(File) <-
      */

file_view(File) :-
   send(new(View,view(File)),open),
   send(View,load,File).


/* file_view(Directory,File) <-
      */

file_view(Directory,File) :-
   name_append(Directory,File,Path),
   file_view(Path).


/******************************************************************/


