

/******************************************************************/
/***                                                            ***/
/***           CMS:  CMS cms_doc_window                         ***/
/***                                                            ***/
/******************************************************************/


:- module(cms_doc_window, []).

:- use_module(library(pce)).
:- use_module(doc(window)).
:- use_module(doc(html)).
:- use_module(doc(url_fetch)).
:- use_module(doc(util)).


:- pce_begin_class(
      cms_doc_window, doc_window,
      "Represent a document").


variable(link_handler,  name*, both, "Prolog link handler").

/*
variable(url, name*, get, "Represented URL").
variable(location, name*, get, "Label last jumped too").
variable(backward_list, chain, get, "->back list of URL's").
variable(forward_list, chain, get, "->forward list of URL's").
variable(page_source,   file*, get, "(Cached) source-page").

class_variable(size, size, size(600, 300)).
*/


initialise(DW, URL:[name]*) :->
   send_super(DW, initialise(URL)),
   send(DW, slot, link_handler, handle_prolog_link).


goto_url(DW, URLSpec:name, Dir:[{forward,backward}]) :->
       "Switch to label or URL with label"::
   ( debug(browser, 'Request for URL \'~w\'~n', [URLSpec]),
     get(DW, link_handler, LH),
     prolog_link(URLSpec, LH)
   ; send(DW, push_location, Dir),
     labeled_url(URLSpec, URL, Label),
     ( URL \== ''
       -> get(DW, url, Base),
       global_url(URL, Base, GlobalURL),
       check_url(GlobalURL, Global_URL),
       send(DW, url, Global_URL)
     ; true ),
     ( Label \== ''
       -> send(DW, goto_label, Label)
     ; true ) ).

labeled_url(Spec, URL, Label) :-
   doc_window:labeled_url(Spec, URL, Label).

check_url(URL, URL_Checked) :-
   wildcard_match('file://*', URL),
   !,
   atom_length(URL, Length),
   Len is Length - 7,
   sub_atom(URL, 7, Len, _, Location),
   atom_concat('file:', Location, URL_Checked).
check_url(URL, URL).

prolog_link(URL, Link_Handler) :-
   sub_atom(URL, 0, 6, _, prolog),
   atom_length(URL, Length_1),
   Length is Length_1 - 7,
   sub_atom(URL, 7, Length, _, Parameters),
   Prolog_Link_Handler =.. [Link_Handler, Parameters],
   call(Prolog_Link_Handler).

handle_prolog_link(_).

/*
handle_prolog_link(Parameters) :-
   concat_atom(['Prolog Link-Handler for prolog:', Parameters, 
      ' should be implemented'], Text), 
   send(@prolog, writeln, Text).
*/

:- pce_end_class.


/******************************************************************/


