

/******************************************************************/
/***                                                            ***/
/***          DisLog:   XPCE Tables                             ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library(tabular)).
:- use_module(library(autowin)).


/*** examples *****************************************************/


test(xpce_display_table, 1) :-
   xpce_display_table(_, _, [a,b,c], [[1,2,3], [4,5,6]]).
test(xpce_display_table, 2) :-
   xpce_display_table_2('Test', [a,b,c],
      [[1,2,3], [4,5,6], [7,8,9],
       [1,2,3], [4,5,6], [7,8,9]]).


/*** interface ****************************************************/


/* xpce_display_file(Mode, File) <-
      */

xpce_display_file(Mode, File) :-
   member(Mode, [csv(';'), csv(':')]),
   dread(Mode, File, Tuples),
   xpce_display_table([], Tuples).


/* xpce_display_table(Attributes, Rows) <-
      */

xpce_display_table(Title, Attributes, Rows) :-
   xpce_display_table(_, _, Title, Attributes, Rows).

xpce_display_table(Attributes, Rows) :-
   xpce_display_table(_, _, Attributes, Rows).


/* xpce_display_table(Table_Window, Tabular, Attributes, Rows) <-
      */

xpce_display_table(Table_Window, Tabular, Attributes, Rows) :-
   xpce_display_table(
      Table_Window, Tabular, 'Table', Attributes, Rows).

xpce_display_table(
      Table_Window, Tabular, Title, Attributes, Rows) :-
   ( dislog_flag_get(xpce_mode, yes) ->
     new(Table_Window, auto_sized_picture(Title)),
     xpce_display_table_in_window(
        Table_Window, Tabular, Attributes, Rows),
     send(Table_Window, open)
   ; writeln('Attributes'=Attributes),
     writeln('Rows ='), writeln_list(Rows) ).

xpce_display_table_in_window(
      Table_Window, Tabular, Attributes, Rows) :-
   rgb_to_colour([7, 7, 7], Colour),
   send(Table_Window, display, new(Tabular, tabular)),
%  send(Tabular, border, 1),
   send(Tabular, border, 0),
%  send(Tabular, cell_padding, size(8, 4)),
%  send(Tabular, cell_spacing, -1),
   send(Tabular, cell_spacing, 1),
   send(Tabular, rules, all),
   send(Tabular, colour, Colour),
   send_attributes(Tabular, Attributes),
   send_rows(Tabular, Rows).


/* xpce_display_table_2(Title, Attributes, Rows) <-
      */

xpce_display_table_2(Title, Attributes, Rows) :-
   xpce_display_table_2(_, _, Title, Attributes, Rows).

xpce_display_table_2(
      Table_Window, Tabular, Title, Attributes, Rows) :-
   new(Table_Window, auto_sized_picture(Title)),
   xpce_display_table_in_window_2(
      Table_Window, Tabular, Attributes, Rows),
   send(Table_Window, open).

xpce_display_table_in_window_2(
      Table_Window, Tabular, Attributes, Rows) :-
   vector_multiply(1/16, [160, 185, 255], RGB0),
%  vector_multiply(1/16, [190, 220, 255], RGB1),
   vector_multiply(1/16, [210, 210, 210], RGB1),
%  vector_multiply(1/16, [220, 245, 255], RGB2),
%  vector_multiply(1/16, [180, 210, 255], RGB2),
   vector_multiply(1/16, [170, 200, 255], RGB2),
   rgb_to_colour(RGB0, C0),
   rgb_to_colour(RGB1, C1),
   rgb_to_colour(RGB2, C2),
   xpce_display_table_in_window_2(
      Table_Window, Tabular, [C0, C1, C2], Attributes, Rows).

xpce_display_table_in_window_2(
      Table_Window, Tabular, Colours, Attributes, Rows) :-
   rgb_to_colour([7, 7, 7], Colour),
   send(Table_Window, display, new(Tabular, tabular)),
%  send(Tabular, border, 1),
   send(Tabular, border, 0),
%  send(Tabular, cell_padding, size(8, 4)),
%  send(Tabular, cell_spacing, -1),
   send(Tabular, cell_spacing, 1),
   send(Tabular, rules, all),
   send(Tabular, colour, Colour),
   Colours = [C0, C1, C2],
   send_attributes(Tabular, C0, Attributes),
   send_rows(Tabular, [C1, C2], Rows).


/* send_attributes(Tabular, Colour, As) <-
      */

send_attributes(Tabular, As) :-
   rgb_to_colour([11.0, 11.0, 12.49], Colour),
%  rgb_to_colour([14.5, 14.5, 15.99], Colour),
%  rgb_to_colour([13, 13, 15.99], Colour),
   send_attributes(Tabular, Colour, As).

send_attributes(Tabular, Colour, [A|As]) :-
   !,
   send(Tabular, append(A, bold, center,
      center, 1, 1, Colour, black)),
   send_attributes(Tabular, Colour, As).
send_attributes(Tabular, _, []) :-
   send(Tabular, next_row).
   

/* send_rows(Tabular, Rows) <-
      */

send_rows(Tabular, Rows) :-
   rgb_to_colour([12.6, 12.6, 12.99], Colour_1),
   rgb_to_colour([12.2, 12.2, 12.99], Colour_2),
%  rgb_to_colour([15.6, 15.6, 15.99], Colour_1),
%  rgb_to_colour([15.2, 15.2, 15.99], Colour_2),
%  colour_hex_to_rgb('#DBE9F9', Rgb_1),
%  vector_multiply(1/16, Rgb_1, Rgb_2),
%  rgb_to_colour(Rgb_2, Colour_2),
%  rgb_to_colour([14.5, 14.5, 15.99], Colour_2),
   send_rows(Tabular, [Colour_1, Colour_2], Rows).

send_rows(Tabular, [C1, C2], [Row_1, Row_2|Rows]) :-
   !,
   send_row(Tabular, C1, Row_1),
   send(Tabular, next_row),
   send_rows(Tabular, [C2, C1], [Row_2|Rows]).
send_rows(Tabular, [Colour, _], [Row]) :-
   !,
   send_row(Tabular, Colour, Row).
send_rows(_, _, []).

 
/* send_next_row(Tabular, Colour, Row) <-
      */

send_next_row(Tabular, Row) :-
   rgb_to_colour([14.5, 14.5, 15.99], Colour),
   send_next_row(Tabular, Colour, Row).

send_next_row(Tabular, Colour, Row) :-
   send(Tabular, next_row),
   send_row(Tabular, Colour, Row).

   
/* send_row(Tabular, Colour, Row) <-
      */

send_row(Tabular, Row) :-
   rgb_to_colour([14.5, 14.5, 15.99], Colour),
   send_row(Tabular, Colour, Row).

send_row(Tabular, Colour, Row) :-
   checklist( send_attribute_value(Tabular, Colour),
      Row ).

send_attribute_value(Tabular, Colour, Value) :-
   ( atomic(Value),
     Atom = Value
   ; term_to_atom(Value, Atom) ),
   !,
   send(Tabular, append(Atom, font(helvetica, roman, 12),
      center, center, 1, 1, Colour, black)).


/* xpce_table_to_rows(Table, N, Rows) <-
      */

xpce_table_to_rows(Table, N, Rows) :-
   xpce_table_to_references(Table, N, [_|Rs]),
   maplist( xpce_references_to_values,
      Rs, Rows ).

xpce_references_to_values(Refs, Values) :-
   foreach(Ref, Refs), foreach(Value, Values) do
      get(Ref, value, Value).


/* xpce_table_to_references(Table, N, Rows) <-
      */

xpce_table_to_references(Table, N, Rows) :-
%  writeq(user, in(xpce_table_to_references(Table, N, Rows))),
   get(Table, graphicals, Chain),
   chain_to_list(Chain, List),
   split(N, List, Rows).
%  writeq(user, out(xpce_table_to_references(Table, N, Rows))).


/* xpce_table_update_row(Table, N, Conditions, Updates) <-
      */

xpce_table_update_row(Table, N, Conditions, Updates) :-
   xpce_table_to_references(Table, N, [_|Rows]),
   checklist( xpce_table_update_row(Conditions, Updates),
      Rows ),
   send(Table, flush).

xpce_table_update_row(Conditions, Updates, Row) :-
   checklist( xpce_table_row_fullfils_condition(Row),
      Conditions ),
   !,
   checklist( xpce_table_row_update(Row),
      Updates ).
xpce_table_update_row(_, _, _).

xpce_table_row_fullfils_condition(Row, N=V) :-
   nth(N, Row, E),
   get(E, value, V).

xpce_table_row_update(Row, N=V) :-
   nth(N, Row, E),
   send(E, value, V).


/* xpce_table_add_scroll_bar(Window) <-
      */

xpce_table_add_scroll_bar(Window) :-
   new(Window_Decorator, window_decorator(Window)),
   send(Window_Decorator, vertical_scrollbar(on)).


/* xpce_table_add_scroll_bar(Window, Tabular) <-
      */

xpce_table_add_scroll_bar(Window, Tabular) :-
   N = 30,
   multify([''], N, Rows),
   send_rows(Tabular, white, Rows),
   send(Tabular, next_row),
%  get(Window, decoration, D),
%  send(D, vertical_scrollbar(true)),
   xpce_window_scroll_vertical(Window, 20).


/* xpce_window_scroll_vertical(Window, N) <-
      */

xpce_window_scroll_vertical(Window, N) :-
   send(Window, scroll_vertical(
      direction:=forwards, unit:=line, amount:=N)).


/******************************************************************/


