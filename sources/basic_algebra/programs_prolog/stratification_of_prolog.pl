

/******************************************************************/
/***                                                            ***/
/***       DDK:  Stratification for Prolog                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* prolog_program_is_stratisfiable(Program) <-
      */

prolog_program_is_stratisfiable(Program) :-
   prolog_rules_to_partition(Program, _),
   !.


/* prolog_program_stratify(Program) <-
      */

prolog_program_stratify(Program) :-
   prolog_program_stratify(Program, [], Partition, Strata),
   writeln_list(user, Partition),
   star_line,
   checklist( dportray(prolog),
      Strata ).


/* prolog_program_stratify(Datalog, Prolog, Partition, Strata) <-
      */

prolog_program_stratify(Datalog, Prolog, Partition, Strata) :-
   maplist( prolog_program_select_definitions(
      Datalog, Prolog),
      Partition, Strata_2 ),
   sublist( list_non_empty,
      Strata_2, Strata ).

prolog_program_semi_stratify(Datalog, Prolog, Partition, Strata) :-
   maplist( prolog_program_select_definitions(
      Datalog, Prolog, Partition),
      Partition, Strata_2 ),
   sublist( list_non_empty,
      Strata_2, Strata ).


/*** implementation ***********************************************/


/* prolog_program_select_definitions(
         Program_1, Rules, Predicates, Program_2) <-
      */

prolog_program_select_definitions(
      Rs_1, Rs, Partition, Predicates, Rs_2) :-
   prolog_program_select_definitions(Rs_1, Predicates, Rs_a),
   findall( Ic,
      ( member(Ic, Rs_1),
        partition_insert_ic(Ic, Partition, Predicates) ),
      Rs_c ),
   append(Rs, Rs_a, Rs_3),
   append(Rs_a, Rs_c, Rs_d),
   prolog_program_to_dynamics_for_bodies(Rs_1, Rs_3, Rs_b),
   ( Rs_d \= [],
     append([Rs_b, Rs_d], Rs_2)
   ; Rs_2 = [] ),
   !.

prolog_program_select_definitions(Rs_1, Rs, Predicates, Rs_2) :-
   prolog_program_select_definitions(Rs_1, Predicates, Rs_a),
   append(Rs, Rs_a, Rs_3),
   prolog_program_to_dynamics_for_bodies(Rs_1, Rs_3, Rs_b),
   ( Rs_a \= [],
     append(Rs_b, Rs_a, Rs_2)
   ; Rs_2 = [] ),
   !.

prolog_program_to_dynamics_for_bodies(Rs_1, Rs_2, Dynamics) :-
   prolog_program_to_dynamic_predicates(Rs_1, Ps1),
   prolog_program_to_body_predicates(Rs_2, Ps2),
   ord_intersection(Ps1, Ps2, Ps),
   ( list_to_comma_structure(Ps, C),
     Dynamics = [:- dynamic C]
   ; Dynamics = [] ).


/* partition_insert_ic(Ic, Partition, Predicates) <-
      */

partition_insert_ic(Ic, Partition, Predicates) :-
   Ic = (:- F),
   prolog_rules_to_edges([(ic :- F)], Edges),
   append(_, [Ps|Partition_B], Partition),
   \+ ( member(Qs, Partition_B),
        member((ic/0)-''-P, Edges),
        member(P, Qs) ),
   !,
   Predicates = Ps.
   

/* prolog_rules_to_partition(Rules, Partition) <-
      */

prolog_rules_to_partition(Rules, Partition) :-
   prolog_rules_to_nodes(Rules, Nodes),
   prolog_rules_to_edges(Rules, Edges),
   prolog_edges_to_positive_and_negative(Edges, Es_1, Es_2),
   append(Es_1, Es_2, Es),
   vertices_edges_to_ugraph(Nodes, Es, Graph),
   reduce(Graph, Graph_2),
   top_sort(Graph_2, Partition_2),
   !,
   stratification_check(Es_2, Partition_2),
   reverse(Partition_2, Partition_3),
   partition_join(Es_2, Partition_3, Partition).

prolog_rules_to_partition_no_stratification(Rules, Partition) :-
   prolog_rules_to_nodes(Rules, Nodes),
   prolog_rules_to_edges(Rules, Edges),
   prolog_edges_to_positive_and_negative(Edges, Es_1, Es_2),
   append(Es_1, Es_2, Es),
   vertices_edges_to_ugraph(Nodes, Es, Graph),
   reduce(Graph, Graph_2),
   top_sort(Graph_2, Partition_2),
   !,
   reverse(Partition_2, Partition_3),
   partition_join(Es_2, Partition_3, Partition).


/* partition_join(Es, Partition_1, Partition_2) <-
      */

partition_join(Es, [Ps_1, Ps_2|Pss], Qss) :-
   partition_joinable(Es, Ps_1, Ps_2),
   append(Ps_1, Ps_2, Ps),
   partition_join(Es, [Ps|Pss], Qss).
partition_join(Es, [Ps_1, Ps_2|Pss], [Ps_1|Qss]) :-
   partition_join(Es, [Ps_2|Pss], Qss).
partition_join(_, Pss, Pss).

partition_joinable(Es, Ps_1, Ps_2) :-
   \+ ( member(P2-P1, Es),
        member(P1, Ps_1),
        member(P2, Ps_2) ).


/* prolog_edges_to_positive_and_negative(Edges, Es_1, Es_2) <-
      */

prolog_edges_to_positive_and_negative(Edges, Es_1, Es_2) :-
   findall( V-W,
      member(V-''-W, Edges),
      Es_1 ),
   findall( V-W,
      ( member(V-T-W, Edges),
        T \= '' ),
      Es_2 ).


/* prolog_rules_to_edges(Rules, Edges) <-
      */

prolog_rules_to_edges(Rules, Edges) :-
   prolog_rules_to_edges_1(Rules, Edges_1),
%  prolog_rules_to_edges_2(Rules, Edges_2),
%  Edges_2 = [],
   prolog_rules_to_edges_3(Rules, Edges_2),
   ord_union(Edges_1, Edges_2, Edges).


/* prolog_rules_to_nodes(Rules, Nodes) <-
      */

prolog_rules_to_nodes(Rules, Nodes) :-
   findall( Node,
      ( member(Rule, Rules),
        prolog_rule_to_node(Rule, Node) ),
      Nodes ).

prolog_rule_to_node(F1 :- F2, Node) :-
   ( prolog_formula_to_predicate(F1, Node)
   ; prolog_formula_to_predicate(F2, Node) ).
prolog_rule_to_node(F, Node) :-
   F \= (_ :- _),
   F \= (:- _),
   prolog_formula_to_predicate(F, Node).
   

/* prolog_rules_to_edges_1(Rules, Edges) <-
      */

prolog_rules_to_edges_1(Rules, Edges) :-
   findall( Edge,
      ( member(Rule, Rules),
        prolog_rule_to_edge(Rule, Edge) ),
      Edges ).

prolog_rule_to_edge(F1 :- F2, Edge) :-
   prolog_formula_to_predicate(F1, P1/N1, _),
   prolog_formula_to_predicate(F2, P2/N2, T2),
   Edge = P1/N1 - T2 -P2/N2.


/* prolog_rules_to_edges_2(Program, Edges) <-
      */

prolog_rules_to_edges_2(Program, Edges) :-
   prolog_program_to_rule_goal_graph(Program, [_, Es]),
   prolog_picture_edges_to_edges(Es, Edges).

prolog_picture_edges_to_edges(Edges_1, Edges_2) :-
   findall( P-Q,
      member(P-Q-_, Edges_1),
      Edges_A ),
   transitive_closure_edges_swi(Edges_A, Edges_B),
   findall( P1/N1- '' - P2/N2,
      ( member(P-Q, Edges_B),
        catch( term_to_atom(P1/N1, P), _, fail ),
        catch( term_to_atom(P2/N2, Q), _, fail ) ),
      Edges_2 ).
 

/* prolog_rules_to_edges_3(Rules, Edges) <-
      */

prolog_rules_to_edges_3(Rules, Edges) :-
   findall( Edge,
      ( member((F:-_), Rules), functor(F, ';', 2),
        term_structure_to_list(';', F, Atoms),
        two_members(A1, A2, Atoms),
        prolog_formula_to_predicate(A1,P1/N1),
        prolog_formula_to_predicate(A2,P2/N2),
        Edge = P1/N1 - '' -P2/N2 ),
      Edges ).


/*** tests ********************************************************/


test(prolog_rules_to_edges, 1) :-
   Prolog = [
      ((a;b) :- c),
      (c :- d, not(e), findall(_,f,_)) ],
   writeln_list(Prolog),
%  dwrite(pl, user, Prolog),
   prolog_rules_to_edges(Prolog, Edges),
   writelnq(user, Edges),
   prolog_edges_to_positive_and_negative(Edges, Es_1, Es_2),
   maplist( term_to_atom, Es_1, As_1 ),
   maplist( term_to_atom, Es_2, As_2 ),
   concat_with_separator(As_1, ', ', C_1),
   concat_with_separator(As_2, ', ', C_2),
   writeln(user, C_1),
   writeln(user, C_2).

test(prolog_edges_to_partition_no_stratification, 1) :-
   Es_1 = [c-d], Es_2 = [a-d,c-a,a-b,b-a],
   writeln(user, Es_1),
   writeln(user, Es_2),
   prolog_edges_to_partition_no_stratification(
      Es_1, Es_2, Partition),
   writelnq(user, Partition).


/******************************************************************/


