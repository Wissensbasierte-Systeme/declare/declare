

/******************************************************************/
/***                                                            ***/
/***       DDK:  Prolog Programs - Dependency Graphs            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* prolog_file_to_dependency_graph(Type, File) <-
      Type can be rule_goal or goal. */

prolog_file_to_dependency_graph(Type, File) :-
   alias_to_file(special_predicates, SP),
   rar:assert_special_predicates(SP),
   rar:reset_rar_database,
   rar:reset_gxl_database,
   program_file_to_xml(File, Xml),
   sca_variable_get(exported_predicates, Exp_Preds),
   xml_file_to_db('', Exp_Preds, Xml),
   alias_to_fn_triple(visur_config, Visur_Config),
%  visur:rule_goal_graph(Visur_Config, _, Type, file:File, Gxl),
%  trace,
   rule_goal_graph(Visur_Config, Type, file:File, Gxl),
   alias_to_fn_triple(visur_gxl_config, Gxl_Conf),
   gxl_to_picture(Gxl_Conf, Gxl, _Picture).


/******************************************************************/


