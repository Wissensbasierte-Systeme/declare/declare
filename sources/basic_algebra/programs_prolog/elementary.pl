


/******************************************************************/
/***                                                            ***/
/***       Declare:  Prolog Programs - Elementary               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* prolog_rule_to_head_predicate(Rule, P/N) <-
      */

prolog_rule_to_head_predicate(Rule, P/N) :-
   \+ functor(Rule, ':-', 1),
   ( Rule = (Atom :- _),
     !
   ; Rule = Atom ),
%  functor(Atom, P, N).
   prolog_formula_to_predicate(Atom, P/N).


/* prolog_program_to_body_predicates(Program, Predicates) <-
      */

prolog_program_to_body_predicates(Program, Predicates) :-
   findall( P/N,
      ( member(_ :- Body, Program),
        prolog_formula_to_predicate(Body, P/N, _) ),
      Predicates_2 ),
   sort(Predicates_2, Predicates).


/* prolog_formula_to_predicate(Formula, P/N, Type) <-
      */

prolog_formula_to_predicate(Formula, Predicate, Type) :-
   comma_structure_to_list(Formula, Atoms),
   member(call(X), Atoms),
   member(X=F, Atoms),
   prolog_formula_to_predicate(F, Predicate),
   Type = ''.

prolog_formula_to_predicate(Formula, P/N, Type) :-
   Formula =.. [Junctor, F1, F2],
   member(Junctor, [',', ';']),
   !,
   ( prolog_formula_to_predicate(F1, P/N, Type)
   ; prolog_formula_to_predicate(F2, P/N, Type) ).
prolog_formula_to_predicate(Formula, P/N, Type) :-
   Formula =.. [Junctor, F],
   member(Junctor, [not, \+]),
   !,
   prolog_formula_to_predicate(F, P/N, T),
   concat('not ', T, Type).
prolog_formula_to_predicate(Formula, P/N, Type) :-
   Formula =.. [Junctor|Xs],
   member(Junctor-M-K1, [
      checklist-1-1,
      maplist-1-2,
      ddbase_aggregate-2-0,
      findall-2-0 ]),
   !,
   nth(M, Xs, F),
   prolog_formula_to_predicate(F, P/K2, T),
   N is K1 + K2,
   concat([Junctor, ' ', T], Type).
prolog_formula_to_predicate(Formula, P/N, '') :-
   functor(Formula, P, N).


/* prolog_program_select_definitions(Rs_1, Predicates, Rs_2) <-
      */

prolog_program_select_definitions(Rs_1, Predicates, Rs_2) :-
   findall( Rule,
      ( member(Rule, Rs_1),
        once(
           ( prolog_rule_to_head_predicate(Rule, P/N),
             member(P/N, Predicates) ) ) ),
      Rs_2 ).


/* prolog_program_to_dynamic_predicates(Program, Predicates) <-
      */

prolog_program_to_dynamic_predicates(Program, Predicates) :-
   findall( Ps,
      ( member(Rule, Program),
        Rule = (:- dynamic C),
        comma_structure_to_list(C, Ps) ),
      Pss ),
   append(Pss, Predicates_2),
   sort(Predicates_2, Predicates).


/* prolog_program_is_indefinite(Prolog) <-
      */

prolog_program_is_indefinite(Prolog) :-
   member(Rule, Prolog),
   Rule = (:- _).


/******************************************************************/


