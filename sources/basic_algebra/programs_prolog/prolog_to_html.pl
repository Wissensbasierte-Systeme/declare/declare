

/******************************************************************/
/***                                                            ***/
/***       Declare:  Prolog Programs - Stratification           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* ddbase_prolog_program_display_stratification <-
      */

ddbase_prolog_program_display_stratification :-
   ddb_gui_input_program_2(Database),
   prolog_program_stratify(Database, [], _, Datalogs),
   dislog_variable_get(home,
      '/sources/basic_algebra/programs_prolog/HTML/', Path),
   concat(Path, 'rules_html.html', File),
   prolog_programs_to_html_file(Datalogs, File),
   rule_file_display_html(File),
   !.
   

/* rule_file_display_html(File) <-
      */

rule_file_display_html(File) :-
   concat(['file://', File], File_2),
   ddk_version(Version_DisLog),
   concat(['Declare (', Version_DisLog, ') - DDBase'], Title),
   current_prolog_flag(version, Version_Prolog),
   ( Version_Prolog < 60000 -> Size = size(550, 400)
   ; gethostname(daniel)    -> Size = size(900, 420)
   ;                           Size = size(600, 540) ),
   file_to_cms_doc_window_no_prune(
      Title, File_2, Size, desktop_link_handler).


/* prolog_programs_to_html_file(Programs, File) <-
      */

prolog_programs_to_html_file(Programs, File) :-
   dislog_variable_get(home,
      '/sources/basic_algebra/programs_prolog/HTML/', Path),
   concat(Path, 'rules_html_2.html', File_2),
   predicate_to_file( File_2,
      prolog_programs_to_html(Programs) ),
   concat(['cat ',
      Path, 'rules_html_1.html ',
      Path, 'rules_html_2.html ',
      Path, 'rules_html_3.html ', '> ', File],
      Command),
   us(Command).


/*** implementation ***********************************************/


/* prolog_programs_to_html(Programs) <-
      */

prolog_programs_to_html(Programs) :-
   foreach(Program, Programs) do
      ( prolog_rules_to_html(Program),
        writeln('<p/><hr/><p/>') ).


/* prolog_rules_to_html(Rules) <-
      */

prolog_rules_to_html(Rules) :-
   foreach(Rule, Rules) do
      ( numbervars(Rule, 0, _),
        prolog_rule_to_html(Rule),
        writeln('.<br/>') ).


/* prolog_rule_to_html(Rule) <-
      */

prolog_rule_to_html(Rule) :-
   Rule = (Head :- Body),
   prolog_formula_to_html(Head),
   write(' &larr; '),
   prolog_formula_to_html(Body).
prolog_rule_to_html(Formula) :-
   prolog_formula_to_html(Formula).


/* prolog_formula_to_html(Formula) <-
      */

prolog_formula_to_html(Formula) :-
   Formula = (A;B),
   prolog_formula_to_html(A),
   write(' &or; '),
   prolog_formula_to_html(B).
prolog_formula_to_html(Formula) :-
   Formula = (A,B),
   prolog_formula_to_html(A),
   write(' &and; '),
   prolog_formula_to_html(B).
prolog_formula_to_html(Formula) :-
   Formula =.. [ddbase_aggregate|Xs],
   write(' <b>ddbase_aggregate</b> ('),
   writeq_list_with_comma(Xs), write(')').
prolog_formula_to_html(Formula) :-
   Formula = not(A),
   write(' <b>not</b> '),
   prolog_formula_to_html(A).
prolog_formula_to_html(Atom) :-
   write('<font color="#00aaaa">'),
   writeq(Atom),
   write('</font>').


/******************************************************************/


