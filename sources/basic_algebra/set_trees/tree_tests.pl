

/******************************************************************/
/***                                                            ***/
/***             DisLog :  Tests for Tree-Ratios                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tree_test_i(Mode,N) :-
      generates a d_fact_tree for a set of d_facts 
      determined by Mode and N,
      the sets are all subsets of the interval <1,N> containing
      N - Mode elements.  */

gen_test_tree_1(Mode,N) :-
   gen_test_sets(Mode,N),
   init_d_fact_trees.
gen_test_tree_2(Mode,N) :-
   gen_test_sets(Mode,N),
   old_init_d_fact_trees.


/*** implementation ***********************************************/


gen_test_sets(Mode,N) :-
   abolish(d_fact,1),
   nl, write('test sets                '), 
   binomial(N,Mode,M), writeln(M),
   generate_interval(1,N,Interval),
   list_to_ord_set(Interval,Interval_1),
   assert_test_sets(Mode,Interval_1).

assert_test_sets(I,Interval) :-
   delete_in_interval(I,Interval,Interval_1),
   assert(d_fact(Interval_1)),
   fail.
assert_test_sets(_,_).

delete_in_interval(0,Interval_1,Interval_1) :-
   !.
delete_in_interval(I,Interval_1,Interval_3) :-
   append(X,[_|Interval_2],Interval_1),
   J is I - 1,
   delete_in_interval(J,Interval_2,Y),
   append(X,Y,Interval_3).
 

/******************************************************************/

