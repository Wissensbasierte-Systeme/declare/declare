

/******************************************************************/
/***                                                            ***/
/***         DisLog :  Set Trees                                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* set_tree_resolve(Resolvents, Tree_1, Tree_2) <-
      */

set_tree_resolve(Resolvents, Tree_1, Tree_2) :-
   set_tree_resolve(Resolvents, [[]], Tree_1, Tree_3),
   set_tree_prune(Tree_3, Tree_2).

set_tree_resolve(_, _, [], []) :-
   !.
set_tree_resolve(Resolvents, Tree, [As|Trees_1], Tree_2) :-
   findall( Xs-T,
      ( member(Xs-T, Resolvents),
        ord_subset(Xs, As) ),
      Pairs ),
   pair_lists_2(List, Ts, Pairs),
   ord_union(List, Cs),
   ord_subtract(As, Cs, Bs),
   set_tree_product([Tree|Ts], T_2),
   ( Trees_1 = [],
     Tree_2 = [Bs, T_2]
   ; maplist( set_tree_resolve(Resolvents, T_2),
        Trees_1, Trees_2 ),
     Tree_2 = [Bs|Trees_2] ).

 
/* set_tree_product(Tree, Tree_1, Tree_2) <-
      */

set_tree_product(Trees, Tree) :-
   iterate_list(set_tree_product, [[]], Trees, Tree).

set_tree_product(Tree, [As], [As, Tree]).
set_tree_product(Tree, [As|Trees_1], [As|Trees_2]) :-
   maplist( set_tree_product(Tree),
      Trees_1, Trees_3 ),
   set_trees_prune([], Trees_3, Trees_2).
set_tree_product(_, [], []).


/* set_tree_prune(Tree_1, Tree_2) <-
      */

set_tree_prune(Tree_1, Tree_2) :-
   set_tree_prune([], Tree_1, Tree_2).

set_tree_prune(Xs, [As, [[]|Trees_1]], [Bs|Trees_2]) :-
   !,
   ord_subtract(As, Xs, Bs),
   ord_union(Xs, As, Ys),
   set_trees_prune(Ys, Trees_1, Trees_2).
set_tree_prune(Xs, [As|Trees_1], [Bs|Trees_2]) :-
   ord_subtract(As, Xs, Bs),
   ord_union(Xs, As, Ys),
   set_trees_prune(Ys, Trees_1, Trees),
   ( member([[]], Trees),
     Trees_2 = []
   ; Bs = [],
     set_trees_further_prune_to_trees(Trees, Trees_2)
   ; Trees_2 = Trees ).
set_tree_prune(_, [], []).

set_trees_prune(Xs, Trees_1, Trees_2) :-
   maplist( set_tree_prune(Xs),
      Trees_1, Trees_2 ).

set_trees_further_prune_to_trees(Trees_1, Trees_2) :-
   maplist( set_tree_further_prune_to_trees,
      Trees_1, List ),
   append(List, Trees_2).

set_tree_further_prune_to_trees([[]|Trees], Trees).
set_tree_further_prune_to_trees(Tree, [Tree]).


/* set_tree_minimize(Tree_1, Tree_2) <-
      */

set_tree_minimize(Tree_1, Tree_2) :-
   tree_to_state(Tree_1, State_1),
   state_can(State_1, State_2),
   state_to_tree(State_2, Tree_2).


/* set_tree_resolvents_sort(Resolvents_1, Resolvents_2) <-
      */

set_tree_resolvents_sort(Resolvents_1, Resolvents_2) :-
   pair_lists_2(Atoms_1, Trees_1, Resolvents_1),
   maplist( tree_to_atoms,
      Trees_1, List_1 ),
   maplist( length,
      List_1, Numbers_1 ),
   triple_lists_2(Numbers_1, Atoms_1, Trees_1, Triples_1),
   sort(Triples_1, Triples_2),
   triple_lists_2(_, Atoms_2, Trees_2, Triples_2),
   pair_lists_2(Atoms_2, Trees_2, Resolvents_2).


/* set_trees_sort(Trees_1, Trees_2) <-
      */

set_trees_sort(Trees_1, Trees_2) :-
   maplist( tree_to_atoms,
      Trees_1, List_1 ),
   maplist( length,
      List_1, Numbers_1 ),
   pair_lists_2(Numbers_1, Trees_1, Pairs_1),
   sort(Pairs_1, Pairs_2),
   pair_lists_2(_, Trees_2, Pairs_2).


/* set_and_set_tree_symmetric_difference_to_set_pairs(Set, Tree, Sets) <-
      */

set_and_set_tree_symmetric_difference_to_set_pairs(Set, Tree, Sets) :-
   set_and_set_tree_symmetric_difference(Set, Tree, Tree_2),
   tree_to_state(Tree_2, Sets).


/* set_and_set_tree_symmetric_difference(S, Tree_1, Tree_2) <-
      */

set_and_set_tree_symmetric_difference(S, [N1|Ts1], [N2|Ts2]) :-
   ord_subtract(N1, S, N),
   ord_subtract(S, N1, S2),
   maplist( set_and_set_tree_symmetric_difference(S2),
      Ts1, Ts2 ),
   ( ( Ts1 = [],
       !,
       N2 = [diff(S2)|N] )
   ; N2 = N ).
set_and_set_tree_symmetric_difference(_, [], []).


/* sets_to_set_tree(Sets, Tree) <-
      */

sets_to_set_tree(Sets, Tree) :-
   maplist( set_to_set_with_id,
      Sets, Sets_2 ),
   state_to_tree(Sets_2, Tree_2),
   tree_remove_set_ids(Tree_2, Tree).

set_to_set_with_id(Set_1, Set_2) :-
   ord_union([set-Set_1], Set_1, Set_2).
   
tree_remove_set_ids([N1|Ts], [N2|Ts]) :-
   member(set-S, N1),
   !,
   ord_del_element(N1, set-S, N2).
tree_remove_set_ids([N|Ts1], [N|Ts2]) :-
   maplist( tree_remove_set_ids,
      Ts1, Ts2 ).
tree_remove_set_ids([], []).


/*** tests ********************************************************/


test(set_trees:set_and_set_tree_symmetric_difference,1) :-
   Sets = [
      [a,b,c],[a,b,d],[b,d] ],
   Set = [a,c,e],
   sets_to_set_tree(Sets,Tree),
   set_and_set_tree_symmetric_difference(Set,Tree,Tree_2),
   dportray(tree,Tree_2),
   tree_to_state(Tree_2,Sets_2),
   dportray(chs,Sets_2).
 
test(set_trees:sets_to_set_tree,1) :-
   Sets = [
      [a,b,c],[a,b,d],[b,d] ],
   sets_to_set_tree(Sets,Tree),
   dportray(chs,Sets),
   dportray(tree,Tree).

test(set_trees, set_tree_product) :-
   Tree_1 = [[a],[[b]],[[e]]],
   Tree_2 = [[], [[d]], [[e]]],
   dportray(tree, [[], Tree_1, Tree_2]),
   set_tree_product([Tree_1, Tree_2], Tree),
   dportray(tree, Tree).

test(set_trees, set_tree_resolve) :-
   T1 = [[a],[[b]],[[e]]],
   T2 = [[], [[d]], [[e]]],
   Tree_1 = [[1], [[2]], [[3]]], 
   set_tree_resolve([[1]-T1, [2]-T2], Tree_1, Tree_2),
   dportray(tree, Tree_2).


/******************************************************************/


