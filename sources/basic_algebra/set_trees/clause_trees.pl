

/******************************************************************/
/***                                                            ***/
/***         DisLog :  Clause Trees                             ***/
/***                                                            ***/
/******************************************************************/


:- dynamic 
      d_fact_tree/1, tree_edge/2, 
      good_edge/2, greedy_root/1,
      join_tree/1, join_tree_fact/1,
      meta_subsumes/2.


/*** interface ****************************************************/


/* tree_simplify(Tree1,Tree2) <-
      the clause tree Tree1 is simplified by removing all empty
      nodes except the root. */

tree_simplify([Node|Trees1],[Node|Trees2]) :-
   remove_empty_nodes(Trees1,Trees2).
tree_simplify([],[]).

remove_empty_nodes([[[]|Trees1]|Trees2],Trees4) :-
   !,
   append(Trees1,Trees2,Trees3),
   remove_empty_nodes(Trees3,Trees4).
remove_empty_nodes([[Node|Trees1]|Trees2],[[Node|Trees3]|Trees4]) :-
   remove_empty_nodes(Trees1,Trees3),
   remove_empty_nodes(Trees2,Trees4).
remove_empty_nodes([],[]).


/* tree_simplify_2(Tree1,Tree2) <-
      the clause tree Tree1 is simplified by removing all empty
      nodes except the root. */

tree_simplify_2([Node|Trees1],[Node|Trees2]) :-
   remove_empty_nodes_2(Trees1,Trees2).
tree_simplify_2([],[]).

remove_empty_nodes_2(Trees1,[]) :-
   member([[]],Trees1),
   !.
remove_empty_nodes_2([[[]|Trees1]|Trees2],Trees4) :-
   !,
   append(Trees1,Trees2,Trees3),
   remove_empty_nodes_2(Trees3,Trees4).
remove_empty_nodes_2([[Node|Trees1]|Trees2],[[Node|Trees3]|Trees4]) :-
   remove_empty_nodes_2(Trees1,Trees3),
   remove_empty_nodes_2(Trees2,Trees4).
remove_empty_nodes_2([],[]).


/* tree_simplify_3(Tree1,Tree2) <-
      the clause tree Tree1 is simplified by gluing nodes with only
      one child with this child. */

tree_simplify_3([Node],[Node]) :-
   !.
tree_simplify_3([Node,Tree],New_Tree) :-
   !,
   tree_simplify_3(Tree,Tree2),
   attach_tree(Node,Tree2,New_Tree).
tree_simplify_3([Node|Trees],New_Tree) :-
   trees_simplify_3(Trees,Trees2),
   attach_trees(Node,Trees2,New_Tree).
tree_simplify_3([],[]).

trees_simplify_3([Tree1|Trees1],[Tree2|Trees2]) :-
   tree_simplify_3(Tree1,Tree2),
   trees_simplify_3(Trees1,Trees2).
trees_simplify_3([],[]).


/* tree_can(Tree1,Tree2) <-
      for a given clause tree Tree1, the clause tree Tree2 represents
      the canonization of the state represented by Tree1. */

tree_can(Tree1,Tree2) :-
   tree_to_state(Tree1,State1),
   state_can(State1,State2),
   ord_subtract(State1,State2,State3), 
   tree_state_subtract(Tree1,State3,Tree2).

tree_can_2(Tree1,Tree2) :-
   tree_to_state(Tree1,State1),
   state_can(State1,State2),
   state_to_tree(State2,Tree2).


/* tree_tree_subtract(Tree1,Tree2,Tree3) <-
      remove all clauses in Tree1 which are subsumed by Tree2,
      the new tree is Tree3. */

tree_tree_subtract(Tree1,Tree2,Tree3) :-
   tree_to_state(Tree2,State2),
   tree_state_subtract(Tree1,State2,Tree3).


/* tree_state_subtract(Tree1,State,Tree2) <-
      remove all clauses in Tree1 which are subsumed by State,
      the new tree is Tree2. */

tree_state_subtract(Tree1,State,Tree2) :-
   write('ts_subtract  '), ttyflush,
   start_timer(tree_state_subtract),
   purify_tree(Tree1,Tree2,State,_),
   stop_timer(tree_state_subtract).

tree_state_subtract_2(Tree1,State,Tree2) :-
   tree_to_state(Tree1,State1),
   state_subtract(State1,State,State2),
   state_to_tree(State2,Tree2).


/* state_max_tree_subtract(State1,Tree,State2) <-
      State2 is the set of all clauses from State1, which 
      are not supersumed by the tree Tree. */
 
state_max_tree_subtract([C|Cs],Tree,State) :-
   tree_supersumes_clause(Tree,C),
   !,
   state_max_tree_subtract(Cs,Tree,State).
state_max_tree_subtract([C|Cs],Tree,[C|State]) :-
   state_max_tree_subtract(Cs,Tree,State).
state_max_tree_subtract([],_,[]).
 

/* tree_tree_difference(Tree1,Tree2,Tree3) <-
      given two clause trees Tree1 and Tree2,
      the clause tree Tree3 representing the clauses of Tree1 which 
      are not represented by Tree2 is computed. */
 
tree_tree_difference(Tree1,Tree2,Tree3) :-
   tree_to_state(Tree2,State),
   delete_clauses_in_tree(State,Tree1,Tree3).


/* tree_subsumes_state(Tree,State) <-
      tests if Tree subsumes State. */

tree_subsumes_state(Tree,[C|Cs]) :-
   tree_subsumes_clause(Tree,C),
   tree_subsumes_state(Tree,Cs).
tree_subsumes_state(_,[]).


/* tree_subsumes_tree(Tree1,Tree2) <-
      tests if Tree1 subsumes Tree2. */

tree_subsumes_tree(Tree1,Tree2) :-
   tree_to_state(Tree2,State2),
   tree_subsumes_state(Tree1,State2).


/* state_to_tree(State,Tree) <-
      transforms an Herbrand state State to a clause tree Tree. */

state_to_tree(State,_) :-
   var(State),
   !.
state_to_tree(State,Tree) :-
   tree_clauses_insert([],Tree,State).


/* tree_to_state(Tree,State) <-
      transforms a clause tree Tree to a state State without 
      duplicates (canonization is not applied to State). */

tree_to_state([],[]) :-
   !.
tree_to_state(Tree,State) :-
   findall( Fact,
      tree_to_d_fact(Tree,Fact),
      State2 ),
   list_to_ord_set(State2,State).


/* state_to_max_tree(State,Tree) <-
      Constructs a clause tree Tree for the maximal clauses 
      in State. */
 
state_to_max_tree(State,Tree) :-
   max_tree_clauses_insert([],Tree,State).
 
 
/* tree_union(Tree1,Tree2,Tree3) <-
      given two clause trees Tree1 and Tree2,
      a clause tree Tree3 is computed which represents the union of the 
      states represented by Tree1 and Tree2. */
 
tree_union([],Tree,Tree) :-
   !.
tree_union(Tree,[],Tree) :-
   !.
tree_union([N|Ts1],[N|Ts2],[N|Ts]) :-
   !,
   append(Ts1,Ts2,Ts).
tree_union([N1|Ts1],[N2|Ts2],[N1|Ts3]) :-
   ord_subset(N1,N2),
   !,
   ord_subtract(N2,N1,N3),
   append(Ts1,[[N3|Ts2]],Ts3).
tree_union([N1|Ts1],[N2|Ts2],[N2|Ts3]) :-
   ord_subset(N2,N1),
   !,
   ord_subtract(N1,N2,N3),
   append([[N3|Ts1]],Ts2,Ts3).
tree_union([N1|Ts1],[N2|Ts2],[N3,[V1|Ts1],[V2|Ts2]]) :-
   ord_intersection(N1,N2,N3),
   ord_subtract(N1,N3,V1),
   ord_subtract(N2,N3,V2).


/* tree_merge(Tree1,Tree2,Tree3) <-
      merges the clause trees Tree1 and Tree2 
      to a new clause tree Tree3.  */

tree_merge(Tree1,Tree2,Tree3) :-
   writeln('tree_merge'),
   !,
   tree_to_state(Tree2,State2),
   dspy(pp_dhs(State2)),
   tree_clauses_insert(Tree1,Tree3,State2).

tree_merge_2([],Tree2,Tree2) :-
   !.
tree_merge_2(Tree1,[],Tree1) :-
   !.
tree_merge_2(Tree1,Tree2,[[],Tree1,Tree2]).


/* tree_remove_tautologies(Tree1,Tree2) <-
      removes all branches in the clause tree Tree1, which contain 
      complementary literals, the new clause tree is Tree2. */

tree_remove_tautologies(Tree1,Tree2) :-
   tree_to_state(Tree1,State1),
   state_remove_tautologies(State1,State2),
   state_to_tree(State2,Tree2).


/* init_d_fact_tree <-
      initialization of the d_fact_tree for a single or fixpoint   
      iteration */

old_init_d_fact_trees :-
   writeln('tree init    '), ttyflush,
   abolish(d_fact_tree,1), abolish(delta_d_fact_tree,1),
   collect_arguments(d_fact,Facts), 
   minimal_steiner_tree(Facts,Tree),
   start_timer(init_d_fact_tree),
   assert(d_fact_tree(Tree)), assert(delta_d_fact_tree(Tree)),
   abolish(d_fact), abolish(delta_d_fact), 
   write('tree assert  '), ttyflush,
   stop_timer(init_d_fact_tree),
   pp_ratio_information.

init_d_fact_trees :-
   write('tree init    '), ttyflush,
   start_timer(init_d_fact_tree),
   collect_arguments(d_fact,Facts), 
%  length_order(Facts,Ordered_Facts),
%  reverse(Ordered_Facts,Rev_Ordered_Facts), 
   tree_clauses_insert([],Tree,Facts),
%  tree_clauses_insert([],Tree,Rev_Ordered_Facts),
   abolish(d_fact_tree,1), abolish(delta_d_fact_tree,1),
   abolish(old_d_fact_tree,1),
   assert(d_fact_tree(Tree)), assert(delta_d_fact_tree(Tree)),
   assert(old_d_fact_tree(Tree)),
   abolish(d_fact), abolish(delta_d_fact),
   stop_timer(init_d_fact_tree),
   pp_ratio_information.

new_init_d_fact_trees :-
   write('tree init    '), ttyflush,
   start_timer(init_d_fact_tree),
   collect_arguments(d_fact,Facts), tree_clauses_insert([],Tree,Facts),
   abolish(d_fact_tree,1), abolish(delta_d_fact_tree,1),
   abolish(old_d_fact_tree,1),
   assert(d_fact_tree(Tree)), assert(delta_d_fact_tree(Tree)),
   assert(old_d_fact_tree([])),
   abolish(d_fact), abolish(delta_d_fact),
   stop_timer(init_d_fact_tree),
   pp_ratio_information.

msp_to_d_fact_tree :-
   write('tree init    '), ttyflush,
   start_timer(init_d_fact_tree),
   minimal_model_state(Msp), tree_clauses_insert([],Tree,Msp),
   abolish(d_fact_tree,1),
   assert(d_fact_tree(Tree)), assert(delta_d_fact_tree(Tree)),
   stop_timer(init_d_fact_tree),
   pp_ratio_information.

 
/* update_d_fact_tree <-
      after each single iteration the actual d_fact_tree is
      augmented by the delta_d_fact_tree for the newly derived
      delta_d_facts */
 
old_update_d_fact_trees(Facts) :-
   write('tree update  '), ttyflush,
   retract(d_fact_tree([[]|Trees])), retract(delta_d_fact_tree(_)),
   minimal_steiner_tree(Facts,[[]|New_Delta_Trees]),
   start_timer(update_d_fact_tree),
   union(Trees,New_Delta_Trees,New_Trees),
   assert(d_fact_tree([[]|New_Trees])),
   assert(delta_d_fact_tree([[]|New_Delta_Trees])),
   write('tree union   '), ttyflush, 
   stop_timer(update_d_fact_tree),
   pp_ratio_information.

update_d_fact_trees(Facts) :-
   write('tree update  '), ttyflush,
   start_timer(update_d_fact_tree),
   retract(d_fact_tree(Tree)), 
   tree_clauses_insert(Tree,New_Tree,Facts),
   assert(d_fact_tree(New_Tree)),
%  retract(delta_d_fact_tree(_)),
%  tree_clauses_insert([],New_Delta_Tree,Facts),
%  assert(delta_d_fact_tree(New_Delta_Tree)),
   delta_d_fact_tree(New_Delta_Tree),
   stop_timer(update_d_fact_tree),
   pp_ratio_information,
   pp_ratio_information(New_Delta_Tree).

new_update_d_fact_trees(Facts) :-
   write('tree update  '), ttyflush,
   start_timer(update_d_fact_tree),
   retract(d_fact_tree(Tree)), retract(delta_d_fact_tree(_)),
   retract(old_d_fact_tree(_)),
   tree_clauses_insert(Tree,New_Tree,Facts),
   tree_clauses_insert([],New_Delta_Tree,Facts),
   assert(d_fact_tree(New_Tree)), assert(delta_d_fact_tree(New_Delta_Tree)),
   assert(old_d_fact_tree(Tree)),
   stop_timer(update_d_fact_tree),
   pp_ratio_information,
   pp_ratio_information(New_Delta_Tree).

purify_d_fact_tree(Facts,I) :-
   write('purify tree  '), ttyflush,
   start_timer(tree_purifying),
   retract(d_fact_tree(Tree)),
   purify_tree(Tree,Purified_Tree,Facts,I),
   assert(d_fact_tree(Purified_Tree)),
   stop_timer(tree_purifying).

 
/*** implementation ***********************************************/


tree_to_atoms([Node|Trees],Tree_Atoms) :-
   trees_to_atoms(Trees,Tree_Atoms_1),
   ord_union(Tree_Atoms_1,Node,Tree_Atoms).
tree_to_atoms([],[]).

trees_to_atoms([Tree|Trees],Tree_Atoms) :-
   tree_to_atoms(Tree,Tree_Atoms_1),
   trees_to_atoms(Trees,Tree_Atoms_2),
   ord_union(Tree_Atoms_1,Tree_Atoms_2,Tree_Atoms).
trees_to_atoms([],[]).
 

tree_fact(Fact) :-
   d_fact_tree(Tree), tree_to_d_fact(Tree,Fact).
delta_tree_fact(Fact) :-
   delta_d_fact_tree(Tree), tree_to_d_fact(Tree,Fact).


/* tree_to_d_fact(Tree,Fact) <-
      generates all the d_facts Fact represented by a tree Tree */

tree_to_d_fact([Node|Trees],Fact) :-
   member(Tree,Trees),
   tree_to_d_fact(Tree,Fact1),
   ord_union(Node,Fact1,Fact).
tree_to_d_fact([Node],Node).
   
tree_to_d_fact_loop([Tree|_],Fact) :-
   tree_to_d_fact(Tree,Fact).
tree_to_d_fact_loop([_|Trees],Fact) :-
   tree_to_d_fact_loop(Trees,Fact).


d_facts_to_tree :-
   d_facts_to_tree(Tree),
   ( retract(d_fact_tree(_)); true ),
   assert(d_fact_tree(Tree)).

d_facts_to_tree(Tree) :-
   collect_arguments(d_fact,Facts),
   tree_clauses_insert([],Tree,Facts).


/* reduce_join_tree(Join_Tree,Clause,Reduced_Join_Tree) <-
      reduces all the clause trees in Join_Tree,
      Reduced_Join_Tree contains the reduced clause trees,
      succeeds only if all reduced clause trees are non-empty. */

reduce_join_tree([L_Ts|L_L_Ts],Clause,[L_Reduced_Ts|L_L_Reduced_Ts]) :-
   reduce_tree([[]|L_Ts],Clause,[[]|L_Reduced_Ts]),
   !,
   reduce_join_tree(L_L_Ts,Clause,L_L_Reduced_Ts).
reduce_join_tree([],_,[]).


/* reduce_tree(Tree,Clause,Reduced_Tree) <-
      given a clause tree Tree representing a Herbrand state S,
      another clause tree Reduced_Tree is constructed that
      represents the reduced Herbrand state S' = reduce(S,C). */

reduce_tree([Node],Clause,[Node]) :-
   ord_disjoint(Node,Clause),
   !.
reduce_tree([_],_,[]) :-
   !.
reduce_tree([Node|Trees],Clause,Reduced_Tree) :-
   ord_disjoint(Node,Clause),
   !,
   reduce_trees(Trees,Clause,Reduced_Trees),
   reduce_tree_if([Node|Reduced_Trees],Reduced_Tree).
reduce_tree(_,_,[]).

reduce_tree_if([_],[]) :-
   !.
reduce_tree_if(Tree,Tree).

reduce_trees([Tree|Trees],Clause,[Reduced_Tree|Reduced_Trees]) :-
   reduce_tree(Tree,Clause,Reduced_Tree),
   Reduced_Tree \== [],
   !,
   reduce_trees(Trees,Clause,Reduced_Trees).
reduce_trees([_|Trees],Clause,Reduced_Trees) :-
   !,
   reduce_trees(Trees,Clause,Reduced_Trees).
reduce_trees([],_,[]).

join_trees(Join_Trees) :-
   findall( L_L_Trees,
      join_tree(L_L_Trees),
      Join_Trees ).

join_tree_fact(Fact) :-
   join_tree(Join_Tree),
   join_tree_to_d_fact(Join_Tree,Fact).

join_trees_to_d_facts(Facts) :-
   collect_arguments(join_tree_fact,Facts).

join_tree_to_d_fact([L_Trees|L_L_Trees],Clause) :-
   tree_to_d_fact_loop(L_Trees,Clause1),
   join_tree_to_d_fact(L_L_Trees,Clause2),
   ord_union(Clause1,Clause2,Clause).
join_tree_to_d_fact([],[]).


successor_trees([Node|Nodes],Prev_Trees,Final_Trees) :-
   good_successors(Node,Successors),
   successor_trees(Successors,[],Trees),
   successor_trees(Nodes,[[Node|Trees]|Prev_Trees],Final_Trees).
successor_trees([],Prev_Trees,Prev_Trees).

good_successors(L1,Successors) :-
   assert(good_edge([L1],[L1])),
   findall( L2,    
      good_edge(L1,L2),
      Successors ),
   retract(good_edge([L1],[L1])).


/* tree_prune(Set,[H|Trees],New_Tree) <-
     removes from the nodes in Tree all the atoms already contained 
     in Set or in some node on the path from the root to the considered 
     node */

tree_prune(Set,[H|Trees],[H1|New_Trees]) :-
   ord_subtract(H,Set,H1),
   ord_union(H,Set,Set1),
   trees_prune(Set1,Trees,New_Trees).
tree_prune(_,[],[]).

trees_prune(Set,[Tree|Trees],[New_Tree|New_Trees]) :-
   tree_prune(Set,Tree,New_Tree),
   trees_prune(Set,Trees,New_Trees).
trees_prune(_,[],[]).
 

/* tree_subsumes_clause(Tree,Clause,Rest_Clause) <-
      the d_fact_tree Tree subsumes the clause Clause 
      leaving the rest clause Rest_Clause.  */

tree_subsumes_clause([Node],Clause,Rest_Clause) :-
   !,
   ord_subset(Node,Clause),
   ord_subtract(Clause,Node,Rest_Clause).
tree_subsumes_clause([Node|Trees],Clause,Rest_Clause) :-
   ord_subset(Node,Clause),
   ord_subtract(Clause,Node,Int_Clause),
   member(Tree,Trees),
   tree_subsumes_clause(Tree,Int_Clause,Rest_Clause).


/* tree_subsumes_clause(Tree,Clause) <-
      the d_fact_tree Tree subsumes the clause Clause.  */ 

measured_tree_subsumes_clause(Tree,Clause) :-
   start_timer(tree_subsumes_clause),
   tree_subsumes_clause(Tree,Clause),
   add_timer(tree_subsumes_clause).

% tree_subsumes_clause([],_).
tree_subsumes_clause([Node],Clause) :-
   !,
   ord_subset(Node,Clause).
tree_subsumes_clause([Node|Trees],Clause) :-
   ord_subset(Node,Clause),
   ord_subtract(Clause,Node,Rest_Clause),
   member(Tree,Trees),
   tree_subsumes_clause(Tree,Rest_Clause).

tree_meta_subsumes_clause([Node],Clause) :-
   !,
   meta_subsumes(Node,Clause).
tree_meta_subsumes_clause([Node|Trees],Clause) :-
   meta_subsumes(Node,Clause),
   member(Tree,Trees),
   tree_meta_subsumes_clause(Tree,Clause).


/* tree_supersumes_clause(Tree1,Clause,Tree2) <-
      generates instantiations of Clause which are supersumed by
      the clause tree Tree1,
      for each Clause the clause Tree2 contains all clauses
      in Tree1 that supersume Clause. */
 
tree_supersumes_clause(Tree1,[A|As],Tree2) :-
   tree_resolve(Tree1,A,New_Tree),
   New_Tree \== [],
   tree_supersumes_clause([[A],New_Tree],As,Tree2).
tree_supersumes_clause(Tree,[],Tree).
 
 
/* tree_supersumes_clause(Tree,Clause) <-
      generates instantiations of Clause, which are supersumed by
      the clause tree Tree. */
 
% tree_supersumes_clause(Tree,Clause) :-
%  var(Tree),
%  !,
%  Clause \== [].
tree_supersumes_clause(Tree,Clause) :-
   var(Tree),
   !,
   ground_instantiate_clause(Clause). 
tree_supersumes_clause(Tree,[A|As]) :-
   tree_resolve(Tree,A,New_Tree),
   New_Tree \== [],
   tree_supersumes_clause([[A],New_Tree],As).
tree_supersumes_clause(_,[]).
 

/* join_tree_remove_supersets(Facts,PurifiedFacts) <- 
      reduces the fact list Facts to can(Facts) 
      The used algorithm is based on the asserted facts for
      join tree. 
      At first all the facts are asserted as new_fact(Fact).  */
  
join_tree_remove_supersets(Facts,PurifiedFacts) :-
   write('purifying    '), ttyflush,
   start_timer(remove_strict_supersets),
   start_timer(list_to_ord_sets),
   list_to_ord_set(Facts,Facts1),
   stop_timer(list_to_ord_sets,Time1),
   assert_arguments(new_fact,Facts1),
   join_tree_remove_supersets,
   collect_new_facts([],PurifiedFacts),
%  collect_arguments(new_fact,List),
%  remove_duplicates(List,PurifiedFacts),
   stop_timer(remove_strict_supersets,Time),
   pp_time(Time), write(' sec.                          '),
   pp_time(Time1), write(' sec. '),
   length(Facts1,K), write('  '), writeln(K).

join_tree_remove_supersets :-
   join_tree(L_L_Trees),
   new_fact(Clause),
   join_tree_superset(L_L_Trees,Clause,Clause),
   retractall(new_fact(Clause)),
   join_tree_remove_supersets.
join_tree_remove_supersets.
 
join_tree_superset([L_Trees|L_L_Trees],Clause,Clause1) :-
   Clause1 \== [],
   tree_subsumes_clause([[]|L_Trees],Clause,Clause3),
%  tree_to_d_fact_loop(L_Trees,Fact),
%  ord_subset(Fact,Clause),
%  ord_subtract(Clause1,Fact,Clause2),
   ord_intersection(Clause1,Clause3,Clause2),
   join_tree_superset(L_L_Trees,Clause,Clause2).
join_tree_superset([],_,[_|_]).


/* tree_clauses_insert(Tree1,Tree3,Clauses) <-
      inserts all clauses in Clauses in the d_fact_tree Tree1
      yielding a new d_fact_tree Tree3.  */

tree_clauses_insert(Tree1,Tree3,[Clause|Clauses]) :-
   tree_clause_insert(Tree1,Tree2,Clause),
   tree_clauses_insert(Tree2,Tree3,Clauses).
tree_clauses_insert(Tree,Tree,[]).

tree_clause_insert([],[Clause],Clause) :-
   !.
tree_clause_insert(Tree,New_Tree,Clause) :-
   best_intersection(Tree,Clause,Sequence,_),
   tree_clause_insert(Tree,New_Tree,Clause,Clause,Sequence),
   !.

tree_clause_insert([H],[H],Clause,Full_Clause,[]) :-
   ord_subset(H,Clause),
   writeln(' ^^^ tried to insert subsumed clause ^^^ '),
   ( retract(delta_d_fact(Full_Clause)); true ),
   !.
tree_clause_insert([H|_],[Clause],Clause,_,[]) :-
   ord_subset(Clause,H),
   !.
tree_clause_insert([H|Ts],[H,[Clause1]|Ts],Clause,_,[]) :-
   ord_subset(H,Clause),
   !,
   ord_subtract(Clause,H,Clause1).
tree_clause_insert([H|Ts],[Node,[Clause1],[H1|Ts]],Clause,_,[]) :-
   !,
   ord_intersection(H,Clause,Node),
   ord_subtract(Clause,H,Clause1),
   ord_subtract(H,Clause,H1).
tree_clause_insert([H|Ts],[H|New_Ts],Clause,Full_Clause,S) :-
   ord_subtract(Clause,H,Clause1),
   trees_clause_insert(Ts,New_Ts,Clause1,Full_Clause,S).

trees_clause_insert([Tree|Trees],[New_Tree|Trees],C1,C2,[I|S]) :-
   I = 1,
   !,
   tree_clause_insert(Tree,New_Tree,C1,C2,S).
trees_clause_insert([Tree|Trees],[Tree|New_Trees],C1,C2,[I|S]) :-
   II is I - 1,
   trees_clause_insert(Trees,New_Trees,C1,C2,[II|S]).


/* tree_clauses_insert(State) <-
      insert all clauses of the state State into the currently 
      asserted d_fact_tree. */

tree_clauses_insert([]) :-
   !. 
tree_clauses_insert(State) :-
   retract(d_fact_tree(Tree1)),  
   tree_clauses_insert(Tree1,Tree2,State), 
   assert(d_fact_tree(Tree2)).    


/* tree_clause_insert(Clause) <-
      insert the clause Clause in the currently asserted
      d_fact_tree.  */
 
tree_clause_insert(Clause) :-
   retract(d_fact_tree(Tree)),
   tree_clause_insert(Tree,New_Tree,Clause),
   assert(d_fact_tree(New_Tree)).
tree_clause_insert(Clause) :-
   tree_clause_insert([],New_Tree,Clause),
   assert(d_fact_tree(New_Tree)).


/* max_tree_clauses_insert(Tree1,Tree2,State) <-
      Inserts into the clause tree Tree1 those maximal clauses 
      of State which are not supersumed by Tree1,
      and obtains a clause tree Tree2. */ 

max_tree_clauses_insert(Tree1,Tree3,[Clause|Clauses]) :-
   max_tree_clause_insert(Tree1,Tree2,Clause),
   max_tree_clauses_insert(Tree2,Tree3,Clauses).
max_tree_clauses_insert(Tree,Tree,[]).
 
max_tree_clause_insert([],[Clause],Clause) :-
   !.
max_tree_clause_insert(Tree,New_Tree,Clause) :-
   best_intersection(Tree,Clause,Sequence,_),
   max_tree_clause_insert(Tree,New_Tree,Clause,Sequence),
   !.

max_tree_clause_insert([Node],[Clause],Clause,[]) :-
   ord_subset(Node,Clause),
   !.
max_tree_clause_insert([Node|Trees],[Node|Trees],Clause,[]) :-
   ord_subset(Clause,Node),
   writeln(' ^^^ tried to insert supersumed clause ^^^ '),
   !.
max_tree_clause_insert([Node|Ts],[Node,[Clause1]|Ts],Clause,[]) :-
   ord_subset(Node,Clause),
   !,
   ord_subtract(Clause,Node,Clause1).
max_tree_clause_insert([N|Ts],[N1,[Clause1],[N2|Ts]],Clause,[]) :-
   !,
   ord_intersection(N,Clause,N1),
   ord_subtract(Clause,N,Clause1),
   ord_subtract(N,Clause,N2).
max_tree_clause_insert([N|Ts],[N|New_Ts],Clause,S) :-
   ord_subtract(Clause,N,Clause1), 
   max_trees_clause_insert(Ts,New_Ts,Clause1,S).

max_trees_clause_insert([Tree|Trees],[New_Tree|Trees],Clause,[I|S]) :-
   I = 1,
   !,
   max_tree_clause_insert(Tree,New_Tree,Clause,S).
max_trees_clause_insert([Tree|Trees],[Tree|New_Trees],Clause,[I|S]) :-
   II is I - 1,
   max_trees_clause_insert(Trees,New_Trees,Clause,[II|S]).


/* trees_clause_delete(Clause) <-
      delete all clauses which are subsumed by Clause from the 
      currently asserted d_fact_tree. */
 
tree_clause_delete(Clause) :-
   retract(d_fact_tree(Tree)),
   purify_tree(Tree,Purified_Tree,[Clause],I),
   write(I), writeln(' clauses deleted'),
   assert(d_fact_tree(Purified_Tree)).


/* best_intersection(Tree,Clause,Sequence,Weight) <-
      determines the best path Sequence starting from the root
      of the d_fact_tree Tree, such that the clause Clause contains
      all the nodes on the path, its weight Weight is maximal,
      the path is described by the numbers of the choosen subtrees.  */

best_intersection([Head|Trees],Clause,Sequence,Weight) :-
   ord_subset(Head,Clause), 
   !,
   ord_subtract(Clause,Head,Clause1),
   best_intersection(Trees,1,Clause1,Sequence,Weight1),
   length(Head,Weight2),
   Weight is Weight1 + Weight2.
best_intersection([Head|_],Clause,[],Weight) :-
   ord_intersection(Head,Clause,Clause1),
   length(Clause1,Weight).

best_intersection([Tree|Trees],I,Clause,Sequence,Weight) :-
   best_intersection(Tree,Clause,Sequence1,Weight1),
   II is I + 1,
   best_intersection(Trees,II,Clause,Sequence2,Weight2),
   max(I,Sequence1,Weight1,Sequence2,Weight2,Sequence,Weight).
best_intersection([],_,_,[],0).

max(I,Sequence1,Weight1,_,Weight2,[I|Sequence1],Weight1) :-
   Weight1 > Weight2,
   !.
max(_,_,_,Sequence2,Weight2,Sequence2,Weight2).


/* purify_tree(Tree1,Tree2,State,I) <-
      given a clause tree Tree1, all clauses of Tree1 are removed
      which are subsumed by the state State, their number is I,
      the resulting clause tree is Tree2.  */

purify_tree(Tree_1,Tree_3,[Clause|Clauses],I) :-
   purify_tree_by_clause(Tree_1,Tree_2,Clause,I1),
   purify_tree(Tree_2,Tree_3,Clauses,I2),
   I is I1 + I2.
purify_tree(Tree_1,Tree_1,[],0).
 
purify_tree_by_clause([H|Ts],[],Clause,I) :-
   ord_subset(Clause,H),
   !,
%  write(Clause), writeln('subsumption'),
   tree_to_state([H|Ts],Subsumed_Facts),
   length(Subsumed_Facts,I).
purify_tree_by_clause([H],[H],_,0) :-
   !.
% purify_tree_by_clause([H,Tree],Purified_Tree,Clause,I) :-
%    ord_subtract(Clause,H,Reduced_Clause),
%    purify_tree_by_clause(Tree,New_Tree,Reduced_Clause,I),
%    purify_tree_by_clause_if([H,New_Tree],Purified_Tree),
%    !.
% purify_tree_by_clause([H|Ts],[H|New_Ts],Clause,I) :-
%    ord_subtract(Clause,H,Reduced_Clause),
%    purify_trees_by_clause(Ts,New_Ts,Reduced_Clause,I),
%    !.
purify_tree_by_clause([H|Ts],Purified_Tree,Clause,I) :-
   ord_subtract(Clause,H,Reduced_Clause),
   purify_trees_by_clause(Ts,New_Ts,Reduced_Clause,I),
   purify_tree_by_clause_if(H,New_Ts,Purified_Tree),
   !.
purify_tree_by_clause([],[],_,0).

purify_tree_by_clause_if(H,[[H1|Trees]],[H2|Trees]) :-
   !,
   ord_union(H,H1,H2).
purify_tree_by_clause_if(_,[],[]) :-
   !.
purify_tree_by_clause_if(H,Trees,[H|Trees]).

% purify_tree_by_clause_if([_,[]],[]) :-
%    !.
% purify_tree_by_clause_if([H,[H1|Trees]],[H2|Trees]) :-
%    ord_union(H,H1,H2).
 
purify_trees_by_clause([T|Ts],New_Ts,Clause,I) :-
   purify_tree_by_clause(T,New_Tree,Clause,I1),
   New_Tree == [],
   !,
   purify_trees_by_clause(Ts,New_Ts,Clause,I2),
   I is I1 + I2.
purify_trees_by_clause([T|Ts],[New_T|New_Ts],Clause,I) :-
   purify_tree_by_clause(T,New_T,Clause,I1),
   purify_trees_by_clause(Ts,New_Ts,Clause,I2),
   I is I1 + I2.
purify_trees_by_clause([],[],_,0).


/* delete_clauses_in_tree(State,Tree1,Tree2) <-
      deletes all branches representing a clause of State in the 
      clause tree Tree1, the result is the clause tree Tree2. */
 
delete_clauses_in_tree(State,Tree1,Tree2) :-
   write('del_in_tree  '), ttyflush,
   start_timer(delete_clauses_in_tree),
   delete_clauses_in_tree_2(State,Tree1,Tree2),
   stop_timer(delete_clauses_in_tree).

delete_clauses_in_tree_2([C|Cs],Tree1,Tree3) :-
   delete_clause_in_tree(C,Tree1,Tree2),
   delete_clauses_in_tree_2(Cs,Tree2,Tree3).
delete_clauses_in_tree_2([],Tree,Tree).


/* delete_clause_in_tree(Clause,Tree1,Tree2) <-
      deletes all branches representing the clause Clause in the clause
      tree Tree1, the result is a new clause tree Tree2. */
 
delete_clause_in_tree(Clause,[Node1|Trees1],Trees3) :-
   ord_subset(Node1,Clause),
   !,
   ord_subtract(Clause,Node1,Clause2),
   delete_clause_in_trees(Clause2,Trees1,[],Trees2),
   ( ( Trees2 == [], Trees3 = [] )
   ; ( Trees3 = [Node1|Trees2] ) ),
   !.
delete_clause_in_tree(_,Tree,Tree).
 
delete_clause_in_trees(Clause,[Tree1|Trees1],Sofar2,Trees2) :-
   delete_clause_in_tree(Clause,Tree1,Tree2),
   Tree2 \== [],
   !,
   delete_clause_in_trees(Clause,Trees1,[Tree2|Sofar2],Trees2).
delete_clause_in_trees(Clause,[_|Trees1],Sofar2,Trees2) :-
   delete_clause_in_trees(Clause,Trees1,Sofar2,Trees2).
delete_clause_in_trees(_,[],Sofar2,Trees2) :-
   reverse(Sofar2,Trees2).


/* clause_in_tree(Clause,Tree) <-
      succeeds if the clause Clause is represented in the clause
      tree Tree. */

clause_in_tree(Clause,[Clause]) :-
   !.
clause_in_tree(Clause,[Node|Trees]) :-
   ord_subset(Node,Clause),
   ord_subtract(Clause,Node,Clause2),
   member(Tree,Trees),
   clause_in_tree(Clause2,Tree).


/* assert_tree_atoms(Atoms) <-
      asserts all the atoms At in Atoms as tree_atom(At)
      for tree_resolve.  */

assert_tree_atoms([Atom|Atoms]) :-
   assert(tree_atom(Atom)),
   assert_tree_atoms(Atoms).
assert_tree_atoms([]) :-
   assert(tree_atom(dummy)).


/* pp_tree_errors(I) <-
      given the clause trees asserted as d_fact_tree(Tree) and
      delta_d_fact_tree(Delta_Tree),
      for I = 1, prints those clauses represented by Delta_Tree 
                 which are not subsumed by Tree,
      for I = 2, prints those clauses represented by Delta_Tree
                 which are subsumed by Tree,
      for I = 3, prints out the number N of clauses in Tree, 
                 the number M of different clauses in Tree, 
                 and K = N-M. */

pp_tree_errors(1) :-
   d_fact_tree(Tree), delta_d_fact_tree(Delta_Tree),
   findall( Fact,
      ( tree_to_d_fact(Delta_Tree,Fact),
        \+ tree_subsumes_clause(Tree,Fact) ),
      Facts ),
   writeln(Facts).

pp_tree_errors(2) :-
   d_fact_tree(Tree), delta_d_fact_tree(Delta_Tree),
   findall( Fact,
      ( tree_to_d_fact(Delta_Tree,Fact),
        tree_subsumes_clause(Tree,Fact) ),
      Facts ),
   writeln(Facts).

pp_tree_errors(3) :-
   writeln('pp_tree_errors'),
   d_fact_tree(Tree),
   findall( Fact,
      tree_to_d_fact(Tree,Fact),
      Facts ),
   list_to_ord_set(Facts,Facts_1),
   length(Facts,N), length(Facts_1,M), K is N - M,
   writeln([N,M,K]),
   writeln('pp_tree_errors').
  

/* pp_tree_errors(2,State) <-
      given the clause tree asserted as d_fact_tree(Tree) and
      a state State,
      prints out the clauses in State which are subsumed by Tree. */

pp_tree_errors(2,State) :-
   d_fact_tree(Tree),
   findall( Fact,
      ( member(Fact,State),
        tree_subsumes_clause(Tree,Fact) ),
      Facts ),
   writeln(Facts),
   writeln('pp_tree_errors').


/* pp_purify_errors(1,State) <-
      prints out the clauses in State which are not contained
      in the canonical form can(State). */

pp_purify_errors(1,State) :-
   writeln('pp_purify_errors'), 
   findall( Fact_2,    
      ( member(Fact_1,State),
        member(Fact_2,State),
        ord_subset(Fact_1,Fact_2),
        ord_subtract(Fact_2,Fact_1,[_|_]) ),
      Facts ),
   writeln(Facts), 
   writeln('pp_purify_errors').

 
/******************************************************************/


