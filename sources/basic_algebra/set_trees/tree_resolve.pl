

/******************************************************************/
/***                                                            ***/
/***             DisLog :  Tree-Resolution                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tree_reduce(Tree1,Literal,Tree2) <-
      Tree2 is the reduction of the clause tree Tree1
      w.r.t. the ground literal Literal. */

tree_reduce([Node|_],Literal,[]) :-
   member(Literal,Node),
   !.
tree_reduce([Node],_,[Node]) :-
   !. 
tree_reduce([Node|Trees],Literal,[Node|Reduced_Trees]) :-
   trees_reduce(Trees,Literal,New_Trees),
   remove_empty_trees(New_Trees,Reduced_Trees),
   Reduced_Trees \== [],
   !. 
tree_reduce(_,_,[]).
 
trees_reduce([Tree|Trees],Literal,[New_Tree|New_Trees]) :-
   tree_reduce(Tree,Literal,New_Tree),
   trees_reduce(Trees,Literal,New_Trees).
trees_reduce([],_,[]).

 
/* tree_based_join(Facts,M) <-
      joins the d_facts with the delta_d_facts,
      yields a set Facts of M derived facts.  */
 
tree_based_join(Facts,M) :-
   dislog_flag_get(join_mode,regular),
   write('tree join    '), ttyflush,
   start_timer(tree_based_join),
   d_fact_tree(Tree),
   tree_to_atoms(Tree,Atoms), assert_tree_atoms(Atoms),
   findall( Pruned_Fact,
      ( d_clause(Head,Body),
        tree_to_clause_delta_resolvable(Tree,Body,Clause),
        head_ord_union(Head,Clause,Fact),
        remove_duplicates(Fact,Pruned_Fact) ),
      Facts ),
   abolish(tree_atom,1),
   length(Facts,M),
   stop_timer(tree_based_join).

tree_based_join(Facts,M) :-
   dislog_flag_get(join_mode,special),
   write('tree join    '), ttyflush,
   start_timer(tree_based_join),
   d_fact_tree(Tree),
   tree_to_atoms(Tree,Atoms), assert_tree_atoms(Atoms),
   findall( Pruned_Fact,
      ( d_clause(Head,Body),
        bi_tree_to_clause_delta_resolvable(Tree,Body,Clause),
        head_ord_union(Head,Clause,Fact),
        remove_duplicates(Fact,Pruned_Fact) ),
      Facts ),
   abolish(tree_atom,1),
   length(Facts,M),
   stop_timer(tree_based_join).

   
tree_join(Program,Tree1,Tree2,State3) :-
   dlp_to_d_clauses(Program),
   assert(d_fact_tree(Tree1)),
   assert(delta_d_fact_tree(Tree2)),
   tree_join(State3,_),
   retract(d_fact_tree(_)),
   retract(delta_d_fact_tree(_)),
   dabolish(d_clause/2).
 

/* tree_join(Facts,M) <-
      joins the d_fact_tree(T1) with the delta_d_fact_tree(T2),
      asserts J derived join trees join_tree(Tree),
      yields a set Facts of M derived facts.  */

tree_join(Facts,M) :-
   dislog_flag_get(join_mode,regular),
   write('tree join    '), ttyflush,
   start_timer(tree_join),
   d_fact_tree(T1), delta_d_fact_tree(T2),
   tree_to_atoms(T1,Atoms), assert_tree_atoms(Atoms),
   abolish(join_tree,1), assert(join_tree([])),
   findall( _,
      ( d_clause(Head,Body),
        tree_to_join_tree_delta_resolvable(T1,T2,Body,Join_Tree),
        list_to_ord_set(Head,Ord_Head),
        JT = [[[Ord_Head]]|Join_Tree],
        reduce_join_tree(JT,Body,RJT),
        assert(join_tree(RJT)) ),
      AJT ),
   retract(join_tree([])),
   abolish(tree_atom,1), 
   join_trees_to_d_facts(Facts), length(Facts,M),
   stop_timer_write(tree_join),
   length(AJT,J), write(J), writeln(' join trees ').

tree_join(Facts,M) :-
   dislog_flag_get(join_mode,special),
   bi_tree_join(Facts,M).

bi_tree_join(Facts,M) :-
   dislog_flag_get(join_mode,special),
   write('tree join    '), ttyflush,
   start_timer(tree_join),
   d_fact_tree(T1), delta_d_fact_tree(T2),
   tree_to_atoms(T1,Atoms), assert_tree_atoms(Atoms),
   abolish(join_tree,1),
   findall( _,
      ( d_clause(Head,Body),
        bi_tree_to_join_tree_delta_resolvable(T1,T2,Body,Join_Tree),
        list_to_ord_set(Head,Ord_Head),
        assert(join_tree([[[Ord_Head]]|Join_Tree])) ),
      AJT ),
   abolish(tree_atom,1),
   join_trees_to_d_facts(Facts), length(Facts,M),
   stop_timer_write(tree_join),
   length(AJT,J), write(J), writeln(' join trees ').


/* new_tree_join(Facts,M) <-
      joins the d_fact_tree(T1) with the delta_d_fact_tree(T2) and 
      old_d_fact_tree(T3), asserts J derived join trees join_tree(Tree),
      yields a set Facts of M derived facts.  */
 
new_tree_join(Facts,M) :-
   write('tree join    '), ttyflush,
   start_timer(tree_join),
   d_fact_tree(T1), delta_d_fact_tree(T2),
   old_d_fact_tree(T3),
   tree_to_atoms(T1,Atoms), assert_tree_atoms(Atoms),
   abolish(join_tree,1),
   findall( _,
      ( d_clause(Head,Body),
        new_tree_to_join_tree_delta_resolvable(T3,T2,Body,Join_Tree),
        list_to_ord_set(Head,Ord_Head),
        assert(join_tree([[[Ord_Head]]|Join_Tree])) ),
      AJT ),
   abolish(tree_atom,1),
   join_trees_to_d_facts(Facts), length(Facts,M),
   stop_timer_write(tree_join),
   length(AJT,J), write(J), writeln(' join trees ').

  

/* tree_to_join_tree_delta_resolvable(Tree,Delta_Tree,Clause,[T1,...,Tn]) <-
      resolves the clause Clause by Tree and Delta_Tree
      yielding a join tree [T1,...,Tn].  */ 

tree_to_join_tree_delta_resolvable(Tree,Delta_Tree,[L|Ls],[H|T]) :-
   append(Xs,[Y|Ys],[L|Ls]), append(Xs,Ys,Zs),    
   tree_to_join_tree_resolvable(Delta_Tree,[Y],[H]),
   tree_to_join_tree_resolvable(Tree,Zs,T).
tree_to_join_tree_delta_resolvable(_,_,[],[]).   
 
new_tree_to_join_tree_delta_resolvable(Tree,Delta_Tree,[L|Ls],[H|T]) :-
   append(Xs,[Y|Ys],[L|Ls]),
   tree_to_join_tree_resolvable(Delta_Tree,[Y],[H]),
   tree_to_join_tree_resolvable(Tree,Xs,T1),
   tree_to_join_tree_resolvable([[],Tree,Delta_Tree],Ys,T2),
   append(T1,T2,T).
new_tree_to_join_tree_delta_resolvable(_,_,[],[]).   
 
bi_tree_to_join_tree_delta_resolvable(Tree,Delta_Tree,[L|Ls],[H|T]) :-
   append(Xs,[Y|Ys],[L|Ls]), append(Xs,Ys,Zs),
   base_literal(Y),
   tree_to_join_tree_resolvable(Delta_Tree,[Y],[H]),
   bi_tree_to_join_tree_resolvable(Tree,Zs,T).
bi_tree_to_join_tree_delta_resolvable(_,_,[],[]).   
 
 
/* tree_to_join_tree_resolvable(Tree,[L1,...,Ln],[T1,...,Tn]) <-
      finds simultaneous resolving trees [T1,...,Tn] for [L1,...,Ln] */

tree_to_join_tree_resolvable(Tree,[L|Ls],[L_New_Trees|L_L_New_Trees]) :-
   tree_atom(L),
%  tree_to_trees_resolve(Tree,L,L_New_Trees),
   tree_to_trees_resolve(Tree,L,[],L_New_Trees),
   L_New_Trees \== [],
   tree_to_join_tree_resolvable(Tree,Ls,L_L_New_Trees).
tree_to_join_tree_resolvable(_,[],[]).

bi_tree_to_join_tree_resolvable(Tree,[L|Ls],L_L_New_Trees) :-
   \+ base_literal(L),
   !, 
   call(L),
   bi_tree_to_join_tree_resolvable(Tree,Ls,L_L_New_Trees).
bi_tree_to_join_tree_resolvable(Tree,[L|Ls],[L_New_Trees|L_L_New_Trees]) :-
   tree_atom(L),
   tree_to_trees_resolve(Tree,L,[],L_New_Trees),
   L_New_Trees \== [],
   bi_tree_to_join_tree_resolvable(Tree,Ls,L_L_New_Trees).
bi_tree_to_join_tree_resolvable(_,[],[]).

 
/* tree_to_clause_delta_resolvable(Tree,Tree_Atoms,[L1,...,Ln],Clause) <-
      finds at least one delta_d_fact and d_facts  Li' v Ci' from the 
      tree where all the literals Li simultaneously can be unified 
      with the literals Li' and sets  Clause = C1' v ... v Cn'
      the atoms occurring in Tree are asserted as tree_atom(At)
      To gain efficiency [L1,...,Ln] is broken into two parts by
      append and the first literal of the second part is tested
      whether it is resolvable with a delta_d_fact. The case that
      the body is resolved by old d_facts only is therefore avoided.
      After the delta test for the chosen Literal is successful the
      remaining parts are tested by the ordinary resolvable
      predicate; this is possible because derived facts are
      asserted as delta_d_facts as well as as d_facts.  */
 
tree_to_clause_delta_resolvable(Tree,[L|Ls],Clause) :-
   delta_d_fact_tree(Delta_Tree),
   !,
   append(Xs,[Y|Ys],[L|Ls]), append(Xs,Ys,Zs),
   tree_to_clause_resolve(Delta_Tree,Y,Clause1),
   tree_to_clause_resolvable(Tree,Zs,Clause2),
   ord_union(Clause1,Clause2,Clause).
tree_to_clause_delta_resolvable(_,[],[]).
 

/* tree_to_clause_resolvable(Tree,[L1,...,Ln],Clause) <-
      finds d_facts  Li' v Ci'  in Tree where all the literals Li
      simultaneously can be unified with the literals Li'
      and sets  Clause = C1' v ... v Cn' */

tree_to_clause_resolvable(Tree,[L|Ls],Clause) :-
   tree_to_clause_resolve(Tree,L,Clause1),
   tree_to_clause_resolvable(Tree,Ls,Clause2),
   ord_union(Clause1,Clause2,Clause).
tree_to_clause_resolvable(_,[],[]).
 
tree_to_clause_resolve(Tree,L,Clause) :-
   tree_atom(L),
   tree_to_trees_resolve(Tree,L,[],New_Trees),
   tree_to_d_fact_loop(New_Trees,Clause).


/* tree_resolve(Tree,Literal,Resolvent) <-
      resolves the clause tree Tree by the literal ~Literal to the
      clause tree Resolvent, 
      Literal can be non-ground, then on backtracking all tree 
      resolvents for different ground instances of Literal are 
      computed including the empty tree.  */

tree_resolve(Tree,Literal,Tree) :-
   dislog_flag_get(join_mode,special),
   special_literal(Literal),
   !,
   call(Literal).
tree_resolve([Node|Trees],Literal,[New_Node|Trees]) :-
   ground(Literal), member(Literal,Node),
   !,
   ord_del_element(Node,Literal,New_Node).
tree_resolve([Node|Trees],Literal,Tree_Resolvent) :-
   ground(Literal), 
   !,
   trees_resolve(Trees,Literal,New_Trees),
   remove_empty_trees(New_Trees,Non_Empty_New_Trees),
   ( ( Non_Empty_New_Trees \== [],
       !,
       remove_trivial_trees(Non_Empty_New_Trees,Non_Trivial_New_Trees),
       Tree_Resolvent = [Node|Non_Trivial_New_Trees] )
   ; Tree_Resolvent = [] ).
tree_resolve([Node|Trees],Literal,[New_Node|Trees]) :-
   member(Literal,Node),
   ord_del_element(Node,Literal,New_Node).
tree_resolve([Node|Trees],Literal,Tree_Resolvent) :-
   trees_resolve(Trees,Literal,New_Trees),
   remove_empty_trees(New_Trees,Non_Empty_New_Trees),
   tree_resolve_if(Node,Non_Empty_New_Trees,Tree_Resolvent).

tree_resolve_if(Node,Trees,[Node|Non_Trivial_Trees]) :-
   Trees \== [], !,
   remove_trivial_trees(Trees,Non_Trivial_Trees).
tree_resolve_if(_,_,[]).


/*

tree_resolve([Node|Trees],Literal,[New_Node|Trees]) :-
   ground(Literal), member(Literal,Node),
   !,
   ord_del_element(Node,Literal,New_Node).
tree_resolve([Node|Trees],Literal,[New_Node|Trees]) :-
   member(Literal,Node),
   ord_del_element(Node,Literal,New_Node).
tree_resolve([Node|Trees],Literal,[Node|Non_Trivial_New_Trees]) :-
   trees_resolve(Trees,Literal,New_Trees),
   remove_empty_trees(New_Trees,Non_Empty_New_Trees),
   Non_Empty_New_Trees \== [],
   remove_trivial_trees(Non_Empty_New_Trees,Non_Trivial_New_Trees).
tree_resolve(_,_,[]).

*/
 
trees_resolve([Tree|Trees],Literal,[New_Tree|New_Trees]) :-
   tree_resolve(Tree,Literal,New_Tree),
   trees_resolve(Trees,Literal,New_Trees).
trees_resolve([],_,[]).
 
tree_to_trees_resolve(Tree,Literal,[New_Tree]) :- 
   tree_to_tree_resolve(Tree,Literal,New_Tree),
   New_Tree \== [],
   !.
tree_to_trees_resolve(_,_,[]).

tree_to_tree_resolve([Node|Trees],Literal,[New_Node|Trees]) :- 
   member(Literal,Node),
   !,
   ord_del_element(Node,Literal,New_Node).
tree_to_tree_resolve([Node|Trees],Literal,
      [Node|Non_Trivial_New_Trees]) :- 
   trees_to_trees_resolve(Trees,Literal,New_Trees),
   remove_empty_trees(New_Trees,Non_Empty_New_Trees),
   Non_Empty_New_Trees \== [],
   !,
   remove_trivial_trees(Non_Empty_New_Trees,Non_Trivial_New_Trees).
tree_to_tree_resolve(_,_,[]).
   
trees_to_trees_resolve([Tree|Trees],Literal,[New_Tree|New_Trees]) :-
   tree_to_tree_resolve(Tree,Literal,New_Tree),
   trees_to_trees_resolve(Trees,Literal,New_Trees).
trees_to_trees_resolve([],_,[]).

remove_empty_trees([[]|Trees],Non_Empty_Trees) :-
   !,
   remove_empty_trees(Trees,Non_Empty_Trees).
remove_empty_trees([Tree|Trees],[Tree|Non_Empty_Trees]) :-
   remove_empty_trees(Trees,Non_Empty_Trees).
remove_empty_trees([],[]).

remove_trivial_trees([[]|Trees],Non_Trivial_Trees) :-
   !,
   remove_trivial_trees(Trees,Non_Trivial_Trees).
remove_trivial_trees([Tree|Trees],[Tree|Non_Trivial_Trees]) :-
   remove_trivial_trees(Trees,Non_Trivial_Trees).
remove_trivial_trees([],[]).

  
tree_to_trees_resolve([Node|Trees],Literal,Acc_Set,[[New_Node|Trees]]) :-
   member(Literal,Node),
   !, 
   ord_del_element(Node,Literal,Reduced_Node),
   ord_union(Reduced_Node,Acc_Set,New_Node).
tree_to_trees_resolve([Node|Trees],Literal,Acc_Set,New_Trees) :-
   ord_union(Node,Acc_Set,New_Acc_Set),
   trees_to_trees_resolve(Trees,Literal,New_Acc_Set,New_Trees).
      
trees_to_trees_resolve([Tree|Trees],Literal,Acc_Set,New_Trees) :-
   tree_to_trees_resolve(Tree,Literal,Acc_Set,New_Trees_1),
   trees_to_trees_resolve(Trees,Literal,Acc_Set,New_Trees_2),
   append(New_Trees_1,New_Trees_2,New_Trees).
trees_to_trees_resolve([],_,_,[]).

  
/* bi_tree_to_clause_delta_resolvable(Tree,[L1,...,Ln],Clause) <-
      finds at least one delta_d_fact and d_facts  Li' v Ci' from the 
      tree where all the literals Li simultaneously can be unified 
      with the literals Li' and sets  Clause = C1' v ... v Cn'
      To gain efficiency [L1,...,Ln] is broken into two parts by
      append and the first literal of the second part is tested
      whether it is resolvable with a delta_d_fact. The case that
      the body is resolved by old d_facts only is therefore avoided.
      After the delta test for the chosen Literal is successful the
      remaining parts are tested by the ordinary resolvable
      predicate; this is possible because derived facts are
      asserted as delta_d_facts as well as as d_facts.  */
 
bi_tree_to_clause_delta_resolvable(Tree,[L|Ls],Clause) :-
   delta_d_fact_tree(Delta_Tree),
   !,
   append(Xs,[Y|Ys],[L|Ls]), append(Xs,Ys,Zs),    
   base_literal(Y),
   tree_to_clause_resolve(Delta_Tree,Y,Clause1),
   bi_tree_to_clause_resolvable(Tree,Zs,Clause2),
   ord_union(Clause1,Clause2,Clause).
%  ord_disjoint(Clause1,Clause2).
bi_tree_to_clause_delta_resolvable(_,[],[]).   
 
 
/* bi_tree_to_clause_resolvable(Tree,[L1,...,Ln],Clause) <-
      finds d_facts  Li' v Ci'  in Tree where all the literals Li
      simultaneously can be unified with the literals Li'
      and sets  Clause = C1' v ... v Cn' */

bi_tree_to_clause_resolvable(Tree,[L|Ls],Clause) :-
   \+ base_literal(L),
   !, 
   call(L),
   bi_tree_to_clause_resolvable(Tree,Ls,Clause).
bi_tree_to_clause_resolvable(Tree,[L|Ls],Clause) :-
   tree_to_clause_resolve(Tree,L,Clause1),
   bi_tree_to_clause_resolvable(Tree,Ls,Clause2),
   ord_union(Clause1,Clause2,Clause).
bi_tree_to_clause_resolvable(_,[],[]).


/******************************************************************/

