

/******************************************************************/
/***             Module :  Model-Trees                          ***/
/******************************************************************/


dddb(
   [ [[e],[b,c]], [[a,b],[]], [[a,c],[]],
     [[c,d,f],[]], [[b],[]], [[f],[a,d]] ]).


/*** interface ****************************************************/


built_tree :-
   dddb(Db),
   ldddb,
   built_tree(Db,[[]],Tree,_),
   writeln('minimal model tree'), nl,
   pp_tree(Tree,0),
   assert(c_fact_tree(Tree)).

ppct :-
   c_fact_tree(Tree),
   nl, pp_tree(Tree,0).


built_tree(Db,_,[[]],[]) :-
   member([[],[]],Db),
   !.
built_tree(Db,Root,Tree,Neg_Set) :-
   maximal_positive_atom(Db,A),
   !,
   positive_resolve_database(A,Db,Db_1),
   negative_resolve_database(A,Db,Db_2),
   list_to_ord_set(Db_1,Db_A),
   list_to_ord_set(Db_2,Db_Not_A),
   ( ( current_num(trace_level,6), 
       write('right '), writeln(A),
       writeln([Db_Not_A,Root,Tree_R,Neg_R]),
       wait )
   ; true ),
   built_tree(Db_Not_A,Root,Tree_R,Neg_R),
   ( ( current_num(trace_level,6), 
       write('up right '), writeln(A),
       writeln([Db_Not_A,Root,Tree_R,Neg_R]), 
       wait )
   ; true ),
   union(Db_A,Neg_R,Db_L),
   ( ( current_num(trace_level,6), 
       write('left '), writeln(A),
       writeln([Db_L,A,Tree_L,Neg_L]), 
       wait )
   ; true ),
   built_tree(Db_L,[[A]],Tree_L,Neg_L),
   ( ( current_num(trace_level,6), 
       write('up left '), writeln(A),
       writeln([Db_L,A,Tree_L,Neg_L]), 
       wait )
   ; true ),
   add_to_clauses([[],[A]],Neg_L,Join),
   union(Neg_R,Join,Neg_Set), 
   melt_trees(Root,Tree_L,Tree_R,Tree).
built_tree(_,Root,Root,[[[],[]]]).
   
melt_trees(_,[[]],Tree_R,Tree_R) :-
   !.
melt_trees(Root,Tree_L,[[]],Tree) :-
   !,
   attach_left_node(Root,Tree_L,Tree).
melt_trees(_,Tree_L,Tree_R,Tree) :-
   attach_left_node(Tree_R,Tree_L,Tree).
   
attach_left_node([Root|Trees],Tree,[Root,Tree|Trees]).
   
   
negative_resolve_database(A,[[C1,C2]|Pairs],[[C3,C2]|New_Pairs]) :-
   \+ member(A,C2),
   resolve(A,C1,C3),
   !,
   negative_resolve_database(A,Pairs,New_Pairs).
negative_resolve_database(A,[[C1,C2]|Pairs],[[C1,C2]|New_Pairs]) :-
   \+ member(A,C2),
   !,
   negative_resolve_database(A,Pairs,New_Pairs).
negative_resolve_database(A,[[_,_]|Pairs],New_Pairs) :-
   negative_resolve_database(A,Pairs,New_Pairs).
negative_resolve_database(_,[],[]).

positive_resolve_database(A,[[C1,C2]|Pairs],[[C1,C3]|New_Pairs]) :-
   \+ member(A,C1),
   resolve(A,C2,C3),
   !,
   positive_resolve_database(A,Pairs,New_Pairs).
positive_resolve_database(A,[[C1,C2]|Pairs],[[C1,C2]|New_Pairs]) :-
   \+ member(A,C1),
   !,
   positive_resolve_database(A,Pairs,New_Pairs).
positive_resolve_database(A,[[_,_]|Pairs],New_Pairs) :-
   positive_resolve_database(A,Pairs,New_Pairs).
positive_resolve_database(_,[],[]).

   
maximal_positive_atom(Db,A) :-
   first_components(Db,Atoms),
   list_to_ord_set(Atoms,[A|_]).
   
first_components(Pairs,Atoms) :-
   first_components(Pairs,[],Atoms).

first_components([[C1,_]|Pairs],Sofar,Atoms) :-
   union(C1,Sofar,New_Sofar),
   first_components(Pairs,New_Sofar,Atoms).
first_components([],Sofar,Sofar).

add_to_clauses([C1,C2],[[C3,C4]|Pairs],[[C5,C6]|New_Pairs]) :-
   union(C1,C3,C5), union(C2,C4,C6),
   add_to_clauses([C1,C2],Pairs,New_Pairs).
add_to_clauses([_,_],[],[]).


/******************************************************************/


