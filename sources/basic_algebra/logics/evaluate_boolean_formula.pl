

/******************************************************************/
/***                                                            ***/
/***         Logics:  Evaluation of Boolean Formulas            ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(evaluate_boolean_formula, 1) :-
   Formula = -((a,b;c)),
   Assignment = [a=1, b=1, c=0],
   evaluate_boolean_formula(
      Assignment, Formula, Value),
   write(user, Value), nl.
test(boolean_formula_to_truth_table, 1) :-
   Formula = -((a,b;c)),
   Atoms = [a,b,c],
   boolean_formula_to_truth_table(Formula, Atoms).
test(boolean_formula_to_models, 1) :-
   Formula = -((a,b)),
   Atoms = [a,b],
   boolean_formula_to_models(Formula, Atoms).


/*** interface ****************************************************/


/* boolean_formula_to_models(Formula, Atoms, Models) <-
      */

boolean_formula_to_models(Formula, Atoms) :-
   boolean_formula_to_models(Formula, Atoms, Models),
   catch( xpce_display_table(Atoms, Models), _, true ),
   ( foreach(Model, Models) do
        write(user, Model), nl ).

boolean_formula_to_models(Formula, Atoms, Models) :-
   boolean_formula_to_truth_table(Formula, Atoms, Rows),
   findall( Model,
      ( member(Row, Rows),
        append(Model, [1], Row) ),
      Models ).


/* boolean_formula_to_truth_table(Formula, Atoms, Rows) <-
      */

boolean_formula_to_truth_table(Formula, Atoms) :-
   boolean_formula_to_truth_table(Formula, Atoms, Rows),
   append(Atoms, ['F'], Attributes),
   catch( xpce_display_table(Attributes, Rows), _, true ),
   ( foreach(Row, Rows) do
        write(user, Row), nl ).

boolean_formula_to_truth_table(Formula, Atoms, Rows) :-
   assignments_generate(Atoms, Assignments),
   ( foreach(Assignment, Assignments), foreach(Row, Rows) do
        evaluate_boolean_formula(Assignment, Formula, Value),
        pair_lists(=, _, Values, Assignment),
        append(Values, [Value], Row) ),
   !.


/* assignments_generate(Atoms, Assignments) <-
      */

assignments_generate(Atoms, Assignments) :-
   length(Atoms, N),
   list_to_cartesian_power(N, [0,1], Tuples_2),
   sort(Tuples_2, Tuples),
   maplist( pair_lists(=, Atoms),
      Tuples, Assignments ).


/* evaluate_boolean_formula(Assignment, Formula, Value) <-
      */

evaluate_boolean_formula(Assignment, (A->B), Value) :-
   evaluate_boolean_formula(Assignment, A, V),
   evaluate_boolean_formula(Assignment, B, W),
   boolean_implication(V, W, Value).
evaluate_boolean_formula(Assignment, (A,B), Value) :-
   evaluate_boolean_formula(Assignment, A, V),
   evaluate_boolean_formula(Assignment, B, W),
   boolean_and(V, W, Value).
evaluate_boolean_formula(Assignment, (A;B), Value) :-
   evaluate_boolean_formula(Assignment, A, V),
   evaluate_boolean_formula(Assignment, B, W),
   boolean_or(V, W, Value).
evaluate_boolean_formula(Assignment, -A, Value) :-
   evaluate_boolean_formula(Assignment, A, V),
   boolean_not(V, Value).
evaluate_boolean_formula(Assignment, A, Value) :-
   boolean_formula_is_atomic(A),
   member(A=Value, Assignment).


/* boolean_and(U, V, W) <-
      W is derived as the boolean conjunction
      of the truth values U and V. */

boolean_and(0, 0, 0).
boolean_and(0, 1, 0).
boolean_and(1, 0, 0).
boolean_and(1, 1, 1).


/* boolean_or(U, V, W) <-
      W is derived as the boolean disjunction
      of the truth values U and V. */

boolean_or(0, 0, 0).
boolean_or(0, 1, 1).
boolean_or(1, 0, 1).
boolean_or(1, 1, 1).


/* boolean_not(U, V) <-
      V is derived as the boolean negation
      of the truth value U. */

boolean_not(0, 1).
boolean_not(1, 0).


/* boolean_implication(U, V, W) <-
      W is derived as the boolean implication of U and V:
      W is 1, iff V >= U; W is 0, iff U = 1 and V = 0. */

boolean_implication(0, 0, 1).
boolean_implication(0, 1, 1).
boolean_implication(1, 0, 0).
boolean_implication(1, 1, 1).


/* boolean_formula_is_atomic(A) <-
      */

boolean_formula_is_atomic(A) :-
   \+ ( functor(A, F, N),
        member(F:N, ['-':1, ',':2, ';':2]) ).


/******************************************************************/


