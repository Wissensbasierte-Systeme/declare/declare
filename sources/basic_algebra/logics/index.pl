

:- multifile
      test/2.

:- ['library/loops.pl'].

:- ['sources/basic_algebra/basics/pair_lists.pl'].
:- ['sources/basic_algebra/basics/elementary.pl'].
:- ['sources/basic_algebra/basics/functions.pl'].
:- ['sources/basic_algebra/basics/lists.pl'].

:- ['sources/basic_algebra/logics/evaluate_boolean_formula.pl'].
:- ['sources/basic_algebra/logics/boolean_normal_forms.pl'].


