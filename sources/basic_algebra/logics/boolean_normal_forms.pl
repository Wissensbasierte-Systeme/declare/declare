

/******************************************************************/
/***                                                            ***/
/***           Logics:  Boolean Normal Forms                    ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(boolean_normalize, knf) :-
   Formula = -((a,(b;c))),
   write(Formula), nl,
   catch( term_to_picture(Formula), _, true ),
   boolean_normalize(knf, Formula, Cs),
   write(Cs), nl,
   boolean_normal_form_to_formula(knf, Cs, F),
   catch( term_to_picture(F), _, true ),
   write(F), nl.
test(boolean_normalize, dnf) :-
   Formula = -((a,(b;c))),
   write(Formula), nl,
   catch( term_to_picture(Formula), _, true ),
   boolean_normalize(dnf, Formula, Cs),
   write(Cs), nl,
   boolean_normal_form_to_formula(dnf, Cs, F),
   catch( term_to_picture(F), _, true ),
   write(F), nl.


/*** interface ****************************************************/


/* boolean_normalize_formula(Mode, Formula_1, Formula_2) <-
      */

boolean_normalize_formula(Mode, Formula_1, Formula_2) :-
   boolean_normalize(Mode, Formula_1, Cs_1),
   writeln(Cs_1),
   boolean_normal_form_prune(Cs_1, Cs),
   state_can(Cs, Cs_2),
   writeln(Cs_2),
   boolean_normal_form_to_formula(Mode, Cs_2, Formula_2).


/* boolean_normalize(Mode, Formula, Lists) <-
      */

% xor
boolean_normalize(Mode, A+B, Cs) :-
   boolean_normalize(Mode, ((A;B),(-A;-B)), Cs).

% negation
boolean_normalize(knf, -A, Cs) :-
   boolean_normalize(dnf, A, Cs_2),
   maplist( literals_to_complements,
      Cs_2, Cs ).
boolean_normalize(dnf, -A, Cs) :-
   boolean_normalize(knf, A, Cs_2),
   maplist( literals_to_complements,
      Cs_2, Cs ).

% disjunction
boolean_normalize(knf, (A;B), Cs) :-
   boolean_normalize(knf, A, As),
   boolean_normalize(knf, B, Bs),
   combine_clauses(As, Bs, Cs).
boolean_normalize(dnf, (A;B), Cs) :-
   boolean_normalize(dnf, A, As),
   boolean_normalize(dnf, B, Bs),
   append(As, Bs, Cs).

% conjunction
boolean_normalize(knf, (A,B), Cs) :-
   boolean_normalize(knf, A, As),
   boolean_normalize(knf, B, Bs),
   append(As, Bs, Cs).
boolean_normalize(dnf, (A,B), Cs) :-
   boolean_normalize(dnf, A, As),
   boolean_normalize(dnf, B, Bs),
   combine_clauses(As, Bs, Cs).

% atoms
boolean_normalize(_, A, [[A]]) :-
   boolean_formula_is_atomic(A).


/* combine_clauses(As, Bs, Cs) <-
      */

combine_clauses(As, Bs, Cs) :-
   findall( C,
      ( member(Ca, As), member(Cb, Bs),
        append(Ca, Cb, C) ),
      Cs ).


/* literals_to_complements(Xs, Ys) <-
      */

literals_to_complements(Xs, Ys) :-
   maplist( literal_to_complement,
      Xs, Ys ).

literal_to_complement(-X, X) :-
   !.
literal_to_complement(X, -X).


/* boolean_normal_form_to_formula(Mode, Lists, Formula) <-
      */

boolean_normal_form_to_formula(knf, Cs, F) :-
   ( foreach(C, Cs), foreach(F, Fs) do
        list_to_functor_structure(';', C, F) ),
   list_to_functor_structure(',', Fs, F).
boolean_normal_form_to_formula(dnf, Cs, F) :-
   ( foreach(C, Cs), foreach(F, Fs) do
        list_to_functor_structure(',', C, F) ),
   list_to_functor_structure(';', Fs, F).


/* boolean_normal_form_prune(Cs1, Cs2) <-
      */

boolean_normal_form_prune(Cs1, Cs2) :-
   boolean_normal_form_prune_loop(Cs1, Cs3),
   sort(Cs3, Cs2).

boolean_normal_form_prune_loop([C1|Cs1], Cs2) :-
   member(A, C1),
   member(-A, C1),
   !,
   boolean_normal_form_prune_loop(Cs1, Cs2).
boolean_normal_form_prune_loop([C1|Cs1], [C2|Cs2]) :-
   sort(C1, C2),
   boolean_normal_form_prune_loop(Cs1, Cs2).
boolean_normal_form_prune_loop([], []).
   

/******************************************************************/


