

/******************************************************************/
/***                                                            ***/
/***          DDK:  Minimal Models                              ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(implications_to_minimal_models, 1) :-
   Program = [[e,c]-[b], [f,d]-[b], [b], []-[e,d]],
   implications_to_minimal_models(Program, _).
test(implications_to_minimal_models, 2) :-
   Program = [ [p(f(X)), p(g(X))]-[p(X)], [p(a)],
      []-[p(f(f(X)))],
      []-[p(g(g(X)))],
      []-[p(f(g(f(X))))],
      []-[p(g(f(g(X))))] ],
   implications_to_minimal_models(Program, _).

test(implications_to_minimal_models, graph_coloring(1)) :-
   dislog_variable_get(example_path,
      'graph_problems/graph_coloring', File),
   dread(lp, File, Program_1),
%  ground_transformation(Program_1, Program_2),
   Program_2 = Program_1,
   program_to_stable_models_dlv(Program_2, Models_1),
   coin_project_on_predicate(color/2, Models_1, Models_2),
   length(Models_1, N),
%  writeln_list(user, Models_2),
   dportray(chs, Models_2),
   writeln(user, N).

test(implications_to_minimal_models, graph_coloring(2)) :-
   dislog_variable_get(example_path,
      'graph_problems/graph_coloring', File),
   dread(lp, File, Program_1),
%  ground_transformation(Program_1, Program_2),
   Program_2 = Program_1,
   implications_to_minimal_models(Program_2, Models_1),
   coin_project_on_color(Models_1, Models_2),
   coin_to_tree(Models_2, Tree_2),
   set_tree_to_picture_bfs(Tree_2).

test(implications_to_minimal_models, ground_transformation) :-
   dislog_variable_get(example_path,
      'graph_problems/graph_coloring', File),
   dread(lp, File, Program_1),
   ground_transformation(Program_1, Program_2),
   dwrite(lp, Program_2),
   length(Program_1, N_1),
   length(Program_2, N_2),
   write_list(user, [
      '\nProgram_1 has ', N_1, ' rules.',
      '\nProgram_2 has ', N_2, ' rules.\n\n' ]).


/*** interface ****************************************************/


/* implications_to_minimal_models(Program, Models) <-
      */

implications_to_minimal_models(Program, Models) :-
   tree_tpi_fixpoint_iteration_non_delta(Program, [[]], Tree),
   tree_to_state(Tree, Models).


/* coin_project_on_color(Models_1, Models_2) <-
      */

coin_project_on_color(Models_1, Models_2) :-
   findall( Model_2,
      ( member(Model_1, Models_1),
        findall( X,
           ( member(color(N, C), Model_1),
             concat([N, =, C], X) ),
           Model_2 ) ),
      Models_2 ).


/******************************************************************/


