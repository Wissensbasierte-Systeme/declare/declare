

/******************************************************************/
/***                                                            ***/
/***          GXL:  GXL to XPCE                                 ***/
/***                                                            ***/
/******************************************************************/


:- module( gxl_to_xpce, [
      gxl_to_picture/1,
      gxl_to_picture/2,
      gxl_graph_to_picture/1,
      gxl_graph_to_new_picture/2 ] ).


/*** interface ****************************************************/


/* gxl_to_picture(+Gxl) <-
      */

gxl_to_picture(Gxl) :-
   gxl_to_picture(Gxl, _).


/* gxl_to_picture(+Gxl, ?Picture) <-
      */

gxl_to_picture(Gxl, Picture) :-
   gxl_to_picture(config:[]:[], Gxl, Picture),
   !.


/* gxl_graph_to_new_picture(+Gxl) <-
      */

gxl_graph_to_new_picture(Gxl) :-
   gxl_to_picture(Gxl, _).


/* gxl_graph_to_new_picture(+Gxl, -Picture) <-
      */

gxl_graph_to_new_picture(Gxl, Picture) :-
   gxl_to_picture(Gxl, Picture).


/* gxl_graph_to_picture(+Gxl) <-
      */

gxl_graph_to_picture(Gxl) :-
   gxl_to_picture(Gxl, _).


/* gxl_graph_to_picture(+Gxl, ?Picture) <-
      */

gxl_graph_to_picture(Gxl, Picture) :-
   xpce_picture_clear(Picture),
   gxl_to_picture(Gxl, Picture).


/* gxl_graph_to_picture_no_clear(+Gxl, ?Picture) <-
      */

gxl_graph_to_picture_no_clear(Gxl, Picture) :-
   gxl_to_picture(Gxl, Picture).


/******************************************************************/


