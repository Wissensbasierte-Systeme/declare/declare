

/******************************************************************/
/***                                                            ***/
/***          GXL:  Graph Layout Variable                       ***/
/***                                                            ***/
/******************************************************************/


:- module( gxl_graph_layout, [
      gxl_layout_variable_get/2,
      gxl_layout_variable_set/2,
      gxl_layout_variable_switch/3 ] ).

:- dynamic
      gxl_layout_variable/2.

gxl_layout_variable(tree_offset_x, 50).


/*** interface ****************************************************/


/* gxl_layout_variable_get(Variable, Value) <-
      */

gxl_layout_variable_get(Variable, Value) :-
   gxl_layout_variable(Variable, Value).


/* gxl_layout_variable_set(Variable, Value) <-
      */

gxl_layout_variable_set(Variable, Value) :-
   retractall(gxl_layout_variable(Variable, _)),
   asserta(gxl_layout_variable(Variable, Value)).


/* gxl_layout_variable_switch(Variable, Old, New) <-
      */

gxl_layout_variable_switch(Variable, Old, New) :-
   gxl_layout_variable_get(Variable, Old),
   gxl_layout_variable_set(Variable, New).


/******************************************************************/


