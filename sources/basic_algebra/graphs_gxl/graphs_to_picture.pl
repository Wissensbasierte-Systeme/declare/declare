

/******************************************************************/
/***                                                            ***/
/***          GXL:  Graphs to Picture                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* graph_edges_to_picture(+Classifier, +Edges) <-
      */

graph_edges_to_picture(Edges) :-
   graph_edges_to_picture(gxl_vertex_classify, Edges).


graph_edges_to_picture(Classifier, Edges) :-
   edges_to_vertices(Edges, Vertices),
   maplist( Classifier,
      Vertices, Vertices_2 ),
   vertices_edges_to_gxl_graph(Vertices_2, Edges, Graph),
   gxl_to_xpce:gxl_graph_to_new_picture(Graph, Picture),
   xpce_graph_layout(wielemaker, Picture),
   graph_picture_to_postscript(Picture),
   !.

graph_picture_to_postscript(Picture) :-
   dislog_variable_get(output_path, Path),
   concat(Path, 'graph_picture', File),
   postscript(Picture, File).


/* graph_edges_to_picture_bfs(+Classifier, +Edges, ?Picture) <-
      */

graph_edges_to_picture_bfs(Classifier, Edges) :-
   graph_edges_to_picture_bfs(Classifier, Edges, _).

graph_edges_to_picture_bfs(Classifier, Edges, Picture) :-
   edges_to_vertices(Edges, Vertices),
   maplist( Classifier,
      Vertices, Vertices_2 ),
%  writeln(user, (Vertices_2, Edges)),
   vertices_edges_to_picture_bfs(Vertices_2, Edges, Picture),
   !.


/* graph_edges_to_picture_bfs_inverted(+Classifier, +Edges) <-
      */

graph_edges_to_picture_bfs_inverted(Classifier, Edges) :-
   findall( Y-X,
      member(X-Y, Edges),
      Edges_2 ),
   graph_edges_to_picture_bfs(Classifier, Edges_2).


/* gxl_vertex_classify(+Vertex, -Gxl_Vertex) <-
      */

gxl_vertex_classify(Vertex, Gxl_Vertex) :-
   Gxl_Vertex = node:[id:Vertex]:[
      prmtr:[color:grey, symbol:circle, size:small]:[
         string:[bubble:'']:[Vertex] ] ].


/******************************************************************/


