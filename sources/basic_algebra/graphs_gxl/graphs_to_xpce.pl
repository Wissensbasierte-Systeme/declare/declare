

/******************************************************************/
/***                                                            ***/
/***            GXL:  Graphs to XPCE                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* edges_to_picture(+Edges, ?Picture) <-
      */

edges_to_picture(Edges) :-
   edges_to_picture(Edges, _).

edges_to_picture(Edges, Picture) :-
   edges_to_gxl(Edges, Gxl),
   gxl_to_picture(Gxl, Picture),
   xpce_graph_layout(xpce, Picture).


/* vertices_edges_to_picture(+Vertices, +Edges, ?Picture) <-
      */

vertices_edges_to_picture(Vertices, Edges) :-
   vertices_edges_to_picture(Vertices, Edges, _).

vertices_edges_to_picture(Vertices, Edges, Picture) :-
   vertices_edges_to_gxl(Vertices, Edges, Gxl),
   gxl_to_picture(Gxl, Picture),
   xpce_graph_layout(xpce, Picture).


/* vertices_edges_to_picture_dfs(Vertices, Edges, Picture) <-
      */

vertices_edges_to_picture_dfs(Vertices, Edges) :-
   vertices_edges_to_picture_dfs(Vertices, Edges, _).

vertices_edges_to_picture_dfs(Vertices, Edges, Picture) :-
   vertices_edges_to_picture(dfs, Vertices, Edges, Picture).


/* vertices_edges_to_picture(
         +Layout, +Vertices, +Edges, ?Picture) <-
      */

vertices_edges_to_picture(
      basic, Vertices, Edges, Picture) :-
   vertices_edges_to_picture(Vertices, Edges, Picture),
   !.
vertices_edges_to_picture(
      wielemaker, Vertices, Edges, Picture) :-
   vertices_edges_to_picture(Vertices, Edges, Picture),
   xpce_graph_layout(wielemaker, Picture),
   !.

vertices_edges_to_picture(bfs, Vertices, Edges, Picture) :-
   vertices_edges_to_picture_bfs(Vertices, Edges, Picture),
   !.
vertices_edges_to_picture(dfs, Vertices, Edges, Picture) :-
   Config = config:[]:[
      gxl_layout:[
         x_start:70, y_start:70,
         x_step:190,  y_step:50,  y_variance:10]:[]],
   vertices_edges_to_picture(
      dfs, Config, Vertices, Edges, Picture).

vertices_edges_to_picture(dfs, Config, Vertices, Edges, Picture) :-
   vertices_edges_to_possible_roots(Vertices, Edges, Roots),
   vertices_edges_to_gxl(Vertices, Edges, Gxl_1),
   gxl_graph_layout(Config, dfs, Roots, Gxl_1, Gxl_2),
%  tree_layout(dfs, Roots, Gxl, Gxl_2),
   gxl_to_picture_with_clear(Gxl_2, Picture),
%  dwrite(xml, Gxl_2),
   !.

vertices_edges_to_picture_with_roots(
      dfs, Roots, Vertices, Edges, Picture) :-
   Config = config:[]:[
      gxl_layout:[
         x_start:70, y_start:70,
         x_step:60,  y_step:70,  y_variance:10 ]:[]],
   vertices_edges_to_gxl(Vertices, Edges, Gxl_1),
   gxl_graph_layout(Config, dfs, Roots, Gxl_1, Gxl_2),
   gxl_to_picture_with_clear(Gxl_2, Picture).


/* vertices_edges_to_possible_roots(+Vertices, +Edges, -Roots) <-
      */

vertices_edges_to_possible_roots(Vertices, Edges, Roots) :-
   vertices_and_edges_to_possible_roots(Vertices, Edges, Roots),
%  writelnq_list(user, ['vertices:'|Vertices]),
%  writelnq_list(user, ['edges:'|Edges]),
%  writelnq_list(user, ['roots:'|Roots]),
   star_line.


/* edges_to_picture_bfs(+Edges, ?Picture) <-
      */

edges_to_picture_bfs(Edges) :-
   edges_to_picture_bfs(Edges, _).

edges_to_picture_bfs(Edges, Picture) :-
   edges_to_vertices(Edges, Vertices),
   vertices_edges_to_picture_bfs(Vertices, Edges, Picture).


/* vertices_edges_to_picture_bfs(+Vertices, +Edges) <-
      */

vertices_edges_to_picture_bfs(Vertices, Edges) :-
   vertices_edges_to_picture_bfs(Vertices, Edges, _).

vertices_edges_to_picture_bfs(Vertices, Edges, Picture) :-
   vertices_without_blanks(Vertices, Vertices_2),
   edges_without_blanks(Edges, Edges_2),
   vertices_edges_to_possible_roots(Vertices_2, Edges_2, Roots),
   vertices_edges_to_picture_bfs(Roots, Vertices_2, Edges_2, Picture).


/* vertices_without_blanks(Vertices_1, Vertices_2) <-
      */

vertices_without_blanks(Vertices_1, Vertices_2) :-
   maplist( vertex_without_blanks,
      Vertices_1, Vertices_2 ).

vertex_without_blanks(X, Y) :-
   ( atomic(X) -> name_remove_elements(" ", X, Y)
   ; X = Y ).


/* edges_without_blanks(Edges_1, Edges_2) <-
      */

edges_without_blanks(Edges_1, Edges_2) :-
   maplist( edge_without_blanks,
      Edges_1, Edges_2 ).

edge_without_blanks(V1-W1, V2-W2) :-
   vertex_without_blanks(V1, V2),
   vertex_without_blanks(W1, W2).


/* vertices_and_edges_to_gxl_to_picture(
         Config, Vertices, Edges, Picture) <-
      */

vertices_and_edges_to_gxl_to_picture(
      Config, Vertices, Edges, Picture) :-
   vertices_edges_to_possible_roots(Vertices, Edges, Roots),
   vertices_edges_to_gxl(Vertices, Edges, Gxl_1),
   gxl_graph_layout(Config, bfs, Roots, Gxl_1, Gxl_2),
   gxl_to_picture_with_clear(Gxl_2, Picture).


/* vertices_edges_to_picture_bfs(
         Roots, Vertices, Edges, Picture) <-
      */

vertices_edges_to_picture_bfs(Roots, Vertices, Edges, Picture) :-
   vertices_edges_to_gxl(Vertices, Edges, Gxl_1),
   Config = config:[]:[
      gxl_layout:[ y_variance:10,
%        x_start:70, y_start:70, x_step:50, y_step:50 ]:[] ],
         x_start:70, y_start:70, x_step:100, y_step:100 ]:[] ],
   gxl_graph_layout(Config, bfs, Roots, Gxl_1, Gxl_2),
%  Gxl_3 := Gxl_2/transform::'fng',
   dislog_variable_get(source_path,
      'applications/link_prediction/link_prediction_fng.pl', Fng),
   fn_item_transform_no_files(Fng, Gxl_2, Gxl_3),
%  dwrite(xml, Gxl_3),
   gxl_to_picture_with_clear(Gxl_3, Picture).


/* gxl_to_picture_with_clear(+Gxl, ?Picture) <-
      */

gxl_to_picture_with_clear(Gxl, Picture) :-
   ( var(Picture),
     gxl_to_xpce:gxl_graph_to_new_picture(Gxl, Picture)
   ; gxl_to_xpce:gxl_graph_to_picture(Gxl, Picture) ).


/******************************************************************/


