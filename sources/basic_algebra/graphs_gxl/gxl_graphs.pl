

/******************************************************************/
/***                                                            ***/
/***          GXL:  Graphs to GXL                               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* graph_edges_to_gxl_file(+Edges, +File) <-
      */

graph_edges_to_gxl_file(Edges, File) :-
   edges_to_vertices(Edges, Vertices),
   vertices_edges_to_gxl_graph(Vertices, Edges, Gxl),
   dwrite(xml, File, Gxl),
   !.


/* edges_to_gxl(Edges, Gxl) <-
      */

edges_to_gxl(Edges, Gxl) :-
   edges_to_vertices(Edges, Vertices),
   vertices_edges_to_gxl_graph(Vertices, Edges, Gxl).


/* vertices_edges_to_gxl(+Vertices, +Edges, -Gxl) <-
      */

vertices_edges_to_gxl(Vertices, Edges, Gxl) :-
   vertices_and_edges_to_gxl(Vertices, Edges, Gxl).


/* vertices_edges_to_gxl_graph(+Vertices, +Edges, -Gxl) <-
      */

vertices_edges_to_gxl_graph(Vertices, Edges, Gxl) :-
   vertices_and_edges_to_gxl(Vertices, Edges, Gxl).


/* gxl_to_vertices_edges(Gxl, Vertices, Edges) <-
      */

gxl_to_vertices_edges(Gxl, Vertices, Edges) :-
   gxl_to_vertices_and_edges(id-v_w, Gxl, Vertices, Edges).


/******************************************************************/


