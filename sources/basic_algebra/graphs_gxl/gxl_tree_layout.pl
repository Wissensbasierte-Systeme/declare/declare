

/******************************************************************/
/***                                                            ***/
/***          GXL:  Tree Layout                                 ***/
/***                                                            ***/
/******************************************************************/


:- module( gxl_tree_layout, [
      gxl_tree_layout/4,
      tree_layout/4,
      tree_layout/5 ] ).


 /*** interface ****************************************************/


/* gxl_tree_layout(+BFS_DFS, +Roots, +Gxl_1, -Gxl_2) <-
      */

tree_layout(BFS_DFS, Roots, Gxl_1, Gxl_2) :-
   gxl_tree_layout(BFS_DFS, Roots, Gxl_1, Gxl_2).

gxl_tree_layout(BFS_DFS, Roots, Gxl_1, Gxl_2) :-
   member(BFS_DFS, [bfs, dfs]),
   Config =
   config:[]:[
      gxl_layout:[
         x_start:70, y_start:70,
         x_step:50, y_step:50, y_variance:10]:[]],
   gxl_graph_layout(Config, BFS_DFS, Roots, Gxl_1, Gxl_2).


/* tree_layout(+BFS_DFS, +Roots, +Vertices, +Edges, -Gxl) <-
      */

tree_layout(BFS_DFS, Roots, Vertices, Edges, Gxl_2) :-
   vertices_and_edges_to_gxl(Vertices, Edges, Gxl_1),
   tree_layout(BFS_DFS, Roots, Gxl_1, Gxl_2).


/*** implementation ***********************************************/


gxl_graphs_tests_sub(Vertices, Edges, [a, e]) :-
   Vertices = [ a, b, c, d, e, f, g, h, i, j, k, l, m, n, r ],
   Edges = [ a-b, a-d, a-f, b-c, b-d, b-f,
      c-a, c-i, c-j, d-c, d-k, f-g, f-h,
      g-n, g-a, g-r, g-f, i-m, i-n, i-a,
      k-l, k-d, k-h, k-b, m-r ].


/******************************************************************/


