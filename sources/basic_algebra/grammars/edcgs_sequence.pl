

/******************************************************************/
/***                                                            ***/
/***       EDCG:  Sequence Operator, Meta-Call Predicates       ***/
/***                                                            ***/
/******************************************************************/


:- module( edcgs_sequence, [
      sequence/5 ] ).

:- module_transparent(sequence/5).
:- module_transparent(apply_sequence/2).


/*** interface ****************************************************/


/* sequence('+', ...) ->
      Apply Predicate at most one,
      before returning the result. */

sequence('?', Predicate, [A], Xs, Ys) :-
   apply_sequence(Predicate, [A, Xs, Ys]).
sequence('?', _, [], Xs, Xs).

/* sequence('+', ...) ->
      Apply Predicate at least one,
      before returning the first result.
      Return further results on backtracking. */

sequence('+', Predicate, [A|As], Xs, Ys) :-
   apply_sequence(Predicate, [A, Xs, Zs]),
   sequence('*', Predicate, As, Zs, Ys).

/* sequence('*', ...) ->
      Apply Predicate as little as possible,
      before returning the first result.
      Return further results on backtracking. */

sequence('*', _, [], Xs, Xs).
sequence('*', Predicate, [A|As], Xs, Ys) :-
   apply_sequence(Predicate, [A, Xs, Zs]),
   sequence('*', Predicate, As, Zs, Ys).

/* sequence('**', ...) ->
      Apply Predicate as many times as possible,
      before returning the first result.
      Return further results on backtracking. */

sequence('**', Predicate, [A|As], Xs, Ys) :-
   apply_sequence(Predicate, [A, Xs, Zs]),
   sequence('**', Predicate, As, Zs, Ys).
sequence('**', _, [], Xs, Xs).


/*** implementation ***********************************************/


% helper predicate

apply_sequence(Predicate, [As, Xs, Ys]) :-
   copy_term(Predicate, P),
   user:term_expansion((head==>P), [Rule]),
   Rule = ( head([head:As], Xs, Ys) :- Body ),
   call(Body).


/******************************************************************/


