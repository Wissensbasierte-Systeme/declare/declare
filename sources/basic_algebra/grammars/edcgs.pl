

/******************************************************************/
/***                                                            ***/
/***          DDK:  DCG Abstraction                             ***/
/***                                                            ***/
/******************************************************************/


:- op(1200, xfx, ==>).


:- dislog_variable_set(edcg_expansion_mode, xml).


/*** interface ****************************************************/


/* term_expansion(X1==>Y1, [Rule]) <-
      */

term_expansion(X1==>Y1, [Rule]) :-
   edcg_rule_to_dcg_rule(X1==>Y1, X2-->Y2),
   expand_term(X2-->Y2, Rule).

   
/*** implementation ***********************************************/


/* edcg_rules_to_dcg_rules <-
      */

edcg_rules_to_dcg_rules :-
   forall( X1==>Y1,
      ( edcg_rule_to_dcg_rule(X1==>Y1, X2-->Y2),
        expand_term(X2-->Y2, Rule),
        assert(Rule) ) ).


/* edcg_rule_to_dcg_rule(X1==>Y1, X2-->Y2) <-
      */

edcg_rule_to_dcg_rule(X1==>Y1, X2-->Y2) :-
   edcg_formula_to_dcg_formula(Y1, Y2, Args),
   X1 =.. As1,
   ( dislog_variable_get(edcg_expansion_mode, list),
     append(As1, [Args], As2)
   ; dislog_variable_get(edcg_expansion_mode, xml),
     functor(X1, T, _),
     append(As1, [[T:Args]], As2) ),
   X2 =.. As2.


/* edcg_formula_to_dcg_formula(X1, X2, Args) <-
      */

edcg_formula_to_dcg_formula(X1, X2, V) :-
   X1 = V^X,
   !,
   edcg_formula_to_dcg_formula(X, X2, V).
edcg_formula_to_dcg_formula(X1, X2, V) :-
   X1 = sequence(_,_),
   !,
   add_variable_to_atom(Vs, X1, X),
   X2 = (X, {append(Vs, V)}).
edcg_formula_to_dcg_formula(X1, X2, V) :-
   X1 = {X},
   !,
   X2 = {X, (V = [])}.
edcg_formula_to_dcg_formula(X1, X2, V) :-
   X1 = !,
   !,
   X2 = (!, {V = []}).
edcg_formula_to_dcg_formula(X1, X2, V) :-
   X1 = (_,_),
   !,
   comma_structure_to_list(X1, Xs1),
   !,
   ( foreach(A, Xs1), foreach(B, Xs2), foreach(C, Vs) do
        edcg_formula_to_dcg_formula(A, B, C) ),
   list_to_comma_structure(Xs2, X),
   X2 = (X, {append(Vs, V)}),
   !.
edcg_formula_to_dcg_formula(X1, X2, V) :-
   X1 = (_;_),
   !,
   semicolon_structure_to_list(X1, Xs1),
   maplist( edcg_formula_to_dcg_formula_(V),
      Xs1, Xs2 ),
   list_to_semicolon_structure(Xs2, X2),
   !.
edcg_formula_to_dcg_formula(X1, X2, V) :-
   add_variable_to_atom(V, X1, X2).

edcg_formula_to_dcg_formula_(Vs, Y1, Y2) :-
   edcg_formula_to_dcg_formula(Y1, Y2, Vs).


/* add_variable_to_atoms(V, Xs1, Xs2) <-
      */

add_variable_to_atoms(V, [X1|Xs1], [X2|Xs2]) :-
   add_variable_to_atom(V, X1, X2),
   add_variable_to_atoms(V, Xs1, Xs2).
add_variable_to_atoms(_, [], []).


/* add_variable_to_atom(V, X1, X2) <-
      */

add_variable_to_atom(V, X1, X2) :-
   is_list(X1),
   !,
   X2 =.. ['$edcg_append', X1, V].
add_variable_to_atom(V, X1, X2) :-
   X1 =.. As1,
   append(As1, [V], As2),
   X2 =.. As2.


/* '$edcg_append'(Xs, V, Ys, Zs) <-
      */

'$edcg_append'(Xs, V, Ys, Zs) :-
   append(Xs, Zs, Ys),
   Xs = V.


/******************************************************************/


