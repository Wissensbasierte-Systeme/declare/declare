

/*** example ******************************************************/


sentence ==> subject, predicate, object.

% becomes
% sentence([S, P, O]) --> subject(S), predicate(P), object(O).

subject(X) -->
   [X].
predicate(X) -->
   [X].
object(X) -->
   [X].

a ==> (b, (c;d)).

% becomes
% a([X,Y]) -->
%    b(X),
%    ( c(Y)
%    ; d(Y) ).

% impossible: a ==> (b; (c,d)).


% aa -->
%    b, \+ (c; d).


/******************************************************************/


