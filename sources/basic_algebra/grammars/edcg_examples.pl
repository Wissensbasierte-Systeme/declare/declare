

/******************************************************************/
/***                                                            ***/
/***             EDCGS:  Examples                               ***/
/***                                                            ***/
/******************************************************************/



/******************************************************************/


test(edcgs, 1) :-
   sentence(X, [the, boy, eats, the, apple], []),
%  phrase(sentence(X), [the, boy, eats, the, apple]),
   writeln(user, X),
   dwrite(xml, X).

sentence ==> noun_phrase, verb_phrase.
noun_phrase ==> determiner, noun.
verb_phrase ==> ( verb ; verb, noun_phrase ).
noun ==> [boy] ; [boys] ; [apple] ; [apples].
determiner ==> [the].
verb ==> [eat] ; [eats].


/******************************************************************/


test(edcgs, 2) :-
   atom_chars(abcd, Cs),
   e([X], Cs, []),
   dwrite(xml, X).

e ==>
   a,
   sequence('*', (b;c)),
   sequence('*', c),
   d.

a ==> [a].
b ==> [b].
c ==> [c].
d ==> [d].


/******************************************************************/


test(edcgs, 3) :-
   a(X, [c, e, x, y, d, d], []),
   writeln(user, X),
   dwrite(xml, X).


a ==>
   ( (b, { mge(b) })
   ; (c, e, { mge(ce) }) ),
   !,
   [x, y], sequence(+, d).

b([b(1)]) --> [b].
c([c]) --> [c].
d([d]) --> [d].
e([e(2)]) --> [e].

x --> !.

mge(X) :-
   writeln(user, X).

f ==>
   V^g, W^h,
   { [V2] = V, [W2] = W,
     [A] := V2/content::'*', [B] := W2/content::'*', A > B }.

g ==>
   [X],
   { member(X, [1, 2, 3]) }.
h ==>
   [X],
   { member(X, [1, 2, 3]) }.


/******************************************************************/


