

/******************************************************************/
/***                                                            ***/
/***           Declare:  Herbrand Interpretations               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xpce_display_herbrand_interpretation(Title, Interpretation) <-
      */

xpce_display_herbrand_interpretation(Interpretation) :-
   Title = 'Interpretation',
   xpce_display_herbrand_interpretation(Title, Interpretation).

xpce_display_herbrand_interpretation(Title, Interpretation) :-
   length(Interpretation, N),
   ( N =< 1000 ->
     ( herbrand_interpretation_to_tuples(Interpretation, Tuples),
       xpce_display_table(Title, ['Predicate', 'Tuples'], Tuples) )
   ; write_list(user, [N, ' facts\n']) ).
%  xpce_display_table_2(Title, ['Predicate', 'Tuples'], Tuples).


/* xxul_dislay_table(Attributes, Tuples) <-
      */

xxul_dislay_herbrand_interpretation(Interpretation) :-
   herbrand_interpretation_to_tuples(Interpretation, Tuples),
   xxul_dislay_table(['Predicate', 'Tuples'], Tuples).

xxul_dislay_table(Attributes, Tuples) :-
   dislog_variable_get(morpheme_term_table, XXul_Spreadsheet),
   send(XXul_Spreadsheet, clear),
   xxul_morpheme_table_attributes(XXul_Spreadsheet, Attributes),
   forall( member(Tuple, Tuples),
      xxul_morpheme_table_tuple(XXul_Spreadsheet, Tuple) ).


/* herbrand_interpretation_to_tuples(Interpretation, Tuples) <-
      */

herbrand_interpretation_to_tuples(Interpretation, Tuples) :-
   ddbase_aggregate( [Predicate, tuples_to_atom(Tuple)],
      ( member(Atom, Interpretation),
        Atom =.. [P|Arguments],
        functor(Atom, P, Arity),
        term_to_atom(P/Arity, Predicate),
        list_to_tuple_atom(Arguments, Tuple) ),
      Tuples ). 

tuples_to_atom(Tuples, Atom) :-
   ( dislog_variable_get(tuples_to_atom, N) ->
     true
   ; N = 3 ),
   list_to_multiple_line_atom(N, Tuples, Atom).
%  names_append_with_separator(Tuples, ', ', Atom).


/******************************************************************/


