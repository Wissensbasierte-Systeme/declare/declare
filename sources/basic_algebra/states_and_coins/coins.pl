

/******************************************************************/
/***                                                            ***/
/***         DisLog:  Basic Operations on Coins                 ***/
/***                                                            ***/
/******************************************************************/
 

/* "Coin": 
      A coin is a collection of Herbrand interpretations.
      E.g. the minimal Herbrand models of a program form a 
      coin. */


/*** interface ****************************************************/


/* state_operator_disjunctive(Coin,State) <-
      Given a coin Coin, the set State of all positive disjunctions 
      that are true in all Herbrand interpretations in Coin is 
      computed. */
 
state_operator_disjunctive(Coin,State) :-
   tree_based_boolean_dualization(Coin,State).

state_operator(Coin,State) :-
   tree_based_boolean_dualization(Coin,State).


/* state_operator_negative_s(Program,Coin,Negative_State) <-
      Given a DNLP Program and a coin Coin, the set Negative_State
      of all negative disjunctions that are true in all Herbrand 
      interpretations in Coin is computed. */
 
state_operator_negative_s(Program,Coin,Negative_State) :-
   tree_based_boolean_dualization(Coin,Disjunctive_State),
   state_to_gcwa_egcwa(Program,Disjunctive_State,Gcwa_Egcwa),
   negate_state(Gcwa_Egcwa,Negative_State), !.
 
state_operator_negative_i(Program,Coin,Negative_State) :-
   tree_based_coin_to_gcwa_egcwa(Program,Coin,Gcwa_Egcwa),
   negate_state(Gcwa_Egcwa,Negative_State), !.
 

/* state_operator_general(Program,Coin,General_State) <-
      Given a DNLP Program and a coin Coin, the set General_State
      of all general disjunctions that are true in all Herbrand 
      interpretations in Coin is computed. */

state_operator_general(Program,Coin,General_State) :-
   ord_union(Coin,Atoms),
   state_operator_general_non_atomary(Atoms,Coin,State_1),
   dnlp_atoms_to_gcwa(Program,Atoms,Gcwa_State),
   negate_state(Gcwa_State,State_2),
   ord_union(State_2,State_1,General_State), !.
 
state_operator_general_2(Program,Coin,General_State) :-
   dnlp_to_herbrand_base(Program,Atoms),
   state_operator_general_non_atomary(Atoms,Coin,General_State), !.

state_operator_general_non_atomary(Atoms,Coin,General_State) :-
   coin_complete(Atoms,Coin,Completions),
   tree_based_boolean_dualization(Completions,Dual_State),
   state_remove_tautologies(Dual_State,General_State).


/* state_to_coin(State,Coin) <-
      Given a disjunctive Herbrand state State, the coin Coin of
      all minimal Herbrand models of State is computed. */

coin_to_state(Coin,State) :-
   state_to_coin(Coin,State).

state_to_coin(State,Coin) :-
   boolean_dualization(State,Coin).

tree_based_state_to_coin(State,Coin) :-
   tree_based_boolean_dualization(State,Coin).


/* state_to_coin_to_egcwa(State,Egcwa) <-
      Given a disjunctive Herbrand state State, the set Egcwa of 
      all conjunctions which are false in all minimal Herbrand
      models of State is computed. */

tree_based_state_to_coin_to_egcwa(Model_State,Egcwa) :-
   tree_based_boolean_dualization(Model_State,Minimal_Models),
   tree_based_coin_to_egcwa(Minimal_Models,Egcwa).
 
state_to_coin_to_egcwa(State,Egcwa) :-
   state_to_coin(State,Minimal_Models),
   coin_to_egcwa(Minimal_Models,Egcwa).
 

/* coin_to_egcwa(Coin,Egcwa) <-
      Given a coin Coin, the set Egcwa of all conjunctions 
      which are false in all interpretations in Coin is computed. */
 
tree_based_coin_to_egcwa(Coin,Egcwa) :-
   announcement('tree_based_coin_to_egcwa'),
   start_timer(tree_based_coin_to_egcwa),
   ord_union(Coin,Atoms),
   ord_subtract_loop(Atoms,Coin,Complementary_Coin),
   tree_based_boolean_dualization(Complementary_Coin,Egcwa),
   write('total time   '),
   stop_timer(tree_based_coin_to_egcwa).

coin_to_egcwa(Coin,Egcwa) :-
   announcement('coin_to_egcwa'),
   start_timer(coin_to_egcwa),
   ord_union(Coin,Atoms),
   ord_subtract_loop(Atoms,Coin,Complementary_Coin),
   boolean_dualization(Complementary_Coin,Egcwa),
   write('total time   '),
   stop_timer(coin_to_egcwa).
 
ord_subtract_loop(Set1,[Set2|Sets2],[Set3|Sets3]) :-
   ord_subtract(Set1,Set2,Set3),
   ord_subtract_loop(Set1,Sets2,Sets3).
ord_subtract_loop(_,[],[]).


/* coin_to_gcwa(Program,Coin,Gcwa) <-
      Given a DNLP Program and a coin Coin, the set Gcwa of all atoms
      that are false in all interpretations in Coin is computed. */
 
coin_to_gcwa(Program,Coin,Gcwa) :-
   state_or_coin_to_gcwa(Program,Coin,Gcwa).
 
state_or_coin_to_gcwa(Program,Sets,Gcwa) :-
   ord_union(Sets,Atoms),
   dnlp_atoms_to_gcwa(Program,Atoms,Gcwa).
 
dnlp_atoms_to_gcwa(Program,Atoms,Gcwa) :-
   dnlp_to_herbrand_base(Program,Herbrand_Base),
   ord_subtract(Herbrand_Base,Atoms,Gcwa_Atoms),
   list_of_elements_to_relation(Gcwa_Atoms,Gcwa).
 

/* coin_complete(Atoms,Coin1,Coin2) <-
      Given a coin Coin1, every interpretation I in Coin1 is
      completed by adding a negative literal ~A for every atom
      A in Atoms that is not in I. */
 
coin_complete(Atoms,[I1|Is1],[I2|Is2]) :-
   ord_subtract(Atoms,I1,Complement),
   negate_general_clause(Complement,Negated_Complement),
   ord_union(I1,Negated_Complement,I2),
   coin_complete(Atoms,Is1,Is2). 
coin_complete(_,[],[]).


/* coin_can_wrt_ordering(Ordering,Coin1,Coin2) <-
      Given a partial coin Coin1 and an ordering Ordering on
      partial Herbrand interpretation, the partial coin Coin2
      containing all minimal pertial Herbrand interpretations
      w.r.t. Ordering is computed. */

coin_can_wrt_ordering(Ordering,Coin1,Coin2) :-
   coin_can_wrt_ordering(Ordering,Coin1,Coin1,[],Coin2).
 
coin_can_wrt_ordering(Ordering,Coin,[Int|Coin1],Sofar,Coin2) :-
   minimal_interpretation_wrt_ordering(Ordering,Int,Coin),
   !,
   New_Sofar = [Int|Sofar],
   coin_can_wrt_ordering(Ordering,Coin,Coin1,New_Sofar,Coin2).
coin_can_wrt_ordering(Ordering,Coin,[_|Coin1],Sofar,Coin2) :-
   coin_can_wrt_ordering(Ordering,Coin,Coin1,Sofar,Coin2).
coin_can_wrt_ordering(_,_,[],Coin,Coin).
 
minimal_interpretation_wrt_ordering(Ordering,Int,Coin) :-
   \+ ( member(I,Coin), I \== Int,   
        Goal =.. [Ordering,I,Int],
        call(Goal) ).


/* coin_project_on_predicate(
         Predicate/Arity, Models_1, Models_2) <-
      */

coin_project_on_predicate(
      Predicate/Arity, Models_1, Models_2) :-
   findall( Model_2,
      ( member(Model_1, Models_1),
        findall( Atom,
           ( member(Atom, Model_1),
             functor(Atom, Predicate, Arity) ),
           Model_2 ) ),
      Models_2 ).


/******************************************************************/

