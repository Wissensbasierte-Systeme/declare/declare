

/******************************************************************/
/***                                                            ***/
/***      DisLog:  States and the Prolog Databbase              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* state_atoms_assert(State, Predicate/Arity) <-
      */

state_atoms_assert(State, Predicate/Arity) :-
   n_free_variables(Arity, Variables),
   Atom =.. [Predicate|Variables],
   retractall(Atom),
   forall( member([Atom], State),
      assert(Atom) ).


/* state_atoms_collect(Predicate/Arity, State) <-
      */

state_atoms_collect(Predicate/Arity, State) :-
   n_free_variables(Arity, Variables),
   Atom =.. [Predicate|Variables],
   findall( [Atom],
      Atom,
      State ).


/******************************************************************/


