

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Boolean Dualization                   ***/
/***                                                            ***/ 
/******************************************************************/ 
 


/*** interface ****************************************************/


/* tree_based_boolean_dualization(State1,State2) <-
      Given a d-state or c-state State1, the equivalent c-state 
      State1 or d-state State2, respectively, is computed. */

tree_based_boolean_dualization(Normal_Form,Dual_Normal_Form) :-
   writeln('tree_based_boolean_dualization'),
   start_timer(tree_based_boolean_dualization),
   state_to_tree(Normal_Form,Tree),
%  pp_tree(Tree,0),
   tree_to_models(Tree,Form_1), length(Form_1,I1),
   state_prune(Form_1,Form_2,I2),
   state_can(Form_2,Form_3,I3),
   state_prune(Form_3,Dual_Normal_Form,I4),
   pp_numbers([I1,I2,I3,I4]),
%  writeln(Dual_Normal_Form),
   write('dualization  '),
   stop_timer(tree_based_boolean_dualization),
   !.


/* model_tree_to_clause_tree(Tree1,Tree2) <-
      Given a model tree Tree1 representing a set of models,
      a clause tree Tree2 representing the set of disjunctions
      which are implied logically by all models constructed. */

model_tree_to_clause_tree(Tree1,Tree2) :-
%  write('mtree2ctree  '), ttyflush,
%  start_timer(mtree2ctree),
   tree_to_models(Tree1,State1),
   state_prune(State1,State2),
   state_can(State2,State3),
   state_to_tree(State3,Tree2).
%  stop_timer(mtree2ctree).


/* clause_tree_to_model_tree(Tree1,Tree2) <-
      Given a clause tree Tree1 representing a disjunctive state State,
      a model tree Tree2 representing the set Models of minimal models 
      of State is constructed. */

clause_tree_to_model_tree(Tree1,Tree2) :-
   tree_to_models(Tree1,State1),
   state_prune(State1,State2),
   state_can(State2,State3),
   state_to_tree(State3,Tree2).


/* state_to_model_tree(State,Model_Tree) <-
      A model tree Tree2 representing the set of minimal models 
      of a given disjunctive state State is constructed. */

state_to_model_tree(State,Model_Tree) :-
   state_to_tree(State,Clause_Tree),
   clause_tree_to_model_tree(Clause_Tree,Model_Tree).


/* boolean_dualization(State1,State2) <-
      Given a d-state or c-state State1, the equivalent c-state State1
      or d-state State2, respectively, is computed. */

boolean_dualization(Normal_Form,Dual_Normal_Form) :-
   writeln('boolean_dualization'),
   start_timer(boolean_dualization),
   cartesian_product(Normal_Form,Form_1), length(Form_1,I1),
   prune_states(Form_1,Form_2),
   state_prune(Form_2,Form_3,I2),
   state_can(Form_3,Form_4,I3),
   state_prune(Form_4,Dual_Normal_Form,I4),
   pp_numbers([I1,I2,I3,I4]),
   write('dualization  '),
   stop_timer(boolean_dualization).


/* tree_to_models(Tree,Minimal_Models) <-
      Given a clause tree Tree representing a d-state State, the set 
      Minimal_Models of all minimal models of State is computed. */

tree_to_models(Tree,Minimal_Models) :-
   write('tree_models  '), ttyflush,
   start_timer(tree_to_models),
   tree_to_c_facts(Tree,Minimal_Models),
   stop_timer(tree_to_models).
 

/* tree_to_dual_state(Tree,Dual_State) <-
      Given a clause/model tree Tree representing a d-/c-state
      State, the dual c-/d-state Dual_State is computed. */

tree_to_dual_state([Node],State) :-
   !,
   list_of_elements_to_relation(Node,State).
tree_to_dual_state([Node1,[[Node2]|Trees]],State) :-
   !,
   ord_union(Node1,Node2,Node),
   tree_to_dual_state([Node|Trees],State).
tree_to_dual_state([Node,Tree1,Tree2|Trees],State) :-
   list_of_elements_to_relation(Node,State1),
   tree_to_dual_state(Tree1,State2),
   state_trees_to_dual_state(State2,[Tree2|Trees],State3),
   ord_union(State1,State3,State).

state_trees_to_dual_state(State1,[Tree|Trees],State5) :-
   !,
   tree_to_dual_state(Tree,State),
   state_disjunction(State1,State,State2),
   state_prune(State2,State3),
   state_can(State3,State4),
   state_trees_to_dual_state(State4,Trees,State5).
state_trees_to_dual_state(State,[],State).


/*** implementation ***********************************************/


/* prune_states(States,Pruned_States) <-
      Given a set States of sets S,
      the set Pruned_States of sets S' is computed,
      where S' is obtained from S by duplicate elimination. */

prune_states([State|States],[Pruned_State|Pruned_States]) :-
   state_prune(State,Pruned_State),
   prune_states(States,Pruned_States).
prune_states([],[]).
 

/* tree_to_c_facts(Tree,Minimal_Models) <-
      Given a clause tree Tree representing a d-state State, the set
      Minimal_Models of all minimal models of State is computed. */

tree_to_c_facts([],[[]]) :-
   !.
tree_to_c_facts(Tree,C_Facts) :-
   findall( C_Fact,
      tree_to_c_fact(Tree,C_Fact),
      C_Facts ).
 
tree_to_c_fact([Clause|_],[Atom]) :-
   member(Atom,Clause).
tree_to_c_fact([_|Trees],C_Fact) :-
   trees_to_c_fact(Trees,C_Fact).
   
trees_to_c_fact([Tree|Trees],C_Fact) :-
   tree_to_c_fact(Tree,C_Fact_1),
   trees_to_c_fact(Trees,C_Fact_2),
   ord_union(C_Fact_1,C_Fact_2,C_Fact).
trees_to_c_fact([Tree],C_Fact) :-
   tree_to_c_fact(Tree,C_Fact).


/******************************************************************/


