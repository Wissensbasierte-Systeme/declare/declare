

/******************************************************************/
/***                                                            ***/
/***      DisLog:  Basic Operations on Herbrand States          ***/
/***                                                            ***/
/******************************************************************/


:- dynamic 
      dn_fact/1.


/*** interface ****************************************************/


/* state_prune(State1,State2) <-
      removes duplicates in the list State1, the result is 
      the disjunctive Herbrand state State2. */

state_prune(State1,State2) :-
   list_to_ord_set(State1,State2).


/* state_prune(State1,State2,N) <-
      removes duplicates in the list State1, the result is 
      the disjunctive Herbrand state State2,
      and N is the number of clauses in State2. */

state_prune(State1,State2,N) :-
   write('pruning      '), ttyflush,
   start_timer(state_prune),
   list_to_ord_set(State1,State2),
   length(State2,N),
   stop_timer(state_prune).


/* lists_minimize(Lists_1, Lists_2) <-
      */

lists_minimize(Lists_1, Lists_2) :-
   maplist( sort,
      Lists_1, Lists ),
   state_can(Lists, Lists_2).


/* state_can(State1,State2) <-
      the Herbrand state State2 is the canonical form of
      the Herbrand state State1. */

state_can(State1,State3) :-
   state_can(State1,State2,_),
   reverse(State2,State3).


/* state_can(State1,State2,N) <-
      the Herbrand state State2 is the canonical form of
      the Herbrand state State1,
      and N is the number of clauses in State2. */
 
state_can(State1,State2,N) :-
   write('can          '), ttyflush,
   start_timer(state_can),
   remove_implied_new_facts(State1,State2),
   length(State2,N),
   stop_timer(state_can).


/* state_subsumes(State1,State2) <-
      is true when the Herbrand state State1
      subsumes the Herbrand state State2. */

state_subsumes(Cs1,[C2|Cs2]) :-
   member(C1,Cs1), ord_subset(C1,C2),
   state_subsumes(Cs1,Cs2).
state_subsumes(_,[]) :-
   !.


/* state_supersumes_clause(State,Clause) <-
      generates instantiations of Clause, which are supersumed by
      the state State. */

state_supersumes_clause(State,Clause) :-
   member(Clause1,State),
   clause_supersumes_clause(Clause1,Clause).
state_supersumes_clause(_,[]).


/* clause_supersumes_clause(Clause1,Clause2) <-
      generates instantiations of Clause2, which are supersumed by
      the ground clause Clause1. */

clause_supersumes_clause(Clause,[A|As]) :-
   resolve(A,Clause,_),
   clause_supersumes_clause(Clause,As).
clause_supersumes_clause(_,[]).


/* clause_subsumes_some_clause(Clause,State) <-
      the clause Clause subsumes some clause in the state State. */

clause_subsumes_some_clause(C1,[C2|_]) :-
   subset(C1,C2),
   !.
clause_subsumes_some_clause(C1,[_|Cs2]) :-
   clause_subsumes_some_clause(C1,Cs2).


/* state_disjunction(S1,S2,S3) <-
      the state S3 = S1 v S2 is the state disjunction of the
      states S1 and S2. */

state_disjunction(S1,S2,S3) :-
   state_disjunction_basic(S1,S2,S3).

state_disjunction([State1,State2],State3) :-
   state_disjunction(State1,State2,State3).
state_disjunction([State1|States1],State3) :-
   state_disjunction(States1,State2),
   state_disjunction(State1,State2,State3).
state_disjunction([],[[]]).


/* state_disjunction_optimized(S1,S2,S3) <-
      optimized version,
      computes a state S3 which is equivalent to the set
      disjunction S1 v S2 of the states S1 and S2,
      S3 is a multiset. */
 
state_disjunction_optimized(S1,S2,S3) :-
   ord_intersection(S1,S2,S12),
   ord_subtract(S1,S12,S13), ord_subtract(S2,S12,S23),
   state_disjunction_basic(S13,S23,S),
   state_subtract(S,S12,Delta_S),
   append(S12,Delta_S,S3).

 
/* state_disjunction_basic(S1,S2,S3) <-
      computes the set disjunction S3 = S1 v S2 of the states
      S1 and S2 as a multiset. */
 
state_disjunction_basic(S1,S2,S3) :-
   write('product      '),
   start_timer(product_of_list),
   state_disjunction_basic(S1,S2,[],S3),
   add_timer(product_of_list),
   stop_timer(product_of_list).

state_disjunction_basic([C1|Cs1],Cs2,Sofar,Cs4) :-
   state_disjunction_basic_loop(C1,Cs2,Cs3),
   append(Cs3,Sofar,New_Sofar),
   state_disjunction_basic(Cs1,Cs2,New_Sofar,Cs4).
state_disjunction_basic([],_,Cs,Cs).

state_disjunction_basic_loop(C1,[C2|Cs2],[C3|Cs3]) :-
   ord_union(C1,C2,C3),
   state_disjunction_basic_loop(C1,Cs2,Cs3).
state_disjunction_basic_loop(_,[],[]).


/* state_resolve(State1,Atom,State2) <-
      the disjunctive Herbrand state State2 contains the resolvents
      of the disjunctive clauses in State1 with the atom Atom. */

state_resolve([C|Cs],Atom,[R|Rs]) :-
   resolve(Atom,C,R), 
   !,
   state_resolve(Cs,Atom,Rs).
state_resolve([_|Cs],Atom,Rs) :-
   state_resolve(Cs,Atom,Rs).
state_resolve([],_,[]).


/* state_reduce(State1,Atom,State2) <-
      the disjunctive Herbrand state State2 contains those clauses
      in State1 which do not resolve with the atom Atom. */ 
 
state_reduce([C|Cs],Atom,Rs) :-
   resolve(Atom,C,_), 
   !,
   state_reduce(Cs,Atom,Rs).
state_reduce([C|Cs],Atom,[C|Rs]) :-
   state_reduce(Cs,Atom,Rs).
state_reduce([],_,[]).


/* state_reduce2(State1,Atoms,State2) <-
      the disjunctive Herbrand state State2 contains those clauses
      in State1 which do not contain an atom of Atoms. */

state_reduce2([C|Cs],Atoms,[C|Rs]) :-
   ord_disjoint(C,Atoms),
   !,
   state_reduce2(Cs,Atoms,Rs).
state_reduce2([_|Cs],Atoms,Rs) :-
   state_reduce2(Cs,Atoms,Rs).
state_reduce2([],_,[]).


/* state_union(State1,State2,State3) <-
      computes in State3 the union of the two states State1 and 
      State2. */
 
state_union(State1,State2,State3) :-
   ord_union(State1,State2,State3).

 
/* state_subtract(State1,State2,State3) <-
      computes in State3 all facts from State1 which are
      not subsumed by facts from State2. */
 
state_subtract(State1,State2,State3) :-
   write('delta hs     '), ttyflush,
   start_timer(state_subtract),
   state_subtract_loop(State1,State2,State3),
   stop_timer(state_subtract).

state_subtract_loop([],_,[]).
state_subtract_loop([C1|Cs1],Cs,Cs2) :-
   member(C2,Cs), ord_subset(C2,C1),
   !,
   state_subtract_loop(Cs1,Cs,Cs2).
state_subtract_loop([C1|Cs1],Cs,[C1|Cs2]) :-
   state_subtract_loop(Cs1,Cs,Cs2).
  

/* state_supertract(State1,State2,State3) <-
      State3 contains those clauses form State1, which are not
      supersumed by State2. */
 
state_supertract(State1,State2,State3) :-
   write('supertract   '), ttyflush,
   start_timer(state_supertract),
   state_supertract_loop(State1,State2,[],State3),
   stop_timer(state_supertract).
 
state_supertract_loop([C|Cs],State1,Sofar2,State2) :-
   state_supersumes_clause(State1,C),
   !,
   state_supertract_loop(Cs,State1,Sofar2,State2).
state_supertract_loop([C|Cs],State1,Sofar2,State2) :-
   state_supertract_loop(Cs,State1,[C|Sofar2],State2).
state_supertract_loop([],_,Sofar2,State2) :-
   list_to_ord_set(Sofar2,State2).
 

state_tree_subtract(State1,Tree,State2) :-
   write('tree delta   '), ttyflush,
   start_timer(state_tree_subtract),
   retractall(delta_d_fact(_)),
   tree_delta_facts(Tree,State1,_),
   collect_arguments(delta_d_fact,State2),
   retractall(delta_d_fact(_)),
   stop_timer(state_tree_subtract).


/* state_models_subtract(State1,Models,State2) <-
      given a g-state State1, a g-state State2 containing all
      disjunctions of State1 which are false in some model of
      Models is computed. */

state_models_subtract([C|Cs1],Models,[C|Cs2]) :-
   member(Model,Models),
   \+ disjunction_true_in_model(Model,C),
   !,
   state_models_subtract(Cs1,Models,Cs2).
state_models_subtract([_|Cs1],Models,Cs2) :-
   state_models_subtract(Cs1,Models,Cs2).
state_models_subtract([],_,[]).


/* state_to_atoms(State,Atoms) <-
      returns the set Atoms = atoms(State) of all atoms
      which occur in a clause of the Herbrand state State. */
 
state_to_atoms(State,Atoms) :-
   ord_union(State,Atoms).


state_remove_tautologies(State1,State2) :-
   state_remove_tautologies(State1,[],State2).

state_remove_tautologies([C1|Cs1],Sofar,Cs2) :-
   tautological_clause(C1),
   !,
   state_remove_tautologies(Cs1,Sofar,Cs2).
state_remove_tautologies([C1|Cs1],Sofar,Cs2) :-
   state_remove_tautologies(Cs1,[C1|Sofar],Cs2).
state_remove_tautologies([],Sofar,Cs2) :-
   list_to_ord_set(Sofar,Cs2).
 
tautological_clause(Clause) :-
   negate_general_clause(Clause,Clause2),
   ord_intersect(Clause,Clause2).

 
/* remove_tautological_dn_facts(Facts1,Facts2) <-
      Facts2 is the set of non-tautological dn_facts from
      Facts1. */

remove_tautological_dn_facts(Facts1,Facts2) :-
   remove_tautological_dn_facts(Facts1,[],Facts2).

remove_tautological_dn_facts([Pos-Neg|Facts1],Sofar2,Facts2) :-
   ord_intersect(Pos,Neg),
   !,
   remove_tautological_dn_facts(Facts1,Sofar2,Facts2).
remove_tautological_dn_facts([Pos-Neg|Facts1],Sofar2,Facts2) :-
   remove_tautological_dn_facts(Facts1,[Pos-Neg|Sofar2],Facts2).
remove_tautological_dn_facts([],Sofar2,Facts2) :-
   list_to_ord_set(Sofar2,Facts2).


/* disjunction_true_in_model(Model,Disjunction) <-
      tests if the model Model implies the disjunction
      Disjunction. */
 
disjunction_true_in_model(Model,[~A|Ls]) :-
   !,
   ( \+ member(A,Model)
   ; disjunction_true_in_model(Model,Ls) ).
disjunction_true_in_model(Model,[A|Ls]) :-
   !,
   ( member(A,Model)
   ; disjunction_true_in_model(Model,Ls) ).


canonize_dn_facts(Facts1,Facts2) :-
   assert_arguments(dn_fact,Facts1),
   remove_implied_dn_facts_loop(Facts1,Facts2),
   retractall(dn_fact(_)).

remove_implied_dn_facts_loop([Pos1-Neg1|Facts1],Facts2) :-
   dn_fact(Pos2-Neg2), 
   Pos1-Neg1 \== Pos2-Neg2,
   ord_subset(Pos1,Pos2), ord_subset(Neg1,Neg2),
   !,
   retract(dn_fact(Pos2-Neg2)),
   remove_implied_dn_facts_loop([Pos1-Neg1|Facts1],Facts2).
remove_implied_dn_facts_loop([_|Facts1],Facts2) :-
   remove_implied_dn_facts_loop(Facts1,Facts2).
remove_implied_dn_facts_loop([],Facts2) :-
   collect_arguments(dn_fact,List),
   list_to_ord_set(List,Facts2).

dn_fact_subsumes_dn_fact(Pos1-Neg1,Pos2-Neg2) :-
   ord_subset(Pos1,Pos2), ord_subset(Neg1,Neg2).


/* state_consistent(State) <-
      */

state_consistent(State) :-
   \+ state_inconsistent(State).


/* state_inconsistent(State) <-
      tests if the g-state State is inconsistent. */

state_inconsistent(State) :-
   binary_resolve_fixpoint(State,State2),
   !,
   member([],State2).


/* state_pair_inconsistent([T,F]) <-
      tests if the state pair [T,F] is inconsistent. */

state_pair_inconsistent([T,F]) :-
   state_pair_to_general_state([T, F], Gs),
   state_inconsistent(Gs).

state_pair_inconsistent_test(State_Pair) :-
   state_pair_inconsistent(State_Pair),
   !,
   writeln(user,'inconsistent state pair').
state_pair_inconsistent_test(_) :-
   writeln(user,'consistent state pair').


/* state_pair_to_general_state([T, F], G) <-
      */

state_pair_to_general_state([T, F], G) :-
   negate_state(F, N),
   ord_union(T, N, G).


/* partial_herbrand_interpretation_to_literals([T, F], Ls) <-
      */

partial_herbrand_interpretation_to_literals([T, F], Ls) :-
   negate_general_clause(F, N),
   ord_union(T, N, Ls).


/* partial_herbrand_interpretation_is_consistent([T, F]) <-
      */

partial_herbrand_interpretation_is_consistent([T, F]) :-
   \+ partial_herbrand_interpretation_is_inconsistent([T, F]).


/* partial_herbrand_interpretation_is_inconsistent([T, F]) <-
      */

partial_herbrand_interpretation_is_inconsistent([T, F]) :-
   ord_intersect(T, F).


/* dnlp_and_state_pair_inconsistent_test(Program,[T,F]) <-
      tests if the DNLP Program 
      is inconsistent with the state pair [T,F]. */

dnlp_and_state_pair_inconsistent_test(Program,[T,F]) :-
   negate_state(F,N),
   ord_union(T,N,G_State),
   dnlp_logical_implication(Program,G_State,G_State2),
   member([],G_State2),
   !,
   writeln(user,'dnlp inconsistent with state pair').
dnlp_and_state_pair_inconsistent_test(_,_) :-
   writeln(user,'dnlp consistent with state pair').


/* state_sort(State1,State2) <-
      State1 is a disjunctive Herbrand state, that possibly is
      not sorted and consists of clauses that are not sorted.
      State2 is the corresponding sorted state of sorted
      clauses. */

state_sort(State1,State2) :-
   lists_to_ord_sets(State1,State2).


/******************************************************************/


/* state_can_3(Facts,Purified_Facts,N) <-
      Purified_Facts is the canonical form of Facts,
      it contains N facts,
      a new delta_d_fact_tree representing them is asserted.  */

state_can_3(Facts,Purified_Facts,N) :-
   write('can          '), ttyflush,
   start_timer(purifying),
   dabolish(delta_d_fact_tree/1),
   start_timer(length_order_2),
   length_order_2(Facts,Len_Facts),
%  length_order(Facts,Len_Facts),
   stop_timer(length_order_2,Time),
   init_timer(tree_subsumes_clause),
   init_timer(tree_clause_insert),
   state_can_3(Len_Facts,[],Purified_Facts,N),
%  state_can_3(Len_Facts,[],[],Purified_Facts,0,N),
   stop_timer(purifying),
   write('             '), ttyflush,
   pp_time(Time), writeln(' sec. '),
   write('             '), ttyflush,
   close_timer(tree_subsumes_clause),
   write('             '), ttyflush,
   close_timer(tree_clause_insert).


state_can_3([[_,Fact]|Len_Facts],[],[Fact|Purified_Facts],N) :-
   !,
   state_can_3(Len_Facts,[[],[Fact]],Purified_Facts,M),
   N is M + 1.
state_can_3([[_,Fact]|Facts],Tree,Purified_Facts,N) :-
   measured_tree_subsumes_clause(Tree,Fact),
   !,
   state_can_3(Facts,Tree,Purified_Facts,N).
state_can_3([[_,Fact]|Facts],Tree,[Fact|Purified_Facts],N) :-
   !,
   start_timer(tree_clause_insert),
   tree_clause_insert(Tree,New_Tree,Fact),
   add_timer(tree_clause_insert),
   state_can_3(Facts,New_Tree,Purified_Facts,M),
   N is M + 1.
state_can_3([],Tree,[],0) :-
   !,
   assert(delta_d_fact_tree(Tree)).


/* state_can_3(Facts,[],Purified_Facts,N) <-
      with accumulators. */

state_can_3([Fact|Facts],[],[],Purified_Facts,N,K) :-
   !,
   M is N + 1,
   state_can_3(Facts,[[],[Fact]],[Fact],Purified_Facts,M,K).
state_can_3([Fact|Facts],Tree,Sofar,Purified_Facts,N,K) :-
   measured_tree_subsumes_clause(Tree,Fact),
   !,
   state_can_3(Facts,Tree,Sofar,Purified_Facts,N,K).
state_can_3([Fact|Facts],Tree,Sofar,Purified_Facts,N,K) :-
   !,
   start_timer(tree_clause_insert),
   tree_clause_insert(Tree,New_Tree,Fact),
   add_timer(tree_clause_insert),
   M is N + 1,
   state_can_3(Facts,New_Tree,[Fact|Sofar],Purified_Facts,M,K).
state_can_3([],Tree,Sofar,Sofar,N,N) :-
   !,
   assert(delta_d_fact_tree(Tree)).


remove_implied_new_facts([],[]) :-
   !.
remove_implied_new_facts(State1,State2) :-
   assert_arguments(new_fact,State1),
   remove_implied_new_facts_loop(State1,State2).

remove_implied_new_facts_loop([Fact|Facts],PurifiedFacts) :-
   new_fact(Fact1), Fact \== Fact1, ord_subset(Fact,Fact1),
   !,
   retract(new_fact(Fact1)),
   remove_implied_new_facts_loop([Fact|Facts],PurifiedFacts).
remove_implied_new_facts_loop([_|Facts],PurifiedFacts) :-
   remove_implied_new_facts_loop(Facts,PurifiedFacts).
remove_implied_new_facts_loop([],PurifiedFacts) :-
%  collect_arguments(new_fact,List),
%  remove_duplicates(List,PurifiedFacts).
   collect_new_facts([],PurifiedFacts).
 
collect_new_facts(Facts,PurifiedFacts) :-
   new_fact(Fact),
   !,
   retract(new_fact(Fact)),
   collect_new_facts([Fact|Facts],PurifiedFacts).
collect_new_facts(Facts,PurifiedFacts) :-
   remove_duplicates(Facts,PurifiedFacts).


/* length_order(State1,State2) <-
      given a state State1, State2 is the list of clauses
      from State1 sorted by their length. */

length_order(State1,State2) :-
   !,
   length_count_loop(State1,State3),
   assert_arguments_generic(length_d_fact_pair,State3),
   length_order_collect_d_facts(0,[],State4),
   reverse(State4,State2).

length_order_collect_d_facts(N,Sofar,State) :-
   length_d_fact_pair(_,_),
   length_d_fact_pair(N,_),
   !,
   length_order_collect_d_facts_of_certain_length(N,[],State1),
   append(State1,Sofar,New_Sofar),
   M is N + 1,
   length_order_collect_d_facts(M,New_Sofar,State).
length_order_collect_d_facts(N,Sofar,State) :-
   length_d_fact_pair(_,_),
   M is N + 1,
   length_order_collect_d_facts(M,Sofar,State).
length_order_collect_d_facts(_,State,State).


length_order_collect_d_facts_of_certain_length(N,Sofar,State) :-
   retract(length_d_fact_pair(N,Clause)),
   !,
   length_order_collect_d_facts_of_certain_length(
      N,[Clause|Sofar],State).
length_order_collect_d_facts_of_certain_length(_,State,State).


/* length_order_2(State1,L_State2) <-
      L_State is the list of pairs [L,C] for the elements C of
      State1 and their lengths L,
      L_State2 is an ordered set beginning with small numbers L.  */
 
length_order_2(State1,L_State2) :-
   !,
   length_count_loop(State1,L_State),
   list_to_ord_set(L_State,L_State2).

length_count_loop([],[]).
length_count_loop([C|Cs],[[L,C]|LCs]) :-
   length(C,L),
   length_count_loop(Cs,LCs).


/******************************************************************/

 
