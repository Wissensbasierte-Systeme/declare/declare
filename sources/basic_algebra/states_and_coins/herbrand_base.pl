

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Herbrand Base                           ***/
/***                                                            ***/
/******************************************************************/


:- dynamic 
      herbrand_base/1,
      herbrand_tuples/2, 
      program_atom/1.


/*** interface ****************************************************/


/* conjunction_consistent(Conjunction) <-
      */

conjunction_consistent(Conjunction) :-
   \+ ( member(~A, Conjunction), member(A, Conjunction) ).


/* val_dis(Disjunction,Value) <-
      computes the truth value Value of a ground disjunction 
      Disjunction w.r.t. the state pair (d_fact,n_fact). */

val_dis(Disjunction,Value) :-
   list_to_ord_set(Disjunction,Disjunction1),
   ( ( val_dis_true(Disjunction1), 
       Value = t );
     ( val_dis_false(Disjunction1), 
       Value = f );
     ( Value = u ) ).

/* val_conj(Conjunction,Value) <-
      computes the truth value Value 
      of a ground disjunction Conjunction
      w.r.t. the state pair (d_fact,n_fact). */

val_conj(Conjunction,Value) :-
   list_to_ord_set(Conjunction,Conjunction1),
   ( ( val_conj_false(Conjunction1), 
       Value = f );
     ( val_conj_true(Conjunction1), 
       Value = t );
     ( Value = u ) ).


/* dnlp_to_herbrand_base(Program,Herbrand_Base) <-
      for a given DNLP Program the Herbrand base Herbrand_Base is
      determined. */

dnlp_to_herbrand_base(Program, Herbrand_Base) :-
   dnlp_to_dn_clauses(Program),
   program_to_herbrand_base(Herbrand_Base),
   retractall(dn_clause(_, _, _)).


/* compute_herbrand_base <-
      computes the Herbrand base HB of the program given by the currently
      asserted dn_clauses and d_facts, asserts herbrand_base(HB).  */

compute_herbrand_base :-
   compute_herbrand_base(_),
   lhb.

compute_herbrand_base(Herbrand_Base) :-
   retractall(herbrand_base(_)),
   program_to_herbrand_base(Herbrand_Base),
   assert(herbrand_base(Herbrand_Base)).


/* dnlp_to_herbrand_universe(Program, Herbrand_Universe) <-
      */

program_to_herbrand_universe(Program, Herbrand_Universe) :-
   dnlp_to_herbrand_universe(Program, Herbrand_Universe).

dnlp_to_herbrand_universe(Program, Herbrand_Universe) :-
   dnlp_to_dn_clauses(Program),
   program_to_herbrand_universe(Herbrand_Universe),
   retractall(dn_clause(_, _, _)).

program_to_herbrand_universe(Herbrand_Universe) :-
   start_timer(program_to_herbrand_universe),
   get_program_atoms(Atoms),
   get_constants_from_clause(Atoms, Herbrand_Universe),
   abolish(herbrand_tuples/2),
   stop_timer(program_to_herbrand_universe),
   !.


/* program_to_herbrand_base(Herbrand_Base) <-
      computes the Herbrand base Herbrand_Base of the program given 
      by the currently asserted dn_clauses and d_facts. */

program_to_herbrand_base(Herbrand_Base) :-
   write('herbrand     '), ttyflush,
   start_timer(program_to_herbrand_base),
   get_program_atoms(Atoms),
   get_predicates(Atoms,Predicates),
   get_constants_from_clause(Atoms, Constants),
   get_maximal_arity(Predicates, Arity),
   generate_herbrand_tuples(Constants, Arity),
   generate_herbrand_atoms(Predicates, Herbrand_Atoms),
   list_to_ord_set(Herbrand_Atoms, Herbrand_Base),
   abolish(herbrand_tuples/2),
   stop_timer(program_to_herbrand_base),
   !.


dlp_to_predicates_and_arities(Program,Pairs) :-
   dlp_to_atoms(Program,Atoms),
   get_predicates(Atoms,Pairs).


/* program_atom(Atom) <-
      generates all atoms that occur in some dn_clause or d_fact atom
      of the currently asserted DNLP. */

program_atom(Atom) :-
   dn_clause(Head,Body1,Body2),
   ( member(Atom,Head); member(Atom,Body1); member(Atom,Body2) ).
program_atom(Atom) :-
   d_fact(Atoms),
   member(Atom,Atoms).


/* get_program_atoms(Atoms) <-
      Atoms is the set of all atoms that occur in some dn_clause or 
      d_fact atom of the currently asserted DNLP. */

get_program_atoms(Atoms) :-
   collect_arguments_generic(dn_clause/3, Arguments), 
   append(Arguments, Clauses1), append(Clauses1, Atoms1),
   collect_arguments(d_fact, Clauses2),
   append(Clauses2, Atoms2),
   append(Atoms1, Atoms2, Atoms3),
   list_to_ord_set(Atoms3, Atoms).


dlp_to_atoms(Program,Atoms) :-
   dlp_to_atoms(Program,[],Atoms).

dlp_to_atoms([R|Rs],Sofar,Atoms) :-
   parse_dislog_rule(R,H,P,N),
   append([H,P,N],List),
   list_to_ord_set(List,As),
   ord_union(Sofar,As,New_Sofar),
   dlp_to_atoms(Rs,New_Sofar,Atoms).
dlp_to_atoms([],Atoms,Atoms).


/* negate_general_clause(Clause1,Clause2) <-
      computes the negation Clause2 of the g-clause Clause1. */

negate_general_clause(Clause1,Clause2) :-
   negate_literals(Clause1,Clause),
   list_to_ord_set(Clause,Clause2).

negate_literals([L1|Ls1],[L2|Ls2]) :-
   negate_literal(L1,L2),
   negate_literals(Ls1,Ls2).
negate_literals([],[]).

negate_literal(~Atom, Atom) :-
   !.
negate_literal(not(Atom), Atom) :-
   !.
negate_literal(\+ Atom, Atom) :-
   !.
negate_literal(Atom, ~Atom).


/* negate_state(State1,State2) <-
      computes the negation State2 of the d-state State1. */

negate_state([C|Cs],[N|Ns]) :-
   negate_atoms(C,N),
   negate_state(Cs,Ns).
negate_state([],[]).

negate_atoms([A|As],[L|Ls]) :-
   negate_atom(A,L),
   negate_atoms(As,Ls).
negate_atoms([],[]).
 

/* negate_atom(Atom,~Atom) <-
      computes the negation ~Atom of the atom Atom. */

negate_atom(~Atom,Atom) :-
   !.
negate_atom(Atom,~Atom).
 

/* atom_in_literal(Literal,Atom) <-
      succeeds if the atom Atom occurs in the literal Literal. */

atom_in_literal(~Atom,Atom) :-
   !.
atom_in_literal(Atom,Atom).


/* purely_positive_clause(Clause) <-
      succeeds if the clause Clause is a d-clause. */

purely_positive_clause(Clause) :-
   \+ member(~_,Clause).


/* purely_negative_clause(Clause) <-
      succeeds if the clause Clause is an n-clause. */

purely_negative_clause([~_|Ls]) :-
   purely_negative_clause(Ls).
purely_negative_clause([~_]).


/* mixed_clause(Clause) <-
      succeeds if the clause Clause neither is a d- nor an n-clause. */

mixed_clause(Clause) :-
   \+ purely_positive_clause(Clause),
   \+ purely_negative_clause(Clause).


/* partition_general_herbrand_state(State,Pos,Neg,Mixed) <-
      a given g-state State is partitioned 
      into its d-state,n-state and mixed state. */

partition_general_herbrand_state(State,Pos,Neg,Mixed) :-
   partition_general_herbrand_state(State,[],Pos,[],Neg,[],Mixed).

partition_general_herbrand_state([C|Cs],Sofar1,Pos,Sofar2,Neg,Sofar3,Mixed) :-
   purely_positive_clause(C), !,
   partition_general_herbrand_state(Cs,[C|Sofar1],Pos,Sofar2,Neg,Sofar3,Mixed).
partition_general_herbrand_state([C|Cs],Sofar1,Pos,Sofar2,Neg,Sofar3,Mixed) :-
   purely_negative_clause(C), !,
   partition_general_herbrand_state(Cs,Sofar1,Pos,[C|Sofar2],Neg,Sofar3,Mixed).
partition_general_herbrand_state([C|Cs],Sofar1,Pos,Sofar2,Neg,Sofar3,Mixed) :-
   partition_general_herbrand_state(Cs,Sofar1,Pos,Sofar2,Neg,[C|Sofar3],Mixed).
partition_general_herbrand_state([],Sofar1,Pos,Sofar2,Neg,Sofar3,Mixed) :-
   list_to_ord_set(Sofar1,Pos),
   list_to_ord_set(Sofar2,Neg),
   list_to_ord_set(Sofar3,Mixed).


atoms_to_predicate_symbols(Atoms,Ps) :-
   maplist( atom_to_predicate_symbol,
      Atoms, Ps ).

atom_to_predicate_symbol(A,P) :-
   A =.. [P|_].


/* atoms_with_predicate_symbol(P,Atoms_1,Atoms_2) <-
      Given a predicate symbol P and a list Atoms_1 of atoms,
      the list Atoms_2 containing all atoms in Atoms_1 with the
      predicate symbol P is computed. */

atoms_with_predicate_symbol(P,Atoms_1,Atoms_2) :-
   findall( [P|Args],
      member([P|Args],Atoms_1),
      Atoms_2).


/*** implementation **********************************************/


/* val_...(Clause) <-
      tests the value of the clause Clause w.r.t. the currently 
      asserted atoms for d_fact and n_fact. */

val_dis_true(Disjunction) :-
   d_fact(Fact),
   sublist(Fact,Disjunction).

val_dis_false([]).
val_dis_false([Atom|Atoms]) :-
   n_fact([Atom]),
   val_dis_false(Atoms).

val_conj_false(Conjunction) :-
   n_fact(Fact),
   sublist(Fact,Conjunction).

val_conj_true([]).
val_conj_true([Atom|Atoms]) :-
   d_fact([Atom]),
   val_conj_true(Atoms).


/* get_predicates(Atoms,Predicates) <-
      given a set Atoms of atoms, the set Predicates of all
      occurring predicate symbols is computed. */

get_predicates(Atoms,Predicates) :-
   get_predicates_loop(Atoms,Predicates1),
   list_to_ord_set(Predicates1,Predicates).

get_predicates_loop([],[]).
get_predicates_loop([Atom|Atoms],[[Predicate,Arity]|Predicates]) :-
   functor(Atom,Predicate,Arity),
   get_predicates_loop(Atoms,Predicates).


/* get_constants_from_clause(Clauses,Constants) <-
      given a set Clauses of clauses, the set Constants of all
      occurring constant symbols is computed. */

get_constants_from_clause([],[]).
get_constants_from_clause([Atom|Atoms],Constants_4) :-
   Atom =.. [_|Arglist],
   get_constants_from_list(Arglist,Constants_1),
   list_to_ord_set(Constants_1,Constants_2),
   get_constants_from_clause(Atoms,Constants_3),
   ord_union(Constants_2,Constants_3,Constants_4).

get_constants_from_list([H|T],[H|Constants]) :-
   nonvar(H),
   !,
   get_constants_from_list(T,Constants).
get_constants_from_list([_|T],Constants) :-
   get_constants_from_list(T,Constants).
get_constants_from_list([],[]).


/* get_maximal_arity(Predicate_Arity_List,Maxarity) <-
      Maxarity is determined as the maximal value of Arity
      among all pairs [Predicate,Arity] in Predicate_Arity_List. */

get_maximal_arity([[_,Arity]|List],Maxarity) :-
   get_maximal_arity(List,Maxarity),
   Maxarity >= Arity,
   !.
get_maximal_arity([[_,Arity]|_],Arity).
get_maximal_arity([],0).


/* generate_herbrand_atoms(Predicate_Arity_List,Atoms) <-
      given a list Predicate_Arity_List of pairs [Predicate,Arity]
      and the asserted atom herbrand_tuples(Arity,Tuples),
      the set of all ground atoms with the predicate symbol
      Predicate and argument tuple from Tuples is computed. */
 
generate_herbrand_atoms([[Predicate,Arity]|List],Atoms) :-
   herbrand_tuples(Arity,Tuples),
   generate_herbrand_atoms(Predicate,Tuples,Atoms1),
   generate_herbrand_atoms(List,Atoms2),
   append(Atoms1,Atoms2,Atoms).
generate_herbrand_atoms([],[]).
 
generate_herbrand_atoms(Predicate,[Tuple|Tuples],[Atom|Atoms]) :-
   Atom =.. [Predicate|Tuple],
   generate_herbrand_atoms(Predicate,Tuples,Atoms).
generate_herbrand_atoms(_,[],[]).
 

generate_herbrand_tuples(Constants,Arity) :-
   assert(herbrand_tuples(0,[[]])),
   list_of_elements_to_relation(Constants,Tuples),
   generate_herbrand_tuples(Tuples,0,Arity).

generate_herbrand_tuples(_,M,M) :-
   !.
generate_herbrand_tuples(Tuples,M,N) :-
   herbrand_tuples(M,Tuples1),
   cartesian_product(Tuples,Tuples1,Tuples2),
   K is M + 1,
   assert(herbrand_tuples(K,Tuples2)),
   generate_herbrand_tuples(Tuples,K,N).


/* flatten_triple_sets(Triple_Sets,Atoms) <-
      given a set Triple_Sets of sets of atoms, the set Atoms of 
      all atoms occurring in some of the sets is computed. */

flatten_triple_sets([Triple_Set|Triple_Sets],Atoms) :-
   append(Triple_Set,Atoms1),
   flatten_triple_sets(Triple_Sets,Atoms2),
   append(Atoms1,Atoms2,Atoms).
flatten_triple_sets([],[]).


/*** tests ********************************************************/


test(herbrand, program_to_herbrand_universe) :-
   Program = [
      [a(X, 1)]-[b(X, 2)],
      [c(3, 4)] ],
   program_to_herbrand_universe(Program, Herbrand_Universe),
   writeq(user, Herbrand_Universe).


/******************************************************************/


