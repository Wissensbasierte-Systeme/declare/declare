

/******************************************************************/
/***                                                            ***/
/***         Graphs:  Rule/Goal Graphs in XPCE                  ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set( rule_goal_graph_layout,
      [x_step: 200, y_step: 50]
%     [x_step: 50,  y_step: 50]
   ).


/*** tests ********************************************************/


test(graphs_to_xpce, vertices_and_edges_to_picture) :-
   program_vertices_and_edges_to_picture([a,b], [a-b], _).


/*** interface ****************************************************/


/* program_file_to_dependency_graph_xpce(File, Picture) <-
      */

program_file_to_dependency_graph_xpce(File) :-
   program_file_to_dependency_graph_xpce(File, _).

program_file_to_dependency_graph_xpce(File, Picture) :-
   dislog_consult(File, Program),
   program_to_dependency_graph_xpce(Program, Picture).


/* program_to_dependency_graph_xpce(Program) <-
      */

program_to_dependency_graph_xpce(Program) :-
   program_to_dependency_graph_xpce(Program, _).

program_to_dependency_graph_xpce(Program, Picture) :-
   dislog_program_to_rule_goal_graph(Program, [Vertices, Edges]),
   program_vertices_and_edges_to_picture(
      Vertices, Edges, Picture).


/* program_vertices_and_edges_to_picture(
         Vertices, Edges, Picture) <-
      */

program_vertices_and_edges_to_picture(Vertices, Edges, Picture) :-
%  writeln(user, vertices=Vertices),
%  writeln(user, edges=Edges),
   xpce_picture_clear(Picture),
   vertices_and_edges_to_gxl(Vertices, Edges, Gxl_1),
%  dwrite(xml, Gxl_1),
   Layout_Config = config:[]:[
      gxl_layout:[ mode:bfs,
         x_start:30, y_start:30,
         x_step: 50, y_step: 50, y_variance:10 ]:[] ],
   gxl_graph_layout(Layout_Config, Gxl_1, Gxl_2),
%  dwrite(xml, Gxl_2),
   gxl_to_picture(Gxl_2, Picture).
%  vertices_edges_to_picture_bfs(Vertices, Edges, Picture).


/* program_vertices_and_edges_to_picture_2(
         Predicates, Vertices, Edges, Picture) <-
      */

program_vertices_and_edges_to_picture_2(
      Predicates, Vertices, Edges, Picture) :-
   xpce_picture_clear(Picture),
   vertices_and_edges_to_gxl(Vertices, Edges, Gxl_1),
   dislog_variable_get(rule_goal_graph_layout,
      [x_step: X_step, y_step: Y_step]),
   Layout_Config = config:[]:[
      gxl_layout:[ mode:bfs,
         x_start:30, y_start:30,
         x_step:X_step, y_step:Y_step, y_variance:10 ]:[] ],
   gxl_graph_layout_2(Layout_Config, Predicates, Gxl_1, Gxl_2),
   gxl_to_picture(config:[]:[], Gxl_2, Picture).


/******************************************************************/


