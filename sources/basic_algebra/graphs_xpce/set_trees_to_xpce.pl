

/******************************************************************/
/***                                                            ***/
/***          GXL:  Set Trees to XPCE                           ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(tree_to_picture, set_tree_to_picture_bfs) :-
   set_tree_to_picture_bfs([[a], [[b]], [[c]]]).


/*** interface ****************************************************/


set_tree_to_picture_dfs([]) :-
   !.
set_tree_to_picture_dfs(Tree) :-
   set_tree_to_picture_dfs(Tree, _).

set_tree_to_picture_dfs(Tree, Picture) :-
%  gxl_layout_variable_switch(tree_offset_x, X_Offset, 75),
   set_tree_to_vertices_edges(Tree, Vertices, Edges),
%  set_tree_to_vertices(Tree, Vertices),
%  set_tree_to_edges(Tree, Edges),
   length(Vertices, N),
   ( N < 40,
     vertices_edges_to_picture_dfs(Vertices, Edges, Picture)
%    vertices_edges_to_picture(Vertices, Edges)
   ; N >= 40,
     vertices_edges_to_picture_dfs(Vertices, Edges, Picture),
%    vertices_edges_to_picture(dfs, Vertices, Edges, Picture),
     xpce_graph_layout(xpce, Picture),
     true ).
%  gxl_layout_variable_set(tree_offset_x, X_Offset).


/* set_tree_to_picture_bfs(Tree) <-
      */

set_tree_to_picture_bfs([]) :-
   !.
set_tree_to_picture_bfs(Tree) :-
   set_tree_to_picture_bfs(Tree, _).

set_tree_to_picture_bfs(Tree, Picture) :-
%  gxl_layout_variable_switch(tree_offset_x, X_Offset, 75),
   set_tree_to_vertices_edges(Tree, Vertices, Edges),
%  set_tree_to_vertices(Tree, Vertices),
%  set_tree_to_edges(Tree, Edges),
   length(Vertices, N),
   ( N < 20,
     vertices_edges_to_picture_bfs(Vertices, Edges, Picture)
%    vertices_edges_to_picture(Vertices, Edges)
   ; N >= 20,
     vertices_edges_to_picture_bfs(Vertices, Edges, Picture),
%    vertices_edges_to_picture(dfs, Vertices, Edges, Picture),
     xpce_graph_layout(xpce, Picture),
     true ).
%  gxl_layout_variable_set(tree_offset_x, X_Offset).


/* set_tree_to_vertices(Tree, Vertices) <-
      */

set_tree_to_vertices(Tree, Vertices) :-
   set_tree_to_vertices([], Tree, Vertices).

set_tree_to_vertices(Xs, [Root|Trees], [Vertex|Vertices]) :-
   ord_union(Xs, Root, Vs),
   list_to_vertex_label(Root, Label),
   Vertex = Vs-Label-rhombus-blue-medium,
   maplist( set_tree_to_vertices(Vs),
      Trees, List ),
   append(List, Vertices).

list_to_vertex_label(Xs, Label) :-
   list_to_comma_structure(Xs, X2),
   term_to_atom(X2, X3),
   name_remove_elements("\'", X3, Label).
list_to_vertex_label([], ' ').


/* set_tree_to_edges([Root|Trees], Edges) <-
      */

set_tree_to_edges([Root|Trees], Edges) :-
   set_tree_to_edges([], [Root|Trees], Edges).

set_tree_to_edges(Xs, [Root|Trees], Edges) :-
   ord_union(Xs, Root, Vs),
   findall( Vs-Ws,
      ( member([R|_], Trees),
        ord_union(Vs, R, Ws) ),
      Edges_2 ),
   maplist( set_tree_to_edges(Vs),
      Trees, List ),
   append([Edges_2|List], Edges).


/* set_tree_to_vertices_edges(Tree, Vertices, Edges) <-
      */

set_tree_to_vertices_edges(Tree, Vertices, Edges) :-
   set_tree_number(Tree, Tree_2),
   numbered_set_tree_to_vertices(Tree_2, Vertices),
   numbered_set_tree_to_edges(Tree_2, Edges).

numbered_set_tree_to_vertices([N-Root|Trees], Vertices) :-
   maplist( numbered_set_tree_to_vertices,
      Trees, Lists ),
   list_to_vertex_label(Root, Label),
   Vertex = N-Label-circle-blue-medium,
   append([[Vertex]|Lists], Vertices).
numbered_set_tree_to_vertices([], []).

numbered_set_tree_to_edges([N-_|Trees], Edges) :-
   findall( N-M,
      member([M-_|_], Trees),
      Edges_1 ),
   maplist( numbered_set_tree_to_edges,
      Trees, Edges_2 ),
   append([Edges_1|Edges_2], Edges).
numbered_set_tree_to_edges([], []).


/* set_tree_number(Tree_1, Tree_2) <-
      */

set_tree_number(Tree_1, Tree_2) :-
   set_tree_number(1, Tree_1, _, Tree_2).

set_tree_number(N, [], M, []) :-
   M is N - 1.
set_tree_number(N, [Root|Trees_1], M, [N-Root|Trees_2]) :-
   K is N + 1,
   set_trees_number(K, Trees_1, M, Trees_2).

set_trees_number(I, [T1|Ts1], J, [T2|Ts2]) :-
   set_tree_number(I, T1, K, T2),
   L is K + 1,
   set_trees_number(L, Ts1, J, Ts2).
set_trees_number(I, [], J, []) :-
   J is I - 1.


/******************************************************************/


