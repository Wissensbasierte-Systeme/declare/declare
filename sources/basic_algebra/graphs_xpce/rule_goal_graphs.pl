

/******************************************************************/
/***                                                            ***/
/***       Declare:  Rule/Goal Graphs                           ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(rule_goal_graph, prolog_rule_to_rule_goal_graph(1)) :-
   Rule = (d(_,_) :- a,(b;c)),
%  Rule = (d(_,_) :- a,(b,c)),
%  Rule = (d(_,_) :- (a,b),c),
%  Rule = (d(_,_)),
   prolog_rule_to_rule_goal_graph(Rule, [Vertices, Edges]),
   program_vertices_and_edges_to_picture(Vertices, Edges, _).
test(rule_goal_graph, prolog_rule_to_rule_goal_graph(2)) :-
   dislog_variable_get(example_path,
      'teaching/prolog/find_trust.pl', File),
   dread(pl, File, Program),
   prolog_program_to_rule_goal_graph_to_picture(Program, _).


/*** interface ****************************************************/


/* prolog_program_to_rule_goal_graph_to_picture(
         Program, Picture) <-
      */

prolog_program_to_rule_goal_graph_to_picture(Program, Picture) :-
   prolog_program_without_dynamics(Program, Program_2),
   prolog_program_to_rule_goal_graph(
      Program_2, [Vertices, Edges]),
%  writelnq(user, vertices(Vertices)),
%  writelnq(user, edges(Edges)),
%  prolog_program_to_predicate_symbols(
%     Program_2, Predicates),
   prolog_program_to_predicate_symbols_with_arity(
      Program_2, Predicates_2),
   maplist( term_to_atom, Predicates_2, Predicates ),
%  writelnq(user, Predicates),
%  writelnq(user, program_vertices_and_edges_to_picture_2(
%     Predicates, Vertices, Edges, Picture)),
   program_vertices_and_edges_to_picture_2(
      Predicates, Vertices, Edges, Picture).
%  program_vertices_and_edges_to_picture(
%     Vertices, Edges, Picture).


/* prolog_program_to_rule_goal_graph(Program, [Vertices, Edges]) <-
      */

prolog_program_to_rule_goal_graph(Program, [Vertices, Edges]) :-
   maplist( prolog_rule_to_rule_goal_graph,
      Program, Tuples ),
   ( foreach([Vs, Es], Tuples),
     foreach(Vs, Vss), foreach(Es, Ess) do true ),
   append(Vss, Vs_2),
   append(Ess, Es_2),
   list_to_ord_set(Vs_2, Vertices),
   list_to_ord_set(Es_2, Edges).
 
xxx_prolog_rule_to_rule_goal_graph(Rule, [Vertices, Edges]) :-
   parse_prolog_rule(Rule, (A :- B)),
   gensym(rule_, N),
   ( B = true -> Color = white
   ; Color = blue ),
   Vertex_Rule = N-''-triangle-Color-small,
   functor(A, P, _),
   Vertex_A = P-P-circle-grey-small,
   prolog_formula_to_rule_goal_graph(B, [Id, Vs, Es]),
   Vertices = [Vertex_A, Vertex_Rule|Vs],
   Edges = [P-N-grey, N-Id-grey|Es].


/* prolog_rule_to_rule_goal_graph(Rule, [Vertices, Edges]) <-
      */

prolog_rule_to_rule_goal_graph(Rule, [Vertices, Edges]) :-
   parse_prolog_rule(Rule, (A :- B)),
   prolog_equalities_execute(B),
%  writelnq(user, (A :- B)),
   gensym(rule_, N),
   Vertex_Rule = N-''-triangle-blue-small,
   prolog_atom_to_predicate_with_arity(A, P),
   Vertex_A = P-P-circle-grey-small,
   ( B = true -> Goals = []
   ; comma_structure_to_list(B, Goals) ),
   maplist( prolog_formula_to_rule_goal_graph,
      Goals, Triples ),
   ( foreach([Id, Vs, Es], Triples), foreach(N-Id-grey, Ess_1),
     foreach(Vs, Vss), foreach(Es, Ess_2) do true ),
   append(Vss, Vs_2),
   append([Ess_1|Ess_2], Ess),
   Vertices = [Vertex_A, Vertex_Rule|Vs_2], 
   Edges = [P-N-grey|Ess].


/* prolog_equalities_execute(Goal) <-
      */

prolog_equalities_execute(Goal) :-
   comma_structure_to_list(Goal, Xs),
%  forall( member(X=Y, Xs),  call(X=Y) ),
   prolog_equalities_execute_loop(Xs).

prolog_equalities_execute_loop([(X=Y)|Atoms]) :-
   call(X=Y),
   !,
   prolog_equalities_execute_loop(Atoms).
prolog_equalities_execute_loop([_|Atoms]) :-
   prolog_equalities_execute_loop(Atoms).
prolog_equalities_execute_loop([]).


/* prolog_formula_to_rule_goal_graph(Goal, [Id, Vertices, Edges]) <-
      */

prolog_formula_to_rule_goal_graph(Goal, [Id, Vertices, Edges]) :-
   Goal = (!),
   !,
   gensym('cut', Id),
   Vertices = [Id-'!'-circle-red-small],
   Edges = [].
prolog_formula_to_rule_goal_graph(Goal, [Id, Vertices, Edges]) :-
   Goal = (A,B),
   !,
   gensym(',_', Id),
   prolog_formula_to_rule_goal_graph(A, [Id_A, Vertices_A, Edges_A]),
   prolog_formula_to_rule_goal_graph(B, [Id_B, Vertices_B, Edges_B]),
   V = Id-','-circle-white-small,
   append([ [Id-Id_A-grey, Id-Id_B-grey], Edges_A, Edges_B ], Edges),
   append([V|Vertices_A], Vertices_B, Vertices).
prolog_formula_to_rule_goal_graph(Goal, [Id, Vertices, Edges]) :-
   Goal = (A;B),
   !,
   gensym(';_', Id),
   prolog_formula_to_rule_goal_graph(A, [Id_A, Vertices_A, Edges_A]),
   prolog_formula_to_rule_goal_graph(B, [Id_B, Vertices_B, Edges_B]),
   V = Id-';'-circle-white-small,
   append([ [Id-Id_A-grey, Id-Id_B-grey], Edges_A, Edges_B ], Edges),
   append([V|Vertices_A], Vertices_B, Vertices).
prolog_formula_to_rule_goal_graph(Goal, [Id, Vertices, Edges]) :-
   Goal = findall(_, G, _),
   !,
   gensym('findall_', Id),
   prolog_formula_to_rule_goal_graph(G, [Id_G, Vertices_G, Edges_G]),
   V = Id-'findall/3'-circle-red-small,
   Edges = [Id-Id_G-grey|Edges_G],
   Vertices = [V|Vertices_G].
prolog_formula_to_rule_goal_graph(Goal, [Id, Vertices, Edges]) :-
   Goal = ddbase_aggregate(_, G, _),
   !,
   gensym('ddbase_aggregate_', Id),
   prolog_formula_to_rule_goal_graph(G, [Id_G, Vertices_G, Edges_G]),
   V = Id-'ddbase_aggregate/3'-circle-red-small,
   Edges = [Id-Id_G-grey|Edges_G],
   Vertices = [V|Vertices_G].
prolog_formula_to_rule_goal_graph(Goal, [Id, Vertices, Edges]) :-
   Goal = maplist(G, _, _),
   !,
   gensym('maplist_', Id),
   prolog_formula_to_rule_goal_graph(G, [Id_G, Vertices_G, Edges_G]),
   V = Id-'maplist/3'-circle-red-small,
   Edges = [Id-Id_G-grey|Edges_G],
   Vertices = [V|Vertices_G].
prolog_formula_to_rule_goal_graph(Goal, [Id, Vertices, Edges]) :-
   Goal = checklist(G, _),
   !,
   gensym('checklist_', Id),
   prolog_formula_to_rule_goal_graph(G, [Id_G, Vertices_G, Edges_G]),
   V = Id-'checklist/2'-circle-red-small,
   Edges = [Id-Id_G-grey|Edges_G],
   Vertices = [V|Vertices_G].
prolog_formula_to_rule_goal_graph(Goal, [Id, Vertices, Edges]) :-
   Goal = not(G),
   !,
   gensym('not_', Id),
   prolog_formula_to_rule_goal_graph(G, [Id_G, Vertices_G, Edges_G]),
   V = Id-'not/1'-circle-red-small,
   Edges = [Id-Id_G-grey|Edges_G],
   Vertices = [V|Vertices_G].
prolog_formula_to_rule_goal_graph(Goal, [Id, Vertices, Edges]) :-
   Goal = (\+ G),
   !,
   gensym('not_', Id),
   prolog_formula_to_rule_goal_graph(G, [Id_G, Vertices_G, Edges_G]),
   V = Id-'not/1'-circle-red-small,
   Edges = [Id-Id_G-grey|Edges_G],
   Vertices = [V|Vertices_G].
prolog_formula_to_rule_goal_graph(Goal, [Id, Vertices, Edges]) :-
   Goal = call(G),
   !,
   gensym('call_', Id),
   V = Id-'call/1'-circle-green-small,
   ( nonvar(G) ->
     prolog_formula_to_rule_goal_graph(G, [Id_G, Vertices_G, Edges_G]),
     Edges = [Id-Id_G-grey|Edges_G],
     Vertices = [V|Vertices_G]
   ; Edges = [],
     Vertices = [V] ).
prolog_formula_to_rule_goal_graph(Goal, [Id, Vertices, Edges]) :-
   functor(Goal, F, 2),
   member(F, [=, \=]),
   !,
   gensym('binary_', Id),
   term_to_atom(F/2, P),
   V = Id-P-circle-green-small,
   Edges = [],
   Vertices = [V].
prolog_formula_to_rule_goal_graph(Goal, [P, [Vertex], []]) :-
   prolog_atom_to_predicate_with_arity(Goal, P),
   Vertex = P-P-circle-grey-small.

prolog_atom_to_predicate_with_arity(Atom, P_N) :-
   functor(Atom, P, N),
   term_to_atom(P/N, P_N).


/* dislog_program_to_rule_goal_graph(
         Program, [Vertices, Edges]) <-
      */

dislog_program_to_rule_goal_graph(Program, [Vertices, Edges]) :-
   maplist( dislog_rule_to_rule_goal_graph,
      Program, Pairs ),
   pair_lists(Vs, Es, Pairs),
   append(Vs, Vertices_2),
   append(Es, Edges_2),
   list_to_ord_set(Vertices_2, Vertices),
   list_to_ord_set(Edges_2, Edges).


/* dislog_rule_to_rule_goal_graph(Rule, [Vertices, Edges]) <-
      */

dislog_rule_to_rule_goal_graph(Rule, [Vertices, Edges]) :-
   parse_dislog_rule(Rule, Head, Body_1, Body_2),
   findall( P-P-circle-grey-small,
      ( ( member(A, Head)
        ; member(A, Body_1)
        ; member(A, Body_2) ),
        functor(A, P, _) ),
      Vertices_2 ),
   gensym(rule_, N),
   ( Body_1 = [], Body_2 = [] -> Color = white
   ; Color = blue ),
   Vertex = N-''-triangle-Color-small,
   list_to_ord_set([Vertex|Vertices_2], Vertices),
%  edges
   findall( P-N-grey,
      ( member(A, Head),
        functor(A, P, _) ),
      Edges_0 ),
   findall( N-P-grey,
      ( member(A, Body_1),
        functor(A, P, _) ),
      Edges_1 ),
%  negation
   findall( N-P-orange,
      ( member(A, Body_2),
        functor(A, P, _) ),
      Edges_2 ),
   append([Edges_0, Edges_1, Edges_2], Edges_3),
   list_to_ord_set(Edges_3, Edges).


/******************************************************************/


