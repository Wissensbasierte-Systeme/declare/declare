

/******************************************************************/
/***                                                            ***/
/***             DDK:  Heaps                                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* heap_init <-
      initializes a heap with size 0, 
      i.e. asserts heap_size(0). */

heap_init :-
   abolish(heap_size),
   assert(heap_size(0)),
   abolish(a).


/* pp_heap <-
      pretty_prints the heap given by the asserted atoms 
      a(I,Item,Weight), 
      the size of the heap is the asserted atom heap_size(N). */

pp_heap :-
   heap_size(N),
   pp_heap_all(N),
   !.


/* heap_insert_list(List) <-
      for each pair [X,G] in the list List an item X with the
      weight G is inserted into the heap given by the asserted 
      atoms a(I,Item,Weight). */

heap_insert_list([[X,G]|Ps]) :-
   heap_insert(X,G),
   heap_insert_list(Ps).
heap_insert_list([]).


/* heap_insert(X,G) <-
      inserts an item X with weight G in the heap given by the
      asserted atoms a(I,Item,Weight). */

hi(X,G) :-
   heap_insert(X,G), !.
heap_insert(X,G) :-
   retract(heap_size(0)),
   assert(heap_size(1)),
   assert(a(1,X,G)).
heap_insert(X,G) :-
   a(_,X,AG),
   AG < G.
heap_insert(X,G) :-
   retract(a(I,X,_)),
   up_insert(I,X,G).
heap_insert(X,G) :-
   retract(heap_size(N)),
   NN is N + 1,
   assert(heap_size(NN)),
   up_insert(NN,X,G).


/* heap_delete_list(List) <-
      each item X in the list List is deleted from the heap given 
      by the asserted atoms a(I,Item,Weight). */

heap_delete_list([X|Xs]) :-
   a(_,X,_),
   heap_delete(X),
   !,
   heap_delete_list(Xs).
heap_delete_list([_|Xs]) :-
   heap_delete_list(Xs).
heap_delete_list([]).


/* heap_delete(X) <-
      deletes the item X from the heap given by the asserted atoms 
      a(I,Item,Weight), if it was there, and fails, otherwise. */

hd(X) :-
   heap_delete(X), !.
heap_delete(_) :-
   retract(heap_size(1)),
   !,
   retract(a(_,_,_)),
   assert(heap_size(0)).
heap_delete(X) :-
   !,
   retract(heap_size(N)),
   retract(a(I,X,G)),
   retract(a(N,NX,NG)),
   NN is N - 1,
   assert(heap_size(NN)),
   ud_insert(I,NX,NG,G).


/*** implementation ***********************************************/


/* pp_heap_all(N) <-
      pretty_prints the heap given by the asserted atoms for the
      predicate a/3, 
      first the size N of the heap is written as 
         heap_size(N)
      then for each atom a(I,Item,Weight) a line
         a[I] = (Item,Weight) 
      is written. */

pp_heap_all(0) :- 
   !,
   listing(heap_size), 
   !.
pp_heap_all(I) :-
   II is I - 1,
   pp_heap_all(II),
   a(I,X,G),
   write('a['), write(I), write('] = ('), 
   write(X), write(','), write(G), writeln(')').
   
ud_insert(I,X,G,AG) :-
   G > AG,
   down_insert(I,X,G).
ud_insert(I,X,G,_) :-
   up_insert(I,X,G).

up_insert(1,X,G) :-
   assert(a(1,X,G)).
up_insert(I,X,G) :-
   IV is I // 2,
   a(IV,_,GV),
   GV < G,
   assert(a(I,X,G)).
up_insert(I,X,G) :-
   IV is I//2,
   retract(a(IV,XV,GV)),
   assert(a(I,XV,GV)),
   up_insert(IV,X,G).

down_insert(I,X,G) :-
   heap_size(N),
   H is 2 * I,
   H > N,
   assert(a(I,X,G)).
down_insert(I,X,G) :-
   change_with(I,IS),
   a(IS,XS,GS),
   GS < G,
   retract(a(IS,_,_)),
   assert(a(I,XS,GS)),
   down_insert(IS,X,G).
down_insert(I,X,G) :-
   assert(a(I,X,G)).

change_with(I,I2) :-
   I1 is 2 * I,
   I2 is I1 +1,
   a(I2,_,G2),
   a(I1,_,G1),
   G2 < G1.
change_with(I,IS) :-
   IS is 2 * I.


/******************************************************************/


