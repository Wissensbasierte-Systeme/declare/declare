

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Lattice Handling for Leda               ***/
/***                                                            ***/
/******************************************************************/



/*** data files ***************************************************/
  
 
leda_input_file('../steiner/graphein').
leda_exact_output_file('../steiner/graphaus.exact').
leda_wellen_output_file('../steiner/graphaus').
leda_mst_output_file('../steiner/graphaus').
leda_gmst_output_file('../steiner/graphaus').


/*** interface ****************************************************/


er :-
   er(2).
wr :-
   wr(2).
mr :-
   mr(2).
gmr :-
   gmr(2).

er(Mode) :-
   complete_lattice(Mode), leda_pp_graph,
   leda_steiner_exact, leda_exact_output_file(File), 
   leda_tree_ratio(File).

wr(Mode) :-
   complete_lattice(Mode), leda_pp_graph,
   leda_steiner_wellen, leda_wellen_output_file(File), 
   leda_tree_ratio(File).

mr(Mode) :-
   complete_lattice(Mode), leda_pp_graph,
   leda_steiner_mst, leda_mst_output_file(File), 
   leda_tree_ratio(File).

gmr(Mode) :-
   complete_lattice(Mode), leda_pp_graph,
   leda_steiner_gmst, leda_gmst_output_file(File), 
   leda_tree_ratio(File).

leda_tree_ratio(File) :-
   d_facts_volume(Volume1),
   steiner_tree_volume(File,Volume2),
   K is Volume1 / Volume2,
   nl, write('c_r = '), format("~2f",K), writeln(' '),
   !.

steiner_tree_volume(File,Volume) :-
   see(File),
   readword(_,46), readword(_,10),
   readword(int,10), readword(int,10),
   readword(N,10), read_n_numbers(N),
   readword(M,10), sum_up_m_third_positions(M,Volume),
   seen.
 

/* comment

leda_steiner_exact :-
   unix(system('cp steiner/graphein randomgraph; \
      steiner/exact > /dev/null; \
      mv randomgraph steiner/.; \
      mv graphaus.exact steiner/.; \
      mv GEaus steiner/.')).

leda_steiner_wellen :-
   unix(system('cp steiner/graphein .; \
      steiner/wellen; \
      rm graphein; mv graphaus steiner/.; \
      mv GEaus steiner/.')).

leda_steiner_mst :-
   unix(system('cp steiner/graphein .; \
      steiner/mst; \
      rm graphein; mv graphaus steiner/.; \
      mv GEaus steiner/.')).
 
leda_steiner_gmst :-
   unix(system('cp steiner/graphein .; \
      steiner/gmst; \
      rm graphein; mv graphaus steiner/.; \
      mv GEaus steiner/.')).
*/

 
/*** implementation ***********************************************/


/* leda_pp_graph <-
      given a hasse diagram as a set of asserted facts
      d_fact(Fact1), i_fact(Fact2), 
      the nodes of the hasse diagram are numbered and the nodes and 
      edges are written inot a file that can be used as input for 
      Leda, the nodes are denoted by their numbers, which can be
      derived by node_number(Node,I), and they are marked as 
      required or steiner nodes. */
 
leda_pp_graph :-
   assert_all_hasse_nodes, hasse_cardinalities,
   retractall(required_node(_,_)), retractall(steiner_node(_,_)), 
   leda_input_file(File), switch(F,File),
   writeln('LEDA.GRAPH'),
   writeln(int), writeln(int),
   leda_pp_nodes,
%  rev_leda_pp_nodes,
   leda_pp_edges,
   switch(F),
   retractall(hasse_node(_)), 
   retractall(cardinality_with_hasse_nodes(_,_)),
   retractall(i_fact(_)).


/* leda_pp_nodes <-
      given a hasse diagram as a set of asserted facts 
      d_fact(Fact1), i_fact(Fact2),
      corresponding facts required_node(Fact1,I1) and 
      steiner_node(Fact2,I2) are asserted with different numbers Ii,
      the nodes of the hasse diagram are written inot a file that 
      can be used as input for Leda,
      the nodes are marked as required or steiner nodes. */

leda_pp_nodes :-
   collect_arguments(d_fact,Nodes1), 
   Required_Nodes = [[]|Nodes1],
   length(Required_Nodes,I),
   number_items(1,Required_Nodes,Numbered_Required_Nodes),
   assert_arguments_generic(required_node,Numbered_Required_Nodes),
   collect_arguments(i_fact,Nodes2),
   list_to_ord_set(Nodes2,Nodes3),
   ord_del_element(Nodes3,[],Steiner_Nodes),
   length(Steiner_Nodes,J),
   II is I + 1,
   number_items(II,Steiner_Nodes,Numbered_Steiner_Nodes),
   assert_arguments_generic(steiner_node,Numbered_Steiner_Nodes),
   K is I + J, writeln(K),
   leda_pp_required_nodes,
   leda_pp_steiner_nodes.

rev_leda_pp_nodes :-
   number_items(1,[[]],Numbered_Required_Nodes_1),
   assert_arguments_generic(required_node,Numbered_Required_Nodes_1),
   collect_arguments(i_fact,Nodes2),
   list_to_ord_set(Nodes2,Nodes3),
   ord_del_element(Nodes3,[],Steiner_Nodes),
   length(Steiner_Nodes,J),
   number_items(2,Steiner_Nodes,Numbered_Steiner_Nodes),
   assert_arguments_generic(steiner_node,Numbered_Steiner_Nodes),
   collect_arguments(d_fact,Required_Nodes),
   length(Required_Nodes,I),
   II is J + 2,
   number_items(II,Required_Nodes,Numbered_Required_Nodes),
   assert_arguments_generic(required_node,Numbered_Required_Nodes),
   K is I + J + 1, writeln(K),
   writeln('0'),
   leda_pp_steiner_nodes,
   retract(required_node([],1)),
   leda_pp_required_nodes,
   assert(required_node([],1)).

number_items(I,[H|T],[[H,I]|NT]) :-
   J is I + 1,
   number_items(J,T,NT).
number_items(_,[],[]).

leda_pp_required_nodes :-
   required_node(_,_),
   writeln('0'),
   fail.
leda_pp_required_nodes.

leda_pp_steiner_nodes :-
   steiner_node(_,_),
   writeln('1'),
   fail.
leda_pp_steiner_nodes.


/* leda_pp_edges <-
      given a hasse diagram as a set of asserted facts 
      d_fact(Fact), i_fact(Fact), hasse_node(Node),
      cardinality_with_hasse_nodes(I,Nodes),
      required_node(Node,I), steiner_node(Node,I),
      the edges of the hasse diagram are written inot a file 
      that can be used as input for Leda. */

leda_pp_edges :-
   findall( [Node,Successors],
      ( required_node(Node,_),
        hasse_successors_opt(Node,Successors) ),
      Required_Adjacency_Lists ),
   findall( [Node,Successors],
      ( steiner_node(Node,_), 
        hasse_successors_opt(Node,Successors) ),
      Steiner_Adjacency_Lists ),
   compute_leda_edges(Required_Adjacency_Lists,Required_Edges),
   length(Required_Edges,I),
   compute_leda_edges(Steiner_Adjacency_Lists,Steiner_Edges),
   length(Steiner_Edges,J),
   K is I + J, writeln(K),
   leda_pp_edges(Required_Edges), leda_pp_edges(Steiner_Edges).

  
/* leda_pp_edges(Edges) <-
      pretty prints the edges [I,J,K] in Edges in an input file
      for Leda. */

leda_pp_edges([[I,J,K]|Edges]) :-
   write(I), write(' '),
   write(J), write(' '),
   writeln(K),
   leda_pp_edges(Edges).
leda_pp_edges([]).


/* compute_leda_edges(Adjacency_Lists,Leda_Edges) <-
      the adjacency lists [Node,Nodes] in Adjacency_Lists
      are transformed according to the node numbering f given by
      the asserted facts required_node(Node,I) and steiner_node(Node,I),
      i.e. f(Node) = I and
      f([Node,[N1,...,Nn]]) = [f(Node),[f(N1),...,f(Nn)]]. */
 
compute_leda_edges(Adjacency_Lists,Leda_Edges) :-
   compute_leda_edges(Adjacency_Lists,[],Leda_Edges).

compute_leda_edges([H|T],Sofar,Leda_Edges) :-
   compute_leda_edges_loop(H,Edges),
   append(Edges,Sofar,New_Sofar),
   compute_leda_edges(T,New_Sofar,Leda_Edges).
compute_leda_edges([],Leda_Edges,Leda_Edges).

compute_leda_edges_loop([Node,Nodes],Edges) :-
   node_number(Node,I),
   compute_leda_edges_loop([Node,Nodes],I,[],Edges).

compute_leda_edges_loop([Node,[N|Ns]],I,Sofar,Edges) :-
   node_number(N,J),
   ord_subtract(N,Node,Diff), length(Diff,K),
   compute_leda_edges_loop([Node,Ns],I,[[I,J,K]|Sofar],Edges).
compute_leda_edges_loop([_,[]],_,Edges,Edges).
   
node_number(Node,I) :-
   required_node(Node,I).
node_number(Node,I) :-
   steiner_node(Node,I).


readword(X,Y) :-
   readword(96,X,Y).
 
read_n_numbers(N) :-
   N > 0,
   !,
   readword(_,_),
   M is N - 1,
   read_n_numbers(M).
read_n_numbers(0).
 
sum_up_m_third_positions(M,S) :-
   M > 0,
   !,
   readword(_,32), readword(_,32), readword(S1,10),
   N is M - 1,
   sum_up_m_third_positions(N,S2),
   S is S1 + S2.
sum_up_m_third_positions(0,0).


/******************************************************************/


