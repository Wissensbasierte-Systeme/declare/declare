

/******************************************************************/
/***                                                            ***/
/***             Colin Barker:  Dijkstra Edges                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dijkstra_graph_barker(Graph) <-
      */

dijkstra_graph_barker(Graph) :-
   findall( U-(V-W),
      dijkstra_edge_barker(U, V, W),
      Graph ).


/* dijkstra_edge_barker(X, Y, Z) <-
      */

dijkstra_edge_barker(X, Y, Z) :-
   dijkstra_distance_barker(X, Y, Z).
dijkstra_edge_barker(X, Y, Z) :-
   dijkstra_distance_barker(Y, X, Z).


/* dijkstra_distance_barker(X, Y, D) <-
      a subset of the data from EXAMPLES\SALESMAN.PL
      in LPA Win-Prolog */

dijkstra_distance_barker(aberdeen,    edinburgh,   115).
dijkstra_distance_barker(aberdeen,    glasgow,     142).
dijkstra_distance_barker(aberystwyth, birmingham,  114).
dijkstra_distance_barker(aberystwyth, cardiff,     108).
dijkstra_distance_barker(aberystwyth, liverpool,   100).
dijkstra_distance_barker(aberystwyth, nottingham,  154).
dijkstra_distance_barker(aberystwyth, sheffield,   154).
dijkstra_distance_barker(aberystwyth, swansea,      75).
dijkstra_distance_barker(birmingham,  bristol,      86).
dijkstra_distance_barker(birmingham,  cambridge,    97).
dijkstra_distance_barker(birmingham,  cardiff,     100).
dijkstra_distance_barker(birmingham,  liverpool,    99).
dijkstra_distance_barker(birmingham,  manchester,   80).
dijkstra_distance_barker(birmingham,  nottingham,   48).
dijkstra_distance_barker(birmingham,  oxford,       63).
dijkstra_distance_barker(birmingham,  sheffield,    75).
dijkstra_distance_barker(birmingham,  swansea,     125).
dijkstra_distance_barker(brighton,    bristol,     136).
dijkstra_distance_barker(brighton,    dover,        81).
dijkstra_distance_barker(brighton,    oxford,       96).
dijkstra_distance_barker(brighton,    portsmouth,   49).
dijkstra_distance_barker(brighton,    london,       52).
dijkstra_distance_barker(bristol,     exeter,       76).
dijkstra_distance_barker(bristol,     oxford,       71).
dijkstra_distance_barker(bristol,     portsmouth,   97).
dijkstra_distance_barker(bristol,     swansea,      89).
dijkstra_distance_barker(bristol,     london,      116).
dijkstra_distance_barker(cambridge,   nottingham,   82).
dijkstra_distance_barker(cambridge,   oxford,       80).
dijkstra_distance_barker(cambridge,   london,       54).
dijkstra_distance_barker(cardiff,     swansea,      45).
dijkstra_distance_barker(carlisle,    edinburgh,    93).
dijkstra_distance_barker(carlisle,    glasgow,      94).
dijkstra_distance_barker(carlisle,    leeds,       117).
dijkstra_distance_barker(carlisle,    liverpool,   118).
dijkstra_distance_barker(carlisle,    manchester,  120).
dijkstra_distance_barker(carlisle,    newcastle,    58).
dijkstra_distance_barker(carlisle,    york,        112).
dijkstra_distance_barker(dover,       london,       71).
dijkstra_distance_barker(edinburgh,   glasgow,      44).
dijkstra_distance_barker(edinburgh,   newcastle,   104).
dijkstra_distance_barker(exeter,      penzance,    112).
dijkstra_distance_barker(exeter,      portsmouth,  126).
dijkstra_distance_barker(glasgow,     newcastle,   148).
dijkstra_distance_barker(hull,        leeds,        58).
dijkstra_distance_barker(hull,        nottingham,   90).
dijkstra_distance_barker(hull,        sheffield,    65).
dijkstra_distance_barker(hull,        york,         37).
dijkstra_distance_barker(leeds,       manchester,   41).
dijkstra_distance_barker(leeds,       newcastle,    89).
dijkstra_distance_barker(leeds,       sheffield,    34).
dijkstra_distance_barker(leeds,       york,         23).
dijkstra_distance_barker(liverpool,   manchester,   35).
dijkstra_distance_barker(liverpool,   nottingham,  100).
dijkstra_distance_barker(liverpool,   sheffield,    70).
dijkstra_distance_barker(manchester,  newcastle,   130).
dijkstra_distance_barker(manchester,  sheffield,    38).
dijkstra_distance_barker(newcastle,   york,         80).
dijkstra_distance_barker(nottingham,  sheffield,    38).
dijkstra_distance_barker(oxford,      london,       57).


/******************************************************************/


