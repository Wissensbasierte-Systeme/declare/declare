

/******************************************************************/
/***                                                            ***/
/***             DDK:  Dijkstra Algorithm                       ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      heap_visited/1, dijkstra_edge/3,
      dijkstra_expand_node/2.

:- multifile
      dijkstra_expand_node/2.


/*** tests ********************************************************/


test(dijkstra_find_path, ddk_algorithm) :-
   Source = london,
%  Target = liverpool,
   Target = cardiff,
   dijkstra_graph_barker(Graph),
   Goal = dijkstra_find_path(Graph, Source, Target, Cost:Path),
   measure_until_measurable(Goal, Time),
   write(Time), writeln(' sec'),
   writeln(user, Cost:Path).
test(dijkstra_find_path, barker_algorithm) :-
   Source = london,
%  Target = liverpool,
   Target = cardiff,
   dijkstra_graph_barker(Graph),
   Goal = dijkstra_find_path_barker(
      Graph, Source, Target, Cost:Path),
   measure_until_measurable(Goal, Time),
   write(Time), writeln(' sec'),
   writeln(user, Cost:Path).
test(dijkstra_find_path, 3) :-
   dijkstra_find_path([a-(b-1), b-(c-2)], a, c, Cost:Path),
   writeln(user, Cost:Path).


/*** interface ****************************************************/


/* dijkstra_find_path(Graph, Source, Target, Cost:Path) <-
      */

dijkstra_find_path(Graph, Source, Target, Cost:Path) :-
   dabolish(dijkstra_edge/3),
   ( foreach(U-(V-W), Graph) do
        assert(dijkstra_edge(U, V, W)) ),
   dijkstra_find_path(Source, Target, Cost:Path),
   dabolish(dijkstra_edge/3).


/* dijkstra_find_path(Source, Target, Cost:Path) <-
      */

dijkstra_find_path(Source, Target, Cost:Path) :-
   dabolish(heap_visited/1),
   empty_heap(Heap_1),
   add_to_heap(Heap_1, 0, [Source], Heap_2),
   dijkstra_find_path_loop(Target, Heap_2, Cost:Path_),
   reverse(Path_, Path),
   dabolish(heap_visited/1).

dijkstra_find_path_loop(Target, Heap, Cost:Path) :-
   heap_delete_minimum(Heap, Cost:[Target|Vs], _),
   !,
   Path = [Target|Vs].
dijkstra_find_path_loop(Target, Heap, Cost:Path) :-
   dijkstra_expand_node(Heap, Heap_2),
   dijkstra_find_path_loop(Target, Heap_2, Cost:Path).


/* dijkstra_expand_node(Heap_1, Heap_2) <-
      */

dijkstra_expand_node(Heap_1, Heap_2) :-
   heap_delete_minimum(Heap_1, Cost_1:[V|Vs], Heap_3),
   findall( Cost_2:[W,V|Vs],
      ( dijkstra_edge(V, W, Dist),
        Cost_2 is Cost_1 + Dist ),
      Pairs ),
   heap_insert_list(Pairs, Heap_3, Heap_2).


/*** implementation ***********************************************/


/* heap_delete_minimum(Heap_1, Cost:Path, Heap_2) <-
      */

heap_delete_minimum(Heap_1, Cost:Path, Heap_2) :-
   get_from_heap(Heap_1, C, [V|Vs], Heap_3),
   ( \+ heap_visited(V) ->
     Cost = C,
     Path = [V|Vs],
     Heap_2 = Heap_3,
     assert(heap_visited(V))
   ; heap_delete_minimum(Heap_3, Cost:Path, Heap_2) ).


/* heap_insert_list(Pairs, Heap_1, Heap_2) <-
      */

heap_insert_list([Pair|Pairs], Heap_1, Heap_2) :-
   heap_decrease_key(Pair, Heap_1, Heap_3),
   heap_insert_list(Pairs, Heap_3, Heap_2).
heap_insert_list([], Heap, Heap).


/* heap_decrease_key(C:Vs, Heap_1, Heap_2) <-
      */

heap_decrease_key(C:Vs, Heap_1, Heap_2) :-
   add_to_heap(Heap_1, C, Vs, Heap_2).


/*** data *********************************************************/


/* dijkstra_edge(V, W, Dist) <-
      */

/*
dijkstra_edge(a, c, 3).
dijkstra_edge(a, b, 1).
dijkstra_edge(b, c, 1).
*/


/******************************************************************/


