

/******************************************************************/
/***                                                            ***/
/***             DisLog:  U-Graphs                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* edges_to_transitive_closure(Edges, Tc_Edges) <-
      */

edges_to_transitive_closure(Edges, Tc_Edges) :-
   transitive_closure_edges_swi(Edges, Tc_Edges).

transitive_closure_edges_swi(Edges, Tc_Edges) :-
   edges_to_ugraph(Edges, Graph),
   transitive_closure(Graph, Tc_Graph),
   ugraph_to_edges(Tc_Graph, Tc_Edges).


/* vertices_edges_reduce(
         Vertices_1, Edges_1, Vertices_2, Edges_2) <-
      */

vertices_edges_reduce(
      Vertices_1, Edges_1, Vertices_2, Edges_2) :-
   vertices_edges_to_ugraph(Vertices_1, Edges_1, Graph_1),
   reduce(Graph_1, Graph_2),
   ugraph_to_vertices_edges(Graph_2, Vertices_2, Edges_2).


/* ugraph_to_vertices_edges(Graph, Vertices, Edges) <-
      */

ugraph_to_vertices_edges(Graph, Vertices, Edges) :-
   vertices(Graph, Vertices),
   edges(Graph, Edges).


/* edges_to_transitive_reduction(Edges_1, Edges_2) <-
      */

edges_to_transitive_reduction(Edges_1, Edges_2) :-
   edges_to_ugraph(Edges_1, Graph_1),
   reduce(Graph_1, Graph_2),
   ugraph_to_transitive_reduction(Graph_2, Graph_3),
   findall( X-Y,
      ( member(X-Xs, Graph_3),
        member(Y, Xs) ),
      Edges_3 ),
   findall( X-[],
      ( member(X-[], Graph_3),
        \+ ( member(_-Xs, Graph_3),
             member(X, Xs) ) ),
      Edges_4 ),
   append(Edges_4, Edges_3, Edges_2).


/* ugraph_to_transitive_reduction(Graph_1, Graph_2) <-
      */

ugraph_to_transitive_reduction(Graph_1, Graph_2) :-
   transitive_closure(Graph_1, Graph_3),
   maplist( ugraph_to_transitive_reduction_sub(Graph_3),
      Graph_1, Graph_2 ).

ugraph_to_transitive_reduction_sub(Graph, V-Ws1, V-Ws2) :-
   findall( W,
      ( member(W, Ws1),
        \+ ( member(U, Ws1), U \= V, U \= W,
             member(U-Us, Graph),
             member(W, Us) ) ),
      Ws2 ).


/* edges_to_transitive_reduction_acyclic(Edges_1, Edges_2) <-
      */

edges_to_transitive_reduction_acyclic(Edges_1, Edges_2) :-
   findall( C1-C2,
      ( member(C1-C2, Edges_1),
        \+ ( member(C1-C, Edges_1), C \= C1, C \= C2,
             member(C-C2, Edges_1) ) ),
      Edges_2 ).


/* edges_to_reduced_ugraph_to_trees(Edges, Trees) <-
      */

edges_to_reduced_ugraph_to_trees(Edges, Trees) :-
   edges_to_ugraph(Edges, U_Graph),
   reduce(U_Graph, Reduced_U_Graph),
   ugraph_to_root_nodes(Reduced_U_Graph, Roots),
   ugraph_to_trees_2(Reduced_U_Graph, Roots, Trees).

ugraph_to_root_nodes(U_Graph, Roots) :-
   findall( Root,
      ugraph_to_root_node(U_Graph, Root),
      Roots ).

ugraph_to_root_node(U_Graph, Root) :-
   member(Root-_, U_Graph),
   \+ ( member(_-Vs, U_Graph), member(Root, Vs) ).


/* ugraph_to_edges(Graph, Edges) <-
      */

ugraph_to_edges(Graph, Edges) :-
   edges(Graph, Edges).
%  findall( V-W,
%     ( member(V-Ws, Graph),
%       member(W, Ws) ),
%     Edges ).


/* edges_to_ugraph(Edges, Graph) <-
      */

edges_to_ugraph(Edges, Graph) :-
   edges_to_vertices(Edges, Vertices),
   vertices_edges_to_ugraph(Vertices, Edges, Graph).


/* edges_to_vertices(Edges, Vertices) <-
      */

edges_to_vertices(Edges, Vertices) :-
   findall( V,
      ( member(Edge, Edges),
        edge_to_vertex(Edge, V) ),
      Vs ),
   list_to_ord_set(Vs, Vertices).

edge_to_vertex(Edge, Vertex) :-
   minus_separated_list_to_list(Edge, [V, W|_]),
   !,
   member(Vertex, [V, W]).


/* ugraph_to_tree(Graph, Vertex, Tree) <-
      */

ugraph_to_tree(Graph, Vertex, [Vertex|Trees]) :-
   neighbours(Vertex, Graph, Neighbours),
   make_edges(Vertex-Neighbours, Edges),
   del_edges(Graph, Edges, Graph_2),
   ugraph_to_trees(Graph_2, Neighbours, Trees).

make_edges(Vertex-[V|Vs], [V-Vertex, Vertex-V|Edges]) :-
   make_edges(Vertex-Vs, Edges).
make_edges(_-[], []).


/* ugraph_to_trees(Graph, Vertices, Tress) <-
      */

ugraph_to_trees(Graph, Vertices, Tress) :-
   maplist( ugraph_to_tree(Graph),
      Vertices, Tress ).


/* ugraph_to_trees_2(U_Graph, Roots, Trees) <-
      */

ugraph_to_trees_2(U_Graph, Roots, Trees) :-
   ugraph_to_trees_2(U_Graph, Roots, [], _, Trees).

ugraph_to_trees_2(U_Graph, [R|Rs], Visited, Visited_2, [T|Ts]) :-
   writeln(uuu, visit(R)),
   ugraph_to_tree_2(U_Graph, R, Visited, Visited_1, T),
   ugraph_to_trees_2(U_Graph, Rs, [R|Visited_1],Visited_2, Ts).
ugraph_to_trees_2(_, [], Visited, Visited, []).


ugraph_to_tree_2(U_Graph, Root, Tree) :-
   ugraph_to_tree_2(U_Graph, Root, [], _, Tree).

ugraph_to_tree_2(_, Root, Visited, Visited, [Representative]) :-
   member(Root, Visited),
   first(Root, Representative),
%  writeln(uuu, already_visited(Root)),
   !.
ugraph_to_tree_2(U_Graph, Root, Visited, Visited_2,
      [Root|Trees_2]) :-
%  writeln(uuu, neighbours(Vs)),
   neighbours(Root, U_Graph, Vs),
%  writeln(uuu, neighbours(Vs)),
   delete(U_Graph, Root-Vs, U_Graph_2),
   !,
   ugraph_to_trees_2(U_Graph_2, Vs, Visited, Visited_2, Trees),
   remove_empty_trees(Trees, Trees_2).
ugraph_to_tree_2(_, Root, Visited, Visited, [Root]).


/* reduced_trees_to_trees(Graph, Trees_1, Trees_2) <-
      */

reduced_trees_to_trees(Graph, Trees_1, Trees_2) :-
   maplist( reduced_tree_to_tree(Graph),
      Trees_1, Trees_2 ).

reduced_tree_to_tree(Graph, [Vs|Trees_1], [Tree_2|Trees_2]) :-
   writeln(hello(Vs)),
   Vs = [Root|_],
   findall( N-Ns1,
      ( member(N, Vs), member(N-Ns, Graph),
        intersection(Ns, Vs, Ns1), Ns1 \== [] ),
      U_Graph ),
   ugraph_to_tree(U_Graph, Root, Tree_2),
   writeln(ugraph_to_tree(U_Graph, Root, Tree_2)),
   reduced_trees_to_trees(Graph, Trees_1, Trees_2).
reduced_tree_to_tree(_, [], []).


/*** tests ********************************************************/


test(ugraphs, 1) :-
   Edges = [a-b, a-c, b-c, c-d, c-e, e-f, f-c, e-g],
   edges_to_ugraph(Edges, U_Graph),
   ugraph_to_tree_2(U_Graph, a, Tree),
   writeln(Tree),
   dportray(tree, Tree).

test(ugraphs, 2) :-
   Edges = [a-b, a-c, b-c, c-d, c-e, e-f, f-c, e-g],
   maplist( edge_to_packed_edge,
      Edges, Edges_2 ),
   edges_to_ugraph(Edges_2, U_Graph),
   writeln(edges_to_ugraph(Edges_2, U_Graph)),
   ugraph_to_tree_2(U_Graph, [a], Tree),
   writeln(ugraph_to_tree_2(U_Graph, [a], Tree)),
   dportray(tree, Tree),
   trees_to_html_file([Tree], xxx).

test(ugraphs, transitive_closure_edges_swi) :-
   Edges = [a-b, b-c, c-b, c-d],
   transitive_closure_edges_swi(Edges, Tc_Edges),
   Tc_Edges = [a-b, a-c, a-d, b-c, b-d, c-b, c-d].


/******************************************************************/


