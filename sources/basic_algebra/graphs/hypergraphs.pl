

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Hypergraphs                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* graham_reduction_algorithm(Hypergraph1,Hypergraph2) <-
      the reduction algorithm of Graham is applied to the
      hypergraph Hypergraph1 and yields the result Hypergraph2,
      if Hypergraph2 is empty, then Hypergraph1 is 
      alpha-acyclic. */

graham_reduction_algorithm([],[]) :-
   show_hypergraphs(graham_reduction_algorithm,[],[]),
   !.
graham_reduction_algorithm(Hypergraph1,Hypergraph3) :-
   show_hypergraphs(graham_reduction_algorithm,Hypergraph1,Hypergraph3),
   append(X1,[_-Edge1|Y1],Hypergraph1),
   Edge1 = [],
   append(X1,Y1,Hypergraph2),
%  writeln('after append'),
   !,
%  writeln(Hypergraph2),
   graham_reduction_algorithm(Hypergraph2,Hypergraph3).
graham_reduction_algorithm(Hypergraph1,Hypergraph3) :-
   show_hypergraphs(graham_reduction_algorithm,Hypergraph1,Hypergraph3),
   append(X1,[N1-Edge1|Y1],Hypergraph1),
   append(X1,Y1,Hypergraph),
   graham_reduce_edge(Edge1,Edge,Hypergraph),
   append(X1,[N1-Edge|Y1],Hypergraph2),
   !,
%  writeln(Hypergraph2),
   graham_reduction_algorithm(Hypergraph2,Hypergraph3).
graham_reduction_algorithm(Hypergraph1,Hypergraph3) :-
   show_hypergraphs(graham_reduction_algorithm,Hypergraph1,Hypergraph3),
   append(X1,[N1-Edge1|Y1],Hypergraph1),
   member(N2-Edge2,Hypergraph1),
   N1 \== N2,
%  writeln('before ord_subset'),
   ord_subset(Edge1,Edge2),
%  writeln('after ord_subset'),
   append(X1,Y1,Hypergraph2),
   !,
   write('--- join tree edge ---     '),
   write(N2), write(' -> '), writeln(N1),
%  writeln(Hypergraph2),
   graham_reduction_algorithm(Hypergraph2,Hypergraph3).
graham_reduction_algorithm(Hypergraph1,Hypergraph1) :-
   writeln('terminated').



/*** implementation ***********************************************/


graham_reduce_edge(Edge1,Edge2,Hypergraph) :-
%  writeln('graham_reduce_edge'),
   graham_reduce_edge_loop(Edge1,Edge,Hypergraph),
   ord_subtract(Edge1,Edge,Edge2),
   Edge1 \== Edge2.

graham_reduce_edge_loop([],[],_) :-
   !.
graham_reduce_edge_loop(Edge1,Edge3,[_-Edge|Hypergraph]) :-
   ord_subtract(Edge1,Edge,Edge2),
   graham_reduce_edge_loop(Edge2,Edge3,Hypergraph).
graham_reduce_edge_loop(Edge1,Edge1,[]).

show_hypergraphs(_,_,_) :-
   !.
show_hypergraphs(Text,Hypergraph1,Hypergraph2) :-
   write(Text), write('('),
   write(Hypergraph1), write(','),
   write(Hypergraph2), writeln(')').


/******************************************************************/


