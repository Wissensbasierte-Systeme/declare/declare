

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Graph Display with `edge'               ***/
/***                                                            ***/ 
/******************************************************************/ 
 

:- dynamic 
      i_fact/1, 
      hasse_node/1.


/*** files ********************************************************/
  
 
lattice_file('results/lattice.grl').


/*** interface ****************************************************/


/* grl_pp_graph(Edges) <-
      the binary, directed graph given by the edges in Edges
      is displayed by the tool edge. */


grl_pp_graph(Edges) :-
   lattice_file(File), switch(F,File),
   ord_union(Edges,List),
   list_to_ord_set(List,Nodes),
   grl_pp_file_header(graph),
   nl, writeln('  /* list of nodes */ '), nl,
   grl_pp_graph_nodes(Nodes),
   nl, writeln('  /* list of edges */ '), nl,
   grl_pp_graph_edges(Edges,
      'thickness: 1   arrowstyle: none'),
   grl_pp_file_tail,
   switch(F),
   edge.

grl_pp_labeled_graph(Edges) :-
   lattice_file(File), switch(F,File),
   labeled_edges_to_nodes(Edges,Nodes,Label_Nodes),
   grl_pp_file_header(graph),
   nl, writeln('  /* list of nodes */ '), nl,
   grl_pp_graph_nodes(Nodes),
   grl_pp_graph_nodes_labeled(Label_Nodes),
   nl, writeln('  /* list of edges */ '), nl,
   grl_pp_labeled_graph_edges(
      'thickness: 1   arrowstyle: none'-'thickness: 1',
      Edges),
   grl_pp_file_tail,
   switch(F),
   edge.
   

labeled_edges_to_nodes(Edges,Nodes,Label_Nodes) :-
   labeled_edges_to_nodes(Edges,[],N_List,[],L_List),
   list_to_ord_set(N_List,Nodes),
   list_to_ord_set(L_List,Label_Nodes).
   
labeled_edges_to_nodes([[A,B,C]|Edges],
      N_Sofar,Nodes,L_Sofar,L_Nodes) :-
   append([A,B],N_Sofar,New_N_Sofar),
   New_L_Sofar = [[A,B,C]-C|L_Sofar],
   labeled_edges_to_nodes(Edges,
      New_N_Sofar,Nodes,New_L_Sofar,L_Nodes).
labeled_edges_to_nodes([],
      Nodes,Nodes,Label_Nodes,Label_Nodes).


grl_pp_dependency_graph(Nodes,
      Equal_Edges,Positive_Edges,Negative_Edges) :-
   lattice_file(File), switch(F,File),
   grl_pp_file_header(graph),
   nl, writeln('  /* list of nodes */ '), nl,
   grl_pp_graph_nodes(Nodes),
   nl, writeln('  /* list of edges */ '), nl,
   grl_pp_graph_edges(Equal_Edges,
      'linestyle: dotted   arrowstyle: none'),
   grl_pp_graph_edges(Positive_Edges,
      'thickness: 1   label: +   linestyle: continuous'),
   grl_pp_graph_edges(Negative_Edges,
      'thickness: 1   label: -   linestyle: dashed'),
   grl_pp_file_tail,
   switch(F),
   edge.


/* grl_pp_tree_lattice(Size,Tree) <-
      a set Tree of trees is displayed by the tool edge in the
      size Size. */

grl_pp_trees(Trees) :-
   lattice_file(File), switch(F,File),
   grl_pp_file_header(tree),
   grl_pp_trees(0,Trees),
   grl_pp_file_tail,
   switch(F),
   edge(cross).


/* grl_pp_state_lattice(Size,State) <-
      the completed Hasse diagram of the d-state State 
      is displayed by the tool edge in the size Size. */

grl_pp_state_lattice(Size,State) :-
   dabolish(d_fact/1),
   assert_arguments(d_fact,State),
   grl_pp_state_lattice(Size),
   dabolish(d_fact/1).
 
grl_pp_state_lattice(Size) :-
   complete_lattice(state),
   grl_pp_state_lattice,
   edge(Size).

dl :-
   grl_pp_state_lattice(normal).


/* grl_pp_tree_lattice(Size,Tree) <-
      the clause tree Tree is displayed by the tool edge in the
      size Size. */

grl_pp_tree_lattice(Size,Tree) :-
   dabolish(d_fact/1),
   assert(d_fact_tree(Tree)),
   grl_pp_tree_lattice(Size),
   retract(d_fact_tree(Tree)),
   dabolish(d_fact/1).

grl_pp_tree_lattice(Size) :-
   nl,
   grl_pp_tree_lattice,
   edge(Size).

dtl :-
   grl_pp_tree_lattice(normal).


/* grl_pp_tree_lattices(Size,Trees) <-
      a set Trees of trees is displayed by edge. */

grl_pp_tree_lattices(Size,Trees) :-
   grl_pp_tree_lattices(Trees),
   edge(Size).

grl_pp_tree_lattices(Trees) :-
   lattice_file(File), switch(F,File),
   grl_pp_file_header(tree),
   grl_pp_d_fact_trees(0,Trees),
   grl_pp_file_tail,
   switch(F).


/* edge(Size) <- 
      displays the file 'results/lattice.grl' by the tool edge
      in the size Size. */

edge :- 
   edge(normal).

edge(Mode) :- 
   edge_geometry(Mode,Geometry),
   lattice_file(Lattice_File),
   concat(['$EDGETOOLS/edge -display $HOST:0.0 -geometry ',
      Geometry,' ',Lattice_File,' > /dev/null 2> /dev/null &'],
      Command),
   unix(system(Command)).

edge_geometry(normal,'1000x700+50+60').
edge_geometry(cross,'1000x550+50+60').
edge_geometry(small,'500x500+200+100').


/*** implementation ***********************************************/


grl_pp_graph_nodes([Node|Nodes]) :-
   grl_pp_graph_node_labeled(Node,Node,'borderwidth: 1'),
   grl_pp_graph_nodes(Nodes).
grl_pp_graph_nodes([]).

grl_pp_graph_node(Node,Mode) :-
   write('  node: { title: "'), write(Node), writeln('"'),
   write('          '), write(Mode), writeln(' }').


grl_pp_graph_nodes_labeled(Nodes_Labels) :-
   grl_pp_graph_nodes_labeled(Nodes_Labels,'borderwidth: 0').

grl_pp_graph_nodes_labeled([Node-Label|Nodes_Labels],Mode) :-
   grl_pp_graph_node_labeled(Node,Label,Mode),
   grl_pp_graph_nodes_labeled(Nodes_Labels,Mode).
grl_pp_graph_nodes_labeled([],_).

grl_pp_graph_node_labeled(Node,Label,Mode) :-
   write('  node: { title: "'), write(Node),
   write('"   label: "'), write(Label), writeln('"'),
   write('          '), write(Mode), writeln(' }').


grl_pp_graph_edges([Edge|Edges],Mode) :-
   grl_pp_graph_edge(Edge,Mode),
   grl_pp_graph_edges(Edges,Mode).
grl_pp_graph_edges([],_).

grl_pp_graph_edge([A,B],Mode) :-
   !,
   write('  edge: { sourcename: "'), write(A),
   write('"   targetname: "'), write(B), writeln('"'),
   write('          '), write(Mode), writeln(' }').
grl_pp_graph_edge(_,_).


grl_pp_labeled_graph_edges(Mode,Edges) :-
   checklist( grl_pp_labeled_graph_edge(Mode),
      Edges ).

grl_pp_labeled_graph_edge(Mode1-Mode2,[A,B,C]) :-
   !,
   write('  edge: { sourcename: "'), write(A),
   write('"   targetname: "'), write([A,B,C]), writeln('"'),
   write('          '), write(Mode1), writeln(' }'),
   write('  edge: { sourcename: "'), write([A,B,C]),
   write('"   targetname: "'), write(B), writeln('"'),
   write('          '), write(Mode2), writeln(' }').
grl_pp_labeled_graph_edge(_,_).


/* grl_pp_state_lattice <-
      given a set of asserted facts d_fact(Node) and i_fact(Node),
      a grl file for the hasse diagram over these nodes is
      generated, which can be displayed by edge. */
 
grl_pp_state_lattice :-
   lattice_file(File), switch(F,File),
   grl_pp_file_header(graph),
   grl_pp_hasse_nodes, nl,
   grl_pp_hasse_edges,
   grl_pp_file_tail,
   switch(F).
 
grl_pp_tree_lattice :-
   lattice_file(File), switch(F,File),
   grl_pp_file_header(tree),
   grl_pp_d_fact_tree,
   grl_pp_file_tail,
   switch(F).
 

/* assert_d_i_facts_as_hasse_nodes <-
      asserts all nodes Node given by asserted atoms d_fact(Node) 
      or i_fact(Node) as atoms hasse_node(Node). */

assert_d_i_facts_as_hasse_nodes :-
   d_or_i_fact(Node),
   assert_hasse_node(Node),
   fail.
assert_d_i_facts_as_hasse_nodes :-
   assert_hasse_node([]).

assert_hasse_node(L) :-
   hasse_node(L),
   !.
assert_hasse_node(L) :-
   assert(hasse_node(L)).


hasse_root(Node) :-
   hasse_node(Node),
   \+ ( hasse_node(Node2),
        subset_not_eq(Node2,Node) ).


/* fact_pair(Fact1,Fact2) <-
      generates all pairs Fact1, Fact2 of clauses which are asserted
      as d_fact or i_fact atoms. */

fact_pair(Fact1,Fact2) :-
   d_or_i_fact(Fact1),
   d_or_i_fact(Fact2).


/* d_or_i_fact(Fact) <-
      generates all clauses Fact which are asserted as an atom
      d_fact(Fact) or i_fact(Fact). */

d_or_i_fact(Fact) :-
   ( d_fact(Fact)
   ; i_fact(Fact) ).


/* hasse_successors(Node,Successors) <-
      Successors is the list of all successors Node2 of Node in the 
      hasse diagram for the asserted facts hasse_node(Node2), 
      i.e. the direct successors. */

hasse_successors(Node,Successors) :-
   findall( Node2,
      hasse_successor(Node,Node2),
      Successors ).
         
/* hasse_successor(Node1,Node2) <-
      Node1 is a direct successor of Node1 among the asserted facts
      hasse_node(Node2). */

hasse_successor(Node1,Node2) :-
   length(Node1,N), N > 0,
   !,
   hasse_node(Node2),
   subset_not_eq(Node1,Node2).
hasse_successor(Node1,Node2) :-
   hasse_node(Node2),
   subset_not_eq(Node1,Node2),
   \+ ( hasse_node(Node3),
        subset_not_eq(Node1,Node3), 
        subset_not_eq(Node3,Node2) ).


/* hasse_successors_opt(Node,Successors) <-
      Successors is the list of all successors Node2 of Node in the 
      hasse diagram for the asserted facts hasse_node(Node2), 
      i.e. the direct successors. */
 
hasse_successors_opt(Node,Successors) :-
   length(Node,I),
   J is I + 1,
   hasse_successors_opt(J,Node,[],Successors),
   !.
 
hasse_successors_opt(I,Node,Sofar,Successors) :-
   cardinality_with_hasse_nodes(J,_), J >= I,
   !,
   findall( Node2,
      ( cardinality_with_hasse_nodes(I,Nodes),
        member(Node2,Nodes), ord_subset(Node,Node2),
        \+ ( member(Node3,Sofar),
             ord_subset(Node3,Node2) ) ),
      New_Successors ),
   ord_union(Sofar,New_Successors,New_Sofar),
   K is I + 1,
   hasse_successors_opt(K,Node,New_Sofar,Successors).
hasse_successors_opt(_,_,Successors,Successors).
            
hasse_successor_opt(Node1,Node2) :-
   length(Node1,N), N > 2,
   !, 
   hasse_node(Node2),
   subset_not_eq(Node1,Node2).
hasse_successor_opt(Node1,Node2) :-
   hasse_successors_opt(Node1,Successors),
   member(Node2,Successors).


/* hasse_cardinalities <-
      for all numbers I, the set Nodes of all nodes Node in asserted 
      facts hasse_node(Node), such that the cardinality of Node is I,
      is asserted as cardinality_with_hasse_nodes(I,Nodes). */

hasse_cardinalities :-
   hasse_cardinalities_part_1,
   hasse_cardinalities_part_2(0).

hasse_cardinalities_part_1 :-
   hasse_node(Node),
   assert(card_node(Node)),
   fail.
hasse_cardinalities_part_1.

hasse_cardinalities_part_2(I) :-
   card_node(_),
   !,
   findall( Node,
      ( card_node(Node), length(Node,I),
        retract(card_node(Node)) ),
      Nodes ),
   assert(cardinality_with_hasse_nodes(I,Nodes)),
   J is I + 1,
   hasse_cardinalities_part_2(J).
hasse_cardinalities_part_2(_).


/* complete_lattice <-
      For each set Node that is the intersection of the sets 
      Node1 and Node2 of two asserted d_facts d_fact(Node1) and 
      d_fact(Node2), an atom i_fact(Node) is asserted.
      Furthermore i_fact([]) is asserted. */

complete_lattice(tree) :-
   !,
   nl,
   write(user,'completing lattice '), ttyflush,
   ( ( d_fact_tree(Tree), !,
       assert_tree_i_facts(Tree,[]) )
   ; true ),
   writeln(user,'  finished').

assert_tree_i_facts([Node|Trees],Sofar) :-
   member(New_Tree,Trees),
   ord_union(Node,Sofar,New_Sofar),
   insert_i_fact(New_Sofar),
   assert_tree_i_facts(New_Tree,New_Sofar).


complete_lattice(state) :-
   complete_lattice(2),
   !.

complete_lattice(I) :-
   nl,
   write(user,'completing lattice '), ttyflush,
   complete_lattice_basic,
   J is I - 2,
   assert_i_facts(J),
   insert_i_fact([]),
   writeln(user,'  finished').

assert_i_facts(0) :-
   !.
assert_i_facts(_) :-
   d_fact(Fact1), i_fact(Fact2),
   \+ ( ord_subset(Fact1,Fact2)
      ; ord_subset(Fact2,Fact1) ),
   ord_intersection(Fact1,Fact2,Fact3), insert_i_fact(Fact3),
   fail.
assert_i_facts(I) :-
   J is I - 1,
   assert_i_facts(J).

complete_lattice_basic :-
   d_fact(Fact1), d_fact(Fact2),
   \+ ( ord_subset(Fact1,Fact2)
      ; ord_subset(Fact2,Fact1) ),
   ord_intersection(Fact1,Fact2,Fact3), insert_i_fact(Fact3),
   fail.
complete_lattice_basic.


insert_i_fact(Fact) :-
   \+ ( d_or_i_fact(Fact2), 
        ord_seteq(Fact,Fact2) ),
   asserta(i_fact(Fact)),
   write(user,'.'), ttyflush.
insert_i_fact(_).


/* grl_pp_nodes, grl_pp_edges <-
      writes all d_facts and i_facts and the hasse edges to the
      current output stream */

grl_pp_file_header(tree) :-
   writeln('graph: { '), nl,
   writeln('  /* graph attributes */ '),
   writeln('  layoutalgorithm: tree '),
   writeln('  status: grey '),
   writeln('  xbase:   5     ybase:   5 '),
   writeln('  xspace: 10     yspace: 40 '), nl.
grl_pp_file_header(graph) :-
   grl_pp_file_header(graph,[5-5,10-40]).

grl_pp_file_header(graph,[Xbase-Ybase,Xspace-Yspace]) :-
   writeln('graph: { '), nl,
   writeln('  /* graph attributes */ '),
   writeln('  layoutalgorithm: barycenter '),
   writeln('  status: grey '),
   write_list(['  xbase:   ',Xbase,'     ybase:   ',Ybase,' \n']),
   write_list(['  xspace: ',Xspace,'     yspace: ',Yspace,' \n\n']).

grl_pp_file_tail :-
   writeln('} '), nl.


/* grl_pp_hasse_nodes <-
      Pretty printing of the sets Node that are asserted as
      d_fact(Node) or as i_fact(Node) as hasse nodes for 
      input to edge. */

grl_pp_hasse_nodes :-
   write(user,'writing nodes ...  '), ttyflush,
   writeln('  /* list of hypernodes */ '), nl,
   fail. 
grl_pp_hasse_nodes :-
   d_fact(Fact), grl_pp_hasse_node(Fact),
   fail.
grl_pp_hasse_nodes :-
   i_fact(Fact), grl_pp_i_hasse_node(Fact),
   fail.
grl_pp_hasse_nodes :-
   writeln(user,'finished').


/* grl_pp_hasse_node(Node) <-
      pretty printing of a set Node as a hasse node
      for input to edge. */

grl_pp_hasse_node(Node) :-
   grl_pp_hasse_node(Node,Node).

grl_pp_hasse_node(Node,Label) :-
   write('  node: { title: "'), write(Node),
   write('"   label: "'),
   ( ( current_num(edge_mode,long), !,
       write_atoms_for_grl_pp(Label),
       writeln('"   borderwidth: 1  }') )
   ; ( current_num(edge_mode,short),
       length(Node,I), write(I),
       writeln('"   borderwidth: 3  }') ) ).


/* grl_pp_i_hasse_node(Node) <-
      pretty printing of a fact i_fact(Node) as a hasse node 
      for input to edge. */

grl_pp_i_hasse_node(Node) :-
   grl_pp_i_hasse_node(Node,Node).

grl_pp_i_hasse_node(Node,Label) :-
   write('  node: { title: "'), write(Node),
   write('"   label: "'),          
   ( ( current_num(edge_mode,long), !, 
       write_atoms_for_grl_pp(Label),
       writeln('"   borderwidth: 1  }') )
   ; ( current_num(edge_mode,short),
       length(Node,I), write(I),  
       writeln('"   borderwidth: 1  }') ) ).
 

grl_pp_hasse_edges :-
   write(user,'writing edges ...  '), ttyflush,
   writeln('  /* list of hyperedges */ '), nl,
   fail.
grl_pp_hasse_edges :-
   hasse_neighbours(Node1,Node2),
   grl_pp_hasse_edge(Node1,Node2),
   fail.
grl_pp_hasse_edges :-
   writeln(user,'finished').

hasse_neighbours(Node1,Node2) :-
   fact_pair(Node1,Node2),
   subset_not_eq(Node1,Node2),
   \+ ( d_or_i_fact(Node3),
        subset_not_eq(Node1,Node3),
        subset_not_eq(Node3,Node2) ).


/* grl_pp_hasse_edge(Node1,Node2) <-
      pretty printing of a hasse edge for input to edge. */

grl_pp_hasse_edge(Node1,Node2) :-
   grl_pp_graph_edge([Node1,Node2],
      'thickness: 1   arrowstyle: none').

grl_pp_tree_pointer(Label,Node) :-
   grl_pp_graph_edge([[Label,label],[Label|Node]],
      'thickness: 1   arrowstyle: solid   linestyle: dashed').


write_arg_lists([]) :-
   !.
write_arg_lists([H|T]) :-
   H =.. [_|Args], write(Args),
   write_arg_lists(T).


write_atoms_for_grl_pp([]) :-
   !,
   write(' ').
write_atoms_for_grl_pp(Atoms) :-
   write_atoms(Atoms).


grl_pp_d_fact_tree :-
   d_fact_tree(Tree),
   write(user,'writing nodes ...  '), ttyflush,
   writeln('  /* list of hypernodes */ '), nl,
   grl_pp_d_fact_tree_nodes(Tree),
   writeln(user,'finished'), nl,
   write(user,'writing edges ...  '), ttyflush,
   writeln('  /* list of hyperedges */ '), nl,
   grl_pp_d_fact_tree_edges(Tree),
   writeln(user,'finished'), nl.

grl_pp_d_fact_trees(I,[Tree|Trees]) :-
   write(user,'writing nodes ...  '), ttyflush,
   writeln('  /* list of hypernodes */ '), nl,
   name_append('Tree',I,Label),
   grl_pp_tree_label([Label]),
   grl_pp_d_fact_trees_nodes([Label],[Tree]),
   writeln(user,'finished'), nl,
   write(user,'writing edges ...  '), ttyflush,
   writeln('  /* list of hyperedges */ '), nl,
   grl_pp_d_fact_tree_edges_2(Label,Tree),
   writeln(user,'finished'), nl,
   J is I + 1,
   grl_pp_d_fact_trees(J,Trees).
grl_pp_d_fact_trees(_,[]).

grl_pp_tree_label([Label]) :-
   write('  node: { title: "'), write([Label,label]),
   write('"   label: "'), write(Label),
   writeln('"   borderwidth: 0  }').

grl_pp_d_fact_tree_edges_2(_,[]) :-
   !.
grl_pp_d_fact_tree_edges_2(Label,[Node|Trees]) :-
   grl_pp_tree_pointer(Label,Node),
   grl_pp_d_fact_trees_edges([Label|Node],Trees).

grl_pp_d_fact_tree_nodes([Node|Trees]) :-
   grl_pp_i_hasse_node(Node,Node),
   grl_pp_d_fact_trees_nodes(Node,Trees).
grl_pp_d_fact_tree_nodes([]).

grl_pp_d_fact_trees_nodes(Sofar,[Tree|Trees]) :-
   grl_pp_d_fact_tree_nodes(Sofar,Tree),
   grl_pp_d_fact_trees_nodes(Sofar,Trees).
grl_pp_d_fact_trees_nodes(_,[]).

grl_pp_d_fact_tree_nodes(Sofar,[Node]) :-
   !,
   ord_union(Node,Sofar,Hasse_Node),
   grl_pp_hasse_node(Hasse_Node,Node).
grl_pp_d_fact_tree_nodes(Sofar,[Node|Trees]) :-
   ord_union(Node,Sofar,Hasse_Node),
   grl_pp_i_hasse_node(Hasse_Node,Node),
   grl_pp_d_fact_trees_nodes(Hasse_Node,Trees).
grl_pp_d_fact_tree_nodes(_,[]).


grl_pp_d_fact_tree_edges([Node|Trees]) :-
   grl_pp_d_fact_trees_edges(Node,Trees).
grl_pp_d_fact_tree_edges([]).

grl_pp_d_fact_trees_edges(Node,[Tree|Trees]) :-
   grl_pp_d_fact_tree_edges(Node,Tree),
   grl_pp_d_fact_trees_edges(Node,Trees).
grl_pp_d_fact_trees_edges(_,[]).

grl_pp_d_fact_tree_edges(Sofar,[Node|Trees]) :-
   ord_union(Node,Sofar,Hasse_Node),
   grl_pp_hasse_edge(Sofar,Hasse_Node),
   grl_pp_d_fact_trees_edges(Hasse_Node,Trees).
grl_pp_d_fact_tree_edges(_,[]).


/* grl_pp_trees(I,Trees) <-
      the trees in Trees are pretty printed for edge, 
      where each node title is a list starting with the number I+J 
      for the J-th tree. */

grl_pp_trees(I,[Tree|Trees]) :-
   grl_pp_tree(I,Tree),
   J is I + 1,
   grl_pp_trees(J,Trees).
grl_pp_trees(_,[]).


/* grl_pp_tree(I,Tree) <-
      the tree Tree is pretty printed for edge, where each node title
      is a list starting with the number I. */

grl_pp_tree(I,Tree) :-
   write(user,'writing nodes ...  '), ttyflush,
   writeln('  /* list of hypernodes */ '), nl,
   name_append('tree',I,Label),
   write('  node: { title: "'), write([I]),
   write('"   label: "'), write(Label),
   writeln('"   borderwidth: 0  }'),
   grl_pp_tree_nodes([1,I],Tree),
   writeln(user,'finished'), nl,
   write(user,'writing edges ...  '), ttyflush,
   writeln('  /* list of hyperedges */ '), nl,
   grl_pp_graph_edge([[I],[1,I]],
      'thickness: 1   arrowstyle: solid   linestyle: dashed'),
   grl_pp_tree_edges([1,I],Tree),
   writeln(user,'finished'), nl.


grl_pp_tree_nodes(Path,[Node|Trees]) :-
   grl_pp_title_and_label(Path,Node),
   grl_pp_trees_nodes(1,Path,Trees).
grl_pp_tree_nodes(Path,[]) :-
   write('  node: { title: "'), write(Path),
   write('"   label: "'),       write('*'),
   writeln('"   borderwidth: 0  }').

grl_pp_trees_nodes(I,Path,[Tree|Trees]) :-
   grl_pp_tree_nodes([I|Path],Tree),
   J is I + 1,
   grl_pp_trees_nodes(J,Path,Trees).
grl_pp_trees_nodes(_,_,[]).


grl_pp_tree_edges(Path,[_|Trees]) :-
   grl_pp_trees_edges(1,Path,Trees). 
grl_pp_tree_edges(_,[]).
 
grl_pp_trees_edges(I,Path,[Tree|Trees]) :- 
   grl_pp_hasse_edge(Path,[I|Path]),
   grl_pp_tree_edges([I|Path],Tree),
   J is I + 1, 
   grl_pp_trees_edges(J,Path,Trees). 
grl_pp_trees_edges(_,_,[]). 
 

grl_pp_title_and_label(Title,Label) :-
   write('  node: { title: "'), write(Title),
   write('"   label: "'),
   write_atoms_for_grl_pp(Label),
   writeln('"   borderwidth: 1  }').

   
/******************************************************************/


