

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Depth-First-Search                    ***/
/***                                                            ***/ 
/******************************************************************/ 
 


/*** interface ****************************************************/


/* Computing connected components of graphs:

   atom_list_to_1cc_s([
      a(a,b),a(b,c),a(c,d),a(d,a),
      a(e,f),b(f,g)],P).
   atom_list_to_2cc_s([
      a(a,b),a(b,c),a(c,d),a(d,a),
      a(e,f),b(f,g),c(g,h)],P).
   hyperedges_to_2cc_s([
      [a,b],[b,c],[c,d],[d,a],
      [e,f],[f,g],[g,h]],P).
*/


/* DFS algorithm to detect all connected components 
   of an arbitrary hypergraph */

hyperedges_to_1cc_s(Hyperedges, Components) :-
   dislog_flag_switch(connected_atoms_mode, Mode, sets),
   create_partition_dfs(Hyperedges, Components),
   dislog_flag_set(connected_atoms_mode, Mode).

atom_list_to_1cc_s(Atoms, Components) :-
   dislog_flag_switch(connected_atoms_mode, Mode, atoms),
   create_partition_dfs(Atoms, Components),
   dislog_flag_set(connected_atoms_mode, Mode).

create_partition_dfs(Atoms, Partition) :-
   assertz_graph(Atoms),
   write(user,'connected components '), ttyflush,
   create_partition_dfs_loop(Partition).

create_partition_dfs_loop([Component|Components]) :-
   retract(node(V)),
   !,
   write(user,'.'),
   writeln('------------------ \\\\'),
   lpp_tab(0), write(' $'), write(V), writeln('$ \\\\'),
   graph_dfs_1cc(V,Component),
   create_partition_dfs_loop(Components).
create_partition_dfs_loop([]) :-
   writeln('------------------   '), nl,
   writeln(user,' ').


/* DFS algorithm to detect all biconnected components 
   of an arbitrary hypergraph */

hyperedges_to_2cc_s(Hyperedges, Components) :-
   dislog_flag_switch(connected_atoms_mode, Mode, sets),
   create_factorization_dfs(Hyperedges, Components),
   dislog_flag_set(connected_atoms_mode, Mode).

atom_list_to_2cc_s(Atoms, Components) :-
   dislog_flag_switch(connected_atoms_mode, Mode, atoms),
   create_factorization_dfs(Atoms, Components),
   dislog_flag_set(connected_atoms_mode, Mode).

create_factorization_dfs(Atoms, Factorization) :-
   dfs_prepare_analysis,
   assertz_graph(Atoms),
   create_factorization_dfs_loop(Factorization),
   dfs_perform_analysis.

create_factorization_dfs_loop([Component|Components]) :-
   retract(node(V)),
   !,
   write_list(user,['connected component ','.']),
   write('-------------------------- \\\\'),
   lpp_tab(0), write(' $'), write(V), writeln('$ \\\\'),
   graph_dfs_2cc(V,Component),
   pp_factorization(V,Component),
   create_factorization_dfs_loop(Components).
create_factorization_dfs_loop([]) :-
   writeln('--------------------------   '), nl.


/* The DFS algorithms can be applied to a hypergraph that is 
   either given by a set of atoms or by a set of sets.
   This is specified by the current value of the dislog flag
   connected_atoms_mode. */

connected_atoms(Atom1, Atom2) :-
   dislog_flag_get(connected_atoms_mode, sets),
   !,
   intersects(Atom1, Atom2).
connected_atoms(Atom1,Atom2) :-
   dislog_flag_get(connected_atoms_mode, atoms),
   !,
   Atom1 =.. [_|Arguments1],
   Atom2 =.. [_|Arguments2],
   intersects(Arguments1, Arguments2).


/*** implementation ***********************************************/


/* initialize graphs */

assertz_graph([H|T]) :-
   assertz(node(H)), write(user,'.'),
   assertz_graph(T).
assertz_graph([]) :-
   writeln(user,' ').
   
assertz_hypergraph([E|T]) :-
   assertz(hyperedge(E)), write(user,'e'),
   assertz_hypergraph(T).
assertz_hypergraph([]) :-
   writeln(user,' ').

assert_2cc([H|T]) :-
   assert(node_2cc(H)), write(user,'p'),
   assert_2cc(T).
assert_2cc([]).


/* DFS algorithm to detect and pretty print 
   one connected component of an arbitrary hypergraph */

graph_dfs_1cc(V,ZK) :-
   graph_dfs_1cc(0,V,ZK).

graph_dfs_1cc(I,V,ZK) :-
   node(W), connected_atoms(V,W),
   !,
   retract(node(W)),
   lpp_tab(I), tab(I), 
   write_list([' $-->$ $',W,'$ \\']), nl,
   J is I + 4,
   graph_dfs_1cc(J,W,ZK1),
   graph_dfs_1cc(I,V,ZK2),
   append(ZK1,ZK2,ZK).
graph_dfs_1cc(_,V,[V]).

   
/* DFS algorithm to detect 
   one biconnected component of an arbitrary hypergraph */

graph_dfs_2cc(V,Fa) :-
   assert(factorization([])),
   graph_dfs_2cc(V),
   retractall(edge(_,_)),  
   retract(factorization(Fa)).

graph_dfs_2cc(V) :-
   measure_and_add(
      ( node(W), node_test_executed, 
        connected_atoms(V,W) ),
      search_time ),
   !,
   retract(node(W)), successful_node_test_executed, 
   assertz(edge(V,W)),
%  write_list(user,['  ',V,'->',W,'  ']),
   append_to_factorization([V,W]),
   graph_dfs_2cc(W),
   graph_dfs_2cc(V).
graph_dfs_2cc(V) :-
   maximal_back_edge(V,W),
   !,
   find_tree_path(W,V,Tree_Path),
   insert_into_factorization(Tree_Path). 
graph_dfs_2cc(_).


maximal_back_edge(V,W) :-
   edge(X,V), maximal_connected(V,V,X,W), W \== V.

maximal_connected(V,_,X,Max2) :-
   edge(Y,X), connected_atoms(V,Y),
   !,
   maximal_connected(V,Y,Y,Max2).
maximal_connected(V,Max1,X,Max2) :-
   edge(Y,X),
   maximal_connected(V,Max1,Y,Max2).
maximal_connected(_,Max1,_,Max1).


find_tree_path(A,A,[A]) :-
   !.
find_tree_path(A,C,Tree_Path_1) :-
   edge(B,C), find_tree_path(A,B,Tree_Path),
   append(Tree_Path,[C],Tree_Path_1).


append_to_factorization([V,W]) :-
   measure_and_add(
      ( retract(factorization(Fa1)),
        assert(factorization([[V,W]|Fa1])) ),
      append_time ),
   write(user,'a').

insert_into_factorization(Tree_Path) :-
   measure_and_add(
      ( retract(factorization(Fa1)),
        insert_into_factorization(Tree_Path,Fa1,Cycle,Fa2), 
        assert(factorization([Cycle|Fa2])) ),
      insert_time ),
   write(user,'i').

insert_into_factorization(Tree_Path,[H|T],Cycle2,Fa) :-
   n_intersects(2,Tree_Path,H),
   !,
   insert_into_factorization(Tree_Path,T,Cycle1,Fa),
   setdifference(H,Cycle1,HH), append(Cycle1,HH,Cycle2).
insert_into_factorization(Tree_Path,[H|T],Cycle,[H|Fa]) :-
   insert_into_factorization(Tree_Path,T,Cycle,Fa).
insert_into_factorization(Tree_Path,[],Tree_Path,[]).


/* pretty printer for factorizations, i.e. acyclic hypergraphs */

pp_factorization(V,Fa) :-
   assertz_hypergraph(Fa),
   write(user,'  and its biconnected components '),
   pp_factorization_dfs(0,[V]),
   writeln(user,' '),
   garbage_collect.
   
pp_factorization_dfs(I,[V|T]) :-
   hyperedge(E), member(V,E),
   !,
   retract(hyperedge(E)),
   J is I + 4,
   setdifference(E,[V],F),
   pp_2cc(I,V,F),
%  lspp_atom_list(I,E),
%  i_atom_list(I_Atom_List), connected_i_atoms(E,I_Atom_List,L),
%  lpp_tab(I), tab(I), write(' $-->$ $ '), pp_atom_list(E), writeln(' $ \\'),
%  lpp_tab(J), tab(J), write(' $   $ $ '), pp_atom_list(L), writeln(' $ \\'),
   pp_factorization_dfs(J,F),
   pp_factorization_dfs(I,[V|T]).
pp_factorization_dfs(I,[_|T]) :-
   pp_factorization_dfs(I,T).
pp_factorization_dfs(_,[]).


connected_i_atoms(E,[W|T],[W|L]) :-
   member(V,E), 
   connected_atoms(V,W),
   !,
   connected_i_atoms(E,T,L).
connected_i_atoms(E,[_|T],L) :-
   connected_i_atoms(E,T,L).
connected_i_atoms(_,[],[]).

pp_2cc(I,V,E) :-
   write(user,'.'),
   assert_2cc(E),
   writeln(user,''),
   lpp_tab(I), tab(I), 
   write_list([' $-->$ $',V,'$ \\\\']), nl,
   J is I + 4,
   pp_2cc_dfs(J,1,V),
   retractall(edge_2cc(_,_)).

pp_2cc_dfs(I,N,V) :-
   node_2cc(W), connected_atoms(V,W),
   !,
   retract(node_2cc(W)),
   assertz(edge_2cc(V,W)),
%  maximal_back_edge_count_2cc(W,K),
   M is N + 1,
   back_edge_counts_2cc(M,W,K),
   lpp_tab(I), tab(I), 
%  comment
%  write_list([' $   $ $',W,'$ \ - \ $< ',M,',',K,' >$ \\\\']), nl,
   write_list([K]), nl,
   J is I + 1,
   pp_2cc_dfs(J,M,W),
   pp_2cc_dfs(I,N,V).
pp_2cc_dfs(_,_,_).


/* back-edge detection */

maximal_back_edge_count_2cc(V,K) :-
   maximal_connected_2cc(V,V,V,W),
   !,
   find_tree_path_2cc(W,V,Tree_Path),
   length(Tree_Path,I),
   K is I - 1.

maximal_connected_2cc(V,_,X,Max2) :-
   edge_2cc(Y,X), connected_atoms(V,Y),
   !,
   maximal_connected_2cc(V,Y,Y,Max2).
maximal_connected_2cc(V,Max1,X,Max2) :-
   edge_2cc(Y,X),
   maximal_connected_2cc(V,Max1,Y,Max2).
maximal_connected_2cc(_,Max1,_,Max1).

find_tree_path_2cc(A,A,[A]) :-
   !.
find_tree_path_2cc(A,C,Tree_Path_1) :-
   edge_2cc(B,C), 
   find_tree_path_2cc(A,B,Tree_Path),
   append(Tree_Path,[C],Tree_Path_1).

back_edge_counts_2cc(N,V,K) :-
   edge_2cc(X,V),
   !,
   M is N - 1,
   back_edge_counts_2cc(M,V,X,K).
back_edge_counts_2cc(_,_,[]).

back_edge_counts_2cc(N,V,X,[M|T]) :-
   edge_2cc(Y,X), connected_atoms(V,Y),
   !,
   M is N - 1,
   back_edge_counts_2cc(M,V,Y,T).
back_edge_counts_2cc(N,V,X,T) :-
   edge_2cc(Y,X),
   M is N - 1,
   back_edge_counts_2cc(M,V,Y,T).
back_edge_counts_2cc(_,_,_,[]).


/******************************************************************/


lpp_tab(I) :-
   J is 3 * I,
   name('\\hspace*{',X), base_b_list(10,J,Y), name('mm}',U),
   append(X,Y,Z), append(Z,U,V),
   name(L,V), write(L).


node_test_executed :-
   retract(counter1(I)),
   J is I + 1,
   assert(counter1(J)).

successful_node_test_executed :-
   retract(counter2(I)),
   J is I + 1,
   assert(counter2(J)).

dfs_prepare_analysis :-
   ddk_time(T),
   assert(start_time(T)),
   assert(append_time(0)),
   assert(insert_time(0)),
   assert(search_time(0)),
   assert(counter1(0)),
   assert(counter2(0)).

dfs_perform_analysis :-
   retract(start_time(Time1)),
   retract(append_time(T1)),
   retract(insert_time(T2)),
   retract(search_time(T3)),
   retract(counter1(I1)),
   retract(counter2(I2)),
   ddk_time(Time2),
   Time is ( Time2 - Time1 ) / 1000,
   telling(F), tell(user),
   write('used time in seconds  '), pp_time(Time), writeln(' '),
   write('time for append, insert, search operations  '),
   pp_time(T1), write(', '), 
   pp_time(T2), write(', '), 
   pp_time(T3), writeln(' '),
   write('node tests, successful node tests  '),
   write(I1), write(', '), writeln(I2),
   tell(F).


/******************************************************************/


