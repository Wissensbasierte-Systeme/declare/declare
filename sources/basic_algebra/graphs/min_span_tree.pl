

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Greedy Algorithm                      ***/
/***                                                            ***/
/******************************************************************/


:- dynamic 
      graph_node/1, 
      graph_edge/3, 
      graph_root/1.


/*** interface ****************************************************/


minimal_steiner_tree(Facts,Pruned_Tree) :-
   mst_prepare(Facts),
   start_timer(mst),
   write('steiner tree '), ttyflush,
   mst(Facts,Tree),
   abolish(cardinality_with_hasse_nodes),
   tree_prune([],Tree,Pruned_Tree),
   stop_timer(mst),
   !.
 

mst(Facts,Tree) :-
   hasse_root(Node), assert(greedy_root(Node)),
   greedy,
   !,
   good_nodes(Facts),
   abolish(tree_edge),
   successor_trees([Node],[],[Tree]),
   abolish(good_edge).
mst(_,[]).
 

greedy :-
   heap_init,
   greedy_root(V),
   retract(hasse_node(V)),
   heap_insert_outgoing_edges(V),
   !,
   greedy_step.
 
 
/*** implementation ***********************************************/


mst_prepare(Facts) :-
   start_timer(mst_prepare),
   write('mst_prepare  '), ttyflush,
   findall( Node,
      ( member(Node,Facts)
      ; ( member(Fact1,Facts), member(Fact2,Facts),
          ord_intersection(Fact1,Fact2,Node) ) ),
      Prev_Hasse_Nodes ),
   list_to_ord_set(Prev_Hasse_Nodes,Hasse_Nodes),
   assert_all_hasse_nodes([[]|Hasse_Nodes]),
   length(Hasse_Nodes,N), K is N + 1, length(Facts,M),
   hasse_cardinalities,
   stop_timer_write(mst_prepare),
   write(K), write(' hasse nodes for '), write(M), writeln(' facts').

greedy_step :-
   heap_size(N), N > 0,
   heap_min_pop([[V,W],_]),
   retract(hasse_node(W)),
   assert(tree_edge(V,W)),
%  pp_tree_edge(V,W),
   heap_insert_outgoing_edges(W),   
   greedy_step.
greedy_step :-
   heap_size(N), N > 0,
   !,
   greedy_step.
greedy_step :-
   !.

pp_tree_edge(V,W) :-
   write(V), write(' --> '), writeln(W).
  
hasse_edge(V,W,G) :- 
%  hasse_successor(V,W),
   hasse_successor_opt(V,W),
   ord_subtract(W,V,U), length(U,G).

heap_insert_outgoing_edges(V) :-
   hasse_edge(V,W,G),
   hasse_node(W),
   hi([V,W],G),
   fail. 
heap_insert_outgoing_edges(_).
   
heap_min_pop([[V,W],G]) :-
%  pp_heap,
   heap_size(N), !, N > 0,
   a(1,[V,W],G),
   hd([V,W]),
   !.

good_nodes([]) :-
   !.
good_nodes(L) :-
   findall( V,
      ( member(W,L),
        retract(tree_edge(V,W)),
        assert(good_edge(V,W)) ),
      L1 ),
   good_nodes(L1).


/* good_node(V) <-
      not used any more. */

good_node(V) :-
   d_fact(V).
good_node(V) :-
   listing(tree_edge),
   tree_edge(V,_),
   findall( W,
      ( W = []
      ; ( write('^'), ttyflush,
          tree_edge(V,W),
          W \== [],
          write('&'), ttyflush,
          good_node(W),
          assert(good_edge(V,W)) ) ),
      L ),
   L \== [[]].
good_node([]).


/******************************************************************/


