

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Hypergraphs Transversals              ***/
/***                                                            ***/
/******************************************************************/



/******************************************************************/
/***             ALGORITHM : hypergraph_min_trans               ***/
/******************************************************************/


/* hypergraph_min_trans(+Hypergraph,?Minimal_Transversals) <-
      Computes the set Minimal_Transversals of all minimal 
      transversals of the hypergraph Hypergraph. */

hypergraph_min_trans([],[]) :-
   !.
hypergraph_min_trans(Hypergraph,Minimal_Transversals) :-
   lists_to_ord_sets(Hypergraph,Hypergraph_1),
   state_can_2(Hypergraph_1,Hypergraph_2),
%  beta_acyclic_components(Hypergraph_2,B_A_Components),
   beta_acyclic_components_1(Hypergraph_2,B_A_Components),
%  rehang_a(B_A_Components_1,B_A_Components),
   min_trans_of_components(B_A_Components,Min_Trans_of_Components),
   coin_conjunction(Min_Trans_of_Components,Minimal_Transversals),
%  coin_conjunction_2(Min_Trans_of_Components,Minimal_Transversals),
   !.


/******************************************************************/
/***             ALGORITHM : min_trans_of_components            ***/
/******************************************************************/


/* min_trans_of_components(+Acyclic_Components,?Coins) <-
      Computes the set Coins consisting many sets of minimal 
      transversals, namely the sets of minimal transversals 
      of the acyclic components in Acyclic_Components. */

min_trans_of_components([],[]) :-
   !.
min_trans_of_components([Component|Components],[Coin|Coins]) :-
   hypergraph_min_trans_beta_acyclic(Component,Coin),
   min_trans_of_components(Components,Coins),
   !.


/******************************************************************/
/***             ALGORITHM : beta_acyclic_components            ***/
/******************************************************************/


/* beta_acyclic_components(+Hypergraph,?Components) <-
      Partitions the hypergraph Hypergraph into a set
      Components of beta-acyclic components. */

beta_acyclic_components([],[]) :-
   !.
beta_acyclic_components(Edges,[Bac|Bacs]) :-
   beta_acyclic_component(Edges,Bac_List),
   list_to_ord_set(Bac_List,Bac),
   ord_subtract(Edges,Bac,Rest_Edges),
   beta_acyclic_components(Rest_Edges,Bacs),
   !.


/*** implementation ***********************************************/


/* beta_acyclic_component(+Edges,?Acyclic_Component) <-
      Computes an acyclic component of Edges using the following
      strategy for extension: always pick the first edge in Edges, 
      that keeps the component acyclic. */

beta_acyclic_component(Edges,Acyclic_Component) :-
   beta_acyclic_component(Edges,[],Acyclic_Component).

beta_acyclic_component([],[],[]) :-
   !.
beta_acyclic_component([],Component,Component) :-
   !.
beta_acyclic_component(Edges,U_Es,Component) :-
   ord_union(U_Es,Vertices),
   ( ( bac_extension_generate(Edges,Vertices,U_Es,_,New_Edge),
       ord_del_element(Edges,New_Edge,Edges_2),
       list_to_ord_set([New_Edge|U_Es],U_Es_2),
       beta_acyclic_component(Edges_2,U_Es_2,Component) )
   ; ( Component = U_Es, ! ) ),
   !.


/* bac_extension_generate(+Edges,+U_Vs,+U_Es,?U_E,?New_Edge) <-
      Finds the edge of Edges that will be added to 
      the existing acyclic hypergraph. */

bac_extension_generate([Edge|_],_,[],_,Edge) :-
   !.
bac_extension_generate([E|_],U_Vs,U_Es,U_E,E) :-
   bac_extension_test(U_Vs,U_Es,E,U_E,_),
   !.
bac_extension_generate([_|Es],U_Vs,U_Es,U_E,New_Edge) :-
   bac_extension_generate(Es,U_Vs,U_Es,U_E,New_Edge),
   !.


/* bac_extension_test(+U_Vs,+U_Es,+New_Edge,?U_E,?New_Vertices) <-
      Tests, if New_Edge can be added to the acyclic hypergraph 
      denoted by U_Es without getting a cycle. */

bac_extension_test(U_Vs,[U_E|_],New_Edge,U_E,New_Vertices) :-
   ord_intersection(New_Edge,U_Vs,Intersection),
   Intersection \== [],
   ord_subset(Intersection,U_E),
   ord_subtract(New_Edge,Intersection,New_Vertices),
   !.
bac_extension_test(U_Vs,[_|U_Es],New_Edge,U_E,New_Vertices) :-
   bac_extension_test(U_Vs,U_Es,New_Edge,U_E,New_Vertices).


/******************************************************************/
/***             ALGORITHM : beta_acyclic_components_1          ***/
/******************************************************************/


/* beta_acyclic_components_1(+Hypergraph,?Components) <-
      Partitions the hypergraph Hypergraph into a set
      Components of beta-acyclic components. */

beta_acyclic_components_1([],[]) :-
   !.
beta_acyclic_components_1(Edges,[Component|Components]) :-
   lists_to_ord_sets(Edges,Edges_1),
   beta_acyclic_component_1(Edges_1,Component_1),
   list_to_ord_set(Component_1,Component),
   ord_subtract(Edges_1,Component,Edges_2),
   beta_acyclic_components_1(Edges_2,Components),
   !.


/*** implementation ***********************************************/


/* beta_acyclic_component_1(+Edges,?Acyclic_Component) <-
      Computes a beta-acyclic component Acyclic_Component of 
      Edges using the following strategy for extension: 
      always pick the edge in Edges having the largest 
      intersection with the actual component. */

beta_acyclic_component_1(Edges,Acyclic_Component) :-
   beta_acyclic_component_1(Edges,[],Acyclic_Component).

beta_acyclic_component_1([],[],[]) :-
   !.
beta_acyclic_component_1([],Component,Component) :-
   !.
beta_acyclic_component_1(Edges,U_Es,Component) :-
   ord_union(U_Es,Vertices),
   ( ( bac_extension_generate_1(Edges,Vertices,U_Es,New_Edge),
       ord_del_element(Edges,New_Edge,Edges_2),
       list_to_ord_set([New_Edge|U_Es],U_Es_2),
       beta_acyclic_component_1(Edges_2,U_Es_2,Component) )
   ; ( Component = U_Es, ! ) ),
   !.


/* bac_extension_generate_1(+Edges,+U_Vs,+U_Es,?New_Edge) <-
      Finds the edge of Edges that will be added to 
      the existing acyclic hypergraph. */

bac_extension_generate_1([Edge|_],_,[],Edge) :-
  !.
bac_extension_generate_1(Edges,U_Vs,U_Es,New_Edge) :-
   bac_extension_generate_2(Edges,U_Vs,U_Es,[N,New_Edge]),
   !,
   N \== 0.

bac_extension_generate_2([],_,_,[0,_]) :-
   !.
bac_extension_generate_2([E|Es],U_Vs,U_Es,New_Edge) :-
   bac_extension_generate_2(Es,U_Vs,U_Es,[L1,E1]),
   bac_extension_test_2(U_Vs,U_Es,[L2,E]),
   ( ( L1 > L2, 
       New_Edge = [L1,E1] )
   ; ( New_Edge = [L2,E] ) ),
   !.


/* bac_extension_test_2(+U_Vs,+U_Es,+New_E) <-
      Tests, if New_E can be put to the acyclic hypergraph 
      denoted by U_Es without getting a cycle. */

bac_extension_test_2(_,[],[0,_]) :-
   !.
bac_extension_test_2(U_Vs,[U_E|U_Es],[L,E]) :-
   bac_extension_test_2(U_Vs,U_Es,[L1,E]),
   ( ( bac_extension_test_2_sub(U_Vs,[U_E|U_Es],[L2,E],U_E,_),
       ( ( L1 = 0 
         ; L2 > L1 ), 
         L = L2 ) )
   ; ( L = L1 ) ),
   !.

bac_extension_test_2_sub(U_Vs,[U_E|_],[L,New_E],U_E,New_Vs) :-
   ord_intersection(New_E,U_Vs,Intersection),
   Intersection \== [],    
   length(Intersection,L),
   ord_subset(Intersection,U_E),
   ord_subtract(New_E,Intersection,New_Vs),
   !.
 

/******************************************************************/
/***       ALGORITHM : hypergraph_min_trans_beta_acyclic        ***/
/******************************************************************/


/* hypergraph_min_trans_beta_acyclic(+Hypergraph,?Transversals) <-
      Computes the set Transversals of minimal transversals 
      of the beta-acyclic hypergraph Hypergraph. */

hypergraph_min_trans_beta_acyclic([],[[]]) :-
   !.
hypergraph_min_trans_beta_acyclic([[]],[]) :-
   !.
hypergraph_min_trans_beta_acyclic(Hypergraph,Transversals) :-
   ord_union(Hypergraph,Vertices),
   hypergraph_ear_node(Hypergraph,Edge,Ear),
   hypergraph_remove_ear_edge_1(Hypergraph,Vertices,Edge,H_1),
   hypergraph_remove_ear_node(Hypergraph,Vertices,Ear,H_2),
   hypergraph_min_trans_beta_acyclic(H_1,Ts_1),
   hypergraph_min_trans_beta_acyclic(H_2,Ts_2),
   combine_hypergraph_transversals(Ts_1,Ts_2,Ear,Transversals).


/* hypergraph_min_trans_beta_acyclic_1(+Hypergraph,?Transversals) <-
      Computes the set Transversals of minimal transversals
      of the beta-acyclic hypergraph Hypergraph.
      Same algorithm as hypergraph_min_trans_beta_acyclic, 
      but the input may be a disordered acyclic hypergraph. */

hypergraph_min_trans_beta_acyclic_1(Hypergraph,Transversals) :-
   lists_to_ord_sets(Hypergraph,Hypergraph_1),
   state_can_2(Hypergraph_1,Hypergraph_2),
   hypergraph_min_trans_beta_acyclic(Hypergraph_2,Transversals),
   !.


/*** implementation ***********************************************/


/* hypergraph_ear_node(+Hypergraph,?Edge,?Ear_Node) <-
      Locates an ear node Ear_Node and its containing edge Edge
      in the hypergraph Hypergraph. */

hypergraph_ear_node(Hypergraph,Edge,Ear_Node) :-
   member(Edge,Hypergraph),
   member(Ear_Node,Edge),
   ord_del_element(Hypergraph,Edge,Hypergraph_2),
   \+ member_of_lists(Ear_Node,Hypergraph_2).


/* member_of_lists(+Element,+Lists) <-
      Is true, if Element is an element of one of the lists in 
      Lists. */

member_of_lists(E,[L|_]) :-
   member(E,L),
   !.
member_of_lists(E,[_|Ls]) :-
   member_of_lists(E,Ls),
   !.


/* hypergraph_remove_ear_edge(+H1,+V,+Edge,?H4) <-
      Computes the canonical form H4 of the state H1(V-Edge)-{[]}, 
      where H1(V-Edge) denotes the hypergraph H1 resticted to 
      the set of vertices V-Edge. */

hypergraph_remove_ear_edge_1(H1,V,Edge,H4) :-
   ord_subtract(V,Edge,V2),
   hypergraph_projection(H1,V2,H2),
   ord_subtract(H2,[[]],H3),
   state_can_2(H3,H4).


/* hypergraph_projection(+H1,+V1,?H2) <-
      Computes the restriction H2 of the hypergraph H1 to the 
      set of vertices V1. The result is in proper order. */

hypergraph_projection(H1,V1,H2) :-
   hypergraph_projection_list(H1,V1,Disordered_H2),
   list_to_ord_set(Disordered_H2,H2).

hypergraph_projection_list([],_,[]) :-
   !.
hypergraph_projection_list([E1|Es1],V,[E2|Es2]) :-
   ord_intersection(E1,V,E2),
   hypergraph_projection_list(Es1,V,Es2).


/* hypergraph_remove_ear_node(+H,+V,+N,?H1) <-
      Computes the canonical form H1 of the hypergraph H restricted 
      to the set of vertices V \ N, 
      where V is a set of vertices and N is a single vertex. */

hypergraph_remove_ear_node(H,V,N,H1) :-
   ord_del_element(V,N,V_Minus_N),
   hypergraph_projection(H,V_Minus_N,H_Restricted_onto_V_Minus_N),
   state_can_2(H_Restricted_onto_V_Minus_N,H1).


/* combine_hypergraph_transversals(+Ts1,+Ts2,+Ear_Node,?Ts) <-
      Combines the two sets Ts1 and Ts2 of minimal traversals
      with the ear node Ear_Node to a single set Ts of
      minimal traversals. */

combine_hypergraph_transversals(Ts1,Ts2,Ear_Node,Ts) :-
   ord_add_element_loop(Ts1,Ear_Node,Ts3),
   ord_union(Ts3,Ts2,Ts).

ord_add_element_loop([],_,[]) :-
   !.
ord_add_element_loop([L1|Ls1],E,[L2|Ls2]) :-
   ord_add_element(L1,E,L2),
   ord_add_element_loop(Ls1,E,Ls2).


/******************************************************************/
/***             ALGORITHM : coin_conjunction                   ***/
/******************************************************************/


/* coin_conjunction(+Coins,?Coin) <-
      Computes the coin Coin consisting of the minimal
      interpretations in the conjunction of Coins. */

coin_conjunction([Coin_1,Coin_2|Coins],Coin) :-
   coin_conjunction_union(Coin_1,Coin_2,Coin_3),
   coin_conjunction([Coin_3|Coins],Coin),
   !.
coin_conjunction([Coin],Coin) :-
   !.
coin_conjunction([],[]).


/*** implementation ***********************************************/


/* coin_conjunction_union(+Coin_1,+Coin_2,?Coin) <-
      Computes the coin Coin consisting of the minimal 
      interpretations in the conjunction of the two coins
      Coin_1 and Coin_2. */

coin_conjunction_union([],_,[]) :-
   !.
coin_conjunction_union(_,[],[]) :-
   !.
coin_conjunction_union(Coin_1,Coin_2,Coin) :-
   ord_union(Coin_1,Vertices_1),
   ord_union(Coin_2,Vertices_2),
   ord_intersection(Vertices_1,Vertices_2,Vertices),
   ( ( Vertices = [],
       coin_conjunction_union_2(Coin_1,Coin_2,Coin) )
   ; ( partition_coin(Coin_1,Vertices,Coin_1_a,Coin_1_b),
       partition_coin(Coin_2,Vertices,Coin_2_a,Coin_2_b),
       coin_conjunction_union_22(Coin_1_a,Coin_2_a,
          Dangerous_Coin_aa,Coin_aa_1),
          state_can_2(Dangerous_Coin_aa,Dangerous_Coin),
          state_subtract(Coin_aa_1,Dangerous_Coin,Coin_aa_2),
          ord_union(Dangerous_Coin,Coin_aa_2,Coin_aa),
       coin_conjunction_union_2(Coin_1_a,Coin_2_b,Probable_Coin_ab),
          state_subtract(Probable_Coin_ab,Dangerous_Coin,Coin_ab),
       coin_conjunction_union_2(Coin_2_a,Coin_1_b,Probable_Coin_ba),
          state_subtract(Probable_Coin_ba,Dangerous_Coin,Coin_ba),
       coin_conjunction_union_2(Coin_1_b,Coin_2_b,Coin_bb),
       ord_union([Coin_aa,Coin_ab,Coin_ba,Coin_bb],Coin) ) ),
    !.


/* partition_coin(+Coin,+Int,?Coin1,?Coin2) <-
      A coin Coin is partitioned w.r.t. an interpretation Int into
      the coin Coin1 of all interpretations intersecting Int and 
      the coin Coin2 of all interpretations not intersecting Int. */

partition_coin([I|Is],Int,Coin1,Coin2) :-
   partition_coin(Is,Int,Coin3,Coin4),
   ord_intersection(I,Int,Intersection),
   ( ( Intersection = [],
       ord_add_element(Coin4,I,Coin2), Coin1 = Coin3 )
   ; ( ord_add_element(Coin3,I,Coin1), Coin2 = Coin4 ) ),
   !.
partition_coin([],_,[],[]).


/* coin_conjunction_union_22(
         +Int_Min_Trs_1,+Int_Min_Trs_2,
         ?Dangerous_Int_Min_Trs,?Min_Trs) <-
      Computes the set Min_Trs of minimal transversals we can get 
      out of all intersecting minimal transversals of both acyclic 
      components, i.e. the union of each minimal transversal of 
      component 1 with each minimal transversal of component 2, 
      and the set Dangerous_Int_Min_Trs. This set contains all 
      those minimal transversals of the union of both acyclic 
      components, that may subsume other possible minimal 
      transversals of the union. */

coin_conjunction_union_22([],_,[],[]) :-
   !.
coin_conjunction_union_22(_,[],[],[]) :-
   !.
coin_conjunction_union_22(Int_M_Ts_1,[Int_M_Ts_2_h|Int_M_Ts_2_t],
      Dangerous_Int_Min_Trs,Min_Trs) :-
   length(Int_M_Ts_2_h,Length),
   coin_conjunction_union_11(Int_M_Ts_1,Int_M_Ts_2_h,Length,
      Dangerous_Int_Min_Trs_1,Min_Trs_1),
   coin_conjunction_union_22(Int_M_Ts_1,Int_M_Ts_2_t,
      Dangerous_Int_Min_Trs_2,Min_Trs_2),
   ord_union(Dangerous_Int_Min_Trs_1,Dangerous_Int_Min_Trs_2,
      Dangerous_Int_Min_Trs),
   ord_union(Min_Trs_1,Min_Trs_2,Min_Trs),
   !.

/* coin_conjunction_union_11(+Int_Min_Trs,+Int_Min_Tr,+Length,
         ?Dangerous_Min_Trs,?Min_Trs) <-
      computes the set Min_Trs of minimal transversals of the union of 
      the first acyclic component with the minimal transversal Int_Min_Tr 
      of the second acyclic component, i.e. the unions of each minimal
      transversal in Int_Min_Trs with Int_Min_Tr. Besides, all the
      "dangerous" minimal transversals are computed, that contain the
      minimal transversal Int_Min_Tr, i.e. that are the union of two
      intersecting minimal transversals, one of the first acyclic 
      component and Min_Tr of the second acyclic component. */

coin_conjunction_union_11([],_,_,[],[]) :-
   !.
coin_conjunction_union_11(_,[],_,[],[]) :-
   !.
coin_conjunction_union_11([I_M_Ts_h|I_M_Ts_t],Int_Min_Tr,Length,
      Dangerous_Min_Trs,Min_Trs):-
   coin_conjunction_union_11(I_M_Ts_t,Int_Min_Tr,Length,
      Dangerous_Min_Trs_1,Min_Trs_1),
   length(I_M_Ts_h,Length_1),
   Length_2 is Length_1 + Length,
   ord_union(I_M_Ts_h,Int_Min_Tr,Min_Tr_1),
   length(Min_Tr_1,Length_3),
   ( ( Length_3 < Length_2,
       ord_add_element(Dangerous_Min_Trs_1,Min_Tr_1,Dangerous_Min_Trs),
       ord_add_element(Min_Trs_1,Min_Tr_1,Min_Trs) )
   ; ( Dangerous_Min_Trs = Dangerous_Min_Trs_1,
       ord_add_element(Min_Trs_1,Min_Tr_1,Min_Trs) ) ),
   !.


/* coin_conjunction_union_2(+List_1,+List_2,?List) <-
      Computes the unions of every list in List_1 with every list 
      in List_2.
      Attention: if List_2 is the empty list, the empty list is 
      returned ! This is necessary for coin_conjunction. */

coin_conjunction_union_2([],_,[]) :-
   !.
coin_conjunction_union_2(_,[],[]) :-
   !.
coin_conjunction_union_2(L1,[L2|Ls2],List) :-
   coin_conjunction_union_1(L1,L2,L3),
   coin_conjunction_union_2(L1,Ls2,L4),
   ord_union(L3,L4,List),
   !.


/* coin_conjunction_union_1(+Lists_1,+List,?Lists_2) <-
      Lists_2 is computed by union of every list in Lists_1
      with the list List. */

coin_conjunction_union_1([],_,[]) :-
   !.
coin_conjunction_union_1([L1|Ls1],List,[L2|Ls2]):-
   ord_union(L1,List,L2),
   coin_conjunction_union_1(Ls1,List,Ls2),
   !.


/******************************************************************/
/***             ALGORITHM : coin_conjunction_2                 ***/
/******************************************************************/


/* coin_conjunction_2(+Coins,?Coin) <-
      Computes the minimal transversals of the hypergraph using 
      the minimal transversals of the components. 
      The most intersecting transversal sets are combined first. */

coin_conjunction_2([],[]) :-
   !.
coin_conjunction_2([Coin],[Coin]) :-
   !.
coin_conjunction_2(Coins,Coin) :-
   set_partition_0(Coins,Partition),
   coin_conjunction_3(Partition,Coin),
   !.

coin_conjunction_3(Partition,Coin) :-
%  last_element(Partition,[_,_,List_2,List_3]),
   member([X,Y,List_2,List_3],Partition),
   member([A,B,C,List_2],Partition),
   ord_del_element(Partition,[X,Y,List_2,List_3],Partition_1),
   ord_del_element(Partition_1,[A,B,C,List_2],Partition_2),
   coin_conjunction(List_2,List_3,List_4),
   ( ( Partition_2 = [], Coin = List_4 )
   ; ( intersection_loop_1(List_4,Partition_2,Partition_3,New_Element),
       list_to_ord_set(Partition_3,Partition_4),
       new_partition(Partition_4,List_2,List_3,List_4,Partition_5),
       ord_add_element(Partition_5,New_Element,Partition_6),
       write(Partition_6),
       coin_conjunction_3(Partition_6,Coin) ) ),
   !.


/*** implementation ***********************************************/


/* set_partition_0(+Set,?Partition) <-
      Computes the most intersecting minimal transversal set to 
      every minimal transversal set in Set and yields the 
      resulting pairs together with the length of the 
      intersection to Partition. */

set_partition_0(Set,Partition) :-
   copy_the_argument(Set,Copy),
   lists_to_ord_sets(Copy,Copy_1),
   set_partition_1(Set,Copy_1,Partition),
   !.

set_partition_1([],_,[]) :-
   !.
set_partition_1([S_head|S_tail],Copy,Partition) :-
   set_partition_1(S_tail,Copy,Partition_1),
   ord_union(S_head,Vertices),
   ord_del_element(Copy,S_head,Copy_1),
   intersection_loop(Vertices,Copy_1,[Length,Set]),
   ord_add_element(Partition_1,[Length,Vertices,Set,S_head],Partition),
   !.


/* intersection_loop(+Vertices,+Sets,?[Length,Set]) <-
      Finds the set Set in Sets having the largest intersection with
      Vertices and the cardinality Length of this intersection. */

intersection_loop(_,[],[0,[]]) :-
   !.
intersection_loop(Vertices,[Set1|Sets1],[L2,Set2]) :-
   intersection_loop(Vertices,Sets1,[L3,Set3]),
   ord_union(Set1,Vertices1),
   ord_intersection(Vertices,Vertices1,Intersection),
   length(Intersection,L1),
   ( ( L1 > L3, 
       L2 = L1, Set2 = Set1 )
   ; ( L2 = L3, Set2 = Set3 ) ),
   !.


/* new_partition(+Partition,+Coin1,+Coin2,+Coin3,?New_Partition) <-
      Replaces the earlier used minimal transversal sets Coin1 and 
      Coin2 by the minimal transversal set Coin3 which is 
      the minimal transversal set of the union of the components 
      having the minimal transversal sets Coin1 and Coin2. */

new_partition([],_,_,_,[]) :-
   !.
new_partition([[L1,V,S1,S2]|Par],Coin1,Coin2,Coin3,
      [[L2,V,S3,S2]|New_Par]) :-
   new_partition(Par,Coin1,Coin2,Coin3,New_Par),
   ( ( ( S1 = Coin1 
       ; S1 = Coin2 ), 
       S3 = Coin3,
       ord_intersection(Coin3,S2,Intersection), 
       length(Intersection,L2) )
   ; ( S3 = S1, 
       L2 = L1 ) ),
   !.


/* intersection_loop_1(+List,+Partition_1,Partition_2,?New_Element) <-
      Computes the new order of minimal transversal sets. 
      The result is returned in Partition_2). */

intersection_loop_1(List,[],[],[0,Vertices,[],List]) :-
   ord_union(List,Vertices),
   !.
intersection_loop_1(List,[[L_1,V,S_1,S_2]|List_t],
      [[L_2,V,S_3,S_2]|List_1_t],New_Element) :-
   intersection_loop_1(List,List_t,List_1_t,[L_3,Vertices,S_4,List]),
%  ord_union(List,Vertices),
   ord_intersection(Vertices,V,Intersection),
   length(Intersection,Length),
   ( ( Length > L_1, L_2 = Length, S_3 = List )
   ; ( L_2 = L_1, S_3 = S_1 ) ),
   ( ( L_3 > Length, New_Element = [L_3,Vertices,S_4,List] )
   ; ( New_Element = [Length,Vertices,S_2,List] ) ),
   !.


/******************************************************************/
/***             ALGORITHM : state_can_2                        ***/
/******************************************************************/


/* state_can_2(+State_1,?State_2) <-
      Computes the canonical form State_2 of a given state 
      State_1 using the length of the facts. */

state_can_2([],[]) :-
   !.
state_can_2([C],[C]) :-
   !.
state_can_2(State_1,State_2) :-
   ( ( member([],State_1),
       State_2 = [[]] )
   ; ( list_to_ord_set(State_1,Ordered_State_1),
       set_partition(Ordered_State_1,[[_,State]|N_States]),
       state_can_2_loop(N_States,State,State_2) ) ),
   !.

state_can_2_loop([[_,State_1]|N_States_1],State,Can) :-
   state_subtract(State_1,State,State_3),
   ord_union(State,State_3,State_4),
   state_can_2_loop(N_States_1,State_4,Can),
   !.
state_can_2_loop([],State,State).
 

/* set_partition(+Set_of_Sets,?Set_Partition) <-
      Computes a partition of Set_of_Sets, where each component 
      is a set containing all sets of Sets_of_Sets having the 
      same length. 
      The length is put at the top of the component. */

set_partition([],[]) :-
   !.
set_partition([Set|[]],[[Length,[Set|[]]]]) :-
   length(Set,Length),
   !.
set_partition([L_head|L_tail],Partition) :-
   set_partition(L_tail,Partition_1),
   length(L_head,Length),
   ( ( member([Length,Sets_With_length_Length],Partition_1),
       ord_del_element(Partition_1,
          [Length,Sets_With_length_Length],Partition_2),
       ord_add_element(Sets_With_length_Length,
          L_head,Sets_With_length_Length_1),
       ord_add_element(Partition_2,
          [Length,Sets_With_length_Length_1],Partition) )
   ; ( ord_add_element(Partition_1,[Length,[L_head]],Partition) ) ),
   !.


/******************************************************************/
/***             ALGORITHM : hypergraph_to_join_tree            ***/
/******************************************************************/


/* hypergraph_to_join_tree(+Acyclic_Hypergraph,?Join_Tree) <-
      computes the join tree of an acyclic and reduced 
      hypergraph using the graham reduction algorithm. */

hypergraph_to_join_tree([],[]):-
   !.
hypergraph_to_join_tree(Hypergraph,Hypergraph):-
   length(Hypergraph,1),
   !.
hypergraph_to_join_tree(Hypergraph,[Root|Trees]) :-
   give_numbers_to_sets(Hypergraph,_,Hypergraph_1),
   reverse(Hypergraph_1,Hypergraph_2),
   copy_the_argument(Hypergraph_2,Hypergraph_2_Copy),
   graham_join_tree(Hypergraph_2,Hypergraph_2_Copy,[],[],[Tree|_]),
   Tree = [Root_Nr|Trees_List],
   member(Root_Nr-Root,Hypergraph_2),
   list_to_ord_set(Trees_List,Trees),
   !.


/* hypergraph_to_join_tree_2(+Unreduced_Hypergraph,?Join_Tree) <-
      computes the join-tree of an unreduced and disordered 
      join-tree. */

hypergraph_to_join_tree_2(Unreduced_Hypergraph,Join_Tree) :-
   state_max_2(Unreduced_Hypergraph,Reduced_Hypergraph),
   hypergraph_to_join_tree(Reduced_Hypergraph,Join_Tree),
   !.



/*** implementation ***********************************************/


/* give_numbers_to_sets(+List,+Nr,?New_List) <-
      Puts different numbers to every set in List. */

give_numbers_to_sets([],0,[]) :-
   !.
give_numbers_to_sets([Set|[]],1,[1-Set|[]]) :-
   !.
give_numbers_to_sets([Set|Sets],N,[N-Set|N_Sets]) :-
   give_numbers_to_sets(Sets,M,N_Sets),
   N is M + 1,
   !.


/* graham_join_tree(+Ac_H_With_Numbers,+Copy,
         +Bigger_Ele,+Bigger_Ele_Attr,?Join_Tree) <-
      Computes the join-tree of the hypergraph represented by
      Ac_H_With_Numbers using the reduction-algorithm of Graham.
      The numbers in Ac_H_With_Numbers are deleted, so that a 
      proper join-tree is returned. 
      Only the root is represented by its number. */

graham_join_tree([],_,Bigger_Ele,_,Bigger_Ele) :-
   !.
graham_join_tree([X-_],_,[],[],[X]) :-
   !.
graham_join_tree([_-_],_,Bigger_Ele,Bigger_Ele_Attr,Bigger_Ele) :-
   length(Bigger_Ele_Attr,1),
   !.
graham_join_tree([],_,_,_,[]) :-
   !.
graham_join_tree(N_Hyp,Copy,Bigger_Ele,Bigger_Ele_Attr,Join_Tree):-
   hypergraph_delete_ears(N_Hyp,N_Hyp_1),
   !,
   new_join_tree(N_Hyp_1,Copy,Bigger_Ele,Bigger_Ele_Attr,
      N_Hyp_2,B_E_1,B_E_A_1,Join_Tree),
   !,
   graham_join_tree(N_Hyp_2,Copy,B_E_1,B_E_A_1,Join_Tree),
   !.


/* hypergraph_delete_ears(+Hypergraph_1,?Hypergraph_2) <-
      Deletes the ears of the acyclic hypergraph represented 
      by Hypergraph_1 and yields the result Hypergraph_2.
      Here the edges of a hypergraph are of the form N-E, 
      where N is a unique number for each edge E. */

hypergraph_delete_ears([],[]) :-
   !.
hypergraph_delete_ears(Hypergraph_1,Hypergraph_4) :-
   hypergraph_ear_node_1(Hypergraph_1,Edge_1,Ear_Node),
   ord_del_element_in_o_set(Edge_1,Ear_Node,Edge_2),
   ord_del_element(Hypergraph_1,Edge_1,Hypergraph_2),
   ord_add_element(Hypergraph_2,Edge_2,Hypergraph_3),
   hypergraph_delete_ears(Hypergraph_3,Hypergraph_4),
   !.
hypergraph_delete_ears(Hypergraph,Hypergraph) :-
   !.

ord_del_element_in_o_set(N-Set_1,E,N-Set_2) :-
   ord_del_element(Set_1,E,Set_2),
   !.


/* hypergraph_ear_node_1(+Hypergraph,?Nr-Edge,?Ear_Node) <-
      finds an ear node in the edge Edge of the hypergraph 
      represented by Hypergraph. */

hypergraph_ear_node_1(Hypergraph,Nr-Edge,Ear_Node) :-
   member(Nr-Edge,Hypergraph),
   member(Ear_Node,Edge),
   ord_del_element(Hypergraph,Nr-Edge,Hypergraph_2),
   \+ member_of_o_lists(Ear_Node,Hypergraph_2),
   !.

member_of_o_lists(E,[_-List|_]) :-
   member(E,List),
   !.
member_of_o_lists(E,[_|O_Lists]) :-
   member_of_o_lists(E,O_Lists),
   !.


/* new_join_tree(+A_H_W_N,+Copy,+Bigger_Ele,+Bigger_Ele_Attr,
         ?A_H_W_N_2,?B_E_2,?B_E_A_2,?J_T) <-
      Finds the edges, that are proper subsets of other edges and 
      deletes them. 
      Besides the elements in Bigger_Ele are extended by adding
      the original sets of these subsets or new elements are made 
      out of subset-edge and upper-edge. */

new_join_tree([X-_],_,[],[],[],[],[],[X]) :-
   !.
new_join_tree([_-_],_,Bigger_Ele,Bigger_Ele_Attr,[],
      Bigger_Ele,Bigger_Ele_Attr,J_T) :-
   length(Bigger_Ele_Attr,1),
   J_T = Bigger_Ele,
   !.
new_join_tree(A_H_W_N,Copy,Bigger_Ele,Bigger_Ele_Attr,
      A_H_W_N_2,B_E_2,B_E_A_2,J_T) :-
   member(Nr1-Set1,A_H_W_N),
   member(Nr2-Set2,A_H_W_N),
   Nr1 \== Nr2,
   ord_subset(Set1,Set2),
   ord_del_element(A_H_W_N,Nr1-Set1,A_H_W_N_1),
   new_join_tree_step(
      Copy,Nr1,Nr2,Bigger_Ele,Bigger_Ele_Attr,B_E_1,B_E_A_1),
   new_join_tree(
      A_H_W_N_1,Copy,B_E_1,B_E_A_1,A_H_W_N_2,B_E_2,B_E_A_2,J_T),
   !.
new_join_tree(A_H_W_N,_,Bigger_Ele,Bigger_Ele_Attr,A_H_W_N,
      Bigger_Ele,Bigger_Ele_Attr,_) :-
   !.


/* new_join_tree_step(+Copy,+Nr1,+Nr2,+Bigger_Ele,+Bigger_Ele_Attr,
         ?B_E_1,?B_E_A_1) <-
      Here where we decide how to extend one branch of the join tree.
      There are four cases:
      1. Both, the extended and the extending element are in Bigger_Ele.
      2. The extending element is in Bigger_Ele, the extended is only
         in Ac_H_W_N.
      3. The extended element is in Bigger_Ele, the extending is only
         in Ac_H_W_N.
      4. Both, the extended and the extending element are only in
         Ac_H_W_N. */

new_join_tree_step(Copy,Nr1,Nr2,Bigger_Ele,Bigger_Ele_Attr,
      Bigger_Ele_3,Bigger_Ele_Attr_1) :-
   member(Nr1,Bigger_Ele_Attr),
   member(Nr2,Bigger_Ele_Attr),
   member([Nr1|X],Bigger_Ele),
   member([Nr2|Y],Bigger_Ele),
   member(Nr1-Set,Copy),
   New_Ele = [Nr2|[[Set|X]|Y]],
   ord_del_element(Bigger_Ele,[Nr1|X],Bigger_Ele_1),
   ord_del_element(Bigger_Ele_1,[Nr2|Y],Bigger_Ele_2),
   ord_add_element(Bigger_Ele_2,New_Ele,Bigger_Ele_3),
   ord_del_element(Bigger_Ele_Attr,Nr1,Bigger_Ele_Attr_1),
   !.

new_join_tree_step(Copy,Nr1,Nr2,Bigger_Ele,Bigger_Ele_Attr,
      Bigger_Ele_2,Bigger_Ele_Attr_2) :-
   member(Nr1,Bigger_Ele_Attr),
   member([Nr1|X],Bigger_Ele),
   member(Nr1-Set,Copy),
   New_Ele = [Nr2,[Set|X]],
   ord_del_element(Bigger_Ele,[Nr1|X],Bigger_Ele_1),
   ord_add_element(Bigger_Ele_1,New_Ele,Bigger_Ele_2),
   ord_del_element(Bigger_Ele_Attr,Nr1,Bigger_Ele_Attr_1),
   ord_add_element(Bigger_Ele_Attr_1,Nr2,Bigger_Ele_Attr_2),
   !.

new_join_tree_step(Copy,Nr1,Nr2,Bigger_Ele,Bigger_Ele_Attr,
      Bigger_Ele_2,Bigger_Ele_Attr) :-
   member(Nr2,Bigger_Ele_Attr),
   member([Nr2|Y],Bigger_Ele),
   member(Nr1-Set,Copy),
   New_Ele = [Nr2|[[Set]|Y]],
   ord_del_element(Bigger_Ele,[Nr2|Y],Bigger_Ele_1),
   ord_add_element(Bigger_Ele_1,New_Ele,Bigger_Ele_2),
   !.

new_join_tree_step(Copy,Nr1,Nr2,Bigger_Ele,Bigger_Ele_Attr,
      Bigger_Ele_1,Bigger_Ele_Attr_1) :-
   member(Nr1-Set,Copy),
   New_Ele = [Nr2,[Set]],
   ord_add_element(Bigger_Ele,New_Ele,Bigger_Ele_1),
   ord_add_element(Bigger_Ele_Attr,Nr2,Bigger_Ele_Attr_1),
   !.


/* copy_the_argument(X,X) <-
      makes a copy of one of the arguments. */
 
copy_the_argument(X,X) :-
   !.
 

/******************************************************************/
/***             ALGORITHM : state_max                          ***/
/******************************************************************/
 

/* state_max(State_1,State_2) <-
      The state State_2 is computed as the set of all maximal
      elements of the state State_1. */

state_max(State_1,State_2) :-
   length_order_lists(State_1,State_a),
   real_subsets_out(State_a,State_b),
   list_to_ord_set(State_b,State_2),
   !.

 
/* state_max_2(+Hypergraph,?Reduced_Hypergraph) <-
      The state State_2 is computed as the set of all maximal
      elements of the state State_1.
      Contrary to algorithm state_max, the state may be
      an unordered list of unordered lists, where Elements and 
      clauses are allowed to occur several times. */
 
state_max_2(Hypergraph,Reduced_Hypergraph) :-
   list_of_lists_to_list_of_ord_sets(Hypergraph,Hypergraph_2),
   state_max(Hypergraph_2,Reduced_Hypergraph),
   !.
 

/*** implementation ***********************************************/


/* length_order_lists(+Lists,?O_Lists) <-
      Orders the lists in Lists by using their length. */

length_order_lists(Lists,O_Lists) :-
   assign_lengths_to_lists_1(Lists,O_Lists_1),
   keysort(O_Lists_1,O_Lists),
   !.


/* assign_lengths_to_lists_1(+List,?O_List) <-
      Attributes the elements of a given list with their 
      respective lengths. */

assign_lengths_to_lists_1([],[]) :-
   !.
assign_lengths_to_lists_1([L|Ls],[N-L|O_Ls]) :-
   length(L,N),
   assign_lengths_to_lists_1(Ls,O_Ls).


/* real_subsets_out(+O_Sets,Sets) <-
      The set Sets is computed as the set of all maximal 
      elements of the length ordered set O_Sets. */

real_subsets_out([],[]) :-
   !.
real_subsets_out([L-S|O_Sets],Sets) :-
   real_subset_of_one_of_the_sets(L-S,O_Sets),
   real_subsets_out(O_Sets,Sets).
real_subsets_out([_-S|O_Sets],[S|Sets]) :-
   real_subsets_out(O_Sets,Sets).

real_subset_of_one_of_the_sets(L1-S1,[L2-S2|_]) :-
   L1 < L2,
   ord_subset(S1,S2),
   !.
real_subset_of_one_of_the_sets(L1-S1,[_|O_Sets]) :-
   real_subset_of_one_of_the_sets(L1-S1,O_Sets).


/******************************************************************/


