

/******************************************************************/
/***                                                            ***/
/***          Graphs:  Tree Traversals                          ***/
/***                                                            ***/
/******************************************************************/


:- module( tree_traversals, [ ] ).

:- dynamic edge_tmp/2, node_visited/1.


/*** interface ****************************************************/


/* traverse(Mode, Roots, Edges, Tree_Edges) <-
      Mode can be bfs or dfs. */

traverse(_, Roots, _, []) :-
   var(Roots),
   !.
traverse(bfs, Roots, Edges, Tree_Edges) :-
   prepare_traverse(Roots, Edges),
   traverse(bfs, Edges, Roots, [], Tree_Edges),
   !.
traverse(dfs, Roots, Edges, Tree_Edges) :-
   prepare_traverse(Roots, Edges),
   traverse(dfs, Edges, Roots, [], Tree_Edges_2),
   reverse(Tree_Edges_2, Tree_Edges),
   !.


/*** implementation ***********************************************/


/* prepare_traverse(Roots, Edges) <-
      */

prepare_traverse(Roots, Edges) :-
   retractall( edge_tmp(_, _) ),
   retractall( node_visited(_) ),
   forall( member(V-W, Edges),
      assert( edge_tmp(V, W) )  ),
   forall( member(R, Roots),
      assert( node_visited(R) )  ).


/* traverse(Mode, Edges, Roots, Temps, Tree_Edges) <-
      computes a set Tree_Edges. */

traverse(bfs, Edges, [Node|Nodes_1], Ts_1, Tree_Edges) :-
   node_to_sons(Node, Sons),
   set_tree_edge(Node, Sons, Ts_1, Ts_2),
   forall( member(S, Sons),
      assert( node_visited(S) ) ),
   append_with_list_to_set(Nodes_1, Sons, Nodes_2),
   traverse(bfs, Edges, Nodes_2, Ts_2, Tree_Edges).
traverse(dfs, Edges, [V|Vs_1], Ts_1, Tree_Edges) :-
   node_to_sons(V, [W|Ws]),
   Ts_2 = [V-W|Ts_1],
   assert( node_visited(W) ),
   append_with_list_to_set([W, V|Ws], Vs_1, Vs_2),
   traverse(dfs, Edges, Vs_2, Ts_2, Tree_Edges).
traverse(dfs, Edges, [Node|Nodes], Ts, Tree_Edges) :-
   node_to_sons(Node, []),
   traverse(dfs, Edges, Nodes, Ts, Tree_Edges).
traverse(_, _, [], Tree_Edges, Tree_Edges).


/* append_with_list_to_set(L_1, L_2, L_4) <-
      */

append_with_list_to_set(L_1, L_2, L_4) :-
   append(L_1, L_2, L_3),
   list_to_set(L_3, L_4).


/* node_to_sons(Edges, Visited, Node, Sons) <-
      */

node_to_sons(Edges, Visited, Node, Sons) :-
   findall( Son,
      ( member(Node-Son, Edges),
        not(member(Son, Visited)) ),
      Sons ).


/* node_to_sons(Node, Sons) <-
      */

node_to_sons(Node, Sons) :-
   findall( Son,
      ( tree_traversals:edge_tmp(Node, Son),
        not( tree_traversals:node_visited(Son) ) ),
      Sons ).


/* set_tree_edge(Node, Sons, Tree_Edges_1, Tree_Edges_2) <-
      */

set_tree_edge(Node, Sons, Tree_Edges_1, Tree_Edges_2) :-
   findall( Node-Son,
      member(Son, Sons),
      Tree_Edges ),
   append(Tree_Edges_1, Tree_Edges, Tree_Edges_2).


/******************************************************************/


