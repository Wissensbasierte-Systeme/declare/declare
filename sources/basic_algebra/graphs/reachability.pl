

/******************************************************************/
/***                                                            ***/
/***          Graphs:  Reachability                             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* reaching_edges_multiple(Vertices, Edges_1, Edges_2) <-
      */

reaching_edges_multiple(Vertices, Edges_1, Edges_2) :-
   maplist( edge_invert,
      Edges_1, Edges_3 ),
   reachable_edges_multiple(Vertices, Edges_3, Edges_4),
   maplist( edge_invert,
      Edges_4, Edges_2 ).


/* reachable_edges_multiple(Vertices, Edges_1, Edges_2) <-
      */

reachable_edges_multiple(Vertices, Edges_1, Edges_2) :-
   reachable_through_edges_multiple(
      Vertices, Edges_1, Vertices_2),
   edges_projection(Vertices_2, Edges_1, Edges_2).

reachable_through_edges_multiple(
      Vertices_1, Edges, Vertices_2) :-
   vertices_edges_to_ugraph(Vertices_1, Edges, Graph),
   ugraphs:reachable(
      Vertices_1, Graph, Vertices_1, Vertices_2).


/* reaching_edges(Vertex, Edges_1, Edges_2) <-
      */

reaching_edges(Vertex, Edges_1, Edges_2) :-
   maplist( edge_invert,
      Edges_1, Edges_3 ),
   reachable_edges(Vertex, Edges_3, Edges_4),
   maplist( edge_invert,
      Edges_4, Edges_2 ).

edge_invert(V-W, W-V).


/* reachable_edges(Vertex, Edges_1, Edges_2) <-
      */

reachable_edges(Vertex, Edges_1, Edges_2) :-
   reachable_through_edges(Vertex, Edges_1, Vertices),
   edges_projection(Vertices, Edges_1, Edges_2).

reachable_through_edges(Vertex, Edges, Vertices) :-
   vertices_edges_to_ugraph([Vertex], Edges, Graph),
   reachable(Vertex, Graph, Vertices).


/* edges_projection(Vertices, Edges_1, Edges_2) <-
      */

edges_projection(Vertices, Edges_1, Edges_2) :-
   findall( V-W,
      ( member(V-W, Edges_1),
        subset([V, W], Vertices) ),
      Edges_2 ).


/******************************************************************/


