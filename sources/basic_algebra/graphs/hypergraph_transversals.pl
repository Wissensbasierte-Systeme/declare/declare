

/******************************************************************/
/***                                                            ***/
/***       Hypergraphs:  Transversals                           ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(hypergraphs, transversals) :-
   Edges = [[a,b], [c,d,e], [f,g]],
   maplist( concat,
      Edges, Strings_1 ),
   hypergraph_transversals(Edges, Transversals),
   maplist( concat,
      Transversals, Strings_2 ),
   writeln(Strings_1 -> Strings_2).


/*** interface ****************************************************/


/* hypergraph_transversals(Edges, Transversals) <-
      */

hypergraph_transversals(Edges, Transversals) :-
   findall( Transversal,
      hypergraph_transversal(Edges, Transversal),
      Transversals ).

hypergraph_transversal([E|Es], [V|Vs]) :-
   member(V, E),
   hypergraph_transversal(Es, Vs).
hypergraph_transversal([], []).


/******************************************************************/


