

/******************************************************************/
/***                                                            ***/
/***             Colin Barker:  Dijkstra Algorithm              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dijkstra_find_path_barker(
         Graph, Source, Target, Cost:Path) <-
      */

dijkstra_find_path_barker(Graph, Source, Target, Cost:Path) :-
   dabolish(dijkstra_edge/3),
   ( foreach(U-(V-W), Graph) do
        assert(dijkstra_edge(U, V, W)) ),
   dijkstra_barker(Source, Target, Cost:Path),
   dabolish(dijkstra_edge/3).


/* dijkstra_find_path_barker(Source, Target, Cost:Path) <-
      */

dijkstra_find_path_barker(Source, Target, Cost:Path) :-
   Rule =
      (dijkstra_edge(U, V, W) :- dijkstra_edge_barker(U, V, W)),
   assert(Rule),
   dijkstra_barker(Source, Target, Cost:Path),
   retract(Rule).


/* dijkstra_barker(Source, Target, Cost:Path) <-
      */

dijkstra_barker(Source, Target, Cost:Path) :-
   dijkstra_barker(Source, Structure),
   member(s(Target, Cost, Path), Structure).


/* dijkstra_barker(Vertex0, Ss) <-
      is true if Ss is a list of structures s(Vertex, Dist, Path)
      containing the shortest Path from Vertex0 to Vertex,
      the distance of the path being Dist.
      The graph is defined by e/3.
      e.g. dijkstra_barker(penzance, Ss) */

dijkstra_barker(Vertex, Ss) :-
   dijkstra_create(Vertex, [Vertex], Ds),
   dijkstra_barker_1(Ds, [s(Vertex,0,[])], Ss).

dijkstra_barker_1([], Ss, Ss).
dijkstra_barker_1([D|Ds], Ss0, Ss) :-
   dijkstra_best(Ds, D, S),
   dijkstra_delete([D|Ds], [S], Ds1),
   S = s(Vertex, Distance, Path),
   reverse([Vertex|Path], Path1),
   dijkstra_merge(Ss0, [s(Vertex, Distance, Path1)], Ss1),
   dijkstra_create(Vertex, [Vertex|Path], Ds2),
   dijkstra_delete(Ds2, Ss1, Ds3),
   dijkstra_incr(Ds3, Distance, Ds4),
   dijkstra_merge(Ds1, Ds4, Ds5),
   dijkstra_barker_1(Ds5, Ss1, Ss).


/*** implementation ***********************************************/


/* dijkstra_path(Vertex0, Vertex, Path, Dist) <-
      is true if Path is the shortest path from Vertex0 to Vertex,
      and the length of the path is Dist.
      The graph is defined by e/3.
      e.g. dijkstra_path(penzance, london, Path, Dist) */

dijkstra_path(Vertex0, Vertex, Path, Dist) :-
   dijkstra_barker(Vertex0, Ss),
   member(s(Vertex, Dist, Path), Ss),
   !.


/* dijkstra_create(Start, Path, Edges) <-
      is true if Edges is a list of structures
      s(Vertex, Distance, Path) containing, for each Vertex
      accessible from Start, the Distance from the Vertex
      and the specified Path.
      The list is sorted by the name of the Vertex. */

dijkstra_create(Start, Path, Edges) :-
   setof( s(Vertex, Edge, Path),
      dijkstra_edge(Start, Vertex, Edge),
      Edges ),
   !.
dijkstra_create(_, _, []).


/* dijkstra_best(Edges, Edge0, Edge) <-
      is true if Edge is the element of Edges, a list of structures
      s(Vertex, Distance, Path), having the smallest Distance.
      Edge0 constitutes an upper bound. */

dijkstra_best([], Best, Best).
dijkstra_best([Edge|Edges], Best0, Best) :-
   shorter(Edge, Best0),
   !,
   dijkstra_best(Edges, Edge, Best).
dijkstra_best([_|Edges], Best0, Best) :-
  dijkstra_best(Edges, Best0, Best).

shorter(s(_,X,_), s(_,Y,_)) :-
   X < Y.


/* dijkstra_delete(Xs, Ys, Zs) <-
      is true if Xs, Ys and Zs are lists of structures
      s(Vertex, Distance, Path) ordered by Vertex,
      and Zs is the result of deleting from Xs those elements
      having the same Vertex as elements in Ys. */

dijkstra_delete([], _, []). 
dijkstra_delete([X|Xs], [], [X|Xs]) :-
   !. 
dijkstra_delete([X|Xs], [Y|Ys], Ds) :-
   dijkstra_eq(X, Y),
   !, 
   dijkstra_delete(Xs, Ys, Ds). 
dijkstra_delete([X|Xs], [Y|Ys], [X|Ds]) :-
   dijkstra_lt(X, Y),
   !,
   dijkstra_delete(Xs, [Y|Ys], Ds). 
dijkstra_delete([X|Xs], [_|Ys], Ds) :-
   dijkstra_delete([X|Xs], Ys, Ds). 


/* dijkstra_merge(Xs, Ys, Zs) <-
      is true if Zs is the result of merging Xs and Ys,
      where Xs, Ys and Zs are lists of structures
      s(Vertex, Distance, Path), and are ordered by Vertex.
      If an element in Xs has the same Vertex as an element
      in Ys, the element with the shorter Distance
      will be in Zs. */

dijkstra_merge([], Ys, Ys). 
dijkstra_merge([X|Xs], [], [X|Xs]) :-
   !. 
dijkstra_merge([X|Xs], [Y|Ys], [X|Zs]):-
   dijkstra_eq(X, Y),
   shorter(X, Y),
   !, 
   dijkstra_merge(Xs, Ys, Zs).
dijkstra_merge([X|Xs], [Y|Ys], [Y|Zs]) :-
   dijkstra_eq(X, Y),
   !, 
   dijkstra_merge(Xs, Ys, Zs).
dijkstra_merge([X|Xs], [Y|Ys], [X|Zs]) :-
   dijkstra_lt(X, Y),
   !, 
   dijkstra_merge(Xs, [Y|Ys], Zs).
dijkstra_merge([X|Xs], [Y|Ys], [Y|Zs]) :-
   dijkstra_merge([X|Xs], Ys, Zs).


/* dijkstra_eq(s(X,_,_), s(X,_,_)) <-
      */

dijkstra_eq(s(X,_,_), s(X,_,_)).  


/* dijkstra_lt(s(X,_,_), s(Y,_,_)) <-
      */

dijkstra_lt(s(X,_,_), s(Y,_,_)) :-
   X @< Y.


/* dijkstra_incr(Xs, Incr, Ys) <-
      is true if Xs and Ys are lists of structures
      s(Vertex, Distance, Path), the only difference being
      that the value of Distance in Ys is Incr more than
      that in Xs. */

dijkstra_incr([], _, []).
dijkstra_incr([s(V, D1, P)|Xs], Incr, [s(V, D2, P)|Ys]) :-
   D2 is D1 + Incr,
   dijkstra_incr(Xs, Incr, Ys).


/* member(X, Ys) <-
      is true if the element X is contained in the list Ys. */

/*
member(X, [X|_]).
member(X, [_|Ys]) :-
   member(X, Ys).
*/


/* reverse(Xs, Ys) <-
      is true if Ys is the result of reversing the order
      of the elements in the list Xs. */

/*
reverse(Xs, Ys) :-
   reverse_1(Xs, [], Ys).

reverse_1([], As, As).
reverse_1([X|Xs], As, Ys) :-
   reverse_1(Xs, [X|As], Ys).
*/


/* dijkstra_edge(V, W, Dist) <-
      */

/*
dijkstra_edge(V, W, Dist) :-
   dijkstra_edge_barker(V, W, Dist).
*/


/******************************************************************/


