

/******************************************************************/
/***                                                            ***/
/***          Declare:  Dependency Graphs                       ***/
/***                                                            ***/
/******************************************************************/


/* program_to_vertices_and_edges(Program,Vertices,Edges) <-
      */

program_to_vertices_and_edges(Program,Vertices,Edges) :-
   maplist( rule_to_vertices,
      Program, Vertices_Multiple ),
   ord_union(Vertices_Multiple,Vertices),
   maplist( rule_to_edges,
      Program, Edges_Multiple ),
   ord_union(Edges_Multiple,Edges).

rule_to_vertices(Rule,Vertices) :-
   parse_dislog_rule(Rule,Head,Pos,Neg),
   atoms_to_predicate_symbols(Head,P_Head),
   atoms_to_predicate_symbols(Pos,P_Pos),
   atoms_to_predicate_symbols(Neg,P_Neg),
   ord_union([P_Head,P_Pos,P_Neg],Vertices).

rule_to_edges(Rule,Edges) :-
   parse_dislog_rule(Rule,Head,Pos,Neg),
   atoms_to_predicate_symbols(Head,Ps_Head),
   atoms_to_predicate_symbols(Pos,Ps_Pos),
   atoms_to_predicate_symbols(Neg,Ps_Neg),
   findall( P1-P2-'=',
      ( member(P1,Ps_Head), member(P2,Ps_Head), P1 \= P2 ),
      Edges_Head ),
   findall( P1-P2-'+',
      ( member(P1,Ps_Head), member(P2,Ps_Pos), P1 \= P2 ),
      Edges_Pos ),
   findall( P1-P2-'-',
      ( member(P1,Ps_Head), member(P2,Ps_Neg), P1 \= P2 ),
      Edges_Neg ),
   append([Edges_Head,Edges_Pos,Edges_Neg],Edge_List),
   list_to_ord_set(Edge_List,Edges).


/* program_to_dependency_graph( Program,
         [Nodes,Equal_Edges,Positive_Edges,Negative_Edges]) <-
      */

program_to_dependency_graph_file(Database_Id) :-
   dconsult(Database_Id,Program),
   program_to_dependency_graph(Program).

program_to_dependency_graph(Program) :-
   ground_transformation(Program,Ground_Program),
   ground_program_to_dependency_graph(Ground_Program).

program_to_dependency_graph(Program,
      [Nodes,Equal_Edges,Positive_Edges,Negative_Edges]) :-
   ground_transformation(Program,Ground_Program),
   ground_program_to_dependency_graph(Ground_Program,
      Nodes,Equal_Edges,Positive_Edges,Negative_Edges).

ground_program_to_dependency_graph_file(Database_Id) :-
   dconsult(Database_Id,Program),
   ground_program_to_dependency_graph(Program).

ground_program_to_dependency_graph(Program) :-
   ground_program_to_dependency_graph(Program,
      Nodes,Equal_Edges,Positive_Edges,Negative_Edges),
   grl_pp_dependency_graph(Nodes,
      Equal_Edges,Positive_Edges,Negative_Edges).

ground_program_to_dependency_graph(Program,
      Nodes,Equal_Edges,Positive_Edges,Negative_Edges) :-
   ground_program_to_occuring_atoms(Program,Nodes),
   findall( [A,B],
      equally_depends_on(Program,B,A),
      Equal_Edges ),
   findall( [A,B],
      positively_depends_on(Program,B,A),
      Positive_Edges ),
   findall( [A,B],
      negatively_depends_on(Program,B,A),
      Negative_Edges ).

positively_depends_on(Program,Atom1,Atom2) :-
   member(Rule,Program),
   parse_dislog_rule(Rule,Head,Pos_Body,_Neg_Body),
   member(Atom2,Pos_Body),
   member(Atom1,Head).
negatively_depends_on(Program,Atom1,Atom2) :-
   member(Rule,Program),
   parse_dislog_rule(Rule,Head,_Pos_Body,Neg_Body),
   member(Atom2,Neg_Body),
   member(Atom1,Head).
equally_depends_on(Program,Atom1,Atom2) :-
   member(Rule,Program),
   parse_dislog_rule(Rule,Head,_Pos_Body,_Neg_Body),
   append(_,[Atom2|List],Head),
   member(Atom1,List),
   Atom1 \== Atom2.


/******************************************************************/


