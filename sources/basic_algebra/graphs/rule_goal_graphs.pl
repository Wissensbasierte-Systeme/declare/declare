

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Rule/Goal Graphs                         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* program_to_predicate_symbols(Program, Predicates) <-
      */

program_to_predicate_symbols(Program, Predicates) :-
   findall( P,
      ( member(Rule, Program),
        parse_dislog_rule(Rule, Head, Body_1, Body_2),
        ( member(A, Head)
        ; member(A, Body_1)
        ; member(A, Body_2) ),
        functor(A, P, _) ),
      Ps ),
   list_to_ord_set(Ps, Predicates).


/* prolog_program_to_predicate_symbols(Prolog, Predicates) <-
      */

prolog_program_to_predicate_symbols(Prolog, Predicates) :-
   prolog_rules_to_dislog_rules(Prolog, DisLog),
   program_to_predicate_symbols(DisLog, Predicates).


/* prolog_program_to_predicate_symbols_with_arity(
         Program, Predicates) <-
      */

prolog_program_to_predicate_symbols_with_arity(
      Program, Predicates) :-
   findall( P,
      ( member(Rule, Program),
        Rule = (F1 :- F2),
        ( prolog_formula_to_predicate(F1, P) 
        ; prolog_formula_to_predicate(F2, P) ) ),
      Ps ),
   sort(Ps, Predicates).

 
/* prolog_program_dynamize_predicate_symbols(Module, Program) <-
      */

prolog_program_dynamize_predicate_symbols(Program) :-
   prolog_program_dynamize_predicate_symbols(user, Program).

prolog_program_dynamize_predicate_symbols(Module, Program) :-
   prolog_program_to_predicate_symbols_with_arity(
      Program, Predicates),
%  writeln(user, Predicates),
   forall( member(Predicate, Predicates),
      prolog_program_dynamize_predicate_symbol(
         Module, Predicate) ).

prolog_program_dynamize_predicate_symbol(_, P/N) :-
   functor(Atom, P, N),
   catch(user:clause(Atom, _), _, fail),
   !.
prolog_program_dynamize_predicate_symbol(Module, P/N) :-
   catch(Module:dynamic(P/N), _, true).


/* program_to_rule_goal_graph(
         Program, Vertices, Edges_P, Edges_N) <-
      */

program_to_rule_goal_graph(
      Program, Vertices, Edges_P, Edges_N) :-
   maplist( rule_to_vertices_and_edges,
      Program, Triples ),
   triple_lists(Vs, Es_P, Es_N, Triples),
   append(Vs, Vertices_2),
   list_to_ord_set(Vertices_2, Vertices),
   append(Es_P, Edges_1),
   list_to_ord_set(Edges_1, Edges_P),
   append(Es_N, Edges_2),
   list_to_ord_set(Edges_2, Edges_N).

rule_to_vertices_and_edges(Rule, [Vertices, Edges_P, Edges_N]) :-
   parse_dislog_rule(Rule, Head, Body_1, Body_2),
   findall( P,
      ( ( member(A, Head)
        ; member(A, Body_1)
        ; member(A, Body_2) ),
        functor(A, P, _) ),
      Vertices_2 ),
   gensym(rule_, N),
   list_to_ord_set([N|Vertices_2], Vertices),
   findall( P-N,
      ( member(A, Head),
        functor(A, P, _) ),
      Edges_1 ),
   findall( N-P,
      ( member(A, Body_1),
        functor(A, P, _) ),
      Edges_2 ),
   append([Edges_1, Edges_2], Edges_P),
   findall( N-P,
      ( member(A, Body_2),
        functor(A, P, _) ),
      Edges_N ).


/* predicate_is_recursive(Program, Predicate) <-
      */

predicate_is_recursive(Program, Predicate) :-
   program_to_rule_goal_graph(Program, _, Es, Fs),
   ord_union(Es, Fs, Edges),
   transitive_closure_edges(Edges, Tc_Edges),
   member(Predicate-Predicate, Tc_Edges),
   program_to_predicate_symbols(Program, Predicates),
   !,
   member(Predicate, Predicates).


/*** tests ********************************************************/


test(rule_goal_graphs, 1) :-
   Program = [
      [tc(X, Y)]-[arc(X, Y)],
      [tc(X, Y)]-[arc(X, Z), tc(Z, Y)] ],
   predicate_is_recursive(Program, tc).


/******************************************************************/


