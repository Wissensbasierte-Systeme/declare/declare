

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Matrices                                ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(vectors, matrix_vector_product) :-
   matrix_vector_product([[1,1],[-1,1]], [1,0], Vector),
   writeq(user, Vector).


/*** interface ****************************************************/


/* affine_transformation(Matrix, Offset, Vector_1, Vector_2) <-
      Vector_2 = Matrix * Vector_1 + Offeset. */

affine_transformation(Matrix, Offset, Vector_1, Vector_2) :-
   matrix_vector_product(Matrix, Vector_1, Vector_3),
   vector_add(Offset, Vector_3, Vector_2).


/* matrix_multiply(Factor, Matrix_1, Matrix_2) <-
      */

matrix_multiply(Factor, Matrix_1, Matrix_2) :-
   maplist( vector_multiply(Factor),
      Matrix_1, Matrix_2 ).


/* matrix_power(N, Matrix_1, Matrix_2) <-
      */

matrix_power(N, Matrix_1, Matrix_2) :-
   N > 1,
   !,
   M is N - 1,
   matrix_power(M, Matrix_1, Matrix_3),
   matrix_product(Matrix_3, Matrix_1, Matrix_2).
matrix_power(1, Matrix, Matrix).


/* matrix_transpose(Matrix_1, Matrix_2) <-
      */

matrix_transpose(Matrix_1, Matrix_2) :-
   findall( Vector,
      maplist( nth(_),
         Matrix_1, Vector ),
      Matrix_2 ).


/* matrix_product(Matrix_1, Matrix_2, Matrix_3) <-
      */

matrix_product(Matrix_1, Matrix_2, Matrix_3) :-
   matrix_transpose(Matrix_2, Vectors_a),
   maplist( matrix_vector_product(Matrix_1),
      Vectors_a, Vectors_b ),
   matrix_transpose(Vectors_b, Matrix_3).

  
/* matrix_vector_product(Matrix, Vector_1, Vector_2) <-
      */

matrix_vector_product(Matrix, Vector_1, Vector_2) :-
   foreach(Row, Matrix), foreach(X_2, Vector_2) do
      scalar_product(Row, Vector_1, X_2).


/* matrix_to_colomn(N, Matrix, Column) <-
      */

matrix_to_colomn(N, Matrix, Column) :-
   maplist( nth(N),
      Matrix, Column ).


/* matrix_generate(N*M, Range, Matrix) <-
      */

matrix_generate(N*M, Range, Matrix) :-
   for(_, 1, M), foreach(Row, Matrix) do
      row_generate(N, Range, Row).

row_generate(N, [I,J], Row) :-
   for(_, 1, N), foreach(Value, Row) do
        between(I, J, Value).


/******************************************************************/


