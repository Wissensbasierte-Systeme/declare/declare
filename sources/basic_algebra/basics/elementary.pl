

/******************************************************************/
/***                                                            ***/
/***           DDK:  Elementary Predicates                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* multify(X, N, Xs) <-
      */

multify(_, 0, []) :-
   !.
multify(X, N, [X|Xs]) :-
   M is N - 1,
   multify(X, M, Xs).


/* n_free_variables(N, Vs) <-
      */

n_free_variables(N, Vs) :-
   n_free_variables(1, N, Vs).
 
n_free_variables(I, N, [V|Vs]) :-
   I =< N,
   !,
   copy_term(_, V),
   J is I + 1,
   n_free_variables(J, N, Vs).
n_free_variables(_, _, []).


/* arg_multiple(Ns, Term, As) <-
      */

arg_multiple(Ns, Term, As) :-
   foreach(N, Ns), foreach(A, As) do
      arg(N, Term, A).


/* terms_unify(Terms) <-
      */

terms_unify(Terms) :-
   maplist( =(_), Terms ).


/* term_to_term_depth(Term, Depth) <-
      */

term_to_term_depth(Term, 0) :-
   Term =.. [_],
   !.
term_to_term_depth(Term, Depth) :-
   Term =.. [_|Terms],
   maplist( term_to_term_depth,
      Terms, Depths ),
   maximum(Depths, Depth_2),
   Depth is Depth_2 + 1.
 

/* generate_interval(I, J, Interval) <-
      the interval Interval = [I,I+1,...,J] of all integers
      between two given integers I and J is generated. */

generate_interval(I, J, []) :-
   I > J,
   !.
generate_interval(I, J, [I|Interval]) :-
   K is I + 1,
   generate_interval(K, J, Interval).


/* randoms(N,Max,Randoms) <-
      */

randoms(N,Max,Randoms) :-
   randoms(0,N,Max,Numbers),
   list_to_ord_set(Numbers,Randoms),
   length(Randoms,N).
 
randoms(I,N,Max,[X|L]) :-
   I < N,
   !,
   X is random(Max),
   J is I + 1,
   randoms(J,N,Max,L).
randoms(_,_,_,[]).


/* list_to_cartesian_power(N, Xs, Tuples) <-
      */

list_to_cartesian_power(N, Xs, Tuples) :-
   list_of_elements_to_relation(Xs, Ts),
   list_to_cartesian_power_loop(N, Ts, Tuples).
   
list_to_cartesian_power_loop(0, _, [[]]) :-
   !.
list_to_cartesian_power_loop(N, Ts, Tuples) :-
   M is N - 1,
   list_to_cartesian_power_loop(M, Ts, Tuples_2),
   cartesian_product(Ts, Tuples_2, Tuples).
   

/* cartesian_product(Relations,Product) :-
      the relation Product is the cartesian product of the
      relations in the list Relations. */
 
cartesian_product([Set],Product) :-
   list_of_elements_to_relation(Set,Product),
   !.
cartesian_product([Set|Sets],Product) :-
   cartesian_product(Sets,Relation1),
   list_of_elements_to_relation(Set,Relation2),
   cartesian_product(Relation1,Relation2,Product).
cartesian_product([],[]).
 
 
/* cartesian_product(Relation1,Relation2,Relation3) <-
      the relation Relation3 is the cartesian product of the
      relations Relation1 and Relation2. */
 
cartesian_product(Relation1,Relation2,Relation3) :-
   cartesian_product(Relation1,Relation2,[],Relation3).
 
cartesian_product(
      [Tuple1|Relation1],Relation2,Sofar,Relation3) :-
   cartesian_product_loop(Tuple1,Relation2,Relation),
   append(Relation,Sofar,New_Sofar),
   cartesian_product(Relation1,Relation2,New_Sofar,Relation3).
cartesian_product([],_,Sofar,Sofar).
 
cartesian_product_loop(Tuple,Relation1,Relation2) :-
   cartesian_product_loop(Tuple,Relation1,[],Relation2).
 
cartesian_product_loop(
      Tuple,[Tuple1|Relation1],Sofar,Relation2) :-
   append(Tuple,Tuple1,Tuple2),
   cartesian_product_loop(
      Tuple,Relation1,[Tuple2|Sofar],Relation2).
cartesian_product_loop(_,[],Sofar,Sofar).


/* size_of_cartesian_product(Css, N) <-
      */

size_of_cartesian_product(Css, N) :-
   maplist( length,
      Css, Ns ),
   multiply(Ns, N).


/* setof2(Template,Goal,Set) <-
      Set is the set of all instances of Template such that Goal
      is satisfied.
      Contrary to the standard predicate setof, here the set
      Set can be empty. */

setof2(Template,Goal,Set) :-
   ( setof( Template,
        Goal, Set )
   ; Set = [] ).


/* ord_power_set(Set,Power_Set) <-
      */

ord_power_set([],[[]]) :-
   !.
ord_power_set([X],[[],[X]]) :-
   !.
ord_power_set(Set,Power_Set) :-
   balanced_partition(Set,Set_1,Set_2),
   ord_power_set(Set_1,Power_Set_1),
   ord_power_set(Set_2,Power_Set_2),
   state_disjunction_basic(Power_Set_1,Power_Set_2,Power_List),
   list_to_ord_set(Power_List,Power_Set).


/* balanced_partition(Set,Set_1,Set_2) <-
      */

balanced_partition(Set,Set_1,Set_2) :-
   length(Set,L),
   N is ( L // 2 ) + 1,
   nth(N,Set,X),
   append(Set_1,[X|T],Set),
   Set_2 = [X|T].


/* equidistant_partition([X,Y],Width,Partition) <-
      Partition is the list of all numbers X + I*Width,
      between X and Y. */

equidistant_partition([X,Y],Width,[]) :-
   X > Y + Width / 10,
   !.
equidistant_partition([X,Y],Width,[X|Partition]) :-
   Z is X + Width,
   equidistant_partition([Z,Y],Width,Partition).


/* generation of the proper name for the backslash <-
      If "feature(character_escapes,true)" succeeds,
      then "assert(backslash('\\'))" will assert a backslash,
      if "feature(character_escapes,false)" succeeds,
      then "assert(backslash('\'))" will assert a backslash. */

:- name(BS, [92]),
   retractall(backslash(_)),
   assert(backslash(BS)).

% backslash_for_write('\\').
%
% :- backslash_for_write(Name),
%    treat_backslashes_in_name(Name,BS),
%    retractall(backslash(_)),
%    assert(backslash(BS)).

treat_backslashes_in_name(Name_1,Name_2) :-
   name(Name_1,String_1),
   treat_backslashes_in_string(String_1,String_2),
   name(Name_2,String_2).

treat_backslashes_in_string([92,92|List_1],[92|List_2]) :-
   !,
   treat_backslashes_in_string(List_1,List_2).
treat_backslashes_in_string([X|List_1],[X|List_2]) :-
   treat_backslashes_in_string(List_1,List_2).
treat_backslashes_in_string([],[]).


/******************************************************************/


