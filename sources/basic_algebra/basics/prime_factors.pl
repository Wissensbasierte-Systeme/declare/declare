

/******************************************************************/
/***                                                            ***/
/***         Declare:  Prime Factors                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* prime_factors(N, Factors, Multiset) <-
      */

prime_factors(N, Factors, Multiset) :-
   findall( Factor,
      prime_factor(N, Factor),
      Factors ),
   list_to_multiset(Factors, Multiset).

prime_factor(N, Factor) :-
   prime_factor(N, 2, Factor).

prime_factor(N, Factor, Factor) :-
   0 is N mod Factor.
prime_factor(N, F, Factor) :-
   F < N,
   ( 0 is N mod F ->
     ( M is N / F,
       prime_factor(M, F, Factor) )
   ; ( G is F + 1,
       prime_factor(N, G, Factor) ) ).


/******************************************************************/


