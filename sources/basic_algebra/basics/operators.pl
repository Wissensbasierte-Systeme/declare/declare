

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Operators                               ***/
/***                                                            ***/
/******************************************************************/


:- op(5, xfx, like),
   op(800, yfx, and),
   op(800, yfx, or),
   op(900, fy, ~).


/* current_op_to_xpce_table <-
      */

current_op_to_xpce_table :-
   findall( [X, Y, Z],
      current_op(X,Y,Z),
      Rows_1 ),
   length(Rows_1, N),
   current_op_rows_sort(Rows_1, Rows_2),
   xpce_display_table(['Prec', 'Spec', 'Name'], Rows_2),
   write_list(['DisLog currently uses ', N, ' operators.']),
   nl.

current_op_rows_sort(Rows_1, Rows_2) :-
   maplist( current_op_row_transform,
      Rows_1, Rows_A ),
   sort(Rows_A, Rows_B),
   maplist( current_op_row_transform,
      Rows_B, Rows_2 ).

current_op_row_transform([Y, X, Z], [X, Y, Z]).


/******************************************************************/


