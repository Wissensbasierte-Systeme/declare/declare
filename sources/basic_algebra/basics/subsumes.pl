

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Subsumption                             ***/
/***                                                            ***/
/******************************************************************/


% :- module_transparent
%       clause_subsumes/3,
%       clause_subsumes_chk/3.


/*** interface ****************************************************/


/* clause_subsumes_chk(Subsumption, As, Bs) <-
      */

clause_subsumes_chk(Subsumption, As, Bs) :-
   \+ \+ clause_subsumes(Subsumption, As, Bs).

clause_subsumes(Subsumption, As, Bs) :-
   copy_term(Bs, Cs),
   free_variables(Bs, Vs_Bs),
   free_variables(Cs, Vs_Cs),
   clause_subsumes_(Subsumption, As, Cs),
   numbervars(Vs_Bs, '$VAR', 0, _),
   Vs_Bs = Vs_Cs.

clause_subsumes_(Subsumption, [A|As], Bs) :-
   member(B, Bs),
   apply(Subsumption, [A, B]),
   clause_subsumes_(Subsumption, As, Bs).
clause_subsumes_(_, [], _).


/* clause_variant_chk(As, Bs) <-
      */

clause_variant_chk(As, Bs) :-
   clause_subsumes_chk(As, Bs),
   clause_subsumes_chk(Bs, As).

   
/* clause_subsumes_chk(As, Bs) <-
      */

clause_subsumes_chk(As, Bs) :-
   copy_term(As, As2),
   copy_term(Bs, Bs2),
   members(As2, Bs2),
   term_variant_chk(Bs, Bs2).

clause_subsumes(As, Bs) :-
   copy_term(Bs, Bs1),
   copy_term(Bs, Bs2),
   members(As, Bs2),
   term_variant_chk(Bs1, Bs2).


/*
clause_subsumes(As, Bs) :-
   copy_term(Bs, Bs_),
   copy_term(As, As_),
   free_variables(Bs, Vs_Bs),
   free_variables(Bs_, Vs_Bs_),
   subset_non_ground(As_, Bs_),
   numbervars(Vs_Bs, '$VAR', 0, _),
   Vs_Bs = Vs_Bs_.
*/


/* clause_variant(A, B) <-
      */

clause_variant(A, B) :-
   term_variant_chk(A, B).

term_variant_chk(A, B) :-
   copy_term(A, A_),
   copy_term(B, B_),
   numbervars(A_, 0, N),
   numbervars(B_, 0, N),
   A_ == B_.


/*

/* subsumes_chk(A, B) <-
      */

subsumes_chk(A, B) :-
   \+ \+ (
      copy_term(B, C),
      free_variables(B, Bs),
      free_variables(C, Cs),
      A=C,
      numbervars(Bs, '$VAR', 0, _),
      Bs=Cs ).

subsumes_chk(T1,T2) :-
   \+ ( varnames(T2),
        \+ (T1 = T2) ).

*/


/*** tests ********************************************************/


test(subsumes, clause_subsumes) :-
   clause_subsumes([p(X,Y)], [p(a,X), q(X), r(X,Y)]),
   writeln(
      'clause_subsumes([p(X,Y)], [p(a,X), q(X), r(X,Y)])'),
   write('X'=X).
test(subsumes, clause_subsumes_chk) :-
   clause_subsumes_chk([p(X,Y)], [p(a,X), q(X), r(X,Y)]).
test(subsumes, clause_variant_chk) :-
   clause_variant_chk([p(X,Y), q(X,Y)], [q(Y,X), p(Y,X)]).
test(subsumes, not_clause_variant_chk) :-
   \+ clause_variant_chk([p(X,Y), q(X,Y)], [q(X,Y), p(Y,X)]).


/******************************************************************/


