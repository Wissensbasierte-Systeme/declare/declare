

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Date                                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* max_date(Dates, Date) <-
      */

max_date(Date_1, Date_2, Date_3) :-
   max_date([Date_1, Date_2], Date_3).

max_date(Dates, Date) :-
   sort(Dates, Dates_2),
   last(Dates_2, Date).


/*
date(date(Year, Month, Day)) :-
   get_time(Time),
   convert_time(Time, Year, Month, Day, _, _, _, _).
*/

date_is_current(Date) :-
   name(Date, Xs),
   member(D, ["2006", "2007"]),
   append(_, D, Xs).


/* date_swi_to_sql(date(Year, Month, Day), Date) <-
      */

date_swi_to_sql(Date_Swi, Date_Sql) :-
   nonvar(Date_Swi),
   Date_Swi = date(Year, Month, Day),
   ( Month < 10 ->
     M = '-0'
   ; M = '-' ),
   ( Day < 10 ->
     D = '-0'
   ; D = '-' ),
   concat([Year, M, Month, D, Day], Date_Sql).


/******************************************************************/


