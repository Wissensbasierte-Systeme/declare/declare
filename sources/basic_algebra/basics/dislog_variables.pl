

/******************************************************************/
/***                                                            ***/
/***        Dislog :  Global Variables                          ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      dislog_flag/2,
      dislog_variable/2,
      dsa_variable/2,
      dislog_counter/2.

:- multifile
      dislog_flag/2,
      dislog_variable/2,
      dsa_variable/2.

:- discontiguous
      dislog_counter/2, dislog_counter/4.


/*** interface ****************************************************/


/* dislog_variables <-
     */

dislog_variables :-
   html_display_prolog_facts(dislog_variable/2),
   fail.
dislog_variables :-
   nl,
   dislog_variable(Variable,Value),
   write(Variable), write(' = '), writeln(Value),
   fail.
dislog_variables.


/* dislog_variables_get <-
      */

dislog_variables_get([X-Y|Ps]) :-
   dislog_variable_get(X,Y),
   dislog_variables_get(Ps).
dislog_variables_get([X:Y|Ps]) :-
   dislog_variable_get(X,Y),
   dislog_variables_get(Ps).
dislog_variables_get([]).
 

dislog_variable_get(Variable, E1, E2, Value) :-
   dislog_variable_get(Variable, E1, V),
   concat(V, E2, Value).

dislog_variable_get(Variable, Extension, Value) :-
   dislog_variable_get(Variable, V),
   concat(V, Extension, Value).

dislog_variable_get(Variable, Value) :-
   dislog_variable(Variable, Value).


/* dislog_variables_set <-
     */

dislog_variables_set([X-Y|Ps]) :-
   dislog_variable_set(X,Y),
   dislog_variables_set(Ps).
dislog_variables_set([X:Y|Ps]) :-
   dislog_variable_set(X,Y),
   dislog_variables_set(Ps).
dislog_variables_set([]).
 
dislog_variable_set(Variable,Value) :-
   retractall(dislog_variable(Variable,_)),
   assert(dislog_variable(Variable,Value)).
 
 
/* dislog_variable_switch(Variable,Old_Value,New_Value) <-
      */

dislog_variable_switch(Variable,Old_Value,New_Value) :-
   dislog_variable_get(Variable,Old_Value),
   dislog_variable_set(Variable,New_Value).


/* dislog_variable_increment(Variable) <-
      */

dislog_variable_increment(Variable) :-
   dislog_variable_get(Variable,Old_Value),
   New_Value is Old_Value + 1,
   dislog_variable_set(Variable,New_Value).


/* dislog_variable_add_element(Variable,Element) <-
      */

dislog_variable_add_element(Variable,Element) :-
   dislog_variable_get(Variable,Elements),
   dislog_variable_set(Variable,[Element|Elements]).


/* dislog_flags <-
      Several flags are used for controlling the execution of 
      DisLog predicates, the value Value of a flag Flag is 
      asserted as current_num(Flag,Value), 
      dislog_flags prints a list of all current DisLog flags. */

dislog_flags :-
   nl,
   current_num(Flag, Value),
   write(Flag), write(' = '), writeln(Value),
   fail.
dislog_flags.
 

/* dislog_flag_set(Flag, Value) <-
      Sets the current value of the DisLog flag Flag to Value. */

dislog_flag_set(Flag, Value) :-
   set_num(Flag, Value).

dislog_flags_set([X-Y|Ps]) :-
   dislog_flag_set(X, Y),
   dislog_flags_set(Ps).
dislog_flags_set([X:Y|Ps]) :-
   dislog_flag_set(X, Y),
   dislog_flags_set(Ps).
dislog_flags_set([]).
 

/* dislog_flag_get(Flag, Current_Value) <-
      Looks up the current value Current_Value
      of the DisLog flag Flag. */

dislog_flag_get(Flag, Current_Value) :-
   current_num(Flag, Current_Value).

dislog_flags_get([X-Y|Ps]) :-
   dislog_flag_get(X, Y),
   dislog_flags_get(Ps).
dislog_flags_get([X:Y|Ps]) :-
   dislog_flag_get(X, Y),
   dislog_flags_get(Ps).
dislog_flags_get([]).
 

/* dislog_flag_switch(Flag, Old_Value, New_Value) <-
      Looks up the current value Old_Value of the DisLog flag 
      Flag and updates the current value to New_Value. */

dislog_flag_switch(Flag, Old_Value, New_Value) :-
   dislog_flag_get(Flag, Old_Value),
   dislog_flag_set(Flag, New_Value).


/* prolog_flag_switch(Flag, Old_Value, New_Value) <-
      */

prolog_flag_switch(Flag, Old_Value, New_Value) :-
   prolog_flag(Flag, Old_Value),
   set_prolog_flag(Flag, New_Value).


/* dislog_counter(Operation, Counter) <-
      */

dislog_counter(increase_max, Counter, N) :-
   dislog_variable_get(max(Counter), Max),
   N is Max + 1,
   dislog_variable_set(max(Counter), N).

dislog_counter(forward, Counter, N) :-
   dislog_variable_get(max(Counter), Max),
   dislog_variable_get(act(Counter), M),
   M < Max,
   N is M + 1,
   dislog_variable_set(act(Counter), N).

dislog_counter(backward, Counter, N) :-
   dislog_variable_get(act(Counter), M),
   M > 1,
   N is M - 1,
   dislog_variable_set(act(Counter), N).

dislog_counter(increase, Counter, N) :-
   dislog_variable_get(act(Counter), M),
   N is M + 1,
   dislog_variable_set(act(Counter), N),
   dislog_variable_get(max(Counter), Max),
   dislog_counter(adapt_max, Counter, N, Max).

dislog_counter(set, Counter, N) :-
   dislog_variable_set(act(Counter), N),
   dislog_variable_set(max(Counter), N).

dislog_counter(get, Counter, N) :-
   dislog_variable_get(max(Counter), N).

dislog_counter(adapt_max, Counter, N, Max) :-
   N > Max,
   !,
   dislog_variable_set(max(Counter), N).
dislog_counter(adapt_max, _, _, _).

dislog_counter(init, Counter) :-
   dislog_variable_set(act(Counter), 0),
   dislog_variable_set(max(Counter), 0).


/* dislog_counter_increase(Counter) <-
      */

dislog_counter_increase(Counter) :-
   ( retract(dislog_counter(Counter, N))
   ; N = 0 ),
   M is N + 1,
   assert(dislog_counter(Counter, M)),
   !.


/******************************************************************/


