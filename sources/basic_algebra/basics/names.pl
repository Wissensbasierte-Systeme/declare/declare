

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Name Handling                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* name_remove_elements(Elements, Name_1, Name_2) <-
      */

name_remove_elements(Elements, Name_1, Name_2) :-
   atom_codes(Name_1, String_1),
   list_remove_elements(Elements, String_1, String_2),
   atom_codes(Name_2, String_2).


/* name_remove_elements(Where, Elements, Name_1, Name_2) <-
      */

name_remove_elements(Where, Elements, Name_1, Name_2) :-
   atom_codes(Name_1, String_1),
   list_remove_elements(Where, Elements, String_1, String_2),
   atom_codes(Name_2, String_2).


/* name_exchange_elements(Substitution, Name_1, Name_2) <-
      */

name_exchange_elements(Substitution, Name_1, Name_2) :-
   atom_codes(Name_1, List_1),
   list_exchange_elements(Substitution, List_1, List_2),
   atom_codes(Name_2, List_2).


/* name_exchange_sublist(Substitution, Name_1, Name_2) <-
      */

name_exchange_sublist(Substitution, Name_1, Name_2) :-
   atom_codes_(Name_1, List_1),
   list_exchange_sublist(Substitution, List_1, List_2),
   atom_codes(Name_2, List_2).

atom_codes_([], [91, 93]) :-
   !.
atom_codes_(X, Y) :-
   atom_codes(X, Y).


/* name_split_at_position(Cut_Lists, Name, Names) <-
      Given a list of cut lists, which are atoms in ASCII
      code representation.
      Splits an atom Name at all occurrences of a cut list
      from Cut_Lists, yielding a list Names of atoms. */

name_split_at_position(Cut_Lists, Name, Names) :-
   atom_codes(Name, List),
   list_split_at_position(Cut_Lists, List, Lists),
   maplist( atom_codes,
      Names, Lists ).


/* name_start_after_position(Cut_Lists, Name_1, Name_2) <-
      */

name_start_after_position(Cut_Lists, Name_1, Name_2) :-
   atom_codes(Name_1, List_1),
   list_start_after_position(Cut_Lists, List_1, List_2),
   atom_codes(Name_2, List_2).


/* name_cut_at_position(Cut_Lists, Name_1, Name_2) <-
      */

name_cut_at_position(Cut_Lists, Name_1, Name_2) :-
   atom_codes(Name_1, List_1),
   list_cut_at_position(Cut_Lists, List_1, List_2),
   atom_codes(Name_2, List_2).


/* lists_to_names(Lists, Names) <-
      */

lists_to_names(Lists, Names) :-
   maplist( name,
      Names, Lists ).


/* concat_with_separator(Names, Separator, Name) <-
      */

concat_with_separator(Names, Separator, Name) :-
   names_append_with_separator(Names, Separator, Name).


/* concat_with_separator(Name_1, Separator, Name_2, Name) <-
      */

concat_with_separator(Name_1, Separator, Name_2, Name) :-
   names_append_with_separator(Name_1, Separator, Name_2, Name).


/* names_append_with_separator(Names, Separator, Name) <-
      */

names_append_with_separator([N1, N2|Ns], Separator, Name) :-
   names_append_with_separator([N2|Ns], Separator, Name_2),
   name_append_with_separator(N1, Separator, Name_2, Name).
names_append_with_separator([Name], _, Name).
names_append_with_separator([], _, '').
   

/* name_append_with_comma(Name_1, Name_2, Name_3) <-
      */

name_append_with_comma(Name_1, Name_2, Name_3) :-
   name_append_with_separator(Name_1, ', ', Name_2, Name_3).


/* name_append_with_separator(Name_1, Separator, Name_2, Name_3) <-
      */

% name_append_with_separator('', _, Name_2, Name_2) :-
%    !.
% name_append_with_separator(Name_1, _, '', Name_1) :-
%    !.
name_append_with_separator(Name_1, Separator, Name_2, Name_3) :-
   name_append([Name_1, Separator, Name_2], Name_3).


/* name_append(Name_A, Name_B, Name_C) <-
      */

name_append(Name_A, Name_B, Name_C) :-
   var(Name_B),
   !,
   atom_codes(Name_A, List_A),
   atom_codes(Name_C, List_C),
   append(List_A, List_B, List_C),
   atom_codes(Name_B, List_B).
name_append(Name_A, Name_B, Name_C) :-
   var(Name_C),
   !,
   atom_codes(Name_A, List_A),
   atom_codes(Name_B, List_B),
   append(List_A, List_B, List_C),
   atom_codes(Name_C, List_C).
name_append(Name_A, Name_B, Name_C) :-
   var(Name_A),
   !,
   atom_codes(Name_C, List_C),
   atom_codes(Name_B, List_B),
   append(List_A, List_B, List_C),
   atom_codes(Name_A, List_A).
name_append(Name_A, Name_B, Name_C) :-
   atom_codes(Name_C, List_C),
   atom_codes(Name_B, List_B),
   atom_codes(Name_A, List_A),
   append(List_A, List_B, List_C).


/* name_append(Names, Name) <-
      */

name_append([], '').
name_append([N|Ns], Name) :-
   name_append(Ns, Name_2),
   name_append(N, Name_2, Name).


/* list_name_append <-
      */

list_name_append([Name_A], Name_A) :-
   !.
list_name_append([Name_A|Names_A], Name_B) :-
   name_append(Name_A, ' ', Name_A_Space),
   list_name_append(Names_A, Name),
   name_append(Name_A_Space, Name, Name_B).


/* concat_names(Names, Name) <-
      Concatenates the list Names of names to a single name
      Name. */

concat_names(Names, Name) :-
   concat_names(Names, '', Name).

concat_names([Name|Names], Old_Name, New_Name) :-
   atom_codes(Name, L1),
   concat_names(Names, Old_Name, N2),
   atom_codes(N2, L2),
   append(L1, L2, L12),
   atom_codes(New_Name, L12).
concat_names([], Name, Name).


/* concat_atoms(Atoms, Atom) <-
      */

concat_atoms(Atoms, Atom) :-
   concat_atom(Atoms, Atom).


/* list_to_name(List, Name) <-
     */

list_to_name(List, Name) :-
   list_to_name_list(List, N_List),
   atom_codes(Name, N_List).

list_to_name_list([],[]).
list_to_name_list([A|B], Name_List) :-
   atomic(A),
   atom_codes(A, Name),
   append(Name, [32], N),
   list_to_name_list(B, N_L),
   append(N, N_L, Name_List).
list_to_name_list([A|B], Name_List) :-
   is_list(A),
   list_to_name_list(A, N_L1),
   list_to_name_list(B, N_L2),
   append(N_L1, N_L2, Name_List).


/* name_to_quoted_name(Name_1, Name_2) <-
      */

name_to_quoted_name(Name_1, Name_2) :-
   atom_codes(Name_1, List_1),
   append(["'", List_1,"'"], List_2),
   atom_codes(Name_2, List_2).


/* name_contains_name_formula(Name, Formula) <-
      */

name_contains_name_formula(Name, (F,G)) :-
   !,
   name_contains_name_formula(Name, F),
   name_contains_name_formula(Name, G).
name_contains_name_formula(Name, (F;G)) :-
   !,
   ( name_contains_name_formula(Name, F) 
   ; name_contains_name_formula(Name, G) ).
name_contains_name_formula(Name, F) :-
   name_contains_name(Name, F).


/* name_contains_name(Name_1, Name_2) <-
      */

name_contains_name(Name_1, Name_2) :-
   atomic(Name_1),
   atom_codes(Name_1, List_1),
   atom_codes(Name_2, List_2),
   ( append(_, List_2, List_1)
   ; ( list_start_after_position([List_2], List_1, List_3),
       !,
       List_3 \= [] ) ).


/* string_to_names(Xs, Ns) <-
      */

string_to_names([X|Xs], [N|Ns]) :-
   atom_codes(N, [X]),
   string_to_names(Xs, Ns).
string_to_names([], []).


/* first_n_characters(N, Name_1, Name_2) <-
      */

first_n_characters(N, Name_1, Name_2) :-
   atom_codes(Name_1, List_1),
   first_n_elements(N, List_1, List_2),
   atom_codes(Name_2, List_2).


/* atom_number_compare(Pred, A1, A2) <-
      */

atom_number_compare(Pred, A1, A2) :-
   atom_number(A1, N1),
   atom_number(A2, N2),
   call(Pred, N1, N2).


/* char_type_of_atom(Atom, Type) <-
      */

char_type_of_atom(Atom, Type) :-
   atom_chars(Atom, [C|_]),
   char_type(C, Type).


/* char_between(X, Y, A) <-
      */

char_between(X, Y, A) :-
   atom_codes(X, [I]),
   atom_codes(Y, [J]),
   atom_codes(A, [K]),
   between(I, J, K).


/* characters_capital_to_lower(Name_1, Name_2) <-
      */

characters_capital_to_lower(Name_1, Name_2) :-
   atom_chars(Name_1, Cs_1),
   maplist( character_capital_to_lower,
      Cs_1, Cs_2 ),
   atom_chars(Name_2, Cs_2).


/* character_capital_to_lower(C1, C2) <-
      */

character_capital_to_lower(C1, C2) :-
   name(C1, [N1|Ns]),
   ( 65 =< N1, N1 =< 90
   ; member(N1, [196, 214, 220]) ),    % ÄÖÜ
   N2 is N1 + 32,
   name(C2, [N2|Ns]).


/* characters_lower_to_capital(Name_1, Name_2) <-
      */

characters_lower_to_capital(Name_1, Name_2) :-
   atom_chars(Name_1, Cs_1),
   maplist( character_lower_to_capital,
      Cs_1, Cs_2 ),
   atom_chars(Name_2, Cs_2).


/* character_lower_to_capital(C1, C2) <-
      */

character_lower_to_capital(C1, C2) :-
   name(C1, [N1|Ns]),
   N2 is N1 - 32,
   ( 65 =< N2, N2 =< 90
   ; member(N1, [228, 246, 252]) ),    % äöü
   name(C2, [N2|Ns]).


/* character_change_between_lower_and_capital(C1, C2) <-
      */

character_change_between_lower_and_capital(C1, C2) :-
   ( character_capital_to_lower(C1, C2)
   ; character_lower_to_capital(C1, C2) ),
   !.


/******************************************************************/


