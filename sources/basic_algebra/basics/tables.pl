

/******************************************************************/
/***                                                            ***/
/***           Declare:  Tables                                 ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(tables, tables_join) :-
   T1 = [[a,1], [a,2], [b,2]],
   T2 = [[1,10], [2,20], [2,30]],
   Source = [1:1=X, 1:2=Y, 2:1=U, 2:2=V, 1=A, 2=B],
   tables_join([T1, T2], Source, [X,V]=A+B, (Y=U), T),
   writeln_list(user, T).


/*** interface ****************************************************/


/* tables_join(Tables, Source, Target, Condition, Table) <-
      */

tables_join(Tables, Source, Target, Condition, Table) :-
   findall( Target,
      ( maplist( member,
           Tuples, Tables ),
        tables_join_tuples(Tuples, Source, Condition) ),
      Table ).

tables_join_tuples(Tuples, Source, Condition) :-
   ( foreach(P=X, Source) do
        ( ( P = T:N ->
            nth(T, Tuples, Tuple), nth(N, Tuple, X)
          ; P = T, nth(T, Tuples, X) ) ) ),
   call(Condition).


/******************************************************************/


