

/******************************************************************/
/***                                                            ***/
/***           DisLog:  I/O and File Handling                   ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      dwrite/2.


/*** interface ****************************************************/


/* dread(Type, File, X) <-
      */

dread(lp, File, DisLog_Program) :-
   style_check(-singleton),
   dislog_consult(File, DisLog_Program),
   style_check(+singleton).

dread(pl, File, Prolog_Program) :-
   prolog_consult(File, Prolog_Program).

dread(txt, File, Name) :-
   read_file_to_name(File, Name).

dread(xls, File, Table) :-
   read_star_office_file(File, Table).


/* dwrite(Type, File, X) <-
      */

dwrite(lp, Program) :-
   dwrite(lp, user, Program).

dwrite(lp, File, DisLog_Program) :-
   dportray(File, lp_xml, DisLog_Program).

dwrite(pl, File, Prolog_Program) :-
   dportray(File, prolog, Prolog_Program).

dwrite(txt, File, Name) :-
   predicate_to_file( File,
      write(Name) ).

dwrite(xls, File, Table) :-
   dwrite(csv, File, Table).

dwrite(csv, File, Table) :-
   write_star_office_file(Table, File).

dwrite(csv(Separator), File, Table) :-
   write_star_office_file(Separator, Table, File).


/* read_file_to_lines(File, Lines) <-
      */

read_file_to_lines(File, Lines) :-
   read_file_to_name(File, Name),
   name_split_at_position(["\n"], Name, Lines).


/* read_file_to_name(File, Name) <-
      */

read_file_to_name(File, Name) :-
   read_file_to_codes(File, String),
   name(Name, String).


/* read_file_to_codes(File, Codes) <-
      */

read_file_to_codes(File, Codes) :-
   read_file_to_codes(File, Codes, []).

read_file_to_string(File, Codes) :-
   read_file_to_codes(File, Codes).


/* write_list(File, Xs) <-
      */

write_list(File, Xs) :-
   predicate_to_file( File,
      ( foreach(X, Xs) do write(X) ) ).

write_list(Xs) :-
   foreach(X, Xs) do write(X).


/* writeln_list(File, Xs) <-
     */

writeln_list(File, Xs) :-
   predicate_to_file( File,
      ( foreach(X, Xs) do writeln(X) ) ).

writeln_list(Xs) :-
   foreach(X, Xs) do writeln(X).


/* writelnq_list(File, Xs) <-
      */

writelnq_list(File, Xs) :-
   predicate_to_file( File,
      writelnq_list(Xs) ).

writelnq_list(Xs) :-
   foreach(X, Xs) do
      writeq(X), nl.


/* writeq_list_with_comma(Xs) <-
      */

writeq_list_with_comma(Xs) :-
   append(Ys, [X], Xs),
   ( foreach(Y, Ys) do
        writeq(Y), write(', ') ),
   writeq(X).


/* writelnq(File, X) <-
      */

writelnq(File, X) :-
   predicate_to_file( File,
      writelnq(X) ).

writelnq(X) :-
   writeq(X), nl.


/* writeln(File, X) <-
      */

writeln(File, X) :-
   telling(F),
   tell(File),
   writeln(X),
   tell(F).


/* write_list__(File, Xs) <-
      */

write_list__(Xs) :-
   write_list__(user, Xs).

write_list__(File, Xs) :-
   foreach(X, Xs) do write__(File, X).

write__(File, X) :-
   File <== write___(X).

write___(X, File) :-
   write(File, X).


/* switch(File1, File2) <-
      Remembers the current standard output as File1
      and switches the standard output to File2. */

switch(File1, File2) :-
   telling(File1),
   tell(File2).


/* switch(File) <-
      switches the standard output to File. */

switch(File) :-
   telling(F),
   F = File,
   !.
switch(File) :-
   told,
   tell(File).


/* predicate_to_file(File, Goal) <-
      */

predicate_to_file(File, Goal) :-
   switch(Previous, File),
   call(Goal),
   switch(Previous),
   !.


/* predicate_from_file(File, Goal) <-
      */

predicate_from_file(File, Goal) :-
   seeing(Previous),
   see(File),
   call(Goal),
   seen,
   see(Previous),
   !.


/* write_list_with_separators(
         Write_Middle,Write_Final,Xs) <-
      */

write_list_with_separators(
      Write_Middle,Write_Final,[X1,X2|Xs]) :-
   !,
   apply(Write_Middle,[X1]),
   write_list_with_separators(
      Write_Middle,Write_Final,[X2|Xs]).
write_list_with_separators(_,Write_Final,[X]) :-
   !,
   apply(Write_Final,[X]).
write_list_with_separators(_,_,[]).


/* readword(C,W,C1) <-
      Read in a single word, given an initial character,
      and remembering what character came after the word,
      cf. Clocksin-Mellish, page 104. */

readword(C,W,C1) :-
   single_character(C),
   !,
   name(W,[C]),
   get0(C1).
readword(C,W,C2) :-
   in_word(C,NewC),
   !,
   get0(C1),
   restword(C1,Cs,C2),
   name(W,[NewC|Cs]).
readword(_,W,C2) :-
   get0(C1),
   readword(C1,W,C2).

restword(C,[NewC|Cs],C2) :-
   in_word(C,NewC),
   !,
   get0(C1),
   restword(C1,Cs,C2).
restword(C,[],C).


/* These characters form words on their own */

single_character(C) :-
   member(C, ",;:?!.").


/* in_word(C1, C2) <-
      These characters can appear within a word.
      The second clause converts charcters to lower case. */

in_word(C, C) :-
   C > 96,                    /* a b...z */
   C < 123.
in_word(C, L) :-
   C > 64,                    /* A B...Z */
   C < 91,
   L is C + 32.
in_word(C, C) :-
   C > 47,                    /* 1 2...9 */
   C < 58.

in_word(C) :-
   member(C, "'-").


/******************************************************************/


