

/******************************************************************/
/***                                                            ***/
/***            Declare:  Interface                             ***/
/***                                                            ***/
/******************************************************************/


:- dynamic 
      special_predicates/1.


/*** dislog variables *********************************************/


/* dislog_variable(Variable, Path) <-
      */

dislog_variable(home, Path) :-
   home_directory(Home),
   concat(Home, '/Declare', Path).

dislog_variable(source_path, Path) :-
   dislog_variable(home, '/sources/', Path).
dislog_variable(example_path, Path) :-
   dislog_variable(home, '/examples/', Path).
dislog_variable(library_path, Path) :-
   dislog_variable(home, '/library/', Path).
dislog_variable(output_path, Path) :- 
   dislog_variable(home, '/results/', Path).
dislog_variable(data_path, Path) :- 
   dislog_variable(home, '/results/data/', Path).
dislog_variable(control_data_file, File) :- 
   dislog_variable(home, '/results/control_data', File).
dislog_variable(image_directory, Path) :- 
%  dislog_variable(home, '/images/', Path).
   dislog_variable(home, '/research/projects/D3/d3_images/', Path).


/* dislog_variable(X, Y, Z) <-
      */

dislog_variable(X, Y, Z) :-
   dislog_variable_get(X, V),
   name_append(V, Y, Z).


/*** interface ****************************************************/


/* dislog_information <-
      */

i :-
   dislog_information.

dislog_information :-
   dotted_line,
   writeln_list([
      '    ',
      '     DDK - The Declare Developers Kit  ']),
   dotted_line,
   write_list([
      '     \n',
      '     The DDK consists of 2 layers.',
      '     \n\n',
      '     The kernel layer consists of the units', '\n',
      '       - basic_algebra', '\n',
      '       - nm_reasoning', '\n',
      '       - xml', '\n',
      '     The top layer consists of the units', '\n',
      '       - databases', '\n',
      '       - development', '\n',
      '       - projects', '\n',
      '       - stock_tool', '\n\n',
      '     For starting GUIs of applications please call', '\n',
      '        ?- dislog_desktop_html.', '\n' ]),
   dotted_line.


/* special_predicates(List) <-
      Indicates those predicates which are called (and not 
      resolved like all the others) when evaluating a DNLP 
      with Declare. */

special_predicates([]).


/* announcement(Title) <-
      Pretty prints Title depending on the current value of the
      Declare flag trace_level. */

announcement(Title) :-
   current_num(trace_level,a1),
   !,
   write_list([Title,' -- ']),
   dislog_variable(control_data_file,File1),
   switch(File,File1),
   star_line,
   writeln(Title),
   star_line,
   tell(File).
announcement(Title) :-
   star_line,
   writeln(Title),
   star_line.


/* announcement(I,Title) <-
      Pretty prints Title depending on I and the current value of 
      the Declare flag trace_level. */

announcement(10,Title) :-
   current_num(trace_level,a1),
   !,
   writeln(' '),
   write_list([Title,' -- ']),
   dislog_variable(control_data_file,File1),
   switch(File,File1),
   star_line,
   writeln(Title),
   star_line,
   tell(File).
announcement(10,Title) :-
   star_line,
   writeln(Title),
   star_line.

announcement(11,Title) :-
   current_num(trace_level,a1),
   !,
   writeln(' '),
   write_list([Title,' -- ']).
announcement(11,_).

announcement(12,_).
 

/* star_line <-
      The character '*' is printed 68 times with '/' and a new 
      line before and behind. */

star_line :-
   nl, write('/*********************************'),
   writeln('*********************************/'), nl.

star_line_2 :-
   write('*********************************'),
   writeln('*********************************').


/* bar_line <-
      The character '-' is printed 74 times. */

bar_line_std_out :-
   telling(File),
   bar_line(File).

bar_line :-
   bar_line(user).
bar_line_2 :-
   bar_line_2(user).

bar_line(File) :-
   write(File,'  -------------------------------------'),
   writeln(File,'------------------------------------- ').
bar_line_2(File) :-
   nl, write(File,'-------------------------------------'),
   writeln(File,'------------------------------------- ').


/* dotted_line <-
      The character '.' is printed 74 times. */

dotted_line :-
   write(user,'  .....................................'),
   writeln(user,'..................................... ').


/* list_database(File) <-
      Consults a DNLP from the file 'examples/File' 
      and pretty prints the DNLP. */

lp(File) :-
   list_database(File).

list_database(File) :-
   dconsult(File),
   lp.


/* edit_database(File) <-
      Calls the editor 'vi' on the file File in the Declare
      examples directory. */

edit_database(File) :-
   dis_ex_extend_filename(File,Pathname),
   us(vi,Pathname).


/* us(Command_Name,Argument) <-
      Calls the unix system to execute the command Command_Name 
      with the argument Argument. */

us(Command_Name,Argument) :-
   name(Command_Name,L1),
   name(Argument,L2),
   append(L1,[32|L2],L3),
   name(Full_Command,L3),
   unix(system(Full_Command)).

us(Command) :-
   unix(system(Command)).


/* us_declare(Command_List) <-
      */

us_declare(Command_List) :-
   concat(Command_List, Command),
   us(Command).


/* unix_command(Command_Words) <-
      Executes a unix command given in a list of words/pieces of
      the command, the pieces must include necessary spaces. */

unix_command(Command_Words) :-
   concat_names(Command_Words,Command),
   unix(shell(Command)).


user_help :-
   dman.


/* dman(Tool) <-
      display the Declare-Library Manual by the tool Tool. */

dman :-
   us('gv manuals/nm_reasoning/library.ps > /dev/null 2> /dev/null &').


/* handling of the example directory <-
      */

dcdup :-
   dcd('..').

dcd(Directory) :-
   cd_ex(Directory).

cd_ex(Directory) :-
   retract(dislog_variable(example_path,Path)),
   concat_atoms([Path,Directory,'/'],New_Path),
   prune_path_from_redundancy(New_Path,Pruned_New_Path),
   assert(dislog_variable(example_path,Pruned_New_Path)).

dpwd :-
   pwd_ex.
 
pwd_ex :-
   dislog_variable(example_path,Path),
   nl, writeln(Path).


/* lex <-
      Lists the contents of the examples directory examples. */

lex :-
   nl, writeln('Declare-Examples'),
   ls_ex.

ls_ex :-
   dislog_variable(example_path,Path),
   concat_atoms(['ls -F ',Path],Command),
   nl,
   unix(system(Command)). 


/* leda_graph_editor <-
      */

leda_graph_editor :-
   unix(system('tools/implode &')).


/* save_dislog <-
      */

save_dislog :-
   save_dislog_special.


/* start_xdislog <-
      Starts an X-interface to Declare
      based on the SICStus Prolog graphics manager. */

start_xdislog :-
   use_module(library(gmlib)),
   start,
   ['x_dislog/cffp','x_dislog/xdislog'],
   xdislog,
   end.


/* top_level <-
      */

top_level :-
   read(Command),
   ( Command = halt
   ; ( call(Command),
       top_level )
   ; top_level ).

% ?- prompt(_,'Declare% ').
% ?- top_level.


/******************************************************************/


