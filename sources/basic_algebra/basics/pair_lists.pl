

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Lists and Tuples                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* re_pair_list(Mode, Pairs_1, Pairs_2) <-
      */

re_pair_list(Mode, Pairs_1, Pairs_2) :-
   pair_lists(_, Xs, Ys, Pairs_1),
   pair_lists(Mode, Xs, Ys, Pairs_2).


/* pair_lists(Mode, Xs, Ys, Pairs) <-
      Mode is one of '[]', ',', ';', :, -, pair, ... */

pair_lists(Mode, [X|Xs], [Y|Ys], [Pair|Pairs]) :-
   pair_elements(Mode, X, Y, Pair),
   !,
   pair_lists(Mode, Xs, Ys, Pairs).
pair_lists(_, [], [], []).

pair_elements(Mode, X, Y, Pair) :-
   ( Mode = '[]',
     Pair = [X, Y]
   ; Pair =.. [Mode, X, Y] ).


/* pair_lists(Xs,Ys,XYs) <-
      */

pair_lists(Xs, Ys, Zs) :-
   pair_lists('[]', Xs, Ys, Zs).

pair_lists_2(Xs, Ys, Zs) :-
   pair_lists(-, Xs, Ys, Zs).


/* triple_lists(Xs,Ys,Zs,XYZs) <-
      */

triple_lists([X|Xs],[Y|Ys],[Z|Zs],[[X,Y,Z]|XYZs]) :-
   triple_lists(Xs,Ys,Zs,XYZs).
triple_lists([],[],[],[]).

triple_lists_2([X|Xs],[Y|Ys],[Z|Zs],[X-Y-Z|XYZs]) :-
   triple_lists_2(Xs,Ys,Zs,XYZs).
triple_lists_2([],[],[],[]).


/* minus_separated_list_to_list(Xs,Zs) <-
      */

minus_separated_list_to_list(Xs,Zs) :-
   var(Xs),
   !,
   reverse(Ys,Zs),
   minus_separated_list_to_list_inverse(Xs,Ys).
minus_separated_list_to_list(Xs,Zs) :-
   minus_separated_list_to_list_inverse(Xs,Ys),
   reverse(Ys,Zs).
 
minus_separated_list_to_list_inverse(Xs-X,[X|Ys]) :-
   minus_separated_list_to_list_inverse(Xs,Ys).
minus_separated_list_to_list_inverse(X,[X]).


/* list_of_tuples_to_tuple_of_lists(Tuples, Tuple) <-
      */

list_of_tuples_to_tuple_of_lists(Tuples, Tuple) :-
   list_of_tuples_to_list_of_lists(Functor, Tuples, Lists),
   list_to_functor_structure(Functor, Lists, Tuple).


/* list_of_tuples_to_list_of_lists(Functor, Tuples, Lists) <-
      */

list_of_tuples_to_list_of_lists(Functor, [T|Ts], Lists) :-
   functor_structure_to_list(Functor, T, Hs),
   ( Ts = [],
     elements_to_lists(Hs, Lists)
   ; list_of_tuples_to_list_of_lists(Functor, Ts, Rs),
     ( foreach(List, Lists), foreach(H, Hs), foreach(R, Rs) do
          List = [H|R] ) ).


/* tuple_of_lists_to_list_of_tuples(Tuple, Tuples) <-
      */

tuple_of_lists_to_list_of_tuples(Tuple, Tuples) :-
   functor_structure_to_list(Functor, Tuple, Lists),
   list_of_lists_to_list_of_tuples(Functor, Lists, Tuples).


/* list_of_lists_to_list_of_tuples(Functor, Lists, Tuples) <-
      */

list_of_lists_to_list_of_tuples(Functor, Lists, [T|Ts]) :-
   ( foreach(List, Lists), foreach(H, Hs), foreach(R, Rs) do
        List = [H|R] ),
   list_to_functor_structure(Functor, Hs, T),
   list_of_lists_to_list_of_tuples(Functor, Rs, Ts).
list_of_lists_to_list_of_tuples(_, _, []).


/* functor_structure_to_list(Functor, Structure, List) <-
      */

functor_structure_to_list('[]', List, List) :-
   is_list(List),
   !.
functor_structure_to_list(Functor, Structure, List) :-
   Structure =.. [Functor, A, B],
   !,
   ( current_op(_, xfy, Functor),
     functor_structure_to_list(Functor, B, As),
     List = [A|As]
   ; current_op(_, yfx, Functor),
     functor_structure_to_list(Functor, A, Bs),
     append(Bs, [B], List) ),
   !.
functor_structure_to_list(_, X, [X]).


/* list_to_functor_structure(Functor, List, Structure) <-
      */

list_to_functor_structure('[]', List, List) :-
   !.
list_to_functor_structure(and, [], true) :-
   !.
list_to_functor_structure(or, [], false) :-
   !.
list_to_functor_structure(Functor, List, Structure) :-
   Structure =.. [Functor, A, B],
   ( current_op(_, xfy, Functor),
     List = [A|As],
     list_to_functor_structure(Functor, As, B)
   ; current_op(_, yfx, Functor),
     append(Bs, [B], List),
     list_to_functor_structure(Functor, Bs, A) ),
   !.
list_to_functor_structure(_, [X], X).


/* colon_structure_to_list(Structure, List) <-
      */

colon_structure_to_list(S, [X|Ys]) :-
   nonvar(S),
   S = (X:Xs),
   !,
   colon_structure_to_list(Xs, Ys).
colon_structure_to_list(X, [X]).


/* list_to_colon_structure(List, Structure) <-
      */

list_to_colon_structure([X|Xs], X:Ys) :-
   list_to_colon_structure(Xs, Ys).
list_to_colon_structure([X], X).


/* semicolon_structure_to_list(Structure, List) <-
      */

semicolon_structure_to_list(S, [X|Ys]) :-
   nonvar(S),
   S = (X;Xs),
   !,
   semicolon_structure_to_list(Xs, Ys).
semicolon_structure_to_list(X, [X]).


/* list_to_semicolon_structure(List, Structure) <-
      */

list_to_semicolon_structure([X|Xs], (X;Ys)) :-
   list_to_semicolon_structure(Xs, Ys).
list_to_semicolon_structure([X], X).


/* term_structure_to_list(Op, Structure, List) <-
      */

term_structure_to_list(Op, Structure, List) :-
   nonvar(Structure),
   Structure =.. [Op, X, Y],
   !,
   term_structure_to_list(Op, X, Xs),
   term_structure_to_list(Op, Y, Ys),
   append(Xs, Ys, List).
term_structure_to_list(_, X, [X]).


/* comma_structure_to_list(Structure, List) <-
      */

comma_structure_to_list(S, [X|Ys]) :-
   nonvar(S),
   S = (X,Xs),
   !,
   comma_structure_to_list(Xs, Ys).
comma_structure_to_list(X, [X]).


/* list_to_comma_structure(List, Structure) <-
      */

list_to_comma_structure([X|Xs], (X,Ys)) :-
   list_to_comma_structure(Xs, Ys).
list_to_comma_structure([X], X).


/* comma_structure_to_ord_set(Structure, Ordset) <-
      */

comma_structure_to_ord_set(Structure, Ordset) :-
   list_to_comma_structure(List, Structure),
   list_to_ord_set(List, Ordset),
   !.


/* list_to_minus_structure(List, Structure) <-
      */

list_to_minus_structure([X1, X2|Xs], X1-S) :-
   list_to_minus_structure([X2|Xs], S).
list_to_minus_structure([X], X).


/* select from minus structures <-
      */

select_first_of_two_list(Xs, Ys) :-
   maplist( select_first_of_two,
      Xs, Ys ).

select_second_of_two_list(Xs, Ys) :-
   maplist( select_second_of_two,
      Xs, Ys ).

select_first_of_two(X-_, X).

select_second_of_two(_-Y, Y).

select_second_and_third_of_three(_-Y-Z, Y-Z).


/*** tests ********************************************************/


test(pair_lists, functor_structure_to_list) :-
   functor_structure_to_list(Functor, a:b:c, List),
   !,
   Functor = :,
   List = [a, b, c].

test(pair_lists, list_to_functor_structure) :-
   list_to_functor_structure(-, [a, b, c], Structure),
   !,
   Structure = a-b-c.

test(pair_lists, tuple_of_lists_to_list_of_tuples) :-
   Tuple = [a, b, c]:[x, y, z]:[1, 2, 3],
   tuple_of_lists_to_list_of_tuples(Tuple, Tuples),
   !,
   Tuples = [a:x:1, b:y:2, c:z:3].

test(pair_lists, list_of_tuples_to_tuple_of_lists) :-
   Tuples = [a:x:1, b:y:2, c:z:3],
   list_of_tuples_to_tuple_of_lists(Tuples, Tuple),
   !,
   Tuple = [a, b, c]:[x, y, z]:[1, 2, 3].


/******************************************************************/


