

/******************************************************************/
/***                                                            ***/
/***           DDK:  Email Handling                             ***/
/***                                                            ***/
/******************************************************************/


:- set_prolog_flag(encoding, utf8).


/*** interface ****************************************************/


/* format_email_file(File_1, File_2) <-
      */

format_email_file(File) :-
   format_email_file(File, File).

format_email_file(File_1, File_2) :-
   prolog_flag_switch(encoding, Flag, iso_latin_1),
   read_file_to_codes(File_1, String_1),
   Substitution = [
      ["_", " "], ["=22", "\""],
      ["=2E", "."], ["=2C", ","], ["=2c", ","],
      ["=\n", ""], ["=\r\n", ""], ["?=", " "],
      ["\r\n", "\n"], ["=C2=A0 ", "  "],
      ["=E4", "ä"], ["=C3=A4", "ä"],
      ["=F6", "ö"], ["=C3=B6", "ö"], ["=c3=b6", "ö"],
      ["=FC", "ü"], ["=C3=BC", "ü"], ["=c3=bc", "ü"],
      ["=C4", "Ä"], ["=D6", "Ö"],
%     ["=C3-", "Ö"],
      ["=DC", "Ü"], ["=C3=9C", "Ü"],
      ["=DF", "ß"], ["=C3=9F", "ß"], ["=c3=9f", "ß"],
      ["=E9", "é"], ["=A9", "©"],
      ["=84", "``"], ["=91", "``"],
%     ["=85", "..."],
      ["=92", "''"], ["=93", "''"], ["=94", "''"],
      ["=96", "-"], ["=20", " "], ["=3D", "="],
      ["=?UTF-8?Q?", ""], ["=?ISO-8859-15?Q?", ""],
      ["=?iso-8859-1?Q?", ""], ["=?iso-8859-1?q?", ""],
      ["=E2=80=A6", "..."], ["=E2=80''", "-"] ],
%     ["=E2=80=9E", "''"], ["=E2=80=9C", "''"]
   list_exchange_sublist(Substitution, String_1, String_2),
   name(Name, String_2),
   predicate_to_file(File_2, write(Name)),
   set_prolog_flag(encoding, Flag),
   us_declare(['iconv -t iso-8859-15 ', File_2, ' -o ', File_2]).


/* file_change_encoding_iso_latin(File_1, File_2) <-
      */

file_change_encoding_iso_latin(File) :-
   file_change_encoding_iso_latin(File, File).

file_change_encoding_iso_latin(File_1, File_2) :-
   us_declare(['iconv -t iso-8859-15 ', File_1, ' -o ', File_2]).


/* format_and_beautify_email_file(File_1, File_2) <-
      */

format_and_beautify_email_file(File_1, File_2) :-
   prolog_flag_switch(encoding, Flag, iso_latin_1),
   read_header_lines(File_1, Lines),
   dislog_variable_get(output_path, 'email_tmp.txt', File),
   open(File, write, Stream),
   set_output(Stream),
   writeln_list(Lines),
   close(Stream),
   set_prolog_flag(encoding, Flag),
   format_email_file(File, File_2).


/* read_header_lines(File, Lines) <-
      */

read_header_lines(File, Lines) :-
   open(File, read, Stream),
   set_input(Stream),
   read_header_lines_(Lines),
   close(Stream),
   writeln_list(Lines).

read_header_lines_(Lines) :-
   read_line_(L),
   !,
   ( member(X, ['From', 'Date', 'To', 'CC', 'Cc', 'Subject',
        ' <', '  <', '   <', '    <']),
     concat(X, _, L) ->
     beautify_header_line(L, M),
     read_header_lines_(Ls),
     Lines = [M|Ls]
   ; name_contains_name(L, '<'),
     name_contains_name(L, '>') ->
     read_header_lines_(Ls),
     Lines = [L|Ls]
   ; L = '' ->
     read_body_lines_(Ls),
     Lines = Ls
   ; read_header_lines_(Ls),
     Lines = Ls ).
read_header_lines_([]).

beautify_header_line(L, M) :-
   ( member(X, [' <', '  <', '   <', '    <']),
     concat(X, Y, L) ->
     concat('    <', Y, M)
   ; L = M ).

read_body_lines_(Lines) :-
   read_line_(L),
   !,
   ( member(X, ['Content-Disposition']),
     concat(X, _, L) ->
     read_attachment_lines_(Ls)
   ; member(X, ['Content-Type: text/html']),
     concat(X, _, L) ->
     skip_html_lines_(Ls)
   ; read_body_lines_(Ls) ),
   Lines = [L|Ls].
read_body_lines_([]).

skip_html_lines_(Lines) :-
   read_line_(L),
   !,
   ( member(X, ['</html>', '</HTML>']),
     name_contains_name(L, X) ->
     read_body_lines_(Lines)
   ; skip_html_lines_(Lines) ).
skip_html_lines_([]).

read_attachment_lines_(Lines) :-
   read_line_(L),
   !,
   ( ( member(X, ['Content-Disposition']),
       concat(X, _, L)
     ; name_contains_name(L, 'name="') ) ->
     writeln(user, L),
     read_attachment_lines_(Ls),
     Lines = [L|Ls]
   ; read_attachment_lines_(Ls),
     Lines = Ls ).
read_attachment_lines_([]).


/* read_lines_(File, Texts) <-
      */

read_lines_(File, Texts) :-
   open(File, read, Stream),
   set_input(Stream),
   read_lines_(Texts),
   close(Stream),
   writeln_list(Texts).

read_lines_(Texts) :-
   read_lines__([], Ts),
   reverse(Ts, Texts).
   
read_lines__(Ts_1, Ts_2) :-
   read_line_(T),
   !,
   read_lines__([T|Ts_1], Ts_2).
read_lines__(Ts, Ts).


/* read_line_(Text) <-
      */

read_line_(Text) :-
   read_line__([], Cs_1),
   reverse(Cs_1, Cs_2),
   concat(Cs_2, Text).

read_line__(Cs_1, Cs_2) :-
   get_char(C),
   !,
   ( end_of_line(C) ->
     Cs_2 = Cs_1
   ; \+ end_of_file(C),
     read_line__([C|Cs_1], Cs_2) ).
read_line__(Cs, Cs).

end_of_line('\n').

end_of_file(end_of_file).


/******************************************************************/


:- set_prolog_flag(encoding, iso_latin_1).


/******************************************************************/


