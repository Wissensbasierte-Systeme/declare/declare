

/******************************************************************/
/***                                                            ***/
/***       DisLog:  Functions                                   ***/
/***                                                            ***/
/******************************************************************/


:- op(990, xfx, '<=').
:- op(990, xfx, '<-').
:- op(990, xfx, '<==').

:- dynamic
      '<='/2.

:- multifile
      '<='/2.


/*** tests ********************************************************/


test(functions, set_construction) :-
   Numbers <= { N | between(1, 5, N), even(N) },
   writeln(Numbers).


/*** interface ****************************************************/


1 <- fac(0). 
N * fac(N-1) <- fac(N).


X <== ddbase_aggregate(Template, Goal) :-
   !,
   ddbase_aggregate(Template, Goal, Result),
   member(X, Result).
X <== Y :-
   nonvar(X),
   functor(X, @, 1),
   X = @Z,
   !,
   Value <= Y,
   global_variable_set(Z, Value).
X <== Y :-
   X <= Y.

X <= @Y :-
   !,
   global_variable_get(Y, X).
X <= Y :-
   Y =.. [Function|Arguments_1],
   Arguments_1 \= [],
   member(Function, [-, +, *, /]),
   !,
   maplist( <=,
      Arguments_2, Arguments_1 ),
   Goal =.. [Function|Arguments_2],
   !,
   X is Goal.
X <= Y :-
   Y =.. [Function, _, _],
   member(Function, [@, ^]),
   !,
   X := Y.
X <= Y :-
   Y =.. [Function|Arguments_1],
   Arguments_1 \= [],
   member(Function, [is, :=, <=, <-]),
   !,
   maplist( <=,
      Arguments_1, Arguments_2 ),
   Goal =.. [Function|Arguments_2],
   apply(Goal, [X]).
X <= Y :-
%  Y =.. [{}, (T'|'Goal)],
   Y = { T | Goal },
   !,
   findall( T, Goal, X ). 
X <= Y :-
   functor(Y, Function, _),
   member(Function, [findall, maplist, checklist]),
   !,
   call(Y, X).
X <= Y :-
   Y =.. [Function|Arguments_1],
   Arguments_1 \= [],
   not(member(Function, ['.', ':', ',', ';', -, ^, @, /])),
   !,
   maplist( <=,
      Arguments_2, Arguments_1 ),
   Goal =.. [Function|Arguments_2],
   apply(Goal, [X]).
X <= Y :-
   X = Y.


/******************************************************************/


