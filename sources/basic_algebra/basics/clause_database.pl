

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Clause Database                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* prolog_consult(File, Program) <-
      */

prolog_consult(File, Program) :-
   see(File),
   prolog_consult_loop(Program),
   seen.

prolog_consult_loop([R|Rs]) :-
   read(R),
   R \= end_of_file,
   !,
   prolog_consult_loop(Rs).
prolog_consult_loop([]).

/*
prolog_consult(File, Program) :-
   dislog_consult(File, Datalog),
   dislog_rules_to_prolog_rules(Datalog, Program).
*/


/* dislog_rules_to_prolog_rules(Rules_1, Rules_2) <-
      */

dislog_rules_to_prolog_rules(Rules_1, Rules_2) :-
   maplist( dislog_rule_to_prolog_rule,
      Rules_1, Rules_2 ).

dislog_rule_to_prolog_rule(Rule_1, Rule_2) :-
   parse_dislog_rule(Rule_1, [A], [ ], [ ]),
   !,
   Rule_2 = A.
dislog_rule_to_prolog_rule(Rule_1, Rule_2) :-
   parse_dislog_rule(Rule_1, As, Bs, Cs),
%  ( foreach(C, Cs), foreach(D, Ds) do
%       D = not(C) ),
   ( (foreach(C, Cs), foreach(D, Ds)) do
        D = not(C) ),
%  do( (foreach(C, Cs), foreach(D, Ds)),
%      D = not(C) ),
   append(Bs, Ds, Atoms),
   list_to_comma_structure(Atoms, Body),
   ( As = [],
     Rule_2 = (:- Body)
%  ; As = [A],
   ; list_to_semicolon_structure(As, A),
     Rule_2 = (A :- Body) ).


/* prolog_rules_to_dislog_rules(Rules_1, Rules_2) <-
      */

prolog_rules_to_dislog_rules(Rules_1, Rules_2) :-
   maplist( prolog_rule_to_dislog_rule,
      Rules_1, Rules_2 ).

prolog_rule_to_dislog_rule(Rule_1, Rule_2) :-
   ( Rule_1 = (A :- Body),
%    As = [A]
     semicolon_structure_to_list(A, As)
   ; Rule_1 = (:- Body),
     As = [] ),
   !,
   ( Body = true,
     Rule_2 = As
   ; prolog_rule_body_split(Body, Bs, Cs),
     Rule_2 = As-Bs-Cs ),
   !.
prolog_rule_to_dislog_rule(A, [A]).


/* prolog_rule_body_split(Body, Bs, Cs) <-
      */

prolog_rule_body_split(Body, Bs, Cs) :-
   comma_structure_to_list(Body, Atoms),
   sublist( positive_atom,
      Atoms, Bs ),
   sublist( negative_atom,
      Atoms, Ds ),
   maplist( negate_prolog_atom,
      Ds, Cs ).

positive_atom(Atom) :-
   Atom \= not(_),
   Atom \= \+(_).
negative_atom(Atom) :-
   ( Atom = not(_)
   ; Atom = \+(_) ).

negate_prolog_atom(not(Atom), Atom) :-
   !.
negate_prolog_atom(\+(Atom), Atom) :-
   !.
negate_prolog_atom(Atom, not(Atom)).


/* assert_facts(+Facts) <-
      */

assert_facts(Facts) :-
   foreach(A, Facts) do
      assert(A).

assert_facts(Module, Facts) :-
   foreach(A, Facts) do
      assert(Module:A).


/* assert_relation(Module:Predicate, Tuples) <-
      */

assert_relation(Module:Predicate, Tuples) :-
   !,
   ( foreach(Tuple, Tuples) do
        Fact =.. [Predicate|Tuple],
        assert(Module:Fact) ).
assert_relation(Predicate, Tuples) :-
   assert_relation(user:Predicate, Tuples).


/* store_facts_to_file(Predicate/Arity, File) <-
      */

store_facts_to_file(Predicate/Arity, File) :-
   collect_facts(Predicate/Arity, Facts),
   predicate_to_file( File,
      ( foreach(Fact, Facts) do
           writeq(Fact), writeln('.') ) ).


/* collect_facts(+Module:Predicate/Arity, -Facts) <-
      */

collect_facts(Module:Predicate/Arity, Facts) :-
   n_free_variables(Arity, Variables),
   A =.. [Predicate|Variables],
   findall( A,
      Module:clause(A, true),
      Facts ).

collect_facts(Predicate/Arity, Facts) :-
   collect_facts(user:Predicate/Arity, Facts).


/* retract_facts(+Module:Predicate/Arity, -Facts) <-
      */

retract_facts(Mpa) :-
   retract_facts(Mpa, _).

retract_facts(Module:Predicate/Arity, Facts) :-
   n_free_variables(Arity, Variables),
   A =.. [Predicate|Variables],
   findall( A,
      Module:retract(A),
      Facts ).

retract_facts(Predicate/Arity, Facts) :-
   retract_facts(user:Predicate/Arity, Facts).


/* assert_rules(+Rules) <-
      */

assert_rules(Rules) :-
   foreach(Rule, Rules) do
      assert(Rule).

assert_rules(Module, Rules) :-
   foreach(Rule, Rules) do
      Module:assert(Rule).


/* assert_dislog_rules(+Rules) <-
      */

assert_dislog_rules(Rules) :-
   dislog_rules_to_prolog_rules(Rules, Rules_2),
   assert_rules(Rules_2).


/* collect_rules(+Module:Predicate/Arity, -Rules) <-
      */

collect_rules(Module:Predicate/Arity, Rules) :-
   predicate_and_arity_to_atom(Predicate/Arity, A),
   findall( Rule,
      ( Module:clause(A, As),
        ( As = true,
          Rule = A
        ; As \= true,
          Rule = (A :- As) ) ),
      Rules ).

collect_rules(Predicate/Arity, Rules) :-
   collect_rules(user:Predicate/Arity, Rules).

collect_rules(Module, Rules) :-
   dislog_variable_get(output_path, Results),
   concat(Results, 'prolog_listing.tmp', File),
   predicate_to_file( File, Module:listing ),
   prolog_consult(File, Rules).


/* collect_dislog_rules(+Mpa, -Rules) <-
      */

collect_dislog_rules(Mpa, Rules) :-
   collect_rules(Mpa, Rules_2),
   prolog_rules_to_dislog_rules(Rules_2, Rules).


/* retract_rules(+Module:Predicate/Arity, -Rules) <-
      */

retract_rules(Mpa) :-
   retract_rules(Mpa, _).

retract_rules(Module:Predicate/Arity, Rules) :-
   predicate_and_arity_to_atom(Predicate/Arity, A),
   findall( (A :- As),
      Module:retract(A :- As),
      Rules ).

retract_rules(Predicate/Arity, Rules) :-
   retract_rules(user:Predicate/Arity, Rules).


/* retract_dislog_rules(+Predicate/Arity, -Rules) <-
      */

retract_dislog_rules(Predicate/Arity, Rules) :-
   retract_rules(Predicate/Arity, Rules_2),
   prolog_rules_to_dislog_rules(Rules_2, Rules).


/******************************************************************/


