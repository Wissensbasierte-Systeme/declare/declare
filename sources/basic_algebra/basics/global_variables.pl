

/******************************************************************/
/***                                                            ***/
/***          DDK:  Global Variables                            ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      global_variable/2.

:- multifile
      global_variable/2.


/*** interface ****************************************************/


/* global_variables <-
      displays the names and values of the global variables */

global_variables :-
   global_variables(_).

global_variables(Id) :-
   nl,
   forall( global_variable(Id, Variable, Value),
      write_list([Id, ': ', Variable, ' = ', Value, '\n']) ).


/* global_variables_get <-
      */

global_variables_get(Id, [X-Y|Ps]) :-
   global_variable_get(Id, X,Y),
   global_variables_get(Id, Ps).
global_variables_get(Id, [X:Y|Ps]) :-
   global_variable_get(Id, X,Y),
   global_variables_get(Id, Ps).
global_variables_get(_, []).


/* global_variable_get(Id, Variable, Value) <-
      gets the value of a global variable */

global_variable_get(Id, Variable, Value) :-
   global_variable(Id, Variable, Value).


/* global_variable_get(Variable, Value) <-
      gets the value of a global variable */

global_variable_get(Variable, Value) :-
   global_variable(Variable, Value).


/* global_variables_set <-
      */

global_variables_set(Id, [X-Y|Ps]) :-
   global_variable_set(Id, X,Y),
   global_variables_set(Id, Ps).
global_variables_set(Id, [X:Y|Ps]) :-
   global_variable_set(Id, X,Y),
   global_variables_set(Id, Ps).
global_variables_set(_, []).


/* global_variable_set(Id, Variable, Value) <-
      sets the value of a global variable */

global_variable_set(Id, Variable, Value) :-
   retractall(global_variable(Id, Variable, _)),
   assert(global_variable(Id, Variable, Value)).


/* global_variable_set(Variable, Value) <-
      sets the value of a global variable */

global_variable_set(Variable, Value) :-
   retractall(global_variable(Variable, _)),
   assert(global_variable(Variable, Value)).


/* global_variable_switch(Variable, Old_Value, New_Value) <-
      */

global_variable_switch(Id, Variable, Old_Value, New_Value) :-
   global_variable_get(Id, Variable, Old_Value),
   global_variable_set(Id, Variable, New_Value).


/* global_variable_destroy(Variable, Value) <-
      */

global_variable_destroy(Variable) :-
   global_variable_destroy(Variable, _).

global_variable_destroy(Variable, Value) :-
   retract(global_variable(Variable, Value)),
   !.
global_variable_destroy(_, 0).


/* global_variable_add(Variable, Value) <-
      */

global_variable_add(Variable, Value) :-
   retract(global_variable(Variable, Value_1)),
   !,
   Value_2 is Value_1 + Value,
   asserta(global_variable(Variable, Value_2)).
global_variable_add(Variable, Value) :-
   asserta(global_variable(Variable, Value)).


/* global_variable_increment(Variable) <-
      */

global_variable_increment(Id, Variable) :-
   global_variable_get(Id, Variable, Old_Value),
   New_Value is Old_Value + 1,
   global_variable_set(Id, Variable, New_Value).


/* global_variable_add_element(Id, Variable, Element) <-
      if a global variable stores a list,
      then Element is appended in front of this list */

global_variable_add_element(Id, Variable, Element) :-
   global_variable_get(Id, Variable, Elements),
   global_variable_set(Id, Variable, [Element|Elements]).


/*** tests ********************************************************/


test(global_variables, global_variables) :-
   global_variable_set(test1, var1, val1),
   global_variable_get(test1, var1, val1),
   global_variable_set(test1, var1, val2),
   global_variable_set(test1, var2, val2),
   global_variable_set(test2, var1, val1),
   global_variable_set(test2, var2, val2),
   global_variable_set(test2, var3, val3),
   global_variables.


/******************************************************************/


