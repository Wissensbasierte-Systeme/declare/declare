

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Pretty Printer                          ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      portray/2.

:- multifile
      portray/2.


/*** interface ****************************************************/


/* portray(term, Term) <-
      */

portray(term, Term) :-
   pretty_print_term(0, Term).

pretty_print_term(I, X) :-
   var(X),
   !,
   tab(I),
   write(X).
pretty_print_term(I, [X|Xs]) :-
   !,
   tab(I),
   writeln('['),
   J is I + 2,
   write_list_with_separators(
      pretty_print_term_with_comma(J),
      pretty_print_term(J),
      [X|Xs] ),
   nl,
   tab(I),
   write(']').
pretty_print_term(I, []) :-
   !,
   tab(I),
   write('[]').
pretty_print_term(I, A=B) :-
   !,
   tab(I),
   write(A=B).
pretty_print_term(I, Term) :-
   Term =.. [P],
   !,
   tab(I),
   write(P).
pretty_print_term(I, Term) :-
   Term =.. [P, A|As],
   tab(I),
   write(P), writeln('('),
   J is I + 3,
   write_list_with_separators(
      pretty_print_term_with_comma(J),
      pretty_print_term(J),
      [A|As] ),
   nl,
   tab(I),
   write(')').

pretty_print_term_with_comma(I, Term) :-
   pretty_print_term(I, Term),
   writeln(',').


/* portray(list, Xs) <-
      */

portray(list, Xs) :-
   pretty_print_list(Xs, 0).

pretty_print_list(Xs, I) :-
   is_list(Xs),
   length(Xs, N), N > 0,
   !,
   J is I + 3,
   ( foreach(X, Xs) do
        pretty_print_list(X, J) ),
   nl.
pretty_print_list(X, I) :-
   tab(I),
   writeln(X).


/* dportray(lists, List) <-
      */

dportray(lists, List) :-
   write('['),
   pp_list(List),
   nl,
   write(']'),
   write('.').
pp_list([E|Es]) :-
   nl,
   write(E),
   write_separator(Es),
   pp_list(Es).
pp_list([]).

write_separator(Es) :-
   Es \== [],
   write(',').
write_separator([]).


/******************************************************************/


