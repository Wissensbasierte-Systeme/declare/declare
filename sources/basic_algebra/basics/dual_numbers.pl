

/******************************************************************/
/***                                                            ***/
/***           DDK:  Dual System                                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* positive_integer_to_dual(Number, Dual) <-
      */

positive_integer_to_dual(Number, Number) :-
   Number =< 1.
positive_integer_to_dual(Number, Dual) :-
   Number_1 is Number // 2,
   Dual_2 is Number mod 2,
   positive_integer_to_dual(Number_1, Dual_1),
   concat(Dual_1, Dual_2, Dual).


/* dual_to_positive_integer(Dual, Number) <-
      */

dual_to_positive_integer(Dual, Number) :-
   atom_chars(Dual, Chars),
   dual_to_positive_integer_loop(Chars, 0, Number).

dual_to_positive_integer_loop([C|Cs], Acc, Number) :-
   atom_number(C, N),
   Acc_2 is 2 * Acc + N,
   dual_to_positive_integer_loop(Cs, Acc_2, Number).
dual_to_positive_integer_loop([], Acc, Acc).

 
/* number_to_hex(Number, Hex) <-
      */

number_to_hex(Number, Hex) :-
   Number < 10,
   !,
   N is Number + 48,
   name(Hex, [N]).
number_to_hex(Number, Hex) :-
   Number < 16,
   !,
   N is Number + 87,
   name(Hex, [N]).
number_to_hex(Number, Hex) :-
   Number_1 is Number // 16,
   Number_2 is Number mod 16,
   number_to_hex(Number_1, Hex_1),
   number_to_hex(Number_2, Hex_2),
   concat(Hex_1, Hex_2, Hex).


/* hex_to_number(Hex, Number) <-
      */

hex_to_number(Hex, Number) :-
   name(Hex, Ns),
   maplist( hex_to_number_1,
      Ns, As ),
   hex_to_number_2(As, Number).

hex_to_number_1(N, A) :-
   48 =< N,
   N =< 57,
   A is N - 48.
hex_to_number_1(N, A) :-
   65 =< N,
   N =< 90,
   A is N - 55.
hex_to_number_1(N, A) :-
   97 =< N,
   N =< 102,
   A is N - 87.

hex_to_number_2(As, Number) :-
   hex_to_number_2(As, _, Number).

hex_to_number_2([A|As], M, Number) :-
   hex_to_number_2(As, N, Number_2),
   M is N * 16,
   Number is A * M + Number_2.
hex_to_number_2([A], 1, A).


/******************************************************************/


