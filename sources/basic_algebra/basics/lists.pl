

/******************************************************************/
/***                                                            ***/
/***           DDK:  Elementary List Processing                 ***/
/***                                                            ***/
/******************************************************************/


:- op(30, xfy, \).


/*** tests ********************************************************/


test(lists, append) :-
   List = [a, b, c, d],
   Partitions <= { Partition | append(Partition, List) },
   writeln_list(Partitions).
test(lists, difference_list) :-
   list_to_difference_list([a,b], Xs\Ys),
   list_to_difference_list([c], Ys\Zs),
   writeq(Xs\Zs).


/*** interface ****************************************************/


/* list_functor(Functor) <-
      */


list_functor(Functor) :-
   [_] =.. [Functor|_].


/* bag(Xs, Xs) <-
      */

bag(Xs, Xs).


/* list(Xs, Xs) <-
      */

list(Xs, Xs).


/* count(Xs, N) <-
      */

count(Xs, N) :-
   length(Xs, N).


/* length(Xs, K, N) <-
      */

length(Xs, K, N) :-
   length(Xs, M),
   N is K + M.


/* element(Element, List) <-
      */

element(X, [X|_]).
element(X, [_|Xs]) :-
   element(X, Xs).


/* union(Xs, X) <-
      */

union(Xs, X) :-
   iterate_list(union, [], Xs, X).


/* append_and_sort(List, Xs) <-
      */

append_and_sort(List, Xs) :-
   append(List, Xs_2),
   sort(Xs_2, Xs).


/* append(Lists, List) <-
      */

append(Lists, List) :-
   var(Lists),
   !,
   ( Lists = [List]
   ; append(Xs, Ys, List),
     Xs \= [], Ys \= [],
     append(Xss, Ys),
     Lists = [Xs|Xss] ).

append(Lists, List) :-
   must_be(list, Lists),
   lists:append_(Lists, List).

/*
append(Lists, List) :-
   append_loop(Lists, [], List).

append_loop([X|Xs], Sofar, List) :-
   append(Sofar, X, New_Sofar),
   append_loop(Xs, New_Sofar, List).
append_loop([], Sofar, Sofar).
*/

append_2([X|Xs], L) :-
   append_2(Xs, L1),
   append(X, L1, L).
append_2([], []).


/* append_diff(Xs\Ys, Ys\Zs, Xs\Zs) <-
      append for difference lists.  */

append_diff(Xs\Ys, Ys\Zs, Xs\Zs).



/* list_to_difference_list(Xs, Zs\Ys) <-
      */

list_to_difference_list(Xs, Zs\Ys) :-
   append(Xs, Ys, Zs).


/* difference_list_to_list(Xs\Ys, Xs) <-
      */

difference_list_to_list(Xs\Ys, Xs) :-
   Ys = [].


/*

/* append(Xs, Ys, Zs) <-
      */

append([X|Xs], Ys, [X|Zs]) :-
   append(Xs, Ys, Zs).
append([], Ys, Ys).


/* reverse(Xs, Ys) <-
      */

reverse(Xs, Ys) :-
   reverse(Xs, [], Ys).

reverse([X|Xs], Ys, Zs) :-
   reverse(Xs, [X|Ys], Zs).
reverse([], Ys, Ys).

*/


/* first(Xs, X) <-
      */

first([X|_], X).


/* list_to_first_and_last(List, First, Last) <-
      */

list_to_first_and_last(List, First, Last) :-
   first(List, First),
   last(List, Last).


/* n_th_element(List,N,Element) <-
      */

n_th_element([X|_],1,X) :-
   !.
n_th_element([_|Xs],N,X) :-
   M is N - 1,
   n_th_element(Xs,M,X).


/* first_n_elements(N,List_1,List_2) <-
      */

first_n_elements(N,List_1,List_2) :-
   first_n_elements(List_1,N,List_2,_).
 
first_n_elements([],_,[],[]) :-
   !.
first_n_elements(Xs,0,[],Xs) :-
   !.
first_n_elements([X|Xs],N,[X|Ys],Zs) :-
   N > 0,
   M is N - 1,
   !,
   first_n_elements(Xs,M,Ys,Zs).

/*
first_n_elements(List,N,List_h,List_r) :-
   first_n_elements(List,N,[],List_h,List_r).
 
first_n_elements([X|Xs],N,Sofar,List_h,List_r) :-
   N > 0,
   M is N - 1,
   first_n_elements(Xs,M,[X|Sofar],List_h,List_r).
first_n_elements(List_r,N,Sofar,List_h,List_r) :-
   ( N = 0
   ; List_r = [] ),
   reverse(Sofar,List_h).
*/


/* last_n_elements(N, Xs, Ys) <-
      */

last_n_elements(N, Xs, Ys) :-
   reverse(Xs, Us),
   first_n_elements(N, Us, Vs),
   reverse(Vs, Ys).


/* delete_last_element(Xs, Ys) <-
      */

delete_last_element(Xs, Ys) :-
   delete_last_element(Xs, _, Ys).


/* delete_last_element(Xs, X, Ys) <-
      */

delete_last_element(Xs, X, Ys) :-
   reverse(Xs, [X|Zs]),
   reverse(Zs, Ys).


/* select_every_nth_element(N, Xs, Ys) <-
      */

select_every_nth_element(N, [X|Xs], [X|Ys]) :-
   split(N, [X|Xs], _, Zs),
   !,
   select_every_nth_element(N, Zs, Ys).
select_every_nth_element(_, _, []).


/* nth_multiple(Ns, Tuple, Xs) <-
      */

/*
nth_multiple(Ns, Tuple, Xs) :-
   foreach(N, Ns), foreach(X, Xs) do
      nth(N, Tuple, X).
*/

nth_multiple([N|Ns], Tuple, [X|Xs]) :-
   nth(N, Tuple, X),
   nth_multiple(Ns, Tuple, Xs).
nth_multiple([], _, []).


/* n_members(N, Xs, Ys) <-
      */

n_members(0, [], _) :-
   !.
n_members(N, [X|Xs], Ys) :-
   append(As, [X|Bs], Ys),
   append(As, Bs, Zs),
   M is N - 1,
   n_members(M, Xs, Zs).


/* n_ordered_members(N, Xs, Ys) <-
      */

n_ordered_members(0, [], _) :-
   !.
n_ordered_members(N, [X|Xs], Ys) :-
   append(_, [X|Zs], Ys),
   M is N - 1,
   n_members(M, Xs, Zs).


/* split(N, Xs, Yss) <-
      Splits the list Xs into a list Yss of sublists
      of length N.
      The last sublist may be shorter. */

split(_, [], []) :-
   !.
split(N, Xs, [Ys|Yss]) :-
   split(N, Xs, Ys, Zs),
   split(N, Zs, Yss).


/* split(N, Xs, Ys, Zs) <-
      Ys will contain the first N elements of the
      list Xs,
      and Zs will contain the further elements.
      If the length of Xs is smaller than N,
      then Ys = Xs and Zs = []. */

split(0, Xs, [], Xs) :-
   !.
split(N, [X|Xs], [X|Ys], Zs) :-
   M is N - 1,
   split(M, Xs, Ys, Zs).
split(_, [], [], []).


/* split_multiple(N, List, Lists) <-
      */

split_multiple(_, [], []).
split_multiple(N, List, [List_1|Lists_2]) :-
   split(N, List, List_1, List_2),
   split_multiple(N, List_2, Lists_2).


/* middle_split(L, L1, L2) <-
      Splits the list L into two equally sized sublists
      L1 and L2. */
 
middle_split(L, L1, L2) :-
   length(L, I),
   J is I // 2,
   split(J, L, L1, L2).


/* middle_sequence(Xs, Ys) <-
      */

middle_sequence(Xs, Ys) :-
   append(_, Zs, Xs),
   append(Ys, _, Zs).


/* sub_sequence(Xs, Ys) <-
      */

sub_sequence([X|Xs], Ys) :-
   append(_, [X|Ys_2], Ys),
   sub_sequence(Xs, Ys_2).
sub_sequence([], _).


/* list_partition(N, List, Lists) <-
       - list_partition(3, [a,b,c,d,e,f,g,h], Lists).
         Lists = [[a,b,c], [d,e,f], [g,h]]
       - list_partition(5, [a,b,c], Lists).
         Lists = [[a], [b], [c], [], []]
      */

list_partition(N, List, Lists) :-
   length(List, L),
   L >= N,
   !,
%  truncate(L/N, J),
   J is L//N,
   K is (L-J*N) * (J+1),
   split(K, List, List_1, List_2),
   I is J + 1,
   split_multiple(I, List_1, Lists_1),
   split_multiple(J, List_2, Lists_2),
   append(Lists_1, Lists_2, Lists).
list_partition(N, List, Lists) :-
   length(List, L),
   L < N,
   list_partition(L, List, Lists_1),
   K is N - L,
   multify([], K, Lists_2),
   append(Lists_1, Lists_2, Lists).


/* list_exclude_elements(Ns, Xs, Ys) <-
      */

list_exclude_elements(Ns, Xs, Ys) :-
   list_exclude_elements(Ns, 1, Xs, Ys).
   
list_exclude_elements(Ns, N, [_|Xs], Ys) :-
   member(N, Ns),
   M is N + 1,
   list_exclude_elements(Ns, M, Xs, Ys).
list_exclude_elements(Ns, N, [X|Xs], [X|Ys]) :-
   M is N + 1,
   list_exclude_elements(Ns, M, Xs, Ys).
list_exclude_elements(_, _, [], []).


/* list_remove_elements(Elements, Xs, Ys) <-
      */

list_remove_elements(Elements, [X|Xs], Ys) :-
   member(X, Elements),
   !,
   list_remove_elements(Elements, Xs, Ys).
list_remove_elements(Elements, [X|Xs], [X|Ys]) :-
   !,
   list_remove_elements(Elements, Xs, Ys).
list_remove_elements(_, [], []).


/* list_remove_elements(Location, Elements, Xs, Ys) <-
      */

list_remove_elements(front, Elements, [X|Xs], Ys) :-
   member(X, Elements),
   !,
   list_remove_elements(front, Elements, Xs, Ys).
list_remove_elements(front, _, Xs, Xs).

list_remove_elements(back, Elements, Xs, Ys) :-
   reverse(Xs, Us),
   list_remove_elements(front, Elements, Us, Vs),
   reverse(Vs, Ys).

list_remove_elements(both, Elements, Xs, Ys) :-
   list_remove_elements(front, Elements, Xs, Zs),
   list_remove_elements(back, Elements, Zs, Ys).


/* list_exchange_elements(Substitution, Xs, Ys) <-
      */

list_exchange_elements(Substitution, [X|Xs], [Y|Ys]) :-
   member([X, Y], Substitution),
   !,
   list_exchange_elements(Substitution, Xs, Ys).
list_exchange_elements(Substitution, [X|Xs], [X|Ys]) :-
   list_exchange_elements(Substitution, Xs, Ys).
list_exchange_elements(_, [], []).


/* list_exchange_sublist(Substitution, List_1, List_2) <-
      */

list_exchange_sublist(Substitution, List_1, List_2) :-
   member(List_X-Predicate, Substitution),
   copy_term(List_X, List_C),
   append(List_C, List_3, List_1),
   !,
   list_exchange_sublist(Substitution, List_3, List_4),
   apply(Predicate,  [List_C, List_Y]),
   append(List_Y, List_4, List_2).
list_exchange_sublist(Substitution, List_1, List_2) :-
   member([List_X, List_Y], Substitution),
   append(List_X, List_3, List_1),
   !,
   list_exchange_sublist(Substitution, List_3, List_4),
   append(List_Y, List_4, List_2).
list_exchange_sublist(Substitution, [X|Xs], [X|Ys]) :-
   list_exchange_sublist(Substitution, Xs, Ys).
list_exchange_sublist(_, [], []).


/* list_split_at_position(Cut_Lists, List, Lists) <-
      */

list_split_at_position(_, [], []) :-
   !.
list_split_at_position(Cut_Lists, List, [List_2|Lists]) :-
   list_cut_at_position(Cut_Lists, List, List_2),
   list_start_after_position(Cut_Lists, List, List_3),
   list_split_at_position(Cut_Lists, List_3, Lists).


/* list_cut_at_position(Cut_Lists, List_1, List_2) <-
      */

list_cut_at_position(Cut_Lists, List_1, []) :-
   member(Cut_List, Cut_Lists),
   append(Cut_List, _, List_1),
   !.
list_cut_at_position(Cut_Lists, [X|Xs], [X|Ys]) :-
   list_cut_at_position(Cut_Lists,Xs, Ys).
list_cut_at_position(_, [], []).


/* list_start_after_position(Cut_Lists, List_1, List_2) <-
      */

list_start_after_position(Cut_Lists, List_1, List_2) :-
   member(Cut_List, Cut_Lists),
   append(Cut_List, List_2, List_1),
   !.
list_start_after_position(Cut_Lists, [_|Xs], Ys) :-
   list_start_after_position(Cut_Lists, Xs, Ys).
list_start_after_position(_, [], []).


/* list_contains_sequence(Xs, [N, M], E) <-
      Given a list Xs, two natural numbers N and M,
      and an element E, it is tested if Xs contains E
      in all positions from N to M. */
 
list_contains_sequence(Xs, [N, M], E) :-
   length(Xs, K),
   M =< K,
   list_contains_sequence(Xs, 1, [N, M], E).
 
list_contains_sequence([_|Xs], K, [N, M], E) :-
   K < N, L is K + 1,
   !,
   list_contains_sequence(Xs, L, [N, M], E).
list_contains_sequence([X|Xs], K, [N, M], E) :-
   K =< M, L is K + 1,
   !,
   X = E,
   list_contains_sequence(Xs, L, [N, M], E).
list_contains_sequence(_, _, _, _).


/* ord_projection(N, Xs, Ys) <-
      */

ord_projection(N, Xs, Ys) :-
   projection(N, Xs, Zs),
   list_to_ord_set(Zs, Ys).
 
projection(N, [X|Xs], [Y|Ys]) :-
   n_th_element(X, N, Y),
   projection(N, Xs, Ys).
projection(_, [], []).


/* table_projection(Ns, Xs, Ys) <-
      */

table_projection(Ns, Xs, Ys) :-
   maplist( nth_multiple_with_null(Ns),
      Xs, Ys ).

nth_multiple_with_null(Ns, Xs, Ys) :-
   foreach(N, Ns), foreach(Y, Ys) do
      nth_with_null(N, Xs, Y).

nth_with_null(N, Xs, Y) :-
   nth(N, Xs, Y),
   !.
nth_with_null(_, _, '-').


/* list_of_elements_to_relation(Es, Relation) <-
      */

list_of_elements_to_relation([E|Es], [[E]|Relation]) :-
   list_of_elements_to_relation(Es, Relation).
list_of_elements_to_relation([], []).


/* elements_to_lists(Es, Ls) <-
      */

elements_to_lists(Es, Ls) :- 
   maplist( element_to_list,
      Es, Ls ). 

element_to_list(E, [E]).


/* singleton_lists_to_elements(Ls, Es) <-
      */

singleton_lists_to_elements(Ls, Es) :-
   maplist( singleton_list_to_element,
      Ls, Es ).

singleton_list_to_element([E], E).


/* lists_to_ord_sets(List_of_Lists, Ord_Set) <-
      A list List_of_Lists consisting of lists is converted
      to the ordered set Ord_Set consisting of the respective
      ordered lists. */

lists_to_ord_sets(List_of_Lists, Ord_Set) :-
   lists_to_ord_sets(List_of_Lists, [], List_of_Ord_Sets),
   list_to_ord_set(List_of_Ord_Sets, Ord_Set).

lists_to_ord_sets([List|Lists], Acc, Ord_Sets) :-
   list_to_ord_set(List, Ord_Set),
   lists_to_ord_sets(Lists, [Ord_Set|Acc], Ord_Sets).
lists_to_ord_sets([], Ord_Sets, Ord_Sets).


/* list_to_tuple_atom(Xs, Atom) <-
      */

list_to_tuple_atom(Xs, Atom) :-
   maplist( term_to_atom, Xs, Atoms),
   concat_with_separator(Atoms, ', ', A),
   concat(['(', A, ')'], Atom).


/******************************************************************/


