

/******************************************************************/
/***                                                            ***/
/***           DDK:  Complex Numbers                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* multiply_complex(X, Y, Z) <-
      */

multiply_complex(X, Y, Z) :-
   normalize_complex(X, A1+B1*i),
   normalize_complex(Y, A2+B2*i),
   A3 is A1*A2 - B1*B2,
   B3 is A1*B2 + A2*B1,
   simplify_complex(A3+B3*i, Z).


/* sqrt_complex(U, V) <-
      */

sqrt_complex(U, V) :-
   normalize_complex(U, A+B*i),
   C is sqrt(A^2+B^2) - A,
   C > 0,
   !,
   X is B / sqrt(2*C),
   Y is sqrt(C/2),
   simplify_complex(X+Y*i, V).
sqrt_complex(U, V) :-
   normalize_complex(U, A+_*i),    % since B = 0, here
   ( A >= 0 ->
     sqrt(A, V)
   ; sqrt(abs(A), Y),
     V = Y*i ).


/* normalize_complex(X, Y) <-
      */

normalize_complex(X, Y) :-
   ( X = A+i ->
     Y = A+1*i
   ; X = A-i ->
     Y = A-1*i
   ; X = i ->
     Y = 0+1*i
   ; X = -i ->
     Y = 0-1*i
   ; X = A-B*i ->
     C is -B,
     Y = A+C*i
   ; X = A+B*i ->
     Y = A+B*i
   ; X = B*i ->
     Y = 0+B*i
   ; X = A ->
     Y = A+0*i ).


/* simplify_complex(X, Y) <-
      */

simplify_complex(X, Y) :-
   ( X = A + B*i,
     numbers_are_equal(B, 0) ->
     Y = A
   ; X = A + B*i,
     numbers_are_equal(A, 0) ->
     Y = B*i
   ; Y = X ).


/* numbers_are_equal(X, Y) <-
      */

numbers_are_equal(X, Y) :-
   X =< Y,
   Y =< X.


/******************************************************************/


