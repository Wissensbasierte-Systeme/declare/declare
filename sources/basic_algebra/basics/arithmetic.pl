

/******************************************************************/
/***                                                            ***/
/***           DDK:  Arithmetic                                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* sum_atoms(Xs, Sum) <-
      */

sum_atoms(Xs, Sum) :-
   maplist( atom_number,
      Xs, Numbers ),
   sum(Numbers, Sum).


/* sum_list(Xs, X) <-
      */

/*
sum_list(Xs, X) :-
   sumlist(Xs, X).
*/


/* is_integer(N) <-
      */

is_integer(0).
is_integer(N) :-
   is_integer(M),
   N is M + 1.

is_natural_number(1).
is_natural_number(N) :- 
   is_natural_number(M),
   N is M + 1.


/* maximum(List, Maximum) <-
      */

max(List, Maximum) :-
   maximum(List, Maximum).

maximum([X], X) :-
   !.
maximum([X|Xs], Max) :-
   maximum(Xs, Y),
   maximum(X, Y, Max).

maximum(X, '', X) :-
   !.
maximum('', Y, Y) :-
   !.
maximum(X, Y, X) :-
   X > Y,
   !.
maximum(X, Y, Y) :-
   X =< Y.


/* minimum(List, Minimum) <-
      */

min(List, Minimum) :-
   minimum(List, Minimum).

minimum([X], X) :-
   !.
minimum([X|Xs], Max) :-
   minimum(Xs, Y),
   minimum(X, Y, Max).

minimum(X, '', X) :-
   !.
minimum('', Y, Y) :-
   !.
minimum(X, Y, X) :-
   X < Y,
   !.
minimum(X, Y, Y) :-
   X >= Y.


/* max(Compare, X, Y, Z) <-
      */

max(Compare, X, Y, Z) :-
   apply(Compare, [X, Y]),
   !,
   Z = X.
max(_, _, Y, Y).


/* greater_equal_list(Xs, Ys) <-
      */

greater_equal_list([X|_], [Y|_]) :-
   X > Y.
greater_equal_list([X|Xs], [X|Ys]) :-
   greater_equal_list(Xs, Ys).
greater_equal_list(_, []).


/* float_equal(X, Y) <-
      */

float_equal(X, Y) :-
   X =< Y,
   Y =< X.


/* float_between(X, Y, Z) <-
      */

float_between(X, Y, Z) :-
   nonvar(X), nonvar(Y), nonvar(Z),
   X =< Z, Z =< Y,
   !.
float_between_2(X, Y, Z) :-
   nonvar(X), nonvar(Y), nonvar(Z),
   X =< Z, Z < Y,
   !.


/* square(X, Y) <-
      */

square(X, Y) :-
   Y is X * X.


/* power(X, I, Y) <-
      */

power(_, 0, 1) :-
   !.
power(X, I, Y) :-
   nonvar(I),
   !,
   J is I - 1,
   power(X, J, Z),
   Y is X * Z.
power(X, I, Y) :-
   nonvar(Y),
   !,
   Z is Y // X,
   power(X, J, Z),
   I is J + 1.


/* factorial(N, F) <-
      */

factorial(N, 1) :-
   N =< 1,
   !.
factorial(N, F) :-
   M is N - 1,
   factorial(M, G),
   F is N * G.


/* binomial(N, K, I) <-
      */

binomial(N, K, 0) :-
   K > N,
   !.
binomial(N, N, 1) :-
   !.
binomial(N, K, I) :-
%  K < N // 2,
   K < N / 2,
   !,
   M is N - K,
   binomial(N, M, I).
binomial(N, K, I) :-
   J is K + 1,
   product(J, N, A),
   M is N - K,
   factorial(M, B),
%  I is A // B.
   I is A / B.


/* product(Is, Product) <-
      */

product(Is, Product) :-
   product_(1, Is, Product).

product_(J, [I|Is], Product) :-
   K is J * I,
   product_(K, Is, Product).
product_(J, [], J).


/* product(I, J, P) <-
      */

product(I, J, P) :-
   product(I, J, 1, P).
product(I, I, P_1, P_2) :-
   !,
   P_2 is I * P_1.
product(I, J, P_1, P_3) :-
   K is I + 1,
   product(K, J, P_1, P_2),
   P_3 is I * P_2.


/* sum(Xs, Sum) <-
      */

sum([X|Xs], Sum) :-
   sum(Xs, Sum_2),
   Sum is X + Sum_2.
sum([], 0).


/* add(Xs, Sum) <-
      */

add(Xs, Sum) :-
   add_loop(Xs, 0, Sum).

add_loop([X|Xs], Sofar, Sum) :-
   Sofar_2 is Sofar + X,
   add_loop(Xs, Sofar_2, Sum).
add_loop([], Sum, Sum).


/* add(N, M, K) <-
      K = N + M. */

add(N, M, K) :-
   ( var(N) -> N is K - M
   ; var(M) -> M is K - N
   ; var(K) -> K is N + M ).


/* subtract_(N, M, K) <-
      K = N - M. */

subtract_(N, M, K) :-
   ( var(N) -> N is K + M
   ; var(M) -> M is N - K
   ; var(K) -> K is N - M ).


/* numbers_to_floating_averages(N, Xs, Ys) <-
      */

numbers_to_floating_averages(N, Xs, Ys) :-
   numbers_to_floating_averages_(N, Xs, Zs),
   !,
   numbers_extend_for_floating_averages(N, Zs, Ys).
numbers_to_floating_averages(_, Xs, Ys) :-
   average(Xs, A),
   length(Xs, M),
   multify(A, M, Ys).

numbers_to_floating_averages_(N, Xs, Ys) :-
   length(Xs, M), M >= N,
   !,
   first_n_elements(N, Xs, As),
   sum(As, A), append(As, Bs, Xs),
   numbers_to_floating_sums(A, Bs, Xs, Zs),
   vector_multiply(1/N, Zs, Ys).

numbers_to_floating_sums(A, [U|Us], [V|Vs], [A|Ws]) :-
   B is A + U - V,
   numbers_to_floating_sums(B, Us, Vs, Ws).
numbers_to_floating_sums(A, [], _, [A]).

numbers_extend_for_floating_averages(N, Xs, Ys) :-
   first(Xs, A), last(Xs, B),
   M1 is N // 2, M2 is N - M1 - 1,
   multify(A, M1, As), multify(B, M2, Bs),
   append([As, Xs, Bs], Ys).


/* variance(Xs, Variance) <-
      */

variance(Xs, Variance) :-
   average(Xs, A),
   ( foreach(X, Xs), foreach(Y, Ys) do
        power(X-A, 2, Y) ),
   average(Ys, Variance).
 

/* average(Xs, X) <-
      */

average([], '--') :-
   !.
average(Xs, X) :-
   add(Xs, Sum),
   length(Xs, N),
   X is Sum / N.


/* mean_value_save(Values, Mean),
   mean_value(Values, Mean) <-
      */

mean_value_save([], 1) :-
   !.
mean_value_save(Values, Mean) :-
   mean_value(Values, Mean).
 
mean_value(Values, Mean) :-
   add(Values, Sum),
   length(Values, N),
   Mean is Sum / N.
 
 
/* multiply(X, Y, Z) <-
     */

multiply(X, Y, Z) :-
   Z is X * Y.
 

/* exp(X, Y) <-
      */

exp(X, Y) :-
  Y is exp(X).


/* divisible_by(N, M) <-
      */

divisible_by(N, M) :-
   modulo(N, M, 0).
 
 
/* modulo(X, Y, M) <-
      */

modulo(X, Y, M) :-
   M is X mod Y,
   !.


/* round(X, N, Y) <-
      */

round(X, N, Y) :-
   power(10, N, F),
   Y is round( X * F ) / F.

round_2(N, X, Y) :-
   round(X, N, Y).


/* truncate(X, Y) <-
      */

truncate(X, Y) :-
   Y is truncate(X).


/* base_b_list(B, I, List) <-
      */

base_b_list(B, I, [J]) :-
   I < B,
   !,
   J is I + 48.
base_b_list(B, I, D) :-
   M is I // B,
   N is ( I mod B ) + 48,
   base_b_list(B, M, MD),
   append(MD, [N], D).


/* atom_to_number(Atom, Number) <-
      */

atom_to_number(Atom, Number) :-
   term_to_atom(Number, Atom),
   number(Number),
   !.
atom_to_number(Atom, Atom).


/* log(Base, X, Logarithm) <-
      */

log(Base, X, Logarithm) :-
   Logarithm is log(X) / log(Base).


/* greatest_common_divisor(N, M, G) <-
      The greatest common divisor G of two natural numbers
      N and M is computed. */
 
greatest_common_divisor(N, M, G) :-
   K is rational(N),
   L is rational(M),
   greatest_common_divisor_2(K, L, G).

greatest_common_divisor_2(0, 0, 1) :-
   !.
greatest_common_divisor_2(N, 0, N) :-
   !.
greatest_common_divisor_2(N, M, G) :-
   N >= M,
   !,
   K is N mod M,
   greatest_common_divisor_2(M, K, G).
greatest_common_divisor_2(N, M, G) :-
   greatest_common_divisor_2(M, N, G).
 
greatest_common_divisor([N], N) :-
   !.
greatest_common_divisor([N|Ns], G) :-
   greatest_common_divisor(Ns, M),
   greatest_common_divisor(N, M, G).
greatest_common_divisor([], 1).


/* least_common_multiple(N, M, L) <-
      The least common multiple L of two natural numbers
      N and M is computed. */
 
least_common_multiple(N, M, L) :-
   greatest_common_divisor(N, M, G),
   L is N * M / G.
 
least_common_multiple([N|Ns], L) :-
   least_common_multiple(Ns, M),
   least_common_multiple(N, M, L).
least_common_multiple([], 1).


/******************************************************************/


