

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Diverse Predicates                      ***/
/***                                                            ***/
/******************************************************************/


:- dynamic 
      data_dictionary/3,
      i_files/2.


/******************************************************************/


/* readatom3 <-
      reading a relation from a file;
      reads an atom p(a1,...,an) and the following dot
      from the current input stream */

readatom3(_) :-
   seeing(File),
   set(eof,File,true), 
   !, 
   fail.
readatom3(Atom) :-
   get0(C), 
   readatom1(C,Atom,46).
readatom3(-1).

readatom1(-1,_,_) :-
   !, 
   seeing(File),
   close9(File), 
   fail.
readatom1(255,_,_) :-
   !, 
   seeing(File),
   close9(File), 
   fail.
readatom1(C1,Atom,C3) :-
   in_word(C1,NewC1),
   !,
   readword(NewC1,P,40),       /* ( */
   get0(C2),
   readarguments(C2,L,41),     /* ) */
   get0(C3),
   Atom =.. [P|L].
readatom1(_,Atom,C3) :-
   get0(C2), 
   readatom1(C2,Atom,C3).


/* readterm <-
      reads a term from the current input stream
      starting with the term's first character C1 and
      ending with the first character C2 following the term */

readterm(91,L,C2) :-          /* [ */
   !, 
   get0(C1),
   readlist(C1,L,C2).
readterm(C1,H,C2) :-
   readword(C1,H,C2).


/* readlist <-
      reads a list [a1,...,an]
      starting with the first character C1 of a1 and
      ending with the first character C3 following the
      closing bracket ']' */

readlist(93,[],C3) :-         /* ] */
   !, 
   get0(C3).
readlist(C1,[H|T],C3) :-
   readterm(C1,H,C2),
   restlist(C2,T,C3).

restlist(93,[],C) :-          /* ] */
   !, 
   get0(C).
restlist(44,[H|T],C) :-       /* , */
   get0(C1),
   readterm(C1,H,C2),
   restlist(C2,T,C).


/* readarguments <-
      reads the arguments of an atom p(a1,...,an)
      starting with the first character of a1 and
      ending with the closing bracket ')' following an */

readarguments(C1,[H|T],C3) :-
   readterm(C1,H,C2),
   restarguments(C2,T,C3).

restarguments(41,[],41) :- 
   !.                            /* ) */
restarguments(44,[H|T],C) :-     /* , */
   get0(C1),
   readterm(C1,H,C2),
   restarguments(C2,T,C).


/* open9, close9 :
   setting and removing eof-flag,
   necessary for reading with readatom */

open9(File) :-
   \+ set(eof,File,true),
   !.
open9(File) :-
   retract(set(eof,File,true)).

close9(File) :-
   set(eof,File,true),
   !.
close9(File) :-
   asserta(set(eof,File,true)).


/* data_dictionary, intermediate files <-
      */

data_dictionary(a,b,n).

insert_data_dictionary(ST1,ST2,Filename) :-
   data_dictionary(ST1,ST2,Filename), 
   !.

insert_data_dictionary(ST1,ST2,Filename) :-
   gensym(file,Filename),
   asserta(data_dictionary(ST1,ST2,Filename)).

load_data_dictionary :- 
   retractall(data_dictionary(_,_,_)), 
   [filed].

save_data_dictionary :-
   telling(File_t),
   tell(filed),
   saving_data_dictionary, put(-1),
   close(filed), told,
   tell(File_t).

saving_data_dictionary :-
   data_dictionary(ST1,ST2,Filename),
   Atom =.. [data_dictionary,ST1,ST2,Filename],
   write(Atom), put(46), nl,
   fail.
saving_data_dictionary :-
   current_num(file,Num),
   Atom =.. [current_num,file,Num],
   nl, write(Atom), put(46), nl.


/* intermediate files <-
      */

i_files(filea,fileb).
o_file(fileo).

intermediate_files(Rel1,Rel3) :-
   retract(i_files(Rel1,Rel3)),
   asserta(i_files(Rel3,Rel1)).


/******************************************************************/


