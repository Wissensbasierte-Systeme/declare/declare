

/******************************************************************/
/***                                                            ***/
/***        DDK:  Handling of the Prolog Database               ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(declare_predicate_part, '').


/*** interface ****************************************************/


/* declare_search_tool <-
      */

declare_search_tool :-
   dislog_flag_set(xpce_mode, yes),
   dislog_frame_generic([
      header: 'Declare - Search Tool',
      above: [ ],
      interface: [
         'Search Predicate' - declare_predicate_part ],
      below: [
         'Search' -
            prolog_database_search ],
      close_button: [
         'Close' - right ] ]).

prolog_database_search :-
   dislog_dialog_variables( get,
      [ 'Search Predicate' - Name ] ),
   prolog_database_search(Name).


/* prolog_database_search(Name, Tuples) <-
      Search for Name as a substring of a predicate symbol. */

prolog_database_search(Name) :-
   prolog_database_search(Name, _).

prolog_database_search(Name, Tuples) :-
   ddbase_aggregate( [length(Predicate), Path, list(Predicate)],
      ( predicate_property(Atom, file(File)),
        Atom =.. [Predicate|_],
        name_contains_name(Predicate, Name),
        name_split_at_position(["/"], File, Xs),
        last_n_elements(3, Xs, Ys),
        concat_with_separator(Ys, '/', Path) ),
      Tuples_2 ),
   sort(Tuples_2, Tuples_3), length(Tuples_3, N),
   writeln_list(Tuples_3), write_list([N, ' files \n']),
   ( foreach(T1, Tuples_3), foreach(T2, Tuples_4) do
        ( T1 = [_, B, Ps_1], sort(Ps_1, Ps_2),
          length(Ps_2, M),
          list_to_multiple_line_atom(3, Ps_2, C),
          T2 = [M, B, C] ) ),
   sort(Tuples_4, Tuples_A),
   ddbase_aggregate( [length(Predicate), A,B,C],
      ( predicate_property(Atom, file(File)),
        Atom =.. [Predicate|_],
        name_contains_name(Predicate, Name),
        name_split_at_position(["/"], File, Xs),
        last_n_elements(3, Xs, [A,B,C]) ),
      Tuples_5 ),
   sort(Tuples_5, Tuples_B),
   concat(['prolog_database_search(', Name, ')'], Title),
   xpce_display_table(Title,
      ['#', 'Unit', 'Module', 'File'], Tuples_B),
   xpce_display_table(Title,
      ['#', 'Path', 'Predicates'], Tuples_A),
   Tuples = Tuples_A,
   !.


/*** implementation ***********************************************/


/* list_to_multiple_line_atom(N, Xs, A) <-
      */

list_to_multiple_line_atom(N, Xs, A) :-
   split_multiple(N, Xs, Pss_2),
%  maplist( list_to_comma_structure_, Pss_2, Ps_3 ),
   maplist( names_append_with_separator_(',   '), Pss_2, Ps_3 ),
%  maplist( term_to_atom, Ps_3, P_4 ),
   Ps_3 = P_4,
   names_append_with_separator(P_4, ',\n', A).

names_append_with_separator_(Separator, Xs_1, Xs_2) :-
   names_append_with_separator(Xs_1, Separator, Xs_2).

list_to_comma_structure_([], '') :-
   !.
list_to_comma_structure_(Xs, S) :-
   list_to_comma_structure(Xs, S).


/* listing_all(Predicates) <-
      */

listing_all(Predicates) :-
   foreach(Predicate, Predicates) do
      listing(Predicate).


/* retract_all(Atom) <-
      */

retract_all(Atom) :- 
   retract(Atom),
   fail. 
retract_all(_).                      


/* assert_all(Atoms) <-
      */

assert_all(Atoms) :- 
   foreach(Atom, Atoms) do
      assert(Atom).


/* dabolish(Predicate/Arity) <-
      "abolish" Predicate/Arity and leave it dynamic. */

dabolish_save(Predicate) :-
   dabolish(Predicate),
   !.

dabolish_all(Predicates_with_Arities) :-
   foreach(P, Predicates_with_Arities) do
      dabolish(P).

dabolish(Predicate/Arity) :-
   predicate_and_arity_to_atom(Predicate/Arity, Atom),
   retractall(Atom).


/* predicate_and_arity_to_atom(Module:Predicate/Arity, Atom) <-
      */

predicate_and_arity_to_atom(M:P/A, M:Atom) :-
   !,
   functor(Atom, P, A).
predicate_and_arity_to_atom(P/A, Atom) :-
   functor(Atom, P, A).

/*
predicate_and_arity_to_atom(Module:Predicate/Arity, Atom) :-
   !,
   predicate_and_arity_to_atom(Predicate/Arity, Atom_2),
   Atom = Module:Atom_2.
predicate_and_arity_to_atom(Predicate/Arity, Atom) :-
   n_free_variables(Arity, Variables),
   Atom =.. [Predicate|Variables].
*/


/* collect_arguments(Predicate, Arguments) <-
      */

collect_arguments(Predicate, Arguments) :-
   Dummy =.. [Predicate, dummy],
   assert(Dummy),
   Atom =.. [Predicate, Variable],
   findall( Variable,
      ( call(Atom),
        Atom \== Dummy ),
      Arguments ),
   retract(Dummy).


/* collect_arguments_generic(Predicate/Arity, Arguments) <-
      */

collect_arguments_generic(Predicate/Arity, Arguments) :-
   multify(dummy, Arity, Dummies),
   Dummy =.. [Predicate|Dummies],
   assert(Dummy),
   n_free_variables(Arity, Variables),
   Atom =.. [Predicate|Variables],
   findall( Variables,
      ( call(Atom),
        Atom \== Dummy ),
      Arguments ),
   retract(Dummy).


/* assign_relation(Relation_Name, Tuples) <-
      */

assign_relation(Relation_Name, Tuples) :-
   abolish(Relation_Name, 1),
   assert_arguments(Relation_Name, Tuples).


/* assert_arguments(P, Arguments) <-
      given an unary predicate symbol P,
      for each argument X in Arguments an atom P(X)
      is asserted. */

assert_arguments(P, Xs) :-
   foreach(X, Xs) do
      ( Atom =.. [P, X],
        assert(Atom) ).


/* assert_arguments_generic(P, Arguments) <-
      given an n-ary predicate symbol P,
      for each argument list X = [X1,...,Xn] in Arguments
      an atom P(X1,...,Xn) is asserted. */

assert_arguments_generic(P, Xs) :-
   foreach(X, Xs) do
      ( Atom =.. [P|X],
        assert(Atom) ).


/******************************************************************/


