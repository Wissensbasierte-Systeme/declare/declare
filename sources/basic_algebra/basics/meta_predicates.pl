

/******************************************************************/
/***                                                            ***/
/***       DisLog:  Meta Predicates                             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* maplist(Predicate, Goal) <-
      */

:- ( predicate_property(maplist(_, _), _)
   ; assert(
        maplist(Predicate, Goal) :-
           checklist(Predicate, Goal) ) ).


/*
/* built_in(nv) <-
      */

built_in(nv).
*/


/* foldl(Predicate, Values, Input, Output) <-
      */

foldl(_, [], Input, Input).
foldl(Predicate, [V|Vs], Input, Output) :-
   apply(Predicate, [V, Input, Intermediate]),
   foldl(Predicate, Vs, Intermediate, Output). 


/* foldr(Predicate, Input, Values, Output) <-
      */

foldr(_, Input, [], Input).
foldr(Predicate, Input, [V|Vs], Output) :-
   foldr(Predicate, Input, Vs, Intermediate),
   apply(Predicate, [V, Intermediate, Output]).


/* fold(Predicate, Input, Values, Output) <-
      like iterate_list. */

fold(Predicate, Input, [V|Vs], Output) :-
   !,
   apply(Predicate, [Input, V, Intermediate]),
   fold(Predicate, Intermediate, Vs, Output).
fold(_, Input, [], Input).


/* iterate_list(Predicate, Input, Values, Output) <-
      Predicate is applied iteratively starting with Input.
      In each iteration one of the elements of Values is
      applied to the current item.
      The final result is Output. */
      
iterate_list(Predicate, Input, [V|Vs], Output) :-
   !,
   apply(Predicate, [Input, V, Intermediate]),
   iterate_list(Predicate, Intermediate, Vs, Output).
iterate_list(_, Input, [], Input).


/* omega_iteration(Predicate, Input, Output, Condition-[X, Y]) <-
      Predicate is applied iteratively starting with Input.
      When the termination condition Condition on a current item
      Y and its predecessor X is satisfied, then Output = Y.
      Input, Output, X, Y, must be of the same type. */

omega_iteration(Predicate, Input, Output, Check_Rule) :-
   assert(omega_iteration_current(Input)),
   Predicate =.. L_Predicate,
   append(L_Predicate, [X, Y], L_Goal),
   Goal =.. L_Goal,
   repeat,
      omega_iteration_single_step(Goal, X, Y),
   omega_iteration_ready(X, Y, Check_Rule),
   !,
   retract(omega_iteration_current(Output)).

omega_iteration_single_step(Goal, X, Y) :-
   retract(omega_iteration_current(X)),
   call(Goal),
   assert(omega_iteration_current(Y)),
   !.

omega_iteration_ready(X, Y, [Condition]-[X, Y]) :-
   call(Condition).


/* fixpoint(Predicate, X, Y) <-
      */

fixpoint(Predicate, X, Y) :-
   apply(Predicate, [X, Z]),
   ( ( X = Z,
       !,
       Y = Z )
   ; fixpoint(Predicate, Z, Y) ).


/* call_predicates(Ps, X, Y) <-
      */

call_predicates([P|Ps], X, Y) :-
   call(P, X, Z),
   call_predicates(Ps, Z, Y).
call_predicates([], X, X).


/* dislog_file_to_file(Operator, Input_Files, Output_Files) <-
      The DisLog-operator Operator is applied to the objects
      given in Input_Files, the resulting objects are written
      to Output_Files. */
 
dislog_file_to_file(Operator, Input_Files, Output_Files) :-
   maplist( dconsult,
      Input_Files, Input_Objects ),
   length(Output_Files, N),
   n_free_variables(N, Output_Objects),
   Operator =.. L1,
   append([L1, Input_Objects, Output_Objects], L2),
   Goal =.. L2,
   call(Goal),
   pair_lists(Output_Objects, Output_Files, Pairs),
%  forall( member([Object, File], Pairs),
%     ( dstore(Object, File),
%       edit_database(File) ) ),
   forall( member([Object, File], Pairs),
      dstore(Object, File) ),
   !.


/* stop_trace <-
      */

ntnd :-
   notrace, nodebug.

stop_trace :-
   notrace, nodebug.


/*** tests ********************************************************/


test(meta_predicates,iterate_list(add)) :-
   iterate_list( add,
      0, [1,2,3,4,5], Sum ),
   writeln(sum(Sum)).

test(meta_predicates, iterate_list(multiply)) :-
   iterate_list( multiply,
      1, [1,2,3,4,5], Sum ),
   writeln(product(Sum)).

test(meta_predicates, omega_iteration(tps_prime_operator)) :-
   omega_iteration(
      tps_prime_operator([[c]-[a], [c]-[b]]),
      [[a,b]], State, [state_subsumes(SX,SY)]-[SX,SY] ),
   nl, nl,
   writeln('Final State = '),
   dportray(dhs, State).

tps_prime_operator(Program, State_1, State_2) :-
   tps_operator(Program, State_1, State_3),
   dportray(dhs, State_3),
   state_union(State_1, State_3, State_2).

test(meta_predicates:dislog_file_to_file,
      tps_fixpoint_iteration) :-
   dislog_file_to_file( tps_fixpoint_iteration,
      ['nmr/elementary/abcd1'], ['nmr/elementary/test_output'] ).

test(meta_predicates, fixpoint) :-
   fixpoint( number_iteration_special,
      17, Y ),
   writeln(Y).

number_iteration_special(1, 1) :-
   !.
number_iteration_special(X, Y) :-
   even(X),
   write(X), write(' '),
   !,
   Y is X / 2.   
number_iteration_special(X, Y) :-
   write(X), write(' '),
   Y is ( X - 1) / 2.   

even(X) :-
   0 is X mod 2.

odd(X) :-
   1 is X mod 2.


/******************************************************************/


