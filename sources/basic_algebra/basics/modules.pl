

/******************************************************************/
/***                                                            ***/
/***        Dislog :  Prolog Modules                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* prolog_module_to_xpce(Module) <-
      */

prolog_module_to_xpce(Module) :-
   collect_rules(Module, Program_1),
   prolog_program_group_dynamics(Program_1, Program_2),
   prolog_rules_to_dislog_rules(Program_2, DisLog),
%  dportray(lp_gui, DisLog).
   dislog_rules_to_xpce(Module, DisLog).


/* prolog_module_to_head_predicates(Module, Predicates) <-
      */

prolog_module_to_head_predicates(Module, Predicates) :-
   collect_rules(Module, Program),
   findall( P/N,
      ( member(Rule, Program),
        prolog_rule_to_head_predicate(Rule, P/N) ),
      Predicates_2 ),
   sort(Predicates_2, Predicates).


/* prolog_program_group_dynamics(Prolog_1, Prolog_2) <-
      */

prolog_program_group_dynamics(Prolog_1, Prolog_2) :-
   findall( P/N,
      member((:- dynamic P/N), Prolog_1),
      Predicates ),
   findall( Rule,
      ( member(Rule, Prolog_1),
        Rule \= (:- dynamic P/N) ),
      Prolog_3 ),
   list_to_comma_structure(Predicates, Xs),
   Prolog_2 = [(:- dynamic Xs)|Prolog_3].


/* dislog_rules_to_xpce(Label, Rules) <-
      */

dislog_rules_to_xpce(Label, Rules) :-
   dportray(lp_gui(File), Rules),
   concat('Module ', Label, Label_2),
   file_view_for_gui(Label_2, File).


/******************************************************************/


