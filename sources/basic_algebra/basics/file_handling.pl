

/******************************************************************/
/***                                                            ***/
/***           DDK:  File Handling                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* file_copy(File_1, File_2) <-
      */

file_copy(File_1, File_2) :-
   concat(['cp ', File_1, ' ', File_2], Command),
   us(Command).


/* file_remove(File) <-
      */

file_remove(File) :-
   us_declare(['rm ', File]).


/* read_lines_from_file(File, N-M, Atoms) <-
      */

read_lines_from_file(File, N-M, Atoms) :-
   open(File, read, Stream),
   ( for(_, 1, N-1) do
        read_line_to_atom(Stream, _) ),
   ( for(_, 0, M-N), foreach(Atom, Atoms) do
        read_line_to_atom(Stream, Atom) ),
   close(Stream).


/* read_line_to_atom(Stream, Atom) <-
      */

read_line_to_atom(Stream, Atom) :-
   read_line_to_codes(Stream, Codes),
   ( Codes = end_of_file ->
     fail
   ; atom_codes(Atom, Codes) ).


/* concat_file_and_name(File_1, Name_2, File_3) <-
      */

concat_file_and_name(File_1, Name_2, File_3) :-
   read_file_to_codes(File_1, String_1),
   name(Name_2, String_2),
%  append([String_1, String_2, "\n"], String_3),
   append([String_1, String_2, [10]], String_3),
   name(Name_3, String_3),
   tell(File_3),
   write(Name_3),
   told.


/* concat_name_and_file(Name_1, File_2, File_3) <-
      */

concat_name_and_file(Name_1, File_2, File_3) :-
   name(Name_1, String_1),
   read_file_to_codes(File_2, String_2),
%  append([String_1, "\n", String_2], String_3),
   append([String_1, [10], String_2], String_3),
   name(Name_3, String_3),
   tell(File_3),
   write(Name_3),
   told.


/* concat_files(File_1, File_2, File_3) <-
      */

concat_files(File_1, File_2, File_3) :-
   read_file_to_codes(File_1, String_1),
   read_file_to_codes(File_2, String_2),
   append(String_1, String_2, String_3),
   name(Name_3, String_3),
   tell(File_3),
   write(Name_3),
   told.


/* directory_contains_directories(+Directory, ?Directories) <-
      Returns the list Directories of all directories within
      Directory */

directory_contains_directories(Directory, Directories) :-
   directory_contains_files(Directory, Files),
   directory_with_ending_slash(Directory, Directory_2),
   findall( Subdir,
      ( member(Dir, Files),
        concat(Directory_2, Dir, Subdir),
        exists_directory(Subdir) ),
        Dirs ),
   maplist( directory_with_ending_slash,
      Dirs, Directories ).


/* directory_contains_files_recursive(
         +Extensions, +Directory, ?Files) <-
      Recursively lists all Files under Directory
      with a particular file extension in Extensions. */

directory_contains_files_recursive(
      Extensions, Directory, Files) :-
   directory_contains_files(Extensions, Directory, Files_1),
   directory_with_ending_slash(Directory, Dir),
   maplist( concat(Dir),
      Files_1, Files_2 ),
   directory_contains_directories(Directory, Dirs),
   maplist( directory_contains_files_recursive(Extensions),
      Dirs, List_of_Files ),
   append([Files_2|List_of_Files], Files).


/* directory_with_ending_slash(+Directory_1, ?Directory_2) <-
      Appends a slash at the end of Directory_1. */

directory_with_ending_slash(Directory_1, Directory_2) :-
   ( sub_string(Directory_1, _, 1, 0, '/'),
     Directory_2 = Directory_1
   ; concat(Directory_1, '/', Directory_2) ).


/* directory_contains_files(Extensions, Directory, Files) <-
      */

directory_contains_files(Extensions, Directory, Files) :-
   is_list(Extensions),
   !,
   ( foreach(Extension, Extensions), foreach(Fs, Fss) do
        directory_contains_files(Extension, Directory, Fs) ),
   append(Fss, Files).


/* directory_contains_files(Extension, Directory, Files) <-
      */

directory_contains_files(Extension, Directory, Files) :-
   directory_contains_files(Directory, Files_2),
   sublist( file_has_ending(Extension),
      Files_2, Files ).

file_has_ending(Extension, File) :-
   concat('.', Extension, Extension_2),
   concat(_, Extension_2, File).


/* files_subtract_ending(Extension, Files_1, Files_2) <-
      */

files_subtract_ending(Extension, Files_1, Files_2) :-
   concat('.', Extension, Extension_2),
   findall( File_2,
      ( member(File_1, Files_1),
        concat(File_2, Extension_2, File_1) ),
      Files_2 ).


/* directory_contains_files_absolute(Directory, Paths) <-
      */

directory_contains_files_absolute(Extensions, Directory, Paths) :-
   directory_contains_files(Extensions, Directory, Files),
   concat(Directory, '/', Dir),
   maplist( concat(Dir),
      Files, Paths ).

directory_contains_files_absolute(Directory, Paths) :-
   directory_contains_files(Directory, Files),
   concat(Directory, '/', Dir),
   maplist( concat(Dir),
      Files, Paths ).


/* directory_contains_files(Directory, Files) <-
      determines the list Files of all files in Directory. */

directory_contains_files(Directory, Files) :-
   swi_directory_contains_files(Directory, Files).

swi_directory_contains_files(Directory, Files) :-
   concat(Directory, '/*', Wildcard),
   expand_file_name(Wildcard, Paths),
   maplist( file_base_name,
      Paths, Files ).
unix_directory_contains_files(Directory, Files) :-
   concat([
      'ls ', Directory, ' > results/file_data' ],
      Command_ls ),
   unix(system(Command_ls)),
   unix(system('echo "eof" > results/eof')),
   concat([
      'cat results/file_data results/eof > ',
      'results/filenames'], Command_cat ),
   unix(system(Command_cat)),
   read_file_names('results/filenames', Files).


/* read_file_names(File, Filenames) <-
      */

read_file_names(File, Filenames) :-
   see(File), get0(C),
   read_file_names_loop(C, Filenames),
   seen.
   
read_file_names_loop(C1, All_Filenames) :-
   read_file_name(C1, Filename, C2),
   Filename \== eof,
   ( ( C2 \== 47,
       All_Filenames = [Filename|Filenames],
       C3 = C2 )
   ; ( get0(C3),
       All_Filenames = Filenames ) ),
   read_file_names_loop(C3, Filenames).
read_file_names_loop(_, []).

read_file_name(26, _, _) :-
   !, fail.
read_file_name(C, Filename, C2) :-
   in_file_name(C), !,
   get0(C1),
   rest_file_name(C1, Cs, C2),
   name(Filename, [C|Cs]).
read_file_name(_, Filename, C2) :-
   get0(C1),
   read_file_name(C1, Filename, C2).

rest_file_name(C, [C|Cs], C2) :-
   in_file_name(C), !,
   get0(C1),
   rest_file_name(C1, Cs, C2).
rest_file_name(C, [], C).

in_file_name(C) :-
   in_word(C,_).
in_file_name(46).
in_file_name(95).


/* read_csv_file_to_rows(Separator, File, Rows) <-
      */

read_csv_file_to_rows(Separator, File, Rows) :-
   read_file_to_csv_row_facts(Separator, File),
   findall( Row,
      retract(csv_row(_, Row)),
      Rows ).


/* read_file_to_csv_row_facts(Separator, File) <-
      */

read_file_to_csv_row_facts(File) :-
   read_file_to_csv_row_facts(";", File).

read_file_to_csv_row_facts(Separator, File) :-
   retract_all(csv_row(_, _)),
   open(File, read, Stream),
   read_file_to_csv_row_facts_loop(0, Separator, Stream),
   close(Stream),
   nl.

read_file_to_csv_row_facts_loop(N, Separator, Stream) :-
   read_line_to_atom(Stream, Name),
   ( Name \= '' ->
     name_split_at_position([Separator], Name, Tuple),
     assert(csv_row(N, Tuple)),
     ( 0 is N mod 10000 -> write('.'), ttyflush
     ; true ),
     M is N + 1
   ; M = N ),
   !,
   read_file_to_csv_row_facts_loop(M, Separator, Stream).
read_file_to_csv_row_facts_loop(N, _, _) :-
   write_list(user, ['\n', N, ' facts read: csv_row(N, Tuple)\n']).


/* prune_path_from_redundancy(Path_1, Path_2) <-
      */

prune_path_from_redundancy(Path_1, Path_2) :-
   name(Path_1, Path_1_S),
   delete(Path_1_S, 32, Path_2_S),
%  all_sublists_without_element(Path_2_S, 47, Sub_Strings),
   list_split_at_position(["/"], Path_2_S, Sub_Strings),
   reverse(Sub_Strings, Sub_Strings_Rev),
   delete_dots_and_upper_dir(
      Sub_Strings_Rev, New_Sub_Strings_Rev),
   reverse(New_Sub_Strings_Rev, New_Sub_Strings_1),
   reconstruct_path(New_Sub_Strings_1, New_Path),
   name(Path_2, New_Path).


delete_dots_and_upper_dir(String, New_String) :-
   delete_dots_and_upper_dir(String, n, New_String).

delete_dots_and_upper_dir([Str|Strs], n, New_Strs) :-
   Str = "..",
   delete_dots_and_upper_dir(Strs, d, New_Strs).
delete_dots_and_upper_dir([Str|Strs], n, [Str|New_Strs]) :-
   delete_dots_and_upper_dir(Strs, n, New_Strs).
delete_dots_and_upper_dir([_|Strs], d, New_Strs) :-
   delete_dots_and_upper_dir(Strs,n,New_Strs).
delete_dots_and_upper_dir([], n, []).


reconstruct_path(Substrings, Path_2) :-
  reconstruct_path(Substrings, [], Path_2).

reconstruct_path([Sub_Str|Sub_Strs], Sofar, Path) :-
   append([Sofar, Sub_Str, "/"], New_Sofar),
   reconstruct_path(Sub_Strs, New_Sofar, Path).
reconstruct_path([], Sofar, Sofar).


/* consult_if_exists(File) <-
      */

consult_if_exists(File) :-
   ( file_exists(File) ->
     [File]
   ; true ).


/* file_exists(File) <-
      Tests, if there is a file File.  */

file_exists(File) :-
   nofileerrors,
   absolute_file_name(File, Full_Name),
   file_exists(Full_Name, Success),
   fileerrors,
   !,
   Success = true.

file_exists(File, true) :-
   open(File, read, Stream),
   close(Stream),
   !.
file_exists(_, false).


/* directory_dos_to_unix(Extensions, Directory) <-
      */

directory_dos_to_unix(Extensions, Directory) :-
   directory_contains_files_absolute(Extensions, Directory, Files),
   checklist( file_dos_to_unix,
      Files ).


/* file_dos_to_unix(File_1, File_2) <-
      */

file_dos_to_unix(File) :-
   file_dos_to_unix(File, File).

file_dos_to_unix(File_1, File_2) :-
   read_file_to_codes(File_1, String_1),
   list_remove_elements("\r", String_1, String_2),
   name(Name_2, String_2),
   predicate_to_file( File_2,
      write(Name_2) ).


/* working_directory(X) <-
      */

working_directory(X) :-
   working_directory(X, X).

pwd_ :-
   working_directory(X),
   writeln(user, X).

ls_ :-
   directory_contains_files('.', Files),
   writeln_list_in_groups(4, Files).


/* writeln_list_in_groups(N, Xs) <-
      */

writeln_list_in_groups(_, []) :-
   !.
writeln_list_in_groups(N, Xs) :-
   first_n_elements(Xs, N, Ys, Zs),
   write_list_with_separators(write_tabulated(20), writeln, Ys),
   writeln_list_in_groups(N, Zs).

write_tabulated(N, Name) :-
   name_length(Name, M),
   write(Name),
   K is N-M,
   ( for(_, 1, K) do write(' ') ).


/* absolute_path(Pathname) <-
      */

absolute_path(Pathname) :-
   name(Pathname, String),
   ( String = [47|_]
   ; String = [_, 58|_] ).


/* make_directory_if_not_exists(Directory) <-
      */

make_directory_if_not_exists(Directory) :-
   ( not(exists_directory(Directory)) ->
     make_directory(Directory)
   ; true ).


/* make_directory_recursive(Directory) <-
      */

make_directory_recursive(Directory) :-
   path_to_sub_paths(Directory, Directories),
   checklist( make_directory_if_not_exists,
      Directories ),
   !.

path_to_sub_paths('/', []).
path_to_sub_paths(Directory, [Directory]) :-
   file_base_name(Directory, Directory).
path_to_sub_paths(Directory, Directories) :-
   file_directory_name(Directory, Directory_2),
   path_to_sub_paths(Directory_2, List),
   append(List, [Directory], Directories).


/******************************************************************/


