

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Vectors                                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* vector_scale(Scale, Vector_1, Vector_2) <-
      */

vector_scale(Scale, Vector_1, Vector_2) :-
   vector_length(Vector_1, Length),
   vector_multiply(Scale/Length, Vector_1, Vector_2).


/* vector_length(Vector, Length) <-
      */

vector_length(Vector, Length) :-
   scalar_product(Vector, Vector, X),
   Length is sqrt(X).


/* euklidian_distance(Vector_1, Vector_2, Distance) <-
      */

euklidian_distance(Vector_1, Vector_2, Distance) :-
   vector_subtract(Vector_1, Vector_2, Vector_3),
   scalar_product(Vector_3, Vector_3, Product),
   Distance is sqrt(Product).


/* scalar_product(Vector_1, Vector_2, Product) <-
      */

scalar_product(Vector_1, Vector_2, Product) :-
   scalar_product(Vector_1, Vector_2, _, Product).
 
scalar_product(Vector_1, Vector_2, Vector, Product) :-
   vectors_multiply(Vector_1, Vector_2, Vector),
   add(Vector, Product).

 
/* vector_multiply(X, Ys, Zs) <-
      */

vector_multiply(X, Ys, Zs) :-
   ( foreach(Y, Ys), foreach(Z, Zs) do
        Z is X * Y ).


/* vectors_multiply(Xs, Ys, Zs) <-
      */

vectors_multiply(Xs, Ys, Zs) :-
   ( foreach(X, Xs), foreach(Y, Ys), foreach(Z, Zs) do
        Z is X * Y ).


/* vectors_divide(Xs, Ys, Zs) <-
      */

vectors_divide(Xs, Ys, Zs) :-
   ( foreach(X, Xs), foreach(Y, Ys), foreach(Z, Zs) do
        Z is X / Y ).


/* vector_add(Xs, Ys, Zs) <-
      */

vector_add(Xs, Ys, Zs) :-
   ( foreach(X, Xs), foreach(Y, Ys), foreach(Z, Zs) do
        Z is X + Y ).


/* vector_subtract(Xs, Ys, Zs) <-
      */

vector_subtract(Xs, Ys, Zs) :-
   ( foreach(X, Xs), foreach(Y, Ys), foreach(Z, Zs) do
        Z is X - Y ).


/* multiply_list(Xs, Y, Zs) <-
      */

multiply_list([--|Xs], Y, [--|Zs]) :-
   !,
   multiply_list(Xs, Y, Zs).
multiply_list([X|Xs], Y, [Z|Zs]) :-
   !,
   Z is X * Y,
   multiply_list(Xs, Y, Zs).
multiply_list(_, _, []).                 


/* divide_list(Xs, Y, Zs) <-
      */

divide_list(Xs, Y, Zs) :-
   D is 1 / Y,
   multiply_list(Xs, D, Zs).

 
/******************************************************************/


