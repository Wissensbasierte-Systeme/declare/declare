

/******************************************************************/
/***                                                            ***/
/***         DisLog:  Handling of 'current_num'                 ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      current_num/2.


/*** interface ****************************************************/


/* get_num(Root, Num) <-
      */

get_num(Root, Num) :-
   retract(current_num(Root, Num1)),
   !,
   Num is Num1 + 1,
   asserta(current_num(Root, Num)).
get_num(Root, 1) :-
   asserta(current_num(Root, 1)).


/* set_num(Root, Num) <-
      */

set_num(Root, Num) :-
   retract(current_num(Root, _)),
   !,
   asserta(current_num(Root, Num)).
set_num(Root, Num) :-
   asserta(current_num(Root, Num)).


/* add_num(Root, Num) <-
      */

add_num(Root, Num) :-
   retract(current_num(Root, Num1)),
   !,
   Num2 is Num + Num1,
   asserta(current_num(Root, Num2)).
add_num(Root, Num) :-
   asserta(current_num(Root, Num)).


/* close_num(Root, Num) <-
      */

close_num(Root) :-
   close_num(Root, _).

close_num(Root, Num) :-
   retract(current_num(Root, Num)),
   !.
close_num(_, 0).


/* max_num(Root, Num) <-
      */

max_num(Root, Num) :-
   retract(current_num(Root, Num1)),
   !,
   maximum(Num, Num1, Num2),
   asserta(current_num(Root, Num2)).
max_num(Root, Num) :-
   asserta(current_num(Root, Num)).

 
/******************************************************************/


