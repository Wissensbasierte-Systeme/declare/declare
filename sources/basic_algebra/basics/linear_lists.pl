

/******************************************************************/
/***                                                            ***/
/***        DisLog:  Operations for Linear Lists                ***/
/***                                                            ***/
/******************************************************************/


/* The linear list is given by the DisLog-variable 'Graph',
   i.e. by an asserted atom
      dislog_variable('Graph',Nodes-Edges).
   The number N of its actual element is given by the
   DisLog-variable 'Actual', i.e. by an asserted atom
      dislog_variable('Actual',N). */


/* linear_list_delete <-
      */

linear_list_delete(N) :-
   dislog_variable_get('Graph',Graph_1),
   linear_list_delete(Graph_1,N-M,Graph_2),
   dislog_variable_set('Actual',M),
   dislog_variable_set('Graph',Graph_2),
   !.

linear_list_delete(Ns1-Es1,Node-Actual,Ns2-Es2) :-
   linear_list_trans(Ns1-Es1,Node,Es3),
   delete(Ns1,Node,Ns2),
   ( member(M-Node,Es1)
   ; member(Node-K,Es1)
   ; true ),
   subtract(Es1,[M-Node,Node-K],Es4),
   ( ( nonvar(K), Actual = K )
   ; ( nonvar(M), Actual = M )
   ; ( Actual = 0 ) ),
   append(Es3,Es4,Es2).

linear_list_trans(_-Es,N,[M-K]) :-
   member(M-N,Es),
   member(N-K,Es),
   !.
linear_list_trans(_,_,[]).


/* linear_list_insert <-
      */

linear_list_insert(N) :-
   dislog_variable_get('Actual',M),
   dislog_variable_get('Graph',Graph_1),
   linear_list_insert(Graph_1,M-N,Graph_2),
   dislog_variable_set('Graph',Graph_2).

linear_list_insert(Ns1-Es1,Actual-New,Ns2-Es2) :-
   linear_list_insert_change_edges(Ns1-Es1,Actual-New,Es3-Es4),
   Ns2 = [New|Ns1],
   append(Es3,Es1,Es5),
   subtract(Es5,Es4,Es2).

linear_list_insert_change_edges([]-[],_-_,[]-[]) :-
   !.
linear_list_insert_change_edges(_-Edges,M-N,[M-N,N-K]-[M-K]) :-
   member(M-K,Edges),
   !.
linear_list_insert_change_edges(_-_,M-N,[M-N]-[]).


/* linear_list_find(Mode,N,M) <-
      */

linear_list_find(next,N,M) :-
   linear_list_next(N,M).

linear_list_find(previous,N,M) :-
   linear_list_next(M,N).

linear_list_find(goto,_,M) :-
   xpce_formular_get('Number',M).

linear_list_find(last,N,M) :-
   linear_list_next(N,K),
   !,
   linear_list_find(last,K,M).
linear_list_find(last,N,N).

linear_list_find(first,N,M) :-
   linear_list_next(K,N),
   !,
   linear_list_find(first,K,M).
linear_list_find(first,N,N).


/* linear_list_next(N,M) <-
      */

linear_list_next(N,M) :-
   dislog_variable_get('Graph',_-Edges),
   member(N-M,Edges).


/******************************************************************/


