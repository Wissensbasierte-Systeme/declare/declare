

/******************************************************************/
/***                                                            ***/
/***         DisLog:  Substitutions                             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* substitute_tree(Tree1,Substitution,Tree2) <-
      appliying the substitution Substitution to the clause 
      tree Tree1 yields the clause tree Tree2. */

substitute_tree([Node1|Trees1],S,[Node2|Trees2]) :-
   substitute_clause(Node1,S,Node2),
   substitute_trees(Trees1,S,Trees2).
substitute_tree([],_,[]).
   
substitute_trees([T1|Ts1],S,[T2|Ts2]) :-
   substitute_tree(T1,S,T2),
   substitute_trees(Ts1,S,Ts2).
substitute_trees([],_,[]).


/* substitute_state(State1,Substitution,State2) <-
      appliying the substitution Substitution to the state 
      State1 yields the state State2. */

substitute_state(State1,Substitution,State2) :-
   substitute_state_loop(State1,Substitution,State),
   list_to_ord_set(State,State2).

substitute_state_loop([C1|Cs1],S,[C2|Cs2]) :-
   substitute_clause(C1,S,C2), 
   substitute_state_loop(Cs1,S,Cs2). 
substitute_state_loop([],_,[]).


/* substitute_clause(Clause1,Substitution,Clause2) <-
      appliying the substitution Substitution to the clause 
      Clause1 yields the clause Clause2. */

substitute_clause(Clause1,Substitution,Clause2) :-
   substitute_clause_loop(Clause1,Substitution,Clause),
   list_to_ord_set(Clause,Clause2).

substitute_clause_loop([A|As],S,[B|Bs]) :-
   A =.. [P|Tuple1],
   substitute_tuple(Tuple1,S,Tuple2),
   B =.. [P|Tuple2],
   substitute_clause_loop(As,S,Bs).
substitute_clause_loop([],_,[]).


/* substitute(Atoms1,Mgu,Atoms2) <-
      The substitution Mgu is applied to the list Atoms1
      of atoms, leading to the new list Atoms2. */

substitute([A1|As1],Mgu,[A2|As2]) :-
   A1 =.. [P|Args1],
   substitute_tuple(Args1,Mgu,Args2),
   A2 =.. [P|Args2],
   substitute(As1,Mgu,As2).
substitute([],_,[]).


/* substitute_tuple(Tuple1,Substitution,Tuple2) <-
      Tuple2 is derived from Tuple1 by replacing all components 
      X of Tuple1 such that there is a pair [X,Y] in
      Substitution by Y. */

substitute_tuple([X|Xs],S,[Y|Ys]) :-
   member([X,Y],S),
   !,
   substitute_tuple(Xs,S,Ys).
substitute_tuple([X|Xs],S,[X|Ys]) :-
   atomic(X),
   !,
   substitute_tuple(Xs,S,Ys).
substitute_tuple([X|Xs],S,[Y|Ys]) :-
   substitute([X],S,[Y]),
   substitute_tuple(Xs,S,Ys).
substitute_tuple([],_,[]).


/* unify(List1,List2,Mgu) <-
      Given two element lists List1 and List2 of the same
      length, the mgu Mgu of the lists is computed */

unify([E|Es1],[E|Es2],Mgu) :-
   !,
   unify(Es1,Es2,Mgu).
unify([E1|Es1],[E2|Es2],[[E2,E1]|Mgu]) :-
   unify(Es1,Es2,Mgu).
unify([],[],[]).


/* ground_instantiate_clause(Atoms,Clause) <-
      generates ground instances of the clause Clause,
      if Clause is ground already, then the predicate succeeds,
      else each atom of Clause is unified with some atom
      from Atoms. */

ground_instantiate_clause(_,Clause) :-
   ground(Clause),
   !.
ground_instantiate_clause(Atoms,[~A|As]) :-
   !,
   member(A,Atoms),
   ground_instantiate_clause(Atoms,As).
ground_instantiate_clause(Atoms,[A|As]) :-
   member(A,Atoms),
   ground_instantiate_clause(Atoms,As).
ground_instantiate_clause(_,[]).

ground_instantiate_clause(Clause) :-
   herbrand_base(Herbrand_Base),
   ground_instantiate_clause(Herbrand_Base,Clause).


/* listvars(VarName,N,N1) <-
      Assigns names XN, ..., XM, where M = N1-1,
      to all variables in VarName,
      used for pretty-printing of variables. */
 
listvars(VarName,N) :-
   listvars(VarName,N,_).
 
listvars(VarName,N,N1) :-
   var(VarName),
   !,
   N1 is N + 1,
   name_append('X',N,VarName).
listvars(Term,N1,N2) :-
   nonvar(Term),
   !,
   functor(Term,_,N),
   listvars(0,N,Term,N1,N2).
listvars(N,N,_,N1,N1):-
   !.
listvars(I,N,Term,N1,N3) :-
   I < N,
   !,
   I1 is I + 1,
   arg(I1,Term,Arg),
   listvars(Arg,N1,N2),
   listvars(I1,N,Term,N2,N3).


/******************************************************************/


