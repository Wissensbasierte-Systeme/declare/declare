

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Multisets                                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* multiset_union(M1, M2, M3) <-
      */

multiset_union(M1, M2, M3) :-
   findall( Element,
      ( member(Element:_, M1)
      ; member(Element:_, M2) ),
      List ),
   list_to_ord_set(List, Set),
   findall( Element:Multiplicity,
      ( member(Element, Set),
        multiplicity(Element, M1, N1),
        multiplicity(Element, M2, N2),
        Multiplicity is N1 + N2 ),
      M3 ).


/* multiset_normalize(M1, M2) <-
      */

multiset_normalize(Pairs, Multiset) :-
   findall( X:M,
      ( bagof( N, member(X:N, Pairs), Ns ),
        sum(Ns, M) ),
      Multiset ).

/*
multiset_normalize(M1, M2) :-
   sort(M1, M),
   multiset_normalize_(M, M2).
*/

multiset_normalize_([X:N|Pairs], M) :-
   multiset_normalize_(X:N, Pairs, M).
multiset_normalize_([], []).

multiset_normalize_(X:N1, [X:N2|Pairs], M) :-
   !,
   N is N1 + N2,
   multiset_normalize_(X:N, Pairs, M).
multiset_normalize_(X:N, Pairs, [X:N|M]) :-
   multiset_normalize_(Pairs, M).


multiset_normalize_2(M1, M2) :-
   multiset_to_elements(M1, Elements),
   maplist( element_to_element_with_multiplicity(M1),
      Elements, M2 ).

multiset_to_elements(Multiset, Elements) :-
   findall( Element,
      member(Element:_, Multiset),
      Elements_2 ),
   list_to_ord_set(Elements_2, Elements).

element_to_element_with_multiplicity(
      Multiset, Element, Element:Multiplicity) :-
   findall( N,
      member(Element:N, Multiset),
      Ns ),
   add(Ns, Multiplicity).


/* multiplicity(Element, Multiset, Multiplicity) <-
      */

multiplicity(Element, Multiset, Multiplicity) :-
   member(Element:Multiplicity, Multiset),
   !.
multiplicity(_, _, 0).


/* list_to_multiset(List, Multiset) <-
      */

list_to_multiset(List, Multiset) :-
   findall( X:M,
      ( bagof(_, member(X, List), Ys),
        length(Ys, M) ),
      Multiset ).

/*
list_to_multiset(List, Multiset) :-
   list_to_ord_set(List, Set),
   findall( Element:Multiplicity,
      ( member(Element, Set),
        findall( Element,
           member(Element, List),
           Elements ),
        length(Elements, Multiplicity) ),
      Multiset ).
*/


/******************************************************************/


