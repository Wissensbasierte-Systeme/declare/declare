


/******************************************************************/
/***                                                            ***/
/***                DisLog:  SLI - Resolution                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


sli_refusable(L) :-
   findall( Node,
      sli_refutation(Node,[[],[]],[[],[]]),
      L ).
 
sli_refutation(Node) :-
   sli_refutation(Node,[[],[]],[[],[]]).
 
compute_all_sn(All_SN) :-
   findall( [[X],Head],
      sli_reduceable([[],[X]],[],Head,[[],[]],[[],[]]),
      All_SN ).
 
compute_sn(X,SN) :-
   findall( Head,
      sli_reduceable([[],[X]],[],Head,[[],[]],[[],[]]),
      SN ).


/*** implementation ***********************************************/


sli_clause([Head,[]]) :-
   d_fact(Head).
sli_clause([Head,Body]) :-
   d_clause(Head,Body).


/* sli_refutation(Node,Gamma,Delta) <-
      execution of the SLI-refutation with top-clause 'Node'
      if there exists such a SLI-refutation for top-clause 'Node'
      the execution succeeds, otherwise it fails. */
      
sli_refutation(Node,Gamma,Delta) :-
    ( sli_member_p(Node,Gamma)
    ; sli_member_n(Node,Delta) ),
    !.
sli_refutation(Node,Gamma,Delta) :-
    ( sli_variant_p(Node,Delta)
    ; sli_variant_n(Node,Gamma) ), 
    !,
    fail.
sli_refutation(Node,Gamma,Delta) :-
   sli_clause(Clause),
   sli_resolve(Node,Clause,Clause1),
   sli_loop(Node,Clause1,Gamma,Delta).

sli_loop(_,[[],[]],_,_).
sli_loop(Node,[[],[C|T2]],Gamma,Delta) :-
   sli_union(Gamma,[[],T2],Gamma1),
   sli_union(Delta,Node,Delta1),
   sli_refutation([[],[C]],Gamma1,Delta1), 
   sli_loop(Node,[[],T2],Gamma,Delta).
sli_loop(Node,[[C|T1],T2],Gamma,Delta) :-
   sli_union(Gamma,[T1,T2],Gamma1),
   sli_union(Delta,Node,Delta1),
   sli_refutation([[C],[]],Gamma1,Delta1),
   sli_loop(Node,[T1,T2],Gamma,Delta).


/* sli_reduceable(Node,L1,L2,Gamma,Delta) <- 
      execution of a SLINF-derivation with top-clause 'Node'
      (only negative unmarked literals will be resolved),
      L1 is the postive unmarked clause bevor the derivation 
      and L2 is the positive unmarked clause after the derivation
      the derivation stops, when there is no more negative unmarked
      literal to resolve */


sli_reduceable(Node,L,L,Gamma,Delta) :-
    ( sli_member_p(Node,Gamma)
    ; slinf_member_n(Node,Delta) ),
    !.
sli_reduceable(Node,L,L,Gamma,Delta) :-
    ( sli_variant_p(Node,Delta)
    ; sli_variant_n(Node,Gamma) ), 
    !,
    fail.
sli_reduceable(Node,Ac_SN,Fi_SN,Gamma,Delta) :-
   sli_clause(Clause),
   slinf_resolve(Node,Clause,Clause1),
   slinf_union(Ac_SN,Clause1,Ac_SN1),
   slinf_loop(Node,Ac_SN1,Fi_SN,Clause1,Gamma,Delta).

slinf_loop(_,A,B,[P,[]],_,_) :-
   union(A,P,B).
slinf_loop(Node,Ac_SN,Fi_SN,[P,[C|T2]],Gamma,Delta) :-
   sli_union(Gamma,[P,T2],Gamma1),
   sli_union(Delta,Node,Delta1),
   sli_reduceable([[],[C]],Ac_SN,Ac_SN1,Gamma1,Delta1),
   slinf_loop(Node,Ac_SN1,Fi_SN,[P,T2],Gamma,Delta).


/* help-predicates for sli- and slinf-resolution */

sli_member_p([[X],[]],[[X|_],_]).
sli_member_p([[X],[]],[[_|Y],_]) :- 
   member(X,Y).
sli_member_p([[],[X]],[_,[X|_]]).
sli_member_p([[],[X]],[_,[_|Y]]) :- 
   member(X,Y).

sli_member_n([[X],[]],[_,[X|_]]).
sli_member_n([[X],[]],[_,[_|Y]]) :-
   member(X,Y).
sli_member_n([[],[X]],[[X|_],_]).
sli_member_n([[],[X]],[[_|Y],_]) :-
   member(X,Y).

slinf_member_n([[],[X]],[[X|_],_]).
slinf_member_n([[],[X]],[[_|Y],_]) :-
   member(X,Y).

sli_resolve([[X],[]],[H,B1],[H,B2]) :-
   resolve(X,B1,B2).
sli_resolve([[],[X]],[H1,B],[H2,B]) :-
   resolve(X,H1,H2).

slinf_resolve([[],[X]],[H1,B],[H2,B]) :-
   resolve(X,H1,H2).

sli_union([X1,X2],[Y1,Y2],[Z1,Z2]) :-
   ord_union(X1,Y1,Z1),
   ord_union(X2,Y2,Z2).

slinf_union(X1,[Y1,_],Z1) :-
   ord_union(X1,Y1,Z1).

sli_variant_p(_,[[],_]) :- fail.
sli_variant_p([[Atom],[]],[[Head|_],_]) :-
   variant(Atom,Head),
   !.
sli_variant_p([[Atom],[]],[[_|Tail],_]) :-
   sli_variant_p([[Atom],[]],[Tail,_]).

sli_variant_p(_,[_,[]]) :- fail.
sli_variant_p([[],[Atom]],[_,[Head|_]]) :-
   variant(Atom,Head),
   !.
sli_variant_p([[],[Atom]],[_,[_|Tail]]) :-
   sli_variant_p([[],[Atom]],[_,Tail]).

sli_variant_n(_,[_,[]]) :- fail.
sli_variant_n([[Atom],[]],[_,[Head|_]]) :-
   variant(Atom,Head),
   !.
sli_variant_n([[Atom],[]],[_,[_|Tail]]) :-
   sli_variant_n([[Atom],[]],[_,Tail]).

sli_variant_n(_,[[],_]) :- fail.
sli_variant_n([[],[Atom]],[[Head|_],_]) :-
   variant(Atom,Head),
   !.
sli_variant_n([[],[Atom]],[[_|Tail],_]) :-
   sli_variant_n([[],[Atom]],[Tail,_]).

/* variant(Atom1,Atom2) <-
       tests, if Atom1 and Atom2 are variants */

variant3(Atom1,Atom2) :-
   numbervars(Atom1,0,N),
   numbervars(Atom2,0,N),
   Atom1 == Atom2.

variant2(Atom1,Atom2) :-
   write('Variant ?'), write(Atom1), writeln(Atom2),
   Atom1 =.. [P|Arguments1], Atom2 =.. [P|Arguments2],
   extract_variables(Arguments1,List1),
   extract_variables(Arguments2,List2),
   list_to_ord_set(List1,OSet1), length(OSet1,N1),
   list_to_ord_set(List2,OSet2), length(OSet2,N2),
   Atom1 = Atom2,
   list_to_ord_set(OSet1,OSet3), length(OSet3,N1),
   list_to_ord_set(OSet2,OSet4), length(OSet4,N2),
   all_var(OSet3), all_var(OSet4),
   !.

extract_variables([H|T],[H|List]) :-
   var(H),
   !,
   extract_variables(T,List).
extract_variables([H|T],List) :-
   H =.. [_|H_Term],
   extract_variables(H_Term,H_List),
   extract_variables(T,T_List),
   append(H_List,T_List,List).
extract_variables([],[]).
 
all_var([]).
all_var([H|T]) :-
   var(H),
   all_var(T).


/******************************************************************/


