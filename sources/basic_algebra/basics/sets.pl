

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Sets                                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* subset_non_ground(Xs, Ys) <-
      */

subset_non_ground(Xs, Ys) :-
   members(Xs, Ys).


/* members(Xs, Ys) <-
      */

members([X|Xs], Ys) :-
   member(X, Ys),
   members(Xs, Ys).
members([], _).


/* subset_all_different(Xs, Ys) <-
      */

subset_all_different([X|Xs], Ys) :-
   append(Ys_1, [X|Ys_2], Ys),
   append(Ys_1, Ys_2, Ys_3),
   subset_all_different(Xs, Ys_3).
subset_all_different([], _).


/* subsets_with_cardinality(Ordset,N,Ordsets) <-
      */

subsets_with_cardinality(_,0,[[]]) :-
   !.
subsets_with_cardinality(Xs,N,[]) :-
   length(Xs,L),
   L < N,
   !.
subsets_with_cardinality([X|Xs],N,Ss) :-
   subsets_with_cardinality(Xs,N,Ss1),
   M is N - 1,
   subsets_with_cardinality(Xs,M,Ss2),
   maplist( append([X]),
      Ss2, Ss3 ),
   ord_union(Ss1,Ss3,Ss4),
   maplist( list_to_ord_set,
      Ss4, Ss ).


/* subset_not_eq(X,Y) <-
      */

subset_not_eq(X,Y) :-
   subset(X,Y), \+ subset(Y,X).
%  sublist(X,Y), \+ permutation(Y,X).
%  sublist(X,Y), \+ sublist(Y,X).


/* setequal(X,Y) <-
      */

setequal(X,Y) :-
   subset(X,Y),
   subset(Y,X).


/* setdifference(Xs,Ys,Zs) <-
      */

setdifference([],_,[]) :-
   !.
setdifference([X|Xs],Ys,Zs) :-
   element(X,Ys),
   !,
   setdifference(Xs,Ys,Zs).
setdifference([X|Xs],Ys,[X|Zs]) :-
   setdifference(Xs,Ys,Zs).

setdifference_non_ground([],_,[]) :-
   !.
setdifference_non_ground([X|Xs],Ys,Zs) :-
   element(X,Ys),
   setdifference_non_ground(Xs,Ys,Zs).
setdifference_non_ground([X|Xs],Ys,[X|Zs]) :-
   setdifference_non_ground(Xs,Ys,Zs).

setdifference_2(Xs,[Y|Ys],Zs) :-
   delete(Xs,Y,Us),
   setdifference_2(Us,Ys,Zs).
setdifference_2(Xs,[],Xs).


/* setintersection(Xs,Y) <-
      */

setintersection([X|Xs],Y) :-
   setintersection(Xs,Z),
   intersection(X,Z,Y).
setintersection([X],X).


/* setintersection(Xs,Ys,Zs) <-
      */

setintersection([X|Xs],Ys,[X|Zs]) :-
   element(X,Ys),
   !,
   setintersection(Xs,Ys,Zs).
setintersection([_|Xs],Ys,Zs) :-
   setintersection(Xs,Ys,Zs).
setintersection([],_,[]).


/* intersects(Xs,Ys) <-
      */

intersects([X|_],Ys) :-
   element(X,Ys),
   !.
intersects([_|Xs],Ys) :-
   intersects(Xs,Ys).


/* n_intersects(N,Xs,Ys) <-
      */

n_intersects(0,_,_).
n_intersects(N,[X|Xs],Ys) :-
   element(X,Ys),
   !,
   M is N - 1,
   n_intersects(M,Xs,Ys).
n_intersects(N,[_|Xs],Ys) :-
   n_intersects(N,Xs,Ys).


/*
list_to_set([],[]).
list_to_set([X|Xs],Set) :-
   element(X,Xs),
   !,
   list_to_set(Xs,Set).
list_to_set([X|Xs],[X|Set]) :-
   list_to_set(Xs,Set).
*/


/* remove_empty_sets(Sets_1, Sets_2) <-
      */

remove_empty_sets(Sets_1, Sets_2) :-
   findall( Set,
      ( member(Set, Sets_1),
        Set \= [] ),
      Sets_2 ).


/******************************************************************/


