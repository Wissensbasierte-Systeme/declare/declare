

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Sources                                 ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      dislog_source/1.

:- dynamic
      dislog_source/1.

:- dynamic
      dislog_module_block/1.


/*** interface ****************************************************/


/* dislog_sources <-
      */

dislog_sources :-
   dislog_sources(Xs),
   length(Xs, N),
   writeln(Xs), write(N), writeln(' files').

dislog_sources(Xs) :-
   findall( X,
      dislog_source(X),
      Xs ).


/* dislog_source(X) <-
      */

dislog_source(X) :-
   dislog_unit_to_source(_, X).


/* dislog_unit_to_sources(Unit, Sources) <-
      */

dislog_unit_to_sources(Unit, Sources) :-
   findall( X,
      dislog_unit_to_source(Unit, X),
      Sources ).


/* dislog_unit_to_source(Unit, X) <-
      */

dislog_unit_to_source(Unit, X) :-
   dislog_unit(Unit, Modules),
   member(Module, Modules),
   dislog_module(Module, Files),
   member(File, Files),
   concat([Unit, '/', Module, '/', File], X).


/* dislog_modules <-
      */

dislog_modules(DisLog_Modules) :-
   findall( Module,
      dislog_module(_, Module, _),
      DisLog_Modules ).

dislog_modules :-
   dislog_modules(DisLog_Modules),
   writeln(DisLog_Modules),
   length(DisLog_Modules, N),
   write(N), writeln(' modules').


/*** implementation ***********************************************/


/* dislog_module(Module, Module_Path, Files) <-
      */

dislog_module(Module, Module_Path, Files) :-
   dislog_module(Module, Files_in_Module),
   dislog_module_sub(Module, Module_Path, Files_in_Module, Files).

dislog_module_sub(Module, Module_Path, Files_in_Module, Files) :-
   dislog_module_to_unit(Module, Unit),
%  \+ dislog_block(Unit, _, _),
   concat([Unit, '/', Module, '/'], Module_Path),
   \+ ( dislog_module_block(Module_Path_),
        concat(Module_Path_, '/', Module_Path) ),
   maplist( concat(Module_Path),
      Files_in_Module, Files ).

dislog_module_description(Module, Module_Path, Files) :-
   findall( File,
      ( dislog_module(Module, Files_in_Module),
        member(File, Files_in_Module)
      ; dislog_module_description(Module, Files_in_Module),
        member(File, Files_in_Module) ),
      Fs ),
   dislog_module_sub(Module, Module_Path, Fs, Files).


/* dislog_module_to_unit(Module, Unit) <-
      */

dislog_module_to_unit(Module, Unit) :-
   dislog_unit(Unit, Modules),
   member(Module, Modules).


/* xdislog_sources(Sources) <-
      */

xdislog_sources(Sources) :-
   Sources = [cffp, xdislog].


/******************************************************************/


