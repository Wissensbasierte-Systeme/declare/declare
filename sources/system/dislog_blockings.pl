

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Blocked Sources                         ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      dislog_block/0,
      dislog_block/1,
      dislog_block/2,
      dislog_block/3.

% :- 'sources/basic_algebra/basics/names.pl'.
% :- 'sources/basic_algebra/basics/lists.pl'.


/*** interface ****************************************************/


/* dislog_unit_to_sources_non_blocked(Unit, Files) <-
      */

dislog_unit_to_sources_non_blocked(Unit, Files) :-
   dislog_unit_to_sources(Unit, Xs),
   findall( X,
      ( member(X, Xs),
        \+ dislog_source_block(X) ),
      Files ).


/* dislog_sources_non_blocked(Files) <-
      */

dislog_sources_non_blocked(Files) :-
   dislog_sources(Xs),
   findall( X,
      ( member(X, Xs),
        \+ dislog_source_block(X) ),
      Files ).


/* dislog_source_block(Path) <-
      */

dislog_source_block(Path) :-
   name_split_at_position(["/"], Path, As),
   Block =.. [dislog_block|As],
   call(Block).


/* dislog_block(Unit, Module, File) <-
      */

dislog_block(artificial_intelligence, _, _).
dislog_block(biology_and_medicine, _, _).
dislog_block(linguistics, _, _).
dislog_block(projects, _, _).
dislog_block(source_code_analysis, _, _).
dislog_block(stock_tool, _, _).

dislog_block(applications, pecap, _).
dislog_block(databases, ddbase_gui, _).
dislog_block(databases, pdiax, _).
dislog_block(databases, probabilistic_databases, _).

dislog_block(xml,
   fn_query, fn_query_gui).
dislog_block(software_engineering,
   refactoring_browser, refactoring_browser_classes).
dislog_block(software_engineering,
   developer_advisor, dislog_help).

% 'applications/pecap/root_cause_analsysis_pn/stablenet_rules.pl' ?


/******************************************************************/


