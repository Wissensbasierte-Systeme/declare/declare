

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Units                                   ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      dislog_unit/2,
      dislog_module/2,
      dislog_module/3,
      dislog_module_description/2.

:- multifile
      dislog_unit/2,
      dislog_module/2.


/*** DisLog Units *************************************************/


/* dislog_unit(Unit, Modules) <-
      */

dislog_unit( basic_algebra, [
   basics,
   graphs, set_trees,
   graphs_gxl, graphs_xpce,
   programs_dislog, programs_prolog,
   states_and_coins,
   utilities_xpce, utilities,
   grammars, logics ] ).

dislog_unit( xml, [
   xml_pillow,
   fn_query, fn_case_studies,
   gn_query,
   prolog_in_xml,
   trees_in_xml,
   xslt, xul, xxul ] ).

dislog_unit( reasoning, [
   nmr_interfaces,
   disjunctive_consequence_operators, tp_operators,
   closed_world_assumptions,
%  cd_databases,
   mm_satchmo,
   model_based_semantics,
   possible_model_semantics,
   well_founded_semantics ] ).

dislog_unit( databases, [
   oracle_interface,
   mysql_interface, mysql_to_prolog,
   mongodb_interface,
   database_schema_design,
   ddbase, ddbase_gui,
   rule_analysis,
   time_series,
   query_processing, linear_sirups,
   pdiax, probabilistic_databases,
%  database_schema_analysis,
   data_mining,
   squash ] ).

dislog_unit( software_engineering, [
   management,
   developer_advisor, documentation,
   refactoring, refactoring_qualimed,
   refactoring_browser,
   program_analysis, program_analysis_rar,
   jaml, pljava, cobol_parser,
   diverse ] ).

dislog_unit( artificial_intelligence, [
   ontologies, ontologies_swrl,
   triple,
   d3 ] ).

dislog_unit( stock_tool, [
   stock_administration,
   stock_knowledge_base,
   stock_states,
   stock_gui,
   stock_hierarchy,
   stock_charts,
   stock_pattern_search,
   stock_spooler,
   stock_mining,
   energy, energy_reasoning ] ).

dislog_unit( biology_and_medicine, [
   qualimed, dentalog,
   genetics,
   protein_states, protein_pathways,
%  drug_design, keggpad,
   platelet_kinase_network ] ).

dislog_unit( linguistics, [
   language_processing,
   jean_paul,
   adelung, adelung_parsing, adelung_word_lists,
   wdg, campe,
   morpheme_annotation, morpheme_annotation_xxul,
   morpheme_decomposition,
   alignment, alignment_toolkit ] ).

dislog_unit( applications, [
   selli, tennis, evu,
   small_applications,
   cardinality_constraints, 
   data_structures_and_algorithms,
   ddb_case_study,
   conferences, teaching,
   travelling, sudoku,
   pictures,
%  pecap,
   messages,
   music_xml_analysis,
   link_prediction ] ).


/*** DisLog Modules ***********************************************/


/* dislog_module(Module, Files) <-
      */

% basic_algebra

dislog_module( basics, [
   dislog_variables,
   operators, meta_predicates, functions,
   clause_database, prolog_database, modules,
   substitutions, subsumes, sli_resolve,
   current_num, global_variables,
   elementary, algebraic, date,
   arithmetic, complex_numbers, dual_numbers,
   prime_factors, fraction_simplify,
   lists,
   pair_lists,
   linear_lists, sets, multisets,
   names,
   vectors, matrices, tables,
   pretty_printer, interface,
   input_output, file_handling, email_handling, sort,
   diverse ] ).

dislog_module( programs_dislog, [
   program_operations,
   transformations,
   tu_annotation, cd_transformations,
   stratification, semi_stratification,
   stratification_special, all_stratifications,
   simplification, folding,
   completion,
   range_restriction ] ).

dislog_module( programs_prolog, [
   elementary,
   stratification_of_prolog,
   dependency_graphs,
   prolog_to_html ] ).

dislog_module( graphs, [
   reachability, tree_traversals,
   dijkstra, dijkstra_barker, dijkstra_edges_barker,
   dependency_graphs, rule_goal_graphs,
   hypergraphs,
   hypergraphs_trans, hypergraph_transversals,
   heaps,
   steiner_trees, min_span_tree,
   depth_first_search, ugraphs_dislog,
   edge_display ] ).

dislog_module( set_trees, [
   clause_trees, model_trees, tree_resolve,
   set_trees,
   tree_tests ] ).

dislog_module( graphs_gxl, [
   gxl_graphs, gxl_to_picture,
   gxl_tree_layout,
   gxl_layout_variable,
   graphs_to_picture, graphs_to_xpce ] ).

dislog_module( graphs_xpce, [
   rule_goal_graphs,
   rule_goal_graphs_to_xpce,
   set_trees_to_xpce ] ).

dislog_module( states_and_coins, [
   herbrand_base, herbrand_interpretations,
   states, states_to_prolog,
   coins, dualization ] ).

dislog_module( utilities_xpce, [
   desktop, elementary,
   file_viewer, editor,
   xpce_interface, xpce_dialogs, status_bar,
   show_html,
   tables, database_tables,
   tables_for_tests, tables_html, cms, tables_html_cms_doc,
   bar_charts, plotter,
   terms_xpce ] ).

dislog_module( utilities, [
   time_statistics, state_statistics,
   test_tool, test_predicates, test_hierarchy,
   tracing, type_check,
   mutex, thread_tests,
%  compile_dislog,
   comment_search,
   code_analysis, code_analysis_xpce, code_analysis_html,
   unix_move_commands, word_count, www,
   star_office,
   latex, latex_index, latex_pictures,
   graphviz ] ).

dislog_module( grammars, [
   edcgs, edcgs_sequence ] ).

dislog_module( logics, [
   boolean_normal_forms,
   evaluate_boolean_formula,
   minimal_models ] ).


% xml

dislog_module( fn_query, [
   fn_query_gui,
   fn_query, fn_path, fn_list_select, fn_select,
   fn_update, fn_update_plus, fn_update_minus,
   fn_files_compare,
   fn_transform, fn_transform_item,
   fn_algebra, fn_operations,
   fn_input_output,
   fn_extract, fn_schema,
   fn_mathml, fn_to_gn_database,
   pl4xml,
   xqueryx, xquery,
%  xqueryx_plcafe,
   xml_pillow_to_fn,
   html_file_prune_lists ] ).

dislog_module( fn_case_studies, [
   elementary,
   biology, che, cms,
   mathml, party, stock,
   db_world ] ).

dislog_module( gn_query, [
   fn_to_gn,
   gn_path_parser,
   gn_to_fn,
   xp_query,
   gn_case_study,
   gn_query,
   gn_vs_fn_tests ] ).

dislog_module(prolog_in_xml, [
   prolog_to_xml,
   xml_to_prolog,
   prolog_and_xml ] ).

dislog_module(trees_in_xml, [
   xml_to_picture,
   xml_schema_graphs_to_picture,
   xml_proof_trees ] ).

dislog_module( xslt, [
   xslt_to_visur ] ).

dislog_module( xul, [
   xul_loader,
   xul_forms, xul_form_to_fn_item, xul_update,
   xul_to_database_schema, xul_transform,
   xul_database_updates,
   container_to_database, database_to_container,
   xul_selli ] ).

dislog_module( xxul, [
   xxul ] ).

dislog_module( xml_pillow, [
   ascii_text, hash_table,
   xml_pillow_test,
   xml_pillow, xml_pillow_alternative,
   xml_pillow_data_operation,
   xml_pillow_examples,
   xml_pillow_field_notation ,
   xml_pillow_navigate, xml_pillow_print,
   xml_pillow_query, xml_pillow_transform,
   xml_pillow_type_check,
   xml_pillow_www_support ] ).


% reasoning
 
dislog_module( nmr_interfaces, [
   load, demo_dislog, menues,
%  tcl_tk_interface,
   file_interface,
   ufi_interface, www_interface, pretty_print,
   dlv_interface, smodels_interface,
   evaluation_semi_stratified,
   nmr_tests ] ).

dislog_module( disjunctive_consequence_operators, [
   tps_fixpoint, tps_delta,
%  tps_fixpoint_cc, 
   tpi_fixpoint, tps_tpi_iterations, tps_tpi_tests ] ).

dislog_module( tp_operators, [
   tc_generic,
   tp_operator_dislog, tp_operator_prolog,
   tp_operator_prolog_with_pt, tp_operator_prolog_with_pt_tests,
   program_expand_with_pt,
   tp_operator_visualization,
   hres_iteration ] ).

dislog_module( model_based_semantics, [
   minimal_models,
   perfect_models, perfect_model_for_prolog,
   stable_models, stable_normal, stable_disjunctive,
   stable_models_gen,
   partial_stable_models, evidential_stable_models,
   query_evaluation,
   model_explanations, model_explanations_xpce,
   cc_semantics ] ).

dislog_module( mm_satchmo, [
%  mm_satchmo_2, mm_satchmo_examples,
   mm_satchmo_elem ] ).

dislog_module( possible_model_semantics, [
%  possible_models,
   possible_models_loenne, possible_models_si,
   possible_models_split,
   possible_models_tests, possible_models_tpi,
   epistemic_transformation, ground_transformation,
   split_transformation ] ).

dislog_module( well_founded_semantics, [
   gamma_operator, gamma_operator_prolog,
   adwfs, bdwfs, gnaf,
   gdwfs_st, gdwfs_sp,
   mdwfs, mdwfs_cbi,
   sdwfs, ddwfs,
   static_dislog ] ).

dislog_module( closed_world_assumptions, [
   supp_for_neg, supp_for_min,
   egcwa, egcwa_bubble, egcwa_bin_graph, egcwa_bin_tree, 
   egcwa_tests ] ).


% databases

dislog_module( mysql_interface, [
   mysql_odbc, mysql_odbc_refresh,
   mysql_odbc_updates, odbc,
   mysql_select, mysql_select_odbc,
   mysql_coupling,
   mysql_administration, mysql_database_schema,
   mysql_foreign_keys,
   mysql_schema_analysis,
   mysql_and_xml,
   mysql_tests ] ).

dislog_module( mysql_to_prolog, [
   mysql_statement_extract, mysql_to_prolog ] ).

dislog_module( oracle_interface, [
   sql_connect, sql_elementary,
   sql_fixpoint, sql_interface, sql_state ] ).

dislog_module( mongodb_interface, [
   mongodb_interface ] ).

dislog_module( database_schema_design, [
   er_schema_to_database_schema,
   er_diagrams, er_diagrams_to_grl,
   fd_algorithms_3nf, fd_algorithms_bcnf,
   fd_algorithms ] ).

dislog_module( ddbase, [
   ddbase,
   ddbase_integrity, ddbase_aggregate,
   ddbase_xml,
   ddbase_mediator,
   ddbase_datalog_to_sql, ddbase_fn_to_sql,
   ddql_query,
   ddql_parser,
   ddql_annotation,
   ddql_optimizer ] ).

dislog_module( ddbase_gui, [
   ddbase_gui,
   ddbase_programs ] ).

dislog_module( rule_analysis, [
   rule_analysis,
   rule_goal_graph_analysis,
   rule_display,
   rule_visualization,
   rule_examples,
   if_then_rules_read,
   rule_connector ] ).

dislog_module( time_series, [
   time_series ] ).

dislog_module( query_processing, [
%  'SQLCompiler.pl', dislog_to_sql,
   magic_sets_evaluation, magic_sets_transformation,
   query_processing,
   stability_chains ] ).

dislog_module( linear_sirups, [
   cknf, cknf_evaluation, cknf_normalization, cknf_tests,
   slf_expansion, sirup_as_graph,
   linear_sirups_elementary, example_sirups ] ).

/*
dislog_module( pdiax, [
   'pdiax_knowledge_base.pl',
   'pdiax_elementary.pl',
   'pdiax_gui.pl',
   'pdiax_program_transformations.pl',
   'pdiax_tpe_delta.pl',
   'pdiax_evidences.pl' ] ).
*/

dislog_module( probabilistic_databases, [
%  probabilistic_config,
%  probabilistic_elementary,
%  probabilistic_interval,
%  probabilistic_predicates,
%  probabilistic_main,
   probabilistic_1,
   probabilistic_2 ] ).

/*
dislog_module( database_schema_analysis, [
   database_schema_analysis,
   database_schema_txt_to_xml,
   database_schema_analysis_loader ] ).
*/

xxx_dislog_module( squash, [
   'analysis/join_conditions',
   'analysis/foreign_key_constraints',
   squash_loader ] ).

dislog_module( data_mining, [
   classification, decision_trees,
   entropy_experiment,
   frequent_itemsets,
   association_rules, association_rules_redundancy,
   association_rules_subsumption,
   association_rule_base,
   clustering,
   dsdk,
   html_output,
   demos,
   weka_parser, weka_wrapper, weka ] ).


% applications

dislog_module( selli, [
   selli_iso_latin,
   selli_elementary,
   selli_gui, selli_gui_advanced, selli_xpce,
   selli_xpce_tables,
   selli_database_dml,
   selli_database_files, selli_database_merge_files,
   selli_db_reports, selli_report_incremental,
   selli_diagnosis, selli_statistics,
   selli_receipts, selli_receipts_html, selli_receipts_ps,
   selli_xml ] ).

dislog_module( tennis, [
   tennis_elementary, tennis_file_handling,
   tennis_gui, tennis_browser, tennis_dml,
   tennis_time, tennis_variables, tennis_video,
   tennis_court_picture, tennis_visualization, tennis_colouring,
   tennis_court_operations,
   tennis_search_pattern,
   tennis_statistics, tennis_statistics_display,
   tennis_values, tennis_html,
   tennis_diagnosis,
   tennis_xml_file_analysis, tennis_hit_analysis,
   tennis_point_display,
   tennis_transactions, tennis_tables,
   tennis_association_rules,
   tennis_tesselation, tennis_distributions,
%  tennis_add_ons,
   tennis_ddbase ] ).

dislog_module( evu, [
   evu_iso_latin,
   evu_config, evu_questions,
   evu_prepare, evu_prepare_coupons, evu_prepare_courses,
   evu_evaluate, evu_evaluate_to_xml, evu_evaluate_gui,
   evu_mysql_queries,
   evu_to_latex, evu_to_latex_questions,
   evu_to_teachers, evu_teachers, evu_statistics ] ).

dislog_module( small_applications, [
   dentalog,
   area_problem,
   hypermanual,
   graphics,
   refactoring_fowler,
   datalog_to_jess,
   evolution_calendar ] ).

dislog_module( cardinality_constraints, [
   star_office_cc, merge_files,
   elementary, basic_operations,
   calculus_basic, calculus_improved, calculus_opt,
   analysis, charts, charts_so,
   set_trees,
   hyperresolution_positive, cardinality_constraints_smodels,
   mine_sweeper_board,
   mine_sweeper_play,
   mine_sweeper_reasoning ] ).

dislog_module( data_structures_and_algorithms, [
   ds_and_alg,
   binary_trees, search_trees, trees,
   sorting, merge_sort, quick_sort, heap_sort,
   merge_sort_generic,
   heaps_insert, heaps_delete ] ).

dislog_module( ddb_case_study, [
   ddb_case_study,
   d3_ddk ] ).

dislog_module( conferences, [
   elementary, algebra,
   mailing, statistics,
   reviews, proceedings, springer ] ).

dislog_module( teaching, [
   parts_of_list,
   odbc_example,
   labyrinth, anti_trust_control,
   ask_employee, ask_employee_odbc,
   factorial ] ).
   
dislog_module( travelling, [
   opodo ] ).

dislog_module( sudoku, [
   solve_sudoku ] ).

dislog_module( pictures, [
   show_pictures ] ).

dislog_module( pecap, [
   root_cause_analysis_ds,
   'root_cause_analysis_pn/main' ] ).

dislog_module( messages, [
   messages ] ).

dislog_module( music_xml_analysis, [
   music_xml_basics,
   music_xml_chords,
   music_xml_counterpoint,
   music_xml_intervals,
   music_xml_structure,
   music_xml_time ] ).

dislog_module( link_prediction, [
   clingo_evaluate,
   clingo_interface,
   clingo_explanations,
   link_prediction,
   perfect_model_for_clingo,
   link_prediction_authors,
   link_prediction_authors_2 ] ).


% artificial_intelligence

dislog_module( ontologies, [
%  triple_20_frontend,
%  dbn_on_rdf,
   ontology_examples,
   ontology_evaluation, ontology_evaluation_fnquery,
   owl_ontology_to_rules,
   partition_errors, partition_errors_graph,
   ontology_graph, ontology_measures,
   object_properties,
   swrl_refactoring,
   swrl_to_prolog_rules,
   sumo_to_prolog_rules ] ).

dislog_module( ontologies_swrl, [
   ontology_evaluation_class,
   ontology_evaluation_rules,
   swrl_to_class_rules,
   swrl_to_rule_rules,
   swrl_analysis ] ).

dislog_module( triple, [
   triple_parser, triple_to_xml,
   triple_transform_xml, triple_xml_to_prolog,
   triple_stratification, triple_reasoning ] ).

dislog_module( d3, [
   d3_case_studies, d3_html,
   d3_knowledge_base,
   d3_rule_goal_graphs,
   d3_knowledge_base_to_edges,
   d3_knowledge_base_to_rule_goal_graph,
   d3_knowledge_base_slicing,
   d3_knowledge_base_to_rules, d3_conditions,
   d3_dialogs, ddk_dialogs,
   d3_diagnostic_reasoning, d3_diagnostic_reasoning_2,
   d3_program_completion,
   d3_abduction, d3_abduction_multiple,
   d3_diagnosis_implication,
   d3_statistics,
   d3_to_ruleml,
   d3_explanations ] ).


% biology_and_medicine

dislog_module( qualimed, [
   qm_variables,
   qm_main, qm_elementary,
   qm_text_buffers,
%  qm_chains,
   qm_warnings, qm_preselections,
   qm_dialog_generation,
   qm_database_access,
   qm_database_interface ] ).

dislog_module( dentalog, [
   dentalog_to_xml,
   dentalog_diagnostic_reasoning,
   dentalog_kb_to_rule_goal_graph,
   dentalog_kb_to_decision_tree,
   dentalog_tests ] ).

dislog_module( genetics, [
   its_charts, its_analysis, its_clustering,
   dot_plot,
   append_fast ] ).

dislog_module( protein_states, [
   loader ] ).

dislog_module( protein_pathways, [
   soup_graph_search, soup_graph_transform,
   soup_node_to_html, soup_graph_to_xpce,
   soup_graph_tests ] ).

dislog_module( platelet_kinase_network, [
   abduction ] ).

dislog_module( drug_design, [
   kegg_pathway_to_models_analysis,
   kegg_pathway_to_models, kegg_pathway_to_models_ds,
   kegg_dlv_file_handling, kegg_pathway_elementary,
   kegg_to_pathway_edges, pathway_edges_to_program,
   pathway_edges_examples ] ).

dislog_module( keggpad, [
   kegg_pad, kegg_data,
   kegg_frame, kegg_graphics,
   kegg_tools, kegg_functions,
   kegg_cycles, kegg_simplify,
   kegg_soap, kegg_app,
   kegg_graph_analysis ] ).


% stock_tool

dislog_module( stock_administration, [
   stock_dynamics,
   stock_elementary, stock_date,
   stock_config, stock_files,
   stock_items, stock_nice_names,
   stock_admin, stock_data_cleaning,
   stock_analysis, stock_tests,
   stock_statistics, stock_tables ] ).

dislog_module( stock_knowledge_base, [
   branches, nice_names,
   pools, portfolios, special_dax_values,
   updates_general, updates ] ).

dislog_module( stock_states, [
   stock_states, stock_states_avg,
   stock_states_consult, stock_states_mysql,
   stock_states_analysis,
   stock_states_portfolio ] ).

dislog_module( stock_hierarchy, [
   stock_hierarchy_prepare,
   stock_hierarchy_window ] ).

dislog_module( stock_gui, [
   gui, dialogs, preferences,
   portfolio_manager, portfolio_editor,
   search_manager, cleaning_manager, analysis_manager,
   pulldown_menus, menu_bars,
   utilities, history ] ).

dislog_module( stock_charts, [
   interface, graph,
   latex, gnuplot, xpce, xpce_colons, plotting,
   chart_xml, chart_algebra ] ).

dislog_module( stock_pattern_search, [
   boxes, clustering,
   gui, visualization,
   elementary, dialogs,
   evaluation, tests ] ).

dislog_module( stock_spooler, [
   interface,
   www_read, www_to_pl,
   html_to_pl, html_to_pl_hopp, html_to_pl_ft,
   pl_to_dl, dl_to_sql, www_get ] ).
   
dislog_module( stock_mining, [
   interface, utilities, files, demo ] ).

dislog_module( energy, [
   energy_tests,
   energy, energy_gui,
   energy_reasoning, energy_graphics,
   energy_arrows, energy_prepare ] ).

dislog_module( energy_reasoning, [
   technical_chart_analysis, ranking, tests ] ).


% source_code_analysis

:- consult(
      'sources/source_code_analysis/source_code_analysis.pl').

/*
dislog_module( common, [
   xml_io, settings,
   fn_patches, basics ] ).

dislog_module( xpce_tools, [
   xpce_picture,
   hierarchy_browser_ddk_class,
   hierarchy_browser_ddk,
   hierarchy_browser_ddk_tests,
   xml_click_gesture_and_popups ] ).

dislog_module( hierarchy_browser_ddk, [
   hierarchy_browser_ddk_class,
   hierarchy_browser_ddk,
   hierarchy_browser_ddk_tests ] ).

dislog_module( gxl, [
   gxl ] ).

dislog_module_description( gxl, [
   arrows_handles,
   basics,
   basic_gxl_transformations,
   bfs_grouping,
   click_gesture_and_popups_examples,
   compound_methods,
   examples,
   gxl_to_vertices_edges,
   gxl_to_picture,
   gxl_to_tree_forward_back_cross_edges,
%  interface_gxl_graphs,
%  interface_graphs_to_xpce,
   gxl_to_single_vertices,
   layout,
   layout_tests,
   metrics,
   presettings,
   picture_to_gxl,
   scc,
   symbols_labels,
   tests,
   vertices_edges_to_gxl ] ).

dislog_module( rar, [
   rar ] ).

dislog_module_description( rar, [
   dislog_sources_to_xml_tree, tree_management,
   calls_pp, calls_pp_test,
   calls_ff, calls_ff_test,
%  variables,
   settings, temp,
   loading, saving,
   parser,
   database, db_update,
   special_predicates,
   retrieval, dml,
   messages, manual ] ).

dislog_module( visur, [
   visur ] ).

dislog_module_description( visur, [
   messages,
   cross_references,
   rule_goal_graphs,
   gui, gui_class,
   graph_common_methods,
   call_adapter_browser,
   call_adapter_picture,
   graphs ] ).

dislog_module( slicing, [
   slice, slice_tests ] ).

dislog_module( visur_rar, [
%  rar,
   rar_variables, rar_global_variables, rar_config,
   rar_parser, rar_parser_icb, rar_loading, rar_saving,
   rar_database, rar_dml, rar_retrieval, rar_query,
   rar_special_predicates,
   rar_manual, rar_tests,
   rar_and_xml,
%  visur,
   visur_config, visur_elementary,
   visur_graphs, visur_dependency_graphs,
   visur_file_goal_graphs,
   visur_components,
   visur_popups, visur_tests,
   rar_init ] ).

dislog_module( gxl_graphs, [
   gxl_database, xpce_pictures,
   gxl_graphs, gxl_to_xpce,
   graphs_to_xpce, set_trees_to_xpce,
   rule_goal_graphs,
   xpce_graphs_dfs, gxl_graph_layout, gxl_tree_layout,
   gxl_graphs_tests ] ).

dislog_module( visur_rar, [
   abbreviations,
   rar, rar_config, rar_parser,
   visur, visur_elementary, visur_components,
   visur_popups, visur_tests ] ).

dislog_module( gxl, [
   gxl_loader ] ).
*/


% development

dislog_module( refactoring_qualimed, [
   qualimed_dialog_parser, xpce_dialog_parser,
   refactoring_demos,
   refactoring_dialog,
   refactoring_pretty_print,
   refactoring_elementary ] ).

dislog_module( program_analysis, [
   meta_predicates,
   recursion_patterns,
   units_and_levels,
   predicate_dependency_graph,
   predicate_dependency_graph_levels,
   package_dependency_graphs, package_cycle_analysis,
   package_dependencies,
   prolog_database_calls_pp,
   prolog_file_to_interface,
   wc ] ).

dislog_module( program_analysis_rar, [
   rar_query, rar_variable ] ).

dislog_module( refactoring, [
   pretty_print_prolog_file,
   extract_method ] ).

dislog_module( jaml, [
   jaml_files, jaml_elementary,
   jaml_basic_relations, jaml_graphs,
   jaml_analysis, jaml_reasoning,
   jaml_clone_detection ] ).

dislog_module( pljava, [
   compile ] ).

dislog_module( management, [
   archiving,
   data_management ] ).

dislog_module( developer_advisor, [
   dd_advisor, dda, dda_tests,
   signature, signature_db,
   config, heuristic,
   create_info, metrics_db,
   read_source, parse_source, parse_source_extension,
   parse_source_elementary, dcg_trees,
   tools, dislog_help ] ).

dislog_module( documentation, [
   dislog_to_html_overview ] ).

dislog_module( refactoring_browser, [
   prolog_to_xml,
   prolog_in_xml_layout_transformation,
   refactoring_browser_classes,
   substitution_rules_application,
   xml_to_prolog,
   init ] ).

dislog_module( diverse, [
   rename ] ).

dislog_module( cobol_parser, [
   ebnf_parser_to_xml, ebnf_tokenizer_to_xml,
   ebnf_xml_to_edcg,
   cobol_parser ] ).


% linguistics

dislog_module( jean_paul, [
   jean_paul_to_latex,
   jean_paul_transform_gui,
   jean_paul_transform,
   jean_paul_transform_fn_item, jean_paul_transform_codes,
   jean_paul_codes_dcg, jean_paul_fn_item_dcg,
   jean_paul_basic_dcg,
   jean_paul_fn_item_substitutions,
   jean_paul_tests ] ).

dislog_module( adelung, [
   adelung, adelung_1, adelung_2, adelung_3,
   adelung_citations,
   adelung_sentences,
   adelung_tests, adelung_analysis ] ).

dislog_module( adelung_parsing, [
   adelung_parser,
   adelung_sentence_parsing,
   adelung_postprocessing ] ).

dislog_module( adelung_word_lists, [
   adelung_coarsen,
%  adelung_word_lists,
   adelung_word_list_operations,
   adelung_mark_sentences,
   adelung_highlight_bible_citations,
   adelung_highlight_keywords,
   adelung_pos_tagging,
   adelung_proper_numbering,
   adelung_file_to_html,
   adelung_tests ] ).

dislog_module( language_processing, [
   lexemes,
   lexer, lexer_load ] ). 

dislog_module( morpheme_decomposition, [
   morpheme_decomposition,
   morpheme_decomposition_with_parser,
   morfessor, morfessor_output ] ).

dislog_module( morpheme_annotation, [
   morpheme_term_operations, morpheme_term_join,
   morpheme_term_visualization,
   morpheme_term_analysis,
%  morpheme_term_database_prolog,
   morpheme_term_database, morpheme_term_database_loader,
   morpheme_data_sources,
   morpheme_annotation, morpheme_annotation_rules,
   morpheme_knowledge,
   morpheme_term_expand, morpheme_term_expand_new,
   morpheme_editor, morpheme_editor_updates,
   morpheme_term_to_xml ] ).

dislog_module( morpheme_annotation_xxul, [
   xxul_morpheme_editor,
   xxul_morpheme_editor_updates,
   xxul_morpheme_term_database ] ).

dislog_module( wdg, [
   wdg,
   wdg_word_class_extraction,
   wdg_entry_extraction,
   wdg_xml_to_txt,
   bnc_extraction ] ).

dislog_module( campe, [
   cwds_transformations, cwds_parsing,
   cwds_verb_inflections, cwds_noun_inflections,
   cwds_elementary ] ).

dislog_module( alignment, [
   alignment_visualization,
   alignment_comparison,
   alignment_analysis,
   alignment_workflows,
   alignment_dynamic,
   alignment_chars_to_similarities,
   alignment_tables, alignment_databases,
   mhd_from_nhd, mhdnhd_file_enrich ] ).

dislog_module( alignment_toolkit, [
   alignment,
   alignment_library, alignment_processing,
   alignment_rules, alignment_substitution ] ).


/******************************************************************/


