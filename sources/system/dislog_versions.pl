

/******************************************************************/
/***                                                            ***/
/***           DisLog:  Versions                                ***/
/***                                                            ***/
/******************************************************************/


/* ddk_version(Version, Date) <-
      */

ddk_version(Version, Date) :-
   ddk_version_(Version, Date),
   !.

ddk_version(Version) :-
   ddk_version(Version, _).

ddk_version_('2.0.4', '24.03.2023').
ddk_version_('2.0.3', '01.01.2022').
ddk_version_('2.0.2', '06.06.2021').
ddk_version_('2.0.1', '01.03.2021').
ddk_version_('2.0.0', '08.05.2020').
ddk_version_('1.9.4', '13.01.2020').
ddk_version_('1.9.3', '17.11.2019').
ddk_version_('1.9.2', '19.02.2019').
ddk_version_('1.9.1', '16.06.2018').
ddk_version_('1.9.0', '22.09.2017').
ddk_version_('1.8.6', '20.03.2017').
ddk_version_('1.8.5', '01.01.2017').
ddk_version_('1.8.4', '01.06.2016').
ddk_version_('1.8.3', '01.02.2016').
ddk_version_('1.8.2', '04.12.2015').
ddk_version_('1.8.1', '08.06.2015').
ddk_version_('1.8.0', '23.09.2014').
ddk_version_('1.7.4', '04.12.2012').
ddk_version_('1.7.3', '09.07.2012').
ddk_version_('1.7.2', '23.05.2012').
ddk_version_('1.7.1', '01.01.2012').
ddk_version_('1.7.0', '16.06.2011').
ddk_version_('1.6.6', '16.11.2010').
ddk_version_('1.6.5', '08.05.2010').
ddk_version_('1.6.4', '12.08.2009').
ddk_version_('1.6.3', '15.08.2008').
ddk_version_('1.6.2', '23.03.2008').
ddk_version_('1.6.1', '23.05.2007').
ddk_version_('1.6.0', '01.03.2007').
ddk_version_('1.5.9', '26.11.2006').
ddk_version_('1.5.8', '11.08.2006').
ddk_version_('1.5.7', '13.07.2006').
ddk_version_('1.5.6', '02.03.2006').
ddk_version_('1.5.5', '13.11.2005').


/******************************************************************/


