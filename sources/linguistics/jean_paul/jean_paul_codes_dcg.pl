

/******************************************************************/
/***                                                            ***/
/***        Jean Paul Edition:  Codes Replacement DCG           ***/
/***                                                            ***/
/******************************************************************/


:- module( jean_paul_codes_dcg, [
      jp_replace_codes/3 ]).


/*** interface ****************************************************/


/* jp_replace_codes(Xs_1, Xs_2, Xs_3) <-
      DCG rules for transforming
       - from the HTML source layer
       - to the XML migration layer. */

jp_replace_codes("<line/>") -->
   "-----".

jp_replace_codes("<longDash/>") -->
   "---".

% jp_replace_codes("<image/>") -->
%    "&#8721;&nbsp;&nbsp;&nbsp;&nbsp;&#8721;&#8721;".
jp_replace_codes("") -->
   "&#8721;&nbsp;&nbsp;&nbsp;&nbsp;&#8721;&#8721;".

% jp_replace_codes("<spatium/>") -->
%    "&nbsp;&nbsp;&nbsp;&nbsp;".
jp_replace_codes("") -->
   "&nbsp;&nbsp;&nbsp;&nbsp;".

jp_replace_codes(" ") -->
   "&nbsp;".

jp_replace_codes("</greek>") -->
   "&#8486;&#8486;".
jp_replace_codes("<greek>") -->
   "&#8486;".

% jp_replace_codes("<quot/>") -->
%    "&quot;".
jp_replace_codes("") -->
   "&quot;".


/* jp_replace_codes_2(Xs_1, Xs_2, Xs_3) <-
      */

jp_replace_codes_2("<letend><signedp>") -->
   "<signedp>µµ".
jp_replace_codes_2("<letend><letp>") -->
   "<letp>µµ".
jp_replace_codes_2("</signedp></letend>") -->
   "µµµ </signedp>".
jp_replace_codes_2("</letp></letend>") -->
   "µµµ </letp>".


/******************************************************************/


