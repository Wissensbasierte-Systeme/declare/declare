

/******************************************************************/
/***                                                            ***/
/***        Jean Paul Edition: XML to LaTeX                     ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      jean_paul_xml_to_latex/2.


/*** files ********************************************************/


/* jean_paul_file_to_latex(File) <-
      */

jean_paul_file_to_latex(File) :-
   jean_paul_file_to_latex(File, LaTeX),
   write_list(['---> ', 'jp140_input.tex', '\n']),
   predicate_to_file( 'projects/Jean_Paul/jp140_input.tex',
      jean_paul_write_tree(LaTeX) ).

jean_paul_file_to_latex(File, LaTeX) :-
   write_list(['<--- ', File, '\n']),
   jean_paul_directory(Directory),
   concat(Directory, File, Path),
   dread(xml, Path, [_, Xml]),
   jean_paul_xml_to_latex(Xml, LaTeX).
   

/* jean_paul_xml_to_latex(Xml, LaTeX) <-
      */

jean_paul_xml_to_latex(Xml, LaTeX) :-
   parse_fn_term(Xml, letter:_:Xmls),
   !,
   maplist( jean_paul_xml_to_latex,
      Xmls, LaTeX ).
jean_paul_xml_to_latex(Xml, LaTeX) :-
   parse_fn_term(Xml, letp:_:Xmls),
   !,
   maplist( jean_paul_xml_to_latex,
      Xmls, LaTeX_2 ),
   append(LaTeX_2, ['\\\\'], LaTeX).
jean_paul_xml_to_latex(Xml, LaTeX) :-
   parse_fn_term(Xml, nav_block:_:Xmls),
   !,
   maplist( jean_paul_xml_to_latex,
      Xmls, LaTeX_2 ),
   append(LaTeX_2, ['\n\n'], LaTeX).
jean_paul_xml_to_latex(Xml, LaTeX) :-
   parse_fn_term(Xml, nav_line:_:Xmls),
   !,
   maplist( jean_paul_xml_to_latex,
      Xmls, LaTeX_2 ),
   append(LaTeX_2, ['\n\n'], LaTeX).
jean_paul_xml_to_latex(Xml, LaTeX) :-
   parse_fn_term(Xml, pfad_el:_:Xmls),
   !,
   maplist( jean_paul_xml_to_latex,
      Xmls, LaTeX ).
jean_paul_xml_to_latex(Xml, LaTeX) :-
   parse_fn_term(Xml, letHead:_:Xmls),
   !,
   maplist( jean_paul_xml_to_latex,
      Xmls, LaTeX ).
jean_paul_xml_to_latex(Xml, LaTeX) :-
   parse_fn_term(Xml, nav_note:_:Xmls),
   !,
   maplist( jean_paul_xml_to_latex,
      Xmls, LaTeX ).
jean_paul_xml_to_latex(Xml, LaTeX) :-
   parse_fn_term(Xml, letformrightp:_:[Text]),
   !,
   name_append(['\n \\hfill ', Text, '\n'], LaTeX).
jean_paul_xml_to_latex(Xml, LaTeX) :-
   parse_fn_term(Xml, 'html:a':Attributes:[Text]),
   !,
   File := Attributes^href,
   jean_paul_xml_to_latex_html(File, Text, LaTeX).

jean_paul_xml_to_latex_html(File, Text, LaTeX) :-
   number_text(Text, Number),
   jean_paul_directory(Directory),
   concat(Directory, File, Path),
   exists_file(Path),
   !,
   jean_paul_file_to_latex(File, LaTeX_2),
   LaTeX = [
      '\\footnote[', Number, ']{', File, ':', LaTeX_2, '}'].
jean_paul_xml_to_latex_html(File, Text, LaTeX) :-
   number_text(Text, Number),
   !,
   LaTeX = [
      '\\footnote[', Number, ']{', File, '}'].
jean_paul_xml_to_latex_html(File, Text, LaTeX) :-
   name_append([Text, '\\footnote{', File, '}'], LaTeX).

jean_paul_xml_to_latex(Xml, '--') :-
   parse_fn_term(Xml, longDash:[]:[]),
   !.
jean_paul_xml_to_latex(Xml, LaTeX) :-
   parse_fn_term(Xml, note:_:Xmls),
   !,
   maplist( jean_paul_xml_to_latex,
      Xmls, LaTeX ).
jean_paul_xml_to_latex(Xml, LaTeX) :-
   parse_fn_term(Xml, jpzit:_:LaTeX_2),
   !,
   LaTeX = [
      '\\flqq', LaTeX_2, '\\frqq' ].

jean_paul_xml_to_latex(Xml, LaTeX) :-
   LaTeX = Xml.


/* jean_paul_write_tree(Xs) <-
      */

jean_paul_write_tree(Xs) :-
   is_list(Xs),
   !,
   checklist( jean_paul_write_tree,
      Xs ).
jean_paul_write_tree(X) :-
   writeln(X).

   
/* number_text(Text, Number) <-
      */

number_text('1', 1).
number_text('2', 2).
number_text('3', 3).
number_text('4', 4).
number_text('[', 0).


/******************************************************************/


