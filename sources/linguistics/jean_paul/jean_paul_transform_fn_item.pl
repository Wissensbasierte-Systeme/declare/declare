

/******************************************************************/
/***                                                            ***/
/***        Jean Paul Edition: Transform FN Items               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* jp_transform_fn_item(Item_1, Item_2) <-
      */

jp_transform_fn_item(Item_1, Item_2) :-
   assert(jean_paul_fn_item_substitutions:letterHeadFlag),
   fn_item_transform(jp_substitute_fn_item, Item_1, Item_2),
   retractall(jean_paul_fn_item_substitutions:letterHeadFlag).


/* jp_transform_fn_item(Path, Item_1, Item_2) <-
      */

jp_transform_fn_item(Path, Item_1, Item_2) :-
   fn_item_transform(jp_substitute_fn_item,
      Path, Item_1, Item_2).


/******************************************************************/


