

/******************************************************************/
/***                                                            ***/
/***        Jean Paul Edition: Transform Files                  ***/
/***                                                            ***/
/******************************************************************/


tustep_directory(Directory) :-
   home_directory(Home),
   concat(Home, '/research/projects/TUSTEP/', Directory).

jean_paul_directory(Directory) :-
   tustep_directory(Tustep),
   member(X,
      ['2004_10_05_Praetor/', 'Jean_Paul_2004_03_26/band1/']),
   concat(Tustep, X, Directory).


/*** interface ****************************************************/


/* jean_paul_transformation(Target_Layer, File) <-
      */

jean_paul_transformation(Target_Layer) :-
   dislog_variable_get(fn_transform_directory, Directory),
   directory_contains_files(['HTM'], Directory, Files_2),
   files_subtract_ending('HTM', Files_2, Files_3),
   checklist( jean_paul_transformation(Target_Layer),
      Files_3 ).

jean_paul_transformation(all, File) :-
   jean_paul_transformation(migration, File),
%  jean_paul_transformation_alternative(migration, File),
   jean_paul_transformation(archive, File),
   jean_paul_transformation(presentation, File).

jean_paul_transformation(migration, File) :-
   jean_paul_transformation_files(File, 'HTM'-mig, Path_1-Path_2), 
   jean_paul_transformation_htm_to_item(Path_1, Item),
   jp_transform_fn_item(Item, Xml),
   jp_write_to_file(Path_2, Xml),
   jp_pretty_print_archive_file(Path_2).
jean_paul_transformation(archive, File) :-
   jean_paul_transformation_files(File, mig-arc, Path_1-Path_2), 
   dread(xml, Path_1, T),
   Item_1 := (document:T)^comment,
   jp_transform_fn_item(Item_1, Item_2),
   jp_write_to_file(Path_2, [Item_2]).
jean_paul_transformation(presentation, File) :-
   jean_paul_transformation_files(File, arc-xml, Path_1-Path_2), 
   dread(xml, Path_1, T),
   Item_1 := (document:T)^comment,
   jp_archive_item_extract_notes(Item_1, Item_2),
   jp_write_to_file(Path_2, [Item_2]),
   netscape:www_open_url(mozilla, Path_2).

jean_paul_transformation_htm_to_item(Path, Item) :-
   jp_file_to_codes(Path, Codes_1),
   jp_transform_codes(jp_replace_codes, Codes_1, Codes_2),
   name(Html, Codes_2),
   html_name_to_fn_item(Html, Item).

jean_paul_transformation_files(
      File, Type_1-Type_2, Path_1-Path_2) :-
   dislog_variable_get(fn_transform_directory, Directory),
   concat([Directory, File, '.', Type_1], Path_1),
   concat([Directory, File, '.', Type_2], Path_2).


/* jp_pretty_print_archive_file(File) <-
      */

jp_pretty_print_archive_file(File) :-
   dread(xml, File, T),
   Item := (document:T)^comment,
   jp_write_to_file(File, [Item]).


/* jp_file_to_codes(File, Codes) <-
      */

jp_file_to_codes(File, Codes) :-
   write_list(user, ['<--- ', File, '\n']),
   read_file_to_codes(File, Codes_1),
   list_exchange_sublist(
      [["&gt;", "<i>"], ["&lt;", "</i>"], ["\"../", "\""]],
      Codes_1, Codes_2),
   name(Name_2, Codes_2),
   concat(File, '.tmp', File_2),
   predicate_to_file( File_2,
      write(Name_2) ),
   read_file_to_codes(File_2, Codes),
   delete_file(File_2).


/* jp_write_to_file(File, Xml) <-
      */

jp_write_to_file(File, Xml) :-
   write_list(user, ['---> ', File, '\n']),
   predicate_to_file( File,
      ( writeln('<?xml version="1.0" encoding="iso-8859-1"?>'),
        writeln(
           '<?xml-stylesheet type="text/css" href="jpxstyle.css" ?>'),
        checklist( dwrite(xml),
            Xml ) ) ).


/* html_name_to_fn_item(Html_Name, Item) <-
      */

html_name_to_fn_item(Html_Name, Item) :-
   tell('results/tmp.html'),
   write(Html_Name),
   told,
   dread(sgml, 'results/tmp.html', [Item]).


/* jp_archive_item_extract_notes(Item_1, Item_2) <-
      */

jp_archive_item_extract_notes(Item_1, Item_2) :-
   Item_2 := Item_1 <-> [^ednote^note^remark].


/******************************************************************/


