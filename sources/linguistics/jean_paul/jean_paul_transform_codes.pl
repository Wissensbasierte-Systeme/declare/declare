

/******************************************************************/
/***                                                            ***/
/***       Jean Paul Edition: Transform Character Codes         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* jp_transform_codes(Table, Cs_1, Cs_2) <-
      */

jp_transform_codes(_, [], []) :-
   !.	
jp_transform_codes(Table, Cs_1, Cs_2) :-
   Goal =.. [Table, Cs],
   phrase(Goal, Cs_1, Cs_a),
   !,
   jp_transform_codes(Table, Cs_a, Cs_b),
   append(Cs, Cs_b, Cs_2).
jp_transform_codes(Table, [C|Cs_1], [C|Cs_2]) :-
   !,
   jp_transform_codes(Table, Cs_1, Cs_2).


/* jp_transform_atom(+Table, +Atom_1, -Atom_2) <-
      Given the functor Table of a transformation table,
      Atom_1 is transformed to Atom_2.
      Each character in Atom_1 can be replaced by serveral
      characters. */

jp_transform_atom(Table, Atom_1, Atom_2):-
   atom_chars(Atom_1, Cs_1),
   jp_transform_codes(Table, Cs_1, Cs_2),
   atom_chars(Atom_2, Cs_2).


/******************************************************************/


