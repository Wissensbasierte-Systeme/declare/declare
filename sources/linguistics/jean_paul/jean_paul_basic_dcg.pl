

/******************************************************************/
/***                                                            ***/
/***        Jean Paul Edition: Transform Files                  ***/
/***                                                            ***/
/******************************************************************/


:- module( jean_paul_basic_dcg, [
      jean_paul_transformation_alternative/2,
      jean_paul_transformation_alternative_migration/2 ] ).


/*** implementation ***********************************************/


/* jean_paul_transformation_alternative(Target_Layer, File) <-
      */

jean_paul_transformation_alternative(migration, File) :-
   jean_paul_transformation_files(File, 'HTM'-mig, Path_1-Path_2),
   jean_paul_transformation_alternative_migration(
      Path_1, Path_2).

jean_paul_transformation_alternative_migration(Path_1, Path_2) :-
   jean_paul_basic_fng_transformation(Path_1, Item),
   !,
   body:_:Items := Item^body,
   jean_paul_comment(Comment, Items, []),
   jp_write_to_file(Path_2, [Comment]).

jean_paul_basic_fng_transformation(File, Item) :-
%  dread(xml, File, [Item_2]),
   jean_paul_transformation_htm_to_item(File, Item_2),
   dislog_variable_get(source_path, Sources),
   concat(Sources,
      'projects/jean_paul/jean_paul_basic_fng', File_S),
   fn_transform_fn_item_fng(File_S, Item_2, Item).


/*** DCG **********************************************************/


jean_paul_comment(comment:Es) -->
   jean_paul_comment_header(H),
   jean_paul_comment_block('Überlieferung', X),
   jean_paul_comment_block('Erläuterungen', Y),
   { append(H, [X, Y], Es) }.

jean_paul_comment_header([notep:[]:[commentHead:Es_1]|Es_2]) -->
   sequence_of_notep([notep:[]:[page:_:Es_1]|Es_2]).

jean_paul_comment_block(Type, ednote:[type:Type]:Es) -->
   sequence_of_notep([notep:_:[Type]|Es]).
   
sequence_of_notep([notep:As:Es]) -->
   [notep:As:Es].
sequence_of_notep([notep:As:Es|Xs]) -->
   [notep:As:Es],
   sequence_of_notep(Xs).


/******************************************************************/


