

/******************************************************************/
/***                                                            ***/
/***        Jean Paul Edition: Tests                            ***/
/***                                                            ***/
/******************************************************************/



/*** test *********************************************************/


test(xml:jean_paul, basic_dcg_transformation) :-
   home_directory(Home),
   concat([Home, '/research/projects/TUSTEP',
      '/Jean_Paul_2004_03_26/test/JPK001.HTM'], Path_1),
   dislog_variable_get(output_path, Results),
   concat(Results,
      'jean_paul_basic_dcg_transformation_tmp_file.xml', Path_2),
   jean_paul_transformation_alternative_migration(
      Path_1, Path_2),
   file_view(Path_2),
   delete_file(Path_2).


/******************************************************************/


