

/******************************************************************/
/***                                                            ***/
/***        Jean Paul Edition: Transform Table                  ***/
/***                                                            ***/
/******************************************************************/


:- module( jean_paul_fn_item_substitutions, [
      jp_substitute_fn_item/2 ] ).
 

/*** interface ****************************************************/


/* jp_substitute_fn_item(X, Y) <-
      substitution rules for transforming
       - from the HTML source layer
         to the XML migration layer and
       - from the XML migration layer
         to the archive layer. */

jp_substitute_fn_item(X, Y) :-
   X ---> Y.


/*** implementation ***********************************************/


/* X ---> Y <-
      transformation from the HTML source layer
      to the XML migration layer. */

table:_:Es ---> page:Es.
br:_:Es ---> page:Es.
b:_:Es ---> h4:Es.

html:_:Es ---> Es.
head:_:_ ---> ''.
body:_:Es ---> comment:Es.

p:As:[''] ---> '</ednote>' :-
   'margin-top:0;' := As^style.
p:As:Es ---> signedp:Es :-
   right := As^align.
p:_:['Überlieferung'] --->
   '<ednote type="Überlieferung">'.
p:_:['Erläuterungen'] --->
   '</ednote><ednote type="Erläuterungen">'.
p:_:[font:_:Es] ---> notep:Es.
p:_:[''] ---> ''.
p:_:Es ---> notep:Es.

font:As:Es ---> lat:Es :-
   'small-caps' := As^'font-variant'.
font:_:[X] ---> X.
font:_:[] ---> ''.

em:_:Es ---> commentHead:Es :-
   retract(letterHeadFlag).
em:_:Es ---> page:Es.

strong:_:Es ---> lemma:Es.
u:_:Es ---> spaced:Es.


/* X ---> Y <-
      transformation from the XML migration layer
      to the archive layer. */

notep:_:[''] ---> ''.
notep:_:Es ---> note:[id:I]:[Remark|Text] :-
   _ := (notep:_:Es)^page,
   _ := (notep:_:Es)^lemma,
   !,
   get_num(note_count, I),
   phrase( jean_paul_fn_item_dcg:remark(Remark),
      Es, Text ).


/* X ---> Y <-
      general rule. */

T:As:Es ---> T:As:Es.


/******************************************************************/


