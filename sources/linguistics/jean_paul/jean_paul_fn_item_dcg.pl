

/******************************************************************/
/***                                                            ***/
/***        Jean Paul Edition: Item Replacement DCG             ***/
/***                                                            ***/
/******************************************************************/


:- module( jean_paul_fn_item_dcg, [
      ]).
 

/*** interface ****************************************************/


remark(remark:Positions_and_Lemmas) -->
   positions(Ps), lemmas(Ls),
   { append(Ps, Ls, Positions_and_Lemmas) }.

positions([X, Y]) --> position(X), position(Y).
positions([X]) --> position(X).

position(position:[page:Page, line:Line]:[]) -->
   [page:_:[P]], [Line],
   { name_cut_at_position([","], P, Page) }.

lemmas([L1, L2]) -->
   lemma(L1), [bis], lemma_final(L2),
   { ! }.
lemmas([L]) --> lemma_final(L).

lemma(lemma:[Lemma]) --> [lemma:_:[Lemma]].
lemma(lemma:[Lemma]) --> [lemma:[Lemma]].

lemma_final(lemma:[Lemma]) --> lemma(lemma:[L]),
   { name_cut_at_position(["]"], L, Lemma) }.


/******************************************************************/


