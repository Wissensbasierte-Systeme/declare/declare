

/******************************************************************/
/***                                                            ***/
/***        Jean Paul Edition: Transform Files GUI              ***/
/***                                                            ***/
/******************************************************************/


:- home_directory(Home),
   concat([Home,
      '/research/projects/TUSTEP',
      '/Jean_Paul_2004_03_26/test/'], Directory),
   dislog_variable_set(fn_transform_directory, Directory),
   dislog_variable_set(jean_paul_font_size, 10).


/*** interface ****************************************************/


/* jean_paul_transform_gui <-
      */

jean_paul_transform_gui :-
   new(Frame, frame('Jean Paul Edition - Transformations')),
   send(Frame, background, colour(white)),
   send(Frame, append, new(Dialog_Header, dialog)),
   send(Dialog_Header, append, new(Menu_Bar, menu_bar)),
   send(new(Dialog, dialog), below, Dialog_Header),
   send(Dialog, background, lightsteelblue3),
   jean_paul_transform_gui_create_dialog(Dialog, [S, N, D]),
   jean_paul_send_pulldown_menus(Frame, Menu_Bar, [S, N, D]),
   jean_paul_gui_send_toolbar(Dialog_Header, [S, N, D]),
   rgb_to_colour([13.5,13.5,14], Colour_A),
   rgb_to_colour([14,14,15.99], Colour_B),
   send(Dialog_Header, background, Colour_A),
   send(Dialog, background, Colour_B),
   send(Frame, open).


/* jean_paul_transform_gui_create_dialog(Dialog, [S, N, D]) <-
      */

jean_paul_transform_gui_create_dialog(Dialog, [S, N, D]) :-
   dislog_variable_get(fn_transform_directory, Directory),
   send_list(Dialog, append, [
      new(S, menu('Target Layer')),
      new(N, text_item('Source Directory')),
      new(D, menu('Source File', cycle)) ]),
   send(S, on_image, 'on_marked.bm'),
   send(S, off_image, 'off_marked.bm'),
   send(S, gap, size(0, 0)),
   send_list(S, append, [all, migration, archive, presentation]),
   synchronise_jean_paul_source_files(Directory, D),
   send(N, size, size(400,50)),
%  send(D, item_size, size(40,10)),
   send(N, selection, Directory),
   jean_paul_transform_gui_send_fonts([S, N, D]).

jean_paul_transform_gui_send_fonts([S, N, D]) :-
   dislog_variable_get(jean_paul_font_size, Size),
   send(S, value_font, font(helvetica, roman, Size)),
   send(N, value_font, font(helvetica, roman, Size)),
   send(D, value_font, font(helvetica, roman, Size)),
   send(S, label_font, font(helvetica, bold, Size)),
   send(N, label_font, font(helvetica, bold, Size)),
   send(D, label_font, font(helvetica, bold, Size)).


/* jean_paul_send_pulldown_menus(Frame, Menu_Bar, [S, N, D]) <-
      */

jean_paul_send_pulldown_menus(Frame, Menu_Bar, [S, N, D]) :-
   send_pulldown_menu(Menu_Bar, 'File', [
      'Chdir' - change_jean_paul_source_directory(N, D),
      'Quit' - send(Frame, destroy) ]),
   send_pulldown_menu(Menu_Bar, 'Edit', [
      'Transform File' -
         jean_paul_transformation(S?selection, D?selection),
      'Transform Directory' -
         jean_paul_transformation(S?selection) ]),
   send_pulldown_menu(Menu_Bar, 'Help', []).


/* jean_paul_gui_send_toolbar(Dialog, [S, N, D]) <-
      */

jean_paul_gui_send_toolbar(Dialog, [S, N, D]) :-
   send(Dialog, append, new(TB, tool_bar)),
   send(TB, append, new(Transform_File, tool_button(
      message(@prolog,
         jean_paul_transformation, S?selection, D?selection),
      'file.bm'))),
   send(Transform_File,
      help_message, tag, 'Transform File'),
   send(Transform_File, colour, lightsteelblue4),
   send(TB, gap, size(4, 0)),
   send(TB, append, new(Transform_Directory, tool_button(
      message(@prolog,
         jean_paul_transformation, S?selection),
      'dir.bm'))),
   send(Transform_Directory,
      help_message, tag, 'Transform Directory'),
   send(Transform_Directory, colour, lightsteelblue4),
   send(TB, append, new(Chdir, tool_button(
      message(@prolog,
         change_jean_paul_source_directory, N, D),
      'cycle.bm'))),
   send(Chdir,
      help_message, tag, 'Change Directory'),
   send(Chdir, colour, lightsteelblue4).


/* change_jean_paul_source_directory(N, D) <-
      */

change_jean_paul_source_directory(N, D) :-
   get(@finder, file(open, *), Directory),
   send(N, selection, Directory),
   dislog_variable_set(fn_transform_directory, Directory),
   synchronise_jean_paul_source_files(Directory, D).


/* synchronise_jean_paul_source_files(Directory, Menu) <-
      */

synchronise_jean_paul_source_files(Directory, Menu) :-
   send(Menu, clear),
   directory_contains_files(['HTM'], Directory, Sources_2),
   files_subtract_ending('HTM', Sources_2, Sources_3),
   append(Sources_3, ['                 '], Sources),
   send_list(Menu, append, Sources),
   send(Menu, synchronise).


/******************************************************************/


