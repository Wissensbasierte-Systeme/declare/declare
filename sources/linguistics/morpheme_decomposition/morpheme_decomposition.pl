

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morphemes                              ***/
/***                                                            ***/
/***       Generating / Testing Morphemes on Wordlists          ***/
/***       Version: 0.1                                         ***/
/***       Date: 25.06.2009                                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


grimm :-
   grimm('grimm_sorted.xml',
      'grimm_mit_id.xml',
      'grimm_out.xml').


/* grimm(Sorted, Source, File) <-
      */

grimm(Sorted, Source, File) :-
   grimm_list_elements(Sorted, List), 
   dread(xml, Source, [Grimm]), 
   List1 := Grimm/content::'*', 
   grimm_vs_grimm(List, List1, Ts), 
   Ts1 = grimm:Ts,
   dwrite(xml, File, Ts1).

grimm_assert_sorted :-
   grimm_list_elements('grimm_sorted_del.xml', Grimm1), 
   grimm_list_elements('grimm_sorted_del.xml.1', Grimm2), 
   asserta(sorted_up(Grimm1)),
   asserta(sorted_down(Grimm2)).

grimm_single(Headword, Y) :-
   sorted_up(Grimm1),
   sorted_down(Grimm2),
   H = [headword:[headword:Headword, id:111]:[]], 
   grimm_vs_grimm(Grimm1, Grimm2, H, Y).


/*** implementation ***********************************************/


code_to_atom(X, Y) :-
   atom_codes(Y, [X]).

name_to_name_list(Name, List) :-
   atom_chars(Name, Xs),
   maplist(code_to_atom, Xs, List).

grimm_element_to_list(Element, List) :-
   name_to_name_list(Element, Xs),
   asserta(element_index(1)),
   maplist(list_pos, Xs, List), !,
   retractall(element_index(_)).

list_pos(X, Y) :-
   element_index(I),
   Y = I:X,
   I1 is I + 1,
   asserta(element_index(I1)).


/* grimm_list_elements(Source, List) <-
      */

grimm_list_elements(Source, List) :-
   dread(xml, Source, [SourceFN]),
   Elements := SourceFN/content::'*',
   findall( X,
      ( member(Element, Elements),
        X := Element@headword ),
      Xs ),
   maplist(atom_codes, Xs, List).


/* grimm_vs_grimm(List1, List2, Es, Ts) <-
      */

grimm_vs_grimm(_, _, [], []) :-
   !.
grimm_vs_grimm(List1, List2, [E|Es], [T|Ts]) :-
   E1 := E@headword,
   ID := E@id,
   grimm_vs_grimm_single_set(E1, List1, List2, T1),
   T = headword:[value:E1, id:ID]:T1,
   dwrite(xml, T),
   grimm_vs_grimm(List1, List2, Es, Ts).


/* grimm_element(SourceFN, Xs, X) <-
      */

grimm_element(SourceFN, Xs, X) :-
   Xs := SourceFN/content::'*',
   member(X1, Xs),
   X := X1@headword. 


/* grimm_vs_grimm_single_set(H, Es1, Es2, [T]) <-
      */

grimm_vs_grimm_single_set(H, Es1, Es2, [T]) :-
   grimm_vs_grimm_single(H, Es1, Es2, T).

grimm_vs_grimm_single(H, Es1, Es2, [T|Ts]) :-
   atom_codes(H, H1),
   member(E, Es2),
   \+ E = H1,
   append(X, E, H1),
   grimm_constraints(H, X, E, X1, T),
%  H2 := T1/content::'*',
%  grimm_vs_grimm_single(H2, Es1, Es2, T),
   grimm_vs_grimm_single(X1, Es1, Es2, Ts).
grimm_vs_grimm_single(H, Es1, Es2, [T|Ts]) :-
   atom_codes(H, H1),
   member(E, Es2),
%  \+ E = H1,
   append(E, X, H1),
   grimm_constraints(H, E, X, X1, T),
%  H2 := T1/content::'*',
%  grimm_vs_grimm_single(H2, Es1, Es2, T),
   grimm_vs_grimm_single(X1, Es1, Es2, Ts).
grimm_vs_grimm_single(H, _, _, [H1]) :- 
   H1 = morph:[H].

grimm_constraints(H, E, X, X1, T) :-
   length(E, L),
   L >= 1,
   \+ E = [Y, Y],
   atom_codes(E1, E),
   \+ E1 = H,
   \+ morphs_not_allowed(E1),
   T = morph:E1,
   atom_codes(X1, X),
   \+ morphs_not_allowed(E1, X1),
   \+ morphs_not_allowed(X1).

grimm_constraints_right(H, E, X, X1, T) :-
%  length(X, L),
%  L >= 2,
   \+ X = [Y, Y],
   atom_codes(E1, E),
   \+ morphs_not_allowed(E1),
   T = morph:E1,
   atom_codes(X1, X),
   \+ X1 = H,
   \+ morphs_not_allowed(X1, E1),
   \+ morphs_not_allowed(X1).


/* morphs_not_allowed(X) <-
      */

morphs_not_allowed(X) :-
   NotAllowed = [
      'ä', 'ü', 'ö', b, d, f, g, h, k, l, m, p, x, y, z, 
      bl, gr, ck, ho, wa, ch, kr, fi, ft, fr, lk, ng, nd,
      le, sp, ug, ze, pt, sch, der, die, das, den],
   atom_codes(X, X1),
   ( member(X, NotAllowed)
   ; X1 = [L, L] ).
%  length(X1, L),
%  L >= 4,
%  \+ contains_no_vocal(X1) ).


/* morphs_not_allowed(X, Y) <-
      */

morphs_not_allowed(X, Y) :-
   NotAllowed = [
      ac, ar, au, ck, ei, el, ie, hl, ft, is, in, 
      le, ic, ch, rg, ra, uh, ur, us, ut,
      st, sc, sz, at, it, ot, ut, et,
      ah, eh, ih, oh, uh,
      wa, we, wi, wo, wu,
      za, ze, zi, zo, zu],
   atom_codes(X, X1),
   atom_codes(Y, Y1),
   last(X1, L1),
   first(Y1, L2),
   append([L1], [L2], As),
   atom_codes(As1, As),
   ( member(As1, NotAllowed)
   ; L1 = L2,
     \+ member(As1, [hh]) ).

contains_no_vocal(X) :-
   atom_codes(aeiou, Vocals),
   \+ maplist(not_member, Vocals, X).

not_member(X, Xs) :-
   \+ member(X, Xs).


/* sort_grimm(Xs, Ys) <-
      */

sort_grimm(Xs, Ys) :-
   predsort(sort_grimm_order, Xs, Ys).

sort_grimm_order(Op, Grimm1, Grimm2) :-
   V1 := Grimm1@headword,
   V2 := Grimm2@headword,
   string_length(V1, L1),
   string_length(V2, L2),
   ( L1 =< L2, Op = '>'
   ; L1 == L2, Op = '=='
   ; L1 >= L2, Op = '<' ).


/* sort_morphemes(Source, Target) <-
      */

sort_morphemes(Source, Target) :-
   Source = T:As:Xs,
   predsort(sort_morphemes_order, Xs, Ys),
   Target = T:As:Ys.

sort_morphemes_order(>, Morpheme1, Morpheme2) :-
   Morpheme1Value := Morpheme1@value,
   Morpheme2Value := Morpheme2@value,
   string_length(Morpheme1Value, Length1),
   string_length(Morpheme2Value, Length2),
   Length1 =< Length2.
sort_morphemes_order(=, Morpheme1, Morpheme2) :-
   Morpheme1Value := Morpheme1@value,
   Morpheme2Value := Morpheme2@value,
   string_length(Morpheme1Value, Length1),
   string_length(Morpheme2Value, Length2),
   Length1 == Length2.
sort_morphemes_order(<, Morpheme1, Morpheme2) :-
   Morpheme1Value := Morpheme1@value,
   Morpheme2Value := Morpheme2@value,
   string_length(Morpheme1Value, Length1),
   string_length(Morpheme2Value, Length2),
   Length1 >= Length2.


/* apply_morphemes(SourceFile, MorphemeFile, Out) <-
      */

apply_morphemes(SourceFile, MorphemeFile, Out) :-
   dread(xml, SourceFile, [SourceFN]),
   dread(xml, MorphemeFile, [MorphemeFNUnsorted]),
   sort_morphemes(MorphemeFNUnsorted, MorphemeFN),
   apply_morphemes_(SourceFN, MorphemeFN, Xs1),
   Xs2 = morphemes:Xs1,
   dwrite(xml, Out, Xs2).
   
apply_morphemes_(SourceFN, MorphemeFN, Xs) :-
   MorphemeFN = _:_:Morphemes,
   SourceFN = _:_:Sources,
   findall(X, 
      ( member(Source, Sources),
        SourceValue := Source@headword,
        ID := Source@id,
        POS := Source@pos,
        atom_codes(SourceValue, SourceAtomValue),
        derive_morphemes(
           Morphemes, SourceAtomValue, POS, ID, X) ), 
      Xs).


/* derive_morphemes(Morphemes, Codes, POS, ID, X) <-
      */

derive_morphemes(Morphemes, Codes, POS, ID, X) :-
   ( derive_morphemes_(Morphemes, Codes, POS, front, [Ys]),
     Ys = _:_:[Value1],
     atom_codes(Value1, Codes1),
     derive_morphemes_(Morphemes, Codes1, POS, end, [Ys1]),
     Es = [Ys, Ys1], 
     !
   ; derive_morphemes_(Morphemes, Codes, POS, front, Es)
   ; derive_morphemes_(Morphemes, Codes, POS, end, Es) ),
   atom_codes(Value, Codes),
   X = headword:[value:Value, id:ID, pos:POS]:Es.

derive_morphemes_(
      Morphemes, AtomicHeadword, POS, Position, Ys) :-
   findall(Y, 
      ( member(Morpheme, Morphemes),
        Position := Morpheme@position,
	MorphemePOS := Morpheme@pos,
	MorphemeValue := Morpheme@value,
        ( POS = MorphemePOS
        ; POS = none, 
          MorphemePOS = 'VER' ),
        ( Position = end,
          atom_codes(MorphemeValue, MorphemeAtomValue),
          append(Y_1, MorphemeAtomValue, AtomicHeadword)
        ; Position = front,
          atom_codes(MorphemeValue, MorphemeAtomValue),
          append(MorphemeAtomValue, Y_1, AtomicHeadword) ),
        atom_codes(Y_2, Y_1), 
        !,
        Y = morph:[
           value:MorphemeValue, position:Position]:[Y_2] ),
      Ys).


/******************************************************************/


