

/******************************************************************/
/***                                                            ***/
/***          Linguistics:  Morpheme Decomposition              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* morphemize(Lemma, FN) <-
      */

morphemize(Lemma, FN) :-
   parse_lemma(Ys, [Lemma], []),
   flatten(Ys, Ys1),
   FN = lemma:Ys1.

parse_lemma(Xs) -->
   sequence('*', lemma, Xs).


/* lemma(Tokens, Lemma) -->
      */

lemma([], []) -->
   !.
lemma([X|Xs], Lemma) -->
   ( lexeme(X, Lemma, Rest),
     rest(X, Lemma, Rest) ),
   lemma(Xs, Rest).

lexeme(X, Lemma, Rest) -->
   { find_lexeme(Lemma, Lexeme, Rest),
     X = lexeme:[value:Lexeme]:[] }.
lexeme(X, Lemma, Lexeme) -->
   { find_rest_with_lexeme(Lemma, Lexeme, Rest),
     X = rest:[value:Rest]:[] }.
rest(X, Lemma, []) -->
   { atom_codes(Rest, Lemma),
     X = rest:[value:Rest]:[] }.


/*** implementation ***********************************************/


/*
lemma([X|Xs], Lemma) -->
   ( morpheme(X, Lemma, Lemma1)
   ; mistrest(X, Lemma, Lemma1) ),
   lemma(Xs, Lemma1), !.
lemma([X], Lemma) -->
   { atom_codes(L, Lemma),
     ( sorted_down(Source),
       member(Lemma, Source),
       X = lexeme:[value:L]:[] 
     ; test_rest(X, Lemma, Lemma1, Rest),
       X = mistrest:[value:Rest]:[] ) }.

morpheme(X, Lemma, Lemma1) -->
   [A],
   { ( append(Lemma, [A], Lemma2),
       \+ is_morpheme(Lemma2),
       is_morpheme(Lemma),
       atom_codes(L, Lemma),
       Lemma1 = [A],
       X = lexeme:[value:L]:[],
       !
     ; append(Lemma, [A], Lemma1), X = [] ) }.
     
mistrest(X, Lemma1, Lemma2) -->
   [A],
   { test_rest(A, Lemma1, Lemma2, Rest),
     X = mistrest:[value:Rest]:[] }.
    
test_rest(X, Lemma1, Lemma2, Rest) :-
    append(Lemma1, [X], Lemma3),
    sorted_down(Source),
    member(Lemma2, Source),
    append(Y, Lemma1, Lemma3),
    atom_codes(Rest, Y).
*/

find_lexeme(Lemma, Lexeme, Rest) :-
   sorted_down(Source),
   member(LexemeCodes, Source),
   append(LexemeCodes, Rest, Lemma),
   atom_codes(Lexeme, LexemeCodes).

find_rest_with_lexeme(Lemma, LexemeCodes, Rest) :-
   sorted_down(Source),
   member(LexemeCodes, Source),
   append(RestCodes, LexemeCodes, Lemma),
   \+ RestCodes = [],
   atom_codes(Rest, RestCodes).

is_morpheme(X) :-
   sorted_down(Source),
   member(X, Source).

 
/******************************************************************/


