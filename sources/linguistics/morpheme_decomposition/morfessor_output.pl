

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  WDG Morphessor                         ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(morfessor, wdg_morfessor_annotate) :-
   home_directory(Home),
   concat([ Home, '/research/projects/TUSTEP/',
      '2010/2010_12_13_Daten/' ], Data ),
   concat(Data, 'wdg_decomposition_pruned.xml', File_1),
   concat(Data, 'wdg_decomposition_annotated.xml', File_2),
   wdg_morfessor_file_annotate(File_1, File_2).

test(morfessor, wdg_morfessor_output) :-
   home_directory(Home),
   concat([ Home, '/research/projects/TUSTEP/',
      '2010/2010_12_13_Daten/wdg_decomposition.txt' ], File_1),
   File_2 = a,
   morfessor_file_to_xml(File_1, File_2).
   

/*** implementation ***********************************************/


/* wdg_morfessor_file_annotate(File_1, File_2) <-
      */

wdg_morfessor_file_annotate(File_1, File_2) :-
   dread(xml, File_1, [Item]),
   findall( E2,
      ( _ := Item/entry::[@key=Key, @decomposition=D1, @frequency=F],
        morpheme_atom_to_term(D1, T1),
        ( wdg_entry(Key, E),
          C := E/gramGrp@word_class ->
          T2 = T1*C,
          writeln(user, Key), ttyflush
        ; T2 = T1 ),
        term_to_atom(T2, D2),
        E2 := entry:[key:Key, decomposition:D2, frequency:F]:[] ),
      Items ),
   dwrite(xml, File_2, entries:Items, [encoding('utf-8')]).


/* morfessor_file_to_xml(File_1, File_2) <-
      */

morfessor_file_to_xml(File_1, File_2) :-
   read_file_to_lines(File_1, Lines),
   maplist( morfessor_line_to_xml,
      Lines, Entries ),
   dwrite(xml, File_2, entries:Entries).


/* morfessor_line_to_xml(Text, Xml) <-
      */

morfessor_line_to_xml(Text, Xml) :-
   name_cut_at_position([" "], Text, F),
   name_start_after_position([" "], Text, Decomposition),
   name_exchange_sublist([[" + ", ""]], Decomposition, Key),
   Xml = entry:[key:Key,
      decomposition:Decomposition, frequency:F]:[].


/******************************************************************/


