

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morphessor                             ***/
/***                                                            ***/
/******************************************************************/



/*** implementation ***********************************************/


/* morfessor(File_1, File_2) <-
      */

morfessor(File_1, File_2) :-
   home_directory(Home),
   concat([Home, '/soft/morfessor/morfessor1.0.perl -data ',
      File_1, ' > ', File_2], Command),
   us(Command).


/******************************************************************/


