

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Grammar Analysis                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* adelung_terminals_to_words_table <-
      */

adelung_terminals_to_words_table :-
   adelung_sentence_parsing_terminals(Terminals),
   adelung_terminals_to_words_table(Terminals, Html),
   html_to_display(Html).


/* adelung_sentence_parsing_terminals(Terminals) <-
      */

adelung_sentence_parsing_terminals(Terminals) :-
   dislog_variable_get(source_path,
      'projects/adelung_parsing/adelung_sentence_parsing.pl',
      File),
   program_file_to_xml(File, Xml),
   findall( Terminal,
      ( Terminal := Xml/rule/head/
           atom::[@predicate='-->'/2]/nth_child::1@functor,
        adelung_terminal_to_words(Terminal, _) ),
      Terminals_2 ),
   sort(Terminals_2, Terminals).
   

/* adelung_terminals_to_words_table(Terminals, Html) <-
      */

adelung_terminals_to_words_table(Terminals, Html) :-
   maplist( adelung_terminal_to_table_row,
      Terminals, Rows ),
   Html = html:[ table:[align:left, border:1]:[
      tr:[th:['Terminal'], th:['Words']]|Rows] ].

adelung_terminal_to_table_row(Terminal, Table_Row) :-
   adelung_terminal_to_words(Terminal, Words),
   names_append_with_separator(Words, ', ', Word),
   Table_Row = tr:[td:[Terminal], td:[Word]].


/* adelung_terminal_to_words(Predicate, Words) <-
      */

adelung_terminal_to_words(Predicate, Words) :-
   Atom =.. [Predicate, _, _, _],
   clause(adelung_sentence_parsing:Atom, Body),
   comma_structure_to_list(Body, [_, Member|_]),
   Member =.. [member, _, Words_2],
   sort(Words_2, Words).


/******************************************************************/


