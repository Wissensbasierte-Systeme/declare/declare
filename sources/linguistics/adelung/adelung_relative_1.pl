

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Parsing of Sentences                    ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_detect_relative, [
      adelung_detect_relative_clauses_2/0,
      relative_clauses/2 ] ).


/*** interface ****************************************************/


/* adelung_detect_relative_clauses <-
      */

adelung_detect_relative_clauses_2 :-
   dislog_variable_get(source_path, Sources),
   concat(Sources,
      'projects/adelung/adelung_relative_fng.pl', Path_fng),
   dislog_variable_get(example_path, Examples),
   concat(Examples, 'adelung/adelung_ausschnitt_3.xml', Path_1),
   concat(Examples, 'adelung/adelung_relative_1_3.xml', Path_2),
   fn_transform_xml_file_fng(Path_fng, Path_1, Path_2).


/*** implementation ***********************************************/


/* relative_clauses(Name, Clauses) <-
      */

relative_clauses(Name, Clauses) :-
   Substitution = [ [", ", " , "],
      ["."," . "], [". ", " . "], ["; ", " ; "] ],
   name_exchange_sublist(Substitution, Name, Name_2),
   name_exchange_sublist([["  ", " "]], Name_2, Name_3),
   name_split_at_position(
      [" ", /*".", ",", ";", */ "\n", "\n\n"], Name_3, Names),
   relative_clauses_(Clauses, Names, _).

relative_clauses_([X|Xs]) -->
   relative_clause(X),
   ( relative_clauses_(Xs)
   ; { Xs = [] } ).

relative_clause ==>
   comma, ( prep ; void ), relative_pronoun,
   ( sequence_2
   ; verb
   ; ( sequence_3 ; adjective ), sequence_2
   ; noun_phrase, prefix, sequence_2
   ; prefix, adjective, verb
   ; noun_phrase, adjective, ( sequence_2 ; verb )
   ; noun_phrase, sequence_3, verb
   ; sequence_3, comma, adjective, verb ).

relative_clause([relative_clause:[Word]]) -->
   [Word].

sequence_2 ==>
   ( prep ; pronoun ; void ), noun_phrase, verb.

sequence_3 ==>
   ( prep ; conjunction ; prefix ),
   adjective, ( adjective ; prep ).


% verb structures

verb ==>
   verb_phrase ; verb_.

verb_phrase ==>
   ( is_verb, infinitive, is_modal
   ; ( is_verb ; infinitive ), ( is_modal ; is_verb ) ).

verb_phrase([verb_phrase:[Verb]]) -->
   verb_(Verb),
   { \+ concat(ge, _, Verb),
     \+ concat(abge, _, Verb),
     \+ concat(ange, _, Verb) }.

verb_ ==>
   [Verb],
   { char_type_of_atom(Verb, lower),
     ( ( member(X, [et, t, en]),
         concat(_, X, Verb)
       ; member(Verb, [sind]) ),
       \+ concat(abge, _, Verb),
       \+ concat(_, aus, Verb) ),
     \+ member(Verb, [mit, zun�chst, vermittelst, einen, den,
           matten, einigen, angenehmen, faserigen, abgearbeiteten,
           folgenden, thierischen, weit, d�nnen, neugebornen,
           zwischen, selten, erst, gedehnten, gefangenen, keinen,
           gegen, eingebogenen, glatten, gr��ten, heut, linken,
           nicht, veralteten, todten, entbl��ten]) }.


% noun phrases

noun_phrase ==>
   ( noun, conjunction, noun_phrase
   ; article, adjective, adjective, noun_phrase
   ; article, noun, conjunction, noun
   ; noun, noun_phrase
   ; article, adjective, noun
   ; article, noun, noun_phrase
   ; conjunction, adjective, noun_phrase
   ; ( adjective ; article ), noun
   ; prep, ( article ; adjective ), noun
   ; pronoun
   ; noun
   ; adverb ).

prefix ==>
   [sich] ; [man].

adverb ==>
   [einander] ; [miteinander] ; [dadurch].

adjective ==>
   [Adj],
   { char_type_of_atom(Adj, lower),
     \+ verb_(Adj, _, _)
   ; member(Adj, [alsdann, 'Abends', h�ufig, gleich, zun�chst,
        gemeinen, �blich, nur, matten, einigen, angenehmen,
        faserigen, abgearbeiteten, folgenden, thierischen,
        weit, d�nnen, neugebornen, zwischen, selten, erst,
        gedehnten, gefangenen, keinen, gegen, eingebogenen,
        unter, nur, gewissen, sehr, weit, sonst]) }.

noun ==>
   [Noun],
   { char_type_of_atom(Noun, upper)
   ; member(Noun, [andern, '�pfel', '�thiopien']) }.

relative_pronoun ==>
   [das] ; [dem] ; [den] ; [denen] ; [der] ; [deren] ; [des] ;
   [dessen] ; [die] ; [was] ; [welche] ; [welchem] ; [welchen] ;
   [welcher] ; [welches] ; [wo].

article ==>
   [das] ; [dem] ; [den] ; [der] ; [des] ; [die] ;
   [ein] ; [eine] ; [einem] ; [einen] ; [einer] ; [eines].

pronoun ==>
   [der] ; [die] ; [das] ; [dem] ; [den] ; [er] ; [sie] ; [es] ;
   [ein] ; [einer] ; [einen] ; [eine] ; [einem] ; [eines] ;
   [solches] ; [solchen] ; [eure] ; [etwas] ; [gewissen].

prep ==>
   [in] ; [an] ; [vermittelst] ; [mit] ; [aus] ; [durch] ;
   [auf] ; [von] ; [am] ; [bey] ; [im] ; [zu] ; [in] ;
   [sonst] ; [so] ; [nach] ; [bei].

infinitive ==>
   [Verb_Inf],
   { ( concat(_, en, Verb_Inf)
     ; member(Verb_Inf, [seyn]) ) }.

is_modal ==>
   [hat] ; [wird] ; [worden] ; [ist] ; [sind] ; [soll].

conjunction ==>
   [aber] ; [als] ; [auch] ; [denn] ; [doch] ; [entweder] ;
   [jedoch] ; [noch] ; [oder] ; [sowie] ; [sowohl] ; [und] ;
   [weder] ; [anstatt] ; [ohne] ; [statt] ; [um] ; [als] ;
   [ bevor] ; [da] ; [damit] ; [dass] ; [da�] ; [falls] ;
   [indem] ; [indes] ; [ob] ; [obwohl] ; [obzwar] ;
   [seit] ; [so] ; [sobald] ; [sofern] ; [solang] ; [sooft] ;
   [sosehr] ; [soviel] ; [soweit] ; [sowie] ;
   [statt] ; [weil] ; [wenn] ; [zumal].

comma ==>
   [','] ; [';'].


/******************************************************************/


