

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Processing - 3                          ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_3, [
      adelung_process_document_3/2 ] ).


/*** interface ****************************************************/


/* adelung_process_document_3(X, Y) <-
      */

adelung_process_document_3(X, Y) :-
   fn_item_transform(adelung_3:'--->', X, Y),
   Path = 'results/adelung_3.html',
   write_list(user, ['---> ', Path, '\n']),
   dwrite(html, Path, Y),
   html_to_display(Y).


/*** implementation ***********************************************/


defs:_:Es ---> html:[E|Es] :-
   E = h1:[font:[color:'#8698fd']:['Adelung - Definitions']].
def:[key:Key]:Es ---> p:[E|Es] :-
   E = h2:[font:[color:'#8698fd']:[Key]].

list:_:Es ---> ul:Es.
Item ---> li:[Text|Es] :-
   Item = item:_:Es,
   Text := Item@text.
remark:_:Es ---> h3:Es.
ref:_:Es ---> font:[color:'#8698fd']:['-->'|Es].
'MILESTONE':As:_ ---> font:[color:'#fd8698']:[Milestone] :-
   ( foreach(A:V, As), foreach(E, Es) do
        concat([A, '="', V, '"'], E) ),
   names_append_with_separator(['&lt;MILESTONE'|Es], ' ', X),
   concat(X, '/&gt;', Milestone).


X ---> X.


/******************************************************************/


