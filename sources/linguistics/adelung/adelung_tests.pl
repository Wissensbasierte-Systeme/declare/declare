

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Processing                              ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(adelung, adelung_sentence) :-
   adelung_sentence(Sentence),
   Sentence = sentence:[
      word:[text:'Hier']:[],
      word:[text:kommt]:[],
      citation:[
         text:'1. Mos 2, 17', book:1, seperator:'.',
         author:'Mos', chapter:2, verse:17]:[],
      word:[text:und]:[],
      citation:[text:'2 Buch Josua 3, 18',
         book:2, seperator:'Buch', author:'Josua',
         chapter:3, verse:18]:[],
      punctuation_mark:[text:!]:[] ].

test(adelung, lists(1)) :-
   dread(xml, 'examples/adelung/adelung_aber.xml', [Xml]),
   Def := Xml/descendant::'ENTRY'::[@'KEY'='Aber']/'DEF',
   test_adelung_lists('Aber', Def).

test(adelung, lists(2)) :-
   concat('I. x II. y 1.  (1) (2) (3) (4) a) b) c) Gell. ',
      'd) u. s. f.2.  (1) (2)', Text),
   Def = 'DEF':[]:[Text],
   test_adelung_lists(test, Def).

test_adelung_lists(Key, Def) :-
   adelung_process_single_document_1(Key, Def, A),
   adelung_process_document_2(A, B),
   adelung_process_document_3(B, _).

test(adelung, does_not_work) :-
   !,
   fail.


/******************************************************************/


