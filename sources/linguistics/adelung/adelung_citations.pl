

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Citations                               ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_citations, [
      adelung_sentence/1,
      adelung_sentence/3 ] ).


/*** interface ****************************************************/


adelung_sentence(Sentence) :-
   Words = ['Hier', 'kommt', '1', '.', 'Mos', '2', ',', '17',
      'und', '2', 'Buch', 'Josua', '3', ',', '18', '!'],
   adelung_sentence(Sentence, Words, []),
   dwrite(xml, Sentence),
   !.


/* adelung_sentence(sentence:Ws, Xs, Ys) <-
      */

adelung_sentence(sentence:Ws, Xs, Ys) :-
   adelung_sentence_(Ws, Xs, Ys).


/*** implementation ***********************************************/

   
adelung_sentence_([W|Ws]) -->
   word(W),
   ( adelung_sentence_(Ws)
   ; [],
     { Ws = [] } ).


word(W) -->
   citation(W).
word(W) -->
   punctuation_mark(W).
word(word:[text:W]:[]) -->
   [W].


citation(Citation) -->
   book_number(Number_1),
   [X], { member(X, ['.', 'Buch']) },
   book_name(Name),
   book_number(Number_2),
   [','],
   book_number(Number_3),
   !,
   { ( X = '.',
       Y = '. '
     ; X = 'Buch',
       Y = ' Buch ' ),
     concat([Number_1, Y, Name, ' ', Number_2, ', ', Number_3],
     Text),
     Citation = citation:[
        text:Text,
        book:Number_1,
        seperator:X,
        author:Name,
        chapter:Number_2,
        verse:Number_3 ]:[] }.


punctuation_mark(punctuation_mark:[text:M]:[]) -->
   [M],
   { member(M, ['.', ',', ';', ':', '-', '!']) }.

% punctuation_mark('(').
% punctuation_mark(')').


book_name(Name) -->
   [Name].
%  { member(Name, ['Mos']) }.

book_number(Number) -->
   [N],
   { name_to_number(N, Number),
     number(Number) }.


/******************************************************************/


