

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Processing                              ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung, [
      adelung_gui/0,
      adelung_gui/1,
      adelung_load_documents/1,
      adelung_process_document/0,
      adelung_process_document/2,
      adelung_process_document/4,
      adelung_process_single_document/1,
      adelung_process_single_document/2 ] ).


/*** interface ****************************************************/


/* adelung_gui <-
      */

adelung_gui :-
   File = 'adelung_a_b.xml',
   dislog_variable_get(example_path, Examples),
   concat([Examples, 'adelung/', File], Path),
   adelung_gui(Path).

adelung_gui(File) :-
   retract_all(adelung:adelung_document(_, _)),
   adelung_load_documents(File),
   findall( li:[a:[href:Prolog_Call]:[Key]],
      ( adelung:adelung_document(Key, _),
        concat(['prolog:adelung_process_single_document(''',
           Key, ''')'], Prolog_Call) ),
      Keys_html ),
   Html = html:[
      body:[bgcolor:'#ffeecc']:[
         h3:[font:[color:'#8698fd']:['Adelung']],
         br:[],
         ol:Keys_html ] ],
   html_to_cms_doc_window('Adelung', Html, size(300,200)).


/* adelung_process_document(X, Y) <-
      */

adelung_process_document :-
   adelung_process_document(_, _).

adelung_process_document(X, Y) :-
%  File = 'adelung_a_b.xml',
   File = 'adelung_aber.xml',
%  File = 'adelung_gehen.xml',
%  File = 'adelung_liegen.xml',
   adelung_process_document(File, _, X, Y).


/* adelung_process_document(File, Key, X, Y) <-
      */

adelung_process_document(File, Key, X, Y) :-
   dislog_variable_get(example_path, Examples),
   concat([Examples, 'adelung/', File], Path),
   write_list(user, ['<--- ', Path, '\n']),
   dread(xml, Path, [X]),
   adelung_process_document_1(Key, X, A),
   adelung_process_document_2(A, B),
   adelung_process_document_3(B, Y).


/* adelung_process_single_document(Key, Y) <-
      */

adelung_process_single_document(Key) :-
   adelung_process_single_document(Key, _).

adelung_process_single_document(Key, Y) :-
   adelung_process_single_document_1(Key, A),
   adelung_process_document_2(A, B),
   adelung_process_document_3(B, Y).


/* adelung_load_documents(File) <-
      */

adelung_load_documents(File) :-
   write_list(user, ['<--- ', File, '\n']),
   dread(xml, File, [KB]),
   forall(
      Def := KB^'TEXT'^'BODY'^'DIV0'^'ENTRY'::[@'KEY'=Key]^'DEF',
      ( write(user, '.'), ttyflush,
        assert(adelung_document(Key, Def)) ) ).


% http://mdz.bib-bvb.de/digbib/lexika/adelung/text/band2/
%    @Generic__BookView;cs=default;ts=default?q=gehen
%    &DwebQueryForm=%24q+inside+%3CORTH%3E


/******************************************************************/


