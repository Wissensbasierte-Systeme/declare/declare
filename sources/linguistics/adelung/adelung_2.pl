

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Processing - 2                          ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_2, [
      adelung_process_document_2/2 ] ).


/*** interface ****************************************************/


/* adelung_process_document_2(X, Y) <-
      */

adelung_process_document_2(X, Y) :-
   findall( Def,
      ( Def_2 := X^def,
        adelung_process_definition(Def_2, Def) ),
      Defs ),
   Y = defs:Defs,
   Path = 'results/adelung_2.xml',
   write_list(user, ['---> ', Path, '\n']),
   dwrite(xml, Path, Y).


/*** implementation ***********************************************/


/* adelung_process_definition(Def_1, Def_2) <-
      */

adelung_process_definition(Def_1, Def_2) :-
   Ns := Def_1^content::'*',
   ( Key := Def_1@'KEY'
   ; Key := Def_1@key ),
   length(Ns, L),
   write_list(user, ['Phase 2: ', Key, ' (', L, ' items)\n']),
   !,
   items(Items, Ns, []),
   Def = def:[key:Key]:Items,
   !,
   fn_triple_condense(Def, Def_2).


/* items(Items, Xs, Ys) <-
      */

items([]) -->
   { true }.
items([I|Is]) -->
   item(I),
   items(Is).

item(I) -->
   xml_element(I).
item(I) -->
   token(I).
item(I) -->
   adelung_list([], [], I).


/* adelung_list(Path, Sofar, Items, Xs, Ys) <-
      */

adelung_list(Path, [], Items)  -->
   [Item],
   { is_adelung_item(Item, Item_2),
     ! },
   adelung_list(Path, [Item_2], Items).
adelung_list(Path, [I|Is], Items)  -->
   [Item],
   { is_adelung_item(Item, Item_2),
     list_item_next(I, Item),
     \+ ( \+ list_item_next_exact(I, Item),
          member(J, Path),
          list_item_next_exact(J, Item) ),
     ! },
   adelung_list(Path, [Item_2, I|Is], Items).
adelung_list(Path, Is, Items, Xs, Xs) :-
   first(Xs, X),
   is_adelung_item(X, _),
   member(I, Path),
   list_item_next(I, X),
   Is \= [],
   reverse(Is, Js),
   Items = list:Js.
adelung_list(Path, [I|Is], Items)  -->
   adelung_list([I|Path], [], Is2),
   { I = item:As:Es1,
     append(Es1, [Is2], Es2) },
   adelung_list(Path, [item:As:Es2|Is], Items).
adelung_list(_, Is, Items)  -->
   { Is \= [],
     reverse(Is, Js),
     Items = list:Js }.


/* is_adelung_item(Item_1, Item_2) <-
      */

is_adelung_item(Item_1, Item_2) :-
   fn_item_parse(Item_1, item:As:Es_1),
   items(Es_2, Es_1, []),
   Item_2 = item:As:Es_2.


/* list_item_next(I1, I2) <-
      */

list_item_next_exact(I1, I2) :-
   list_item_next(I1, I2),
   D := I1@delimiter,
   D := I2@delimiter.

list_item_next(I1, I2) :-
   T := I1@type,
   T := I2@type,
   N1 := I1@number,
   N2 := I2@number,
   list_number_next(N1, N2).

list_number_next(N1, N2) :-
   to_number(N1, M1),
   to_number(N2, M2),
   M2 is M1 + 1.
list_number_next(N1, N2) :-
   sublist([N1, N2], [a, b, c, d, e, f, g, h, i, j, k, l]).
list_number_next(N1, N2) :-
   sublist([N1, N2], ['I', 'II', 'III', 'IV', 'V']).

to_number(N, M) :-
   name(N, Ns),
   name(M, Ns),
   number(M).


/* xml_element(E, Xs, Ys) <-
      */

xml_element(T:As:Es_2) -->
   [E],
   { fn_item_parse(E, T:As:Es_1),
     T \= item,
     items(Es_2, Es_1, []) }.


/* token(T, Xs, Ys) <-
      */

token(T) -->
   [T],
   { \+ ( _ := T^tag::'*' ) }.


dont_wait.


/******************************************************************/


