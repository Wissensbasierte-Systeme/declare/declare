

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Sentences                               ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_sentences, [
      adelung_highlight_sentences/1,
      adelung_highlight_sentences/2 ] ).


/*** interface ****************************************************/


/* adelung_highlight_sentences(Sentences) <-
      */

adelung_highlight_sentences(Sentences) :-
   Words = [
      word:[]:['Nach'],
      word:[]:['dem'],
      abbr:[text:'Buch']:[],
      citation:[text: '1. Mos. 7, 11']:[],
      word:[]:['gilt'],
      punctuation_mark:[text:'.']:[],
      word:[]:['Deshalb'],
      word:[]:['nicht'],
      punctuation_mark:[text:'.']:[] ],
   adelung_highlight_sentences(Words, Sentences).


/* adelung_highlight_sentences(Words, Sentences) <-
      */

adelung_highlight_sentences(Words, Sentences) :-
   sentences_(Sentences, Words, []),
   dwrite(xml, words:Words),
   fn_triple_to_picture(words:Words, _),
   dwrite(xml, sentences:Sentences),
   fn_triple_to_picture(sentences:Sentences, _),
   !.


sentences_([], [], []).
sentences_([S|Ss]) -->
   sentence_(S),
   !,
   sentences_(Ss).

sentence_(sentence:Ws) -->
   words(Ws),
   !.

words([W:As:Es|Ws]) -->
   [W:As:Es],
   { W == word
   ; W == abbr
   ; W == 'REF'
   ; W == citation },
   !,
   words(Ws).
words([X]) -->
   punctuation_mark_([X]).
words([], [], []).

punctuation_mark_([X]) -->
   [X],
   { X = punctuation_mark:_:_ }.


/******************************************************************/


