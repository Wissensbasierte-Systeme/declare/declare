

'DEF':As:[Es_1] ---> 'DEF':As:[Es_2] :-
   relative_clauses(Es_1, Es_2).

def:As:Es_1 ---> def:As:Es_2 :-
   relative_clauses(Es_1, Es_2).

X ---> X.

relative_clauses(X, Y) :-
   adelung_detect_relative:relative_clauses(X, Y).


