

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Processing - 1                          ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_1, [
      adelung_process_document_1/2,
      adelung_process_document_1/3,
      adelung_process_single_document_1/2,
      adelung_process_single_document_1/3,
      adelung_process_definition/2 ] ).


/*** interface ****************************************************/


/* adelung_process_document_1(X, Y) <-
      */

adelung_process_document_1(X, Y) :-
   adelung_process_document_1(_, X, Y).

adelung_process_document_1(Key, X, Y) :-
   adelung_fn_item_transform(X, Doc),
   findall( [Def, Key],
      Def := Doc^text^body^'DIV0'^entry::[@'KEY'=Key]^def,
      Pairs ),
   adelung_process_pairs(Pairs, Y).

adelung_process_single_document_1(Key) :-
   adelung_process_single_document_1(Key, _).

adelung_process_single_document_1(Key, Y) :-
   findall( [Def, Key],
      ( adelung:adelung_document(Key, Doc),
        adelung_fn_item_transform(Doc, Def) ),
      Pairs ),
   adelung_process_pairs(Pairs, Y).

adelung_process_single_document_1(Key, Doc, Y) :-
   findall( [Def, Key],
      adelung_fn_item_transform(Doc, Def),
      Pairs ),
   adelung_process_pairs(Pairs, Y).


/*** implementation ***********************************************/


/* adelung_fn_item_transform(Item_1, Item_2) <-
      */

adelung_fn_item_transform(Item_1, Item_2) :-
   fn_item_transform(adelung_1:'--->', Item_1, Item),
   fn_item_purify(Item, Item_2),
   !.

'MILESTONE':As:Es_1 ---> Es_2 :-
   append(
      ['MILESTONE':[type:begin|As]:[]|Es_1],
      ['MILESTONE':[type:end|As]:[]], Es_2).
X ---> X.


/* adelung_process_pairs(Pairs, Y) <-
      */

adelung_process_pairs(Pairs, Y) :-
   maplist_with_status_bar( adelung_process_definition,
      Pairs, Definitions ),
   Y = 'defs':Definitions,
   Path = 'results/adelung_1.xml',
   write_list(user, ['---> ', Path, '\n']),
   dwrite(xml, Path, Y).


/* fn_item_purify(Item_1, Item_2) <-
      */

fn_item_purify(Item_1, Item_2) :-
   dwrite(xml, 'results/fn_item_tmp.xml', Item_1),
   dread(xml, 'results/fn_item_tmp.xml', [Item_2]).


/* adelung_process_definition([Def_1, Key], Def_2) <-
      */

adelung_process_definition([Def_1, Key], Def_2) :-
   Ns := Def_1^content::'*',
   length(Ns, L),
   write_list(user, ['Phase 1: ', Key, ' (', L, ' items)\n']),
%  dwrite(xml, Def_1),
   maplist( adelung_modify_part(Key),
      Ns, Ms ),
   append(Ms, Ks),
%  write(user, Ks),
   !,
   items(Items, Ks, []),
   Def = def:['KEY':Key]:Items,
   !,
   fn_triple_condense(Def, Def_2).


/* adelung_modify_part(Key, Name, Names) <-
      */

adelung_modify_part(Key, Name, Names) :-
   atomic(Name),
   !,
   adelung_highlight_key(Key, Name, Name_1),
   Substitution_1 = [ [".", ". "], [",", ", "], [":", ": "] ],
   name_exchange_sublist(Substitution_1, Name_1, Name_2),
   Substitution_2 = [ ["  ", " "] ],
   name_exchange_sublist(Substitution_2, Name_2, Name_3),
   Substitution_3 = [ ["u. s. f.", "u.s.f."], ["s. f.", "s.f."],
      ["u. s. w.", "u.s.w."], ["d. i", "d.i"] ],
   name_exchange_sublist(Substitution_3, Name_3, Name_4),
   name_split_at_position([" "], Name_4, Names_4),
   list_exchange_elements(
      [['Anm.', remark:['Anm.']]], Names_4, Names).
adelung_modify_part(Key, Xml_1, [Xml_2]) :-
   fn_item_parse(Xml_1, T:As:Es_1),
   !,
   maplist( adelung_modify_part(Key),
      Es_1, Fs ),
   append(Fs, Es_2),
   Xml_2 = T:As:Es_2.


/* adelung_highlight_key(Key, Name_1, Name_2) <-
      */

adelung_highlight_key(Key, Name_1, Name_2) :-
   adelung_highlight_key_(Key, Name_1, Name_3),
   capital_letters(Key, Key_2),
   adelung_highlight_key_(Key_2, Name_3, Name_2).

adelung_highlight_key_(Key, Name_1, Name_2) :-
   name(Key, K1),
   concat(['<font color="#00ee00">', Key, '</font>'], Key_2),
   name(Key_2, K2),
   name_exchange_sublist([[K1, K2]], Name_1, Name_2).

capital_letters(Name_1, Name_2) :-
   ( character_lower_to_capital(Name_1, Name_2)
   ; character_capital_to_lower(Name_1, Name_2)
   ; Name_2 = Name_1 ).


/* items(Items, Xs, Ys) :-
      */

items([]) -->
   { true }.
items([I|Is]) -->
   item(I),
   items(Is).

item(I) -->
   xml_element(I).
item(I) -->
   token(I).
item(I) -->
   list_item(I).


/* xml_element(E, Xs, Ys) <-
      */

xml_element(T:As:Es_2) -->
   [E],
   { fn_item_parse(E, T:As:Es_1),
     items(Es_2, Es_1, []) }.


/* tokens(Ts, Xs, Ys) <-
      */

tokens([T|Ts]) -->
   ( token(T)
   ; xml_element(T) ),
   { \+ ( remark := T^tag::'*' ) },
   tokens(Ts).
tokens([]) -->
   { true }.


/* token(T, Xs, Ys) <-
      */

token(T) -->
   [X, Y],
   { adelung_numbering([X, Y], T) }.
token(T) -->
   [T],
   { \+ fn_item_parse(T, _:_:_),
%    write(user, '.'), ttyflush,
     \+ list_item_start(T, _, _) }.

adelung_numbering([X, 'f.'], T) :-
   number(X),
   concat(X, ' f.', T).
adelung_numbering([X, Y], T) :-
   member(X, ['S.', 'Kap.']),
   atomic(Y),
   member(D, [',', '.']),
   concat_possible(N, D, Y),
   number(N),
   concat([X, ' ', Y], T).
adelung_numbering([X, Y], T) :-
   atomic(X),
   member(D1, [',', '.']),
   concat_possible(N1, D1, X),
   ( number(N1)
   ; member(N1, ['I', 'II', 'III', 'IV', 'V']) ),
   atomic(Y),
   member(D2, [',', '.']),
   concat_possible(N2, D2, Y),
   ( number(N2)
   ; member(N2, ['I', 'II', 'III', 'IV', 'V']) ),
   concat([X, ' ', Y], T).


/* concat_possible(X, Y, Z) <-
      */

concat_possible(X, Y, Z) :-
   name(Y, Ys),
   name(Z, Zs),
   append(Xs, Ys, Zs),
   name(X, Xs).


/* list_item(Item, Xs, Ys) <-
      */

list_item(item:[number:N|Type]:Ts) -->
   [S],
   { list_item_start(S, N, Type) },
   tokens(Ts),
   { ! }.


/* list_item_start(X, Y, As) <-
      */

list_item_start(X, Y, [text:X, type:T, delimiter:D]) :-
   atomic(X),
   member(D, [')', '.']),
   concat_possible(Y, D, X),
   ( number(Y),
     T = arabic
   ; member(Y, ['I', 'II', 'III', 'IV', 'V']),
     T = roman
   ; member(Y, [a, b, c, d, e, f, g, h, i, j, k, l]),
     T = character ).
list_item_start(X, Y, [text:X, type:T, delimiter:'()']) :-
   atomic(X),
   name(X, Zs),
   append(Us, ")", Zs),
   append("(", Ys, Us),
   name(Y, Ys),
   ( number(Y),
     T = arabic
   ; member(Y, [a, b, c, d, e, f, g, h, i, j, k, l]),
     T = character ).


/******************************************************************/


