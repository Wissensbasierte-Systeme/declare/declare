

/******************************************************************/
/***                                                            ***/
/***             Alignment: Processing                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* alignpro_read_alignment(+Row, -Lem) <-
      */

alignpro_read_alignment(row(AtmA, AtmB), [LemA, LemB]) :-
   atom_chars(AtmA, ChsA),
   atom_chars(AtmB, ChsB),
   alignpro_chars_to_signs(ChsA, LemA),
   alignpro_chars_to_signs(ChsB, LemB).


/* alignpro_write_alignment(+Lem, -Row) <-
      */

alignpro_write_alignment([LemA, LemB], row(AtmA, AtmB)) :-
   alignpro_signs_to_chars(LemA, ChsA),
   alignpro_signs_to_chars(LemB, ChsB),
   atomic_list_concat(ChsA, AtmA),
   atomic_list_concat(ChsB, AtmB).


/* alignpro_find_strings(+SgnsA, +SgnsB, +Input) <-
      */

alignpro_find_strings(SgnA, SgnB, Input) :-
   csv_read_file(Input, Rows, [separator(59)]),
   maplist(alignpro_read_alignment, Rows, Lems0),
   include(alignlib_test_length, Lems0, Lems1),
   include(alignpro_test_strings(SgnA, SgnB), Lems1, Lems2),
   new(Win, view('Alignment Toolkit', size(40, 20))),
   send_list(Win?editor, [ editable(@off),
      font(font(screen, roman, 14)) ]),
   forall( member(Lem, Lems2),
      ( alignpro_write_alignment(Lem, row(AtmA, AtmB)),
        format(atom(Atom), '> LemA: ~w~n  LemB: ~w~n', [AtmA, AtmB]),
%       writeln(Atom),
        send(Win, insert(Atom)) ) ),
   send_list(Win, [point_to_top_of_file, open_centered]).


/*** implementation ***********************************************/


alignpro_chars_to_signs(['['|Chs], [Sgn|Sgns]) :-
   !,
   alignpro_chars_to_parts(Chs, Prt0, Prt1),
   atomic_list_concat(Prt0, Sgn),
   alignpro_chars_to_signs(Prt1, Sgns).
alignpro_chars_to_signs([Sgn|Chs], [Sgn|Sgns]) :-
   !,
   alignpro_chars_to_signs(Chs, Sgns).
alignpro_chars_to_signs([], []).

alignpro_chars_to_parts([']'|Chs], [], Chs) :-
   !.
alignpro_chars_to_parts([Ch|Chs0], [Ch|Chs1], Chs2) :-
   alignpro_chars_to_parts(Chs0, Chs1, Chs2).

alignpro_signs_to_chars([Sgn|Sgns], [Sgn|Chs]) :-
   atom_length(Sgn, 1),
   !,
   alignpro_signs_to_chars(Sgns, Chs).
alignpro_signs_to_chars([Sgn|Sgns], Chs2) :-
   !,
   atom_chars(Sgn, Chs0),
   alignpro_signs_to_chars(Sgns, Chs1),
   append([['['], Chs0, [']'], Chs1], Chs2).
alignpro_signs_to_chars([], []).

alignpro_test_strings(SgnA, SgnB, [[SgnA|_], [SgnB|_]]) :-
   !.
alignpro_test_strings(SgnA, SgnB, [[_|SgnsA], [_|SgnsB]]) :-
   alignpro_test_strings(SgnA, SgnB, [SgnsA, SgnsB]).


/******************************************************************/


