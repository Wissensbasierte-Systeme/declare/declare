

/******************************************************************/
/***                                                            ***/
/***                Alignment:  Rules                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* alignrul_apply_rules(+Input, +AtmA0, -AtmA1) <-
      */

alignrul_apply_rules(Input, AtmA0, AtmA1) :-
   alignpro_read_alignment(row(AtmA0, ''), [LemA0, []]),
   dread(xml, Input, [rules:[]:Rules]),
   alignrul_headshift(Rules, LemA0, LemA1),
   alignrul_tailshift(Rules, LemA1, LemA2),
   alignrul_bodyshift(Rules, LemA2, LemA3),
   alignrul_unlock(LemA3, LemA4),
   alignpro_write_alignment([LemA4, []], row(AtmA1, '')).


/*** implementation ***********************************************/


alignrul_headshift([rule:As:[]|_], LemA0, LemA1) :-
   memberchk(position:head, As),
   alignrul_signs_to_terms(As, List, Term),
   append(List, Sgns, LemA0),
   !,
   append([Term], Sgns, LemA1).
alignrul_headshift([_|Rules], LemA0, LemA1) :-
   !,
   alignrul_headshift(Rules, LemA0, LemA1).
alignrul_headshift([], LemA, LemA).

alignrul_tailshift([rule:As:[]|_], LemA0, LemA1) :-
   memberchk(position:tail, As),
   alignrul_signs_to_terms(As, List, Term),
   append(Sgns, List, LemA0),
   !,
   append(Sgns, [Term], LemA1).
alignrul_tailshift([_|Rules], LemA0, LemA1) :-
   !,
   alignrul_tailshift(Rules, LemA0, LemA1).
alignrul_tailshift([], LemA, LemA).

alignrul_bodyshift(Rules, LemA0, LemA2) :-
   Rules = [rule:As:[]|_],
   memberchk(position:body, As),
   alignrul_signs_to_terms(As, List, Term),
   append_swi([Sgns0, List, Sgns1], LemA0),
   !,
   append_swi([Sgns0, [Term], Sgns1], LemA1),
   alignrul_bodyshift(Rules, LemA1, LemA2).
alignrul_bodyshift([_|Rules], LemA0, LemA1) :-
   !,
   alignrul_bodyshift(Rules, LemA0, LemA1).
alignrul_bodyshift([], LemA, LemA).

alignrul_signs_to_terms(As, List, Term) :-
   memberchk(sgnA:SgnA, As),
   memberchk(sgnB:SgnB, As),
   atom_chars(SgnA, List),
   atom_chars(SgnB, Temp),
   Term =.. [locked|Temp].

alignrul_unlock([Term|Sgns0], Sgns3) :-
   Term =.. [locked|Sgns1],
   alignrul_unlock(Sgns0, Sgns2),
   append(Sgns1, Sgns2, Sgns3),
   !.
alignrul_unlock([Sgn|Sgns0], [Sgn|Sgns1]) :-
   alignrul_unlock(Sgns0, Sgns1),
   !.
alignrul_unlock([], []).


/* append_swi(ListOfLists, List) <-
      */

append_swi(ListOfLists, List) :-
   must_be(list, ListOfLists),
   append_swi_(ListOfLists, List).

append_swi_([], []).
append_swi_([L|Ls], As) :-
   append(L, Ws, As),
   append_swi_(Ls, Ws).


/******************************************************************/


