

/******************************************************************/
/***                                                            ***/
/***               Alignment: Toolkit                           ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library(csv)).
:- use_module(library(pce)).

:- set_prolog_flag(encoding, utf8).


/*** interface ****************************************************/


/* align_help <-
      */

align_help :-
   Str0 = 'Available Functions:',
   Str1 = '--------------------',
   Str2 = '> alignsub_create_table(+Input)',
   Str3 = '> alignsub_load_table(+Input)',
   Str4 = '> alignsub_modify_table',
   Str5 = '> alignsub_save_table(+Output)',
   Str6 = '> alignsub_csv_to_sql(+Input, +Output)',
   Str7 = '> alignpro_find_strings(+SgnsA, +SgnsB, +Input)',
   Str8 = '> alignrul_apply_rules(+Input, +AtmA0, -AtmA1)',
   atomic_list_concat([ '~n', '~w~n', '~w~n', '~w~n', '~w~n',
      '~w~n', '~w~n', '~w~n', '~w~n', '~w~n', '~n' ], Frm),
   format(user, Frm, [ Str0, Str1, Str2, Str3, Str4, Str5, Str6,
      Str7, Str8 ]).


/******************************************************************/


