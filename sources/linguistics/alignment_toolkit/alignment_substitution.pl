

/******************************************************************/
/***                                                            ***/
/***                  Alignment: Substitution                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* alignsub_create_table(+Input) <-
      */

alignsub_create_table(Input) :-
   send(@align_submatrix, clear),
   csv_read_file(Input, Rows, [separator(59)]),
   maplist(alignpro_read_alignment, Rows, Lems),
   alignsub_get_signs_from_lemmas(Lems, SgnsA, SgnsB),
   alignsub_calculate_frequencies(SgnsA, SgnsB, Lems).


/* alignsub_load_table(+Input) <-
      */

alignsub_load_table(Input) :-
   send(@align_submatrix, clear),
   csv_read_file(Input, Rows, [separator(59)]),
   forall( member(row(SgnA, SgnB, Freq), Rows),
      alignlib_set_frequency(SgnA, SgnB, Freq) ).


/* alignsub_modify_table <-
      */

alignsub_modify_table :-
   alignsub_get_signs_from_table([_|_], [_|_]),
   new(Fr, alignsub_interface),
   send(Fr, open_centered).


/* alignsub_save_table(+Output) <-
      */

alignsub_save_table(Output) :-
   alignsub_get_signs_from_table(SgnsA, SgnsB),
   findall( row(SgnA, SgnB, Freq),
      ( member(SgnA, SgnsA),
        member(SgnB, SgnsB),
        alignlib_get_frequency(SgnA, SgnB, Freq),
        Freq > 0 ),
      Rows ),
   csv_write_file(Output, Rows, [separator(59)]).


/* alignsub_csv_to_sql(+Input, +Output) <-
      */

alignsub_csv_to_sql(Input, Output) :-
   csv_read_file(Input, Rows, [separator(59)]),
   open(Output, write, Stream),
   forall( member(row(SgnA, SgnB, Freq), Rows),
      ( atom_number(AtmFreq, Freq),
        atomic_list_concat([ 'INSERT INTO ',
           'Subtable(SgnA, SgnB, Freq) VALUES (\'', SgnA,
           '\', \'', SgnB, '\', ', AtmFreq, ');' ], Stm),
        format(Stream, '~w~n', [Stm]) ) ),
   close(Stream).


/*** implementation ***********************************************/


alignsub_get_signs_from_lemmas(Lems, SgnsA1, SgnsB1) :-
   alignsub_collect_signs_from_lemmas(Lems, SgnsA0, SgnsB0),
   sort(SgnsA0, SgnsA1),
   sort(SgnsB0, SgnsB1).

alignsub_collect_signs_from_lemmas([Lem|Lems], SgnsA1, SgnsB1) :-
   !,
   Lem = [LemA, LemB],
   alignsub_collect_signs_from_lemmas(Lems, SgnsA0, SgnsB0),
   append(LemA, SgnsA0, SgnsA1),
   append(LemB, SgnsB0, SgnsB1).
alignsub_collect_signs_from_lemmas([], [], []).

alignsub_calculate_frequencies(SgnsA, SgnsB, Lems0) :-
   send(@align_counter, clear),
   include(alignlib_test_length, Lems0, Lems1),
   maplist(alignsub_count_in_lemma, Lems1),
   forall( member(SgnA, SgnsA),
      ( alignlib_count(point(SgnA, null), 0, NSgnA_),
        forall( member(SgnB, SgnsB),
           ( alignlib_count(point(SgnA, SgnB), 0, NSgnASgnB),
             ( NSgnASgnB == 0 -> true
             ; Freq is NSgnASgnB/NSgnA_,
               alignlib_set_frequency(SgnA, SgnB, Freq) ) ) ) ) ),
   send(@align_counter, clear).

alignsub_count_in_lemma([[], []]) :-
   !.
alignsub_count_in_lemma([[SgnA|SgnsA], [SgnB|SgnsB]]) :-
   alignlib_count(point(SgnA, null), 1, _),
   alignlib_count(point(SgnA, SgnB), 1, _),
   alignsub_count_in_lemma([SgnsA, SgnsB]).

alignsub_get_signs_from_table(SgnsA1, SgnsB1) :-
   new(Keys0, chain),
   send(@align_submatrix, for_all(message(Keys0, append, @arg1))),
   chain_list(Keys0, Keys1),
   maplist(alignlib_key_to_args, Keys1, SgnsA0, SgnsB0),
   sort(SgnsA0, SgnsA1),
   sort(SgnsB0, SgnsB1),
   free(Keys0).


/*** class definition *********************************************/


:- pce_begin_class(alignsub_interface, frame).

variable(alignsub_diag, window, both).
variable(alignsub_ctrl, window, both).
variable(alignsub_sgna, name, both).
variable(alignsub_sgnb, name, both).
variable(alignsub_sgns, dict, both).

initialise(Fr) :->
   send_super(Fr, initialise('Alignment Toolkit')),
   alignsub_get_signs_from_table(SgnsA, SgnsB),
   maplist(get(Fr), [ browser(selecta, SgnsA),
      browser(selectb, SgnsB), diagram,
      controls ], [Win0, Win1, Win2, Win3]),
   send_list(Fr, [ append(Win0), append(Win1), append(Win2),
      append(Win3), can_resize(@off) ]),
   get(Win0?list_browser?members, head, ItemA),
   get(Win1?list_browser?members, head, ItemB),
   send_list(Fr, [ alignsub_sgna(ItemA?key),
      alignsub_sgnb(ItemB?key), selecta(ItemA?key),
      selectb(ItemB?key) ]),
   send(Win0?list_browser, select(ItemA)),
   send(Win1?list_browser, select(ItemB)),
   send(Win3, below(Win2)),
   send(Win2, right(Win1)),
   send(Win1, right(Win0)).

browser(Fr, Fun:name, Chain:chain, Win:browser) :<-
   new(Win, browser(size := size(6,1))),
   ( Fun = selecta -> true
   ; send(Fr, alignsub_sgns(Win?list_browser?dict)),
     send_list( Win?list_browser,
        style(blue, style(background := light_blue)) ) ),
   send_list(Win?list_browser, [ font(font(screen, roman, 12)),
      select_message(message(Fr, Fun, @arg1?key)),
      selection_style(style(bold := @on)) ]),
   chain_list(Chain, Sgns),
   forall( member(Sgn, Sgns),
      ( atomic_list_concat([' ', Sgn, ' '], Label),
        send(Win, append(dict_item(Sgn, Label))) ) ),
   send_list(Win?tile, [ hor_shrink(0), hor_stretch(0),
      ver_shrink(100), ver_stretch(100) ]).

diagram(Fr, Win:window) :<-
   new(Win, alignsub_diagram),
   send(Fr, alignsub_diag(Win)),
   send_list(Win?tile, [ hor_shrink(0), hor_stretch(0),
      ver_shrink(0), ver_stretch(0) ]).

controls(Fr, Win:window) :<-
   new(Win, alignsub_controls( message(Fr, edit),
      message(Fr, update) )),
   send(Fr, alignsub_ctrl(Win)),
   send_list(Win?tile, [ hor_shrink(0), hor_stretch(0),
      ver_shrink(0), ver_stretch(0) ]).

selecta(Fr, SgnA:name) :->
   send(Fr, alignsub_sgna(SgnA)),
   send(Fr, update).

selectb(Fr, SgnB:name) :->
  send(Fr, alignsub_sgnb(SgnB)),
  send(Fr, update).

edit(Fr) :->
   get(Fr, alignsub_sgna, SgnA),
   get(Fr, alignsub_sgnb, SgnB),
   alignsub_get_signs_from_table(_, SgnsB),
   new(AtmFreqs, hash_table),
   forall( member(TmpSgnB, SgnsB),
      ( alignlib_get_frequency(SgnA, TmpSgnB, TmpFreq),
        atom_number(TmpAtmFreq, TmpFreq),
        send(AtmFreqs, append(TmpSgnB, TmpAtmFreq)) ) ),
   get(Fr?alignsub_ctrl, selection, AtmFreq),
   ( catch(atom_number(AtmFreq, Freq), _, fail),
     Freq > 0 -> send(AtmFreqs, append(SgnB, AtmFreq))
   ; send(AtmFreqs, append(SgnB, '0')) ),
   findall( TmpFreq,
      ( member(TmpSgnB, SgnsB),
        get(AtmFreqs, member(TmpSgnB), TmpAtmFreq),
        atom_number(TmpAtmFreq, TmpFreq) ),
      Freqs ),
   sumlist(Freqs, Sum),
   ( Sum > 0 -> FreqSum = Sum
   ; FreqSum = 1 ),
   forall( member(TmpSgnB, SgnsB),
      ( get(AtmFreqs, member(TmpSgnB), TmpAtmFreq),
        atom_number(TmpAtmFreq, TmpFreq0),
        TmpFreq1 is TmpFreq0 / FreqSum,
        alignlib_set_frequency(SgnA, TmpSgnB, TmpFreq1) ) ),
   send(Fr, update),
   free(AtmFreqs).

update(Fr) :->
   get(Fr, alignsub_sgna, SgnA),
   get(Fr, alignsub_sgnb, SgnB),
   alignsub_get_signs_from_table(_, SgnsB),
   forall( member(TmpSgnB, SgnsB),
      ( alignlib_get_frequency(SgnA, TmpSgnB, 0) ->
        get(Fr?alignsub_sgns, member(TmpSgnB), Item),
        send(Item, style(@default))
      ; get(Fr?alignsub_sgns, member(TmpSgnB), Item),
        send(Item, style(blue)) ) ),
   send(Fr?alignsub_diag, update(SgnA)),
   send(Fr?alignsub_ctrl, update(SgnA, SgnB)).

:- pce_end_class.

:- pce_begin_class(alignsub_diagram, window).

variable(alignsub_area, device, both).
variable(alignsub_bars, hash_table, both).

initialise(Win) :->
   alignsub_get_signs_from_table(_, SgnsB),
   length(SgnsB, LenSgnsB),
   send_super(Win, initialise(size := size(LenSgnsB * 6, 300))),
   new(Dev, device),
   send_list(Dev, [width(LenSgnsB * 6), height(300)]),
   send_list(Win, [ alignsub_area(Dev),
      alignsub_bars(new(Bars, hash_table)),
      display(Dev, point(0, 0)) ]),
   new(Count, number(0)),
   forall( member(SgnB, SgnsB),
      ( new(Bar, box(6, 0)),
        send_list(Bar, [fill_pattern(colour(light_blue)), pen(0)]),
        send(Dev, display(Bar, point(Count, 300))),
        send(Bars, append(SgnB, Bar)),
        send(Count, plus(6)) ) ).

resize(Win) :->
   send(Win?alignsub_area, y(Win?height - 300)).

update(Win, SgnA:name) :->
   alignsub_get_signs_from_table(_, SgnsB),
   forall( member(SgnB, SgnsB),
      ( alignlib_get_frequency(SgnA, SgnB, Freq),
        Height is round(Freq * 300),
        get(Win?alignsub_bars, member(SgnB), Bar),
        send_list(Bar, [ y(Bar?y + Bar?height - Height),
           height(Height) ]) ) ).

:- pce_end_class.

:- pce_begin_class(alignsub_controls, window).

variable(alignsub_item, text_item, both).

initialise(Win, Msg0:message, Msg1:message) :->
   alignsub_get_signs_from_table(_, SgnsB),
   length(SgnsB, LenSgnsB),
   send_super(Win, initialise(size := size(LenSgnsB * 6, 25))),
   new(Table, table),
   send_list(Table, [ cell_spacing(size(0, 0)), rules(none),
      width(Win?width) ]),
   get(Table, column(1, @on), Col1),
   new(Rub1, rubber(1, 0, 0)),
   send_list(Rub1, [maximum(80), minimum(80)]),
   send(Col1, rubber(Rub1)),
   get(Table, column(2, @on), Col2),
   new(Rub2, rubber(1, 100, 100)),
   send(Col2, rubber(Rub2)),
   get(Table, column(3, @on), Col3),
   new(Rub3, rubber(1, 0, 0)),
   send_list(Rub3, [maximum(40), minimum(40)]),
   send(Col3, rubber(Rub3)),
   get(Table, column(4, @on), Col4),
   new(Rub4, rubber(1, 0, 0)),
   send_list(Rub4, [maximum(40), minimum(40)]),
   send(Col4, rubber(Rub4)),
   send(Win, layout_manager(Table)),
   new(Row, alignsub_row(Msg0, Msg1)),
   send(Table, insert_row(1, Row)),
   send(Win, alignsub_item(Row?alignsub_item)).

selection(Win, AtmFreq:name) :<-
   get(Win?alignsub_item, selection, AtmFreq).

update(Win, SgnA:name, SgnB:name) :->
   alignlib_get_frequency(SgnA, SgnB, Freq),
   atom_number(AtmFreq, Freq),
   send(Win?alignsub_item, selection(AtmFreq)).

:- pce_end_class.

:- pce_begin_class(alignsub_row, table_row).

variable(alignsub_item, text_item, both).

initialise(Row, Msg0:message, Msg1:message) :->
   send_super(Row, initialise),
   new(Cell1, table_cell(text('Frequency:'))),
   send_list(Cell1, [ background(light_blue),
      cell_padding(size(6, 4)), halign(stretch) ]),
   new(Cell2, table_cell(new(Item, text_item))),
   send(Item, show_label(@off)),
   send(Item?value_text, background(white)),
   send_list(Cell2, [cell_padding(size(0, 0)), halign(stretch)]),
   new(Cell3, table_cell(new(Ctrl0, button(edit, Msg0)))),
%  send(Ctrl0, label(image('alignment_edit.bmp'))),
%  new(Img_0, image('alignment_edit.bmp')),
%  send(Ctrl0, label(Img_0)),
   send(Ctrl0, label('update')),
   send_list(Cell3, [cell_padding(size(0, 0)), halign(stretch)]),
   new(Cell4, table_cell(new(Ctrl1, button(cancel, Msg1)))),
%  send(Ctrl1, label(image('alignment_cancel.bmp'))),
   send(Ctrl1, label('undo')),
   send_list(Cell4, [cell_padding(size(0, 0)), halign(stretch)]),
   send_list(Row, [ append(Cell1), append(Cell2), append(Cell3),
      append(Cell4), alignsub_item(Item) ]).

:- pce_end_class.


/******************************************************************/


