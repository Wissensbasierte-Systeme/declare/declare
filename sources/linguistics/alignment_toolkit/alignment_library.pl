

/******************************************************************/
/***                                                            ***/
/***             Alignment:  Library                            ***/
/***                                                            ***/
/******************************************************************/


:- pce_global(@align_counter, new(hash_table)).
:- pce_global(@align_submatrix, new(hash_table)).


/*** interface ****************************************************/


/* alignlib_test_length(+Lem) <-
      */

alignlib_test_length([LemA, LemB]) :-
    length(LemA, Len),
    length(LemB, Len).


/* alignlib_count(+Point, +Step, -Val0) <-
      */

alignlib_count(Point, Step, Val0) :-
   ( alignlib_get(@align_counter, Point, Val0) ->
     true
   ; Val0 = 0 ),
   Val1 is Val0 + Step,
   alignlib_set(@align_counter, Point, Val1).


/* alignlib_set_frequency(+SgnA, +SgnB, +Freq) <-
      */

alignlib_set_frequency(SgnA, SgnB, Freq) :-
   ( Freq > 0 ->
     atom_number(AtmFreq, Freq),
     alignlib_set(@align_submatrix, point(SgnA, SgnB), AtmFreq)
   ; alignlib_delete(@align_submatrix, point(SgnA, SgnB)) ).


/* alignlib_get_frequency(+SgnA, +SgnB, -Freq) <-
      */

alignlib_get_frequency(SgnA, SgnB, Freq) :-
   ( alignlib_get(@align_submatrix, point(SgnA, SgnB), AtmFreq) ->
     atom_number(AtmFreq, Freq)
   ; Freq = 0 ).


/* alignlib_set(+Table, +Point, +Value) <-
      */

alignlib_set(Table, Point, Value) :-
   alignlib_point_to_key(Point, Key),
   send(Table, append(Key, Value)).


/* alignlib_get(+Table, +Point, -Value) <-
      */

alignlib_get(Table, Point, Value) :-
   alignlib_point_to_key(Point, Key),
   get(Table, member(Key), Value).


/* alignlib_delete(+Table, +Point) <-
      */

alignlib_delete(Table, Point) :-
   alignlib_point_to_key(Point, Key),
   ( get(Table, member(Key), _) ->
     send(Table, delete(Key))
   ; true ).


/* alignlib_key_to_args(+Key, -M, -N) <-
      */

alignlib_key_to_args(Key, M, N) :-
   alignlib_key_to_point(Key, point(M, N)).

/* alignlib_key_to_point(+Key, -Point) <-
      */

alignlib_key_to_point(Key, point(M, N)) :-
   atom_chars(Key, Chs),
   alignlib_split_chars(Chs, ';', ChsA, ChsB),
   atomic_list_concat(ChsA, M),
   atomic_list_concat(ChsB, N).


/* alignlib_point_to_key(+Point, -Key) <-
      */

alignlib_point_to_key(point(M, N), Key) :-
   atomic_list_concat([M, N], ';', Key).


/*** implementation ***********************************************/


alignlib_split_chars([Sep|Chs], Sep, [], Chs) :-
   !.
alignlib_split_chars([Ch|Chs], Sep, [Ch|ChsA], ChsB) :-
   alignlib_split_chars(Chs, Sep, ChsA, ChsB).


/******************************************************************/


