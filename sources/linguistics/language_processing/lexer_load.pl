

/******************************************************************/
/***                                                            ***/
/***       Language Processing:  Loading a File                 ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


/* load_structure_test(File, Content, Xml) <-
      */

load_structure_test(File, Content, Xml) :-
   Options = [dialect(xml), max_errors(10000), space(remove)],
   home_directory(Home),
   concat([Home, '/research/projects/TUSTEP/2009/',
      '2009_12_03/tests/', File], Path),
   load_structure(Path, Content, Options),
   xml_swi_to_fn(Content, [Xml]),
   dwrite(xml, aaa, Xml).


/******************************************************************/


