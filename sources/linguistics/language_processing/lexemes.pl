

/******************************************************************/
/***                                                            ***/
/***          Language Processing:  Morphemes                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* grimm_to_lexeme_facts <-
      */

grimm_to_lexeme_facts :-
   home_directory(Home),
   concat([Home, '/research/projects/TUSTEP/letters/letter.308/',
      'grimm_complete.xml'], File),
   lexicon_to_lexeme_facts(File).


/* lexeme_decomposition(Word, Lexemes) <-
      */

lexeme_decomposition(Word, Lexemes) :-
   atom_chars(Word, Characters),
   lexemes(Lexemes, Characters, []).


/*** implementation ***********************************************/


/* lexemes(Lexemes, Characters_1, Characters_2) <-
      */

lexemes([L|Ls]) -->
   lexeme_(L),
   lexemes(Ls).
lexemes([L]) -->
   lexeme(L).
lexemes([]) -->
   [].


/* lexeme(Lexeme, Characters_1, Characters_2) <-
      */

lexeme_(Lexeme, [X|Xs], Ys) :-
   lexeme(X, Cs),
   append(Cs, Ys, [X|Xs]),
   Ys \= [],
   atom_chars(Lexeme, Cs).
%  writeln(user, Lexeme).
lexeme(Lexeme, [X|Xs], Ys) :-
   lexeme(X, Cs),
   append(Cs, Ys, [X|Xs]),
   Ys = [],
   atom_chars(Lexeme, Cs).


/* lexicon_to_lexeme_facts(File) <-
      */

lexicon_to_lexeme_facts(File) :-
   retractall(lexeme(_, _)),
   dread(xml, File, [Xml]),
   findall( Lexeme,
      long_lexeme(Xml, Lexeme),
      Lexemes ),
   writeln(user, a),
   length_order_inverse(Lexemes, Lexemes_2),
   writeln(user, b),
   checklist( lexeme_to_database,
      Lexemes_2 ).

long_lexeme(Xml, Lexeme) :-
   Word := Xml/headword@headword,
   atom_chars(Word, Lexeme),
   length(Lexeme, N),
   N > 2.

length_order_inverse(Xs, Ys) :-
   findall( N-X,
      ( member(X, Xs),
        length(X, N) ),
      Pairs_1 ),
   sort(Pairs_1, Pairs_2),
   findall( Y,
      member(_-Y, Pairs_2),
      Ys ).

lexeme_to_database(Lexeme) :-
   first(Lexeme, X),
   asserta(lexeme(X, Lexeme)).


/******************************************************************/


