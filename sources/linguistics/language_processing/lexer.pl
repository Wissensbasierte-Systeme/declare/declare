

/******************************************************************/
/***                                                            ***/
/***       Lexer:  Extract Data                                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* lexer_extract_to_file(X, PosElements, Path) <-
      */

lexer_extract_to_file(X, PosElements, Path) :-
   lexer_extract_data_from_directory(X, PosElements, Y),
   dwrite(xml, Path, Y).


/*** implementation ***********************************************/


/* lexer_extract_data_from_directory(
         Directory, PosElements, Ys) <-
      */

lexer_extract_data_from_directory(Directory, PosElements, Ys) :-
   directory_contains_files(xml, Directory, Files),
   ( foreach(File, Files), foreach(Y, Ys_1) do
        lexer_extract_data_from_file(File, PosElements, Y) ),
   Ys = lexer:Ys_1.

lexer_extract_data_from_file(X, PosElements, Ys_1) :-
   dread(xml, X, [X1]), 
   findall( Y1,
      lexer_entry(X1, PosElements, Y1),
      Ys_2 ),
   sort(Ys_2, Ys_1).

lexer_entry(X, PosElements, Y) :-
   member(P, PosElements),
   ( P = Pos
   ; P = '*',
     true ),
   Entry := X/descendant::entry,
   entry_to_lemma(Entry, Lemma),
   entry_to_pos(Entry, Pos),
   Y = lemma:[value:Lemma, pos:Pos]:[].

entry_to_lemma(X, Y) :-
   [T1, [T2]] := X/descendant_or_self::form-[@type, /content::'*'],
   member(T1, [lemma, reflemma]),
   ( [Y] := T2/content::'*' 
   ; Y = T2, 
     \+ T2 = ref:_:_ ).

entry_to_pos(X, Y) :-
   ( Y := X/descendant_or_self::gramGrp/gram@type,
     !
   ; Y = undefined ).

extract_gramtypes(F, Categories, Pos, TriplePos, Ys) :-
   dread(xml, F, [FN]),
   findall( Y,
      ( Y := FN/descendant_or_self::'gramtype',
        concat('category_triple_', TriplePos, TriplePos_1),
        Triple := Y@TriplePos_1,
        atom_to_term(Triple, Triple_1, _),
        triple_position(Pos, Triple_1, Category),
        member(Category, Categories) ),
      Ys ).

triple_position(N, X, Y) :-
   nth(X, [Y-_-_, _-Y-_,  _-_-Y], N),
   !.


/******************************************************************/


