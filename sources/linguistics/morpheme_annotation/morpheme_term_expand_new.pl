

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Term Expand - New             ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(morpheme_terms_join, 2) :-
   Terms = [ (a+b)*1, ab*2 ],
   morpheme_terms_join(Terms, Term),
   writeq(Term).

test(morpheme_term_lists_combine, 1) :-
%  Css = [[a,b], [c,d,e], [f,g]],
   Css = [[a+b,ab], [cde, c+de, cd+e]],
   morpheme_term_lists_combine_all(Css, [1,2], Terms_1),
   writeln(Terms_1),
   morpheme_term_lists_combine(Css, [1,2], Terms_2),
   writeln(Terms_2).

test(morpheme_term_to_expansions, 1) :-
%  Term = an+bau+en,
   Term = an+(b+au)+en,
   morpheme_term_to_expansions(Term, Terms),
   writeln_list(Terms),
   checklist( morpheme_term_to_picture,
      Terms ),
   length(Terms, N),
   writeln(N).


/*** interface ****************************************************/


/* morpheme_term_to_expansions(Term, Terms) <-
      */

morpheme_term_to_expansions(Term, Terms) :-
   morpheme_term_to_components_and_annotations(
      Term, Components, Annotations),
   maplist( morpheme_terms_from_database_extended,
      Components, Css1 ),
   ( foreach(Component, Components), foreach(Cs1, Css1),
     foreach(Cs2, Css2) do
        Cs2 = [Component|Cs1] ),
   morpheme_term_lists_combine(Css2, Annotations, Terms),
   !.
   

/* morpheme_term_lists_combine(Css, Annotations, Terms) <-
      */

morpheme_term_lists_combine(Css, Annotations, Terms) :-
   size_of_cartesian_product(Css, N),
   ( N =< 50 ->
     morpheme_term_lists_combine_all(Css, Annotations, Terms)
   ; maplist( morpheme_terms_join,
        Css, Terms_1 ),
     morpheme_components_and_annotations_to_term(
        Terms_1, Annotations, Term_2),
     Terms = [Term_2] ).

morpheme_term_lists_combine_all(Css, Annotations, Terms) :-
   hypergraph_transversals(Css, Dss),
   ( foreach(Ds, Dss), foreach(T, Terms) do
        morpheme_components_and_annotations_to_term(
           Ds, Annotations, T) ).


/******************************************************************/


