

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Editor                        ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(
      morpheme_term_expand_mode, interactive).
:- dislog_variable_set(
      morpheme_editor_user, seipel).
:- dislog_variable_set(
      morpheme_editor_gui_mode, xxul).
%     morpheme_editor_gui_mode, xpce).


/*** tests ********************************************************/


test(morpheme_editor, select_morpheme_term(Type)) :-
   member(Type, ['OC', 'MC']),
   Text = lehrerschaft,
   Terms = [ (lehrer*plural*noun+schaft)*word*noun,
      (lehrer*plural+schaft)*word ],
   morpheme_editor_select_morpheme_term(
      Type, Text, Terms, Term),
   writeln(user, Term),
   ( Type = 'OC' ->
     morpheme_term_to_picture(Term)
   ; checklist( morpheme_term_to_picture,
        Term ) ).

test(morpheme_editor, start) :-
   Text = '(lehrer*noun*plural+schaft)*noun*word.',
   morpheme_editor_new(Editor),
   editor_insert(Editor, Text).

 
/*** interface ****************************************************/


/* morpheme_editor_start(User, Password) <-
      */

morpheme_editor_start(User, Password) :-
   mysql_odbc_connect(User, Password, mysql),
   morpheme_editor_start.

morpheme_editor_start :-
   morpheme_editor_new(_).


/* morpheme_editor_new(Editor) <-
      */

morpheme_editor_new(Editor) :-
   dislog_variable_get(morpheme_editor_gui_mode, xxul),
   !,
   dislog_variable_get( morpheme_editor_path,
      'morpheme_editor.xxul', Path ),
   dread(xml, Path, [Item]),
   xxul_item_to_gui(Item, Frame),
   xxul_morpheme_editor_frame_setting(Frame, Editor),
   send(Frame, open).

morpheme_editor_new(Editor) :-
   new(Frame, frame('Morpheme Annotation')),
   new(Dialog_A1, dialog),
   new(Dialog_A2, dialog),
   new(Dialog_A3, dialog),
   new_morpheme_editor_text_editor(Editor),
   checklist( send_append(Dialog_A1), [
%     'Show DB' - morpheme_term_database_show,
      'DB Connect' - morpheme_database_connect,
      'Give me work' - morpheme_editor_select_not_checked(Editor),
      'Expand & Update DB' -
%        morpheme_editor_change_line(Editor),
         morpheme_term_expand_and_update_database(Editor),
      'Update DB' - morpheme_term_database_update(Editor),
%     'Insert Term' -  morpheme_term_database_insert(Editor),
%     'Delete Term' - morpheme_term_database_delete(Editor),
%     'Prune Morpheme' - morpheme_term_database_prune(Editor),
      'Substring Search' - morpheme_term_database_show_like(Editor),
      'Exact Search' - morpheme_term_database_show(Editor) ] ),
   send(Dialog_A1, append, button('Quit',
      message(Frame, destroy)) ),
   send(Dialog_A2, append, button('Show Term',
      message(@prolog, morpheme_editor_term_to_picture, Editor)) ),
   send(Dialog_A1, left, Dialog_A2),
   send(Dialog_A3, below, Dialog_A1),
   send(Dialog_A1, append, Editor),
   send(Frame, append, Dialog_A1),
   new_morpheme_editor_picture(Picture),
   send(Dialog_A2, append, Picture),
   new_morpheme_editor_table_window(Table_Window),
   send(Dialog_A3, append, Table_Window),
   send(Frame, open).

new_morpheme_editor_text_editor(Editor) :-
   new(Editor, editor),
   dislog_variable_set(morpheme_editor, Editor),
   send(Editor, style, underline, style(underline := @on)),
   send(Editor, key_binding('#',
      message(@prolog, morpheme_editor_change_line, Editor))),
   send(Editor, size, size(90, 20)).

new_morpheme_editor_picture(Picture) :-
   new(Picture, picture),
   send(Picture, size, size(422, 318)),
   dislog_variable_set(morpheme_term_picture, Picture).

new_morpheme_editor_table_window(Table_Window) :-
   new(Table_Window, picture),
   send(Table_Window, size, size(1175, 200)),
   send(Table_Window, display, new(Tabular, tabular)),
   dislog_variable_set(morpheme_term_table, Table_Window:Tabular).


/* editor_send_key_binding(Editor, Key := Binding) <-
      */

editor_send_key_binding(Editor, Key := Binding) :-
   Binding =.. Xs,
   Message =.. [message, @prolog|Xs],
   send(Editor, key_binding(Key, Message)).

   
/* morpheme_editor(Editor) <-
      */

morpheme_editor(Editor) :-
   new(Dialog, dialog('Morpheme Annotation')),
   morpheme_editor(Dialog, Editor),
   send(Dialog, open).

morpheme_editor(Dialog, Editor) :-
   new(Editor, editor),
   send(Editor, size, size(80, 20)),
   send(Dialog, append, Editor),
   checklist( send_append(Dialog), [
      'Show DB' - morpheme_term_database_show,
      'Expand Term' - morpheme_editor_change_line(Editor),
      'Insert Term' - morpheme_term_database_insert(Editor),
      'Delete Term' - morpheme_term_database_delete(Editor),
      'Show Morpheme' - morpheme_term_database_show(Editor),
      'Prune Morpheme' - morpheme_term_database_prune(Editor) ] ),
   send(Dialog, append, button('Quit',
      message(Dialog, destroy)) ),
   send(Editor, style, underline, style(underline := @on)),
   send(Editor, key_binding('#',
      message(@prolog, morpheme_editor_change_line, Editor))).


/* morpheme_annotation_editor_display_table(Attributes, Tuples) <-
      */

morpheme_annotation_editor_display_table(Attributes, Tuples) :-
   dislog_variable_get(morpheme_editor_gui_mode, xxul),
   !,
   dislog_variable_get(morpheme_term_table, Win),
   send(Win, clear),
   xxul_morpheme_table_attributes(Win, Attributes),
   forall( member(Tuple, Tuples),
      xxul_morpheme_table_tuple(Win, Tuple) ).

morpheme_annotation_editor_display_table(Attributes, Tuples) :-
   ( dislog_variable_get(
        morpheme_term_table, Table_Window:Tabular) ->
     send(Tabular, destroy),
     xpce_display_table_in_window(
        Table_Window, Tabular_2, Attributes, Tuples),
     dislog_variable_set(
        morpheme_term_table, Table_Window:Tabular_2)
   ; xpce_display_table(Attributes, Tuples) ).


/*** implementation ***********************************************/


/* morpheme_term_database_insert(Editor) <-
      */

morpheme_term_database_insert(Editor) :-
   morpheme_editor_read_term(Editor, Term),
   morpheme_term_to_text(Term, Text),
   morpheme_term_database_insert_fact(
      morpheme_term_database(Text, Term)).


/* morpheme_term_database_delete(Editor) <-
      */

morpheme_term_database_delete(Editor) :-
   morpheme_editor_read_term(Editor, Term),
   morpheme_term_to_text(Term, Text),
   send(Editor, kill_line, 0),
   morpheme_term_database_delete_fact(
      morpheme_term_database(Text, Term)).


/* morpheme_term_database_prune(Editor) <-
      */

morpheme_term_database_prune(Editor) :-
   morpheme_editor_read_term(Editor, Term),
   morpheme_term_to_text(Term, Text),
   setof( Term_1,
      morpheme_term_database(Text, Term_1),
      Terms_1 ),
   morpheme_editor_select_morpheme_term(
      'MC', Text, Terms_1, Terms_2),
   ( foreach(Term_2, Terms_2) do
        morpheme_term_database_delete_fact(
           morpheme_term_database(Text, Term_2)) ).


/* morpheme_editor_change_line(Editor) <-
      */

morpheme_editor_change_line(Editor) :-
   morpheme_editor_read_term(Editor, Term_1),
   morpheme_term_expansion(step, Term_1, Term_2),
   !,
   send(Editor, kill_line, 0),
   morpheme_term_to_editor(Editor, Term_2),
   editor_insert(Editor, '.'),
%  morpheme_term_to_text(Term_2, Text_Form),
%  morpheme_term_purify(Term_2, Term),
%  morpheme_term_database_insert_tuple(
%     morpheme_term_database(Text_Form, Term)),
   morpheme_term_to_picture(Term_2),
   !.


/* morpheme_editor_read_term(Editor, Term) <-
      */

morpheme_editor_read_term(Editor, Term) :-
   morpheme_editor_read_atom(Editor, Atom),
   morpheme_atom_to_term(Atom, Term),
   !.

morpheme_editor_read_atom(Editor, Atom) :-
   editor_eat_line(Editor, Text),
   ( concat(Atom, '.', Text) ->
     true
   ; Atom = Text ).


/* morpheme_editor_select_not_checked(Editor) <-
      */

morpheme_editor_select_not_checked(Editor) :-
   morpheme_editor_read_term(Editor, Term),
   morpheme_term_to_text(Term, Text),
   N = 5,
   morpheme_term_database_not_checked(Text, N, Texts),
   checklist( editor_insert_with_new_line(Editor),
      Texts ).


/* morpheme_term_to_editor(Editor, Term) <-
      */

morpheme_term_to_editor(Editor, Term) :-
   morpheme_term_to_components_and_annotations(
      Term, Terms, Annotations),
   morpheme_annotations_prune(Annotations, Annotations_2),
   ( Annotations_2 = [],
     Terms = [T] ->
     editor_insert(Editor, T)
   ; morpheme_terms_to_editor(Editor, Terms),
     ( Annotations_2 = [] ->
       true
     ; editor_insert(Editor, '*'),
       morpheme_annotations_to_editor(Editor, Annotations_2) ) ).

morpheme_terms_to_editor(Editor, Terms) :-
   length(Terms, N), N >= 2,
   !,
   editor_insert(Editor, '('),
   morpheme_terms_to_editor_(Editor, Terms),
   editor_insert(Editor, ')').
morpheme_terms_to_editor(Editor, [T]) :-
   morpheme_term_to_editor(Editor, T).

morpheme_terms_to_editor_(Editor, [T1, T2|Ts]) :-
   morpheme_term_to_editor(Editor, T1),
   editor_insert(Editor, '+'),
   morpheme_terms_to_editor_(Editor, [T2|Ts]).
morpheme_terms_to_editor_(Editor, [T]) :-
   morpheme_term_to_editor(Editor, T).


/* morpheme_annotations_to_editor(Editor, Annotations) <-
      */

morpheme_annotations_to_editor(Editor, [A1, A2|As]) :-
   morpheme_annotation_to_editor(Editor, A1),
   editor_insert(Editor, '*'),
   morpheme_annotations_to_editor(Editor, [A2|As]).
morpheme_annotations_to_editor(Editor, [A]) :-
   morpheme_annotation_to_editor(Editor, A).
morpheme_annotations_to_editor(_, []).

morpheme_annotation_to_editor(Editor, As) :-
   is_list(As),
   !,
   morpheme_annotations_to_editor_with_underline(Editor, As).
morpheme_annotation_to_editor(Editor, A) :-
   editor_insert(Editor, A).


/* morpheme_annotations_to_editor_with_underline(Editor, As) <-
      */

morpheme_annotations_to_editor_with_underline(
      Editor, [A1, A2|As]) :-
   editor_insert_with_underline(Editor, A1),
   editor_insert(Editor, '*'),
   morpheme_annotations_to_editor_with_underline(
      Editor, [A2|As]).
morpheme_annotations_to_editor_with_underline(Editor, [A]) :-
   editor_insert_with_underline(Editor, A).
morpheme_annotations_to_editor_with_underline(_, []).


/* morpheme_editor_select_morpheme_term(
         Type, Text, Terms, Term) <-
      */

morpheme_editor_select_morpheme_term(_, _, Terms, Term) :-
   dislog_variable_get(morpheme_term_expand_mode, automatic),
   !,
   nth(1, Terms, Term).
morpheme_editor_select_morpheme_term(Type, Text, Terms, Term) :-
   morpheme_terms_to_question(Type, Text, Terms, Question),
   ddk_dialog(Question, N),
   ( Type  = 'OC' ->
     nth(N, Terms, Term)
   ; Type  = 'MC',
     nth_multiple(N, Terms, Term) ),
   !.

morpheme_terms_to_question(Type, Text, Terms, Question) :-
   findall( Answer,
      ( nth(N, Terms, Term),
        term_to_atom(Term, A),
        concat(' ', A, B),
        Answer = 'Answer':['ID':N, type:'AnswerChoice']:[
           'Text':[B] ] ),
      Answers ),
   Question = 'Question':['ID':'Choose a Term', type:Type]:[
      'Text':[Text], 'Answers':Answers ].


/* morpheme_editor_user_get(User) <-
      */

morpheme_editor_user_get(User) :-
   ( getenv('USER', User) ->
     true
   ; dislog_variable_get(morpheme_editor_user, User) ).


/******************************************************************/


