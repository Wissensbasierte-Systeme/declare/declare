

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Data Sources                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* morpheme_dictionary_files_sizes <-
      */

morpheme_dictionary_files_sizes :-
   morpheme_dictionary_files(Files),
   ddbase_aggregate( [F, length(Entry)],
      ( member(File, Files),
        name_split_at_position(["/"], File, Fs),
        last(Fs, F),
        Entry := doc(File)/entry ),
      Tuples ),
   xpce_display_table(['File', 'Entries'], Tuples),
   !.


/* morpheme_dictionary_files(Files) <-
      */

morpheme_dictionary_files(Files) :-
   dislog_variable_get(morpheme_dictionary_directory,
      'Data/WDG/', Directory),
   directory_contains_files(Directory, Fs),
   findall( File,
      ( member(F, Fs),
        file_has_ending(xml, F),
        concat(Directory, F, File) ),
      Files ).


/* morpheme_dictionary_affixes(Affixes) <-
      */

morpheme_dictionary_affixes(Affixes) :-
   dislog_variable_get(morpheme_dictionary_directory,
      'Morpheme_Term_Files/affixe.mts', File),
   read_file_to_lines(File, Affixes).


/******************************************************************/


