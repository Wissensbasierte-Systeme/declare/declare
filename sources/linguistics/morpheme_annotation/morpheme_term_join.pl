

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Term Join                     ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(morpheme_terms_join, 1) :-
   Term_1 = (a*6+b*7)*1+c+d+h+ef,
   Term_2 = a+bc+dh*5+e*3+f,
   morpheme_terms_join(Term_1, Term_2, Term_3),
   morpheme_term_to_picture(Term_1*1+Term_2*2+Term_3*'1*2', _),
   writeq(user, Term_3).


/*** interface ****************************************************/


/* morpheme_terms_join(Terms, Term) <-
      */

morpheme_terms_join([T|Ts], Term) :-
   fold( morpheme_terms_join,
      T, Ts, Term ).

morpheme_terms_join_2(Terms, Term) :-
   maplist( morpheme_term_to_components_and_annotations,
      Terms, Css, Ass ),
   append(Ass, Annotations_2),
   sort(Annotations_2, Annotations),
   first(Css, Component),
   morpheme_components_and_annotations_to_term(
      Component, Annotations, Term).


/* morpheme_terms_join(Term_1, Term_2, Term_3) <-
      */

morpheme_terms_join(Term_1, Term_2, Term_3) :-
   morpheme_term_to_components_and_annotations(
      Term_1, Components_1, Annotations_1),
   morpheme_term_to_components_and_annotations(
      Term_2, Components_2, Annotations_2),
   morpheme_terms_align(Components_1, Components_2, Groups),
%  writeq(user, Groups),
   maplist( morpheme_term_groups_join,
      Groups, Tss ),
   append(Tss, Components_3),
   morpheme_annotations_join(
      Annotations_1, Annotations_2, Annotations_3),
   morpheme_components_and_annotations_to_term(
      Components_3, Annotations_3, Term_3),
   !.


/* morpheme_term_groups_join(Terms_1-Terms_2, Terms_3) <-
      */

morpheme_term_groups_join([Term_1]-Terms_2, [Term_3]) :-
   morpheme_term_to_components_and_annotations(
      Term_1, [_], Annotations_1),
   !,
   morpheme_components_and_annotations_to_term(
      Terms_2, Annotations_1, Term_3).
morpheme_term_groups_join(Terms_1-[Term_2], [Term_3]) :-
   morpheme_term_to_components_and_annotations(
      Term_2, [_], Annotations_2),
   !,
   morpheme_components_and_annotations_to_term(
      Terms_1, Annotations_2, Term_3).
morpheme_term_groups_join(Terms_1-_, Terms_1).
 

/* morpheme_terms_join_2(Term_1, Term_2, Term_3) <-
      */

morpheme_terms_join_2(Term_1, Term_2, Term_1) :-
   atomic(Term_2),
   !.
morpheme_terms_join_2(Term_1, Term_2, Term_3) :-
   morpheme_term_to_components_and_annotations(
      Term_1, Components_1, Annotations_1),
   morpheme_term_to_components_and_annotations(
      Term_2, Components_2, Annotations_2),
   length(Components_1, N_1),
   length(Components_2, N_2),
   ( N_1 = N_2 ->
     ( foreach(C1, Components_1), foreach(C2, Components_2),
       foreach(C3, Components_3) do
          morpheme_terms_join(C1, C2, C3) )
   ; Components_2 = [_] ->
     Components_3 = Components_1
   ; Components_3 = Components_2 ),
   morpheme_components_and_annotations_to_term(
      Components_3, Annotations_2, Term),
   morpheme_term_enrich(Annotations_1, Term, Term_3).


/* morpheme_terms_align(Terms_1, Terms_2, Pairs) <-
      */

morpheme_terms_align(Ts_1, Ts_2, [Ts_1a-Ts_2a|Pairs]) :-
   append(Ts_1a, Ts_1b, Ts_1), Ts_1a \= [],
   append(Ts_2a, Ts_2b, Ts_2), Ts_2a \= [],
   maplist( morpheme_term_to_text,
      Ts_1a, Texts_1 ),
   maplist( morpheme_term_to_text,
      Ts_2a, Texts_2 ),
   concat(Texts_1, Text),
   concat(Texts_2, Text),
   !,
   morpheme_terms_align(Ts_1b, Ts_2b, Pairs).
morpheme_terms_align(Ts_1, Ts_2, [Ts_1-Ts_2]) :-
   Ts_1 \= [].
morpheme_terms_align(_, _, []).


/* morpheme_annotations_join(
         Annotations_1, Annotations_2, Annotations_3) <-
      */

morpheme_annotations_join(
      Annotations_1, Annotations_2, Annotations_3) :-
   append(Annotations_1, Annotations_2, Annotations),
   sort(Annotations, Annotations_3),
   !.
morpheme_annotations_join(
      Annotations_1, Annotations_2, Annotations_3) :-
   morpheme_annotations_enrich(
      Annotations_2, Annotations_1, Annotations_3).


/******************************************************************/


