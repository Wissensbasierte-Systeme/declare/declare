

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Term Visualization            ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(morpheme_term_picture_mode, same).


/*** tests ********************************************************/


test(morpheme_term_to_picture, 1) :-
   Text = '(huhu*aa+Nabel*mkwx*s+((aus*mb*adv+fall*bm*v)*konv*vst)*mk)*s',
   morpheme_atom_to_term(Text, Term),
   morpheme_term_to_picture(Term).

test(morpheme_term_to_picture, 2) :-
   Text = '[huhu_aa++Nabel_mkwx_s++[[aus_mb_adv++fall_bm_v]_konv_vst]_mk]_s',
   morpheme_atom_to_term_old(Text, Term),
   morpheme_term_transform(Term, Term_2),
   term_to_picture(Term_2),
   !.


/*** interface ****************************************************/


/* morpheme_editor_term_to_picture <-
      */

morpheme_editor_term_to_picture(Editor) :-
   morpheme_editor_read_term(Editor, Term),
   morpheme_term_to_picture(Term).


/* morpheme_term_to_picture(Term) <-
      */

morpheme_term_to_picture(Term) :-
   ( dislog_variable_get(morpheme_term_picture_mode, same),
     dislog_variable_get(morpheme_term_picture, Picture)
   ; true ),
   morpheme_term_to_picture(Term, Picture),
   !.


/* morpheme_term_to_picture(Term, Picture) <-
      */

morpheme_term_to_picture(Term, Picture) :-
   morpheme_term_to_compact_form(Term, Term_2),
   term_to_vertices_and_edges(Term_2, [], Vertices, Edges),
   morpheme_term_vertices_to_x_step(Vertices, X),
   Config = config:[]:[
      gxl_layout:[ y_variance:10,
         x_start:100, y_start:70, x_step:X, y_step:50 ]:[] ],
   vertices_and_edges_to_gxl_to_picture(
      Config, Vertices, Edges, Picture).

morpheme_term_vertices_to_x_step(Vertices, X) :-
   ddbase_aggregate( [maximum(N)],
      ( member(Vertex, Vertices),
        Vertex = _-Label-_-_-_,
        name(Label, Cs),
        length(Cs, N) ),
      [Max] ),
   X is 20 + Max * 5.


/*** implementation ***********************************************/


/* morpheme_atom_to_term_old(Text, Term) <-
      */

morpheme_atom_to_term_old(Text, Term) :-
   S1 = [["[","('"], ["_", "', '"], ["++", "') + ('"], ["]", "')"]],
   name_exchange_sublist(S1, Text, Text_1),
   S2 = [["'(", "("], [")'", ")"]],
   name_exchange_sublist(S2, Text_1, Text_2),
   concat(['(', Text_2, ''')'], Text_3),
   morpheme_atom_to_term(Text_3, Term).


/* morpheme_term_transform(Term_1, Term_2) <-
      */

morpheme_term_transform_(Term_1, Term_2) :-
   comma_structure_to_list(Term_1, [Term|Annotations]),
   term_structure_to_list(+, Term, Terms_1),
   Terms_1 = [_,_|_],
   !,
   maplist( morpheme_term_transform,
      Terms_1, Terms_2 ),
   concat_with_separator(Annotations, '_', Label),
   Term_2 =.. [Label|Terms_2].

morpheme_term_transform(Term_1, Term_2) :-
   comma_structure_to_list(Term_1, [Term|Annotations]),
   Term = Term_A + Term_B,
   !,
   morpheme_term_transform(Term_A, Term_C),
   morpheme_term_transform(Term_B, Term_D),
   concat_with_separator(Annotations, '_', Label),
   Term_2 =.. [Label, Term_C, Term_D].
morpheme_term_transform(Term_1, Term_2) :-
   comma_structure_to_list(Term_1, [Term|Annotations]),
   ( atomic(Term) -> Term_T = Term
   ; morpheme_term_transform(Term, Term_T) ),
   concat_with_separator(Annotations, '_', Label),
   Term_2 =.. [Label, Term_T].


/******************************************************************/


