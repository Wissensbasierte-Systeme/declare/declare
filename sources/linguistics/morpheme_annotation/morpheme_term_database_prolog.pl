

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Term Database in Prolog       ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      morpheme_term_database/2.
:- multifile
      morpheme_term_database/2.


/*** interface ****************************************************/


morpheme_term_database(lehrer, lehrer*noun).
morpheme_term_database(schaft, schaft*a).
morpheme_term_database(schaft, schaft*b).
morpheme_term_database(lehrerschaft, (lehrer+schaft)*c).


/******************************************************************/


