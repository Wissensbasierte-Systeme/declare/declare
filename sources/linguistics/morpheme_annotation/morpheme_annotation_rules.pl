

/*
verb(a).

noun(X) :-
   mc(X, A, B),
   text(A, [cba]),
   text(B, [fed]).
*/

noun(X) :-
   mc(X, A, B),
   ( noun(A)
   ; verb(A)
   ; adj(A) ),
   wbm(B, [chen, erchen, er, heit, lein, schaft, ei, erei,
      ien, ler, tum, el, in, ling, ung, en, erich, land]).

noun(X) :-
   mc(X, A, B, C),
   noun(A),
   fe(B, [s, es]),
   wbm(C, [tum]).

noun(X) :-
   mc(X, A, B, C),
   ( adj(A)
   ; noun(A) ),
   noun(B),
   wbm(C, [er, erei, ler, ner]).

noun(X) :-
   mc(X, A, B), noun(A),
   wbm(B, [ade, �r, azee, esse, ia, ier, in, ist, ium, ur, ane, arium,
      m, e, ette, iat, ine, it, ol, aner, ase, erie, eur, id, iere, iner,
      om, ar, at, ese, euse, ie, iker, ismus, iter, ose]).

noun(X) :-
   mc(X, A, B),
   noun(A), plural(A),
   text(B, [heit, tum, schaft]).

noun(X) :-
   mc(X, A, B),
   wbm(A, [ab, an, auf, aus, au�en, bei, binnen, erz, fehl, f�r,
      ge, gegen, grund, haupt, innen, miss, mit, nach, neben, ober,
      r�ck, sonder, �ber, um, un, unter, ur, vor, wider, zu, zwischen]),
   noun(B).

noun(X) :-
   mc(X, A, B),
   wbm(A, [a, an, ad, anti, co, de, des, dis, dys, en, ex, extra,
      in, infra, inter, ko, kon, konter, makro, meta, mikro, mini,
      multi, neo, non, per, post, pr�, pro, pseudo, re, retro,
      semi, sub, super, supra, trans, ultra, vize]),
   noun(B).

noun(X) :-
   mc(X, A, B),
   adj(A),
   wbm(B, [e, erchen, heit, keit, igkeit, nis, schaft, ling, tum]).

noun(X) :-
   mc(X, A, B),
   ( ordinal(A), text(B, [el])
   ; cardinal(A), text(B, [er]) ),
   wbm(B).

noun(X) :-
   mc(X, A, B),
   adj(A),
   wbm(B, [e, erie, ie, ismus, ist, it�t, anz]).

noun(X) :-
   mc(X, A, B),
   verb_inf(A),
   wbm(B, [e, el, er, ei, erei, ling, nis, sal, schaft, sel, ung]).

noun(X) :-
   mc(X, A, B),
   verb_ablaut(A),
   text(B, [e, el, er]).

noun(X) :-
   mc(X, A, B),
   verb(A),
   text(B, [ade, age, ament, and, ans, ant, anz, ar, at, ate, ateur,
      ation, ator, atur, ee, �e, �, ement, end, ens, ent, enz, erie,
      eur, euse, iment, ion, ist, it, iteur, ition, itor, itur, ium,
      or, ur]).

adj(X) :-
   mc(X, A, B),
   noun(A),
   wbm(B, [en, erig, ern, haft, ig, isch, lich, los, m��ig, sam, sch]).

adj(X) :-
   mc(X, A, B, C),
   noun(A),
   fe(B, [en, s, e, er, es]),
   wbm(C, [haft, m��ig, los]).

adj(X) :-
   mc(X, A, B, C),
   ( adj(A)
   ; noun(A) ),
   noun(B),
   wbm(C, [rig, erig, ig, isch, lich]).

adj(X) :-
   mc(X, A, B),
   noun(A),
   wbm(B, [al, alisch, anisch, ar, �r, arisch, atisch, ell, esk,
      etisch, ial, id, iell, in, inisch, istisch, itisch, iv,
      �s, orisch, ual, uell]).

adj(X) :-
   mc(X, A, B),
   adj(A),
   wbm(B, [ig, isch, istisch, lich, sam, erlei, fach]).

adj(X) :-
   mc(X, A, B),
   wbm(A, [ab, aller, au�er, binnen, erz, ge, grund, inner,
      miss, nach, ober, �ber, un, unter, ur, vor, wider, zwischen]),
   adj(B).

adj(X) :-
   mc(X, A, B),
   wbm(A, [a, anti, bi, de, dis, en, ex, extra, hyper, in, inter, intra,
      kon, konter, multi, non, para, post, pr�, pro, pseudo,
      retro, semi, sub, super, supra, trans, ultra, zirkum]),
   adj(B).

adj(X) :-
   mc(X, A, B),
   verb(A),
   wbm(B, [bar, rig, erig, haft, ig, isch, lich, sam]).

adj(X) :-
   mc(X, A, B, C),
   wbm(A, [un]),
   verb(B),
   text(C, [bar, ig, lich, sam]).

adj(X) :-
   mc(X, A, B, C),
   adj(A),
   verb(B),
   wbm(C, [ig]).

adj(X) :-
   mc(X, A, B),
   verb(A),
   wbm(B, [abel, ant, at, ativ, ent, ibel, il, it, itiv, iv]).

adj(X) :-
   mc(X, A, B),
   adv(A),
   wbm(B, [ig]).

verb(X) :-
   mc(X, A, B, C),
   noun(A),
   wbm(B, [el, er, ig]),
   fm(C, [n, en]).

verb(X) :-
   mc(X, A, B),
   noun(A),
   wbm(B, [ier, ifizier, isier]).

verb(X) :-
   mc(X, A, B, C, D),
   wbm(A, [be, en, ent, in, ver]),
   noun(B),
   wbm(C, [er, ig, ier, isier, el]),
   fm(D, [n, en]).

verb(X) :-
   mc(X, A, B, C),
   adj(A),
   wbm(B, [el, ig]),
   fm(C, [n, en]).

verb(X) :-
   mc(X, A, B, C),
   adj(A),
   wbm(B, [ier, ifizier, isier, izier]),
   fm(C, [n, en]).

verb(X) :-
   mc(X, A, B, C),
   verb(A), wbm(B, [el, er]),
   fm(C, [n, en]).

verb(X) :-
   mc(X, A, B),
   wbm(A, [be, ent, er, ge, hinter, miss, ver, zer]),
   verb(B).

verb(X) :-
   mc(X, A, B),
   wbm(A, [ab, an, auf, aus, bei, dar, ein, fehl, f�r,
      inne, los, nach, r�ck, vor, wieder, zu, zurecht, zwischen]),
   verb(B).

verb(X) :-
   mc(X, A, B),
   wbm(A, [durch, �ber, um, unter, wider]),
   verb(B).

verb(X) :-
   mc(X, A, B),
   wbm(A, [ab, ad, ante, co, de, des, dis, en, ex, in, inter, intro,
      kon, konter, kontra, per, post, pr�, pro, re, retro,
      sub, trans, zirkum]),
   verb(B).

verb(X) :-
   mc(X, A, B, C),
   wbm(A, [be, ver]),
   adv(B),
   fm(C, [n, en]).

adv(X) :-
   mc(X, A, B),
   noun(A),
   wbm(B, [halber, lings, mal, s, seits, w�rts, weise]).

adv(X) :-
   mc(X, A, B, C),
   noun(A), fe(B, [s]),
   wbm(C, [halber, weise]).

adv(X) :-
   mc(X, A, B),
   ( adj(A)
   ; pronoun(A) ),
   wbm(B, [falls, iter, lich, lings, mal, mals, s, seits, ens]).

adv(X) :-
   mc(X, A, B, C),
   ( adj(A)
   ; pronoun(A) ),
   fe(B, [e, en, er, es]),
   wbm(C, [dings, falls, mal, ma�en, seits, teils, weise]).

adv(X) :-
   mc(X, A, B, C),
   possesiv_pronoun(A),
   fe(B, [et]),
   wbm(C, [halben, wegen, willen]).

adv(X) :-
   mc(X, A, B),
   verb(A),
   wbm(B, [weise]).

adv(X) :-
   mc(X, A, B),
   adv(A),
   wbm(B, [mals]).

adv(X) :-
   mc(X, A, B),
   wbm(A, [aller]),
   adverb_ending_ens(B).

adv(X) :-
   mc(X, A, B),
   preposition(A),
   text(B, [mals, seits, w�rts]).

adv(X) :-
   mc(X, A, B),
   possesiv_pronoun(A),
   wbm(B, [ig]).

pronoun(X) :-
   mc(X, A, B, C),
   pronoun(A),
   fe(B, [es]),
   wbm(C, [gleichen]).


