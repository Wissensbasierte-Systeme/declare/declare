

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Term Analysis                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* morpheme_term_database_to_leaf_components(Components) <-
      */

morpheme_term_database_to_leaf_components(Components) :-
   findall( Cs,
      ( morpheme_term_database_to_term(Term),
%       morpheme_term_database(_, Term),
        write('.'), ttyflush,
        morpheme_term_to_leaf_components(Term, Cs) ),
      Css ),
   flatten(Css, Cs),
   write(' -> '), ttyflush,
   sort(Cs, Components),
   length(Components, N),
   writeln(user, N).


/* morpheme_term_to_leaf_components(Term, Components) <-
      */

morpheme_term_to_leaf_components(Term, Components) :-
   morpheme_term_to_components(Term, Cs),
   length(Cs, N),
   ( N > 1 ->
     maplist( morpheme_term_to_leaf_components,
        Cs, Css ),
     append(Css, Components)
   ; Cs = Components ).


/* morpheme_term_database_to_term(Term) <-
      */

morpheme_term_database_to_term(Term) :-
   mysql_use_database(mysql, morphemes),
   concat('select * from morpheme_decomposition ',
      'where text_form like \'%\';', Sql),
   odbc:odbc_query(mysql, Sql, Row, []),
   arg(4, Row, Atom_1),
   name_exchange_sublist([[[195, 164], [228]]], Atom_1, Atom_2),
   term_to_atom(Term, Atom_2).


/******************************************************************/


