

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Annotation                    ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      derived_morpheme_type/2.


/*** tests ********************************************************/


test(morpheme_term_to_type, N) :-
   Terms = [
      (lehrer*noun*plural+schaft)*word,
      (lehrer*noun+chen*wbm)*dito,
      lehrer*noun+s*fe+tum*wbm,
      lehrer*noun+ig*wbm,
      gelb*adj+ifizier*wbm+en*fm,
      (durch*wbm+lesen*verb)*word,
      (auf*bm*adv+lauf*bm*v)*mk*konv*s,
      (apfel*bm+(auf*bm*adv+lauf*bm*v)*mk*konv*s)
         *mk*komp_det*nhd*uer*s ],
   nth(N, Terms, Term),
   derived_morpheme_types(Term, Types),
   writeln(Term -> Types).


/*** basic predicates *********************************************/


/* derived_morpheme_types(Term, Types) <-
      */

derived_morpheme_types(Term, Types) :-
   findall( Type,
      derived_morpheme_type(Term, Type),
      Types ).


:- Types = [
      noun, pronoun, possesiv_pronoun, preposition,
      verb, adj, fe, fm, wbm,
      plural, ordinal, cardinal,
      verb_inf, verb_ablaut,
      adverb_ending_ens ],
   ( foreach(Type, Types) do
        multifile(Type/1),
        G1 =.. [Type, Term],
        assert( G1 :- morpheme_term_to_type(Term, Type) ),
        assert( derived_morpheme_type(Term, Type) :- G1 ),
        G2 =.. [Type, Term, Alternatives],
        assert( G2 :- morpheme_term_to_type_and_alternatives(
           Term, Type, Alternatives) ) ).


/* mc(Term, ...) <-
      */

mc(Term, A, B) :-
   morpheme_term_to_components(Term, [A, B]).
mc(Term, A, B, C) :-
   morpheme_term_to_components(Term, [A, B, C]).
mc(Term, A, B, C, D) :-
   morpheme_term_to_components(Term, [A, B, C, D]).


noun(Term) :-
   morpheme_term_to_type(Term, s).

verb(Term) :-
   morpheme_term_to_type(Term, v).

text(Term, Alternatives) :-
   morpheme_term_to_text(Term, Text),
   member(Text, Alternatives).


/******************************************************************/


