

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Term Expand                   ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      morpheme_term_expansion_cached/1.

:- dislog_variable_set(
      morpheme_term_expansion_mode, bulk ).


/*** interface ****************************************************/


/* morpheme_term_expansion(Mode, Term_1, Term_2) <-
      */

morpheme_term_expansions(Term, Terms) :-
   findall( T,
      morpheme_term_expansion(Term, T),
      Terms ).

morpheme_term_expansion(Term_1, Term_2) :-
   dabolish(morpheme_term_expansion_cached/1),
   dislog_variable_get(morpheme_term_expansion_mode, Mode),
   morpheme_term_expansion(Mode, Term_1, Term_2).

morpheme_term_expansion(iteration, Term_1, Term_2) :-
   morpheme_term_expansion(step, Term_1, Term_A),
   morpheme_term_purify(Term_A, Term_B),
   ( Term_1 = Term_B ->
     Term_2 = Term_1
   ; morpheme_term_expansion(iteration, Term_B, Term_2) ).

morpheme_term_expansion(step, Term_1, Term_2) :-
   findall( Term_2,
      morpheme_term_expansion_cached(Term_1->Term_2),
      Terms_2 ),
   Terms_2 \= [],
   !,
   member(Term_2, Terms_2).
morpheme_term_expansion(step, Term_1, Term_2) :-
   morpheme_term_expand_components(Term_1, Term_A),
   morpheme_term_expand_from_database(Term_A, Term_B),
   morpheme_term_expand_with_rules(Term_B, Term_C),
   morpheme_term_normalize(Term_C, Term_2),
   assert(morpheme_term_expansion_cached(Term_1->Term_2)),
   writeln(user, Term_1->Term_2),
   writeln(protocol, Term_1->Term_2).

morpheme_term_expansion(bulk, Term_1, Term_2) :-
   morpheme_term_expansion(bulk_all, Term_1, Terms_2),
   member(Term_2, Terms_2).

morpheme_term_expansion(bulk_all, Term, Terms) :-
   morpheme_term_to_components_and_annotations(
      Term, Components, Annotations),
   ( Components = [_] ->
     Terms_2 = [Term]
   ; maplist( morpheme_term_expansion(bulk_all),
        Components, Css1 ),
     ( foreach(Component, Components), foreach(Cs1, Css1),
       foreach(Cs2, Css2) do
          maplist( morpheme_terms_join(Component),
             Cs1, Cs2 ) ),
     morpheme_term_lists_combine(Css2, Annotations, Terms_2) ),
   writeq(user, 2:Term->Terms_2), nl(user),
   morpheme_terms_from_database_extended(Term, Terms_3),
   writeq(user, 3:Term->Terms_3), nl(user),
   ( foreach(Term_2, Terms_2), foreach(Term_4, Terms_4) do
        fold( morpheme_terms_join,
           Term_2, Terms_3, Term_4) ),
   writeq(user, 4:Term->Terms_4), nl(user),
   findall( Term_7,
      ( member(Term_4, Terms_4),
        morpheme_term_expand_with_rules(Term_4, Term_5),
        morpheme_term_normalize(Term_5, Term_6),
        morpheme_term_purify(Term_6, Term_7),
        writeq(user, Term_4->Term_5->Term_6->Term_7), nl(user) ),
      Terms_7 ),
   sort(Terms_7, Terms),
   writeq(user, 7:Term->Terms), nl(user),
   !.

morpheme_term_expansion(step_maximal, Term_1, Term_2) :-
   findall( Term_D,
      ( morpheme_term_expand_components(Term_1, Term_A),
        morpheme_term_expand_from_database(Term_A, Term_B),
        morpheme_term_expand_with_rules(Term_B, Term_C),
        morpheme_term_normalize(Term_C, Term_D),
        Term_D \= Term_1,
        assert(morpheme_term_expansion_cached(Term_1->Term_D)),
        writeln(user, Term_1->Term_D),
        writeln(protocol, Term_1->Term_D) ),
      Terms ),
   ( Terms \= [] ->
     member(Term_2, Terms)
   ; Term_2  = Term_1 ).

morpheme_term_expand_components(Term_1, Term_2) :-
   morpheme_term_to_components_and_annotations(
      Term_1, Components_1, Annotations),
   length(Components_1, N),
   ( N > 1 ->
     maplist( morpheme_term_expansion(step),
        Components_1, Components_2 ),
     morpheme_components_and_annotations_to_term(
        Components_2, Annotations, Term_2)
   ; Term_2 = Term_1 ).


/*** implementation ***********************************************/


/* morpheme_term_expand_with_rules(Term_1, Term_2) <-
      */

morpheme_term_expand_with_rules(Term_1, Term_2) :-
   derived_morpheme_types(Term_1, Annotations),
   morpheme_term_enrich(Annotations, Term_1, Term_2).


/* morpheme_term_expand_from_database(Term_1, Term_2) <-
      */

morpheme_term_expand_from_database(Term_1, Term_2) :-
   morpheme_terms_from_database_extended(Term_1, Terms),
   ( Terms \= [] ->
     member(Term, Terms),
%    morpheme_terms_join(Term, Term_1, Term_2)
     morpheme_terms_join(Term_1, Term, Term_2)
   ; Term_2 = Term_1 ).

/*
morpheme_term_expand_from_database(Term_1, Term_2) :-
   morpheme_terms_from_database(Term_1, Terms),
   ( Terms = [] ->
     Term_2 = Term_1
   ; Terms = [Term_2] -> true
   ; morpheme_editor_select_morpheme_term(
        'OC', Text, Terms, Term_2) ).
*/


/******************************************************************/


