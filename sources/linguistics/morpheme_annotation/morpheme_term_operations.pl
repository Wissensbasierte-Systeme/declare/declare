

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Term Operations               ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(
      morpheme_term_mode, term ).


/*** interface ****************************************************/


/* morpheme_term_to_text(Term, Text) <-
      */

morpheme_term_to_text(Term, Term) :-
   atomic(Term),
   !.
morpheme_term_to_text(Term, Text) :-
   morpheme_term_to_components(Term, Components),
   maplist( morpheme_term_to_text,
      Components, Texts ),
   concat(Texts, Text).

% morpheme_term_to_text(Term, Text) :-
%    morpheme_term_to_xml(Term, Xml),
%    morpheme_xml_to_written_form(Xml, Text).


/* morpheme_term_to_type(Term, Type) <-
      */

morpheme_term_to_type(Term, Type) :-
   morpheme_term_to_annotations(Term, _, Annotations),
   member(Type, Annotations).


/* morpheme_term_to_type_and_alternatives(
         Term, Type, Alternatives) <-
      */

morpheme_term_to_type_and_alternatives(Term, Type, Alternatives) :-
   morpheme_term_to_type(Term, Type),
   morpheme_term_to_text(Term, Text),
   member(Text, Alternatives).


/* morpheme_components_and_annotations_to_term(
         Components, Annotations, Term) <-
      */

morpheme_components_and_annotations_to_term(
      Components, Annotations, Term) :-
   dislog_variable_get(morpheme_term_mode, xml),
   !,
   morpheme_term_annotations_to_xml(Annotations, As),
   Term = m:As:Components.
morpheme_components_and_annotations_to_term(
      Components, Annotations, Term) :-
   morpheme_components_add_brackets(Components, Components_2),
   list_to_functor_structure(+, Components_2, T),
   list_to_functor_structure(*, [T|Annotations], Term).

morpheme_components_add_brackets(Cs_1, Cs_2) :-
   ( Cs_1 = [C|Cs],
     Cs \= [],
     functor(C, +, _) ->
     Cs_2 = [[C]|Cs]
   ; Cs_2 = Cs_1 ).


/* morpheme_term_to_components(Term, Components) <-
      */

morpheme_term_to_components(Term, Components) :-
   morpheme_term_to_components_and_annotations(
      Term, Components, _).


/* morpheme_term_to_components_and_annotations(
         Term, Components, Annotations) <-
      */

morpheme_term_to_components_and_annotations(
      Term, Components, Annotations) :-
   fn_item_parse(Term, m:_:Components),
   !,
   A := Term@a,
   functor_structure_to_list(*, A, Annotations).
morpheme_term_to_components_and_annotations(
      Term, Components, Annotations) :-
   morpheme_term_to_annotations(Term, T, Annotations),
   ( T = [T2] ->
     true
   ; T2 = T ),
   functor_structure_to_list(+, T2, Components).


/* morpheme_term_to_annotations(Term, T, Annotations) <-
      */

morpheme_term_to_annotations(Term, T, Annotations) :-
   functor_structure_to_list(*, Term, [T|Annotations]).


/* morpheme_term_to_compact_form(Term_1, Term_2) <-
      */

morpheme_term_to_compact_form(Term, Term) :-
   atomic(Term),
   !.
morpheme_term_to_compact_form(Term_1, Term_2) :-
   morpheme_term_to_components_and_annotations(
      Term_1, Components, Annotations),
   flatten(Annotations, Annotations_2),
   sort(Annotations_2, Annotations_3),
   maplist( morpheme_term_to_compact_form,
      Components, Terms ),
   ( Annotations_3 = [] ->
     F = ''
   ; list_to_functor_structure(*, Annotations_3, Annotation),
     term_to_atom(Annotation, F) ),
   Term_2 =.. [F|Terms].


/* morpheme_term_enrich(Annotations, Term_1, Term_2) <-
      */

morpheme_term_enrich(Annotations, Term_1, Term_2) :-
   dislog_variable_get(morpheme_term_mode, xml),
   !,
   fn_item_parse(Term_1, m:_:Es),
   let( Xs := Term_1@a ),
   append(Xs, Annotations, Ys),
   morpheme_term_annotations_to_xml(Ys, As2),
   Term_2 = m:As2:Es.
morpheme_term_enrich(Annotations, Term_1, Term_2) :-
   functor_structure_to_list(*, Term_1, [T|Annotations_1]),
   morpheme_annotations_enrich(
      Annotations, Annotations_1, Annotations_2),
   list_to_functor_structure(*, [T|Annotations_2], Term_2).


/* morpheme_annotations_enrich(Annotations, As_1, As_2) <-
      */

morpheme_annotations_enrich(Annotations, As_1, As_2) :-
   findall( A,
      ( member(A, Annotations),
        \+ morpheme_annotation_synonym_present(A, As_1),
        \+ morpheme_annotations_inconsistent(A, As_1) ),
      Annotations_2 ),
   append(Annotations_2, As_1, Xs),
   sort(Xs, Ys),
   append(Ys, [Annotations_2], As_2).


morpheme_annotation_synonym_present(A, As) :-
   morpheme_annotation_to_synonym(A, B),
   member(B, As).

morpheme_annotation_to_synonym_or_self(A, B) :-
   ( morpheme_annotation_to_synonym(A, B)
   ; B = A ).

morpheme_annotation_to_synonym(A, B) :-
   morpheme_annotations_synonyms(Synonyms),
   member(Xs, Synonyms),
   member(A, Xs),
   member(B, Xs).

morpheme_annotations_synonyms(Synonyms) :-
   Synonyms = [
      [v, verb], [s, noun, n] ].


morpheme_annotations_inconsistent(As) :-
   append(Xs, [A|Ys], As),
   append(Xs, Ys, Bs),
   morpheme_annotations_inconsistent(A, Bs).

morpheme_annotations_inconsistent(A, As) :-
   member(B, As),
   morpheme_annotations_inconsistent_(A, B).

morpheme_annotations_inconsistent_(A, B) :-
   morpheme_annotation_to_synonym_or_self(A, C),
   morpheme_annotation_to_synonym_or_self(B, D),
   morpheme_annotations_inconsistent__(C, D).

morpheme_annotations_inconsistent__(A, B) :-
   List = [ noun-verb, noun-adj, verb-adj, praefix-suffix ],
   ( member(A-B, List)
   ; member(B-A, List) ).


/* morpheme_annotations_prune(Annotations_1, Annotations_2) <-
      */

morpheme_annotations_prune(Annotations_1, Annotations_2) :-
   findall( A1,
      ( member(A1, Annotations_1),
        \+ is_list(A1),
        \+ ( member(A2, Annotations_1), member(A1, A2) ) ),
      Bs ),
   sort(Bs, As_1),
   findall( A2,
      ( member(A2, Annotations_1),
        is_list(A2) ),
      Cs ),
   append(Cs, Ds),
   sort(Ds, Es),
   ord_subtract(Es, As_1, As_2),
   ( As_2 = [] ->
     Annotations_2 = As_1
   ; append(As_1, [As_2], Annotations_2) ).


/* morpheme_term_subsumes(Term_1, Term_2) <-
      */

morpheme_term_subsumes(Term_1, Term_2) :-
   \+ atomic(Term_1),
   morpheme_term_to_components_and_annotations(Term_1, Cs_1, As_1),
   morpheme_term_to_components_and_annotations(Term_2, Cs_2, As_2),
   maplist( morpheme_term_subsumes,
      Cs_1, Cs_2 ),
   subset(As_2, As_1).
morpheme_term_subsumes(Term, Term).


/* morpheme_term_normalize(Term_1, Term_2) <-
      */

morpheme_term_normalize(Term, Term) :-
   atomic(Term),
   !.
/*
morpheme_term_normalize(Term_1, _) :-
   morpheme_term_to_components_and_annotations(
      Term_1, _, Annotations_1),
   morpheme_annotations_inconsistent(Annotations_1),
   !,
   fail.
*/
morpheme_term_normalize(Term_1, Term_2) :-
   morpheme_term_to_components_and_annotations(
      Term_1, Components_1, Annotations_1),
   maplist( morpheme_term_normalize,
      Components_1, Components_2 ),
   Substitution = [ [n,s], [noun,s], [verb,v],
      [[n],[s]], [[noun],[s]], [[verb],[v]] ],
   list_exchange_elements(Substitution, Annotations_1, Annotations_3),
   sort(Annotations_3, Annotations_2),
   ( member(v, Annotations_2),
     Components_2 = [X, Y],
     morpheme_term_normalize_fm(Y, Z) ->
     Components_3 = [X, Z]
   ; Components_3 = Components_2 ),
   morpheme_components_and_annotations_to_term(
      Components_3, Annotations_2, Term_2),
   !.

morpheme_term_normalize_fm(Term_1, Term_2) :-
   morpheme_term_to_components_and_annotations(
      Term_1, [Fm], Annotations_1),
   member(fm, Annotations_1),
   member(Fm, [en, n]),
   ( member([fm], Annotations_1) ->
     Annotations_2 = [fm, [fm]]
   ; Annotations_2 = [fm] ),
   morpheme_components_and_annotations_to_term(
      [Fm], Annotations_2, Term_2).
 

/* morpheme_term_purify(Term_1, Term_2) <-
      */

morpheme_term_purify(Term, Term) :-
   atomic(Term),
   !.
morpheme_term_purify(Term_1, Term_2) :-
   morpheme_term_to_components_and_annotations(
      Term_1, Components_1, Annotations_1),
   maplist( morpheme_term_purify,
      Components_1, Components_2 ),
   findall( A,
      ( member(A, Annotations_1),
        \+ is_list(A) ),
      Annotations ),
   sort(Annotations, Annotations_2),
   morpheme_components_and_annotations_to_term(
      Components_2, Annotations_2, Term_2),
   !.


/* morpheme_term_modify(Mode, Term_1, Term_2) <-
      Mode can be capital_to_lower or lower_to_capital. */

morpheme_term_modify(Mode, Term_1, Term_2) :-
   atomic(Term_1),
   !,
   concat(character_, Mode, Predicate),
   call(Predicate, Term_1, Term_2).
morpheme_term_modify(Mode, Term_1, Term_2) :-
   morpheme_term_to_components_and_annotations(
      Term_1, [C_1|Components], Annotations),
   morpheme_term_modify(Mode, C_1, C_2),
   morpheme_components_and_annotations_to_term(
      [C_2|Components], Annotations, Term_2).


/* morpheme_atoms_to_terms(Atoms, Terms) <-
      */

morpheme_atoms_to_terms([A|As], [T|Ts]) :-
   catch( morpheme_atom_to_term(A, T), _, false ),
   !,
   morpheme_atoms_to_terms(As, Ts).
morpheme_atoms_to_terms([_|As], Ts) :-
   morpheme_atoms_to_terms(As, Ts).
morpheme_atoms_to_terms([], []).


/* morpheme_atom_to_term(Atom, Term) <-
      */

morpheme_atom_to_term(Atom, Term) :-
   atom_to_term(Atom, Term, Substitutions),
   checklist( call,
      Substitutions ).


/******************************************************************/


