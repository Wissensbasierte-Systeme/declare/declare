

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Term Database                 ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(
      morpheme_term_database_connection, mysql),
   dislog_variable_set(
      morpheme_term_database_dsn, mysql).


/*** interface ****************************************************/


/* morpheme_database_connect(DSN, User, PW) <-
      */

morpheme_database_connect :-
   dislog_variable_get(morpheme_editor_gui_mode, xxul),
   !,
   dislog_variable_get( morpheme_editor_path,
      'morpheme_term_database.xxul', Path ),
   dread(xml, Path, [Item]),
   xxul_item_to_gui(Item, Frame),
   send(Frame, open).

morpheme_database_connect :-
   new(Dialog, dialog('DB Login')),
   send(Dialog, background, lightsteelblue3),
   send_list(Dialog, append, [
      new(N1, text_item('DSN')),
      new(N2, text_item('User')),
      new(N3, text_item('Password')),
      button(cancel, message(Dialog, destroy)),
      button(login, message(@prolog, morpheme_database_connect,
         N1?selection, N2?selection, N3?selection, Dialog ) ) ]),
   send(Dialog, default_button, login),
   send(Dialog, open).

morpheme_database_connect(DSN, User, PW, Dialog) :-
   ( morpheme_database_connect(DSN, User, PW) ->
     send(Dialog, destroy),
     ddk_message(['User:', User, ' connected with DSN:', DSN])
   ; ddk_message(['Error ! \nUser:',
        User, ' could not connect with DSN:', DSN]) ).

morpheme_database_connect(DSN, User, PW) :-
   dislog_variable_get(
      morpheme_term_database_connection, Connection_1),
   Connection_2 = morpheme_term_database,
   ( Connection_1 = Connection_2 ->
     odbc_disconnect(Connection_1)
   ; true ),
   mysql_odbc_connect(DSN, User, PW, Connection_2, multiple),
   dislog_variable_set(
      morpheme_term_database_connection, Connection_2),
   dislog_variable_set(
      morpheme_term_database_dsn, DSN).


/* morpheme_term_database_create <-
      */

morpheme_term_database_create :-
   dislog_variable_get(
      morpheme_term_database_connection, Connection),
   Database = morphemes,
   mysql_create_database(Connection, Database).


/* morpheme_term_database_create_table(
         morpheme_decomposition) <-
      */

morpheme_term_database_create_table(morpheme_decomposition) :-
   dislog_variable_get(
      morpheme_term_database_connection, Connection),
   Database = morphemes,
   Table = morpheme_decomposition,
   concat([ 'create table morpheme_decomposition ( ',
      'id int(10) AUTO_INCREMENT, ',
      'source varchar(20) DEFAULT '''', ',
      'text_form varchar(100) NOT NULL DEFAULT '''', ',
      'decomposition varchar(200) NOT NULL DEFAULT '''', ',
      'author varchar(15) DEFAULT '''', ',
      'timestamp varchar(25) DEFAULT '''', ',
      'checked tinyint(1) DEFAULT ''1'', ',
      'primary key (id) ) ',
      'ENGINE=InnoDB' ],
      Create_Statement ),
   mysql_create_table(
      Connection, Database:Table, Create_Statement).


/* morpheme_term_database_update_fact(
         morpheme_term_database(Id, Source, Text, Term)) <-
      */

morpheme_term_database_update_fact(
      morpheme_term_database(Id, Source, Text, Term)) :-
   morpheme_term_database_update_fact(
      morpheme_term_database(Id, Source, Text, Term, 1)).

morpheme_term_database_update_fact(
      morpheme_term_database(Id, Source, Text, Term, Checked)) :-
   ( atomic(Term) ->
     Atom = Term
   ; term_to_atom(Term, Atom) ),
   ddk_date_time(Time),
   morpheme_editor_user_get(Author),
   Pairs_Set = [source:Source,
      text_form:Text, decomposition:Atom,
      author:Author, timestamp:Time, checked:Checked],
   Pairs_Where = [id:Id],
   dislog_variable_get(morpheme_term_database_connection, Con),
   D:T = morphemes:morpheme_decomposition,
   writeq(user, mysql_update_table(Con, D:T, Pairs_Set, Pairs_Where)),
   catch( mysql_update_table(Con, D:T, Pairs_Set, Pairs_Where),
      _, true ).


/* morpheme_term_database_insert_fact(
         morpheme_term_database(Source, Text, Term)) <-
      */

morpheme_term_database_insert_fact(
      morpheme_term_database(Id, Source, Text, Term)) :-
   morpheme_term_database_insert_fact(
      morpheme_term_database(Id, Source, Text, Term, 1)).

morpheme_term_database_insert_fact(
      morpheme_term_database(_Id, Source, Text, Term, Checked)) :-
%  ( atomic(Term) ->
%    Atom = Term
%  ; term_to_atom(Term, Atom) ),
   term_to_atom(Term, Atom_2),
   name_exchange_sublist([["'", "''"]], Atom_2, Atom),
   ddk_date_time(Time),
   morpheme_editor_user_get(Author),
   Pairs = [source:Source,
      text_form:Text, decomposition:Atom,
      author:Author, timestamp:Time, checked:Checked],
   dislog_variable_get(morpheme_term_database_connection, Con),
   D:T = morphemes:morpheme_decomposition,
   catch( mysql_insert_tuple(Con, D:T, Pairs),
      _, true ).


/* morpheme_term_database_delete_fact(
         morpheme_term_database(Text, Term)) <-
      */

morpheme_term_database_delete_fact(
      morpheme_term_database(Id, Source, Text, Term)) :-
   ( atomic(Term) ->
     Atom = Term
   ; term_to_atom(Term, Atom) ),
   Pairs_Where = [id:Id, source:Source,
      text_form:Text, decomposition:Atom],
   dislog_variable_get(morpheme_term_database_connection, Con),
   D:T = morphemes:morpheme_decomposition,
   catch( mysql_delete_tuple(Con, D:T, Pairs_Where),
      _, true ).


/* morpheme_term_database(Text, Id, Source, Term) <-
      */

morpheme_term_database(Text, Term) :-
   morpheme_term_database(Text, _, _, Term).

morpheme_term_database(Text, Id, Source, Term) :-
   morpheme_term_database_to_tuples(Text, Tuples),
   member(Tuple, Tuples),
   Tuple = [Id, Source, Text, Atom|_],
   morpheme_atom_to_term(Atom, Term).

morpheme_term_database_2(Text, Id, Source, Term) :-
   morpheme_term_database_to_tuples(Text, Tuples),
   findall( Atom,
      ( member(Tuple, Tuples),
        Tuple = [_, _, Text, Atom|_] ),
      Atoms_1 ),
   sort(Atoms_1, Atoms_2),
   member(Atom, Atoms_2),
   once(member([Id, Source, Text, Atom|_], Tuples)),
   morpheme_atom_to_term(Atom, Term).


/* morpheme_term_database_to_tuples(Texts, Tuples) <-
      */

morpheme_term_database_to_tuples(Texts, Tuples) :-
   dislog_variable_get(
      morpheme_term_database_connection, Connection),
   Database = morphemes,
   Table = morpheme_decomposition,
   ( var(Texts),
     Condition = []
   ; is_list(Texts) ->
     length(Texts, N),
     multify(text_form, N, As),
     pair_lists(:, As, Texts, Pairs),
     list_to_functor_structure(';', Pairs, Condition)
   ; Texts = Text,
     Condition = [text_form:Text] ),
   mysql_table_select(
      Connection, Database:Table, Condition, Tuples).


/* morpheme_term_database_max_id(Id) <-
      */

morpheme_term_database_max_id(Id) :-
   dislog_variable_get(
      morpheme_term_database_connection, Connection),
   Database = morphemes,
   mysql_use_database(Connection, Database),
   Statement = 'select max(Id) from morpheme_decomposition',
   odbc_query_to_tuples(Connection, Statement, [], [[Id]]).


/* morpheme_terms_from_database(Term, Terms) <-
      */

morpheme_terms_from_database_extended(Term, Terms) :-
   morpheme_term_to_text(Term, Text),
   morpheme_terms_from_database(Term, Text, Terms_1),
   ( character_capital_to_lower(Text, Text_2) ->
     morpheme_terms_from_database(Term, Text_2, Terms_A),
     maplist( morpheme_term_modify(lower_to_capital),
        Terms_A, Terms_2 )
   ; character_lower_to_capital(Text, Text_2) ->
     morpheme_terms_from_database(Term, Text_2, Terms_A),
     maplist( morpheme_term_modify(capital_to_lower),
        Terms_A, Terms_2 )
   ; Terms_2 = [] ),
   append(Terms_1, Terms_2, Terms).

morpheme_terms_from_database(Term, Terms) :-
   morpheme_term_to_text(Term, Text),
   morpheme_terms_from_database(Term, Text, Terms).

morpheme_terms_from_database(Term, Text, Terms) :-
   morpheme_term_purify(Term, T1),
   setof( T2,
      ( morpheme_term_database(Text, T2),
        \+ morpheme_term_subsumes(T1, T2) ),
      Terms ),
   !.
morpheme_terms_from_database(_, _, []).


/* morpheme_term_database_show(Editor) <-
      */

morpheme_term_database_show :-
   xpce_display_morpheme_term_database(_).

morpheme_term_database_show(Editor) :-
   morpheme_editor_read_term(Editor, Term),
   morpheme_term_to_text(Term, Text),
   xpce_display_morpheme_term_database(Text).


/* morpheme_term_database_show_like(Editor) <-
      */

morpheme_term_database_show_like(Editor) :-
   morpheme_editor_read_term(Editor, Term),
   morpheme_term_to_text(Term, Text),
   xpce_display_morpheme_term_database_like(Text).


/* xpce_display_morpheme_term_database(Text) <-
      */

xpce_display_morpheme_term_database(Text) :-
   dislog_variable_get(
      morpheme_term_database_connection, Connection),
   Database = morphemes,
   Table = morpheme_decomposition,
   ( nonvar(Text) ->
     Pairs_Where = [text_form:Text]
   ; Pairs_Where = [] ),
   mysql_table_select(
      Connection, Database:Table, Pairs_Where, Tuples),
   morpheme_annotation_editor_display_table(Tuples),
   !.


/* xpce_display_morpheme_term_database_like(Text) <-
      */

xpce_display_morpheme_term_database_like(Text) :-
   dislog_variable_get(
      morpheme_term_database_connection, Connection),
   Database = morphemes,
   Table = morpheme_decomposition,
   ground(Text),
   mysql_use_database(Connection, Database),
   concat([ 'select * from ', Table, ' ',
      'where text_form like ''%', Text, '%'';' ],
      Statement),
   odbc_query_to_tuples(Connection, Statement, Tuples),
   morpheme_annotation_editor_display_table(Tuples),
   !.


/* morpheme_annotation_editor_display_table(Tuples) <-
      */

morpheme_annotation_editor_display_table(Tuples) :-
%  writeq(user, Tuples),
   Attributes = ['Id', 'Source', 'Morpheme', 'Decomposition',
      'Author', 'Time', 'Checked'],
   morpheme_annotation_editor_display_table(Attributes, Tuples).


/* morpheme_term_database_not_checked(Text, N, Texts) <-
      */

morpheme_term_database_not_checked(Text, N, Texts) :-
   dislog_variable_get(
      morpheme_term_database_connection, Connection),
   Database = morphemes,
   Table = morpheme_decomposition,
   ground(Text),
   mysql_use_database(Connection, Database),
   concat([ 'select distinct(decomposition) from ', Table, ' ',
      'where text_form like ''', Text, '%'' and checked=''0'';' ],
      Statement),
   findall( T,
      ( odbc_query(Connection, Statement, Row, []),
        Row =.. [row, T] ),
      Texts_2 ),
   first_n_elements(N, Texts_2, Texts).


/* morpheme_term_database_set_checked(Id) <-
      */

morpheme_term_database_set_checked(Id) :-
   dislog_variable_get(
      morpheme_term_database_connection, Connection),
   Database = morphemes,
   Table = morpheme_decomposition,
   mysql_use_database(Connection, Database),
   concat([ 'update ', Table, ' set checked=''1'' ',
      'where id=''', Id, ''';' ],
      Statement),
   odbc_query(Connection, Statement, Row, []),
   writeln(user, Row).


/******************************************************************/


