

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Knowledge                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* morpheme_annotation_to_attribute_value_pair(Annotation, A:V) <-
      */

morpheme_annotation_to_attribute_value_pair(A:V, A:V) :-
   !.
morpheme_annotation_to_attribute_value_pair(Annotation, A:V) :-
   ( A = existiert_in_Region,
     member(Annotation -> V, [
        hd ->    '#Hochdeutsch',
        md ->    '#Mitteldeutsch',
        omd ->   '#Ostmitteldeutsch',
        wmd ->   '#Westmitteldeutsch',
        od ->    '#Oberdeutsch',
        wod ->   '#Westoberdeutsch',
        nd ->    '#Niederdeutsch',
        '�r' ->  '#�berregional',
        uer ->   '#�berregional' ])
   ; A = existiert_in_Zeitraum,
     member(Annotation -> V, [
        ahd ->   '#Althochdeutsch',
        fnhd ->  '#Fr�hneuhochdeutsch',
        mhd ->   '#Mittelhochdeutsch',
        nhd ->   '#Neuhochdeutsch' ])
   ; A = hat_Quelle,
     member(Annotation -> V, [
        bmz ->   '#BMZ',
        ewb ->   '#ElsWB',
        dwb ->   '#Grimm',
        lx ->    '#Lexer',
        lwb ->   '#LothrWB',
        pwb ->   '#PfWB',
        rwb ->   '#RhWB',
        wdg ->   '#WDG' ])
   ; A = hat_WK,
     member(Annotation -> V, [
        adj ->   '#Adjektiv',
        adv ->   '#Adverb',
        art ->   '#Artikel',
        int ->   '#Interjektion',
        kon ->   '#Konjunktion',
        ptk ->   '#Partikel',
        pro ->   '#Pronomen',
        'pr�' -> '#Pr�position',
        praep -> '#Pr�position',
        sjk ->   '#Subjunktion',
        s ->     '#Substantiv',
        v ->     '#Verb',
        pa ->    '#Partizip',
        pa1 ->   '#Partizip I',
        pa2 ->   '#Partizip II' ])
   ; A = hat_Worttyp,
     member(Annotation -> V, [
        bw ->    '#Basiswort',
        der ->   '#Derivat',
        derv ->  '#Derivat',
        komp ->  '#Kompositum',
        deko ->  '#Determinativkompositum',
        komp_det -> '#Determinativkompositum',
        koko ->  '#Kopulativkompositum',
        poko ->  '#Possesivkompositum',
        kurz ->  '#Kurzwort',
        konv ->  '#Konversion' ]) ).
morpheme_annotation_to_attribute_value_pair(V, V:V).


/* morpheme_annotation_prune(As_1, As_2) <-
      */

morpheme_annotation_prune(As_1, As_2) :-
   member(X -> Type, [
      fe ->  'Fugenelement',
      bm ->  'Basismorphem',
      fm ->  'Flexionsmorphem',
      mk ->  'komlexes_Morphem',
      wbm -> 'Wortbildungsmorphem' ]),
   member(X:X, As_1),
   delete(As_1, X:X, As_3),
   As_2 = [type:Type|As_3].
morpheme_annotation_prune(As, As).


/* morphem_type_to_suffix(Type, suffix) <-
      */

morphem_type_to_suffix('Basismorphem', bm).
morphem_type_to_suffix('Flexionsmorphem', fm).
morphem_type_to_suffix('Wortbildungsmorphem', wbm).
morphem_type_to_suffix('komlexes_Morphem', mk).
morphem_type_to_suffix('Morphemkomplex', mk).
morphem_type_to_suffix('Variet�tenlemma', vl).


/******************************************************************/


