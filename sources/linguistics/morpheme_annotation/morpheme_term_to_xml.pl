

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Terms and XML                 ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(morpheme_term_to_xml_and_owl, N) :-
   Terms = [
      ( apfel*bm + ( auf*bm*adv + lauf*bm*v )*mk*konv*s )
          *mk*komp_det*nhd*uer*s,
      ( ver*wbm*praep + ( blass*bm*adj + en*fm*v )*mk*derv )
          *mk*v*dwb*nhd*uer,
      ( a*bm + b*bm + c*bm )*mk ],
   nth(N, Terms, Term),
   morpheme_term_to_xml(Term, Xml),
   dwrite(xml, Xml),
   mopheme_xml_to_owl(Xml, Owl),
   dwrite(xml, Owl).


/*** interface ****************************************************/


/* morpheme_term_to_xml_short(Term, Xml) <-
      */

morpheme_term_to_xml_short(Term, Xml) :-
   morpheme_term_to_components_and_annotations(
      Term, Components_1, Annotations),
   ( Components_1 = [C], atomic(C) ->
     Components_2 = [C]
   ; maplist( morpheme_term_to_xml_short,
        Components_1, Components_2 ) ),
   morpheme_term_annotations_to_xml(Annotations, As),
   Xml = m:As:Components_2.

morpheme_term_annotations_to_xml(Annotations, As) :-
   ( list_to_functor_structure(*, Annotations, A) ->
     As = [a:A]
   ; As = [] ).


/* morpheme_term_to_xml(Term, Xml) <-
      */

morpheme_term_to_xml(Term, Xml) :-
   morpheme_term_to_annotations(Term, T, Annotations),
   maplist( morpheme_annotation_to_attribute_value_pair,
      Annotations, As_1 ),
   morpheme_term_to_components(T, Ts),
   ( length(Ts, 1) ->
     Xs = Ts
   ; maplist( morpheme_term_to_xml,
        Ts, Xs ) ),
   morpheme_annotation_prune(As_1, As_2),
   Xml = morpheme:As_2:Xs,
   !.


/* mopheme_xml_to_owl(Xml, Owl) <-
      */

mopheme_xml_to_owl(Xml, Owl) :-
   mopheme_xml_to_owls(Xml, [Owl]),
   !.

mopheme_xml_to_owls(Xml, Owls) :-
   Xml = morpheme:As:Es,
   member(type:Type, As),
   delete(As, type:Type, As_2),
   ( foreach(A:V, As_2), foreach(P, Properties) do
        P = A:['rdf:resource':V]:[] ),
   maplist( mopheme_xml_to_owls,
      Es, Es_2 ),
   morphemes_to_kernel_and_rest(Es_2, Owls_2),
   morpheme_xml_to_written_form(Xml, Word),
   S = hat_Schriftform:['Schriftform':['rdf:ID':Word]:[]],
   Xs = [S|Properties],
   ( Type = 'komlexes_Morphem' ->
     concat(Word, '_vl', Id_vl),
     Vl = 'Variet�tenlemma':['rdf:ID':Id_vl]:Xs,
     M = 'Morphemkomplex':[realisiert:[Vl]|Owls_2]
   ; M = Type:Xs ),
   morphem_type_to_suffix(Type, Suffix),
   concat([Word, '_', Suffix], Id),
   Morphem := M*[@'rdf:ID':Id],
   Owls = [Morphem].
mopheme_xml_to_owls(_, []).


/* morphemes_to_kernel_and_rest([M1, M2], Ms) <-
       */

morphemes_to_kernel_and_rest([[M1], [M2]], Ms) :-
   T1 := M1/tag::'*',
   T2 := M2/tag::'*',
   \+ member(T1, ['Wortbildungsmorphem', 'Flexionsmorphem']),
   member(T2, ['Wortbildungsmorphem', 'Flexionsmorphem']),
   !,
   Ms = ['enth�lt_Kern':[M1], 'enth�lt_Rest':[M2]].
morphemes_to_kernel_and_rest([M1, M2], Ms) :-
   !,
   Ms = ['enth�lt_Rest':M1, 'enth�lt_Kern':M2].
morphemes_to_kernel_and_rest(Ms, Ms).


/* morpheme_xml_to_written_form(Xml, Text) <-
      */

morpheme_xml_to_written_form(Xml, Text) :-
   findall( T,
      [T] := Xml/descendant_or_self::morpheme::[
         @type = Type -> Type \= 'komlexes_Morphem' ; true ]
         /content::'*',
      Ts ),
   concat(Ts, Text).


/******************************************************************/


