

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Editor                        ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(
      morpheme_editor_update_item_lines, 3).


/*** interface ****************************************************/


/* morpheme_term_expand_and_update_database(Source, Term) <-
      */

morpheme_term_database_update(Editor) :-
   morpheme_editor_read_term(Editor, Term),
   morpheme_term_expand_and_update_database(no, '', Term).

morpheme_term_expand_and_update_database(Editor) :-
   morpheme_editor_read_term(Editor, Term),
   morpheme_term_expand_and_update_database(yes, '', Term).

morpheme_term_expand_and_update_database(Source, Term) :-
   morpheme_term_expand_and_update_database(yes, Source, Term).

morpheme_term_expand_and_update_database(Expand, Source, Term) :-
   morpheme_term_to_text(Term, Text_1),
   character_change_between_lower_and_capital(Text_1, Text_2),
   Texts = [Text_1, Text_2],
   findall( [Id, Source_2, Text, Atom, delete],
      ( member(Text, Texts),
        morpheme_term_database(Text, Id, Source_2, T),
        term_to_atom(T, Atom) ),
      Tuples_Delete ),
   ( morpheme_term_tuples_delete_to_unique_source(
        Tuples_Delete, Source_2) -> true
   ; Source_2 = Source ),
%  writeln(user, found_tuples_delete),
   findall( ['', Source_2, Text_1, Atom, insert],
      ( ( Expand = yes ->
          morpheme_term_expansion(Term, Term_1)
        ; Term_1 = Term ),
        morpheme_term_purify(Term_1, Term_2),
        \+ morpheme_term_database(Text_1, _, _, Term_2),
        term_to_atom(Term_2, Atom) ),
      Tuples_Insert_2 ),
%  writeln(user, found_tuples_insert),
   sort(Tuples_Insert_2, Tuples_Insert),
   append(Tuples_Delete, Tuples_Insert, Tuples_1),
   morpheme_term_number_tuples(Tuples_1, Tuples_2),
   morpheme_editor_update_table(Tuples_2).

morpheme_term_number_tuples(Tuples_1, Tuples_2) :-
   findall( [N|Tuple],
      nth(N, Tuples_1, Tuple),
      Tuples_2 ).

morpheme_term_tuples_delete_to_unique_source(Tuples, Source) :-
   findall( X,
      member([_, X, _, _, _], Tuples),
      Xs ),
   sort(Xs, [Source]).

 
/*** implementation ***********************************************/


/* morpheme_editor_update_table(Tuples) <-
      */

morpheme_editor_update_table(Tuples) :-
   dislog_variable_get(morpheme_editor_gui_mode, xxul),
   !,
   dislog_variable_get( morpheme_editor_path,
      'morpheme_editor_updates.xxul', Path ),
   dread(xml, Path, [Item_0]),
   xxul_morpheme_editor_updates_build_frame( Item_0,
      Tuples, Item_1 ),
   xxul_item_to_gui(Item_1, Frame),
   forall( member(Tuple, Tuples),
      xxul_morpheme_editor_updates_edit_frame(Frame, Tuple) ),
   send(Frame, open).

morpheme_editor_update_table(Tuples) :-
   new(Frame, frame('Morpheme Updates')),
   new(Dialog, dialog),
   send(Dialog, background, white),
   morpheme_editor_update_row(Dialog,
      [0, 'Id', 'Source', 'Morpheme', 'Decomposition', void]),
   checklist( morpheme_editor_update_row(Dialog),
      Tuples ),
   send(Frame, append, Dialog),
   send(Frame, open),
   !.


/* morpheme_editor_update_row(Dialog, Tuple) <-
      */

morpheme_editor_update_row(Dialog, Tuple) :-
   Tuple = [N, Id, Source, Text, Term, Mode],
   send(Dialog, gap, size(2,4)),
   send(Dialog, append, new(E_Id, editor)),
   send(Dialog, append, new(E_Source, editor), right),
   send(Dialog, append, new(E_Text, editor), right),
   send(Dialog, append, new(E_Term, editor), right),
   editor_send_key_binding(
      E_Term, '#' := morpheme_editor_change_line(E_Term)),
   editor_insert(E_Id, Id),
   editor_insert(E_Source, Source),
   editor_insert(E_Text, Text),
   editor_insert(E_Term, Term),
   Y = 16,
   send(E_Id, reference, point(0, Y)),
   send(E_Source, reference, point(0, Y)),
   send(E_Text, reference, point(0, Y)),
   send(E_Term, reference, point(0, Y)),
   ( Mode = void ->
     rgb_to_colour([14.5, 14.5, 15.99], Colour),
     send(E_Id, colour, black),
     send(E_Source, colour, black),
     send(E_Text, colour, black),
     send(E_Term, colour, black)
   ; Xs = [N, E_Id, E_Source, E_Text, E_Term, Mode],
     morpheme_editor_update_row_buttons(Dialog, Xs, Colour) ),
   send(E_Id, background, Colour),
   send(E_Source, background, Colour),
   send(E_Text, background, Colour),
   send(E_Term, background, Colour),
   dislog_variable_get(morpheme_editor_update_item_lines, Lines),
   send(E_Id, size, size(10, Lines)),
   send(E_Source, size, size(10, Lines)),
   send(E_Text, size, size(20, Lines)),
   send(E_Term, size, size(40, Lines)).
%  send(E_Term, size, size(40, Lines)).

morpheme_editor_update_row_buttons(Dialog, Xs, Colour) :-
   Xs = [N, E_Id, E_Source, E_Text, E_Term, Mode],
   ( Mode = insert ->
     Image_Ins_Del = image('file.bm'),
     rgb_to_colour([15.2, 15.2, 15.99], Colour)
   ; Mode = delete ->
     Image_Ins_Del = image('16x16/trashcan.xpm'),
     rgb_to_colour([15.6, 15.6, 15.99], Colour) ),
   ddk_dialog_append_button(Dialog, Button_Ins_Del, '',
      morpheme_editor_update_row(Mode, N,
         E_Id, E_Source, E_Text, E_Term), right),
   ddk_dialog_append_button(Dialog, Button_Update, '',
      morpheme_editor_update_row(update, N,
         E_Id, E_Source, E_Text, E_Term), right),
   ddk_dialog_append_button(Dialog, Button_Check, '',
      morpheme_editor_check_row(E_Id), right),
   send(Button_Check, selection, image('16x16/done.xpm')),
   send(Button_Check, reference, point(0, 14)),
   send(Button_Ins_Del, selection, Image_Ins_Del),
   send(Button_Ins_Del, reference, point(0, 14)),
   send(Button_Update, selection, image('16x16/redo.xpm')),
   send(Button_Update, reference, point(0, 14)),
   assert(morpheme_editor_update_row_button(
      N, E_Text, E_Term, Button_Ins_Del)),
   ddk_dialog_append_button(Dialog, Button_Tree, 'Tree',
      morpheme_editor_term_to_picture(E_Term), right),
   send(Button_Tree, selection, image('16x16/graph.xpm')),
   send(Button_Tree, reference, point(0, 14)).


/* morpheme_editor_update_row(
         Mode, N, E_Id, E_Source, E_Text, E_Term) <-
      */

morpheme_editor_update_row(
      Mode, _, E_Id, E_Source, E_Text, E_Term) :-
   dislog_variable_get(morpheme_editor_gui_mode, xxul),
   !,
   ( morpheme_editor_read_term(E_Id, Id) -> true
   ; Id = 0 ),
   morpheme_editor_read_term(E_Source, Source),
   morpheme_editor_read_term(E_Text, Text),
   morpheme_editor_read_term(E_Term, Term),
   concat(['morpheme_term_database_', Mode, '_fact'], P),
   Goal =.. [P, morpheme_term_database(Id, Source, Text, Term)],
   writeq(user, Goal), nl(user),
%  call(P, morpheme_term_database(Id, Source, Text, Term)),
   call(Goal),
   ( Mode = insert ->
     morpheme_term_database_max_id(Id_2),
     send(E_Id, kill_line, 0),
     editor_insert(E_Id, Id_2)
   ; true ).

morpheme_editor_update_row(
      Mode, N, E_Id, E_Source, E_Text, E_Term) :-
   ( morpheme_editor_read_term(E_Id, Id) -> true
   ; Id = 0 ),
   morpheme_editor_read_term(E_Source, Source),
   morpheme_editor_read_term(E_Text, Text),
   morpheme_editor_read_term(E_Term, Term),
   writeln(user, morpheme_editor_update_row(Mode, N,
      Id, Source, Text, Term)), ttyflush,
   concat(['morpheme_term_database_', Mode, '_fact'], P),
   Goal =.. [P, morpheme_term_database(Id, Source, Text, Term)],
   writeq(user, Goal), nl(user),
%  call(P, morpheme_term_database(Id, Source, Text, Term)),
   call(Goal),
   morpheme_editor_update_row_button(
      N, E_Text, E_Term, Button_Ins_Del),
   ( member(Mode, [insert, delete]) ->
     send(Button_Ins_Del, destroy)
   ; true ),
   ( Mode = insert ->
     morpheme_term_database_max_id(Id_2),
     send(E_Id, kill_line, 0),
     editor_insert(E_Id, Id_2)
   ; true ).

morpheme_editor_check_row(E_Id) :-
   morpheme_editor_read_term(E_Id, Id),
   morpheme_term_database_set_checked(Id),
   send(E_Id, colour, red).


/* ddk_dialog_append_button(
         Dialog, Button, Label, Goal, Position) <-
      */

ddk_dialog_append_button(Dialog, Button, Label, Goal, Position) :-
   Goal =.. Xs,
   Message =.. [message, @prolog|Xs],
   send(Dialog, append,
      new(Button, button(Label, Message)), Position).


/******************************************************************/


