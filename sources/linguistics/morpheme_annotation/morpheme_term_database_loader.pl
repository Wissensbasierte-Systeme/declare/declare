

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Term Database Loader          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* morpheme_dictionary_affixes_to_database <-
      */

morpheme_dictionary_affixes_to_database :-
   morpheme_dictionary_affixes(Atoms),
   Source = 'Affixes',
   ( foreach(Atom, Atoms) do
        atom_to_term(Atom, Term, _),
        morpheme_term_to_text(Term, Key),
        morpheme_term_database_insert_fact(
           morpheme_term_database(_, Source, Key, Atom, 1)) ).


/* morpheme_dictionary_file_to_database(File) <-
      */

morpheme_dictionary_files_to_database :-
   morpheme_dictionary_files(Files),
   checklist_with_status_bar(
      morpheme_dictionary_file_to_database,
      Files ).

morpheme_dictionary_file_to_database(File) :-
   forall(
      morpheme_dictionary_file_to_triple(File, [Key, Source, W]),
      ( morpheme_dictionary_prepare_morpheme_and_decomposition(
           Key, W, Morpheme, Decomposition),
        morpheme_term_database_insert_fact(
           morpheme_term_database(
              _, Source, Morpheme, Decomposition, 0)) ) ).

morpheme_dictionary_file_to_triple(File, [Key, Source, W]) :-
   Entry := doc(File)/entry,
   [Text, Source] := Entry-[@key, @id],
   ( W := Entry/gramGrp@word_class ->
     true
   ; W = '--' ),
   name_split_at_position([" "], Text, Keys),
   member(Key, Keys).

morpheme_dictionary_prepare_morpheme_and_decomposition(
      Key, W, Morpheme, Decomposition) :-
   ( morpheme_key_to_decomposition_(Key, Atom_1) -> true
   ; Atom_1 = Key ),
   morpheme_decomposition_atom_to_term(Atom_1, Atom_2),
   ( W = '--' ->
     term_to_atom(Atom_2, Atom_3)
   ; term_to_atom(Atom_2*W, Atom_3) ),
   morpheme_decomposition_atom_nicer_2(Atom_3, Decomposition),
   morpheme_decomposition_atom_nicer_2(Key, Morpheme).

morpheme_decomposition_atom_to_term(Atom, Term) :-
   morpheme_decomposition_atom_nicer(Atom, A),
   atom_to_term(A, Term, _).

morpheme_decomposition_atom_nicer(Atom_1, Atom_2) :-
   Substitution = [["\'", "\\\'"], [" + ", "\' + \'"]],
   name_exchange_sublist(Substitution, Atom_1, Atom),
   concat(['\'', Atom, '\''], Atom_2).

morpheme_decomposition_atom_nicer_2(Atom_1, Atom_2) :-
   Substitution = [["\'", "\\\'"], [" + ", "\' + \'"]],
   name_exchange_sublist(Substitution, Atom_1, Atom_2).


/* morpheme_decomposition_file_to_prolog(File) <-
      */

morpheme_decomposition_file_to_prolog :-
   dislog_variable_get(morpheme_dictionary_directory,
      'wdg_decomposition/wdg_decomposition.xml', File),
   morpheme_decomposition_file_to_prolog(File).

morpheme_decomposition_file_to_prolog(File) :-
   abolish(morpheme_key_to_decomposition/2),
   forall( [K, D] := doc(File)/entry-[@key, @decomposition],
      assert(morpheme_key_to_decomposition(K, D)) ).


/* morpheme_key_to_decomposition_(Key, Atom) <-
      */

morpheme_key_to_decomposition_(Key, Atom) :-
   ( character_capital_to_lower(Key, Key_2),
     morpheme_key_to_decomposition(Key_2, Atom_2),
     character_lower_to_capital(Atom_2, Atom)
   ; morpheme_key_to_decomposition(Key, Atom) ).


/* morpheme_term_file_to_database(Annotations, File) <-
      */

morpheme_term_file_to_database(File) :-
   morpheme_term_file_to_database([], File).

morpheme_term_file_to_database(Annotations, File) :-
   ( var(File) ->
     get(@finder, file(exists := @on, directory := '.'), F)
   ; F = File ),
   ddk_message(['Loading morpheme term file ', F, ' ...']),
   read_file_to_lines(F, Atoms),
   morpheme_atoms_to_database(Annotations, Atoms).


/* morpheme_database_expand(Annotations) <-
      */

morpheme_database_expand(Annotations) :-
   dislog_variable_get(output_path,
      'morpheme_database_expand.txt', File),
   findall( Term,
      morpheme_term_database(_, Term),
      Terms_1 ),
   sort(Terms_1, Terms_2),
   predicate_to_file( File,
        morpheme_terms_expand_in_database(Annotations, Terms_2) ).


/* morpheme_file_expand_in_database(Annotations, File) <-
      */

morpheme_file_expand_in_database(File) :-
   morpheme_file_expand_in_database([], File).

morpheme_file_expand_in_database(Annotations, File) :-
   ( var(File) ->
     get(@finder, file(exists := @on, directory := '.'), F)
   ; F = File ),
   ddk_message(['Expanding morpheme term file ', F, ' ...']),
   read_file_to_lines(F, Atoms),
   morpheme_atoms_expand_in_database(Annotations, Atoms).


/* morpheme_atoms_to_database(Annotations, Atoms) <-
      */

morpheme_atoms_to_database(Annotations, Atoms) :-
   checklist_with_status_bar(
      morpheme_atom_to_database(Annotations),
      Atoms ).


/* morpheme_atom_to_database(Annotations, Atom) <-
      */

morpheme_atom_to_database(Annotations, Atom) :-
   catch( morpheme_atom_to_term(Atom, Term_1), _, false ),
   !,
   morpheme_components_and_annotations_to_term(
      [Term_1], Annotations, Term_2),
   morpheme_term_to_morpheme_database(Term_2),
   ttyflush.
morpheme_atom_to_database(Atom) :-
   write_list(user, ['The term ''', Atom,
      ''' could not be inserted !\n']),
   ttyflush.

morpheme_term_to_morpheme_database(Term) :-
   morpheme_term_to_text(Term, Text),
   Checked = 0,
   morpheme_term_database_insert_fact(
      morpheme_term_database(Text, Term, Checked)).


/* morpheme_atoms_expand_in_database(Annotations, Atoms) <-
      */

morpheme_atoms_expand_in_database(Annotations, Atoms) :-
   morpheme_atoms_to_terms(Atoms, Terms),
   morpheme_terms_expand_in_database(Annotations, Terms).


/* morpheme_terms_expand_in_database(Annotations, Terms) <-
      */

morpheme_terms_expand_in_database(Annotations, Terms) :-
   dislog_variable_switch(
      morpheme_term_expand_mode, Mode, automatic),
   checklist_with_status_bar(
      morpheme_term_expand_in_database(Annotations),
      Terms ),
   dislog_variable_set(morpheme_term_expand_mode, Mode).


/* morpheme_term_expand_in_database(Annotations, Term) <-
      */

morpheme_term_expand_in_database(Annotations, Term) :-
   morpheme_components_and_annotations_to_term(
      [Term], Annotations, Term_A),
   morpheme_term_purify(Term_A, Term_B),
   morpheme_term_expansion(Term_B, Term_C),
   ( Term_C \= Term_B ->
     assert(morpheme_term_expansion_done(Term = Term_B -> Term_C)),
     writeln(Term = Term_B -> Term_C),
%    term_to_atom(Term = Term_B -> Term_C, Message),
%    Goal = ddk_message([Message]),
%    thread_create(Goal, _, [detached(true)]),
     morpheme_term_to_text(Term, Text),
     morpheme_term_database_delete_fact(
        morpheme_term_database(Text, Term)),
     Checked = 0,
     morpheme_term_database_insert_fact(
        morpheme_term_database(Text, Term_C, Checked))
   ; true ).
   

/******************************************************************/


