

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  WDG Word Class Extraction              ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      wdg_entry/2.


/*** tests ********************************************************/


test(wdg_file_extract_word_class, a) :-
   dislog_variable_get( morpheme_dictionary_directory,
      'WDG/wgda.sgml', File_1 ),
   File_2 = a,
   wdg_file_extract_word_class(File_1, File_2).


/*** interface ****************************************************/


/* wdg_files_xml_to_prolog(Directory, X-Y) <-
      */

wdg_files_xml_to_prolog(Directory, X-Y) :-
   name(X, [N]),
   name(Y, [M]),
   forall( between(N, M, I),
      ( name(Suffix, [I]),
        concat([Directory, wgd, Suffix, '.xml'], File),
        wdg_file_xml_to_prolog(File),
        writeln(user, File), ttyflush ) ).

wdg_file_xml_to_prolog(File) :-
   dread(xml, File, [Item]),
   forall( Entry := Item/entry,
      wdg_entry_xml_to_prolog(Entry) ).

wdg_entry_xml_to_prolog(Entry) :-
   forall( wdg_entry_xml_to_key(Entry, Key),
      assert(wdg_entry(Key, Entry)) ).

wdg_entry_xml_to_key(Entry, Key) :-
   Name := Entry@key,
   name_split_at_position([" "], Name, Names_1),
   maplist( downcase_atom,
      Names_1, Names_2 ),
   sort(Names_2, Names_3), 
   member(Key, Names_3).


/* wdg_files_extract_word_class(Directory, X-Y) <-
      */

wdg_files_extract_word_class(Directory, X-Y) :-
   name(X, [N]),
   name(Y, [M]),
   forall( between(N, M, I),
      ( name(Suffix, [I]),
        concat([Directory, wgd, Suffix, '.sgml'], File_1),
        concat([Directory, wdg_, Suffix, '.xml'], File_2),
        wdg_file_extract_word_class(File_1, File_2),
        writeln(user, File_2), ttyflush ) ).


/* wdg_file_extract_word_class(File_1, File_2) <-
      */

wdg_file_extract_word_class(File_1, File_2) :-
   dread(sgml, File_1, [Item]),
   findall( Item_2,
      ( Item_1 := Item/descendant::entry,
        wdg_entry_extract_word_class(Item_1, Item_2) ),
      Items ),
   Options = [encoding('utf-8')],
   dwrite(xml, File_2, entries:Items, Options).

wdg_entry_extract_word_class(Entry_1, Entry_2) :-
   fn_item_parse(Entry_1, T:As:_),
   wdg_entry_extract_gram_group(Entry_1, Gram_Grp),
   Entry_2 = T:As:[Gram_Grp],
   !.

wdg_entry_extract_gram_group(Entry, Gram_Grp) :-
   member(T, [gramgrp, gramGrp]),
   [Text] := Entry/T/content::'*',
   member(S -> C:G, [
      der -> noun:m,
      die -> noun:f,
      das -> noun:n ]),
   concat(S, _, Text),
   !,
   Gram_Grp = gramGrp:[word_class:C, gender:G]:[].
wdg_entry_extract_gram_group(Entry, Gram_Grp) :-
   member(T, [gramgrp, gramGrp]),
   [Text] := Entry/T/content::'*',
   member(S -> C, [
      'Vb.' -> verb,
      'Adj.' -> adj,
      'part. Adj.' -> part_adj,
      'Präp.' -> prep,
      'Indef. pron.' -> indef_pronoun,
      'Konj.' -> konj,
      'Ausruf' -> exclamation,
      'Adv.' -> adv ]),
   concat('/<typ1>', S, T),
   name_contains_name(Text, T),
   !,
   Gram_Grp = gramGrp:[word_class:C]:[].
wdg_entry_extract_gram_group(_, Gram_Grp) :-
   Gram_Grp = gramGrp:[]:[].


/******************************************************************/


