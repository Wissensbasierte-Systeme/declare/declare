

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  WDG - XML to Text                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* wdg_file_xml_to_text(File_1, File_2) <-
      */

wdg_file_xml_to_text :-
   dislog_variable_get(morpheme_dictionary_directory, Dir),
   concat(Dir, 'wdg_decomposition_annotated.xml', File_1),
   concat(Dir, 'wdg_decomposition_annotated.txt', File_2),
   wdg_file_xml_to_text(File_1, File_2),
   !.

wdg_file_xml_to_text(File_1, File_2) :-
   dread(xml, File_1, [Xml]),
   predicate_to_file( File_2,
      forall( D := Xml/entry@decomposition,
         writeln(D) ) ),
   !.


/******************************************************************/


