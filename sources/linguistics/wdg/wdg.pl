

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  WDG                                    ***/
/***                                                            ***/
/******************************************************************/


:- home_directory(Home),
   X = '/research/projects/TUSTEP/2010/2010_12_13_Daten/',
   concat(Home, X, Dir),
   dislog_variable_set(morpheme_dictionary_directory, Dir).


/*** tests ********************************************************/


test(wdg, wdg_entry_to_display) :-
   wdg_entry_to_display('WA00001').


/*** interface ****************************************************/


/* wdg_entries_to_display(Ids) <-
      */

wdg_entry_to_display(Id) :-
   wdg_entries_to_display([Id]).

wdg_entries_to_display(Ids) :-
   maplist( wdg_entry_to_list,
      Ids, Lists ),
   append(Lists, List),
   Html = html:List,
%  dwrite(xml, Html),
   html_to_display(Html),
   !.

wdg_entry_to_list(Id, List) :-
   name(Id, [_, C|_]),
   name(C1, [C]),
   character_capital_to_lower(C1, C2), 
   dislog_variable_get(morpheme_dictionary_directory, Dir),
   concat([Dir, 'Data/WDG/wgd', C2, '.sgml'], File),
   dread(xml, File, [X]),
   Entry := X/descendant::entry::[@id=Id],
   List = [id='"', Id, '"', pre:[Entry]].
wdg_entry_to_list(_, []).


/******************************************************************/


