

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  WDG Entry Extraction                   ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(wdg_entry_extraction, a) :-
   home_directory(Home),
   concat([Home, '/research/projects/TUSTEP/2010',
      '/2010_12_13_Daten/Data/WDG/wdg_test.sgml'], File),
   findall( Y,
      ( X := doc(File)/descendant::superentry,
        wdg_superentry_transform(X, Y) ),
      Ys ),
   dwrite(xml, superentries:Ys).


/*** interface ****************************************************/


/* wdg_superentry_transform(Superentry_1, Superentry_2) <-
      */

wdg_superentry_transform(Superentry, superentry:[E|Entries]) :-
   fn_item_parse(Superentry, superentry:_:Es_1),
   Es_1 = [E2|Es_2],
   entry := E2/tag::'*',
   !,
   Key := E2@key,
   wdg_entry_extract_gram_group(E2, Gram_Grp),
   fn_item_parse(E2, _:As:_),
   E = entry:As:[Gram_Grp],
   maplist( wdg_superentry_entry_transform(1, Key),
      Es_2, Entries ).
wdg_superentry_transform(Superentry, superentry:Entries) :-
   fn_item_parse(Superentry, superentry:_:Es_1),
   Es_1 = [E2|Es_2],
   form := E2/tag::'*',
   !,
   [Key] := E2/orth/content::'*',
   maplist( wdg_superentry_entry_transform(2, Key),
      Es_2, Entries ).
wdg_superentry_transform(Superentry, Superentry).


/* wdg_superentry_entry_transform(Type, Key, E1, E2) <-
      */

wdg_superentry_entry_transform(Type, Key, E1, E2) :-
   wdg_superentry_entry_transform_(Type, Key, E1, E3),
   wdg_entry_reduce_key(E3, E2).

wdg_superentry_entry_transform_(_, Key, E1, E2) :-
   K := E1@key,
   wdg_superentry_key_is_prefix(Key, K),
   !,
   wdg_entry_extract_gram_group(E1, Gram_Grp),
   fn_item_parse(E1, _:As:_),
   E2 = entry:As:[Gram_Grp].
wdg_superentry_entry_transform_(1, Key, E1, E2) :-
   wdg_entry_extract_gram_group(E1, Gram_Grp),
   fn_item_parse(E1, _:As:_),
   [Name_1] := E1/form/orth/content::'*',
   name_remove_elements(front, "-", Name_1, Name_2),
   name_remove_elements(back, ",", Name_2, Name_3),
   name_remove_elements(back, "-", Key, Key_1),
   concat(Key_1, Name_3, Key_2),
   K := E1@key,
   E2 := (entry:As:[Gram_Grp])*[@key:Key_2, @key_1:K].
wdg_superentry_entry_transform_(2, Key, E1, E2) :-
   wdg_entry_extract_gram_group(E1, Gram_Grp),
   fn_item_parse(E1, _:As:_),
   Name_1 := E1@key,
   name_remove_elements(front, "-", Name_1, Name_2),
   name_remove_elements(back, ",", Name_2, Name_3),
   name_remove_elements(back, "-", Key, Key_1),
   concat(Key_1, Name_3, Key_2),
   E2 := (entry:As:[Gram_Grp])*[@key:Key_2, @key_1:Name_1].


/* wdg_superentry_key_is_prefix(Key, Name) <-
      */

wdg_superentry_key_is_prefix(Key, Name) :-
   name_split_at_position([" ", "-", ","], Key, [N1|_]),
   name_split_at_position([" ", "-", ","], Name, [N2|_]),
   character_capital_to_lower(N1, N3),
   character_capital_to_lower(N2, N4),
   concat(N3, _, N4).


/* wdg_entry_reduce_key(Entry_1, Entry_2) <-
      */

wdg_entry_reduce_key(Entry_1, Entry_2) :-
   Key_1 := Entry_1@key,
   name_split_at_position([" "], Key_1, Keys),
   ( noun := Entry_1/gramGrp@word_class ->
     findall( K,
        ( member(K, Keys),
          \+ character_lower_to_capital(K, _) ),
        Ks )
   ; _ := Entry_1/gramGrp@word_class ->
     findall( K,
        ( member(K, Keys),
          character_lower_to_capital(K, _) ),
        Ks )
   ; Ks = Keys ),
   ( Ks = [] ->
     Ks_2 = Keys
   ; Ks_2 = Ks ), 
   concat_with_separator(Ks_2, ' ', Key_2),
   Entry_2 := Entry_1*[@key:Key_2, @key_2:Key_1].


/******************************************************************/


