

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  BNC Extraction                         ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(
      bnc_extraction_mode, text ).


/*** implementation ***********************************************/


/* bcn_directory_to_words_file(Mode, Directory, File) <-
      */

bcn_directory_to_words_file(Mode, Directory, File) :-
   dislog_variable_set(bnc_extraction_mode, Mode),
   bcn_directory_to_words_file(Directory, File).

bcn_directory_to_words_file(Directory, File) :-
   bcn_directory_to_words(Directory, Words),
   ( dislog_variable_get(bnc_extraction_mode, text) ->
     writeln_list(File, Words)
   ; dwrite(xml, File, words:Words) ).

bcn_directory_to_words(Directory, Words) :-
   directory_contains_files(Directory, Files),
   maplist( concat(Directory),
      Files, Paths ),
   maplist_with_status_bar( bnc_file_to_words,
      Paths, Wss ),
   append(Wss, Words_2),
   sort(Words_2, Words).

bnc_file_to_words(File, Words) :-
   ( dislog_variable_get(bnc_extraction_mode, text) ->
     let( Words := doc(File)/descendant::w@hw )
   ; findall( word:[name:W, class:C]:[],
        ( Word := doc(File)/descendant::w,
          W := Word@hw,
          C := Word@pos ),
        Words ) ).


/******************************************************************/


