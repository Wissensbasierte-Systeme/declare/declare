

/******************************************************************/
/***                                                            ***/
/***             Alignment:  Construction                       ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* aligncon_read_table(+File, -Cols) <-
      */

aligncon_read_table(File, Cols) :-
   dread(xml, File, [table:[]:ColsA]),
   maplist(aligncon_translate_column, ColsA, Cols).

aligncon_translate_column(TripleA, TripleB) :-
   TripleA = column:[code:ChA]:ElsA,
   term_to_atom(ChB, ChA),
   maplist(aligncon_translate_element, ElsA, ElsB),
   TripleB = column:[code:ChB]:ElsB.

aligncon_translate_element(TripleA, TripleB) :-
   TripleA = element:[code:ChA]:[FreqA],
   term_to_atom(ChB, ChA),
   term_to_atom(FreqB, FreqA),
   TripleB = element:[code:ChB]:[FreqB].


/* aligncon_create_weighted_graph(+L0, +L1, +Cs, -Graph) <-
      */

aligncon_create_weighted_graph(Lem0, Lem1, Cols, Wgraph) :-
   length(Lem0, Len0),
   length(Lem1, Len1),
   aligncon_create_ugraph(Len1, Len0, Ugraph),
   aligncon_create_wgraph(Lem1, Lem0, Cols, Ugraph, Wgraph).

aligncon_create_ugraph(Len1, Len0, Ugraph) :-
   findall( [X0, Y0]-[X1, Y1],
      ( between(0, Len1, X0),
	between(0, Len0, Y0),
	aligncon_create_uedge([X0, Y0], Len1, Len0, [X1, Y1]) ),
      Ugraph ).

aligncon_create_uedge([X0, Y0], _, Len0, [X0, Y1]) :-
   Y0 < Len0,
   Y1 is Y0 + 1.
aligncon_create_uedge([X0, Y0], Len1, _, [X1, Y0]) :-
   X0 < Len1,
   X1 is X0 + 1.
aligncon_create_uedge([X0, Y0], Len1, Len0, [X1, Y1]) :-
   Y0 < Len0,
   X0 < Len1,
   Y1 is Y0 + 1,
   X1 is X0 + 1.

aligncon_create_wgraph(Lem1, Lem0, Cols, Ugraph, Wgraph) :-
   Ugraph = [Uedge|Uedges],
   Wgraph = [Wedge|Wedges],
   aligncon_create_wedge(Lem1, Lem0, Cols, Uedge, Wedge),
   !,
   aligncon_create_wgraph(Lem1, Lem0, Cols, Uedges, Wedges).
aligncon_create_wgraph(Lem1, Lem0, Cols, [_|Ugraph], Wgraph) :-
   !,
   aligncon_create_wgraph(Lem1, Lem0, Cols, Ugraph, Wgraph).
aligncon_create_wgraph(_, _, _, [], []).

aligncon_create_wedge(_, Lem0, Cols, Uedge, Wedge) :-
   Uedge = [X0, Y0]-[X0, Y1],
   !,
   nth1(Y1, Lem0, Ch0),
   aligncon_get_weight(Ch0, 37, Cols, Weight),
   Wedge = [X0, Y0]-([X0, Y1]-Weight).
aligncon_create_wedge(Lem1, _, Cols, Uedge, Wedge) :-
   Uedge = [X0, Y0]-[X1, Y0],
   !,
   nth1(X1, Lem1, Ch1),
   aligncon_get_weight(37, Ch1, Cols, Weight),
   Wedge = [X0, Y0]-([X1, Y0]-Weight).
aligncon_create_wedge(Lem1, Lem0, Cols, Uedge, Wedge) :-
   Uedge = [X0, Y0]-[X1, Y1],
   nth1(Y1, Lem0, Ch0),
   nth1(X1, Lem1, Ch1),
   aligncon_get_weight(Ch0, Ch1, Cols, Weight),
   Wedge = [X0, Y0]-([X1, Y1]-Weight).

aligncon_get_weight(Ch0, Ch1, Cols, Weight) :-
   memberchk(column:[code:Ch0]:Els, Cols),
   memberchk(element:[code:Ch1]:[Freq], Els),
   Weight is -log(Freq).


/******************************************************************/


