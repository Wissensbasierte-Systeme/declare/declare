

/******************************************************************/
/***                                                            ***/
/***          Alignment:  Databases                             ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      dictionary_to_chars_in_chache/2.

:- DSN = mysql,
   User = seipel,
   Alias = alignment_table_connection,
   dislog_variable_set(alignment_table_connection, Alias),
   catch(
      mysql_odbc_connect(DSN, User, '', Alias, multiple),
      _, true ).


/*** interface ****************************************************/


/* prefix_to_lexer_tuple(Prefix, Tuple) <-
      */

prefix_to_lexer_tuple(Prefix, Tuple) :-
   odbc_query(mysql, 'use Alignment', _, []),
   odbc_query(
      mysql, 'select * from Lexer', row(N, Id, Word), []),
   concat(Prefix, _, Word),
   Tuple = [N, Id, Word].


/* dictionary_to_chars_cached(Table, Chars) <-
      */

dictionary_to_chars_cached(Table, Chars) :-
   ( dictionary_to_chars_in_chache(Table, Chars) ->
     true
   ; dictionary_to_chars(Table, Chars),
     assert(dictionary_to_chars_in_chache(Table, Chars)) ).


/* dictionary_to_chars(Table, N, Chars) <-
      */

dictionary_to_chars(Table, N, Chars) :-
   dictionary_to_chars(Table, Chars_2),
   first_n_elements(N, Chars_2, Chars).

dictionary_to_chars(Table, Chars) :-
   findall( Cs,
      ( ddbase_call(odbc(mysql), 'Alignment':Table, T),
        T = [_, _, Word],
        atom_chars(Word, Cs) ),
      Chars_2 ),
   sort(Chars_2, Chars).


/* char_lists_group(Lists, Groups) <-
      */

char_lists_group(Lists, Groups) :-
   ddbase_aggregate( [C, list(Chars)],
      ( member(Chars, Lists),
        Chars = [C|_] ),
      Pairs ),
   pair_lists('[]', _, Groups, Pairs).


/* alignment_tuple_insert_maximum(Tuple, Pairs) <-
      */

alignment_tuple_insert_maximum(Tuple, Pairs) :-
   ( last(Pairs, Similarity:Word) ->
     alignment_tuple_insert_or_update(Tuple, Similarity:Word)
   ; alignment_tuple_insert_or_update(Tuple, '-') ).


/* alignment_tuple_insert_or_update(Tuple, Update) <-
      */

alignment_tuple_insert_or_update(Tuple, Update) :-
   dislog_variable_get(
      alignment_table_connection, Connection),
   Database:Table = 'Alignment':'Alignment',
   ( Update = Similarity:Word ->
     Attributes = [
        'NR', 'LID', 'Lemma_Lexer', 'Lemma_DWB', 'Similarity'],
     append(Tuple, [Word, Similarity], Values)
   ; Update = '-',
     Attributes = ['NR', 'LID', 'Lemma_Lexer'],
     Values = Tuple ),
   pair_lists(:, Attributes, Values, Pairs),
   mysql_insert_or_update(Connection, Database:Table, Pairs).


/* alignment_tuple_exists_in_database_tuples(
         Tuples, Tuple, Values) <-
      */

% alignment_tuple_exists_in_database_tuples(_, _, _) :-
%    !,
%    fail.
alignment_tuple_exists_in_database_tuples(Tuples, Tuple, Values) :-
   member(Tuple_DB, Tuples),
   append(Tuple, Values, Tuple_DB).
%  alignment_tuple_exists_in_database(Tuple, Pairs),
%  pair_lists(:, _, Values_2, Pairs),
%  nth_multiple([4, 5], Values_2, Values).


/* alignment_tuple_exists_in_database(Tuple, Pairs) <-
      */

alignment_tuple_exists_in_database(Tuple) :-
   alignment_tuple_exists_in_database(Tuple, _).

alignment_tuple_exists_in_database(Tuple, Pairs) :-
   dislog_variable_get(
      alignment_table_connection, Connection),
   Database:Table = 'Alignment':'Alignment',
   Attributes = ['NR', 'LID', 'Lemma_Lexer'],
   pair_lists(:, Attributes, Tuple, Pairs_Where),
   ddbase_call(
      odbc(Connection), Database:Table, Pairs_Where, Pairs).


/* words_to_first_char_distribution(Table) <-
      */

words_to_first_char_distribution(Table) :-
   words_to_first_char_distribution(Table, Pairs),
   xpce_display_table([], Pairs).

words_to_first_char_distribution(Table, Pairs) :-
   ddbase_aggregate( [First, length(T)],
      ( ddbase_call(odbc(mysql), 'Alignment':Table, T),
        T = [_, _, W],
        atom_chars(W, [First|_]) ),
      Pairs ).


/* alignment_databases_to_coverage_tuples <-
      */

alignment_databases_to_coverage_tuples :-
   dislog_variable_get(
      alignment_table_connection, Connection),
   alignment_databases_to_coverage_tuples(Connection, Tuples),
   Attributes = ['', 'Lexer', '+', '-', '?'],
   xpce_display_table(Attributes, Tuples).

alignment_databases_to_coverage_tuples(Connection, Tuples) :-
   A = 'Alignment':'Alignment',
   mysql_table_select(Connection, A, [], Tuples_A),
   assert_relation(alignment, Tuples_A),
   write(user, 'Alignment '), ttyflush,
   B = 'Alignment':'Lexer',
   mysql_table_select(Connection, B, [], Tuples_B),
   write(user, 'Lexer '), ttyflush,
   ddbase_aggregate( [First, length(T)],
      ( member(T, Tuples_B),
        T = [_, _, W],
        atom_chars(W, [First|_]) ),
      Pairs_1 ),
   write(user, '.'), ttyflush,
   T_2 = [W_d, S]-(W_d \= '$null$', S \= '$null$'),
   alignment_databases_to_coverage_pairs(Tuples_B, T_2, Pairs_2),
   T_3 = [W_d, S]-(W_d = '$null$', S = '$null$'),
   alignment_databases_to_coverage_pairs(Tuples_B, T_3, Pairs_3),
   retract_facts(alignment/5),
   alignment_pairs_combine(Pairs_1, Pairs_2, Pairs_3, Tuples).

alignment_databases_to_coverage_pairs(Tuples, Test, Pairs) :-
   Test = [W_d, S]-Condition,
   ddbase_aggregate( [First, length(T)],
      ( member(T, Tuples),
        T = [Nr, Id, W_l],
        atom_chars(W_l, [First|_]),
        alignment(Nr, Id, W_l, W_d, S),
        call(Condition) ),
      Pairs ),
   write(user, '.'), ttyflush.

alignment_pairs_combine(Pairs_1, Pairs_2, Pairs_3, Tuples) :-
   ( foreach([C, N], Pairs_1),
     foreach([C, N, I, J, K], Tuples) do
        ( member([C, I], Pairs_2) ; I = 0 ),
        ( member([C, J], Pairs_3) ; J = 0 ),
        K is N - I - J ).


/******************************************************************/


