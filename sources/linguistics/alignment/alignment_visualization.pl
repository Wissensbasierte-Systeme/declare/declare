

/******************************************************************/
/***                                                            ***/
/***           Alignment:  Visualization                        ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library(pce)).

:- [ alignment_construction,
     alignment_dijkstra,
     alignment_library ].


/*** tests ********************************************************/


test(alignvis_compare_lemmas, 1) :-
   dislog_variable_get(source_path,
      'linguistics/alignment/table.xml', Table),
   alignvis_compare_lemmas(stefan, florian, Table, Value),
   writeln(user, Value).

test(lemma_similarity, 1) :-
   lemma_similarity(salary, salaries, Similarity),
   writeln(user, Similarity).


/*** interface ****************************************************/


/* lemma_similarity_table_generate <-
      */

lemma_similarity_table_generate :-
   generate_interval(97, 122, Interval),
   ( foreach(I, Interval), foreach(Xml, Xmls) do
        (
%         name(N, [I]),
%         Xml = column:[code:[I], name:N]:[
%            element:[code:[I], name:N]:[1]] ) ),
%         Xml = column:[code:[I]]:[
%            element:[code:[I]]:[1]] ) ),
          Xml = column:[code:I]:[
             element:[code:I]:[1]] ) ),
   E_1 = column:[code:"ies"]:[element:[code:"y"]:[0.8]],
   E_2 = column:[code:"y"]:[element:[code:"ies"]:[0.8]],
   Table = table:[E_1,E_2|Xmls],
   dislog_variable_get(source_path,
      'linguistics/alignment/table_2.xml', Path),
   dwrite(xml, Path, Table).


/* lemma_similarity(Lemma_1, Lemma_2, Similarity) <-
      */

lemma_similarity(Lemma_1, Lemma_2, Similarity) :-
   dislog_variable_get(source_path,
      'linguistics/alignment/table_2.xml', Table),
%  alignvis_compare_lemmas(
%     Lemma_1, Lemma_2, Table, Value, _, _, _, _),
%  alignvis_compare_lemmas(
%     Lemma_1, Lemma_2, Table, Value),
   alignvis_compare_lemmas(
      Lemma_1, Lemma_2, Table, Value, L_1, L_2, Graph, Path),
   alignvis_draw_graph(L_1, L_2, Graph, Path),
   writeln(user, Path),
   ( Value = 0 ->
     Similarity = 1
   ; Similarity = Value).


/* alignvis_compare_lemmas(+Atm0, +Atm1, +Table, Value) <-
      */

alignvis_compare_lemmas(Atm0, Atm1, Table, Value) :-
   alignvis_compare_lemmas(
      Atm0, Atm1, Table, Value, Len1, Len0, Graph, Path),
   alignvis_draw_graph(Len1, Len0, Graph, Path).

alignvis_compare_lemmas(
      Atm0, Atm1, Table, Value, Len1, Len0, Graph, Path) :-
   alignlib_parse_lemma(Atm0, Lem0),
   alignlib_parse_lemma(Atm1, Lem1),
   length(Lem0, Len0),
   length(Lem1, Len1),
   aligncon_read_table(Table, Cols),
   aligncon_create_weighted_graph(Lem0, Lem1, Cols, Graph),
   ( aligndij_find_path(Graph, [0, 0], [Len1, Len0], Cost:Path) ->
     Value is exp(-Cost)
   ; Path = [],
     Value = 0 ).


/* alignvis_draw_graph(+Rows, +Cols, +Graph, +Path) <-
      */

alignvis_draw_graph(Rows, Cols, Graph, Path) :-
   new(Grid, alignvis_grid(Rows, Cols)),
   forall( member([X0, Y0]-([X1, Y1]-_), Graph),
      send(Grid, alignvis_connect(X0, Y0, X1, Y1)) ),
   forall( heap_visited([X, Y]),
      send(Grid, alignvis_mark_node(X, Y, colour(yellow))) ),
   forall( member([X, Y], Path),
      send(Grid, alignvis_mark_node(X, Y, colour(blue))) ),
   send(Grid, open),
   forall( append(_, [[X0, Y0], [X1, Y1]|_], Path),
      send(Grid, alignvis_mark_edge(X0, Y0, X1, Y1)) ).


/*** implementation ***********************************************/


:- pce_begin_class(alignvis_grid, picture).

variable(alignvis_link, link, both).
variable(alignvis_nodes, chain, both).

initialise(Win, Rows:int, Cols:int) :->
   Height is Rows * 60 + 60,
   Width is Cols * 60 + 60,
   send_super(Win, initialise('Edit Graph', size(Width, Height))),
   send(Win, alignvis_set_attributes),
   forall( between(0, Rows, Row),
      forall( between(0, Cols, Col),
         send(Win, alignvis_new_node(Row, Col)) ) ),
   send(Win, alignvis_shift(28)).

alignvis_set_attributes(Win) :->
   send(Win, alignvis_nodes(new(_, chain))),
   new(Line, line(arrows := none)),
   send(Win, alignvis_link(new(_, link(link, link, Line)))).

alignvis_new_node(Win, Row:int, Col:int) :->
   new(Node, alignvis_node(Row, Col)),
   send(Win?alignvis_nodes, append(Node)),
   send(Win, display(Node)),
   send(Node, center(point(Col * 60, Row * 60))).

alignvis_shift(Win, Val:int) :->
   get(Win, alignvis_nodes, Chain),
   chain_list(Chain, Nodes),
   forall( member(Node, Nodes),
      ( send(Node, x(Node?x + Val)),
	send(Node, y(Node?y + Val)) ) ).

alignvis_connect(Win, Row0:int, Col0:int, Row1:int, Col1:int) :->
   get(Win, alignvis_nodes, Chain),
   chain_list(Chain, Nodes),
   member(Node0, Nodes),
   get(Node0, alignvis_row, Row0),
   get(Node0, alignvis_col, Col0),
   member(Node1, Nodes),
   get(Node1, alignvis_row, Row1),
   get(Node1, alignvis_col, Col1),
   new(_, connection(Node0, Node1, Win?alignvis_link)).

alignvis_mark_node(Win, Row:int, Col:int, Colour:colour) :->
   get(Win, alignvis_nodes, Chain),
   chain_list(Chain, Nodes),
   member(Node, Nodes),
   get(Node, alignvis_row, Row),
   get(Node, alignvis_col, Col),
   send(Node, fill_pattern(Colour)).

alignvis_mark_edge(Win, Row0:int, Col0:int, Row1:int, Col1:int) :->
   get(Win, alignvis_nodes, Chain),
   chain_list(Chain, Nodes),
   member(Node0, Nodes),
   get(Node0, alignvis_row, Row0),
   get(Node0, alignvis_col, Col0),
   member(Node1, Nodes),
   get(Node1, alignvis_row, Row1),
   get(Node1, alignvis_col, Col1),
   get(Node0, connected(Node1), Edge),
   send(Edge, pen(2)),
   send(Edge, colour(colour(blue))).

:- pce_end_class.


:- pce_begin_class(alignvis_node, circle).

variable(alignvis_col, int, both).
variable(alignvis_row, int, both).

initialise(Node, Row:int, Col:int) :->
   send_super(Node, initialise(15)),
   send(Node, handles),
   send(Node, alignvis_col(Col)),
   send(Node, alignvis_row(Row)).

handles(Node) :->
   send(Node, handle(handle(3, 3))),
   send(Node, handle(handle(0, h/2))),
   send(Node, handle(handle(w/2, h))),
   send(Node, handle(handle(w-3, h-3))),
   send(Node, handle(handle(w, h/2))),
   send(Node, handle(handle(w/2, 0))).

:- pce_end_class.


/******************************************************************/


