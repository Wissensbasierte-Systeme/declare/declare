

/******************************************************************/
/***                                                            ***/
/***          Alignment:  Matrices                              ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      alignment_point_value/2,
      alignment_symbol_difference/3,
      alignment_transitions/2.

:-
   Mode = hash_table,
%  Mode = assert,
   dislog_variable_set(alignment_matrix, Mode).


/*** interface ****************************************************/


/* alignment_is_initialized <-
      */

alignment_is_initialized :-
   alignment_symbol_difference(_, _, _),
   !.


/* alignment_initialize <-
      */

alignment_initialize :-
   alignment_matrix_to_prolog_facts,
   alignment_symbol_differences_to_transitions.


/* alignment_matrix_to_prolog_facts <-
      */

alignment_matrix_to_prolog_facts :-
   retractall(alignment_symbol_difference(_, _, _)),
   send(@align_counter, clear),
   forall( ddbase_call(odbc(mysql), 'Alignment':'Subtable', T),
      ( T = [_, V, W, Value_1],
        ( V = '%' -> X = '' ; X = V ),
        ( W = '%' -> Y = '' ; Y = W ),
        ( number(Value_1) -> Value = Value_1
        ; atom_number(Value_1, Value) ),
        Value_2 is -log(Value),
        assert(alignment_symbol_difference(X, Y, Value_2)),
        alignlib_set(@align_counter, point(X, Y), Value_2) ) ),
   listing(alignment_symbol_difference/3).


/* alignment_symbol_differences_to_transitions <-
      */

alignment_symbol_differences_to_transitions :-
   retractall(alignment_transitions(_, _)),
   ddbase_aggregate( [C, list(Z)],
      ( alignment_symbol_difference(X, Y, V),
        atom_chars(X, [C|Cs]),
        atom_chars(Y, Ds),
        Z = ([C|Cs], Ds, V) ),
      Pairs ),
   ( foreach([C, Transitions], Pairs) do
        alignment_transitions_sort_by_distance(
           Transitions, Transitions_2),
        assert(alignment_transitions(C, Transitions_2)) ),
   findall( Z,
      ( alignment_symbol_difference('', Y, V),
        atom_chars(Y, Ds),
        Z = ([], Ds, V) ),
      Transitions ),
   alignment_transitions_sort_by_distance(
      Transitions, Transitions_2),
   assert(alignment_transitions('', Transitions_2)),
   listing(alignment_transitions/2).

alignment_transitions_sort_by_distance(
      Transitions_1, Transitions_2) :-
   maplist( functor_structure_to_list(','),
      Transitions_1, Tuples_1 ),
   sort([3], Tuples_1, Tuples_2),
   maplist( list_to_functor_structure(','),
      Tuples_2, Transitions_2 ).


/* alignment_point_values_to_matrix(Chars_1, Chars_2) <-
      */

alignment_point_values_to_matrix(Chars_1, Chars_2) :-
   length(Chars_1, M),
   length(Chars_2, N),
   alignment_point_values_to_matrix(N, M, Tuples),
   Spaces = '          ',
   ( foreach(Xs, Tuples), foreach(C, [''|Chars_1]),
     foreach([X|Xs], Ts) do concat([Spaces, C, Spaces, '\n'], X) ),
   T = ['', '\n'|Chars_2],
   last(Tuples, Tuple),
   last(Tuple, Value),
   concat('Alignment:  sim = ', Value, Label),
   xpce_display_table(Label, T, Ts).

alignment_point_values_to_matrix(N, M, Tuples) :-
   for(J, 0, M), foreach(Tuple, Tuples) do
      for(I, 0, N), foreach(D, Tuple) do
         ( alignment_point_value_get(point(I, J), Value) ->
%          D is e^(-Value)
           round(e^(-Value), 4, D)
         ; D = '      -      ' ).


/* alignment_point_value_set(Point, Value) <-
      */

alignment_point_value_set(Point, Value) :-
   dislog_variable_get(alignment_matrix, hash_table),
   alignlib_set(@align_submatrix, Point, Value).
alignment_point_value_set(Point, Value) :-
   dislog_variable_get(alignment_matrix, assert),
   assert(alignment_point_value(Point, Value)).


/* alignment_point_value_get(Point, Value) <-
      */

alignment_point_value_get(Point, Value) :-
   dislog_variable_get(alignment_matrix, hash_table),
   alignlib_get(@align_submatrix, Point, Value).
alignment_point_value_get(Point, Value) :-
   dislog_variable_get(alignment_matrix, assert),
   alignment_point_value(Point, Value).


/* alignment_point_values_clear <-
      */

alignment_point_values_clear :-
   dislog_variable_get(alignment_matrix, hash_table),
   send(@align_submatrix, clear).
alignment_point_values_clear :-
   dislog_variable_get(alignment_matrix, assert),
   retractall(alignment_point_value(_, _)).


/* alignment_symbol_difference_get(X, Y, D) <-
      */

alignment_symbol_difference_get(X, Y, D) :-
   dislog_variable_get(alignment_matrix, hash_table),
   alignlib_get(@align_counter, point(X, Y), D).
alignment_symbol_difference_get(X, Y, D) :-
   dislog_variable_get(alignment_matrix, assert),
   alignment_symbol_difference(X, Y, D).


/******************************************************************/


:- dynamic
      alignment_minimum/1.

alignment_minimum_close :-
   retractall(alignment_minimum(_)).

alignment_minimum_set(Value) :-
   alignment_minimum_close,
   assert(alignment_minimum(Value)).

alignment_minimum_get(Value) :-
   alignment_minimum(Value).

alignment_minimum_decrease(Value) :-
   ( alignment_minimum_get(Min),
     Value >= Min ->
     true
   ; alignment_minimum_set(Value) ).

/*
alignment_minimum_decrease(Value) :-
   ( retract(alignment_minimum(Min)),
     Value >= Min ->
     assert(alignment_minimum(Min))
   ; assert(alignment_minimum(Value)) ).
*/


/******************************************************************/


