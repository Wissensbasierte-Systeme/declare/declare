

/******************************************************************/
/***                                                            ***/
/***         Alignment:  Dynamic Programming                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* chars_to_similarity_dynamic(
         Options, Chars_1, Chars_2, Similarity) <-
      */

chars_to_similarity_dynamic(
      Options, Chars_1, Chars_2, Similarity) :-
   member(step:Step, Options),
   alignment_point_value_set(point(0, 0), 0),
   length(Chars_1, M),
   length(Chars_2, N),
   ( for(I, 0, N) do for(J, 0, M) do
        alignment_point_update(
           Step, Chars_1, Chars_2, point(I, J)) ),
   !,
   ( alignment_point_value_get(point(N, M), Value) ->
     alignment_minimum_decrease(Value),
     Similarity is e^(-Value)
%    writeln(user, sim = Similarity)
   ; Similarity = '-' ).


/*** implementation ***********************************************/


/* alignment_point_update(Step, Chars_1, Chars_2, Point) <-
      */

alignment_point_update(Step, Chars_1, Chars_2, Point) :-
   findall( D,
      alignment_point_to_distance(
         Step, Chars_1, Chars_2, Point, D),
      Ds ),
   ( min(Ds, Value) ->
     alignment_point_value_set_conditionally(Point, Value)
   ; true ).

alignment_point_update(Step, Chars_1, Chars_2, Point) :-
   ( ddbase_aggregate( [min(D)],
        alignment_point_to_distance(
           Step, Chars_1, Chars_2, Point, D),
        [[Value]] ) ->
     alignment_point_value_set(Point, Value)
   ; true ).

alignment_point_value_set_conditionally(Point, Value) :-
   ( alignment_minimum_get(Min),
     Value >= Min ->
     true
   ; alignment_point_value_set(Point, Value) ).


/* alignment_point_to_distance(
         Step, Chars_1, Chars_2, Point, D) <-
      */

alignment_point_to_distance(Step, Chars_1, Chars_2, Point, D) :-
   alignment_point_to_predecessor(Step, Point, Pred),
   alignment_point_value_get(Pred, D1),
   alignment_lists_between_points(
      Pred, Point, Chars_1:Chars_2, Chars_3:Chars_4),
   atom_chars(W_3, Chars_3), atom_chars(W_4, Chars_4),
   alignment_symbol_difference_get(W_3, W_4, D2),
   D is D1 + D2.

alignment_point_to_predecessor(
      Step, point(I1, J1), point(I2, J2)) :-
   I is max(I1-Step, 0),
   J is max(J1-Step, 0),
   between(I, I1, I2),
   between(J, J1, J2).

alignment_lists_between_points(
      point(I1, J1), point(I2, J2),
      Chars_1:Chars_2, Chars_3:Chars_4) :-
   I is I1 + 1, generate_interval(I, I2, Is),      
   J is J1 + 1, generate_interval(J, J2, Js),
   nth_multiple(Js, Chars_1, Chars_3),
   nth_multiple(Is, Chars_2, Chars_4).


/******************************************************************/


