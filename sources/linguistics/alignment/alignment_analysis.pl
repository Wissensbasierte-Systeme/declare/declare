

/******************************************************************/
/***                                                            ***/
/***             Alignment:  Analysis                           ***/
/***                                                            ***/
/******************************************************************/


:- [alignment_library].


/*** interface ****************************************************/


/* alignana_calculate_table(+Input, +Output) <-
      */

alignana_calculate_table(Input, Output) :-
   alignana_read_alignment(Input, Lems),
   alignana_get_chars_from_lemmas(Lems, Chs0, Chs1),
   alignana_calculate_freqs(Chs0, Chs1, Lems, Table),
   dwrite(xml, Output, Table).

alignana_read_alignment(File, Lems) :-
   dread(xml, File, [mlalign:[]:Cs]),
   maplist(alignana_parse_alignment, Cs, Lems).

alignana_parse_alignment(alignment:[]:Cs, [Lem0, Lem1]) :-
   Cs = [lemma:[]:[Atm1], lemma:[]:[Atm0]],
   alignlib_parse_lemma(Atm0, Lem0),
   alignlib_parse_lemma(Atm1, Lem1).

alignana_get_chars_from_lemmas(Lems, Chs0, Chs1) :-
   alignana_collect_chars_from_lemmas(Lems, Chs0A, Chs1A),
   sort(Chs0A, Chs0),
   sort(Chs1A, Chs1).

alignana_collect_chars_from_lemmas([Lem|Lems], Chs0, Chs1) :-
   !,
   Lem = [Lem0, Lem1],
   alignana_collect_chars_from_lemmas(Lems, Chs0A, Chs1A),
   append(Lem0, Chs0A, Chs0),
   append(Lem1, Chs1A, Chs1).
alignana_collect_chars_from_lemmas([], [], []).

alignana_calculate_freqs(Chs0, Chs1, LemsA, table:[]:Cols) :-
   include(alignana_test_length, LemsA, Lems),
   findall( Col,
      ( member(Ch0, Chs0),
	alignana_count_in_lemmas(Ch0, Lems, NCh0_),
	findall( El,
	  ( member(Ch1, Chs1),
	    alignana_count_in_lemmas(Ch0, Ch1, Lems, NCh0Ch1),
	    Freq is NCh0Ch1/NCh0_,
	    alignana_format_element(Ch1, Freq, El) ),
	  Els ),
	alignana_format_column(Ch0, Els, Col) ),
      Cols ).

alignana_test_length([Lem0, Lem1]) :-
    length(Lem0, Len),
    length(Lem1, Len).

alignana_count_in_lemmas(Ch0, Lems, NCh0_) :-
   maplist(alignana_count_in_lemma(Ch0), Lems, NsCh0_),
   sumlist(NsCh0_, NCh0_).

alignana_count_in_lemma(Ch0, [Lem0, _], NCh0_) :-
   include(=(Ch0), Lem0, Ch0s),
   length(Ch0s, NCh0_).

alignana_count_in_lemmas(Ch0, Ch1, Lems, NCh0Ch1) :-
   maplist(alignana_count_in_lemma(Ch0, Ch1), Lems, NsCh0Ch1),
   sumlist(NsCh0Ch1, NCh0Ch1),
   not(NCh0Ch1 = 0).

alignana_count_in_lemma(Ch0, Ch1, [Lem0, Lem1], NCh0Ch1) :-
   pair_lists([], Lem0, Lem1, Lem0Lem1),
   include(=([Ch0, Ch1]), Lem0Lem1, Ch0Ch1s),
   length(Ch0Ch1s, NCh0Ch1).

alignana_format_element(Ch1, Freq, El) :-
   term_to_atom(Ch1, AtmCh1),
   term_to_atom(Freq, AtmFreq),
   El = element:[code:AtmCh1]:[AtmFreq].

alignana_format_column(Ch0, Els, Col) :-
   term_to_atom(Ch0, Atm),
   Col = column:[code:Atm]:Els.


/* alignana_get_compounds_from_chars(+Chs0, +Chs1, -Cmps) <-
      */

alignana_get_compounds_from_chars(Chs0, Chs1, Cmps) :-
   append(Chs0, Chs1, Chs),
   findall( Cmp,
      ( member(Cmp, Chs),
	is_list(Cmp) ),
      CmpsA ),
   sort(CmpsA, Cmps).


/* alignana_write_compounds_to_file(+Cmps, +File) <-
      */

alignana_write_compounds_to_file(Cmps, File) :-
   open(File, write, Stream),
   forall( member(Cmp, Cmps),
      format(Stream, '[~s]\n', [Cmp]) ),
   close(Stream).


/******************************************************************/


