

/******************************************************************/
/***                                                            ***/
/***         Alignment:  Chars to Similarities                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* chars_to_similarities(Options, Lists_1, Lists_2, Tuples) <-
      */

chars_to_similarities(Options, Lists_1, Lists_2, Tuples) :-
   !,
   char_lists_group(Lists_2, Groups),
   chars_to_similarities_grouped(Options, Lists_1, Groups, Tuples).
chars_to_similarities(Options, Lists_1, Lists_2, Tuples) :-
   findall( Tuple,
      ( member(Chars_1, Lists_1),
        write('.'), ttyflush,
        alignment_minimum_close,
        member(Chars_2, Lists_2),
        chars_to_similarity(
           Options, Chars_1, Chars_2, Similarity),
        atom_chars(Word_1, Chars_1),
        atom_chars(Word_2, Chars_2),
        Tuple = [Word_1, Word_2, Similarity] ),
      Tuples ).


/*** implementation ***********************************************/


/* chars_to_similarities_grouped(Options, Lists, Groups, Tuples) <-
      */

chars_to_similarities_grouped(Options, Lists, Groups, Tuples) :-
   findall( Tuple,
      ( member(Chars_1, Lists),
        write('.'), ttyflush,
%       alignment_minimum_close,
        Value is -log(0.01),
        alignment_minimum_set(Value),
        ( alignment_find_group(same_char, Chars_1, Groups, Group),
          ( member(Chars_1, Group) ->
            Lists_2 = [Chars_1|Group]
          ; Lists_2 = Group )
        ; alignment_find_group(diff_char, Chars_1, Groups, Group),
          Lists_2 = Group ),
        member(Chars_2, Lists_2),
        chars_to_similarity(
           Options, Chars_1, Chars_2, Similarity),
        atom_chars(Word_1, Chars_1),
        atom_chars(Word_2, Chars_2),
        Tuple = [Word_1, Word_2, Similarity] ),
      Tuples ).


/* alignment_find_group(Type, Chars, Groups, Group) <-
      */

alignment_find_group(same_char, Chars, Groups, Group) :-
   Chars = [C|_],
   member(Group, Groups),
   member([C|_], Group),
   write(C), ttyflush,
   !.
alignment_find_group(diff_char, Chars, Groups, Group) :-
   Chars = [C1|_],
   member(Group, Groups),
   once(member([C2|_], Group)),
   C1 \= C2,
   chars_could_be_aligned_to_char(Chars, C2),
   write(C2), ttyflush.


/* chars_to_similarity(Options, Chars_1, Chars_2, Similarity) <-
      */

chars_to_similarity(Options, Chars_1, Chars_2, Similarity) :-
   ( chars_could_be_aligned(Chars_1, Chars_2) ->
     ( dislog_variable_get(alignment_mode, dijkstra) ->
       chars_to_similarity_dijkstra(
          Options, Chars_1, Chars_2, Similarity)
     ; dislog_variable_get(alignment_mode, dynamic_programming) ->
       chars_to_similarity_dynamic(
          Options, Chars_1, Chars_2, Similarity),
       alignment_point_values_clear )
   ; Similarity = '-' ),
   !.


/* chars_could_be_aligned(Chars_1, Chars_2) <-
      */

chars_could_be_aligned(_, _) :-
   !.
chars_could_be_aligned(Chars_1, Chars_2) :-
   first(Chars_1, X),
   first(Chars_2, X),
   \+ ( length(Chars_1, N), length(Chars_2, M), abs(N-M) > 3 ).


/* chars_could_be_aligned_to_char(Chars, Char) <-
      */

chars_could_be_aligned_to_char(Chars, Char) :-
   alignment_minimum_get(Min),
   chars_could_be_aligned_to_char(Chars, Char, Min, _).

chars_could_be_aligned_to_char(Chars, Char, Min, Distance) :-
   Chars = [C1|_],
   alignment_transitions(C1, Transitions),
   ddbase_aggregate( [C2, min(Dist)],
      ( member((Chars_1, Chars_2, Dist), Transitions),
        prefix(Chars_1, Chars),
        Chars_2 = [C2|_],
        Dist < Min ),
      Pairs ),
   member([Char, Distance], Pairs).


/*** implementation ***********************************************/


/* chars_to_similarity_dijkstra(
         Options, Chars_1, Chars_2, Similarity) <-
      So far, Options is not used. */

chars_to_similarity_dijkstra(Chars_1, Chars_2, Similarity) :-
   ( alignment_is_initialized -> true
   ; alignment_initialize ),
   alignment_minimum_close,
   chars_to_similarity_dijkstra(
      _, Chars_1, Chars_2, Similarity),
   alignment_minimum_close.

chars_to_similarity_dijkstra(
      _, Chars_1, Chars_2, Similarity) :-
   dijkstra_find_path([Chars_1, Chars_2], [[],[]], Value:Path),
   writeln_list(user, Path),
   alignment_minimum_decrease(Value),
   Similarity is e^(-Value).


/* dijkstra_expand_node(Heap_1, Heap_2) <-
      */

dijkstra_expand_node(Heap_1, Heap_2) :-
   heap_delete_minimum(Heap_1, Cost_1:[V|Vs], Heap_3),
   findall( Cost_2:[W,V|Vs],
      ( dijkstra_edge(V, W, Dist),
        Cost_2 is Cost_1 + Dist,
        alignment_distance_is_admissible(Cost_2) ),
      Pairs ),
   heap_insert_list(Pairs, Heap_3, Heap_2).


/* dijkstra_edge(Chars_1, Chars_2, Distance) <-
      */

dijkstra_edge([Xs_1, Ys_1], [Xs_2, Ys_2], Distance) :-
   Xs_1 = [X|_],
   alignment_transitions(X, Transitions),
   member((As, Bs, Distance), Transitions),
   alignment_distance_is_admissible(Distance),
   append(As, Xs_2, Xs_1),
   append(Bs, Ys_2, Ys_1).


/* alignment_distance_is_admissible(Distance) <-
      */

alignment_distance_is_admissible(Distance) :-
   ( alignment_minimum_get(Min) ->
     Distance < Min
   ; true ).


/******************************************************************/


