

/******************************************************************/
/***                                                            ***/
/***             Alignment:  Library                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* alignlib_parse_lemma(+Atom, -Lemma) <-
      */

alignlib_parse_lemma(Atom, Lemma) :-
   atom_codes(Atom, Codes),
   alignlib_build_lemma(Codes, Lemma).


/*** implementation ***********************************************/


alignlib_build_lemma([Cd|CdsA], [El|Els]) :-
   Cd = 91,
   !,
   alignlib_build_sublemma(CdsA, El, CdsB),
   alignlib_build_lemma(CdsB, Els).
alignlib_build_lemma([Cd|Cds], [El|Els]) :-
   !,
   El = Cd,
   alignlib_build_lemma(Cds, Els).
alignlib_build_lemma([], []).

alignlib_build_sublemma([Cd|CdsA], [El|Els], CdsC) :-
   Cd  = 91,
   !,
   alignlib_build_sublemma(CdsA, El, CdsB),
   alignlib_build_sublemma(CdsB, Els, CdsC).
alignlib_build_sublemma([Cd|Cds], [], Cds) :-
   Cd = 93,
   !.
alignlib_build_sublemma([Cd|CdsA], [El|Els], CdsB) :-
   El = Cd,
   alignlib_build_sublemma(CdsA, Els, CdsB).


/******************************************************************/


