

/******************************************************************/
/***                                                            ***/
/***             Alignment:  Comparison                         ***/
/***                                                            ***/
/******************************************************************/


:- [ alignment_construction,
     alignment_dijkstra,
     alignment_library ].

:- dynamic
      aligncom_lines/1,
      aligncom_number/1.


/*** interface ****************************************************/


/* aligncom_start(+File0, +File1, +Table, +Output) <-
      */

aligncom_start(File0, File1, Table, Output) :-
   aligncom_initialise(File0),
   aligncom_work(File1, Table, Output).

aligncom_initialise(File0) :-
   aligncom_read_text_to_lines(File0, Lines0),
   retractall(aligncom_lines(_)),
   asserta(aligncom_lines(Lines0)),
   retractall(aligncom_number(_)),
   asserta(aligncom_number(0)).

aligncom_work(File1, Table, Output) :-
   aligncom_compare_file(File1, Table, Output),
   !,
   aligncom_work(File1, Table, Output).
aligncom_work(_, _, _).


/* aligncom_compare_file(+File, +Table, -Output) <-
      */

aligncom_compare_file(File1, Table, Output) :-
   retract(aligncom_lines(Lines0)),
   retract(aligncom_number(NumberA)),
   not(Lines0 = []),
   aligncom_partition_list(100, Lines0, Top, Tail),
   NumberB is NumberA + 1,
   aligncom_read_text_to_lines(File1, Lines1),
   aligncon_read_table(Table, Cols),
   aligncom_compare_lines(Top, Lines1, Cols, Rows),
   atomic_list_concat([Output, NumberB, '.csv'], Name),
   csv_write_file(Name, Rows, [separator(59)]),
   asserta(aligncom_lines(Tail)),
   asserta(aligncom_number(NumberB)).

aligncom_partition_list(_, [], Top, Tail) :-
   !,
   Top = [],
   Tail = [].
aligncom_partition_list(0, Lines, Top, Tail) :-
   !,
   Top = [],
   Tail = Lines.
aligncom_partition_list(N, [Line|Lines], [Line|Top], Tail) :-
   M is N - 1,
   aligncom_partition_list(M, Lines, Top, Tail).

aligncom_compare_lines(Lines0, Lines1, Cols, Rows) :-
   findall( row(Atm0, Atm1, Value),
      ( member(Cds0, Lines0),
        member(Cds1, Lines1),
	atom_codes(Atm0, Cds0),
	atom_codes(Atm1, Cds1),
        alignlib_parse_lemma(Atm0, Lem0),
	alignlib_parse_lemma(Atm1, Lem1),
        length(Lem0, Len0),
	length(Lem1, Len1),
	aligncon_create_weighted_graph(Lem0, Lem1, Cols, Graph),
	aligndij_find_path(Graph, [0, 0], [Len1, Len0], Cost:_),
	Value is exp(-Cost) ),
      Rows ).


/* aligncom_read_text_to_lines(+File, -Lines) <-
      */

aligncom_read_text_to_lines(File, Lines) :-
   open(File, read, Stream),
   aligncom_read_stream_to_lines(Stream, Lines),
   close(Stream).

aligncom_read_stream_to_lines(Stream, Lines) :-
   read_line_to_codes(Stream, Line),
   aligncom_read_stream_to_lines(Stream, Line, Lines).

aligncom_read_stream_to_lines(_, Line, []) :-
   Line = end_of_file,
   !.
aligncom_read_stream_to_lines(Stream, Line, Lines) :-
   member(63, Line),
   !,
   aligncom_read_stream_to_lines(Stream, Lines).
aligncom_read_stream_to_lines(Stream, Line, [Line|Lines]) :-
   aligncom_read_stream_to_lines(Stream, Lines).


/******************************************************************/


