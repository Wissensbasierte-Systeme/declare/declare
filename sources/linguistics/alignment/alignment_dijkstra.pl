

/******************************************************************/
/***                                                            ***/
/***             Alignment:  Dijkstra                           ***/
/***                                                            ***/
/******************************************************************/


:- dynamic aligndij_edge/3, heap_visited/1.


/*** interface ****************************************************/


/* aligndij_find_path(Graph, Source, Target, Cost:Path) <-
      */

aligndij_find_path(Graph, Source, Target, Cost:Path) :-
   retractall(aligndij_edge(_, _, _)),
   forall( member(U-(V-W), Graph),
      assert(aligndij_edge(U, V, W)) ),
   aligndij_find_path(Source, Target, Cost:Path),
   retractall(aligndij_edge(_, _, _)).


/* aligndij_find_path(Source, Target, Cost:Path) <-
      */

aligndij_find_path(Source, Target, Cost:Path) :-
   retractall(heap_visited(_)),
   empty_heap(Heap_1),
   add_to_heap(Heap_1, 0, [Source], Heap_2),
   aligndij_find_path_loop(Target, Heap_2, Cost:Path_),
   reverse(Path_, Path).

aligndij_find_path_loop(Target, Heap, Cost:Path) :-
   heap_delete_minimum(Heap, Cost:[Target|Vs], _),
   !,
   Path = [Target|Vs].
aligndij_find_path_loop(Target, Heap, Cost:Path) :-
   aligndij_expand_node(Heap, Heap_2),
   aligndij_find_path_loop(Target, Heap_2, Cost:Path).


/* aligndij_expand_node(Heap_1, Heap_2) <-
      */

aligndij_expand_node(Heap_1, Heap_2) :-
   heap_delete_minimum(Heap_1, Cost_1:[V|Vs], Heap_3),
   findall( Cost_2:[W,V|Vs],
      ( aligndij_edge(V, W, Dist),
        Cost_2 is Cost_1 + Dist ),
      Pairs ),
   heap_insert_list(Pairs, Heap_3, Heap_2).


/******************************************************************/


