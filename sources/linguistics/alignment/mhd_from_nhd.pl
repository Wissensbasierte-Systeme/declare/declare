

/******************************************************************/
/***                                                            ***/
/***         Alignment:  Mhd from Nhd                           ***/
/***                                                            ***/
/******************************************************************/


% set_prolog_flag(encoding, iso_latin_1).


/*** interface ****************************************************/


/* mhd_nhd_transform_to_xml(File_1, File_2) <-
      */

mhd_nhd_transform_to_xml :-
   home_directory(Home),
   X = '/research/projects/TUSTEP/2012/2012_10_22_Mhd_Nhd/',
   concat(Home, X, Path),
   File_1 = 'mhdnhd.txt',
   File_2 = 'mhdnhd.xml',
%  File_1 = 'mhdnhd_test.txt',
%  File_2 = 'mhdnhd_test.xml',
   concat(Path, File_1, Path_1),
   concat(Path, File_2, Path_2),
   mhd_nhd_transform_to_xml(Path_1, Path_2).

mhd_nhd_transform_to_xml(File_1, File_2) :-
   dread(txt, File_1, Name),
   atom_chars(Name, Cs),
%  first_n_elements(311, Cs, Ds),
   Ds = Cs,
   atom_chars('\r\n', Ss),
   list_split_at_position([Ss], Ds, Ess),
   maplist( concat,
      Ess, Lines ),
   maplist( mhd_nhd_line_parse,
      Lines, Items_1 ),
   sort(Items_1, Items_2),
   reverse(Items_2, Items_3),
   dwrite(xml, File_2, entries:Items_3).


/* mhd_nhd_line_parse(Name, Item) <-
      */

mhd_nhd_line_parse(Name, Item) :-
   name_split_at_position(["<nrf>", "</nrf>"], Name, [X|Ys]),
   findall( nrf:[text:A, type:B, id:C]:[],
      ( member(Y, Ys),
        name_split_at_position(["_"], Y, [A, B, C]) ),
      Nrfs ),
   mhd_name_to_names(X, Xs),
   Item_2 = entry:[mhd:Xs|Nrfs],
   mhd_nhd_extend_by_similarity(Item_2, Item),
   ( atom_chars(Name, [D|_]) -> true
   ; D = '.' ),
   write(D), ttyflush,
   !.
mhd_nhd_line_parse(Name, Item) :-
   Item = entry:[Name],
   write('*'), ttyflush.


/* mhd_nhd_extend_by_similarity(Item_1, Item_2) <-
      */

mhd_nhd_extend_by_similarity(Item_1, Item_2) :-
   Item_1 = entry:[mhd:Xs|Nrfs],
   Nrfs = [nrf:[text:Nhd, type:_, id:_]:[]],
   ( foreach(Mhd, Xs), foreach(Y, Ys) do
        words_to_similarity(Mhd, Nhd, Sim_2),
        ( Sim_2 = '-' -> Sim = 0
        ; Sim = Sim_2 ),
        Y = mhd:[sim:Sim]:[Mhd] ),
   sort(Ys, Ys_2),
   reverse(Ys_2, Zs),
   ddbase_aggregate( [max(Sim)],
      Sim := (mhds:Ys)/mhd@sim,
      [[Max]] ),
   append(Nrfs, [mhds_with_sims:Zs, mhds_original:Xs], Es),
   Item_2 = entry:[type:'', maximal_sim:Max]:Es,
   !.
mhd_nhd_extend_by_similarity(Item, Item).

/*
mhd_nhd_extend_by_similarity(Item_1, Item_2) :-
   Item_1 = entry:[mhd:Xs|Nrfs],
   Xs = [Mhd],
   Nrfs = [nrf:[text:Nhd, type:_, id:_]:[]],
   words_to_similarity(Mhd, Nhd, Sim),
   Item_2 = entry:[mhd:[sim:Sim]:Xs|Nrfs],
   !.
mhd_nhd_extend_by_similarity(Item, Item).
*/


/* mhd_name_to_names(Name, Names) <-
      */

mhd_name_to_names(Name, Names) :-
   Splitters = ["_", ",", ";", " "],
   name_split_at_position(Splitters, Name, Xs),
   sublist( mhd_allowed_name,
      Xs, Names ).

mhd_allowed_name(Name) :-
   Name \= '',
   \+ ( concat('(', X, Name), concat(Y, ')', X),
        name(Y, Numbers), name(Z, Numbers), number(Z) ).


/******************************************************************/


