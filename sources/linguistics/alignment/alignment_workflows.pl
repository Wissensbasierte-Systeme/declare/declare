

/******************************************************************/
/***                                                            ***/
/***         Alignment:  Methods                                ***/
/***                                                            ***/
/******************************************************************/


:-
   Mode = dynamic_programming,
%  Mode = dijkstra,
   dislog_variable_set(alignment_mode, Mode).

:- dabolish(dijkstra_expand_node/2).


/*** tests ********************************************************/


test(words_to_similarity, 1) :-
   alignment_initialize.
test(words_to_similarity, 2) :-
   Options = [step:4, show_matrix:yes],
   words_to_similarity(Options, huond, hund, D),
   writeln(user, huond->hund:D).
test(words_to_similarity, 3) :-
   N = 4, M = 18,
   dictionary_to_chars('Lexer', N, Lists_1),
   dictionary_to_chars('DWB', M, Lists_2),
   writeln(user, '\ndatabases loaded'),
   Options = [step:4],
   measure(
      chars_to_similarities(Options, Lists_1, Lists_2, Tuples) ),
   xpce_display_table([], Tuples).
test(words_to_similarity, 4) :-
   Word = huond,
%  Word = abbeteie,
%  Word = abedrücken,
%  Word = gehilfe,
%  Word = calamarbe,
   word_to_similarities(Word).


/*** interface ****************************************************/


/* prefix_to_similarities(Options, Prefix, Pairs) <-
      */

prefixes_to_similarities(Prefixes) :-
   checklist( prefix_to_similarities,
      Prefixes ).

prefix_to_similarities(Prefix) :-
   Options = [step:4],
   prefix_to_similarities(Options, Prefix).

prefix_to_similarities(Options, Prefix) :-
   dictionary_to_chars_cached('DWB', Lists),
%  dictionary_to_chars('DWB', Lists),
   dislog_variable_get(
      alignment_table_connection, Connection),
   mysql_table_select(
      Connection, 'Alignment':'Alignment', [], Tuples),
   nl,
   forall( prefix_to_lexer_tuple(Prefix, Tuple),
      ( write_list([Tuple, ' ..']), ttyflush,
        ( alignment_tuple_exists_in_database_tuples(
             Tuples, Tuple, Values) ->
          write_list(user, ['. ', Values, '\n'])
        ; last(Tuple, Word),
          atom_chars(Word, Chars),
          chars_to_similarities_single_measure(
             Options, Chars, Lists, Pairs),
          writeln(user, Pairs),
          alignment_tuple_insert_maximum(Tuple, Pairs) ) ) ).


/* word_to_similarities(Options, Word, Pairs) <-
      */

word_to_similarities(Word) :-
   Options = [step:4],
   word_to_similarities(Options, Word, Pairs),
   writeln(user, Pairs).

word_to_similarities(Options, Word, Pairs) :-
   atom_chars(Word, Chars),
   dictionary_to_chars_cached('DWB', Lists),
%  dictionary_to_chars('DWB', Lists),
   nl,
   chars_to_similarities_single_measure(
      Options, Chars, Lists, Pairs).


/* words_to_similarity(Options, Word_1, Word_2, Similarity) <-
      */

words_to_similarity(Word_1, Word_2, Similarity) :-
   Options = [step:4],
   words_to_similarity(Options, Word_1, Word_2, Similarity).

words_to_similarity(Options, Word_1, Word_2, Similarity) :-
   atom_chars(Word_1, Chars_1),
   atom_chars(Word_2, Chars_2),
   alignment_minimum_close,
   ( dislog_variable_get(alignment_mode, dynamic_programming) ->
     chars_to_similarity_dynamic(
        Options, Chars_1, Chars_2, Similarity),
     ( member(show_matrix:yes, Options) ->
       alignment_point_values_to_matrix(Chars_1, Chars_2)
     ; true ),
     alignment_point_values_clear
   ; chars_to_similarity_dijkstra(
        Chars_1, Chars_2, Similarity) ),
   !.

/*
words_to_similarity(Options, Word_1, Word_2, Similarity) :-
   atom_chars(Word_1, Chars_1),
   atom_chars(Word_2, Chars_2),
   alignment_minimum_close,
   chars_to_similarity(Options, Chars_1, Chars_2, Similarity).
*/


/*** implementation ***********************************************/


/* chars_to_similarities_single_measure(
         Options, Chars, Lists, Pairs) <-
      */

chars_to_similarities_single_measure(
      Options, Chars, Lists, Pairs) :-
   Lists_1 = [Chars],
   ( member(Chars, Lists) ->
     Lists_2 = [Chars|Lists]
   ; Lists_2 = Lists ),
%  Lists_2 = Lists,
%  Lists_2 = [Chars|Lists],
   chars_to_similarities_measure(
      Options, Lists_1, Lists_2, Pairs).


/* chars_to_similarities_measure(
         Options, Lists_1, Lists_2, Pairs) <-
      */

chars_to_similarities_measure(
      Options, Lists_1, Lists_2, Pairs) :-
   measure(
      ( chars_to_similarities(
           Options, Lists_1, Lists_2, Tuples),
        write(' '), ttyflush ) ),
   write(' '), ttyflush,
   findall( S:W,
      ( member([_, W, S], Tuples),
        S \= '-' ),
      Pairs_2 ),
   sort(Pairs_2, Pairs).


/******************************************************************/


