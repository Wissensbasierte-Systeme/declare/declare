

/******************************************************************/
/***                                                            ***/
/***         Alignment:  Enrich Mhd from Nhd                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* mhdnhd_file(Path) <-
      */

mhdnhd_file(Path) :-
   home_directory(Home),
   File = 'mhdnhd.xml',
%  File = 'm.xml',
   concat([Home, '/research/projects/TUSTEP/2012',
      '/2012_10_22_Mhd_Nhd/', File], Path).


/* lexer_relation_to_prolog <-
      */

lexer_relation_to_prolog :-
   dabolish(lexer/2),
   Source = 'Alignment':'Lexer',
   forall( ddbase_call(odbc(mysql), Source, [_, Id, Text]),
      assert(lexer(Text, Id)) ).


/* mhdnhd_file_enrich(Path_1, Path_2) <-
      */

mhdnhd_file_enrich :-
   mhdnhd_file(Path_1),
   concat(Path_1, '.enriched', Path_2),
   mhdnhd_file_enrich(Path_1, Path_2).

mhdnhd_file_enrich(Path_1, Path_2) :-
   dislog_variable_get(source_path,
      'linguistics/alignment/mhdnhd_file_enrich.fng', File_S),
   Options = [file, fng(File_S)],
   fn_transform(Options, Path_1, Path_2).


/******************************************************************/


