

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Editor in XXUL                ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_get(source_path,
      'linguistics/morpheme_annotation_xxul/', Path),
   dislog_variable_set(morpheme_editor_path, Path).


/*** interface ****************************************************/


/* morpheme_editor_new(Editor) <-
      */

/* morpheme_annotation_editor_display_table(
         Attributes, Tuples) <-
      */


/*** implementation ***********************************************/


xxul_morpheme_editor_frame_setting(Frame, Editor) :-
   get(Frame, member(editor), Editor),
   dislog_variable_set(morpheme_editor, Editor),
   send(Editor, style, underline, style(underline := @on)),
   Msg = message(@prolog, morpheme_editor_change_line, Editor),
   send(Editor, key_binding('#', Msg)),
   get(Frame, member(picture), Picture),
   dislog_variable_set(morpheme_term_picture, Picture),
   get(Frame, member(table), Table),
   dislog_variable_set(morpheme_term_table, Table).

xxul_morpheme_table_attributes(Win, Attributes) :-
   As = [editable:false, font:'roman bold 12'],
   findall( cell:As:[Val],
      member(Val, Attributes),
      Cs ),
   Item = row:[background:lightblue, line:false]:Cs,
   xxul_item_to_object_as_child(Item, Row, Win),
   send(Win, append(Row)).

xxul_morpheme_table_tuple(Win, Tuple) :-
   findall( cell:[editable:false]:[Atom],
      ( member(Val, Tuple),
        with_output_to(atom(Atom), write(Val)) ),
      Cs ),
   Item = row:[line:true]:Cs,
   xxul_item_to_object_as_child(Item, Row, Win),
   send(Win, append(Row)).

xxul_morpheme_editor_quit(Frame) :-
   send(Frame, destroy).


/******************************************************************/


