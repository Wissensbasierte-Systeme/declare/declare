

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Editor Updates in XXUL        ***/
/***                                                            ***/
/******************************************************************/



/*** implementation ***********************************************/


/* morpheme_editor_update_table(Tuples) <-
      */


/* xxul_morpheme_editor_updates_build_frame(
         Item_0, Tuples, Item_1) <-
      */

xxul_morpheme_editor_updates_build_frame(
      Item_0, Tuples, Item_1) :-
   Item_0 = frame:As:[Header|_],
   findall( Box,
      ( member(Tuple, Tuples),
        xxul_morpheme_editor_updates_build_box(Tuple, Box) ),
      Boxes ),
   Item_1 = frame:As:[Header|Boxes].
%  dwrite(xml, Item_1).


/* xxul_morpheme_editor_updates_edit_frame(Frame, Tuple) <-
      */

xxul_morpheme_editor_updates_edit_frame(Frame, Tuple) :-
   Tuple = [Num, Idn, Src, Txt, Trm, _],
   xxul_morpheme_editor_updates_get_ids(Num, Ids),
   findall( member(Id),
      member(Id, Ids),
      Args ),
   maplist( get(Frame),
      Args, [E_idn, E_src, E_txt, E_trm] ),
   send(E_trm, key_binding('#',
      message(@prolog, morpheme_editor_change_line, E_trm))),
   maplist( editor_insert,
      [E_idn, E_src, E_txt, E_trm], [Idn, Src, Txt, Trm] ).


/* xxul_morpheme_editor_updates_build_box(Tuple, Box) <-
      */

xxul_morpheme_editor_updates_build_box(Tuple, Box) :-
   Tuple = [Num, _, _, _, _, Mod],
   dislog_variable_get( morpheme_editor_path,
      'morpheme_editor_updates.xxul', Path ),
   dread(xml, Path, [Item]),
   ( Mod = insert ->
     Item = _:_:[_, hbox:[]:Cs_0, _]
   ; Item = _:_:[_, _, hbox:[]:Cs_0] ),
   once(append(Es_0, [D_0], Cs_0)),
   xxul_morpheme_editor_updates_get_ids(Num, Ids),
   maplist( xxul_morpheme_editor_updates_set_id,
      Es_0, Ids, Es_1 ),
   xxul_morpheme_editor_updates_build_buttons(
      D_0, [Num, Mod], D_1 ),
   append(Es_1, [D_1], Cs),
   Box = hbox:[]:Cs.

xxul_morpheme_editor_updates_set_id(Item_0, Id, Item_1) :-
   Item_0 = T:As_0:Cs,
   Item_1 = T:[id:Id|As_0]:Cs.

xxul_morpheme_editor_updates_build_buttons(
      D_0, [Num, Mod], D_1 ) :-
   D_0 = dialog:As_A:[dbox:As_B:Cs_0],
   maplist( xxul_morpheme_editor_updates_set_oncommand([Num, Mod]),
      [mod, upd, chk, tre], Cs_0, Cs_1 ),
   D_1 = dialog:As_A:[dbox:As_B:Cs_1].

xxul_morpheme_editor_updates_set_oncommand(
      [Num, Mod], mod, Item_0, Item_1) :-
   !,
   atomic_list_concat([xxul_morpheme_update_row_, Mod], Fun),
   xxul_morpheme_editor_updates_get_ids(Num, Ids),
   Cmd =.. [Fun|Ids],
   xxul_morpheme_editor_updates_set_oncommand(Item_0, Cmd, Item_1).
xxul_morpheme_editor_updates_set_oncommand(
      [Num, _], upd, Item_0, Item_1) :-
   !,
   xxul_morpheme_editor_updates_get_ids(Num, Ids),
   Cmd =.. [xxul_morpheme_update_row_update|Ids],
   xxul_morpheme_editor_updates_set_oncommand(Item_0, Cmd, Item_1).
xxul_morpheme_editor_updates_set_oncommand(
      [Num, _], chk, Item_0, Item_1) :-
   !,
   xxul_morpheme_editor_updates_get_ids(Num, [E_idn|_]),
   Cmd = morpheme_editor_check_row(E_idn),
   xxul_morpheme_editor_updates_set_oncommand(Item_0, Cmd, Item_1).
xxul_morpheme_editor_updates_set_oncommand(
      [Num, _], tre, Item_0, Item_1) :-
   xxul_morpheme_editor_updates_get_ids(Num, [_, _, _, E_trm]),
   Cmd = morpheme_editor_term_to_picture(E_trm),
   xxul_morpheme_editor_updates_set_oncommand(Item_0, Cmd, Item_1).

xxul_morpheme_editor_updates_set_oncommand(Item_0, Cmd, Item_1) :-
   Item_0 = button:As_0:[],
   term_to_atom(Cmd, Atm),
   append(As_0, [oncommand:Atm], As_1),
   Item_1 = button:As_1:[].


/* xxul_morpheme_editor_updates_get_ids(
         Num, [E_idn, E_src, E_txt, E_trm]) <-
      */

xxul_morpheme_editor_updates_get_ids(
      Num, [E_idn, E_src, E_txt, E_trm]) :-
   term_to_atom(e_idn:Num, E_idn),
   term_to_atom(e_src:Num, E_src),
   term_to_atom(e_txt:Num, E_txt),
   term_to_atom(e_trm:Num, E_trm).

xxul_morpheme_update_row_insert(E_idn, E_src, E_txt, E_trm) :-
   morpheme_editor_update_row(
      insert, _, E_idn, E_src, E_txt, E_trm).

xxul_morpheme_update_row_delete(E_idn, E_src, E_txt, E_trm) :-
   morpheme_editor_update_row(
      delete, _, E_idn, E_src, E_txt, E_trm).

xxul_morpheme_update_row_update(E_idn, E_src, E_txt, E_trm) :-
   morpheme_editor_update_row(
      update, _, E_idn, E_src, E_txt, E_trm).


/* morpheme_editor_update_row(
         Mode, _, E_Id, E_Source, E_Text, E_Term) <-
      */


/******************************************************************/


