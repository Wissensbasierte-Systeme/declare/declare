

/******************************************************************/
/***                                                            ***/
/***       Linguistics:  Morpheme Term Database in XXUL         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* morpheme_database_connect <-
      */


/*** implementation ***********************************************/


xxul_morpheme_database_connect(Txt1, Txt2, Txt3, Frame) :-
   get(Txt1, selection, DSN),
   get(Txt2, selection, User),
   get(Txt3, selection, PW),
   morpheme_database_connect(DSN, User, PW, Frame).


/******************************************************************/


