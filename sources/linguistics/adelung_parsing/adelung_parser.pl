

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Parsing of Sentences                    ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_parser, [
      adelung_parser/0,
      adelung_parser/2 ] ).


/*** interface ****************************************************/


/* adelung_parser(File_in, File_out) <-
      */

adelung_parser :-
   dislog_variable_get(example_path,
      'adelung/adelung_aber.xml', File_in),
   dislog_variable_get(example_path,
      'adelung/adelung_aber_parser.html', File_out),
   adelung_parser(File_in, File_out).

adelung_parser(File_in, File_out) :-
   adelung_preprocessing(File_in, Temp_1),
   adelung_sentence_parsing(Temp_1, Temp_2),
   adelung_postprocessing(1, Temp_2, Temp_3),
   adelung_postprocessing(2, Temp_3, Temp_4),
   adelung_postprocessing(3, Temp_4, Temp_5),
   adelung_postprocessing(4, Temp_5, Temp_6),
   adelung_postprocessing(5, Temp_6, File_out).


/* adelung_preprocessing(Path_1, Path_2) <-
      */

adelung_preprocessing(Path_1, Path_2) :-
   dislog_variable_get(source_path,
      'projects/adelung_parsing/adelung_preprocessing.fng',
      Path_fng),
   ( var(Path_2),
     concat([Path_1, '.', prep], Path_2)
   ; true ),
   fn_transform_xml_file_fng(Path_fng, Path_1, Path_2).


/******************************************************************/


