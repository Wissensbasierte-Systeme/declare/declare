

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Parsing of Sentences                    ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_sentence_parsing, [
      adelung_sentence_parsing/2,
      adelung_sentence_parsing_2/2 ] ).


/*** interface ****************************************************/


/* adelung_sentence_parsing(Path_1, Path_2) <-
      */

adelung_sentence_parsing(Path_1, Path_2) :-
   dislog_variable_get(source_path,
      'projects/adelung_parsing/adelung_mark_sentences.fng',
      Path_fng),
   ( var(Path_2),
     concat([Path_1, '.', adc], Path_2)
   ; true ),
   fn_transform_xml_file_fng(Path_fng, Path_1, Path_2).


/* adelung_sentence_parsing_2(Name, Clauses) <-
      */

adelung_sentence_parsing_2(File_1, File_2) :-
   dread(txt, File_1, Name),
   adelung_sentence_parsing_2_(Name, Clauses),
   !,
   concat(File_1, '.tmp', File),
   predicate_to_file( File,
      foreach(Clause, Clauses) do
         ( fn_item_parse(Clause, T:_:E),
           member(T, [punct, word]),
           write(E), write(' ')
         ; dwrite(xml, Clause) ) ),
   adelung_tricky_clean(File, File_2).

adelung_tricky_clean(File_1, File_2) :-
   dread(txt, File_1, Name_1),
   Substitution = [ [" . ", "."], [" / ","/"] ],
   name_exchange_sublist(Substitution, Name_1, Name_2),
   concat(File_1, '.tmp', File),
   dwrite(txt, File, Name_2),
   pretty_print_xml_file(File, File_2).

adelung_sentence_parsing_2_(Name, Clauses) :-
   Substitution_1 = [
      [",", " , "], ["."," . "], [".", " . "],
      [";", " ; "], [":", " : "], ["!", " ! "],
      ["(", " ( "], [")", " ) "], ["/", " / "] ],
   name_exchange_sublist(Substitution_1, Name, Name_2),
   Substitution_2 = [ ["  ", " "] ],
   name_exchange_sublist(Substitution_2, Name_2, Name_3),
   Substitution_3 = [
      ["u . s . f .", "u.s.f."], ["s . f .", "s.f."],
      ["u . s . w .", "u.s.w."], ["d . i", "d.i"] ],
   name_exchange_sublist(Substitution_3, Name_3, Name_4),
   name_split_at_position([" ", "\n"], Name_4, Names),
   adelung_clauses(Clauses, Names, _).


/*** implementation ***********************************************/


adelung_clauses([X|Xs]) -->
   adelung_clause(X),
   ( adelung_clauses(Xs)
   ; { Xs = [] } ).

adelung_clause(Clause) -->
   ( relative_clause(Clause)
   ; conjunctional_clause(Clause)
   ; adjective_declaration(Clause)
   ; verbal_declaration(Clause)
   ; noun_declaration(Clause)
   ; reference(Clause)
   ; quot(Clause)
   ; emph(Clause)
   ; spo_sentence(Clause)
   ; bible_citation(Clause)
   ; numbering(Clause)
   ; remark(Clause) ),
   { writeln(user, Clause) }.

adelung_clause(punct:Punct) -->
   [Punct],
   { member(Punct, [',', '.', ';', ':', '!', ')']) }.

adelung_clause(word:Word) -->
   [Word].


/* relative_clause(relative_clause:Clause) -->
      */

relative_clause(relative_clause:Clause) -->
   relative_clause_(Cs),
   { flatten_and_append(Cs, Clause),
     ! }.

relative_clause_ ==> entry_w, words, verb, end.
relative_clause_ ==> entry_d, words_d, verb.


/* conjunctional_clause(conjunctional_clause:Clause) -->
      */

conjunctional_clause(conjunctional_clause:Clause) -->
   conjunctional_clause_(Cs),
   { flatten_and_append(Cs, Clause),
     ! }.

conjunctional_clause_ ==>
   comma, attribute, words, verb, end.

words ==> ( noun ; prefix ; aapp ; article ; junctor ), words.
words ==> noun ; aapp ; prefix.

words_d ==>
   ( prefix ; adverb ; article ; pronoun ; prep ; junctor ),
   words_d_2.
words_d ==> adverb ; pronoun ; prep ; prefix.

words_d_2 ==>
   ( noun ; prefix ; aapp ; article ; junctor ),
   words_d_2.
words_d_2 ==> noun ; aapp ; prefix.


entry_w ==> comma_all, relative_pronoun_w.
entry_w ==> comma_all, prep, relative_pronoun_w.

entry_d ==> comma_all, relative_pronoun_d.


junctor ==> comma ; conjunction.

aapp ==> adverb ; adjective ; pronoun; prep.


/* adjective_declaration(adjective_declaration:Clause) -->
      */

adjective_declaration(adjective_declaration:Clause) -->
   adjective_declaration_(Cs),
   { flatten_and_append(Cs, Clause),
     ! }.

adjective_declaration_ ==>
   endings, comma, adjective_declaration_.
adjective_declaration_ ==>
   adjective_decl, junct,
   ( punctuation ; void ), adjective_declaration_.
adjective_declaration_ ==> adjective_decl.

adjective_decl ==> adjective_decl_, punctuation.


/* verbal_declaration(verbal_declaration:Clause) -->
      */

verbal_declaration(verbal_declaration:Clause) -->
   verbal_declaration_(Cs),
   { flatten_and_append(Cs, Clause),
     ! }.

verbal_declaration_ ==>
   verb_decl, regularity, indic_v, junct, indic_v.
verbal_declaration_ ==>
   verb_decl, regularity, (indic_v; void).

verb_decl ==> verb_decl_, punctuation.

regularity ==> regularity_, punctuation.

indic_v ==> indic_v_, punctuation.


/* noun_declaration(noun_declaration:Clause) -->
      */

noun_declaration(noun_declaration:Clause) -->
   noun_declaration_(Cs),
   { flatten_and_append(Cs, Clause),
     ! }.

noun_declaration_ ==> genitiv_decl, plural_decl.
noun_declaration_ ==> plural_decl.

genitiv_decl ==>
   des, endings, comma, junct, endings, comma.
genitiv_decl ==>
   des, endings, ( comma ; void ).
genitiv_decl ==>
   des, noun, comma.

plural_decl ==>
   plur, junct, ( punctuation ; void ), indic_p, indic_p.
plural_decl ==> plur, ( indic_p ; plur_creation ).

plur_creation ==> article, ( endings ; noun ).

plur ==> plur_, punctuation.

indic_p ==> indic_p_, punctuation.


% back transformation of tags

reference(Clause) -->
   reference_(Cs),
   { flatten_and_append(Cs, Clause),
     ! }.

reference_(Ref) -->
   [Ref_1, Ref_2],
   { Ref_1 == '[ref_o]',
     Ref_2 == '/',
     Ref = '<ref>'
   ; Ref_1 == '/',
     Ref_2 == '[ref_c]',
     Ref = '</ref>' }.

emph(Clause) -->
   emph_(Cs),
   { flatten_and_append(Cs, Clause),
     ! }.

emph_(Emph) -->
   [Emph_1, Emph_2],
   { Emph_1 == '[emph_o]',
     Emph_2 == '/',
     Emph = '<emph>'
   ; Emph_1 == '/',
     Emph_2 == '[emph_c]',
     Emph = '</emph>' }.

quot(Clause) -->
   quot_(Cs),
   { flatten_and_append(Cs, Clause),
     ! }.

quot_(Quot) -->
   [Quot_1, Quot_2],
   { Quot_1 == '[q_o]',
     Quot_2 == '/',
     Quot = '<q>'
   ; Quot_1 == '/',
     Quot_2 == '[q_c]',
     Quot = '</q>' }.


/* spo_sentence(spo_sentence:Clause) -->
      */

spo_sentence(spo_sentence:Clause) -->
   spo_sentence_(Cs),
   { flatten_and_append(Cs, Clause),
     ! }.

spo_sentence_ ==>
   full_stop, subject, predicate, object,
   ( predicate ; void ), full_stop.
spo_sentence_ ==>
   full_stop, subject, predicate, full_stop.

subject ==> noun_phrase.

predicate ==> verb.

object ==> art_noun ; pronoun ; noun ; adjective.

noun_phrase ==>
   article_capital, ( adjective ; void ), noun.
noun_phrase ==>
   prep_capital, ( article ; void ), ( adjective ; void ), noun.
noun_phrase ==>
   pronoun_capital ; noun.

art_noun ==>
   ( prep_capital ; void ),
   ( article ; void ),
   ( adjective ; void ),
   noun.


/* bible_citation(citation:Clause) -->
      */

bible_citation(citation:Clause) -->
   bible_citation_(Cs),
   { flatten_and_append(Cs, Clause),
     ! }.

bible_citation_ ==>
   book_name, separator,
   book_number, separator, separator_1, separator,
   book_number.

bible_citation_ ==>
   book_number, ( separator_1 ; void ),
   book_name, ( separator ; void ),
   book_number, separator,
   book_number.

bible_citation_ ==>
   book_name, ( separator ; void ),
   book_number, ( separator ; void ),
   ( book_number ; void ),
   ( further ; void ).

bible_citation_ ==>
   book_name, separator,
   book_name, separator,
   book_number, ( separator ; void ),
   ( book_number ; void ).

book_name(Name) -->
   [Name],
   { member(Name, ['Amos', 'Apg', 'Chron', 'Hebr', 'Hiob',
        'Jac', 'Joh', 'Kap', 'Luc',  'Macc',
        'Marc', 'Math', 'Matth', 'Mos', 'Orthogr', 'Phil',
        'Pred', 'Ps', 'Richt', 'R�m',
        'S', 'Sal', 'Sam', 'Sprichw', 'Theuerd', 'Tit',
        'Zach']) }.

book_number(Number) -->
   [N],
   { name_to_number(N, Number),
     number(Number) }.
     
separator_1(Sep) -->
   [Sep],
   { member(Sep, ['.', 'Buch', 'Kap', 'S']) }.

separator(Sep) -->
   [Sep],
   { member(Sep, ['.', ',', ';', ':', '-']) }.

further(F) -->
   [F],
   { F = f }.


/* numbering(item:[number:N, type:T, delimiter:D]:Clause) -->
      */

numbering(item:[number:N, type:T, delimiter:D]:Clause) -->
   numbering_(Cs, N, T, D),
   { flatten_and_append(Cs, Clause),
     ! }.

numbering_(Clause, Num, T, Bc) -->
   (bracket_open(Bo); void(Bo)), number_(Num, T),
   (addition(Bc); bracket_close(Bc); void(Bc)),
   { Clause = [Bo, Num, Bc] }.

numbering_(Clause, Num, T, Bc) -->
   (bracket_open(Bo); void(Bo)), character_(Num, T),
   (bracket_close(Bc); addition(Bc)),
   { Clause = [Bo, Num, Bc] }.

number_(Num, T) -->
   [Num],
   { name_to_number(Num, Num_1),
     number(Num_1),
     Num_1 =< 25,
%    member(Num, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
%        11, 12, 13, 14, 15, 16, 17, 18, 19, 20]),
     T = arabic
   ; member(Num, ['I', 'II', 'III', 'IV', 'V']),
     T = 'roman' }.

character_(Num, T) -->
   [Num],
   { member(Num, [a, b, c, d, e, f, g, h, i, j, k, l]),
     T = 'character' }.

bracket_close(B) -->
   [B],
   { B = ')' }.

bracket_open(B) -->
   [B],
   { B = '(' }.

addition(Add) -->
   [Add],
   { member(Add, [')', '.']) }.


/* remark(remark:Clause) -->
      */

remark(remark:Clause) -->
   remark_(Cs),
   { flatten_and_append(Cs, Clause),
     ! }.

remark_ ==>
   anmerkung, full_stop.

anmerkung(Anm) -->
   [Anm],
   { Anm = 'Anm' }.


/******************************************************************/
/*** Adelung Processes ********************************************/

/* Verbstrukturen */

verb(Verb) -->
   ( verb_phrase(Verb)
   ; is_verb(Verb) ).

verb_phrase ==>
   is_verb, infinitive, is_modal.
verb_phrase ==>
   ( is_verb; infinitive), (is_modal; is_verb).


% /** Erkennung von Verben ******************************************/
% /** Die Regel beschreibt vorerst, dass ein Verb mit              **/
% /** einem Kleinbuchstaben beginnen muss, anschliessend           **/
% /** wird untersucht, ob eventuell eine Endung 'en' oder 't'      **/
% /** vorhanden ist, oder ob das Wort mit einem Element            **/
% /** der Liste des Pr�dikats member �bereinstimmt.                **/
% /** Ausgeschlossen werden W�rter mit der Endung 'aus'            **/
% /** und Adjektive, da es mit dieser Wortart am meisten           **/
% /** zu Verwechslungen kam. Anschlie�end folgt noch eine          **/
% /** Liste, in der W�rter eingef�gt werden k�nnen, welche         **/
% /** in keinem Fall als Verb erkannt werden d�rfen.               **/
% /******************************************************************/

is_verb(Verb) -->
   [Verb],
   { char_type_of_atom(Verb, lower),
     ( ( concat(_, t, Verb)
       ; concat(_, en, Verb)
       ; member(Verb, [sind, ausgehe, ist, verwechseln,
%           haben, zuschrieb, wei�, hat, erfordert,
            habe, kann]) ),
       \+ concat(_, aus, Verb) ),
     \+ is_adjective(Verb),
     \+ member(Verb, [mit, vermittelst, einen, den, einigen,
           zwischen, erst, keinen, gegen, heut, ihren,
           jemanden, dessen, willen, selbst, ingleichen,
           geschwinden, honneten, desselben, guten,
           biblischen]) }.

flatten_and_append(Clause_1, Clause_5):-
   flatten(Clause_1, Clause_2),
   names_append_with_separator(Clause_2, ' ', Clause_3),
   name_exchange_sublist([["  ", " "]], Clause_3, Clause_4),
   Substitution = [
      [" ,", ","], [" .","."], [" ;", ";"],
      [" :", ":"], [" !", "!"],
      [" ( ", "("], [" ) ", ")"] ],
   name_exchange_sublist(Substitution, Clause_4 , Clause_5).


% /******************************************************************/
% /*** Wortlisten ***************************************************/


% /** Pr�fixe *******************************************************/
% /** Pr�fixe sind in diesem Fall als ein Wort definiert,          **/
% /** welches zu einem Verb zugeh�rig ist                          **/
% /******************************************************************/

prefix(Prefix) -->
   [Prefix],
   { member(Prefix, [sich, man]) }.

adverb(Adverb) -->
   [Adverb],
   { member(Adverb, [einander, miteinander, dadurch]) }.


% /** Adjektive *****************************************************/
% /** Erkennung erfolgt nach Abgleich der unten angegebenen        **/
% /** Liste oder nach Beginn mit Kleinbuchstaben                   **/
% /******************************************************************/

adjective(Adjective) -->
   [Adjective],
   { is_adjective(Adjective)
   ; char_type_of_atom(Adjective, lower) }.

is_adjective(Adjective) :-
   member(Adjective, [alsdann, 'Abends', abgearbeiteten,
      alten, '�ltesten', '�hnlich', '�hnlichen', allein,
      angenehmen, abgebrochenem, besondere, 'beigef�gte',
      biblische, biblischen, besonders, breiten,
      besonders, bisherige, blaue, 'd�nnen', dicken,
      demselben, dunkeln, edler, einigen, erst, ersten,
      einiger, 'entbl��ten', eingebogenen, folgenden,
      faserigen, fahle, gedehnten, gefangenen, gegen,
      gelindern, gleich, gemeinen, 'geh�rig', gewissen,
      glatten, herab, hervorstechenden, 'h�ufig', hinab,
      heut, heutigen, junge, kurz, keinen, linken,
      letzten, matten, meisten, mittlern, neugebornen,
      neuen, nicht, 'n�her', nur, noch, oft,
      protestantischen, sehr, sonst, selten, 'tr�ben',
      thierischen, unter, '�blich', 'urspr�nglichen',
      '�blichsten', '�blicher', 'v�llig', veralteten, weit,
      'zun�chst', zwischen, uneigentlichen]).


% /** Substantive/Nomen *********************************************/
% /** Erkennung erfolgt �ber den beginnenden Gro�buchstaben,       **/
% /** weiterhin k�nnen in einer Liste weitere W�rter hinzu-        **/
% /** gef�gt werden, wie z.B. W�rter mit beginnendem Umlaut        **/
% /** da diese Probleme bereiteten. Des Weiteren k�nnen            **/
% /** W�rter, die im Dokument fehlerhaft sind, mit in eine         **/
% /** Liste aufgenommen werden.                                    **/
% /******************************************************************/

noun(Noun) -->
   [Noun],
   { char_type_of_atom(Noun, upper)
   ; member(Noun, [andern, '�pfel', '�thiopien', '�ser',
        eigentlichen, '�bersetzung']) }.


% /** Relativpronomen_w *********************************************/
% /** Relativpronomen, welche keine Betrachtung des                **/
% /** nachfolgenden Worts erfordern                                **/
% /******************************************************************/

relative_pronoun_w(Pronoun) -->
   [Pronoun],
   { member(Pronoun, [dem, denen, deren, dessen,
        was, welche, welchem, welchen, welcher,
        welches, 'wof�r', worin, wovon, womit, wo]) }.


% /** Relativpronomen_d *********************************************/
% /** Relativpronomen, bei denen das nachstehende Wort mit         **/
% /** untersucht werden muss, um sicher zu gehen, dass es          **/
% /** sich um einen relativen Nebensatz handelt                    **/
% /******************************************************************/

relative_pronoun_d(Pronoun) -->
   [Pronoun],
   { member(Pronoun, [das, der, die, des, den]) }.


% /** Artikel *******************************************************/
% /** einfache Auflistung aller m�glichen Artikel                  **/
% /******************************************************************/

article(Article) -->
   [Article],
   { member(Article, [das, dem, den, der, des, die,
        ein, eine, einem, einen, einer, eines]) }.
        
article_capital(Article) -->
   [Article],
   { member(Article, ['Das', 'Dem', 'Den', 'Der', 'Des', 'Die',
        'Ein', 'Eine', 'Einem', 'Einen', 'Einer', 'Eines']) }.


% /** Pronomen ******************************************************/
% /** Auflistung der m�glichen Pronomen                            **/
% /******************************************************************/

pronoun(Pronoun) -->
   [Pronoun],
   { member(Pronoun, [
        er, sie, es,
        ein, einer, einen, eine, einem, eines,
        solches, solchen, eure,
        das, den, der, dem, die, dessen,
        etwas, gewissen, ihren, ihre, ihres, ihrer,
        jemanden]) }.

pronoun_capital(Pronoun) -->
   [Pronoun],
   { member(Pronoun, [
        'Er', 'Sie', 'Es',
        'Solches', 'Solchen', 'Solche', 'Solch', 'Solchem', 'Eure',
        'Das', 'Den', 'Der', 'Dem', 'Die', 'Dessen',
        'Etwas', 'Ihren', 'Ihre', 'Ihres', 'Ihrer']) }.


% /** Pr�positionen *************************************************/
% /** Einfache Auflistung aller m�glichen Pr�positionen            **/
% /******************************************************************/

prep(Preposition) -->
   [Preposition],
   { member(Preposition, [in, an, vermittelst, mit, aus, durch,
        auf, von, am, bey, im, zu, sonst, so, nach,
        bei, vor, '�ber']) }.
        
prep_capital(Preposition) -->
   [Preposition],
   { member(Preposition, ['In', 'An', 'Vermittelst', 'Mit',
        'Aus', 'Durch', 'Auf', 'Von', 'Am', 'Bey', 'Im',
        'Zu', 'Sonst', 'So', 'Nach', 'Bei', 'Vor', '�ber']) }.


% /** Infinitive ****************************************************/
% /** Der Infinitiv eines Verbs darf vorerst keinem Adjektiv       **/
% /** entsprechen, und muss mit der Endung 'en' behaftet sein.     **/
% /** Eine Liste gibt die M�glichkeit, Ausnahmen, wie das Wort     **/
% /** 'seyn' einzuf�gen.                                           **/
% /******************************************************************/

infinitive(Verb_Inf) -->
   [Verb_Inf],
   \+ is_adjective(Verb_Inf),
   { concat(_, en, Verb_Inf)
   ; member(Verb_Inf, [seyn]) }.


% /** Modalverben ***************************************************/
% /** einfache Auflistung der Modalverben                          **/
% /******************************************************************/

is_modal(Modal) -->
   [Modal],
   { member(Modal, [hat, wird, worden, ist, sind, 'k�nnen',
        'w�rde', kann, 'h�tte', wurde, hab, sei, habe, habt, seid,
        bin, bist, hab, habe, habest, habet, habt, hast, hatte,
        hatten, hattest, hattet, 'h�tten', 'h�ttest', 'h�ttet',
        seid, seien, seiest, seiet, war, waren, warest, warst,
        wart, 'w�re', 'w�ren', 'w�rest', 'w�ret', warst, wart,
        'd�rfen', 'm�gen', 'm�ssen', sollen, wollen,
        darf, darfst, durfte, durften, durftest, durftet,
        'd�rfe', 'd�rfen', 'd�rfest', 'd�rfet', 'd�rft',
        'd�rfte', 'd�rften', 'd�rftest', 'd�rftet',
        kann, kannst, konnte, konnten, konntest, konntet,
        'k�nne', 'k�nnen', 'k�nnest', 'k�nnet',
        'k�nnt', 'k�nnte', 'k�nnten', 'k�nntest', 'k�nntet',
        mag, magst, mochte, mochten, mochtest, mochtet,
        'mu�', 'mu�t', 'mu�te', 'mu�ten', 'mu�test', 'mu�tet',
        'm�chte', 'm�chten', 'm�chtest', 'm�chtet',
        'm�ge', 'm�gen', 'm�gend',
        'm�gendst', 'm�gendste', 'm�gendsten', 'm�gest', 'm�get',
        'm�gt', 'm�sse', 'm�ssen', 'm�ssend',
        'm�ssest', 'm�sset', 'm��t',
        'm��te', 'm��ten', 'm��test', 'm��tet',
        soll, solle, sollen, sollend,
        sollest, sollet, sollst, sollt, sollte,
        sollten, solltest, solltet,
        will, willst, wolle, wollen,
        wollest, wollet, wollt, wollte,  wollten, wolltest,
        wolltet]) }.


% /** Konjunktionen *************************************************/
% /** Auflistung aller Konjunktionen                               **/
% /******************************************************************/

conjunction(C) -->
   [C],
   { member(C, [aber, als, auch, denn, doch, entweder,
        jedoch, noch, oder, sowie, sowohl, und, weder,
        anstatt, ohne, statt, um, als, bevor, da, damit,
        dass, 'da�', falls, indem, indes, ob, obwohl, obzwar,
        seit, sobald, sofern, solang, sooft, sosehr, soviel,
        soweit, sowie, statt, weil, wenn, zumal]) }.


% /** Attribute *****************************************************/
% /** Attribute sind die einleitenden W�rter f�r konj.             **/
% /** Nebens�tze. conjunction wird bereits mit allen               **/
% /** Konjunktionen belegt.                                        **/
% /******************************************************************/

attribute(A) -->
   [A],
   { member(A, [als, anstatt, auch, bevor, bis,
        da, damit, dass, 'da�', denn, doch, ehe, entweder,
        falls, indem, jedoch, nachdem, noch,
        ob, obwohl, obzwar, ohne,
        seit, seitdem, sobald, sofern, solang, solange,
        soweit, sowie, sooft, sosehr, soviel, statt,
        trotzdem, um, 'w�hrend', weder, weil, wenn, zumal]) }.

comma_all(C) -->
   [C],
   { member(C, [',', ';']) }.

comma(C) -->
   [C],
   { C = ',' }.

end(E) -->
   ( [E],
     { member(E, [';', '.', 'u.s.f.']) }
   ; { E = '' } ).

full_stop(F) -->
   [F],
   { F = '.' }.

verb_decl_(V) -->
   [V],
   { V = 'verb' }.

regularity_(R) -->
   [R],
   { member(R, [reg, irreg, regular, regul]) }.

indic_v_(I) -->
   [I],
   { member(I, [act, recipr, reciproc, neutr, imperf]) }.

indic_p_(I) -->
   [I],
   { member(I, [inusit, inus, car, nom, nomin, sing, singul]) }.

endings(End) -->
   [End],
   { concat('-', _, End) }.

adjective_decl_(Decl) -->
   [Decl],
   { member(Decl, [adj, adv]) }.
   
des(Des) -->
   [Des],
   { member(Des, [des]) }.

punctuation(P) -->
   ( [P],
     { P = '.'
     ; P = ',' }
   ; void(P) ).

void(V) -->
   { V = '' }.

junct(J) -->
   [J],
   { member(J, [et, oder, ut, und]) }.

plur_(P) -->
   [P],
   { P = 'plur'
   ; P = 'Plur' }.

abbreviation(Abb) -->
   [Abb],
   { Abb = 'S' }.

word_(Word) -->
   [Word],
   { char_type_of_atom(Word, alpha) }.


/******************************************************************/


