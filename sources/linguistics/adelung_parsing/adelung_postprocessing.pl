

/******************************************************************/
/***                                                            ***/
/***        Adelung: Postprocessing for Sentence Parsing        ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_postprocessing, [
      adelung_postprocessing/3,
      adelung_listings/2,
      adelung_make_listings/2,
      adelung_delete_spaces/2,
      adelung_concatenation/2 ] ).


/*** interface ****************************************************/


/* adelung_postprocessing(N, Path_1, Path_2) <-
      */

adelung_postprocessing(N, Path_1, Path_2) :-
   dislog_variable_get(source_path,
      'projects/adelung_parsing/adelung_postprocessing/',
      Path),
   concat([Path, N, '.fng'], Path_fng),
   ( var(Path_2),
     concat([Path_1, '.', N], Path_2)
   ; true ),
   fn_transform_xml_file_fng(Path_fng, Path_1, Path_2).


/*** implementation ***********************************************/


/* adelung_postprocessing_1 */

adelung_listings(Words, Sentences) :-
   adelung_listings_1(Sentences, Words, []).
   
adelung_listings_1([], [], []).
adelung_listings_1([X|Xs]) -->
   adelung_listing(X),
   !,
   adelung_listings_1(Xs).

adelung_listing(li:As:[W|Ws]) -->
   numbering_1(W, As),
   tokens(Ws),
   !.

adelung_listing(W:As:Es) -->
   [W:As:Es].

numbering_1(item:As:Es, As) -->
   [item:As:Es].

tokens([W:As:Es|Ws]) -->
   [W:As:Es],
   { member(W, [word, punct, relative_clause, ref,
        conjunctional_clause, spo_sentence, citation,
        noun_declaration, verb_declaration,
        adjective_declaration, q, emph, cit]) },
   tokens(Ws).
tokens([], [], []).


/* adelung_postprocessing_2 */

adelung_make_listings(Es_1, Es_2) :-
   items(Es_2, Es_1, []).

items([]) -->
   { true }.
items([I|Is]) -->
   item(I),
   items(Is).

item(I) -->
   xml_element(I).
item(I) -->
   token(I).
item(I) -->
   adelung_list([], [], I).


/* adelung_list(Path, Sofar, Items, Xs, Ys) <-
      */

adelung_list(Path, [], Items)  -->
   [Item],
   { is_adelung_item(Item, Item_2),
     ! },
   adelung_list(Path, [Item_2], Items).
adelung_list(Path, [I|Is], Items)  -->
   [Item],
   { is_adelung_item(Item, Item_2),
     list_item_next(I, Item),
     \+ ( \+ list_item_next_exact(I, Item),
          member(J, Path),
          list_item_next_exact(J, Item) ),
     ! },
   adelung_list(Path, [Item_2, I|Is], Items).
adelung_list(Path, Is, Items, Xs, Xs) :-
   first(Xs, X),
   is_adelung_item(X, _),
   member(I, Path),
   list_item_next(I, X),
   Is \= [],
   reverse(Is, Js),
   Items = ul:Js.
adelung_list(Path, [I|Is], Items)  -->
   adelung_list([I|Path], [], Is2),
   { I = li:As:Es1,
     append(Es1, [Is2], Es2) },
   adelung_list(Path, [li:As:Es2|Is], Items).
adelung_list(_, Is, Items)  -->
   { Is \= [],
     reverse(Is, Js),
     Items = ul:Js }.


/* is_adelung_item(Item_1, Item_2) <-
      */

is_adelung_item(Item_1, Item_2) :-
   fn_item_parse(Item_1, li:As:Es_1),
   items(Es_2, Es_1, []),
   Item_2 = li:As:Es_2.


/* list_item_next(I1, I2) <-
      */

list_item_next_exact(I1, I2) :-
   list_item_next(I1, I2),
   D := I1@delimiter,
   D := I2@delimiter.

list_item_next(I1, I2) :-
   T := I1@type,
   T := I2@type,
   N1 := I1@number,
   N2 := I2@number,
   list_number_next(N1, N2).

list_number_next(N1, N2) :-
   to_number(N1, M1),
   to_number(N2, M2),
   M2 is M1 + 1.
list_number_next(N1, N2) :-
   sublist([N1, N2], [a, b, c, d, e, f, g, h, i, j, k, l]).
list_number_next(N1, N2) :-
   sublist([N1, N2], ['I', 'II', 'III', 'IV', 'V']).

to_number(N, M) :-
   name(N, Ns),
   name(M, Ns),
   number(M).


/* xml_element(E, Xs, Ys) <-
      */

xml_element(T:As:Es_2) -->
   [E],
   { fn_item_parse(E, T:As:Es_1),
     T \= li,
     items(Es_2, Es_1, []) }.


/* token(T, Xs, Ys) <-
      */

token(T) -->
   [T],
   { \+ ( _ := T^tag::'*' ) }.


/* adelung_postprocessing_3 */

adelung_delete_spaces(Es_1, Es_2) :-
   adelung_delete_spaces(Es_2, Es_1, []).

adelung_delete_spaces([], [], []).
adelung_delete_spaces([X|Xs]) -->
   word_end(X),
   !,
   adelung_delete_spaces(Xs).

word_end(w_p:[A, B]) -->
   word_(A),
   end(B),
   !.

word_end(Y) -->
   [Y].
   
word_end([], [], []).

word_(W1:As:Es) -->
   [W1:As:Es],
   { member(W1, [word, relative_clause, conjunctional_clause,
        noun_declaration, verbal_declaration,
        adjective_declaration, q, emph, ref]) }.

end(punct:As:Es) -->
   [punct:As:Es].


/* adelung_postprocessing_4 */

adelung_concatenation(Es_1, Es_2) :-
   adelung_concatenation(Es_2, Es_1, []).

adelung_concatenation([], [], []).
adelung_concatenation([X|Xs]) -->
   adelung_concatenation_(X),
   !,
   adelung_concatenation(Xs).

adelung_concatenation_(Word:Es) -->
   [Word:_:[Es_1], _:_:[Es_2]],
   { member(Word, [word,
        relative_clause, conjunctional_clause,
        noun_declaration, verbal_declaration,
        adjective_declaration, emph, q]),
     concat(Es_1, Es_2, Es) },
   !.


/* adelung_postprocessing_5,
      no further predicates */


/******************************************************************/


