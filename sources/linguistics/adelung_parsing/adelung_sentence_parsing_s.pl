

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Parsing of Sentences                    ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_sentence_parsing, [
      adelung_detect_relative_clauses/0 ] ).


/*** interface ****************************************************/


/* adelung_detect_relative_clauses <-
      */

adelung_detect_relative_clauses :-
   File = 'examples/adelung/adelung_relative.txt',
   read_file_to_name(File, Name_1),
   name_cut_at_position(["stop"], Name_1, Name_2),
   name_split_at_position(
      [" ", "\n\n", ".\n\n"], Name_2, Names),
   writeq(user, Names), nl(user),
   phrase( relative_clauses(Xs),
      Names, Rest ),
%  relative_clauses(Xs, Names, Rest),
   dwrite(xml, relative_clauses:Xs),
   writeln(user, Rest).


/*** implementation ***********************************************/


relative_clauses([X|Xs]) -->
   relative_clause(X),
   ( relative_clauses(Xs)
   ; { Xs = [] } ).


relative_clause(Clause) -->
   relative_pronoun(Pronoun),
   location(Locator, Location),
   verb_with_locator(Verb, Locator),
   { Clause = relative_clause:[pronoun:Pronoun]:[
        location:[
           verb:Verb,
           locator:Locator, location:Location ]:[] ],
     ! }.

relative_clause(Clause) -->
   locator_for_verb(Locator, Verb),
   relative_pronoun(Pronoun),
   noun_phrase(Phrase),
   verb_with_locator(Verb, Locator),
   { Clause = relative_clause:[locator:Locator, pronoun:Pronoun]:[
        Phrase, verb:[Verb] ],
     ! }.

relative_clause(Clause) -->
   relative_pronoun(Pronoun),
   akkusative(Akkusative),
   verb_phrase(akk, Verb_Phrase),
   { Clause = relative_clause:[pronoun:Pronoun]:[
        Akkusative, Verb_Phrase ],
     ! }.

relative_clause(Clause) -->
   [Word],
   { Clause = word:[Word] }.


verb_with_locator(Verb, Locator) -->
   [Verb],
   { verb(Verb, locators:Locators),
     member(Locator, Locators) }.

verb_phrase(Case, Phrase) -->
   [Verb, Haben],
   { verb(Verb, cases:Cases),
     member(Case, Cases),
     member(Haben, [hat]),
     Phrase = verb_phrase:[haben: Haben, verb:Verb]:[] }.
verb_phrase(Case, Phrase) -->
   [Verb],
   { verb(Verb, cases:Cases),
     member(Case, Cases),
     \+ concat(ge, _, Verb),
     Phrase = verb_phrase:[verb:Verb]:[] }.

noun_phrase(Phrase) -->
   pronoun(Pronoun),
   noun(Noun),
   { Phrase = noun_phrase:[pronoun:Pronoun, noun:Noun]:[] }.
 
akkusative(akkusative:[article:Article, noun:Noun]:[]) -->
   akkusative_article(Article),
   noun(Noun).
akkusative(akkusative:[noun:Noun]:[]) -->
   noun(Noun).
akkusative(akkusative:[pronoun:Pronoun]:[]) -->
   pronoun(Pronoun).

location(Locator, Location) -->
   [Locator, Location],
   { member(Location, [
        '�thiopien', 'Berlin', 'Abend' ]) }.


noun(Noun) -->
   [Noun],
   { member(Noun, [
       'Achse', 'Beeren', 'Fall', 'Wapenkunst', 'Zirkel' ]) }.

relative_pronoun(Pronoun) -->
   [Pronoun],
   { member(Pronoun, [
        der, die, das,
        welcher, welche, welches ]) }.

pronoun(Pronoun) -->
   [Pronoun],
   { member(Pronoun, [
        er, sie, es, einer, eine, eines ]) }.

akkusative_article(A) -->
   [A],
   { member(A, [einen, eine, den, die, das]) }.

locator_for_verb(Locator, Verb) -->
   [Locator],
   { verb(Verb, locators:Locators),
     member(Locator, Locators) }.

 
verb(gehet, locators:[durch]).
verb(w�chset, locators:[in]).
verb(lebt, locators:[in]).
verb(kommt, locators:[aus]).
verb(haben, cases:[akk]).
verb(verstehet, cases:[akk]).
verb(tr�gt, cases:[akk]).
verb(ber�hret, cases:[akk]).
verb(geentert, cases:[akk]).


/******************************************************************/


