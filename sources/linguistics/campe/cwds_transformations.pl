

/******************************************************************/
/***                                                            ***/
/***          Campe:  Transformations                           ***/
/***                                                            ***/
/******************************************************************/


:- home_directory(Home),
   P1 = '/research/projects/TUSTEP',
   P2 = '/2012/2012_01_12/2012_08_09_CWDS_1_Basisdaten/',
   concat([Home, P1, P2], Path),
   dislog_variable_set(campe_data_directory, Path).


/*** tests ********************************************************/


test(cwds_transform, '10000_b') :-
   cwds_transform_and_annotate('10000_b').


/*** interface ****************************************************/


/* cwds_transform_and_annotate(File) <-
      */

cwds_transform_and_annotate(File) :-
   concat(File, '.', F),
   dislog_variable_get(campe_data_directory, F, Path),
   concat(Path, txt, Path_1),
   concat(Path, xml, Path_2),
   concat(Path, yml, Path_3),
   cwds_transform_and_annotate(Path_1, Path_2, Path_3).


/* cwds_transform_and_annotate(File_1, File_2, File_3) <-
      */

cwds_transform_and_annotate(File_1, File_2, File_3) :-
   cwds_transform(File_1, File_2),
   dread(xml, File_2, [Xml]),
   cwds_item_annotate_lists(Xml, Xmls),
   dwrite(xml, File_3, Xmls, [encoding(utf8)]).

cwds_transform(File_1, File_2) :-
   read_file_to_lines(File_1, Lines_1),
   ( foreach(Line_1, Lines_1), foreach(Line_2, Lines_2) do
        cwds_transform_line_2(Line_1, Line_3),
        cwds_transform_line(Line_3, Line_2),
        writeln(user, Line_2) ),
   dwrite(xml, File_2, cwds:Lines_2, [encoding(utf8)]).


/* cwds_transform_line(Line_1, Line_2) <-
      */

% cwds_transform_line(Line_1, Line_2) :-
%    concat('^@^@0^@', Line_2, Line_1),
%    !.
cwds_transform_line(Line_1, Line_2) :-
   concat('^@^@1^@', Text, Line_1),
   name_split_at_position(
      ["<S", ">"], Text, [Title_1, N, Title_2]),
   !,
   Line_2 = cb:[n:N, title1:Title_1, title2:Title_2]:[].
cwds_transform_line(Line_1, Line_2) :-
   concat('^@^@1^@', Text, Line_1),
   name_split_at_position(
      ["<SE", "<S", ">"], Text, ['', N]),
   !,
   Line_2 = pb:[n:N]:[].
cwds_transform_line(Line_1, Line_2) :-
   concat('^$', Text, Line_1),
   name_cut_at_position([" "], Text, N),
   name_split_at_position(["."], N, [_, _]),
   name_start_after_position([" "], Text, Text_2),
   !,
%  Line_2 = lb:[n:N]:[Text_2].
   Line_2 = [lb:[n:N]:[], Text_2].
cwds_transform_line(Line_1, Line_2) :-
   concat('<dollarnull/>', Text_1, Line_1),
   name_cut_at_position([" "], Text_1, Text_2),
   name_split_at_position(["."], Text_2, [P, L]),
   name_start_after_position([" "], Text_1, Text_3),
   Line_2 = [marker:[page:P, line:L]:[], Text_3].
cwds_transform_line(Line, Line).


/* cwds_transform_line_2(Line_1, Line_2) <-
      */

cwds_transform_line_2(Line_1, Line_2) :-
%  prolog_flag(encoding, E),
%  set_prolog_flag(encoding, iso_latin_1),
%  set_prolog_flag(encoding, utf8),
   Substitution = [
      ["<W+3>", "<W3>"], ["</W+3>", "</W3>"],
      ["<W+2>", "<W2>"], ["</W+2>", "</W2>"],
      ["<W+1>", "<W1>"], ["</W+1>", "</W1>"],
      ["<W-1>", "<M1>"], ["</W-1>", "</M1>"],
      ["^#s+", "<S>"],   ["^#s-", "</S>"],
      ["^@Z", "<atZ/>"], ["^@z", "<atz/>"],
      ["^$0", "<dollarnull/>"], ["duolan 2", "duolantwo"],
      ["^@^@0^@", ""], ["^@^@3^@", ""],
%     ["^@^@", "<atat/>"],
%     ["^@^@1^@", "<atat n=\"1\"/>"],
%     ["^@^@3^@", "<atat n=\"3\"/>"],
      ["<-P>", "<MP/>"],
      ["filename:cwds1", "image filename=\""],
      [".jpg/>", ".jpg\"/>"] ],
%  set_prolog_flag(encoding, E),
%  writeq(Substitution), nl,
%  atom_codes(Line_1, Codes), writeq(Codes), nl,
   name_exchange_sublist(Substitution, Line_1, Line_2).


/* cwds_item_annotate_lists(Item, Items) <-
      */

cwds_item_annotate_lists(Item, Items) :-
   write(user, a), ttyflush,
   cwds_modify_item(Item, Items_1),
   write(user, b), ttyflush,
   adelung_highlight_keywords:items(Items_2, Items_1, []),
   write(user, c), ttyflush,
   !,
   maplist( cwds_proper_numbering,
      Items_2, [Item_3] ),
   writeln(user, d),
   !,
   fn_item_to_pretty_fn_item(Item_3, Item_4),
   Items = [Item_4].


/* cwds_modify_item(Item, Items) <-
      */

cwds_modify_item(Name, Items) :-
   atomic(Name),
   !,
   ( concat(Name_2, ',', Name) ->
     Xs = [pc:[',']]
   ; Name_2 = Name,
     Xs = [] ),
   name_split_at_position([" "], Name_2, Names),
   append(Names, Xs, Items).
cwds_modify_item(Item_1, [Item_2]) :-
   fn_item_parse(Item_1, 'A':[]:Es),
   !,
   Item_2 = hi:[rend:roman]:Es.
cwds_modify_item(Item, Items) :-
   fn_item_parse(Item, T:As:Es_1),
   !,
   maplist( cwds_modify_item,
      Es_1, Es_3 ),
   append(Es_3, Es_2),
   Items = [T:As:Es_2].


/* cwds_proper_numbering(Item_1, Item_2) <-
      */

cwds_proper_numbering(Name, Name) :-
   atomic(Name),
   !.
cwds_proper_numbering(Item_1, Item_2) :-
   fn_item_parse(Item_1, 'negEZ':As:Es_1),
   !,
   adelung_proper_numbering:items(Es_2, Es_1, []),
   Item_2 = 'entry':As:Es_2.
cwds_proper_numbering(Item_1, Item_2) :-
   fn_item_parse(Item_1, T:As:Es_1),
   !,
   maplist( cwds_proper_numbering,
      Es_1, Es_2 ),
   Item_2 = T:As:Es_2.
cwds_proper_numbering(Item, Item).


% xml_string_to_fn_term
% fn_term_to_xml_string


/******************************************************************/


