

/******************************************************************/
/***                                                            ***/
/***          Campe:  Nouns                                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* cwds_noun_inflections(Inflections) -->
      */

cwds_noun_inflections(Inflections) -->
   [Token],
   { fn_item_parse(Token, 'W1':_:Xs),
     cwds_noun_inflection_pc_s(Inflections, Xs, []) }.

cwds_noun_inflection_pc_s(Xs) -->
   cwds_noun_inflection(Is),
   sequence('?', cwds_pc, Js),
   cwds_noun_inflection_pc_s(Ks),
   { append([Is, Js, Ks], Xs) }.
cwds_noun_inflection_pc_s(Xs) -->
   cwds_noun_inflection(Xs).

cwds_noun_inflection([I]) -->
   [Abbr, Name],
   { fn_item_parse(Abbr, abbr:[]:['Mz.']),
     atomic(Name),
     name_split_at_position([" "], Name, [Det, '--', Hw]),
     concat('-', Hw, HW),
     I = form:[type:inflected]:[
        gramGrp:[ case:[value:nominativ]:[],
           number:[value:plural]:['Mz.'] ],
        form:[type:determiner]:[orth:[Det]],
        form:[type:headword]:[orth:[ovar:[oref:[], HW]]] ] }.
cwds_noun_inflection([I]) -->
   [Name],
   { atomic(Name),
     name_split_at_position([" "], Name, [Det, '--', Hw]),
     member(Det:Case, [des:genitiv, die:nominativ]),
     concat('-', Hw, HW),
     I = form:[type:inflected]:[
        gramGrp:[ case:[value:Case]:[],
           number:[value:singular]:[] ],
        form:[type:determiner]:[orth:[Det]],
        form:[type:headword]:[orth:[ovar:[oref:[], HW]]] ] }.


/******************************************************************/


