

/******************************************************************/
/***                                                            ***/
/***          Campe:  Verbs                                     ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(cwds, words_to_verb_inflections(1)) :-
   Items = [ ich, dringe, pc:[','], du, dringest, pc:[','],
      er, dringet, pc:[;], wir, 'drin=',
      lb:[n:'0754.05']:[], gen, abbr:['(etc.)'] ],
   cwds_words_to_verb_inflections(Items, Inflections),
   dwrite(xml, inflections:Inflections).
test(cwds, words_to_verb_inflections(2)) :-
   Items = [ ich, drang, '(drung' ],
   cwds_words_to_verb_inflections(Items, Inflections),
   dwrite(xml, Inflections).
test(cwds, verb_inflections) :-
   Items = [
      'W1':['ich dringe, du dringest, er dringet; wir drin=',
         lb:[n:'0754.05']:[], gen, abbr:['(etc.)']],
      '; unlängst vergang. Zeit', pc:[','],
      'W1':['ich drang (drung)'],
      'gebundene', lb:[n:'0754.06']:[], 'Form', pc:[','],
      'W1':[]:['ich dränge (drünge)'],
      ';', abbr:['Mittelw.'], der, abbr:['verg. Z.'],
      'W1':[gedrungen],
      ';', lb:[n:'0754.07']:[], abbr:['Befehlsw.'],
      'W1':[dringe],
      pc:['.'] ],
   cwds_verb_inflections(Items, Groups, Inflections),
   dwrite(xml, groups:Groups),
   star_line,
   File = 'dringen_verb_inflections.tei',
   dislog_variable_get(
      campe_data_directory, File, Path),
   dwrite(xml, Path, inflections:Inflections).


/*** interface ****************************************************/


/* cwds_verb_inflections(Items, Inflections) <-
      */

cwds_verb_inflections(Items, Inflections) :-
   cwds_verb_inflections(Items, _, Inflections).

cwds_verb_inflections(Items, Groups, Inflections) :-
   cwds_items_annotate_punctuation_characters(
      [',', ';', '(', ')'], Items, Items_2),
   cwds_verb_items_to_groups(Groups, Items_2, []),
   maplist( cwds_group_to_verb_inflections,
      Groups, Inflections ).


/* cwds_verb_items_to_groups(Groups, Xs, Ys) <-
      */

cwds_verb_items_to_groups(Groups, Xs, Ys) :-
   append(Xs_1, [W1|Xs_2], Xs),
   fn_item_parse(W1, 'W1':As:Es),
   ( cwds_verb_items_to_groups(Groups_2, Xs_2, Ys)
   ; Groups_2 = [group:Xs_2], Ys = [] ),
   append(Xs_1, ['W1':As:Es], G),
   Groups = [group:G|Groups_2].
cwds_verb_items_to_groups([], [], []) :-
   !.
cwds_verb_items_to_groups(Groups, Xs, []) :-
   Groups = [group:Xs].


/* cwds_group_to_verb_inflections(group:Xs, Items) <-
      */

cwds_group_to_verb_inflections(group:Xs, Items) :-
   append(Ys, [W1], Xs),
   fn_item_parse(W1, 'W1':_:Es),
   !,
   cwds_items_annotate_punctuation_characters(
      [',', ';'], Es, Fs),
   cwds_words_to_verb_inflections(Fs, Inflections),
   ( Ys = [Pc|Zs],
     fn_item_parse(Pc, pc:_:_) ->
     Pc_Items = [Pc]
   ; Zs = Ys,
     Pc_Items = [] ),
   ( Inflections = [I],
     fn_item_parse(I, T:As:Is) ->
     Items_2 = [T:As:[qual:Zs|Is]]
   ; Items_2 = [qual:Zs|Inflections] ),
   append(Pc_Items, Items_2, Items).
cwds_group_to_verb_inflections(group:Items, Items).


/* cwds_words_to_verb_inflections(List, Items) <-
      */

cwds_words_to_verb_inflections(List, Items) :-
   ( append([First|Ws], [Pc|Xs], List),
     fn_item_parse(Pc, pc:_:_) ->
     Items = [I, Pc|Is]
   ; List = [First|Ws],
     Xs = [],
     Items = [I|Is] ),
   Pairs = [
      ich:1-singular, du:2-singular, er:3-singular,
      wir:1-plural ],
   ( member(First:Per-Number, Pairs) ->
     ( foreach(W, Ws), foreach(V, Vs) do
          cwds_item_annotate_punctuation_characters(
             [',', ';', '(', ')'], W, Ws_2),
          V = orth:[oVar:Ws_2] ),
     I = form:[type:inflected]:[
        gramGrp:[
           per:[value:Per]:[],
           number:[value:Number]:[],
           tns:[value:present]:[],
           mood:[value:indicative]:[] ],
        form:[type:person]:[orth:[First]],
        form:[type:headword]:Vs ]
   ; I = form:[type:inflected]:[
        gramGrp:[],
        form:[type:headword]:[orth:[oVar:[First]]] ] ),
   cwds_words_to_verb_inflections(Xs, Is).
cwds_words_to_verb_inflections([], []).


/******************************************************************/


