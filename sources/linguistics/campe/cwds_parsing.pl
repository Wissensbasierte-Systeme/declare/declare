

/******************************************************************/
/***                                                            ***/
/***          Campe:  Parsing                                   ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(cwds, parsing(verb)) :-
   Item_1 = entry:[
      'W2':[claire], pc:[','],
      abbr:[1], abbr:[2], abbr:[3], b ],
   cwds_entry_parse(Item_1, Item_2),
   dwrite(xml, Item_2).
test(cwds, parsing(noun)) :-
   Item_1 = entry:[
      'W2':['Der Aal'], pc:[','],
      'W1':['des -- es', pc:[','], abbr:['Mz.'], 'die -- e'],
      pc:[','] ],
   cwds_entry_parse(Item_1, Item_2),
   dwrite(xml, Item_2).
test(cwds, noun_inflections) :-
   Items_1 = [
      'W1':[]:['des -- es', pc:[','],
      abbr:['Mz.'], 'die -- e'], pc:[','] ],
   cwds_noun_inflections(Items, Items_1, Items_2),
   dwrite(xml, form:Items),
   dwrite(xml, rest:Items_2).
test(cwds, cwds_item_parse) :-
   dislog_variable_get(campe_data_directory, Path),
   concat(Path, 'test.yml', File_1),
   concat(Path, 'test.tei', File_2),
   dread(xml, File_1, [Item_1]),
   cwds_item_parse(Item_1, Item_2),
   dwrite(xml, File_2, Item_2, [encoding(utf8)]).


/*** interface ****************************************************/


/* cwds_item_parse(Item_1, Item_2) <-
      */

cwds_item_parse(Item_1, Item_2) :-
   entry := Item_1/tag::'*',
   !,
   cwds_entry_parse(Item_1, Item_2).
cwds_item_parse(Item_1, Item_2) :-
   fn_item_parse(Item_1, T:As:Es_1),
   !,
   maplist( cwds_item_parse,
      Es_1, Es_2 ),
   Item_2 = T:As:Es_2.
cwds_item_parse(Item, Item).


/* cwds_entry_parse(Item_1, Item_2) <-
      */

cwds_entry_parse(Item_1, Item_2) :-
   fn_item_parse(Item_1, entry:_:Xs),
   ( cwds_noun_items(Items_1, Xs, Ys)
   ; cwds_verb_items(Items_1, Xs, Ys) ),
   append(Items_1, [sense:Ys], Items_2),
   Item_2 = form:Items_2,
   !.
cwds_entry_parse(Item, Item).


/* cwds_noun_items(Xs) -->
      */

cwds_noun_items(Xs) -->
   cwds_headword(Hs), cwds_pc(Ps),
   cwds_noun_inflections(Inflections),
   { append(Hs, Ps, Ys),
     Xs = [form:[type:lemma]:Ys|Inflections] }.


/* cwds_verb_items(Items, Xs, Ys) <-
      */

cwds_verb_items(Items, Xs, Ys) :-
   cwds_verb([Lemma], Xs, Ys),
   ( Ys = []
   ; Ys = [Y|_], \+ fn_item_parse(Y, abbr:_:_) ),
   fn_item_parse(Lemma, _:_:Es),
   Items = [form:[type:lemma]:Es].

cwds_verb ==>
   cwds_headword,
   sequence('?', cwds_pc),
   cwds_verb_gram_grp,
   sequence('?', cwds_headword_reflexive).

cwds_verb_gram_grp([Gram_Group]) -->
   sequence('*', cwds_abbreviation, Abbreviationss),
   sequence('?', cwds_gram_element, Xs),
   { append(Abbreviationss, Abbreviations),
     append(Abbreviations, Xs, Es),
     Gram_Group = gramGrp:Es }.

cwds_gram_element([lbl:[mit], gram:Ys]) -->
   [Gram],
   { fn_item_parse(Gram, gram:[]:[mit|Xs]),
     ( foreach(X, Xs), foreach(Y, Ys) do
          ( fn_item_parse(X, 'W1':[]:[Word]) ->
            Y = hi:[rend:large]:[Word]
          ; Y = X ) ) }.

cwds_headword_reflexive([Item]) -->
   [W1],
   { fn_item_parse(W1, 'W1':[]:Es),
     Item = form:[type:headword_reflexive]:[orth:Es] }.


/******************************************************************/


