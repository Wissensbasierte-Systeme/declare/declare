

/******************************************************************/
/***                                                            ***/
/***          Campe:  Elementary                                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* cwds_items_annotate_punctuation_characters(
         Cs, Items_1, Items_2) <-
      Cs is the list of punctuation characters,
      which should be annotated. */

cwds_items_annotate_punctuation_characters(
      Cs, Items_1, Items_2) :-
   maplist( cwds_item_annotate_punctuation_characters(Cs),
      Items_1, Itemss_2 ),
   append(Itemss_2, Items_2).

cwds_item_annotate_punctuation_characters(Cs, Item, Items) :-
   atomic(Item),
   !,
   atom_chars(Item, Codes),
   cwds_codes_annotate_punctuation_characters(
      Cs, Codes, Items_2),
   findall( I,
      ( member(I, Items_2),
        I \= '' ),
      Items ).
cwds_item_annotate_punctuation_characters(_, Item, [Item]).

cwds_codes_annotate_punctuation_characters(Cs, Codes, Items) :-
   append(Codes_1, [C|Codes_2], Codes),
   member(C, Cs),
   !,
   atom_chars(Name, Codes_1),
   name_split_at_position([" "], Name, Names),
   cwds_codes_annotate_punctuation_characters(Cs, Codes_2, Is),
   append(Names, [pc:[C]|Is], Items).
cwds_codes_annotate_punctuation_characters(_, [], []) :-
   !.
cwds_codes_annotate_punctuation_characters(_, Codes, Names) :-
   atom_chars(Name, Codes),
   name_split_at_position([" "], Name, Names).


/* cwds_headword(Headwords) -->
      */

cwds_headword(Headwords) -->
   [X],
   { fn_item_parse(X, 'W2':_:Names),
     last(Names, Name),
     name_split_at_position([" "], Name, Ns),
     ( Ns = [W] ->
       Headwords = [form:[type:headword, norm:W]:[orth:[W]]]
     ; Ns = [Article, W],
       member(Article:Gender, ['Der':m, 'Die':w]),
       Headwords = [
          gramGrp:[
             pos:[value:noun]:[],
             gen:[value:Gender]:[] ],
          form:[type:determiner]:[orth:[Article]],
          form:[type:headword, norm:W]:[orth:[W]] ] ) }.


/* cwds_abbreviation(Abbreviations) -->
      */

cwds_abbreviation([Item]) -->
   [Abbr],
   { fn_item_parse(Abbr, abbr:_:_),
     cwds_abbreviation_transform(Abbr, Item) }.

cwds_abbreviation_transform(Item_1, Item_2) :-
   abbr := Item_1/tag::'*',
   ( ['v.'] := Item_1/hi::[@rend=roman]/content::'*' ->
     Item_2 = pos:[value:verb]:[Item_1]
   ; ['trs.'] := Item_1/hi::[@rend=roman]/content::'*' ->
     Item_2 = subc:[value:transitive]:[Item_1]
   ; ['unregelm.'] := Item_1/content::'*' ->
     Item_2 = subc:[value:irregular]:[Item_1]
   ; ['rec.'] := Item_1/hi::[@rend=roman]/content::'*' ->
     Item_2 = subc:[value:reciprocal]:[Item_1]
   ; ['ntr.'] := Item_1/hi::[@rend=roman]/content::'*' ->
     Item_2 = subc:[value:neuter]:[Item_1] ).
cwds_abbreviation_transform(Item, Item).


/* cwds_pc(Items) -->
      */

cwds_pc(Items) -->
   [Token],
   { fn_item_parse(Token, pc:_:[C]),
     Items = [pc:[C]] }.


/* cwds_void([]) -->
      */

cwds_void([]) -->
   { true }.


/* list_partition_wrt_condition(Condition, List, Lists) <-
      */

list_partition_wrt_condition(Condition, Xs, [Ys, X|Yss]) :-
   append(Ys, [X|Xs_2], Xs),
   apply(Condition, [X]),
   !,
   list_partition_wrt_condition(Condition, Xs_2, Yss).
list_partition_wrt_condition(_, Xs, [Xs]).


/******************************************************************/


