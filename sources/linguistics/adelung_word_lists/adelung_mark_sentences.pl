

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Sentences                               ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_mark_sentences, [
      adelung_mark_sentences/0,
      adelung_mark_sentences/2,
      adelung_mark_sentences_in_fng/2 ] ).


/*** intterface ***************************************************/


/* adelung_mark_sentences(File_1, File_2) <-
      */

adelung_mark_sentences :-
   dislog_variable_get(example_path, Examples),
   concat(Examples, 'adelung/POS_Tagging.xml', File_1),
   concat(Examples, 'adelung/Sentences.xml', File_2),
   adelung_mark_sentences(File_1, File_2).

adelung_mark_sentences(File_1, File_2) :-
   dislog_variable_get(source_path,
      'projects/adelung_word_lists/adelung_mark_sentences.fng',
      Path_fng),
   fn_transform_xml_file_fng(Path_fng, File_1, File_2).


/* adelung_mark_sentences_in_fng(Words, Sentences) <-
      */

adelung_mark_sentences_in_fng(Words, Sentences) :-
   sentences_1(Sentences, Words, []),
   !.


/*** implementation ***********************************************/


sentences_1([], [], []).
sentences_1([S|Ss]) -->
   sentences(S),
   !,
   sentences_1(Ss).

sentences(definition:Ws) -->
   words1(Ws),
   !.

sentences(definition:Ws) -->
   words2(Ws),
   !.

sentences(example:Ws) -->
   words3(Ws),
   !.
sentences(listings:Ws) -->
   words4(Ws),
   !.

words1([W:As:Es|Ws]) -->
   [W:As:Es],
   { member(W, [
        list,prefix,item, remark, bibl,def_art, indef_art,
        conjunction, article,
        rel_pronoun, dem_pronoun, poss_pronoun, refl_pronoun,
        verb, pers_pronoun_acc, comma, beginning, adjective,
        noun, q, w, abbr, ref, 'REF', 'CIT', 'Q',
        'BIBL','MILESTONE', adverb, milestone, 'EMPH' ]) },
   !,
   words1(Ws).

words1([X]) -->
   punctuation_mark_([X]).

words1([], [], []).

words2([W:As:Es|Ws]) -->
   [W:As:Es],
   { member(W, [
        basis, list, remark, item,bibl, conjunction,
        dem_pronoun, poss_pronoun, refl_pronoun, pers_pron_acc,
        def_art, indef_art, comma, beginning,
        article, rel_pronoun, verb, adjective, noun, q, adverb,
        w, abbr, ref, 'REF', 'CIT', 'Q',
        'BIBL','MILESTONE', milestone, 'EMPH', emph ]) },
   !,
   words2(Ws).


words2([X]) -->
   punctuation_mark_([X]).

words2([], [], []).

words3([W:As:Es|Ws]) -->
   [W:As:Es],
   { member(W, [
        key_word, item, remark, bibl,conjunction, prefix, basis,
        dem_pronoun, poss_pronoun,  refl_pronoun, pers_pron_acc,
        article, def_art, indef_art, comma, rel_pronoun,
        beginning, verb,
        w, adverb, adjective, noun, q, 'CIT', 'REF', 'Q',
        'BIBL', 'MILESTONE', abbr, ref,
        'EMPH', milestone, emph ]) },
   !,
   words3(Ws).

words3([X]) -->
   punctuation_mark_([X]).

words3([], [], []).

words4([W:As:Es|Ws]) -->
   [W:As:Es],
   { member(W, [
        list,key_word, item, bibl,conjunction, prefix, basis,
        articleandnoun, plain_article, relative_pronoun,
        verb, word, adjective, noun, q, 'CIT', 'REF', 'Q',
        word, 'BIBL', 'MILESTONE', abbr, ref,
        'EMPH', milestone, emph ]) },
   !,
   words4(Ws).
words4([X]) -->
   out([X]).
words4([], [], []).

out([X]) -->
   [X],
   { X = remark:_:_ }.

punctuation_mark_([X]) -->
   [X],
   { X = punctuation_mark:_:_ }.


/******************************************************************/


