

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Word Lists                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* adelung_to_word_list <-
      Requires the document to be asserted as facts
         adelung:adelung_document(Key, Xml).
      Asserts facts
         found_word_in_text(Word, Key).
      Displays an XPCE table with the attributes
         ['Word', '#']. */

adelung_to_word_list :-
   retract_all(found_word_in_text(_, _)),
   forall( adelung:adelung_document(Key, Xml),
      ( writeln(user, Key),
        adelung_key_normalize(Key, Key_2),
        fn_triple_to_word_list(Key_2, Xml) ) ),
%  findall( Key,
%     adelung:adelung_document(Key, _),
%     Keys ),
%  checklist_with_status_bar( adelung_to_word_list,
%     adelung_key_to_found_word_in_text, Keys ),
   adelung_found_words_in_text_to_rows(Rows),
   xpce_display_table(_, _, ['Word', '#'], Rows).

adelung_key_to_found_word_in_text(Key) :-
   adelung:adelung_document(Key, Xml),
   !,
   writeln(user, Key),
   adelung_key_normalize(Key, Key_2),
   fn_triple_to_word_list(Key_2, Xml).


/* adelung_found_words_in_text_to_xml <-
      Requires facts
         found_word_in_text(Word, Key).
      Asserts facts
         adelung_multiplicity(Word, N).
      Creates an XML file 'results/adelung_words.xml'
      of the form
         words:[
            word:[multiplicity:N, word:Word]:[],
            ... ]. */

adelung_found_words_in_text_to_xml :-
   retract_all(adelung_multiplicity(_, _)),
   adelung_found_words_in_text_to_rows(Rows),
   ( foreach([Word, N], Rows),
     foreach(word:[multiplicity:N, word:Word]:[], Xs) do
        assert(adelung_multiplicity(Word, N)) ),
   sort(Xs, Ys),
   dwrite(xml, 'results/adelung_words.xml', words:Ys).


/* adelung_cycles(Cycles) <-
      Requires facts
         found_word_in_text(Word, Key). 
      Creates an XML file 'results/adelung_cycles.xml'
      of the form
         cycles:[
            cycle:[key:X, word:Y]:[],
            ... ]. */

adelung_cycles(Cycles) :-
   findall( cycle:[key:X, word:Y]:[],
      adelung_cycle(X, Y),
      Cycles_2 ),
   sort(Cycles_2, Cycles),
   dwrite(xml, 'results/adelung_cycles.xml', cycles:Cycles).

adelung_cycle(X, Y) :-
   found_word_in_text(Key, Word),
   ( found_word_in_text(Word, Key)
   ; character_lower_to_capital(Word, Word_2),
     found_word_in_text(Word_2, Key) ),
   Key \= Word,
   sort([Key, Word], [X, Y]),
   \+ concat(X, _, Y).


/* adelung_coarsen <-
      Requires the document to be asserted as facts
         adelung:adelung_document(Key, Xml).
      Requires facts
         adelung_multiplicity(Word, N).
      Creates an XML file 'results/adelung_coarsened.xml'
      of the form
         adelung:[
            key:[key:Key]:[
               entry:[word:Word, mult:N, global:G]:[],
               ... ],
            ... ]. */

adelung_coarsen :-
   findall( key:[key:Key]:Entries,
      ( adelung:adelung_document(Key, Xml),
        writeln(user, Key),
        adelung_key_normalize(Key, Key_2),
        fn_triple_to_word_list(Key_2, Xml, Rows),
        adelung_rows_to_coarsened_entries(Rows, Entries) ),
      Xmls ),
   dwrite(xml, 'results/adelung_coarsened.xml', adelung:Xmls).

adelung_rows_to_coarsened_entries(Rows, Entries) :-
   findall( entry:[word:Word, mult:N, global:G]:[],
      ( member([Word, N], Rows),
        adelung_multiplicity(Word, G), 
        10 < G, G < 50 ),
      Entries ).


/*** implementation ***********************************************/


/* fn_triple_to_word_list(Key, Xml, Rows) <-
      */

fn_triple_to_word_list(Key, Xml, Rows) :-
   fn_triple_to_word_list(Key, Xml),
   adelung_found_words_in_text_to_rows(Rows),
   retract_all(found_word_in_text(_, _)).


/* fn_triple_to_word_list(Key, Xml) <-
      */

fn_triple_to_word_list(Key, Text) :-
   atomic(Text),
   !,
   Cut_Lists = [".", ",", ";", ":", "!", "?", "(", ")", " "],
   name_split_at_position(Cut_Lists, Text, Words),
   ( foreach(Word, Words) do
        assert(found_word_in_text(Word, Key)) ).
fn_triple_to_word_list(Key, Xml) :-
   fn_item_parse(Xml, _:_:Es),
   checklist( fn_triple_to_word_list(Key),
      Es ).


/* adelung_found_words_in_text_to_rows(Rows) <-
      */

adelung_found_words_in_text_to_rows(Rows) :-
   found_words_in_text(Words),
   ( foreach(Word, Words), foreach([Word, N], Rows) do
        word_to_multiplicity(Word, N) ).

found_words_in_text(Words) :-
   findall( Word,
      found_word_in_text(Word, _),
      Words_2 ),
   sort(Words_2, Words).

word_to_multiplicity(Word, Multiplicity) :-
   findall( Word,
      found_word_in_text(Word, _),
      Words ),
   length(Words, Multiplicity).


/* adelung_key_normalize(Key_1, Key_2) <-
      */

adelung_key_normalize(Key_1, Key_2) :-
   member(Article, ['Der', 'Die', 'Das']),
   concat(Article, ' ', A),
   concat(A, Key_2, Key_1),
   !.
adelung_key_normalize(Key, Key).


/******************************************************************/


