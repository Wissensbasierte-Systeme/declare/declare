

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Word List Operations                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* adelung_identical_word_lists(Pairs) <-
      */

adelung_identical_word_lists(Pairs) :-
   findall( [Name_1, Name_2],
      ( adelung_key(Name_1, N, Xs),
        adelung_key(Name_2, N, Ys),
        Name_1 \= Name_2, Xs \= [], Ys \= [],
        ord_set_distance_2(Xs, Ys, M),
        M < -2,
        writeln(user, [Name_1, Name_2]:N:M) ),
      Pairs ).


/* adelung_word_lists_assert <-
      */

adelung_word_lists_assert :-
   dread(xml, 'results/adelung_coarsened.xml', [X]),
   forall( Key := X/key,
      ( Name := Key@key,
        Words := Key/content::'*',
        length(Words, N),
        assert(adelung_key(Name, N, Words)) ) ).


/* ord_set_distance(Xs, Ys, N) <-
      */

ord_set_distance_3(Xs, Ys, N) :-
   ord_intersection(Xs, Ys, Zs),
   length(Zs, M),
   M is -N.

ord_set_distance_2(Xs, Ys, N) :-
   ord_set_distance(Xs, Ys, N_1),
   ord_intersection(Xs, Ys, Zs),
   length(Zs, N_2),
   N is N_1 - N_2.

ord_set_distance(Xs, Ys, N) :-
   ord_subtract(Xs, Ys, Zs_1),
   ord_subtract(Ys, Xs, Zs_2),
   length(Zs_1, N_1),
   length(Zs_2, N_2),
   !,
   N is N_1 + N_2.


/******************************************************************/


