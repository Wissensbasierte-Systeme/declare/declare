

/******************************************************************/
/***                                                            ***/
/***          Adelung: Word List Operations 2                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* adelung_identical_word_lists(Pairs) <-
      */
      
adelung_identical_word_lists(Pairs) :-
   findall( pair:[N_1, N_2|Entries],
      ( adelung_key(Name_1, M_1, Xs),
        adelung_key(Name_2, M_2, Ys),
        Name_1 \= Name_2, Xs \= [], Ys \= [],
        M_1 > 3, M_2 > 3,
        ord_set_distance(Xs, Ys, N, Entries),
        N < 15,
        ( 100/M_1 * N = P
        ; 100/M_2 * N = P),
        P > 40,
        N_1 = entry:[key:Name_1]:[],
        N_2 = entry:[key:Name_2]:[]),
        Pairs ),
   dwrite(xml, 'results/test2.xml', adelung:Pairs).


/* adelung_word_lists_assert(File) <-
      */
      
adelung_word_lists_assert(File) :-
   dread(xml, File, [X]),
   forall( Key := X/entry,
      ( Name := Key@key,
        Words := Key/content::'*',
        length(Words, N),
        assert(adelung_key(Name, N, Words)) ) ).


/* ord_set_distance(Xs, Ys, M, Zs) <-
      */
      
ord_set_distance(Xs, Ys, M, Zs) :-
   ord_intersection(Xs, Ys, Zs),
   length(Zs, M).


/******************************************************************/


