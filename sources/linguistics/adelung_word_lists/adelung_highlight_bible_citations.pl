

/******************************************************************/
/***                                                            ***/
/***          Adelung: Citations                                ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_highlight_bible_citations, [
      adelung_highlight_bible_citations/2,
      adelung_highlight_bible_citations_/2 ] ).


/*** interface ****************************************************/


/* adelung_highlight_bible_citations(File_1, File_2) <-
      */

adelung_highlight_bible_citations(File_1, File_2) :-
   dread(xml, File_1, [Xml_1]),
   adelung_highlight_bible_citations_(Xml_1, Xml_2),
   dwrite(xml, File_2, Xml_2).

adelung_highlight_bible_citations_(Xml, Sentence) :-
   atomic(Xml),
   adelung_insert_blanks(Xml, NormText),
   !,
   name_split_at_position([" "], NormText, Words),
   adelung_sentences(Sentence, Words, []),
   !.
adelung_highlight_bible_citations_(Xml, T:As:Es) :-
   fn_item_parse(Xml, T:As:Es_2),
   maplist( adelung_highlight_bible_citations_,
      Es_2, Es ).

adelung_insert_blanks(Name, Names) :-
   Substitution_1 = [
      [".", " . "], [",", " , "], [":", " : "], [";", " ; "] ],
   name_exchange_sublist(Substitution_1, Name, Name_2),
   Substitution_2 = [
      ["u . s . f .", "u.s.f ."], ["s . f.", "s.f."],
      ["u . s . w .", "u.s.w ."], ["d . i", "d.i"] ],
   name_exchange_sublist(Substitution_2, Name_2, Name_3),
   Substitution_3 = [ ["  ", " "] ],
   name_exchange_sublist(Substitution_3, Name_3, Names).


/* adelung_sentences(Ws, Xs, Ys) <-
      */

adelung_sentences(Ws, Xs, Ys) :-
   adelung_sentence_1(Ws, Xs, Ys).


/*** implementation ***********************************************/


/* adelung_sentence_1(Ws, Xs, Ys) <-
      */

adelung_sentence_1([W|Ws]) -->
   word(W),
   ( adelung_sentence_1(Ws)
   ; [],
     { Ws = [] } ).

word(W) -->
   citation(W).

word(W) -->
  remark(W).

word(W) -->
   numbers(W).

word(W) -->
   [W].


citation(Citation) -->
   book_number(Number_1),
   [X],
   { member(X, ['.', 'Buch']) },
   book_name(Name),
   seperator(Sep_1),
   book_number(Number_2),
   seperator(Sep_2),
   book_number(Number_3),
   !,
   { ( X = '.',
       Y = '. '
     ; X = 'Buch',
       Y = ' Buch ' ),
     concat([Number_1, Y, Name, Sep_1], Title),
     concat([Number_2, Sep_2], Chapter),
     Citation = bibl:[
        titles:Title, chapter:Chapter, verse:Number_3]:[ ] }.

citation(Citation) -->
   book_number(Number_1),
   [X],
   { member(X, ['.', 'Buch']) },
   book_name(Name),
   book_number(Number_2),
   seperator(Sep_2),
   book_number(Number_3),
   !,
   { ( X = '.',
       Y = '. '
     ; X = 'Buch',
       Y = ' Buch ' ),
     concat([Number_1, Y, Name], Title),
     concat( [Number_2, Sep_2], Chapter),
     Citation = bibl:[
        titles:Title, chapter:Chapter, verse:Number_3]:[] }.

citation(Citation) -->
   book_number(Number_1),
   book_name(Name),
   seperator(Sep_1),
   book_number(Number_2),
   seperator(Sep_2),
   book_number(Number_3),
   !,
   { concat([Number_1, ' ', Name, Sep_1], Title),
     concat([Number_2, Sep_2], Chapter),
     Citation = bibl:[
        title:Title, chapter:Chapter, verse:Number_3 ]:[] }.

citation(Citation) -->
   book_number(Number_1),
   book_name(Name),
   book_number(Number_2),
   seperator(Sep_2),
   book_number(Number_3),
   !,
   { concat([Number_1, ' ', Name], Title),
     concat([ Number_2, Sep_2],Chapter),
     Citation = citation:[
        titles:Title, chapter:Chapter, verse:Number_3]:[ ] }.

citation(Citation) -->
   book_name(Name),
   seperator(Sep_1),
   book_number(Number_2),
   seperator(Sep_2),
   book_number(Number_3),
   !,
   { concat([Name, Sep_1], Title),
     concat([ Number_2, Sep_2], Chapter),
     Citation = bibl:[
        titles:Title, chapter:Chapter, verse:Number_3 ]:[] }.

citation(Citation) -->
   book_name(Name),
   book_number(Number_2),
   seperator(Sep_2),
   book_number(Number_3),
   !,
   { concat([Number_2, Sep_2], Chapter),
     Citation = bibl:[
        titles:Name, chapter:Chapter, verse:Number_3 ]:[] }.

citation(Citation) -->
   book_name(Name),
   seperator(Sep),
   book_number(Number),
   !,
   { concat([Name, Sep], Chapter),
     Citation = bibl:[
        chapter:Chapter, verse:Number]:[] }.

seperator(Separator) -->
   [Separator],
   { member(Separator, ['.', ',', ';', ':', '-', '!']) }.


book_name(Name) -->
   [Name],
   { member(Name, [
        'Anmerk','Amos', 'Apg', 'Apostelg',
        'Chron', 'Chr', 'Cor', 'Dan', 'Es',
        'Halsgerichtsordnung', 'Hebr', 'Hiob',
        'Jac', 'Jer', 'Jerem', 'Joh', 'Jos',
        'Luc', 'Kap', 'K�n',
        'Macc', 'Maccab', 'Marc', 'Math', 'Matth', 'Mos', 'Mose',
        'Phil', 'Ps', 'Richt', 'R�m',
        'Sam', 'Schwabenspiegel', 'Sprichw',
        'Theuerd', 'Theuerdank', 'Zach' ] ) }.


book_number(Number) -->
   [N],
   ( { name_to_number(N, Number),
       number(Number) }
   ; { member(N, ['Kap', 'Art', 'Kapitel']),
       N = Number } ).


remark(remark:[]:[R]) -->
   [W],
   { member(W, ['Anm']) },
   [T],
   { member(T, ['.']),
     concat(W, T, R) }.


numbers(T) -->
   [W],
   ( { name_to_number(W, Number),
       number(Number),
       Number < 20 }
   ; { member(W, ['I', 'II', 'III', 'IV', 'V']),
       W = Number } ),
   [Z],
   { member(Z, ['.']),
     concat(Number, Z, T) }.

   
/******************************************************************/


