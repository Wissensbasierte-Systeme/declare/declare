

/******************************************************************/
/***                                                            ***/
/***          Adelung: Rendering in HTML                        ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_file_to_html, [
      adelung_file_to_html/0,
      adelung_file_to_html/2 ] ).


/*** interface ****************************************************/


/* adelung_file_to_html <-
      */

adelung_file_to_html :-
   dislog_variable_get(example_path, Examples),
   concat(Examples,
      'adelung/Sentences.xml', File_1),
   concat(Examples,
      'adelung/Adelung_marked_Examples.html', File_2),
   adelung_file_to_html(File_1, File_2).

adelung_file_to_html(File_1, File_2) :-
   dislog_variable_get(source_path,
      'projects/adelung_word_lists/adelung_file_to_html.fng',
      Path_fng),
   File = 'results_tmp.xml',
   fn_transform_xml_file_fng(Path_fng, File_1, File),
   dread(xml, File, [X]),
   dwrite(html, File_2, X).


/******************************************************************/


