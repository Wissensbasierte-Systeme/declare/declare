

/******************************************************************/
/***                                                            ***/
/***          Adelung: Highlight Keywords                       ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_highlight_keywords, [
      adelung_highlight_keywords/2,
      adelung_highlight_keywords/3 ] ).

:- dynamic
      suffix/1.


/*** interface ****************************************************/


/* adelung_highlight_keywords(File_1, File_2) <-
      */

adelung_highlight_keywords(File_1, File_2) :-
   dread(xml, File_1, [Xml_1]),
   adelung_highlight_keywords(_, Xml_1, Xml_2),
   dwrite(xml, File_2, Xml_2).

adelung_highlight_keywords(Key, X, Y) :-
   fn_item_transform(adelung_1:'--->', X, Doc),
   findall( [Def, Key_2],
      ( Def := Doc/'TEXT'/'BODY'/'DIV0'
           /'ENTRY'::[@'KEY'=Key]/'DEF',
        writeln(user, Key),
        adelung_remove_signs(Key, Key_2)),
      Pairs ),
   !,
   adelung_process_pairs(Pairs, Y).

adelung_remove_signs(Key_1, Key_2) :-
    member(C, ['* ', '+ ', 'Der ', 'Die ', 'Das ']),
    concat(C, Key_2, Key_1),
    !.
adelung_remove_signs(Key, Key).


/*** implementation ***********************************************/


/* adelung_process_pairs(Pairs, Defs) <-
      */

adelung_process_pairs(Pairs, Defs) :-
   maplist( adelung_process_definition_1,
      Pairs, Definitions ),
   Defs = defs:Definitions.


/* adelung_process_definition_1([Def_1, Key], Def_2) <-
      */

adelung_process_definition_1([Def_1, Key], Def_2) :-
   writeln(user, Key),
   Ns := Def_1/content::'*',
   ( word_with_prefix(Key, Ns, Ms)
   ; word_with_verb_ending(Key, Ns, Ms)
   ; word_with_noun_ending(Key, Ns, Ms)
   ; maplist( adelung_modify_part(key_word, Key),
        Ns, Ms ) ),
   append(Ms, Ks),
   !,
   items(Items, Ks, []),
   Def = def:['KEY':Key]:Items,
   !,
   fn_triple_condense(Def, Def_2).
adelung_process_definition_1([Def, _], Def).


/* word_with_prefix(Key, Ns, Ms) <-
      */

word_with_prefix(Key, Ns, Ms):-
   prefix(Prefix),
   concat(Prefix, B, Key),
   adelung_process_definition_2([Ns, Key], Ks_2),
   maplist( adelung_modify_part(basis, B),
      Ks_2, Ms ).


/* word_with_verb_ending(Key, Ns, Ms) <-
      */

word_with_verb_ending(Key, Ns, Ms):-
   concat(_, en, Key),
   adelung_process_definition_3([Ns, Key], Ms_1),
   maplist( adelung_modify_part(key_word, Key),
      Ms_1, Ms ).


/* word_with_noun_ending(Key, Ns, Ms) <-
      */

word_with_noun_ending(Key, Ns, Ms):-
   suffix(Suffix),
   concat(_, Suffix, Key),
   adelung_process_definition_3([Ns, Key], Ms_1),
   maplist( adelung_modify_part(key_word, Key),
      Ms_1, Ms ).


/* adelung_process_definition_2([Ns, Key], Ks_5) <-
      */

adelung_process_definition_2([Ns, Key], Ks_5) :-
   prefix(Prefix),
   concat(Prefix, B, Key),
   atom_chars(B, C),
   length(C, D),
   D > 4,
   maplist( adelung_modify_part(prefix, Prefix),
      Ns, Ms ),
   append(Ms, Ks_2),
   !,
   adelung_process_rest(B, Ks_2, Ks_4),
   adelung_process_noun(B, Ks_4, Ks_5).


/* adelung_process_definition_3([Ns, Key], Ks_5) <-
      */

adelung_process_definition_3([Ns, Key], Ms) :-
   ( adelung_exchange_ending(en-ung, Key, Key_2)
   ; adelung_exchange_ending(ung-en, Key, Key_2) ),
   maplist( adelung_modify_part(key_word, Key_2),
      Ns, Ks ),
   append(Ks, Ms),
   !.


/* adelung_process_rest(B, Ks_2, Ks_4) <-
      */

adelung_process_rest(B, Ks_2, Ks_4) :-
   concat(A, en, B),
   maplist( adelung_modify_part(basis, A),
      Ks_2, Ks_3 ),
   append(Ks_3, Ks_4).


/* adelung_process_noun(B, Ks_4, Ks_5) <-
      */

adelung_process_noun(B, Ks_4, Ks_5):-
   ( adelung_exchange_ending(en-ung, B, T),
     maplist( adelung_modify_part(basis, T),
        Ks_4, Ks_6 ),
     append(Ks_6, Ks_5)
   ; maplist( adelung_modify_part(basis, B),
        Ks_4, Ks_5 ) ).


/* adelung_exchange_ending(X-Y, Name_1, Name_2) <-
      */

adelung_exchange_ending(X-Y, Name_1, Name_2) :-
   concat(Name, X, Name_1),
   concat(Name, Y, Name_2).


/* adelung_modify_part(Tag, Key, Name, Names) <-
      1: key_word
      2: prefix
      3: basis */

adelung_modify_part(Tag, Key, Name, Names) :-
   atomic(Name),
   !,
   adelung_highlight_key(Tag, Key, Name, Name_2),
   name_split_at_position([" "], Name_2, Names).
adelung_modify_part(Tag, Key, Xml_1, [Xml_2]) :-
   fn_item_parse(Xml_1, T:As:Es_1),
   !,
   maplist( adelung_modify_part(Tag, Key),
      Es_1, Fs ),
   append(Fs, Es_2),
   Xml_2 = T:As:Es_2.


/* adelung_highlight_key(Tag, Key, Name_1, Name_2) <-
      */

adelung_highlight_key(Tag, Key, Name_1, Name_2) :-
   adelung_highlight_key_(Tag, Key, Name_1, Name_3),
   capital_letters(Key, Key_2),
   adelung_highlight_key_(Tag, Key_2, Name_3, Name_2).

adelung_highlight_key_(Tag, Key, Name_1, Name_2) :-
   name(Key, K1),
   concat(['<', Tag, '>', Key, '</', Tag, '>'], Key_2),
   name(Key_2, K2),
   name_exchange_sublist([[K1, K2]], Name_1, Name_2).

capital_letters(Name_1, Name_2) :-
   ( character_lower_to_capital(Name_1, Name_2)
   ; character_capital_to_lower(Name_1, Name_2)
   ; Name_2 = Name_1 ).


/*** adelung numbering ********************************************/


items([]) -->
   { true }.
items([I|Is]) -->
   item(I),
   items(Is).

item(I) -->
   xml_element(I).
item(I) -->
   token(I).
item(I) -->
   list_item(I).


/* xml_element(E, Xs, Ys) <-
      */

xml_element(T:As:Es_2) -->
   [E],
   { fn_item_parse(E, T:As:Es_1),
     items(Es_2, Es_1, []) }.

/* tokens(Ts, Xs, Ys) <-
      */

tokens([T|Ts]) -->
   ( token(T)
   ; xml_element(T) ),
   { \+ ( remark := T/tag::'*' ) },
   tokens(Ts).
tokens([]) -->
   { true }.


/* token(T, Xs, Ys) <-
      */

token(T) -->
   [X, Y],
   { adelung_numbering([X, Y], T) }.
token(T) -->
   [T],
   { \+ fn_item_parse(T, _:_:_),
     \+ list_item_start(T, _, _) }.

adelung_numbering([X, 'f.'], T) :-
   number(X),
   concat(X, ' f.', T).
adelung_numbering([X, Y], T) :-
   member(X, ['S.', 'Kap.']),
   atomic(Y),
   member(D, [',', '.']),
   concat_possible(N, D, Y),
   number(N),
   concat([X, ' ', Y], T).

adelung_numbering([X, Y], T) :-
   atomic(X),
   member(D1, [',', '.']),
   concat_possible(N1, D1, X),
   ( number(N1)
   ; member(N1, ['I', 'II', 'III', 'IV', 'V']) ),
   atomic(Y),
   member(D2, [',', '.']),
   concat_possible(N2, D2, Y),
   ( number(N2)
   ; member(N2, ['I', 'II', 'III', 'IV', 'V']) ),
   concat([X, ' ', Y], T).


/* concat_possible(X, Y, Z) <-
      */

concat_possible(X, Y, Z) :-
   name(Y, Ys),
   name(Z, Zs),
   append(Xs, Ys, Zs),
   name(X, Xs).


/* list_item(Item, Xs, Ys) <-
      */

list_item(item:[number:N|Type]:Ts) -->
   [S],
   { list_item_start(S, N, Type) },
   tokens(Ts),
   { ! }.


/* list_item_start(X, Y, As) <-
      */

list_item_start(X, Y, [text:X, type:T, delimiter:D]) :-
   atomic(X),
   member(D, [')', '.']),
   concat_possible(Y, D, X),
   ( number(Y),
%    member(Y, ['1', '2', '3', '4', '5']),
     T = arabic
   ; member(Y, ['I', 'II', 'III', 'IV', 'V']),
     T = roman
   ; member(Y, [a, b, c, d, e, f, g, h, i, j, k, l]),
     T = character ).
list_item_start(X, Y, [text:X, type:T, delimiter:'()']) :-
   atomic(X),
   name(X, Zs),
   append(Us, ")", Zs),
   append("(", Ys, Us),
   name(Y, Ys),
   ( number(Y),
     T = arabic
   ; member(Y, [a, b, c, d, e, f, g, h, i, j, k, l]),
     T = character ).

prefix(Prefix) :-
   member(Prefix, ['Auf', 'Ab', 'An', 'Aus', 'Bey']).


/******************************************************************/


