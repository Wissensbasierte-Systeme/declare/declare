

/******************************************************************/
/***                                                            ***/
/***          Adelung: POS Tagging                              ***/
/***                                                            ***/
/******************************************************************/


:- module( adelung_pos_tagging, [
      adelung_pos_tagging/0,
      adelung_pos_tagging/2 ] ).


/*** interface ****************************************************/


/* adelung_pos_tagging <-
      */

adelung_pos_tagging :-
   dislog_variable_get(example_path, Examples),
   concat(Examples, 'adelung/List_Numbering.xml', File_1),
   concat(Examples, 'adelung/POS_Tagging.xml', File_2),
   adelung_pos_tagging(File_1, File_2).

adelung_pos_tagging(File_1, File_2) :-
   dread(xml, File_1, [Xml_1]),
   adelung_find_words(Xml_1, Xml_2),
   dwrite(xml, File_2, Xml_2).


/* adelung_find_words(Xml_1, Xml_2) <-
      */

adelung_find_words(Xml_1, Xml_2) :-
   atomic(Xml_1),
   !,
   name_split_at_position([" "], Xml_1, Words),
   sentence(Xml_2, Words, []),
   !.
adelung_find_words(Xml, T:As:Es) :-
   fn_item_parse(Xml, T:As:Es_2),
   maplist( adelung_find_words,
      Es_2, Es ).

sentence(Ws, Xs, Ys) :-
   sentence_(Ws, Xs, Ys).


/*** implementation ***********************************************/


/* sentence_(Ws, Xs, Ys) <-
      */

sentence_([W|Ws]) -->
   word(W),
   ( sentence_(Ws)
   ; [],
     { Ws = [] } ).


word(W) -->
   abbreviation(W).

word(W) -->
   punctuation_mark(W).

word(W) -->
   comma(W).

word(w:[]:[W]) -->
  [W].


remark(remark:[]:[R]) -->
   [W],
   { member(W, ['Anm']) },
   [T],
   { member(T, ['.']),
     concat(W, T, R) }.


punctuation_mark(punctuation_mark:[]:[M]) -->
   [M],
   { member(M, ['.']) }.


comma(comma:[]:[M]) -->
   [M],
   { member(M, [',']) }.


abbreviation(abbr:[]:[Abbreviation]) -->
   [Abbr],
   { member(Abbr, [
        a, 'A', act, 'Activ', adj, adv, aftra,
        'Angels', 'Antw', arum, 'Augsb', 'Axelteeth', b, 'B',
        'Balla-c', bzw, c, car, 'Chur-Braunschw', 'Churf�rstl',
        'Conj', 'Cron', d, 'D�n', 'D�nisch', e, 'Engl', et, f,
        'F�min', 'fig�rl', 'Fig�rl', franz, 'Franz', 'Franz�s',
        g, genae, 'Genit', 'Gesn', 'Glossar', 'Goth', 'Griech',
        h, haneh, 'Hebr', 'Holl', 'Holl�nd',
        i, imperf, 'Imperf', inus, inusit, irreg,
        'Isl�nd', 'Ital', 'Kap', 'L', 'Lat',
        'Latein', 'Less', neutr, 'Nieders', nom, nomin,
        'Oberd', 'Oberdeutschl', 'Observat', 'Orth', 'Orthogr',
        'Particip', passiv, plur, 'Plur', pred, 'Ps',
        'R', recipr, reciproc, reg, regul, 'Rythm',
        'S', s, sal, 'Sal', 'Schaupl', 'Schleg', 'Schwed', sing,
        'u.s.f', singul, 'Sprichw', 'Span',
        'Th', 'Tit', 'Theuerd',
        ut, u, v, verb, vulg, w, z, 'Z' ] ) },
   [X],
   { member(X, ['.', '. ', ' . ']),
     concat([Abbr, X], Abbreviation) }.


/******************************************************************/


