

/******************************************************************/
/***                                                            ***/
/***          Adelung: Word List Operations                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* adelung_identical_word_lists(Pairs) <-
      */
      
adelung_identical_word_lists(File) :-
   findall( pair:[N_1, N_2|Entries],
      ( adelung_key(Name_1, N, Xs),
        adelung_key(Name_2, N, Ys),
        Name_1 \= Name_2, Xs \= [], Ys \= [],
        ord_set_distance_2(Xs, Ys, M, Entries),
        M < -1,
        N_1 = entry:[key:Name_1]:[],
        N_2 = entry:[key:Name_2]:[] ),
      Pairs ),
   dwrite(xml, File, dictionary:Pairs).


/* adelung_word_lists_assert(File) <-
      */
      
adelung_word_lists_assert(File) :-
   dread(xml, File, [X]),
   forall( Key := X/entry,
      ( Name := Key@key,
        Words := Key/content::'*',
        length(Words, N),
        assert(adelung_key(Name, N, Words)) ) ).


/* ord_set_distance_3(Xs, Ys, N) <-
      */
      
ord_set_distance_3(Xs, Ys, N) :-
   ord_intersection(Xs, Ys, Zs),
   length(Zs, M),
   M is -N.


/* ord_set_distance_2(Xs, Ys, N, Zs) <-
      */
      
ord_set_distance_2(Xs, Ys, N, Zs) :-
   ord_set_distance(Xs, Ys, N_1),
   ord_intersection(Xs, Ys, Zs),
   length(Zs, N_2),
   N is N_1 - N_2.


/* ord_set_distance(Xs, Ys, N) <-
      */
      
ord_set_distance(Xs, Ys, N) :-
   ord_subtract(Xs, Ys, Zs_1),
   ord_subtract(Ys, Xs, Zs_2),
   length(Zs_1, N_1),
   length(Zs_2, N_2),
   !,
   N is N_1 + N_2.


/******************************************************************/


