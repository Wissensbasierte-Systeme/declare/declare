

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Tests                                   ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(adelung_proper_numbering, 1) :-
   Items_1 = ['I)', a, 'II)', b, 'IV)', c, '1)', d],
   adelung_highlight_keywords:items(Items_2, Items_1, []),
   dwrite(xml, Items_2),
   adelung_proper_numbering:items(Items_3, Items_2, []),
   dwrite(xml, Items_3).


/******************************************************************/


