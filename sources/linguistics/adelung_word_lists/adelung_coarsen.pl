

/******************************************************************/
/***                                                            ***/
/***          Adelung:  Word Lists                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* adelung_to_word_list <-
      */

adelung_to_word_list :-
   retract_all(found_word_in_text(_, _)),
   forall( adelung:adelung_document(Key, Xml),
      ( writeln(user, Key),
        adelung_key_normalize(Key, Key_2),
        fn_triple_to_word_list(Key_2, Xml) ) ),
   adelung_found_words_in_text_to_rows(Rows),
   xpce_display_table(_, _, ['Word', '#'], Rows).


/* adelung_cycles(Cycles) <-
      */

adelung_cycles(Cycles) :-
   findall( cycle:[key:X, word:Y]:[],
      ( found_word_in_text(Key, Word),
        ( found_word_in_text(Word, Key)
        ; character_lower_to_capital(Word, Word_2),
          found_word_in_text(Word_2, Key) ),
        Key \= Word,
        sort([Key, Word], [X, Y]),
        \+ concat(X, _, Y) ),
      Cycles_2 ),
   sort(Cycles_2, Cycles),
   File = 'results/adelung_cycles.xml',
   dwrite(xml, File, cycles:Cycles).


/* adelung_coarsen(L, U) <-
      */

adelung_coarsen(L, U) :-
   File = 'results/adelung_coarsened_40_60.xml',
   adelung_coarsen(L, U, File).

adelung_coarsen(L, U, File) :-
   findall( entry:[key:Key]:Multiset,
      ( adelung:adelung_document(Key, Xml),
        writeln(user, Key),
        adelung_key_normalize(Key, Key_2),
        fn_triple_to_word_list(Key_2, Xml, Rows),
        adelung_rows_to_coarsened_entry(L, U, Rows, Multiset)),
      Xmls ),
   dwrite(xml, File, adelung:Xmls).

adelung_rows_to_coarsened_entry(L, U, Rows, Multiset) :-
   findall( word:[mult:N, global:Global]:[Word],
      ( member([Word, N], Rows),
        adelung_multiplicity(Word, Global),
        Global > L,
        Global < U ),
      Multiset ).


/*** implementation ***********************************************/


/* fn_triple_to_word_list(Key, Xml, Rows) <-
      */

fn_triple_to_word_list(Key, Xml, Rows) :-
   fn_triple_to_word_list(Key, Xml),
   adelung_found_words_in_text_to_rows(Rows),
   retract_all(found_word_in_text(_, _)).


/* fn_triple_to_word_list(Key, Xml) <-
      */

fn_triple_to_word_list(Key, Text) :-
   atomic(Text),
   !,
   Cut_Lists = [".", ",", ";", ":", "!", "?", "(", ")", " "],
   name_split_at_position(Cut_Lists, Text, Words),
   ( foreach(Word, Words) do
        assert(found_word_in_text(Word, Key)) ).

fn_triple_to_word_list(Key, Xml) :-
   fn_item_parse(Xml, _:_:Es),
   checklist( fn_triple_to_word_list(Key),
      Es ).


/* adelung_found_words_in_text_to_xml <-
      */

adelung_found_words_in_text_to_xml :-
   File = 'results/word_list1-20.xml',
   adelung_found_words_in_text_to_xml(File).

adelung_found_words_in_text_to_xml(File) :-
   retract_all(adelung_multiplicity(_, _)),
   adelung_found_words_in_text_to_rows(Rows),
   ( foreach([Word, N], Rows),
     foreach(word:[multiplicity:N, word:Word]:[], Xs) do
        assert(adelung_multiplicity(Word, N)) ),
   sort(Xs, Ys),
   dwrite(xml, File, words:Ys).


/* adelung_found_words_in_text_to_rows(Rows) <-
      */

adelung_found_words_in_text_to_rows(Rows) :-
   found_words_in_text(Words),
   ( foreach(Word, Words) ,foreach([Word, N], Rows) do
        word_to_multiplicity(Word, N) ).

found_words_in_text(Words) :-
   findall( Word,
      found_word_in_text(Word, _),
      Words_2 ),
   sort(Words_2, Words).

word_to_multiplicity(Word, Multiplicity) :-
   findall( Word,
      found_word_in_text(Word, _),
      Words ),
   length(Words, Multiplicity).
%  writeln(user, Word:Multiplicity).


/* adelung_key_normalize(Key_1, Key_2) <-
      */

adelung_key_normalize(Key_1, Key_2) :-
   member(Article, ['Der', 'Die', 'Das']),
   concat(Article, ' ', A),
   concat(A, Key_2, Key_1),
   !.
adelung_key_normalize(Key, Key).


/******************************************************************/


