

/******************************************************************/
/***                                                            ***/
/***      Deductive Databases: Magic Sets Transformation        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* magic_sets_transformation(
         +Program, +Query, ?Magic_Program, ?Adorned_Query) <-
      First, adorned/4 calculates the adorned program,
      then modify_ad_program/4 transforms the rules
      to get the magic program.
      Double rules are removed with remove_duplicates_magic/2
      and the seed Mag_Query for the magic program is computed
      by get_magic_atom/2 and added. */
 
magic_sets_transformation(Program_Id, Query) :-
   dconsult(Program_Id, Program),
   magic_sets_transformation(Program, Query, Magic_Program),
   dportray(lp_magic, Magic_Program).

magic_sets_transformation(Program, Query, Magic_Program) :-
   magic_sets_transformation(Program, Query, Magic_Program, _).

magic_sets_transformation(
      Program, Query, Magic_Program, Ad_Query) :-
   nonvar(Program),
   nonvar(Query),
   adorned(Query, Ad_Query, Program, Ad_Program),
   modify_ad_program(Ad_Program, Mag_Program_Dup),
   remove_duplicates_magic(Mag_Program_Dup, Mag_Program),
   get_magic_atom(Ad_Query, Mag_Query),
   Magic_Program = [[Mag_Query] | Mag_Program].


/*** implementation ***********************************************/


/* modify_ad_program(+Ad_Program, ?Mag_Program) <-
      For every adorned rule Head-Body generate a modified rule
      Head-[magic[Head]|Body],
      and add all magic rules: for every IDB-atom B in the body
      [A1,...,Ai,B,A(i+1),...,An] add a magic rule
      magic[B] :- magic[Head], A1, ..., Ai. */
 
modify_ad_program([],[]) :-
   !.
modify_ad_program([[Head] - Body|Ad_Rest], Mag_Program) :-
   get_magic_atom(Head, Magic_Head),
   Mag_Rule = [Head] - [Magic_Head | Body],
   get_magic_rules(Magic_Head, Body, Body, Magic_Rules),
   append([Mag_Rule|Magic_Rules], Mag_Rest, Mag_Program),
   modify_ad_program(Ad_Rest, Mag_Rest),
   !.
modify_ad_program([[Fact]|Ad_Rest], [[Fact]|Mag_Program]) :-
   modify_ad_program(Ad_Rest, Mag_Program),
   !.
modify_ad_program(_,_) :-
   write('The program can only contain rules of the form:'), nl,
   write('     Head - Body'), nl,
   write('     Fact'), nl, nl,
   fail.


/* get_magic_atom(+Atom, ?Magic_Atom) <-
      for a given atom Atom get Magic_Atom=magic[Atom] */
 
get_magic_atom(Atom, Magic_Atom) :-
   nonvar(Atom),
   Atom =.. [aug, Functor, Augment | Arg_List],
   !,
   get_bound_args(Augment, Arg_List, Bound_Args),
   Magic_Atom =.. [magic, Functor, Augment | Bound_Args].
get_magic_atom(_,_) :-
   write('The first argument must be of the form'), nl,
   write('aug(name, adornment, var1, var2,...)'),
   nl, nl,
   fail.


/* get_bound_args(+Aug, +Args, ?Bound) <-
      For a given list Aug of adornments and a given list Args
      of arguments, get the names of the bound arguments in
      Bound. */
 
get_bound_args([b|Aug], [X|Args], [X|Bound]) :-
   get_bound_args(Aug, Args, Bound),
   !.
get_bound_args([f|Aug], [_|Args], Bound) :-
   get_bound_args(Aug, Args, Bound),
   !.
get_bound_args([], [], []) :-
   !.
get_bound_args(Aug,_,_) :-
   write('Each Element of the list '), write(Aug), nl,
   write('must be ''f'' or ''b'''), nl,
   write('and the first and second list'), nl,
   write('must have the same length'), nl, nl,
   fail.


/* get_magic_rules(+M, +Body, ?Atoms, ?Rules) <-
      From the given body
         Body = [A1, ..., Ai, B, A(i+1), ... An]
      and an IDB-atom B create the body of a magic rule as
         Rule = [magic[B]] - [magic[Head], A1, ..., Ai]
      (M = magic[Head]).
      The Atoms ist the list [B, A(i+1), ..., An] */
  
get_magic_rules(_, _, [], []) :-
   !.
get_magic_rules(M, Body, [Atom | Atoms], [Rule|Rules]) :-
   nonvar(M),
   Atom =.. [aug|_],
   append(Rule_Body, [Atom |Atoms], Body),
   get_magic_atom(Atom, Magic_Atom),
   Rule = [Magic_Atom] - [M | Rule_Body],
   Rule \== [Magic_Atom] - [Magic_Atom],
   !,
   get_magic_rules(M, Body, Atoms, Rules).
get_magic_rules(M, Body, [_ | Atoms], Rules) :-
   get_magic_rules(M, Body, Atoms, Rules),
   !.
get_magic_rules(_,_,_,_) :-
   write('Error in get_magic_rules/4.'), nl, nl,
   fail.


/* remove_duplicates_magic(+Program_Dup, ?Program) <-
      remove_duplicates_magic/2 removes duplicate rules.
      The program Program_Dup is copied and the rules are normalized
      (variables are instantiated), so that they can be compared
      without unification of variables.
      A rule, for which the normalized program contains at least
      two equivalent rules, is added only once to Program. */
 
remove_duplicates_magic(Program_Dup, Program) :-
   normalize_program(Program_Dup, Program_Norm),
   check_duplicates(Program_Dup, Program_Norm, Program),
   !.
remove_duplicates_magic(_,_) :-
   write('Error in remove_duplicates_magic/2.'), nl, nl,
   fail.


/* normalize_program(+Program, ?Norm_Program) <-
      For a rule, copy the rule and instantiate the variables
      (name + number, number starting with 1) in the copy of the
      rule. */

normalize_program([],[]) :-
   !.
normalize_program([Rule|Rules], [Norm| Norms]) :-
   copy_term(Rule, Norm),
   replace_vars(Norm, 1, _),
   normalize_program(Rules, Norms),
   !.
normalize_program(_,_) :-
   write('Error in normalize_program/2.'), nl, nl,
   fail.


/* replace_vars(Norm, Nr, NewNr) <-
      */

replace_vars(Norm, Nr, NewNr) :-
   var(Norm),
   !,
   NewNr is Nr + 1,
   concat_atom(['X@$%@*)  :-)   ;-)', Nr], Norm).
replace_vars(Norm, Nr, NewNr) :-
   functor(Norm, _, N),
   replace_vars(0,N,Norm,Nr, NewNr),
   !.
replace_vars(_,_,_) :-
   write('Error in replace_vars/3.'), nl, nl,
   fail.

replace_vars(N,N,_,N1, N1) :-
   !.
replace_vars(I,N,Norm,N1, N3) :-
   I < N,
   !,
   I1 is I + 1,
   arg(I1, Norm, Arg),
   replace_vars(Arg, N1, N2),
   replace_vars(I1, N, Norm, N2, N3).
replace_vars(_,_,_,_,_) :-
   write('Error in replace_vars/5.'), nl, nl,
   fail.


/* check_duplicates(+Dup, +Norms, ?Prog) <-
      Check if a normalized rule (Norm) is contained in the
      normalized program (Norms),
      don't add the corresponding rule of Dup to Prog. */

check_duplicates([], [], []) :-
   !.
check_duplicates([_ | Dup], [Norm | Norms], Prog) :-
   member(Norm, Norms),
   !,
   check_duplicates(Dup, Norms, Prog).
check_duplicates([Rule | Dup], [_ | Norms], [Rule | Prog]) :-
   check_duplicates(Dup, Norms, Prog),
   !.
check_duplicates(_,_,_) :-
   write('Error in check_duplicates/3.'), nl, nl,
   fail.


/* dportray(lp_magic, +Program) <-
      Prints the rules of Program.
      Magic rules and magic predicates are transformed, i.e.:
         aug(name, [b,f], X,Y) ---> name_bf(X,Y)
         magic(name, [b,f], X) ---> magic_name_bf(X) */

dportray(lp_magic, Program) :-
   maplist( copy_term,
      Program, Program1 ),
   pp_magic(Program1, Program2),
   dportray(lp_xml, Program2),
   !.
dportray(lp_magic, _) :-
   write('Error in dportray(lp_magic, _).'), nl, nl,
   fail.

pp_magic([Head - Body | Rules], [Head1 - Body1|Rules1]) :-
   rewrite_head(Head, Head1),
   rewrite_body(Body, Body1),
   pp_magic(Rules, Rules1),
   !.
pp_magic([Head|Rules], [Head1|Rules1]) :-
   rewrite_head(Head, Head1),
   !,
   pp_magic(Rules, Rules1).
pp_magic([], []) :-
   !.
pp_magic(_,_) :-
   write('Error in pp_magic/2.'), nl, nl,
   fail.


/* rewrite_head(Head, New_Head1) <-
      */

rewrite_head([Head], [Head1]) :-
   !,
   rewrite_head(Head, Head1).
rewrite_head(Head, Head1) :-
   Head =.. [aug, Name, Aug | Args],
   !,
   concat_atom(Aug, Aug_String),
   concat_atom([Name, '_', Aug_String], Name1),
   Head1 =.. [Name1 | Args].
rewrite_head(Head, Head1) :-
   Head =.. [magic, Name, Aug | Args],
   !,
   concat_atom(Aug, Aug_String),
   concat_atom([magic, '_', Name, '_', Aug_String], Name1),
   Head1 =.. [Name1 | Args].
rewrite_head(Head, Head).
rewrite_body([Atom | Body], [Atom1 | Body1]) :-
   rewrite_head(Atom, Atom1),
   rewrite_body(Body, Body1).

/* rewrite_body(Body, New_Body) <-
      */

rewrite_body([], []).


/* adorned(+Query, ?Ad_Query, +Progam, ?Ad_Program) <-
      Construct new adorned version Ad_Program (resp. Ad_query)
      for program Program (resp. query Query). */

adorned(Query, Ad_Query, Program, Ad_Program) :-
   get_adorned_query(Query, Ad_Query),
   get_idb_edb_sorted(Program, IDB, EDB),
   get_ad_program(Ad_Query, Program, IDB, EDB, Ad_Program).

get_adorned_query(Query, Ad_Query) :-
   Query =.. [Functor | Args],
   not(current_op(_, _, Functor)),
   get_atom_adornment([], _, Args, Adornment),
   Ad_Query =.. [aug, Functor, Adornment | Args].


/* get_atom_adornment(+Bound, ?NewBound, +Args, ?Augs) <-
      With get_atom_adornment/4 you can get the adornment (Augs)
      for a given set of variables (Args) and a given set of bound
      variables (Bound),
      or you can get the new list of bound arguments (NewBound)
      and verify the adornment for a given set of variables
      (Args), bound variables (Bound) and adornments (Augs).
      Arguments have to be atomic or variables. */
 
get_atom_adornment(Bound, Bound, [], []) :-
   !.
get_atom_adornment(Bound, NewBound, [Arg|Args], [b|Augs]) :-
   atomic(Arg),
   !,
   get_atom_adornment(Bound, NewBound, Args, Augs).
get_atom_adornment(Bound, NewBound, [Arg|Args], [Aug|Augs]) :-
   var(Arg),
   in_list(Arg, Bound),
   !, 
   Aug = b,
   get_atom_adornment(Bound, NewBound, Args, Augs).
get_atom_adornment(Bound, [Arg|NewBound], [Arg|Args], [Aug|Augs]) :-
   var(Arg),
   Aug == b,
   !,
   get_atom_adornment(Bound, NewBound, Args, Augs).
get_atom_adornment(Bound, NewBound, [Arg|Args], [Aug|Augs]) :-
   Aug == f,
   (var(Arg); atomic(Arg)),
   !,
   get_atom_adornment(Bound, NewBound, Args, Augs).
get_atom_adornment(Bound, [Arg|NewBound], [Arg|Args], [f|Augs]) :-
   ( var(Arg)
   ; atomic(Arg) ),
   !,
   get_atom_adornment(Bound, NewBound, Args, Augs).

get_atom_adornment(_, _, [Arg|_], _) :-
   % no structure allowed
   % this could be changed: if all arguments of the structure are
   % bound, then the argument (structure) is also bound
   not(var(Arg)),
   not(atomic(Arg)),
   !,
   write('No structure allowed for argument '), write(Arg),
   write('!.'), nl, nl,
   fail.

get_atom_adronment(_,_,_,_) :-
   write('Error in get_atom_adornment/4.'), nl, nl,
   fail.


/* in_list(?X, +List)
      in_list/2 checks if an element is an element of a list,
      but unlike member/2 does not unify variables */

in_list(_, []) :-
   fail.
in_list(_, X) :-
   var(X),
   fail.
in_list(X, [Head | _]) :-
   X == Head,
   !.
in_list(X, [_ | Body]) :-
   in_list(X, Body).


/* comp_preds(Predicates) <-
      comp_preds/1 is the list of all comparison predicates (and
      the corresponding arity), which are allowed in a program. */

comp_preds([=,<,>,=<,>=,\=]).


/* get_idb_edb(+Program, ?IDB, ?EDB) <-
      For a given program Program, get_idb_edb_sorted/3 gets the
      intensional (IDB) and extensional (EDB) predicates.
      All arguments of extensional predicates have to be atomic.
      The predicate lists are sorted to remove duplicates. */

get_idb_edb_sorted(Program, IDB, EDB) :-
   get_idb_edb(Program, Unsort_IDB, Unsort_EDB),
   sort(Unsort_EDB, EDB),
   sort(Unsort_IDB, IDB).

get_idb_edb([], [], []) :-
   !.
get_idb_edb([Rule|Rules], IDB, [(Predicate, Arity)|EDB]) :-
   parse_dislog_rule(Rule, [Head], [], []),
   Head =.. [Predicate|Args],
   check_atomic_list(Args),
   length_of_list(Args, Arity),
   !,
   get_idb_edb(Rules, IDB, EDB).
get_idb_edb([Rule|Rules], [(Predicate, Arity)|IDB], EDB) :-
   parse_dislog_rule(Rule, [Head], _, _),
   Head =.. [Predicate|Args],
   length_of_list(Args, Arity),
   !,
   get_idb_edb(Rules, IDB, EDB).
get_idb_edb(_, _, _) :-
   writeln('Error in get_idb_edb/3.'),
   fail.   


check_atomic_list(Xs) :-
   checklist( atomic,
      Xs ).
check_atomic_list(_) :-
   writeln(
      'All arguments of extensional predicates have to be atomic.'),
   nl,
   fail.


/* get_ad_program(+Ad_Query, +Prog, +IDB, +EDB, ?Ad_Prog) <-
      get_ad_program/5 specifies a sip for the program Prog.
      First all predicates of the bodies of rules are sorted,
      so that the extensional predicates precede the intensional
      predicates (sort_program/4),
      then adorned_rules/6 searches for the first predicate
      in the body of a rule, which has a bound argument (because
      of the sorting, the extensional predicates are found first).
      This predicate is added to the body of the new rule and then
      the algorithm starts for the remaining body-predicates. */

get_ad_program(Ad_Query, Prog, IDB, EDB, Ad_Prog) :-
   sort_program(Prog, Sort_Prog, IDB, EDB),
   Ad_Query =.. [aug, Name, Aug | _],
   adorned_rules([], [(Name, Aug)], Sort_Prog, Ad_Prog, IDB, EDB).


/* sort_program(+Prog, ?NewProg, +IDB, +EDB) <-
      sort predicates of the bodies of rules, so that the extensional
      predicates precede the intensional predicates */

sort_program([], [], _, _) :-
   !.
sort_program([[Head] | Prog], [[Head] | NewProg], IDB, EDB) :-
   sort_program(Prog, NewProg, IDB, EDB),
   !.
sort_program([[Head] - B | P], [[Head] - NewB | NewP], IDB, EDB) :-
   get_idb_edb_body(B, I_Atoms, E_Atoms, Rest, IDB, EDB),
   append(Rest, E_Atoms, B1),
   append(B1, I_Atoms, NewB),
   sort_program(P, NewP, IDB, EDB),
   !.
sort_program(_,_,_,_) :-
   write('The program can only contain rules of the form:'), nl,
   write('     Head - Body'), nl,
   write('     Fact'), nl, nl,
   fail.


/* get_idb_edb_body(+Body, ?I_Body, ?E_Body, ?Rest +IDB, +EDB) <-
      get intensional (I_Atoms), extensional (E_Atoms) and binary
      build-in (Rest, i.e. comparison) predicates of a rule-body */

get_idb_edb_body([], [], [], [], _, _) :-
   !.
get_idb_edb_body([B | Bs], [B | I_Atoms], E_Atoms, Rest, IDB, EDB) :-
   functor(B, Name, Arity),
   member((Name, Arity), IDB),
   !,
   get_idb_edb_body(Bs, I_Atoms, E_Atoms, Rest, IDB, EDB).
get_idb_edb_body([B | Bs], I_Atoms, [B | E_Atoms], Rest, IDB, EDB) :-
   functor(B, Name, Arity),
   member((Name, Arity), EDB),
   !,
   get_idb_edb_body(Bs, I_Atoms, E_Atoms, Rest, IDB, EDB).
get_idb_edb_body([B |Bs], I_Atoms, E_Atoms, [B|Rest], IDB, EDB) :-
   functor(B, Name, 2),
   comp_preds(CP),
   member(Name, CP),
   !,
   get_idb_edb_body(Bs, I_Atoms, E_Atoms, Rest, IDB, EDB).
get_idb_edb_body(_, _, _, _, _, _):-
   write('An atom in the body is neither an extensional'), nl,
   write('nor an intensional predicate.'), nl, nl,
   fail.


/* adorned_rules(?S, +Augs, +Prog, ?Aug_Rules, +Idb, +EDB) <-
      For a given set of predicate-names and adornments (Augs) get
      the adorned rules of those rules, where the name and arity
      of the head predicate matches the given predicate name and
      arity (number of elements in the adornment),
      until all necessary adorned rules are considered (Augs = []).
      The first argument of adorned_rules/6 is the list of
      predicate_names and adornments, for which adorned rules have
      already been calculated */

adorned_rules(_, [], _, [], _, _).
adorned_rules(S, [Aug|Augs], Prog, Aug_Rules, IDB, EDB) :-
   member(Aug, S), !,
   adorned_rules(S, Augs, Prog, Aug_Rules, IDB, EDB).
adorned_rules(S, [Aug|Augs], Prog, Aug_Rules, IDB, EDB) :-
   sort_aug(Aug, NewAug, Prog, Aug_Rules2, IDB, EDB),
   append(Aug_Rules2, Aug_Rules3, Aug_Rules),
   append(Augs, NewAug, Augs1),
   adorned_rules([Aug | S], Augs1, Prog, Aug_Rules3, IDB, EDB),
   !.


/* sort_aug(+Aug, ?NewAug, +Prog, ?Aug_Rules, +IDB, +EDB) <-
      for a given predicate with given adornment get the sip for
      one rule, where the head-predicate has the same functor and
      arity as the given predicate.
      If this rule contains intensional predicates in the body,
      you need to get also new rules for these predicates
      (NewAug). */

sort_aug(_, [], [], [], _, _).
sort_aug(Aug, NewAug, [[Head]|Prog], [[Head]|Aug_Rules], IDB, EDB) :-
   sort_aug(Aug, NewAug, Prog, Aug_Rules, IDB, EDB).
sort_aug((Name,Aug), NewAug, [[H]-B|Prog], [Rule|Rules], IDB, EDB) :-
   H =.. [Name | Args],
   length_of_list(Args, Arity),
   length_of_list(Aug, Arity),
   !,
   % check if the adornment of H is the same as the given one
   % and get list of bound arguments (BoundArgs)
   get_atom_adornment([], BoundArgs, Args, Aug),
   R_Head =.. [aug, Name, Aug | Args],
   % get new Body (NewB) for adorned rule
   get_sip(NewAug1, B, NewB, BoundArgs, IDB, EDB),
   % remove duplicates from list NewAug1
   sort(NewAug1, NewAug2),
   Rule = [R_Head] - NewB,
   append(NewAug2, NewAug3, NewAug),
   sort_aug((Name, Aug), NewAug3, Prog, Rules, IDB, EDB),
   !.
sort_aug(Aug, NewAug, [_|Prog], Rules, IDB, EDB) :-
   sort_aug(Aug, NewAug, Prog, Rules, IDB, EDB).


/* get_sip(?ToDo, +Body, ?NewBody, +Bound, +IDB, +EDB) <-
      get first predicate of a rule-body, which has a bound argument.
      This predicate is added to the tail of the new rule.
      Repeat until all body-predicates are part of the new rule.
      ToDo is a list, which contains all adornments of body
      predicates, for which you also need to get adorned rules. */

get_sip([], [], [], _,_,_) :-
   !.
get_sip(ToDo, Body, [NewB|NewBody], Bound, IDB, EDB) :-
   get_first_bound(Body, B, Aug, Unbound, Rest, Bound, NewBound),
   append(Unbound, Rest, Body1),
   functor(B, Name, N),
   ( member((Name, N), IDB),
     !,
     B =.. [Name | Args],
     NewB =.. [aug, Name, Aug | Args],
     ToDo = [(Name, Aug) | ToDo1]
   ; member((Name, N), EDB), !,
     NewB = B,
     ToDo = ToDo1
   ; N = 2,
     comp_preds(CP),
     member(Name, CP),
     NewB = B,
     ToDo = ToDo1,
     ! ),
   get_sip(ToDo1, Body1, NewBody, NewBound, IDB, EDB).
get_sip(_,_,_,_,_,_) :-
   write('Error in get_sip/6.'), nl, nl,
   fail.


/* get_first_bound(+Body, ?B, ?Aug, ?UBound, ?R, +Bound, ?NBound) <-
      get the name of the first predicate (B), which has bound
      arguments, Body = [UBound, B, R].
      There should always be a predicate with bound arguments,
      otherwise the predicate doesn't contribute to the solution
      of the rule. */

get_first_bound([B|Body], B, Aug, [], Body, Bound, NBound) :-
   B = =(X,Y),
   get_atom_adornment(Bound, NBound, [X,Y], Aug),
   in_list(b,Aug),
   !.
get_first_bound([B|Body], B, Aug, [], Body, Bound, NBound) :-
   B =.. [Name, Arg1, Arg2],
   comp_preds(CP),
   member(Name, CP),
   get_atom_adornment(Bound, NBound, [Arg1, Arg2], Aug),
   Aug = [b,b],
   !.
get_first_bound([B|Body], B1, Aug, [B|UBound], R, Bound, NBound) :-
   B =.. [Name, _, _],
   comp_preds(CP),
   member(Name, CP),
   !,
   get_first_bound(Body, B1, Aug, UBound, R, Bound, NBound).
get_first_bound([B|Body], B, Aug, [], Body, Bound, NBound) :-
   B =.. [_ | Args],
   get_atom_adornment(Bound, NBound, Args, Aug),
   in_list(b, Aug),
   !.
get_first_bound([B|Body], B1, Aug, [B|UBound], R, Bound, NBound) :-
   get_first_bound(Body, B1, Aug, UBound, R, Bound, NBound),
   !.
get_first_bound(_,_,_,_,_,_,_) :-
   write('Error in get_first_bound/6.'), nl, nl,
   fail.


/* length_of_list(+List, ?Length) <-
      get length of a list. */

length_of_list(List, Length) :-
   nonvar(List),
   !,
   length_of_list(List, 0, Length).
length_of_list(_, _) :-
   write('First argument must be instantiated.'), nl, nl,
   fail.
length_of_list([], Length, Length).
length_of_list([_|List], N, Length) :-
   N1 is N + 1,
   length_of_list(List, N1, Length).


/***  tests *******************************************************/


test(deductive_databases:magic_sets, magic_sets_transformation) :-
   Program = [
      [tc(X, Y)]-[arc(X, Y)],
      [tc(X, Y)]-[arc(X, Z), tc(Z, Y)],
      [arc(a, b)], [arc(b, c)] ],
   magic_sets_transformation(Program, tc(a,Y),
      Magic_Program, Adorned_Query),
   dportray(lp_xml, Magic_Program),
   nl(user),
   dportray(lp_xml, [[]-[Adorned_Query]]),
   nl(user),
   dportray(lp_magic, Magic_Program).


/******************************************************************/


