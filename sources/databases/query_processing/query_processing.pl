

/******************************************************************/
/***                                                            ***/
/***      Deductive Databases: Recursive Query Processing       ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* up_flat_program_evaluate_all(
         Tools, Widths, Heights, Charts) <-
      */

up_flat_program_evaluate_all(
      Tools, Widths, Heights, Charts) :-
   findall( Tool:Chart,
      ( member(Tool, Tools),
        findall( (Width, Height):Time,
           ( member(Width, Widths),
             member(Height, Heights),
             up_flat_program_evaluate(
                Tool, Width, Height, Time) ),
           Chart ) ),
      Charts ),
   findall( [Width_Height, Colour]-Time,
      ( member(Tool:Chart_2, Charts),
        sort(Chart_2, Chart),
        nmr_tool_to_colour(Tool, Colour),
        member((Width, Height):Time, Chart),
        concat(['(', Width, ',', Height, ')'], Width_Height) ),
      Distribution ),
   findall( T,
      member(_-T, Distribution),
      Ts ),
   maximum(Ts, Max),
   Limit is Max + 0.1,
   sort(Distribution, Distribution_2),
   dislog_bar_chart(bridges, Distribution_2, Limit).

nmr_tool_to_colour(prolog, green).
nmr_tool_to_colour(dlv, orange).
nmr_tool_to_colour(smodels, red).


/* up_flat_program_evaluate(Tool, Width, Height, Time) <-
      */

up_flat_program_evaluate(prolog, Width, Height, Time) :-
   File = 'results/bridges',
   up_flat_program_to_file(Width, Height, File),
   consult(File),
   measure(
      findall( bridge(X, Y, Z),
         bridge(X, Y, Z),
         Bridges ),
      Time ),
   length(Bridges, K),
   write_list([
      'Width = ', Width, ', Height = ', Height, ' --> ',
      K, ' bridges']), nl,
   write_list([prolog, ' ', Time, ' sec.']), nl.
up_flat_program_evaluate(Tool, Width, Height, Time) :-
   member(Tool, [dlv, smodels]),
   up_flat_program(Width, Height, Program),
   concat(program_to_stable_models_, Tool, Predicate),
   measure(
      apply(Predicate, [Program, [Model]]), Time ),
   findall( bridge(X, Y, Z),
      member(bridge(X, Y, Z), Model),
      Bridges ),
   length(Bridges, K),
   write_list([
      'Width = ', Width, ', Height = ', Height, ' --> ',
      K, ' bridges']), nl,
   write_list([Tool, ' ', Time, ' sec.']), nl.


/* up_flat_program_to_file(Width, Height, File) <-
      */

up_flat_program_to_file(Width, Height, File) :-
   up_flat_program(Width, Height, Program),
   predicate_to_file( File,
      dportray(lp_xml, Program) ).


/* up_flat_program(Width, Height, Program) <-
      */

up_flat_program(Width, Height, Program) :-
   findall( Fact,
      ( between(1, Width, K),
        N is (K-1)*Height + 1,
        M is N + Height - 1,
        up_flat_facts(N, M, Fs),
        member(Fact, Fs) ),
      Facts_2 ),
   sort(Facts_2, Facts),
   Rules = [
      [tc(X, Y)]-[up(X, Y)],
      [tc(X, Y)]-[up(X, Z), tc(Z, Y)],
      [bridge(X, U, V)]-[
         tc(X, U),
         flat(U, V),
         tc(X, V)] ],
   append(Rules, Facts, Program).

up_flat_facts(N, M, Facts) :-
   findall( [flat(X, X)],
      between(N, M, X),
      Flats ),
   findall( [up(X, Y)],
      ( K is M - 1,
        between(N, K, X),
        Y is X + 1 ),
      Ups ),
   append(Flats, Ups, Facts).


/*** tests ********************************************************/


test(deductive_databases:up_flat_program_evaluate_all, 1) :-
   up_flat_program_evaluate_all(
      [prolog, dlv], [2, 4, 8], [20, 40], Charts),
   writeln_list(user, Charts).


/******************************************************************/


