

/******************************************************************/
/***                                                            ***/
/***      Deductive Databases: Magic Sets Evaluation            ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(deductive_databases:magic_sets, number) :-
%  N = 3, Query = anc(2,_),
   N = 33, Query = anc(25,_),
   File = 'examples/deductive/transitive_closure/anc_abcd.pl',
   dread(lp, File, Program),
   ( for(I, 1, N), foreach(Fact, Facts) do
        ( J is I+1, Fact = [par(I, J)] ) ),
   magic_sets_evaluation(Program, Facts, Query, State),
   dportray(dhs, State).


/*** interface ****************************************************/


/* magic_sets_evaluation_file(Program_Id, Query) <-
      */

magic_sets_evaluation_file(Program_Id, Query) :-
%  dconsult(Program_Id, Program),
   dread(lp, Program_Id, Program),
   magic_sets_evaluation(Program, Query, State),
   dportray(dhs, State).


/* magic_sets_evaluation(Program, Query, State) <-
      */

magic_sets_evaluation(Program, Query, State) :-
   magic_sets_evaluation(Program, [], Query, State).

magic_sets_evaluation(Program, Facts, Query, State) :-
   magic_sets_transformation(
      Program, Query, Magic_Program, Ad_Query),
   tps_fixpoint_iteration(1, Magic_Program, Facts, Magic_State),
%  dportray(lp_xml,Magic_Program),
%  dportray(dhs, Magic_State),
%  adorned(Query, Ad_Query, [], []),
   findall( [Query_Instance],
      ( member([Ad_Query], Magic_State),
        Ad_Query =.. [_, Predicate, _|Arguments],
        Query_Instance =.. [Predicate|Arguments] ),
      State ),
   !.


/* tp_fixpoint_magic(Program, Query, State) <-
      */

tp_fixpoint_magic(Program, Query, State) :-
   magic_sets_transformation_2(Program, Query, Program_2),
   tps_fixpoint(Program_2, [], State).

magic_sets_transformation_2(Program_1, Query, Program_2) :-
   magic_sets_transformation(Program_1, Query, Program_a),
   maplist( copy_term,
      Program_a, Program_b ),
   pp_magic(Program_b, Program_2).


/***  tests *******************************************************/


test(deductive_databases:magic_sets, magic_sets_evaluation) :-
   Program = [
      [tc(X, Y)]-[arc(X, Y)],
      [tc(X, Y)]-[arc(X, Z), tc(Z, Y)],
      [arc(a, b)], [arc(b, c)] ],
   magic_sets_evaluation(Program, tc(a,Y), I),
   dportray(dhs, I).
 

/******************************************************************/


