


/* Bottom-Up Interpreter fuer stratifizierbare 'normal programs' 
   Hans Argenton, Oktober 1989

 
    - name verlangt jetzt atom an erster Stelle, integers sind nicht
      atoms!
    - Ausgabeformat jetzt wie Eingabeformat: Fakten werden mit Punkt
      abgeschlossen, und Kommentare nach '%'.
    - teilweise neu formatiert
   Guenther Specht, Januar 1990

    - Angepasst an Sicstus Prolog
   Hans Argenton, Maerz 1993
*/




/* extended fixpoint: Aufruf; variable Fakten werden mit allen
                      gefundenen Individuenkonstanten instanziiert. 
*/

extfp(X) :- asserta(extend_flag),
            fp(X),
            retract(extend_flag).




/* query Praedikat: damit koennen nach der Fixpunktberechnung Anfragen
                    gestellt werden.
*/
qu(X) :- write('% query_facts:'),nl,nl,
         list_q_facts(X),         /* erscheint am Ende des Progr. */
         nl,
         abolish(vq_fact,1),
         fail.                   /* damit Vars auf Toplevel
				    nicht gebunden werden. */


/* fixpoint: Hauptprozedur, berechnet den kleinsten Fixpunkt 
             eines Logikprogramms.
*/
fp(X) :- my_init(State),
         clean_up,
         read_pg(X),
         stratifiable,
         find_ranks,
         compute_strata,
         print_fix,
         my_exit(State).

my_init(State) :- 
	    use_module(library(lists)),
	    unknown(State,fail).

my_exit(State) :- unknown(fail,State).


/* Aufraeumen
*/
clean_up :- abolish(fact,1),
            abolish(rule,2),
            abolish(g_fact,1),
            abolish(v_fact,1),
            abolish(indiv,1).



/* Einlesen des Programms: das Programm muss in PROLOG Syntax verfasst
   sein. Negative Literale in einem Regelkoerper werden an das Ende des 
   Regelkoerpers gesetzt; wichtig fuer die Abarbeitung, damit bei Aus-
   wertung von neg. Lit. saemtliche Bindungen durch Unifikation mit 
   Atomen schon gegeben sind.
*/
read_pg(X) :- see(X),
              read_clauses,
              get_indiv.

read_clauses :- read(Clause),
                scan_clause(Clause).


scan_clause(end_of_file) :- 
            !,seen.
scan_clause(':-'(Head, ','(Lit,BodyRest))) :- 
            !,body_list(','(Lit,BodyRest),BodyList),
            reorder_body(BodyList,SortedBody),
            assertz(rule(Head,SortedBody)),
            read_clauses.
scan_clause(':-'(Head,Body)) :- 
            !,assertz(rule(Head,[Body])),
            read_clauses.
scan_clause(Fact) :- 
            assertz(fact(Fact)),
            read_clauses.

body_list(Body,BodyList) :- 
            body_list(Body,[],BodyList).
body_list(','(Head,Tail),TempList,BodyList) :- 
            !, body_list(Tail,[Head|TempList],BodyList).
body_list(Body,TempList,[Body|TempList]).


reorder_body(Xs,Zs) :- 
            r_body(Xs,[],[],Zs).


r_body([not(X)|Xs],PosList,NegList,Zs) :- 
            !, r_body(Xs,PosList,[not(X)|NegList],Zs).
r_body([X|Xs],PosList,NegList,Zs) :- 
            r_body(Xs,[X|PosList],NegList,Zs).
r_body([],PosList,NegList,Zs) :- 
            append(PosList,NegList,Zs).


/* Ermitteln der in den Regeln des  Programms schon erscheinenden 
   Individuenkonstanten, nur fuer extfp.
*/
get_indiv :- extend_flag,  
             rule(X,[Y|Ys]),
             ground_args(X),
             find_indiv([Y|Ys]),
             fail.
get_indiv.

find_indiv([not(X)|Xs]) :- 
             !, 
             ground_args(X),
             find_indiv(Xs).
find_indiv([X|Xs]) :- 
             ground_args(X),
             find_indiv(Xs).
find_indiv([]).




/* Test auf Stratifizierbarkeit, Analogie zum Vorfahren-Problem: 
   Faktenbasis sind alle Praedikate in neg. Lit., 
   direkte Vorfahren eines Praedikats P sind alle Praedikate, die
   als Kopf in einer Regel vorkommen, in der P im Regelkoerper steht.
   Ueber diese Relation wird die trans. Huelle gebildet. 
   Falls ein Praedikat existiert, dass Vorfahre von sich selbst ist,
   ist das Programm nicht stratifizierbar (negation crosses recursion). 
*/                         
stratifiable :- abolish(str_anc,4),
	        abolish(delta,4),
	        abolish(new_delta,4),
                anc_base,
                comp_ancs,
                \+(str_anc(X,N,X,N)),!.
stratifiable :- write('Programm nicht stratifizierbar, '), nl,
                write('Negation crosses Recursion in'), nl,
                list_ancs,
                abolish(str_anc,4).

                

anc_base :- anc_loop.

anc_loop :- rule(X,Y),
            functor(X,P,A),
            member(Z,Y),
            anc_cond(P,A,Z),
            fail.
anc_loop :- abolish(delta,4).


anc_cond(P,A,not(Term)) :- !,functor(Term,P2,A2),
                      \+(str_anc(P,A,P2,A2)),
                      assertz(str_anc(P,A,P2,A2)),
                      assertz(new_delta(P,A,P2,A2)).

anc_cond(P,A,Term) :- functor(Term,P2,A2),
                      delta(P2,A2,P3,A3),
                      \+(str_anc(P,A,P3,A3)),
                      assertz(str_anc(P,A,P3,A3)),
                      assertz(new_delta(P,A,P3,A3)).

comp_ancs :- \+(new_delta(X,Y,Z,A)),!.
comp_ancs :- shift_deltas,
             anc_loop,
             comp_ancs.

shift_deltas :- new_delta(X,Y,Z,U), 
                assertz(delta(X,Y,Z,U)), 
                fail.
shift_deltas :- abolish(new_delta,4).


list_ancs :- str_anc(X,N,X,N),
             write('Praedikat  '),
             write(X),
             write('  Arity  '),
             write(N),nl,
             fail.
list_ancs.

/* Ermitteln der Praedikatsrangfolge (partielle Ordnung)
   Zusicherungen: 
      1. Jeder Rangwert von 0 bis max ist belegt.
      2. die rank Praedikate sind aufsteigend im Programm geordnet.
*/
find_ranks :- abolish(rank,3),
              abolish(pos_Lit,4),
              abolish(neg_Lit,4),

              pos_and_negs,
              rank_base,
              comp_ranks, 
              complete_ranks,  
              abolish(pos_Lit,4).


/* Katalogisieren, welche Praedikate als pos. und/oder neg. Lit. 
   vorkommen.
*/
pos_and_negs :- rule(X,Xs),
                member(Lit,Xs),
                pos_or_neg(Lit,X),
                fail.
pos_and_negs.

pos_or_neg(not(Atom),Head) :- !,
              functor(Atom,Pred,Arity),
              functor(Head,HPred,HArity),
              assertz(neg_Lit(Pred,Arity,HPred,HArity)).
pos_or_neg(Lit,Head) :- 
              functor(Lit,Pred,Arity),
              functor(Head,HPred,HArity),
              assertz(pos_Lit(Pred,Arity,HPred,HArity)).


/* Rangbasis: neg.Lit., die nicht als Regelkoepfe erscheinen 
   ( = undefiniert)
*/
rank_base :- neg_Lit(Pred,Arity,HP,HA),
             \+(neg_Lit(BPred,BArity,Pred,Arity)),
             \+(rank(Pred,Arity,0)),
             assertz(rank(Pred,Arity,0)),
             fail.
rank_base.



/* Ermitteln der Raenge */
comp_ranks :- rank(BPred,BArity,N1),
	      test_lit(BPred,BArity,HPred,HArity,N1,N),
              test_rank(HPred,HArity,N),
              fail.
comp_ranks.


test_lit(BPred,BArity,HPred,HArity,N1,N) :-
	neg_Lit(BPred,BArity,HPred,HArity),
	N is N1 + 1.

test_lit(BPred,BArity,HPred,HArity,N1,N1) :-
	pos_Lit(BPred,BArity,HPred,HArity).

test_rank(Pred,Arity,Rank) :- \+(rank(Pred,Arity,N)),!,
                              assertz(rank(Pred,Arity,Rank)).
test_rank(Pred,Arity,Rank) :- rank(Pred,Arity,N),
                              Rank > N,!,
                              retract(rank(Pred,Arity,N)),
                              assertz(rank(Pred,Arity,Rank)).



/* Komplettieren:
   Praedikate, die noch keinen Rang haben, erhalten Rang 0.
*/
complete_ranks :-  pos_Lit(Pred,Arity,HP,HA),
                   \+(rank(Pred,Arity,N)),
                   asserta(rank(Pred,Arity,0)),
                   fail.
complete_ranks.




/* sukzessives Berechnen der Fixpunkte der einzelnen Straten
*/
compute_strata :- fact_base,
                  strata,
                  last_stratum.

/* Behandlung der Faktenbasis, s. auch stratum_fp
*/
fact_base :- fact(X),
             ground_args(X),
             treat_fact(X),
             fail.
fact_base :- complete_fp,
             abolish(fact,1),abolish(new_delta,1).


/* Schleife zum Berechnen des Fixp. eines Stratums:
   Der kleinste noch existierende Rang N wird ermittelt 
   (durch Zusicherung tritt er in dem ersten rank-Fakt auf).
   Alle Regeln mit Praedikaten als Kopf, die einen Rang <= N haben,
   werden durch stratum(N) als str_rule (Stratum-Regel) zur Verfuegung 
   gestellt.
   stratum_base liefert saemtliche bis dahin deduzierte Fakten 
   als Anfangs-delta.
   stratum_fp berechnet den Fixpunkt des Stratums; 
   durch complete_fp wird er evtl. komplettiert.
   remove_stratum loescht die benutzten rank und rule Praedikate, sodass
   keine schon gebrauchten Regeln in der naechsten Iteration wieder 
   genommen werden.
   Durch strata werden nur die Straten berechnet, die Praedikate 
   enthalten, die als neg. Lit. im Programm erscheinen.  Das
   letzte Stratum bleibt uebrig, es wird durch last_stratum berechnet.
*/
strata :- rank(P,A,N),
          neg_Lit(P,A,HP,HA),
          stratum(N),
          stratum_base,
          stratum_fp,
          complete_fp,
          remove_stratum(N),
          fail.
strata.

stratum(N) :- rule(X,Xs),
              functor(X,P,A),
              rank(P,A,N1),
              \+(N1 > N),
              assertz(str_rule(X,Xs)),
              fail.
stratum(N).

stratum_base :- g_fact(X),assertz(delta(X)),fail.
stratum_base :- v_fact(X),assertz(delta(X)),fail.
stratum_base.

/* stratum_fp und complete_fp weiter unten.
  (Berechnung und evtl. Komplettierung des Fixp. eines normalen 
  Programms) 
*/

remove_stratum(N) :- rank(P,A,N1),
                     \+(N1 > N),
                     clean_negLits(P,A),
                     retract(rank(P,A,N1)),
                     functor(X,P,A),
                     retract(rule(X,Y)),
                     fail.
remove_stratum(N) :- abolish(str_rule,2).



clean_negLits(P,A) :- neg_Lit(P,A,X,Y),
                      retract(neg_Lit(P,A,X,Y)),
                      fail.
clean_negLits(P,A).



last_stratum :- rest_of_rules,
                stratum_base,
                stratum_fp,
                complete_fp,
                abolish(neg_Lit,4),
                abolish(rank,3),
                abolish(str_rule,2).





rest_of_rules :- rule(X,[Y|Ys]),
                 functor(X,P,A),
                 rank(P,A,N),
                 assertz(str_rule(X,[Y|Ys])),
                 fail.
rest_of_rules.





/* Berechnung des Fixp. eines normalen Programms
*/ 
stratum_fp :- fp_base,
              run_fp.

fp_base :- new_facts.

run_fp :- \+(new_delta(X)),!,
          abolish(delta,1).

run_fp :- abolish(delta,1),
          delta_shift,
          new_facts,
          run_fp.

delta_shift :- new_delta(X),
               assertz(delta(X)),
               fail.
delta_shift :- abolish(new_delta,1).




/* Iteration
*/
new_facts :- new_fact(X),
             ground_args(X),
             treat_fact(X),
             fail.
new_facts :- \+(new_delta(X)),!.
new_facts.



/* Deduktion eines neuen Fakts
*/
new_fact(X) :- str_rule(X,[Y|Ys]),
               proved_body([Y|Ys]).

proved_body(Ys) :- asserta(first_Lit),
                   append(Vs,[Y|Us],Ys), 
                   delta_test(Y),
                   abolish(first_Lit,0),
                   proved_body_part(Vs),
                   proved_body_part(Us).

/* Falls die Regel Atome enthaelt, muss mindestens ein Atom mit einem 
   Fakt aus der vorigen Iteration unifiziert werden koennen, sonst 
   wird kein neues sondern ein schon bekanntes Fakt deduziert.
   Falls die Regel nur neg. Lit. enthaelt, wird nur ein Deduktions-
   versuch unternommen (mit dem 1. Lit.), da neg.Lit. keine neuen 
   Bindungen liefern.
*/
delta_test(not(X)) :- 
                 !,
                 first_Lit,
                 treat_negLit(X).
delta_test(X) :- delta(Z),
                 unify(X,Z).

/* durch den delta Test entstehen neben dem delta Lit. zwei Regelteile,
   die jeweils durch proved_body_part behandelt werden.
*/
proved_body_part([X|Xs]) :- 
                 lit_test(X), 
                 proved_body_part(Xs).
proved_body_part([]).

lit_test(not(X)) :- !, 
               treat_negLit(X).
lit_test(X) :- g_fact(X).
lit_test(X) :- v_fact(Y),
               unify(X,Y).

treat_negLit(X) :- \+(g_fact(X)),
                   \+(unifies_v_fact(X)).

unifies_v_fact(X) :- v_fact(Y),
                     unify(X,Y).



/* Test auf groundness und gleichzeitig Erkennen neuer 
   Individuenkonstanten des deduzierten Fakts (fuer extfp)
*/
ground_args(X) :- asserta(ground_flag),
                  functor(X,F,N),
                  ground_args(X,N).


ground_args(X,N) :- N>0, 
                    arg(N,X,Arg),
                    ground_arg(Arg),
                    N1 is N-1,
                    ground_args(X,N1).
ground_args(X,0).


ground_arg(X) :- ground(X),!,
                 aux(X).
ground_arg(X) :- abolish(ground_flag,0).


ground(X) :- nonvar(X),
             atomic(X),!.
ground(X) :- nonvar(X),
             functor(X,F,N),
             ground(X,N).

ground(X,N) :- N>0,!,
               N1 is N-1,
               arg(N,X,Arg),
               ground(Arg),
               ground(X,N1).
ground(X,0).

aux(X) :- extend_flag,
          \+(indiv(X)),!,
          assertz(indiv(X)).
aux(X).




/* Entscheidung, ob ein nocht nicht bekanntes Fakt deduziert wurde,
   wenn ja, Einteilung in ground-fact oder variable-fact und Einfuegen
   in delta-neu.
   Dabei werden saemtliche Fakten und delta-neu-Fakten entfernt,
   die (Teil-)Instanziierungen des neu deduzierten Fakts sind
   (also insbesondere auch variable Fakten).
*/
treat_fact(X) :- ground_flag,!,
                 retract(ground_flag),
                 \+(g_fact(X)),
                 assertz(g_fact(X)),
                 assertz(new_delta(X)).
treat_fact(X) :- \+(inst_of_v_fact(X)),
                 clean_facts(X),
                 clean_delta(X),
                 assertz(v_fact(X)),
                 assertz(new_delta(X)).

inst_of_v_fact(X) :- 
                  v_fact(Y),
                  inst_of(X,Y).

clean_facts(X) :- g_fact(X),
                  retract(g_fact(X)),
                  fail.
clean_facts(X) :- v_fact(Y),
                  inst_of(Y,X),
                  retract(v_fact(Y)),
                  fail.
clean_facts(X).



clean_delta(X) :- retract(new_delta(X)),
                  fail.
clean_delta(X).   



/* Unifikation mit occurs-check
*/
unify(X,Y) :- var(X),var(Y),!,X=Y.
unify(X,Y) :- var(X),nonvar(Y),!,not_occurs_in(X,Y),X=Y.
unify(X,Y) :- var(Y),nonvar(X),!,not_occurs_in(Y,X),Y=X.
unify(X,Y) :- nonvar(X),nonvar(Y),atomic(X),atomic(Y),!,X=Y.
unify(X,Y) :- nonvar(X),nonvar(Y),\+(atomic(X)),\+(atomic(Y)),!,
              term_unify(X,Y).

term_unify(X,Y) :- functor(X,F,N),
                   functor(Y,F,N),
                   unify_args(N,X,Y).


unify_args(N,X,Y) :- N>0,!,
                     unify_arg(N,X,Y),
                     N1 is N-1,
                     unify_args(N1,X,Y).
unify_args(0,X,Y).


unify_arg(N,X,Y) :- arg(N,X,ArgX), 
                    arg(N,Y,ArgY),
                    unify(ArgX,ArgY).



/* Instanziierungs-Test mit occurs-check
*/
inst_of(X,Y) :- var(X),var(Y),!,X=Y.
inst_of(X,Y) :- var(Y),nonvar(X),!,not_occurs_in(Y,X),Y=X.
inst_of(X,Y) :- nonvar(X),nonvar(Y),atomic(X),atomic(Y),!,X=Y.
inst_of(X,Y) :- nonvar(X),nonvar(Y),\+(atomic(X)),\+(atomic(Y)),!,
                term_inst_of(X,Y).

term_inst_of(X,Y) :- functor(X,F,N),
                     functor(Y,F,N),
                     ti_args(N,X,Y).

ti_args(N,X,Y) :- N>0,!,
                  ti_arg(N,X,Y),
                  N1 is N-1,
                  ti_args(N1,X,Y).
ti_args(0,X,Y).


ti_arg(N,X,Y) :- arg(N,X,ArgX), 
                 arg(N,Y,ArgY),
                 inst_of(ArgX,ArgY).


/* occurs-check
*/
not_occurs_in(X,Y) :- var(Y),!,X\==Y.
not_occurs_in(X,Y) :- nonvar(Y),
                      atomic(Y),!.
not_occurs_in(X,Y) :- nonvar(Y),
                      \+(atomic(Y)),!,
                      functor(Y,F,N),
                      not_occurs_in(N,X,Y).


not_occurs_in(N,X,Y) :- N>0,!,
                        arg(N,Y,Arg),
                        not_occurs_in(X,Arg),
                        N1 is N-1,
                        not_occurs_in(N1,X,Y).
not_occurs_in(0,X,Y).




/* Komplettierung des Fixp. eines normalen Programms,
   nur fuer extfp.
*/
complete_fp :- extend_flag,
               v_fact(X),
               inst(X),
               \+(g_fact(X)),
               assertz(g_fact(X)),fail.
complete_fp.

inst(Term) :- functor(Term,Name,N),
              inst(0,N,Term).
inst(N,N,Term).
inst(I,N,Term) :- I<N,
                  I1 is I+1,
                  arg(I1,Term,Arg),
                  inst_arg(Arg),
                  inst(I1,N,Term).

inst_arg(Var) :- indiv(Var).
           



/* Auflisten des Fixpunktes eines Logikprogramms
*/
print_fix :- nl,write('% fixpoint:'),nl,
             list_fix.


list_fix :- g_fact(X),
            write(X),write('.'),nl,
            fail.
list_fix :- nl,
            list_v_facts.


list_v_facts :-  
            v_fact(X),
            listvars(X,1,N),
            write(X),write('.'),nl,
            fail.
list_v_facts :-  nl,nl.

/* pretty-print fuer Variablen
*/
listvars(VarName,N,N1) :- 
            var(VarName),!,
            N1 is N+1,
%           make_atom(N,NA),
%           name(NA,Y),
%           append([88],Y,String),
            name_append('X',N,VarName).
listvars(Term,N1,N2) :- 
	    nonvar(Term),!,
            functor(Term,Name,N),
            listvars(0,N,Term,N1,N2).
listvars(N,N,Term,N1,N1):- 
	    !.
listvars(I,N,Term,N1,N3) :- 
	    I<N,!,
            I1 is I+1,
            arg(I1,Term,Arg),
            listvars(Arg,N1,N2),
            listvars(I1,N,Term,N2,N3).




/* Auflisten der Antworten einer Anfrage
*/
list_q_facts(X) :- g_fact(X),
                   write(X),write('.'),nl,
                   fail.
list_q_facts(X) :- nl,
                   list_vq_facts(X).


list_vq_facts(X) :- v_fact(Y),
                    unify(X,Y), 
                    test_vfact(X), 
                    fail.
list_vq_facts(X).


test_vfact(X) :- ground(X),!,
                 \+(g_fact(X)),
                 \+(vq_fact(X)),
                 write(X),write('.'),nl.
test_vfact(X) :- listvars(X,1,N),
                 treat_vq(X).


treat_vq(X) :- vq_fact(X),!.
treat_vq(X) :- assertz(vq_fact(X)),
               write(X),write('.'),nl.



