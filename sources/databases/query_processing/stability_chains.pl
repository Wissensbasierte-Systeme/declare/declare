

/******************************************************************/
/***                                                            ***/
/***      Deductive Databases: Stability Chains                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* rule_to_chains(Rule,Chains) <-
      */

rule_to_chains(Rule,Chains) :-
   listvars(Rule,1),
   bar_line,
   write('rule:'),
   dportray(lp,[Rule]),
   findall( Chain,
      rule_to_chain(Rule,Chain),
      Chains_2 ),
   list_to_ord_set(Chains_2,Chains),
   findall( Chain,
      ( member(Chain,Chains),
        stability_chain(Chain) ),
      Stable_Chains ),
   write('stable chains:'),
   dportray(lp,Stable_Chains),
   findall( Chain,
      ( member(Chain,Chains),
        \+ stability_chain(Chain) ),
      Unstable_Chains ),
   write('unstable chains:'),
   dportray(lp,Unstable_Chains),
   bar_line.

rule_to_chain([A]-Atoms,[A]-Chain) :-
   member(B,Atoms),
   atoms_have_same_predicate(A,B),
   delete(Atoms,B,Atoms_2),
   atoms_are_connected_by_chain(Atoms_2,A,B,Chain_2),
   append(Chain_2,[B],Chain).
 
atoms_are_connected_by_chain(_,A1,A3,[v(X)]) :-
   atoms_are_connected_by_argument(A1,A3,X).
atoms_are_connected_by_chain(Atoms,A1,A3,[v(X),A2|As]) :-
   member(A2,Atoms),
   delete(Atoms,A2,Atoms_2),
   atoms_are_connected_by_argument(A1,A2,X),
   atoms_are_connected_by_chain(Atoms_2,A2,A3,As).

atoms_are_connected_by_argument(Atom_1,Atom_2,X) :-
   atom_to_argument(Atom_1,X),
   atom_to_argument(Atom_2,Y),
%  var(X), var(Y),
   X == Y.


/* atom_to_argument(Atom,Argument) <-
      */

atom_to_argument(Atom,Argument) :-
   Atom =.. [_|Arguments],
   member(Argument,Arguments).


/* atoms_have_same_predicate(Atom_1,Atom_2) <-
      */

atoms_have_same_predicate(Atom_1,Atom_2) :-
   Atom_1 =.. [P|Xs],
   Atom_2 =.. [P|Ys],
   length(Xs,N), length(Ys,N).


/* stability_chain(Chain) <-
      */

stability_chain([Atom_1]-[v(X)|Chain]) :-
   append(_,[v(Y),Atom_2],[v(X)|Chain]),
   \+ variables_are_at_different_position(Atom_1-X,Atom_2-Y).
 
variables_are_at_different_position(Atom_1-X,Atom_2-Y) :-
   Atom_1 =.. [_|Xs],
   Atom_2 =.. [_|Ys],
   nth(N_1,Xs,X),
   nth(N_2,Ys,Y),
   N_1 \= N_2.


/******************************************************************/


test(deductive_databases:stability_chains,1) :-
   dconsult('teaching/chain_analysis.dl',Program),
   maplist( rule_to_chains,
      Program, Chains ),
   writeln_list(Chains).


/******************************************************************/


