

/******************************************************************/
/***                                                            ***/
/***          DDK:  Rule Display                                ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(rule_analysis, wenn_dann_rules_listing) :-
   listing(dann/2).

test(rule_analysis, html_file_to_display) :-
   wenn_dann_rules_to_html(Html),
   wenn_dann_rules_to_display(Html).


/*** interface ****************************************************/


/* wenn_dann_rules_to_display(Html) <-
      */

wenn_dann_rules_to_display(Html) :-
   dislog_variable_get(home, '/results/tmp.html', Path),
   Header = [
      '<?xml version="1.0" encoding="utf8"?>' ],
   predicate_to_file( Path,
      ( writeln_list(Header),
        field_notation_to_xml(0, Html) ) ),
%  predicate_to_file( Path,
%       field_notation_to_xml(0, Html) ),
   html_file_to_display(Path).


/* wenn_dann_rules_to_html(Html) <-
      */

wenn_dann_rules_to_html(Html) :-
   findall( Lines,
      ( dann(wenn(Head), Tail),
        wenn_dann_rule_to_html((Tail :- Head), Lines) ),
      Xs ),
   length(Xs, N),
   append(Xs, Ys),
   Zs = [
      font:[color:blue]:['Regelmenge'],
      p:[], 'Es folgen ', N, ' Regeln.',
      br:[], p:[] ],
   append(Zs, Ys, Htmls),
   Meta = meta:['http-equiv':'Content-Type',
      content:'text/html; charset=utf-8']:[],
   Html = html:[Meta|Htmls].


/* wenn_dann_rule_to_html((Head :- Tail), Lines) <-
      */

wenn_dann_rule_to_html((Head :- Tail), Lines) :-
   Red = '#992222',
   formula_to_htmls(Head, Hs),
   formula_to_htmls(Tail, Ts),
   append([ [font:[color:Red]:['wenn &nbsp;']], Ts, [br:[]],
      [font:[color:Red]:['dann &nbsp;']], Hs, [p:[]] ], Lines).
%  Lines = [
%     font:[color:Red]:['wenn &nbsp;'], Tail, br:[],
%     font:[color:Red]:['dann &nbsp;'], Head, p:[] ].


/* formula_to_htmls(Formula, Htmls) <-
      */

formula_to_htmls(Formula, Htmls) :-
   Green = '#229922',
   Blue = '#222299',
   ( Formula = (A und B), Op = und, Color = Green
   ; Formula = (A oder B), Op = oder, Color = Blue ),
   !,
   formula_to_htmls(A, Htmls_A),
   formula_to_htmls(B, Htmls_B),
   append([ ['('], Htmls_A,
      [font:[color:Color]:[Op]|Htmls_B], [')'] ], Htmls).
formula_to_htmls(Formula, [Atom]) :-
   term_to_atom(Formula, Atom).


/******************************************************************/


