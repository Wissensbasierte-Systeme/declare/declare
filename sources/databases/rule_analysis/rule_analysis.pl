

/******************************************************************/
/***                                                            ***/
/***          Declare:  Rule Analysis                           ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      dann/2.
:- dynamic
      dann/2.

:- op(1000, fx,  wenn).
:- op(1100, yfx, dann).
:- op( 800, yfx, und).
:- op( 900, yfx, oder).
:- op( 950, yfx, ansonsten).


/*** tests ********************************************************/


test(rule_analysis, formula_to_picture) :-
   Term = (wenn 'A' = a und 'B' = b oder 'C' = c
           dann 'E' = e und 'F' = f),
   writeq(Term),
   term_to_picture(Term).

test(rule_analysis, rules_to_datalog) :-
   rules_to_datalog(Rules),
   dwrite(lp, 'rules.pl', Rules).

test(expert_atoms_containing_text, 1) :-
   expert_atoms_containing_text('Puffer').


/*** interface ****************************************************/


/* rules_to_datalog(Rules) <-
      */

rules_to_datalog(Rules_1, Rules_2) :-
   findall( Head-Body,
      ( member((wenn B dann A), Rules_1),
        rule_conjunction_to_list(B, Body),
        rule_disjunction_to_list(A, Head) ),
      Rules_2 ).

rules_to_datalog(Rules) :-
   findall( Head-Body,
      ( (wenn B dann A),
        rule_conjunction_to_list(B, Body),
        rule_disjunction_to_list(A, Head) ),
      Rules ).

rule_conjunction_to_list((A und B), Atoms) :-
   !,
   rule_conjunction_to_list(A, Atoms_A),
   rule_conjunction_to_list(B, Atoms_B),
   append(Atoms_A, Atoms_B, Atoms).
rule_conjunction_to_list(A, [A]).

rule_disjunction_to_list((A oder B), Atoms) :-
   !,
   rule_disjunction_to_list(A, Atoms_A),
   rule_disjunction_to_list(B, Atoms_B),
   append(Atoms_A, Atoms_B, Atoms).
rule_disjunction_to_list(A, [A]).
   

/* transtively_depends_on(Mode) <-
      */

transtively_depends_on(Mode) :-
   transtively_depends_on(Mode, Tc_Edges),
   ddbase_aggregate( [F_1, list(F_2)],
      ( member(F_1-F_2, Tc_Edges),
        \+ depends_on(Mode, F_1, F_2) ),
      Rows ),
   term_to_atom(transtively_depends_on:Mode, Title),
   Attributes = [dann, wenn],
   xpce_display_table(Title, Attributes, Rows).
%  html_display_table(Title, Attributes, Rows, size(300, 500)).


/* transtively_depends_on(Mode, Tc_Edges) <-
      */

transtively_depends_on(feature-feature, Tc_Edges) :-
   findall( Feature_1-Feature_2,
      ( depends_on(finding-finding, F_1, F_2),
        F_1 =.. [_, Feature_1, _],
        F_2 =.. [_, Feature_2, _] ),
      Edges ),
   edges_to_transitive_closure(Edges, Tc_Edges).
transtively_depends_on(finding-finding, Tc_Edges) :-
   findall( F_1-F_2,
      depends_on(finding-finding, F_1, F_2),
      Edges ),
   edges_to_transitive_closure(Edges, Tc_Edges).


/* depends_on(finding-finding, F_1, F_2) <-
      */

depends_on(finding-finding, F_1, F_2) :-
   dann(wenn(Formula_2), Formula_1),
   formula_to_findings(Formula_1, Fs_1),
   formula_to_findings(Formula_2, Fs_2),
   member(F_1, Fs_1),
   member(F_2, Fs_2).


/* formula_to_findings(Formula, Findings) <-
      */

formula_to_findings(F = V, [F = V]) :-
   !.
formula_to_findings(Formula, Fs) :-
   Formula =.. [_|Formulas],
   maplist( formula_to_findings,
      Formulas, Fss ),
   append(Fss, Fs).


/* rule_calls_rule_chain <-
      */

rule_calls_rule_chains :-
%  dislog_variable_get(ki_nergy_rules, 'Rules.pl', Path),
%  dislog_variable_get(ki_nergy_rules, 'Rules_Test.pl', Path),
   dislog_variable_get(ki_nergy_rules, 'Rules_Test_2.pl', Path),
   dread(lp, Path, Rules),
   findall( Chain,
      rule_calls_rule_chain(Rules, Chain),
      Chains ),
%  dislog_variable_get(ki_nergy_rules, 'Chains', Path_Chains),
   Path_Chains = user,
   predicate_to_file( Path_Chains, writeln_list(Chains) ).

rule_calls_rule_chain :-
   dislog_variable_get(ki_nergy_rules, 'Rules.pl', Path),
   dread(lp, Path, Rules),
   rule_calls_rule_chain(Rules, Chain),
%  star_line, writeln_list(Chain), star_line.
   star_line, dwrite(lp, Chain), star_line.

rule_calls_rule_chain(Rules, Chain) :-
   rule_calls_rule(Rules, Rule_1, Rule_2),
   rule_calls_rule_chain(Rules, [Rule_1, Rule_2], Chain).


/* rule_calls_rule_chain(Rules, Rules_1, Rules_2) <-
      */

rule_calls_rule_chain(Rules, Rules_1, Rules_2) :-
   Rules_1 = [Rule_1|_],
   rule_calls_rule(Rules, Rule, Rule_1),
   member(Rule, Rules_1),
   Rules_2 = [[cyclic], Rule|Rules_1].
rule_calls_rule_chain(Rules, Rules_1, Rules_2) :-
   Rules_1 = [Rule_1|_],
   rule_calls_rule(Rules, Rule, Rule_1),
   \+ member(Rule, Rules_1),
   !,
   rule_calls_rule_chain(Rules, [Rule|Rules_1], Rules_2).
rule_calls_rule_chain(_, Rules, Rules).
   

/* rule_calls_rule(Rules, Rule_1, Rule_2) <-
      */

rule_calls_rule(Rules, Rule_1, Rule_2) :-
   member(Rule_1, Rules), Rule_1 = _-Bs,
   member(Rule_2, Rules), Rule_2 = As-_,
   member(A, As), member(A, Bs).


/* expert_rules_to_number_of_atoms <-
      */

expert_rules_to_number_of_atoms :-
   dislog_variable_get(ki_nergy_rules, 'Rules.pl', Path),
   dread(lp, Path, Program),
   length(Program, M),
   expert_rules_to_atoms(Program, Atoms),
   ( foreach(Atom, Atoms) do writeq(Atom), nl ),
   length(Atoms, N),
   write_list(user, [N, ' atoms\n']),
   write_list(user, [M, ' rules\n']).


/* expert_rules_to_atoms(Rules, Atoms) <-
      */

expert_rules_to_atoms(Atoms) :-
   dislog_variable_get(ki_nergy_rules, 'Rules.pl', Path),
   dread(lp, Path, Program),
   expert_rules_to_atoms(Program, Atoms).

expert_rules_to_atoms(Rules, Atoms) :-
   maplist( expert_rule_to_atoms,
      Rules, Atomss ),
   append(Atomss, Atoms_2),
%  Atoms_2 = Atoms,
   sort(Atoms_2, Atoms).


/* expert_rule_to_atoms(Rule, Atoms) <-
      */

expert_rule_to_atoms(Rule, Atoms) :-
   Rule = Xs-Ys,
   append(Xs, Ys, Zs),
   sort(Zs, Atoms).


/* expert_atoms_containing_text(Text, Atoms) <-
      */

expert_atoms_containing_text(Text, Atoms) :-
   expert_rules_to_atoms(Atoms_2),
   structures_containing_text(Atoms_2, Text, Atoms).

expert_atoms_containing_text(Text) :-
   expert_atoms_containing_text(Text, Atoms),
   writelnq_list(Atoms).


/* structures_containing_text(Structures_1, Text, Structures_2) <-
      */

structures_containing_text(Structures_1, Text, Structures_2) :-
   findall( Structure,
      ( member(Structure, Structures_1),
        structure_contains_text(Structure, Text) ),
      Structures_2 ).


/* structure_contains_text(Structure, Text) <-
      */

structure_contains_text(Structure, Text) :-
   atomic(Structure),
   !,
   name_contains_name(Structure, Text).
structure_contains_text(Structure, Text) :-
   \+ atomic(Structure),
   nonvar(Structure),
   Structure =.. Structures,
   member(S, Structures),
   structure_contains_text(S, Text).


/* finding_rules_nicen(Rules_1, Rules_2) <-
      */

finding_rules_nicen(File) :-
   dread(lp, File, Rules_1),
   finding_rules_nicen(Rules_1, Rules_2),
   dislog_variable_get(example_path,
      '/deductive/rule_bases/Rules_nice.pl', Path),
   dwrite(lp, Path, Rules_2).

finding_rules_nicen(Rules_1, Rules_2) :-
   findall( Rule_2,
      ( member(Rule_1, Rules_1),
        finding_rule_nicen(Rule_1, Rule_2) ),
      Rules_2 ).

finding_rule_nicen(Head_1-Body_1, Rule) :-
%  writelnq(Head_1-Body_1),
   Head_1 = [und(A1, B1)], !,
   finding_atom_nicen(A1, A2),
   finding_atom_nicen(B1, B2),
   maplist( finding_atom_nicen, Body_1, Body_2 ),
   ( Rule = [A2]-Body_2
   ; Rule = [B2]-Body_2 ).
finding_rule_nicen(Head_1-Body_1, Head_2-Body_2) :-
%  writelnq(Head_1-Body_1),
   maplist( finding_atom_nicen, Head_1, Head_2 ),
   maplist( finding_atom_nicen, Body_1, Body_2 ).

finding_atom_nicen(finding(X, Ys), Atom) :-
   nonvar(X), !,
   Atom =.. [X, Ys].
finding_atom_nicen(Atom_1, Atom_2) :-
   nonvar(Atom_1),
   Atom_1 = und(A1, A2),
   finding_atom_nicen(A1, B1),
   finding_atom_nicen(A2, B2),
   Atom_2 = und(B1, B2),
   !.
finding_atom_nicen(Atom_1, Atom_2) :-
   nonvar(Atom_1),
   Atom_1 = ';'(A1, A2),
   finding_atom_nicen(A1, B1),
   finding_atom_nicen(A2, B2),
   Atom_2 = oder(B1, B2),
   !.
finding_atom_nicen(Atom, Atom) :-
   !.


/******************************************************************/


