

/******************************************************************/
/***                                                            ***/
/***          Declare:  Rules Engine - Connector                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* rule_connector(Seconds, File) <-
      In a loop, every number Seconds of seconds,
      the file File is read. */

rule_connector :-
   rule_connector(3, rule_connector_query).

rule_connector(Seconds, File) :-
   repeat,
      wait_seconds(Seconds),
      ( file_exists(File) ->
        catch(dread(txt, File, Text), _, fail),
        write('query: '), write(Text)
      ; writeln('no query') ),
   fail.

rule_connector_2(Seconds, File) :-
   repeat,
      wait_seconds(Seconds),
      ( file_exists(File) ->
        catch(hres_reasoning, _, fail),
        write('query: ')
      ; writeln('no query') ),
   fail.

hres_reasoning :-
   dread(lp, rule_connector_query_2, Rules),
   hres(Rules, [], I),
   writeln(Rules),
   writeln(I).


/******************************************************************/


