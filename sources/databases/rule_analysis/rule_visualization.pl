

/******************************************************************/
/***                                                            ***/
/***          DDK:  Rule Visualization                          ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(rule_analysis, wenn_dann_rules) :-
   Vertices = ['E'],
   wenn_dann_rules_to_reachable_edges(Vertices, Edges),
   writeln_list(Edges).


/*** interface ****************************************************/



/* wenn_dann_rules_to_reachable_edges(Vertices, Edges) <-
      */

wenn_dann_rules_to_reachable_edges(Vertices, Edges) :-
   wenn_dann_rules_to_edges(Edges_2),
   reachable_edges_multiple(Vertices, Edges_2, Edges),
%  edges_to_picture_bfs(Edges).
   change_management_rule_goal_edges_to_picture(Edges).


/* wenn_dann_rules_to_edges(Edges) <-
      */

wenn_dann_rules_to_edges(Edges) :-
   wenn_dann_rules_number(Numbered_Rules),
   findall( Es,
      ( member(Rule, Numbered_Rules),
        change_management_rule_to_edges(Rule, Es) ),
       Ess ),
   append(Ess, Edges).


/* change_management_rule_to_edges(Numbered_Rule, Edges) <-
      */

change_management_rule_to_edges(Numbered_Rule, Edges) :-
   Numbered_Rule = (N=dann(wenn(Formula_2), Formula_1)),
   formula_to_findings(Formula_1, Fs_1),
   formula_to_findings(Formula_2, Fs_2),
   findall( Edge,
      ( member(F1=_, Fs_1), member(F2=_, Fs_2),
        concat(rule_, N, R),
        ( Edge = F1-R
        ; Edge = R-F2 ) ),
      Edges ).

wenn_dann_rules_number(Numbered_Rules) :-
   findall( Rule,
      ( dann(wenn(Formula_2), Formula_1),
        Rule = dann(wenn(Formula_2), Formula_1) ),
      Rules ),
   length(Rules, N),
   generate_interval(1, N, I),
   pair_lists(=, I, Rules, Numbered_Rules).


/* change_management_rule_goal_edges_to_picture(Edges) <-
      */

change_management_rule_goal_edges_to_picture(Edges) :-
   maplist( change_management_finding_edge_to_edge,
      Edges, Edges_2 ),
   edges_to_vertices(Edges_2, Vertices),
   maplist( change_management_rule_goal_node_classify,
      Vertices, Vertices_1 ),
   ( foreach(A-B-C-D-E, Vertices_1),
     foreach(A-B-C-D-E-(0,0), Vertices_2) do
        true ),
   vertices_and_edges_to_picture(bfs, Vertices_2, Edges_2, _).

change_management_finding_edge_to_edge(X-Y, X-Y).

change_management_rule_goal_node_classify(Node, Node-N-triangle-blue-small) :-
   concat(rule_, N, Node),
   !.
change_management_rule_goal_node_classify(Node, Node-and-circle-grey-small) :-
   concat(and, _, Node),
   !.
change_management_rule_goal_node_classify(Node, Node-or-triangle-white-small) :-
   concat(or, _, Node),
   !.
change_management_rule_goal_node_classify(Node, Node-not-rhombus-orange-small) :-
   concat(not, _, Node),
   !.
change_management_rule_goal_node_classify(Node_1, Node_2) :-
   d3_vertex_classify(Node_1, Node_2).


/******************************************************************/


