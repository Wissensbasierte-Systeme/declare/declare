

/******************************************************************/
/***                                                            ***/
/***          Declare:  If-Then-Rule Analysis                   ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_get(home,
      '/projects/KI_Nergy/rules/', Path),
   dislog_variable_set(ki_nergy_rules, Path).


/*** interface ****************************************************/


/*
Workflow
 - read_if_then_rules
   KI_Nergy_Regeln.txt -> KI_Nergy_Regeln.pl
 - consult_if_then_rules
   KI_Nergy_Regeln.pl -> Prolog database
 - if_then_rules_refine
   Prolog database -> KI_Nergy_Regeln_refined.pl
*/


/* read_if_then_rules,
      */

read_if_then_rules :-
   dislog_variable_get(ki_nergy_rules,
      'KI_Nergy_Regeln.txt', File_1),
   dislog_variable_get(ki_nergy_rules,
      'KI_Nergy_Regeln.pl', File_2),
   read_if_then_rules(File_1, File_2).


/* consult_if_then_rules <-
      */

consult_if_then_rules :-
   retractall(dann(_,_)),
   dislog_variable_get(ki_nergy_rules,
      'KI_Nergy_Regeln.pl', File),
   consult(File).


/* if_then_rules_refine <-
      */

if_then_rules_refine :-
   findall( Rule,
      ( dann(wenn(Body_1), Head_1),
        Rule = dann(wenn(Body_1), Head_1) ),
      Rules_1 ),
   if_then_rules_refine(Rules_1, Rules_2),
   dwrite(pl, user, Rules_2),
   rules_to_datalog(Rules_2, Rules_3),
   dislog_variable_get(ki_nergy_rules,
      'KI_Nergy_Regeln_refined.pl', File),
   predicate_to_file( File, dwrite(lp, Rules_3) ).


/*** implementation ***********************************************/


/* read_if_then_rules(File_1, File_2) <-
      */

read_if_then_rules(File_1, File_2) :-
   read_file_to_name(File_1, Name_1),
   Substitution_1 = [
      [" an und aus ", " an_und_aus "],
      [" ggf. ", " ggf "],
      [". (", ".\n% ("],
      ["(Bemerkung:", "% (Bemerkung:"] ],
   name_exchange_sublist(Substitution_1, Name_1, Name_2),
   Substitution_2 = [
      ["Wenn ", "wenn '"],
      [", dann ", "' dann '"],
      [", ansonsten ", "' ansonsten '"],
      [" und ", "' und '"],
      [" oder ", "' oder '"],
      [".", "'."] ],
   name_exchange_sublist(Substitution_2, Name_2, Name_3),
   Substitution_3 = [
      [" an_und_aus ", " an und aus "],
      [" ggf ", " ggf. "] ],
   name_exchange_sublist(Substitution_3, Name_3, Name_4),
   predicate_to_file( File_2, writeln(Name_4) ).


/* if_then_rule_refine(Rule_1, Rule_2) <-
      */

if_then_rules_refine(Rules_1, Rules_2) :-
   maplist( if_then_rule_refine, Rules_1, Rules_2 ).

if_then_rule_refine(Rule_1, Rule_2) :-
   Rule_1 = dann(wenn(Body_1), Head_1),
   if_then_formula_refine(Body_1, Body_2),
   if_then_formula_refine(Head_1, Head_2),
   Rule_2 = dann(wenn(Body_2), Head_2).

if_then_formula_refine(Formula_1, Formula_2) :-
   Formula_1 = (A_1 oder B_1),
   if_then_formula_refine(A_1, A_2),
   if_then_formula_refine(B_1, B_2),
   Formula_2 = (A_2 oder B_2).
if_then_formula_refine(Formula_1, Formula_2) :-
   Formula_1 = (A_1 und B_1),
   if_then_formula_refine(A_1, A_2),
   if_then_formula_refine(B_1, B_2),
   Formula_2 = (A_2 und B_2).
if_then_formula_refine(Formula_1, Formula_2) :-
   Formula_1 = (A_1 ansonsten B_1),
   if_then_formula_refine(A_1, A_2),
   if_then_formula_refine(B_1, B_2),
   Formula_2 = (A_2 ansonsten B_2).
if_then_formula_refine(Formula_1, Formula_2) :-
   atomic(Formula_1),
   name_split_at_position([" "], Formula_1, [N|Ns]),
   Names = [
      'Gasverbrauch', 'Kessel', 'Kesselpumpe',
      'Mischer', 'Brenner', 'Kennlinie',
      'Temperatur', 'Speicherladepumpe', 'Zirkulationspumpe',
      'Ursache:', 'Außentemperaturfühler', 'Fehler:',
      'Wärmeverluste', 'Vorlauftemperatur', 'Kesselvolumenstrom',
      'T_RL', 'T_VL', 'RL_Temp', 'RL_T', 'STB',
      'WWB', 'WWT', 'HZG', 'HK', 'TWE', 'DWM' ],
   member(N, Names),
   Formula_2 = finding(N, Ns).
if_then_formula_refine(Formula_1, Formula_2) :-
   atomic(Formula_1),
   name_split_at_position([" "], Formula_1, Ns),
   Namess = [ ['KVS', des, 'Ventils'],
      ['HZG', 'Puffer', prim], ['HZG', 'Puffer'] ],
   member(Names, Namess),
   append(Names, Ms, Ns),
   concat_with_separator(Names, ' ', Name),
   Formula_2 = finding(Name, Ms).
if_then_formula_refine(Formula_1, Formula_2) :-
   atomic(Formula_1),
   name_split_at_position([" "], Formula_1, [N|Ns]),
   N = 'Anforderung',
   Formula_2 = anforderung(todo, Ns).
if_then_formula_refine(Formula_1, Formula_2) :-
   atomic(Formula_1),
   name_split_at_position([" "], Formula_1, Ns),
   last(Ns, 'angefordert'),
   Formula_2 = anforderung(done, Ns).
if_then_formula_refine(Formula, Formula).


/******************************************************************/


