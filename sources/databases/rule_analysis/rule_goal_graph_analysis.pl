

/******************************************************************/
/***                                                            ***/
/***        Declare:  Rule/Goal Graph Analysis                  ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(rule_analysis, rule_goal_graph_analysis(1)) :-
%  F = 'deductive/rule_bases/Rules.pl',
   F = 'deductive/rule_bases/Rules_nice.pl',
%  F = 'deductive/rule_bases/test.pl',
   dislog_variable_get(example_path, F, File),
   dread(lp, File, Program),
   rule_goal_graph_analysis(Program, Pairs),
   writeln_list(user, Pairs),
   star_line.
test(rule_analysis, rule_goal_graph_analysis(2)) :-
   Vs = [a,b,c,d,e],
   E = [a-b, b-c, c-b, d-e],
   vertices_edges_to_connected_components(Vs, E, F, CC),
   writeln_list(F), star_line,
   writeln_list(CC).


/*** interface ****************************************************/


/* rule_goal_graph_analysis(Program, Pairs) <-
      */

rule_goal_graph_analysis(Program, Pairs) :-
   program_to_rule_goal_graph(Program, Vertices,
      Positive, _Negative),
%  length(Vertices, N),
%  writeln(user, vertices(N)),
%  ttyflush, wait,
   star_line,
   writeln(user, Vertices), star_line,
   positive_edges_to_rule_edges(Positive, Rule_Edges),
%  edges_symmetrize(Rule_Edges, Edges),
   Rule_Edges = Edges,
   vertices_process(Vertices, Edges, Pairs_2),
   findall( V-Es,
      ( member(V-Es_2, Pairs_2),
        Es_2 \= [],
        intersection(Rule_Edges, Es_2, Es) ),
      Pairs_3 ),
   writeln_list(user, Pairs_3), star_line,
   findall( V,
      ( member(V, Vertices),
        concat(rule_, _, V) ),
      Vs ),
   dislog_variable_get(example_path,
      'deductive/rule_bases/rule_kb/', F),
   set_num(F, 0),
   ( foreach(_Rule_Id_3-CC_3, Pairs_3),
     foreach(_Rule-CC, Pairs) do
        ( maplist( rule_edge_to_rules(Program, Vs),
             CC_3, CC ),
%         rule_id_to_rule(Program, Vs, Rule_Id_3, Rule),
          rule_cc_to_rules(CC, Rules), star_line,
          ddk_gensym(F, File),
%         dwrite(lp, File, [Rule|Rules])
          dwrite(lp, File, Rules) ) ).

ddk_gensym(Root, Name) :-
   get_num(Root, Num),
   concat(Root, Num, Name).

rule_cc_to_rules(CC, Rules) :-
   findall( V,
      ( member(V-_, CC)
      ; member(_-V, CC) ),
      Vs ),
   sort(Vs, Rules).
%  edges_to_vertices(CC, Rules).

vertices_process([V|Vs], Es_1, [V-Es_2|Pairs]) :-
   reachable_edges(V, Es_1, Es_2),
   subtract(Es_1, Es_2, Es_3),
   vertices_process(Vs, Es_3, Pairs).
vertices_process([], _, []).

edges_symmetrize(Edges_1, Edges_2) :-
   findall( W-V,
      member(V-W, Edges_1),
      Edges_A ),
   union(Edges_1, Edges_A, Edges_B),
   sort(Edges_B, Edges_2).


/* vertices_edges_to_connected_components(
         Vertices, Edges, F, CC) <-
      */

vertices_edges_to_connected_components(Vertices, Edges, F, CC) :-
   vertices_edges_to_ugraph(Vertices, Edges, Graph),
   reduce(Graph, F),
   vertices(F,CC).


/* positive_edges_to_rule_edges(Positive, Edges) <-
      */

positive_edges_to_rule_edges(Positive, Edges) :-
   findall( R1-R2,
      ( member(R1-V, Positive),
        concat(rule_, _, R1),
        member(V-R2, Positive) ),
      Edges ).


/* rule_edge_to_rules(
         Program, Vertices, Id_1-Id_2, Rule_1-Rule_2) <-
      */

rule_edge_to_rules(Program, Vertices, Id_1-Id_2, Rule_1-Rule_2) :-
   rule_id_to_rule(Program, Vertices, Id_1, Rule_1),
   rule_id_to_rule(Program, Vertices, Id_2, Rule_2).


/* rule_id_to_rule(Program, Vertices, Rule_Id, Rule) <-
      */

rule_id_to_rule(Program, Vertices, Rule_Id, Rule) :-
%  length(Vertices, N1),
%  length(Program, N2),
%  nth(M1, Vertices, Rule_Id),
%  M2 is M1 - (N1 - N2),
%  M2 is M1 - (N1 - N2),
%  nth(M2, Program, Rule).
   nth(M, Vertices, Rule_Id),
   nth(M, Program, Rule).


/******************************************************************/


