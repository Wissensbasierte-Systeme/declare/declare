search_method_parameter(RT, Value) :-                                nl_m(info), write_m(info, ['Achtung: REVERSE Methodenaufruf!!!']),
   runtime_to_searched_id(RT, Id),                                   write_m(info, ['searched variable: ', Id]),
   runtime_to_method_id_and_param_list(RT, Method_Id, Param_List),   write_m(info, ['ParameterList = ', Param_List]),
   get_parameter_number(Id, Param_List, Number),                     write_m(info, ['Searched parameter is at number ', Number]),
	runtime_to_fitting_method_invocation(RT, Method_Id, Invoc_Path),  write_m(info, ['fitting Method-Invocation at ', Invoc_Path]),
	runtime_to_parameter_value(RT, Invoc_Path, Number, Value).

runtime_to_searched_id(_-(Init_Path:_), Id) :-
   jsquash_repository('variable-declaration', _, Id, _, Init_Path).

runtime_to_method_id_and_param_list(_-(Init_Path:_), Method_Id, Param_List) :-	
	jsquash_repository('method-declaration', Method_Id, _, Method_Param_Path, _), 
   Method_Param_Path \= [], 
   append(Method_Param_Path, _, Init_Path),
	findall( [Param_Id, Param_Path], 
      ( jsquash_repository('variable-declaration', _, Param_Id, [], Param_Path), 
        append(Method_Param_Path, _, Param_Path) ),
	   Param_List ).

get_parameter_number(Id, [[Id,_]|_], Number) :- 
   Number = 1,
   !.
get_parameter_number(Id, [_|Param_Ids], Number) :- 
   get_parameter_number(Id, Param_Ids, Next_Number), 
   Number is Next_Number + 1.	

% # Finde alle Methodenaufrufe, welche genau diese Methode aufrufen und sammle deren Pfade.
% # Dies l�sst sich herausfinden, indem �berpr�ft wird, ob das optional vorangestellte Objekt derjenigen Klasse angeh�rt,
% # in welcher die Methode definiert ist    
runtime_to_fitting_method_invocation(RT-(Init_Path:_), Method_Id, Invoc_Path) :-	
   first(Init_Path, File_Number),
   jsquash_repository('method-invocation', Ref, Qualifier, 
      Method_Id, _, _, Invoc_Path ),
   Ref = Object_Path:_,
   ( Object_Path \= [], Qualifier = [],
     eval(RT-Ref, [Class_Name, Package_Name|_]),
     jsquash_repository('java-class', Package_Name, Class_Name, File_Number)
   ; Object_Path = [], Qualifier \= [], 
     jsquash_repository('java-class', _, Qualifier, File_Number)
   ; Object_Path = [], Qualifier = [], Invoc_Path = [File_Number|_]).
    
% # Betrachte diese Methodenaufrufe, hole den X-ten Wert aus der jeweiligen Parameterliste und sammle diese Werte f�r das Ergebnis.
runtime_to_parameter_value(RT-(Init_Path:_), Invoc_Path, Number, Value) :-
	jsquash_repository('method-invocation', _, _, _, 
      Parameter_Expressions, Global_Method_Id, Invoc_Path),
   get_parameter_value_at_number(Number, Ref, Parameter_Expressions),
   get_runtime(RT, [method_invocation@rev_inst=Rev_Inst, 
      method_invocation@reverse_instance=Reverse_Instance]),
	append(Reverse_Instance, [Global_Method_Id], Reverse_Instance_2),		
	check_compatibility(Reverse_Instance_2, Rev_Inst, Max_Rev_Inst),  write_m(info, ['Referenz f�r den Parameter gefunden, suche Wert: ', Ref]),
   set_runtime(RT, RT2, [method_invocation@rev_inst=Max_Rev_Inst,
      method_invocation@reverse_instance=Reverse_Instance_2]),
   update_valuehistory_attr(RT2-_, RT3-_, Init_Path, _),
	eval(RT3-Ref, Value).

get_parameter_value_at_number(Number, Value, Parameter_Expressions) :- 
   get_parameter_value_at_number(Number, 1, Value, Parameter_Expressions).
get_parameter_value_at_number(Actual_Number, Actual_Number, P, [P|_]) :- !.
get_parameter_value_at_number(Searched_Number, Actual_Number, Value, [_|Parameter_Expressions]) :- 
	Actual_Number \= Searched_Number, Next_Number is Actual_Number + 1,
	get_parameter_value_at_number(Searched_Number, Next_Number, Value, Parameter_Expressions).