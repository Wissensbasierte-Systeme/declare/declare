decide_if_statement(RT-Ref, RT3, Possibility) :-                     write_m(rulename, ['    check *if-statement*']),
   get_runtime(RT, [inner_block@path=IB_Path, 
      inner_block@type=IB_Type]),
   member(IB_Type, [ifThenBlock, ifElseBlock]),                      write_m(info, ['########### AKTUELLER BLOCK: ', IB_Type]),
   jsquash_repository('if-statement', Cond_Ref, If, Else, _),
   member(IB_Path, [If, Else]),
   set_runtime(RT, RT2, [ current_loop@assert_loop_value=false]),    write_m(ifinfo, ['    Checking If-Condition']),
   eval(RT2-Cond_Ref, Condition),                                    write_m(ifinfo, ['    Wert der If-Condition: ', Condition]),
   decide_if_condition(RT-Ref, RT3, Condition, Possibility).

decide_if_condition(RT-Ref, RT3, Condition, Possibility) :-
   get_runtime(RT, [method_invocation@rev_inst=RevInst,
      inner_block@path=IB_Path, inner_block@type=IB_Type]),                        
   Condition = [boolean, C_Value, CondRevInst, _],   
   check_compatibility(RevInst, CondRevInst, _),   
   ( IB_Type = ifThenBlock, C_Value = true
   ; IB_Type = ifElseBlock, C_Value = false ),
   set_runtime(RT, RT2, [ current_block@path=IB_Path,
      method_invocation@rev_inst=CondRevInst]),                      write_m(info, [IB_Type, ' accepted ==> Forwarding']),
   check_local_actuality(RT2-Ref, RT3, Possibility).

decide_cond_expr(RT-_, Cond, C_Value, Ref, Value) :-
   Cond = [boolean, C_Value, RevInst, _],
   set_runtime(RT, RT2, [method_invocation@rev_inst=RevInst]),
   eval(RT2-Ref, Value).
   
decide_try_statement(RT-Ref, RT3, Possibility) :-                    write_m(rulename, ['    check *try-statements*']),
   get_runtime(RT, [inner_block@path=IB_Path, 
      inner_block@type=tryStatement]),
   first(Possibility, Init_Path),
   jsquash_repository('try-statement',_, Catch_Path, _, IB_Path),
   not( append(Catch_Path, _, Init_Path) ),
   set_runtime(RT, RT2, [ current_block@path=IB_Path ]),
   check_local_actuality(RT2-Ref, RT3, Possibility).