create_fn_objects(JavaClassesPath, XMLFilesPath, Files, Objects) :-   
   prepare_database,
   write('Transformation Java --> FN ................................ '),
   Endung = '.jaml.xml',
   findall( [FNObject, Package, FileName], 
      ( member([Package, FileName], Files), 
        transform_package_to_path(Package, Path), 
        concat([Path, FileName, Endung], SrcFile), 
        concat([JavaClassesPath, SrcFile], SrcFileName),
        concat([FileName, Endung], DstFile), concat([XMLFilesPath, DstFile], DstFileName),
        xml_beautifier(SrcFileName, DstFileName),
        src_to_fn_object(SrcFileName, FNObject) ), 
      Objects ),
   writeln('fertig').

transform_package_to_path(Package, Path) :-
   name(Package, List), transform_list_to_path(List, List2),
   append(List2, [47], List3), name(Path, List3).
   
transform_list_to_path([], []) :- !.
transform_list_to_path([46|A], [47|B]) :- transform_list_to_path(A,B).
transform_list_to_path([L|A], [L|B]) :- L \= 46, transform_list_to_path(A,B).

xml_beautifier(Old_XML_File, New_XML_File) :- 
	xml_file_to_fn_term(Old_XML_File, FN_Object), 
	fn_to_xml_file(FN_Object, New_XML_File).

src_to_fn_object(SrcFile, FNObject) :- xml_file_to_fn_term(SrcFile, [FNObject]).

create_repository(Objects) :-
   write('Erstellen des Repositories ................................ '),
   tell('h:/studium/Hauptstudium/Diplomarbeit/programming/prolog/files/switch.txt'),
   read_objects(Objects),
   create_return_expressions,
   told, writeln('fertig').
   
read_objects([], _) :- !.   
read_objects([O|Objects], FileNumber) :-
   O = [FNObject, Package, FileName],
   jaml_asserter('file', FNObject / 'file', [FileNumber]), 
   assert(jsquash_repository('java-class', Package, FileName, FileNumber)),
   NextFileNumber is FileNumber + 1, read_objects(Objects, NextFileNumber).
read_objects(Objects) :- read_objects(Objects, 0).

% # l�scht alles aus der Datenbank, was sp�ter mal asserted werden soll
prepare_database :- 
	retractall(jsquash_repository(_,_)),
	retractall(jsquash_repository(_,_,_)),
	retractall(jsquash_repository(_,_,_,_)),
	retractall(jsquash_repository(_,_,_,_,_)),
	retractall(jsquash_repository(_,_,_,_,_,_)),
	retractall(jsquash_repository(_,_,_,_,_,_,_)),
	retractall(sql_statement(_,_)),
   assert(jsquash_repository('global-method-id', 1)),
   assert(actualMethodInstance(0)),
   assert(jsquash_repository('global-loop-value-id', 1)).	