local_variables_in_system(Variable_Name) :-
   jsquash_repository('variable-declaration', _, Variable_Name, _, _).
   
field_variables_in_system(Field_Name) :-
   jsquash_repository('field-declaration', _, Field_Name, _, _).
   
local_variables_in_system(Class_Name, Variable_Name) :-
   jsquash_repository('java-class', _, Class_Name, Nr),
   jsquash_repository('variable-declaration', _, Variable_Name, _, Path),
   append([Nr], _, Path).

field_variables_in_system(Class_Name, Field_Name) :-
   jsquash_repository('java-class', _, Class_Name, Nr),
   jsquash_repository('field-declaration', _, Field_Name, _, Path),
   append([Nr], _, Path).

loops_and_conditions(Class_Name, Condition_Reference) :-
   jsquash_repository('java-class', _, Class_Name, Nr),
   jsquash_repository('while-statement', Condition_Reference, Path, _),
   append([Nr], _, Path).
   
methods_in_system(Class_Name, Method_Name, Signature) :-
   jsquash_repository('java-class', _, Class_Name, Nr), 
   jsquash_repository('method-declaration', Method_Name, Signature, _, Path), 
   append([Nr], _, Path).

methods_with_several_returns(Class_Name, Method_Name, Signature) :- 
   jsquash_repository('java-class', _, Class_Name, Nr),
   jsquash_repository('method-declaration', Method_Name, Signature, _, Path_1),
   append([Nr], _, Path_1),
   findall( Path_2, (
      jsquash_repository('return_expression', _, _, Path_2),
      append(Path_1, _, Path_2)),
   Paths),
   length(Paths, Length),
   Length > 1.
   
unnecessary_if_statements(If_Path) :-
   jsquash_repository('if-statement', Condition_Reference, _, _, If_Path), 
   init_runtime(RT), 
   forall( eval(RT-Condition_Reference, Value), Value = [boolean, false |_ ] ).