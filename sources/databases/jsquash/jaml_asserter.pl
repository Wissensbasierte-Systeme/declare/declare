
:- dynamic jsquash_repository/2.
:- dynamic jsquash_repository/3.
:- dynamic jsquash_repository/4.
:- dynamic jsquash_repository/5.
:- dynamic jsquash_repository/6.
:- dynamic jsquash_repository/7.
:- dynamic sql_statement/2.

% # /

jaml_asserter('array-access-expression', Array_Access_Expr, Path) :-
   element_to_position(Array_Access_Expr, Pos), 
   append(Path, [Pos], Access_Path),
   Type := Array_Access_Expr / 'type-ref'@'class-name',
   Id := Array_Access_Expr / _ / 'identifier', Id = _:_:[Identifier],
   ( Quali := Array_Access_Expr / 'array-qualifier' / 'expression' / 'variable-access-expression', 
     Pos := Quali@'pos', Kind = 'variable-access-expression', !
   ; Quali := Array_Access_Expr / 'array-qualifier' / 'expression' / 'field-access-expression', 
     Pos := Quali@'pos', Kind = 'field-access-expression', !
   ; Quali := Array_Access_Expr / 'array-qualifier' / 'expression' / _ / 'variable-access-expression', 
     Pos := Quali@'pos', Kind = 'variable-access-expression', !
   ; Quali := Array_Access_Expr / 'array-qualifier' / 'expression' / _ / 'field-access-expression', 
     Pos := Quali@'pos', Kind = 'field-access-expression', !
   ; !, fail ), 
	readQualifier(Quali, QualifierPath:QualifierName, _, Access_Path),
   ( QualifierPath = [], QualifierName = [], Qualifier = []
   ; Qualifier = QualifierPath:QualifierName ),
	jaml_asserter('array-access-list', Array_Access_Expr, AccessList, Access_Path),	!,
	reverse(AccessList, AccessList2), 
	assert(jsquash_repository('array-access-expression',Type, Identifier, Kind, AccessList2, Qualifier, Access_Path)).

jaml_asserter('array-access-list', Array_Access_Expression, Access_List, Path) :-	
   Next_Expression := Array_Access_Expression / 'array-qualifier' / 'expression',
   ( _ := Next_Expression / 'variable-access-expression'
   ; _ := Next_Expression / 'field-access-expression' ), 
   !,
	Number_Expression := Array_Access_Expression / 'expression',
	readNextExpression(Number_Expression, Access_Name, Access_Pos), 
   append(Path, [Access_Pos], Access_Path),
	jaml_asserter('expression', Number_Expression, Path),
	Access_List = [Access_Path:Access_Name].
	
jaml_asserter('array-access-list', Array_Access_Expression, [Access_Element|List], Path) :-
	Next_Expression := Array_Access_Expression / 'array-qualifier' / 'expression',
	Next_Access_Expression := Next_Expression / 'array-access-expression', 
   !,
	Number_Expression := Array_Access_Expression / 'expression',
	readNextExpression(Number_Expression, Access_Type, Access_Pos), 
   append(Path, [Access_Pos], Access_Path),
	jaml_asserter('expression', Number_Expression, Path),
	Access_Element = Access_Path:Access_Type,
	jaml_asserter('array-access-list', Next_Access_Expression, List, Access_Path).	
	
jaml_asserter('array-creation-expression', _, _) :- fail.
	
jaml_asserter('array-initializer-expression', Array_Initializer_Expr, Path) :-
	Expression_List := Array_Initializer_Expr / 'expression-list',
	element_to_position(Array_Initializer_Expr, List_Pos),
	append(Path, [List_Pos], List_Path),
	findall( Expr_Path:Expr_Type, 
      ( Expr := Expression_List / 'expression',
		  readNextExpression(Expr, Expr_Type, Expr_Pos),
		  append(List_Path, [Expr_Pos], Expr_Path),
		  jaml_asserter('expression', Expr, List_Path) ), 
   Expressions ),
	assert(jsquash_repository('array-declaration', Expressions, List_Path)).

jaml_asserter('assignment-expression', Assignment_Expr, Path) :-
	element_to_position(Assignment_Expr, Pos), 
   append(Path, [Pos], New_Path),
	jaml_asserter('left-hand-side', Assignment_Expr / 'left-hand-side', New_Path), 
	assignment_to_sign(Assignment_Expr, Sign),
	jaml_asserter('right-hand-side', Assignment_Expr / 'right-hand-side', New_Path),
	readNextExpression(Assignment_Expr / 'left-hand-side' / 'expression', Left_Expr, Left_Pos), 
	readNextExpression(Assignment_Expr / 'right-hand-side' / 'expression', Right_Expr, Right_Pos), 
   append(New_Path, [Left_Pos], Left_Path),
   append(New_Path, [Right_Pos], Right_Path),   
   % # Achtung: SONDERFALL bei folgender Zuweisung: 	X += Y wird zu X = X + Y
   ( Sign = '+=', S = '+'
   ; Sign = '-=', S = '-'
   ; Sign = '*=', S = '*'
   ; Sign = '/=', S = '/'
   ; S = Sign ),
   !,
   ( member(Sign, ['+=', '-=', '*=', '/=']),
     assert(jsquash_repository('binary-expression', Left_Path:Left_Expr, S, Right_Path:Right_Expr, New_Path)), 
     assert(jsquash_repository('assignment-expression', Left_Path:Left_Expr, '=', New_Path:'binary-expression', New_Path))
   ; assert(jsquash_repository('assignment-expression', Left_Path:Left_Expr, S, Right_Path:Right_Expr, New_Path)) ).
	
assignment_to_sign(Assignment, Sign) :-
   ( X := Assignment / 'equal', X = _:_:[Sign]
   ; X := Assignment / 'plus-equal', X = _:_:[Sign]
   ; X := Assignment / 'minus-equal', X = _:_:[Sign]
   ; X := Assignment / 'asterisk-equal', X = _:_:[Sign]
   ; X := Assignment / 'divide-equal', X = _:_:[Sign]
   ; X := Assignment / 'or-equal', X = _:_:[Sign]
   ; X := Assignment / 'ampersand-equal', X = _:_:[Sign]
   ; X := Assignment / 'remainder-equal', X = _:_:[Sign]
   ; X := Assignment / 'right-shift-equal', X = _:_:[Sign]
   ; X := Assignment / 'left-shift-equal', X = _:_:[Sign]
   ; X := Assignment / 'xor-equal', X = _:_:[Sign] ).
	
jaml_asserter('binary-expression', Binary_Expression, Path) :-
   element_to_position(Binary_Expression, Pos), 
   append(Path, [Pos], New_Path),
   readNextExpression(Binary_Expression / 'expr-1' / 'expression', Left_Expr, Left_Pos), 
   binary_expression_to_sign(Binary_Expression, Sign), 
   readNextExpression(Binary_Expression / 'expr-2' / 'expression', Right_Expr, Right_Pos), 
   jaml_asserter('expr-1', Binary_Expression / 'expr-1', New_Path),
   jaml_asserter('expr-2', Binary_Expression / 'expr-2', New_Path),
   append(New_Path, [Left_Pos], Left_Path),
   append(New_Path, [Right_Pos], Right_Path),
   assert(jsquash_repository('binary-expression', Left_Path:Left_Expr, Sign, Right_Path:Right_Expr, New_Path)).
	
binary_expression_to_sign(Binary_Expression, Sign) :-
   ( X := Binary_Expression / 'plus', X = _:_:[Sign]
   ; X := Binary_Expression / 'minus', X = _:_:[Sign]
   ; X := Binary_Expression / 'asterisk', X = _:_:[Sign]
   ; X := Binary_Expression / 'divide', X = _:_:[Sign]
   ; X := Binary_Expression / 'less-equal', X = _:_:[Sign]
   ; X := Binary_Expression / 'greater-equal', X = _:_:[Sign]
   ; X := Binary_Expression / 'left-chevron', X = _:_:[Sign]
   ; X := Binary_Expression / 'right-chevron', X = _:_:[Sign]
   ; X := Binary_Expression / 'equalequal', X = _:_:[Sign]
   ; X := Binary_Expression / 'not-equal', X = _:_:[Sign]
   ; X := Binary_Expression / 'remainder', X = _:_:[Sign]
   ; X := Binary_Expression / 'ampersand', X = _:_:[Sign]
   ; X := Binary_Expression / 'ampersand-ampersand', X = _:_:[Sign]
   ; X := Binary_Expression / 'or', X = _:_:[Sign]
   ; X := Binary_Expression / 'or-or', X = _:_:[Sign]
   ; X := Binary_Expression / 'xor', X = _:_:[Sign] ).

jaml_asserter('block', Block, Path) :-
   element_to_position(Block, Pos), 
   append(Path, [Pos], NewPath),
   forall( Statement := Block / 'statement', jaml_asserter('statement', Statement, NewPath) ).

jaml_asserter('break-statement', _, _).
	
jaml_asserter('case-statement', Case_Statement, Content) :-
   jaml_asserter('expression', Case_Statement / 'expression', ContentExp),
   Content = ['case', ContentExp].

jaml_asserter('catch-clause', Catch_Clause, Path) :-
   jaml_asserter('block', Catch_Clause / 'block', Path).
	
jaml_asserter('class-block', Class_Block, Path) :-
   element_to_position(Class_Block, Class_Pos), 
   append(Path, [Class_Pos], New_Path),
   assert(jsquash_repository('class-block', New_Path)),
   forall( 
      Element := Class_Block / 'class-block-element', 
      jaml_asserter('class-block-element', Element, New_Path) ).
	
jaml_asserter('class-block-element', Element, Path) :-
   element_to_position(Element, Pos),
   ( append(Path, [1, Pos], FieldPath), 
     jaml_asserter('field-declaration-statement', Element / 'field-declaration-statement', FieldPath)
   ; append(Path, [2, Pos], ConstrPath), 
     jaml_asserter('constructor-declaration', Element / 'constructor-declaration', ConstrPath)
   ; append(Path, [3, Pos], MethodPath), 
     jaml_asserter('method-declaration', Element / 'method-declaration', MethodPath)
   ; element_to_position(Element, P), write(P), write(': '), writeln('*UNKNOWN CLASSBLOCKELEMENT*')	),
   !.

jaml_asserter('class-definition', Class_Definition, Path) :-
   jaml_asserter('class-block', Class_Definition / 'class-block', Path).	

jaml_asserter('condition', Condition, Path, Expr_Path:Expr_Type) :-
   element_to_position(Condition, Pos), 
   append(Path, [Pos], Cond_Path),
   jaml_asserter('expression', Condition / 'expression', Cond_Path),
   readNextExpression(Condition / 'expression', Expr_Type, Expr_Pos), 
   append(Cond_Path, [Expr_Pos], Expr_Path).
	
jaml_asserter('conditional-expression', Cond_Expr, Path) :-
   element_to_position(Cond_Expr, Cond_Expr_Pos), append(Path, [Cond_Expr_Pos], Cond_Expr_Path),
   jaml_asserter('condition', Cond_Expr / 'condition', Cond_Expr_Path, Cond_Path:Cond_Type),
   readNextExpression(Cond_Expr / 'then-clause' / 'expression', Then_Type, Then_Pos),
   readNextExpression(Cond_Expr / 'else-clause' / 'expression', Else_Type, Else_Pos),
   jaml_asserter('expression', Cond_Expr / 'then-clause' / 'expression', Cond_Expr_Path),	
   jaml_asserter('expression', Cond_Expr / 'else-clause' / 'expression', Cond_Expr_Path),
   append(Cond_Expr_Path, [Then_Pos], Then_Path),
   append(Cond_Expr_Path, [Else_Pos], Else_Path),
   assert(jsquash_repository('conditional-expression',Cond_Path:Cond_Type, Then_Path:Then_Type, Else_Path:Else_Type, Cond_Expr_Path)).
	
jaml_asserter('constructor-declaration', Constr_Decl, Path) :-
   ( Pos := Constr_Decl / 'parameters'@pos, append(Path, [Pos], Param_Path)
   ; Param_Path = [] ),
	jaml_asserter('parameters', Constr_Decl / 'parameters', Param_Path),
	declaration_to_signature(Constr_Decl, Signature),
	Id := Constr_Decl / 'identifier', identifier_to_value(Id, Identifier),
   assert(jsquash_repository('constructor-declaration', Identifier, Signature, Param_Path, Path)),
	jaml_asserter('block', Constr_Decl / 'block', Path).
	
jaml_asserter('continue-statement', _, _).

jaml_asserter('do-statement', Do_Statement, Path) :-
	element_to_position(Do_Statement, Pos), append(Path, [Pos], New_Path),
	append(NewPath, [1], Temp_Cond_Path), 
   jaml_asserter('condition', Do_Statement / 'condition', Temp_Cond_Path, Cond_Path:Cond_Name),
	append(NewPath, [2], Block_Path), jaml_asserter('statement', Do_Statement / 'statement', Block_Path),
	append(NewPath, [5], Access_Path),
	% # der 4. Parameter ist false, weil dies die Abfrage f�r doWhile ist
	assert(jsquash_repository('while-statement', Cond_Path:Cond_Name, New_Path, 'true')),
	assertBlocksAccesses(New_Path, Access_Path).

jaml_asserter('else-clause', Else_Clause, Path, Else_Path) :-
	element_to_position(Else_Clause, Pos), 
   append(Path, [Pos], Else_Path),
	jaml_asserter('statement', Else_Clause / 'statement', Else_Path).
	
jaml_asserter('empty-statement', _, _).
	
jaml_asserter('expr-1', Expr1, Path) :-
	jaml_asserter('expression', Expr1 / 'expression', Path).

jaml_asserter('expr-2', Expr2, Path) :-
   jaml_asserter('expression', Expr2 / 'expression', Path).

jaml_asserter('expression', Expression, Path) :-
   ( Expr := Expression / 'conditional-expression', jaml_asserter('conditional-expression', Expr, Path)
   ; Expr := Expression / 'array-creation-expression', jaml_asserter('array-creation-expression', Expr, Path)
   ; Expr := Expression / 'array-access-expression', jaml_asserter('array-access-expression', Expr, Path)
   ; Expr := Expression / 'array-initializer-expression', jaml_asserter('array-initializer-expression', Expr, Path)
   ; Expr := Expression / 'parenthesis-expression', jaml_asserter('parenthesis-expression', Expr, Path)
   ; Expr := Expression / 'prefix-expression', jaml_asserter('prefix-expression', Expr, Path)
   ; Expr := Expression / 'postfix-expression', jaml_asserter('postfix-expression', Expr, Path)
   ; Expr := Expression / 'variable-declaration-expression', jaml_asserter('variable-declaration-expression', Expr, Path)
   ; Expr := Expression / 'method-invocation', jaml_asserter('method-invocation', Expr, Path)
   ; Expr := Expression / 'instance-creation', jaml_asserter('instance-creation', Expr, Path)
   ; Expr := Expression / 'this-expression', jaml_asserter('this-expression', Expr, Path)
   ; Expr := Expression / 'field-access-expression', jaml_asserter('field-access-expression', Expr, Path)
   ; Expr := Expression / 'variable-access-expression', jaml_asserter('variable-access-expression', Expr, Path)
   ; Expr := Expression / 'assignment-expression', jaml_asserter('assignment-expression', Expr, Path)
   ; Expr := Expression / 'literal-expression', jaml_asserter('literal-expression', Expr, Path)
   ; Expr := Expression / 'binary-expression', jaml_asserter('binary-expression', Expr, Path)
   ; element_to_position(Expression, Pos),
     write(Pos), write(': '), writeln('*UNKNOWN EXPRESSION*'), writeln(Expression) ),
   !.

jaml_asserter('expression-statement', Expr_Statement, Path) :-
   jaml_asserter('expression', Expr_Statement / 'expression', Path).

jaml_asserter('field-access-expression', Field_Access_Expr, Path) :-
	element_to_position(Field_Access_Expr, Pos), append(Path, [Pos], New_Path),
	Id := Field_Access_Expr / 'identifier', identifier_to_value(Id, Identifier),
	readQualifier(Field_Access_Expr, Qualifier_Path:Qualifier_Type, Qualifier, New_Path),
   ( Qualifier_Path = [], Qualifier_Type = [], Qualifier_2 = Qualifier, !
   ; Qualifier_2 = Qualifier_Path:Qualifier_Type),
	  assert(jsquash_repository('field-access-expression', [], Identifier, Qualifier_2, New_Path) ).
	
jaml_asserter('field-declaration-list', Field_Decl_List, Type, Path) :-
   forall(Field_Declaration := Field_Decl_List / 'field-declaration',
      ( element_to_position(Field_Declaration, Pos), 
        append(Path, [Pos], New_Path),
        Id := Field_Declaration / 'identifier', identifier_to_value(Id, Identifier),
        ( jaml_asserter('initializer', Field_Declaration / 'initializer', Expression, New_Path)
        ; Expression = []:[] ),
        assert(jsquash_repository('field-declaration', Type, Identifier, Expression, New_Path)) ) ).
	
jaml_asserter('field-declaration-statement', Field_Decl_Stmt, Path) :-
   jaml_asserter('type', Field_Decl_Stmt / 'type', Type),
	jaml_asserter('field-declaration-list', Field_Decl_Stmt / 'field-declaration-list', Type, Path).

jaml_asserter('file', File, Path) :-
	jaml_asserter('type-definition', File / 'type-definition', Path).

jaml_asserter('finally-clause', Finally_Clause, Path) :-
	jaml_asserter('block', Finally_Clause / 'block', Path).

jaml_asserter('for-init-statement', For_Init_Stmt, Path) :-
	findall(_, jaml_asserter('expression', For_Init_Stmt / 'expression', Path), _).

jaml_asserter('for-statement', For_Statement, Path) :-
	element_to_position(For_Statement, Pos), 
   append(Path, [Pos, Pos], Loop_Path),
   % # Die last_loop_values erhalten den Pfad: Loop_Path + [0] (==> an erster Stelle innerhalb der Schleife)
   % # somit haben sie folgende Pfade Path + [Pos, Pos, 0]
   % # Der Schleifenpfad bildet sich durch anh�ngen von [Pos, Pos] an Path
	% # Die Init-Statements sollen VOR die Schleife ==> append [Pos, 0]
	append(Path, [Pos, 0], Init_Path), 
   jaml_asserter('for-init-statement', For_Statement / 'for-init-statement', Init_Path),
	% # Der Rest steht IN der Schleife ==> neuer PATH
	append(Loop_Path, [1], Temp_Cond_Path), 
   append(Loop_Path, [2], Block_Path), 
   append(Loop_Path, [3], Update_Path),
   append(Loop_Path, [5], Access_Path),
   jaml_asserter('condition', For_Statement / 'condition', Temp_Cond_Path, Cond_Path:Cond_Name),
	jaml_asserter('statement', For_Statement / 'statement', Block_Path), 
   jaml_asserter('for-update-statement', For_Statement / 'for-update-statement', Update_Path),
	% # der 4. Parameter ist false, weil dies die Abfrage f�r doWhile ist
	assert(jsquash_repository('while-statement', Cond_Path:Cond_Name, Loop_Path, 'false')),
	assertBlocksAccesses(Loop_Path, Access_Path).
		
jaml_asserter('for-update-statement', For_Update_Stmt, Path) :-
	findall(_, jaml_asserter('expression', For_Update_Stmt / 'expression', Path), _).

identifier_to_value(Identifier, Value) :- Identifier = _:_:[Value].

jaml_asserter('if-statement', If_Statement, Path) :-
	element_to_position(If_Statement, Pos), 
   append(Path, [Pos], New_Path),
	jaml_asserter('condition', If_Statement / 'condition', New_Path, Cond_Reference),
	jaml_asserter('then-clause', If_Statement / 'then-clause', New_Path, Then_Path),
	( jaml_asserter('else-clause', If_Statement / 'else-clause', New_Path, Else_Path)
   ; Else_Path = ['undefined'] ),
	assert(jsquash_repository('if-statement', Cond_Reference, Then_Path, Else_Path, New_Path)).

jaml_asserter('initializer', Initializer, Expr_Path:Expr_Name, Path) :-
   readNextExpression(Initializer / 'expression', Expr_Name, Expr_Pos), 
   append(Path, [Expr_Pos], Expr_Path),
   jaml_asserter('expression', Initializer / 'expression', Path).

jaml_asserter('instance-creation', Instance_Creation, Path) :-
	element_to_position(Instance_Creation, Pos), 
   append(Path, [Pos], Creation_Path),
	Class_Name := Instance_Creation / 'type-ref'@'class-name',
	Package_Name := Instance_Creation / 'type-ref'@'package',
	creation_to_signature(Instance_Creation, Signature),
	readMethodParameters(Instance_Creation, Parameter_Exprs, Creation_Path),
	assert(jsquash_repository('instance-creation', Class_Name, Package_Name, Signature, Parameter_Exprs, Creation_Path)).
	
jaml_asserter('labeled-statement', Labeled_Statement, Path) :-
   jaml_asserter('statement', Labeled_Statement / 'statement', Path).
	
jaml_asserter('left-hand-side', Left_Hand_Side, Path) :-
   jaml_asserter('expression', Left_Hand_Side / 'expression', Path).
	
jaml_asserter('literal-expression', Literal_Expression, Path) :-
	element_to_position(Literal_Expression, Pos), 
   append(Path, [Pos], New_Path),
	Type := Literal_Expression@'type', Value := Literal_Expression@'literal',
	assert(jsquash_repository('literal-expression', Type, Value, New_Path)).
	
jaml_asserter('method-declaration', Method_Declaration, Path) :-
   jaml_asserter('return-type', Method_Declaration / 'return-type', Return_Type), 
   assert(jsquash_repository('return-type', Return_Type, Path)),
   ( Param_Pos := Method_Declaration / 'parameters'@pos, append(Path, [Param_Pos], Param_Path)
   ; Param_Path = [] ),
   jaml_asserter('parameters', Method_Declaration / 'parameters', Param_Path),
   declaration_to_signature(Method_Declaration, Signature),
   Id := Method_Declaration / 'identifier', identifier_to_value(Id, Identifier), 
   assert(jsquash_repository('method-declaration', Identifier, Signature, Param_Path, Path)),
   jaml_asserter('block', Method_Declaration / 'block', Path).

jaml_asserter('method-invocation', Method_Invocation, Path) :-
	element_to_position(Method_Invocation, Pos), append(Path, [Pos], New_Path),
	readQualifier(Method_Invocation, Expr_Path:Expr_Type, Qualifier, New_Path),
	Id := Method_Invocation / 'identifier', identifier_to_value(Id, Identifier),
	readMethodParameters(Method_Invocation, Parameter_Exprs, New_Path),
	update_global_method_id(Global_Method_Id),
	assert(jsquash_repository('method-invocation', Expr_Path:Expr_Type, Qualifier, Identifier, Parameter_Exprs, Global_Method_Id, New_Path)).

readMethodParameter(ParameterDeclaration, ExprName, ExprPath, InvocationPath) :-
	Expr := ParameterDeclaration / 'expression',
	readNextExpression(Expr, ExprName, ExprPos), append(InvocationPath, [ExprPos], ExprPath),
	jaml_asserter('expression', ParameterDeclaration / 'expression', InvocationPath).

readMethodParameters(Parent, ParameterExpressions, Path) :-
	findall( ExprPath:ExprName, (Parameter := Parent / 'parameters' / 'parameter', readMethodParameter(Parameter, ExprName, ExprPath, Path)), ParameterExpressions).

readNextExpression(Expression, Name, Pos) :-
   ( Pos := Expression / 'conditional-expression'@pos, !, Name = 'conditional-expression'
   ; Pos := Expression / 'array-access-expression'@pos, !, Name = 'array-access-expression'
   ; Pos := Expression / 'array-initializer-expression'@pos, !, Name = 'array-declaration'
   ; Pos := Expression / 'parenthesis-expression'@pos, !, Name = 'parenthesis-expression'
   ; Pos := Expression / 'prefix-expression'@pos, !, Name = 'prefix-expression'
   ; Pos := Expression / 'postfix-expression'@pos, !, Name = 'postfix-expression'
   ; Pos := Expression / _ / 'variable-declaration'@pos, !, Name = 'variable-declaration'
   ; Pos := Expression / 'super-method-invocation'@pos, !, Name = 'super-method-invocation'
   ; Pos := Expression / 'method-invocation'@pos, !, Name = 'method-invocation'
   ; Pos := Expression / 'instance-creation'@pos, !, Name = 'instance-creation'
   ; Pos := Expression / 'this-expression'@pos, !, Name = 'this-expression'
   ; Pos := Expression / 'field-access-expression'@pos,!, Name = 'field-access-expression'
   ; Pos := Expression / 'variable-access-expression'@pos, !, Name = 'variable-access-expression'
   ; Pos := Expression / 'assignment-expression'@pos, !, Name = 'assignment-expression'
   ; Pos := Expression / 'literal-expression'@pos, !, Name = 'literal-expression'
   ; Pos := Expression / 'binary-expression'@pos, !, Name = 'binary-expression'
   ; Name = [] ).

jaml_asserter('parameter-declaration', Parameter_Decl, Path) :-
   jaml_asserter('type', Parameter_Decl / 'type', Type),
   Y := Parameter_Decl / 'identifier', Y = _:_:[Identifier],
   assert(jsquash_repository('variable-declaration', Type, Identifier, [], Path)).
	
jaml_asserter('parameters', Parameters, Path) :-
   forall( Parameter_Declaration := Parameters / 'parameter-declaration',
      ( element_to_position(Parameter_Declaration, Pos), append(Path, [Pos], New_Path),
        jaml_asserter('parameter-declaration', Parameter_Declaration, New_Path) ) ).

jaml_asserter('parenthesis-expression', Parenthesis_Expr, Path) :-
	element_to_position(Parenthesis_Expr, Pos),
   append(Path, [Pos], New_Path),
	readNextExpression(Parenthesis_Expr / 'expression', Par_Name, Par_Pos), 
   append(New_Path, [Par_Pos], Par_Path),
	assert(jsquash_repository('parenthesis-expression', Par_Path:Par_Name, New_Path)),
	jaml_asserter('expression', Parenthesis_Expr / 'expression', New_Path).
		
postfix_expression_to_postfix(Postfix_Expression, Postfix) :-
   ( Sign := Postfix_Expression / 'plus-plus'
   ; Sign := Postfix_Expression / 'minus-minus'),
   Sign = _:_:[Postfix].

prefix_expression_to_prefix(Prefix_Expression, Prefix) :-
   ( Sign := Prefix_Expression / 'plus-plus'
   ; Sign := Prefix_Expression / 'minus-minus'
   ; Sign := Prefix_Expression / 'not'
   ; Sign := Prefix_Expression / 'minus' ),
   Sign = _:_:[Prefix], !.

element_to_position(Parent, Position) :- 
   ( Position := Parent@pos
   ; Position = ['undefined'] ).
	
jaml_asserter('postfix-expression', Postfix_Expression, Path) :-   
	element_to_position(Postfix_Expression, Pos), 
   append(Path, [Pos], New_Path),
	jaml_asserter('expression', Postfix_Expression / 'expression', New_Path),
	readNextExpression(Postfix_Expression / 'expression', Expr_Type, Expr_Pos), 
   append(New_Path, [Expr_Pos], Expr_Path),
	postfix_expression_to_postfix(Postfix_Expression, Postfix),
	assert(jsquash_repository('postfix-expression', Postfix, Expr_Path:Expr_Type, New_Path)).
	
jaml_asserter('prefix-expression', Prefix_Expression, Path) :-
	element_to_position(Prefix_Expression, Pos), 
   append(Path, [Pos], New_Path),
	jaml_asserter('expression', Prefix_Expression / 'expression', New_Path),
	readNextExpression(Prefix_Expression / 'expression', Expr_Type, Expr_Pos), 
   append(New_Path, [Expr_Pos], Expr_Path),
	prefix_expression_to_prefix(Prefix_Expression, Prefix),
	assert(jsquash_repository('prefix-expression', Prefix, Expr_Path:Expr_Type, New_Path)).
   
readQualifier(Parent, Object, Qualifier, Path) :-
   ( ( Q := Parent / 'qualifier'
     ; Q := Parent / 'qualifier-expr' ),
     !,
     ( Expression := Q / 'expression',
       readNextExpression(Expression, Expr_Name, Expr_Pos), 
       append(Path, [Expr_Pos], Expr_Path), 
       jaml_asserter('expression', Expression, Path), 
       Qualifier = []
     ; X := Q / 'type' / 'type-ref', Qualifier := X@'fully-qualified-class-name'
     ; X := Q / 'type' / 'type-name' / 'identifier', X = _:_:[Qualifier] )
   ; Qualifier = [] ),
   ( Expr_Path = [], Expr_Name = [], !
   ; true),
   Object = Expr_Path:Expr_Name.

jaml_asserter('return-statement', Return_Statement, Path) :-
	element_to_position(Return_Statement, Pos), 
   append(Path, [Pos], New_Path), 
	readNextExpression(Return_Statement / 'expression', Expr_Type, Expr_Pos), 
   append(New_Path, [Expr_Pos], Expr_Path),
	jaml_asserter('expression', Return_Statement / 'expression', New_Path),
	assert(jsquash_repository('return-expression-temp', Expr_Path:Expr_Type, New_Path)).

jaml_asserter('return-type', Return_Type, Type) :-
	Type := Return_Type / 'type-ref'@'class-name'.

jaml_asserter('right-hand-side', Right_Hand_Side, Path) :-
	jaml_asserter('expression', Right_Hand_Side / 'expression', Path).
   
creation_to_signature(Instance_Creation, Signature) :-
	Params := Instance_Creation / 'parameters',
	findall( Sig, Sig := Params / 'parameter' / 'declared-type-ref' / 'type-ref'@'fully-qualified-class-name', Signature).

declaration_to_signature(Parent, Signature) :-
	Params := Parent / 'parameters',
	findall( Sig, 
     ( Par := Params / 'parameter-declaration',
       Type := Par / 'type-ref'@'fully-qualified-class-name',
       Identifier := Par@'name',
       Array := Par / 'type-ref'@'is-array',
       Sig = [Type, Identifier, Array] ), 
   Signature ).
	
jaml_asserter('statement', Statement, Path) :-
   ( Stmt := Statement / 'labeled-statement', jaml_asserter('labeled-statement', Stmt, Path)
   ; Stmt := Statement / 'continue-statement', jaml_asserter('continue-statement', Stmt, Path)
   ; Stmt := Statement / 'break-statement', jaml_asserter('break-statement', Stmt, Path)
   ; Stmt := Statement / 'do-statement', jaml_asserter('do-statement', Stmt, Path)
   ; Stmt := Statement / 'empty-statement', jaml_asserter('empty-statement', Stmt, Path)
   ; Stmt := Statement / 'throw-statement', jaml_asserter('throw-statement', Stmt, Path)
   ; Stmt := Statement / 'switch-statement', jaml_asserter('switch-statement', Stmt, Path)
   ; Stmt := Statement / 'if-statement', jaml_asserter('if-statement', Stmt, Path)
   ; Stmt := Statement / 'while-statement', jaml_asserter('while-statement', Stmt, Path)
   ; Stmt := Statement / 'block', jaml_asserter('block', Stmt, Path)
   ; Stmt := Statement / 'for-statement', jaml_asserter('for-statement', Stmt, Path)
   ; Stmt := Statement / 'try-statement', jaml_asserter('try-statement', Stmt, Path)
   ; Stmt := Statement / 'expression-statement', jaml_asserter('expression-statement', Stmt, Path)
   ; Stmt := Statement / 'variable-declaration-statement', jaml_asserter('variable-declaration-statement', Stmt, Path)
   ; Stmt := Statement / 'return-statement', jaml_asserter('return-statement', Stmt, Path)
   ; element_to_position(Statement, Pos), write(Pos), write(': '), writeln('*UNKNOWN STATEMENT*') ),
   !.

jaml_asserter('switch-block', SwitchBlock, Condition_Reference, BlockPath) :-
   write_m(switch, ['SCHRITT 1: Sammeln aller Labels']),
   
   findall( Label,
      ( Block := SwitchBlock / 'switch-label' / 'case-statement' / 'expression',
        readNextExpression(Block, ExprName, Pos), 
        append(BlockPath, [Pos], ExprPath), 
        jaml_asserter('expression', Block, BlockPath),
        Label = [ExprPath:ExprName, 'switch-label'] 
      ; Block := SwitchBlock / 'switch-label' / 'default-statement',
        Pos := Block@pos, 
        append(BlockPath, [Pos], ExprPath), 
        Label = [ExprPath:'default-statement', 'switch-label'] ), 
      Labels ),
 
   write_m(switch, ['SCHRITT 2: Sammeln aller Statements']), 
	findall( Switch_Block_Statement,
      ( Block := SwitchBlock / 'statement' / 'block' / 'statement',   
        ( not(_ := Block / 'break-statement'), 
          Pos := Block@pos, 
          append(BlockPath, [Pos], ExprPath), 
          Switch_Block_Statement = [ExprPath:Block, 'statement']
        ; Break := Block / 'break-statement', 
          Pos := Break@pos, 
          append(BlockPath, [Pos], ExprPath), 
          Switch_Block_Statement = [ExprPath:[], 'break-statement'] ) ),
   Switch_Block_Statements ),
   
   append(Labels, Switch_Block_Statements, Statements),
   
   write_m(switch, ['SCHRITT 3: Sortieren der Statements']),
   
   sortSwitchBlockStatements(Statements, SortedStatements), 
   ( foreach(S, SortedStatements) do (write_m(switch, ['   ', S]))), nl_m,
	% # Zu jedem Label werden die Statements einsortiert, die durchgef�hrt werden, wenn die jeweilige Bedingung zutrifft
	% # ==> break wird hierbei schon beachtet
   
   write_m(switch, ['SCHRITT 4: Statements in CASE-Bl�cke aufteilen']),
   
   group_statements_by_labels(SortedStatements, Labels2),

   write_m(switch, ['SCHRITT 5: Nach der Aufteilung gibt es folgende Labels: ']),
	( foreach(L, Labels2) do ( write_m(switch, ['   ', L]))),
   
	Labels2 = [[[BasePath:_, _],_]|_], 
   write_m(switch, ['SCHRITT 6: Schreiben der Labels in die Wissensbasis...']),!,
   assertAllLabels(Labels2, Labels2, BasePath, Condition_Reference).

group_statements_by_labels(SortedStatements, Labels) :-
   findall( [Label, Stmts], 
      ( member(Label, SortedStatements), 
        Label = [_, 'switch-label'],
        get_statements_of_label(SortedStatements, Label, Stmts),
        write_m(switch, ['found statements for label ', Label]),
        ( foreach(S, Stmts) do ( write_m(switch, ['   ', S]))) ),
      Labels ).

get_statements_of_label([], _, []).    
get_statements_of_label([Stmt|Stmts], Label, ResultSet) :-
   ( biggerSwitchBlockStatement(Label, Stmt)
   ; Stmt = [_, 'switch-label'] ),
   !,
   get_statements_of_label(Stmts, Label, ResultSet).
   
get_statements_of_label([Stmt|_], _, ResultSet) :-
   Stmt = [_, 'break-statement'], ResultSet = [], !.
   
get_statements_of_label([Stmt|Stmts], Label, [Stmt|ResultSet]) :-
   get_statements_of_label(Stmts, Label, ResultSet).
      
assertAllLabels([], AllLabels, ElsePath, _) :-
	write_m(switch, ['Nachbearbeitung des DEFAULTS:']),
	member(DefaultLabel, AllLabels), 
   DefaultLabel = [[_:'default-statement', _], DefaultStatements],
	( foreach([_:StmtBlock, _], DefaultStatements) do ( jaml_asserter('statement', StmtBlock, ElsePath))),
	jsquash_repository('if-statement', ConditionPath:ConditionName, ThenPath, 'to_be_updated', IfPath),
	retractall(jsquash_repository('if-statement', ConditionPath:ConditionName, ThenPath, 'to_be_updated', IfPath)),
	write_m(switch, ['IF assert: ', ThenPath, ElsePath, IfPath]),
	assert(jsquash_repository('if-statement', ConditionPath:ConditionName, ThenPath, ElsePath, IfPath)).
	
assertAllLabels([L|Labels], AllLabels, IfPath, CondPath:CondName) :-
   L = [Label, Statements], Label = [LabelPath:LabelName, _], 
   ( LabelName \= 'default-statement',
     write_m(switch, ['assertAllLabels mit normalem Label...']),
     append(IfPath, [1], ConditionPath), 
     append(IfPath, [2], ThenPath), 
     append(IfPath, [3], ElsePath),
     assert(jsquash_repository('binary-expression', CondPath:CondName, '==', LabelPath:LabelName, ConditionPath)),
     write_m(switch, ['next label: ']),
     getNextLabel(AllLabels, AllLabels, Label, NextLabel), nl_m,
     write_m(switch, ['found next label']),
     ( NextLabel \= [[_:'default-statement', _], _],  
       assert(jsquash_repository('if-statement', ConditionPath:'binary-expression', ThenPath, ElsePath, IfPath))
     ; NextLabel =  [[_:'default-statement', _], _],  
       assert(jsquash_repository('if-statement', ConditionPath:'binary-expression', ThenPath, 'to_be_updated', IfPath)) ),
       ( foreach([_:StmtBlock, _], Statements) do ( jaml_asserter('statement', StmtBlock, ThenPath) )),
       assertAllLabels(Labels, AllLabels, ElsePath, CondPath:CondName)
	; LabelName = 'default-statement', 
     write_m(switch, ['assertAllLAbels mit default-label...']),
     assertAllLabels(Labels, AllLabels, IfPath, CondPath:CondName) ).
	
getNextLabel([TempNextLabel|Labels], AllLabels, Label, NextLabel) :- 
   TempNextLabel = [N, _], 
   biggerSwitchBlockStatement(N, Label),
   ( N \= [_:'default-statement', _], write_m(switch, ['a1 ']), NextLabel = TempNextLabel
   ; N = [_:'default-statement', _], write_m(switch, ['a2 ']),
     getNextLabel(Labels, AllLabels, Label, NextLabel) ).
   
getNextLabel([TempNextLabel|Labels], AllLabels, Label, NextLabel) :- 
   write_m(switch,['b1 ']),
   TempNextLabel = [N, _], 
   ( not(biggerSwitchBlockStatement(N, Label)) 
   ; N = [_:'default-statement', _] ), 
   getNextLabel(Labels, AllLabels, Label, NextLabel).
   
getNextLabel([], AllLabels, _, NextLabel) :- 
   write_m(switch, ['c ']),
   member(NextLabel, AllLabels), 
   NextLabel = [N, _], 
   N = [_:'default-statement', _].
	
getNextBreak(SortedStatements, Label, Break) :-
	findall( B, ( member(B, SortedStatements), B = [_, 'break-statement'] ), Bs),
   !, 
	getNextBreakA(Bs, Label, Break).
getNextBreakA([], _, []).
getNextBreakA([B|_], L, B) :- biggerSwitchBlockStatement(B, L), !.
getNextBreakA([X|Bs], L, B) :- getNextBreakA(Bs, L, B).
	
jaml_asserter('switch-statement', Switch_Statement, Path) :- 
	element_to_position(Switch_Statement, Switch_Pos), 
   append(Path, [Switch_Pos], Switch_Stmt_Path),
	append(Switch_Stmt_Path, [1], Switch_Path),
	readNextExpression(Switch_Statement / 'condition' / 'expression', Cond_Name, Cond_Pos), 
   append(Switch_Path, [Cond_Pos], Cond_Path),
	jaml_asserter('expression', Switch_Statement / 'condition' / 'expression', Switch_Path),
	append(Switch_Stmt_Path, [2], Block_Path),
	jaml_asserter('switch-block', Switch_Statement / 'switch-block', Cond_Path:Cond_Name, Block_Path).

jaml_asserter('then-clause', Then_Clause, Path, Then_Path) :- 
	element_to_position(Then_Clause, Pos), 
   append(Path, [Pos], Then_Path),
	jaml_asserter('statement', Then_Clause / 'statement', Then_Path).
	
jaml_asserter('this-expression', _, _).

jaml_asserter('throw-statement', _, _).
	
jaml_asserter('try-statement', Try_Statement, Path) :-
   element_to_position(Try_Statement, Pos), 
   element_to_position(Try_Statement / 'block', TryPos), 
   element_to_position(Try_Statement / 'catch-clause', CatchPos), 
   element_to_position(Try_Statement / 'finally-clause', FinallyPos), 
   append(Path, [Pos], NewPath),
   append(NewPath, [TryPos], TryPath),
   append(NewPath, [CatchPos], CatchPath),
   ( FinallyPos = ['undefined'], FinallyPath = FinallyPos 
   ; append(NewPath, [FinallyPos], FinallyPath) ),
   jaml_asserter('block', Try_Statement / 'block', TryPath),
   jaml_asserter('catch-clause', Try_Statement / 'catch-clause', CatchPath),
   ( jaml_asserter('finally-clause', Try_Statement / 'finally-clause', FinallyPath)
   ; true),
   assert(jsquash_repository('try-statement',TryPath, CatchPath, FinallyPath, NewPath)).

jaml_asserter('type', Type, Type_Value) :-
   ( Type_Element := Type / 'type-name' / 'identifier'
   ; Type_Element := Type / 'primitive-type' / _ ), 
   Type_Element = _:_:[Type_Value].

jaml_asserter('type-definition', Type_Definition, Path) :- 
   jaml_asserter('class-definition', Type_Definition / 'class-definition', Path).
	
jaml_asserter('variable-access-expression', Variable_Access_Expression, Path) :-
   element_to_position(Variable_Access_Expression, Pos), 
   append(Path, [Pos], New_Path),
   readQualifier(Variable_Access_Expression, _, Qualifier,_),
   Id := Variable_Access_Expression / 'identifier', 
   identifier_to_value(Id, Identifier),
   assert(jsquash_repository('variable-access-expression', [], Identifier, Qualifier, New_Path)).

jaml_asserter('variable-declaration-expression', Variable_Decl_Expr, Path) :-
   jaml_asserter('type', Variable_Decl_Expr / 'type', Type),
   jaml_asserter('variable-declaration-list', Variable_Decl_Expr / 'variable-declaration-list', Type, Path).

jaml_asserter('variable-declaration', Variable_Declaration, Type, Path) :-
	element_to_position(Variable_Declaration, Pos), 
   append(Path, [Pos], New_Path),
	X := Variable_Declaration / 'identifier', X = _:_:[Identifier], 
	( jaml_asserter('initializer', Variable_Declaration / 'initializer', Expression, New_Path)
   ; Expression = []:[] ),
	assert(jsquash_repository('variable-declaration', Type, Identifier, Expression, New_Path)).
		
jaml_asserter('variable-declaration-list', Variable_Declaration_List, Type, Path) :-
   forall( Variable_Declaration := Variable_Declaration_List / 'variable-declaration', 
     jaml_asserter('variable-declaration', Variable_Declaration, Type, Path) ).

jaml_asserter('variable-declaration-statement', Variable_Decl_Stmt, Path) :-
   jaml_asserter('type', Variable_Decl_Stmt / 'type', Type),
	jaml_asserter('variable-declaration-list', Variable_Decl_Stmt / 'variable-declaration-list', Type, Path).

jaml_asserter('while-statement', While_Statement, Path) :-
	element_to_position(While_Statement, Pos), 
   append(Path, [Pos], Loop_Path),
	append(Loop_Path, [1], TempCondPath), 
	append(Loop_Path, [2], Block_Path), 
	append(Loop_Path, [5], Access_Path),
   jaml_asserter('condition', While_Statement / 'condition', TempCondPath, CondPath:CondName),
   jaml_asserter('statement', While_Statement / 'statement', Block_Path),
	% # der 4. Parameter ist false, weil dies die Abfrage f�r doWhile ist
	assert(jsquash_repository('while-statement', CondPath:CondName, Loop_Path, 'false')),
	assertBlocksAccesses(Loop_Path, Access_Path).
	
assertBlocksAccesses(Loop_Path, Access_Path) :-
	append(Loop_Path, [2], Block_Path),
	append(Loop_Path, [3], Update_Path),    
   write_m(['Block_Path : ', Block_Path]),
   write_m(['Update_Path: ', Update_Path]),
   write_m(['Access_Path: ', Access_Path]),nl_m,
	findall( [SortPath, Id, AccessList, Qualifier, Scope, LType, Data_Type], 
      ( ( jsquash_repository('assignment-expression', LPath:LType, _, _, SortPath)
        ; jsquash_repository('postfix-expression', _, LPath:LType, SortPath)
        ; jsquash_repository('prefix-expression', _, LPath:LType, SortPath) ),
        ( append(Block_Path, _, SortPath) 
        ; append(Update_Path, _, SortPath) ),
        ( member(LType, ['variable-access-expression', 'field-access-expression']),
          Scope = LType, AccessList = [],
          jsquash_repository(LType, Data_Type, Id, Qualifier, LPath)
        ; LType = 'array-access-expression', 
          jsquash_repository(LType, Data_Type, Id, Scope, AccessList, Qualifier, LPath) )
      ; jsquash_repository('variable-declaration', Data_Type, Id, Qualifier, SortPath), 
        ( append(Block_Path, _, SortPath) 
        ; append(Update_Path, _, SortPath)), 
        LType = 'variable-access-expression', Scope = LType, AccessList = [] ), 
      Accesses ),
   write_m(['Hier sind die Accesses:']),
	( foreach(Access, Accesses) do ( write_m([Access]) )),nl_m,
   write_m(['----------------------------- searching non-dublicate accesses in the block -----------------']),
   init_runtime(RT),
   findall( Access, 
     ( member(Access, Accesses),
       Access = [Path, Id, AccessList, Object, Scope, LType, _],
       ( foreach(Access2, Accesses) do (
            Access2 = [Path2, Id2, AccessList2, Object2, Scope2, LType2, _],
            ( bigger_path(Path, Path2)          % nachfolger werden nicht beachtet
            ; equal_path(Path, Path2)           % identisch wird nicht betrachtet
            ; bigger_path(Path2, Path),         % !!! durchsuche vorg�nger nach identischen... 
             ( Id \= Id2
             ; Id = Id2,
               ( LType \= LType2
               ; ( LType = 'array-access-expression',
                   ( not(check_index_expression_equality(RT, AccessList, AccessList2)), write_m(['INDIZES SIND NICHT GLEICH'])
                   ; ( Scope \= Scope2, write_m(['INDIZES SIND GLEICH, aber SCOPE NICHT'])
                     ; Scope = Scope2, Scope = 'field-access-expression',
                       not(check_object_equality(Path, Object, Object2)) ) )
                 ; LType \= 'array-access-expression',
                   ( Scope \= Scope2
                   ; Scope = Scope2, Scope = 'field-access-expression',
                     not(check_object_equality(Path, Object, Object2)) ) ) ) ) ) ) ) ),
      AssertAccesses ),
   ( foreach(Access, AssertAccesses) do (
      Access = [Path, Id, AccessList, Qualifier, Scope, LType, Data_Type],
      % Fallunterscheidung: ist Path ein Block- [1] oder Update-Path [2] ==> in den Finalpath die entsprechende Zahl f�r die Sortierung einbinden
      ( append(Block_Path, _, Path), Sort_Nr = 1
      ; Sort_Nr = 2 ),
      reverse(Path, RevSortPath), 
      first(RevSortPath, Pos), 
      append(Access_Path, [Sort_Nr, Pos], FinalPath),
      assert(jsquash_repository('assignment-expression', FinalPath:LType, '=', FinalPath:LType, FinalPath)),
      ( member(LType, ['variable-access-expression', 'field-access-expression']),
        assert(jsquash_repository(LType, Data_Type, Id, Qualifier, FinalPath))
      ; LType = 'array-access-expression', 
        assert(jsquash_repository('array-access-expression', Data_Type, Id, Scope, AccessList, Qualifier, FinalPath)))
    ; true ) ), 
    write_m(['----------------------------- ready------------------------------------------------------']).
    
check_object_equality(Sort_Path, Obj_1, Obj_2) :-
   object_to_file_number_and_signature(Sort_Path, Obj_1, File_Number, Signature),
   object_to_file_number_and_signature(Sort_Path, Obj_2, File_Number, Signature).
     
object_to_file_number_and_signature(Sort_Path, Object, File_Number, Signature) :-
   ( Object = Path:'this-expression',
     first(Path, File_Number), Signature = []
   ; Object = [],
     first(Sort_Path, File_Number), Signature = []
   ; object_to_details(Object, File_Number, _, Signature)
   ; not(member(Object, [[], _:_])),
     jsquash_repository('java-class', Package, Class_Name, File_Number),
     concat(Package, '.', X), concat(X, Class_Name, Object),
     Signature = []).