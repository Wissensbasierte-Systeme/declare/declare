
var selectedSqlStatement = -1;

var flagstyle = "plain";
var flagcolors = new Array();
flagcolors = new Array("FF4D4D", "FF6666", "FF8080", "FF9999", "FFB3B3", "FFCCCC", "FFE5E5");

function getIndexOfSearchPath(searchpath) {
	var i = 0;
	for (var i = 0; i < values.length; i++) {
		if (values[i] == searchpath) {
			return i;
		}
	}
}

function deleteAllFlags() {
	var divs = document.getElementsByTagName("div");
	for( var i = 0; i < divs.length; i++) {
		var class = divs[i].className;

		if ((class=='local') || (class=='field') || (class=='literal')) {
			divs[i].style.backgroundColor = "";
		}
	}
}

function flagFileHeads(flagList) {

	// at first reset: classcnt is the number of the project's classes
	for (var i = 0; i < classcnt; i++) {
		document.getElementById("filehead["+i+"]").style.backgroundColor = "EDEDED";
	}	

	var Ausdruck = /^\s*\[\s*(\w*).*]\s*$/;
	var fileid = "";
	
	for (var i = 0; i < flagList.length; i++) {
		id = flagList[i];
		Ausdruck.exec(id);
		fileid = "filehead["+RegExp.$1+"]";
		document.getElementById(fileid).style.backgroundColor = "FF6666";	
	}
}

// setzt die Farben der Variablen (bei der Markierung der History)
function flagVariable(id, colorindex) {
	if (flagstyle == "coloured") {
		if (colorindex > 6) { 
			colorindex = 6; 
		}				
		document.getElementById(id).style.backgroundColor = flagcolors[colorindex];
	} else {
		document.getElementById(id).style.backgroundColor = flagcolors[1];
	}
}

// berechnet die in flag(searchpath) ben�tigte umrechnung von ### --> hochkomma (wg. sql in javascript)
function demaskSearchpath(searchpath) {
	var demaskedSearchpath = searchpath.replace(/###/g, "'");
	return demaskedSearchpath;
}

function flagSelectedStatement(style) {
	flagstyle = style;
	
	if (selectedSqlStatement >= 0) {
		flag(values[selectedSqlStatement], selectedSqlStatement);
	}
}

// setzt die Markierung der Variablen im Quellcode abh�ngig vom selektierten SQL-Statement.
// Au�erdem werden die entsprechenden filehead-divs gef�rbt.
// Die Variable "selectedSqlStatement" h�lt die id des markierten SQL-Statements.
function flag(searchpath, selectedId) {

	// die ID des eben angeklickten Statements wird gesichert
	selectedSqlStatement = selectedId;
	// und das Statement als angeklickt gef�rbt
	// TODO
	
	deleteAllFlags();
	
	// in Prolog werden Hochkommata maskiert: ' ==> ###
	// Grund: in HTML werden javaskript-Aufrufe syntaktisch ung�ltig, wenn das Argument ein SQL-Statement mit Hochkomma ist
	// Bsp:  <div onclick="javascript:flag('select * from tabelle where s = '7' and t = '8'')">
	// Dies muss jetzt wieder demaskiert werden
	var demaskedSearchpath = demaskSearchpath(searchpath);
	
	var index = getIndexOfSearchPath(demaskedSearchpath);
	var colorindex = 0;
	var flagList = new Array();
	var flagListIndex = 0;
	
	for (var i = 0; i < pfade[index].length; i++) {
		for (var j = pfade[index][i].length-1; j >= 0; j--) {
			if (document.getElementById(pfade[index][i][j]) != null) {
				colorindex = pfade[index][i].length-1 - j;
				
				// die Variablen markieren
				flagVariable(pfade[index][i][j], colorindex);
				
				// der filehead der betroffenen Klasse wird in eine Liste aufgenommen, dass
				// zum Schluss auch die Klassen markiert werden k�nnen (flagList)
				flagList[flagListIndex++] = pfade[index][i][j];
			}
		}
	}
	flagFileHeads(flagList);
}

	
	
	
	