/* ------------------------------------------------------------- */
 
check_actuality(RT-Ref, RT3, Possibility) :- 
   ( get_runtime(RT, [ @scope='variable-access-expression' ]),
     update_outer_block(RT-Ref, RT2-Ref),
     check_local_actuality(RT2-Ref, RT3, Possibility)
   ; get_runtime(RT, [ @scope='field-access-expression' ]),
     check_field_actuality(RT-Ref, RT3, Possibility)).
 
check_local_actuality(RT-Ref, RT3, Possibility) :-
   update_inner_block(RT-Ref, RT2-Ref),                              write_m(core_rulename, ['    check_local_actuality for Possibility ', Possibility]),
   check_array_index_equality(RT2, Possibility),
   Methods = [ accept_possibility,
      decide_if_statement, 
      decide_try_statement,
      decide_loop ],
   member(Method, Methods),
   apply(Method, [RT2-Ref, RT3, Possibility]).
   
check_field_actuality(RT-Ref, RT2, Possibility) :-                   write_m(core_rulename, ['    check_field_actuality for Possibility ', Possibility]),
   Ref = Expr_Path:_,
   check_array_index_equality(RT, Possibility),
   ( jsquash_repository('field-access-expression', _, _, Object_1, Expr_Path)
   ; jsquash_repository('array-access-expression', _, _, 'field-access-expression', _, Object_1, Expr_Path)),  
                                                                     write_m(info, ['Das vorangestellte Objekt: ', Object_1]), 
   possibility_to_preobject(Possibility, Object_2),
   check_class_membership(RT-Ref, RT2, Possibility, Object_1, Object_2).   
 
accept_possibility(RT-Ref, RT-Ref, Possibility) :-                   write_m(rulename, ['    check *accept*']),
   Ref = Expr_Path:_,
   get_runtime(RT, [
      inner_block@path=Path_1, inner_block@type=Type_1,
      outer_block@path=Path_2, outer_block@type=Type_2 ]),
   first(Possibility, Init_Path),
   not( ( append(Init_Path, _, Expr_Path)
        ; append(Expr_Path, _, Init_Path)) ),
   ( Path_1 = Path_2, Type_1 = Type_2
   ; append(Path_1, _, Expr_Path)),                                  write_m(finish, ['    ==> accept_solution: (Innerblock == Outerblock)']).
 
possibility_to_value(RT-(Expr_Path:_), Possibility, Value) :-        write_m(rulename, ['possibility_to_value']),
   update_valuehistory_attr(RT-_, RT2-_, Expr_Path, _),
   Possibility = [Path, Type |_],
   set_runtime(RT2, RT3, [current_loop@assert_loop_value=false]),
	eval(RT3-(Path:Type), Temp), 
	( Type = 'postfix-expression' ->
     jsquash_repository('postfix-expression', Sign, _, Path),
	  calculate_postfix_expression(Sign, Temp, Value)
   ; Value = Temp ),                                                 get_runtime(RT, [@searched_id=Id]), Value = [_, Lit, _,_], write_m(searchfinish, ['               ', Id, ' = ', Lit]),
   ( get_runtime(RT, [ current_loop@assert_loop_value=true ]),
     assert('new-loop-value', RT2, Possibility, Value, Expr_Path)
   ; true ),
   ( get_runtime(RT, [@scope='field-access-expression',
        method_invocation@constructor_instance=Instance]),
     Last_Instance is Instance - 1,
     retractall(jsquash_repository('temp-constr-parameter', _, _, Last_Instance))
   ; true ).
   
% # ACHTUNG: DIES MUSS NOCH IN DIE eval-Methode aufgenommen werden (plus der ganze
% # andere Rest, der noch so in possibility_to_value [runtime] rumschwirrt am Ende ...
handle_postfix((Path:Type), Temp, Value) :-
   ( Type = 'postfix-expression' -> 
     jsquash_repository('postfix-expression', Sign, _, Path),
	  calculate_postfix_expression(Sign, Temp, Value)
   ; Value = Temp ).
     
possibility_to_preobject(Possibility, Object) :-                     write_m(fieldrulename, ['possibility_to_preobject: ']), 
   Possibility = [_, _, _, _, Decl_Path],
   ( jsquash_repository('assignment-expression', Field_Path:'field-access-expression' , _, _, Assign_Path),
     append(Assign_Path, _, Decl_Path),
     jsquash_repository('field-access-expression', _, _, Object, Field_Path), 
     !,
                                                                     write_m(fieldfinish, ['==> fall 1: access has preobject'])
   ; jsquash_repository('last-loop-value', _, _, Object, _, Assign_Path), 
     append(Assign_Path, _, Decl_Path),
     !,                                                              write_m(fieldfinish, ['==> fall 2: loop-value has preobject'])
   ; Object = [],                                                    write_m(fieldfinish, ['==> fall 3: access has no preobject'])).

% # Expr = this (FN im Pfad)
% # (case A1) Pos = this (FN im Pfad)    ==> Gleiche FN
% # (case A2) Pos hat Obj (Obj �berpr�fen)     ==> Obj = Klasse der Expr
% # (case A3) Pos statisch (FN der Pre-Klasse)    ==> Gleiche FN

% # Expr hat Obj (Obj �berpr�fen)
% # (case B1) Pos = this (FN im Pfad)    ==> (FN = Obj-Klassen-FN)
% # (case B2) Pos hat Obj (Obj �berpr�fen)     ==> Creation_Path der Objekte gleich
% # (case B3) Pos statisch !!! FEHLER !!!

% # Expr statisch (FN der Pre-Klasse)
% # (case C1) Pos = this (FN im Pfad)    ==> gleiche FN
% # (case C2) Pos hat Obj !!! FEHLER !!!
% # (case C3) Pos statisch (FN im Pfad)     ==> gleiche FN
check_class_membership(RT, RT2, Possibility, Obj_1, Obj_2) :-        write_m(fieldrulename, ['check_class_membership: ', Obj_1, ' und ', Obj_2]),
   member(Method, [ case_A, case_B, case_C ]),
   apply(Method, [RT, RT2, Possibility, Obj_1, Obj_2]).

case_A(RT-Ref, RT2, Possibility, Obj_1, Obj_2) :-                    write_m(fieldrulename, ['*** checking case A']),
   member(Obj_1, [[], _:'this-expression']),
   Ref = (Expr_Path:_), 
   first(Expr_Path, File_Number),
   member(Method, [ case_A1, case_A2, case_A3 ]),
   apply(Method, [RT-Ref, RT2, Possibility, Obj_2, File_Number]).
   
case_A1(RT, RT2, Possibility, Obj, File_Number) :-                   write_m(fieldrulename, ['*** checking case A1']),
   member(Obj, [[], _:'this-expression']),                      
   first(Possibility, Variable_Path),                  
   first(Variable_Path, File_Number), 
   decide_final_location(RT, RT2, Possibility, []),                  write_m(fieldfinish, ['--- checked: case A1']).

case_A2(RT, RT2, Possibility, Obj, File_Number) :-                   write_m(fieldrulename, ['*** checking case A2']),
   object_to_details(Obj, File_Number, _, []),
   decide_final_location(RT, RT2, Possibility, []),                  write_m(fieldfinish, ['--- checked: case A2']).

case_A3(RT, RT2, Possibility, Obj, File_Number) :-                   write_m(fieldrulename, ['*** checking case A3']),
   not(member(Obj, [[], _:_])),
   jsquash_repository('java-class', Package, Class_Name, File_Number),
   concat(Package, '.', X), concat(X, Class_Name, Obj),
   decide_final_location(RT, RT2, Possibility, []),                  write_m(fieldfinish, ['--- checked: case A3']).   
   
case_B(RT, RT2, Possibility, Obj_1, Obj_2) :-                        write_m(fieldrulename, ['*** checking case B']),
   object_to_details(Obj_1, File_Number, Creation_Path, _),
   member(Method, [ case_B1, case_B2, case_B3 ]),
   apply(Method, [RT, RT2, Possibility, Obj_2, File_Number, 
      Creation_Path]),                                               write_m(fieldfinish, ['--- checked: case B']).

case_B1(RT, RT2, Possibility, Obj, File_Number, Creation_Path) :-    write_m(fieldrulename, ['*** checking case B1']),   
   member(Obj, [[], _:'this-expression']), 
   first(Possibility, Variable_Path),
   first(Variable_Path, File_Number),
   decide_possibility_location(RT, RT2, Possibility, File_Number,
      Creation_Path),                                                write_m(fieldfinish, ['--- checked: case B1']).   
     
case_B2(RT, RT2, Possibility, Obj, _, Creation_Path) :-              write_m(fieldrulename, ['*** checking case B2']),
   write_m(['Die Possibility hat das Objekt: ', Obj]),
   object_to_details(Obj, File_Number, Creation_Path, _),
   decide_possibility_location(RT, RT2, Possibility, File_Number,
      Creation_Path),                                                write_m(fieldfinish, ['--- checked: case B2']).
      
case_B3(_, _, _, Obj, _, _) :-                                       write_m(fieldrulename, ['*** checking case B3']),
   not(member(Obj, [[], _:_])), fail.
   
case_C(RT, RT2, Possibility, Obj_1, Obj_2) :-                        write_m(fieldrulename, ['*** checking case C']),
   not(member(Obj_1, [[], _:_])),
   jsquash_repository('java-class', Package, Class_Name, File_Number),
   concat(Package, '.', X), concat(X, Class_Name, Obj_1),
   member(Method, [case_C1, case_C2, case_C3]),
   apply(Method, [RT, RT2, Possibility, Obj_2, File_Number]),        write_m(fieldfinish, ['--- checked: case C']).
   
case_C1(RT-Ref, RT2, Possibility, Obj, File_Number) :-               write_m(fieldrulename, ['*** checking case C1']),
   member(Obj, [[], _:'this-expression']),
   first(Possibility, Init_Path),
   first(Init_Path, File_Number),
   Ref = Expr_Path:_,
   ( first(Expr_Path, File_Number),
     decide_final_location(RT-Ref, RT2, Possibility, [])
   ; not(first(Expr_Path, File_Number)),
     possibility_is_field_declaration(Possibility), 
     RT-Ref = RT2),                                                      write_m(fieldfinish, ['--- checked: case C1']).
     
case_C2(_, _, _, Obj, _) :-                                          write_m(fieldrulename, ['*** checking case C2']),
    Obj = _:_, fail.
   
case_C3(RT, RT2, Possibility, Obj, File_Number) :-                   write_m(fieldrulename, ['*** checking case C3']),
   not(member(Obj, [[], _:_])),
   jsquash_repository('java-class', Package, Class_Name, File_Number),
   concat(Package, '.', X), concat(X, Class_Name, Obj),
   decide_static_location(RT, RT2, Possibility),                     write_m(fieldfinish, ['--- checked: case C1']).
 
decide_static_location(RT-Ref, RT2, Possibility) :-                     write_m(fieldrulename, ['decide_static_location']),
   Ref = Expr_Path:_,
   first(Expr_Path, Expr_File_Number),
   first(Possibility, Init_Path),
   ( first(Init_Path, Expr_File_Number),
     decide_final_location(RT-Ref, RT2, Possibility, [])
   ; not(first(Expr_Path, Expr_File_Number)),
     possibility_is_field_declaration(Possibility), 
     RT-Ref = RT2 ).

decide_final_location(RT-Ref, RT3, Possibility, Creation_Path) :-       write_m(fieldrulename, ['decide_final_location PART II']),
   ( possibility_in_same_method(RT-Ref, Possibility),
     update_outer_block(RT-Ref, RT2-Ref), 
     check_local_actuality(RT2-Ref, RT3, Possibility)
   ; possibility_in_constructor(RT-Ref, RT3, Possibility, Creation_Path)
   ; possibility_is_field_declaration(Possibility), 
     RT-Ref = RT3 ).
     
decide_possibility_location(RT-Ref, RT2, Possibility, File_Number, Creation_Path) :-
   Ref = Expr_Path:_,
   ( first(Expr_Path, File_Number),
     decide_final_location(RT-Ref, RT2, Possibility, [])
   ; not(first(Expr_Path, File_Number)),
     decide_final_location(RT-Ref, RT2, Possibility, Creation_Path) ).   
        
object_to_details(Object, File_Number, Creation_Path, Signature) :-
   init_runtime(RT),
   eval(RT-Object, Object_Class),
   Object_Class = [_, _, Creation_Path, _],
   jsquash_repository('instance-creation', Class_Name, Package, Signature, _, Creation_Path),
   jsquash_repository('java-class', Package, Class_Name, File_Number). 
   
possibility_in_constructor(RT-Ref, RT2, Possibility, Creation_Path) :- write_m(fieldrulename, ['possibility_in_constructor']),
   first(Possibility, Init_Path),
   ( Creation_Path \= [], 
     jsquash_repository('instance-creation', Class_Name, _, Signature, Expressions, Creation_Path)
   ; Creation_Path = [],
     get_runtime(RT, [method_invocation@constr_call_path=Creation_Path_Final]),
     ( Creation_Path_Final \= [],
       jsquash_repository('instance-creation', Class_Name, _, Signature, Expressions, Creation_Path_Final)
     ; Creation_Path_Final = [], Signature = [] )),
   jsquash_repository('constructor-declaration', Class_Name, Tmp_Sig, Parameter_Path, Path),
   append(Path, _, Init_Path),                                       write_m(fieldinfo, ['checking signatures equality: called constr: ', Signature]),
                                                                     write_m(fieldinfo, ['                              actual viewed constr: ', Tmp_Sig]),
   findall(Type, member([Type,_,_], Tmp_Sig), Signature),            write_m(fieldinfo, ['Signature ok: ', Signature]),
   assert_temp_constructor_params(RT-Ref, RT2, Parameter_Path, Expressions).

assert_temp_constructor_params(RT-Ref, RT2-Ref, Parameter_Path, Expressions) :- write_m(fieldrulename, ['assert_constructor_params']),
   ( Parameter_Path \= [],
     get_runtime(RT, [ method_invocation@constructor_instance=Instance]), 
     Next_Instance is Instance + 1,
     set_runtime(RT, RT2, [method_invocation@constructor_instance=Next_Instance]), write_m(info, ['parameterpath: ', Parameter_Path]),
     findall(Parameter, (
        jsquash_repository('variable-declaration', Type, Id, [], Decl_Path),
        append(Parameter_Path, _, Decl_Path),
        Parameter = [Type, Id, Decl_Path]),
     Declarations),                                                  write_m(['test', Declarations]),
     maplist(assert_constr_param(Instance), Declarations, Expressions)
   ; Parameter_Path = [], 
     RT = RT2,                                                       write_m(fieldfinish, ['no assertions needed (default constructor used)'])).
 
assert_constr_param(Instance, Declaration, Expression) :-            
   Declaration = [Type, Id, Path],                                   write_m(fieldrulename, ['assert_constr_param: ', Id, ' at ', Path]),
   assert(jsquash_repository('variable-declaration', Type, Id, Path:['temp-constr-parameter', Instance], Path)),
   assert(jsquash_repository('temp-constr-parameter', Expression, Path, Instance)).

retract_constr_param(Instance, Declaration, _) :-
   Declaration = [Type, Id, Path],
   retractall(jsquash_repository('temp-constr-parameter', _, Path, Instance)),
   retractall(jsquash_repository('variable-declaration', Type, Id, Path:['temp-constr-parameter',Instance], Path)).
   
possibility_in_same_method(_-(Expr_Path:_), Possibility) :-                       write_m(fieldrulename, ['possibility_in_same_method']),
   first(Possibility, Init_Path),                                    write_m(fieldinfo, ['Die Pfade: Expr_Path  : ', Expr_Path]), write_m(['           Init_Path  : ', Init_Path]),
   jsquash_repository('method-declaration', _, _, _, Method_Path),   write_m(fieldinfo, ['      ---> Method_Path: ', Method_Path]),
   append(Method_Path, _, Expr_Path),                                
   append(Method_Path, _, Init_Path),                                write_m(fieldfinish, ['      ---> possibility_in_same_method !!']).
   
possibility_is_field_declaration(Possibility) :-
   first(Possibility, Variable_Path),
   jsquash_repository('field-declaration', _, _, Variable_Path:_, _).