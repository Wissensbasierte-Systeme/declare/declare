/* ------------------------------------------------------------- */

runtime_to_possibility(RT-Ref, RT3, Possibility) :-
   ( get_runtime(RT, [ @scope='variable-access-expression' ]),
     runtime_to_variable_possibilities(RT-Ref, Possibilities)
   ; get_runtime(RT, [ @scope='field-access-expression' ]),
     runtime_to_field_possibilities(RT-Ref, Possibilities)),
   member(Possibility, Possibilities),
   first(Possibility, Init_Path),
   set_runtime(RT, RT2, [current_block@path=Init_Path]),
   check_actuality(RT2-Ref, RT3, Possibility).

runtime_to_field_possibilities(RT, Possibilities) :-
   search_field_possibilities(RT, Pos_1),                    
   remove_possibilities_in_other_methods(RT, Pos_1, Pos_2),
   remove_wrong_field_postfixes(RT, Pos_2, Pos_3),                   write_m(info, ['Nach der Reinigung:']), forall( member(Expr, Pos_3), write_m(info, [Expr]) ), 
   sort_field_possibilities(RT, Pos_3, Possibilities),               write_m(fposinfo, ['Nach der Sortierung (field):']), forall( member(Expr, Possibilities), write_m(fposinfo, [Expr]) ).
   
search_field_possibilities(RT, Possibilities) :-                     write_m(rulename, ['*** search_field_possibilities']),
   Methods = [ field_declaration_is_other_case,
      field_prefix_and_postfix, field_assignment,
      pos_loop_value ],
   findall( Possibility,
      ( member(Method, Methods),
        apply(Method, [RT, Possibility]) ),
      Possibilities). 
    
field_declaration_is_other_case(RT, Declaration) :-
   runtime_to_field_declaration(RT, Declaration),
   Declaration = [_, Type, _, _, _], 
   not( member(Type, [[], 'search-method-parameter',
     ['temp-method-parameter', _], ['temp-constr-parameter', _]])),  write_m(finish, ['    field_declaration_is_other_case']).
    
runtime_to_field_declaration(RT-_, Declaration) :-
   get_runtime(RT, [ array@index=Index, @searched_id=Id ]),
   jsquash_repository('field-declaration', _, Id, Expression, Decl_Path),
   ( Expression = Path:Type
   ; Expression = [], Path = [], Type = [] ),
   Declaration = [Path, Type, Id, Index, Decl_Path].
 
field_assignment(RT-_, Assignment) :-
   get_runtime(RT, [ @searched_id=Id,
      method_invocation@reverse_instance=Reverse_Instance,
      array@index=Array_List_1]),
   jsquash_repository('assignment-expression', LPath:_, _, Path:Type, _),
   ( jsquash_repository('array-access-expression', _, Id, 'field-access-expression', Index_1, _, LPath),
     Access_List_Final = [Index_1, Reverse_Instance]
   ; jsquash_repository('field-access-expression', _, Id, _, LPath),
     ( not(jsquash_repository('array-access-expression', _, _, _, _, _,  Path)),
       Access_List_Final = Array_List_1
     ; jsquash_repository('array-access-expression', _, _, _, Index_2, _, Path), 
       Access_List_Final = [Index_2, Reverse_Instance] )),
   Assignment = [Path, Type, Id, Access_List_Final, LPath],          write_m(finish, ['    field_assignment_leftside_field']).
    
field_prefix_and_postfix(RT-_, Fix) :-
   member(Fix_Type, ['prefix-expression', 'postfix-expression']),
   jsquash_repository(Fix_Type, _, Path:_, Fix_Path),                write_m(finish, ['*** field_prefix_and_postfix']),
   get_runtime(RT, [ array@index=Access_List, @searched_id=Id,
      method_invocation@reverse_instance=Reverse_Instance ]),
   ( get_runtime(RT, [ @search_mode=variable ]),
     jsquash_repository('field-access-expression', _, Id, _, Path),
     Access_List_Final = Access_List
   ; get_runtime(RT, [ @search_mode=array ]),
     jsquash_repository('array-access-expression', _, Id, 'field-access-expression', Index, _, Path),
     Access_List_Final = [Index, Reverse_Instance] ),
   Fix = [Fix_Path, Fix_Type, Id, Access_List_Final, Path].
   
sort_field_possibilities(_-(Expr_Path:_), Possibilities, Sorted_Possibilities) :-
   first(Expr_Path, File_Number),
   findall(Possibility,( 
      member(Possibility, Possibilities),
      first(Possibility, Possibility_Path),
      first(Possibility_Path, File_Number) ),
      In_Class_Possibilities),
   findall( Possibility,
     ( member(Possibility, Possibilities),
      first(Possibility, Possibility_Path),
      not(first(Possibility_Path, File_Number)) ),
      Other_Possibilities ),
   sort_possibilities(In_Class_Possibilities, In_Class_Sorted),
   sort_possibilities(Other_Possibilities, Other_Sorted),
   append(Other_Sorted, In_Class_Sorted, Reverse_Sorted_Possibilities),
   reverse(Reverse_Sorted_Possibilities, Sorted_Possibilities).
   
remove_possibilities_in_other_methods(_-(Expr_Path:_), Possibilities, Clean_Set) :-
   ( jsquash_repository('method_-declaration', _, _, _, This_Path)
   ; jsquash_repository('constructor-declaration', _, _, _, This_Path)),
   append(This_Path, _, Expr_Path),
   findall(Possibility, 
      ( member(Possibility, Possibilities), 
        first(Possibility, Init_Path),
        ( append(This_Path, _, Init_Path),
          bigger_path(Expr_Path, Init_Path)
        ; forall( jsquash_repository('method-declaration', _, _, _, Other_Path), 
            not(append(Other_Path, _, Init_Path)) ) ) ),
      Clean_Set ).

remove_wrong_field_postfixes(_, [], []).
remove_wrong_field_postfixes(RT-Ref, [Sol|SolsA], [NewSol|SolsB]) :- write_m(rulename, ['remove_wrong_field_postixes']),
   get_runtime(RT, [array@index=Index]),
   Sol = [Path, 'postfix-expression', Id, _, HistoryElement],
   % # case_1: i = i++   ==> i = i
   % # case_2: f[i] = f[i]++     ==> f[i] = f[i]
   % # case_3: s = t++   ==> s = t
   % # case_4: s = f[i]++    ==> s = f[i]
   ( remove_field_postfix_case_1(Path, Id, HistoryElement, Index, NewSol)
   ; remove_field_postfix_case_2(Path, Id, HistoryElement, Index, NewSol)
   ; remove_field_postfix_case_3_and_4(Path, Id, HistoryElement, Index, NewSol) ),
   remove_wrong_field_postfixes(RT-Ref, SolsA, SolsB).

remove_wrong_field_postfixes(RT, [Sol|SolsA], [Sol|SolsB]) :-
   remove_wrong_field_postfixes(RT, SolsA, SolsB).      

remove_field_postfix_case_1(InitPath, Id, HistoryElement, Index, NewSol) :-   write_m(rulename, ['postfix case 1']),
   jsquash_repository('postfix-expression', _, PostVarPath:_, InitPath), 
   jsquash_repository('variable-access-expression', _, Id, _, PostVarPath),
   jsquash_repository('assignment-expression', LPath:_, _, _, AssignPath), 
   jsquash_repository('variable-access-expression', _, Id, _, LPath),
   append(AssignPath, _, InitPath),                                  write_m(finish, ['ENTFERNE das Postfix: i = i++']),
   NewSol = [PostVarPath, 'variable-access-expression', Id, Index, HistoryElement].

remove_field_postfix_case_2(InitPath, Id, HistoryElement, Index, NewSol) :-   write_m(rulename, ['postfix case 2']),
   jsquash_repository('postfix-expression', _, PostVarPath:_, InitPath), 
   jsquash_repository('array-access-expression',_, Id, _,_,_, PostVarPath),
   jsquash_repository('assignment-expression', LPath:_, _, _, AssignPath), 
   jsquash_repository('array-access-expression',_, Id,_,_,_,LPath),
   append(AssignPath, _, InitPath),                                  write_m(finish, ['ENTFERNE das Postfix: f[i] = f[i]++ ']), 
   NewSol = [PostVarPath, 'array-access-expression', Id, Index, HistoryElement].

remove_field_postfix_case_3_and_4(InitPath, Id, HistoryElement, Index, NewSol) :-write_m(rulename, ['postfix case 3']),
   jsquash_repository('postfix-expression', _, PostVarPath:_, InitPath), 
   ( jsquash_repository('variable-access-expression', _, Id2, _, PostVarPath), 
     Id \= Id2,                                                      write_m(finish, ['ENTFERNE das Postfix: s = t++']),
     NewSol = [PostVarPath, 'variable-access-expression', Id, Index, HistoryElement] 
   ; jsquash_repository('array-access-expression',_, Id2, _,_, _,PostVarPath), 
     Id \= Id2,                                                      write_m(finish, ['ENTFERNE das Postfix: s = f[i]++']),
     NewSol = [PostVarPath, 'array-access-expression', Id, Index, HistoryElement]).