

start :-

%  load and consult jsquash files
   read_file_paths(
      JSquash_Path, Java_Classes_Path, XML_Files_Path),
   read_jsquash_files(JSquash_Files),
   consult_program_files(JSquash_Path, JSquash_Files),
   
%  create the jsquash_repository
   read_java_files(Java_Files),  
   create_fn_objects(
      Java_Classes_Path, XML_Files_Path, Java_Files, FN_Objects),   
   create_repository(FN_Objects),
   
%  do analysis
   run_jsquash_analysis,
   
%  save repository
   save_repository('jsquash_repository'),
   save_repository('sql_statements'),  
   
%  create html result page
   write_history_for_js,
   write_html_result_page(FN_Objects).


read_jsquash_files(JSquash_Files) :-
   JSquash_Files = [
      'jsquash_logging.pl',
      'save_repository.pl',
      'jaml_asserter_helper.pl',
      'jaml_asserter.pl',
      'html_generator_helper.pl',
      'html_generator.pl',
      'java_resource.pl',
      'jsquash_analysis.pl',
      'runtime_attributes.pl',
      'eval.pl',
      'runtime_to_variable_possibilities.pl',
      'runtime_to_field_possibilities.pl',
      'check_actuality.pl',
      'statements.pl',
      'loops.pl',
      'method_return_value.pl',
      'method_search_parameter.pl',
      'operations.pl',
      'arrays.pl',
      'stuff.pl',
      'code_questions.pl' ].
   

read_file_paths(JSquash_Path, Java_Classes_Path, XML_Files_Path) :-
   JSquash_Path =
      'h:/studium/Hauptstudium/Diplomarbeit/programming/prolog/',
   Java_Classes_Path =
      'h:/studium/Programmierung/Java/workspace_2007/JamlProject/src/',
   XML_Files_Path =
      'h:/studium/Hauptstudium/Diplomarbeit/programming/xml_dateien/'.


consult_program_files(Path, [File|Files]) :- 
   concat(Path, File, ConsultPath),
   [ConsultPath], 
   consult_program_files(Path, Files).
consult_program_files(_, []) :-
   nl.

:- start.


