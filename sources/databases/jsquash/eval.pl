/* ------------------------------------------------------------- */

eval(_-(P:T), _) :-                                                  write_m(core_rulename, ['eval: ', T, ' at ', P]), fail.
    
eval(RT-Ref, Value) :-
   Ref = Path:'literal-expression',
   update_methodinvocation_attr(RT-Ref, RT2-Ref, Rev_Inst), 
   update_valuehistory_attr(RT2-_, _, Path, Value_History),
   jsquash_repository('literal-expression', Type, Lit, Path), 
   remove_quotation_marks(Lit, Lit2),
   Value = [Type, Lit2, Rev_Inst, [[Lit2, Path, Value_History]]],    write_m(finish, ['Wert des Literals: ', Lit]).
    
eval(RT-(Path:'loop-value'), Value) :-
   get_runtime(RT, [@searched_id=Id ]),
   jsquash_repository('last-loop-value', _, Value, _, _, Path),
   Value = [_, Lit, _, _],                                           write_m(finish, ['Wert des Loop-Values f�r ', Id, ': ', Lit]).
   
eval(RT-(Expr_Path:'parenthesis-expression'), Value) :-
   jsquash_repository('parenthesis-expression', Ref, Expr_Path),
   eval(RT-Ref, Value).

eval(_-(Path:'instance-creation'), Value) :-
   jsquash_repository('instance-creation', Class, Package, _, _, Path),
   Value = [Class, Package, Path, []].
  
eval(RT-Ref, Value) :-
   Ref = Path:'conditional-expression',
   jsquash_repository('conditional-expression', Cond_Ref, Then_Ref, Else_Ref, Path),
   eval(RT-Cond_Ref, Condition),
   ( decide_cond_expr(RT-Ref, Condition, true, Then_Ref, Value)
   ; decide_cond_expr(RT-Ref, Condition, false, Else_Ref, Value)).
             
eval(RT-Ref, Value) :-
   Ref = _:'array-declaration',
   get_runtime(RT, [ array@index=Index,
      method_invocation@method_instance=Instance,
      method_invocation@constructor_instance=Instance2]),            write_m(info, ['    gesuchter Index: ', Index]),
   Instance3 is Instance + 1,
   Instance4 is Instance2 + 1,
   Index = [Reference, Index_Reverse_Instance],
   set_runtime(RT, RT2, [
      method_invocation@method_instance=Instance3,
      method_invocation@constructor_instance=Instance4,
      method_invocation@reverse_instance=Index_Reverse_Instance]),
   runtime_to_array_value(RT2-Ref, Reference, Value).
    
eval(RT-(Fix_Path:'postfix-expression'), Value) :-
   jsquash_repository('postfix-expression', _, Ref, Fix_Path),
   eval(RT-Ref, Value).
  
eval(RT-(Fix_Path:'prefix-expression'), Value) :-
   jsquash_repository('prefix-expression', Sign, Ref, Fix_Path),
   eval(RT-Ref, Temp_Value),
   calculate_prefix_expression(Sign, Temp_Value, Value).
   
eval(RT-Ref, Value) :-
   Ref = _:'method-invocation',
   runtime_to_method_return_value(RT-Ref, Value). 

eval(RT-(Expr_Path:['temp-method-parameter', Last_Instance]), Value) :-
   get_runtime(RT, [method_invocation@method_instance=Instance]),
   Last_Instance is Instance - 1,
   jsquash_repository('temp-method-parameter', Ref, Expr_Path, Last_Instance),
   update_valuehistory_attr(RT-_, RT2-_, Expr_Path, _),
   eval(RT2-Ref, Value).

eval(RT-(Expr_Path:['temp-constr-parameter', Last_Instance]), Value) :-
   get_runtime(RT, [method_invocation@constructor_instance=Instance]),
   Last_Instance is Instance - 1,
   jsquash_repository('temp-constr-parameter', Ref, Expr_Path, Last_Instance),
   update_valuehistory_attr(RT-_, RT2-_, Expr_Path, _),
   eval(RT2-Ref, Value).
   
eval(RT-Ref, Value) :-
   Ref = _:'search-method-parameter',
   search_method_parameter(RT-Ref, Value).
   
eval(RT-(Path:'binary-expression'), Value) :-
   jsquash_repository('binary-expression', Left_Ref, Sign, Right_Ref, Path),
   eval(RT-Left_Ref, L_Value),
   eval(RT-Right_Ref, R_Value),                                      nl_m(calcinfo), write_m(calcinfo, ['Es werden folgende Werte verbunden: ']), 
                                                                     write_m(calcinfo, [L_Value]), write_m(calcinfo, [R_Value]),   
   calculate_operation([L_Value, Sign, R_Value], Value),             write_m(calcinfo, ['ERGEBNIS: ', Value]), nl_m(calcinfo).
  
eval(RT-Ref, Value) :-
   Ref = Expr_Path:Expr_Type,
   member(Expr_Type, ['variable-access-expression', 
      'field-access-expression']),
   write_m(searchinfo, ['XXX hier kommt die Runtime']),
   write(RT), nl,
   jsquash_repository(Expr_Type, _, Id, _, Expr_Path),   
   set_runtime(RT, RT2, [ @searched_id=Id, @search_mode=variable, 
      @scope=Expr_Type ]),                                           write_m(searchinfo, ['##### =======> searching non-array: ', Id]), 
   runtime_to_possibility(RT2-Ref, RT3-Ref, Possibility),
   possibility_to_value(RT3-Ref, Possibility, Value), !.

eval(RT-Ref, Value) :-
   Ref = Expr_Path:'array-access-expression',
   get_runtime(RT, [method_invocation@reverse_instance=Reverse_Instance]),
   jsquash_repository('array-access-expression', _, Id, Scope, Index, _, Expr_Path),   
   Access_List = [Index, Reverse_Instance],
   set_runtime(RT, RT2, [ @searched_id=Id, @search_mode=array,
      @scope=Scope, array@index=Access_List ]),                      write_m(searchinfo, ['##### =======> searching array-variable: ', Id]), 
   runtime_to_possibility(RT2-Ref, RT3-Ref, Possibility),
   possibility_to_value(RT3-Ref, Possibility, Value), !.
