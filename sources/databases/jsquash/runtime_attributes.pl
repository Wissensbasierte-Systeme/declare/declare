get_runtime_attributes_pattern(Pattern) :-
   xml_file_to_fn_term('h:/studium/Hauptstudium/Diplomarbeit/programming/prolog/runtime_pattern.xml', [Pattern]).

init_runtime(RT2) :-
   get_runtime_attributes_pattern(RT),
   set_runtime(RT, RT2, [ @search_mode=normal,
      method_invocation@method_instance=0,
      method_invocation@constructor_instance=0,
      method_invocation@reverse_instance=[0],
      method_invocation@rev_inst=[0],
      method_invocation@constr_call_path=[],
      array@index=[],
      value_history@value=[],
      current_loop@path=[],
      current_loop@assert_loop_value=false,
      current_loop@last_loop_run=false,
      current_loop@border_path=[]]).

get_runtime(RT, Selections) :- 
   checklist(get_runtime_(RT), Selections).
get_runtime_(RT, Selector@Attribute=Value) :- 
   Value := RT / Selector@Attribute.
get_runtime_(RT, @Attribute=Value) :- 
   Value := RT@Attribute.   
 
set_runtime(RT, RT, []) :- !.
set_runtime(RT, RT3, [Selector@Attribute=Value|Selections]) :-       
   RT2 := RT *[ / Selector@Attribute:Value],
   set_runtime(RT2, RT3, Selections).
set_runtime(RT, RT3, [@Attribute=Value|Selections]) :-               
   RT2 := RT * [ @Attribute:Value],
   set_runtime(RT2, RT3, Selections).
   
update_outer_block(RT-Ref, RT2-Ref) :-                               write_m(rulename, ['update_outer_block: ']),
   Ref = Expr_Path:_,
   get_runtime(RT, [ current_block@path=Current_Path ]),
	findall( Path:Type, (
      append(Path, _, Expr_Path), 
      append(Path, _, Current_Path), 
		( jsquash_repository('if-statement', _, _, _,Path), 
        Type = 'ifStatement'
		; jsquash_repository('while-statement', _, Path, _), 
        Type = 'whileStatement'
		; jsquash_repository('try-statement',_, _, _, Path), 
        Type = 'tryStatement'
		; jsquash_repository('method-declaration', _, _, _,Path), 
        Type = 'method-declaration'
		; jsquash_repository('constructor-declaration', _, _, _,Path), 
        Type = 'constructorDeclaration'
		; jsquash_repository('class-block',Path), 
        Type = 'classBlock')),
	Blocks),
   sort(Blocks, Sorted_Blocks),
   reverse(Sorted_Blocks, [Path:Type|_]),                            write_m(info, [' ==> Outer_Block = ',Type,' at ', Path]),
   set_runtime(RT, RT2, [ outer_block@path=Path,
      outer_block@type=Type] ).
	
update_inner_block(RT-Ref, RT2-Ref) :-                              write_m(rulename, ['update_inner_block: ']),
   get_runtime(RT, [ outer_block@path=Outer_Block_Path,
      current_block@path=Current_Path ]),
	findall( Path:Type, (
      ( jsquash_repository('if-statement', _, Path, _, _), 
        Type = 'ifThenBlock'
		; jsquash_repository('if-statement', _, _, Path, _), 
        Type = 'ifElseBlock'
		; jsquash_repository('while-statement', _, Path, _), 
        Type = 'whileStatement'
		; jsquash_repository('try-statement',_, _, _, Path), 
        Type = 'tryStatement'
		; jsquash_repository('method-declaration', _, _, _, Path), 
        Type = 'method-declaration'
		; jsquash_repository('constructor-declaration', _, _, _, Path), 
        Type = 'constructorDeclaration'
		; jsquash_repository('class-block', Path), 
        Type = 'classBlock'),
      append(Outer_Block_Path, _, Path), 
      append(Path, A, Current_Path), 
      A \=[]),
	Blocks),
	sort(Blocks, Sorted_Blocks),                                      write_m(info, ['      ==> Current_path= ', Current_Path]),
   reverse(Sorted_Blocks, [Path:Type|_]),                            write_m(info, ['      ==> Inner_Block = ', Path, ' (', Type, ')']),
                                                                     write_m(info, ['      ==> Outer_Block = ', Outer_Block_Path]),
   set_runtime(RT, RT2, [inner_block@path=Path, 
      inner_block@type=Type]).
       
update_methodinvocation_attr(RT-Ref, RT2-Ref, MaxRevInst) :-
   get_runtime(RT, [method_invocation@reverse_instance=ReverseInstance,
      method_invocation@rev_inst=RevInst]),
   ( append(ReverseInstance, _, RevInst), 
     MaxRevInst = RevInst
   ; not(append(ReverseInstance, _, RevInst)), 
     append(RevInst, _, ReverseInstance), 
     MaxRevInst = ReverseInstance),
   set_runtime(RT, RT2, [ method_invocation@rev_inst=MaxRevInst ]).
    
update_valuehistory_attr(RT-Ref, RT2-Ref, Element, Value_History) :-
   get_runtime(RT, [value_history@value=Old_Value_History]),
   append([Element], Old_Value_History, Value_History),
   set_runtime(RT, RT2, [value_history@value=Value_History]).