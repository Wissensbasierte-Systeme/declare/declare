decide_loop(RT-Ref, RT3, Possibility) :-                            write_m(rulename, ['    check *while-statement*']),
   Ref = Expr_Path:_,
   get_runtime(RT, [ inner_block@path=Loop_Path, 
      inner_block@type=IB_Type ]),
   first(Possibility, Init_Path),
   ( append(Loop_Path, _, Init_Path),
     not( ( append(Init_Path, _, Expr_Path)
          ; append(Expr_Path, _, Init_Path) ) ),
     IB_Type = whileStatement,                                         
     jsquash_repository('while-statement', _, Loop_Path, _),
     retractall('all-loop-values', Loop_Path),                       write_m(loopinfo, ['    Checking Loop-Condition']),
     decide_loop_condition(RT-Ref, RT3, Possibility, Loop_Path, 0)
   ; not(append(Loop_Path, _, Init_Path)),
     set_runtime(RT, RT2, [current_loop@last_loop_run=true,
        current_loop@path=Loop_Path, current_block@path=Loop_Path]), write_m(loopinfo, ['    !!! SCHLEIFE beginnt �berhaupt nicht ===> Weitere Betrachtung des Kontrollflusses.']),
     check_local_actuality(RT2-Ref, RT3, Possibility) ).

decide_loop_condition(RT-Ref, RT4, Possibility, Loop_Path, Count) :- write_m(rulename, ['    decide_loop_condition']),
   jsquash_repository('while-statement', Condition, Loop_Path, Do_While),
   ( Do_While = true, Count = 0,
     C_Value = [boolean, true, _, _]
   ; ( Do_While = true, Count \= 0
     ; Do_While = false ),
     Condition = C_Path:C_Type,
     set_runtime(RT, RT2, [current_loop@assert_loop_value=false]),
     eval(RT2-(C_Path:C_Type), C_Value) ),                           write_m(loopinfo, ['    Wert der Loop-Condition: ', C_Value]),
   ( C_Value = [boolean, true |_],
     Next_Count is Count + 1,
     run_loop(RT-Ref, RT4, Possibility, Loop_Path, Next_Count)
   ; C_Value = [boolean, false |_],                                  write_m(loopinfo, ['**** Ausf�hrung letzter Schleifendurchlauf: ', Loop_Path, ' *************']),
     set_runtime(RT, RT3, [current_loop@last_loop_run=true,
        current_loop@path=Loop_Path, current_block@path=Loop_Path]), write_m(loopinfo, ['**** ENDG�LTIGES SCHLEIFENENDE ===> Weitere Betrachtung des Kontrollflusses.']),
     check_local_actuality(RT3-Ref, RT4, Possibility)).
     
run_loop(RT, RT2, Possibility, Loop_Path, Count) :-                  write_m(loopinfo, ['##### run_loop at ', Loop_Path, '################################################']),
   get_access_references(Loop_Path, References),
   update_references_in_loop(RT, Loop_Path, References),             write_m(loopinfo, ['##### ready running loop at ', Loop_Path, ' - copying loop_values']),
   transform_loop_values(Loop_Path),
   decide_loop_condition(RT, RT2, Possibility, Loop_Path, Count).
   
get_access_references(Loop_Path, References) :-                      write_m(rulename, ['get_access_references of loop at ', Loop_Path]),
  append(Loop_Path, [5], Access_Path),
  findall(Path:Type:Id, 
     ( member(Type, ['variable-access-expression', 
          'field-access-expression', 'array-access-expression']),
       ( jsquash_repository(Type, _, Id, _, Path)
       ; jsquash_repository(Type, _, Id, _, _, _, Path) ),
       append(Access_Path, _, Path) ), 
     Unsorted_References ),
  sort(Unsorted_References, Sorted_References),
  reverse(Sorted_References, References),                            ( foreach(Reference, References) do ( write_m([Reference]) )).
  
update_references_in_loop(RT-_, Loop_Path, References) :-            write_m(rulename, ['update_references_in_loop']),
   ( foreach(Ref, References) do (                                   write_m(info, ['#### Reference: ', Ref]),
      Ref = Path:Type:_,
      set_runtime(RT, RT2, [ current_loop@assert_loop_value=true,
         current_loop@path=Loop_Path ]),
      eval(RT2-(Path:Type), _)) ).
      
transform_loop_values(Loop_Path) :-
   retractall('last-loop-value', Loop_Path),
   forall(jsquash_repository('loop-value', A, B, C, Loop_Path, Loop_Value_Path), 
      ( assert(jsquash_repository('last-loop-value', A, B, C, Loop_Path, Loop_Value_Path)),
                                                                     get_runtime(A, [@searched_id=Id]),                              
                                                                     write_m(lastlooprun, ['asserting last-loop-value: ',Id, ' = ', B, ')']))),
   retractall('loop-value', Loop_Path),                              write_m(lastlooprun, ['##### loop finished ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++']).
   
check_last_loop_run(RT-_, Possibility_Path) :-                         
   get_runtime(RT, [current_loop@last_loop_run=Last_Loop_Run,
        current_loop@path=Loop_Path]),                               write_m(lastlooprun, ['    ==> check_last_loop_run: ', Possibility_Path]),
   ( Last_Loop_Run = true,                                           write_n(lastlooprun, ['    last_loop_run = true; ']),
     ( append(Loop_Path, _, Possibility_Path),                       write_m(lastlooprun, ['in Loop; #==> fail']),
       fail
     ; not(append(Loop_Path, _, Possibility_Path)),                  write_m(lastlooprun, ['nicht in Loop #==> ok']))
   ; Last_Loop_Run = false,                                          write_m(lastlooprun, ['    last_loop_run = false #==> ok'])).