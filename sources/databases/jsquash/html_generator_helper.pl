write_html_result_page(Objects) :-
   write('Erstellen der HTML-Anzeige ................................ '),
   tell('h:/studium/Hauptstudium/Diplomarbeit/programming/prolog/html/result.html'),
	write_html_head, !,	
	write_sql_statement_list, !,
	write_legend, !,
	fn_to_html(Objects),
	writeln('</b></body></html>'), told,
   writeln('fertig').

% # traverse all fn-objects and create a html-view for the result-page.
fn_to_html([], _) :- !.	
fn_to_html([O|Objects], FileNumber) :- 
	O = [FNObject, _, _], html_generator('file', FNObject / 'file', [FileNumber]), 
	NextFileNumber is FileNumber + 1, fn_to_html(Objects, NextFileNumber).
fn_to_html(Objects) :- fn_to_html(Objects, 0).


% # HTML - HEAD (with JAVASCRIPT-Container)
write_html_head :-
   writeln('<html>'),
   writeln('<head>'),
   writeln('<link rel="stylesheet" type="text/css" href="css/style2.css">'),
   writeln('<script src="js/functions.js" type="text/javascript"></script>'),
   writeln('<script src="js/history.js" type="text/javascript"></script>'),
   writeln('<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>'),
   writeln('<script type="text/javascript">'),
   writeln('$(document).ready(function() { '),
   writeln('   $("div[@class=filehead]").toggle( '),
   writeln('      function() { '),
   writeln('         var index = this.id.substring(8, this.id.length);'),
   writeln('         document.getElementById("fileheadimg"+index).src="pics/minus.jpg";'),
   writeln('         $(document.getElementById("fileblock"+index)).show("slow");'),
   writeln('      },'),
   writeln('      function() { '),
   writeln('         var index = this.id.substring(8, this.id.length);'),
   writeln('         $(document.getElementById("fileblock"+index)).hide("slow");'),
   writeln('         document.getElementById("fileheadimg"+index).src="pics/plus.jpg";'),
   writeln('      }'),
   writeln('   ); '),
   writeln('}); '),
   writeln('$(document).ready(function() { '),
   writeln('$("div[@class=fileblock]").hide(\'fast\'); '),
   writeln('});'),
   writeln('$(document).ready(function() { '),
   writeln('   $("div[@class=classhead]").toggle( '),
   writeln('      function() { var blockid="classblock"+ this.id.substring(9, this.id.length); $(document.getElementById(blockid)).hide("slow");},'),
   writeln('      function() { var blockid="classblock"+ this.id.substring(9, this.id.length); $(document.getElementById(blockid)).show("slow");'),
   writeln('   }); '),
   writeln('}); '),
   writeln('$(document).ready(function() { '),
   writeln('   $("div[@class=methodhead]").toggle( '),
   writeln('      function() { var blockid="methodblock"+ this.id.substring(10, this.id.length); $(document.getElementById(blockid)).hide("slow");},'),
   writeln('      function() { var blockid="methodblock"+ this.id.substring(10, this.id.length); $(document.getElementById(blockid)).show("slow");'),
   writeln('   }); '),
   writeln('}); '),
   writeln('$(document).ready(function() { '),
   writeln('   $("div[@class=constructorhead]").toggle( '),
   writeln('      function() { var blockid="constructorblock"+ this.id.substring(15, this.id.length); $(document.getElementById(blockid)).hide("slow");},'),
   writeln('      function() { var blockid="constructorblock"+ this.id.substring(15, this.id.length); $(document.getElementById(blockid)).show("slow");'),
   writeln('   }); '),
   writeln('}); '),
   writeln('</script>'),
   writeln('</head>'),
   writeln('<body><b>').

% # statements_list (on the right side of the result page)
% # statements_list shows all statements found during the analysis.   
write_sql_statement_list :-
   writeln('<div class="statementcontainer">'),
   writeln('<h3>Liste vom Quellcode erzeugter SQL-Statements</h3>'),
   writeln('<div class="statementlist" id="statementlist">'),
   writeln('<ul>'),
   findall( Statement, sql_statement(Statement,_), Statements ),
   write_sql_statements(Statements, 0),   
   writeln('</ul><br></div>'),
   writeln('<br>Bitte ein Statement ausw�hlen, um die bei der Berechnung<br>'),
   writeln('beteiligten Variablen im Quellcode zu markieren!</div>').


% # legend (on the right half of the result page)   
write_legend :-
   writeln('<div class="legend">'),
   writeln('<h3>Einstellungen</h3>'),
   writeln('<h4>Markierungsfarbe</h4>'),
   writeln('<input onclick="javascript:flagSelectedStatement(\'plain\');" type="radio" name="flagstyle" id="plain" checked>einfarbig</input>'),
   writeln('<input onclick="javascript:flagSelectedStatement(\'coloured\');" type="radio" name="flagstyle" id="coloured">mehrfarbig (abfallend)</input><br></div>').
 
% # create the file: history.js
% # history.js holds all arrays to be able to show which variables did create a sql-statement
write_history_for_js :-
   write('Sichern der History in history.js f�r HTML-Anzeige ........ '),
   tell('h:/studium/Hauptstudium/Diplomarbeit/programming/prolog/html/js/history.js'),
   writeln('var pfade = new Array();'),
   writeln('var searchpaths = new Array();'),
   writeln('var values = new Array();'),
   findall( [Value, History], sql_statement(Value, History), Statements),
   write_statements(Statements, 0), 
   findall( A, jsquash_repository('java-class', _,A,_), Classes), length(Classes, X),
   write('var classcnt = '), write(X), writeln(';'),
   told,
   writeln('fertig').
      
write_statements([],_) :- !.
write_statements([[Value,History]|Statements], Cnt) :- !,
   write('pfade['), write(Cnt), writeln('] = new Array();'),
   write('values['), write(Cnt), write('] = "'), write(Value), writeln('";'),
   write_history(History, Cnt, 0), NextCnt is Cnt + 1, write_statements(Statements, NextCnt).
   
write_history([],_,_) :- !.
write_history([[_,_,LitHistory]|History], StmtCnt, HistCnt) :- !,
   write('pfade['), write(StmtCnt), write(']['), write(HistCnt), write('] = new Array('),
   write_lithistory(LitHistory), writeln(');'),
   NextHistCnt is HistCnt + 1, write_history(History, StmtCnt, NextHistCnt).

write_lithistory([LitPath|LitHistory]) :- LitHistory \= [], !, write('"'), write(LitPath), write('", '), write_lithistory(LitHistory).
write_lithistory([LitHistory]) :- !, LitHistory \= [], write('"'), write(LitHistory), write('"').
 