/* ------------------------------------------------------------- */

runtime_to_array_value(RT, [], Value) :-                             write_m(rulename, ['runtime_to_array_value: finished ==> evaluating the expression']),
   eval(RT, Value).
   
runtime_to_array_value(RT-(Path:_), [Ref|Refs], Value) :-            write_m(rulename, ['runtime_to_array_value']),
   get_runtime(RT, [method_invocation@rev_inst=Rev_Inst]),
	jsquash_repository('array-declaration', Array_Elements, Path), 
   eval(RT-Ref, [_, Index, Rev_Inst_2, _]),                          write_m(array, ['searched the index: ', Index, ' --> checking compatibility']),
   check_compatibility(Rev_Inst, Rev_Inst_2, Max_Rev_Inst),          write_m(array, ['compatibility checked --> getting next expression from ', Array_Elements]),
   get_expression_at_index(Array_Elements, Index, 0, Expression),    write_m(array, ['got expression: ', Expression]),
   set_runtime(RT, RT2, [method_invocation@rev_inst=Max_Rev_Inst]),
   runtime_to_array_value(RT2-Expression, Refs, Value).

get_expression_at_index([Ref|_], Index, Index, Ref).
get_expression_at_index([_|Array_Elements], Index, Current_Index, Ref) :-   write_m(rulename, ['get_expression_at_index (index = ', Index, ', current_index = ', Current_Index, ')']),
   Current_Index \= Index, 
   Next_Index is Current_Index + 1, 
   get_expression_at_index(Array_Elements, Index, Next_Index, Ref).

% # hier braucht es nur RT, kein RT-(P:T)
check_array_index_equality(RT, Possibility) :-                       write_m(rulename, ['check_array_index_equality-part-I']),
   Possibility = [Init_Path, _, Id, Index, _],                       write_m(info, ['    Index aus Pos: ', Index]),
   get_runtime(RT, [array@index=Index_2]),                           write_m(info, ['    Index aus RT : ', Index_2]),
   not(jsquash_repository('assignment-expression', _:'array-access-expression', _, Init_Path:_, _)),
   not(jsquash_repository('postfix-expression', _, _:'array-access-expression', Init_Path)),
   not(( jsquash_repository('last-loop-value', RT2, _, _, _, Init_Path),
         get_runtime(RT2, [array@index=Index, @searched_id=Id]),
         Index \= [])),
   !,                                                                write_m(finish, ['    ==> NO ARRAY SEARCHED (OK)']).

% # hier braucht es nur RT, kein RT-(P:T)   
check_array_index_equality(RT, Possibility) :-                       write_m(rulename, ['check_array_index_equality-part-II']), 
                                                                     write_m(array, ['test 1']),
   get_runtime(RT, [array@index=Searched_Index ]),                   write_m(array, ['test 2']),
   Possibility = [_, _, _, Possibility_Index, _],                    write_m(array, ['test 3']),
   first(Searched_Index, Expressions_1),                             write_m(array, ['test 4']),
   first(Possibility_Index, Expressions_2),                          write_m(array, ['test 5']),
   check_index_expression_equality(RT, Expressions_1, Expressions_2).

% # hier braucht es nur RT, kein RT-(P:T)     
check_index_expression_equality(_, [], []) :-                        write_m(info, ['    ---- checked index equality']).
check_index_expression_equality(RT, [A|As], [B|Bs]) :-               write_m(rulename, ['    check_index_expression_equality']),
   eval(RT-A, [_, Expression_Value, Rev_Inst_1, _]),
   eval(RT-B, [_, Expression_Value, Rev_Inst_2, _]),
   check_compatibility(Rev_Inst_1, Rev_Inst_2, Max_Rev_Inst),        write_m(finish, ['    compatibility checked: ', Max_Rev_Inst]),
   set_runtime(RT, RT2, [method_invocation@rev_inst=Max_Rev_Inst]),
   check_index_expression_equality(RT2, As, Bs).