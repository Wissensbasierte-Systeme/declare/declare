runtime_to_method_return_value(_-(Invoc_Path:_), Value) :-          write_m(rulename, ['runtime_to_method_return_value: Invoc_Path = ', Invoc_Path]),
   handle_system_methods(Invoc_Path, Value), 
   !.
   
runtime_to_method_return_value(RT, Value) :-   
   get_methods_file_nr(RT, RT2, File_Nr),                            write_m(info, ['found filenumber: ', File_Nr]),
   get_decl_and_param_path(RT2, File_Nr, Decl_Path, Param_Path),     write_m(info, ['found decl and parampath: ', Decl_Path, ', ',Param_Path]),
   instantiate_params(RT2, RT3, Param_Path, Decl_Params),  
   get_return_value(RT2, RT3, Decl_Path, Decl_Params, Value).		

handle_system_methods(Invoc_Path, Value) :-
	jsquash_repository('method-invocation', _, _, Id, _, _, Invoc_Path),  write_m(info, ['called method: ', Id]),
   ( Id = prepareStatement, 
     Value = ['PreparedStatement', 'java.sql', Invoc_Path, []]
	; Id = createStatement, 
     Value = ['Statement', 'java.sql', Invoc_Path, []]
	; % # TODO: korrekte L�nge der Zeichenkette berechnen
     Id = length, 
     Value = [boolean, true, Invoc_Path, []] ).

get_methods_file_nr(RT-Ref, RT2-Ref, File_Nr) :-                     write_m(rulename, ['get_methods_filenumber']),
   Ref = Invoc_Path:_,
   jsquash_repository('method-invocation', Path:Type, Qualifier, 
      _, _, _, Invoc_Path),
   ( Path \= [], 
     eval(RT-(Path:Type), Value), 
     Value = [Class, Package, Instance_Path, _], 
     jsquash_repository('java-class', Package, Class, File_Nr)
   ; Qualifier \= [], Instance_Path = [],
     jsquash_repository('java-class', Package, Class, File_Nr),
     concat(Package, '.', A), 
     concat(A, Class, Qualifier)
   ; Path = [], Qualifier = [], Instance_Path = [],
     first(Invoc_Path, File_Nr) ),
   set_runtime(RT, RT2, [method_invocation@constr_call_path=Instance_Path]).

get_decl_and_param_path(_-(Invoc_Path:_), File_Nr, Decl_Path, Param_Path) :- write_m(rulename, ['get_decl_and_param_path']),
   jsquash_repository('method-invocation', _, _, Id, _, _, Invoc_Path),
   jsquash_repository('method-declaration', Id, _, Param_Path, Decl_Path),
   first(Decl_Path, File_Nr).

instantiate_params(RT-Ref, RT2-Ref, Param_Path, Decl_Params) :-      write_m(rulename, ['instantiate_parameters']),
   Ref = Invoc_Path:_,
   get_runtime(RT, [method_invocation@method_instance=Instance]),
   jsquash_repository('method-invocation', _, _, _, Invoc_Params, _, Invoc_Path),
   findall( Decl_Parameter, 
      ( jsquash_repository('variable-declaration', Type, Id, [], Path),
        append(Param_Path, [Element], Path), 
        not(Element = [_|_]), 
        Decl_Parameter = [Type, Id, Path] ), 
      Decl_Params ),
   Next_Instance is Instance + 1,
   set_runtime(RT, RT2, [method_invocation@method_instance=Next_Instance]), 
   maplist(assert_instantiation(Instance), Decl_Params, Invoc_Params).  
                                                                     
assert_instantiation(Instance, Decl_Parameter, Expr_Ref) :-          write_m(rulename, ['assert_instantiation']),
   Decl_Parameter = [Type, Id, Path], 
   assert(jsquash_repository('variable-declaration', Type, Id, 
      Path:['temp-method-parameter',Instance], Path)),
   assert(jsquash_repository('temp-method-parameter', Expr_Ref, 
      Path, Instance)).
   
retract_instantiation(Instance, Decl_Parameter, _) :-
   Decl_Parameter = [Type, Id, Path],
   retractall(jsquash_repository('temp-method-parameter', _, Path, Instance)),
   retractall(jsquash_repository('variable-declaration', Type, Id, 
      Path:['temp-method-parameter',Instance], Path)).
   
get_return_value(RT-Ref, RT2-Ref, Decl_Path, Decl_Params, Return_Value) :-  write_m(methodrule, ['get_return_value']),
   get_runtime(RT, [method_invocation@method_instance=Instance]),
   % # Nachdem die Methodenparameter instanziiert sind, kann das ReturnStatement geholt werden
   append(Decl_Path, _, Return_Path), 
   jsquash_repository('return_expression', _, Expr_Path:Expr_Type, Return_Path),
   % # finden des Variablenwertes der Returnexpression
   update_valuehistory_attr(RT2-Ref, RT3-Ref, Expr_Path, _),
   set_runtime(RT3, RT4, [@search_mode=normal]),                     write_m(methodcall, ['Suchen des R�ckgabewertes der Methode...']),
   eval(RT4-(Expr_Path:Expr_Type), Return_Value),                    write_m(finish, ['R�ckgabewert: ', Return_Value]),
   maplist(retract_instantiation(Instance), Decl_Params, _).

create_return_expressions :-
	retractall(jsquash_repository('return_expression', _, _, _)),
	findall(_, 
      ( jsquash_repository('return-expression-temp', EP:ET, P), 
        P = [A,B,C,D|_], 
		  jsquash_repository('return-type', T, [A,B,C,D]), 
        assert(jsquash_repository('return_expression', T,EP:ET,P))), 
   _),
	retractall(jsquash_repository('return-expression-temp', _,_)),
	retractall(jsquash_repository('return-type', _,_)).

update_global_method_id(Id) :- 
   jsquash_repository('global-method-id', Id), retractall(jsquash_repository('global-method-id', Id)), 
   adelung_2:to_number(Id, X), Next is X + 1, assert(jsquash_repository('global-method-id', Next)).