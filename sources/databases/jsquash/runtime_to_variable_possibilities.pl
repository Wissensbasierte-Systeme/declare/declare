/* ------------------------------------------------------------- */

runtime_to_variable_possibilities(RT, Possibilities) :-
   search_variable_possibilities(RT, Pos_1),
   sort_variable_possibilities(RT, Pos_1, Pos_2),                    write_m(vposinfo, ['Nach der Sortierung (variable):']), forall( member(Expr, Pos_2), write_m(vposinfo, [Expr]) ),
   remove_wrong_variable_postfixes(RT, Pos_2, Possibilities),        write_m(info, ['Nach der Reinigung:']), forall( member(Expr, Possibilities), write_m(info, [Expr]) ). 

search_variable_possibilities(RT, Possibilities) :-                  write_m(rulename, ['search_variable_possibilities']),
   Methods = [ var_declaration_is_methodparameter,
      var_declaration_is_constructorparameter,      
      var_declaration_is_reverse_methodparameter,
      var_declaration_is_other_case, var_assignment,
      var_prefix_and_postfix, pos_loop_value ],
   findall( Possibility,
      ( member(Method, Methods),
        apply(Method, [RT, Possibility]) ),
      Possibilities ). 
   
runtime_to_var_declaration(RT-_, Declaration) :-           
   get_runtime(RT, [ array@index=Index, @searched_id=Id ]),
   jsquash_repository('variable-declaration', _, Id, Expression, Decl_Path),
   ( Expression = Path:Type
   ; Expression = [], Path = [], Type = [] ),
   Declaration = [Path, Type, Id, Index, Decl_Path].
   
var_declaration_is_methodparameter(RT-Ref, Declaration) :-	            
   get_runtime(RT, [method_invocation@method_instance=Instance]),
   runtime_to_var_declaration(RT-Ref, Declaration), 
   Declaration = [_, Type, _, _, _], 
   Last_Instance is Instance - 1, 
   Type = ['temp-method-parameter', Last_Instance],                  write_m(finish, ['*** var_declaration_is_methodparameter']).

var_declaration_is_constructorparameter(RT-Ref, Declaration) :-          
   get_runtime(RT, [ method_invocation@constructor_instance=Constr_Inst ]),
   runtime_to_var_declaration(RT-Ref, Declaration),
   Declaration = [_, Type, _, _, _], 
   Last_Constr_Inst is Constr_Inst - 1, 
   Type = ['temp-constr-parameter', Last_Constr_Inst],               write_m(finish, ['*** declaration_is_constructorparameter']).
 
var_declaration_is_reverse_methodparameter(RT-Ref, Declaration) :-
   Ref = Expr_Path:_,
   runtime_to_var_declaration(RT-Ref, [_, [], Id, Index, Decl_Path]),
   jsquash_repository('method-declaration', _, _, Param_Path, Method_Path),
   Param_Path \= [], 
   append(Method_Path, _, Expr_Path),
   jsquash_repository('variable-declaration', _, Id, [], Decl_Path),
   not(jsquash_repository('variable-declaration', _, Id, _:['temp-method-parameter', _], Decl_Path)), 
   append(Param_Path, _, Decl_Path),
   Declaration = [Decl_Path, 'search-method-parameter', Id, Index, Decl_Path],  
                                                                     write_m(finish, ['*** var_declaration_is_reverse_methodparameter']).

var_declaration_is_other_case(RT, Declaration) :-
   runtime_to_var_declaration(RT, Declaration),
   Declaration = [_, Type, _, _, _], 
   not( member(Type, [[], ['temp-method-parameter', _],
      ['temp-constr-parameter',_], 'search-method-parameter'])),     write_m(finish, ['*** var_declaration_is_other_case']).
   
var_assignment(RT-Ref, Assignment) :-
   get_runtime(RT, [ @searched_id=Id, array@index=Access_List,
      method_invocation@reverse_instance=Reverse_Instance ]),
   jsquash_repository('assignment-expression', LPath:_, _, Path:Type, _),
   ( jsquash_repository('array-access-expression', _, Id, 'variable-access-expression', Index_1, _, LPath),
     Access_List_Final = [Index_1, Reverse_Instance]
   ; jsquash_repository('variable-access-expression', _, Id, _, LPath),
     ( not(jsquash_repository('array-access-expression',_, _, _, _, _,  Path)),
       Access_List_Final = Access_List
     ; jsquash_repository('array-access-expression',_, _, _, Index_2, _, Path), 
       Access_List_Final = [Index_2, Reverse_Instance] )),
   Assignment = [Path, Type, Id, Access_List_Final, LPath],          write_m(finish, ['*** var_assignment_leftside_varible']),
   check_last_loop_run(RT-Ref, Path),                                write_m(finish, ['  ==> TAKEN']).

var_prefix_and_postfix(RT-Ref, Fix) :-
   member(Fix_Type, ['prefix-expression', 'postfix-expression']),
   jsquash_repository(Fix_Type, _, Path:_, Fix_Path),
   get_runtime(RT, [ array@index=Access_List, @searched_id=Id,
      @search_mode=Search_Mode,
      method_invocation@reverse_instance=Reverse_Instance ]),
   ( Search_Mode = variable,
     jsquash_repository('variable-access-expression', _, Id, _, Path),
     Access_List_Final = Access_List
   ; Search_Mode = array,
     jsquash_repository('array-access-expression', _, Id, 'variable-access-expression', Index, _, Path),
     Access_List_Final = [Index, Reverse_Instance] ),
   Fix = [Fix_Path, Fix_Type, Id, Access_List_Final, Path],          write_m(finish, ['*** var_prefix_and_postfix']),
   check_last_loop_run(RT-Ref, Fix_Path),                            write_m(finish, ['  ==> TAKEN']).
 
pos_loop_value(RT-_, Loop_Value) :-
   get_runtime(RT, [ @searched_id=Id, @scope=Scope ]),
   jsquash_repository('last-loop-value', RT2, _, _, _, Loop_Value_Path),
   get_runtime(RT2, [ @searched_id=Id, @scope=Scope,
      array@index=Access_List ]),
   Loop_Value = [Loop_Value_Path, 'loop-value', Id, Access_List, Loop_Value_Path],
                                                                     write_m(finish, ['*** pos_loop_value']).
 
sort_variable_possibilities(_-(Expr_Path:_), Possibilities, Sorted_Possibilities) :-
   first(Expr_Path, File_Number),
   findall( Possibility,
      ( member(Possibility, Possibilities), 
        first(Possibility, Pos_Path),
        first(Pos_Path, File_Number),
        bigger_path(Expr_Path, Pos_Path) ), 
      Possibilities_2),
   sort_possibilities(Possibilities_2, Possibilities_3),
   reverse(Possibilities_3, Sorted_Possibilities).

sort_possibilities([], []).
sort_possibilities(Xs, [X|Ys]) :-
   max_pos(Xs, X),
   remove_x_from_list(X, Xs, Clean),
   sort_possibilities(Clean, Ys).
      
remove_wrong_variable_postfixes(_, [], []).
remove_wrong_variable_postfixes(RT-Ref, [Sol|SolsA], [NewSol|SolsB]) :- write_m(rulename, ['remove_wrong_variable_postixes']),
   get_runtime(RT, [array@index=Index]),
   Sol = [Path, 'postfix-expression', Id, _, HistoryElement],
   % # case_1: i = i++   ==> i = i
   % # case_2: f[i] = f[i]++     ==> f[i] = f[i]
   % # case_3: s = t++   ==> s = t
   % # case_4: s = f[i]++    ==> s = f[i]
   ( remove_var_postfix_case_1(Path, Id, HistoryElement, Index, NewSol)
   ; remove_var_postfix_case_2(Path, Id, HistoryElement, Index, NewSol)
   ; remove_var_postfix_case_3_and_4(Path, Id, HistoryElement, Index, NewSol) ),
   remove_wrong_varible_postfixes(RT-Ref, SolsA, SolsB).

remove_wrong_variable_postfixes(RT, [Sol|SolsA], [Sol|SolsB]) :-
   remove_wrong_variable_postfixes(RT, SolsA, SolsB).      

remove_var_postfix_case_1(InitPath, Id, HistoryElement, Index, NewSol) :-   write_m(rulename, ['postfix case 1']),
   jsquash_repository('postfix-expression', _, PostVarPath:_, InitPath), 
   jsquash_repository('variable-access-expression', _, Id, _, PostVarPath),
   jsquash_repository('assignment-expression', LPath:_, _, _, AssignPath), 
   jsquash_repository('variable-access-expression', _, Id, _, LPath),
   append(AssignPath, _, InitPath),                                  write_m(finish, ['ENTFERNE das Postfix: i = i++']),
   NewSol = [PostVarPath, 'variable-access-expression', Id, Index, HistoryElement].

remove_var_postfix_case_2(InitPath, Id, HistoryElement, Index, NewSol) :-   write_m(rulename, ['postfix case 2']),
   jsquash_repository('postfix-expression', _, PostVarPath:_, InitPath), 
   jsquash_repository('array-access-expression',_, Id, _,_,_, PostVarPath),
   jsquash_repository('assignment-expression', LPath:_, _, _, AssignPath), 
   jsquash_repository('array-access-expression',_, Id,_,_,_,LPath),
   append(AssignPath, _, InitPath),                                  write_m(finish, ['ENTFERNE das Postfix: f[i] = f[i]++ ']),
   NewSol = [PostVarPath, 'array-access-expression', Id, Index, HistoryElement].

remove_var_postfix_case_3_and_4(InitPath, Id, HistoryElement, Index, NewSol) :-write_m(rulename, ['postfix case 3']),
   jsquash_repository('postfix-expression', _, PostVarPath:_, InitPath), 
   ( jsquash_repository('variable-access-expression', _, Id2, _, PostVarPath), 
     Id \= Id2,                                                      write_m(finish, ['ENTFERNE das Postfix: s = t++']),
     NewSol = [PostVarPath, 'variable-access-expression', Id, Index, HistoryElement] 
   ; jsquash_repository('array-access-expression',_, Id2, _,_, _,PostVarPath), 
     Id \= Id2,                                                      write_m(finish, ['ENTFERNE das Postfix: s = f[i]++']),
     NewSol = [PostVarPath, 'array-access-expression', Id, Index, HistoryElement]).