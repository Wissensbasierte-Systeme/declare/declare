log_level([
   core_rulename, searchinfo, searchfinish
   ,array
   ,switch
   ,rulename
   ,calcinfo
   ,fieldrulename ,fieldfinish ,fieldinfo ,fposinfo
   ,vposinfo
   ,info
   ,loopinfo, ifinfo
   ,lastlooprun
   ,methodrulename, methodcall
   ,finish
    ,test
   ]).

write_m([]).
write_m([_|_]).
% write_m([A|B]) :- write(A), write_m(B).
write_m(A) :- A \= [], A \= [_|_], write(A).
nl_m.

nl_m(A) :-
   ( check_log_level(A), nl
   ; not(check_log_level(A))).
write_m(A, [B|C]) :- 
   check_log_level(A),
   write(B), write_m(A, C).
write_m(A, []) :- 
   check_log_level(A), nl.
write_m(A, _) :-
   not(check_log_level(A)).
check_log_level(A) :-
   log_level(Log_Level),
   member(A, Log_Level).
write_n(A, [B|C]) :-
   check_log_level(A),
   write(B), write_n(A, C).
write_n(_, []).
write_n(A, _) :-
   not(check_log_level(A)).