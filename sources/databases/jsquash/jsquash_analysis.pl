/* ------------------------------------------------------------- */

run_jsquash_analysis :- 
   write('Suchen der SQL-Statements ................................. '),
   tell('h:/studium/Hauptstudium/Diplomarbeit/programming/prolog/files/report.txt'),
   findall( Statement, 
      ( search_sql_statement(Statement) ), 
   Statements ),                                                      nl_m(info), nl_m(info), write_m(info, [Statements]),
   flatten(jaml, Statements, St),
   assert_all(sql_statement, St),
   told, writeln('fertig').
 
search_sql_statement(Statement) :-
   init_runtime(RT),
   runtime_to_sql_expression(RT, Ref),
   eval(RT-Ref, Statement).
   
runtime_to_sql_expression(RT, Expression) :-
   jsquash_repository('method-invocation', Ref, _, Method_Id, 
      _, _, Method_Path),
   ( Method_Id = execute 
   ; Method_Id = executeUpdate 
   ; Method_Id = executeQuery ),
   eval(RT-Ref, Object),
   ( Object = ['PreparedStatement', 'java.sql', Instance_Path|_],
     jsquash_repository('method-invocation', _, _, prepareStatement, 
        [Expression], _, Instance_Path)
   ; Object = ['Statement', 'java.sql', Instance_Path|_],
     jsquash_repository('method-invocation', _, _, Method_Id, 
        [Expression], _, Method_Path) ).
