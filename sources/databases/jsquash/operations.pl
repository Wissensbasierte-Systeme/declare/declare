/* ------------------------------------------------------------- */

% # berechnet das Ergebnis des in der Liste stehenden Ausdruckes. 
calculate_operation(List, Result) :-
   List = [[LType,LVal,RevInst1, LeftInfo], Sign, [RType,RVal,RevInst2, RightInfo]], 
   check_compatibility(RevInst1, RevInst2, MaxRevInst),
   % # Die ValueHistories konkatenieren   
   append(LeftInfo, RightInfo, ResultInfo),
   % # Fallunterscheidung: verschiedene Operationen
   ( operation_is_string(LType, LVal, RType, RVal, Value)
   ; operation_is_arithmetic(LType, LVal, RType, RVal, Sign, Value)
   ; operation_is_bool(LType, LVal, RType, RVal, Sign, Value)),
   append(Value, [MaxRevInst, ResultInfo], Result),
   !,                                                                write_m(finish, [LVal,' ', Sign, ' ', RVal, ' = ', Value]), nl_m(finish).	
   
% # Konkatenation zweier Zeichenketten (mindestens eine davon muss vom Typ java.lang.String sein
operation_is_string( LType, LVal, RType, RVal, ResultValue) :-       write_m(rulename, ['operation_is_string']),
   ( LType = 'java.lang.String' 
   ; RType = 'java.lang.String' ),
   concat(LVal, RVal, ResVal), 
   ResultValue = ['java.lang.String', ResVal].
	
% # Ausf�hren einer arithmetischen Operation	
operation_is_arithmetic(int, LVal, int, RVal, Sign, Value) :-        write_m(rulename, ['operation_is_arithmethic']),
   member(Sign, ['+','-','*','/']), 
	ArithOp =.. [Sign, LVal, RVal], 
	ResVal is ArithOp, 
   Value = [int, ResVal].

% # Ausf�hren einer booleschen Operation
operation_is_bool( LType, LVal, RType, RVal, Sign, Value) :-         write_m(rulename, ['operation_is_bool']),
   ( LType = int, RType = int, 
     member(Sign, ['==', '!=', '<', '>', '<=', '>=']),
     sign_to_functor(Sign, Functor), 
     BoolOp =.. [Functor, LVal, RVal], 
	  ( call(BoolOp), Value = [boolean, true]
     ; Value = [boolean, false] ),
     ! 
   ; LType = boolean, RType = boolean, 
     ( member(Sign, ['==', '!=']),
       sign_to_functor(Sign, Functor), 
       BoolOp =.. [Functor, LVal, RVal], 
	    call(BoolOp), Value = [boolean, true]
     ; not(member(Sign, ['==', '!='])),
       sign_to_functor(Sign, Functor),
       BoolOp =.. [Functor, LVal, RVal, ResVal], 
       call(BoolOp), Value = [boolean, ResVal]
     ; Value = [boolean, false] ),
     ! ).

calculate_prefix_expression(S, Val, Value) :-
   S = ++, calculate_operation([Val, +, [int, 1, [0], []]], Value)
 ; S = --, calculate_operation([Val, -, [int, 1, [0], []]], Value)
 ; S = -, calculate_operation([[int, 0, [0], []], -, Val], Value).
 
calculate_postfix_expression(S, Val, Value) :-
   ( S = ++, Sign = '+'
   ; S = --, Sign = '-'),
   calculate_operation([Val, Sign, [int, 1, [0], []]], Value).
     
logical_or('true', 'true', 'true').
logical_or('true', 'false', 'true').
logical_or('false', 'true', 'true').
logical_or('false', 'false', 'false').

logical_and('true', 'true', 'true').
logical_and('true', 'false', 'false').
logical_and('false', 'false', 'false').
logical_and('false', 'true', 'false').

sign_to_functor('||', logical_or).
sign_to_functor('&&', logical_and).
sign_to_functor('+', '+').
sign_to_functor('-', '-').
sign_to_functor('*', '*').
sign_to_functor('/', '/').
sign_to_functor('==', '=').
sign_to_functor('!=', '\=').
sign_to_functor('<', '<').
sign_to_functor('>', '>').
sign_to_functor('<=', '=<').
sign_to_functor('>=', '>=').