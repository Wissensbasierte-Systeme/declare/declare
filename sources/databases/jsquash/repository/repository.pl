
:- dynamic jsquash_repository/2.

jsquash_repository('global-loop-value-id', 1).
jsquash_repository('class-block', [0, '146']).
jsquash_repository('global-method-id', 5).

:- dynamic jsquash_repository/3.


:- dynamic jsquash_repository/4.

jsquash_repository('literal-expression', int, '5', [0, '146', 3, '146', '191', '202', '206']).
jsquash_repository('postfix-expression', ++, [0, '146', 3, '146', '191', '216', '220', '220', '220', '220']:'variable-access-expression', [0, '146', 3, '146', '191', '216', '220', '220', '220']).
jsquash_repository('postfix-expression', ++, [0, '146', 3, '146', '191', '216', '220', '220', '226', '226']:'variable-access-expression', [0, '146', 3, '146', '191', '216', '220', '220', '226']).
jsquash_repository('postfix-expression', ++, [0, '146', 3, '146', '191', '216', '220', '232', '232']:'variable-access-expression', [0, '146', 3, '146', '191', '216', '220', '232']).
jsquash_repository('literal-expression', 'java.lang.String', '""', [0, '146', 3, '146', '191', '254', '261', '261']).
jsquash_repository('java-class', beispiel5, 'Switch', 0).

:- dynamic jsquash_repository/5.

jsquash_repository('variable-declaration', boolean, aCondition1, [], [0, '146', 3, '146', '170', '170']).
jsquash_repository('method-declaration', sendMessage, [[boolean, aCondition1, false]], [0, '146', 3, '146', '170'], [0, '146', 3, '146']).
jsquash_repository('variable-declaration', int, a, [0, '146', 3, '146', '191', '202', '206']:'literal-expression', [0, '146', 3, '146', '191', '202']).
jsquash_repository('variable-access-expression', [], a, [], [0, '146', 3, '146', '191', '216', '220', '220', '220', '220']).
jsquash_repository('variable-access-expression', [], a, [], [0, '146', 3, '146', '191', '216', '220', '220', '226', '226']).
jsquash_repository('binary-expression', [0, '146', 3, '146', '191', '216', '220', '220', '220']:'postfix-expression', +, [0, '146', 3, '146', '191', '216', '220', '220', '226']:'postfix-expression', [0, '146', 3, '146', '191', '216', '220', '220']).
jsquash_repository('variable-access-expression', [], a, [], [0, '146', 3, '146', '191', '216', '220', '232', '232']).
jsquash_repository('binary-expression', [0, '146', 3, '146', '191', '216', '220', '220']:'binary-expression', +, [0, '146', 3, '146', '191', '216', '220', '232']:'postfix-expression', [0, '146', 3, '146', '191', '216', '220']).
jsquash_repository('variable-declaration', int, b, [0, '146', 3, '146', '191', '216', '220']:'binary-expression', [0, '146', 3, '146', '191', '216']).
jsquash_repository('variable-access-expression', [], b, [], [0, '146', 3, '146', '191', '254', '261', '266']).
jsquash_repository('binary-expression', [0, '146', 3, '146', '191', '254', '261', '261']:'literal-expression', +, [0, '146', 3, '146', '191', '254', '261', '266']:'variable-access-expression', [0, '146', 3, '146', '191', '254', '261']).
jsquash_repository('variable-declaration', 'String', tSQL, [0, '146', 3, '146', '191', '254', '261']:'binary-expression', [0, '146', 3, '146', '191', '254']).
jsquash_repository('variable-declaration', 'Connection', con, [0, '146', 3, '146', '191', '287', '293']:'method-invocation', [0, '146', 3, '146', '191', '287']).
jsquash_repository('variable-access-expression', [], con, [], [0, '146', 3, '146', '191', '321', '325', '325', '349', '354', '354']).
jsquash_repository('variable-access-expression', [], tSQL, [], [0, '146', 3, '146', '191', '321', '325', '325', '349', '354', '375']).
jsquash_repository('variable-declaration', 'PreparedStatement', st, [0, '146', 3, '146', '191', '321', '325', '325', '349', '354']:'method-invocation', [0, '146', 3, '146', '191', '321', '325', '325', '349']).
jsquash_repository('variable-access-expression', [], st, [], [0, '146', 3, '146', '191', '321', '325', '325', '386', '386']).
jsquash_repository('variable-access-expression', [], e, [], [0, '146', 3, '146', '191', '321', '405', '425', '431', '431']).
jsquash_repository('try-statement', [0, '146', 3, '146', '191', '321', '325'], [0, '146', 3, '146', '191', '321', '405'], [undefined], [0, '146', 3, '146', '191', '321']).

:- dynamic jsquash_repository/6.


:- dynamic jsquash_repository/7.

jsquash_repository('method-invocation', []:[], 'startpaket.DBTools', getConnection, [], 1, [0, '146', 3, '146', '191', '287', '293']).
jsquash_repository('method-invocation', [0, '146', 3, '146', '191', '321', '325', '325', '349', '354', '354']:'variable-access-expression', [], prepareStatement, [[0, '146', 3, '146', '191', '321', '325', '325', '349', '354', '375']:'variable-access-expression'], 2, [0, '146', 3, '146', '191', '321', '325', '325', '349', '354']).
jsquash_repository('method-invocation', [0, '146', 3, '146', '191', '321', '325', '325', '386', '386']:'variable-access-expression', [], execute, [], 3, [0, '146', 3, '146', '191', '321', '325', '325', '386']).
jsquash_repository('method-invocation', [0, '146', 3, '146', '191', '321', '405', '425', '431', '431']:'variable-access-expression', [], printStackTrace, [], 4, [0, '146', 3, '146', '191', '321', '405', '425', '431']).
