 
maskA([],[]).
maskA([X|Value], [X|Masked]) :- X \= 39, maskA(Value,Masked).
maskA([X|Value], [35,35,35|Masked]) :- X = 39, maskA(Value,Masked).
mask(Value, Masked) :- name(Value, Value2), maskA(Value2, Value3), name(Masked, Value3).
   
write_sql_statements([], _).
write_sql_statements([Value|Values], Cnt) :-
   NextCnt is Cnt + 1, remove_quotation_marks(Value, Value2),
   write('<li><div class="sqlstatement" id="'), write(Cnt), 
   write('" onclick="javascript:flag(\''), !,
   % # Wichtig: Hochkommata m�ssen maskiert werden
   mask(Value2, Value3), !, write(Value3), 
   write('\', '),write(Cnt),write(');">'),write(Value),writeln('</div></li>'),
   write_sql_statements(Values, NextCnt).
   
html_generator('array-access-expression', Array_Access_Expr, Path) :-
   element_to_position(Array_Access_Expr, Pos), 
   append(Path, [Pos], Access_Path),
   ( Quali := Array_Access_Expr / 'array-qualifier' / 'expression' / 'variable-access-expression', 
     Pos := Quali@'pos', Kind = 'variable-access-expression'
   ; Quali := Array_Access_Expr / 'array-qualifier' / 'expression' / 'field-access-expression', 
     Pos := Quali@'pos', Kind = 'field-access-expression'
   ; Quali := Array_Access_Expr / 'array-qualifier' / 'expression' / _ / 'variable-access-expression', 
     Pos := Quali@'pos', Kind = 'variable-access-expression'
   ; Quali := Array_Access_Expr / 'array-qualifier' / 'expression' / _ / 'field-access-expression', 
     Pos := Quali@'pos', Kind = 'field-access-expression' ), !, 
   Id := Array_Access_Expr / _ / 'identifier', Id = _:_:[Identifier], 
   write('<div class="'), 
   ( Kind = 'variable-access-expression', write('local')
   ; Kind = 'field-access-expression', write('field') ),
   write('" title="'), write(Access_Path), write('" id="'), write(Access_Path), write('">'), write(Identifier), write('</div>'),
   html_generator('array-access-list', Array_Access_Expr, Access_Path).
   
html_generator('array-access-list', Array_Access_Expr, Path) :-
   Next_Expr := Array_Access_Expr / 'array-qualifier' / 'expression',
   ( _ := Next_Expr / 'variable-access-expression'
   ; _ := Next_Expr / 'field-access-expression' ), !,
   Number_Expr := Array_Access_Expr / 'expression',
   write('['), html_generator('expression', Number_Expr, Path), write(']').

html_generator('array-access-list', Array_Access_Expr, Path) :-   
   Next_Expr := Array_Access_Expr / 'array-qualifier' / 'expression',
   Next_Access_Expr := Next_Expr / 'array-access-expression', !,
   Number_Expr := Array_Access_Expr / 'expression',
   readNextExpression(Number_Expr, _, Access_Pos), 
   append(Path, [Access_Pos], Access_Path),
   html_generator('array-access-list', Next_Access_Expr, Access_Path),
   write('['), html_generator('expression', Number_Expr, Path), write(']').

html_generator('array-creation-expression', _, _).

html_generator('array-initializer-expression', Array_Init_Expr, Path) :-
   write('{ '),
   Expression_List := Array_Init_Expr / 'expression-list',
   element_to_position(Array_Init_Expr, List_Pos),
   append(Path, [List_Pos], List_Path),
   forall( ( SE := Expression_List / child::'*', SE = SEN:_:_ ), 
      ( SEN = 'expression', html_generator(SEN, SE, List_Path)
      ; SEN = 'comma', write(',') 
      ; true ) ),
   write(' }').

html_generator('assignment-expression', Assignment_Expr, Path) :-
   element_to_position(Assignment_Expr, Pos), 
   append(Path, [Pos], New_Path),
   Left_Hand_Side := Assignment_Expr / 'left-hand-side', html_generator('left-hand-side', Left_Hand_Side, New_Path),
   write_assignment_sign(Assignment_Expr),
   html_generator('right-hand-side', Assignment_Expr / 'right-hand-side', New_Path).
      
write_assignment_sign(Assignment_Expr) :-
   member(Sign, ['equal', 'plus-equal', 'plus-minus', 'asterisk-equal', 'divide-equal', 'or-equal', 
     'ampersand-equal', 'remainder-equal', 'right-shift-equal', 'left-shift-equal', 'xor-equal']),
   Sign_Element := Assignment_Expr / Sign, Sign_Element = _:_:[Sign_Value],
   write('&nbsp;'), write(Sign_Value), write('&nbsp;').
   
html_generator('binary-expression', Binary_Expr, Path) :-
   element_to_position(Binary_Expr, Pos), 
   append(Path, [Pos], New_Path),
   html_generator('expr-1', Binary_Expr / 'expr-1', New_Path), 
   write_binary_expression_sign(Binary_Expr), 
   html_generator('expr-2', Binary_Expr / 'expr-2', New_Path).
   
write_binary_expression_sign(Binary_Expr) :-
   member(Sign, ['plus', 'minus', 'asterisk', 'divide', 'less-equal', 'greater-equal', 'left-chevron',
     'right-chevron', 'equalequal', 'not-equal', 'remainder', 'ampersand', 'ampersand-ampersand', 'or',
     'or-or', 'xor']),
   Sign_Element := Binary_Expr / Sign, Sign_Element = _:_:[Sign_Value],
   write('&nbsp;'), write(Sign_Value), write('&nbsp;').

html_generator('block', Block, Path) :-
   element_to_position(Block, Pos), 
   append(Path, [Pos], New_Path),
   forall( ( SE := Block / child::'*', SE = SEN:_:_ ), 
      ( SEN = 'left-brace', write('{')
      ; SEN = 'right-brace', write('}')
      ; SEN = 'newline', write('<br>'), nl 
      ; SEN = 'whitespace', Cnt := SE@'length', html_generator('whitespaces', Cnt)
      ; SEN = 'statement', html_generator('statement', SE, New_Path)
      ; true) ).

html_generator('break-statement', _, _).

html_generator('catch-clause', Catch_Clause, Path) :-
   write('&nbsp;<div class="keyword">catch</div>'), write('&nbsp;('),
   Id := Catch_Clause / 'variable-declaration' / 'type' / 'type-name' / 'identifier', Id = _:_:[Type], 
   write(Type), write('&nbsp;'),
   html_generator('variable-declaration', Catch_Clause / 'variable-declaration', Path), write(') '),
   html_generator('block', Catch_Clause / 'block', Path).
   
html_generator('class-block', Class_Block, Path) :-
   element_to_position(Class_Block, Class_Pos), 
   append(Path, [Class_Pos], New_Path),
   forall( ( SE := Class_Block / child::'*', SE = SEN:_:_ ), 
      ( SEN = 'class-block-element', html_generator('class-block-element', SE, New_Path)
      ; SEN = 'left-brace', write('{')
      ; SEN = 'newline', write('<br>'), nl
      ; SEN = 'whitespace', Cnt := SE@'length', html_generator('whitespaces', Cnt)
      ; SEN = 'right-brace', write('}') 
      ; true ) ).
   
html_generator('class-block-element', Class_Block_Element, Path) :-
   element_to_position(Class_Block_Element, Pos),   
   forall( ( SE := Class_Block_Element / child::'*', SE = SEN:_:_ ), 
      ( SEN = 'field-declaration-statement', append(Path, [1, Pos], Field_Path), 
        html_generator(SEN, Class_Block_Element / SEN, Field_Path)
      ; SEN = 'constructor-declaration', append(Path, [2, Pos], Constr_Path),
        html_generator('constructor-declaration', Class_Block_Element / 'constructor-declaration', Constr_Path)
      ; SEN = 'method-declaration', append(Path, [3, Pos], Method_Path),
        html_generator(SEN, SE, Method_Path)
      ; SEN = 'newline', nl
      ; SEN = 'whitespace', Cnt := SE@'length', html_generator('whitespaces', Cnt)
      ; true ) ).

html_generator('class-definition', Class_Definition, Path) :-
   forall( ( SE := Class_Definition / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'modifiers', write('<div class="classhead" id="classhead'), write(Path), write('">'), 
        Modifiers := Class_Definition / 'modifiers', html_generator('modifiers', Modifiers), write('</div>')
      ; SEN = 'whitespace', write('<div class="classhead" id="classhead'),
        write(Path), write('">&nbsp;</div>')
      ; SEN = 'class', write('<div class="classhead" id="classhead'), write(Path),
        write('"><div class="keyword">class</div></div>')
      ; SEN = 'identifier', write('<div class="classhead" id="classhead'), write(Path), write('">'), 
        Identifier := Class_Definition / 'identifier', html_generator('identifier', Identifier), write('</div>') 
      ; SEN = 'super-class', write('<div class="classhead" id="classhead'), write(Path),
        write('">'), html_generator('super-class', Class_Definition / 'super-class'), write('</div>')
      ; SEN = 'class-block', write('<div class="classblock" id="classblock'), write(Path),
        write('">'), html_generator('class-block', Class_Definition / 'class-block', Path), write('</div>')
      ; SEN = 'interfaces', write('<div class="classhead" id="classhead'), write(Path),
        write('">'), html_generator('interfaces', Class_Definition / 'interfaces'), write('</div>') 
      ; true ) ). 
   
html_generator('condition', Condition, Path) :-
   element_to_position(Condition, Pos), 
   append(Path, [Pos], CondPath),
   html_generator('expression', Condition / 'expression', CondPath).
   
html_generator('conditional-expression', Conditional_Expr, Path) :-
   element_to_position(Conditional_Expr, Cond_Pos), 
   append(Path, [Cond_Pos], Cond_Path),
   forall( ( SE := Conditional_Expr / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'condition', html_generator('condition', Conditional_Expr / 'condition', Cond_Path)
      ; SEN = 'whitespace', write('&nbsp;')
      ; SEN = 'newline', write('<br>'), nl
      ; SEN = 'question', write('?')
      ; SEN = 'then-clause', html_generator('expression', Conditional_Expr / 'then-clause' / 'expression', Cond_Path)
      ; SEN = 'colon', write(':')
      ; SEN = 'else-clause', html_generator('expression', Conditional_Expr / 'else-clause' / 'expression', Cond_Path) 
      ; true ) ).

html_generator('constructor-declaration', Constructor_Decl, Path) :-      
   write('<div class="constructortitle" id="constructortitle"'), write(Path), 
   write('"><div class="constructorhead" title="'), write(Path), 
   write('" id="constructorhead'), write(Path), write('">'),
   forall( ( SE := Constructor_Decl / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'modifiers', html_generator(SEN, SE)
      ; SEN = 'whitespace', write(' ')
      ; SEN = 'identifier', html_generator(SEN, SE), write('</div>')
      ; SEN = 'parameters', ( ParamPos := SE@'pos'; ParamPos = 0 ), 
        append(Path, [ParamPos], Param_Path), 
        html_generator(SEN, SE, Param_Path), write('</div>')
      ; SEN = 'block', write('<div class="constructorblock" id="constructorblock'), 
        write(Path), write('">'), html_generator('whitespaces', 1),
        html_generator('block', Constructor_Decl / 'block', Path), write('</div>') 
      ; true ) ).
      
html_generator('continue-statement', _, _).
   
html_generator('do-statement', Do_Statement, Path) :-
   element_to_position(Do_Statement, Pos), 
   append(Path, [Pos], New_Path),
   append(New_Path, [1], Cond_Path), 
   append(New_Path, [2], Block_Path), 
   forall( ( SE := Do_Statement / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'do', write('<div class="keyword">do</div>')
      ; SEN = 'whitespace', write('&nbsp;')
      ; SEN = 'statement', html_generator('statement', SE, Block_Path)
      ; SEN = 'while', write('<div class="keyword">while</div>')
      ; SEN = 'left-parenthesis', write('(')
      ; SEN = 'condition', html_generator('condition', Do_Statement / 'condition', Cond_Path)
      ; SEN = 'right-parenthesis', write(')')
      ; SEN = 'semicolon', write(';') 
      ; true ) ).

html_generator('else-clause', Else_Clause, Path) :-
   element_to_position(Else_Clause, Pos), 
   append(Path, [Pos], Else_Path),
   html_generator('statement', Else_Clause / 'statement', Else_Path).
   
html_generator('empty-statement', _, _).
   
html_generator('expr-1', Expr1, Path) :-
   html_generator('expression', Expr1 / 'expression', Path).

html_generator('expr-2', Expr2, Path) :-
   html_generator('expression', Expr2 / 'expression', Path).
   
html_generator('expression', Expression, Path) :-
   ( Expr := Expression / 'conditional-expression', html_generator('conditional-expression', Expr, Path)
   ; Expr := Expression / 'array-creation-expression', html_generator('array-creation-expression', Expr, Path)
   ; Expr := Expression / 'array-access-expression', html_generator('array-access-expression', Expr, Path)
   ; Expr := Expression / 'array-initializer-expression', html_generator('array-initializer-expression', Expr, Path)
   ; Expr := Expression / 'parenthesis-expression', html_generator('parenthesis-expression', Expr, Path)
   ; Expr := Expression / 'prefix-expression', html_generator('prefix-expression', Expr, Path)
   ; Expr := Expression / 'postfix-expression', html_generator('postfix-expression', Expr, Path)
   ; Expr := Expression / 'variable-declaration-expression', html_generator('variable-declaration-expression', Expr, Path)
   ; Expr := Expression / 'method-invocation', html_generator('method-invocation', Expr, Path)
   ; Expr := Expression / 'instance-creation', html_generator('instance-creation', Expr, Path)
   ; Expr := Expression / 'this-expression', html_generator('this-expression', Expr, Path)
   ; Expr := Expression / 'field-access-expression', html_generator('field-access-expression', Expr, Path)
   ; Expr := Expression / 'variable-access-expression', html_generator('variable-access-expression', Expr, Path)
   ; Expr := Expression / 'assignment-expression', html_generator('assignment-expression', Expr, Path)
   ; Expr := Expression / 'literal-expression', html_generator('literal-expression', Expr, Path)
   ; Expr := Expression / 'binary-expression', html_generator('binary-expression', Expr, Path)
   ; element_to_position(Expression, Pos), write(Pos), write(': '), writeln('*UNKNOWN EXPRESSION*'), writeln(Expression)), !.

html_generator('expression-statement', Expr_Stmt, Path) :-
   html_generator('expression', Expr_Stmt / 'expression', Path), write(';').

html_generator('field-access-expression', Field_Access_Expr, Path) :-
   element_to_position(Field_Access_Expr, Pos), 
   append(Path, [Pos], New_Path),
   ( html_generator('qualifier', Field_Access_Expr, New_Path), write('.')
   ; true ),
   write('<div class="field" title="'), write(New_Path), write('" id="'), write(New_Path), write('">'), 
   Identifier := Field_Access_Expr / 'identifier', html_generator('identifier', Identifier), write('</div>').
   
html_generator('field-declaration-list', Field_Decl_List, Path) :-
   forall( ( SE := Field_Decl_List / child::'*', SE = SEN:_:_ ),
      ( SEN = 'field-declaration',
        element_to_position(SE, Pos), append(Path, [Pos], New_Path),
        write('<div class="field" title="'), write(New_Path), write('" id="'), write(New_Path), write('">'), 
        Identifier := SE / 'identifier', html_generator('identifier', Identifier), write('</div>&nbsp;'),
        ( Init := SE / 'initializer', html_generator('initializer', Init, New_Path) 
        ; true )
      ; SEN = 'comma', write(', ')
      ; SEN = 'whitespace' ) ).

html_generator('field-declaration-statement', Field_Decl_Stmt, Path) :-      
   forall( ( SE := Field_Decl_Stmt / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'modifiers', html_generator(SEN, SE)
      ; SEN = 'type', html_generator(SEN, SE), write('&nbsp;') 
      ; SEN = 'whitespace' 
      ; SEN = 'field-declaration-list', html_generator(SEN, SE, Path) 
      ; SEN = 'semicolon', write(';<br>') 
      ; true) ).
   
html_generator('file', File, Path) :- 
   write('<div class="filehead" id="filehead'), write(Path), writeln('">'),
   write('<img class="fileheadimg" id="fileheadimg'), write(Path), write('" src="pics/plus.jpg">'),
   Class := File / 'type-definition' / 'class-definition'@'fully-qualified-name',!,
   write('<div class="classname" title="'), write(Path), write('">'), write(Class), write('</div></div>'),
   write('<div class="fileblock" id="fileblock'), write(Path), write('">'),
   write('<div class="packagedeclaration" id="packagedeclaration'), write(Path), write('">'),
   Package_Declaration := File / 'package-declaration', html_generator('package-declaration', Package_Declaration),
   write('</div><br>'), nl,   
   ( _ := File / 'import-declaration',
     write('<div class="importdeclaration" id="importdeclaration'), write(Path), write('">'),
     forall( ImportDeclaration := File / 'import-declaration',
        html_generator('import-declaration', ImportDeclaration) ),
     write('</div><br>'), nl
   ; true),
   html_generator('type-definition', File / 'type-definition', Path),
   write('</div><br>').

html_generator('finally-clause', Finally_Clause, Path) :-
   html_generator('block', Finally_Clause / 'block', Path).

html_generator('for-init-statement', For_Init_Stmt, Path) :-
   forall( ( SE := For_Init_Stmt / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'expression', html_generator(SEN, SE, Path)
      ; SEN = 'comma', write(',')
      ; SEN = 'whitespace', write('&nbsp;')
      ; true ) ).

html_generator('for-statement', For_Statement, Path) :-
   element_to_position(For_Statement, Pos), 
   append(Path, [Pos, Pos], Loop_Path),
   append(Path, [Pos, 0], Init_Path),    
   append(Loop_Path, [1], Cond_Path), 
   append(Loop_Path, [2], Block_Path), 
   append(Loop_Path, [3], Update_Path), 
   forall( ( SE := For_Statement / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'for', write('<div class="keyword">for</div>')
      ; SEN = 'whitespace', write('&nbsp;')
      ; SEN = 'left-parenthesis', write('(')
      ; SEN = 'for-init-statement', html_generator(SEN, For_Statement / SEN, Init_Path)
      ; SEN = 'semicolon', write(';')
      ; SEN = 'condition', html_generator('condition', For_Statement / 'condition', Cond_Path)
      ; SEN = 'for-update-statement', html_generator(SEN, For_Statement / SEN, Update_Path)
      ; SEN = 'right-parenthesis', write(')')
      ; SEN = 'statement', html_generator('statement', For_Statement / 'statement', Block_Path)
      ; true ) ).
      
html_generator('for-update-statement', For_Update_Stmt, Path) :-
   forall( ( SE := For_Update_Stmt / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'expression', html_generator(SEN, SE, Path)
      ; SEN = 'comma', write(',')
      ; SEN = 'whitespace', write('&nbsp;')
      ; true) ).

html_generator('identifier', Identifier) :-
   Identifier = _:_:[Value], write(Value).
   
html_generator('if-statement', If_Statement, Path) :-   
   element_to_position(If_Statement, Pos), 
   append(Path, [Pos], New_Path),
   forall( ( SE := If_Statement / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'if', write('<div class="keyword">if</div>')
      ; SEN = 'whitespace', write('&nbsp;')
      ; SEN = 'left-parenthesis', write('(')
      ; SEN = 'condition', html_generator(SEN, SE, New_Path)
      ; SEN = 'right-parenthesis', write(')')
      ; SEN = 'then-clause', html_generator(SEN, SE, New_Path)
      ; SEN = 'else', write('<div class="keyword">else</div>')
      ; SEN = 'else-clause', html_generator(SEN, SE, New_Path)
      ; true)).
        
html_generator('interfaces', Interfaces) :-
   forall( ( SE := Interfaces / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'implements' , write('<div class="keyword">implements</div>')
      ; SEN = 'whitespace', write('&nbsp;')
      ; SEN = 'type', html_generator(SEN, SE)
      ; true ) ).

html_generator('import-declaration', Import_Declaration) :-      
   ( PackageName := Import_Declaration / _ / 'package-name' 
   ; PackageName := Import_Declaration / 'package-name' ),
   write('import '),
   forall( Identifier := PackageName / 'identifier', ( html_generator('identifier', Identifier), write('.') ) ), 
   Package_Name := Import_Declaration / 'type-name' / 'identifier', html_generator('identifier', Package_Name), 
   write(';'), write('<br>'), nl.

html_generator('initializer', Initializer, Path) :-
   forall( ( SE := Initializer / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'equal', write('=')
      ; SEN = 'whitespace', write('&nbsp;') 
      ; SEN = 'expression', html_generator(SEN, SE, Path) 
      ; true ) ).

html_generator('instance-creation', Instance_Creation, Path) :-
   element_to_position(Instance_Creation, Pos), 
   append(Path, [Pos], Creation_Path),
   write('<div class="keyword">new</div>&nbsp;'), html_generator('type', Instance_Creation / 'type'),
   Parameters := Instance_Creation / 'parameters', html_generator('parameters', Parameters, Creation_Path).
   
html_generator('labeled-statement', Labeled_Stmt, Path) :-
   html_generator('statement', Labeled_Stmt / 'statement', Path).
   
html_generator('left-hand-side', Left_Hand_Side, Path) :-
   html_generator('expression', Left_Hand_Side / 'expression', Path).
   
html_generator('literal-expression', Literal_Expr, Path) :-
   element_to_position(Literal_Expr, Pos), 
   append(Path, [Pos], New_Path),   
   Value := Literal_Expr@'literal', 
   write('<div class="literal" title="'), write(New_Path), write('" id="'), write(New_Path), write('">'), 
   write(Value), write('</div>').
  
html_generator('method-declaration', Method_Declaration, Path) :-  
   write('<div class="methodtitle" title="'), write(Path), 
   write('"><div class="methodhead" id="methodhead'), write(Path), write('">'),
   Modifiers := Method_Declaration / 'modifiers', html_generator('modifiers', Modifiers), 
   Return_Type := Method_Declaration / 'return-type', html_generator('return-type', Return_Type), 
   Identifier := Method_Declaration / 'identifier', html_generator('identifier', Identifier), write('</div>'),
   ( Param_Pos := Method_Declaration / 'parameters'@pos, append(Path, [Param_Pos], Param_Path)
   ; Param_Path = [] ),
   Parameters := Method_Declaration / 'parameters', html_generator('parameters', Parameters, Param_Path),
   write('</div>'), write('<div class="methodblock" id="methodblock'), write(Path), write('">'),
   html_generator('whitespaces', 1),
   html_generator('block', Method_Declaration / 'block', Path), write('</div>').

html_generator('method-invocation', Method_Invocation, Path) :-
   element_to_position(Method_Invocation, Pos), 
   append(Path, [Pos], New_Path),
   ( html_generator('qualifier', Method_Invocation, New_Path)
   ; true),
   forall( ( SE := Method_Invocation / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'type-ref' 
      ; SEN = 'method-ref' 
      ; SEN = 'qualifier-expr'  
      ; SEN = 'dot', write('.')
      ; SEN = 'identifier', write( '<div class="methodinvocation" title="'), write(New_Path), write('">'), 
        html_generator(SEN, SE), write('</div>') 
      ; SEN = 'parameters', html_generator(SEN, SE, New_Path)
      ; true ) ).

html_generator('modifiers', Modifiers) :-
   forall(   Modifier := Modifiers / 'modifier', (
      Mod := Modifier / _,
      Mod = _:_:[Value],
      write('<i>'), write(Value), write('</i>'),write('&nbsp;')
   )).

html_generator('next-expression', Expression, Name, Pos) :-
   ( Pos := Expression / 'conditional-expression'@pos, Name = 'conditional-expression'
   ; Pos := Expression / 'array-access-expression'@pos, Name = 'array-access-expression'
   ; Pos := Expression / 'array-initializer-expression'@pos, Name = 'array-declaration'
   ; Pos := Expression / 'parenthesis-expression'@pos, Name = 'parenthesis-expression'
   ; Pos := Expression / 'prefix-expression'@pos, Name = 'prefix-expression'
   ; Pos := Expression / 'postfix-expression'@pos, Name = 'postfix-expression'
   ; Pos := Expression / _ / 'variable-declaration'@pos, Name = 'variable-declaration'
   ; Pos := Expression / 'super-method-invocation'@pos, Name = 'super-method-invocation'
   ; Pos := Expression / 'method-invocation'@pos, Name = 'method-invocation'
   ; Pos := Expression / 'instance-creation'@pos, Name = 'instance-creation'
   ; Pos := Expression / 'this-expression'@pos, Name = 'this-expression'
   ; Pos := Expression / 'field-access-expression'@pos, Name = 'field-access-expression'
   ; Pos := Expression / 'variable-access-expression'@pos, Name = 'variable-access-expression'
   ; Pos := Expression / 'assignment-expression'@pos, Name = 'assignment-expression'
   ; Pos := Expression / 'literal-expression'@pos, Name = 'literal-expression'
   ; Pos := Expression / 'binary-expression'@pos, Name = 'binary-expression'
   ; Name = [] ), !.
   
html_generator('package-declaration', Package_Decl) :-
   write('package '), 
   Identifier := Package_Decl / 'package-name' / 'identifier',
   html_generator('identifier', Identifier),
   writeln(';'), write('<br>'), nl.

html_generator('parameter-declaration', Param_Decl, Path) :-  
   html_generator('type', Param_Decl / 'type'), write('&nbsp;'),
   write('<div class="local" title="'), write(Path), write('" id="'), write(Path), write('">'), 
   Identifier := Param_Decl / 'identifier', html_generator('identifier', Identifier), write('</div>').
   
html_generator('parameters', Parameters, Path) :-
   forall( ( SE := Parameters / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'left-parenthesis', write('(') 
      ; SEN = 'right-parenthesis', write(')') 
      ; SEN = 'comma', write(', ') 
      ; SEN = 'parameter', Expr := SE / 'expression', html_generator('expression', Expr, Path) 
      ; SEN = 'parameter-declaration', element_to_position(SE, Pos), 
        append(Path, [Pos], New_Path), html_generator(SEN, SE, New_Path)
      ; true ) ).

html_generator('parenthesis-expression', Parenthesis_Expr, Path) :-
   element_to_position(Parenthesis_Expr, Pos), 
   append(Path, [Pos], New_Path),
   write('('), html_generator('expression', Parenthesis_Expr / 'expression', New_Path), write(')').
 
html_generator('postfix', Postfix_Expr) :- 
   ( Postfix := Postfix_Expr / 'plus-plus', Postfix = _:_:[Value]
   ; Postfix := Postfix_Expr / 'minus-minus', Postfix = _:_:[Value] ), 
   write(Value).

html_generator('prefix', Prefix_Expr) :-
   ( Prefix := Prefix_Expr / 'plus-plus', Prefix = _:_:[Value]
   ; Prefix := Prefix_Expr / 'minus-minus', Prefix = _:_:[Value]
   ; Prefix := Prefix_Expr / 'not', Prefix = _:_:[Value]
   ; Prefix := Prefix_Expr / 'minus', Prefix = _:_:[Value] ), 
   !, write(Value).

html_generator('postfix-expression', Postfix_Expr, Path) :-
   element_to_position(Postfix_Expr, Pos), 
   append(Path, [Pos], New_Path),
   html_generator('expression', Postfix_Expr / 'expression', New_Path),
   html_generator('postfix', Postfix_Expr).
  
html_generator('prefix-expression', Prefix_Expr, Path) :-  
   element_to_position(Prefix_Expr, Pos), 
   append(Path, [Pos], New_Path),
   html_generator('prefix', Prefix_Expr),
   html_generator('expression', Prefix_Expr / 'expression', New_Path).

html_generator('qualifier', Parent, Path) :-
  ( Q := Parent / 'qualifier'
  ; Q := Parent / 'qualifier-expr'), !,
  ( Expression := Q / 'expression', html_generator('expression', Expression, Path)   
  ; X := Q / 'type' / 'type-name' / 'identifier', X = _:_:[Qualifier], write(Qualifier) ). 

html_generator('return-statement', Return_Stmt, Path) :-
   element_to_position(Return_Stmt, Pos), 
   append(Path, [Pos], New_Path), 
   write('<div class="keyword">return</div>&nbsp;'),
   html_generator('expression', Return_Stmt / 'expression', New_Path), write(';').

html_generator('return-type', Return_Type) :-
   Type := Return_Type / 'type-ref'@'class-name', 
   write('<div class="keyword">'), write(Type), write('</div>'),write('&nbsp;').

html_generator('right-hand-side', Right_Hand_Side, Path) :-
   html_generator('expression', Right_Hand_Side / 'expression', Path).
   
html_generator('statement', Statement, Path) :-
   ( Stmt := Statement / 'labeled-statement', html_generator('labeled-statement', Stmt, Path)
   ; Stmt := Statement / 'continue-statement', html_generator('continue-statement', Stmt, Path)
   ; Stmt := Statement / 'break-statement', html_generator('break-statement', Stmt, Path)
   ; Stmt := Statement / 'do-statement', html_generator('do-statement', Stmt, Path)
   ; Stmt := Statement / 'empty-statement', html_generator('empty-statement', Stmt, Path)
   ; Stmt := Statement / 'throw-statement', html_generator('throw-statement', Stmt, Path)
   ; Stmt := Statement / 'switch-statement', html_generator('switch-statement', Stmt, Path)
   ; Stmt := Statement / 'if-statement', html_generator('if-statement', Stmt, Path)
   ; Stmt := Statement / 'while-statement', html_generator('while-statement', Stmt, Path)
   ; Stmt := Statement / 'block', html_generator('block', Stmt, Path)
   ; Stmt := Statement / 'for-statement', html_generator('for-statement', Stmt, Path)
   ; Stmt := Statement / 'try-statement', html_generator('try-statement', Stmt, Path)
   ; Stmt := Statement / 'expression-statement', html_generator('expression-statement', Stmt, Path)
   ; Stmt := Statement / 'variable-declaration-statement', html_generator('variable-declaration-statement', Stmt, Path)
   ; Stmt := Statement / 'return-statement', html_generator('return-statement', Stmt, Path)
   ; element_to_position(Statement, Pos), write(Pos), write(': '), writeln('*UNKNOWN STATEMENT FOUND WRITING STATEMENT *') ), !.   

html_generator('super-class', Super_Class) :-
   forall( ( SE := Super_Class / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'extends', write('<div class="keyword">'), write('extends'), write('</div>')
      ; SEN = 'whitespace', write('&nbsp;')
      ; SEN = 'type', html_generator(SEN, SE)
      ; true )).

html_generator('then-clause', Then_Clause, Path) :-
   element_to_position(Then_Clause, Pos), 
   append(Path, [Pos], Then_Path),
   html_generator('statement', Then_Clause / 'statement', Then_Path).
   
html_generator('this-expression', _, _) :-
   write('<div class="keyword">'), write('this'), write('</div>').

html_generator('throw-statement', _, _).

html_generator('try-statement', Try_Stmt, Path) :- 
   element_to_position(Try_Stmt, Pos), 
   element_to_position(Try_Stmt / 'block', Try_Pos), 
   element_to_position(Try_Stmt / 'catch-clause', Catch_Pos), 
   element_to_position(Try_Stmt / 'finally-clause', Finally_Pos), 
   append(Path, [Pos], New_Path),
   append(New_Path, [Try_Pos], Try_Path),
   append(New_Path, [Catch_Pos], Catch_Path),
   ( Finally_Pos = ['undefined'], Finally_Path = Finally_Pos 
   ; append(New_Path, [Finally_Pos], Finally_Path) ),
   write('<div class="keyword">try</div>&nbsp;'), 
   html_generator('block', Try_Stmt / 'block', Try_Path),
   html_generator('catch-clause', Try_Stmt / 'catch-clause', Catch_Path),
   ( html_generator('finally-clause', Try_Stmt / 'finally-clause', Finally_Path) 
   ; true),
   assert(jsquash_repository('try-statement',Try_Path, Catch_Path, Finally_Path, New_Path)).

html_generator('type', Type) :-
   forall( ( SE := Type / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'type-name', Type_Name := SE / 'identifier', Type_Name = _:_:[Value], write(Value) 
      ; SEN = 'left-bracket', write('[')
      ; SEN = 'right-bracket', write(']')
      ; SEN = 'primitive-type', html_generator(SEN, SE)
      ; true ) ).
 
html_generator('primitive-type', Primitive_Type) :- 
   forall( ( SE := Primitive_Type / child::'*', SE = SEN:_:_ ),
     ( SEN = 'left-bracket', write('[]')
     ; not(member(SEN, ['left-bracket', 'right-bracket'])), SE := _:_:[Value], write(Value)
     ; true ) ).

html_generator('type-definition', Type_Definition, Path) :-
   html_generator('class-definition', Type_Definition / 'class-definition', Path).
   
html_generator('variable-access-expression', Variable_Access_Expr, Path) :-
   element_to_position(Variable_Access_Expr, Pos), 
   append(Path, [Pos], New_Path),
   write('<div class="local" title="'), write(New_Path), write('" id="'), write(New_Path), write('">'),
   ( html_generator('qualifier', Variable_Access_Expr, _), write('.')
   ; true),
   Identifier := Variable_Access_Expr / 'identifier', 
   html_generator('identifier', Identifier), write('</div>').

html_generator('variable-declaration-expression', Variable_Decl_Expr, Path) :-
   html_generator('type', Variable_Decl_Expr / 'type'), write('&nbsp;'),
   html_generator('variable-declaration-list', Variable_Decl_Expr / 'variable-declaration-list', Path).

html_generator('variable-declaration', Variable_Decl, Path) :-
   element_to_position(Variable_Decl, Pos), append(Path, [Pos], New_Path),
   forall( ( SE := Variable_Decl / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'type-ref'
      ; SEN = 'identifier', SE = _:_:[Identifier], write('<div class="local" title="'), 
        write(New_Path), write('" id="'), write(New_Path), write('">'), write(Identifier), write('</div>') 
      ; SEN = 'whitespace', write('&nbsp;')
      ; SEN = 'initializer', html_generator(SEN, SE, New_Path)
      ; true ) ).
      
html_generator('variable-declaration-list', Variable_Decl_List, Path) :-
   forall( ( SE := Variable_Decl_List / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'comma', write(',') 
      ; SEN = 'whitespace', write('&nbsp;') 
      ; SEN = 'variable-declaration', html_generator(SEN, SE, Path)
      ; true ) ).

html_generator('variable-declaration-statement', Variable_Decl_Stmt, Path) :-
   html_generator('type', Variable_Decl_Stmt / 'type'), write('&nbsp;'),
   html_generator('variable-declaration-list', Variable_Decl_Stmt / 'variable-declaration-list', Path), write(';').

html_generator('while-statement', While_Statement, Path) :-
   element_to_position(While_Statement, Pos), 
   append(Path, [Pos], New_Path),
   append(New_Path, [1], Cond_Path),
   append(New_Path, [2], Block_Path), 
   forall( ( SE := While_Statement / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'while', write('<div class="keyword">while</div>')
      ; SEN = 'whitespace', write('&nbsp;')
      ; SEN = 'left-parenthesis', write('(')
      ; SEN = 'condition', html_generator(SEN, SE, Cond_Path)
      ; SEN = 'right-parenthesis', write(')')
      ; SEN = 'statement', html_generator(SEN, SE, Block_Path)
      ; true ) ).

html_generator('switch-block', Switch_Block, Path) :-
   forall( ( SE := Switch_Block / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'switch-label', html_generator(SEN, SE, Path)
      ; SEN = 'whitespace', Cnt := SE@'length', html_generator('whitespaces', Cnt)
      ; SEN = 'left-brace', write('{')
      ; SEN = 'condition', html_generator(SEN, SE, Path)
      ; SEN = 'right-brace', write('}')
      ; SEN = 'newline', write('<br>')
      ; SEN = 'statement', html_generator(SEN, SE, Path)
      ; true ) ).
   
html_generator('switch-label', Switch_Label, Path) :-
   forall( ( SE := Switch_Label / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'case-statement', html_generator(SEN, SE, Path)
      ; SEN = 'default-statement', html_generator(SEN, SE, Path)
      ; true ) ).

html_generator('case-statement', Case_Stmt, Path) :-
   forall( ( SE := Case_Stmt / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'case', write('<div class="keyword">case</div>')
      ; SEN = 'whitespace', write('&nbsp;')
      ; SEN = 'colon', write(':')
      ; SEN = 'expression', html_generator(SEN, SE, Path)
      ; true ) ).
  
html_generator('default-statement', Default_Stmt, Path) :-
   forall( ( SE := Default_Stmt / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'default', write('<div class="keyword">default</div>')
      ; SEN = 'whitespace', write('&nbsp;')
      ; SEN = 'colon', write(':')
      ; SEN = 'expression', html_generator(SEN, SE, Path)
      ; true ) ).

html_generator('switch-statement', Switch_Stmt, Path) :-
   element_to_position(Switch_Stmt, Switch_Pos), 
   append(Path, [Switch_Pos], Switch_Stmt_Path),
   append(Switch_Stmt_Path, [1], Switch_Path),
   append(Switch_Stmt_Path, [2], Block_Path),
   forall( ( SE := Switch_Stmt / child::'*', SE = SEN:_:_ ),    
      ( SEN = 'switch', write('<div class="keyword">switch</div>')
      ; SEN = 'whitespace', Cnt := SE@'length', html_generator('whitespaces', Cnt)
      ; SEN = 'left-parenthesis', write('(')
      ; SEN = 'condition', html_generator(SEN, SE, Switch_Path)
      ; SEN = 'right-parenthesis', write(')')
      ; SEN = 'newline', write('<br>')
      ; SEN = 'switch-block', html_generator(SEN, SE, Block_Path)
      ; true ) ).
      
html_generator('whitespaces', Cnt) :-
   adelung_2:to_number(Cnt, Count), html_generator('whitespaces_helper', Count).
html_generator('whitespaces_helper', 0) :- !.
html_generator('whitespaces_helper', X) :- 
   Y is X - 1, write('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'), html_generator('whitespaces_helper', Y).