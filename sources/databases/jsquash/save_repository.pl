% # create the file: sql_statements.pl
% # sql_statements.pl is the list of all result sql_statements found during the analysis.
save_repository('sql_statements') :-
   write('Sichern der SQL-Statements nach sql_statements.pl ......... '),
   tell('h:/studium/Hauptstudium/Diplomarbeit/programming/prolog/repository/sql_statements.pl'), 
   listing(sql_statement/2), 
   told,
   writeln('fertig').
   
% # create the file: 
save_repository('jsquash_repository') :-
   write('Sichern des Repositories nach repository.pl ............... '),
	tell('h:/studium/Hauptstudium/Diplomarbeit/programming/prolog/repository/repository.pl'), 
	listing(jsquash_repository/2),
	listing(jsquash_repository/3),
	listing(jsquash_repository/4),
	listing(jsquash_repository/5),
	listing(jsquash_repository/6),
	listing(jsquash_repository/7),
	told,
   writeln('fertig').