remove_quotation_marks(L1, L2) :-
	name(L1, LA),	
	( LA = [34|X], !, Cut1 = X ; Cut1 = LA ), reverse(Cut1, RevCut), 
	( RevCut = [34|Y], !, Cut2 = Y; Cut2 = RevCut), reverse(Cut2, Z), name(L2, Z).

check_compatibility(RevInst1, RevInst2, MaxRevInst) :-               write_m(rulename, ['check_compatibility with ', RevInst1, ' and ', RevInst2]),
	( append(RevInst1, _, RevInst2), 
     MaxRevInst = RevInst2
	; not(append(RevInst1, _, RevInst2)), 
     append(RevInst2, _, RevInst1), 
     MaxRevInst = RevInst1).
    
% # Hilfsregel f�r myflatten
is_data(A) :- 
   A = [X,Y,Z,C], 
   not(is_list(X)), 
   not(is_list(Y)), 
   is_list(Z), 
   is_list(C).

% # Regel zum Flachmachen der zum Teil mehrelementigen Resultatslisten (Grund hierf�r: reverse-Methodenaufrufe --> hier spaltet sich die Berechnung aufgrund mehrere Methodenaufrufm�glichkeiten auf)
% # Vorgehen: ein Wert besteht aus [ Datentyp, Wert, [ Liste der MethodenIds, �ber die reverse der Wert geholt wurde ] ]
% # Beim normalen flatten w�rden die Tripel aufgel�st, da die Liste der MethodenIds auf flach gemacht w�rde!!
flatten(jaml, [], []) :- !.
flatten(jaml, [A|L], R) :-
	is_list(A), not(is_data(A)), !,
	flatten(jaml, A, R1),
	flatten(jaml, L, R2),
	append(R1, R2, R).
flatten(jaml, [A|L], [A|R]) :- flatten(jaml, L, R).

biggerSwitchBlockStatement(A, B) :-
   A = [Path1:_|_], 
   B = [Path2:_|_], 
   bigger_path(Path1, Path2).

bigger_path([], [_|_]) :- !, fail.
bigger_path([_|_], []) :- !.
bigger_path([A|ListA], [B|ListB]) :-  
    adelung_2:to_number(A, X), adelung_2:to_number(B, Y),
	( X < Y, !, fail
	; Y < X, !, true
	; X = Y, bigger_path(ListA, ListB)).  

sortSwitchBlockStatements(Statements, SortedStatements) :- 
   sortStatementsA(Statements, SortedStatements).
   
sortStatementsA([],[]).
sortStatementsA(Statements, [Maximum|SortedStatements]) :-
	maximumSwitchBlockStatement(Statements, Maximum), 
   remove_x_from_list(Maximum, Statements, Reduced),
	sortStatementsA(Reduced, SortedStatements).
      
maximumSwitchBlockStatement([X|Xs], X) :-
   X = [Path1:_|_],
   forall( member(Y, Xs),
      ( Y = [Path2:_|_],
        bigger_path(Path2, Path1) ) ).
        
maximumSwitchBlockStatement([_|Xs], X) :-
   maximumSwitchBlockStatement(Xs, X).
   
max_pos([X|Xs], X) :-
   first(X, Path1),
   forall(member(Y, Xs),(
     first(Y, Path2), 
     bigger_path(Path2, Path1))).
max_pos([_|Xs], X) :- max_pos(Xs, X).

remove_x_from_list(_, [], []).
remove_x_from_list(X, [X|List], List).
remove_x_from_list(X, [Y|List], [Y|Cleaned]) :-
   X \= Y,
   remove_x_from_list(X, List, Cleaned).
   
equal_path([], []).
equal_path([A|As], [A|Bs]) :-
   equal_path(As, Bs).
    
assert_all(Predicate, Arguments) :-
	( foreach([_, Arg, _, History], Arguments) do ( Result =.. [Predicate,Arg,History], assert(Result))),
                                                                     write_m(finish, ['sql-statements asserted']).
   
assert('new-loop-value', RT, Possibility, Value, Search_Path) :-
   jsquash_repository('while-statement', _, Loop_Path, _),
   append(Loop_Path, _, Search_Path),
   append(Loop_Path, [0], Loop_Value_Path),
   next_global_loop_value_id(Loop_Id),
   append(Loop_Value_Path, [Loop_Id], Last_Loop_Value_Path),
   possibility_to_preobject(Possibility, Preobject),
   assert(jsquash_repository('loop-value', RT, Value, Preobject,
     Loop_Path, Last_Loop_Value_Path)),
                                                                     Value = [_, Val, _,_],
                                                                     get_runtime(RT, [@searched_id=Id, array@index=Index]),
                                                                     write_m(loopinfo, ['---------------------------------']),
                                                                     write_m(loopinfo, ['ASSERT loop_value: ', Id,' = ',Val, ' (',Index,') at ', Last_Loop_Value_Path]),
                                                                     write_m(loopinfo, ['ASSERTING search_path: ', Search_Path]),
                                                                     write_m(loopinfo, ['---------------------------------']), nl_m.
   
retractall(Predicate, Path) :-
   ( member(Predicate, ['last-loop-value', 'loop-value']),
     findall(Found_Path, (
        jsquash_repository('while-statement', _, Found_Path, _),
        append(Path, _, Found_Path)),
     Found_Paths),
     forall(member(Found_Path, Found_Paths),
        retractall(jsquash_repository(Predicate, _, _, _, Found_Path, _)))
   ; Predicate = 'all-loop-values',
     retractall('last-loop-value', Path),
     retractall('loop-value', Path)).
   
next_global_loop_value_id(X) :-
   jsquash_repository('global-loop-value-id', X),
   retract(jsquash_repository('global-loop-value-id', X)),
   Y is X + 1,
   assert(jsquash_repository('global-loop-value-id', Y)).