

/******************************************************************/
/***                                                            ***/
/***          DDK:  MySQL ODBC Database Updates                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* mysql_insert_tuple(Connection, Database:Table, Pairs) <-
      */

mysql_insert_tuple(Connection, Database:Table, Pairs) :-
   pair_lists(:, Attributes, Tuple, Pairs),
   mysql_insert_tuple(
      Connection, Database:Table, Attributes, Tuple).


/* mysql_insert_tuple(
         Connection, Database:Table, Attributes, Tuple) <-
      */

mysql_insert_tuple(
      Connection, Database:Table, Attributes, Tuple) :-
   mysql_insert_tuples(
      Connection, Database:Table, Attributes, [Tuple]).


/* mysql_insert_tuples(
         Connection, Database:Table, Attributes, Tuples) <-
      */

mysql_insert_tuples(
      Connection, Database:Table, Attributes, Tuples, Error) :-
   Goal = mysql_insert_tuples(
      Connection, Database:Table, Attributes, Tuples),
   catch(Goal, Error, true),
   writeln(user, Error).

mysql_insert_tuples(
      Connection, Database:Table, Attributes, Tuples) :-
   !,
   mysql_use_database(Connection, Database),
   mysql_insert_tuples(Connection, Table, Attributes, Tuples).

mysql_insert_tuples(Connection, Table, Attributes, Tuples) :-
   concat_with_separator(Attributes, ', ', As),
   maplist( tuple_to_mysql_tuple,
      Tuples, Tuples_2 ),
   concat_with_separator(Tuples_2, ',\n', Vs),
   concat(['insert into ', Table, ' (', As, ') ',
      'values\n', Vs], Statement),
   mysql_execute_statement(Connection, Statement).

tuple_to_mysql_tuple(Tuple_1, Tuple_2) :-
   concat_with_separator(Tuple_1, ''', ''', Values),
   concat(['(''', Values, ''')'], Tuple_2).


/* mysql_delete_tuple(
         Connection, Database:Table, Attributes, Tuple) <-
      */

mysql_delete_tuple(
      Connection, Database:Table, Attributes, Tuple) :-
   pair_lists(:, Attributes, Tuple, Pairs),
   mysql_delete_tuple(Connection, Database:Table, Pairs).


/* mysql_delete_tuple(Connection, Database:Table, Pairs_Where) <-
      */

mysql_delete_tuple(Connection, Database:Table, Pairs_Where) :-
   mysql_pairs_where_to_where_condition(Pairs_Where, Condition),
   concat(['delete from ', Table, Condition], Statement),
   mysql_execute_statement(Connection, Database, Statement).


/* mysql_update_table(
         Connection, Database:Table, Pairs_Set, Pairs_Where) <-
      */

mysql_update_table(
      Connection, Database:Table, Pairs_Set, Pairs_Where) :-
   attribute_value_pairs_to_string(Pairs_Set, ',', Set),
   mysql_pairs_where_to_where_condition(Pairs_Where, Condition),
   concat(['update ', Table, ' set', Set, Condition], Statement),
   mysql_execute_statement(Connection, Database, Statement).


/* mysql_insert_or_update(
         Connection, DB_Schema, Database:Table, Pairs) <-
      */

mysql_insert_or_update(Connection, Database:Table, Pairs) :-
   mysql_database_schema_to_xml(Database, DB_Schema),
   mysql_insert_or_update(
      Connection, DB_Schema, Database:Table, Pairs).

mysql_insert_or_update(
      Connection, DB_Schema, Database:Table, Pairs) :-
   Item = Table:Pairs:[],
%  fn_item_includes_primary_key(DB_Schema, Item),
   fn_item_to_insert_or_update_statement(
      Connection, DB_Schema, Database, Item, Statement),
   ddk_spy(writeln(user, Statement)),
   call(Statement).


/* fn_item_to_insert_statement(
         Connection, Database, Item, Statement) <-
      */

fn_item_to_insert_statement(
      Connection, Database, Item, Statement) :-
   fn_item_parse(Item, Table:As:[]),
   Statement = mysql_insert_tuple(
      Connection, Database:Table, As).


/* fn_item_to_delete_statement(
         Connection, Database, Item, Statement) <-
      */

fn_item_to_delete_statement(
      Connection, Database, Item, Statement) :-
   fn_item_parse(Item, Table:As:[]),
   Statement = mysql_delete_tuple(
      Connection, Database:Table, As).


/* fn_item_to_update_statement(
         Connection, DB_Schema, Database, Item, Statement) <-
      */

fn_item_to_update_statement(
      Connection, DB_Schema, Database, Item, Statement) :-
   Table := Item/tag::'*',
   mysql_database_schema_table_to_primary_key(
      DB_Schema, Table, Key),
   findall( A:V,
      ( V := Item@A,
        member(A, Key) ),
      Pairs_Where ),
   findall( A:V,
      ( V := Item@A,
        \+ member(A, Key) ),
      Pairs_Set ),
   Pairs_Set \= [],
   Statement = mysql_update_table(
      Connection, Database:Table, Pairs_Set, Pairs_Where).


/* fn_item_to_insert_or_update_statement(
         Connection, DB_Schema, Database, Item, Statement) <-
      */

fn_item_to_insert_or_update_statement(
      Connection, DB_Schema, Database, Item, Statement) :-
   ( \+ fn_item_includes_primary_key(DB_Schema, Item)
   ; \+ fn_item_primary_key_is_in_database(
        Connection, DB_Schema, Database, Item) ),
   fn_item_to_insert_statement(
      Connection, Database, Item, Statement),
   !.
fn_item_to_insert_or_update_statement(
      Connection, DB_Schema, Database, Item, Statement) :-
   fn_item_to_update_statement(
      Connection, DB_Schema, Database, Item, Statement),
   !.

fn_item_primary_key_is_in_database(
      Connection, DB_Schema, Database, Item) :-
   Table := Item/tag::'*',
   mysql_database_schema_table_to_primary_key(
      DB_Schema, Table, Key),
   findall( A:V,
      ( V := Item@A,
        member(A, Key) ),
      Pairs ),
   mysql_table_select(Connection, Database:Table, Pairs, Tuples),
   Tuples \= [].

/*
fn_item_to_insert_or_update_statement(
      DB_Schema, Database, Item, Statement) :-
   fn_item_includes_primary_key(DB_Schema, Item),
   fn_item_to_update_statement(DB_Schema, Database, Item, S),
   S = mysql_update_table(Database:Table, Pairs_Set, Pairs_Where),
   mysql_table_select(mysql, Database:Table, Pairs_Where, Tuples),
   ( Tuples = [] ->
     append(Pairs_Set, Pairs_Where, As),
     Statement = mysql_insert_tuple(Database:Table, As)
   ; Statement = S ),
   !.
fn_item_to_insert_or_update_statement(
      DB_Schema, Database, Item, Statement) :-
   \+ fn_item_includes_primary_key(DB_Schema, Item),
   fn_item_to_insert_statement(Database, Item, Statement),
   !.
*/


/******************************************************************/


