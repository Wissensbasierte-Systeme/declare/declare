

/******************************************************************/
/***                                                            ***/
/***          DisLog:  MySQL - Tests                            ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


/* test(join, X) <-
      the test require certain MySQL tables and Prolog facts. */

test(join, mysql_1) :-
   Sql = [ use:[ddbase],
      select:[a^v, d^w],
      from:[calls-a, calls-b, calls-c, calls-d],
      where:[a^w=b^v and b^w=c^v and c^w=d^v] ],
   dislog_variable_switch(mysql_result_file_prune, X, no),
   measure( mysql_select_execute(Sql, Tuples), Time ),
   dislog_variable_set(mysql_result_file_prune, X),
   join_test_message('mysql: ', Tuples, Time).

test(join, mysql_2) :-
   concat(['use ddbase; ',
      'select a.v, d.w ',
      'from calls a, calls b, calls c, calls d ',
      'where a.w=b.v and b.w=c.v and c.w=d.v;'], Sql),
   measure( mysql_execute(Sql), Time ),
   write_list(user, ['mysql: ', Time, ' sec.\n']).

test(join, ddbase) :-
   dislog_variable_get(home, '/projects/RAR/magic_sets/arc.pl', F),
   measure( ( dread(pl, F, Arc_2), sort(Arc_2, Arc) ), Time_1 ),
   join_test_message('dread:   ', Arc_2, Time_1),
   measure( assert_facts(Arc), Time_2 ),
   write_list(user, ['assert:  ', Time_2, ' sec.\n']),
   measure( findall( [X,Y],
      ( arc(X, A), arc(A, B), arc(B, C), arc(C, Y) ),
      I ), Time_3 ),
   join_test_message('join:    ', I, Time_3),
   measure( retract_facts(arc/2), Time_4 ),
   write_list(user, ['retract: ', Time_4, ' sec.\n']).

join_test_message(Operation, I, Time) :-
   write_list(user, [Operation, Time, ' sec.  ']),
   length(I, M), sort(I, J), length(J, N),
   writeln(user, M->N).


/******************************************************************/


