

/******************************************************************/
/***                                                            ***/
/***          DDK:  MySQL Select                                ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      mysql_password/1.

  :- dislog_variable_set(mysql_connect, shell).
% :- dislog_variable_set(mysql_connect, odbc).

  :- dislog_variable_set(mysql_result_file_prune, yes).
% :- dislog_variable_set(mysql_result_file_prune, no).


/*** tests ********************************************************/


test(mysql, select_execute, employee) :-
   Sql = [ use:[company],
      select:[dno, ssn, {{lname}}],
      from:[employee],
      where:[],
      order_by:[dno] ],
   mysql_select_execute(Sql, Tuples), 
   Attributes = ['DNO', 'SSN', 'LNAME'],
   xpce_display_table(Attributes, Tuples).

test(mysql, select_execute_shell, employee_works_on) :-
   Sql_Statements =
      'use company;
       select ''mysql_result_atom(['',
          SSN, '', '', PNO, '', '', sum(HOURS), '']).''
       from employee, works_on
       where SSN = ESSN
       group by SSN, PNO;',
   mysql_select_execute_shell(Sql_Statements, Tuples),
   Attributes = ['SSN', 'PNO', 'HOURS'],
   xpce_display_table(employee, Attributes, Tuples).


/*** interface ****************************************************/


/* mysql_execute(Sql_Statements) <-
      */

mysql_execute(Sql_Statements) :-
   mysql_execute(writeln, Sql_Statements).

mysql_execute(Write_Predicate, Sql_Statements) :-
   dislog_variable_get(home, DisLog_Directory),
   concat(DisLog_Directory, '/results/mysql_statements', File_1),
   predicate_to_file( File_1,
      ( Goal =.. [Write_Predicate, Sql_Statements],
        call(Goal) ) ),
   concat(DisLog_Directory, '/results/mysql_result', File_2),
   mysql_execute_file_to_file(File_1, File_2).


/* mysql_execute_file_to_file(File_1,File_2) <-
      */

mysql_execute_file_to_file(File_1,File_2) :-
   current_prolog_flag(unix,true),
%  concat(['cat ',File_1,' | mysql > ',File_2],Command),
%  us(Command).
%  mysql_password_get(Password),
%  concat(['mysql -p', Password, ' < ',File_1,' > ',File_2],Command),
   concat(['mysql < ',File_1,' > ',File_2],Command),
   shell(Command,_).
mysql_execute_file_to_file(File_1,File_2) :-
   current_prolog_flag(windows,true),
   concat(
      ['C:/mysql/bin/mysql < ',File_1,' > ',File_2],
      Command_1 ),
   unix_command_to_windows_command(Command_1,Command_2),
   concat('command.com /C ',Command_2,Command_3),
   shell(Command_3).

mysql_password_get(Password) :-
   mysql_password(Password),
   !.
mysql_password_get(Password) :-
   writeln(user, 'please enter the password for MySQL: '),
   read(Password),
   assert(mysql_password(Password)).


/* unix_command_to_windows_command(Command_1,Command_2) <-
      */

unix_command_to_windows_command(Command_1,Command_2) :-
   name(Command_1,List_1),
%  list_exchange_elements(["\/\\"],List_1,List_2),
   list_exchange_elements([47,92],List_1,List_2),
   name(Command_2,List_2).


/* mysql_select_execute(Sql_Command, Tuples) <-
      */

mysql_select_execute(Sql_Command, Tuples) :-
   dislog_variable_get(mysql_connect, Mode),
   ( Mode = odbc ->
     mysql_select_execute_odbc(Sql_Command, Tuples)
   ; Mode = shell ->
     mysql_select_execute_shell(
        mysql_select_command_write,
        Sql_Command, Tuples ) ).


/* mysql_select_execute_shell(Sql_Statements, Tuples) <-
      */

mysql_select_execute_shell(Sql_Statements, Tuples) :-
   mysql_select_execute_shell(
      writeln,
      Sql_Statements, Tuples ).

mysql_select_execute_shell(
      Write_Predicate, Sql_Statements, Tuples) :-
   ( mysql_select_execute_shell_2(
        Write_Predicate, Sql_Statements, Tuples)
   ; Tuples = [] ).

mysql_select_execute_shell_2(
      Write_Predicate, Sql_Statements, Tuples) :-
   mysql_execute(Write_Predicate, Sql_Statements),
   style_check(-singleton),
   dislog_variable_get(home, '/results/mysql_result', File),
   mysql_result_file_prune(File),
   [File],
   style_check(+singleton),
   collect_arguments(mysql_result_atom, [_|Tuples]),
   retractall(mysql_result_atom(_)).


/*** implementation ***********************************************/


/* mysql_result_file_prune(File) <-
      */

mysql_result_file_prune(File) :-
   dislog_variable_get(mysql_result_file_prune, yes),
   !,
   dread(txt, File, Name_1),
   Substitution = [
      [[195, 131, 194, 164], "�"],
      [[195, 164], "�"],
      [[195, 131, 194, 182], "�"],
      [[195, 182], "�"],
      [[195, 131, 194, 188], "�"],
      [[195, 188], "�"],
      [[195, 131, 197, 121], "�"],
      [[195, 132], "�"],
      [[195, 131, 197, 141], "�"],
      [[195, 150], "�"],
      [[195, 131, 197, 147], "�"],
      [[195, 156], "�"],
      [[195, 131, 197, 184], "�"],
      [[195, 159], "�"] ],
   name_exchange_sublist(Substitution, Name_1, Name_2),
   Substitution_1 = [
      ["\t\'\t", [200, 200, 200]] ],
   name_exchange_sublist(Substitution_1, Name_2, Name_3),
   Substitution_2 = [
      ["'", "''"] ],
   name_exchange_sublist(Substitution_2, Name_3, Name_4),
   Substitution_3 = [
      [[200, 200, 200], "\t\'\t"] ],
   name_exchange_sublist(Substitution_3, Name_4, Name_5),
   dwrite(txt, File, Name_5).
mysql_result_file_prune(_).


/* mysql_select_command_write(Sql_Command) <-
      */

mysql_select_command_write(Sql_Command) :-
   [Use] := Sql_Command^use,
   Select := Sql_Command^select,
   From := Sql_Command^from,
   write_list(['use ',Use,';']), nl,
   write('select ''mysql_result_atom(['', '),
   mysql_print_items(', '', '', ',Select),
   writeln(', '']).'''),
   write('from '), mysql_print_items(', ',From), nl,
   call_or_true(
      ( [Where] := Sql_Command^where,
        write('where '), mysql_print_boolean(Where), nl ) ),
   call_or_true(
      ( Group_By := Sql_Command^group_by,
        write('group by '), mysql_print_items(', ',Group_By) ) ),
   call_or_true(
      ( Order_By := Sql_Command^order_by,
        write('order by '), mysql_print_items(', ',Order_By) ) ),
   writeln(';').

call_or_true(Goal) :-
   call(Goal),
   !.
call_or_true(_).


/* mysql_print_boolean(Condition) <-
      */

mysql_print_boolean(X and Y) :-
   mysql_print_boolean(X),
   write(' and '),
   mysql_print_boolean(Y).
mysql_print_boolean((X or Y)) :-
   write(' ( '),
   mysql_print_boolean(X),
   write(' or '),
   mysql_print_boolean(Y),
   write(' ) ').
mysql_print_boolean(A=B) :-
   mysql_print_item(A),
   write(' = '),
   mysql_print_item(B).
mysql_print_boolean(A like B) :-
   mysql_print_item(A),
   write(' like '),
   mysql_print_item(B).
mysql_print_boolean(A>=B) :-
   mysql_print_item(A),
   write(' >= '),
   mysql_print_item(B).
mysql_print_boolean(A=<B) :-
   mysql_print_item(A),
   write(' <= '),
   mysql_print_item(B).
mysql_print_boolean(A\=B) :-
   mysql_print_item(A),
   write(' <> '),
   mysql_print_item(B).

mysql_print_items(Separator,[X1,X2|Xs]) :-
   mysql_print_item(X1), write(Separator),
   mysql_print_items(Separator,[X2|Xs]).
mysql_print_items(_,[X1]) :-
   mysql_print_item(X1).
mysql_print_items(_,[]).

mysql_print_item(min(X)) :-
   write('min('),
   mysql_print_item(X),
   write(')').
mysql_print_item(max(X)) :-
   write('max('),
   mysql_print_item(X),
   write(')').
mysql_print_item(avg(X)) :-
   write('avg('),
   mysql_print_item(X),
   write(')').
mysql_print_item(sum(X)) :-
   write('sum('),
   mysql_print_item(X),
   write(')').
mysql_print_item(count(X)) :-
   write('count('),
   mysql_print_item(X),
   write(')').
mysql_print_item(X*Y) :-
   mysql_print_item(X),
   write('*'),
   mysql_print_item(Y).
mysql_print_item(X/Y) :-
   mysql_print_item(X),
   write('/'),
   mysql_print_item(Y).
mysql_print_item(O-Item) :-
   !,
   write_list([O,' ',Item]).
mysql_print_item(O^Item) :-
   !,
   write_list([O,'.',Item]).
mysql_print_item({{Item}}) :-
   !,
   write(''''''''', '),
   mysql_print_item(Item),
   write(', ''''''''').
mysql_print_item({Item}) :-
   !,
   write(''''),
   mysql_print_item(Item),
   write('''').
mysql_print_item(Item) :-
   write(Item).


/******************************************************************/


