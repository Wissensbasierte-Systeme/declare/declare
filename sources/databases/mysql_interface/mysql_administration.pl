

/******************************************************************/
/***                                                            ***/
/***          DDK:  MySQL Administration                        ***/
/***                                                            ***/
/******************************************************************/


:- mutex_create_(mysql_information_schema_mutex).


/*** interface ****************************************************/


/* mysql_databases_and_tables <-
      */

mysql_databases_and_tables :-
   mysql_databases_and_tables(Tuples),
   xpce_display_table(['Database', 'Table'], Tuples).

mysql_databases_and_tables(Tuples) :-
   dislog_variable_get(
      odbc_connection_data_dictionary, Connection),
   mysql_databases_and_tables(Connection, Tuples).

mysql_databases_and_tables(Connection, Tuples) :-
   Database = information_schema,
   Table = tables,
   with_mutex(mysql_information_schema_mutex,
      mysql_table_select(Connection, Database:Table, Tuples_2)),
   table_projection([2, 3], Tuples_2, Tuples),
   !.


/* mysql_database_to_tables(Database, Tables) <-
      */

mysql_database_to_tables(Database, Tables) :-
   mysql_databases_and_tables(Tuples),
   findall( Table,
      member([Database, Table], Tuples),
      Tables ).


/* mysql_database_describe(Connection, Database, Tuples) <-
      */

mysql_database_describe(Database) :-
   mysql_database_describe(Database, Tuples),
   Attributes =
      ['Table', 'Column', 'is_nullable', 'Type', 'Key', 'Extra'],
   xpce_display_table(Attributes, Tuples).

mysql_database_describe(Database, Tuples) :-
   dislog_variable_get(
      odbc_connection_data_dictionary, Connection),
   mysql_database_describe(Connection, Database, Tuples).

/*
mysql_database_describe(Connection, Database, Tuples) :-
   mysql_use_database(Connection, information_schema),
   concat([ 'select TABLE_NAME, COLUMN_NAME, ',
      'IS_NULLABLE, COLUMN_TYPE, COLUMN_KEY, EXTRA from ',
      'COLUMNS where TABLE_SCHEMA=''', Database, '''' ], Statement),
   with_mutex(mysql_information_schema_mutex,
      odbc_query_to_tuples(Connection, Statement, Tuples)),
   !.
*/

mysql_database_describe(Connection, Database, Tuples) :-
   mysql_use_database(Connection, information_schema),
   concat([ 'select TABLE_NAME, COLUMN_NAME, ',
      'IS_NULLABLE, COLUMN_TYPE, COLUMN_KEY, EXTRA, ',
      'ORDINAL_POSITION from ',
      'COLUMNS where TABLE_SCHEMA=''', Database, ''' ',
      'order by TABLE_NAME, ORDINAL_POSITION' ], Statement),
   with_mutex(mysql_information_schema_mutex,
      odbc_query_to_tuples(Connection, Statement, Tuples_2)),
%  sort_by_components([1, 7], Tuples_2, Tuples_3),
   Tuples_2 = Tuples_3,
%  table_projection([1,2,3,4,5,6,7], Tuples_3, Tuples),
   table_projection([1,2,3,4,5,6], Tuples_3, Tuples),
   !.


/* mysql_database_table_describe(Database:Table, Tuples) <-
      */

mysql_database_table_describe(Database:Table, Tuples) :-
   mysql_database_describe(Database, Rows),
   findall( [Attribute, Type],
      member([Table, Attribute, _, Type, _, _], Rows),
      Tuples ).


/* mysql_database_table_to_create_table(
         Connection, Database:Table, Create) <-
      */

mysql_database_table_to_create_table(
      Connection, Database:Table, Create) :-
   mysql_use_database(Connection, Database),
   concat('SHOW CREATE TABLE ', Table, Statement),
   odbc_query(Connection, Statement, row(_, Create), []).


/* mysql_create_database(Connection, Database) <-
      */

mysql_create_database(Connection, Database) :-
   mysql_drop_database(Connection, Database),
   concat('CREATE DATABASE ', Database, Statement),
   odbc_query(Connection, Statement, Row, []),
   writeln(user, Row).


/* mysql_drop_database(Connection, Database) <-
      */

mysql_drop_database(Connection, Database) :-
   concat('DROP DATABASE IF EXISTS ', Database, Statement),
   odbc_query(Connection, Statement, Row, []),
   writeln(user, Row).


/* mysql_create_tables(
         Connection, Database:Tables, Create_Statements) <-
      */

mysql_create_tables(
      Connection, Database:Tables, Create_Statements) :-
   ( foreach(Table, Tables),
     foreach(Create_Statement, Create_Statements) do
        mysql_create_table(
           Connection, Database:Table, Create_Statement)  ).


/* mysql_create_table(
         Connection, Database:Table, Create_Statement) <-
      */

mysql_create_table(Connection, Database:Table, Create_Statement) :-
   mysql_use_database(Connection, Database),
   concat('DROP TABLE IF EXISTS ', Table, Drop_Statement),
   odbc_query(Connection, Drop_Statement, Row_1, []),
   writeln(user, Row_1),
   odbc_query(Connection, Create_Statement, Row_2, []),
   writeln(user, Row_2).


/* mysql_database_table_comments(Connection, Database, Tuples) <-
      */

mysql_database_table_comments(Database, Tuples) :-
   dislog_variable_get(
      odbc_connection_data_dictionary, Connection),
   mysql_database_table_comments(Connection, Database, Tuples).

mysql_database_table_comments(Connection, Database, Tuples) :-
   mysql_use_database(Connection, information_schema),
   concat([ 'select TABLE_NAME, TABLE_COMMENT from ',
      'TABLES where TABLE_SCHEMA=''', Database, '''' ], Statement),
   with_mutex(mysql_information_schema_mutex,
      odbc_query_to_tuples(Connection, Statement, Tuples)).


/* mysql_table_add_column(
         Connection, Database:Table, Column:Type) <-
      */

mysql_table_add_column(Connection, Database:Table, Column:Type) :-
   mysql_use_database(Connection, Database),
   concat([ 'alter table ', Table,
      ' add column ', Column, ' ', Type ], Statement),
   odbc_query(Connection, Statement, Row, []),
   writeln(user, Row).


/* mysql_table_drop_column(Connection, Database:Table, Column) <-
      */

mysql_table_drop_column(Connection, Database:Table, Column) :-
   mysql_use_database(Connection, Database),
   concat([ 'alter table ', Table,
      ' drop column ', Column ], Statement),
   odbc_query(Connection, Statement, Row, []),
   writeln(user, Row).


/* mysql_table_check_key(Connection, Database:Table, Columns) <-
      */

mysql_table_check_key(Connection, Database:Table, Columns) :-
   mysql_use_database(Connection, Database),
   concat_with_separator(Columns, ', ', Cs),
   concat([ 'select ', Cs, ', count(*) from ', Table,
      ' group by ', Cs ], Statement),
   odbc_query_to_tuples(Connection, Statement, Tuples),
   append(Columns, ['count(*)'], Attributes),
   xpce_display_table(Attributes, Tuples).


/* mysql_database_table_to_xpce(Database:Table) <-
      */

mysql_database_table_to_xpce(Database:Table) :-
   mysql_database_table_describe(Database:Table, Pairs),
   pair_lists([], Attributes, _, Pairs),
   mysql_table_select(Database:Table, Tuples),
   xpce_display_table(Attributes, Tuples).


/******************************************************************/


