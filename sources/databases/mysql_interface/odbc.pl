

/******************************************************************/
/***                                                            ***/
/***          DDK:  ODBC Interface                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* odbc_select(Connection, Database:Table,
         Pairs_Where, Pairss) <-
      */

odbc_select(Connection, Database:Table,
      Pairs_Where, Pairss) :-
   mysql_relation_schema_to_attributes(
      Connection, Database:Table, Attributes),
   mysql_table_select(
      Connection, Database:Table, Pairs_Where, Tuples),
   maplist( pair_lists(:, Attributes),
      Tuples, Pairss ).


/* odbc_insert(Connection, Database:Table, Pairs) <-
      */

odbc_insert(Connection, Database:Table, Pairs) :-
   mysql_insert_tuple(Connection, Database:Table, Pairs).


/* odbc_delete(Connection, Database:Table, Pairs) <-
      */

odbc_delete(Connection, Database:Table, Pairs) :-
   mysql_delete_tuple(Connection, Database:Table, Pairs).


/* odbc_update(Connection, Database:Table,
         Pairs_Set, Pairs_Where) <-
      */

odbc_update(Connection, Database:Table,
      Pairs_Set, Pairs_Where) :-
   mysql_update_table(
      Connection, Database:Table, Pairs_Set, Pairs_Where).


/* odbc_insert_or_update(Connection, Database:Table, Pairs) <-
      */

odbc_insert_or_update(Connection, Database:Table, Pairs) :-
   mysql_insert_or_update(Connection, Database:Table, Pairs).


/******************************************************************/


