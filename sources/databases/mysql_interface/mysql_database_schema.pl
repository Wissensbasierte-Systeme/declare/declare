

/******************************************************************/
/***                                                            ***/
/***          DDK:  MySQL Database Schema                       ***/
/***                                                            ***/
/******************************************************************/


/*** tests ********************************************************/


test(mysql_db_schema, mysql_relation_schema_to_attributes) :-
   mysql_relation_schema_to_attributes(
      company:employee, Attributes),
   writeq(Attributes).

test(mysql_db_schema, mysql_relation_schema_to_xml) :-
   mysql_relation_schema_to_xml(company:employee, Xml),
   dwrite(xml, Xml).

test(mysql_db_schema, mysql_database_schema_to_xml) :-
   mysql_database_schema_to_xml(company, Xml),
   dwrite(xml, Xml).

test(mysql_db_schema, database_schema_xml_to_create_statements) :-
   mysql_database_schema_to_xml(company, Xml),
   dwrite(xml, Xml),
   database_schema_xml_to_create_statements(Xml, Statements),
   writeln_list(Statements).


/*** interface ****************************************************/


/* mysql_database_schemas(Tuples) <-
      */

mysql_database_schemas :-
   mysql_database_schemas(Tuples),
   writeln_list(Tuples).

mysql_database_schemas(Tuples) :-
   Databases = [
      company, wm_2002, selli, stock,
      morphemes, evu, 'Alignment' ],
   ddbase_aggregate(
      [Database, N, average_round_(M1), average_round_(M2)],
      ( member(Database, Databases),
        mysql_database_schemas(Database, Ts),
        mysql_database_to_number_of_relations(Database, N),
        member([_, M1, M2], Ts) ),
      Tuples ).

mysql_database_to_number_of_relations(Database, N) :-
   mysql_database_schemas(Database, self::'*', Ts),
   length(Ts, N),
   !.

average_round_(Xs, X) :-
   average(Xs, Y),
   round(Y, 2, X).


/* mysql_database_schemas(Database, Tuples) <-
      */

mysql_database_schemas(Database, Tuples) :-
   mysql_database_schemas(Database, attribute, Tuples_1),
   mysql_database_schemas(Database, foreign_key, Tuples_2),
   ( foreach([Table, X1], Tuples_1),
     foreach([Table, X1, X2], Tuples) do
        ( member([Table, X2], Tuples_2); X2 = 0 ) ),
   !.

/* mysql_database_schemas(Database, Type, Tuples) <-
      */

mysql_database_schemas(Database, Type, Tuples) :-
   mysql_database_schema_to_xml(Database, Xml),
   ddbase_aggregate( [Table_Name, length(X)],
      ( Table := Xml/table,
        Table_Name := Table@name,
        X := Table/Type ),
      Tuples ).


/* mysql_relation_schema_to_attributes(
         Connection, Database:Table, Attributes) <-
      */

mysql_relation_schema_to_attributes(
      Database:Table, Attributes) :-
   dislog_variable_get(
      odbc_connection_data_dictionary, Connection),
   mysql_relation_schema_to_attributes(
      Connection, Database:Table, Attributes).

/*
mysql_relation_schema_to_attributes(
      mysql, company:Table, Attributes ) :-
   ( Table = employee ->
     Attributes = ['FNAME', 'MINIT', 'LNAME', 'SSN', 'BDATE',
        'ADDRESS', 'SEX', 'SALARY', 'SUPERSSN', 'DNO']
   ; Table = department,
     Attributes = ['DNAME', 'DNUMBER', 'MGRSSN', 'MGRSTARTDATE']
   ; Table = project,
     Attributes = ['DNUM', 'PLOCATION', 'PNAME', 'PNUMBER'] ),
   !.
*/

mysql_relation_schema_to_attributes(
      Connection, Database:Table, Attributes) :-
   mysql_relation_schema_to_xml(Connection, Database:Table, Xml),
   findall( Attribute,
      Attribute := Xml/attribute@name,
      Attributes ).


/* mysql_relation_schema_to_xml(
         Connection, Database:Table, Xml) <-
      */

mysql_relation_schema_to_xml(Database:Table, Xml) :-
   dislog_variable_get(
      odbc_connection_data_dictionary, Connection),
   mysql_relation_schema_to_xml(
      Connection, Database:Table, Xml).

mysql_relation_schema_to_xml(Connection, Database:Table, Xml) :-
   mysql_database_schema_to_xml(Connection, Database, Xml_DB),
   Xml := Xml_DB/table::[@name=Table].
   

/* mysql_database_schema_to_xml(Database, Xml) <-
      */

mysql_database_schema_to_xml(Database, Xml) :-
   dislog_variable_get(
      odbc_connection_data_dictionary, Connection),
   mysql_database_schema_to_xml(Connection, Database, Xml).

mysql_database_schema_to_xml(Connection, Database, Xml) :-
   with_mutex_(mysql_database_schema_to_xml_mutex,
      mysql_database_schema_to_xml_(Connection, Database, Xml)).

mysql_database_schema_to_xml_(Connection, Database, Xml) :-
   mysql_database_foreign_keys_xml(Connection, Database, Xml_1),
   mysql_database_describe(Connection, Database, Ts),
   fn_item_parse(Xml_1, database:As:Tables_1),
   mysql_tables_extend_by_attributes_and_pk(
      Ts, Tables_1, Tables_2),
   Xml = database:As:Tables_2.


/* mysql_database_schema_table_to_primary_key(
         DB_Schema, Table, Key) <-
      */

mysql_database_schema_table_to_primary_key(
      DB_Schema, Table, Key) :-
   setof( A,
      A := DB_Schema/table::[@name=Table]
         /primary_key/attribute@name,
      Key ).


/* database_schema_xml_to_create_statements(
         DB_Schema, Statements) <-
      */

database_schema_xml_to_create_statements(
      DB_Schema, Statements) :-
   findall( Statement,
      ( Relation_Schema := DB_Schema/table,
        relation_schema_xml_to_create_statement(
           Relation_Schema, Statement) ),
      Statements ).


/* relation_schema_xml_to_create_statement(
         Relation_Schema, Statement) <-
      */

relation_schema_xml_to_create_statement(
      Relation_Schema, Statement) :-
   Table := Relation_Schema@name,
   findall( A,
      ( Attribute := Relation_Schema/attribute,
        [Name, Type] := Attribute-[@name, @type],
        ( 'NO' := Attribute@is_nullable ->
          NN = ' NOT NULL'
        ; NN = '' ),
        concat([Name, ' ', Type, NN], A) ),
      Attributes ),
   findall( Name,
      Name := Relation_Schema/primary_key/attribute@name,
      Key ),
   findall( Fk,
      ( I := Relation_Schema/foreign_key,
        fk_xml_to_term(I, Fk) ),
      Fks ),
   table_attributes_key_and_fks_to_create_statement(
      Table, Attributes, Key, Fks, Statement).


/*** implementation ***********************************************/


/* mysql_tables_extend_by_attributes_and_pk(
         Ts, Tables_1, Tables_2) <-
      */

mysql_tables_extend_by_attributes_and_pk(
      Ts, Tables_1, Tables_2) :-
   ddbase_aggregate( [Table, list(A)],
      ( member([Table, Attribute, YN, Type, _, Extra], Ts),
        Xs = [name:Attribute, type:Type, is_nullable:YN],
        ( Extra = auto_increment ->
          Ys = [auto_increment:'YES']
        ; Ys = [] ),
        append(Xs, Ys, As),
        A = attribute:As:[] ),
      A_Tuples ),
   ddbase_aggregate( [Table, list(A)],
      ( member([Table, Attribute, _, _, 'PRI', _], Ts),
        A = attribute:[name:Attribute]:[] ),
      P_Tuples ),
   maplist(
      mysql_table_extend_by_attributes_and_pk(
         A_Tuples, P_Tuples),
      Tables_1, Tables_2 ).

mysql_table_extend_by_attributes_and_pk(
      A_Tuples, P_Tuples, Table_1, Table_2) :-
   As := Table_1/attributes::'*',
   Name := Table_1@name,
   Fks := Table_1/foreign_keys/content::'*',
   Table := Table_1 <-> [foreign_keys],
   Es_1 := Table/content::'*',
   member([Name, Attributes], A_Tuples),
   member([Name, P_Attributes], P_Tuples),
   Pk = primary_key:P_Attributes,
   append([Attributes, Es_1, [Pk], Fks], Es_2),
   Table_2 = table:As:Es_2.


/* table_attributes_key_and_fks_to_create_statement(
         Table, Attributes, Key, Fks, Statement) <-
      */

table_attributes_key_and_fks_to_create_statement(
      Table, Attributes, Key, Fks, Statement) :-
   concat(['CREATE TABLE ', Table, ' (\n   '], A),
   concat_with_separator(Attributes, ',\n   ', B),
   concat_with_separator(Key, ', ', C),
   concat(['   primary key (', C, ')'], Pk),
   maplist( fk_term_to_string,
      Fks, Strings ),
   concat_with_separator([Pk|Strings], ',\n   ', D),
   concat([A, B, ',\n', D, '\n', ') ENGINE=InnoDB'], Statement).


/******************************************************************/


