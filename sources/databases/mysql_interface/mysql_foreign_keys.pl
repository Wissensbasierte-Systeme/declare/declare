

/******************************************************************/
/***                                                            ***/
/***          DDK:  MySQL - Foreign Keys                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* mysql_database_foreign_keys_xml(Database, Xml) <-
      */

mysql_database_foreign_keys_xml(Database, Xml) :-
   dislog_variable_get(
      odbc_connection_data_dictionary, Connection),
   mysql_database_foreign_keys_xml(Connection, Database, Xml).

mysql_database_foreign_keys_xml(Connection, Database, Xml) :-
   mysql_use_database(Connection, information_schema),
   concat([ 'select TABLE_NAME from TABLES ',
      'where TABLE_SCHEMA=''', Database, '''' ], Statement),
   with_mutex(mysql_information_schema_mutex,
      odbc_query_to_tuples(Connection, Statement, Tables)),
   ( foreach([Table], Tables), foreach(T, Ts) do
        mysql_database_table_to_create_table(
           Connection, Database:Table, Create),
        mysql_create_to_foreign_keys_xml(Create, Fs),
        T = table:[name:Table]:[Fs] ),
   Xml = database:[name:Database]:Ts.


/* mysql_create_to_foreign_keys_xml(Create, Xml) <-
      */

test(mysql_foreign_keys, mysql_database_foreign_keys_xml) :-
   concat([
      'CONSTRAINT `fk_1` ',
      'FOREIGN KEY (`a`, `b`) REFERENCES `t_1` (`c`, `d`) ',
      'ON DELETE SET NULL ON UPDATE SET DEFAULT, ',
      '*** text ***',
      'CONSTRAINT `fk_2` ',
      'FOREIGN KEY (`e`, `f`) REFERENCES `t_2` (`g`, `h`) ',
      'ON DELETE CASCADE ON UPDATE NO ACTION, '],
      Statements),
   mysql_create_to_foreign_keys_xml(Statements, Xml),
   dwrite(xml, Xml).

mysql_create_to_foreign_keys_xml(Create, Xml) :-
   atom_chars(Create, Cs),
   mysql_foreign_keys(Fks, Cs, _),
   Xml = foreign_keys:Fks.


/*** implementation ***********************************************/


mysql_foreign_keys(Keys) -->
   ( mysql_foreign_key(K), [','],
     mysql_foreign_keys(Ks),
     { Keys = [K|Ks] }
   ; mysql_foreign_key(K),
     { Keys = [K] }
   ; mysql_token(_, _),
     mysql_foreign_keys(Ks),
     { Keys = Ks }
   ; { Keys = [] } ).


mysql_foreign_key(foreign_key:Es) -->
   mysql_spaces(_),
   ['F', 'O', 'R', 'E', 'I', 'G', 'N', ' ', 'K', 'E', 'Y'],
   mysql_spaces(_),
   mysql_attribute_list(Attributes),
   mysql_spaces(_),
   ['R', 'E', 'F', 'E', 'R', 'E', 'N', 'C', 'E', 'S'],
   mysql_spaces(_),
   mysql_foreign_key_target(Target),
   mysql_foreign_key_actions(Actions),
   mysql_spaces(_),
   { append([Attributes, Target, Actions], Es) }.

mysql_foreign_key_target(
      [references:[table:Name]:Attributes]) -->
   ['`'], mysql_name(Name), ['`'],
   mysql_spaces(_),
   mysql_attribute_list(Attributes).


mysql_foreign_key_actions([Action|Actions]) -->
   mysql_foreign_key_action(Action),
   mysql_foreign_key_actions(Actions).
mysql_foreign_key_actions([]) -->
   { true }.

mysql_foreign_key_action(Action) -->
   mysql_spaces(_),
   { member(X, ['ON DELETE', 'ON UPDATE']),
     atom_chars(X, Cs) },
   Cs,
   mysql_spaces(_),
   { member(Y, ['CASCADE', 'SET NULL', 'SET DEFAULT',
        'RESTRICT', 'NO ACTION']),
     atom_chars(Y, Ds) },
   Ds,
   { Action = trigger:[event:X, action:Y]:[] }.


mysql_attribute_list([A|As]) -->
   mysql_token('(', _),
   mysql_attribute(A), sequence(*, mysql_attribute_, As),
   mysql_token(')', _).


mysql_attribute_(A) -->
   [','],
   mysql_attribute(A).

mysql_attribute(attribute:[name:Name]:[]) -->
   mysql_spaces(_),
   ['`'], mysql_name(Name), ['`'].

mysql_name(Name) -->
   sequence(+, mysql_character, Cs),
   { concat(Cs, Name) }.

mysql_character(C) -->
   [C],
   { char_code(C, N),
     ( between(97, 122, N)
     ; between(65, 90, N)
     ; between(48, 57, N)
     ; N = 95 ) }.

mysql_spaces([]) -->
   ( [' '], mysql_spaces(_)
   ; [] ).

mysql_token(X, []) -->
   [X].


/******************************************************************/


