

/******************************************************************/
/***                                                            ***/
/***          DDK:  MySQL Database Schema Analysis              ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(mysql, mysql_database_schema_to_fk_edges) :-
   Database = company,
   mysql_database_schema_to_fk_edges(Database, Edges),
   edges_to_picture_bfs(Edges).


/*** interface ****************************************************/


/* mysql_database_schema_to_fk_edges(Database, Edges) <-
      */

mysql_database_schema_to_fk_edges(Database) :-
   mysql_database_schema_to_fk_edges(Database, Edges),
   edges_to_picture(Edges).

mysql_database_schema_to_fk_edges(Database, Edges) :-
   mysql_database_schema_to_xml(Database, Xml),
   findall( Edge,
      ( Table := Xml/table,
        mysql_table_schema_to_foreign_key(Table, _, Edge) ),
      Edges ).

mysql_table_schema_to_foreign_key(Table, Fk, W-V) :-
   Fk := Table/foreign_key,
   mysql_foreign_key_attributes_are_not_null(Table, Fk),
   V := Table@name,
   W := Fk/references@table.

mysql_foreign_key_attributes_are_not_null(Table, Fk) :-
   \+ ( A := Fk/attribute@name,
      A := Table/attribute::[@is_nullable='YES']@name ).


/* mysql_database_schema_to_tree_edges(
         Database, Root, Tree_Edges) <-
      */

mysql_database_schema_to_tree_edges(Database, Root) :-
   mysql_database_schema_to_tree_edges(
      Database, Root, Es),
   edges_to_vertices(Es, Vs),
   vertices_edges_to_picture_bfs([Root], Vs, Es, _).

mysql_database_schema_to_tree_edges(
      Database, Root, Tree_Edges) :-
   mysql_database_schema_to_fk_edges(Database, Edges),
   tree_traversals:traverse(bfs, [Root], Edges, Tree_Edges).


/******************************************************************/


