

/******************************************************************/
/***                                                            ***/
/***          DDK:  MySQL to XML                                ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(mysql, mysql_database_to_xml) :-
   Connection = mysql,
   Database = company,
   mysql_database_to_xml(Connection, Database, Xml),
   dwrite(xml, Xml).
test(mysql, mysql_database_to_fn_item) :-
   Connection = mysql,
   Database = company,
   Root = department,
   mysql_database_schema_to_tree_edges(Database, Root, Edges),
   edges_to_picture_bfs(Edges),
   mysql_database_to_fn_item(Connection, Database, Root, Item),
   dwrite(xml, Item).


/*** interface ****************************************************/


/* mysql_database_to_xml(Connection, Database, Xml) <-
      */

mysql_database_to_xml(Connection, Database, Xml) :-
   mysql_database_to_tables(Database, Tables),
   ( foreach(Table, Tables), foreach(Item, Items) do
        mysql_database_table_to_xml(
           Connection, Database:Table, Item) ),
   Xml = database:[name:Database]:Items.


/* mysql_database_table_to_xml(
         Connection, Database:Table, Xml) <-
      */

mysql_database_table_to_xml(
      Connection, Database:Table, Xml) :-
   mysql_database_table_describe(Database:Table, Tuples_1),
   maplist( nth(1),
      Tuples_1, Attributes ),
   mysql_table_select(Connection, Database:Table, Tuples_2),
   mysql_tuples_to_xml(Table, Attributes, Tuples_2, Xml).


/* mysql_tuples_to_xml(Table, Attributes, Tuples, Xml) <-
      */

mysql_tuples_to_xml(Table, Attributes, Tuples, Xml) :-
   ( foreach(Tuple, Tuples), foreach(Item, Items) do
        pair_lists(:, Attributes, Tuple, Bs),
        fn_association_list_delete_null_values(Bs, As),
        Item = row:As:[] ),
   Xml = table:[name:Table]:Items.


/* mysql_database_table_to_fn_items(
         Connection, Database:Table, Items) <-
      */

mysql_database_table_to_fn_items(
      Connection, Database:Table, Items) :-
   mysql_database_table_describe(Database:Table, Tuples_1),
   maplist( nth(1),
      Tuples_1, Attributes ),
   mysql_table_select(Connection, Database:Table, Tuples_2),
   ( foreach(Tuple, Tuples_2), foreach(Item, Items) do
        pair_lists(:, Attributes, Tuple, As_2),
        fn_association_list_delete_null_values(As_2, As),
        Item = Table:As:[] ).

fn_association_list_delete_null_values(As_1, As_2) :-
   findall( A:V,
      ( member(A:V, As_1),
        V \= '$null$' ),
      As_2 ).


/* mysql_database_to_fn_item(Connection, Database, Root, Item) <-
      */

mysql_database_to_fn_item(Connection, Database, Root) :-
   mysql_database_to_fn_item(Connection, Database, Root, Item),
   dwrite(xml, Item).

mysql_database_to_fn_item(Connection, Database, Root, Item) :-
   mysql_database_schema_to_tree_edges(Database, Root, Edges),
   mysql_database_tree_to_fn_item(
      Connection, Database, Edges, Item).


/* mysql_database_tree_to_fn_item(
         Connection, Database, Edges, Item) <-
      */

mysql_database_tree_to_fn_item(
      Connection, Database, Edges, Item) :-
   mysql_database_schema_to_xml(Database, Database_Schema),
   mysql_edges_to_foreign_keys(Database_Schema, Edges, Fks),
   !,
   dag_edges_to_root(Edges, Root),
   mysql_database_to_xml_hierarchy(
      Connection, Database, Fks, Root, Item).

dag_edges_to_root(Edges, Root) :-
   edges_to_ugraph(Edges, Graph),
   ugraphs:top_sort(Graph, [Root|_]).


/* mysql_edges_to_foreign_keys(Database_Schema, Edges, Fks) <-
      */

mysql_edges_to_foreign_keys(Database_Schema, Edges, Fks) :-
   maplist( mysql_edge_to_foreign_key(Database_Schema),
      Edges, Fks ).

mysql_edge_to_foreign_key(Database_Schema, V-W, Fk) :-
   T := Database_Schema/table::[@name=W],
   F := T/foreign_key,
   mysql_foreign_key_attributes_are_not_null(T, F),
   V := F/references@table,
   Fk := F*[@table:W].


/* mysql_database_to_xml_hierarchy(
         Connection, Database, Fks, Table, Item) <-
      */

mysql_database_to_xml_hierarchy(
      Connection, Database, Fks, Table, Xml) :-
   mysql_database_to_xml(Connection, Database, DB),
   findall( Item,
      ( Row := DB/table::[@name=Table]/row,
        xml_database_row_to_xml_hierarchy(
           DB, Fks, Table, Row, Item) ),
      Items ),
   Xml = Database:Items.

xml_database_row_to_xml_hierarchy(Xml, Fks, Table, Row, Item) :-
   findall( Item_2,
      ( member(Fk, Fks),
        Table := Fk/references@table,
        Table_2 := Fk@table,
        Row_2 := Xml/table::[@name=Table_2]/row,
        row_foreign_key_references_row(Fk, Row_2, Row),
        xml_database_row_to_xml_hierarchy(
           Xml, Fks, Table_2, Row_2, Item_2) ),
      Items_2 ),
   fn_item_parse(Row, row:As:[]),
   Item = Table:As:Items_2,
   !.


/* row_foreign_key_references_row(Fk, Row_1, Row_2) <-
      */

row_foreign_key_references_row(Fk, Row_1, Row_2) :-
   findall( V_1,
      ( A := Fk/attribute@name,
        V_1 := Row_1@A ),
      Vs_1 ),
   findall( V_2,
      ( A := Fk/references/attribute@name,
        V_2 := Row_2@A ),
      Vs_2 ),
   Vs_2 = Vs_1.


/* fn_item_includes_primary_key(DB_Schema, Item) <-
      */

fn_item_includes_primary_key(DB_Schema, Item) :-
   Table := Item/tag::'*',
   mysql_database_schema_table_to_primary_key(
      DB_Schema, Table, Key),
   setof( A,
      _ := Item@A,
      As ),
   ord_subset(Key, As).


/******************************************************************/


