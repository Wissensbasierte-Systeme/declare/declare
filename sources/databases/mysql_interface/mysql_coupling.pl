

/******************************************************************/
/***                                                            ***/
/***         DDK:  Coupling of MySQL and Prolog                 ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      mysql_table/2.


/*** interface ****************************************************/


/* mysql_table_to_prolog_module(Database:Table, Module) <-
      */

mysql_table_to_prolog_module(Database:Table, Module) :-
   mysql_table_to_prolog_module(Database:Table, Module, _).

mysql_table_to_prolog_module(Database:Table, Module, Rows) :-
   mysql_table_to_rows(Database:Table, Rows),
   ( foreach(Row, Rows) do
        Atom =.. [Table|Row],
        Module:assert(Atom) ),
   !.

   
/* mysql_call(Database, Atom) <-
      */

mysql_call(Database, Atom) :-
   Atom =.. [Table|Xs],
   mysql_table(Database:Table, Rows),
   !,
   member(Xs, Rows).
mysql_call(Database, Atom) :-
   Atom =.. [Table|Xs],
   mysql_table_to_rows(Database:Table, Rows),
   assert(mysql_table(Database:Table, Rows)),
   !,
   member(Xs, Rows).


/* mysql_odbc_declare_predicate(Database:Goal) <-
      */

mysql_odbc_declare_predicate(Database:Goal) :-
   Goal =.. [Predicate|Attributes],
   length(Attributes, Arity),
   mysql_odbc_cache_predicate(Database:Goal),
   n_free_variables(Arity, Variables),
   Atom =.. [Predicate|Variables],
   Body = mysql_odbc_cache:Atom,
   assert(Atom :- Body).


/* mysql_odbc_delete_predicate(Predicate/Arity) <-
      */

mysql_odbc_delete_predicate(Predicate/Arity) :-
   mysql_odbc_delete_predicate(Predicate/Arity, _).

mysql_odbc_delete_predicate(Predicate/Arity, Facts) :-
   n_free_variables(Arity, Variables),
   A =.. [Predicate|Variables],
   findall( A,
      retract(mysql_odbc_cache:A),
      Facts ),
   retract_rules(Predicate/Arity).


/*** implementation ***********************************************/


/* mysql_table_to_rows(Database:Table, Rows) <-
      */

mysql_table_to_rows(Database:Table, Rows) :-
%  downcase_atom(Table, T),
   T = Table,
   Statement = [ use:[Database], select:[*], from:[T] ],
   ( mysql_select_execute_odbc(Statement, Rows)
   ; mysql_select_execute(Statement, Rows) ).


/* mysql_odbc_cache_predicate(Database:Goal) <-
      */

mysql_odbc_cache_predicate(Database:Goal) :-
   Goal =.. [Predicate|Attributes],
   Sql_Command = [
      use:[ Database ],
      select:Attributes,
      from:[ Predicate ] ],
   mysql_select_execute_odbc(Sql_Command, Tuples),
   ( foreach(Tuple, Tuples) do
        ( Atom =.. [Predicate|Tuple],
          assert(mysql_odbc_cache:Atom) ) ).


/*** tests ********************************************************/


test(mysql_odbc, 4) :-
   mysql_odbc_delete_predicate(employee/2),
   mysql_odbc_delete_predicate(employee/4),
   mysql_odbc_declare_predicate(
      company:employee({{lname}}, ssn, {{bdate}}, salary)),
   mysql_odbc_declare_predicate(
      company:employee(ssn, superssn)),
   findall( [Name_1, Bdate_1, Sal_1, Name_2, Bdate_2, Sal_2],
      ( employee(SSN_1, SSN_2),
        nonvar(SSN_1), nonvar(SSN_2),
        employee(Name_1, SSN_1, Bdate_1, Sal_1),
        employee(Name_2, SSN_2, Bdate_2, Sal_2) ),
      Tuples ),
   writeln_list(Tuples).


/******************************************************************/


