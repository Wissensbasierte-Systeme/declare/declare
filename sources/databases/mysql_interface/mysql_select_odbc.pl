

/******************************************************************/
/***                                                            ***/
/***          DDK:  MySQL Select ODBC                           ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(mysql_odbc, 1) :-
   ( odbc_current_connection(
        odbc_connection_data_dictionary, mysql),
     odbc_current_connection(mysql, mysql) -> true
   ; writeln(user, 'Declare: mysql_odbc_connections_init'),
     declare_mysql_odbc_connections_init ).
%  mysql_odbc_connect.

test(mysql_odbc, 2) :-
   Sql_Command = [
      use:[ company ],
      select:[ {{e^lname}}, {{p^pname}} ],
      from:[ employee-e, works_on-w, project-p ],
      where:[ e^ssn = w^essn and w^pno = p^pnumber ] ],
   mysql_select_execute_odbc(Sql_Command, Tuples),
   writeln_list(Tuples).

test(mysql_odbc, 3) :-
   Sql_Command = [
      use:[ stock ],
      select:[ f^wkn, f^value, {{f^date}} ],
      from:[ finanztreff-f ],
      where:[ wkn = 982570 and date >= {'2002-10-28'} ] ],
%     where:[ wkn = 982570 and date >= '2002-10-28' ] ],
   mysql_select_execute_odbc(Sql_Command, Tuples),
   writeln_list(Tuples),
   Attributes = ['WKN', 'VALUE', 'DATE'],
   ( dislog_variable_get(ddbase_show_tables_xpce, yes) ->
     xpce_display_table(stock, Attributes, Tuples)
   ; true ).

test(mysql_odbc, 5) :-
   Sql_Command = [
      use:[ company ],
      select:[ {{fname}}, {{minit}}, {{lname}},
         {{ssn}}, {{bdate}}, {{address}}, {{sex}}, {{salary}},
         {{superssn}}, {{dno}} ],
      from:[ employee ],
      where:[ ] ],
   mysql_select_execute_odbc(Sql_Command, Tuples),
   writeln_list(Tuples),
   writeq(Tuples),
   Attributes = ['FNAME', 'MINIT', 'LNAME', 'SSN', 'BDATE',
      'ADDRESS', 'SEX', 'SALARY', 'SUPERSSN', 'DNO'],
   ( dislog_variable_get(ddbase_show_tables_xpce, yes) ->
     xpce_display_table(employee, Attributes, Tuples)
   ; true ).


test(mysql_odbc, 6) :-
   Connection = mysql,
   mysql_use_database(Connection, company),
   Statement = 'select LNAME, SSN, PNO, HOURS
      from employee, works_on
      where SSN = ESSN',
   odbc_query_to_tuples(Connection, Statement, Tuples),
   Attributes = ['LNAME', 'SSN', 'PNO', 'HOURS'],
   ( dislog_variable_get(ddbase_show_tables_xpce, yes) ->
     xpce_display_table(works_on, Attributes, Tuples)
   ; true ).

test(mysql_odbc, 7) :-
   Connection = mysql,
   mysql_use_database(Connection, company),
   Statement = 'select LNAME, SSN, sum(HOURS)
      from employee, works_on
      where SSN = ESSN
      group by LNAME, SSN
      order by SSN',
   odbc_query_to_tuples(Connection, Statement, Tuples),
   Attributes = ['LNAME', 'SSN', 'HOURS'],
   ( dislog_variable_get(ddbase_show_tables_xpce, yes) ->
     xpce_display_table(works_on, Attributes, Tuples)
   ; true ).


/*** interface ****************************************************/


/* mysql_select_execute_odbc(Connection, Sql_Command, Tuples) <-
      */

mysql_select_execute_odbc(Sql_Command, Tuples) :-
   mysql_select_execute_odbc(mysql, Sql_Command, Tuples).

mysql_select_execute_odbc(Connection, Sql_Command, Tuples) :-
   mysql_select_command_to_types(Sql_Command, Types),
   mysql_select_execute_odbc(
      Connection, Sql_Command, Types, Tuples).

mysql_select_execute_odbc(Connection, Sql_Command, Types, Tuples) :-
   mysql_select_command_to_name_1(Sql_Command, Use_Command),
   odbc_query(Connection, Use_Command),
   mysql_select_command_to_name_2(Sql_Command, Statement),
   odbc_query_to_tuples(Connection, Statement, Types, Rows),
   mysql_select_command_to_conversion(Sql_Command, Conversion),
   mysql_result_rows_convert(Conversion, Rows, Tuples),
   !.


/*** implementation ***********************************************/


/* mysql_select_command_to_name_1(Sql_Command, Use_Command) <-
      */

mysql_select_command_to_name_1(Sql_Command, Use_Command) :-
   [Use] := Sql_Command^use,
   concat(['use ', Use, ';'], Use_Command).


/* mysql_select_command_to_name_2(Sql_Command, Select_Command) <-
      */

mysql_select_command_to_name_2(Sql_Command, Select_Command) :-
   Select := Sql_Command^select,
   From := Sql_Command^from,
   mysql_items_to_name(', ', Select, Select_2),
   mysql_items_to_name(', ', From, From_2),
   ( ( [Where] := Sql_Command^where,
       mysql_boolean_to_name(Where,Where_1),
       name_append('where ',Where_1,Where_2) )
   ; Where_2 = '' ),
   ( ( Group_By := Sql_Command^group_by,
       mysql_items_to_name(', ',Group_By,Group_By_1),
       name_append('group by ',Group_By_1,Group_By_2) )
   ; Group_By_2 = '' ),
   ( ( Order_By := Sql_Command^order_by,
       mysql_items_to_name(Order_By,Order_By_1),
       name_append('order by ',Order_By_1,Order_By_2) )
   ; Order_By_2 = '' ),
   concat([
      'select ', Select_2, ' ',
      'from ', From_2, ' ',
      Where_2, ' ', Group_By_2, ' ', Order_By_2, ';' ],
      Select_Command).


/* mysql_select_command_to_types(Sql_Command, Types) <-
      */

mysql_select_command_to_types(Sql_Command, Types) :-
   mysql_select_command_to_types_(Sql_Command, Types),
   writeln(Types).

mysql_select_command_to_types_(Sql_Command, []) :-
   [*] := Sql_Command^select,
   !.
mysql_select_command_to_types_(Sql_Command, [types(As)]) :-
   Select := Sql_Command^select,
   length(Select, N),
   multify(atom, N, As).


/* mysql_boolean_to_name(Boolean,Name) <-
      */

mysql_boolean_to_name(true, true) :-
   !.
mysql_boolean_to_name(X and Y,C) :-
   ( X = true, Z = Y
   ; Y = true, Z = X ),
   !,
   mysql_boolean_to_name(Z, C).
mysql_boolean_to_name(X and Y, C) :-
   mysql_boolean_to_name(X, A),
   mysql_boolean_to_name(Y, B),
   concat([A,' and ',B], C).
mysql_boolean_to_name((X or Y), C) :-
   mysql_boolean_to_name(X, A),
   mysql_boolean_to_name(Y, B),
   concat([' ( ',A,' or ',B,' ) '], C).
mysql_boolean_to_name(A=B, C) :-
   mysql_item_to_name(A, A1),
   mysql_item_to_name(B, B1),
   concat([A1,' = ',B1], C).
mysql_boolean_to_name(A like B, C) :-
   mysql_item_to_name(A, A1),
   mysql_item_to_name(B, B1),
   concat([A1,' like ',B1], C).
mysql_boolean_to_name(A>=B, C) :-
   mysql_item_to_name(A, A1),
   mysql_item_to_name(B, B1),
   concat([A1,' >= ',B1], C).
mysql_boolean_to_name(A=<B,C) :-
   mysql_item_to_name(A, A1),
   mysql_item_to_name(B, B1),
   concat([A1,' <= ',B1], C).
mysql_boolean_to_name(A\=B, C) :-
   mysql_item_to_name(A, A1),
   mysql_item_to_name(B, B1),
   concat([A1,' <> ',B1], C).
mysql_boolean_to_name(A<B, C) :-
   mysql_item_to_name(A, A1),
   mysql_item_to_name(B, B1),
   concat([A1,' < ',B1], C).
mysql_boolean_to_name(A>B, C) :-
   mysql_item_to_name(A, A1),
   mysql_item_to_name(B, B1),
   concat([A1,' > ',B1], C).


/* mysql_items_to_name(Separator,Items,Name) <-
      */

mysql_items_to_name(Separator,[X1,X2|Xs],C) :-
   mysql_item_to_name(X1,C1),
   mysql_items_to_name(Separator,[X2|Xs],C2),
   concat([C1,Separator,C2],C).
mysql_items_to_name(_,[X1],C) :-
   mysql_item_to_name(X1,C).
mysql_items_to_name(_,[],'').

mysql_item_to_name(T,C) :-
   T =.. [F,X],
   member(F,[min,max,avg,sum,count]),
   !,
   mysql_item_to_name(X,A),
   concat([F,'(',A,')'],C).
mysql_item_to_name(T,C) :-
   T =.. [F,X,Y],
   member(F,[*,/]),
   !,
   mysql_item_to_name(X,A),
   mysql_item_to_name(Y,B),
   concat([A,F,B],C).
mysql_item_to_name({{Item}},C) :-
   !,
   mysql_item_to_name(Item,A),
   concat([A],C).
mysql_item_to_name({Item},C) :-
   !,
   mysql_item_to_name(Item,A),
   concat(['''',A,''''],C).
mysql_item_to_name(O-Item,C) :-
   !,
   concat([O,' ',Item],C).
mysql_item_to_name(O^Item,C) :-
   !,
   concat([O,'.',Item],C).

mysql_item_to_name(Item,Item).


/* mysql_select_command_to_conversion(Sql_Command, Conversion) <-
      */

mysql_select_command_to_conversion(Sql_Command, Conversion) :-
   Select := Sql_Command^select,
   maplist( mysql_select_to_conversion,
      Select, Conversion ).

mysql_select_to_conversion({{_}},no) :-
   !.
mysql_select_to_conversion(_,yes).


/* mysql_result_rows_convert(Conversion, Rows_1, Rows_2) <-
      */

mysql_result_rows_convert(Conversion, Rows_1, Rows_2) :-
   maplist( mysql_result_row_convert(Conversion),
      Rows_1, Rows_2 ).

mysql_result_row_convert(Conversion, Row_1, Row_2) :-
   ( pair_lists(Conversion, Row_1, Pairs)
   ; length(Row_1, N),
     multify(no, N, Conversion_2),
     pair_lists(Conversion_2, Row_1, Pairs) ),
   maplist( mysql_result_entry_convert,
      Pairs, Row_2 ),
   !.

mysql_result_entry_convert([yes, '$null$'], _) :-
   !.
% mysql_result_entry_convert([yes, X], X) :-
%    !.
mysql_result_entry_convert([yes, X], Y) :-
   ( term_to_atom(Y, X),
     nonvar(Y) ->
     true
   ; Y = X ).
mysql_result_entry_convert([no, X], X).


/******************************************************************/


