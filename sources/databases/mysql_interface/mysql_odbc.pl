

/******************************************************************/
/***                                                            ***/
/***          DDK:  MySQL ODBC Interface                        ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(mysql, odbc_query_to_xml) :-
   Connection = mysql,
   mysql_use_database(Connection, company),
   Statement = 'select employee.LNAME, PNO, sum(HOURS)
      from employee, works_on
      where SSN = ESSN
      group by LNAME, PNO',
   odbc_query_to_xml(Connection, Statement, Xml),
   dwrite(xml, Xml).

test(mysql, odbc_query_to_xpce_table) :-
   Connection = mysql,
   mysql_use_database(Connection, company),
   Statement = 'select employee.LNAME, PNO, sum(HOURS)
      from employee, works_on
      where SSN = ESSN
      group by LNAME, PNO',
   odbc_query_to_tuples(Connection, Statement, Tuples),
   Attributes = ['LNAME', 'PNO', 'HOURS'],
   xpce_display_table(employee, Attributes, Tuples).


/*** interface ****************************************************/


/* mysql_odbc_connections(User, Password,
         Database_Connection, Dictionary_Connection) <-
      */

mysql_odbc_connections(User, Password,
      Database_Connection, Dictionary_Connection) :-
   DSN = mysql,
   Open_Mode = multiple,
   mysql_odbc_connections(DSN, User, Password,
      Database_Connection, Dictionary_Connection, Open_Mode).

mysql_odbc_connections(DSN, User, Password,
      Database_Connection, Dictionary_Connection, Open_Mode) :-
   mysql_odbc_connect(
      DSN, User, Password, Database_Connection, Open_Mode),
   mysql_odbc_connect(
      DSN, User, Password, Dictionary_Connection, Open_Mode),
   dislog_variable_set(
      odbc_connection_data_dictionary, Dictionary_Connection).


/* mysql_odbc_connect(User, Password, Connection) <-
      */

mysql_odbc_connect :-
   getenv('USER', User),
   ( Password = ''
   ; readln([Password]) ),
   mysql_odbc_connect(User, Password, mysql).

mysql_odbc_connect(User, Password, Connection) :-
   mysql_odbc_connect(
      mysql, User, Password, Connection, multiple).


/* mysql_odbc_connect(
         DSN, User, Password, Connection, Open_Mode) <-
      */

mysql_odbc_connect(
      DSN, User, Password, Connection, Open_Mode) :-
   odbc_connect(DSN, _, [
      user(User), password(Password),
      alias(Connection), open(Open_Mode) ]).


/* mysql_odbc_current_connection <-
      */

mysql_odbc_current_connection :-
   findall( [Connection, DSN], 
      odbc_current_connection(Connection, DSN),
      Tuples ),
   xpce_display_table(['Connection', 'DSN'], Tuples),
   !.


/* mysql_use_database(Connection, Database) <-
      */

mysql_use_database(Database) :-
   mysql_use_database(mysql, Database).

mysql_use_database(_, Database) :-
   var(Database),
   !.
mysql_use_database(Connection, Database) :-
   odbc_connection_refresh(Connection),
   concat('use ', Database, Statement),
   ddk_spy(writeln(Statement)),
   odbc_query(Connection, Statement, Row, []),
   ddk_spy(write_list(['--> ', Row, '\n'])).


/* odbc_query_to_tuples(Connection, Statement, Types, Tuples) <-
      */

odbc_query_to_tuples(Connection, Statement, Tuples) :-
   odbc_query_to_tuples(Connection, Statement, [], Tuples).

odbc_query_to_tuples(Connection, Statement, Types, Tuples) :-
   ddk_spy(writeln(Statement)),
   findall( Tuple,
      ( odbc_query(Connection, Statement, Row, Types),
        Row =.. [row|Tuple] ),
      Tuples ).


/* mysql_execute_statements(Connection, Database, Statements) <-
      */

mysql_execute_statements(Connection, Database, Statements) :-
   mysql_use_database(Connection, Database),
   checklist( mysql_execute_statement(Connection),
      Statements ).

mysql_execute_statement(Connection, Database, Statement) :-
   mysql_use_database(Connection, Database),
   mysql_execute_statement(Connection, Statement).

mysql_execute_statement(Connection, Statement) :-
   ddk_spy(writeln(Statement)),
   odbc_query(Connection, Statement, Row, []),
   ddk_spy(write_list(['--> ', Row, '\n'])).


/* mysql_table_select(
         Connection, Database:Table, Pairs_Where, Tuples) <-
      */

mysql_table_select(Database:Table, Tuples) :-
   mysql_table_select(mysql, Database:Table, Tuples).

mysql_table_select(Connection, Database:Table, Tuples) :-
   mysql_table_select(Connection, Database:Table, [], Tuples).

mysql_table_select(
      Connection, Database:Table, Pairs_Where, Tuples) :-
   mysql_use_database(Connection, Database),
   mysql_pairs_where_to_where_condition(Pairs_Where, Condition),
   concat(['select * from ', Table, Condition], Statement),
   odbc_query_to_tuples(Connection, Statement, Tuples).

mysql_pairs_where_to_where_condition(Pairs_Where, Condition) :-
   ( Pairs_Where = [] ->
     Condition = ''
   ; is_list(Pairs_Where) ->
     attribute_value_pairs_to_string(Pairs_Where, ' and', Where),
     concat(' where', Where, Condition)
   ; sql_attribute_value_condition_to_string(Pairs_Where, Where),
     concat(' where ', Where, Condition) ).

sql_attribute_value_condition_to_string(A:V, String) :-
   concat([A, '= "', V, '"'], String).
sql_attribute_value_condition_to_string((C1;C2), String) :-
   sql_attribute_value_condition_to_string(C1, S1),
   sql_attribute_value_condition_to_string(C2, S2),
   concat(['(', S1, ' or ', S2, ')'], String).
sql_attribute_value_condition_to_string((C1,C2), String) :-
   sql_attribute_value_condition_to_string(C1, S1),
   sql_attribute_value_condition_to_string(C2, S2),
   concat([S1, ' and ', S2], String).


/* odbc_query_to_xml(Connection, Statement, Xml) <-
      */

odbc_query_to_xml(Connection, Statement, Xml) :-
   odbc_query_to_fn_items(Connection, Statement, Items),
   name_exchange_sublist([["\n", " "]], Statement, Name),
   Xml = table:[name:Name]:Items.


/* odbc_query_to_fn_items(Connection, Statement, Items) <-
      */

odbc_query_to_fn_items(Connection, Statement, Items) :-
   findall( Item,
      ( odbc_query(Connection, Statement, Row, [source(true)]),
        Row =.. [row|Cs],
        ( foreach(C, Cs), foreach(Pair, Pairs) do
             C = column(Table, Column, Value),
             ( Table = '' ->
               T = agg
             ; T = Table ),
             concat([T, ':', Column], A),
             Pair = A:Value ),
        Item = row:Pairs:[] ),
      Items ).


/******************************************************************/


