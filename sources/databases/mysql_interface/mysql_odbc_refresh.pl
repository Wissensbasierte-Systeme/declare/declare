

/******************************************************************/
/***                                                            ***/
/***          DDK:  MySQL ODBC Interface - Refresh              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* odbc_connection_refresh(Connection) <-
      */

odbc_connection_refresh(Connection) :-
   concat('use ', information_schema, Statement),
   Test = odbc_query(Connection, Statement, _, []),
   ( catch(Test, _, fail) ->
     true
   ; odbc_disconnect(Connection),
     mysql_odbc_connect_xpce(Connection) ).


/* mysql_odbc_connect_xpce(Connection) <-
      */

mysql_odbc_connect_xpce(Connection) :-
   new(Dialog, dialog('DB Login')),
   send(Dialog, background, lightsteelblue3),
   send_list(Dialog, append, [
      new(N1, text_item('DSN')),
      new(N2, text_item('User')),
      new(N3, text_item('Password')),
      button(cancel, message(Dialog, destroy)),
      button(login, message(@prolog, mysql_odbc_connect_xpce,
         N1?selection, N2?selection, N3?selection,
         Connection, Dialog ) ) ]),
   send(Dialog, default_button, login),
   send(Dialog, open).

mysql_odbc_connect_xpce(DSN, User, PW, Connection, Dialog) :-
   ( mysql_odbc_connect(
        DSN, User, PW, Connection, multiple) ->
     send(Dialog, destroy),
     ddk_message(['User:', User, ' connected with DSN:', DSN])
   ; ddk_message(['Error ! \nUser:',
        User, ' could not connect with DSN:', DSN]) ).


/******************************************************************/


