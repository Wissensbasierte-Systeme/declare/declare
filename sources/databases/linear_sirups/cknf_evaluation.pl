

/******************************************************************/
/***                                                            ***/
/***         Declare:  CKNF-Evaluation                          ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous cknf/3, cknf/4.


/*** interface ****************************************************/


cknf_evaluation(Goal) :-
   Goal =.. [cknf,Mode,I|Arguments],
   test_sirup(I,Rule),
   pp_slf_rule(Rule),
   Goal_2 =.. [cknf,Mode,Rule|Arguments],
   call(Goal_2).

cknf(iterate,Rule,Iterated_Rule) :-
   slf_rule_iterate(Rule,Iterated_Rule).

cknf(iterate,Rule,L,Iterated_Rule) :-
   slf_rule_iterate(Rule,L,Iterated_Rule).

cknf(sg,Rule,S_Edges) :-
   slf_rule_to_s_edges(Rule,S_Edges).

cknf(ssg,Rule,S_Star_Edges) :-
   slf_rule_to_s_star_edges(Rule,S_Star_Edges).

cknf(asg,Rule,AS_Edges) :-
   slf_rule_to_as_edges(Rule,AS_Edges).

cknf(fasg,Rule,AS_Edges,Folded_AS_Edges) :-
   slf_rule_to_as_edges(Rule,AS_Edges),
   as_edges_to_folded_as_edges(AS_Edges,Folded_AS_Edges).

cknf(kl,Rule,K,L) :-
   slf_rule_to_sidedness(Rule,K),
   slf_rule_to_period(Rule,L).

cknf(cknf,Rule,Recursive_Rule,Exit_Rules) :-
   slf_rule_to_knf_sirup(Rule,Recursive_Rule,Exit_Rules).


/******************************************************************/


