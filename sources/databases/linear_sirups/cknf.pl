

/******************************************************************/
/***                                                            ***/
/***         Declare:  KNF for SLF-Sirups                       ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(linear_sirups, linear_sirup_to_knf) :-
%  File = 'examples/linear_sirups/sirup_2',
%  File = 'examples/linear_sirups/sirup_1_var',
%  File = 'examples/linear_sirups/sirup_3_var',
   File = 'examples/linear_sirups/sirup_4_var',
%  dislog_consult(File, Program_1),
   cknf_read_program(File, Program_1),
   linear_sirup_to_knf(Program_1, Program_2),
   dportray(lp_window, Program_2),
   dportray(lp, Program_2),
   linear_sirup_to_as_graph_edges_to_picture(Program_1).


/*** interface ****************************************************/


/* linear_sirup_to_knf(Program_1, Program_2) <-
      */

linear_sirup_to_knf(Program_1, Program_2) :-
   star_line,
   writeln(user, 'Linear Sirup to KNF'),
   linear_sirup_to_knf_(Program_1, Program_3),
%  dportray(lp_window, Program_3),
   superscript_program_to_program(Program_3, Program_2),
   star_line.

linear_sirup_to_knf_(Program_1, Program_2) :-
   slf_sirup_program_split(Program_1, Recursive, Exit, Rest),
   !,
   dislog_rule_format_to_slf(Recursive, Recursive_2),
   slf_rule_to_knf_sirup(Recursive_2, Recursive_3, Helper),
   slf_rule_to_sidedness(Recursive_2, K),
   write_list(user, [' * ', sidedness, ' = ', K, '\n']),
   !,
   slf_rule_to_period_nc_path(Recursive_2, Period),
   write_list(user, [' * ', period, ' = ', Period, '\n']),
   L is Period - 1,
   slf_rule_expansion(Recursive_2, L, Expansion_1),
   !,
   M is Period + 1,
   dislog_rule_format_to_slf(Exit, Exit_2),
   superscribe_rule(Exit_2, M, Exit_3),
   ( foreach(H-G1-As, Expansion_1),
     foreach(H-G2-As, Expansion_2) do
        slf_rule_expand_goal(G1, Exit_3, G2) ),
   append([Recursive_3|Helper], Expansion_2, Program_A),
   maplist( slf_rule_format_to_dislog,
      Program_A, Program_B ),
   append(Program_B, Rest, Program_2).


/* slf_rule_expansion(Rule, Rules) <-
      */

slf_rule_expansion(Rule, Rules) :-
   slf_rule_to_period_nc_path(Rule, Period),
   slf_rule_expansion(Rule, Period, Rules).

slf_rule_expansion(Rule, L, Rules) :-
   Rule = H-_-_,
   slf_rule_expand(Rule, [H], 1, L, Gs),
   ( foreach(G, Gs), foreach(R, Rules) do
        dislog_rule_format_to_slf(H-G, R) ).


/* slf_rule_format_to_dislog(Rule_1, Rule_2) <-
      */

slf_rule_format_to_dislog(Rule_1, Rule_2) :-
   parse_dislog_rule(Rule_1, [A], As, Bs),
   append(As, Bs, Body),
   Rule_2 = [A]-Body.


/* dislog_rule_format_to_slf(Rule_1, Rule_2) <-
      */

dislog_rule_format_to_slf(Rule_1, Rule_2) :-
   parse_dislog_rule(Rule_1, [A], As, []),
   functor(A, P, N),
   member(B, As),
   functor(B, P, N),
   !,
   delete(As, B, Bs),
   Rule_2 = [A]-[B]-Bs.
dislog_rule_format_to_slf(Rule_1, Rule_2) :-
   parse_dislog_rule(Rule_1, [A], As, []),
   Rule_2 = [A]-[]-As.


/* slf_sirup_program_split(Program, Recursive, Exit, Rest) <-
      */

slf_sirup_program_split(Program, Recursive, Exit, Rest) :-
   sublist( sirup_rule,
      Program, [Recursive] ),
   delete(Program, Recursive, Program_2),
   parse_dislog_rule(Recursive, [A], _, []),
   functor(A, P, N),
   exit_rule_for_predicate(Program_2, P/N, Exit),
   delete(Program_2, Exit, Rest).

sirup_rule(Rule) :-
   parse_dislog_rule(Rule, [A], As, []),
   functor(A, P, N),
   member(B, As),
   functor(B, P, N),
   delete(As, B, Bs),
   \+ ( member(C, Bs),
        functor(C, P, N) ).

exit_rule_for_predicate(Program, P/N, Rule) :-
   member(Rule, Program),
   parse_dislog_rule(Rule, [A], As, []),
   functor(A, P, N),
   \+ ( member(B, As),
        functor(B, P, N) ).


/* superscript_program_to_program(Program_1, Program_2) <-
      */

superscript_program_to_program(Program_1, Program_2) :-
   foreach(Rule_1, Program_1), foreach(Rule_2, Program_2) do
      parse_dislog_rule(Rule_1, H1, P1, N1),
      maplist( superscript_atoms_to_atoms,
         [H1, P1, N1], [H2, P2, N2] ),
      parse_dislog_rule(Rule_2, H2, P2, N2).

superscript_atoms_to_atoms(As_1, As_2) :-
   foreach(A_1, As_1), foreach(A_2, As_2) do
      A_1 =.. [P_1|Args],
      name_cut_at_position(["^"], P_1, P_2),
      A_2 =.. [P_2|Args].


/******************************************************************/


