

/******************************************************************/
/***                                                            ***/
/***         Declare:  CKNF-Normalization                       ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* slf_rule_to_knf_sirup(Rule,R_Rule,NR_Rules) <-
      */

slf_rule_to_knf_sirup(Rule,R_Rule,NR_Rules) :-
   slf_rule_iterate(Rule,G_L),
   slf_rule_to_as_edges(Rule,AS_Edges),
   as_edges_to_folded_as_edges(AS_Edges,Folded_AS_Edges),
   connected_components(Folded_AS_Edges,CCs),
   edges_to_u_graph(Folded_AS_Edges,Folded_U_Graph),
   rule_to_extensional_predicates(Rule,Ext),
   ccs_to_cycles(
      Folded_AS_Edges,Folded_U_Graph,Ext,CCs,Cycles),
   join_predicates(
      Rule,Folded_U_Graph,Ext,G_L,Joined,Join_Lists),
   join_rules(Join_Lists,Join_Rules),
   cknf_chains(Cycles,Folded_AS_Edges,Ext,Joined,KNF_Chains),
   chain_args(KNF_Chains,Arg_Lists),
   select_exit_rules(
      Arg_Lists,KNF_Chains,Chain_Lists,Joined,Residue),
   construct_exit_rules(b,Chain_Lists,Chain_Rules),
   append(Chain_Rules,Join_Rules,NR_Rules),
   construct_recursive_rule(NR_Rules,Residue,Rule,G_L,R_Rule).


slf_rule_iterate(Rule,G_L) :-
   slf_rule_to_period_nc_path(Rule,X),
   slf_rule_expand(Rule,X,G_L).

slf_rule_iterate(Rule,L,G_L) :-
   slf_rule_expand(Rule,L,G_L).


/* slf_rule_to_sidedness(Rule, Sidedness) <-
      */

slf_rule_to_sidedness(Rule, Sidedness) :-
   slf_rule_to_period_list(Rule, Periods),
   sum_list(Periods, K),
   round(K, 0, Sidedness).


/* slf_rule_to_period(Rule,L) <-
      */

slf_rule_to_period(Rule,L) :-
   slf_rule_to_period_list(Rule,Periods),
   least_common_multiple(Periods,Aux),
   control_zero(Aux,L). 


/* s_star_period(Rule,Period) <-
      */

s_star_period(Rule,Period) :-
   slf_rule_to_s_star_edges(Rule,SSE),
   as_edges_to_folded_as_edges(SSE,FSSE),
   edges_to_u_graph(FSSE,FUG),
   connected_components(SSE,CCs),
   ccs_to_cycles(FSSE,FUG,_,CCs,Cycles),
   cycle_lengths(FSSE,Cycles,Lengths),
   least_common_multiple([1|Lengths],Period).


/*** implementation ***********************************************/


/* slf_rule_to_period_list(Rule,Periods) <-
      Given the rule SLF-Rule, the list Periods of the
      periods of the connected components of the corresponding
      AS-Graph is computed. */

slf_rule_to_period_list(Rule,Periods) :-
   slf_rule_to_as_edges(Rule,_S_Star_Edges,AS_Edges),
   as_edges_to_folded_as_edges(AS_Edges,Folded_AS_Edges),
   connected_components(Folded_AS_Edges,CCs),
   edges_to_u_graph(Folded_AS_Edges,Folded_U_Graph),
   rule_to_extensional_predicates(Rule,Extensional),
   ccs_to_cycles(Folded_AS_Edges,Folded_U_Graph,
      Extensional,CCs,Cycles),
   cycle_lengths(Folded_AS_Edges,Cycles,Periods).


/* rule_to_extensional_predicates(Rule,Pred_List) <-
      Given the recursive rule Rule of a linear datalog sirup,
      the list Pred_List of extensional body predicate symbols
      is computed */

rule_to_extensional_predicates(_-_-EB,List) :-
   atoms_to_predicate_symbols(EB,Ps),
   list_of_elements_to_relation(Ps,List).


/* ccs_to_cycles(Folded_AS_Edges,Folded_U_Graph,
         Ext,CCs,Cycles) ->
      Given the set Folded_AS_Edges of Edges of the folded
      AS-Graph, the unweighted Graph Folded_U_Graph, the list
      CCs of the connected components of the AS-Graph and the
      list Ext of extensional predicates of the recursive rule,
      the list Cycles of cycles in the AS-graph containing
      extensional predicates is returned. */

ccs_to_cycles(Folded_AS_Edges,Folded_U_Graph,
      Ext,CCs,Cycles) :-
   maplist( cc_to_cycle(Folded_AS_Edges,Folded_U_Graph,Ext),
      CCs, Cycles_2 ),
   sublist( list_non_empty,
      Cycles_2, Cycles ).

cc_to_cycle(Folded_AS_Edges,Folded_U_Graph,Ext,CC,Cycle) :-
   length(CC,L),
   L > 1,
   contains_extensional_predicate(CC,Ext,Pred),
   !,
   vertices(Folded_U_Graph,Folded_Vertices),
   folded_graph_to_cycle(Pred,
      Folded_Vertices,Folded_U_Graph,Folded_AS_Edges,Cycle).
cc_to_cycle(_,_,_,_,[]).


/* contains_extensional_predicate(Vertices,Ext,[Vertex]) <-
      Given the list Vertices and the list Ext of extensional
      predicates, the first extensional predicate symbol Vertex
      contained in Ext is returned, if it exists; otherwise
      the goal fails. */

contains_extensional_predicate(Vertices,Ext,Vertex) :-
   maplist( vertices_to_bracket_vertices(Ext),
      Vertices, Bracket_Vertices ),
   sublist( list_non_empty,
      Bracket_Vertices, List ),
   List = [[Vertex]|_].

vertices_to_bracket_vertices(Ext,Vertices,Bracket_Vertices) :-
   Vertices = [Pred|_],
   member([Pred],Ext),
   !,
   list_of_elements_to_relation(Vertices,Bracket_Vertices).
vertices_to_bracket_vertices(_,_,[]).


/* folded_graph_to_cycle(Pred,Folded_Vertices,
         Folded_U_Graph,Folded_AS_Edges,Cycle) <-
      Given the extensional predicate symbol Pred, the vertices
      Folded_U_Vertices of the unweighted graph Folded_U_Graph and
      the list Folded_AS_Edges of edges of the folded AS-graph,    
      the cyclic path Cycle, which contains Pred, is returned. */

folded_graph_to_cycle([Pred],
      [U_Vertex|_],U_Graph,Folded_AS_Edges,Cycle) :-
   member(Pred,U_Vertex),
   path(U_Vertex,U_Graph,Path),
   list_to_first_and_last(Path,First,Last),
   member(Last-First-_,Folded_AS_Edges),
   append(Path,[First],Cycle),
   !.
folded_graph_to_cycle([Pred],
      [_|U_Vertices],U_Graph,Folded_AS_Edges,Cycle) :-
   folded_graph_to_cycle([Pred],
      U_Vertices,U_Graph,Folded_AS_Edges,Cycle).


/* cycle_lengths(Folded_AS_Edges,Cycles,Periods) <-
      Given the list Cycles of cycles of the folded AS-Graph
      as well as its edge set Folded_AS_Edges, the list 
      Periods is computed. Its elements are the periods of
      the connected components corresponding to the cycles. */

cycle_lengths(Folded_AS_Edges,Cycles,Periods) :-
   maplist( path_length(Folded_AS_Edges),
      Cycles, Periods ).


/* path_length(Folded_AS_Edges,Path,Length) <-
      Given a path Path in the AS-graph with edgelist Folded_AS_Edges,
      the length Length of Path is returned */
  
path_length(Folded_AS_Edges,Cycle,Length) :- 
   path_length(Cycle,Folded_AS_Edges,0,Length).

path_length([Vertex|Vertices],Folded_AS_Edges,Length1,Length3) :-
   length(Vertices,L),
   L > 0,
   first(Vertices,First),
   member(Vertex-First-Length,Folded_AS_Edges),
   Length2 is Length1 + Length,
   path_length(Vertices,Folded_AS_Edges,Length2,Length3).
path_length(_,_,Length,Length).


/* atoms_to_lists(Atoms,Lists) <-
      Converts the list of extensional atoms of the L-th iterate
      of the sirup to the list Lists which contains as its 
      elements lists consisting on their part of the predicate
      symbol as first element, the iteration level as the 
      second, followed by the arguments of the predicate. */ 

atoms_to_lists(Atoms,Lists) :-
   maplist( atom_to_list,
      Atoms, Lists ).

atom_to_list(Atom,List) :-
   Atom =.. [P|Args],
   split_annotated_predicate_symbol(P,Symbol_List,Super_Script),
   name(Pred_Symbol,Symbol_List),
   char_list_to_number(Super_Script,Exp),
   append([[Pred_Symbol]],[Exp],Aux_List),
   append(Aux_List,Args,List).


/* lists_to_atoms(Lists,Atoms) <-
      Transforms a list List containing list representations of 
      extensional atoms into the list Atoms of the corresponding
      extensional atoms. */

lists_to_atoms(Lists,Atoms) :-
   maplist( list_to_atom,
      Lists, Atoms ).

list_to_atom([P|[Exp|Args]],Atom) :-
   transform_predicate_symbol(P,Pred_Symbol_List),
   number_to_char_list(Exp,Exp_List),
   append([Pred_Symbol_List,[94],Exp_List],List),
   name(P_Symb,List),
   Atom =.. [P_Symb|Args].


transform_predicate_symbol([P|Ps],List) :-
   name(P,Pred_Symbol_List),
   transform_predicate_symbol(Ps,Pred_Symbol_List,List).

transform_predicate_symbol([P|Ps],List1,List3) :-
   name(P,[_|Num]),
   append(List1,Num,List2),
   transform_predicate_symbol(Ps,List2,List3).
transform_predicate_symbol([],List,List).


/* cknf_chains(Cycles,Folded_AS_Edges,Ext,Ext_Atoms,KNF_Chains) <- 
      Given the set Cycles of cycles of the folded AS-Graph,
      the set Folded_AS_Edges of edges of the folded AS-Graph,
      the set Ext of extensional predicate symbols that occur
      in the recursive rule of the sirup and
      the list Ext_Atoms containing the extensional atoms of the
      L-th iterate, the list KNF_Chains are constructed.
      Its elements are subsets of Ext_Atoms that correspond to
      the recurrent equivalence classes of the AS-Graph */

cknf_chains(Cycles,Folded_AS_Edges,
      Ext,Ext_Atoms,KNF_Chains) :-
   cknf_chains(Cycles,Folded_AS_Edges,
      Ext,Ext_Atoms,[],KNF_Chains).

cknf_chains([Cycle|Cycles],Folded_AS_Edges,Ext,Ext_Atoms,
      List1,List3) :-
   cycle_chains(Cycle,Folded_AS_Edges,Ext,Ext_Atoms,Chains),
   append(List1,Chains,List2),
   cknf_chains(Cycles,Folded_AS_Edges,
      Ext,Ext_Atoms,List2,List3).
cknf_chains([],_,_,_,List,List).


/* cycle_chains(Cycle,Folded_AS_Edges,Ext,Ext_Atoms,Chains) <- 
      One call of this rule leads to the construction of one 
      single element of the list KNF_Chains,
      that is constructed in the above rule. */

cycle_chains(Cycle,Folded_AS_Edges,Ext,Ext_Atoms,Chains) :-
   cycle_paths(Cycle,Folded_AS_Edges,Ext,Cycle_Paths),
   remove_duplicates(Cycle,Cycle_Set),
   cycle_to_extensional_predicates(Cycle_Set,Ext,Ext_Predicates),
   cycle_atoms(Ext_Atoms,Ext_Predicates,Cycle_Atoms),
   chains(Cycle_Atoms,Cycle_Paths,Chains). 


/* cycle_paths(Cycle,Folded_AS_Edges,Ext,List) <- 
      Given a cycle Cycle and the set Folded_AS_Edges of edges of
      the folded AS-Graph as well as the List Ext of extensional
      predicate symbols ocurring in the sirup.
      The list List is constructed, which contains start- and
      endpoints as well as the length of all the paths in Cycle
      that start and end in vertices that correspond to
      extensional predicate symbols. */

cycle_paths(Cycle,Folded_AS_Edges,Ext,List) :-
   length(Cycle,Length), 
   cycle_paths(Cycle,1,Length,Folded_AS_Edges,Ext,[],List).

cycle_paths(Cycle,Count,Length,Folded_AS_Edges,Ext,List1,List3) :-
   Count < Length,
   nth(Count,Cycle,Vertex),
   vertex_of_extensional_predicate_symbol(Vertex,Ext),
   vertex_paths(Vertex,Cycle,Folded_AS_Edges,Ext,Paths),
   append(List1,Paths,List2),
   New_Count is Count + 2,
   cycle_paths(Cycle,New_Count,Length,Folded_AS_Edges,Ext,
      List2,List3).
cycle_paths(Cycle,Count,Length,Folded_AS_Edges,Ext,List1,List2) :-
   Count < Length,
   New_Count is Count + 1,
   cycle_paths(Cycle,New_Count,Length,Folded_AS_Edges,Ext,
      List1,List2).
cycle_paths(_,Length,Length,_,_,List,List).


/* vertex_of_extensional_predicate_symbol(Vertex,Ext) <-
      Returns true, if the vertex Vertex corresponds to an
      extensional predicate symbol. */ 

vertex_of_extensional_predicate_symbol([Pred_Symbol|_],Ext) :-
   member([Pred_Symbol],Ext).


vertex_paths(Vertex,Cycle,Folded_AS_Edges,Ext,Paths) :-
   vertex_paths(Vertex,Cycle,Folded_AS_Edges,Ext,[],Paths).      

vertex_paths(Vertex,Cycle,Folded_AS_Edges,Ext,Paths1,Paths3) :-
   path_length(Folded_AS_Edges,Cycle,Length),
   vertex_to_elementary_cycle(Vertex,Length,Elementary_Cycle), 
   append(Paths1,[Elementary_Cycle],Paths2),
   non_cyclic_paths(Vertex,Cycle,Folded_AS_Edges,Ext,Nc_Paths),
   append(Paths2,Nc_Paths,Paths3).

vertex_to_elementary_cycle(Vertex,Length,[Vertex,Vertex,Length]).


/* non_cyclic_paths(Vertex,Cycle,Folded_AS_Edges,Ext,Paths) <-
      The list Paths that is constructed by calling this rule
      consists of lists being constructed following the scheme
      described in the comment to the rule cycle_paths/4.
      Each one of those lists corresponds to a non-cyclic path 
      with startpoint Vertex. */ 

non_cyclic_paths(Vertex,Cycle,Folded_AS_Edges,Ext,Paths) :-
   remove_duplicates(Cycle,Cycle_Set),
   nth(N,Cycle_Set,Vertex),
   M is N - 1,
   list_rotate_left(Cycle_Set,M,[_|R_Cycle_Set]),
   nc_paths(R_Cycle_Set,Ext,[Vertex],[],Nc_Paths),
   atom_paths(Nc_Paths,Folded_AS_Edges,Paths).

nc_paths([Edge|Edges],Ext,Path_Acc,Paths1,Paths3) :-
   vertex_of_extensional_predicate_symbol(Edge,Ext),
   append(Path_Acc,[Edge],Path_Acc_New),
   append(Paths1,[Path_Acc_New],Paths2),
   nc_paths(Edges,Ext,Path_Acc_New,Paths2,Paths3).
nc_paths([Edge|Edges],Ext,Path_Acc,Paths1,Paths2) :-
   append(Path_Acc,[Edge],Path_Acc_New),
   nc_paths(Edges,Ext,Path_Acc_New,Paths1,Paths2).
nc_paths([],_,_,Paths,Paths).


/* atom_paths(Paths,Folded_AS_Edges,Path_List) <-
      Given the list Paths of paths between Vertices of the 
      folded AS-Graph that correspond to extensional predicates 
      and the set Folded_AS_Edges of edges of the folded 
      AS-Graph, the list Path_List is constructed.
      It contains lists containing the startpoint and the endpoint 
      of the corresponding path as well as its length. */

atom_paths(Paths,Folded_AS_Edges,Path_List) :-
   maplist( path_to_start_end_length(Folded_AS_Edges),
      Paths, Path_List ).

path_to_start_end_length(Folded_AS_Edges,Path,[Start,End,Length]) :-
   path_length(Folded_AS_Edges,Path,Length),
   list_to_first_and_last(Path,Start,End).


/* cycle_to_extensional_predicates(Cycle,Ext,Predicate_Symbols) <- 
      Given a cycle Cycle of the folded AS-Graph and the list
      Ext of the extensional predicates of the nonrecursive rule,
      the list Predicate_Symbols of the extensional predicate
      symbols contained in Cycle is returned. */

cycle_to_extensional_predicates(Cycle,Ext,Predicate_Symbols) :-
   maplist( vertex_to_extensional_predicates(Ext),
      Cycle, Lists_of_Predicates ),
   append(Lists_of_Predicates,Predicate_Symbols).

vertex_to_extensional_predicates(Extensional,Vertex,Vertex) :-  
   first(Vertex,Predicate_Symbol),
   member([Predicate_Symbol],Extensional).
vertex_to_extensional_predicates(_,_,[]).



/* cycle_atoms(Ext_Atoms,Ext_Predicates,Cycle_Atoms) <-
      Given the list Ext_Atoms containing the extensional atoms 
      of the L-th iterate and the list Ext_Predicates of 
      extensional predicate symbols of the L-th iterate, the list 
      Cycle_Atoms is computed. It contains those elements of 
      Ext_Atoms whose predicate symbols are members of 
      Ext_Predicates. */

cycle_atoms(Ext_Atoms,Ext_Predicates,Cycle_Atoms) :-
   cycle_atoms(Ext_Atoms,Ext_Predicates,[],Cycle_Atoms).

cycle_atoms(Ext_Atoms,[Pred|Preds],List1,List3) :-
   atoms_with_predicate_symbol([Pred],Ext_Atoms,Atom_List),
   append(List1,Atom_List,List2),
   cycle_atoms(Ext_Atoms,Preds,List2,List3).
cycle_atoms(_,[],List,List).
 

/* chains(Cycle_Atoms,Cycle_Paths,KNF_Chains) <-
      */

chains(Cycle_Atoms,Cycle_Paths,KNF_Chains) :-
   length(Cycle_Atoms,L),
   chains(1,L,Cycle_Atoms,Cycle_Paths,[],KNF_Chains).

chains(Pos,Length,Atoms,Paths,List1,List3) :-
   Pos =< Length,
   nth(Pos,Atoms,Atom),
   new_chain_atom(Atom,List1),
   cknf_chain(Atom,Atoms,Paths,Chain),
   append(List1,[Chain],List2),
   New_Pos is Pos + 1,
   chains(New_Pos,Length,Atoms,Paths,List2,List3).
chains(Pos,Length,Atoms,Paths,List1,List2) :-
   Pos =< Length,
   New_Pos is Pos + 1,  
   chains(New_Pos,Length,Atoms,Paths,List1,List2).
chains(Length,Length,_,_,List,List).


new_chain_atom(Atom,[Head|_]) :-
   member(Atom,Head),
   !,
   fail.
new_chain_atom(Atom,[_|Tail]) :-
   new_chain_atom(Atom,Tail).
new_chain_atom(_,[]).

         
cknf_chain(Atom,Atoms,Paths,Chain) :-
   construct_chain(Atoms,Paths,[Atom],Rev_Chain),
   length(Rev_Chain,Length),
   Length > 1,
   reverse(Rev_Chain,Chain).


construct_chain(Atoms,Paths,List1,List3) :-
   first(List1,Atom),
   possible_paths(Atom,Paths,Pos_Paths),
   not(empty(Pos_Paths)),
   shortest_path(Pos_Paths,Shortest_Path),
   construct_chain_next_element(Atom,Atoms,Shortest_Path,Next),
   append([Next],List1,List2),
   construct_chain(Atoms,Paths,List2,List3).
construct_chain(_,_,List,List). 

construct_chain_next_element([P|[Exp|_]],Atoms,[P,P2,L],
      [P2|[X|Args]]) :-
   X is Exp + L,
   member([P2|[X|Args]],Atoms).
   
possible_paths([P|_],Paths,Pos_Paths) :-
   findall( [P|Args],
      member([P|Args],Paths),
      Pos_Paths).

shortest_path([Path],Path) :-
   !.
shortest_path([Path|Paths],Shortest_Path) :-
   shortest_path(Paths,Path,Shortest_Path).

shortest_path([[X,Y,L1]|Paths],[_,_,L2],Shortest_Path) :-
   L1 < L2,
   !,
   shortest_path([Paths],[X,Y,L1],Shortest_Path).
shortest_path([_|Paths],Path,Shortest_Path) :-
   shortest_path(Paths,Path,Shortest_Path).
shortest_path([],Path,Path).



/* split_annotated_predicate_symbol(P_E,P,E) <-
      Splits an annotated predicate symbol P_E = P^E into the
      corresponding predicate symbol P and the exponent number E.
      For example, the predicate symbol `a1^3' is split into `a1'
      and `3'. */

split_annotated_predicate_symbol(P_E,P,E) :-
   name(P_E,PE_List),
   append(P,[94|E],PE_List).


/* construct_annotated_predicate_symbol(P,E,P_E) <-
      Concats the predicate symbol P and the exponent number E
      yielding the annotated predicate symbol P_E = P^E.
      For example the predicate symbol `a3' and the number `2'
      are joined to `a3^2'. */

construct_annotated_predicate_symbol(P,E,P_E) :-
   name(P,List1),
   append(List1,[94],List2),
   number_to_char_list(E,List3),
   append(List2,List3,List4),
   name(P_E,List4).



/* char_list_to_number(Char_List,Num) -> 
      Converts the list Char_List of numeric characters to the 
      corresponding number Num. */

char_list_to_number([123|Tail],Num) :-
   delete(Tail,125,Char_List),
   char_list_to_number(Char_List,1,0,Num).
char_list_to_number(Char_List,Num) :-
   char_list_to_number(Char_List,1,0,Num).

char_list_to_number([Char|List],Pot,Num1,Num3) :-
   Num is Char - 48,
   Num2 is Num1 + Num * Pot,
   New_Pot is Pot * 10,
   char_list_to_number(List,New_Pot,Num2,Num3).
char_list_to_number([],_,Num,Num).


/* number_to_char_list(Num,Char_List) ->
      Converts the number Num to the corresponding character
      list Char_List. */

number_to_char_list(Num,List) :-
   Num < 10, 
   number_to_char_list(Num,[],List),
   !.
number_to_char_list(Num,List) :-
   number_to_char_list(Num,[],Aux),
   append([123|Aux],[125],List).

number_to_char_list(Num,List1,List2) :-
   Num < 10,
   Char is Num + 48,
   append([Char],List1,List2).
number_to_char_list(Num,List1,List3) :-
   Res is Num mod 10,
   X is Num - Res,
   Num2 is X / 10,
   Char is Res + 48,
   append([Char],List1,List2),
   number_to_char_list(Num2,List2,List3).


/* list_rotate_left(List,Number,R_List) <- 
      Given the input list List and the number Number, the 
      resulting list R_List is the list obtained applying the 
      cyclic permutation on list that rotates it Number
      positions to the left direction. */

list_rotate_left(List1,Number,List3) :-
   Number > 0,
   list_shift_left(List1,List2),
   New_Number is Number - 1,
   list_rotate_left(List2,New_Number,List3).
list_rotate_left(List,0,List).

    
list_shift_left([H|T],L) :-
   append(T,[H],L). 


/* slf_rule_to_period_nc_path(Rule,X) <-
      The equivalent KNF-sirup to the given sirup with the
      recursive rule Rule is determined by its X-th iterate
      which is computed by this rule. */

slf_rule_to_period_nc_path(Rule,X) :- 
   slf_rule_to_period(Rule,L),
   s_star_period(Rule,L_Star),
   nc_path_length(Rule,NC_L),
   least_common_multiple([L,L_Star,NC_L],M),
   round(M, 0, X).



/* nc_path_length(Rule,L) <-
      Given Rule R, the length L of the longest non-cyclic path
      of the corresponding S*-Graph is computed */ 

nc_path_length(Rule,L) :-
   slf_rule_to_s_edges(Rule,SG),
   edges_to_u_graph(SG,UG),
   vertices(UG,Vertices),
   slf_rule_to_s_star_edges(Rule,SSG),
   max_length(Vertices,UG,SSG,0,Aux),
   control_zero(Aux,L).


max_length([Vertex|Vertices],UG,SSG,L1,L3) :-
   path(Vertex,UG,Path),
   path_length(SSG,Path,L2),
   L2 > L1,
   max_length(Vertices,UG,SSG,L2,L3).
max_length([_|Vertices],UG,SSG,L1,L2) :-
   max_length(Vertices,UG,SSG,L1,L2).
max_length([],_,_,L,L).


/* control_zero(N,M) <-
      Given a number N, the number M is set to 1, if N is 0; 
      otherwise M is set to the value of N. */
 
control_zero(0,1) :-
   !.
control_zero(L,L).


/* join_predicates(Rule,Folded_U_Graph,Ext,G_L,Joined) <- 
      All the extensional predicates of the L-th iterate G_L 
      that correspond to the same vertice in the folded AS-Graph 
      Folded_U_Graph are joined together to one atom at each 
      iteration level.
      E.g. if [a1,a2] is a vertice of the folded AS-Graph,
      then the atoms a1^1(x1,x3,x4) and a2^1(u1^1,x3) are joined
      to one single atom a12^1(x1,x3,x4,u1^1). */

join_predicates(Rule,Folded_U_Graph,Ext,[_|Ext_Atoms],Joined,Rules) :-
   vertices(Folded_U_Graph,F_Vertices),
   slf_rule_to_period_nc_path(Rule,X),
   atoms_to_lists(Ext_Atoms,Ext_Lists),
   join_predicates(X,F_Vertices,Ext,Ext_Lists,Joined,[],Rules).

join_predicates(X,[F_Vertex|F_Vertices],
      Ext,List1,List3,Rules1,Rules3) :-
   first(F_Vertex,Pred),
   member([Pred],Ext),
   length(F_Vertex,Length),
   Length > 1,
   join_vertex(X,F_Vertex,List1,List2,Rules1,Rules2),
   join_predicates(X,F_Vertices,Ext,List2,List3,Rules2,Rules3).
join_predicates(X,[_|F_Vertices],Ext,List1,List2,Rules1,Rules2) :-
   join_predicates(X,F_Vertices,Ext,List1,List2,Rules1,Rules2).
join_predicates(_,[],_,List,List,Rules,Rules).

join_vertex(L,F_Vertex,List1,List2,Rules1,Rules2) :-
   join_vertex(1,L,F_Vertex,List1,List2,Rules1,Rules2).

join_vertex(I,L,F_Vertex,List1,List3,Rules1,Rules3) :-
   I =< L,
   join_vertex_i(I,F_Vertex,List1,List2,Join_Rule),
   append(Rules1,[Join_Rule],Rules2),
   I_new is I + 1,
   join_vertex(I_new,L,F_Vertex,List2,List3,Rules2,Rules3).
join_vertex(_,_,_,List,List,Rules,Rules).

join_vertex_i(I,F_Vertex,List1,List2,Join_Rule) :-
   construct_join_list(I,F_Vertex,List1,Join_List),
   setdifference_2(List1,Join_List,Residue),
   join_atoms(Join_List,Rule_Head),
   append([Rule_Head],[Join_List],Join_Rule),
   append(Residue,[Rule_Head],List2).


construct_join_list(I,F_Vertex,Pred_List,Join_List) :-
   construct_join_list(I,F_Vertex,Pred_List,[],Join_List).

construct_join_list(I,[P|Ps],P_List,List1,List3) :-
   member([[P]|[I|Args]],P_List),
   append(List1,[[[P]|[I|Args]]],List2),
   construct_join_list(I,Ps,P_List,List2,List3).
construct_join_list(_,[],_,List,List).


join_atoms([A1,A2|As1],As2) :-
   join_two_atoms(A1,A2,A),
   join_atoms([A|As1],As2).
join_atoms([A],[A]).
join_atoms([],[]).

join_two_atoms([P1|Args1],[P2|Args2],[P|Args]) :-
   append(P1,P2,P),
   append(Args1,Args2,Aux),
   remove_duplicates(Aux,Args).


/* join_rules(List,Join_Rules) */

join_rules(List,Join_Rules) :-
   join_rules(List,[],Join_Rules).

join_rules([R|Rs],Rs1,Rs3) :- 
   join_rule(R,J_R),
   append(Rs1,[J_R],Rs2),
   join_rules(Rs,Rs2,Rs3).
join_rules([],Rs,Rs).

join_rule([Head_List,Body_List],[Head_Atom]-[]-Body_Atoms) :-
   list_to_atom(Head_List,Head_Atom),
   lists_to_atoms(Body_List,Body_Atoms).
   

/* chain_args(Chains,List) <-
      Given the list Chains of connected components of the L-th
      iterate of the sirup, the list List of argument lists of
      the non-recursive rules corresponding to the connected
      components is computed */ 

chain_args(Chains,List) :-
   chain_args(Chains,[],List).

chain_args([C|Cs],List1,List3) :-
   collect_args(C,Args),
   append(List1,[Args],List2),
   chain_args(Cs,List2,List3).
chain_args([],List,List).


/* collect_args(Chain,Head_Args) <-
      Given a chain Chain basing on a connected component of the
      Folded AS-Graph, the argument list Head_Args is constructed.
      It contains the arguments of the head atom of the new 
      non-recursive rule constructed on the base of Chain */ 

collect_args([[_|[_|Args]]|Atoms],Head_Args) :-
   collect_args(Atoms,Args,Head_Args).

collect_args([Atom|Atoms],Args_acc,Args2) :-
   collect_args_dual(Args_acc,Atom,Args1),
   collect_args(Atoms,Args1,Args2).
collect_args([],Args,Args).

collect_args_dual(Args1,[_|[_|Args2]],Joined) :-
   list_to_ord_set(Args1,Ord_Args1),
   list_to_ord_set(Args2,Ord_Args2),
   ord_intersection(Ord_Args1,Ord_Args2,Int),
   list_union(Args1,Args2,Union), 
   setdifference_2(Union,Int,Joined).
   

/* list_union(List1,List2,List) <-
      */

list_union(List1,List2,List) :-
   append(List1,List2,Aux),
   remove_duplicates(Aux,List).


/* select_exit_rules(Arg_Lists,Chains,NR_Rules,R_Rule1,R_Rule2) <-
      */ 

select_exit_rules(Arg_Lists,Chains,NR_Rules,R_Rule1,R_Rule2) :-
   select_exit_rules(Arg_Lists,Chains,[],NR_Rules,R_Rule1,R_Rule2).
  
select_exit_rules([Arg_List|Arg_Lists],[C|Cs],Rs1,Rs3,As1,As3) :- 
   append(Rs1,[[Arg_List|C]],Rs2),
   setdifference_2(As1,C,As2),
   select_exit_rules(Arg_Lists,Cs,Rs2,Rs3,As2,As3).
select_exit_rules([],[],Rs,Rs,As,As).


/* construct_exit_rules(Char,NRR_List_1,NRR_List_2) <-
      Given a character Char and the list NRR_List_1, each of 
      whose elements contains the argument list of the head atom 
      as well as the body atoms of the corresponding non-recursive
      rule, the list NRR_List_2 of non-recursive rules of the
      KNF-Sirup is constructed */

construct_exit_rules(Char,Chains,NR_Rules) :-
   construct_exit_rules(Char,1,Chains,[],NR_Rules).

construct_exit_rules(Char,I,[Chain|Chains],List1,List3) :-
   construct_exit_rule(Char,I,Chain,Rule),
   append(List1,[Rule],List2),
   J is I + 1,
   construct_exit_rules(Char,J,Chains,List2,List3).
construct_exit_rules(_,_,[],List,List).


/* construct_exit_rule(Char,I,NRR_List,NR_Rule) <-
      Here a non-recursive rule is constructed, given the
      character Char,the integer I and list NRR_List (being built 
      up as described in construct_exit_rules/3 */

construct_exit_rule(Char,I,[Arg_List|Chain_Lists], [Head]-[]-Chain) :-
   name(Char,Aux),
   name(I,Num_List),
   append(Aux,Num_List,Aux_List),
   name(Functor,Aux_List),
   Head =.. [Functor|Arg_List],
   lists_to_atoms(Chain_Lists,Chain). 


/* construct_recursive_rule(NR_Rules,Residue,Rule,G_L,R_Rule) <-
      The recursive rule R_Rule of the KNF-Sirup is constructed 
      as follows: its head atom is the head atom of the recursive
      rule Rule of the input sirup, its recursive body atom is 
      the recursive body atom of the L-th iterate G_L and its
      extensional body atoms are either the heads of the newly 
      constructed non-recursive rules (contained in the list 
      NR_Rules) or extensional atoms of the input sirup 
      (contained in the list Residue) */

construct_recursive_rule(NR_Rules,Residue_Lists,
      Head-_-_,[R_Atom|_],Head-[R_Atom]-Ext_Atoms) :-
   lists_to_atoms(Residue_Lists,Residue),
   findall( X,
      member([X]-_-_,NR_Rules),
      Aux ),
   append(Aux,Residue,Ext_Atoms_Aux),
   remove_duplicates(Ext_Atoms_Aux,Ext_Atoms).


/******************************************************************/


