

/******************************************************************/
/***                                                            ***/
/***         Declare:  CKNF-Normalization                       ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(cknf, slf_rule_to_knf_sirup) :-
   Rule = [p(x1, x2, x3, x4, x5, x6)]-
      [p(u1, u2, u3, x3, x5, u6)]-
      [a1(x1, u1), a2(x2, u3), a3(x4, u2), a4(x6)],
   slf_rule_to_knf_sirup(Rule, R_Rule, NR_Rules),
   dwrite(rule, Rule),
   dwrite(rule, R_Rule),
   dwrite(rules, NR_Rules).

test(cknf, join_predicates) :-
   Rule = [p(x1, x2, x3, x4, x5, x6)]-
      [p(u1, u2, u3, x3, x5, u6)]-
      [a1(x1, u1), a2(x2, u3), a3(x4, u2), a4(x6)],
   Folded_U_Graph =
      [ [1]-[[a1]], [2]-[[a2]], [3]-[[4]],
        [4]-[[a3]], [5]-[[5]], [6]-[[a4]],
        [a1]-[[1]], [a2]-[[3]], [a3]-[[2]], [a4]-[] ],
   Ext = [ [a1], [a2], [a3], [a4] ],
   G_L = [ p('u1^3', 'u2^3', 'u3^3', 'u3^2', x5, 'u6^3'),
      'a1^1'(x1, 'u1^1'), 'a2^1'(x2, 'u3^1'),
      'a3^1'(x4, 'u2^1'), 'a4^1'(x6),
      'a1^2'('u1^1', 'u1^2'), 'a2^2'('u2^1', 'u3^2'),
      'a3^2'(x3, 'u2^2'), 'a4^2'('u6^1'),
      'a1^3'('u1^2', 'u1^3'), 'a2^3'('u2^2', 'u3^3'),
      'a3^3'('u3^1', 'u2^3'), 'a4^3'('u6^2') ],
   join_predicates(Rule, Folded_U_Graph, Ext, G_L, X, Y),
   writeln(X-Y).

test(cknf, slf_rule_to_sidedness) :-
   Rule = [p(x1, x2, x3, x4, x5, x6)]-
      [p(u1, u2, u3, x3, x5, u6)]-
      [a1(x1, u1), a2(x2, u3), a3(x4, u2), a4(x6)],
   slf_rule_to_sidedness(Rule, Sidedness),
   writeln(Sidedness).

test(cknf, ccs_to_cycles) :-
   ccs_to_cycles(
      [ [3]-[4]-1, [5]-[5]-1,
        [a1]-[1]-1/2, [a3]-[2]-1/2, [a2]-[3]-1/2,
        [1]-[a1]-1/2, [2]-[a2]-1/2,
        [4]-[a3]-1/2, [6]-[a4]-1/2 ],
      [ [1]-[[a1]], [2]-[[a2]], [3]-[[4]],
        [4]-[[a3]], [5]-[[5]], [6]-[[a4]],
        [a1]-[[1]], [a2]-[[3]], [a3]-[[2]], [a4]-[] ],
      [ [a1], [a2], [a3], [a4] ],
      [ [[1], [a1]], [[2], [3], [4], [a2], [a3]],
        [[5]], [[6]], [[a4]] ],
      Cycles ),
   writeln(Cycles).

test(cknf, cc_to_cycle) :-
   cc_to_cycle(
      [ [3]-[4]-1, [5]-[5]-1,
        [a1]-[1]-1/2, [a3]-[2]-1/2, [a2]-[3]-1/2,
        [1]-[a1]-1/2, [2]-[a2]-1/2,
        [4]-[a3]-1/2, [6]-[a4]-1/2 ],
      [ [1]-[[a1]], [2]-[[a2]], [3]-[[4]],
        [4]-[[a3]], [5]-[[5]], [6]-[[a4]],
        [a1]-[[1]], [a2]-[[3]], [a3]-[[2]], [a4]-[] ],
      [ [a1], [a2], [a3], [a4] ],
      [ [1], [a1] ], Cycle ),
   writeln(Cycle).

test(cknf, folded_graph_to_cycle) :-
   folded_graph_to_cycle(
      [a1],
      [[1], [2], [3], [4], [5], [6],
       [a1], [a2], [a3], [a4]],
      [[1]-[[a1]], [2]-[[a2]], [3]-[[4]],
       [4]-[[a3]], [5]-[[5]], [6]-[[a4]],
       [a1]-[[1]], [a2]-[[3]], [a3]-[[2]], [a4]-[]],
      [[3]-[4]-1, [5]-[5]-1,
       [a1]-[1]-1/2, [a3]-[2]-1/2, [a2]-[3]-1/2,
       [1]-[a1]-1/2, [2]-[a2]-1/2,
       [4]-[a3]-1/2, [6]-[a4]-1/2],
      Cycle ),
   writeln(Cycle).


/******************************************************************/


