

/******************************************************************/
/***                                                            ***/
/***         Declare:  AS - Graphs                              ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(cknf, sirup_to_as_graph) :-
   forall( test_sirup(I, Rule),
      ( write_list(['rule ', I, ' = ']),
        pp_slf_rule(Rule),
        slf_rule_to_as_edges(Rule,_S_Star_Graph,AS_Graph),
        writeln(AS_Graph),
        as_edges_to_labeled_edges(AS_Graph,Labeled_AS_Graph),
        dportray(edge(labeled_graph),Labeled_AS_Graph),
        as_edges_to_folded_as_edges(AS_Graph,Folded_AS_Graph),
        star_line, writeln(Folded_AS_Graph),
        as_edges_to_labeled_edges(Folded_AS_Graph,
           Labeled_Folded_AS_Graph),
        writeln(Labeled_Folded_AS_Graph),
        dportray(edge(labeled_graph),Labeled_Folded_AS_Graph) ) ).


/*** interface ****************************************************/


/* linear_sirup_to_as_graph_edges_to_picture(Sirup) <-
      */

linear_sirup_to_as_graph_edges_to_picture(Sirup) :-
   linear_sirup_to_as_graph_edges(Sirup, AS_Edges),
%  writeq(AS_Edges),
   as_graph_edges_to_picture(AS_Edges).


/* linear_sirup_to_as_graph_edges(Sirup, AS_Edges) <-
      */

linear_sirup_to_as_graph_edges(Sirup, AS_Edges) :-
   slf_sirup_program_split(Sirup, Recursive, _, _),
   dislog_rule_format_to_slf(Recursive, Recursive_2),
   slf_rule_to_as_edges(Recursive_2, _S_Star_Edges, AS_Edges).


/* as_graph_edges_to_picture(AS_Edges) <-
      */

as_graph_edges_to_picture(AS_Edges) :-
   as_graph_edges_to_vertices(AS_Edges, Vertices),
   as_graph_edges_to_edges(AS_Edges, Edges),
   as_graph_edges_to_roots(AS_Edges, Roots),
   vertices_edges_to_picture_with_roots(
      dfs, Roots, Vertices, Edges, _ ).

as_graph_edges_to_vertices(AS_Edges, Vertices) :-
   setof( V,
      W^L^( member([V]-[W]-L, AS_Edges)
      ; member([W]-[V]-L, AS_Edges) ),
      Vertices ).

as_graph_edges_to_edges(AS_Edges, Edges) :-
   findall( Edge,
      ( ( member([V]-[W]-L, AS_Edges), Edge = V-W )
      ; ( member([W]-[V]-L, AS_Edges), Edge = W-V ) ),
      Edges_2 ),
   sort(Edges_2, Edges).

as_graph_edges_to_roots(AS_Edges, Roots) :-
   as_graph_edges_to_vertices(AS_Edges, Vertices),
   setof( V,
      ( member(V, Vertices),
        number(V) ),
      Roots ).


/* slf_rule_to_as_edges(Rule,AS_Graph) <-
      Given the recursive rule Rule of a Linear Datalog Sirup,
      the corresponding AS-graph AS_Graph is constructed. */

slf_rule_to_as_edges([H]-[A]-B,AS_Graph) :-
   H =.. [_|Xs], A =.. [_|Us],
   slf_rule_to_s_star_edges([H]-[A]-B,S_Star_Graph),
   slf_rule_to_as_edges(Xs,Us,B,S_Star_Graph,AS_Graph).

slf_rule_to_as_edges([H]-[A]-B,S_Star_Graph,AS_Graph) :-   
   H =.. [_|Xs], A =.. [_|Us],
   slf_rule_to_s_edges(Xs,Us,S_Graph),
   slf_rule_to_s_star_edges(Us,S_Graph,S_Star_Graph),
   slf_rule_to_as_edges(Xs,Us,B,S_Star_Graph,AS_Graph).


/* slf_rule_to_s_edges(Rule,S_Graph) <-
      Given the recursive rule Rule of a Linear Datalog Sirup,
      the corresponding S-graph S_Graph is constructed */

slf_rule_to_s_edges([H]-[A]-_,S_Graph) :-
   H =.. [_|Xs], A =.. [_|Us],
   slf_rule_to_s_edges(Xs,Us,S_Graph).


/* slf_rule_to_s_star_edges(Rule,S_Star_Graph) <-
      Given the recursive rule Rule of a Linear Datalog Sirup,
      the corresponding S*-graph S_Star_Graph is constructed */

slf_rule_to_s_star_edges([H]-[A]-_,S_Star_Graph) :-
   H =.. [_|Xs], A =.. [_|Us],
   slf_rule_to_s_edges(Xs,Us,S_Graph),
   slf_rule_to_s_star_edges(Us,S_Graph,S_Star_Graph).


as_edges_to_labeled_edges(As_Edges,Labeled_Edges) :-
   maplist( as_edge_to_labeled_edge,
      As_Edges, Labeled_Edges ).

as_edge_to_labeled_edge(X-Y-L,[X,Y,L]).



/*** implementation ***********************************************/


/* slf_rule_to_s_edges(Xs,Us,S_Graph) <-
      Given the list Xs of variables in the recursive head atom,
      and the list Us of variables in the recursive body atom,
      the S-graph S_Graph of the corresponding linear sirup is
      constructed. */

slf_rule_to_s_edges(Xs,Us,S_Graph) :-
   slf_rule_to_s_edges(Xs,Us,1,[],S_Graph).

slf_rule_to_s_edges([X|Xs],Us,X_pos,Edges1,Edges3) :-
   x_to_us_edges(X,Us,X_pos,Edges1,Edges2), 
   X_pos_new is X_pos + 1,
   slf_rule_to_s_edges(Xs,Us,X_pos_new,Edges2,Edges3).
slf_rule_to_s_edges([],_,_,Edges,Edges).


/* x_to_us_edges(X,Us,Xpos,Edges1,Edges2) <-
      given the element X at position Xpos in the list Xs, the
      list Us and the list Edges1 of S-Edges constructed so far,
      for each occurrence of X in Us an Edge from Xpos to the 
      corresponding position of X in Us with length 1 is created
      and added to Edges1, resulting in Edgelist Edges2. */ 

x_to_us_edges(X,Us,Xpos,Edges1,Edges2) :-
   x_to_us_edges(X,Us,Xpos,1,Edges1,Edges2).

x_to_us_edges(_,[],_,_,Edges,Edges).
x_to_us_edges(X,[X|Us],Xpos,Upos,Edges1,Edges3) :-
   append(Edges1,[[Xpos] - [Upos] - 1],Edges2),
   !,
   Upos_new is Upos + 1,
   x_to_us_edges(X,Us,Xpos,Upos_new,Edges2,Edges3).
x_to_us_edges(X,[_|Us],Xpos,Upos,Edges1,Edges2) :-
   Upos_new is Upos + 1,
   x_to_us_edges(X,Us,Xpos,Upos_new,Edges1,Edges2).
 


/* slf_rule_to_s_star_edges(Us,S_Graph,S_Star_Graph) <-
      Given the list Us of variables of the recursive body atom
      and the S-Graph S_Graph, the S*-Graph S_Star_Graph of the
      linear sirup is constructed */ 

slf_rule_to_s_star_edges(Us,S_Graph,S_Star_Graph) :-
   remove_duplicates(Us,Us_Elements),
   us_elements_position_lists(Us_Elements,Us,S_Star_Vertices),
   construct_s_star_edges(S_Star_Vertices,S_Graph,S_Star_Graph).


/* us_elements_position_lists(Us_Elements,Us,S_Star_Vertices) <-
      Given the list Us_Elements of the Elements of the list Us
      and the list Us itself, the list S_Star_Vertices is 
      constructed. It contains as its elements lists of the 
      positions of the ocurrences of the elements of Us_elements
      in Us. */

us_elements_position_lists(Us_Elements,Us,S_Star_Vertices) :-
   us_elements_position_lists(Us_Elements,Us,[],S_Star_Vertices).
    
us_elements_position_lists([Us_Elem|Us_Elems],U,List1,List3) :- 
   us_elem_position_list(Us_Elem,U,Pos_List),
   append(List1,[Pos_List],List2),
   us_elements_position_lists(Us_Elems,U,List2,List3).
us_elements_position_lists([],_,List,List). 


/* us_elem_position_list(Us_Elem,U,Pos_List) <-
      Given an element Us_elem and the list U, the list Pos_List
      that contains the positions of the ocurrences of Us_elem
      in U is constructed */

us_elem_position_list(Us_Elem,U,Pos_List) :-
   us_elem_position_list(Us_Elem,U,1,[],Pos_List).

us_elem_position_list(_,[],_,Pos_List,Pos_List).
us_elem_position_list(Us_Elem,[Us_Elem|Us],
      Pos,Pos_List1,Pos_List3) :-
   append(Pos_List1,[Pos],Pos_List2),
   Newpos is Pos + 1,
   us_elem_position_list(Us_Elem,Us,Newpos,Pos_List2,Pos_List3).
us_elem_position_list(Us_Elem,[_|Us],
      Pos,Pos_List1,Pos_List3) :-
   Newpos is Pos + 1,
   us_elem_position_list(Us_Elem,Us,Newpos,Pos_List1,Pos_List3).


/* construct_s_star_edges(S_Star_Vertices,S_Graph,S_Star_Graph) <-
      Given the list S_Star_Vertices that contains the position  
      lists of the Elements of Us_Elements (respectively of the  
      recursive body arguments) in U and the S-Graph S_Graph, the 
      additional edges of the S*-Graph are constructed, leading to  
      the list of S*-Edges S_Star_Graph */ 
      
construct_s_star_edges([],Edges,Edges).
construct_s_star_edges([Vertex|Vertices],Edges1,Edges3) :-
   length(Vertex,1),
   % If the list Vertex contains only one element, i.e. the corres-
   % ponding argument occurs only one time in the argument list of 
   % the recursive body atom, Vertex is already a vertex of the
   % S-Graph,so no additional work is necessary
   construct_s_star_edges(Vertices,Edges1,Edges3).
construct_s_star_edges([Vertex|Vertices],Edges1,Edges3) :-
   % Here the position list Vertex contains more than one element,
   % so it must be added and linked to the graph by additional 
   % edges. This is done by calling the rule
   s_star_edge(Vertex,Edges1,Edges2),
   construct_s_star_edges(Vertices,Edges2,Edges3). 


/* s_star_edge(Vertex,Edges1,Edges2) :-
      Given a new S*-Vertex Vertex, i.e. an element position list 
      with more than one element, and the list Edges1 of Edges of 
      the S*-Graph constructed so far, the rule adds edges of 
      length 1 from the position list to each of its elements. 
      The result is stored in the edgelist Edges2. */

s_star_edge(Vertex,Edges1,Edges2) :-
   S =.. ['s*'|Vertex],
   s_star_edge([S],Vertex,Edges1,Edges2).

s_star_edge(A,[Pos|PosList],Edges1,Edges3) :-
   append(Edges1,[A - [Pos] - 1],Edges2),
   s_star_edge(A,PosList,Edges2,Edges3).
s_star_edge(_,[],Edges,Edges).



/* slf_rule_to_as_edges(Xs,Us,Ext_Atoms,S_Star_Graph,AS_Graph) <- 
      Given the list Xs of arguments of the head atom, the list Us 
      of arguments of the recursive body atom, the list Ext_Atoms 
      containing the extensional body atoms and the S*-Graph 
      S_Star_Graph, the AS-Graph AS_Graph is constructed */

slf_rule_to_as_edges(Xs,Us,Ext_Atoms,Edges1,Edges3) :-
   rec_body_edges(Us,Ext_Atoms,Edges1,Edges2),
   head_edges(Xs,Ext_Atoms,Edges2,Edges3). 


/* rec_body_edges(Us,Ext_Atoms,Edges1,Edges2) <-
      Given  the list Us of arguments of the recursive body atom,
      the list Ext_Atoms containing the extensional body atoms and
      the edgelist Edges1, the edges of the AS-Graph which are  
      based on the sharing of arguments between the intensional 
      body atom and the extensional body atoms are constructed and 
      added to the edgelist, leading to list Edges2 */

rec_body_edges(Us,Ext_Atoms,Edges1,Edges2) :-
   rec_body_edges(Us,Ext_Atoms,Edges1,Edges2,1).

rec_body_edges([],_,Edges,Edges,_).
rec_body_edges([U|Us],Ext_Atoms,Edges1,Edges3,U_pos) :-
   u_edges(U,Ext_Atoms,Edges1,Edges2,U_pos),
   U_pos_new is U_pos + 1,
   rec_body_edges(Us,Ext_Atoms,Edges2,Edges3,U_pos_new).
   

/* u_edges(U,Ext_Atoms,Edges1,Edges2,U_pos) <-
      Given an argument U of the intensional body atom, its  
      position U_pos in the argument list, the list Ext_Atoms of 
      extensional body atoms and the list Edges1 of AS-Edges 
      constructed so far, for each ocurrence of U in the argument 
      list of one of the extensional an AS-edge is constructed and 
      added to the graph, leading to edgelist Edges2 */

u_edges(_,[],Edges,Edges,_).
u_edges(U,[Ext_Atom|Ext_Atoms],Edges1,Edges3,U_pos) :-
   Ext_Atom =.. [Functor|Args],
   member(U,Args),
   append(Edges1,[[Functor] - [U_pos] - 1/2],Edges2),
   u_edges(U,Ext_Atoms,Edges2,Edges3,U_pos).
u_edges(U,[_|Ext_Atoms],Edges1,Edges2,U_pos) :-
   u_edges(U,Ext_Atoms,Edges1,Edges2,U_pos).  


/* head_edges(Xs,Ext_Atoms,Edges2,Edges3) <- 
      Given the list Xs of arguments of the head atom, the list
      Ext_Atoms containing the extensional body atoms and the
      edgelist Edges2, the edges of the AS-Graph which are based on
      the sharing of arguments between the head atom and the exten-
      sional body atoms are constructed and added to the edgelist,
      leading to edgelist Edges3 */

head_edges(Xs,Ext_Atoms,Edges2,Edges3) :-
   head_edges(Xs,Ext_Atoms,Edges2,Edges3,1).

head_edges([],_,Edges,Edges,_).
head_edges([X|Xs],Ext_Atoms,Edges1,Edges3,X_pos) :-
   x_edges(X,Ext_Atoms,Edges1,Edges2,X_pos),
   X_pos_new is X_pos + 1,
   head_edges(Xs,Ext_Atoms,Edges2,Edges3,X_pos_new).


/* x_edges(X,Ext_Atoms,Edges1,Edges2,X_pos) <-
      Given an argument X of the head atom, its position X_pos in
      the argument list, the list Ext_Atoms of extensional body 
      atoms and the list Edges1 of edges constructed so far, for 
      each ocurrence of X in the argument list of one of the 
      extensional atoms an AS-edge is constructed and added to the 
      graph, leading to the edgelist Edges2 */

x_edges(_,[],Edges,Edges,_).
x_edges(X,[Ext_Atom|Ext_Atoms],Edges1,Edges3,X_pos) :-
   Ext_Atom =.. [Functor|Args],
   member(X,Args),
   append(Edges1,[[X_pos] - [Functor] - 1/2],Edges2),
   x_edges(X,Ext_Atoms,Edges2,Edges3,X_pos).
x_edges(X,[_|Ext_Atoms],Edges1,Edges2,X_pos) :-
   x_edges(X,Ext_Atoms,Edges1,Edges2,X_pos).



/* as_edges_to_folded_as_edges(Edges,Folded_Edges) <-
       applies the folding algorithm to the set Edges of edges of a
       graph, leading to the edge set Folded_Edges of the corres-
       ponding folded graph */

as_edges_to_folded_as_edges(Edges1,Edges3) :-
   cknf_fold_graph_step(Edges1,Edges2),
   as_edges_to_folded_as_edges(Edges2,Edges3).
as_edges_to_folded_as_edges(Edges,Edges).


cknf_fold_graph_step(Edges1,Edges2) :-
   cknf_branch_equal(Edges1,V1-W-L,V2-W-L),
   ord_union(V1,V2,V),
   rename_edges(Edges1,[V1-V,V2-V],Edges2).
cknf_fold_graph_step(Edges1,Edges2) :-
   cknf_branch_equal(Edges1,V-W1-L,V-W2-L),
   ord_union(W1,W2,W),
   rename_edges(Edges1,[W1-W,W2-W],Edges2).
cknf_fold_graph_step(Edges1,Edges3) :-
   cknf_branch_not_equal(Edges1,V1-W-L1,V2-W-L2),
   ord_del_element(Edges1,V2-W-L2,Edges2),
   cknf_subtract(L2,L1,L),
   ord_add_element(Edges2,V2-V1-L,Edges3).
cknf_fold_graph_step(Edges1,Edges3) :-
   cknf_branch_not_equal(Edges1,V-W1-L1,V-W2-L2),
   ord_del_element(Edges1,V-W2-L2,Edges2),
   cknf_subtract(L2,L1,L),
   ord_add_element(Edges2,W1-W2-L,Edges3).


cknf_subtract(1,1/2,1/2) :-
   !.
cknf_subtract(L2,L1,L) :-
   L is L2 - L1.


/* rename_edges(Edges1,Substitutions,Edges2) <-
       Given the set Edges1 of edges of a graph and the set
       Substitutions of substitutions (which are renaming vertices
       of the graph), the edge set Edges2 is computed. It is
       constructed applying the substitutions to the in- or
       outgoing vertices of the elements of Edges1. */

rename_edges(Edges1,[S|Ss],Edges3) :-
   maplist( rename_edge(S),
      Edges1, Edges2 ),
   rename_edges(Edges2,Ss,Edges3).
rename_edges(Edges1,[],Edges2) :-
   list_to_ord_set(Edges1,Edges2).

rename_edge(S,V1-W1-L,V2-W2-L) :-
   rename_node(V1,S,V2),
   rename_node(W1,S,W2).


/* rename_node(Node1,Substitution,Node2) <-
       carries out the substitution Substitution, thus transforming
       a node Node1 to the new node Node2 */

rename_node(U1,U1-U2,U2) :-
   !.
rename_node(V,_,V).


cknf_branch_equal(Edges,V1-W1-L,V2-W2-L) :-
   cknf_branch(Edges,V1-W1-L,V2-W2-L).
cknf_branch_not_equal(Edges,V1-W1-L1,V2-W2-L2) :-
   cknf_branch(Edges,V1-W1-L1,V2-W2-L2),
   L1 < L2.


cknf_branch(Edges,V1-W-L1,V2-W-L2) :-
   weighted_edge(Edges,V1-W-L1),
   weighted_edge(Edges,V2-W-L2),
   V1 \== V2.
cknf_branch(Edges,V-W1-L1,V-W2-L2) :-
   weighted_edge(Edges,V-W1-L1),
   weighted_edge(Edges,V-W2-L2),
   W1 \== W2.


/* weighted_edge(Edges,Edge) <-
       returns true, if edge Edge is a member of the edge set
       Edges */

weighted_edge(Edges,V-W-L) :-
   member(V-W-L,Edges).


/* edges_to_u_graph(Edges,U_Graph) <-
       Given the set Edges of edges of a given graph, the
       corresponding unweighted graph U_Graph is computed */

edges_to_u_graph(Edges,U_Graph) :-
   as_edges_to_u_edges(Edges,U_Edges),
   vertices_edges_to_ugraph([],U_Edges,U_Graph).


/* connected_components(Edges,CC) <-
      Given list Edges of the edges of a graph, the list CC of
      the connected components of the graph is returned. */

connected_components(Edges,CC) :-
   as_edges_to_u_edges(Edges,U_Edges),
   vertices_edges_to_ugraph([],U_Edges,U_Graph),
   reduce(U_Graph,Reduced_Graph),
   vertices(Reduced_Graph,CC).


/* as_edges_to_u_edges(AS_Edges,U_Edges) <-
      converts the edges AS_Edges of the (weighted) AS-Graph
      to the set U_Edges of unweighted edges */

as_edges_to_u_edges(AS_Edges,U_Edges) :-
   findall( U-V,
      member(U-V-_,AS_Edges),
      U_Edges ).


/******************************************************************/

 
