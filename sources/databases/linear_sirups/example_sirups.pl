

/******************************************************************/
/***                                                            ***/
/***         Declare:  CKNF-Sirups                              ***/
/***                                                            ***/
/******************************************************************/


test_sirup(N, Rule) :-
   ( N = 1, Rule = [p(x1,x2,x3,x4,x5,x6)] -
        [p(u1,u2,u3,x3,x5,u6)] -
        [a1(x1,u1),a2(x2,u3),a3(x4,u2),a4(x6)]
   ; N = 2, Rule = [p(x1,x2)] -
        [p(u1,u2)] -
        [a1(x1,u2),a2(x2,u1)]
   ; N = 3, Rule = [p(x1,x2,x3,x4,x5,x6,x7)] -
        [p(x2,x3,x1,x3,x3,u6,u6)] -
        [a1(x1,x4,x6),a2(x5,x7)]
   ; N = 4, Rule = [p(x1,x2,x3,x4)] -
        [p(x1,u1,u2,u3)] -
        [a1(x1,x2,u1,x3,x4)]
   ; N = 5, Rule = [p(x1,x2,x3,x4)] -
        [p(u1,u2,u3,x4)] -
        [a1(x1,x2,x4),a2(u1,x3)]
   ; N = 6, Rule = [p(x1,x2,x3,x4,x5,x6,x7)] -
        [p(u1,u2,u3,x4,u5,u6,x6)] -
        [a1(x1,x2,x4), a2(u1,x3), a3(x5,u6), a4(x7,u5)]
   ; N = 7, Rule = [p(x1,x2,x3,x4,x5,x6,x7)] -
        [p(u1,x2,u3,u4,x4,u6,x6)] -
        [a1(x1,x2,x4), a2(u1,x3), a3(x5,u6), a4(x7,u4)]
   ; N = 8, Rule = [p(x1,x2,x3)] -
        [p(u1,u2,u3)] -
        [a1(x1,u1,u2), a2(x2,x3,u3)] ).
test_sirup(9,
   [p(x1,x2,x3)] -
      [p(u1,x1,x2)] -
      [a1(x1,u1), a2(x3)]).
test_sirup(10,
   [p(x1,x2,x3,x4,x5,x6,x7)] -
      [p(x1,x2,x3,x2,x2,u3,u3)] - 
      [b1(x1,x4,x6,x5,x7), b2(x2,x3,u1), b3(x3,x1,u2)]).
test_sirup(11,
   [p(x1,x2,x3,x4)] -
      [p(u1,u2,u3,u4)] -
      [a1(x1,u1), a2(x2,u2), a3(x3,u3), a4(x4,u4)]).
test_sirup(12,
   [p(x1,x2,x3)] -
      [p(u1,u2,u3)] -
      [a1(x1,u2), a2(x2,u1), a3(x3), a4(u3)]).

test_sirup(13,
   [p(x1,x2,x3,x4,x5,x6)] - 
      [p(u1,u2,u3,u4,x5,u6)] -
      [a1(x1,u1), a2(x2,u2), a3(x4,u4), a4(x3,u3), a5(x6)]).
test_sirup(14,
   [p(x1,x2,x3,x4)] -
      [p(x1,u2,u3,u4)] -
      [a1(x1,x2,u2,x3), a2(x4,u4)]).
test_sirup(15,
   [p(x1,x2,x3,x4,x5)] -
      [p(u1,x1,x2,u4,u5)] -
      [a1(x3,u1), a2(x4,u5), a3(x5,u4)]).
test_sirup(16,
   [p(x1,x2,x3)] -
      [p(x1,u2,u3)] -
      [a1(x1,x2,u2),a2(x3,u3)]).
test_sirup(17,
   [p(x1,x2)] -
      [p(x1,u2)] -
      [a1(x1),a2(x2,u2)]).
test_sirup(18,
   [p(x1,x2,x3,x4,x5,x6)] - 
      [p(u1,u2,u3,u4,x5,u5)] -
      [b1(x1,u1), b2(x2,u2), b3(x4,u4), b4(x3,u3), b5(x6)]).
test_sirup(19,
   [p(x1,x2,x3,x4,x5,x6)] -
      [p(x2,x3,u3,x1,u5,u6)] -
      [a1(x6,u6), a2(x4,u5), a3(x3,u5)]).
test_sirup(20,
   [p(x1,x2,x3,x4,x5)] -
      [p(x2,u2,u3,x3,u5)] -
      [a1(x1), a2(x2,u3), a3(x4,x5)]).
test_sirup(21,
   [p(x1,x2,x3,x4,x5,x6)] -
      [p(u1,u2,x2,x4,u5,u6)] -
      [a1(x1), a2(x3,u2), a3(x5,u6), a4(x6,u5)]).
test_sirup(22,
   [p(x1,x2,x3,x4,x5,x6)] -
      [p(u1,u2,x2,x4,u5,u6)] -
      [a1(x1), a2(x3,u2), a3(x5,x6), a4(x4,x6,u5)]). 
test_sirup(23,
   [p(x1,x2,x3)] -
      [p(x2,x3,u3)] -
      [a1(x1,u3)]).
test_sirup(24,
   [p(x1,x2,x3,x4,x5,x6,x7)] -
      [p(u1,x3,x4,u4,u5,x6,u7)] -
      [a1(x1), a2(x2,u5), a3(x5,u4), a4(x7,u7)]).
test_sirup(25,
   [p(x1,x2,x3,x4)] -
      [p(u1,x2,u3,x2)] -
      [a1(x1,x3,u1), a2(x4,u3)]).
test_sirup(26,
   [p(x1,x2,x3,x4)] -
      [p(x1,u2,u3,u4)] -
      [a1(x1,x2,x3,u2,u3,x4)]).
test_sirup(27,
   [p(x1,x2)] -
      [p(u1,u2)] -
      [a1(x1,u2), a2(x2,u2)]).
test_sirup(28,
   [p(x1,x2,x3,x4)] -
      [p(u1,u2,u3,u4)] -
      [a1(x1,u1,u2), a2(x2,x3,u3), a3(x4,u4)]).


/******************************************************************/


