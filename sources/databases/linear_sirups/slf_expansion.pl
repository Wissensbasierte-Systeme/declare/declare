

/******************************************************************/
/***                                                            ***/
/***         DisLog:   Expansion of SLF-Rules                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* slf_rule_expand(Rule,L,G_L) <-
      Given the recursive rule Rule and the number L, the
      L-th iterate G_L of the SLD-resolution is computed. */

slf_rule_expand(H-IB-EB,L,G_L) :-
   slf_rule_expand(H-IB-EB,[H],1,L,Gs),
   first(Gs,G_L).

slf_rule_expand(Rule,[G1|Gs1],N,Max,Gs2) :-
   N =< Max,
   superscribe_rule(Rule,N,S_Rule),
   slf_rule_expand_goal(G1,S_Rule,G2),
   M is N + 1,
   slf_rule_expand(Rule,[G2,G1|Gs1],M,Max,Gs2).
slf_rule_expand(_,Gs,_,_,Gs).


/*** implementation ***********************************************/


/* superscribe_rule(Rule1,N,Rule2) ->
      Superscribes the given rule Rule1 with the number
      Number, leading to the superscribed rule Rule2. */

superscribe_rule(H1-IB1-EB1,N,H2-IB2-EB2) :-
   base_b_superscript(10,N,Script),
   superscribe_atoms(Script,H1,H2),
   superscribe_atoms(Script,IB1,IB2),
   superscribe_extensional_atoms(Script,EB1,EB2).


/* base_b_superscript(Base,Number,Super_Script) ->
      Given a number Number to base Base (usually 10), the
      corresponding superscript to Number is generated. */

base_b_superscript(Base,Number,Super_Script) :-
   Number < Base,
   !,
   Char is Number + 48,
   append("^",[Char],Super_Script).
base_b_superscript(Base,Number,Super_Script) :-
   base_b_list(Base,Number,D),
   append(["^{",D,"}"],Super_Script).


/* superscribe_atoms(Script,Atoms1,Atoms2) ->
      Superscribes the atoms of list Atoms1 of atoms with
      the script Script, yielding as result the list Atoms2
      of atoms. */

superscribe_atoms(Script,Atoms1,Atoms2) :-
   maplist( superscribe_atom(Script),
      Atoms1, Atoms2 ).

superscribe_atom(Script,Atom1,Atom2) :-
   Atom1 =.. [P|Terms1],
   superscribe_terms(Script,Terms1,Terms2),
   Atom2 =.. [P|Terms2].


/* superscribe_extensional_atoms(Script,Atoms1,Atoms2) <-
      Superscribes each of the atoms in Atoms1 with Script,
      yielding as resulting list Atoms2 of atoms. */

superscribe_extensional_atoms(Script,Atoms1,Atoms2) :-
   maplist( superscribe_extensional_atom(Script),
      Atoms1, Atoms2 ).

superscribe_extensional_atom(Script,Atom1,Atom2) :-
   Atom1 =.. Terms1,
   superscribe_terms(Script,Terms1,Terms2),
   Atom2 =.. Terms2.


superscribe_terms(Script,Terms1,Terms2) :-
   maplist( superscribe_term(Script),
      Terms1, Terms2 ).

superscribe_term(Script,Term1,Term2) :-
   name(Term1,List1),
   append(List1,Script,List2),
   name(Term2,List2).


/* slf_rule_expand_goal(Goal1,Rule,Goal2) :-
      Given the SLF rule Rule, the recursive atom in the goal
      Goal1 is resolved by Rule yielding a new goal Goal2. */

slf_rule_expand_goal(
      [P_Atom|Atoms],[Head_Atom]-IB-EB,Goal) :-
   Head_Atom =.. [_|H_Args],
   P_Atom =.. [_|P_Args],
   unify(P_Args,H_Args,Mgu),
   substitute(IB,Mgu,A),
   substitute(EB,Mgu,B),
   append([A,Atoms,B],Goal).


/******************************************************************/


