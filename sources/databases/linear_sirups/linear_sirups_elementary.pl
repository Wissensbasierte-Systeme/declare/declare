

/******************************************************************/
/***                                                            ***/
/***         Declare:   CKNF-Elementary                         ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(cknf, linear_sirup(all)) :-
   forall( test_sirup(N, Rule),
      ( write_list(['rule ', N, ' = ']),
        dwrite(rule, Rule) ) ).
% test(cknf, linear_sirup(1)) :-
%    test_sirup(1, Rule),
%    dwrite(rule, Rule).


/*** interface ****************************************************/


/* cknf_read_program(File, Program) <-
      */

cknf_read_program(File, Program) :-
   File_Alias = linear_sirup,
   open(File, read, _, [alias(File_Alias)]),
   cknf_read_program_(File_Alias, Program),
   close(File_Alias).

cknf_read_program_(Fd, Program) :-
   cknf_read_rule(Fd, Rule, Variables),
   ( Rule \= [end_of_file] ->
     cknf_read_program_(Fd, Rules),
     checklist( call, Variables ),
     Program = [Rule|Rules]
   ; Program = [] ).

cknf_read_rule(File, Rule_DisLog, Variables) :-
   read_variables(File, Rule_Prolog, Variables),
   prolog_rule_to_dislog_rule(Rule_Prolog, Rule),
   parse_rule(Rule, Rule_DisLog-[]).


list_non_empty(L) :-
   L \= [].

maplist_2(Pred,[X1|Xs1],[X2|Xs2]) :-
   Pred =.. L1,
   append(L1,[X1,X2],L2),
   Goal =.. L2,
   call(Goal),
   !,
   maplist_2(Pred,Xs1,Xs2).
maplist_2(Pred,[_|Xs1],Xs2) :-
   maplist_2(Pred,Xs1,Xs2).
maplist_2(_,[],[]).


dwrite(rules, Rules) :-
   checklist( dwrite(rule), Rules ).

dwrite(rule, [A]-[B]-Cs) :-
   dwrite(lp, [[A]-[B|Cs]]).


/******************************************************************/


