

   
/******************************************************************/
/***                                                            ***/
/***       MySQL Conversion:  Extraction from SquashML          ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      mysql_subquery_extract/3.


/* mysql_statements(Type, X) <-
      */

mysql_statements(all, X) :-
   mysql_statements(select, sqldata:As:Us),
   mysql_statements(create, sqldata:As:Vs),
   append(Us, Vs, Ws),
   X = sqldata:As:Ws.
mysql_statements(Type, X) :-
   member(Type, [select, create]),
   dislog_variable_get(example_path, Examples),
   concat([Examples, 'squash/', Type, '.xml'], File),
   dread(xml, File, [X]).


/*** interface ****************************************************/


/* mysql_statements_to_xml(Type) <-
      */

mysql_statements_to_xml(Type) :-
   mysql_statements_extract(Type, Xml),
   concat(Type, '.xml', File),
   dwrite(xml, File, Xml).


/* mysql_statements_extract(Type, Xml_1, Xml_2) <-
      */

mysql_statements_extract(Type, Xml) :-
   mysql_statements(Type, Xml_2),
   mysql_statements_extract(Type, Xml_2, Xml).

mysql_statements_extract(Type, Xml_1, Xml_2) :-
   ( Type = all,
     Type_2 = _
   ; Type_2 = Type ),
   findall( Statement,
      mysql_statement_extract(Type_2, Xml_1, Statement),
      Statements ),
   Xml_2 = statements:[type:Type]:Statements.


/* mysql_statement_extract(Type, Xml, Statement) <-
      */

mysql_statement_extract(select, Xml, Statement) :-
   Subquery := Xml/_/subquery::[@id=Id],
   mysql_subquery_extract(select, Subquery, Attributes),
   mysql_subquery_extract(from, Subquery, Tables),
   mysql_subquery_extract(where, Subquery, Comparisons),
   Statement = statement:[id:Id]:[
      select:Attributes, from:Tables, where:Comparisons ].

mysql_statement_extract(create, Xml, Statement) :-
   mysql_xml_to_table_columns(Xml, Table, Columns),
   Statement = create_table:[name:Table]:Columns,
   dwrite(xml, user, Statement).


/*** implementation ***********************************************/


/* mysql_subquery_extract(What, Subquery, Extraction) <-
      */

mysql_subquery_extract(from, Subquery, Tables) :-
   findall( Table,
      ( Ref := Subquery/from/table_reference/query_table_reference,
        query_table_reference_to_table(Ref, Table) ),
      Tables ).

query_table_reference_to_table(Ref, Table) :-
   T := Ref/query_table_expression
      /simple_query_table_expression@object,
   ( A := Ref/alias@name,
     Table = table:[name:T, alias:A]:[]
   ; Table = table:[name:T]:[] ),
   !.

mysql_subquery_extract(select, Subquery, Attributes) :-
   findall( Attribute,
      ( O := Subquery/select_list/expr/simple_expr/object,
        mysql_xml_attribute_extract(O, Attribute) ),
      Attributes ).

mysql_subquery_extract(where, Subquery, Comparisons) :-
   findall( Comparison,
      ( Condition := Subquery/where/condition,
        mysql_xml_condition_to_comparison(Condition, Comparison) ),
      Comparisons ).


/* mysql_xml_condition_to_comparison(Condition, Comparison) <-
      */

mysql_xml_condition_to_comparison(Condition, Comparison) :-
   Compound := Condition/
      compound_condition::[@operator=brackets],
%  writeq(user, Compound), nl,
   ( C := Compound/left_condition/condition
   ; C := Compound/right_condition/condition ),
%  writeq(user, C),
   mysql_xml_condition_to_comparison(C, Comparison).

mysql_xml_condition_to_comparison(Condition, Comparison) :-
   Compound := Condition/compound_condition::[@operator=and],
   Left := Compound/left_condition/condition,
   mysql_xml_condition_to_comparison(Left, L),
   Right := Compound/right_condition/condition,
   mysql_xml_condition_to_comparison(Right, R),
   Comparison = and:[]:[L, R].

mysql_xml_condition_to_comparison(Condition, Comparison) :-
   Simple := Condition/simple_comparison_condition,
   Op := Simple@operator,
   Left := Simple/left_expr/expr/simple_expr,
   Right := Simple/right_expr/expr/simple_expr,
   mysql_xml_attribute_extract_from_simple_expr(Left, L),
   mysql_xml_attribute_extract_from_simple_expr(Right, R),
   Comparison = comparison:[operator:Op]:[L, R].


/* mysql_xml_attribute_extract_from_simple_expr(S, Attribute) <-
      */

mysql_xml_attribute_extract_from_simple_expr(S, Attribute) :-
   O := S/object,
   mysql_xml_attribute_extract(O, Attribute).
mysql_xml_attribute_extract_from_simple_expr(S, Attribute) :-
   Attribute := S/number.
mysql_xml_attribute_extract_from_simple_expr(S, Attribute) :-
   Attribute := S/text.


/* mysql_xml_attribute_extract(O, Attribute) <-
      */

mysql_xml_attribute_extract(O, Attribute) :-
   A := O@column,
   ( T := O@table_view,
     Attribute = attribute:[name:A, alias:T]:[]
   ; Attribute = attribute:[name:A]:[] ),
   !.


/* mysql_xml_to_table_columns(Xml, Table_Name, Columns) <-
      */

mysql_xml_to_table_columns(Xml, Table_Name, Columns) :-
   _ := Xml/create_table::[@name=Table_Name],
   findall( column_def:As:[],
      As := Xml/create_table::[@name=Table_Name]
         /relational_properties/column_def/attributes::'*',
      Columns ).

 
/******************************************************************/


