

/******************************************************************/
/***                                                            ***/
/***       MySQL Conversion:  Selects to Prolog                 ***/
/***                                                            ***/
/******************************************************************/


mysql_call(Atom) :-
   mysql_call(wm_2006, Atom).


/*** interface ****************************************************/


/* mysql_query(N) <-
      */

mysql_query(N) :-
   clause(mysql_query(N, Attributes, _), _),
   findall( Row,
      mysql_query(N, _, Row),
      Rows ),
   concat('Query ', N, Title),
   xpce_display_table(_, _, Title, Attributes, Rows).


/* mysql_select_statements_to_program(Program) <-
      */

mysql_select_statements_to_program :-
   dread(xml, 'create.xml', [Creates]),
   dread(xml, 'select.xml', [Selects]),
   retract_rules(mysql_query/2),
   mysql_select_statements_to_program(
      Creates, Selects, Prolog),
   assert_rules(Prolog).

mysql_select_statements_to_program(
      Creates, Selects, Prolog) :-
   findall( Rule,
      ( Select := Selects/nth_child::N,
        mysql_select_statement_to_rule(
           Creates, Select, N, Rule) ),
      Program ),
   dportray(mysql_program, lp_xml, Program),
   dislog_rules_to_prolog_rules(Program, Prolog).


/* mysql_select_statement_to_rule(
         Creates, Statement, N, Rule) <-
      */

mysql_select_statement_to_rule(Creates, Statement, N, Rule) :-
   Select := Statement/select,
   From := Statement/from,
   Where := Statement/where,
   mysql_from_to_prolog_atoms(Creates, From, Atoms_1),
   mysql_select_to_prolog_atom(
      Creates, Atoms_1, Select, N, Atom),
   mysql_where_to_comparisons(
      Creates, Atoms_1, Where, Comparisons),
   ( foreach(Atom_1, Atoms_1), foreach(Atom_2, Atoms_2) do
        Atom_1 = _:A, Atom_2 = mysql_call(A) ),
   append(Atoms_2, Comparisons, Atoms),
   Rule = [Atom]-Atoms.


/*** implementation ***********************************************/


/* mysql_where_to_comparisons(
         Creates, Body, Where, Comparisons) <-
      */

mysql_where_to_comparisons(
      Creates, Body, Where, Comparisons) :-
   findall( Comparison,
      Comparison := Where/descendant::comparison,
      Comparisons_2 ),
   maplist( mysql_comparison_to_prolog(Creates, Body),
      Comparisons_2 , Comparisons ).

mysql_comparison_to_prolog(Creates, Body, Comparison, Atom) :-
   A1 := Comparison/nth_child::1,
   A2 := Comparison/nth_child::2,
   mysql_select_attribute_to_variable(Creates, Body, A1, X1),
   mysql_select_attribute_to_variable(Creates, Body, A2, X2),
   Operator := Comparison@operator,
   ( Operator = '=',
     X1 = X2
   ; Operator \= '=' ),
   Atom_2 =.. [Operator, X1, X2],
   Atom = atom_compare(Atom_2),
   !.

atom_compare(Comparison) :-
   Comparison =.. [Operator, X, Y],
   member(Operator, [<, >, =<, >=]),
   name_to_number(X, M),
   name_to_number(Y, N),
   apply(Operator, [M, N]).


/* mysql_select_to_prolog_atom(Creates, Body, Select, N, Atom) <-
      */

mysql_select_to_prolog_atom(Creates, Body, Select, N, Atom) :-
   findall( A,
      A := Select/attribute,
      As ),
   maplist( mysql_select_attribute_to_variable(Creates, Body),
      As, Xs ),
   findall( Attribute,
      Attribute := Select/attribute@name,
      Attributes ),
   Atom = mysql_query(N, Attributes, Xs).


/* mysql_select_attribute_to_variable(Creates, Body, A, X) <-
      */

mysql_select_attribute_to_variable(Creates, Body, A, X) :-
   Attribute := A@name,
   mysql_select_attribute_to_alias(A, Alias),
   member(Alias:Atom, Body),
   Atom =.. [Table|Variables],
   mysql_creates_table_and_attribute_to_position(
      Creates, Table, Attribute, N),
   nth(N, Variables, X).
mysql_select_attribute_to_variable(_, _, A, X) :-
   X := A@value.

mysql_select_attribute_to_alias(A, Alias) :-
   Alias := A@alias,
   !.
mysql_select_attribute_to_alias(_, _).

mysql_creates_table_and_attribute_to_position(
      Creates, Table, Attribute, N) :-
   Create := Creates/create_table::[@name=Table],
   _ := Create/nth_child::N::[@name=Attribute].


/* mysql_from_to_prolog_atoms(Creates, From, Atoms) <-
      */

mysql_from_to_prolog_atoms(Creates, From, Atoms) :-
   findall( Atom,
      ( T := From/table,
        mysql_from_table_to_alias_and_name(T, Alias:Table),
        mysql_alias_to_prolog_atom(
           Creates, Alias:Table, Atom) ),
      Atoms ).

mysql_from_table_to_alias_and_name(T, Alias:Table) :-
   Table := T@name,
   ( Alias := T@alias
   ; Alias = Table ),
   !.
 
mysql_alias_to_prolog_atom(Creates, Alias:Table, Atom) :-
   Columns := Creates
      /create_table::[@name=Table]/content::'*',
   length(Columns, N),
   n_free_variables(N, Variables),
   A =.. [Table|Variables],
   Atom = Alias:A.


/******************************************************************/


