

/******************************************************************/
/***                                                            ***/
/***          Declare:  Mongo DB Interface                      ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(mongodb_select, 1) :-
   Atom = employee:[sex:male],
%  Atom = employee:[ssn:1],
%  Template = result:[lname:Name, hours:H],
%  _Rule =
%     ( Template :-
%         employee:[ssn:X, lname:Name],
%         works_on:[essn:X, hours:H] ),
   mongodb_select(Atom).
test(mongodb_select, 2) :-
   Atom = b:[ssn:2],
   mongodb_select(Atom).


/*** interface ****************************************************/


/* mongodb_select(Database, Atom, File) <-
      */

mongodb_select(Atom) :-
   Database = collection,
   File = mongodb_results,
   mongodb_select(Database, Atom, File).

mongodb_select(Database, Atom, File) :-
   Atom = Table:[A:V],
   concat(['use test \n',
      'db.', Database, '.find({"', Table, '.', A, '":"', V,
      '" }, {''_id'':0})'],
      Command),
   predicate_to_file( mongodb_commands,
      writeln(Command) ),
   concat(['cat ', mongodb_commands, ' | mongo > ', File],
      OS_Command),
   writeln(user, OS_Command),
   us(OS_Command),
   mongodb_result_read(Results),
   writeln_list(Results),
   maplist( json_to_fn, Results, Fns ),
   dwrite(xml, Fns).


/* mongodb_result_read(Jsons) <-
      */

mongodb_result_read(Jsons) :-
   read_file_to_lines(mongodb_results, Lines_1),
   first_n_elements(4, Lines_1, Lines_2),
   append(Lines_2, Lines_3, Lines_1),
   delete_last_element(Lines_3, Lines_4),
   predicate_to_file( mongodb_results_2,
      writeln_list(Lines_4) ),
   File = mongodb_results_2,
   open(File, read, _Alias, [alias(input)]),
   mongodb_results_read(Jsons),
   close(input).

mongodb_results_read([Json|Jsons]) :-
%  catch(json:json_read_dict(input, Json), _,false),
   catch(json:json_read(input, Json), _,false),
   mongodb_results_read(Jsons).
mongodb_results_read([]).


/* json_to_fn(Json, Xml) <-
      */

json_to_fn(Jsons, Xml) :-
   is_list(Jsons), !,
   maplist( json_to_fn, Jsons, Xml ). 
json_to_fn(Json, Xml) :-
   Json = json(Jsons), !,
   maplist( json_to_fn, Jsons, Xml ). 
json_to_fn(Json, Xml) :-
   Json = (X=Y), !,
   json_to_fn(Y, Z),
   Xml = (X:Z). 
json_to_fn(Json, Json).


/******************************************************************/


