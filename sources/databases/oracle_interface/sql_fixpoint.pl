

/******************************************************************/
/***                                                            ***/
/***         DisLog: SQL Fixpoint                               ***/
/***                                                            ***/
/******************************************************************/


dislog_sql_fixpoint(
   asserted_join_store,tmp_join_store).
dislog_sql_fixpoint(
   asserted_no_of_j_tables,tmp_no_of_j_tables).
dislog_sql_fixpoint(
   asserted_f_t_arities,tmp_f_t_arities).
dislog_sql_fixpoint(
   asserted_j_conditions,tmp_j_conditions).
dislog_sql_fixpoint(
   asserted_j_t_atom_attributes,tmp_j_t_atom_attr).
dislog_sql_fixpoint(
   table_alias_name,alias).
dislog_sql_fixpoint(
   delta_table_suffix,'_delta').


/*** interface ****************************************************/


/* tps_fixpoint_iteration_sql(Program,State) <-
      */

tps_fixpoint_iteration_sql(Program,State) :-
   prepare_tps_fixpoint_iteration(Program),
   !,
   iterate_fixpoint(2),   
   tps_load_state_from_f_tables(State).

iterate_fixpoint(Start_No) :-
   write_list(['tps_iteration_sql ',Start_No,':\n\n']),
   sql_tps_single_iteration,
   nl,   
   succ(Start_No,New_Start_No),
   iterate_fixpoint(New_Start_No).
iterate_fixpoint(_).


/* tps_delta_fixpoint_iteration_sql(Program,State) <-
      */

tps_delta_fixpoint_iteration_sql(Program,State) :-
   prepare_tps_fixpoint_delta_iteration(Program),
   !,
   iterate_delta_fixpoint(2),
   tps_load_state_from_f_tables(State).

iterate_delta_fixpoint(Start_No) :-
   write_list(['tps_delta_iteration_sql ',Start_No,':\n\n']),
   sql_tps_single_delta_iteration,
   nl,
   succ(Start_No,New_Start_No),
   iterate_delta_fixpoint(New_Start_No).
iterate_delta_fixpoint(_).


/*** implementation ***********************************************/


/* prepare_tps_fixpoint_iteration(Program) <-
      */

prepare_tps_fixpoint_iteration(Program) :-
   delete_tmp_flags,
   sql_drop_tables,
   split_program_into_facts_and_rules(Program,Facts_0,Rules),
   state_can(Facts_0,Facts),
   f_t_arities_from_state(Facts,Facts_F_T_Arities),
   f_t_arities_from_rules(Rules,Rules_F_T_Arities),
   ord_union(Facts_F_T_Arities,Rules_F_T_Arities,F_T_Arities),  
   store(f_t_arities,F_T_Arities),
   tps_create_f_tables(f_t_arities,F_T_Arities),
   tps_insert_in_f_tables(state,Facts),
   tps_create_j_tables(rules,Rules), 
   rules_to_enum_rules(Rules,Enum_Rules),
   sql_join_commands_from_enum_rules(Enum_Rules,Join_Commands),
   store(sql_join_commands,Join_Commands),
   load(join_conditions,J_Conditions),
   tps_create_f_t_index(j_conditions,J_Conditions).
   

/* sql_tps_single_iteration <-
      */

sql_tps_single_iteration :-
   load(sql_join_commands,Sql_Join_Commands),
   oracle_send_commands_without_eot(Sql_Join_Commands),
   tps_load_resolvents_from_j_tables(Res),
   delete(Res,[],Resolvents),
   tps_delete_all(j_tables),
   tps_load_state_from_f_tables(Old_State),
   state_union_and_subsumtion(Resolvents,Old_State,
      N_Consequences,B_Subsumed_State),
   write(N_Consequences),nl,
   !,
   N_Consequences \== [],
   tps_delete_from_f_tables(state,B_Subsumed_State),
   tps_insert_in_f_tables(state,N_Consequences).


/* prepare_tps_fixpoint_delta_iteration(Program) <-
      */

prepare_tps_fixpoint_delta_iteration(Program) :-
   delete_tmp_flags,
   sql_drop_tables,
   split_program_into_facts_and_rules(Program,Facts_0,Rules),
   state_can(Facts_0,Facts),
   f_t_arities_from_state(Facts,Facts_F_T_Arities),
   f_t_arities_from_rules(Rules,Rules_F_T_Arities),
   ord_union(Facts_F_T_Arities,Rules_F_T_Arities,F_T_Arities),
   store(f_t_arities,F_T_Arities),
   f_t_arities_to_delta_f_t_arities(F_T_Arities,Delta_F_T_Arities),
   tps_create_f_tables(f_t_arities,F_T_Arities),
   tps_create_f_tables(f_t_arities,Delta_F_T_Arities),   
   tps_insert_in_f_tables(state,Facts),
   tps_insert_in_delta_f_tables(state,Facts),
   tps_create_j_tables(rules,Rules),
   rules_to_enum_rules(Rules,Enum_Rules),
   enum_rules_to_enum_delta_rules(Enum_Rules,Enum_D_Rules),
   sql_join_commands_from_enum_rules(Enum_D_Rules,Join_Commands),
   store(sql_join_commands,Join_Commands),
   load(join_conditions,Join_Conditions),
   tps_create_f_t_index(j_conditions,Join_Conditions).
   

/* sql_tps_single_delta_iteration <-
      */

sql_tps_single_delta_iteration :-
   load(sql_join_commands,Sql_Join_Commands),
   oracle_send_commands_without_eot(Sql_Join_Commands),
   tps_load_resolvents_from_j_tables(Res1),
   delete(Res1,[],Res),
   tps_delete_all(j_tables),
   tps_load_state_from_f_tables(State),
   state_union_and_subsumtion(Res,State,D_State,B_Subsumed_State),
   write(D_State),nl,
   !,
   D_State \== [],
   tps_delete_from_f_tables(state,B_Subsumed_State),
   tps_insert_in_f_tables(state,D_State),
   tps_replace_state_in_delta_f_tables(D_State).

   
/* state_union_and_subsumtion(
         Resolvents,State,New_Consequences, B_Subsumed_State) <-
      */

state_union_and_subsumtion(Resolvents,State,New_Consequences,
      B_Subsumed_State) :-
   state_prune(Resolvents,Resolvents1),
   state_subtract(Resolvents1,State,Resolvents_Diff),
   state_can(Resolvents_Diff,Resolvents_Diff1),
   New_Consequences = Resolvents_Diff1,
   state_subtract(State,New_Consequences,State_Diff),
   ord_subtract(State,State_Diff,B_Subsumed_State).


/* rules_to_enum_rules(Rules,Enumerated_Rules) <-
      enumerates the rules [...] with their position
      in the rule part of the program:
      e.g. [1,[...]], for the first rule. */

rules_to_enum_rules(Rules,Enumerated_Rules) :-
   rules_to_enum_rules(Rules,1,Enumerated_Rules).

rules_to_enum_rules([Rule|Rules],No,[E_Rule|E_Rules]) :-
   E_Rule = [No,Rule],
   succ(No,New_No),
   rules_to_enum_rules(Rules,New_No,E_Rules).
rules_to_enum_rules([],_,[]).


/* enum_rules_to_enum_delta_rules(Enum_Rules,Enum_Delta_Rules) <-
      generates from the enumerated rules Enum_Rules the
      delta-tables using Enum_Delta_Rules */
  
enum_rules_to_enum_delta_rules(Enum_Rules,Enum_Delta_Rules) :-
   enum_rules_to_enum_delta_rules(Enum_Rules,[],Enum_Delta_Rules).

enum_rules_to_enum_delta_rules([E_R|E_Rs],Sofar,Enum_Delta_Rules) :-
   E_R = [No,Head - Body],
   dislog_sql_fixpoint(delta_table_suffix,Suffix),
   enum_rule_to_enum_delta_rule(No,1,Head,Body,Suffix,E_D_Rs),
   append(Sofar,E_D_Rs,New_Sofar),
   enum_rules_to_enum_delta_rules(E_Rs,New_Sofar,Enum_Delta_Rules).
enum_rules_to_enum_delta_rules([],Sofar,Sofar).

enum_rule_to_enum_delta_rule(No,C,Head,Body,S,[D_Rule|D_Rules]) :-
   nth(C,Body,Atom),
   Atom =.. [Pred|Args],
   concat(Pred,S,Delta_Pred),
   Delta_Atom =.. [Delta_Pred|Args], 
   replace_element(Body,C,Delta_Atom,D_Body),
   D_Rule = [No,Head - D_Body],
   succ(C,New_C),
   enum_rule_to_enum_delta_rule(No,New_C,Head,Body,S,D_Rules).
enum_rule_to_enum_delta_rule(_,_,_,_,_,[]).


/* sql_join_commands_from_enum_rules(Rules,SQL_Commands) <-
      compiles the enumerated Rules to SQL_Join_Commands */          

sql_join_commands_from_enum_rules(Rules,Commands) :-
   sql_join_commands_from_enum_rules(Rules,[],Commands).

sql_join_commands_from_enum_rules([E_Rule|E_Rules],Sofar,
      Commands) :-
   E_Rule = [No,Rule],
   disjunctive_rule_to_sql_commands(Rule,No,Comm1,Comm2),
   append([Sofar,[Comm1],[Comm2]],New_Sofar),
   sql_join_commands_from_enum_rules(E_Rules,New_Sofar,Commands).
sql_join_commands_from_enum_rules([],Sofar,Sofar).


/* resolvents_to_state(Resolvents,State) <-
      Resolvent and state subsumption. */

resolvents_to_state([Resolvent|Resolvents],[Conseq|Conseqs]) :-
   resolvent_local_subsumtion(Resolvent,Conseq),
   resolvents_to_state(Resolvents,Conseqs).
resolvents_to_state([],[]).

resolvent_local_subsumtion(Resolvent_Info,Resolvent) :-
   Resolvent_Info = [A-B,C],
   list_to_ord_set(A,Ord_Set_Head),
   list_to_ord_set(B,Ord_Set_Body),
   flatten(C,C1),
   list_to_ord_set(C1,Ord_Set_Body_Disj),
   ord_disjoint(Ord_Set_Head,Ord_Set_Body),
   ord_disjoint(Ord_Set_Body,Ord_Set_Body_Disj),
   ord_union(Ord_Set_Head,Ord_Set_Body_Disj,Resolvent).
resolvent_local_subsumtion(_,[]).


/* delete_tmp_flags <-
      Temporary informations to accelerate tps-iteration. */

delete_tmp_flags :-
   dislog_sql_fixpoint(asserted_no_of_j_tables,Pred_Name1),
   dislog_sql_fixpoint(asserted_f_t_arities,Pred_Name2),
   dislog_sql_fixpoint(asserted_j_t_atom_attributes,Pred_Name3),
   dislog_sql_fixpoint(asserted_j_conditions,Pred_Name4),
   dislog_sql_fixpoint(asserted_join_store,Pred_Name5),
   abolish(Pred_Name1,1),
   abolish(Pred_Name2,1),
   abolish(Pred_Name3,1),
   abolish(Pred_Name4,1),
   abolish(Pred_Name5,1).


load(no_of_j_tables,No) :-
   dislog_sql_fixpoint(asserted_no_of_j_tables,Pred_Name),
   tps_info_load(Pred_Name,No).

load(f_t_arities,F_T_Arities) :-
   dislog_sql_fixpoint(asserted_f_t_arities,Pred_Name),
   tps_info_load(Pred_Name,F_T_Arities).

load(f_t_names,F_T_Names) :-
   load(f_t_arities,F_T_Arities),
   f_t_arities_to_f_t_names(F_T_Arities,F_T_Names).

load(delta_f_t_names,Delta_F_T_Names) :-
   load(f_t_arities,F_T_Arities),
   f_t_arities_to_delta_f_t_arities(F_T_Arities,D_F_T_Arities),
   f_t_arities_to_f_t_names(D_F_T_Arities,Delta_F_T_Names).

load(j_t_atom_attributes,Atom_Attributes) :-
   dislog_sql_fixpoint(asserted_j_t_atom_attributes,Pred_Name),
   tps_info_load(Pred_Name,Atom_Attributes).

load(j_t_atom_attributes,No_of_J_Table,Atom_Attributes) :-
   load(j_t_atom_attributes,All_Atom_Attributes),
   nth1(No_of_J_Table,All_Atom_Attributes,Atom_Attributes).

load(join_conditions,Join_Conditions) :-
   dislog_sql_fixpoint(asserted_j_conditions,Pred_Name),
   tps_info_load(Pred_Name,Join_Conditions).

load(sql_join_commands,Sql_Join_Commands) :-
   dislog_sql_fixpoint(asserted_join_store,Pred_Name),
   tps_info_load(Pred_Name,Sql_Join_Commands).


store(no_of_j_tables,No) :-
   dislog_sql_fixpoint(asserted_no_of_j_tables,Pred_Name),
   tps_info_store(Pred_Name,No).

store(f_t_arity,F_T_Arity) :-
   store(f_t_arities,[F_T_Arity]).

store(f_t_arities,F_T_Arities) :-
   dislog_sql_fixpoint(asserted_f_t_arities,Pred_Name),
   tps_info_append(Pred_Name,F_T_Arities).

store(j_t_atom_attributes,Atom_Attributes) :-
   dislog_sql_fixpoint(asserted_j_t_atom_attributes,Pred_Name),
   tps_info_append(Pred_Name,[Atom_Attributes]).

store(join_conditions,J_Conditions) :-
   dislog_sql_fixpoint(asserted_j_conditions,Pred_Name),
   tps_info_append(Pred_Name,J_Conditions).

store(sql_join_commands,Sql_Join_Commands) :-
   dislog_sql_fixpoint(asserted_join_store,Pred_Name),
   tps_info_store(Pred_Name,Sql_Join_Commands).


tps_info_store(Pred_Name,Arguments) :-
    abolish(Pred_Name,1),
    Fact =.. [Pred_Name,Arguments],
    assert(Fact).


tps_info_append(Pred_Name,Add_Values) :-
   tps_info_load(Pred_Name,Values),
   append(Values,Add_Values,New_Values),
   Old_Fact =.. [Pred_Name,Values],
   New_Fact =.. [Pred_Name,New_Values],
   retract(Old_Fact),
   assert(New_Fact).
tps_info_append(Pred_Name,Add_Values) :-
   tps_info_store(Pred_Name,Add_Values).


tps_info_load(Pred_Name,Value) :-
   Fact =.. [Pred_Name,_],
   clause(Fact,_),
%  current_functor(Pred_Name,1),
   Fact =.. [Pred_Name,Value],
   call(Fact).


/* tps_create_f_tables(f_t_arities,Info) <-
      Same functionalty like sql_create_table_from_def_info,
      but without Add_On_Name, plus adding a column for the
      disjunctions. */

tps_create_f_tables(f_t_arities,[I1|I2]) :-
   I1 = Pred / Arity,
   disjunctive_column_def(_,_,Arg_,Arg_Type,Disj_,Disj_Type),
   generate_column_descriptors(Arg_,Arity,Arg_Type,Descriptor1),
   concat(Disj_,'1',Disj_Column),
   Descriptor2 = [Disj_Column - Disj_Type],
   append(Descriptor1,Descriptor2,Final_Descriptor),
   sql_create_table(Pred,Final_Descriptor),
   tps_create_f_tables(f_t_arities,I2).
tps_create_f_tables(f_t_arities,[]).


/* tps_create_j_tables(rules,List_of_Rules) <-
      Creates for every rule an adequate join-table,
      the join-table names are numerated as their positions in
      the List_of_Rules. */

tps_create_j_tables(rules,List_of_Rules) :-
   tps_create_j_tables(rules,List_of_Rules,1).

tps_create_j_tables(rules,[R1|R2],No) :-
   disj_rule_to_column_descriptors(R1,Descriptors),
   disjunctive_j_table_prefix_name(Table_Name),
   concat(Table_Name,No,Table_Name1),
   sql_create_table(Table_Name1,Descriptors),
   !,
   get_rule_atom_attributes(R1,J_T_Atom_Attributes),
   store(j_t_atom_attributes,J_T_Atom_Attributes),
   succ(No,No1),
   tps_create_j_tables(rules,R2,No1).
tps_create_j_tables(rules,[],No1) :-
   succ(No,No1),
   store(no_of_j_tables,No),
   !.


/* tps_create_f_t_index(j_conditions,J_Conditions) <-
      generates, in dependance of Join_Conditions
      some indexes to accelerate database join operations */

tps_create_f_t_index(j_conditions,J_Conditions) :-
   flatten(J_Conditions,J_Conditions),
   correct_aliases_delete_const(J_Conditions,J_Conditions1),
   tps_create_f_table_index_from_j_conditions(J_Conditions1).

correct_aliases_delete_const([Col|Cols],C_Cols) :-
   atomic(Col),
   correct_aliases_delete_const(Cols,C_Cols).
correct_aliases_delete_const([Col|Cols],[C_Col|C_Cols]) :-
   dislog_sql_fixpoint(table_alias_name,Alias),
   Col =.. [Colname,Arg],
   name(Colname,Col_Str),
   split_list(Col_Str,95,First_Str,Rest_Str),
   name(First,First_Str),
   Alias == First,
   Rest_Str = [_|Rest_Str_1],
   name(Rest,Rest_Str_1),
   C_Col =.. [Rest,Arg],
   correct_aliases_delete_const(Cols,C_Cols).
correct_aliases_delete_const([Col|Cols],[Col|C_Cols]) :-
   correct_aliases_delete_const(Cols,C_Cols).
correct_aliases_delete_const([],[]).

tps_create_f_table_index_from_j_conditions(Elements) :-
   tps_create_f_table_index_from_j_conditions(Elements,[]).

tps_create_f_table_index_from_j_conditions([El|Els],Sofar) :-
   El =.. [Table_Name,Column],
   disjunctive_column_def(_,_,Arg,_,_,_),
   concat(Arg,Column,Column_Name),
   concat_atom(['i_',Column_Name,'_',Table_Name],Index_Name),
   \+ member(Index_Name,Sofar),
   append(Sofar,[Index_Name],New_Sofar),
   sql_create_index(Table_Name,Column_Name,Index_Name),
tps_create_f_table_index_from_j_conditions(Els,New_Sofar).
tps_create_f_table_index_from_j_conditions([_|Els],Sofar) :-
   tps_create_f_table_index_from_j_conditions(Els,Sofar).
tps_create_f_table_index_from_j_conditions([],_).


/* sql_create_index(Table,Column,Index) <-
      */

sql_create_index(Table,Column,Index) :-
   concat_atom(['CREATE INDEX ',Index,' ON ',Table,
      ' (',Column,')'],Command),
   oracle_send_command_without_eot(Command).


/* tps_delete_from_f_tables(state,Facts) <-
      deletes all facts Facts from F-Tables
      in the database */

tps_delete_from_f_tables(state,Facts) :-
   d_facts_to_sym_r_states(Facts,R_States),
   tps_delete_from_f_tables(r_state,R_States).


/* tps_delete_from_f_tables(r_state,R_State) <-
      deletes the Raja-State R_State from the
      existing state in the database */

tps_delete_from_f_tables(r_state,[R_Fact|R_Facts]) :-
    oracle_connect,
    tps_get_delete_r_fact_command(R_Fact,Command),
    oracle_send_command_without_eot(Command),
    tps_delete_from_f_tables(r_state,R_Facts).
tps_delete_from_f_tables(r_state,[]).

tps_get_delete_r_fact_command(Atom - Disjunctives,Command) :-
    Atom =.. [Pred|Args],
    atom_to_where_condition(Pred,1,Args,'',Cond),
    atoms_to_sql_string(Disjunctives,Values2),
    disjunctive_column_def(_,_,_,_,Disj_,_),
    concat_atom([Pred,'.',Disj_,1,'='],Disj_1),
    concat_atom(['DELETE FROM ',Pred,' WHERE ',
       Cond,Disj_1,Values2],Command).

atom_to_where_condition(Pred,No,[Arg|Args],Sofar,Cond) :-
   disjunctive_column_def(_,_,Arg_,_,_,_),
   name(X,"'"),
   concat_atom([X,Arg,X],Sql_Arg),
   concat_atom([Pred,'.',Arg_,No,'=',Sql_Arg,' AND '],S),
   concat(Sofar,S,New_Sofar),
   succ(No,New_No),
   atom_to_where_condition(Pred,New_No,Args,New_Sofar,Cond).
atom_to_where_condition(_,_,[],Sofar,Sofar) .


/* tps_delete_all(j_tables) <-
     deletes all tuples of the exiting J-Tables */

tps_delete_all(j_tables) :-
   load(no_of_j_tables,No),
   disjunctive_j_table_prefix_name(Join_),
   generate_list_of_successors(1,No,Successors),
   product_atom_list(Join_,Successors,Table_Names),
   sql_delete_tables2(Table_Names).


/* tps_delete_all(f_tables) <-
      deletes all tuples of the existing F-Tables */

tps_delete_all(f_tables) :-
   load(f_t_names,F_T_Names),
   sql_delete_tables2(F_T_Names).


/* tps_delete_all(delta_f_tables) <-
      deletes all tuples of the existing Delta-F-Tables */

tps_delete_all(delta_f_tables) :-
   load(delta_f_t_names,Delta_F_T_Names),
   sql_delete_tables2(Delta_F_T_Names).


/* tps_insert_in_f_tables(state,State) <-
      inserts the state State in the appropriate
      F-Tables */

tps_insert_in_f_tables(state,State) :-
   d_facts_to_sym_r_states(State,Sym_R_States),
   tps_insert_in_f_tables(r_states,Sym_R_States).


/* tps_insert_in_f_tables(r_state,R_State) <-
      inserts the r_state R_State in the appropriate
      F-Tables */

tps_insert_in_f_tables(r_states,R_State) :-
   oracle_connect,
   tps_get_insert_r_state_commands(R_State,Commands),
   oracle_send_commands_without_eot(Commands).


/* tps_get_insert_r_state_commands(R_State,Commands) <-
      facts of the form [a(v,c)- [a,b,c]] */

tps_get_insert_r_state_commands(R_State,Commands) :-
   tps_get_insert_commands1(R_State,'',Commands).


/* tps_insert_in_delta_f_tables(state,State) <-
      tries to insert State in Delta Fact Tables */

tps_insert_in_delta_f_tables(state,State) :-
   d_facts_to_sym_r_states(State,Sym_R_State),
   tps_insert_in_delta_f_tables(r_state,Sym_R_State).


/* tps_insert_in_delta_f_tables(r_state,R_State) <-
      tries to insert R_State in Delta Fact Tables */

tps_insert_in_delta_f_tables(r_state,R_State) :-
   oracle_connect,
   dislog_sql_fixpoint(delta_table_suffix,Delta),
   tps_get_insert_commands1(R_State,Delta,Commands),
   oracle_send_commands_without_eot(Commands).

tps_get_insert_commands1([R_Fact|R_Facts],Table_Suffix,[C|Cs]) :-
   tps_get_insert_command2(R_Fact,Table_Suffix,C),
   tps_get_insert_commands1(R_Facts,Table_Suffix,Cs).
tps_get_insert_commands1([],_,[]).

tps_get_insert_command2(Atom-Disj_Rest,Table_Suffix,Command) :-
   Atom =.. [Pred|Args],
   concat(Pred,Table_Suffix,Table_Name),
   sql_values_from_args(Args,Values1),
   atoms_to_sql_string(Disj_Rest,Values2),
   concat_atom(['INSERT INTO ',Table_Name,' VALUES ','(',
      Values1,',',Values2,')'],Command).


/* tps_replace_state_in_f_tables(State) <-
      Deletes the old state and inserts State in the
      Fact Tables. */

tps_replace_state_in_f_tables(State) :-
   delete_contents_of_f_tables,
   d_facts_to_sym_r_states(State,R_State),
   tps_get_insert_r_state_commands(R_State,Sql_Insert_Commands),
   oracle_send_commands_without_eot(Sql_Insert_Commands).


/* tps_replace_state_in_delta_f_tables(Delta_State) <-
      Deletes the old delta-state and inserts Delta State
      in the Delta Tables. */

tps_replace_state_in_delta_f_tables(Delta_State) :-
   load(f_t_arities,F_T_Arities),

f_t_arities_to_delta_f_t_arities(F_T_Arities,Delta_F_T_Arities),
   f_t_arities_to_f_t_names(Delta_F_T_Arities,Delta_F_T_Names),
   sql_delete_tables2(Delta_F_T_Names),
   tps_insert_in_delta_f_tables(state,Delta_State).


/* tps_load_state_from_f_tables(State) <-
      loads the state State from the existing F-Tables */

tps_load_state_from_f_tables(State) :-
   load(f_t_arities,F_T_Arities),
   tps_load_state_from_f_tables(F_T_Arities,'',State1),
   list_to_ord_set(State1,State).


tps_load_state_from_f_tables(Arities,Suffix,State) :-
   tps_load_state_from_f_tables(Arities,Suffix,[],State).

tps_load_state_from_f_tables(
      [Arity|Arities],Suffix,Sofar,State) :-
   sql_table_to_state(Arity,Suffix,S1),
   append(Sofar,S1,New_Sofar),
   tps_load_state_from_f_tables(
      Arities,Suffix,New_Sofar,State).
tps_load_state_from_f_tables([],_,Sofar,Sofar).


/* tps_load_resolvents_from_j_tables(State) <-
      Generates a state of all join tables,
      but the state is neither canonized nor pruned. */

tps_load_resolvents_from_j_tables(State) :-
   load(no_of_j_tables,No_of_J_Tables),
   a_s_j_t_t_s(No_of_J_Tables,1,[],State).

a_s_j_t_t_s(No_of_J_Tables,Act_No,Sofar,State) :-
   Act_No =< No_of_J_Tables,
   tps_load_resolvents_from_j_table(Act_No,State1),
   append(Sofar,State1,New_Sofar),
   succ(Act_No,New_Act_No),
   a_s_j_t_t_s(No_of_J_Tables,New_Act_No,New_Sofar,State).
a_s_j_t_t_s(_,_,State,State).


/* tps_load_resolvents_from_j_table(J_Table_No,State) <-
      Generates the resolvents from the resolvent tables by
      transforming the resolvent information into resolvents,
      with local subsumption. */

tps_load_resolvents_from_j_table(J_Table_No,State) :-
   tps_j_table_to_resolvents(J_Table_No,Resolvents),
   resolvents_to_state(Resolvents,State).

tps_j_table_to_resolvents(No_of_J_Table,Resolvents) :-
   disjunctive_j_table_prefix_name(Join_),
   concat(Join_,No_of_J_Table,Table_Name),
   sql_load_table(Table_Name,Information),
   load(j_t_atom_attributes,No_of_J_Table,Struc),
   t_info_to_res_info(Information,Struc,Resolvents).


/*** specific rule informations ***/


get_rule_atom_attributes(Rule,J_T_Atom_Attributes) :-
   rule_atom_attributes(Rule,Head_Attr,Body_Attr,Disj_Attr),
   J_T_Atom_Attributes = [Head_Attr,Body_Attr,Disj_Attr].


rule_atom_attributes(Rule,P_H_Val,P_B_Val,P_D_Val) :-
   rule_values(Rule,H_Values,B_Values,D_Values),
   size_values_to_position_values(1,H_Values,Stop1,P_H_Val),
   succ(Stop1,Start2),
   size_values_to_position_values(Start2,B_Values,Stop2,P_B_Val),
   succ(Stop2,Start3),
   size_values_to_position_values(Start3,D_Values,_,P_D_Val).

rule_values(Rule,H_Value,B_Value,D_Value) :-
   Rule = Head-Body,
   atom_values(Head,H_Value),
   atom_values(Body,B_Value),
   length(Body,D_Length),
   disj_values(D_Length,D_Value).

atom_values([Atom|Atoms],[Value|Values]) :-
   Atom =.. List,
   length(List,Value),
   atom_values(Atoms,Values).
atom_values([],[]).


disj_values(No,[1|Values]) :-
   No > 0,
   succ(New_No,No),
   disj_values(New_No,Values).
disj_values(_,[]).


/* disj_rule_to_column_descriptors(Rule,Column_Descriptors) <-
      generates the Column_Descriptors of a table, responsible
      for the join operation of Rule */

disj_rule_to_column_descriptors(Rule,Column_Descriptors) :-
   Rule = Head-Body,
   f_t_arities_from_atoms(Head,Head_Info),
   t_arities_to_column_descriptors(Head_Info,Descriptor1),
   disjunctive_column_prefix_name(Prefix),
   f_t_arities_from_atoms(Body,Body_Info),
   t_arities_to_column_descriptors(Body_Info,Descriptor2,Prefix),
   length(Body,Body_Length),
   disjunctive_column_def(_,_,_,_,Disj_Col,Disj_Col_Def),
   generate_column_descriptors(Disj_Col,Body_Length,Disj_Col_Def,
      Descriptor3),
   append([Descriptor1,Descriptor2,Descriptor3],Column_Descriptors).
disj_rule_to_columns_descriptors(Rule,_) :-
   Rule = _-_-_,
   !,
   fail.


t_arities_to_column_descriptors(Table_Infos,Col_Descriptor) :-
   t_arities_to_column_descriptors(Table_Infos,Col_Descriptor,'').

t_arities_to_column_descriptors(Infos,Col_Descr,Prefix) :-
   t_arities_to_column_descr(Infos,Prefix,[],1,Col_Descr).

t_arities_to_column_descr([A|As],Prefix,Sofar,No,Descr1) :-
   A = _/Arity,
   disjunctive_column_def(Pred_Col,Pred_Col_Type,Arg_Col,
   Arg_Col_Type,_,_),
   concat_atom([Prefix,Pred_Col,No],Pred_Col_Name),
   C1 = [Pred_Col_Name - Pred_Col_Type],
   concat_atom([Prefix,Arg_Col,No,'_'],Arg_Col_Name),


generate_column_descriptors(Arg_Col_Name,Arity,Arg_Col_Type,C2),
   append([Sofar,C1,C2],New_Sofar),
   succ(No,No1),
   t_arities_to_column_descr(As,Prefix,New_Sofar,No1,Descr1).
t_arities_to_column_descr([],_,Sofar,_,Sofar).


/* generate_column_descriptors(Base_Name,No_Columns,Type,Info) <-
      generates info list of columns,
      Info := [Base_Name1-Type,Base_Name2-Type,...] */

generate_column_descriptors(Base_Name,Number_Columns,Type,Info):-
   generate_column_descriptors2(Base_Name,Number_Columns,1,Type,[],
       Info).

generate_column_descriptors2(B_Name,Number,Act_Number,
       Type,Tmp_Info,Info) :-
    Number >= Act_Number,
    concat(B_Name,Act_Number,Col_Name),
    Col_Id = Col_Name-Type,
    append(Tmp_Info,[Col_Id],Tmp_Info1),
    succ(Act_Number,Act_Number1),
    generate_column_descriptors2(B_Name,Number,Act_Number1,Type,
       Tmp_Info1,Info).
generate_column_descriptors2(_,_,_,_,Info,Info).


disjunctive_rule_to_sql_commands(Rule,Rule_No,Command1,Command2):-
   split_rule_replace_variables(Rule,Head,Body),
   disjunctive_j_table_prefix_name(Table_Prefix),
   concat(Table_Prefix,Rule_No,J_T_Name),
   build_insert_line(J_T_Name,Head,Body,Line1),
   new_body_with_aliases(Body,New_Body,Alias_Substitutions),
   build_select_line(Head,New_Body,Line2),
   build_from_line(Body,Alias_Substitutions,Line3),
   build_where_line(New_Body,Line4),
   build_update_command(J_T_Name,Head,Body,Command2),
   concat_command_1(Line1,Line2,Line3,Line4,Command1).
  
 
concat_command_1(Line1,Line2,Line3,Line4,Command1) :-
   return_atom(Return),
   concat_atom([Line1,Return,Line2,Return,Line3],C1),
   concat_where_line(C1,Line4,Command1).

concat_where_line(C,Line4,C) :-
   Line4 == [].
concat_where_line(C,Line4,Command) :-
   return_atom(Return),
   concat_atom([C,Return,Line4],Command). 


build_insert_line(J_T_Name,Head,Body,Line) :-
   disjunctive_column_def(_,_,Col_Prefix,_,_,_),
   length(Head,No_H_Atoms),
   variables_positions_of_atom_list(Head,H_Var_Pos),
   select_sql_columns(No_H_Atoms,Col_Prefix,H_Var_Pos,Col_of_Vars),
   generate_disj_columns_for_insert(Body,Disj_Columns),
   generate_body_columns_for_insert(Body,Body_Columns),
   append([Col_of_Vars,Body_Columns,Disj_Columns],Insert_Columns),
   tps_insert_line(J_T_Name,Insert_Columns,Line).


build_select_line(Head,Body,Line) :-
   variables_and_constants_of_atoms(Head,Head_Vars,_),
   select_attributes(Head_Vars,Body,Select_Attributes),
   generate_body_columns_for_select(Body,Body_Columns2),
   generate_disj_columns_for_select(Body,Disj_Columns2),
   append(Body_Columns2,Disj_Columns2,Add_Columns),
   tps_select_line(Select_Attributes,Add_Columns,Line).


build_from_line(Body,Alias_Substitutions,Line) :-
   predicates_of_atom_list(Body,Body_Predicates),
   tps_from_line(Body_Predicates,Alias_Substitutions,Line).



build_where_line(Body,Line) :-
   determine_join_conditions(Body,Join_Conditions1),
   delete(Join_Conditions1,[],Join_Conditions),
   store(join_conditions,Join_Conditions1),
   !,
   tps_where_line(Join_Conditions,Line).


build_update_command(J_T_Name,Head,Body,Comm) :-
   disjunctive_column_prefix_name(Body_Prefix),
   disjunctive_column_def(_,_,Col_Prefix,_,_,_),
   length(Head,No_H_Atoms),
   variables_and_constants_of_atoms(Head,_,H_Consts),
   constants_positions_of_atom_list(Head,Const_Pos),
   select_sql_columns(No_H_Atoms,Col_Prefix,Const_Pos,
      Const_Cols),
   predicate_info_for_sql_update(Head,H_Pred_Values,H_Pred_Cols),
   predicate_info_for_sql_update(Body,Body_Prefix,B_Pred_Values,
      B_Pred_Cols),
   append([Const_Cols,H_Pred_Cols,B_Pred_Cols],Update_Columns),
   append([H_Consts,H_Pred_Values,B_Pred_Values],Update_Values),
   tps_update_command(J_T_Name,Update_Columns,Update_Values,
      Comm).


/*** help rules ***/


/* split_rule_replace_variables(State,Head,Body) <- */

split_rule_replace_variables(State,Head,Body) :-
    State = Head-Body,
    variable_replace_term(VRT),
    numbervars(Body,VRT,0,_).


select_sql_columns(No_Atoms,Base,Lists_of_Positions,Attributes) :-
   select_sql_cols(No_Atoms,Base,1,Lists_of_Positions,[],Attributes).

select_sql_cols(No_of_Atoms,Base,No,[Pos1_L|Pos2_L],Sofar,Attr) :-
   No =< No_of_Atoms,
   concat(Base,No,Arg_No),
   concat(Arg_No,'_',Arg_No_),
   product_atom_list(Arg_No_,Pos1_L,Columns),
   append(Sofar,Columns,New_Sofar),
   succ(No,No1),
   select_sql_cols(No_of_Atoms,Base,No1,Pos2_L,New_Sofar,Attr).
select_sql_cols(_,_,_,[],Sofar,Sofar).


/* new_body_with_aliases(Body,New_Body,Substitutions) <-
      Semantic of Substitutions:  [old-New,...] */

new_body_with_aliases(Body,New_Body,Info_of_Substitutions) :-
   predicates_of_atom_list(Body,B_Preds),
   dislog_sql_fixpoint(table_alias_name,Alias),
   list_to_set(B_Preds,Set_of_B_Preds),
   delete_elements(B_Preds,Set_of_B_Preds,Diff),
   generate_subst_info(Diff,Alias,Info_of_Substitutions),
   substitute_preds_in_body(Body,Info_of_Substitutions,New_Body).


generate_subst_info(List,Alias_Prefix,Info_of_Substitutions) :-
   generate_subst_info(List,Alias_Prefix,1,[],Substitutions),
   sort(Substitutions,Info_of_Substitutions).

generate_subst_info([El|Els],Alias_Prefix,No,Sofar,Infos) :-
   concat_atom([Alias_Prefix,No,'_',El],New_El),
   Info = [El-New_El],
   append(Sofar,Info,New_Sofar),
   succ(No,New_No),
   generate_subst_info(Els,Alias_Prefix,New_No,New_Sofar,Infos).
generate_subst_info([],_,_,Sofar,Sofar).


/* substitute_preds_in_body(
         Body,Info_of_Substitutions,New_Body) <-
      */

substitute_preds_in_body(
      [Atom|Atoms],Infos,[New_Atom|New_Atoms]) :-
   Atom =.. [Pred|Args],
   substitute_preds_in_body2(Pred,Infos,Matched_Info),
   delete_element(Infos,Matched_Info,New_Infos),
   Matched_Info = _-New_Pred,
   New_Atom =.. [New_Pred|Args],
   substitute_preds_in_body(Atoms,New_Infos,New_Atoms).
substitute_preds_in_body([Atom|Atoms],Infos,[Atom|New_Atoms]) :-
   substitute_preds_in_body(Atoms,Infos,New_Atoms).
substitute_preds_in_body([],_,[]).

substitute_preds_in_body2(Pred,[Info|_], Info) :-
   Info = P-_,
   Pred == P.
substitute_preds_in_body2(Pred,[_|Infos],Empty_Info) :-
   substitute_preds_in_body2(Pred,Infos,Empty_Info).
substitute_preds_in_body2(_,[],[]) :-
   !, fail.


/*** sql - specific help rules ***/


generate_disj_columns_for_insert(Atoms,Disj_Columns) :-
   predicates_of_atom_list(Atoms,Predicates),
   length(Predicates,L),
   generate_list_of_successors(1,L,Succ_List),
   disjunctive_column_def(_,_,_,_,Disj_,_),
   product_atom_list(Disj_,Succ_List,Disj_Columns).


generate_body_columns_for_insert(Atoms,All_Columns) :-
   predicates_of_atom_list(Atoms,Predicates),
   length(Predicates,L),
   disjunctive_column_def(_,_,Arg_Col,_,_,_),
   disjunctive_column_prefix_name(Prefix),
   concat(Prefix,Arg_Col,Prefix_Col),
   g_b_c_f_i(Atoms,List_of_Positions),
   select_sql_columns(L,Prefix_Col,List_of_Positions,All_Columns).

g_b_c_f_i([Atom|Atoms],[Positions1|Positions2]) :-
    functor(Atom,_,Arity),
    generate_list_of_successors(1,Arity,Positions1),
    g_b_c_f_i(Atoms,Positions2).
g_b_c_f_i([],[]).


generate_body_columns_for_select(Atoms,All_Columns) :-
   disjunctive_column_def(_,_,Arg_,_,_,_),
   g_b_c_f_s(Atoms,Arg_,[],All_Columns).

g_b_c_f_s([Atom|Atoms],Arg_,Sofar,All_Columns) :-
   functor(Atom,Predicate,Arity),
   generate_list_of_successors(1,Arity,Successors),
   concat_atom([Predicate,'.',Arg_],Column_Prefix),
   product_atom_list(Column_Prefix,Successors,All_Columns_of_Atom),
   append(Sofar,All_Columns_of_Atom,New_Sofar),
   g_b_c_f_s(Atoms,Arg_,New_Sofar,All_Columns).
g_b_c_f_s([],_,Sofar,Sofar).


generate_disj_columns_for_select(Atoms,Disj_Columns) :-
   predicates_of_atom_list(Atoms,Predicates),
   disjunctive_column_def(_,_,_,_,Disj_,_),
   concat_atom(['.',Disj_,'1'],Disj_Column_Suffix),
   product_list_list(Predicates,[Disj_Column_Suffix],Col),
   flatten(Col,Disj_Columns).


select_attributes([H_Var1|H_Var2],Body,[Attr1|Attr2]):-
   find_first_matching_arg_in_atom_list(H_Var1,Body,Pred_Name,Pos),
   Attr1 =.. [Pred_Name,Pos],
   select_attributes(H_Var2,Body,Attr2).
select_attributes([],_,[]).


determine_join_conditions(List_of_Atoms,List_of_Conditions) :-
   determine_join_conditions(List_of_Atoms,[],List_of_Conditions).
determine_join_conditions([Atom|Atoms],Sofar,Condition_List) :-
   Atom =.. [Pred|Args],
   variables_and_constants_of_list(Args,Var_List,Const_List),
   constants_in_atom(Pred,Args,Const_List,Cond_List_1),
   same_variables_in_atom(Pred,Args,Var_List,Cond_List_2),
   list_to_set(Var_List,Var_Set),
   find_arguments_in_atom_list(Pred,Args,Var_Set,Atoms,
      Cond_List_3),
   append([Sofar,Cond_List_1,Cond_List_2,Cond_List_3],Sofar_1),
   determine_join_conditions(Atoms,Sofar_1,Condition_List).
determine_join_conditions([],Sofar,Sofar).


tps_select_line(Attr_List,Add_Columns,Line):-
    t_s_l(Attr_List,'',Part),
    concat_atom(Add_Columns,',',Add_Columns_Part),
    concat_atom(['SELECT ',Part,',',Add_Columns_Part],Line).

t_s_l([Attr1|Attr2],S1,Part):-
   disjunctive_column_def(_,_,Arg_,_,_,_),
%  columns_pre_names_of_relations(Prename1,_),
   Attr1 =.. [Rel,Col],
   concat_atom([S1,Rel,'.',Arg_,Col,', '],Part1),
   t_s_l(Attr2,Part1,Part).
t_s_l([],S1,Part) :-
   concat(Part,', ',S1).


tps_insert_line(Table,List_of_Columns,Command) :-
   concat_atom(List_of_Columns,',',Column_Part),
   concat_atom(['INSERT INTO ',Table,' (',Column_Part,')'],
      Command).

tps_insert_line(Table,Command_String) :-
   concat('INSERT INTO ', Table, Command_String).


tps_from_line(List_of_Tables,Alias_Substitutions,Line) :-
   sort(List_of_Tables,List_of_Tables1),
   alias_substitutions_to_from_arg(
      Alias_Substitutions,From_Alias),
   concat_atom(List_of_Tables1,',',List1),
   concat_atom([List1,From_Alias],From_Arg),
   concat('FROM ',From_Arg,Line).


alias_substitutions_to_from_arg(
      Alias_Substitutions,Sql_Arg) :-
   alias_substitutions_to_from_arg(
      Alias_Substitutions,',',Sql_Arg).

alias_substitutions_to_from_arg([S1|S2],Sofar,Sql_Arg) :-
   S1 = Table_Name - Alias,
   concat_atom([Sofar,Table_Name,' ',Alias,','],New_Sofar),
   alias_substitutions_to_from_arg(S2,New_Sofar,Sql_Arg).
alias_substitutions_to_from_arg([],Sofar,Sql_Arg) :-
   concat(Sql_Arg,',',Sofar). 
alias_substitutions_to_from_arg([],_,'').


tps_where_line(Cond_List,Line):-
    Cond_List \== [],
    t_w_l(Cond_List,'',Part),
    concat('WHERE ',Part,Line).
tps_where_line([],[]).

t_w_l([[C1|C2]|C3],Sofar,Part) :-
    disjunctive_column_def(_,_,Arg_,_,_,_),
    C1 =.. [Pred1,No1],
    [C_2] = C2,
    C_2 =.. [Pred2,No2],
    concat_atom([Sofar, Pred1,'.',Arg_,No1,' = ',
       Pred2,'.',Arg_,No2,' AND '],New_Sofar),
    t_w_l(C3,New_Sofar,Part).
t_w_l([[C1|C2]|C3],Sofar,Part) :-
    disjunctive_column_def(_,_,Arg_,_,_,_),
    C1 =.. [Pred1,No1],
    [C_2] = C2,
    atomic(C_2),
    name(C_2,C_2_String),
    append(["'",C_2_String,"'"],Sql_String_as_String),
    name(Sql_String,Sql_String_as_String),
    concat_atom([Sofar,Pred1,'.',Arg_,No1,' = ',
       Sql_String,' AND '],New_Sofar),
    t_w_l(C3,New_Sofar,Part).
t_w_l([],Sofar,Part) :- concat(Part,' AND ',Sofar).


tps_update_command(Table,Columns,Columns_Settings,Command) :-
   tps_update_command(Columns,Columns_Settings,Comparisons),
   concat_atom(Comparisons,',',Comparisons1),
   concat_atom(
      ['UPDATE ',Table,' SET ', Comparisons1],Command).

tps_update_command([Col|Cols],[Setting|Settings],[Comp1|Comp2]) :-
    name(HK,"'"),
    concat_atom([Col,'=',HK,Setting,HK],Comp1),
    tps_update_command(Cols,Settings,Comp2).
tps_update_command([],[],[]).


predicate_info_for_sql_update(Atoms,Predicates,Columns) :-
   predicate_info_for_sql_update(Atoms,'',Predicates,Columns).

predicate_info_for_sql_update(Atoms,Prefix,Predicates,Columns) :-
   disjunctive_column_def(Pred_,_,_,_,_,_),
   concat(Prefix,Pred_,Prefix1),
   pred_info_for_sql_update(Atoms,1,Prefix1,Predicates,Columns).

pred_info_for_sql_update([A|As],Atom_No,Prefix,[Pred|Ps],[C|Cs]) :-
   functor(A,P,_),
   dislog_sql_fixpoint(delta_table_suffix,Delta),
   concat(Pred,Delta,P),
   concat(Prefix,Atom_No,C),
   succ(Atom_No,Atom_No_1),
   pred_info_for_sql_update(As,Atom_No_1,Prefix,Ps,Cs).

pred_info_for_sql_update([A|As],Atom_No,Prefix,[P|Ps],[C|Cs]) :-
   functor(A,P,_),
   concat(Prefix,Atom_No,C),
   succ(Atom_No,Atom_No_1),
   pred_info_for_sql_update(As,Atom_No_1,Prefix,Ps,Cs).
pred_info_for_sql_update([],_,_,[],[]).


predicates_of_atom_list([Atom|Atoms],[Predicate|Predicates]) :-
   functor(Atom,Predicate,_),
   predicates_of_atom_list(Atoms,Predicates).
predicates_of_atom_list([],[]).


/******************************************************************/


