

/******************************************************************/
/***                                                            ***/
/***        DisLog: SQL Elementary                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* atoms_to_prolog_atom(Atoms,Seperator,Prolog_Atom) <-
      */

atoms_to_prolog_atom(Atoms,Seperator,Prolog_Atom) :-
   atoms_to_prolog_atom(Atoms,Seperator,'',Prolog_Atom).

atoms_to_prolog_atom([A|As],Seperator,Sofar,Prolog_Atom) :-
   term_to_atom(A,Prolog_Atom_1),
   concat_atom([Sofar,Prolog_Atom_1,Seperator],New_Sofar),
   atoms_to_prolog_atom(As,Seperator,New_Sofar,Prolog_Atom).
atoms_to_prolog_atom([],Seperator,Sofar,Prolog_Atom) :-
   concat(Prolog_Atom,Seperator,Sofar).  


/* prolog_atom_to_atoms(Prolog_Atom,Seperator,Atoms) <-
      */

prolog_atom_to_atoms(Prolog_Atom,Seperator,Atoms) :-
   name(Prolog_Atom,String),
   name(Seperator,Seperator_String),
   [Sep] = Seperator_String,
   all_sublists_without_element(String,Sep,Sub_Lists),
   strings_to_atoms(Sub_Lists,Atoms).


/* strings_to_atoms([String|Strings],[Atom|Atoms]) <-
      */

strings_to_atoms([String|Strings],[Atom|Atoms]) :-
   name(Prolog_Atom,String),
   term_to_atom(Atom,Prolog_Atom),
   strings_to_atoms(Strings,Atoms).
strings_to_atoms([],[]).


/* small_to_big_letters(Atom1,Atom2) <-
      all big letters in Atom1 are transformed into
      small letters, the result is Atom2 */

small_to_big_letters(Atom1,Atom2) :-
   name(Atom1,String1),
   small_to_big_letters_in_string(String1,String2),
   name(Atom2,String2).


/* big_to_small_letters(Atom1,Atom2) <-
      all small letters in Atom1 are transformed into
      big letters, the result is Atom2 */

big_to_small_letters(Atom1,Atom2) :-
   name(Atom1,String1),
   big_to_small_letters_in_string(String1,String2),
   name(Atom2,String2).


/* small_to_big_letters_in_string(String1,String2) <-
      all small letters in String1 are transformed into
      big letters, the result is String2 */

small_to_big_letters_in_string([C1|C1_s],[C2|C2_s]) :-
   between(97,122,C1),
   C2 is C1 - 32,
   small_to_big_letters_in_string(C1_s,C2_s).
small_to_big_letters_in_string([C1|C1_s],[C1|C2_s]) :-
   small_to_big_letters_in_string(C1_s,C2_s).
small_to_big_letters_in_string([],[]).


/* big_to_small_letters_in_string(String1,String2) <-
      all big letters in String1 are transformed into
      small letters, the result is String2 */

big_to_small_letters_in_string([C1|C1_s],[C2|C2_s]) :-
   between(65,90,C1),
   C2 is C1 + 32,
   big_to_small_letters_in_string(C1_s,C2_s).
big_to_small_letters_in_string([C1|C1_s],[C1|C2_s]) :-
   big_to_small_letters_in_string(C1_s,C2_s).
big_to_small_letters_in_string([],[]).


/* split_list(List,Seperator,Sublist1,Sublist2) <-
      List is seperated into Sublist1 and Sublist2,
      Seperator is neither in Sublist1 nor Sublist2 */

split_list([El|Els],Seperator,[El|S1_s],Sublist2) :-
   El \== Seperator,
   split_list(Els,Seperator,S1_s,Sublist2).
split_list([_|Els],_,[],Els).
split_list([],_,[],[]).


/* all_sublists_without_element(
         List,Element,List_Of_Sublists) <-
      List_Of_Sublists are all sublists of List
      without element Element. */

all_sublists_without_element(List,Element,List_Of_Sublists) :-
   atomic(Element),
   all_sublists_without_el(List,Element,List_Of_Sublists),
   !,
   List_Of_Sublists \== [].

all_sublists_without_el(List,Element,[Sub1|Sub2]) :-
   first_sublist_without_element(List,Element,Sub1,Rest_List),
   all_sublists_without_el(Rest_List,Element,Sub2).
all_sublists_without_el(_,_,[]).


/* first_sublist_without_element(
        List,Element,Sub_List,Rest_List) <-
     Succeeds if Sublist is a sublist of List without the
     element Element.
     Rest_List is the tail of List after Sublist. */

first_sublist_without_element(
      List,Element,Sub_List,Rest_List) :-
   first_sublist_without_el(
      List,Element,Sub_List,leer,Rest_List).

first_sublist_without_el([L1|L2],El,[L1|SL2],_,Rest_List) :-
   L1 \== El,
   !,
   first_sublist_without_el(L2,El,SL2,voll,Rest_List).
first_sublist_without_el([_|L2],El,Sub_List,leer,Rest_List) :-
   first_sublist_without_el(L2,El,Sub_List,leer,Rest_List).
first_sublist_without_el(Rest_List,_,[],voll,Rest_List).


/* product_list_list(Names_1, Names_2, Lists_of_Names) <-
      */

product_list_list([L1|L1s],List2,[PL|PLs]) :-
   product_atom_list(L1,List2,PL),
   product_list_list(L1s,List2,PLs).
product_list_list([],_,[]).

product_atom_list(Atom,List_of_Atoms,Product_List) :-
   atomic(Atom),
   prod_atom_list(Atom,List_of_Atoms,Product_List).

prod_atom_list(Atom,[L2|L2s],[PL|PLs]) :-
   atomic(L2),
   concat(Atom,L2,PL),
   prod_atom_list(Atom,L2s,PLs).
prod_atom_list(_,[],[]).


/* concat_term_functors_with_atom(Terms_1, Atom, Terms_2) <-
      */

concat_term_functors_with_atom([T|Ts],Atom,[N_Term|N_Terms]) :-
   T =.. [Pred|Args],
   concat(Pred,Atom,New_Pred),
   N_Term =.. [New_Pred|Args],
   concat_term_functors_with_atom(Ts,Atom,N_Terms).
concat_term_functors_with_atom([],_,[]).


/* insert_element_in_lists(Element, Lists_1, Lists_2) <-
      */

insert_element_in_lists(Element,[List|Lists],[List1|Lists1]) :-
   List1 = [Element|List],
   insert_element_in_lists(Element,Lists,Lists1).
insert_element_in_lists(Element,[[]],[Element]).
insert_element_in_lists(_,[],[]).


/* delete_element(List1,Element,List2) <-
      deletes the first member of List1 that unify
      with Element and unify the result with List2. */

delete_element([In_El|In_Els],Element,[In_El|Out_Els]) :-
   In_El \== Element,
   delete_element(In_Els,Element,Out_Els).
delete_element([_|In_Els],_,In_Els).
delete_element([],_,[]).

delete_elements(List1,[L2|L2s],List3) :-
   delete_element(List1,L2,List4),
   delete_elements(List4,L2s,List3).
delete_elements(List3,[],List3).


/* prefix_of_prolog_atom(Prefix,Atom) <-
      */

prefix_of_prolog_atom(Prefix,Atom) :-
   name(Atom,String),
   prefix(P,String),
   name(Prefix,P).


/* replace_element(List1,No,New_Element,List2) <-
      Substitutes element with number No from List1 by
      New_Element, Result in List2 */

replace_element([El|Els],No,New_El,[El|El1s]) :-
   No > 1,
   succ(New_No,No),
   replace_element(Els,New_No,New_El,El1s).
replace_element([_|Els],1,New_El,[New_El|Els]).


/* replace_substructures(
         List,Old_Structure,New_Structure,New_List) <-
      replaces in List a sublist defined as Old_Structure
      through the sublist New_Structure */

replace_substructures(
      List,Old_Structure,New_Structure,New_List) :-
   length(Old_Structure,X),
   X > 0,
   length(List,Y),
   Y > 0,
   replace_substructures(
      List,Old_Structure,New_Structure,[],New_List).

replace_substructures(Els,O_Struct,N_Struct,Sofar,NL) :-
   sublist(O_Struct,Els),
   append(O_Struct,Cont_List,Els),
   append(Sofar,N_Struct,N_Sofar),
   replace_substructures(Cont_List,O_Struct,N_Struct,N_Sofar,NL).
replace_substructures([El|Els],O_Struct,N_Struct,Sofar,NL) :-
   append(Sofar,[El],N_Sofar),
   replace_substructures(Els,O_Struct,N_Struct,N_Sofar,NL).
replace_substructures([],_,_,Sofar,Sofar).


/* generate_list_of_successors(Start,No_of_Elements,List) <-
      List is a list of integers,
      Start is the first element of List,
      and every element is the element before plus one. */

generate_list_of_successors(Start,No_of_Elements,[Start|Ns]) :-
   No_of_Elements > 0,
   succ(Start,Start1),
   succ(No_of_Elements_1,No_of_Elements),
   generate_list_of_successors(Start1,No_of_Elements_1,Ns).
generate_list_of_successors(_,0,[]).


/* split_list_by_position(List1,Position,List2,List3) <-
      splits List1 into List2 and List3, the cut is at 
      Postition */

split_list_by_position([El1|El1s],Position,[El1|El2s],List3) :-
  Position >= 0,
  succ(New_Position,Position),
  split_list_by_position(El1s,New_Position,El2s,List3).
split_list_by_position(List1,0,[],List1). 


/* select_sublist(List1,S,List2) <-
      the first S elements are copied to List2 */

select_sublist(List1,Sizes,List2) :-
   size_values_to_position_values(1,Sizes,_,Positions),
   position_select(List1,Positions,List2).

size_values_to_position_values(Start,Sizes,Stop,Positions) :-
   flatten(Sizes,F_Sizes),
   size_vals_to_pos_vals(Start,F_Sizes,Stop,Positions).
   
size_vals_to_pos_vals(Start,[F_Size|F_Sizes],Stop,[Pos|Poss]) :-
   generate_list_of_successors(Start,F_Size,Pos),
   New_Start is Start + F_Size,
   size_vals_to_pos_vals(New_Start,F_Sizes,Stop,Poss).
size_vals_to_pos_vals(Start,[],Stop,[]) :-
   Stop is Start -1.


/* position_select(List1,Positions,List2) <-
      Maps List1 to the structure List2 by replacing the
      position values of Positions with the appropriate
      element of List1.  Example:
        List1:     [a,b,c,d],
        Positions: [1,[2],[[3,[2,4]]]]
        List2:     [a,[b],[[c,[b,d]]]] */        

position_select(List,[Position|Positions],[El1|El2s]) :-
   is_list(Position),
   position_select(List,Position,El1),
   position_select(List,Positions,El2s).
position_select(List,[Position|Positions],[El1|El2s]) :-
   nth1(Position,List,El1),
   position_select(List,Positions,El2s).
position_select(_,[],[]). 


/* start_real_timer(Id) <-
      */

start_real_timer(Id) :-
   get_time(T),
   convert_time(T,_,_,_,H,M,S,Ms),
   assert(sql_time(Id,H,M,S,Ms)).


/* stop_real_timer_write(Id) <-
      */

stop_real_timer_write(Id) :-
   get_time(T),
   convert_time(T,_,_,_,H,M,S,Ms),
   alex_time(Id,H1,M1,S1,Ms1),
   Diff_Sec is
      (H - H1) * 3600 + (M - M1) * 60 + (S - S1) +
      (Ms - Ms1) / 1000,
   writeln(Diff_Sec),
   retractall(sql_time(Id,_,_,_,_)).


/* generate_atom(Size,[100|As]) <-
      */

generate_atom(Size,[100|As]) :-
   Size > 0,
   succ(New_Size,Size),
   !,
   generate_atom(New_Size,As).
generate_atom(_,[]).


/* return_atom(R) <-
      */

return_atom(R) :-
   name(R,[10]).


/******************************************************************/


