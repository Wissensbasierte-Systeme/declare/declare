

/******************************************************************/
/***                                                            ***/
/***        DisLog: SQL Connect                                 ***/
/***                                                            ***/
/******************************************************************/


oracle_start_command( 'sqlplus' ).
oracle_start_option( '-S' ).
oracle_set_environment( ['set heading off','set feedback off'] ).

dislog_oracle_variable( user_name, feldhoff ).
dislog_oracle_variable( password, feldhoff ).
dislog_oracle_variable( named_pipe, sql_pipe ).

dislog_oracle_variable( asserted_out_stream, out_stream ).
dislog_oracle_variable( asserted_in_stream, in_stream ).

dislog_oracle_variable( end_of_transfer, '@' ).
dislog_oracle_variable( end_of_transfer_string, "@" ).
dislog_oracle_variable( oracle_line_size, 500 ).

% dislog_oracle_variable( oracle_line_size, autodetect ).


/*** interface ****************************************************/


oracle_connect :-
   oracle_connected.

oracle_connect :-
   delete_pipe_if_existing,
   create_pipe,
   !,
   init_output_to_oracle_to_named_pipe,
   init_input_from_named_pipe,
   oracle_set_environment,
   !.

oracle_connected :-
   streams_connected,
   exists_pipe.


oracle_disconnect :-
   disconnect_dislog_oracle_and_named_pipe, 
   !,
   exists_pipe,
   delete_pipe.



/*** implementation ***********************************************/


create_pipe :-
   dislog_oracle_variable(named_pipe,Name_Of_Pipe),
   concat_atom(['mknod ',Name_Of_Pipe,' p'],Unix_Command),
   shell(Unix_Command),
   concat('chmod 666 ',Name_Of_Pipe,Unix_Command2),
   shell(Unix_Command2).

delete_pipe_if_existing :-
   exists_pipe,
   delete_pipe. 
delete_pipe_if_existing.


init_output_to_oracle_to_named_pipe :-
   oracle_start_command(Start_Oracle),
   oracle_start_option(Oracle_Options),
   dislog_oracle_variable(user_name,Account),
   dislog_oracle_variable(password,Password),
   dislog_oracle_variable(named_pipe,Pipe_Name), 
   concat_atom([Start_Oracle,' ',Oracle_Options,' ',
      Account,'/',Password,' >',Pipe_Name],Command),
   dislog_oracle_variable(asserted_out_stream,Predicate),
   retract_fact_and_close_stream_if_possible(Predicate),
   open(pipe(Command),write,Out_Stream),
   New_Fact =.. [Predicate,Out_Stream],
   assert(New_Fact), 
   !.


init_input_from_named_pipe :-
   dislog_oracle_variable(named_pipe,Pipe_Name), 
   dislog_oracle_variable(asserted_in_stream,Predicate), 
   retract_fact_and_close_stream_if_possible(Predicate),
   open(Pipe_Name,read,In_Stream), 
   New_Fact =.. [Predicate,In_Stream],
   assert(New_Fact), 
   !.

disconnect_dislog_oracle_and_named_pipe :-
   dislog_oracle_variable(asserted_out_stream,Predicate1),
   retract_fact_and_close_stream_if_possible(Predicate1),
   dislog_oracle_variable(asserted_in_stream,Predicate2), 
   retract_fact_and_close_stream_if_possible(Predicate2),
   !.


retract_fact_and_close_stream_if_possible(Predicate) :-
   Fact =.. [Predicate,Stream],
   retract(Fact),
   close_stream_if_possible(Stream).
retract_fact_and_close_stream_if_possible(_).


close_stream_if_possible(Stream) :-
   current_stream(_,_,Stream),
   close(Stream).
close_stream_if_possible(_).


get_in_stream(ID) :-
   dislog_oracle_variable(asserted_in_stream,Pred),
   Fact =.. [Pred,ID], !, 
   current_predicate(Pred,_), 
   call(Fact), !,
   current_stream(_,_,ID), 
   !.


get_out_stream(ID) :-
   dislog_oracle_variable(asserted_out_stream,Pred),
   Fact =.. [Pred,ID], 
   !, 
   current_predicate(Pred,_),
   call(Fact), !,
   current_stream(_,_,ID), 
   !.


streams_connected :-
   stream_out_connected,
   stream_in_connected.


stream_out_connected :-
   get_out_stream(Stream),
   current_stream(_,_,Stream), 
   !.

stream_in_connected :-
   get_in_stream(Stream),
   current_stream(_,_,Stream), 
   !.


exists_pipe :-
   dislog_oracle_variable(named_pipe,Pipe),
   exists_file(Pipe), 
   !.
 
delete_pipe :-
   dislog_oracle_variable(named_pipe,Pipe),
   delete_file(Pipe), 
   !.


oracle_set_environment :-
   oracle_send_command_without_eot('set heading off'),
   oracle_send_command_without_eot('set feedback off'),
   detect_max_table_width(Width),
   concat('set line ',Width,Command1),
   oracle_send_command_without_eot(Command1). 


oracle_reset_environment :-
   oracle_send_command_without_eot('set heading on'),
   oracle_send_command_without_eot('set feedback on').


/* detect_max_table_width(Width) :-
      dislog_oracle_variable(oracle_line_size,autodetect),
      get_size_of_widest_table(Width). */

detect_max_table_width(X) :-
   dislog_oracle_variable(oracle_line_size,X),
   integer(X).


sends(Commands) :-
  oracle_reset_environment,
  sending(Commands),
  oracle_set_environment.


sending([Command|Commands]) :-
   send(Command),
   sending(Commands).
sending([]). 


send(Command) :-
   oracle_send_command(Command),
   oracle_receive_atom(Atom),
   write(Atom).

  
oracle_send_string(String):-
   name(Atom,String),
   oracle_send_command(Atom).


oracle_send_command(Command) :-
   current_num(sql_trace_mode,Flag), 
   oracle_send_com(Flag,Command).
   

/* Stream to Oracle */

oracle_send_com(Flag,Command) :-
   sql_stream_control_flags(Flag,_,_),
   get_out_stream(Out_Stream),
   oracle_send_com_on_stream(Out_Stream,Command).

/* Stream to screen */

oracle_send_com(Flag,Command) :-
   sql_stream_control_flags(_,Flag,_),
   oracle_send_com_on_stream_without_eot(user_output,Command). 

/* Stream to both */

oracle_send_com(Flag,Command) :-
   sql_stream_control_flags(A,B,Flag),
   oracle_send_com(A,Command),
   oracle_send_com(B,Command).


oracle_send_command_without_eot(Command) :-
   current_num(sql_trace_mode,Flag),
   oracle_send_com_without_eot(Flag,Command).


/* stream to Oracle */

oracle_send_com_without_eot(Flag,Command) :-
   sql_stream_control_flags(Flag,_,_),
   get_out_stream(Out_Stream),
   oracle_send_com_on_stream_without_eot(Out_Stream,Command).

/* stream to screen */

oracle_send_com_without_eot(Flag,Command) :-
   sql_stream_control_flags(_,Flag,_),
   oracle_send_com_on_stream_without_eot(user_output,Command).

/* stream to both */

oracle_send_com_without_eot(Flag,Command) :-
   sql_stream_control_flags(A,B,Flag),
   oracle_send_com_without_eot(A,Command),
   oracle_send_com_without_eot(B,Command).


oracle_send_commands([Command|Commands]) :-
   oracle_send_command(Command),
   oracle_send_commands(Commands).
oracle_send_commands([]). 


oracle_send_commands_without_eot([Command|Commands]) :-
   oracle_send_command_without_eot(Command),
   oracle_send_commands_without_eot(Commands).
oracle_send_commands_without_eot([]).


oracle_send_com_on_stream(Stream,Command) :-
   oracle_send_com_on_stream_without_eot(Stream,Command),
   oracle_send_eot_on_stream(Stream).


oracle_send_com_on_stream_without_eot(Stream,Command) :-
   concat(Command,';',Comm),
   write(Stream,Comm),
   nl(Stream),
   flush_output(Stream).

oracle_send_eot_on_stream(Stream) :-
   dislog_oracle_variable(end_of_transfer,Eot), 
   concat('prompt ',Eot,Comm),
   write(Stream,Comm),
   nl(Stream),
   flush_output(Stream).   


oracle_receive_string(String) :-
   sql_stream_control_flags(Flag,_,_),
   current_num(sql_trace_mode,Flag),
   best_read(String).
   

oracle_receive_string(String) :-
   sql_stream_control_flags(_,_,Flag),
   current_num(sql_trace_mode,Flag),
   best_read(String).


oracle_receive_atom(Atom) :-
   oracle_receive_string(String),
   name(Atom,String).


best_read(String) :-
   get_in_stream(Id),
   dislog_oracle_variable(end_of_transfer_string,EoT_Str),
   [EoT_Value] = EoT_Str,
   read_data_best(Id,EoT_Value,String).

read_data_best(Id,EoT_Value,[C|Cs]) :-
   get0(Id,C),
   C \==EoT_Value,
   !,
   read_data_best(Id,EoT_Value,Cs).
read_data_best(_,_,[]) :- !.
   

/******************************************************************/

