

/******************************************************************/
/***                                                            ***/
/***         DisLog: SQL Interface                              ***/
/***                                                            ***/
/******************************************************************/



/*** facts ********************************************************/


/* Please define the size of the columns for storing facts:
    - pred_ : max predicate name
    - arg_  : max argument name
    - disj_ : max disjunctive rest */

disjunctive_column_def(
   pred_, varchar2(5),
   arg_, varchar2(5),
   disj_, varchar2(100)).     
disjunctive_column_prefix_name('b_').
disjunctive_j_table_prefix_name('join_').
disjunctive_table_alias_prefix('').
sql_stream_control_flags(oracle, screen, both).
sql_stream_control_flag(oracle).
variable_replace_term(var).
delta_table_suffix_name("_delta").
disjunctive_sql_interface(string_or_operator, ';').


/**** interface ***************************************************/


/* sql_command(Command) <-
      Executes all sqlplus compatible commands
      in the database. */

sql_command(Expression) :-
   oracle_connect,
   oracle_reset_environment,
   oracle_send_command(Expression),
   oracle_receive_atom(Response),
   oracle_set_environment,
   write(Response).


/* sql_create_table(Table_Name,Column_Descriptors) <-
      Creates the table Table_Name, with coloumn
      defined in Column_Descriptors.
      Column_Descriptors is of the following type:
      [Table_Name,[Attribute1-Type1,Attr2-Type1,...]] */

sql_create_table(Table,Column_Descriptors) :-
   oracle_connect,
   return_atom(Return),
   sql_create_table2(Column_Descriptors,Return,'',Struct),
   concat_atom(['CREATE TABLE ',Table,' (',Struct,')'],Command),
   oracle_send_command(Command),
   oracle_receive_atom(Response),
   write(Response).

sql_create_table2([C_D_1|C_D_2],Return,Sofar,Structure) :-
   C_D_1 = Column - Type,
   term_to_atom_corrected(Type,Type_Atom),
   concat_atom([Sofar,Column,' ',Type_Atom,',',Return],New_Sofar),
   sql_create_table2(C_D_2,Return,New_Sofar,Structure).
sql_create_table2([],Return,Sofar,Structure) :-
   concat(',',Return,Wrong_End),
   concat(Structure,Wrong_End,Sofar).

term_to_atom_corrected(Term,Term) :-
   atomic(Term).
term_to_atom_corrected(Term,Atom) :-
   term_to_atom(Term,Atom).


/* sql_insert_in_table(Table_Name,List_of_Values) <-
      inserts the values in table Table_Name.
      List_of_Values is of the following type:
      [[value1_1,value1_2,value1_3],[value2_4,...]] */

sql_insert_in_table(Table_Name,[Values_1|Values_2]) :-
   oracle_connect,
   sql_get_conform_values_atom(Values_1,Atom),
   concat_atom([
      'INSERT INTO ',Table_Name,' VALUES (', Atom,')'],
      Command),
   oracle_send_command(Command),
   oracle_receive_atom(Response),
   !,
   write(Response),
   sql_insert_in_table(Table_Name,Values_2).
sql_insert_in_table(_,[]).

sql_get_conform_values_atom(Values,Atom) :-
   sql_get_conform_values_atom(Values,"",Atom).

sql_get_conform_values_atom([Value|Values],Sofar,Atom) :-
   number(Value),
   name(Value,Number_String),
   append([Sofar,Number_String,","],New_Sofar),
   sql_get_conform_values_atom(Values,New_Sofar,Atom).
sql_get_conform_values_atom([Value|Values],Sofar,Atom) :-
   atom(Value),
   name(Value,Char_String),
   append([Sofar,"'",Char_String,"'",","],New_Sofar),
   sql_get_conform_values_atom(Values,New_Sofar,Atom).
sql_get_conform_values_atom([],Sofar,Atom) :-
   append(New_Sofar,",",Sofar),
   name(Atom,New_Sofar).


/* sql_delete_table(Table) <-
      removes all tupels from table Table */

sql_delete_table(Table) :-
   sql_delete(Table).

sql_delete(Name) :-
   oracle_connect,
   concat('DELETE ',Name,Command),
   oracle_send_command(Command),
   oracle_receive_atom(Response),
   write(Response).


/* sql_delete_tables <-
     removes all tuples from all available tables */

sql_delete_tables :-
   sql_load_table_names(Table_Names),
   sql_delete_tables2(Table_Names).

sql_delete_tables2([T_Name|T_Names]) :-
   sql_delete_table(T_Name),
   sql_delete_tables2(T_Names).
sql_delete_tables2([]).


/* sql_drop_table(Table) <-
      drops the single table Table */

sql_drop_table(Table) :-
   sql_drop(Table,table).

sql_drop(Name,Type) :-
   oracle_connect,
   concat_atom(['DROP ',Type,' ',Name],Command2),
   oracle_send_command(Command2),
   oracle_receive_atom(Response),
   write(Response).


/* sql_drop_tables <-
     always succeeds, tries to delete and drop all tables,
     views, e.t.c. */

sql_drop_tables :-
   sql_load_table(cat,Table_Informations),
   sql_drop_all2(Table_Informations).

sql_drop_all2([Name_and_Type|Name_and_Types]) :-
   Name_and_Type = [Name,Type],
   sql_drop(Name,Type),
   sql_drop_all2(Name_and_Types).
sql_drop_all2([]).


/* sql_subtract_tables(T1,T2,T3) <-
      always succeeds, tries to subtract table T2 from table T1
      and inserts the result in table T3. */

sql_subtract_tables(T1,T2,T3) :-
   oracle_connect,
   generate_sql_select('*',T1,S1),
   generate_sql_select('*',T2,S2),
   return_atom(R),
   concat_atom(['INSERT INTO ',T3,R,'(',S1,R,'MINUS',R,S2,')'],
      Command),
   oracle_send_command(Command),
   oracle_receive_atom(Response),
   write(Response).


/* sql_union_tables(T1,T2,T3) <-
      Always succeeds, tries to unite table T1 with table T2 to
      table T3, eliminates all duplicate rows in T3. */

sql_union_tables(T1,T2,T3) :-
   oracle_connect,
   generate_sql_select('*',T1,S1),
   generate_sql_select('*',T2,S2),
   return_atom(R),
   concat_atom(['INSERT INTO ',T3,R,'(',S1,R,'UNION',R,S2,')'],
      Command),
   oracle_send_command(Command),
   oracle_receive_atom(Response),
   write(Response).


/* sql_load_table_names(Table_Names) <-
      load the table-names of the available tables */

sql_load_table_names(Table_Names):-
   oracle_connect,
   oracle_send_command('SELECT DISTINCT table_name from cols'),
   oracle_receive_string(Table_String),
   length(Table_String,L),
   !,
   L > 0,
   table_string_to_values(Table_String,Lists_of_Tables),
   flatten(Lists_of_Tables,Table_Names).
sql_load_table_names([]).


/* sql_load_attributes(Table,Attributes) :-
      loads the Attributes of the table Table */

sql_load_attributes(Table,Attributes) :-
   oracle_connect,
   C = 'SELECT column_name FROM cols WHERE table_name = ',
   name(Table,Table_Str),
   append(["'",Table_Str,"'"],Sql_Table_Str),
   small_to_big_letters_in_string(Sql_Table_Str,Sql_Table_Str1),
   name(Sql_Table,Sql_Table_Str1),
   concat(C,Sql_Table,Describe_Columns_Command),
   oracle_send_command(Describe_Columns_Command),
   oracle_receive_string(Table_String),
   table_string_to_values(Table_String,Lists_of_Attributes),
   flatten(Lists_of_Attributes,Attributes).


/* sql_load_table(Table,List_of_Values) <-
      loads the table Table and
      divides Table into lines, and the lines
      are consisting of a list of prolog_atoms.
      That means all the tupels are brought into list
      structures. */

sql_load_table(Table,Table_as_Lists) :-
   oracle_connect,
   C ='SELECT DISTINCT * FROM ',
   concat(C,Table,Command),
   oracle_send_command(Command),
   oracle_receive_string(Table_Str),
   table_string_to_values(Table_Str,Table_as_Lists).


/* sql_count_table_rows(T,N) <-
      N is number of rows in table T */

sql_count_table_rows(T,N) :-
   oracle_connect,
   generate_sql_select('count(*)',T,Command),
   oracle_send_command(Command),
   oracle_receive_atom(Atom),
   term_to_atom(N,Atom).


/* sql_store_state_to_tables(State) <-
      inserts the state State in the database, but if there are
      less tables, they will be created and subsumed clauses
      will be removed.  */

sql_store_state_to_tables(State) :-
   list_to_ord_set(State,State_1),
   sql_load_state_from_tables(Old_State),
   ord_subtract(State_1,Old_State,New_State),
   sql_load_all_t_arities(F_T_Arities),
   f_t_arities_from_state(New_State,F_T_Arities_1),
   ord_subtract(F_T_Arities_1,F_T_Arities,Needed_F_T_Arities),
   create_needed_tables(Needed_F_T_Arities),
   tps_insert_in_f_tables(state,New_State).  


create_needed_tables([]).
create_needed_tables(F_T_Arities) :-
   tps_create_f_tables(f_t_arities,F_T_Arities).


/* sql_load_state_from_tables(State) <- 
      loads the current state stored in the database,
      but it must be sure that only fact tables are existing 
      in the database */     

sql_load_state_from_tables(State) :-
   sql_load_all_t_arities(F_T_Arities),
   tps_load_state_from_f_tables(F_T_Arities,'',State1),
   list_to_ord_set(State1,State). 


/* sql_delete_state_from_tables(State)  <-
      deletes the state State from the stored state and
      removes empy tables */

sql_delete_state_from_tables(State) :-
   tps_delete_from_f_tables(state,State),
   sql_remove_empty_tables.   


/* dportray(sql_table,Table_Name) <-
      shows the contents of table Table  */

dportray(sql_table,Table_Name) :-
   oracle_connect,
   generate_sql_select(*,Table_Name,Comm),
   oracle_reset_environment,
   oracle_send_command(Comm),
   oracle_receive_atom(Out_Atom),
   oracle_set_environment,
   write(Out_Atom).


/* dportray(sql_attributes,Table_Name) <-
      shows the attributes of table Table_Name */

dportray(sql_attributes,Table_Name) :-
   oracle_connect,
   concat('DESCRIBE ',Table_Name,Command),
   oracle_send_command(Command),
   oracle_receive_atom(Response),
   write(Response).


/* dportray(sql_catalog) <-
      portrays the catalogue of all user relations */

dportray(sql_catalog) :-
   dportray(sql_table,cat).


sql_set_trace_mode(Flag) :-
   set_num(sql_trace_mode,Flag).


sql_show_trace_mode(Flag) :-
   current_num(sql_trace_mode,Flag).


/*** implementation ***********************************************/


/* sql_load_all_t_arities(T_Arities) <-
      loads all table-arities from the database,
      that means all table-names and all table-attributes
      must be loaded to make the table-arities,
      remember that t_arity is of the structure
      Table-Name / Column -1 */

sql_load_all_t_arities(T_Arities) :-
    sql_load_table_names(T_Names),
    sql_table_names_to_t_arities(T_Names,T_Arities_1),
    list_to_ord_set(T_Arities_1,T_Arities).
   
 
sql_table_names_to_t_arities([T_Name|T_Names],[T_A|T_As]) :-
   sql_load_attributes(T_Name,T_Attributes),
   length(T_Attributes,L),
   succ(Arity,L), 
   big_to_small_letters(T_Name,T_Name_Small),
   T_A = T_Name_Small / Arity,
   sql_table_names_to_t_arities(T_Names,T_As).
sql_table_names_to_t_arities([],[]).   


atoms_to_sql_string(Atoms,Sql_String) :-
   disjunctive_sql_interface(string_or_operator,Op),
   atoms_to_prolog_atom(Atoms,Op,Prlg_Atom),
   name(Prlg_Atom,Prlg_String),
   delete(Prlg_String,32,Prlg_String1),
   name(Prlg_Atom1,Prlg_String1),
   concat_atom(['''',Prlg_Atom1,''''],Sql_String).
atoms_to_sql_string([],'''''').


prolog_atom_to_atoms(Sql_String,Atoms) :-
   disjunctive_sql_interface(string_or_operator,Op),
   prolog_atom_to_atoms(Sql_String,Op,Atoms).


/* sql_remove_empty_tables <-
      removes all tables in the database, which have no tuples */

sql_remove_empty_tables :-
   sql_load_table_names(Table_Names),
   sql_remove_tables_if_empty(Table_Names).

sql_remove_tables_if_empty([Table_Name|Table_Names]) :-
   sql_remove_table_if_empty(Table_Name),
   sql_remove_tables_if_empty(Table_Names).
sql_remove_tables_if_empty([]).

sql_remove_table_if_empty(Table_Name) :- 
   sql_remove_table_if_empty_1(Table_Name).
sql_remove_table_if_empty(_).

sql_remove_table_if_empty_1(Table_Name) :-
   sql_load_table(Table_Name,Content),
   !,
   Content = [],
   sql_drop_table(Table_Name).


/* table_string_to_values(Received_Table,Lists) <- 
      parses the sqlplus table information Received_Table and
      transforms it into prolog list*/
      
table_string_to_values([10],[]).
table_string_to_values([],[]).
table_string_to_values(Received_Table,Lists) :-
   table_string_to_lines(Received_Table,Lines),
   table_string_to_values1(Lines,Lists).

table_string_to_values1([Line|Lines],[List|Lists]) :-
   table_line_string_to_values(Line,List),
   table_string_to_values1(Lines,Lists).
table_string_to_values1([],[]).


/* table_string_to_lines(Received_Table,Lines) */

table_string_to_lines(Received_Table,Lines) :-
   all_sublists_without_element(Received_Table,10,Lines).


/* table_line_string_to_values(Line_of_Table,Values) */

table_line_string_to_values(Line_of_Table,Values) :-
   substitute(9,Line_of_Table,32,Line_Without_Tabs),
   all_sublists_without_element(Line_Without_Tabs,32,Strings),
   strings_to_prolog_atoms(Strings,Values).

strings_to_prolog_atoms([String|Strings],[Atom|Atoms]) :-
   name(Atom,String),
   strings_to_prolog_atoms(Strings,Atoms).
strings_to_prolog_atoms([],[]).


/* sql_values_from_args(Arguments,Sql_Values_as_prolog_atom) <-
      transforms list Arguments (f.e. [a,b,c]) into
      expression Sql_Values_as_prolog_atom (''a','b','c'') */

sql_values_from_args(List,Sql_Values_as_prolog_atom) :-
   length(List,L),
   sql_values_from_args(List,L,'',Sql_Values_as_prolog_atom).

sql_values_from_args([El|Els],L,Sofar,Sql_Values) :-
   L > 0,
   name(El,El_String),
   append(["'",El_String,"'",","],El_String1),
   name(El1,El_String1),
   concat(Sofar,El1,New_Sofar),
   succ(New_L,L),
   sql_values_from_args(Els,New_L,New_Sofar,Sql_Values).
sql_values_from_args([],_,Sofar,Sql_Values) :-
   concat(Sql_Values,',',Sofar).


generate_sql_select(Select_Arg,From_Arg,Command) :-
   return_atom(Ret),
   concat_atom(
      ['SELECT ',Select_Arg,Ret,'FROM ',From_Arg],Command).

generate_sql_select(Select_Arg,From_Arg,Where_Arg,Command) :-
   return_atom(Ret),
   term_to_atom(Where_Arg,Where_Atom_Arg),
   generate_sql_select(Select_Arg,From_Arg,Command1),
   concat_atom([Command1,Ret,'WHERE ',Where_Atom_Arg],Command).


/* sql_variable_check(Term) :-
     succeeds, if Terms functor can be instantiated by the fact
     variable_replace_term(X).  */

sql_variable_check(Term) :-
   variable_replace_term(Var),
   !,
   functor(Term,Var,_).


/******************************************************************/


