

/******************************************************************/
/***                                                            ***/
/***          DisLog: SQL State                                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* split_program_into_facts_and_rules(P,P1,P2) <-
      Splits program P into  program P1, containing the facts, 
      and program P2, containing the rules. */

split_program_into_facts_and_rules([P|Ps],Facts,[P|Rs]) :-
   P = _-_,
   split_program_into_facts_and_rules(Ps,Facts,Rs).
split_program_into_facts_and_rules([P|Ps],[P|Fs],Rules) :-
   split_program_into_facts_and_rules(Ps,Fs,Rules).
split_program_into_facts_and_rules([],[],[]).


/* r_state_to_atoms(R_State,Ord_Set_of_Atoms) <-
      Ord_Set_of_Atoms consists of all different atoms in R_State,
      think of the structure of a r_fact : Atom - [Disjunctions] */

r_state_to_atoms(R_State,Ord_Set_of_Atoms) :-
   r_state_to_atms(R_State,Ord_Set_of_Atoms).

r_state_to_atms([Atom-_|R_Facts],[Atom|Atoms]) :-
   r_state_to_atms(R_Facts,Atoms).
r_state_to_atms([],[]).


/* sql_table_to_state(F_T_Arity,Suffix,State) <-
      generates the State from the F_T_Arity Information,
      Suffix is necessary to recognize Delta-Tables  */    

sql_table_to_state(F_T_Arity,Suffix,State) :-
   F_T_Arity = Table_Name/Arity,
   succ(Arity,Arity1),
   sql_load_table(Table_Name,Tuples),
   concat(Pred_Name,Suffix,Table_Name),
   table_content_to_state(Pred_Name,Arity1,Tuples,State1),
   list_to_ord_set(State1,State).
  
table_content_to_state(Pred_Name,Arity,[Tupel|Tupels],[S1|S2]) :-
   line_content_to_fact(Pred_Name,Arity,Tupel,S0),
   list_to_ord_set(S0,S1),
   table_content_to_state(Pred_Name,Arity,Tupels,S2).
table_content_to_state(_,_,[],[]).

line_content_to_fact(Pred_Name,Arity,Tupel,Fact) :-
   Range is Arity - 1,
   split_list_by_position(Tupel,Range,Args,Disj_Rest),
   Atom =.. [Pred_Name|Args],
   line_content_to_fact1(Atom,Disj_Rest,Fact).

line_content_to_fact1(Atom,Disj_Rest,Fact) :-
   [Disj_Rest1] = Disj_Rest,
   prolog_atom_to_atoms(Disj_Rest1,Disjunctions),
   Fact = [Atom|Disjunctions].
line_content_to_fact1(Atom,[],Fact) :-
   Fact = [Atom].


/* t_info_to_res_info(Lines,Informations,Res_Info) <-
      parses the table consisting of Lines, and 
      with the rule - Informations, it is computing
      the Resolvent-Information Res_Info */
     
t_info_to_res_info(Lines,Informations,Res_Info) :-
   Informations = [_,_,C],
   flatten(C,C1),
   length(C1,L),
   generate_empty_lists(L,E_Lists),
   t_info_to_res_info(Lines,Informations,E_Lists,Res_Info).
  
t_info_to_res_info([Line|Lines],Informations,E_Lists,[RI1|RI2]) :-
   t_info_line_to_struct(Line,Informations,E_Lists,RI1),
   t_info_to_res_info(Lines,Informations,E_Lists,RI2).
t_info_to_res_info([],_,_,[]).


t_info_line_to_struct(Line,Info,E_Lists,[Head-Body,Disj]) :-
   append(Line,E_Lists,Long_Line),
   position_select(Long_Line,Info,Struct),
   Struct = [A,B,C],
   reconstruct_1(A,Head),
   reconstruct_1(B,Body),
   reconstruct_2(C,Disj).
   
reconstruct_1([Info|Infos],[Atom|Atoms]) :-
   Atom =.. Info,
   reconstruct_1(Infos,Atoms).
reconstruct_1([],[]).

reconstruct_2([Info|Infos],[Disj|Disjs]) :-
   Info = [Info1],
   prolog_atom_to_atoms(Info1,Disj),
   reconstruct_2(Infos,Disjs).
reconstruct_2([_|Infos],[[]|Disjs]) :-
   reconstruct_2(Infos,Disjs).
reconstruct_2([],[]).   

generate_empty_lists(No,[[]|Rest]) :-
  No > 0,
  succ(New_No,No),
  generate_empty_lists(New_No,Rest).
generate_empty_lists(_,[]).


/* d_facts_to_sym_r_states(D_Facts,R_States) <-
      generates symmetric R_States from D_Facts */

d_facts_to_sym_r_states(D_Facts,Sym_R_State) :-
   d_facts_to_sym_r_states(D_Facts,[],Sym_R_State).

d_facts_to_sym_r_states([D_Fact|D_Facts],Sofar,Sym_R_State) :-
   d_fact_to_sym_r_state(D_Fact,Sym_R_State1),
   append(Sofar,Sym_R_State1,New_Sofar),
   d_facts_to_sym_r_states(D_Facts,New_Sofar,Sym_R_State).
d_facts_to_sym_r_states([],Sofar,Sofar).


/* d_fact_to_sym_r_state(D_Fact,Sym_R_State) <-
      generates of a disjunctive Fact : A v B v C ,
      all r_facts of the form [atom1 - [disjunctive rest],...],
      for this example : [A - [B,C], B - [A,C], C - [A,B]]. */

d_fact_to_sym_r_state(D_Fact,Sym_R_State) :-
  length(D_Fact,L),
  list_to_ord_set(D_Fact,Ord_D_Fact),
  d_fact_to_sym_r_state(Ord_D_Fact,1,L,[],Sym_R_State).

d_fact_to_sym_r_state(D_Fact,Pos,L,Sofar,Sym_R_State) :-
   Pos =< L ,
   nth1(Pos,D_Fact,Actual_Atom),
   delete(D_Fact,Actual_Atom,Disjunctions),
   Compound = [Actual_Atom - Disjunctions],
   append(Sofar,Compound,Sofar1),
   succ(Pos,Pos1),
   d_fact_to_sym_r_state(D_Fact,Pos1,L,Sofar1,Sym_R_State).
d_fact_to_sym_r_state(_,_,_,Sofar,Sofar).


/* constants_positions_of_atom(Atom,Position_List) <-
      Atom must not have real variables, but they must be
      represented through terms defined in the fact
      variable_replace_term(X).
      So Position_List stores the positions of the so 
      described non-variables. */

constants_positions_of_atom(Atom,Position_List):-
   functor(Atom,_,Arity),
   const_pos_of_atom(Atom,Arity,1,Position_List).

const_pos_of_atom(Atom,Arity,Pos,[Position|Positions]) :-
   Pos =< Arity,
   arg(Pos,Atom,Value),
   \+ sql_variable_check(Value), 
   Position = Pos,
   succ(Pos,New_Pos),
   const_pos_of_atom(Atom,Arity,New_Pos,Positions).
const_pos_of_atom(Atom,Arity,Pos,Position_List) :-
   Pos =< Arity,
   succ(Pos,New_Pos),
   const_pos_of_atom(Atom,Arity,New_Pos,Position_List).
const_pos_of_atom(_,_,_,[]).


/* constants_positions_of_atom_list(Atoms,List_of_Positions) <-
      */
      
constants_positions_of_atom_list([At|Ats],[Pos_L|Pos_Ls]) :-
   constants_positions_of_atom(At,Pos_L),
   constants_positions_of_atom_list(Ats,Pos_Ls).
constants_positions_of_atom_list([],[]).


/* constants_in_atom(Pred,Args,Const_List,Cond_List) <- 
      */

constants_in_atom(Pred,Args,Const_List,Cond_List) :-
    length(Const_List,L),
    L > 0,
    constants_in_atom(Pred,Args,1,Const_List,Cond_List).
constants_in_atom(_,_,[],[]).

constants_in_atom(Pred,[Arg1|Args],Arg_No,Const_List,[L1|L2]) :-
   member(Arg1,Const_List),
   T1 =.. [Pred,Arg_No],
   T2 = Arg1,
   L1 = [T1,T2],
   succ(Arg_No,New_Arg_No),
   constants_in_atom(Pred,Args,New_Arg_No,Const_List,L2).
constants_in_atom(Pred,[_|Args],Arg_No,Const_List,L1) :-
   succ(Arg_No,New_Arg_No),
   constants_in_atom(Pred,Args,New_Arg_No,Const_List,L1).
constants_in_atom(_,[],_,_,[]).


/* variables_positions_of_atom(Atom,Position_List) <-
      the same functionality as constants_positions_of_atom,
      but the positions of the variables are stored in
      Position_List. */

variables_positions_of_atom(Atom,Position_List) :-
   functor(Atom,_,Arity),
   generate_list_of_successors(1,Arity,List_of_Successors),
   constants_positions_of_atom(Atom,Const_Position_List),
   subtract(List_of_Successors,Const_Position_List,Position_List).


variables_positions_of_atom_list([At|Ats],[Pos_L|Pos_Ls]) :-
   variables_positions_of_atom(At,Pos_L),
   variables_positions_of_atom_list(Ats,Pos_Ls).
variables_positions_of_atom_list([],[]).


/* variables_and_constants_of_atoms(Atoms,Variables,Constants) <- 
      */

variables_and_constants_of_atoms(Atoms,Variables,Constants) :-
   arguments_of_atoms(Atoms,[],Arguments),
   variables_and_constants_of_list(Arguments,Variables,Constants).

arguments_of_atoms([Atom|Atoms],Sofar,List_of_Arguments) :-
   Atom =.. [_|Args],
   append(Sofar,Args,New_Sofar),
   arguments_of_atoms(Atoms,New_Sofar,List_of_Arguments).
arguments_of_atoms([],Sofar,Sofar).


/* variables_and_constants_of_list(List,Variables,Constants) <-
      splits List into list Variables, containing the variables
      of List, and list Constants, containing the constants of
      List. Here a variable is a term with functor F, defined
      through variable_replace_term(F). */

variables_and_constants_of_list([El|Els],[El|Vs],Constants) :-
   variable_replace_term(Var_Repl_Term),
   functor(El,Var_Repl_Term,1),
   variables_and_constants_of_list(Els,Vs,Constants).
variables_and_constants_of_list([El|Els],Variables,[El|Cs]) :-
   variables_and_constants_of_list(Els,Variables,Cs).
variables_and_constants_of_list([],[],[]).


/* same_variables_in_atom(+Predicate,+Arguments,+Variable_List,
         -Condition_List) <-
      */

same_variables_in_atom(Pred,Args,Var_List,Cond_List) :-
   list_to_set(Var_List,Var_Set),
   delete_elements(Var_List,Var_Set,Difference_List),
   Difference_List \== [],
   same_variables_in_atom(Pred,Args,1,Difference_List,Cond_List).
same_variables_in_atom(_,_,_,[]).

same_variables_in_atom(Pred,[Arg1|Args],Arg_No,Diff_List,[L1|L2]) :-
   member(Arg1,Diff_List),
   T1 =.. [Pred,Arg_No],
   nth(Pos,Args,Arg1),
   Absolute_Pos is Pos + Arg_No,
   T2 =.. [Pred,Absolute_Pos],
   L1 = [T1,T2],
   delete_element(Diff_List,Arg1,New_Diff_List),
   succ(Arg_No,New_Arg_No),
   same_variables_in_atom(Pred,Args,New_Arg_No,New_Diff_List,L2).
same_variables_in_atom(Pred,[_|Args],Arg_No,Diff_List,List) :-
   succ(Arg_No,New_Arg_No),
   same_variables_in_atom(Pred,Args,New_Arg_No,Diff_List,List).
same_variables_in_atom(_,[],_,[],[]).
same_variables_in_atom(_,_,_,_,_) :- 
   !,
   fail.


find_arguments_in_atom_list(Pred,Args,[Var|Vars],Atom_List,
      [L1|L2]) :-
   find_first_matching_arg_in_atom_list(Var,Atom_List,Pred2,Pos2),
   nth1(Pos1,Args,Var),
   T1 =.. [Pred,Pos1],
   T2 =.. [Pred2,Pos2],
   L1 = [T1,T2],
   find_arguments_in_atom_list(Pred,Args,Vars,Atom_List,L2).
find_arguments_in_atom_list(Pred,Args,[_|Vars],Atom_List,L1) :-
   find_arguments_in_atom_list(Pred,Args,Vars,Atom_List,L1).
find_arguments_in_atom_list(_,_,[],_,[]).


find_first_matching_arg_in_atom_list(Arg,[Atom1|_],Pred_Name,Pos) :-
   find_first_matching_arg_in_atom(Arg,Atom1,Pred_Name,Pos).
find_first_matching_arg_in_atom_list(Arg,[_|Atom2],Pred_Name,Pos) :-
   find_first_matching_arg_in_atom_list(Arg,Atom2,Pred_Name,Pos).
find_first_matching_arg_in_atom_list(_,[],_,_) :-
   !,
   fail.


find_first_matching_arg_in_atom(Arg,Atom,Pred_Name,Pos):-
     Atom=..[Pred_Name|Arg_List],!,
     nth1(Pos, Arg_List, Arg).


/*********** f_t_arity operations ***************

   a t_arity of a table is number of columns without 
   the disj column, that means columns minus 1. 
   f_t_arity means fact_table_arity. */


f_t_arities_to_f_t_names([F_T_A|F_T_As],[F_T_N|F_T_Ns]) :-
   F_T_A = F_T_N / _,
   f_t_arities_to_f_t_names(F_T_As,F_T_Ns).
f_t_arities_to_f_t_names([],[]).


f_t_arities_to_delta_f_t_arities([Fta|Ftas],[DFta|DFtas]) :-
   dislog_sql_fixpoint(delta_table_suffix,Delta),
   Fta = Pred/Arity,
   concat(Pred,Delta,Pred_Delta),
   DFta = Pred_Delta/Arity,
   f_t_arities_to_delta_f_t_arities(Ftas,DFtas).
f_t_arities_to_delta_f_t_arities([],[]).


f_t_arities_from_state(State,Ord_Set_T_Arities) :-
   flatten(State,Atoms),
   f_t_arities_from_atoms(Atoms,T_Arities),
   list_to_ord_set(T_Arities,Ord_Set_T_Arities).


f_t_arities_from_atoms([Atom|Atoms],[T_Arity|T_Arities]) :-
   functor(Atom,Pred_Name,Arity),
   T_Arity = Pred_Name / Arity,
   f_t_arities_from_atoms(Atoms,T_Arities).
f_t_arities_from_atoms([],[]).


f_t_arities_from_rules(Rules,T_Arities) :-
   f_t_ar_from_rules(Rules,T_Arities_1),
   flatten(T_Arities_1,T_Arities_2),
   list_to_ord_set(T_Arities_2,T_Arities).

f_t_ar_from_rules([Rule|Rules],[T_Arity|T_Arities]) :-
   Rule = Head-_,
   f_t_arities_from_atoms(Head,T_Arity),
   f_t_ar_from_rules(Rules,T_Arities).
f_t_ar_from_rules([],[]).


/* f_t_arities_from_r_state(R_State,
         Ord_Set_of_Table_Arity_Infos) <-
      T_Arities: table_name/arity  */

f_t_arities_from_r_state(R_State,O_Set_of_T_A_Infos) :-
   r_state_to_atoms(R_State,O_Set_of_Atoms),
   f_t_arities_from_atoms(O_Set_of_Atoms,O_Set_of_T_A_Infos).


/******************************************************************/


