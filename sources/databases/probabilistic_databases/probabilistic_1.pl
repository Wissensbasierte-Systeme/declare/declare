

/******************************************************************/
/***                                                            ***/
/***   Databases:  probabilistic_1                              ***/
/***                                                            ***/
/******************************************************************/



/*** modes ********************************************************/


/* defined_p_modes(Modes) <-
      probability modes */

defined_p_modes([
   ignorance,
   independence,
   pos_correlation,
   neg_correlation ]).


/* defined_s_modes(Modes) <-
      subsumtion modes */

defined_s_modes([
   icover,
   ilesse ]).


/* defined_a_modes(Modes) <-
      annotation modes */

defined_a_modes([
   atom_c,
   atom_i_c,
   atom_v,
   atom_i_v,
   formula_c,
   formula_i_c,
   formula_v,
   formula_i_v,
   cond_prob     % not implemented yet
   ]).


/*** interface ****************************************************/


/* empty_interval_prob([A,B]) <-
      */

empty_interval_prob([1,0]).     % representing the empty interval
empty_interval_prob([A,B]) :-
   A > B,
   !.


/* vector_less_or_equal([A|As],[B|Bs]) <-
      */

vector_less_or_equal([A|As],[B|Bs]) :-
   !,
   A =< B,
   vector_less_or_equal(As,Bs).
vector_less_or_equal([],[]).


/* interval_less_or_equal([A1,B1],[A2,B2]) <-
      */

interval_less_or_equal([A1,B1],[A2,B2]) :-
   vector_less_or_equal([A1,B1],[A2,B2]),
   !.


/* interval_greater_than(I1,I2) <-
      */

interval_greater_than(I1,I2) :-
   interval_less_or_equal(I2,I1),
   !.


/* interval_cover(I1,I2) <-
      interval I1 covers interval I2 */

interval_cover([A1,B1],[A2,B2]) :-
   float_between(A1,B1,A2),
   float_between(A1,B1,B2),
   !.


/* sub_interval(I1,I2) <-
      interval I1 is a subinterval of I2 */

sub_interval(I1,I2) :-
   interval_cover(I2,I1),
   !.


/* interval_intersection(I1,I2,I2) <-
      */

interval_intersection(I1,I2,I2) :-
   interval_cover(I1,I2),      
   !.  
interval_intersection(I1,I2,I1) :-
   interval_cover(I2,I1),
   !.     
interval_intersection([A1,B1],[A2,_],[A2,B1]) :-
   float_between(A1,B1,A2),
   !.
interval_intersection([A1,B1],[_,B2],[B2,B1]) :-
   float_between(A1,B1,B2),
   !.
interval_intersection(_,_,I) :-
   empty_interval_prob(I).


/* interval_disjoint(I1,I2) <-
      */

interval_disjoint(I1,I2) :-
   empty_interval_prob(I3),
   interval_intersection(I1,I2,I3),
   !.
   

/* interval_min(I1,I2,I3) <-
      */

interval_min([A1,B1],[A2,B2],[A3,B3]) :-
   n_min(A1,A2,A3),
   n_min(B1,B2,B3),
   !.

n_min(X,Y,X) :-
   number(X),
   number(Y),
   X =< Y,
   !.
n_min(X,Y,Y) :-
   number(X),
   number(Y),
   !.


/* interval_max(I1,I2,I3) <-
      */

interval_max([A1,B1],[A2,B2],[A3,B3]) :-
   n_max(A1,A2,A3),
   n_max(B1,B2,B3),
   !.

n_max(X,Y,X) :-
   number(X),
   number(Y),
   X >= Y,
   !.
n_max(X,Y,Y) :-
   number(X),
   number(Y),
   !.


probability_interval_is_defined(I) :-
   not(empty_interval_prob(I)),
   !.

valid_annotation(A) :-
   probability_interval_is_defined(A).

consistent_annotation(A) :-
   probability_interval_is_defined(A).


% truth ordering less or equal for confidence intervals 

less_or_equal_truth(I1-J1,I2-J2) :-
   interval_less_or_equal(I1,I2),
   interval_less_or_equal(J2,J1).
   

% knowledge (information) ordering
% less or equal for confidence intervals 

less_or_equal_knowledge(I1-J1,I2-J2) :-
   less_or_equal_truth(I1-J2,I2-J1).


% precision ordering less or equal for confidence intervals 

less_or_equal_precision(I1-J1,I2-J2) :-
   interval_cover(I1,I2),   
   interval_cover(J1,J2),   
   !.


% "oplus" and "otimes" operators as defined by Subrahmanian et. al.

oplus([A1,B1],[A2,B2],[A3,B3]) :-
   probability_interval_is_defined([A1,B1]),
   probability_interval_is_defined([A2,B2]),
   n_max(A1,A2,A3),
   Temp is B1 + B2,
   n_min(1,Temp,B3).

otimes([A1,B1],[A2,B2],[A3,B3]) :-
   probability_interval_is_defined([A1,B1]),
   probability_interval_is_defined([A2,B2]),
   Temp is A1 + A2 - 1,
   n_max(0,Temp,A3),
   n_min(B1,B2,B3).


% operators "meet" and "join" as defined by Lakshmanan et. al.
% ([Belief1]-[Doubt1],[Belief2]-[Doubt2], [Belief3]-[Doubt3])

meet_truth([A1,B1]-[C1,D1],[A2,B2]-[C2,D2],[A3,B3]-[C3,D3]) :-
   n_min(A1,A2,A3),
   n_min(B1,B2,B3),
   n_max(C1,C2,C3),
   n_max(D1,D2,D3),
   !.
   
join_truth([A1,B1]-[C1,D1],[A2,B2]-[C2,D2],[A3,B3]-[C3,D3]) :-
   n_max(A1,A2,A3),
   n_max(B1,B2,B3),
   n_min(C1,C2,C3),
   n_min(D1,D2,D3).

% swap belief and doubt for conf(not F)

swap_confidence([A,B]-[C,D],[C,D]-[A,B]).


% we only use consistent confidence levels

consistent_confidence([A,B]-[C,D]) :-
   A =< B,
   C =< D,
   A + C =< 1,
   B + D =< 1,
   !.

% conj_ignorance([A1,B1]-[C1,D1],[A2,B2]-[C2,D2],[A3,B3]-[C3,D3])

conj_ignorance_cl([A1,B1]-[C1,D1],[A2,B2]-[C2,D2],[A3,B3]-[C3,D3]) :-
   Temp_1 is A1 + A2 - 1,
   n_max(0, Temp_1, A3),
   n_min(B1, B2, B3),
   n_max(C1, C2, C3),
   Temp_2 is D1 + D2,
   n_min(1, Temp_2, D3).

disj_ignorance_cl(
      [A1,B1]-[C1,D1],[A2,B2]-[C2,D2],[A3,B3]-[C3,D3]) :-
   n_max(A1, A2, A3),
   Temp_1 is B1 + B2,
   n_min(1, Temp_1, B3),
   Temp_2 is C1 + C2 - 1,
   n_max(0, Temp_2, C3),
   n_min(D1, D2, D3).

% the same like meet_truth/3 

conj_pos_correlation(
      [A1,B1]-[C1,D1],[A2,B2]-[C2,D2],[A3,B3]-[C3,D3]) :-
   n_min(A1, A2, A3),
   n_min(B1, B2, B3),
   n_max(C1, C2, C3),
   n_max(D1, D2, D3),
   !.

disj_pos_correlation(
      [A1,B1]-[C1,D1],[A2,B2]-[C2,D2],[A3,B3]-[C3,D3]) :-
   n_max(A1, A2, A3),
   n_max(B1, B2, B3),
   n_min(C1, C2, C3),
   n_min(D1, D2, D3),
   !.


/*** P-strategies *************************************************/


/*** independence ***/

% conj_independence p-strategy

conj_independence([A1,B1],[A2,B2],[A3,B3]) :-
   A3 is A1 * A2,
   B3 is B1 * B2,
   !.

% disj_independence p-strategy

disj_independence([A1,B1],[A2,B2],[A3,B3]) :-
   Temp_1 is A1 + A2 - A1 * A2,
   n_min(1, Temp_1, A3), 
   Temp_2 is B1 + B2 - B1 * B2,
   n_min(1, Temp_2, B3),
   !.


/*** ignorance ***/

% conj_ignorance p-strategy

conj_ignorance([A1,B1],[A2,B2],[A3,B3]) :-
   Temp_1 is A1 + A2 -1, 
   n_max(0, Temp_1, A3),
   n_min(B1, B2, B3),
   !.

% disj_ignorance p-strategy

disj_ignorance([A1,B1],[A2,B2],[A3,B3]) :-
   n_max(A1, A2, A3),
   Temp is B1 + B2,
   n_min(1, Temp, B3),
   !.


/*** positive correlation ***/

% conj_positive_correlation p-strategy

conj_positive_correlation([A1,B1],[A2,B2],[A3,B3]) :-
   n_min(A1, A2, A3),
   n_min(B1, B2, B3),
   !.

% disj_positive_correlation p-strategy

disj_positive_correlation([A1,B1],[A2,B2],[A3,B3]) :-
   n_max(A1, A2, A3),
   n_max(B1, B2, B3),
   !.


/*** negative correlation ***/

% conj_negative_correlation p-strategy -> not coherent

conj_negative_correlation(_,_,[0,0]).  


% disj_negative_correlation p-strategy

disj_negative_correlation([A1,B1],[A2,B2],[A3,B3]) :-
   Temp_1 is A1 + A2,
   n_min(1,Temp_1,A3),
   Temp_2 is B1 + B2,
   n_min(1,Temp_2,B3),
   !.

conjunctive_maximal_interval([A,_],[A,1]).

disjunctive_maximal_interval([_,B],[0,B]).
   
atomic_function(_:I,I).
   

/* clause_subsumes_clause(C1,C2)
      for each Atom A2 in C2 there 
      exists an Atom A1 in C1
      such that A1 subsumes A2 */

clause_subsumes_clause([A1|As1],Clause) :-
   member(A2,Clause),
   atom_subsumes_atom(A1,A2),
   clause_subsumes_clause(As1,Clause).
clause_subsumes_clause([],_).

atom_subsumes_atom(A1, A2) :-
   dislog_flag_get(s_mode, ilesse),
   split_annotated_atom(A1, Atom1, Annot1),  
   split_annotated_atom(A2, Atom2, Annot2),
   Atom1 == Atom2,
   interval_less_or_equal(Annot2, Annot1),
   !.
atom_subsumes_atom(A1, A2) :-
   dislog_flag_get(s_mode, icover),
   split_annotated_atom(A1, Atom1, Annot1),
   split_annotated_atom(A2, Atom2, Annot2),
   Atom1 == Atom2,
   interval_cover(Annot2, Annot1),
   !.

/*
clause_subsumes_clause(Clause,[Atom|Atoms]) :-
   clause_subsumes_atom(Clause, Atom),
   !,
   clause_subsumes_clause(Clause, Atoms).
clause_subsumes_clause(_,[]).   

clause_subsumes_atom([A1|_],A2) :-
   dislog_flag_get(s_mode,ilesse),
   split_annotated_atom(A1,Atom1,Annot1),  
   split_annotated_atom(A2,Atom2,Annot2),
   Atom1 == Atom2,
   interval_less_or_equal(Annot2,Annot1),
   !.
clause_subsumes_atom([A1|_],A2) :-
   dislog_flag_get(s_mode,icover),
   split_annotated_atom(A1,Atom1,Annot1),
   split_annotated_atom(A2,Atom2,Annot2),
   Atom1 == Atom2,       
   interval_cover(Annot2,Annot1),
   !.
clause_subsumes_atom([_|Atoms],A2) :-
   clause_subsumes_atom(Atoms,A2).
clause_subsumes_atom([],[]).
clause_subsumes_atom([],_) :-
   !,
   fail.
*/

/* state_subsumes_clause(S,C) <-
      there exist at least one 
      clause C1 in S such that
      C1 subsumes C */

state_subsumes_clause(S,C) :- 
   member(C1,S),
   clause_subsumes_clause(C1,C),
   !.

/* state_subtract_prob(S1,S2,S3) <-
      computes the State S3 containing 
      all the Clauses from S1 which are
      not subsumed by Clauses from S2 */

state_subtract_prob(State1,State2,State3,N) :-
   write('delta prob     '), ttyflush,
   start_timer(state_subtract_prob),
   state_subtract_prob(State1,State2,State3),
   length(State3,N),
   stop_timer(state_subtract_prob).

state_subtract_prob(S1,S2,S3) :-
   state_subtract_prob_loop(S1,S2,[],S3).

state_subtract_prob_loop([C1|S1],S2,S3,State) :-
   state_subsumes_clause(S2,C1),
   !,
   write_debug_list([C1,' is subsumed']),
   state_subtract_prob_loop(S1,S2,S3,State).
state_subtract_prob_loop([C1|S1],S2,S3,State) :-
   state_subtract_prob_loop(S1,S2,[C1|S3],State).
state_subtract_prob_loop([],_,S3,State) :-
   reverse(S3,State).


/* state_can_pprob(S1,S2) <-
      S2 is the canoical state from S1 */

% state_can_prob(prob_1,State_1,State_2) :-

state_can_prob(S1,S2) :-
   state_can_prob(S1,[],S2).

state_can_prob([C1|Cs],S2,S) :-
   member(C2,Cs),
   write_debug_list([C1,'.....',C2]),
   clause_subsumes_clause(C2,C1),
   state_can_prob(Cs,S2,S).
state_can_prob([C1|Cs],S2,S) :-
   append(S2,[C1],New_S),
   state_can_prob(Cs,New_S,S).
state_can_prob([],Akk,Akk).
      


% state_can(Mode,State_1,State_2) :-
%    state_can_prob(Mode,State_1,State_2).

% state_can_prob(prob_2,State_1,State_2) :-

prune_state(State,P_State) :-
   prune_state(State,State,P_State),
   !.

prune_state([Fact|Facts],State,NS) :-
   is_subsumed(Fact,Facts),
   delete(State, Fact, Dummy_State),
   append(Dummy_State,[Fact],New_State),
   prune_state(Facts,New_State,NS).
prune_state([_|Facts],State,NS) :-
   prune_state(Facts,State,NS).
prune_state([],Akk,Akk).


is_subsumed([Fact1],[[Fact2]|_]) :-
   dislog_flag_get(s_mode,ilesse),
   Fact1=..[:,Pred,Annot1],
   Fact2=..[:,Pred,Annot2],
   interval_less_or_equal(Annot1,Annot2),
   write(Fact1),write(' supersumes '),writeln(Fact2),
   !.
is_subsumed([Fact1],[[Fact2]|_]) :-
   dislog_flag_get(s_mode,ilesse),
   Fact1=..[:,Pred,Annot1],         
   Fact2=..[:,Pred,Annot2],             
   interval_cover(Annot1,Annot2),
   write(Fact1),write(' supersumes '),writeln(Fact2),
   !.
is_subsumed(Fact,[_|Facts]) :-
   is_subsumed(Fact,Facts).



/* select_all_clauses_from_program(Program,Clause_List) <-
   compute the List Clause_List of all clauses of the
   program Program */
 
select_all_clauses_from_program(Program,Clause_List) :-
   announcement(12, select_all_clauses_from_program),
   select_all_clauses_from_program_loop(Program,[],Clause_List).
select_all_clauses_from_program_loop([A-B|C],Clauses,Return_List) :-
   append(Clauses,[A-B],New_List),
   select_all_clauses_from_program_loop(C,New_List,Return_List).
select_all_clauses_from_program_loop([A|C],Clauses,Return_List) :-
   append(Clauses,[A],New_List),
   select_all_clauses_from_program_loop(C,New_List,Return_List).
select_all_clauses_from_program_loop([],Akk,Akk).
 

/* select_all_facts_from_program(Program,Fact_List) <-
   compute the List Fact_List of all facts of the
   program Program */
 
select_all_facts_from_program(Program,Fact_List) :-
   announcement(12, select_all_facts_from_program),
   select_all_facts_from_program_loop(Program,[],Fact_List).
select_all_facts_from_program_loop([_-_|C],Facts,Return_List) :-
   select_all_facts_from_program_loop(C,Facts,Return_List).
select_all_facts_from_program_loop([A|C],Facts,Return_List) :-
   append(Facts,[A],New_List),
   select_all_facts_from_program_loop(C,New_List,Return_List).
select_all_facts_from_program_loop([],Akk,Akk).
 


/* some pretty-prints and tool-predicates */
  
split_annotated_atom(Annotated_Atom,Atom,Annotation) :-
   Annotated_Atom =.. [:,Atom,Annotation],
   !.

create_annotated_atom(Atom,Annotation,Annotated_Atom) :-
   split_annotated_atom(Annotated_Atom,Atom,Annotation).


increment_iteration_counter :-
   dislog_flag_get(iteration_counter, Old_Iteration_Counter),
   Iteration_Counter is Old_Iteration_Counter + 1,
   dislog_flag_set(iteration_counter, Iteration_Counter),
   write('  '), write(Iteration_Counter),
   writeln('.  iteration'),
   bar_line,
   !.

increment_iteration_counter :- 
   reset_iteration_counter.

reset_iteration_counter :-
   dislog_flag_set(iteration_counter, 0),
   !.

reset_gensym(Which_One) :-
   concat('$gs_',Which_One,Flag),
   flag(Flag,_,0).

write_debug(Number,Atom,Annotation,Remark) :-
   dislog_flag_get(debug, 1),
   !,
   write_list([
      '\n',
      Number,'. Atom: ',Atom,
      '   Annotation: ',Annotation,
      '   Remark: ',Remark,'\n']).

write_debug(Atom,Annotation) :-
   dislog_flag_get(debug, 1),
   !,
   write_list([
      '\n',
      'Atom: ',Atom,
      '   Annotation: ',Annotation,'\n']).

write_debug_list(List) :-
   dislog_flag_get(debug, 1),
   !,
   write_list(List),
   nl,
   !.

write_debug_list(_).

write_debug(String) :-
   dislog_flag_get(debug, 1),
   !,
   writeln(String),
   !.

write_debug(_).  

debug_on :-
   dislog_flag_set(debug, 1),
   !.

debug_off :-
   dislog_flag_set(debug, 0),
   !.

print_state(state) :-
   dconsult(state,State),
   write(State).


dislog_flag_store(Flag) :-
   dislog_flag_get(Flag, Value),
   dislog_flag_set(temp_flag_dislog_prob_4_1998, Value),
   !.

dislog_flag_restore(Flag) :-
   dislog_flag_get(temp_flag_dislog_prob_4_1998, Value),
   dislog_flag_set(Flag, Value),
   !.


/******************************************************************/


