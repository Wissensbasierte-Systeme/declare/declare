

/******************************************************************/
/***                                                            ***/
/***   probabilistic_2                                          ***/
/***   tps_fixpoint_prob and tps_fixpoint/pfunc_generation      ***/
/***                                                            ***/
/******************************************************************/


/*** initialisation ***********************************************/

p_init :- 
   dcd(probabilistic),     % change example directory
   dislog_flags_set([
      join_mode:special,
      tps_mode:2,
      p_mode:ignorance,
      a_mode:atom_i_v,
      p_input:p_423,       % set the default input file
      trace_level:2,
      debug:1,
      iteration_counter:0,
      temp_file:tmp_pillo,
%     s_mode:icover.
      s_mode:ilesse ]).


/*** interface ****************************************************/

% using the standard tps-operator in special mode 
% transforming the program by adding p-functions

tpp_1(Program_Id) :-
   dconsult(Program_Id,Program),
   pfunc_generation(Program,Program_2),
   dportray(lp,Program),
   star_line,
   dportray(lp,Program_2),
   star_line,
   writeln(user, Program_2),
   start_timer(tpp),
   writeln(user,tps_fixpoint(Program_2,[],State)),
   tps_fixpoint(Program_2,[],State),
   writeln(user,tps_fixpoint_prob(Program_2,[],State)),
   prune_state(State,Pruned_State),
   stop_timer(tpp),
   dportray(dhs,State),
   writeln('Pruned state: '),
   dportray(dhs,Pruned_State),
   !.


% probabilistic tps-operator without p_func-generation

tpp_2(Program_Id) :-
   dconsult(Program_Id,Program),
   dportray(lp,Program),
   tps_fixpoint_prob(Program,State),
   dportray(dhs,State),
   !.


% the normal tps-operator

tpp_0(Program_Id) :-
   dislog_flag_store(a_mode),
   dconsult(Program_Id,Program),
   dislog_flag_set(a_mode, atom_c),
   write(Program),
   dportray(lp, Program),
   tps_fixpoint(Program, [], State),
   dportray(dhs, State),
   dislog_flag_restore(a_mode),
   !.


/*** implementation ***********************************************/


tpp_1 :-
   dislog_flag_get(p_input, Program_Id),
   tpp_1(Program_Id).

tpp_2 :-
   dislog_flag_get(p_input, Program_Id),
   tpp_2(Program_Id).


base_literal(L) :-
   functor(L,:,_).

pfunc(P1,P2,P3) :-
   dislog_flag_get(a_mode, atom_c),
   !,
   P3 is P1 * P2.

pfunc(_,_,_) :-
   writeln('a_mode is not defined ...'),
   !,
   fail.

pfunc(I1,I2) :-
   dislog_flag_get(s_mode, ilesse),
   !,
   interval_less_or_equal(I1, I2),
   write(user, 'regel feuert! '),
   !.

pfunc(I1,I2) :-
   dislog_flag_get(s_mode, icover),
   !,
   interval_cover(I1,I2),
   write(user, 'regel feuert! '),
   !.

pfunc(_,_) :-
   writeln('p_mode is not defined ...').

pfunc_generation(Program_1, Program_2) :-
   pfunc_generation(Program_1, [], Program),
   dislog_flag_get(temp_file, File),
   dsave(Program,File),
   !,
   dconsult(File,Program_2),
   !.

pfunc_generation([[Fact]|Facts], Prog, NP) :-
   write_debug_list(['Fact: ', Fact]),
   append(Prog, [[Fact]], New_Prog),
   pfunc_generation(Facts, New_Prog, NP).
pfunc_generation([R_Head-R_Body|Rules], Prog, NP) :-
   write_debug_list(['Head: ', R_Head]),
   write_debug_list(['Body: ', R_Body]),
   reset_gensym('A'),
   reset_gensym('B'),
   add_pfunc(R_Body, [], New_Body),
   write_debug_list(['New_Body: ', New_Body]),
   append(Prog, [R_Head-New_Body], New_Prog),
   pfunc_generation(Rules, New_Prog, NP).
pfunc_generation([], Prog, Prog).
   
add_pfunc([Annotated_Atom|Atoms],Body,NB) :-
   Annotated_Atom =..[:,Atom,Annotation],
   write_debug_list(['Atom: ',Atom]),
   Annotation = [A1,A2],
   gensym('A',X),
   gensym('B',Y),
   append([Atom:[X,Y],pfunc([A1,A2],[X,Y])],Body,New_Body),
%  writeln(New_Body),
   add_pfunc(Atoms,New_Body,NB).
add_pfunc([],Akk,Akk).

/* end of tpp_1 implementation - see tps_fixpoint */


/* tps_fixpoint_prob <-
      Basic predicate for computing the least fixpoint of the
      T_ps_prob-operator of a probabilistic DLP */

tps_fixpoint_prob(Program,State) :-
   reset_iteration_counter,
   select_all_clauses_from_program(Program,Program_C),
   select_all_facts_from_program(Program,State_P),
%  writeln(tps_fixpoint_prob(Program_C*State_P*State_P*State*[])),
   tps_fixpoint_prob(Program_C,State_P-State_P,State-[]),   
   !.

tps_fixpoint_prob(Program,S1-D1,S2-D2) :-
   write_debug(tps_fixpoint_prob(Program,S1*D1*S2*D2)),
   tps_delta_operator_dc_prob(Program,S1-D1,S-D),
   tps_fixpoint_prob_if(Program,S-D,S2-D2).

tps_fixpoint_prob_if(_,S-[],S-[]) :-
   !.
tps_fixpoint_prob_if(Program,S-D,S2-D2) :-
   tps_fixpoint_prob(Program,S-D,S2-D2).

tps_delta_operator_dc_prob(Program,S1-D1,S2-D2) :-
   increment_iteration_counter,
   write_debug(tps_delta_operator_dc_prob(Program,S1-D1,S2-D2)),
%  start_timer(tps_single_iteration_prob),      
% no function when timer runs
   delta_join_prob(Program,S1,D1,State),
   write('Delta-State:  '),writeln(State),
%  writeln(delta_join_prob(Program*S1*D1*State)),
   prune_state(State,Pruned_State),   
%  WARNING: prune_state is a DisLog predicate too
   write('state:        '),writeln(State),
   write('pruned state: '),writeln(Pruned_State),
   state_subtract_prob(Pruned_State,S1,Delta_State),
   write_debug(state_subtract_prob(Pruned_State*S1*Delta_State)),
%  wait,
   state_can_prob(Delta_State,D2),
%  write_debug(state_can_prob(Delta_State*D2)),
%  wait,
   state_subtract_prob(S1,D2,State_2),
%  write_debug(state_subtract_prob(S1*D2*State_2)),
%  wait,
   state_union(State_2,D2,State_3),
%  writeln(state_union(State_2*D2*State_3)),
%  wait,
   state_can_prob(State_3,S2),
%  writeln(tps_delta_operator_dc_prob(S1*D1*S2*D2)),
%  dportray(dhs,S1),
%  dportray(dhs,D1),
%  dportray(dhs,S2),
%  dportray(dhs,D2),
%  wait,
%  stop_timer(tps_single_iterarion_prob),
   writeln('end of iteration ...').


delta_join_prob(Program,State1,State2,State3) :-
   write_debug(delta_join_prob(Program,State1,State2,State3)),
   dislog_flag_get(join_mode,special),
%  write('delta join prob   '), ttyflush,
   start_timer(delta_join_prob),
   findall(Pruned_Fact,
      ( member(Head-Body,Program),
        Body \== [],
%       write_debug(bi_delta_resolvable_prob(State1,State2,Body,Clause)),
        bi_delta_resolvable_prob(State1,State2,Body,Clause),
        head_ord_union(Head,Clause,Fact),
        remove_duplicates(Fact,Pruned_Fact),
        ord_disjoint(Pruned_Fact,Body)),
      State3),
   stop_timer(delta_join_prob).



bi_delta_resolvable_prob(State1,State2,[L|Ls],Clause) :-
   write_debug(bi_delta_resolvable_prob(State1,State2,[L|Ls],Clause)),
%  write_list([State1,State2, L, Clause]),nl,
   append(Xs,[Y|Ys],[L|Ls]),
   append(Xs,Ys,Zs),
   base_literal(Y),
%  writeln(Y),           
%  writeln(State2),
   member(Fact,State2),
   write_debug(resolve_prob(L,Fact,Clause1)),
   resolve_prob(Y,Fact,Clause1),
   write_debug_list(['resolved --> ',Clause1]),
   write_debug(bi_resolvable_prob(State1,Zs,Clause2)),
   bi_resolvable_prob(State1,Zs,Clause2),
   ord_union(Clause1,Clause2,Clause).
bi_delta_resolvable_prob(_,_,[],[]).


delta_resolvable_prob(State1,State2,[L|Ls],Clause) :-
   append(Xs,[Y|Ys],[L|Ls]), 
   append(Xs,Ys,Zs),
   member(Fact,State2), 
   resolve_prob(Y,Fact,Fact1),
   resolvable_prob(State1,Zs,Clause1),
   ord_union(Fact1,Clause1,Clause).
delta_resolvable_prob(_,_,[],[]).

resolvable_prob(State,[L|Ls],Clause) :-
   member(Fact,State), 
   resolve_prob(L,Fact,Clause1),
   resolvable_prob(State,Ls,Clause2),
   ord_union(Clause1,Clause2,Clause).
resolvable_prob(_,[],[]).


resolve_prob(Atom:Annot1,[Atom:Annot2|Ls],Ls) :-
   dislog_flag_get(s_mode, ilesse),
   Annot1 = [A1,B1],
   Annot2 = [A2,B2],
   ( ( %  nonvar(A2),
       %  nonvar(B2),
       var(A1), 
       var(B1), 
       A1 = A2,
       B1 = B2 )       
   ; ( %  nonvar(A2),
       %  nonvar(B2),
       nonvar(A1), 
       var(B1),    
       A1 =< A2,
       B1 = B2 )
   ; ( %  nonvar(A2),
       %  nonvar(B2),
       var(A1), 
       nonvar(B1), 
       B1 =< B2,
       A1 = A2 )
   ; ( nonvar(A1),
       nonvar(B1),
       nonvar(A2),
       nonvar(B2),
       interval_less_or_equal(Annot1,Annot2) )
   ).
resolve_prob(Atom:Annot1,[Atom:Annot2|Ls],Ls) :-
   dislog_flag_get(s_mode, icover),
   Annot1 = [A1,B1],
   Annot2 = [A2,B2],
   ( ( %  nonvar(A2),
       %  nonvar(B2),
       var(A1),
       var(B1),
       A1 = A2,
       B1 = B2 )
   ; ( %  nonvar(A2),
       %  nonvar(B2),
       nonvar(A1),
       var(B1),
       A1 =< A2,
       B1 = B2 )
   ; ( %  nonvar(A2),
       %  nonvar(B2),
       var(A1),
       nonvar(B1),
       B2 =< B1,    % !!!! 
       A1 = A2 )
   ; ( nonvar(A1),
       nonvar(B1),
       nonvar(A2),
       nonvar(B2),
       interval_cover(Annot1,Annot2) )  % !!!!
   ).
resolve_prob(Literal,[L|Ls],[L|Ls1]) :-
   resolve_prob(Literal,Ls,Ls1).
 


bi_resolvable_prob(State,[L|Ls],Clause) :-
   \+ base_literal(L),
   !,
   call(L),
   bi_resolvable_prob(State,Ls,Clause).
bi_resolvable_prob(State,[L|Ls],Clause) :-
   member(Fact,State),
   write_debug(resolve_prob(Fact,[L],Clause1)),
   resolve_prob(L,Fact,Clause1),          
   write_debug('resolved in bi_resolvable '),
   bi_resolvable_prob(State,Ls,Clause2),
   ord_union(Clause1,Clause2,Clause).
bi_resolvable_prob(_,[],[]).


/*
resolve_prob([Atom:Annot1],[Atom:Annot2|Annot_Atoms],Annot_Atoms) :-
   interval_less_or_equal(Annot2,Annot1).
resolve_prob(A,[B|C],[B|D]) :-
   resolve_prob(A,C,D).
*/

