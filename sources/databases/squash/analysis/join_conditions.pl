

/******************************************************************/
/***                                                            ***/
/***        Squash:  Join Conditions                            ***/
/***                                                            ***/
/******************************************************************/


:- home_directory(Home),
   concat([ Home, '/teaching/diplomarbeiten/Wetzka_Matthias',
      '/2007_02_25/Dokumente/fallstudie_mascot.xml' ], File ),
   dislog_variable_set(mascot_file, File).

:- dynamic
      foreign_key_constraints_asserted/1.


/*** interface ****************************************************/


/* xml_to_join_conditions_to_picture(Xml, Picture) <-
      */

xml_to_join_conditions_to_picture(Xml, Picture) :-
   xml_to_join_conditions(Xml, Conditions),
   join_conditions_to_picture(Xml, Conditions, Picture).


/* xml_to_join_conditions(Xml, Conditions) <-
      */

xml_to_join_conditions(Xml, Conditions) :-
   set_num(squash_join_condition, 0),
   findall( N-Condition,
      ( xml_to_join_condition(Xml, Condition),
        get_num(squash_join_condition, N) ),
      Conditions ),
   !,
   writeq(user, Conditions).


/* join_conditions_to_picture(Xml, Conditions, Picture) <-
      */

join_conditions_to_picture(Xml, Conditions, Picture) :-
   ( foreign_key_constraints_asserted(_)
   ; xml_to_foreign_key_constraints(Xml, Constraints),
     assert(foreign_key_constraints_asserted(Constraints)) ),
   join_conditions_to_vertices(Xml, Conditions, Vertices),
   join_conditions_to_edges(Conditions, Edges),
   vertices_edges_to_picture_bfs(Vertices, Edges, Picture).


/* xml_to_from_tables(Xml, Tables) <-
      */

xml_to_from_tables(Xml, Tables) :-
   findall( Table:Alias,
      Table := Xml^select^subquery^from^_
         ^query_table_reference::[^alias@name = Alias]
         ^_^simple_query_table_expression@object,
      Tables ).


/* xml_to_join_condition(Xml, T1:A1=T2:A2) <-
      */

xml_to_join_condition(Xml, T1:A1=T2:A2) :-
   Select := Xml^select,
   ( C := Select^_^simple_comparison_condition::[@operator=(=)]
   ; C := Select^_^outer_join_condition ),
   [T1, C1] := C^left_expr
      ^expr^simple_expr^object@[table_view, column],
   [T2, C2] := C^right_expr
      ^expr^simple_expr^object@[table_view, column],
   squash_attribute_pretty(C1, A1),
   squash_attribute_pretty(C2, A2).

squash_attribute_pretty(C, A) :-
   Substitution_1 = [
      ["ID", "ID_"], ["RESULTS", "RESULTS_"],
      ["RES", "RES_"], ["SEARCH", "SEARCH_"],
      ["TBL", "TBL_"], ["MASCOT", "MASCOT_"] ],
   name_exchange_sublist(Substitution_1, C, B),
   Substitution_2 = [
      ["PEPTID_E", "PEPTIDE"] ],
   name_exchange_sublist(Substitution_2, B, A),
   !.


/* join_conditions_to_vertices(Xml, Conditions, Vertices) <-
      */

join_conditions_to_vertices(Xml, Conditions, Vertices) :-
   xml_to_from_tables(Xml, Tables),
   maplist( join_condition_to_vertices(Tables),
      Conditions, Xs ),
   append(Xs, Vertices_2),
   sort(Vertices_2, Vertices).

join_condition_to_vertices(Tables, N-(T1:A1=T2:A2), Vertices) :-
   ( foreign_key_constraint_exists(Tables, T1:A1=T2:A2) ->
     Colour = blue
   ; Colour = white ),
   term_to_atom(T1:A1=T2:A2, C),
%  concat([N, ' ', C], C_Label),
%  concat('fk', N, C_Label),
   C_Label = N,
   Vertices = [
      T1-T1-box-grey-medium,
      T2-T2-box-grey-medium,
      C-C_Label-rhombus-Colour-medium ].

foreign_key_constraint_exists(Tables, T1:A1=T2:A2) :-
   foreign_key_constraints_asserted(Constraints),
   ( member(_-(S1:A1=S2:A2), Constraints)
   ; member(_-(S2:A2=S1:A1), Constraints) ),
   squash_table_synonym(Tables, T1, S1),
   squash_table_synonym(Tables, T2, S2).

squash_table_synonym(Tables, T1, T2) :-
   member(T:T1, Tables),
   member(T:T2, Tables).


/* join_conditions_to_edges(Conditions, Edges) <-
      */

join_conditions_to_edges(Conditions, Edges) :-
   maplist( join_condition_to_edges,
      Conditions, Xs ),
   append(Xs, Edges).

join_condition_to_edges(_-(T1:A1=T2:A2), [T1-C, C-T2]) :-
   term_to_atom(T1:A1=T2:A2, C).


/*** tests ********************************************************/


test(squash, xml_to_join_conditions_to_picture) :-
   dislog_variable_get(mascot_file, File),
   dread(xml, File, [Xml]),
   xml_to_join_conditions_to_picture(Xml, _).


/******************************************************************/


