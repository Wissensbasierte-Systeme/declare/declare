

/******************************************************************/
/***                                                            ***/
/***        Squash:  Foreign Key Constraints                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xml_to_foreign_key_constraints_to_picture(Xml, Picture) <-
      */

xml_to_foreign_key_constraints_to_picture(Xml, Picture) :-
   xml_to_foreign_key_constraints(Xml, Constraints),
   join_conditions_to_picture(Xml, Constraints, Picture).


/* xml_to_foreign_key_constraints(Xml, Constraints) <-
      */

xml_to_foreign_key_constraints(Xml, Constraints) :-
   xml_to_from_tables(Xml, Tables),
   xml_to_foreign_key_constraints(Xml, Tables, Constraints_2),
   writeq(user, Constraints_2),
   findall( N-(Ta1:A1=Ta2:A2),
      ( member(N-(T1:A1=T2:A2), Constraints_2),
        table_names_to_aliases(Tables, T1, T2, Ta1, Ta2) ),
      Constraints ),
   writeq(user, Constraints).

table_names_to_aliases(Tables, T1, T2, Ta1, Ta2) :-
   table_name_to_alias(Tables, T1, Ta1),
   table_name_to_alias(Tables, T2, Ta2),
   !.

table_name_to_alias(Tables, T, Ta) :-
   member(Table:Ta, Tables),
   squash_attribute_pretty(Table, T),
   !.
table_name_to_alias(_, T, _) :-
   write_list(user,
      ['table_name_to_alias failed for ', T, ' !\n'] ),
   fail.


/* xml_to_foreign_key_constraints(Xml, Tables, Constraints) <-
      */

xml_to_foreign_key_constraints(Xml, Tables, Constraints) :-
   set_num(squash_foreign_key_constraint, 0),
   findall( N-Constraint,
      ( member(Table, Tables),
        xml_to_foreign_key_constraint(Xml, Table, Constraint),
        get_num(squash_foreign_key_constraint, N) ),
      Constraints ),
   !.


/* xml_to_foreign_key_constraint(Xml, Table:_, T1:A1=T2:A2) <-
      */

xml_to_foreign_key_constraint(Xml, Table:_, T1:A1=T2:A2) :-
   squash_attribute_pretty(Table, T1),
   Create := Xml^create_table::[@name=Table],
   C := Create^_^out_of_line_foreign_key_constraint,
   C1 := C^column@name,
   T := C^references_clause@object,
   squash_attribute_pretty(C1, A1),
   squash_attribute_pretty(T, T2),
   ( xml_to_primary_key(Xml, T, C2),
     squash_attribute_pretty(C2, A2)
   ; \+ xml_to_primary_key(Xml, T, C2),
     A2 = '??' ).


/* xml_to_primary_key(Xml, Table, Key) <-
      */

xml_to_primary_key(Xml, Table, Key) :-
   Create := Xml^create_table::[@name=Table],
   _ := Create^_^column_def::[@name=Key]
      ^inline_constraint^inline_primary_key_constraint.
   

/*** tests ********************************************************/


test(squash, xml_to_foreign_key_constraints_to_picture) :-
   dislog_variable_get(mascot_file, File),
   dread(xml, File, [Xml]),
   xml_to_foreign_key_constraints_to_picture(Xml, _).


/******************************************************************/


