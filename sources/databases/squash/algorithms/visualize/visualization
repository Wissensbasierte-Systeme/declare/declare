

/******************************************************************/
/***                                                            ***/
/***            Database Schema Visualization                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* draw_schema_graph(Xml) <-
      Draws the table-foreign_key-graph of the database schema. */

draw_schema_graph(Xml) :-
   draw_schema_graph(Xml, _).


/* draw_schema_graph(Xml, Picture) <-
      Draws the table-foreign_key-graph of the database schema. */

draw_schema_graph(Xml, Picture) :-
   findall( Node,
      ( Table_Name := Xml^create_table@name,
        Node = node:[id:Table_Name]:[
           prmtr:[color:white, size:medium, symbol:text_in_box]:[
                 string:[bubble:'']:[Table_Name] ] ] ),
      Ts ),
   findall( Edge,
      ( is_foreign_key(Xml, Table_Name, K_1, Ref, K_2, _),
        term_to_atom(K_1-K_2, Bubble),
        Edge = edge:[from:Table_Name, to:Ref]:[
               prmtr:[arrows:both, color:black,
                  first_arrow:first_arrow,
                  second_arrow:second_arrow]:[
                     string:[bubble:Bubble]:[] ] ] ),
      Rs ),
   draw_graph(Ts, Rs, Picture).


/* draw_schema_graph_color_outgoing(Xml, Picture) <-
      Draws the schema graph and colors the vertices
      according to the number of outgiong edges,
      which corresponds to the foreign key references. */

draw_schema_graph_color_outgoing(Xml, Picture) :-
   findall( Node,
      ( Table_Name := Xml^create_table@name,
        count_foreign_keys(Xml, Table_Name, Count),
        number_to_colour(Count, Colour),
        concat([Table_Name, '-', Count], Bubble),
        Node = node:[id:Table_Name]:[
           prmtr:[color:Colour, size:medium, symbol:text_in_box]:[
                 string:[bubble:'']:[Bubble] ] ] ),
      Ts ),
   findall( Edge,
      ( is_foreign_key(Xml, Table_Name, _, Ref, _, _),
        concat([Table_Name, '-', Ref], Bubble),
        Edge =
        edge:[from:Table_Name, to:Ref]:[
               prmtr:[arrows:both, color:black,
                  first_arrow:first_arrow,
                  second_arrow:second_arrow]:[
                     string:[bubble:Bubble]:[] ] ] ),
      Rs ),
   draw_graph(Ts, Rs, Picture).


/* draw_schema_graph_color_incoming(Xml, Picture) <-
      Draws the schema graph and colors the vertices
      according to the number of incoming edges,
      which corresponds to the elements of the foreign keys
      of that table. */

draw_schema_graph_color_incoming(Xml, Picture) :-
   findall( Node,
      ( Table_Name := Xml^create_table@name,
        count_foreign_key_references(Xml, Table_Name, Count),
        number_to_colour(Count, Colour),
        concat([Table_Name, '-', Count], Bubble),
        Node = node:[id:Table_Name]:[
           prmtr:[color:Colour, size:medium, symbol:text_in_box]:[
                 string:[bubble:'']:[Bubble] ] ] ),
      Ts ),
   findall( Edge,
      ( is_foreign_key(Xml, Table_Name, _, Ref, _, _),
        concat([Table_Name, '-', Ref], Bubble),
        Edge =
        edge:[from:Table_Name, to:Ref]:[
               prmtr:[arrows:both, color:black,
                  first_arrow:first_arrow,
                  second_arrow:second_arrow]:[
                     string:[bubble:Bubble]:[] ] ] ),
      Rs ),
   draw_graph(Ts, Rs, Picture).


/* draw_join_graph(Xml, Subquery_Id, Picture) <-
      Draws a graph consiting of the tables used in joins in the
      subquery identified by Subquery_Id. */

draw_join_graph(Xml, Subquery_Id, Picture) :-
   findall( Edge,
      ( subquery_xml(Xml, Subquery_Id, Subquery),
        is_join(Xml, Subquery, [Table_1, A_1], [Table_2, A_2]),
        Label = '', % Col_1-Col_2
        term_to_atom(Table_1-A_1, F),
        term_to_atom(Table_2-A_2, T),
        Edge = edge:[from:F, to:T]:[
               prmtr:[arrows:both, color:black,
                  first_arrow:first_arrow,
                  second_arrow:second_arrow]:[
                     string:[bubble:'']:[Label] ] ] ),
      Rs ),
   select_uses_tables(Xml, Subquery_Id, Tables_Aliases),
   findall( Node,
      ( member([Table, A], Tables_Aliases),
        term_to_atom(Table-A, T),
        Node = node:[id:T]:[
           prmtr:[ color:white, size:medium, symbol:text_in_box]:[
                      string:[bubble:'']:[T] ] ] ),
      Ts ),
%  writeln(user, Ts-Rs),
   draw_graph(Ts, Rs, Picture).


/* color_join_graph(Xml, Subquery_Id, Gxl_1, Gxl_2) <-
      Highlights the vertices in Gxl_1 which correspond to the
      tables used in the join identified by Subquery_Id. */

color_join_graph(Xml, Subquery_Id, Gxl_1, Gxl_2) :-
   findall( Table_2-Table_1,
      ( subquery_xml(Xml, Subquery_Id, Subquery),
        is_join(Xml, Subquery, [Table_1, _], [Table_2, _]) ),
      Rs ),
   select_uses_tables(Xml, Subquery_Id, Tables_Aliases),
   findall( Table,
      member([Table, _], Tables_Aliases),
      Ts ),
   apply_color_to_nodes(lightskyblue1, Gxl_1, Gxl_3, Ts),
   apply_color_to_edges(red, Gxl_3, Gxl_2, Rs).


/* reset_node_colors(Gxl_1, Gxl_2) <-
      */

reset_node_colors(Gxl_1, Gxl_2) :-
   Gxl_2 := Gxl_1 * [ ^graph^node^prmtr@color:white ].


reset_edge_colors(Gxl_1, Gxl_2) :-
   Gxl_2 := Gxl_1 * [ ^graph^edge^prmtr@color:black ].


apply_color_to_nodes(Color, Gxl_1, Gxl_2, L) :-
   reset_node_colors(Gxl_1, Gxl_3),
   !,
   apply_color_to_nodes_2(Color, Gxl_3, Gxl_2, L).


/*** implementation ***********************************************/


/* draw_graph(Es, Vs, Picture) <-
      Draws the graph with the edges Es and Vertices Vs
      using GXL. */

draw_graph(Ts, Rs, Picture) :-
%  default_file_to_fn_triple(gxl_config, Config),
   alias_to_default_fn_triple(gxl_config, Config),
   vertices_and_edges_to_gxl(Ts, Rs, Gxl_1),
   gxl_to_picture(Config, Gxl_1, Picture).


/* apply_color_to_nodes(Color, Gxl_1, Gxl_2, Ts) <-
      */

apply_color_to_nodes_2(_, Gxl, Gxl, []).
apply_color_to_nodes_2(Color, Gxl_1, Gxl_2, [T]) :-
   color_node(Gxl_1, T, Color, Gxl_2),
   !.
apply_color_to_nodes_2(Color, Gxl_1, Gxl_3, [T|Ts]) :-
   color_node(Gxl_1, T, Color, Gxl_2),
   !,
   apply_color_to_nodes_2(Color, Gxl_2, Gxl_3, Ts).


color_node(Gxl_1, ID, Color, Gxl_2) :-
   Gxl_2 := Gxl_1 * [ ^graph^node::[@id=ID]^prmtr@color:Color ],
   !.
color_node(Gxl, _, _, Gxl).


apply_color_to_edges(Color, Gxl_1, Gxl_3, L) :-
   reset_edge_colors(Gxl_1, Gxl_2),
   apply_color_to_edges_2(Color, Gxl_2, Gxl_3, L).

/* apply_color_to_edges(_, Gxl, Gxl, []) <-
      */

apply_color_to_edges_2(_, Gxl, Gxl, []).
apply_color_to_edges_2(Color, Gxl_1, Gxl_2, [F-T]) :-
   color_edge(Gxl_1, F, T, Color, Gxl_2),
   !.
apply_color_to_edges_2(Color, Gxl_1, Gxl_3, [F-T|Rs]) :-
   color_edge(Gxl_1, F, T, Color, Gxl_2),
   !,
   apply_color_to_edges_2(Color, Gxl_2, Gxl_3, Rs).


/* color_edge(Gxl_1, From, To, Color, Gxl_2) <-
      */

color_edge(Gxl_1, From, To, Color, Gxl_2) :-
   Gxl_2 := Gxl_1 * [ ^graph^edge::[
      @from=From, @to=To]^prmtr@color:Color ].


/******************************************************************/


