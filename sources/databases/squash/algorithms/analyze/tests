

/******************************************************************/
/***                                                            ***/
/***      squash algorithms_analyze: Tests                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(squash:squash_algorithms_analyze, constant_output_column) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   subquery_xml(Xml, 'subquery_1', Subquery),
   constant_output_column(
      Xml, Subquery, ['TBLSEARCHRESULTSSEQUEST', 'A'], Column),
   Column = 'FLAGDELETE'.

test(squash:squash_algorithms_analyze, identical_output_column) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'company.xml', Xml),
   subquery_xml(Xml, 'subquery_13', Subquery),
   identical_output_column(
      Xml, Subquery, ['EMPLOYEE', 'E'], Column),
   Column = 'FNAME'.

test(squash:squash_algorithms_analyze, unnecessary_table) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'company.xml', Xml),
   subquery_xml(Xml, 'subquery_14', Subquery),
   unnecessary_table(Xml, Subquery, T),
   T = 'DEPARTMENT'.

test(squash:squash_algorithms_analyze, partially_referenced_objects) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   partially_referenced_objects(Xml, List),
   member(['subquery_7', 'object_200'], List).

test(squash:squash_algorithms_analyze, possibly_missing_foreign_key) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   subquery(Xml, Subquery),
   possibly_missing_foreign_key(
      Xml, Subquery, Table_1, Col_1, Table_2, Col_2),
   Table_1 = 'TBLSEARCHRESULTSSEQUEST',
   Col_1 = 'IDRESSEARCHPEPTIDES',
   Table_2 = 'TBLRESPEPTIDES2SEQUENCES',
   Col_2 = 'IDPEPTIDE'.

% schema
test(squash:squash_algorithms_analyze, find_isolated_schema_parts) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   find_isolated_schema_parts(Xml, Tables),
   member(['TBLRESSPECLOCK'], Tables),
   member( [ 'TBLRESSEQUESTPRECALCACCIDLST', 
      'TBLRESSEQUESTPRECALCKEYS', 
      'TBLRESSEQUESTPRECALCCOUNTED', 
      'TBLRESSEQUESTPRECALCSPECLIST', 
      'TBLRESSEQUESTPRECALCPPOSCOUNT' ], Tables ).

test(squash:squash_algorithms_analyze, database_schema_is_cyclic) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'company.xml', Xml),
   database_schema_is_cyclic(Xml, Cycles),
   \+ Cycles = [].

test(squash:squash_algorithms_analyze, unused_tables) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'company.xml', Xml),
   unused_tables(Xml, Unused_Tables),
   Unused_Tables = ['DEPT_LOCATIONS', 'WORKS_ON'].

test(squash:squash_algorithms_analyze, unused_columns) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'company.xml', Xml),
   unused_columns(Xml, Unused_Cols),
   member(['DEPARTMENT', 'MGRSTARTDATE'], Unused_Cols).

test(squash:squash_algorithms_analyze, is_possibly_unique_index) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   is_possibly_unique_index(Xml, 'IDXRESDICTPEPPEP01').

test(squash:squash_algorithms_analyze, is_correct_index_hint) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   subquery(Xml, Subquery),
   index_hint(Xml, Subquery, Hint),
   is_correct_index_hint(Xml, Subquery, Hint).

test(squash:squash_algorithms_analyze, possibly_wrong_datatype_columns) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'company.xml', Xml),
   possibly_wrong_datatype_columns(Xml, List),
   List = [[ 'ESSN', 'WORKS_ON', 'DEPENDENT', 
      'CHAR(9)', 'VARCHAR(9)' ]].

test(squash:squash_algorithms_analyze, unnamed_pk_constraints) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'company.xml', Xml),
   unnamed_pk_constraints(Xml, List),
   member(['DEPARTMENT', 'DNUMBER', out_of_line], List).

test(squash:squash_algorithms_analyze, unnamed_fk_constraints) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'company.xml', Xml),
   unnamed_fk_constraints(Xml, List),
   member( ['DEPARTMENT', ['MGRSSN'], 
      'EMPLOYEE', ['SSN'], out_of_line], List ).

test(squash:squash_algorithms_analyze, count_tables) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   count_tables(Xml, Count),
   Count = 46.

test(squash:squash_algorithms_analyze, count_indexes) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   count_indexes(Xml, Count),
   Count = 66.

test(squash:squash_algorithms_analyze, count_defined_subqueries) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   count_defined_subqueries(Xml, Count),
   Count = 12.

test(squash:squash_algorithms_analyze, count_subqueries) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   count_subqueries(Xml, Count),
   Count = 35.

test(squash:squash_algorithms_analyze, count_defined_selects_1) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   count_defined_selects(Xml, Count),
   Count = 9.

test(squash:squash_algorithms_analyze, count_defined_selects_2) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   count_defined_selects(Xml, 'TBLSEQINFO', Count),
   Count = 4.

test(squash:squash_algorithms_analyze, count_foreign_keys_1) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   count_foreign_keys(Xml, 'TBLRESEXPERIMENT', Count),
   Count = 2.

test(squash:squash_algorithms_analyze, count_foreign_keys_2) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   count_foreign_keys(Xml, Count),
   Count = 72.

test(squash:squash_algorithms_analyze, count_foreign_key_references) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   count_foreign_key_references(
      Xml, 'TBLRESSEQUESTPRECALCKEYS', Count),
   Count = 4.

test(squash:squash_algorithms_analyze, count_columns_1) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   count_columns(Xml, Count),
   Count = 531.

test(squash:squash_algorithms_analyze, count_columns_2) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   count_columns(Xml, 'TBLENZYMES', Count),
   Count = 9.

test(squash:squash_algorithms_analyze, avg_columns_per_table) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   avg_columns_per_table(Xml, Avg),
   Avg = 11.54.

test(squash:squash_algorithms_analyze, avg_columns_per_select) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   avg_columns_per_select(Xml, Avg),
   Avg = 13.83.

test(squash:squash_algorithms_analyze, count_columns_for_select) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   subquery_xml(Xml, 'subquery_1', Subquery),
   count_columns_for_select(Xml, Subquery, Count),
   Count = 51.


/******************************************************************/


