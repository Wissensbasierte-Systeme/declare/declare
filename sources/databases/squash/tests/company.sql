
-- -- MySQL dump 8.14
-- --
-- -- Host: localhost    Database: company
-- ----------------------------------------------------------
-- -- Server version	3.23.41-Max-log
--
-- --
-- -- Table structure for table 'department'
-- --

DROP TABLE IF EXISTS department;
CREATE TABLE department (
  DNAME varchar(15) default '' NOT NULL,
  DNUMBER int(11) default '0' NOT NULL,
  MGRSSN varchar(9) default '' NOT NULL,
  MGRSTARTDATE date default NULL,
  PRIMARY KEY  (DNUMBER),
  UNIQUE (DNAME),
  FOREIGN KEY (MGRSSN) REFERENCES "employee"
);

-- --
-- -- Dumping data for table 'department'
-- --

LOCK TABLES department WRITE;
INSERT INTO department VALUES
   ('Research',5,'333445555','1978-05-22'),
   ('Administration',4,'987654321','1985-01-01'),
   ('Headquarters',1,'888665555','1971-06-19');
UNLOCK TABLES;

--
-- Table structure for table 'dependent'
--

DROP TABLE IF EXISTS dependent;
CREATE TABLE dependent (
  ESSN varchar(9) default '' NOT NULL,
  DEPENDENT_NAME varchar(15) default '' NOT NULL,
  SEX char(1) default NULL,
  BDATE date default NULL,
  RELATIONSHIP varchar(8) default NULL,
  PRIMARY KEY  (ESSN,DEPENDENT_NAME),
  FOREIGN KEY (ESSN) REFERENCES "employee"
);

--
-- Dumping data for table 'dependent'
--

LOCK TABLES dependent WRITE;
INSERT INTO dependent VALUES
   ('333445555','Alice','F','1976-04-05','DAUGHTER'),
   ('333445555','Theodore','M','1973-10-25','SON'),
   ('333445555','Joy','F','1948-05-03','SPOUSE'),
   ('987654321','Abner','M','1932-02-29','SPOUSE'),
   ('123456789','Michael','M','1978-01-01','SON'),
   ('123456789','Alice','F','1978-12-31','DAUGHTER'),
   ('123456789','Elizabeth','F','1957-05-05','SPOUSE');
UNLOCK TABLES;

--
-- Table structure for table 'dept_locations'
--

DROP TABLE IF EXISTS dept_locations;
CREATE TABLE dept_locations (
  DNUMBER int(11) default '0' NOT NULL,
  DLOCATION varchar(15) default '' NOT NULL,
  PRIMARY KEY  (DNUMBER,DLOCATION),
  FOREIGN KEY (DNUMBER) REFERENCES "department"
);

--
-- Dumping data for table 'dept_locations'
--

LOCK TABLES dept_locations WRITE;
INSERT INTO dept_locations VALUES
   (1,'Houston'),
   (4,'Stafford'),
   (5,'Bellaire'),
   (5,'Houston'),
   (5,'Sugarland');
UNLOCK TABLES;

--
-- Table structure for table 'employee'
--

DROP TABLE IF EXISTS employee;
CREATE TABLE employee (
  FNAME varchar(15) default '' NOT NULL,
  MINIT char(1) default NULL,
  LNAME varchar(15) default '' NOT NULL,
  SSN varchar(9) default '' NOT NULL,
  BDATE date default NULL,
  ADDRESS varchar(30) default NULL,
  SEX char(1) default NULL,
  SALARY decimal(10,2) default NULL,
  SUPERSSN varchar(9) default NULL,
  DNO int(11) default '0' NOT NULL,
  PRIMARY KEY  (SSN),
  FOREIGN KEY (DNO) REFERENCES "department",
  FOREIGN KEY (SUPERSSN) REFERENCES "employee"
);

--
-- Dumping data for table 'employee'
--

LOCK TABLES employee WRITE;
INSERT INTO employee VALUES
   ('John','B','Smith','123456789','1955-01-09','731 Fondren, Houston, TX','M',30000.00,'333445555',5),
   ('Franklin','T','Wong','333445555','1945-12-08','638 Voss, Houston, TX','M',40000.00,'888665555',5),
   ('Alicia','J','Zelaya','999887777','1958-07-19','3321 Castle, Spring, TX','F',25000.00,'987654321',4),
   ('Jennifer','S','Wallace','987654321','1931-06-20','291 Berry, Bellaire, TX','F',43000.00,'888665555',4),
   ('Ramesh','K','Narayan','666884444','1952-09-15','975 Fire Oak, Humble, TX','M',38000.00,'333445555',5),
   ('Joyce','A','English','453453453','1962-07-31','5631 Rice, Houston, TX','F',25000.00,'333445555',5),
   ('Ahmad','V','Jabbar','987987987','1959-03-29','980 Dallas, Houston, TX','M',25000.00,'987654321',4),
   ('James','E','Borg','888665555','1927-11-10','450 Stone, Houston, TX','M',55000.00,NULL,1);
UNLOCK TABLES;

--
-- Table structure for table 'project'
--

DROP TABLE IF EXISTS project;
CREATE TABLE project (
  PNAME varchar(15) default '' NOT NULL,
  PNUMBER int(11) default '0' NOT NULL,
  PLOCATION varchar(15) default NULL,
  DNUM int(11) default '0' NOT NULL,
  PRIMARY KEY  (PNUMBER),
  UNIQUE (PNAME),
  FOREIGN KEY (DNUM) REFERENCES "department"
);

--
-- Dumping data for table 'project'
--

LOCK TABLES project WRITE;
INSERT INTO project VALUES
   ('ProductX',1,'Bellaire',5),
   ('ProductY',2,'Sugarland',5),
   ('ProductZ',3,'Houston',5),
   ('Computerization',10,'Stafford',4),
   ('Reorganization',20,'Houston',1),
   ('Newbenefits',30,'Stafford',4);
UNLOCK TABLES;

--
-- Table structure for table 'works_on'
--

DROP TABLE IF EXISTS works_on;
CREATE TABLE works_on (
  ESSN char(9) default '' NOT NULL,
  PNO int(11) default '0' NOT NULL,
  HOURS decimal(3,1) default '0.0' NOT NULL,
  PRIMARY KEY  (ESSN,PNO),
  FOREIGN KEY (ESSN) REFERENCES "employee",
  FOREIGN KEY (PNO) REFERENCES "project"
);

--
-- Dumping data for table 'works_on'
--

LOCK TABLES works_on WRITE;
INSERT INTO works_on VALUES
   ('123456789',1,32.5),
   ('123456789',2,7.5),
   ('666884444',3,40.0),
   ('453453453',1,20.0),
   ('453453453',2,20.0),
   ('333445555',2,10.0),
   ('333445555',3,10.0),
   ('333445555',10,10.0),
   ('333445555',20,10.0),
   ('999887777',30,30.0),
   ('999887777',10,10.0),
   ('987987987',10,35.5),
   ('987987987',30,5.0),
   ('987654321',30,20.0),
   ('987654321',20,15.0),
   ('888665555',20,0.0);
UNLOCK TABLES;


-- Query 0:
select BDATE, ADDRESS
from employee
where FNAME = 'John'
and MINIT = 'B'
and LNAME = 'Smith';

-- Query 1:
select FNAME, LNAME, ADDRESS
from employee, department
where DNAME = 'Research'
and DNUMBER = DNO;

-- Query 1A:
select FNAME, employee.LNAME, ADDRESS
from employee, department
where department.DNAME = 'Research'
and department.DNUMBER = employee.DNO;

-- Query 1B:
select E.FNAME, E.LNAME, E.ADDRESS
from employee E, department D
where D.DNAME = 'Research'
and D.DNUMBER = E.DNO;

-- Query 1C:
select *
from employee
where DNO = 5;

-- Query 1D:
select *
from employee, department
where DNAME = 'Research'
and DNO = DNUMBER;

-- Query 2:
select PNUMBER, DNUM, LNAME, ADDRESS, BDATE
from project, department, employee
where DNUM = DNUMBER
and MGRSSN = SSN
and PLOCATION = 'Stafford';

-- Query 8:
select E.FNAME, E.LNAME, S.FNAME, S.LNAME
from employee E, employee S
where E.SUPERSSN = S.SSN;

-- Query 9:
select SSN
from employee;

-- Query 10:
select SSN, DNAME
from employee, department;

-- Query 10A:
select *
from employee, department;

-- Query 12A:
select E.FNAME, E.LNAME
from employee E, dependent D
where D.ESSN = E.SSN
and E.FNAME = D.DEPENDENT_NAME
and D.SEX = E.SEX;

-- Query 12B:
select E.FNAME, E.LNAME, E.SSN, D.DEPENDENT_NAME
from employee E, dependent D
where D.ESSN = E.SSN;

-- 13
select E.FNAME, FNAME
from employee E;

-- 14
select E.FNAME
from employee E, department;

-- 15
select E.FNAME, E.LNAME, E.SSN, D.DEPENDENT_NAME
from employee E JOIN dependent D ON D.ESSN = E.SSN;
