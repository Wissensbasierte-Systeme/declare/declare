

/******************************************************************/
/***                                                            ***/
/***         SQUASH:  SQL QUery And ScHema analyzer             ***/
/***                                                            ***/
/******************************************************************/


:- [ 'module_framework_basic',
     'module_framework_database',
     'module_framework_parser',
     'module_framework_fnquery_extensions',
     'module_algorithms_basic',
     'module_algorithms_optimize',
     'module_algorithms_analyze',
     'module_algorithms_visualize',
     'module_algorithms_generate' ].


/*** special files ************************************************/


:- [ 'squash_settings',
     'squash_and_sql',
     'squash_parser',
     'oracle_interface' ].


/*** tests ********************************************************/


:- [ 'algorithms/analyze/tests',
     'algorithms/basic/tests',
     'algorithms/generate/tests',
     'algorithms/optimize/tests',
     'algorithms/visualize/tests',
     'framework/basic/tests',
     'framework/database/tests',
     'framework/fnquery_extension/tests',
     'framework/parser/tests' ].


/******************************************************************/


