

/******************************************************************/
/***                                                            ***/
/***               Squash Auto Analysis Functions               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* squash_analyze(Headline, Type, R) <-
      Executes an analysis for the auto-analysis framework. */

squash_analyze(
      'Queries - Possibly Missing Foreign Keys', Type, R) :-
   global_variable_get(squash, xml, Xml),
   possibly_missing_foreign_keys(Xml, List),
   squash_report(
      Type, [['Table_1', 'Col_1', 'Table_2', 'Col_2']|List], R).
squash_analyze('Queries - Partially Referenced Columns', Type, R) :-
   global_variable_get(squash, xml, Xml),
   partially_referenced_objects(Xml, List),
   squash_report(Type, [['Subquery', 'ObjectID']|List], R).
squash_analyze('Queries - Unnecessary Tables', Type, R) :-
   global_variable_get(squash, xml, Xml),
   unnecessary_tables(Xml, List),
   squash_report(Type, [['Subquery', 'Tablename']|List], R).
squash_analyze('Queries - Identical Output Columns', Type, R) :-
   global_variable_get(squash, xml, Xml),
   identical_output_columns(Xml, List),
   squash_report(Type, [['Id','TableAliasList','Column']|List], R).
squash_analyze('Queries - Constant Output Columns', Type, R) :-
   global_variable_get(squash, xml, Xml),
   constant_output_columns(Xml, List),
   squash_report(Type, [['Id','TableAliasList','Column']|List], R).
squash_analyze('Schema - Best Join Order', Type, R) :-
   global_variable_get(squash, xml, Xml),
   best_join_order(Xml, min_sel, List),
   squash_report(Type, [['Tables']|List], R).
squash_analyze('Schema - Tables Without Primary Key', Type, R) :-
   global_variable_get(squash, xml, Xml),
   tables_without_primary_key(Xml, List),
   squash_report(Type, [['Tables']|List], R).
squash_analyze('Schema - Unnamed FK Constraints', Type, R) :-
   global_variable_get(squash, xml, Xml),
   unnamed_fk_constraints(Xml, List),
   squash_report(Type,
      [['Table', 'Key', 'Reference', 'RCols', 'Type']|List], R).
squash_analyze('Schema - Unnamed PK Constraints', Type, R) :-
   global_variable_get(squash, xml, Xml),
   unnamed_pk_constraints(Xml, List),
   squash_report(Type, [['Table', 'Column', 'Type']|List], R).
squash_analyze(
      'Schema - Possibly Wrong Datatype Columns', Type, R) :-
   global_variable_get(squash, xml, Xml),
   possibly_wrong_datatype_columns(Xml, List),
   squash_report(
      Type, [['Col', 'Table1', 'Table2', 'Type1', 'Type2']|List],
      R ).
squash_analyze('Schema - Incorrect Index-Hints', Type, R) :-
   global_variable_get(squash, xml, Xml),
   incorrect_index_hints(Xml, List),
   squash_report(Type, [['Index-Hints'], List], R).
squash_analyze('Schema - Possibly Unique Indexes', Type, R) :-
   global_variable_get(squash, xml, Xml),
   possibly_unique_indexes(Xml, List),
   squash_report(Type, [['Indexes']|List], R).
squash_analyze('Schema - Unused Tables', Type, R) :-
   global_variable_get(squash, xml, Xml),
   unused_tables(Xml, Tables),
   squash_report(Type, [['Tables']|Tables], R).
squash_analyze('Schema - Unused Columns', Type, R) :-
   global_variable_get(squash, xml, Xml),
   unused_columns(Xml, Unused_Cols),
   squash_report(Type, [['Table', 'Column']|Unused_Cols], R).
squash_analyze('Schema - Cycles', Type, R) :-
   global_variable_get(squash, xml, Xml),
   database_schema_is_cyclic(Xml, Cycles),
   \+ Cycles = [],
   squash_report(Type, 'The schema is cyclic.', R).
squash_analyze('Schema - Isolated Schema Parts', Type, R) :-
   global_variable_get(squash, xml, Xml),
   find_isolated_schema_parts(Xml, Tables_1),
   sublists_as_stringlists(Tables_1, Tables_2),
   squash_report(Type, [['Subgraph']|Tables_2], R).
squash_analyze('Statistics - Queries', Type, R) :-
   global_variable_get(squash, xml, Xml),
   count_subqueries(Xml, Count_1),
   count_defined_subqueries(Xml, Count_2),
   count_defined_selects(Xml, Count_3),
   squash_report(
      Type, ['Item', 'Subqueries', 'Defined Selects',
      'Defined Subqueries'], ['Value', Count_1, Count_2, Count_3],
      R).
squash_analyze('Statistics - Foreign Keys', Type, R) :-
   global_variable_get(squash, xml, Xml),
   findall( [Table_Name,Out, In],
      ( is_table(Xml, Table_Name),
        count_foreign_keys(Xml, Table_Name, Out),
        count_foreign_key_references(Xml, Table_Name, In) ),
      List ),
   squash_report(Type, [['Tablename', 'FK References From Table',
      'FK References To Table']|List], R).
squash_analyze('Statistics summary', Type, R) :-
   global_variable_get(squash, xml, Xml),
   count_tables(Xml, Table_Count),
   count_indexes(Xml, Index_Count),
   count_subqueries(Xml, Select_Count),
   count_columns(Xml, Col_Count),
   avg_columns_per_table(Xml, Avg_Table_Cols),
   avg_columns_per_select(Xml, Avg_Query_Cols),
   count_foreign_keys(Xml, Fk_Count),
   squash_report( Type, ['Item', 'Tables', 'Indexes',
      'Selects', 'Total Columns', 'Avg. Table Columns',
      'Avg. Query Columns', 'Foreign Keys'], [ 
         'Value', Table_Count, Index_Count, Select_Count, Col_Count,
         Avg_Table_Cols, Avg_Query_Cols, Fk_Count ], R ).


/******************************************************************/


