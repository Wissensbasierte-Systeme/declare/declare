

/******************************************************************/
/***                                                            ***/
/***      squash framework_basic: Tests                         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


% squash framework

test(squash:squash_framework_basic, squash_analyze_and_report) :-
   dislog_variable_get(home, DisLog),
   global_variable_get(squash, test_dir, Dir),
   concat([DisLog, Dir, 'company.xml'], F),
   squash_analyze_and_report(F).

test(squash:squash_framework_basic, squash_analyze_xpce) :-
   dislog_variable_get(home, DisLog),
   global_variable_get(squash, test_dir, Dir),
   concat([DisLog, Dir, 'company.xml'], F),
   squash_analyze_xpce(F, 'Statistics - Foreign Keys').

test(squash:squash_framework_basic, squash_list_functions) :-
   squash_list_functions.


% generic quicksort

test(squash:squash_framework_basic, generic_quicksort) :-
   X = [a-1,z-10,b-0,x-5,c-2,y-7,f-1],
   generic_quicksort(greater_than, X, G),
   G = [b-0, a-1, f-1, c-2, x-5, y-7, z-10],
   generic_quicksort(less_than, X, L),
   L = [z-10, y-7, x-5, c-2, a-1, f-1, b-0],
   Y = [[a,1],[z,10],[b,0],[x,5],[c,2],[y,7],[f,1]],
   generic_quicksort(less_than, Y, Z),
   Z = [[z,10], [y,7], [x,5], [c,2], [a,1], [f,1], [b,0]].


% qualities

test(squash:squash_framework_basic, strip_qualities) :-
   Y = [[a,1],[z,10],[b,0],[x,5],[c,2],[y,7],[f,1]],
   strip_qualities(Y, Z),
   Z = [a, z, b, x, c, y, f] .

test(squash:squash_framework_basic, quality_filter_list) :-
   Y = [[a,1],[z,10],[b,0],[x,5],[c,2],[y,7],[f,1]],
   quality_filter_list(5, Y, Z),
   Z = [[z, 10], [x, 5], [y, 7]].

test(squash:squash_framework_basic, divide_quality_list) :-
   Y = [[a,1],[z,10],[b,0],[x,5],[c,2],[y,7],[f,1]],
   divide_quality_list(Y, 10, Z),
   Z = [ [a,0.1], [z,1], [b,0], [x,0.5],
      [c,0.2], [y,0.7], [f,0.1] ].


% schema

test(squash:squash_framework_basic, database_schema_load) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'company.xml', Xml),
   _ := Xml@version.

test(squash:squash_framework_basic, is_next_id) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   is_next_id(Xml, 'subquery', Id),
   Id = 'subquery_12'.

test(squash:squash_framework_basic, check_sql_dialect) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   check_sql_dialect(Xml).


% schema identifiers

test(squash:squash_framework_basic, unique_object_name) :-
   global_variable_get(squash, test_dir, Dir),
   database_schema_load(Dir, 'squashtest.xml', Xml),
   unique_object_name(Xml, 'TBLGERAET', 'TBLGERAET_0').


% squash gui

test(squash:squash_framework_basic, squash_gui) :-
   dislog_variable_get(home, DisLog),
   global_variable_get(squash, test_dir, Dir),
   concat([DisLog, Dir, 'company.xml'], F),
   squash_gui(F).


% utility functions

test(squash:squash_framework_basic, encapsulate_list) :-
   encapsulate_list([a,b,c], [[a],[b],[c]]).

test(squash:squash_framework_basic, order_elements_like_list) :-
   order_elements_like_list(
      [d,a,b,c,e], [b,a,d, c, z], [d, a, b, c]).

test(squash:squash_framework_basic, generate_subset) :-
   findall( S,
      generate_subset(S, [a,b,c]),
      Ss ),
   Ss = [[a,b,c], [a,b], [a,c], [a], [b,c], [b], [c], []].

test(squash:squash_framework_basic, sublists_as_stringlists) :-
   sublists_as_stringlists([[a], [a,b,c]], Rs),
   Rs = [[a], ['a, b, c']].


/******************************************************************/


