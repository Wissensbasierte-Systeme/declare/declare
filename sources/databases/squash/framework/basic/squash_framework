

/******************************************************************/
/***                                                            ***/
/***             Database Schema Analysis Framework             ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      squash_analyze/3.

:- multifile
      squash_analyze/3.


/*** interface ****************************************************/


/* squash_analyze_and_report(Filename) <-
      Reads filename and starts the auto-analysis. */

squash_analyze_and_report(Filename) :-
   dread(xml, Filename, [Xml]),
   check_sql_dialect(Xml),
   global_variable_set(squash, filename, Filename),
   global_variable_set(squash, xml, Xml),
   squash_analyze_and_report.


/* squash_analyze_and_report <-
      */

squash_analyze_and_report :-
   global_variable_set(squash, report, []),
   fail.
squash_analyze_and_report :-
   squash_analyze(F, html, D),
   nonvar(D),
   global_variable_add_element(squash, report, p:[]:[h2:[]:[F], D]),
   fail.
squash_analyze_and_report :-
   squash_report_headline(h1, 'Squash Report', H1),
   global_variable_get(squash, report, Report),
   global_variable_get(squash, filename, Filename),
   concat(['Report data of ', Filename, '.'], Descr),
   append([H1, p:[]:[Descr]], Report, Body),
   Html = html:[]:[body:[]:Body],
   global_variable_get(squash, output_path, Output_Dir),
   concat([Output_Dir, 'squash-report.html'], Output_File),
   dwrite(xml, Output_File, Html).


/* squash_analyze(X,T,R) <-
      Catchall squash_analyze predicate for the auto analysis. */
squash_analyze(X,_,_) :-
   \+ clause(squash_analyze(X, _, _) , _),
   fail.


/* squash_analyze_xpce(Filename, T) <-
      Executes the auto-analysis T with the specified file. */

squash_analyze_xpce(Filename, T) :-
   dread(xml, Filename, [Xml]),
   check_sql_dialect(Xml),
   global_variable_set(squash, filename, Filename),
   global_variable_set(squash, xml, Xml),
   squash_analyze(T, xpce, [H|Data]),
   xpce_display_table(_, _, T, H, Data).



/* squash_list_functions <-
      Lists all auto analysis functions. */

squash_list_functions :-
   nl,
   clause(squash_analyze(F, _, _) , _),
   nonvar(F),
   writeln(F),
   fail.
squash_list_functions.


/* squash_report(T, Ls, R) <-
      XPCE or HTML formatting of auto-analysis results. */

squash_report(xpce, Ls, Ls) :-
   is_list(Ls),
   !.
squash_report(html, Ls, table:[border:1]:R) :-
   is_list(Ls),
   !,
   squash_report_2(html, Ls, R).
squash_report(xpce, S, [['Result', S]]).
squash_report(html, S, p:[]:[S]).


/* squash_report(T, Ls, Vs, R) <-
      XPCE or HTML formatting of auto-analysis results. */

squash_report(xpce, [], [], []) :- !.
squash_report(xpce, [L|Ls], [V|Vs], [[L, V]|R]) :-
   squash_report(xpce, Ls, Vs, R).
squash_report(html, Ls, Vs, table:[border:1]:R) :-
   squash_report_2(html, Ls, Vs, R).


/* writeln_log(X) <-
      */

writeln_log(X) :-
   writeln_log(0, X),
   !.


/* writeln_log(N, Ss) <-
      Prints out log messages with a customizable 
      verbosity level. */

writeln_log(N, [S]) :-
   global_variable_get(squash, log_level, Lvl),
   Lvl > N,
   writeln(user, S),
   !.
writeln_log(N, [S|L]) :-
   global_variable_get(squash, log_level, Lvl),
   Lvl > N,
   write(user, S),
   write(user, ' '),
   !,
   writeln_log(N, L).
writeln_log(N, S) :-
   global_variable_get(squash, log_level, Lvl),
   Lvl > N,
   writeln(user, S),
   !.
writeln_log(_, _).


/*** implementation ***********************************************/


/* squash_report_headline(Hx, Text, R) <-
      */

squash_report_headline(Hx, Text, R) :-
   R = Hx:[]:[Text].


/* squash_report_paragraph(Text, R) <-
      */

squash_report_paragraph(Text, R) :-
   R = p:[]:[Text].


squash_report_2(html, [], []) :- !.
squash_report_2(html, [L|Ls], [tr:[]:R|Rs]) :-
   squash_report_4(html, L, R),
   squash_report_3(html, Ls, Rs).


squash_report_2(html, [], [], []) :- !.
squash_report_2(html, [L|Ls], [V|Vs], [E|R]) :-
   E = tr:[]:[td:[]:[b:[]:[L]], td:[]:[b:[]:[V]]],
   squash_report_3(html, Ls, Vs, R).


squash_report_3(html, [], []) :- !.
squash_report_3(html, [L|Ls], [tr:[]:R|Rs]) :-
   squash_report_5(html, L, R),
   squash_report_3(html, Ls, Rs).


squash_report_3(html, [], [], []) :- !.
squash_report_3(html, [L|Ls], [V|Vs], [E|R]) :-
   E = tr:[]:[td:[]:[L], td:[]:[V]],
   squash_report_3(html, Ls, Vs, R).


squash_report_4(html, [], []) :- !.
squash_report_4(html, [L|Ls], [td:[]:[b:[]:[L]]|Rs]) :-
   !,
   squash_report_4(html, Ls, Rs).
squash_report_4(html, E, [td:[]:[b:[]:[E]]]).


squash_report_5(html, [], []) :- !.
squash_report_5(html, [L|Ls], [td:[]:[L]|Rs]) :-
   !,
   squash_report_5(html, Ls, Rs).
squash_report_5(html, E, [td:[]:[E]]).


/******************************************************************/


