####################################################################
package SquashMLParser;
####################################################################
#
# Generic parser class for parsing SQL code to SquashML.
# This class must be implemented by mor specific parsers.
#
####################################################################

use strict;
use warnings;
use vars qw($VERSION);

$VERSION = '1.0';

sub new {
  my $this = shift;
  my $class = ref($this) || $this;
  my $self = {};
  bless ($self, $class);
  $self->{'tree'}->{'-attributes'}->{'sqldialect'} = 'sql';
  $self->{'tree'}->{'-attributes'}->{'version'} = $VERSION;
  $self->initialize();
  return $self;
}

sub initialize {
  my $self = shift;
  $self->{'subquery_id_counter'} = 0;
  $self->{'object_id_counter'} = 0;
  $self->{'statement_end'} = ';';

  @{$self->{'RESERVED_WORDS'}} = qw(
ABS ABSOLUTE ACCESS ACQUIRE ACTION ADA ADD ADMIN AFTER AGGREGATE ALIAS
ALL ALLOCATE ALLOW ALTER AND ANY ARE ARRAY AS ASC ASENSITIVE ASSERTION
ASUTIME ASYMMETRIC AT ATOMIC AUDIT AUTHORIZATION AUX AUXILIARY AVG
BACKUP BEFORE BEGIN BETWEEN BIGINT BINARY BIT BIT_LENGTH BLOB BOOLEAN
BOTH BREADTH BREAK BROWSE BUFFERPOOL BULK BY
CALL CALLED CAPTURE CARDINALITY CASCADE CASCADED CASE CAST CATALOG
CCSID CEIL CEILING CHAR CHAR_LENGTH CHARACTER CHARACTER_LENGTH CHECK
CHECKPOINT CLASS CLOB CLOSE CLUSTER CLUSTERED COALESCE COLLATE
COLLATION COLLECT COLLECTION COLLID COLUMN COMMENT COMMIT COMPLETION
COMPRESS COMPUTE CONCAT CONDITION CONNECT CONNECTION CONSTRAINT
CONSTRAINTS CONSTRUCTOR CONTAINS CONTAINSTABLE CONTINUE CONVERT CORR
CORRESPONDING COUNT COUNT_BIG COVAR_POP COVAR_SAMP CREATE CROSS CUBE
CUME_DIST CURRENT CURRENT_COLLATION CURRENT_DATE
CURRENT_DEFAULT_TRANSFORM_GROUP CURRENT_LC_PATH CURRENT_PATH
CURRENT_ROLE CURRENT_SERVER CURRENT_TIME CURRENT_TIMESTAMP
CURRENT_TIMEZONE CURRENT_TRANSFORM_GROUP_FOR_TYPE CURRENT_USER CURSOR
CYCLE
DATA DATABASE DATALINK DATE DAY DAYS DB2GENERAL DB2SQL DBA DBCC DBINFO DBSPACE
DEALLOCATE DEC DECIMAL DECLARE DEFAULT DEFERRABLE DEFERRED DELETE
DENSE_RANK DENY DEPTH DEREF DESC DESCRIBE DESCRIPTOR DESTROY
DESTRUCTOR DETERMINISTIC DIAGNOSTICS DICTIONARY DISALLOW DISCONNECT
DISK DISTINCT DISTRIBUTED DLNEWCOPY DLPREVIOUSCOPY DLURLCOMPLETE
DLURLCOMPLETEONLY DLURLCOMPLETEWRITE DLURLPATH DLURLPATHONLY
DLURLPATHWRITE DLURLSCHEME DLURLSERVER DLVALUE DO DOMAIN DOUBLE DROP
DSSIZE DUMMY DUMP DYNAMIC
EACH EDITPROC ELEMENT ELSE ELSEIF END END-EXEC EQUALS ERASE ERRLVL
ESCAPE EVERY EXCEPT EXCEPTION EXCLUSIVE EXEC EXECUTE EXISTS EXIT EXP
EXPLAIN EXTERNAL EXTRACT
FALSE FENCED FETCH FIELDPROC FILE FILLFACTOR FILTER FINAL FIRST FLOAT
FLOOR FOR FOREIGN FORTRAN FOUND FREE FREETEXT FREETEXTTABLE FROM FULL
FUNCTION FUSION
GENERAL GENERATED GET GLOBAL GO GOTO GRANT GRAPHIC GROUP GROUPING
HANDLER HAVING HOLD HOLDLOCK HOST HOUR HOURS
IDENTIFIED IDENTITY IDENTITY_INSERT IDENTITYCOL IF
IGNORE IMMEDIATE IMPORT IN INCLUDE INCREMENT INDEX INDICATOR INITIAL
INITIALIZE INITIALLY INNER INOUT INPUT INSENSITIVE INSERT INT INTEGER
INTEGRITY INTERSECT INTERSECTION INTERVAL INTO IS ISOBID ISOLATION
ITERATE
JAR JAVA JOIN
KEY KILL
LABEL LANGUAGE LARGE LAST LATERAL LC_CTYPE LEADING LEAVE LEFT LESS
LEVEL LIKE LIMIT LINENO LINKTYPE LN LOAD LOCAL LOCALE LOCALTIME
LOCALTIMESTAMP LOCATOR LOCATORS LOCK LOCKSIZE LONG LOOP LOWER
MAP MATCH MAX MAXEXTENTS MEMBER MERGE METHOD MICROSECOND MICROSECONDS
MIN MINUS MINUTE MINUTES MOD MODE MODIFIES MODIFY MODULE MONTH MONTHS
MULTISET
NAME NAMED NAMES NATIONAL NATURAL NCHAR NCLOB NEW NEXT NHEADER NO
NOAUDIT NOCHECK NOCOMPRESS NODENAME NODENUMBER NONCLUSTERED NONE
NORMALIZE NOT NOWAIT NULL NULLIF NULLS NUMBER NUMERIC NUMPARTS
OBID OBJECT OCTET_LENGTH OF OFF OFFLINE OFFSETS OLD ON ONLINE ONLY
OPEN OPENDATASOURCE OPENQUERY OPENROWSET OPENXML OPERATION
OPTIMIZATION OPTIMIZE OPTION OR ORDER ORDINARILITY OUT OUTER OUTPUT
OVER OVERLAPS OVERLAY
PACKAGE PAD PAGE PAGES PARAMETER PARAMETERS PART PARTIAL PARTITION
PASCAL PATH PCTFREE PCTINDEX PERCENT PERCENT_RANK PERCENTILE_CONT
PERCENTILE_DISC PIECESIZE PLAN POSITION POSTFIX POWER PRECISION PREFIX
PREORDER PREPARE PRESERVE PRIMARY PRINT PRIOR PRIQTY PRIVATE
PRIVILEGES PROC PROCEDURE PROGRAM PSID PUBLIC
QUERYNO
RAISERROR RANGE RANK RAW READ READS READTEXT REAL RECONFIGURE RECOVERY
RECURSIVE REF REFERENCES REFERENCING REGR_AVGX REGR_AVGY REGR_COUNT
REGR_INTERCEPT REGR_R2 REGR_SLOPE REGR_SXX REGR_SXY REGR_SYY RELATIVE
RELEASE RENAME REPEAT REPLICATION RESET RESIGNAL RESOURCE RESTORE
RESTRICT RESULT RETURN RETURNS REVOKE RIGHT ROLE ROLLBACK ROLLUP
ROUTINE ROW ROW_NUMBER ROWCOUNT ROWGUIDCOL ROWID ROWNUM ROWS RRN RULE
RUN
SAVE SAVEPOINT SCHEDULE SCHEMA SCOPE SCRATCHPAD SCROLL SEARCH SECOND
SECONDS SECQTY SECTION SECURITY SELECT SENSITIVE SEQUENCE SESSION
SESSION_USER SET SETS SETUSER SHARE SHUTDOWN SIGNAL SIMILAR SIMPLE
SIZE SMALLINT SOME SOURCE SPACE SPECIFIC SPECIFICTYPE SQL SQLCA
SQLCODE SQLERROR SQLEXCEPTION SQLSTATE SQLWARNING SQRT STANDARD START
STATE STATEMENT STATIC STATISTICS STAY STDDEV_POP STDDEV_SAMP STOGROUP
STORES STORPOOL STRUCTURE STYLESUBPAGES SUBSTRING SUCCESSFUL SUM
SYMMETRIC SYNONYM SYSDATE SYSTEM SYSTEM_USER
TABLE TABLESPACE TEMPORARY TERMINATE TEXTSIZE THAN THEN TIME TIMESTAMP
TIMEZONE_HOUR TIMEZONE_MINUTE TO TOP TRAILING TRAN TRANSACTION
TRANSLATE TRANSLATION TREAT TRIGGER TRIM TRUE TRUNCATE TSEQUAL TYPE
UID UNDER UNDO UNION UNIQUE UNKNOWN UNNEST UNTIL UPDATE UPDATETEXT
UPPER USAGE USE USER USING
VALIDATE VALIDPROC VALUE VALUES VAR_POP VAR_SAMP VARCHAR VARCHAR2
VARIABLE VARIANT VARYING VCAT VIEW VOLUMES
WAITFOR WHEN WHENEVER WHERE WHILE WIDTH_BUCKET WINDOW WITH WITHIN
WITHOUT WLM WORK WRITE WRITETEXT
YEAR YEARS
ZONE );

  # The following variables are used to name the hashkeys in the parsetree and
  # thus correspond also to the tagnames in the XML later.
  $self->{'key'}->{'relational_properties'} = 'relational_properties';
  $self->{'key'}->{'create_table'} = 'create_table';
  $self->{'key'}->{'create_index'} = 'create_index';
  $self->{'key'}->{'alias'} = 'alias';
  $self->{'key'}->{'index_expr'} = 'index_expr';
  $self->{'key'}->{'out_of_line_constraint'} = 'out_of_line_constraint';
  $self->{'key'}->{'out_of_line_ref_constraint'} = 'out_of_line_ref_constraint';
  $self->{'key'}->{'column_def'} = 'column_def';
  $self->{'key'}->{'out_of_line_unique_constraint'} = 'out_of_line_unique_constraint';
  $self->{'key'}->{'out_of_line_primary_key_constraint'} = 'out_of_line_primary_key_constraint';
  $self->{'key'}->{'out_of_line_foreign_key_constraint'} = 'out_of_line_foreign_key_constraint';
  $self->{'key'}->{'out_of_line_check_constraint'} = 'out_of_line_check_constraint';
  $self->{'key'}->{'condition'} = 'condition';
  $self->{'key'}->{'on_delete'} = 'on_delete';
  $self->{'key'}->{'constraint_reference'} = 'constraint_reference';
  $self->{'key'}->{'reference'} = 'reference';
  $self->{'key'}->{'default_expr'} = 'default_expr';
  $self->{'key'}->{'expr_list'} = 'expr_list';
  $self->{'key'}->{'expr'} = 'expr';
  $self->{'key'}->{'inline_constraint'} = 'inline_constraint';
  $self->{'key'}->{'function_expr'} = 'function_expr';
  $self->{'key'}->{'scalar_subquery_expr'} = 'scalar_subquery_expr';
  $self->{'key'}->{'simple_expr'} = 'simple_expr';
  $self->{'key'}->{'compound_expr'} = 'compound_expr';
  $self->{'key'}->{'operator'} = 'operator';
  $self->{'key'}->{'right_expr'} = 'right_expr';
  $self->{'key'}->{'left_expr_list'} = 'left_expr_list';
  $self->{'key'}->{'left_expr'} = 'left_expr';
  $self->{'key'}->{'cursor_expr'} = 'cursor_expr';
  $self->{'key'}->{'number'} = 'number';
  $self->{'key'}->{'text'} = 'text';
  $self->{'key'}->{'rownum'} = 'rownum';
  $self->{'key'}->{'sequence'} = 'sequence';
  $self->{'key'}->{'function_arg'} = 'function_arg';
  $self->{'key'}->{'inline_null_constraint'} = 'inline_null_constraint';
  $self->{'key'}->{'inline_unique_constraint'} = 'inline_unique_constraint';
  $self->{'key'}->{'inline_primary_key_constraint'} = 'inline_primary_key_constraint';
  $self->{'key'}->{'inline_foreign_key_constraint'} = 'inline_foreign_key_constraint';
  $self->{'key'}->{'inline_check_constraint'} = 'inline_check_constraint';
  $self->{'key'}->{'select_list'} = 'select_list';
  $self->{'key'}->{'select'} = 'select';
  $self->{'key'}->{'table_reference'} = 'table_reference';
  $self->{'key'}->{'from'} = 'from';
  $self->{'key'}->{'group_by'} = 'group_by';
  $self->{'key'}->{'having'} = 'having';
  $self->{'key'}->{'combined_subquery'} = 'combined_subquery';
  $self->{'key'}->{'only_table_reference'} = 'only_table_reference';
  $self->{'key'}->{'query_table_expression'} = 'query_table_expression';
  $self->{'key'}->{'simple_query_table_expression'} = 'simple_query_table_expression';
  $self->{'key'}->{'subquery_query_table_expression'} = 'subquery_query_table_expression';
  $self->{'key'}->{'query_table_reference'} = 'query_table_reference';
  $self->{'key'}->{'joined_table'} = 'joined_table';
  $self->{'key'}->{'jointype'} = 'jointype';
  $self->{'key'}->{'join_on'} = 'join_on';
  $self->{'key'}->{'join_using'} = 'join_using';
  $self->{'key'}->{'join'} = 'join';
  $self->{'key'}->{'cross_join'} = 'cross_join';
  $self->{'key'}->{'natural_join'} = 'natural_join';
  $self->{'key'}->{'compound_condition'} = 'compound_condition';
  $self->{'key'}->{'right_condition'} = 'right_condition';
  $self->{'key'}->{'group_comparison_condition'} = 'group_comparison_condition';
  $self->{'key'}->{'simple_comparison_condition'} = 'simple_comparison_condition';
  $self->{'key'}->{'group_membership_condition'} = 'group_membership_condition';
  $self->{'key'}->{'not'} = 'not';
  $self->{'key'}->{'exists_condition'} = 'exists_condition';
  $self->{'key'}->{'is_of_type_condition'} = 'is_of_type_condition';
  $self->{'key'}->{'null_condition'} = 'null_condition';
  $self->{'key'}->{'range_condition'} = 'range_condition';
  $self->{'key'}->{'simple_membership_condition'} = 'simple_membership_condition';
  $self->{'key'}->{'like_condition'} = 'like_condition';
  $self->{'key'}->{'escape'} = 'escape';
  $self->{'key'}->{'key_outer_join_condition'} = 'outer_join_condition';
  $self->{'key'}->{'ltable'} = 'ltable';
  $self->{'key'}->{'lcolumn'} = 'lcolumn';
  $self->{'key'}->{'rtable'} = 'rtable';
  $self->{'key'}->{'rcolumn'} = 'rcolumn';
  $self->{'key'}->{'left_condition'} = 'left_condition';
  $self->{'key'}->{'only'} = 'only';
  $self->{'key'}->{'type_element'} = 'type_element';
  $self->{'key'}->{'lower_bound_expr'} = 'lower_bound_expr';
  $self->{'key'}->{'upper_bound_expr'} = 'upper_bound_expr';
  $self->{'key'}->{'siblings'} = 'siblings';
  $self->{'key'}->{'order_by'} = 'order_by';
  $self->{'key'}->{'order_by_element'} = 'order_by_element';
  $self->{'key'}->{'nulls'} = 'nulls';
  $self->{'key'}->{'references_clause'} = 'references_clause';
  $self->{'key'}->{'name'} = 'name';
  $self->{'key'}->{'type'} = 'type';
  $self->{'key'}->{'column'} = 'column';
  $self->{'key'}->{'column_list'} = 'column_list';
  $self->{'key'}->{'null'} = 'null';
  $self->{'key'}->{'num'} = 'num';
  $self->{'key'}->{'value'} = 'value';
  $self->{'key'}->{'id'} = 'id';
  $self->{'key'}->{'schema'} = 'schema';
  $self->{'key'}->{'table_view'} = 'table_view';
  $self->{'key'}->{'subquery'} = 'subquery';
  $self->{'key'}->{'index'} = 'index';
  $self->{'key'}->{'table_index_clause'} = 'table_index_clause';
  $self->{'key'}->{'ordering'} = 'ordering';
  $self->{'key'}->{'cascade'} = 'cascade';
  $self->{'key'}->{'for_update_clause'} = 'for_update_clause';
  $self->{'key'}->{'object'} = 'object';
  $self->{'key'}->{'table'} = 'table';
  $self->{'key'}->{'dbms_create_table'} = 'dbms_create_table';
  $self->{'key'}->{'dbms_constraint_state'} = 'dbms_constraint_state';
  $self->{'key'}->{'dbms_inline_ref_constraint'} = 'inline_ref_constraint';
  $self->{'key'}->{'dbms_table_index_clause'} = 'dbms_table_index_clause';
  $self->{'key'}->{'dbms_optimizer_hint'} = 'optimizer_hint';
  $self->{'key'}->{'dbms_for_update_clause'} = 'dbms_for_update_clause';
}

sub reset {
  my $self = shift;
  delete $self->{'sql'};
  delete $self->{'tmp'}->{$self->{'key'}->{'table'}};
  delete $self->{'tmp'};
  delete $self->{'varlist'};
  delete $self->{'sqldata'};
  delete $self->{'occurence_count'};
  $self->{'subquery_id_counter'} = 0;
  $self->{'object_id_counter'} = 0;
}

sub prepare_reserved_words {
  my $self = shift;
  $self->{'RESERVED_WORDS_REGEXP'} = "(?:".join('|', @{$self->{'RESERVED_WORDS'}}).")";
}

sub parse {
  my $self = shift;
  my $sql = shift;
  $self->reset();
  $self->prepare_reserved_words();
  $self->{'sql'} = $sql;
  $self->{'linecount'} = 1;
  while ( $self->{'sql'} !~ m/^\s*$/is ) {
    $self->statement();
  }
}

sub statement {
  my $self = shift;
  $self->occurence_count();
  $self->whitespace();
  delete $self->{'tmp'};
  if ( $self->{'sql'} =~ m/^CREATE/is ) {
    $self->create();
  }
  elsif ( $self->{'sql'} =~ m/^SELECT/is ) {
    $self->select();
  }
  elsif ( $self->{'sql'} =~ m/^INSERT/is ) {
    $self->insert();
  }
  elsif ( $self->{'sql'} =~ m/^DROP/is ) {
    $self->drop();
  }
  elsif ( $self->{'sql'} =~ m/^\s*$/is ) { # parsed all sql
    return;
  }
  elsif ( ($self->{'statement_end'} ne '') && $self->{'sql'} =~ s/^$self->{'statement_end'}//iso ) { # empty command
    return;
  }
  elsif ( $self->dbms_statement() ) {
    return;
  }
  else {
    return $self->do_err("SQL command not implemented");
  }
}

sub dbms_statement {
  my $self = shift;
  return 0;
}


sub create {
  my $self = shift;
  $self->{'sql'} =~ s/^CREATE//is;
  $self->whitespace();
  if ( $self->{'sql'} =~ m/^TABLE/is ) {
    $self->create_table();
  }
  elsif ( $self->{'sql'} =~ m/^(UNIQUE|BITMAP|CLUSTERED|NONCLUSTERED|INDEX)/is ) {
    $self->create_index();
  }
  elsif ( $self->{'sql'} =~ m/^SEQUENCE/is ) {
    $self->create_sequence();
  }
  elsif ( $self->{'sql'} =~ s/^OR//is ) {
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^REPLACE//is ) {
      $self->whitespace();
      if ( $self->{'sql'} =~ m/^TRIGGER/is ) {
        $self->create_trigger();
      }
      else {
        return $self->do_err("Unknown create_or_replace statement.");
      }
    }
    else {
      return $self->do_err("Unknown create statement.");
    }
  }
  else {
    return $self->do_err("Unknown create statement.");
  }
}


######################################################################
#
# *** CREATE TABLE ***
#
######################################################################

sub create_table {
  my $self = shift;
  $self->{'sql'} =~ s/^TABLE//is;
  $self->whitespace();

  my $identifier;
  if ( $identifier = $self->identifier() ) {
    if ( $self->{'sql'} =~ s/^\.//is  ) {
      $self->whitespace();
      $self->{'tmp'}->{$self->{'key'}->{'table'}}->{'-attributes'}->{$self->{'key'}->{'schema'}} = $identifier;
      unless ( $identifier = $self->identifier() ) { return $self->do_err("tablename expected"); }
      $self->{'tmp'}->{$self->{'key'}->{'table'}}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    }
    else {
      $self->{'tmp'}->{$self->{'key'}->{'table'}}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    }
  }
  else {
    return $self->do_err("table- or schemaname expected");
  }
  if ( $self->{'sql'} =~ m/^\(/ ) {
    if ( $self->relational_properties() ) {
      $self->{'tmp'}->{$self->{'key'}->{'table'}}->{$self->{'key'}->{'relational_properties'}} = $self->{'tmp'}->{$self->{'key'}->{'relational_properties'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'relational_properties'}};
    }
    else {
      return $self->do_err("relational_properties expected");
    }
  }

  if ( $self->dbms_create_table() ) {
    $self->{'tmp'}->{$self->{'key'}->{'table'}}->{$self->{'key'}->{'dbms_create_table'}} = $self->{'tmp'}->{$self->{'key'}->{'dbms_create_table'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'dbms_create_table'}};
  }

  ${ $self->{'sqldata'}}[@{ $self->{'sqldata'}}]->{$self->{'key'}->{'create_table'}} = $self->{'tmp'}->{$self->{'key'}->{'table'}};
  return $self->do_err("End of statement expected in create") unless ( ($self->{'statement_end'} eq '') || $self->{'sql'} =~ s/^$self->{'statement_end'}//iso  );
}

sub relational_properties {
  my $self = shift;
  my $properties;
  my $match = 0;
  unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
  $self->whitespace();
  do {
    $self->whitespace();
    if ( $self->out_of_line_constraint() ) {
      $match = 1;
      ${$properties->{$self->{'key'}->{'out_of_line_constraint'}}}[@{$properties->{$self->{'key'}->{'out_of_line_constraint'}}}] = $self->{'tmp'}->{$self->{'key'}->{'out_of_line_constraint'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'out_of_line_constraint'}};
    }
    elsif ( $self->out_of_line_ref_constraint() ) {
      $match = 1;
      ${$properties->{$self->{'key'}->{'out_of_line_ref_constraint'}}}[@{$properties->{$self->{'key'}->{'out_of_line_ref_constraint'}}}] = $self->{'tmp'}->{$self->{'key'}->{'out_of_line_ref_constraint'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'out_of_line_ref_constraint'}};
    }
    elsif ( $self->column_def() ) {
      $match = 1;
      ${$properties->{$self->{'key'}->{'column_def'}}}[@{$properties->{$self->{'key'}->{'column_def'}}}] = $self->{'tmp'}->{$self->{'key'}->{'column_def'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'column_def'}};
    }
    else {
      return $self->do_err("parse error in relational_properties.");
    }
    $self->whitespace();
  } while ( $self->{'sql'} =~ s/^,//is );
  $self->whitespace();
  unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in relational_properties"); }

  $self->whitespace();
  $self->{'tmp'}->{$self->{'key'}->{'relational_properties'}} = $properties;
  return $match;
}

sub out_of_line_constraint {
  my $self = shift;
  my $match = 0;
  my $constraint;
  if ( $self->{'sql'} =~ s/^CONSTRAINT//is ) {
    $self->whitespace();
    my $identifier;
    if ( $identifier = $self->identifier() ) {
      $match = 1;
      $constraint->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    }
    else {
      return $self->do_err("string value for constraint expected in out_of_line_constraint");
    }
  }
  if ( $self->{'sql'} =~ s/^UNIQUE//is ) {
    $self->whitespace();
    unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected in out_of_line_constraint"); }
    $self->whitespace();
    $self->column_list();
    unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in out_of_line_constraint"); }
    $self->whitespace();
    $constraint->{$self->{'key'}->{'out_of_line_unique_constraint'}}->{$self->{'key'}->{'column'}} = $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
    $match = 1;
  }
  elsif ( $self->{'sql'} =~ s/^PRIMARY//is ) {
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^KEY//is ) {
      $self->whitespace();
      unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected in out_of_line_constraint"); }
      $self->whitespace();
      $self->column_list();
      unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in out_of_line_constraint"); }
      $self->whitespace();
      $constraint->{$self->{'key'}->{'out_of_line_primary_key_constraint'}}->{$self->{'key'}->{'column'}} = $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
      $match = 1;
    }
    else {
      return $self->do_err("keyword 'KEY' expected in out_of_line_constraint");
    }
  }
  elsif ( $self->{'sql'} =~ s/^FOREIGN//is ) {
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^KEY//is ) {
      $self->whitespace();
      unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected in out_of_line_constraint"); }
      $self->whitespace();
      $self->column_list();
      unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in out_of_line_constraint"); }
      $self->whitespace();
      $constraint->{$self->{'key'}->{'out_of_line_foreign_key_constraint'}}->{$self->{'key'}->{'column'}} = $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
      unless ($self->references_clause()) { return $self->do_err("references clause expected in out_of_line_constraint"); }
      $constraint->{$self->{'key'}->{'out_of_line_foreign_key_constraint'}}->{$self->{'key'}->{'references_clause'}} = $self->{'tmp'}->{$self->{'key'}->{'references_clause'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'references_clause'}};
      $match = 1;
    }
    else {
      return $self->do_err("keyword 'KEY' expected in out_of_line_constraint");
    }
  }
  elsif ( $self->{'sql'} =~ s/^CHECK//is ) {
    $self->whitespace();
    unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected in out_of_line_constraint"); }
    $self->whitespace();
    unless ( $self->condition() ) { return $self->do_err("condition expected in out_of_line_constraint"); }
    $self->whitespace();
    unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in out_of_line_constraint"); }
    $self->whitespace();
    $constraint->{$self->{'key'}->{'out_of_line_check_constraint'}}->{$self->{'key'}->{'condition'}} =  $self->{'tmp'}->{$self->{'key'}->{'condition'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'condition'}};
    $match = 1;
  }
  else {
    if ($match == 1 ) {
      return $self->do_err("unknown out_of_line_constraint constraint");
    }
  }

  if ($match) {
    if ( $self->dbms_constraint_state() ) {
      $constraint->{$self->{'key'}->{'dbms_constraint_state'}} = $self->{'tmp'}->{$self->{'key'}->{'dbms_constraint_state'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'dbms_constraint_state'}};
    }
    $self->whitespace();
    $self->{'tmp'}->{$self->{'key'}->{'out_of_line_constraint'}} = $constraint;
  }
  return $match;
}

sub out_of_line_ref_constraint {
  my $self = shift;
  my $match = 0;
  my $constraint;
  if ( $self->constraint_reference() ) {
    $match = 1;
    $self->{'tmp'}->{$self->{'key'}->{'out_of_line_ref_constraint'}}->{$self->{'key'}->{'constraint_reference'}} = $self->{'tmp'}->{$self->{'key'}->{'constraint_reference'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'constraint_reference'}};
  }
  return $match;
}

sub constraint_reference {
  my $self = shift;
  my $match = 0;
  my $identifier;
  my $reference;
  if ( $self->{'sql'} =~ s/^CONSTRAINT//is ) {
    $self->whitespace();
    $match = 1;
    unless ($identifier = $self->identifier()) {
      return $self->do_err("identifier expected");
    }
    $reference->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
  }
  if ( $self->{'sql'} =~ s/^FOREIGN//is ) {
    $self->whitespace();
    $match = 1;
    if ( $self->{'sql'} =~ s/^KEY//is ) {
      $self->whitespace();
      unless ($identifier = $self->identifier()) {
        return $self->do_err("identifier expected");
      }
      $reference->{'-attributes'}->{$self->{'key'}->{'reference'}} = $identifier;
      unless ( $self->references_clause() ) {
        return $self->do_err("references_clause expected");
      }
      $reference->{$self->{'key'}->{'references_clause'}} = $self->{'tmp'}->{$self->{'key'}->{'references_clause'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'references_clause'}};
      if ($self->dbms_constraint_state() ) {
        $reference->{$self->{'key'}->{'dbms_constraint_state'}} = $self->{'tmp'}->{$self->{'key'}->{'dbms_constraint_state'}};
        delete $self->{'tmp'}->{$self->{'key'}->{'dbms_constraint_state'}};
      }
    }
    else {
      return $self->do_err("keyword 'KEY' expected");
    }
  }
  else {
    if ($match == 1) {
      return $self->do_err("constraint expected");
    }
  }
  $self->{'tmp'}->{$self->{'key'}->{'constraint_reference'}} = $reference;
  return $match;
}

sub column_def {
  my $self = shift;
  my $column_def;
  my $identifier;
  unless ($identifier = $self->identifier()) {
    return 0;
  }
  my $datatype = $self->datatype();
  if ($datatype) {
    $column_def->{'-attributes'}->{$self->{'key'}->{'type'}} = $datatype;
  }
  $column_def->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
  if ( $self->{'sql'} =~ s/^DEFAULT//is ) {
    $self->whitespace();
    $self->expr();
    $column_def->{$self->{'key'}->{'default_expr'}}->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
  }
  if ( $self->{'sql'} =~ m/^IDENTITY/is ) {
    $self->dbms_identity();
    $column_def->{$self->{'key'}->{'dbms_identity'}} = $self->{'tmp'}->{$self->{'key'}->{'dbms_identity'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'dbms_identity'}};
  }
  my $submatch = 0;
  do {
    $submatch = 0;
    if ( $self->inline_constraint() ) {
      $submatch = 1;
      ${$column_def->{$self->{'key'}->{'inline_constraint'}}}[@{$column_def->{$self->{'key'}->{'inline_constraint'}}}] = $self->{'tmp'}->{$self->{'key'}->{'inline_constraint'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'inline_constraint'}};
    }
    elsif ( $self->dbms_inline_ref_constraint() ) {
      $submatch = 1;
      ${$column_def->{$self->{'key'}->{'dbms_inline_ref_constraint'}}}[@{$column_def->{$self->{'key'}->{'dbms_inline_ref_constraint'}}}] = $self->{'tmp'}->{$self->{'key'}->{'dbms_inline_ref_constraint'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'dbms_inline_ref_constraint'}};
    }
  } while ( $submatch == 1);
  $self->{'tmp'}->{$self->{'key'}->{'column_def'}} = $column_def;
  return 1;
}

sub inline_constraint {
  my $self = shift;
  my $match = 0;
  my $constraint;
  if ( $self->{'sql'} =~ s/^CONSTRAINT//is ) {
    $self->whitespace();
    $match = 1;
    my $identifier;
    if ( $identifier = $self->identifier() ) {
      $constraint->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    }
    else {
      return $self->do_err("constraint name expected in inline_constraint");
    }
  }
  if ( $self->{'sql'} =~ s/^NOT//is) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^NULL//is) {
      $self->whitespace();
      $constraint->{$self->{'key'}->{'inline_null_constraint'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = 'not_null';
      $constraint->{$self->{'key'}->{'inline_null_constraint'}}->{'-empty'} = 1;
    }
    else {
      return $self->do_err("keyword 'NULL' expected in inline_constraint");
    }
  }
  elsif ( $self->{'sql'} =~ s/^NULL//is ) {
    $match = 1;
    $self->whitespace();
    $constraint->{$self->{'key'}->{'inline_null_constraint'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = 'null';
    $constraint->{$self->{'key'}->{'inline_null_constraint'}}->{'-empty'} = 1;
  }
  elsif ( $self->{'sql'} =~ s/^UNIQUE//is) {
    $match = 1;
    $self->whitespace();
    $constraint->{$self->{'key'}->{'inline_unique_constraint'}}->{'-empty'} = 1;
  }
  elsif ( $self->{'sql'} =~ s/^PRIMARY//is) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^KEY//is) {
      $self->whitespace();
      $constraint->{$self->{'key'}->{'inline_primary_key_constraint'}}->{'-empty'} = 1;
    }
    else {
      return $self->do_err("keyword 'KEY' expected in inline_constraint");
    }
  }
  elsif ( $self->{'sql'} =~ s/^CHECK//is) {
    $match = 1;
    $self->whitespace();
    unless ( $self->condition() ) { return $self->do_err("condition expected in inline_constraint"); }
    $constraint->{$self->{'key'}->{'inline_check_constraint'}}->{$self->{'key'}->{'condition'}} = $self->{'tmp'}->{$self->{'key'}->{'condition'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'condition'}};
  }
  elsif ( $self->references_clause() ) {
    $match = 1;
    $constraint->{$self->{'key'}->{'inline_foreign_key_constraint'}}->{$self->{'key'}->{'references_clause'}} = $self->{'tmp'}->{$self->{'key'}->{'references_clause'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'references_clause'}};
  }
  else {
    if ($match == 1) {
      return $self->do_err("unknown constraint type in inline_constraint");
    }
  }
  if ($match == 1 ) {
    if ( $self->dbms_constraint_state() ) {
      $constraint->{$self->{'key'}->{'dbms_constraint_state'}} = $self->{'tmp'}->{$self->{'key'}->{'dbms_constraint_state'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'dbms_constraint_state'}};
    }
    $self->{'tmp'}->{$self->{'key'}->{'inline_constraint'}} = $constraint;
  }
  return $match;
}

sub references_clause {
  my $self = shift;
  my $clause;
  unless ( $self->{'sql'} =~ s/^REFERENCES//is ) {
    return 0;
  }
  $self->whitespace();
  my $identifier;
  if ( $identifier = $self->identifier() ) {
    if ( $self->{'sql'} =~ s/^\.//is  ) {
      $self->whitespace();
      $clause->{'-attributes'}->{$self->{'key'}->{'schema'}} = $identifier;
      unless ( $identifier = $self->identifier() ) { return $self->do_err("identifier expected"); }
      $clause->{'-attributes'}->{$self->{'key'}->{'object'}} = $identifier;
    }
    else {
      $clause->{'-attributes'}->{$self->{'key'}->{'object'}} = $identifier;
    }
  }
  else {
    return $self->do_err("identifier expected");
  }

  if ( $self->{'sql'} =~ s/^\(//is ) {
    $self->whitespace();
    do { 
      $self->whitespace();
      unless ( $identifier = $self->identifier() ) { return $self->do_err("Identifier expected in references clause") }
      #${$self->{'tmp'}->{$self->{'key'}->{'table'}}->{$self->{'key'}->{'relational_properties'}}->{$self->{'key'}->{'out_of_line_constraint'}}}[-1]->{$self->{'key'}->{'references_clause'}}->{'-attributes'}->{$self->{'key'}->{'column'}} = $1;
      ${$clause->{$self->{'key'}->{'column'}}}[@{$clause->{$self->{'key'}->{'column'}}}]->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
      ${$clause->{$self->{'key'}->{'column'}}}[-1]->{'-empty'} = 1;
    } while( $self->{'sql'} =~ s/^,//is );
    if ( $self->{'sql'} =~ s/^\)//is ) {
      $self->whitespace();
    }
    else {
      return $self->do_err("closing bracket expected in refernces_clause");
    }
  }
  else {
    $clause->{'-empty'} = 1;
  }
  if ( $self->{'sql'} =~ s/^ON//is ) {
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^DELETE//is) {
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^CASCADE//is ) {
        $clause->{'-attributes'}->{$self->{'key'}->{'on_delete'}} = $self->{'key'}->{'cascade'};
        $self->whitespace();
      }
      elsif ( $self->{'sql'} =~ s/^SET//is) {
        $self->whitespace();
        if ( $self->{'sql'} =~ s/^NULL//is ) {
          $clause->{'-attributes'}->{$self->{'key'}->{'on_delete'}} = 'set_null';
          $self->whitespace();
        }
        else {
          return $self->do_err("'NULL' keyword expected in refernces_clause");
        }
      }
      else {
        return $self->do_err("unknown keyword in refernces_clause");
      }
    }
    else {
      return $self->do_err("'DELETE' keyword expected in refernces_clause");
    }
  }
  $self->{'tmp'}->{$self->{'key'}->{'references_clause'}} = $clause;
  return 1;
}



######################################################################
#
# CREATE TABLE DBMS EXTENSION
#
######################################################################

sub dbms_create_table {
  return 0;
}

sub dbms_constraint_state {
  return 0;
}

sub dbms_inline_ref_constraint {
  return 0;
}

sub dbms_identity {
  return 0;
}


######################################################################
#
# END CREATE TABLE
#
######################################################################



######################################################################
#
# *** CREATE INDEX ***
#
######################################################################

sub create_index {
  my $self = shift;
  my $type;
  if ( $self->{'sql'} =~ s/^(UNIQUE|BITMAP|CLUSTERED|NONCLUSTERED)//is )  {
    $self->{'tmp'}->{$self->{'key'}->{'index'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = lc $1;
    $self->whitespace();
  }
  unless ( $self->{'sql'} =~ s/^INDEX//is )  {
    return 0;
  }
  $self->whitespace();
  my $identifier;
  if ( $identifier = $self->identifier() ) {
    if ( $self->{'sql'} =~ s/^\.//is  ) {
      $self->whitespace();
      $self->{'tmp'}->{$self->{'key'}->{'index'}}->{'-attributes'}->{$self->{'key'}->{'schema'}} = $identifier;
      unless ( $identifier = $self->identifier() ) { return $self->do_err("indexname expected"); }
      $self->{'tmp'}->{$self->{'key'}->{'index'}}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    }
    else {
      $self->{'tmp'}->{$self->{'key'}->{'index'}}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    }
  }
  else {
    return $self->do_err("index- or schemaname expected");
  }
  unless ( $self->{'sql'} =~ s/^ON//is  ) { return $self->do_err("'ON' keyword expected") ; }
  $self->whitespace();
  if ( $self->dbms_cluster_index_clause ) {
  }
  elsif ( $self->table_index_clause() ) {
  }
  elsif ( $self->dbms_bitmap_join_index_clause() ) {
  }
  else {
    return $self->do_err("Cannot parse create index statement") ;
  }
  $self->whitespace();
  ${ $self->{'sqldata'}}[@{ $self->{'sqldata'}}] ->{$self->{'key'}->{'create_index'}} = $self->{'tmp'}->{$self->{'key'}->{'index'}};
  return $self->do_err("End of statement expected in create") unless ( ($self->{'statement_end'} eq '') || $self->{'sql'} =~ s/^$self->{'statement_end'}//iso  );
}

sub table_index_clause {
  my $self = shift;
  my $match = 0;
  my $identifier;
  if ( $identifier = $self->identifier() ) {
    if ( $self->{'sql'} =~ s/^\.//is  ) {
      $self->whitespace();
      $self->{'tmp'}->{$self->{'key'}->{'index'}}->{$self->{'key'}->{'table_index_clause'}}->{'-attributes'}->{$self->{'key'}->{'schema'}} = $identifier;
      unless ( $identifier = $self->identifier() ) { return $self->do_err("identifier expected"); }
      $self->{'tmp'}->{$self->{'key'}->{'index'}}->{$self->{'key'}->{'table_index_clause'}}->{'-attributes'}->{$self->{'key'}->{'table'}} = $identifier;
    }
    else {
      $self->{'tmp'}->{$self->{'key'}->{'index'}}->{$self->{'key'}->{'table_index_clause'}}->{'-attributes'}->{$self->{'key'}->{'table'}} = $identifier;
    }
  }
  else {
    return $self->do_err("identifier expected");
  }
  if ( $identifier = $self->identifier() ) {
    $self->{'tmp'}->{$self->{'key'}->{'index'}}->{$self->{'key'}->{'table_index_clause'}}->{'-attributes'}->{$self->{'key'}->{'alias'}} = $identifier;
  }
  $self->whitespace();
  if (  $self->{'sql'} =~ s/^\(//is  ) {
    do {
      $self->whitespace();
      unless ( $self->index_expr() ) {
        return $self->do_err("Index-expression expected") ;
      }
      ${$self->{'tmp'}->{$self->{'key'}->{'index'}}->{$self->{'key'}->{'table_index_clause'}}->{$self->{'key'}->{'index_expr'}}}[@{$self->{'tmp'}->{$self->{'key'}->{'index'}}->{$self->{'key'}->{'table_index_clause'}}->{$self->{'key'}->{'index_expr'}}}] = $self->{'tmp'}->{$self->{'key'}->{'index_expr'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'index_expr'}};
      $self->whitespace();
    }  while ( $self->{'sql'} =~ s/^,//is );
    unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in table_index_clause"); }
    $self->whitespace();
  }
  else {
    return $self->do_err("Error parsing create index statement") ;
  }
  if ( $self->dbms_table_index_clause() ) {
    $self->{'tmp'}->{$self->{'key'}->{'index'}}->{$self->{'key'}->{'table_index_clause'}}->{$self->{'key'}->{'dbms_table_index_clause'}} = $self->{'tmp'}->{$self->{'key'}->{'dbms_table_index_clause'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'dbms_table_index_clause'}};
  }
  $self->whitespace();
  return 1;
}

sub index_expr {
  my $self = shift;
  my $match = 0;
  my $identifier;
  if ( $identifier = $self->identifier() ) {
    $match = 1;
    $self->{'tmp'}->{$self->{'key'}->{'index_expr'}}->{$self->{'key'}->{'column'}}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    $self->{'tmp'}->{$self->{'key'}->{'index_expr'}}->{$self->{'key'}->{'column'}}->{'-empty'} = 1;
    $self->whitespace();
  }
  else {
    return $self->do_err("Error parsing create index statement") ;
  }
  if ( $match == 1 ) {
    if ( $self->{'sql'} =~ s/^(ASC|DESC)//is ) {
      $self->{'tmp'}->{$self->{'key'}->{'index_expr'}}->{'-attributes'}->{$self->{'key'}->{'ordering'}} = lc $1;
    }
    $self->whitespace();
  }
  return $match;
}


######################################################################
#
# CREATE INDEX DBMS EXTENSION
#
######################################################################


sub dbms_table_index_clause {
  return 0;
}

sub dbms_cluster_index_clause {
  return 0;
}

sub dbms_bitmap_join_index_clause {
  return 0;
}


######################################################################
#
# END CREATE INDEX
#
######################################################################



######################################################################
#
# *** SELECT ***
#
######################################################################

sub select {
  my $self = shift;
  unless ($self->subquery()) { return $self->do_err("No subquery found in select statement"); }
  ${ $self->{'sqldata'}}[@{ $self->{'sqldata'}}]->{$self->{'key'}->{'select'}}->{$self->{'key'}->{'subquery'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
  delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
  if ( $self->dbms_for_update_clause() ) {
    ${ $self->{'sqldata'}}[-1]->{$self->{'key'}->{'select'}}->{$self->{'key'}->{'dbms_for_update_clause'}}->{$self->{'key'}->{'for_update_clause'}} = $self->{'tmp'}->{$self->{'key'}->{'for_update_clause'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'for_update_clause'}};
  }
  unless ( ($self->{'statement_end'} eq '') || $self->{'sql'} =~ s/^$self->{'statement_end'}//iso  ) { return $self->do_err("End of statement expected in select"); }
  if ( $self->{'occurence_count'} > 0 ) {
    ${ $self->{'sqldata'}}[-1]->{$self->{'key'}->{'select'}}->{$self->{'key'}->{'subquery'}}->{'-attributes'}->{'occurence_count'} = $self->{'occurence_count'};
  }
}

sub subquery {
  my $self = shift;
  my $subquery;
  unless ( $self->{'sql'} =~ s/^SELECT//is  ) { return 0; }
  $self->whitespace();
  if ( $self->dbms_hint() ) {
    $subquery->{$self->{'key'}->{'dbms_optimizer_hint'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}}->{$self->{'key'}->{'dbms_optimizer_hint'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
  }
  if ($self->{'sql'} =~ s/^(DISTINCT|UNIQUE|ALL)//is  ) {
    $subquery->{'-attributes'}->{$self->{'key'}->{'type'}} = lc $1;
    delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
    $self->whitespace();
  }
  if ( $self->select_list() ) {
    $subquery->{$self->{'key'}->{'select_list'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}}->{$self->{'key'}->{'select_list'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
  }
  unless ( $self->{'sql'} =~ s/^FROM//is ) { return $self->do_err("FROM clause expected"); }
  $self->whitespace();
  do {
    $self->whitespace();
    unless ( $self->table_reference() ) { return $self->do_err("table_reference expected"); }
    ${$subquery->{$self->{'key'}->{'from'}}->{$self->{'key'}->{'table_reference'}}}[@{$subquery->{$self->{'key'}->{'from'}}->{$self->{'key'}->{'table_reference'}}}] = $self->{'tmp'}->{$self->{'key'}->{'table_reference'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'table_reference'}};
  } while ( $self->{'sql'} =~ s/^,//is );
  if ( $self->{'sql'} =~ s/^WHERE//is ) {
    $self->whitespace();
    if ( $self->condition() ) {
      $subquery->{'where'}->{$self->{'key'}->{'condition'}} = $self->{'tmp'}->{$self->{'key'}->{'condition'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'condition'}};
    }
    else {
      return $self->do_err("condition in where clause expected");
    }
  }
  if ( $self->group_by_clause() ) {
    $subquery->{$self->{'key'}->{'group_by'}} = $self->{'tmp'}->{$self->{'key'}->{'group_by'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'group_by'}};
  }
  if ( $self->{'sql'} =~ s/^HAVING//is ) {
    $self->whitespace();
    unless ( $self->condition() ) { return $self->do_err("condition in having clause expected"); }
    $subquery->{$self->{'key'}->{'having'}}->{$self->{'key'}->{'condition'}} = $self->{'tmp'}->{$self->{'key'}->{'condition'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'condition'}};
  }
  if ( $self->{'sql'} =~ s/^(INTERSECT|MINUS)//is ) {
    $subquery->{$self->{'key'}->{'combined_subquery'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = lc $1;
    $self->whitespace();
    $self->subquery();
    $subquery->{$self->{'key'}->{'combined_subquery'}}->{$self->{'key'}->{'subquery'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
  }
  elsif ( $self->{'sql'} =~ s/^UNION//is ) {
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^ALL//is ) {
      $self->whitespace();
      $subquery->{$self->{'key'}->{'combined_subquery'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = 'union_all';
    }
    else {
      $subquery->{$self->{'key'}->{'combined_subquery'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = 'union';
    }
    $self->whitespace();
    $self->subquery();
    $subquery->{$self->{'key'}->{'combined_subquery'}}->{$self->{'key'}->{'subquery'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
  }
  if ( $self->order_by_clause() ) {
    $subquery->{$self->{'key'}->{'order_by'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}}->{$self->{'key'}->{'order_by'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
  }
  $subquery->{'-attributes'}->{$self->{'key'}->{'id'}} = "subquery_".$self->{'subquery_id_counter'};
  $self->{'subquery_id_counter'}++;
  $self->{'tmp'}->{$self->{'key'}->{'subquery'}} = $subquery;
  return 1;
}

sub select_list {
  my $self = shift;
  my $match = 0;
  my $select_list;
  my $as_match = 0;
  if ( $self->{'sql'} =~ s/^\*//s ) {
    $match = 1;
    ${$select_list}[@{$select_list}]->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'column'}} = '*';
    ${$select_list}[-1]->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'id'}} = "object_".$self->{'object_id_counter'};
    $self->{'object_id_counter'}++;
    ${$select_list}[-1]->{$self->{'key'}->{'object'}}->{'-empty'} = 1;
    $self->whitespace();
  }
  else {
    my $identifier1;
    my $identifier2;
    do {
      my $submatch = 0;
      $self->whitespace();
      if ( $identifier1 = $self->identifier() ) {
        $match = 1;
        if ( $self->{'sql'} =~ s/^\.//s  ) {
          $self->whitespace();
          if ( $identifier2 = $self->identifier() ) {
            if ( $self->{'sql'} =~ s/^\.//s  ) {
              $self->whitespace();
              if ( $self->{'sql'} =~ s/^\*//s  ) {
                $self->whitespace();
                #  schema.object.*
                ${$select_list}[@{$select_list}]->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'schema'}} = $identifier1;
                ${$select_list}[-1]->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'object'}} = $identifier2;
                ${$select_list}[-1]->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'column'}} = '*';
                ${$select_list}[-1]->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'id'}} = "object_".$self->{'object_id_counter'};
                $self->{'object_id_counter'}++;
                ${$select_list}[-1]->{$self->{'key'}->{'object'}}->{'-empty'} = 1;
                $submatch = 1;
              }
              else {
                # undo the nibbling
                $self->{'sql'} = $identifier1.'.'.$identifier2.'.'. $self->{'sql'};
              }
            }
            else {
              # undo the nibbling
              $self->{'sql'} = $identifier1.'.'.$identifier2.' '. $self->{'sql'};
            }
          }
          elsif ( $self->{'sql'} =~ s/^\*//s  ) {
            $self->whitespace();
            #  object.*
            ${$select_list}[@{$select_list}]->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'object'}} = $identifier1;
            ${$select_list}[-1]->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'column'}} = '*';
            ${$select_list}[-1]->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'id'}} = "object_".$self->{'object_id_counter'};
            $self->{'object_id_counter'}++;
            ${$select_list}[-1]->{$self->{'key'}->{'object'}}->{'-empty'} = 1;
            $submatch = 1;
          }
          else {
            # undo the nibbling
            $self->{'sql'} = $identifier1.'.'. $self->{'sql'};
          }
        }
        else {
          # undo the nibbling
          $self->{'sql'} = $identifier1.' '. $self->{'sql'};
        }
        if ( $submatch != 1 ) {
          # expr
          if ( $self->expr() ) {
            $match = 1;
            $submatch = 1;
            ${$select_list}[@{$select_list}]->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
            delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
            $as_match = 0;
            if ( $self->{'sql'} =~ s/^AS//is ) {
              $as_match = 1;
              $self->whitespace();
            }
            unless ( $self->{'sql'} =~ m/^FROM/is ) {
              $self->whitespace();
              if ( $self->{'sql'} =~ s/^((\w|"|'|&)+)//is ) {
                $match = 1;
                ${$select_list}[-1]->{$self->{'key'}->{'alias'}}->{'-attributes'}->{$self->{'key'}->{'name'}} = $1;
                ${$select_list}[-1]->{$self->{'key'}->{'alias'}}->{'-empty'} = 1;
                $self->whitespace();
              }
              elsif ($as_match) {
                return $self->do_err("alias expected");
              }
              delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
            }
          }
        }
        unless ( $submatch == 1 ) { return $self->do_err("cannot parse select_list"); }
      }
      elsif ( $self->expr() ) {
        # expr
        $match = 1;
        ${$select_list}[@{$select_list}]->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
        delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
        $as_match = 0;
        if ( $self->{'sql'} =~ s/^AS//is ) {
          $as_match = 1;
          $self->whitespace();
        }
        unless ( $self->{'sql'} =~ m/^FROM/is ) {
          $self->whitespace();
          if ( $self->{'sql'} =~ s/^\s*((\w|"|'|&)+)//is ) {
            $match = 1;
            ${$select_list}[-1]->{$self->{'key'}->{'alias'}}->{'-attributes'}->{$self->{'key'}->{'name'}} = $1;
            ${$select_list}[-1]->{$self->{'key'}->{'alias'}}->{'-empty'} = 1;
            $self->whitespace();
          }
          elsif ($as_match) {
            return $self->do_err("alias expected");
          }
          delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
        }
      }
      else {
        return $self->do_err("cannot parse select_list");
      }
    } while ( $self->{'sql'} =~ s/^,//s );
  }
  $self->{'tmp'}->{$self->{'key'}->{'subquery'}}->{$self->{'key'}->{'select_list'}} = $select_list;
  return $match;
}

sub t_alias {
  my $self = shift;
  my $match = 0;
  my $alias;
  unless ( $self->{'sql'} =~ m/^(WHERE|INNER|LEFT|RIGHT|FULL|NATURAL|CROSS|JOIN|ON|USING|GROUP)/is ) {
    my $identifier;
    if ( $identifier = $self->identifier() ) {
      $match = 1;
      $alias = $identifier;
    }
  }
  $self->{'tmp'}->{$self->{'key'}->{'alias'}} = $alias;
  return $match;
}

# To make the table_reference parsable more easily, we view the syntax as follows:
#
#    +---[ ONLY ]---( query_table_expression )--- ... ---+   +--- //JOIN// ---+
#    |                                                   |   |                |
# ---+                                                   +---+----------------+-------
#    |                                                   |
#    +---( query_table_expression )-------------- ... ---+
#
# ( a second branch with round brackets arpund the expression above, is necessary, too )

sub table_reference {
  my $self = shift;
  my $match = 0;
  my $table_reference;
  my $brackets = 0;
  if ( $self->{'sql'} =~ s/^\(//is ) {
    $self->whitespace();
    $brackets = 1;
  }
  if ( $self->{'sql'} =~ s/^ONLY//is ) {
    $match = 1;
    $self->whitespace();
    $table_reference->{'-attributes'}->{$self->{'key'}->{'only'}} = 'only';
    unless ( $self->query_table_expression() ) { return $self->do_err("query_table_expression expected"); }
    $table_reference->{$self->{'key'}->{'only_table_reference'}}->{$self->{'key'}->{'query_table_expression'}} = $self->{'tmp'}->{$self->{'key'}->{'query_table_expression'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'query_table_expression'}};
  }
  elsif ( $self->query_table_expression() ) {
    $match = 1;
    $table_reference->{$self->{'key'}->{'query_table_reference'}}->{$self->{'key'}->{'query_table_expression'}} = $self->{'tmp'}->{$self->{'key'}->{'query_table_expression'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'query_table_expression'}};
    if ($self->t_alias()) {
      $table_reference->{$self->{'key'}->{'query_table_reference'}}->{$self->{'key'}->{'alias'}}->{'-attributes'}->{$self->{'key'}->{'name'}} = $self->{'tmp'}->{$self->{'key'}->{'alias'}};
      $table_reference->{$self->{'key'}->{'query_table_reference'}}->{$self->{'key'}->{'alias'}}->{'-empty'} = 1;
      delete $self->{'tmp'}->{$self->{'key'}->{'alias'}};
    }
  }
  else {
    if ($brackets == 1 ) {
      $self->{'sql'} = '(' . $self->{'sql'};
    }
  }
  if ( $match == 1 ) {
    # the join must be preceded by a table_reference, so we can test for it here optionally
    if ( $self->joined_table() ) {
      $self->{'tmp'}->{$self->{'key'}->{'joined_table'}}->{$self->{'key'}->{'table_reference'}} = $table_reference;
      $table_reference = undef;
      $table_reference->{$self->{'key'}->{'joined_table'}} = $self->{'tmp'}->{$self->{'key'}->{'joined_table'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'joined_table'}};
    }
    if ($brackets == 1 ) {
      unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in table_reference"); }
      $self->whitespace();
    }
    $self->{'tmp'}->{$self->{'key'}->{'table_reference'}} = $table_reference;
  }
  return $match;
}



sub group_by_clause {
  my $self = shift;
  my $match = 0;
  my $group_by;
  if ( $self->{'sql'} =~ s/^GROUP//is ) {
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^BY//is ) {
      $match = 1;
      $self->whitespace();
      do {
        $self->whitespace();
        unless ( $self->expr() ) { return $self->do_err("expression expected in group_by_clause"); };
        ${$group_by}[@{$group_by}]->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
        delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
      } while ( $self->{'sql'} =~ s/^,//is );
    }
    else {
      return $self->do_err("keyword 'BY' expected");
    }
    $self->{'tmp'}->{$self->{'key'}->{'group_by'}} = $group_by;
  }
  return $match;
}

sub order_by_clause {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^ORDER//is ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^SIBLINGS//is ) {
      $self->whitespace();
      $self->{'tmp'}->{$self->{'key'}->{'subquery'}}->{$self->{'key'}->{'order_by'}}->{'-attributes'}->{$self->{'key'}->{'siblings'}} = 'siblings';
    }
    unless ( $self->{'sql'} =~ s/^BY//is ) { return $self->do_err("keyword 'BY' expected"); }
    $self->whitespace();
    do {
      $self->whitespace();
      unless ( $self->expr() ) { return $self->do_err("expression expected in order_by_clause"); };
      ${$self->{'tmp'}->{$self->{'key'}->{'subquery'}}->{$self->{'key'}->{'order_by'}}->{$self->{'key'}->{'order_by_element'}}}[@{$self->{'tmp'}->{$self->{'key'}->{'subquery'}}->{$self->{'key'}->{'order_by'}}->{$self->{'key'}->{'order_by_element'}}}]->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
      if ( $self->{'sql'} =~ s/^(ASC|DESC)//is ) {
        ${$self->{'tmp'}->{$self->{'key'}->{'subquery'}}->{$self->{'key'}->{'order_by'}}->{$self->{'key'}->{'order_by_element'}}}[-1]->{'-attributes'}->{$self->{'key'}->{'ordering'}} = lc $&;
        $self->whitespace();
      }
      if ( $self->{'sql'} =~ s/^NULLS//is ) {
        $self->whitespace();
        if ( $self->{'sql'} =~ s/^(FIRST|LAST)//is ) {
          ${$self->{'tmp'}->{$self->{'key'}->{'subquery'}}->{$self->{'key'}->{'order_by'}}->{$self->{'key'}->{'order_by_element'}}}[-1]->{'-attributes'}->{$self->{'key'}->{'nulls'}} = lc $&;
          $self->whitespace();
        }
        else {
          return $self->do_err("keyword 'FIRST' or 'LAST' expected");
        }
      }
    } while ( $self->{'sql'} =~ s/^,//is );
  }
  return $match;
}

sub query_table_expression {
  my $self = shift;
  my $match = 0;
  my $query_table;
  my $identifier;
  if ( $identifier = $self->identifier() ) {
    $match = 1;
    if ( $self->{'sql'} =~ s/^\.//is  ) {
      $self->whitespace();
      ${$query_table}[@{$query_table}]->{$self->{'key'}->{'simple_query_table_expression'}}->{'-attributes'}->{$self->{'key'}->{'schema'}} = $identifier;
      unless ( $identifier = $self->identifier() ) { return $self->do_err("identifier expected"); }
      ${$query_table}[-1]->{$self->{'key'}->{'simple_query_table_expression'}}->{'-attributes'}->{$self->{'key'}->{'object'}} = $identifier;
    }
    else {
      ${$query_table}[@{$query_table}]->{$self->{'key'}->{'simple_query_table_expression'}}->{'-attributes'}->{$self->{'key'}->{'object'}} = $identifier;
    }
    ${$query_table}[-1]->{$self->{'key'}->{'simple_query_table_expression'}}->{'-empty'} = 1;
  }
  elsif ( $self->{'sql'} =~ s/^\(//is ) {
    $match = 1;
    $self->whitespace();
    unless ( $self->subquery() ) { return $self->do_err("subquery expected"); }
    $query_table->{$self->{'key'}->{'subquery_query_table_expression'}}->{$self->{'key'}->{'subquery'}} =  $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
    unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in query_table_expression"); }
    $self->whitespace();
  }
  if ($match ) {
    $self->{'tmp'}->{$self->{'key'}->{'query_table_expression'}} = $query_table;
  }
  return $match;
}



sub joined_table {
  my $self = shift;
  my $match = 0;
  my $joined_table;
  # join type
  if ( $self->join_type() ) {
    $joined_table->{$self->{'key'}->{'join'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = $self->{'tmp'}->{$self->{'key'}->{'jointype'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'jointype'}};
    $match = 1;
  }
  # first table_reference has already been tested before
  if ( $self->{'sql'} =~ s/^JOIN//is ) {
    $match = 1;
    $self->whitespace();
    $self->table_reference();

    $joined_table->{$self->{'key'}->{'join'}}->{$self->{'key'}->{'table_reference'}} = $self->{'tmp'}->{$self->{'key'}->{'table_reference'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'table_reference'}};
    if ( $self->{'sql'} =~ s/^ON//is ) {
       $self->whitespace();
       if ( $self->condition() ) {
         $joined_table->{$self->{'key'}->{'join'}}->{$self->{'key'}->{'join_on'}}->{$self->{'key'}->{'condition'}} = $self->{'tmp'}->{$self->{'key'}->{'condition'}};
         delete $self->{'tmp'}->{$self->{'key'}->{'condition'}};
      }
      else {
        return $self->do_err("condition expected");
      }
    }
    elsif ( $self->{'sql'} =~ s/^USING//is ) {
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^\(//is ) {
        $self->whitespace();
        $self->column_list();
        $joined_table->{$self->{'key'}->{'join'}}->{$self->{'key'}->{'join_using'}}->{$self->{'key'}->{'column'}} = $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
        unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in joined_table"); }
        $self->whitespace();
      }
      else {
        return $self->do_err("opening bracket expected in joined_table");
      }
    }
    else {
      return $self->do_err("either ON or USING identifier expected");
    }
  }
  elsif ( $self->{'sql'} =~ s/^CROSS//is ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^JOIN//is ) {
      $self->whitespace();
      if ( $self->table_reference() ) {
        $joined_table->{$self->{'key'}->{'cross_join'}}->{$self->{'key'}->{'table_reference'}} = $self->{'tmp'}->{$self->{'key'}->{'table_reference'}};
        delete $self->{'tmp'}->{$self->{'key'}->{'table_reference'}};
      }
      else {
        return $self->do_err("table reference expected");
      }
    }
    else {
      return $self->do_err("keyword 'JOIN' expected");
    }
  }
  elsif ( $self->{'sql'} =~ s/^NATURAL//is ) {
    $match = 1;
    $self->whitespace();
    if ( $self->join_type() ) {
      $joined_table->{$self->{'key'}->{'natural_join'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = $self->{'tmp'}->{$self->{'key'}->{'jointype'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'jointype'}};
    }
    if ( $self->{'sql'} =~ s/^JOIN//is ) {
      $self->whitespace();
      if ( $self->table_reference() ) {
        $joined_table->{$self->{'key'}->{'natural_join'}}->{$self->{'key'}->{'table_reference'}} = $self->{'tmp'}->{$self->{'key'}->{'table_reference'}};
        delete $self->{'tmp'}->{$self->{'key'}->{'table_reference'}};
      }
      else {
        return $self->do_err("table reference expected");
      }
    }
    else {
      return $self->do_err("keyword 'JOIN' expected");
    }
  }
  else {
    if ($match) {
      return $self->do_err("keyword 'JOIN' expected");
    }
    return 0;
  }
  $self->{'tmp'}->{$self->{'key'}->{'joined_table'}} = $joined_table;
  return $match;
}

sub join_type {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^INNER//is ) {
    $self->{'tmp'}->{$self->{'key'}->{'jointype'}} = lc $&;
    $match = 1;
    $self->whitespace();
  }
  elsif ( $self->{'sql'} =~ s/^(LEFT|RIGHT|FULL)//is ) {
    $self->{'tmp'}->{$self->{'key'}->{'jointype'}} = lc $&;
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^OUTER//is ) {
      $self->{'tmp'}->{$self->{'key'}->{'jointype'}} .= '_outer';
      $self->whitespace();
    }
  }
  return $match;
}


######################################################################
#
# SELECT DBMS EXTENSION
#
######################################################################


sub dbms_for_update_clause {
  return 0;
}

sub dbms_hint {
  return 0;
}


######################################################################
#
# END SELECT
#
######################################################################




######################################################################
#
# *** CONDITION ***
#
######################################################################


sub condition {
  my $self = shift;
  my $negated_compound_condition = shift; #  not is more binding than AND and OR, so this parameter disables the checking for AND and OR and the end of this method
  my $match = 0;
  my $condition;
  if ( $self->{'sql'} =~ s/^\(//is ) {
    $match = 1;
    $self->whitespace();
    if ( $self->condition() ) {
      # compound_cond
      $condition->{$self->{'key'}->{'compound_condition'}}->{$self->{'key'}->{'right_condition'}}->{$self->{'key'}->{'condition'}} = $self->{'tmp'}->{$self->{'key'}->{'condition'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'condition'}};
      $condition->{$self->{'key'}->{'compound_condition'}}->{'-attributes'}->{$self->{'key'}->{'operator'}} = 'brackets';
      unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in compound_condition"); }
      $self->whitespace();
    }
    elsif ( $self->expr_list() ) {
      my $expr_list = $self->{'tmp'}->{$self->{'key'}->{'expr_list'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'expr_list'}};
      unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected after expr_list in condition"); }
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^(=|!=|\^=|<>)//is ) {
        my $operator = $&;
        $self->whitespace();
        if ( $self->{'sql'} =~ s/^(ANY|SOME|ALL)//is ) {
          my $type = lc $&;
          $self->whitespace();
          # group_comparison_cond
          unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
          $self->whitespace();
          if ( $self->subquery() ) {
            $self->{'tmp'}->{$self->{'key'}->{'group_comparison_condition'}}->{$self->{'key'}->{'subquery'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
            delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
          }
          else {
            my $expr_list_list;
            do {
              $self->whitespace();
              unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
              $self->whitespace();
              unless ( $self->expr_list() ) { return $self->do_err("expr_list expected"); }
              ${$expr_list_list}[@{$expr_list_list}] = $self->{'tmp'}->{$self->{'key'}->{'expr_list'}};
              delete $self->{'tmp'}->{$self->{'key'}->{'expr_list'}};
              unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in group_comparison_condition"); }
              $self->whitespace();
            } while ( $self->{'sql'} =~ s/^,//is );

            $self->{'tmp'}->{$self->{'key'}->{'group_comparison_condition'}}->{$self->{'key'}->{'expr_list'}} = $expr_list_list;
          }
          unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in group_comparison_condition"); }
          $self->whitespace();
          $condition->{$self->{'key'}->{'group_comparison_condition'}}->{$self->{'key'}->{'left_expr_list'}} = $expr_list;
          $condition->{$self->{'key'}->{'group_comparison_condition'}}->{'-attributes'}->{$self->{'key'}->{'operator'}} = $operator;
          $condition->{$self->{'key'}->{'group_comparison_condition'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = $type;
        }
        else {
          # simple_comparison_cond
          unless ( $self->subquery() ) { return $self->do_err("subquery expected"); }
          $condition->{$self->{'key'}->{'simple_comparison_condition'}}->{$self->{'key'}->{'expr_list'}} = $expr_list;
          $condition->{$self->{'key'}->{'simple_comparison_condition'}}->{'-attributes'}->{$self->{'key'}->{'operator'}} = $operator;
          $condition->{$self->{'key'}->{'simple_comparison_condition'}}->{$self->{'key'}->{'subquery'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
          delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
        }
      }
      else {
        # group_membership_cond
        $condition->{$self->{'key'}->{'group_membership_condition'}}->{$self->{'key'}->{'left_expr_list'}} = $expr_list;
        if ( $self->{'sql'} =~ s/^NOT//is ) {
          $self->whitespace();
          $condition->{$self->{'key'}->{'group_membership_condition'}}->{'-attributes'}->{$self->{'key'}->{'not'}} = 'not';
        }
        unless ( $self->{'sql'} =~ s/^IN//is ) { return $self->do_err("keyowrd 'IN' expected"); }
        $self->whitespace();
        unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
        $self->whitespace();
        if ( $self->subquery() ) {
          $self->{'tmp'}->{$self->{'key'}->{'group_membership_condition'}}->{$self->{'key'}->{'subquery'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
          delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
        }
        else {
          my $expr_list_list;
          do {
            $self->whitespace();
            unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
            $self->whitespace();
            unless ( $self->expr_list() ) { return $self->do_err("expr_list expected"); }
            ${$expr_list_list}[@{$expr_list_list}] = $self->{'tmp'}->{$self->{'key'}->{'expr_list'}};
            delete $self->{'tmp'}->{$self->{'key'}->{'expr_list'}};
            unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in group_membership_condition"); }
            $self->whitespace();
          } while ( $self->{'sql'} =~ s/^,//is );

          $self->{'tmp'}->{$self->{'key'}->{'group_membership_condition'}}->{$self->{'key'}->{'expr_list'}} = $expr_list_list;
        }
        unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in group_membership_condition"); }
        $self->whitespace();
      }
    }
    else {
      return $self->do_err("comparison_condition or membership_condition expected");
    }
  }
  elsif ( $self->{'sql'} =~ s/^NOT//is ) {
    $match = 1;
    $self->whitespace();
    # compund_cond
    unless ( $self->condition(1) ) { return $self->do_err("condition expected"); }
    $condition->{$self->{'key'}->{'compound_condition'}}->{$self->{'key'}->{'right_condition'}}->{$self->{'key'}->{'condition'}} = $self->{'tmp'}->{$self->{'key'}->{'condition'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'condition'}};
    $condition->{$self->{'key'}->{'compound_condition'}}->{'-attributes'}->{$self->{'key'}->{'operator'}} = 'not';
  }
  elsif ( $self->{'sql'} =~ s/^EXISTS//is ) {
    $match = 1;
    $self->whitespace();
    # exists_cond
    unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
    $self->whitespace();
    unless ( $self->subquery() ) { return $self->do_err("subquery expected"); }
    $condition->{$self->{'key'}->{'exists_condition'}}->{$self->{'key'}->{'subquery'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
    unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in exists_condition"); }
    $self->whitespace();
  }
  elsif ( $self->expr() ) {
    $match = 1;
    my $expr = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
    if ( $self->{'sql'} =~ s/^IS//is ) {
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^NOT//is ) {
        $self->whitespace();
        $self->{'tmp'}->{'negation'} = 1;
      }
      if ( $self->{'sql'} =~ s/^OF//is ) {
        $self->whitespace();
        # is_of_type_cond
        unless ( $self->is_of_type_cond() ) { return $self->do_err("is_of_type_condition expected"); }
        $condition->{$self->{'key'}->{'is_of_type_condition'}} = $self->{'tmp'}->{$self->{'key'}->{'is_of_type_condition'}};
        delete $self->{'tmp'}->{$self->{'key'}->{'is_of_type_condition'}};
        $condition->{$self->{'key'}->{'is_of_type_condition'}}->{$self->{'key'}->{'expr'}} = $expr;
      }
      elsif ( $self->{'sql'} =~ s/^NULL//is ) {
        $self->whitespace();
        # null_cond
        $condition->{$self->{'key'}->{'null_condition'}}->{$self->{'key'}->{'expr'}} = $expr;
        if ( $self->{'tmp'}->{'negation'} == 1 ) {
          $condition->{$self->{'key'}->{'null_condition'}}->{'-attributes'}->{$self->{'key'}->{'not'}} = 'not';
          delete $self->{'tmp'}->{'negation'};
        }
      }
      else {
        return $self->do_err("keyword 'OF' or 'NULL' expected");
      }
    }
    elsif ( $self->{'sql'} =~ s/^NOT//is ) {
      $self->whitespace();
      $self->{'tmp'}->{'negation'} = 1;
      if ( $self->{'sql'} =~ s/^BETWEEN//is ) {
        $self->whitespace();
        # range_cond
        unless ( $self->range_cond() ) { return $self->do_err("range_condition expected"); }
        $condition->{$self->{'key'}->{'range_condition'}} = $self->{'tmp'}->{$self->{'key'}->{'range_condition'}};
        delete $self->{'tmp'}->{$self->{'key'}->{'range_condition'}};
        $condition->{$self->{'key'}->{'range_condition'}}->{$self->{'key'}->{'left_expr'}}->{$self->{'key'}->{'expr'}} = $expr;
      }
      elsif ( $self->{'sql'} =~ s/^IN//is ) {
        $self->whitespace();
        # simple_membership_cond
        unless ( $self->simple_membership_cond() ) { return $self->do_err("simple_membership_condition expected"); }
        $condition->{$self->{'key'}->{'simple_membership_condition'}} = $self->{'tmp'}->{$self->{'key'}->{'simple_membership_condition'}};
        delete $self->{'tmp'}->{$self->{'key'}->{'simple_membership_condition'}};
        $condition->{$self->{'key'}->{'simple_membership_condition'}}->{$self->{'key'}->{'left_expr'}}->{$self->{'key'}->{'expr'}} = $expr;
      }
      elsif ( $self->{'sql'} =~ s/^(LIKEC|LIKE2|LIKE4|LIKE)//is ) {
        my $operator = lc $&;
        $self->whitespace();
        # like_cond
        unless ( $self->expr() ) { return $self->do_err("expression expected in like_condition"); }
        $condition->{$self->{'key'}->{'like_condition'}}->{$self->{'key'}->{'left_expr'}}->{$self->{'key'}->{'expr'}} = $expr;
        $condition->{$self->{'key'}->{'like_condition'}}->{$self->{'key'}->{'right_expr'}}->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
        delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
        $condition->{$self->{'key'}->{'like_condition'}}->{'-attributes'}->{$self->{'key'}->{'operator'}} = $operator;
        $condition->{$self->{'key'}->{'like_condition'}}->{'-attributes'}->{$self->{'key'}->{'not'}} = 'not';
        if ( $self->{'sql'} =~ s/^ESCAPE//is ) {
          $self->whitespace();
          unless ( $self->expr() ) { return $self->do_err("expression expected in escape-part of like_condition"); }
          $condition->{$self->{'key'}->{'like_condition'}}->{$self->{'key'}->{'escape'}}->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
          delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
        }
      }
      else {
        return $self->do_err("keyword 'BETWEEN', 'IN' or 'LIKE' expected");
      }
    }
    elsif ( $self->{'sql'} =~ s/^BETWEEN//is ) {
      $self->whitespace();
      # range_cond
      unless ( $self->range_cond() ) { return $self->do_err("range_condition expected"); }
      $condition->{$self->{'key'}->{'range_condition'}} = $self->{'tmp'}->{$self->{'key'}->{'range_condition'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'range_condition'}};
      $condition->{$self->{'key'}->{'range_condition'}}->{$self->{'key'}->{'left_expr'}}->{$self->{'key'}->{'expr'}} = $expr;
    }
    elsif ( $self->{'sql'} =~ s/^IN//is ) {
      $self->whitespace();
      # simple_membership_cond
      unless ( $self->simple_membership_cond() ) { return $self->do_err("simple_membership_condition expected"); }
      $condition->{$self->{'key'}->{'simple_membership_condition'}} = $self->{'tmp'}->{$self->{'key'}->{'simple_membership_condition'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'simple_membership_condition'}};
      $condition->{$self->{'key'}->{'simple_membership_condition'}}->{$self->{'key'}->{'left_expr'}}->{$self->{'key'}->{'expr'}} = $expr;
    }
    elsif ( $self->{'sql'} =~ s/^\(\+\)//is ) {
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^=//is ) {
        # left outer join
        # we exract the info from the expr
        my $ltable;
        my $lcolumn;
        my $rtable;
        my $rcolumn;
        my $l_expr = $expr;
        unless ( $self->expr() ) { return $self->do_err("expression expected in outer_join_condition"); }
        my $r_expr = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
        delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
        $condition->{$self->{'key'}->{'key_outer_join_condition'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = 'left';
        $condition->{$self->{'key'}->{'key_outer_join_condition'}}->{$self->{'key'}->{'left_expr'}}->{$self->{'key'}->{'expr'}} = $l_expr;
        $condition->{$self->{'key'}->{'key_outer_join_condition'}}->{$self->{'key'}->{'right_expr'}}->{$self->{'key'}->{'expr'}} = $r_expr;
      }
      else {
        return $self->do_err("'=' expected");
      }
    }
    elsif ( $self->{'sql'} =~ s/^(=|!=|\^=|<>|>=|<=|>|<)//is ) {
      my $operator = $&;
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^(ANY|SOME|ALL)//is ) {
        my $type = lc $&;
        $self->whitespace();
        # group_comparison_cond
        unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
        $self->whitespace();
        if ( $self->subquery() ) {
          $self->{'tmp'}->{$self->{'key'}->{'group_comparison_condition'}}->{$self->{'key'}->{'subquery'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
          delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
        }
        elsif ( $self->expr_list() ) {
          $self->{'tmp'}->{$self->{'key'}->{'group_comparison_condition'}}->{$self->{'key'}->{'expr_list'}} = $self->{'tmp'}->{$self->{'key'}->{'expr_list'}};
          delete $self->{'tmp'}->{$self->{'key'}->{'expr_list'}};
        }
        else {
          return $self->do_err("subquery or expr_list expected");
        }
        unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in group_comparison_condition"); }
        $self->whitespace();
        $condition->{$self->{'key'}->{'group_comparison_condition'}}->{$self->{'key'}->{'left_expr'}}->{$self->{'key'}->{'expr'}} = $expr;
        $condition->{$self->{'key'}->{'group_comparison_condition'}}->{'-attributes'}->{$self->{'key'}->{'operator'}} = $operator;
        $condition->{$self->{'key'}->{'group_comparison_condition'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = $type;
      }
      else {
        # simple_comparison_cond
        unless ( $self->expr() ) { return $self->do_err("expression expected in simple_comparison_cond"); }
        if ( $self->{'sql'} =~ s/^\(\+\)//is ) {
          $self->whitespace();
          # right outer join
          # we exract the info from the expr
          my $ltable;
          my $lcolumn;
          my $rtable;
          my $rcolumn;
          my $l_expr =  $expr;
          my $r_expr = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
          delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
            $condition->{$self->{'key'}->{'key_outer_join_condition'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = 'right';
            $condition->{$self->{'key'}->{'key_outer_join_condition'}}->{$self->{'key'}->{'left_expr'}}->{$self->{'key'}->{'expr'}} = $l_expr;
            $condition->{$self->{'key'}->{'key_outer_join_condition'}}->{$self->{'key'}->{'right_expr'}}->{$self->{'key'}->{'expr'}} = $r_expr;
        }
        else {
          $condition->{$self->{'key'}->{'simple_comparison_condition'}}->{$self->{'key'}->{'left_expr'}}->{$self->{'key'}->{'expr'}} = $expr;
          $condition->{$self->{'key'}->{'simple_comparison_condition'}}->{'-attributes'}->{$self->{'key'}->{'operator'}} = $operator;
          $condition->{$self->{'key'}->{'simple_comparison_condition'}}->{$self->{'key'}->{'right_expr'}}->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
          delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
        }
      }
    }
    elsif ( $self->{'sql'} =~ s/^(LIKEC|LIKE2|LIKE4|LIKE)//is ) {
      my $operator = lc $&;
      $self->whitespace();
      # like_cond
      unless ( $self->expr() ) { return $self->do_err("expression expected in like_condition"); }
      $condition->{$self->{'key'}->{'like_condition'}}->{$self->{'key'}->{'left_expr'}}->{$self->{'key'}->{'expr'}} = $expr;
      $condition->{$self->{'key'}->{'like_condition'}}->{$self->{'key'}->{'right_expr'}}->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
      $condition->{$self->{'key'}->{'like_condition'}}->{'-attributes'}->{$self->{'key'}->{'operator'}} = $operator;
      if ( $self->{'sql'} =~ s/^ESCAPE//is ) {
        $self->whitespace();
        unless ( $self->expr() ) { return $self->do_err("expression expected in escape-part of like_condition"); }
        $condition->{$self->{'key'}->{'like_condition'}}->{$self->{'key'}->{'escape'}}->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
        delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
      }
    }
    else {
      return $self->do_err("like_condition, comparison_condition, membership_condition, range_condition or is_of_type_condition expected");
    }
  }


  unless ( $negated_compound_condition ) {
    if ( $match == 1 && $self->{'sql'} =~ s/^(AND|OR)(\s|\()/$2/is ) { # must not match with 'ORDER'!
      my $operator = lc $1;
      $self->whitespace();
      # compound with AND and OR:
      unless ( $self->condition() ) { return $self->do_err("condition expected"); }
      # retructuring
      my $left_cond = $condition;
      $condition = undef;
      $condition->{$self->{'key'}->{'compound_condition'}}->{'-attributes'}->{$self->{'key'}->{'operator'}} = $operator;
      $condition->{$self->{'key'}->{'compound_condition'}}->{$self->{'key'}->{'left_condition'}}->{$self->{'key'}->{'condition'}} = $left_cond;
      $condition->{$self->{'key'}->{'compound_condition'}}->{$self->{'key'}->{'right_condition'}}->{$self->{'key'}->{'condition'}} = $self->{'tmp'}->{$self->{'key'}->{'condition'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'condition'}};
    }
  }

  $self->{'tmp'}->{$self->{'key'}->{'condition'}} = $condition;
  return $match;
}

sub is_of_type_cond {
  my $self = shift;
  my $condition;
  if ( $self->{'sql'} =~ s/^TYPE//is ) {
    $self->whitespace();
  }
  unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
  $self->whitespace();
  my $elements;
  my $only = 0;
  my $identifier1;
  my $identifier2;
  do {
    $only = 0;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^ONLY//is ) {
      $self->whitespace();
      $only = 1;
    }
    if ( $identifier1 = $self->identifier() ) {
      if ( $self->{'sql'} =~ s/^\.//is ) {
        $self->whitespace();
        if ( $identifier2 = $self->identifier() ) {
          ${$elements}[@{$elements}]->{'-attributes'}->{$self->{'key'}->{'schema'}} = $identifier1;
          ${$elements}[-1]->{'-attributes'}->{$self->{'key'}->{'type'}} = $identifier2;
          if ( $only == 1 ) {
            ${$elements}[-1]->{'-attributes'}->{$self->{'key'}->{'only'}} = 'only';
          }
        }
        else {
          return $self->do_err("identifier expected");
        }
      }
      else {
        ${$elements}[@{$elements}]->{'-attributes'}->{$self->{'key'}->{'type'}} = $identifier1;
        if ( $only == 1 ) {
          ${$elements}[-1]->{'-attributes'}->{$self->{'key'}->{'only'}} = 'only';
        }
      }
    }
    else {
      return $self->do_err("identifier expected");
    }
  } while ( $self->{'sql'} =~ s/^,//is );
  unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in is_of_type_condition"); }
  $self->whitespace();

  $self->{'tmp'}->{$self->{'key'}->{'is_of_type_condition'}}->{$self->{'key'}->{'type_element'}} = $elements;
  if ( $self->{'tmp'}->{'negation'} == 1 ) {
    $self->{'tmp'}->{$self->{'key'}->{'is_of_type_condition'}}->{'-attributes'}->{$self->{'key'}->{'not'}} = 'not';
    delete $self->{'tmp'}->{'negation'};
  }
  return 1;
}

sub range_cond {
  my $self = shift;

  unless ( $self->expr() ) { return $self->do_err("expression expected in range_condition"); }
  $self->{'tmp'}->{$self->{'key'}->{'range_condition'}}->{$self->{'key'}->{'lower_bound_expr'}}->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
  delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};;
  unless ( $self->{'sql'} =~ s/^AND//is ) { return $self->do_err("keyword 'AND' expected"); }
  $self->whitespace();
  unless ( $self->expr() ) { return $self->do_err("expression expected in range_condition"); }
  $self->{'tmp'}->{$self->{'key'}->{'range_condition'}}->{$self->{'key'}->{'upper_bound_expr'}}->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
  delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};;
  if ( $self->{'tmp'}->{'negation'} == 1 ) {
    $self->{'tmp'}->{$self->{'key'}->{'range_condition'}}->{'-attributes'}->{$self->{'key'}->{'not'}} = 'not';
    delete $self->{'tmp'}->{'negation'};
  }
  return 1;
}

sub simple_membership_cond {
  my $self = shift;
  my $condition;

  unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
  $self->whitespace();

  if ( $self->subquery() ) {
    $self->{'tmp'}->{$self->{'key'}->{'simple_membership_condition'}}->{$self->{'key'}->{'subquery'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
  }
  elsif ( $self->expr_list() ) {
    $self->{'tmp'}->{$self->{'key'}->{'simple_membership_condition'}}->{$self->{'key'}->{'expr_list'}} = $self->{'tmp'}->{$self->{'key'}->{'expr_list'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'expr_list'}};
  }
  else {
    return $self->do_err("subquery or expr_list expected");
  }
  unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in simple_membership_condition"); }
  $self->whitespace();

  if ( exists $self->{'tmp'}->{'negation'} && $self->{'tmp'}->{'negation'} == 1 ) {
    $self->{'tmp'}->{$self->{'key'}->{'simple_membership_condition'}}->{'-attributes'}->{$self->{'key'}->{'not'}} = 'not';
    delete $self->{'tmp'}->{'negation'};
  }

  return 1;
}


######################################################################
#
# END CONDITION
#
######################################################################




######################################################################
#
# *** EXPRESSION ***
#
######################################################################


sub expr_list {
  my $self = shift;
  my $match = 0;
  my $exprs;

  if ( $self->expr() ) {
    $match = 1;
    ${$exprs}[@{$exprs}] = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
    while ( $self->{'sql'} =~ s/^,//is ) {
      $self->whitespace();
      unless ( $self->expr() ) {  return $self->do_err("expression expected in expr_list"); }
      ${$exprs}[@{$exprs}] = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
    }
    $self->{'tmp'}->{$self->{'key'}->{'expr_list'}}->{$self->{'key'}->{'expr'}} = $exprs;
  }
  return $match;
}

sub expr {
  my $self = shift;
  my $match = 0;
  my $expression;
  my $operator;
  if ( $self->function_expr() ) {
    # function_expr
    $match = 1;
    $expression->{$self->{'key'}->{'function_expr'}} = $self->{'tmp'}->{$self->{'key'}->{'function_expr'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'function_expr'}};
  }
  elsif ( $self->subquery() ) {
    # scalar_subquery_expr
    $match = 1;
    $expression->{$self->{'key'}->{'scalar_subquery_expr'}}->{$self->{'key'}->{'subquery'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
  }
  elsif ( $self->simple_expr() ) {
    $match = 1;
    $expression->{$self->{'key'}->{'simple_expr'}} = $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}};
  }
  elsif ( $self->{'sql'} =~ s/^\(//is ) {
    $self->whitespace();
    $match = 1;
    # compound_expr
    unless ( $self->expr() ) { return $self->do_err("expression expected in compound expression"); }
    unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in compound expr"); }
    $self->whitespace();
    $expression->{$self->{'key'}->{'compound_expr'}}->{'-attributes'}->{$self->{'key'}->{'operator'}} = 'brackets';
    $expression->{$self->{'key'}->{'compound_expr'}}->{$self->{'key'}->{'right_expr'}}->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
  }
  elsif ( $self->{'sql'} =~ s/^(\+|-|PRIOR)//is ) {
    $operator = lc $1;
    $match = 1;
    $self->whitespace();
    # compound_expr
    unless ( $self->expr() ) { return $self->do_err("expression expected in compound_expr"); }
    $expression->{$self->{'key'}->{'compound_expr'}}->{'-attributes'}->{$self->{'key'}->{'operator'}} = $operator;
    $expression->{$self->{'key'}->{'compound_expr'}}->{$self->{'key'}->{'right_expr'}}->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
  }
  elsif ( $self->{'sql'} =~ s/^CURSOR//is ) {
    $self->whitespace();
    $match = 1;
    # cursor_expr
    unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
    $self->whitespace();
    unless ( $self->subquery() ) {  return $self->do_err("opening bracket expected"); }
    $expression->{$self->{'key'}->{'cursor_expr'}}->{$self->{'key'}->{'subquery'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
    unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in cursor expr"); }
    $self->whitespace();
  }
  elsif ( $self->{'sql'} =~ s/^CASE//is ) {
    $match = 1;
    $self->whitespace();
    return $self->do_err("case expression not implemented in parser");
  }
  if ( $match == 1 ) {
    # datatime_expr
    # interval_expr
    if ( $self->{'sql'} =~ s/^DAY//is ) {
      # interval_expr
      $self->whitespace();
      return $self->do_err("interval expression not implemented in parser");
    }
    elsif ( $self->{'sql'} =~ s/^YEAR//is ) {
      # interval_expr
      $self->whitespace();
      return $self->do_err("interval expression not implemented in parser");
    }
    elsif ( $self->{'sql'} =~ s/^AT//is ) {
      # datatime_expr
      $self->whitespace();
      return $self->do_err("datetime expression not implemented in parser");
    }
    if (  $self->{'sql'} =~ s/^\s*(\*|\/|\+|-|\|\|)//is ) {
      $operator = $1;
      $self->whitespace();
      # compound_expr
      unless ( $self->expr() ) { return $self->do_err("expression expected in compound_expr"); }
      # restructuring
      my $tmp_expr = $expression;
      $expression = undef;
      $expression->{$self->{'key'}->{'compound_expr'}}->{'-attributes'}->{$self->{'key'}->{'operator'}} = $operator;
      $expression->{$self->{'key'}->{'compound_expr'}}->{$self->{'key'}->{'left_expr'}}->{$self->{'key'}->{'expr'}} = $tmp_expr;
      $expression->{$self->{'key'}->{'compound_expr'}}->{$self->{'key'}->{'right_expr'}}->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
    }
    $self->{'tmp'}->{$self->{'key'}->{'expr'}} = $expression;
  }
  return $match;
}

sub simple_expr {
  my $self = shift;
  my $match = 0;
  my $identifier1;
  my $identifier2;
  my $identifier3;
  if ( $self->number() ) {
    $match = 1;
    $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'number'}}->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
    $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'number'}}->{'-empty'} = 1;
  }
  elsif ( $self->{'sql'} =~ s/^'(.*?)'//is ) {
    $match = 1;
    $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'text'}}->{'-attributes'}->{$self->{'key'}->{'value'}} = $1;
    $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'text'}}->{'-empty'} = 1;
    $self->whitespace();
  }
  elsif ( $identifier1 = $self->identifier() ) {
    $match = 1;
    if ( $self->{'sql'} =~ s/^\.//is  ) {
      $self->whitespace();
      if ( $identifier2 = $self->identifier() ) {
        if ( lc $identifier2 eq 'currval' ) {
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'sequence'}}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier1;
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'sequence'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = 'currval';
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'sequence'}}->{'-empty'} = 1;
        }
        elsif ( lc $identifier2 eq 'nextval' ) {
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'sequence'}}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier1;
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'sequence'}}->{'-attributes'}->{$self->{'key'}->{'type'}} = 'nextval';
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'sequence'}}->{'-empty'} = 1;
        }
        elsif ( $self->{'sql'} =~ s/^\.//is  ) {
          $self->whitespace();
          unless ( $identifier3 = $self->identifier() ) { return $self->do_err("identifier expected"); }
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'schema'}} = $identifier1;
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'table_view'}} = $identifier2;
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'column'}} = $identifier3;
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'id'}} = "object_".$self->{'object_id_counter'};
          $self->{'object_id_counter'}++;
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'object'}}->{'-empty'} = 1;
        }
        else {
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'table_view'}} = $identifier1;
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'column'}} = $identifier2;
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'id'}} = "object_".$self->{'object_id_counter'};
          $self->{'object_id_counter'}++;
          $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'object'}}->{'-empty'} = 1;
        }
      }
      else {
        return $self->do_err("identifier expected");
      }
    }
    else {
      $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'column'}} = $identifier1;
      $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'object'}}->{'-attributes'}->{$self->{'key'}->{'id'}} = "object_".$self->{'object_id_counter'};
      $self->{'object_id_counter'}++;
      $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'object'}}->{'-empty'} = 1;
    }
  }
  elsif ( $self->{'sql'} =~ s/^ROWNUM//is ) {
    $match = 1;
    $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'rownum'}}->{'-empty'} = 1;
    $self->whitespace();
  }
  elsif ( $self->{'sql'} =~ s/^NULL//is ) {
    $match = 1;
    $self->{'tmp'}->{$self->{'key'}->{'simple_expr'}}->{$self->{'key'}->{'null'}}->{'-empty'} = 1;
    $self->whitespace();
  }
  return $match;
}

sub function_expr {
  my $self = shift;
  my $match = 0;
  my $expr;
  # first match is for functions without brackets
  if ( $self->{'sql'} =~ s/^SYSDATE//is ) {
    $match = 1;
    $expr->{'-attributes'}->{$self->{'key'}->{'name'}} = $&;
    $expr->{'-empty'} = 1;
    $self->whitespace();
  }
  elsif ( $self->{'sql'} =~ s/^(([a-zA-Z]\w*|_)+)//is ) {
    $match = 1;
    my $name = $1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^\(//is ) {
      $self->whitespace();
      my $arguments;
      do {
         $self->whitespace();
         unless ( $self->expr() ) { return $self->do_err("expression expected in arguments of function_expr"); };
         ${$arguments}[@{$arguments}]->{$self->{'key'}->{'expr'}} = $self->{'tmp'}->{$self->{'key'}->{'expr'}};
         delete $self->{'tmp'}->{$self->{'key'}->{'expr'}};
      } while ( $self->{'sql'} =~ s/^,//is );
      $self->whitespace();
      $expr->{$self->{'key'}->{'function_arg'}} = $arguments;
      $expr->{'-attributes'}->{$self->{'key'}->{'name'}} = $name;
      unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in function_expression"); }
      $self->whitespace();
    }
    else {
      # oops this is probably no function... better clean the mess up and hope no one noticed...
      $self->{'sql'} = $name." ".$self->{'sql'};
      return 0;
    }
  }
  $self->whitespace();
  $self->{'tmp'}->{$self->{'key'}->{'function_expr'}} = $expr;
  return $match;
}


######################################################################
#
# END EXPRESSION
#
######################################################################


# ony a stub
sub insert {
  my $self = shift;
  warn "INSERT command not implemented, trying to skip!\n";
  if ( ($self->{'statement_end'} ne '') && $self->{'sql'} =~ s/^INSERT.*?$self->{'statement_end'}//iso ) {
    my $match = $&; # matched string
    while ( $match =~ s/\n// ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
  }
}

sub create_trigger {
  my $self = shift;
  # not implemented
  warn "CREATE TRIGGER command not implemented, trying to skip!\n";
  if ( ($self->{'statement_end'} ne '') && $self->{'sql'} =~ s/^TRIGGER.*?END$self->{'statement_end'}//iso ) {
    my $match = $&; # matched string
    while ( $match =~ s/\n// ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
    $self->{'sql'} =~ s/^\///is;
    $self->whitespace();
  }
}

sub alter_trigger {
  my $self = shift;
  warn "ALTER TRIGGER command not implemented, trying to skip!\n";
  if ( ($self->{'statement_end'} ne '') && $self->{'sql'} =~ s/^.*?$self->{'statement_end'}//iso ) {
    my $match = $&; # matched string
    while ( $match =~ s/\n// ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
  }
}

sub create_sequence {
  my $self = shift;
  warn "CREATE SEQUENCE command not implemented, trying to skip!\n";
  if ( ($self->{'statement_end'} ne '') && $self->{'sql'} =~ s/^SEQUENCE .*?$self->{'statement_end'}//iso ) {
    my $match = $&; # matched string
    while ( $match =~ s/\n// ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
  }
}

# ony a stub
sub drop {
  my $self = shift;
  warn "DROP command not implemented, trying to skip!\n";
  if ( ($self->{'statement_end'} ne '') && $self->{'sql'} =~ s/^DROP.*?$self->{'statement_end'}//iso ) {
    my $match = $&; # matched string
    while ( $match =~ s/\n// ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
  }
}

# ony a stub
sub alter {
  my $self = shift;
  warn "ALTER command not implemented, trying to skip!\n";
  if ( ($self->{'statement_end'} ne '') && $self->{'sql'} =~ s/^ALTER.*?$self->{'statement_end'}//iso ) {
    my $match = $&; # matched string
    while ( $match =~ s/\n// ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
  }
}

# ony a stub
sub lock {
  my $self = shift;
  if ( ($self->{'statement_end'} ne '') && $self->{'sql'} =~ s/^LOCK.*?$self->{'statement_end'}//is ) {
    warn "LOCK command not implemented, trying to skip!\n";
    my $match = $&; # matched string
    while ( $match =~ s/\n// ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
  }
}

# ony a stub
sub unlock {
  my $self = shift;
  if ( ($self->{'statement_end'} ne '') && $self->{'sql'} =~ s/^UNLOCK.*?$self->{'statement_end'}//is ) {
    warn "UNLOCK command not implemented, trying to skip!\n";
    my $match = $&; # matched string
    while ( $match =~ s/\n// ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
  }
}


######################################################################
#
# *** GENERAL FUNCTIONS ***
#
######################################################################


sub datatype {
  my $self = shift;
  my $type = "";
  my $quant = "";
  if ( $self->{'sql'} =~ m/^(NOT|NULL)/is ) { return undef; } # no type info suplied
  if ( $self->{'sql'} =~ s/^(\w+)//is ) {
    $type = uc $1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^\(//is ) {
       $self->whitespace();
       unless ( $self->number() ) {
         return $self->do_err("number expected");
       }
       $quant = "(".$self->{'tmp'}->{$self->{'key'}->{'num'}};
       delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
       if ( $self->{'sql'} =~ s/^,//is ) {
         $self->whitespace();
         $quant .= ",";
         unless ( $self->number() ) {
           return $self->do_err("number expected");
         }
         $quant .= $self->{'tmp'}->{$self->{'key'}->{'num'}};
         delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
       }
       unless ( $self->{'sql'} =~ s/^\)//is ) {
         return $self->do_err("closing bracket for datatype expected");
       }
       $self->whitespace();
       $quant .= ")";
    }
    return $type.$quant; # return datatype in uppercase letters
  }
  else {
   return undef;
  }
}

sub column_list {
  my $self = shift;
  my $identifier;
  do {
    $self->whitespace();
    unless ( $identifier = $self->identifier() ) { return $self->do_err("identifier expected"); }
    ${$self->{'tmp'}->{$self->{'key'}->{'column_list'}}}[@{$self->{'tmp'}->{$self->{'key'}->{'column_list'}}}]->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    ${$self->{'tmp'}->{$self->{'key'}->{'column_list'}}}[-1]->{'-empty'} = 1;
  } while ( $self->{'sql'} =~ s/^,//is );
  $self->whitespace();
}


sub whitespace {
  my $self = shift;
  my $match;
  while ( $self->{'sql'} =~ s/^\s+//is ||
          $self->{'sql'} =~ s/^--(?:.*)\n//i ||
          $self->{'sql'} =~ s/^\/\*(?:.*?)\*\///is ) {
     $match = $&; # matched string
     while ( $match =~ s/\n//is ) {
       $self->{'linecount'}++;
     }
  }
}

sub identifier {
  my $self = shift;
  if ( $self->{'sql'} =~ m/^$self->{'RESERVED_WORDS_REGEXP'}(\s|;|\)|\(|\.)/iso ) {
    return undef;
  }
  if ( $self->{'sql'} =~ s/^[a-z][a-z_0-9]*//is ) {
    my $val = $&;
    $self->whitespace();
    return uc $val;
  }
  elsif ( $self->{'sql'} =~ s/^"?((?:&&)?[a-zA-Z][a-zA-Z_0-9]*)"?//is ) {
    my $val = $1;
    $self->whitespace();
    return uc $val;
  }
  else {
   return undef;
  }
}

sub number {
  my $self = shift;
  if ( $self->{'sql'} =~ s/^([+-]?(\d+\.\d+|\d+\.|\.\d+|\d+)([eE][+-]?\d+)?)//is ) {
    $self->{'tmp'}->{$self->{'key'}->{'num'}} = $1;
    $self->whitespace();
    return 1;
  }
  else {
   return 0;
  }
}


######################################################################
#
# *** UTILITY FUNCTIONS ***
#
######################################################################


sub occurence_count() {
  my $self = shift;
  if ( $self->{'sql'} =~ s/^(\s*)\/\* occurence:(\d+) \*\///s ) {
    my $whitespace = $1;
    $self->{'occurence_count'} = $2;
    while ( $whitespace =~ s/\n//is ) {
       $self->{'linecount'}++;
    }
  }
  else {
    $self->{'occurence_count'} = 0;
  }
}

sub expect {
  my $self = shift;
  my $pattern = shift;
  my $raise_error;
  unless ( $raise_error = shift ) { $raise_error = 0; }
  if ( $self->{'sql'} =~ s/^$pattern//is ) {
    my $match = $&; # matched string
    while ( $match =~ s/\n//is ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
    return 1;
  }
  else {
    return 0;
  }
}


######################################################################
#
# *** CLASS INTERFACE ***
#
######################################################################


sub tree() {
  my $self = shift;
  $self->{'tree'}->{'sqldata'} = $self->{'sqldata'};
  return $self->{'tree'};
}

sub varlist() {
  my $self = shift;
  return $self->{'varlist'};
}

sub do_err {
    my $self = shift;
    my $msg = shift;
    my $error = "Error: $msg in line ".$self->{'linecount'}." near: ".substr($self->{'sql'}, 0, 70).".\n";
    warn $error;
    $self->recover();
    return 0;
}

# called when a parse error occured
sub recover {
  my $self = shift;
  if ( $self->{'sql'} =~ s/.*?[;]//s ) {
     my $match = $&; # matched string
     while ( $match =~ s/\n//is ) {
       $self->{'linecount'}++;
     }
  }
  else {
    die "Could not recover from previous parse errors. Aborting.\n";
  }
}


1;
