#!/usr/bin/perl

####################################################################
#
# Small shell script for grouping identical lines in a file full
# of SELECT statements and adding an "occurence" comment for the
# SquashML parser.
#
####################################################################

if ( @ARGV == 0 ) {
  help();
  exit(0);
}

open INPUT, "<$ARGV[0]";

$/=\0;

$lines = <INPUT>;

@lines = split(/;/,$lines);

@sorted_lines = sort @lines;

$current;
$count = 1;
@result;

foreach $line (@sorted_lines) {
  if ( $current eq undef) {
    $current = $line;
    next;
  }

  if ( $current eq $line ) {
    $count++;
  }
  else {
    if ($line ne '') {
       push @result, "/* occurence:$count */ $current;\n";
       $count = 1;
       $current = $line;
    }
  }  
}

print @result;



sub help {
  print "Usage: group_selects.pl <sqlfile>.\n\n".
    "WARNING: this script expects every statement to be on a single line!\n".
    "The output of the program is written to stdout.\n";
  exit(0);
}
