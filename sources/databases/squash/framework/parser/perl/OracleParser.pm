####################################################################
package OracleParser;
####################################################################
#
# Parses Oracle SQL code into SquashML.
#
####################################################################

use strict;
use warnings;
use vars qw(@ISA $VERSION);

use SquashMLParser;
@ISA = qw(SquashMLParser);

$VERSION = '1.0';


my $key_commit_statement = 'commit_statement';
my $key_physical_properties = 'physical_properties';
my $key_table_properties = 'table_properties';
my $key_segment_attributes_clause = 'segment_attributes_clause';
my $key_segment_attributes = 'segment_attributes';
my $key_using_index = 'using_index';
my $key_with_rowid = 'with_rowid';
my $key_inline_ref_constraint_def = 'inline_ref_constraint_def';
my $key_dbms_cluster_index_clause = 'dbms_cluster_index_clause';
my $key_maxextents = 'maxextents';
my $key_optimal = 'optimal';
my $key_buffer_pool = 'buffer_pool';
my $key_freelist_groups = 'freelist_groups';
my $key_cluster_clause = 'cluster_clause';
my $key_parallel_clause = 'parallel_clause';
my $key_rebuild_element = 'rebuild_element';
my $key_physical_attributes_clause = 'physical_attributes_clause';
my $key_key_compression = 'key_compression';
my $key_logging_clause = 'logging_clause';
my $key_online = 'online';
my $key_rebuild_clause = 'rebuild_clause';
my $key_move_table_clause = 'move_table_clause';
my $key_data_segment_compression = 'data_segment_compression';
my $key_lob_storage_clause = 'lob_storage_clause';
my $key_tablespace = 'tablespace';
my $key_on_range_or_list_partitioned_table = 'on_range_or_list_partitioned_table';
my $key_local_range_or_list_partition = 'local_range_or_list_partition';
my $key_on_hash_partitioned_table = 'on_hash_partitioned_table';
my $key_local_hash_partition = 'local_hash_partition';
my $key_global_partitioning_clause = 'global_partitioning_clause';
my $key_partition = 'partition';
my $key_global_partitioned_index = 'global_partitioned_index';
my $key_local_partitioned_index = 'local_partitioned_index';
my $key_cluster = 'cluster';
my $key_logging = 'logging';
my $key_lob_column_properties = 'lob_column_properties';
my $key_lob_partition_storage = 'lob_partition_storage';
my $key_lob_item = 'lob_item';
my $key_lob_parameter = 'lob_parameter';
my $key_lob_parameters = 'lob_parameters';
my $key_lob_segname = 'lob_segname';
my $key_table_partitioning_clauses = 'table_partitioning_clauses';
my $key_row_movement_clause = 'row_movement_clause';
my $key_enable_disable_clause = 'enable_disable_clause';
my $key_retention = 'retention';
my $key_lob_cache = 'lob_cache';
my $key_cache_reads = 'cache_reads';
my $key_storage_clause = 'storage_clause';
my $key_range_partitioning = 'range_partitioning';
my $key_range_partition = 'range_partition';
my $key_range_values_clause = 'range_values_clause';
my $key_table_partition_description = 'table_partition_description';
my $key_range_value = 'range_value';
my $key_overflow = 'overflow';
my $key_hash_partitioning = 'hash_partitioning';
my $key_individual_hash_partitions = 'individual_hash_partitions';
my $key_hash_partitions_by_quantity = 'hash_partitions_by_quantity';
my $key_individual_hash_partition = 'individual_hash_partition';
my $key_named_individual_hash_partition = 'named_individual_hash_partition';
my $key_partitioning_storage_clause = 'partitioning_storage_clause';
my $key_partitioning_storage = 'partitioning_storage';
my $key_partitioning_storage_overflow = 'partitioning_storage_overflow';
my $key_lob_partitioning_storage = 'lob_partitioning_storage';
my $key_quantity = 'quantity';
my $key_quantity_store_in = 'quantity_store_in';
my $key_hash_partition_overflow = 'hash_partition_overflow';
my $key_list_partitioning_element = 'list_partitioning_element';
my $key_list_partitioning = 'list_partitioning';
my $key_list_values_clause = 'list_values_clause';
my $key_list_value = 'list_value';
my $key_using_index_element = 'using_index_element';
my $key_using_index_internal = 'using_index_internal';
my $key_local_partitioned_entry = 'local_partitioned_entry';
my $key_on_comp_partitioned_table = 'on_comp_partitioned_table';
my $key_index_sorting = 'index_sorting';
my $key_index_attributes = 'index_attributes';
my $key_statistics = 'statistics';
my $key_nowait = 'nowait';
my $key_wait = 'wait';
my $key_index_hint = 'index_hint';
my $key_index_name = 'index_name';
my $key_column_properties = 'column_properties';
my $key_cache = 'cache';
my $key_nocache = 'nocache';
my $key_movement = 'movement';
my $key_default = 'default';
my $key_validate = 'validate';
my $key_unique_enable_disable_clause = 'unique_enable_disable_clause';
my $key_primary_key_enable_disable_clause = 'primary_key_enable_disable_clause';
my $key_nosort = 'nosort';
my $key_local = 'local';
my $key_index_element = 'index_element';
my $key_data = 'data';
my $key_store_in = 'store_in';
my $key_compute = 'compute';
my $key_constraint = 'constraint';
my $key_alter_index = 'alter_index';
my $key_alter_table = 'alter_table';


sub new {
  my $this = shift;
  my $class = ref($this) || $this;
  my $self = {};
  bless ($self, $class);
  $self->initialize();
  $self->{'tree'}->{'-attributes'}->{'sqldialect'} = 'oracle';
  $self->{'tree'}->{'-attributes'}->{'version'} = $VERSION;
  return $self;
}

sub initialize {
  my $self = shift;
  $self->SquashMLParser::initialize();
  $self->{'statement_end'} = ';';
  @{$self->{'RESERVED_WORDS'}} = qw(ACCESS ADD ALL ALTER AND ANY AS 
    ASC AUDIT BETWEEN BY CHAR CHECK CLUSTER COLUMN COMMENT COMPRESS 
    CONNECT CREATE CURRENT DATE DECIMAL DEFAULT DELETE DESC DISTINCT 
    DROP ELSE EXCLUSIVE EXISTS FILE FLOAT FOR FROM GRANT GROUP 
    HAVING IDENTIFIED IMMEDIATE IN INCREMENT INDEX INITIAL INSERT 
    INTEGER INTERSECT INTO IS LEVEL LIKE LOCK LONG MAXEXTENTS MINUS 
    MLSLABEL MODE MODIFY NOAUDIT NOCOMPRESS NOT NOWAIT NULL NUMBER 
    OF OFFLINE ON ONLINE OPTION OR ORDER PCTFREE PRIOR PRIVILEGES 
    PUBLIC RAW RENAME RESOURCE REVOKE ROW ROWID ROWNUM ROWS SELECT 
    SESSION SET SHARE SIZE SMALLINT START SUCCESSFUL SYNONYM SYSDATE 
    TABLE THEN TO TRIGGER UID UNION UNIQUE UPDATE USER VALIDATE 
    VALUES VARCHAR VARCHAR2 VIEW WHENEVER WHERE WITH);
  $self->{'key'}->{'dbms_create_table'} = 'oracle_create_table';
  $self->{'key'}->{'dbms_constraint_state'} = 'oracle_constraint_state';
  $self->{'key'}->{'dbms_inline_ref_constraint'} = 'oracle_inline_ref_constraint';
  $self->{'key'}->{'dbms_table_index_clause'} = 'oracle_table_index_clause';
  $self->{'key'}->{'dbms_optimizer_hint'} = 'oracle_optimizer_hint';
}


sub dbms_statement {
  my $self = shift;
  if ( $self->{'sql'} =~ m/^DEFINE/is ) {
    $self->sqlplus_define();
  }
  elsif ( $self->{'sql'} =~ m/^ALTER/is ) {
    $self->alter();
  }
  elsif ( $self->{'sql'} =~ m/^COMMIT/is ) {
    $self->commit();
  }
  elsif ( $self->{'sql'} =~ m/^SPO/is ) {
    $self->sqlplus_spool();
  }
  elsif ( $self->{'sql'} =~ m/^SET/is ) {
    $self->sqlplus_set();
  }
  elsif ( $self->{'sql'} =~ m/^SHO/is ) {
    $self->sqlplus_show();
  }
  else {
    return 0;
  }
  return 1;
}

# handles sqlplus substitution variables
# like "define v_ts_tables = USERS" (note: no semicolon at end!)
sub sqlplus_define {
  my $self = shift;
  $self->{'sql'} =~ s/^DEFINE//is;
  $self->whitespace();
  unless ( $self->{'sql'} =~ s/^("|')?(\w+)("|')?//is ) { return $self->do_err("Syntax error in 'define' statement."); }
  my $variable = $2;
  $self->whitespace();
  unless ( $self->{'sql'} =~ s/^=//is ) { return $self->do_err("Syntax error in 'define' statement."); }
  $self->whitespace();
  unless ( $self->{'sql'} =~ s/^("|')?(\w+)("|')?//is ) { return $self->do_err("Syntax error in 'define' statement."); }
  my $value = $2;
  $self->whitespace();
  $self->{'varlist'}->{uc $variable} = $value;
  $self->{'sql'} =~ s/^$self->{'statement_end'}//iso;
  $self->whitespace();
}

# spool is ignored by the parser
# Reference: SPO[OL] [file_name[.ext]|OFF|OUT]
sub sqlplus_spool {
  my $self = shift;
  $self->{'sql'} =~ s/^SPO(OL)?//is;
  $self->whitespace();
  my $identifier;
  $self->{'sql'} =~ s/^.*?(\s)/$1/is;
  $self->whitespace();
  $self->{'sql'} =~ s/^(OFF|OUT)//is;
  $self->whitespace();
  $self->{'sql'} =~ s/^$self->{'statement_end'}//iso;
}

# currently not all SET commands are understood!
sub sqlplus_set {
  my $self = shift;
  $self->{'sql'} =~ s/^SET//is;
  $self->whitespace();
  my $identifier;
  $self->{'sql'} =~ s/^.*?(\s)/$1/is;
  $self->whitespace();
  $self->{'sql'} =~ s/^(ON|OFF)//is;
  $self->whitespace();
  $self->{'sql'} =~ s/^$self->{'statement_end'}//iso;
}

# currently not all SHOW commands are understood!
sub sqlplus_show {
  my $self = shift;
  $self->{'sql'} =~ s/^SHO(W)?//is;
  $self->whitespace();
  $self->{'sql'} =~ s/^\w+//is;
  $self->whitespace();
  $self->{'sql'} =~ s/^$self->{'statement_end'}//iso;
}


######################################################################
#
# CREATE TABLE DBMS EXTENSION
#
######################################################################


sub dbms_create_table {
  my $self = shift;
  my $match = 0;
  if ( $self->commit_statement() ) {
    $match = 1;
    $self->{'tmp'}->{$self->{'key'}->{'dbms_create_table'}}->{$key_commit_statement} = $self->{'tmp'}->{$key_commit_statement};
    delete $self->{'tmp'}->{$key_commit_statement};
  }
  if ( $self->physical_properties() ) {
    $match = 1;
    $self->{'tmp'}->{$self->{'key'}->{'dbms_create_table'}}->{$key_physical_properties} = $self->{'tmp'}->{$key_physical_properties};
    delete $self->{'tmp'}->{$key_physical_properties};
  }
  if ( $self->table_properties() ) {
    $match = 1;
    $self->{'tmp'}->{$self->{'key'}->{'dbms_create_table'}}->{$key_table_properties} = $self->{'tmp'}->{$key_table_properties};
    delete $self->{'tmp'}->{$key_table_properties};
  }
  return $match;
}

sub dbms_constraint_state {
  my $self = shift;
  my $state;
  my $match = 0;
  if ( $self->using_index() ) {
    $match = 1;
    #$constraint->{$self->{'key'}->{'dbms_constraint_state'}}->{$key_using_index} = $self->{'tmp'}->{$key_using_index};
    $state->{$key_using_index} = $self->{'tmp'}->{$key_using_index};
    delete $self->{'tmp'}->{$key_using_index};
    $self->whitespace();
    $self->{'tmp'}->{$self->{'key'}->{'dbms_constraint_state'}} = $state;
  }
  return $match;
}

sub dbms_inline_ref_constraint {
  my $self = shift;
  my $match = 0;
  my $constraint;
  my $identifier;
  if ( $self->{'sql'} =~ s/^WITH//is ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^ROWID//is ) {
      $self->whitespace();
      $constraint->{$key_with_rowid}->{'-empty'} = 1;
    }
    else {
      return $self->do_err("keyword 'ROWID' expected");
    }
  }
  else {
    if ( $self->{'sql'} =~ s/^CONSTRAINT//is ) {
      $match = 1;
      $self->whitespace();
      unless ($identifier = $self->identifier()) {
        return $self->do_err("constraint_name expected");
      }
      $constraint->{$key_inline_ref_constraint_def}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    }
    if ( $self->references_clause() ) {
      $match = 1;
      $constraint->{$key_inline_ref_constraint_def}->{$self->{'key'}->{'references_clause'}} = $self->{'tmp'}->{$self->{'key'}->{'references_clause'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'references_clause'}};
      if ($self->dbms_constraint_state() ) {
        $constraint->{$self->{'key'}->{'dbms_constraint_state'}} =  $self->{'tmp'}->{$self->{'key'}->{'dbms_constraint_state'}};
        delete $self->{'tmp'}->{$self->{'key'}->{'dbms_constraint_state'}};
      }
    }
    else {
      if ( $match == 1 ) {
        return $self->do_err("constraint expected");
      }
    }
  }
  $self->{'tmp'}->{$self->{'key'}->{'dbms_inline_ref_constraint'}} = $constraint;
  return $match;
}

sub commit_statement {
  my $self = shift;
  my $match = 0;
  my $commit;
  if ( $self->{'sql'} =~ s/^ON//is )  {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^COMMIT//is )  {
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^(DELETE|PRESERVE)//is )  {
        $commit->{'-attributes'}->{$self->{'key'}->{'type'}} = lc $1;
        $self->whitespace();
        unless ( $self->{'sql'} =~ s/^ROWS//is )  {
          return $self->do_err("keyword 'ROWS' expected");
        }
        $self->whitespace();
      }
      else {
        return $self->do_err("keyword 'DELETE' or 'PRESERVE' expected");
      }
    }
    else {
      return $self->do_err("keyword 'COMMIT' expected");
    }
    $self->{'tmp'}->{$key_commit_statement} = $commit;
  }
  return $match;
}

sub physical_properties {
  my $self = shift;
  my $match = 0;
  my $properties;
  if ( $self->segment_attributes() ) {
    $match = 1;
    $properties->{$key_segment_attributes} = $self->{'tmp'}->{$key_segment_attributes};
    delete $self->{'tmp'}->{$key_segment_attributes};
  }
  elsif ( $self->{'sql'} =~ s/^CLUSTER//is ) {
    $match = 1;
    $self->whitespace();
    my $identifier;
    unless ( $identifier = $self->identifier() ) {
      return $self->do_err("identifier expected");
    }
    $properties->{$key_cluster_clause}->{'-attributes'}->{$key_cluster} = $identifier;
    unless ( $self->{'sql'} =~ s/^\(//is ) {
      return $self->do_err("opening bracket expected");
    }
    $self->whitespace();
    $self->column_list();
    $properties->{$key_cluster_clause}->{$self->{'key'}->{'column'}} = $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
    unless ( $self->{'sql'} =~ s/^\)//is ) {
      return $self->do_err("closing bracket expected in physical properties");
    }
    $self->whitespace();
  }
  $self->{'tmp'}->{$key_physical_properties} = $properties;
  return $match;
}

sub table_properties {
  my $self = shift;
  my $properties;
  my $match = 0;
  if ( $self->column_properties() ) {
    $match = 1;
    $properties->{$key_column_properties} = $self->{'tmp'}->{$key_column_properties};
    delete $self->{'tmp'}->{$key_column_properties};
  }
  if ( $self->table_partitioning_clauses() ) {
    $match = 1;
    $properties->{$key_table_partitioning_clauses} = $self->{'tmp'}->{$key_table_partitioning_clauses};
    delete $self->{'tmp'}->{$key_table_partitioning_clauses};
  }
  if ( $self->row_movement_clause() ) {
    $match = 1;
    $properties->{$key_row_movement_clause} = $self->{'tmp'}->{$key_row_movement_clause};
    delete $self->{'tmp'}->{$key_row_movement_clause};
  }
  if ($self->{'sql'} =~ s/^(CACHE|NOCACHE)//is ) {
    $properties->{lc $1}->{'-empty'} = 1;
    $match = 1;
    $self->whitespace();
  }
  if ($self->{'sql'} =~ s/^(ROWDEPENDENCIES|NOROWDEPENDENCIES)//is ) {
    $properties->{lc $1}->{'-empty'} = 1;
    $match = 1;
    $self->whitespace();
  }
  if ($self->{'sql'} =~ s/^(MONITORING|NOMONITORING)//is ) {
    $properties->{lc $1}->{'-empty'} = 1;
    $match = 1;
    $self->whitespace();
  }
  if ( $self->parallel_clause() ) {
    $match = 1;
    $properties->{$key_parallel_clause} = $self->{'tmp'}->{$key_parallel_clause};
    delete $self->{'tmp'}->{$key_parallel_clause};
    $self->whitespace();
  }
  if ( $self->enable_disable_clause() ) {
    $match = 1;
    $properties->{$key_enable_disable_clause} = $self->{'tmp'}->{$key_enable_disable_clause};
    delete $self->{'tmp'}->{$key_enable_disable_clause};
  }
  if ( $self->subquery() ) {
    $match = 1;
    $properties->{$self->{'key'}->{'subquery'}} = $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'subquery'}};
  }
  $self->{'tmp'}->{$key_table_properties} = $properties;
}

sub segment_attributes {
  my $self = shift;
  my $match = 0;
  my $attributes;
  if ($self->segment_attributes_clause() ) {
    $match = 1;
    $attributes->{$key_segment_attributes_clause} = $self->{'tmp'}->{$key_segment_attributes_clause};
    delete $self->{'tmp'}->{$key_segment_attributes_clause};
    if ( $self->data_segment_compression() ) {
      $attributes->{$key_data_segment_compression} = $self->{'tmp'}->{$key_data_segment_compression};
      delete $self->{'tmp'}->{$key_data_segment_compression};
    }
  }
  $self->{'tmp'}->{$key_segment_attributes} = $attributes;
  return $match;
}

sub segment_attributes_clause {
  my $self = shift;
  my $match = 0;
  my $submatch = 0;
  my $attributes;
  my $identifier;
  do {
    $submatch = 0;
    if ( $self->{'sql'} =~ s/^TABLESPACE//is ) {
      $match = 1;
      $submatch = 1;
      $self->whitespace();
      if ( $identifier = $self->identifier() ) {
        $attributes->{$key_tablespace}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
        $attributes->{$key_tablespace}->{'-empty'} = 1;
      }
      else {
        return $self->do_err("value expected in segment_attributes");
      }
    }
    elsif ( $self->physical_attributes_clause() ) {
      $match = 1;
      $submatch = 1;
      $attributes->{$key_physical_attributes_clause} = $self->{'tmp'}->{$key_physical_attributes_clause};
      delete $self->{'tmp'}->{$key_physical_attributes_clause};
    }
    elsif ( $self->logging_clause() ) {
      $match = 1;
      $submatch = 1;
      $attributes->{$key_logging_clause} = $self->{'tmp'}->{$key_logging_clause};
      delete $self->{'tmp'}->{$key_logging_clause};
    }
  } while ( $submatch == 1 );
  $self->{'tmp'}->{$key_segment_attributes_clause} = $attributes;
  return $match;
}

sub column_properties {
  my $self = shift;
  my $match = 0;
  my $properties;
  if ( $self->lob_column_properties() ) {
    $match = 1;
    $self->{'tmp'}->{$key_column_properties}->{$key_lob_column_properties} = $self->{'tmp'}->{$key_lob_column_properties};
    delete $self->{'tmp'}->{$key_lob_column_properties};
  }
  return $match;
}

sub lob_column_properties {
  my $self = shift;
  my $match = 0;
  if ( $self->lob_storage_clause() ) {
    $match = 1;
    $self->{'tmp'}->{$key_lob_column_properties}->{$key_lob_storage_clause} = $self->{'tmp'}->{$key_lob_storage_clause};
    delete $self->{'tmp'}->{$key_lob_storage_clause};
    if ( $self->lob_partition_storage() ) {
      $self->{'tmp'}->{$key_lob_column_properties}->{$key_lob_partition_storage} = $self->{'tmp'}->{$key_lob_partition_storage};
      delete $self->{'tmp'}->{$key_lob_partition_storage};
    }
  }
  return $match;
}

sub lob_storage_clause {
  my $self = shift;
  my $match = 0;
  my $lob_storage;
  if ($self->{'sql'} =~ s/^LOB//is ) {
    $match = 1;
    $self->whitespace();
    unless ($self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
    $self->whitespace();
    my $identifier;
    unless ( $identifier = $self->identifier() ) { return $self->do_err("identifier expected"); }
    if ($self->{'sql'} =~ s/^,//is ) {
      $self->whitespace();
      # first branch
      ${$lob_storage->{$key_lob_item}}[@{$lob_storage->{$key_lob_item}}]->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
      ${$lob_storage->{$key_lob_item}}[-1]->{'-empty'} = 1;
      do {
        $self->whitespace();
        unless ( $identifier = $self->identifier() ) { return $self->do_err("identifier expected"); }
        ${$lob_storage->{$key_lob_item}}[@{$lob_storage->{$key_lob_item}}]->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
        ${$lob_storage->{$key_lob_item}}[-1]->{'-empty'} = 1;
      } while ( $self->{'sql'} =~ s/^,//is );
      unless ($self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in lob_storage_clause"); }
      $self->whitespace();
      if ($self->{'sql'} =~ s/^STORE//is ) {
        $self->whitespace();
        if ($self->{'sql'} =~ s/^AS//is ) {
          $self->whitespace();
          if ( $self->{'sql'} =~ s/^\(//is ) {
            $self->whitespace();
            unless ( $self->lob_parameters() )  { return $self->do_err("lob_parameters expected"); }
            $lob_storage->{$key_lob_parameters} = $self->{'tmp'}->{$key_lob_parameters};
            delete $self->{'tmp'}->{$key_lob_parameters};
            unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in lob_storage_clause"); }
            $self->whitespace();
          }
          else {
            return $self->do_err("opening bracket expected");
          }
        }
        else {
          return $self->do_err("keyword 'AS' expected");
        }
      }
      else {
        return $self->do_err("keyword 'STORE' expected");
      }
    }
    else {
      # second branch
      $lob_storage->{$key_lob_item}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
      $lob_storage->{$key_lob_item}->{'-empty'} = 1;
      unless ($self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in lob_storage clause"); }
      $self->whitespace();
      if ($self->{'sql'} =~ s/^STORE//is ) {
        $self->whitespace();
        if ($self->{'sql'} =~ s/^AS//is ) {
          $self->whitespace();
          if ( $self->{'sql'} =~ s/^\(//is ) {
            $self->whitespace();
            unless ( $self->lob_parameters() )  { return $self->do_err("lob_parameters expected"); }
            #$lob_storage->{$key_lob_item}->{$key_lob_parameters} = $self->{'tmp'}->{$key_lob_parameters};
            $lob_storage->{$key_lob_parameters} = $self->{'tmp'}->{$key_lob_parameters};
            delete $self->{'tmp'}->{$key_lob_parameters};
            unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in lob_storage_clause"); }
            $self->whitespace();
          }
          elsif ( $identifier = $self->identifier() ) {
            $lob_storage->{$key_lob_segname}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
            if ( $self->{'sql'} =~ s/^\(//is ) {
              $self->whitespace();
              unless ( $self->lob_parameters() )  { return $self->do_err("lob_parameters expected"); }
              #$lob_storage->{$key_lob_item}->{$key_lob_parameters} = $self->{'tmp'}->{$key_lob_parameters};
              $lob_storage->{$key_lob_parameters} = $self->{'tmp'}->{$key_lob_parameters};
              delete $self->{'tmp'}->{$key_lob_parameters};
              unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in lob_storage_clause"); }
              $self->whitespace();
            }
          }
          else {
            return $self->do_err("unknown lob_storage_clause");
          }
        }
        else {
          return $self->do_err("keyword 'AS' expected");
        }
      }
      else {
        return $self->do_err("keyword 'STORE' expected");
      }
    }
  }
  $self->{'tmp'}->{$key_lob_storage_clause} = $lob_storage;
  return $match;
}

sub lob_parameters {
  my $self = shift;
  my $match = 0;
  while ( $self->lob_parameter() ) {
    $match = 1;
    ${$self->{'tmp'}->{$key_lob_parameters}->{$key_lob_parameter}}[@{$self->{'tmp'}->{$key_lob_parameters}->{$key_lob_parameter}}] = $self->{'tmp'}->{$key_lob_parameter};
    $self->{'tmp'}->{$key_lob_parameter} = undef;
  }
  return $match;
}

sub lob_parameter {
  my $self = shift;
  my $match = 0;
  my $type;
  if ( $self->{'sql'} =~ s/^TABLESPACE//is ) {
    $match = 1;
    $self->whitespace();
    my $identifier;
    unless ( $identifier = $self->identifier() ) { return $self->do_err("identifier expected"); }
    $self->{'tmp'}->{$key_lob_parameter}->{$key_tablespace}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    $self->{'tmp'}->{$key_lob_parameter}->{$key_tablespace}->{'-empty'} = 1;
  }
  elsif ( $self->{'sql'} =~ s/^(ENABLE|DISABLE)//is ) {
    $type = lc $1;
    $match = 1;
    $self->whitespace();
    if (  $self->{'sql'} =~ s/^STORAGE//is ) {
      $self->whitespace();
      if (  $self->{'sql'} =~ s/^IN//is ) {
        $self->whitespace();
        if (  $self->{'sql'} =~ s/^ROW//is ) {
          $self->whitespace();
          $self->{'tmp'}->{$key_lob_parameter}->{'lob_'.$type.'_storage'}->{'-empty'} = 1;
        }
        else {
          return $self->do_err("kexword 'ROW' expected");
        }
      }
      else {
        return $self->do_err("kexword 'IN' expected");
      }
    }
    else {
      return $self->do_err("kexword 'STORAGE' expected");
    }
  }
  elsif ( $self->{'sql'} =~ s/^(CHUNK|PCTVERSION|FREEPOOLS)//is ) {
    $type = lc $1;
    $match = 1;
    $self->whitespace();
    if ( $self->number() ) {
      $self->{'tmp'}->{$key_lob_parameter}->{$type}->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
    }
    else {
      return $self->do_err("number expected");
    }
  }
  elsif ( $self->{'sql'} =~ s/^RETENTION//is ) {
    $match = 1;
    $self->whitespace();
    $self->{'tmp'}->{$key_lob_parameter}->{$key_retention}->{'-empty'} = 1;
  }
  elsif ( $self->{'sql'} =~ s/^CACHE//is ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^READS//is ) {
      $self->whitespace();
      $self->{'tmp'}->{$key_lob_parameter}->{$key_lob_cache}->{$key_cache_reads}->{'-empty'} = 1;
      if ( $self->logging_clause() ) {
        $self->{'tmp'}->{$key_lob_parameter}->{$key_lob_cache}->{$key_logging_clause} = $self->{'tmp'}->{$key_logging_clause};
        delete $self->{'tmp'}->{$key_logging_clause};
      }
    }
    else {
      $self->{'tmp'}->{$key_lob_parameter}->{$key_lob_cache}->{$key_cache}->{'-empty'} = 1;
    }
  }
  elsif ( $self->{'sql'} =~ s/^NOCACHE//is ) {
    $match = 1;
    $self->whitespace();
    $self->{'tmp'}->{$key_lob_parameter}->{$key_lob_cache}->{$key_nocache}->{'-empty'} = 1;
    if ( $self->logging_clause() ) {
      $self->{'tmp'}->{$key_lob_parameter}->{$key_lob_cache}->{$key_logging_clause} = $self->{'tmp'}->{$key_logging_clause};
      delete $self->{'tmp'}->{$key_logging_clause};
    }
  }
  elsif ( $self->storage_clause() ) {
    $match = 1;
    #${$self->{'tmp'}->{$key_lob_parameters}->{$key_storage_clause}}[@{$self->{'tmp'}->{$key_lob_parameters}->{$key_storage_clause}}] = $self->{'tmp'}->{$key_storage_clause};
    $self->{'tmp'}->{$key_lob_parameter}->{$key_storage_clause} = $self->{'tmp'}->{$key_storage_clause};
    delete $self->{'tmp'}->{$key_storage_clause};
  }
  return $match;
}

sub lob_partition_storage {
  my $self = shift;
  my $match = 0;
  my $storage;
  while ( $self->lob_storage_clause() ) {
    $match = 1;
    ${$storage->{$key_lob_storage_clause}}[@{$storage->{$key_lob_storage_clause}}] = $self->{'tmp'}->{$key_lob_storage_clause};
    delete $self->{'tmp'}->{$key_lob_storage_clause};
  }
  $self->{'tmp'}->{$key_lob_partition_storage} = $storage;
  return $match;
}

sub table_partitioning_clauses {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^PARTITION//is  ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^BY//is  ) {
      $self->whitespace();
      if ( $self->range_or_composite_partitioning() ) {
        $match = 1;
      }
      elsif ( $self->hash_partitioning() ) {
        $match = 1;
      }
      elsif ( $self->list_partitioning() ) {
        $match = 1;
      }
      else {
        return $self->do_err("unknown partition_by statement");
      }
    }
    else {
      return $self->do_err("keyword 'BY' expected");
    }
  }
  return $match;
}

sub range_or_composite_partitioning {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^RANGE//is  ) {
    $self->whitespace();
    $match = 1;
    unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
    $self->whitespace();
    $self->column_list();
    unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in range_or_composite_partitioning"); }
    $self->whitespace();
    $self->{'tmp'}->{$key_table_partitioning_clauses}->{$key_range_partitioning}->{$self->{'key'}->{'column'}} = $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
    unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
    $self->whitespace();
    do {
      $self->whitespace();
      unless ( $self->range_partition() )  { return $self->do_err("range_partition expected"); }
      ${$self->{'tmp'}->{$key_table_partitioning_clauses}->{$key_range_partitioning}->{$key_range_partition}}[@{$self->{'tmp'}->{$key_table_partitioning_clauses}->{$key_range_partitioning}->{$key_range_partition}}] = $self->{'tmp'}->{$key_range_partition};
      delete $self->{'tmp'}->{$key_range_partition};
    } while ($self->{'sql'} =~ s/^,//is);
    unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in range_or_composite_partitioning"); }
    $self->whitespace();
  }
  return $match;
}

sub range_partition {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^PARTITION//is  ) {
    $match = 1;
    $self->whitespace();
    my $identifier;
    if ( $identifier = $self->idientifier() ) {
      $self->{'tmp'}->{$key_range_partition}->{'-attributes'}->{$key_partition} = $identifier;
    }
    unless ( $self->range_values_clause() ) { return $self->do_err("range_values_clause expected"); }
    $self->{'tmp'}->{$key_range_partition}->{$key_range_values_clause} = $self->{'tmp'}->{$key_range_values_clause};
    delete $self->{'tmp'}->{$key_range_values_clause};

    unless ( $self->table_partition_description() ) { return $self->do_err("table_partition_description expected"); }
    $self->{'tmp'}->{$key_range_partition}->{$key_table_partition_description} = $self->{'tmp'}->{$key_table_partition_description};
    delete $self->{'tmp'}->{$key_table_partition_description};
  }
  return $match;
}

sub range_values_clause {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^VALUE//is  ) {
    $match = 1;
    $self->whitespace();
    unless ( $self->{'sql'} =~ s/^LESS//is  ) { return $self->do_err("keyword 'LESS' expected"); }
    $self->whitespace();
    unless ( $self->{'sql'} =~ s/^THAN//is  ) { return $self->do_err("keyword 'THAN' expected"); }
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^\(//is  ) {
      $self->whitespace();
      do {
        $self->whitespace();
        if ( $self->number() ) {
          ${$self->{'tmp'}->{$key_range_values_clause}->{$key_range_value}}[@{$self->{'tmp'}->{$key_range_values_clause}->{$key_range_value}}]->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
          delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
          ${$self->{'tmp'}->{$key_range_values_clause}->{$key_range_value}}[-1]->{'-empty'} = 1;
        }
        elsif ( $self->{'sql'} =~ s/^MAXVALUE//is  ) {
          ${$self->{'tmp'}->{$key_range_values_clause}->{$key_range_value}}[@{$self->{'tmp'}->{$key_range_values_clause}->{$key_range_value}}]->{'-attributes'}->{$self->{'key'}->{'value'}} = 'maxvalue';
          ${$self->{'tmp'}->{$key_range_values_clause}->{$key_range_value}}[-1]->{'-empty'} = 1;
          $self->whitespace();
        }
        else {
          return $self->do_err("value expected");
        }

      } while ($self->{'sql'} =~ s/^,//is);
      unless ( $self->{'sql'} =~ s/^\)//is  ) { return $self->do_err("closing bracket expected in range_valuse_clause"); }
      $self->whitespace();
    }
    else {
      return $self->do_err("opening bracket expected");
    }
  }
  return $match;
}

sub table_partition_description {
  my $self = shift;
  my $match = 0;
  my $descr;
  if ( $self->segment_attributes_clause() ) {
    $match = 1;
    $descr->{$key_segment_attributes_clause} = $self->{'tmp'}->{$key_segment_attributes_clause};
    delete $self->{'tmp'}->{$key_segment_attributes_clause};
  }
  if ( $self->data_segment_compression() ) {
    $match = 1;
    $descr->{$key_data_segment_compression} = $self->{'tmp'}->{$key_data_segment_compression};
    delete $self->{'tmp'}->{$key_data_segment_compression};
  }
  if ( $self->key_compression() ) {
    $match = 1;
    $descr->{$key_key_compression} = $self->{'tmp'}->{$key_key_compression};
    delete $self->{'tmp'}->{$key_key_compression};
  }
  if ( $self->{'sql'} =~ s/^OVERFLOW//is  ) {
    $match = 1;
    $self->whitespace();
    if ( $self->segment_attributes_clause() ) {
      $descr->{$key_overflow}->{$key_segment_attributes_clause} = $self->{'tmp'}->{$key_segment_attributes_clause};
      delete $self->{$key_overflow}->{'tmp'}->{$key_segment_attributes_clause};
    }
    else {
      $descr->{$key_overflow}->{'-empty'} = 1;
    }
  }
  while ( $self->lob_storage_clause() ) {
    $match = 1;
    ${$descr->{$key_lob_storage_clause}}[@{$descr->{$key_lob_storage_clause}}] = $self->{'tmp'}->{$key_lob_storage_clause};
    delete $self->{'tmp'}->{$key_lob_storage_clause};
  }
  $self->{'tmp'}->{$key_table_partition_description} = $descr;
  return $match;
}

sub hash_partitioning {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^HASH//is  ) {
    $match = 1;
    $self->whitespace();
    unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
    $self->whitespace();
    $self->column_list();
    unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in hash partitioning"); }
    $self->whitespace();
    $self->{'tmp'}->{$key_table_partitioning_clauses}->{$key_hash_partitioning}->{$self->{'key'}->{'column'}} = $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
    if ( $self->individual_hash_partitions() ) {
      $self->{'tmp'}->{$key_table_partitioning_clauses}->{$key_hash_partitioning}->{$key_individual_hash_partitions} = $self->{'tmp'}->{$key_individual_hash_partitions};
      delete $self->{'tmp'}->{$key_individual_hash_partitions};
    }
    elsif ( $self->hash_partitions_by_quantity() ) {
      $self->{'tmp'}->{$key_table_partitioning_clauses}->{$key_hash_partitioning}->{$key_hash_partitions_by_quantity} = $self->{'tmp'}->{$key_hash_partitions_by_quantity};
      delete $self->{'tmp'}->{$key_hash_partitions_by_quantity};
    }
    else {
      return $self->do_err("unknown hash partitioning");
    }
  }
  return $match;
}

sub individual_hash_partitions {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^\(//is ) {
    $match = 1;
    $self->whitespace();
    do {
      $self->whitespace();
      unless ( $self->individual_hash_partition ) { return $self->do_err("individual_hash_partition expected"); }
      ${$self->{'tmp'}->{$key_individual_hash_partitions}->{$key_individual_hash_partition}}[@{$self->{'tmp'}->{$key_individual_hash_partitions}->{$key_individual_hash_partition}}] = $self->{'temp'}->{$key_individual_hash_partition};
      delete $self->{'temp'}->{$key_individual_hash_partition};
    } while ( $self->{'sql'} =~ s/^,//is );
    unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in individual_hash_partitions"); }
    $self->whitespace();
  }
  return $match;
}

sub individual_hash_partition {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^PARTITION//is  ) {
    $match = 1;
    $self->whitespace();
    my $identifier;
    if ( $identifier = $self->idientifier() ) {
      $self->{'temp'}->{$key_individual_hash_partition}->{$key_named_individual_hash_partition}->{'-attributes'}->{$key_partition} = $identifier;
      unless ( $self->partitioning_storage_clause() ) { return $self->do_err("partitioning_storage_clause expected"); }
      $self->{'temp'}->{$key_individual_hash_partition}->{$key_named_individual_hash_partition}->{$key_partitioning_storage_clause} = $self->{'tmp'}->{$key_partitioning_storage_clause};
      delete $self->{'tmp'}->{$key_partitioning_storage_clause};
    }
    else {
      $self->{'temp'}->{$key_individual_hash_partition}->{'-empty'} = 1;
    }
  }
  return $match;
}

sub partitioning_storage_clause {
  my $self = shift;
  my $match = 0;
  while ( $self->partitioning_storage() ) {
    $match = 1;
    ${$self->{'tmp'}->{$key_partitioning_storage_clause}->{$key_partitioning_storage}}[@{$self->{'tmp'}->{$key_partitioning_storage_clause}->{$key_partitioning_storage}}] = $self->{'tmp'}->{$key_partitioning_storage};
    delete $self->{'tmp'}->{$key_partitioning_storage};
  }
  return $match;
}

sub partitioning_storage {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^TABLESPACE//is ) {
    $match = 1;
    $self->whitespace();
    my $identifier;
    if ( $identifier = $self->identifier() ) {
      $self->{'tmp'}->{$key_partitioning_storage}->{$key_tablespace}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
      $self->{'tmp'}->{$key_partitioning_storage}->{$key_tablespace}->{'-empty'} = 1;
      $self->whitespace();
    }
    else {
      return $self->do_err("value for tablespace expected");
    }
  }
  elsif ( $self->partitioning_storage_overflow() ) {
    $match = 1;
    $self->{'tmp'}->{$key_partitioning_storage}->{$key_partitioning_storage_overflow} = $self->{'tmp'}->{$key_partitioning_storage_overflow};
    delete $self->{'tmp'}->{$key_partitioning_storage_overflow};
  }
  elsif ( $self->lob_partitioning_storage() ) {
    $match = 1;
    $self->{'tmp'}->{$key_partitioning_storage}->{$key_lob_partitioning_storage} = $self->{'tmp'}->{$key_lob_partitioning_storage};
    delete $self->{'tmp'}->{$key_lob_partitioning_storage};
  }
  return $match;
}

sub partitioning_storage_overflow {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^OVERFLOW//is ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^TABLESPACE//is ) {
      $self->whitespace();
      my $identifier;
      if ( $identifier = $self->identifier() ) {
        $self->{'tmp'}->{$key_partitioning_storage_overflow}->{$key_tablespace}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
        $self->{'tmp'}->{$key_partitioning_storage_overflow}->{$key_tablespace}->{'-empty'} = 1;
        $self->whitespace();
      }
      else {
        return $self->do_err("value for tablespace expected");
      }
    }
    else {
      $self->{'tmp'}->{$key_partitioning_storage_overflow}->{'-empty'} = 1;
    }
  }
  return $match;
}

sub lob_partitioning_storage {
  my $self = shift;
  my $match = 0;
  my $lob_storage;
  if ($self->{'sql'} =~ s/^LOB//is ) {
    $match = 1;
    $self->whitespace();
    unless ($self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected"); }
    $self->whitespace();
    my $identifier;
    unless ( $identifier = $self->identifier() ) { return $self->do_err("identifier expected"); }
    $lob_storage->{$key_lob_item}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    unless ($self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in lob_partition_storage"); }
    $self->whitespace();
    if ($self->{'sql'} =~ s/^STORE//is ) {
      $self->whitespace();
      if ($self->{'sql'} =~ s/^AS//is ) {
        $self->whitespace();
        if ( $identifier = $self->identifier() ) {
          $lob_storage->{$key_lob_segname}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
          if ( $self->{'sql'} =~ s/^\(//is ) {
            $self->whitespace();
            if ( $self->{'sql'} =~ s/^TABLESPACE//is ) {
              $self->whitespace();
              my $identifier;
              if ( $identifier = $self->identifier() ) {
                $lob_storage->{$key_tablespace}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
                $lob_storage->{$key_tablespace}->{'-empty'} = 1;
              }
              else {
                return $self->do_err("value for tablespace expected");
              }
            }
            else {
              return $self->do_err("keyword 'TABLESPACE' expected");
            }
            unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in lob_partition_storage"); }
            $self->whitespace();
          }
        }
        elsif ( $self->{'sql'} =~ s/^\(//is ) {
          $self->whitespace();
          if ( $self->{'sql'} =~ s/^TABLESPACE//is ) {
            $self->whitespace();
            my $identifier;
            if ( $identifier = $self->identifier() ) {
              $lob_storage->{$key_tablespace}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
              $lob_storage->{$key_tablespace}->{'-empty'} = 1;
            }
            else {
              return $self->do_err("value for tablespace expected");
            }
          }
          else {
            return $self->do_err("keyword 'TABLESPACE' expected");
          }
          unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in lob_partition_storage"); }
          $self->whitespace();
        }
        else {
          return $self->do_err("unknown lob_storage_clause");
        }
      }
      else {
        return $self->do_err("keyword 'AS' expected");
      }
    }
    else {
      return $self->do_err("keyword 'STORE' expected");
    }
    $self->{'tmp'}->{$key_lob_partitioning_storage} = $lob_storage;
  }
  return $match;
}

sub hash_partitions_by_quantity {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^PARTITIONS//is  ) {
    $match = 1;
    $self->whitespace();
    if ( $self->number() ) {
      $self->{'tmp'}->{$key_hash_partitions_by_quantity}->{'-attributes'}->{$key_quantity} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
      $self->whitespace();
    }
    else {
      return $self->do_err("quantity expected");
    }
    if ( $self->{'sql'} =~ s/^STORE//is  ) {
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^IN//is  ) {
        $self->whitespace();
        unless ( $self->{'sql'} =~ s/^\(//is  ) {  return $self->do_err("opening bracket expected"); }
        $self->whitespace();
        my $identifier;
        do {
          $self->whitespace();
          unless ( $identifier = $self->identifier() ) {  return $self->do_err("identifier expected"); }
          ${$self->{'tmp'}->{$key_hash_partitions_by_quantity}->{$key_quantity_store_in}->{$key_tablespace}}[@{$self->{'tmp'}->{$key_hash_partitions_by_quantity}->{$key_quantity_store_in}->{$key_tablespace}}]->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
          ${$self->{'tmp'}->{$key_hash_partitions_by_quantity}->{$key_quantity_store_in}->{$key_tablespace}}[-1]->{'-empty'} = 1;
        } while ( $self->{'sql'} =~ s/^,//is);
        unless ( $self->{'sql'} =~ s/^\)//is  ) {  return $self->do_err("closing bracket expected in has_partitions_by_quantity"); }
        $self->whitespace();
      }
      else {
        return $self->do_err("keyword 'IN' expected");
      }
    }
    if ( $self->{'sql'} =~ s/^OVERFLOW//is  ) {
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^STORE//is  ) {
        $self->whitespace();
        if ( $self->{'sql'} =~ s/^IN//is  ) {
          $self->whitespace();
          unless ( $self->{'sql'} =~ s/^\(//is  ) {  return $self->do_err("opening bracket expected"); }
          $self->whitespace();
          my $identifier;
          do {
            $self->whitespace();
            unless ( $identifier = $self->identifier() ) {  return $self->do_err("identifier expected"); }
            ${$self->{'tmp'}->{$key_hash_partitions_by_quantity}->{$key_hash_partition_overflow}->{$key_tablespace}}[@{$self->{'tmp'}->{$key_hash_partitions_by_quantity}->{$key_quantity_store_in}->{$key_tablespace}}]->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
            ${$self->{'tmp'}->{$key_hash_partitions_by_quantity}->{$key_hash_partition_overflow}->{$key_tablespace}}[-1]->{'-empty'} = 1;
          } while ( $self->{'sql'} =~ s/^,//is);
          unless ( $self->{'sql'} =~ s/^\)//is  ) {  return $self->do_err("closing bracket expected in hash_partitions_by_quantitiy"); }
          $self->whitespace();
        }
        else {
          return $self->do_err("keyword 'IN' expected");
        }
      }
      else {
        return $self->do_err("keyword 'STORE' expected");
      }
    }
  }
  return $match;
}

sub list_partitioning {
  my $self = shift;
  my $match = 0;
  my $partitoning;
  my $identifier;
  if ( $self->{'sql'} =~ s/^LIST//is  ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^\(//is  ) {
      $self->whitespace();
      unless ( $identifier = $self->identifier() ) {  return $self->do_err("identifier expected"); }
      $partitoning->{$self->{'key'}->{'column'}}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
      unless ( $self->{'sql'} =~ s/^\)//is  ) {  return $self->do_err("closing bracket expected in list_partitioning"); }
      $self->whitespace();
    }
    else {
      return $self->do_err("opening bracket expected");
    }
    if ( $self->{'sql'} =~ s/^\(//is  ) {
      $self->whitespace();
      do {
        $self->whitespace();
        unless ( $self->list_partitioning_element() ) {  return $self->do_err("identifier expected"); }
        ${$partitoning->{$key_list_partitioning_element}}[@{$partitoning->{$key_list_partitioning_element}}] = $self->{'tmp'}->{$key_list_partitioning_element};
        delete $self->{'tmp'}->{$key_list_partitioning_element};
      } while ( $self->{'sql'} =~ s/^,//is);
      unless ( $self->{'sql'} =~ s/^\)//is  ) {  return $self->do_err("closing bracket expected in list_partitioning"); }
      $self->whitespace();
    }
    else {
      return $self->do_err("opening bracket expected");
    }
    $self->{'tmp'}->{$key_table_partitioning_clauses}->{$key_list_partitioning} = $partitoning;
  }
  return $match;
}

sub list_partitioning_element {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^PARTITION//is  ) {
    $match = 1;
    $self->whitespace();
    my $identifier;
    if ( $identifier = $self->identifier() ) {
      $self->{'tmp'}->{$key_list_partitioning_element}->{'-attributes'}->{$key_partition} = $identifier;
    }
    unless ( $self->list_values_clause()  ) {  return $self->do_err("list_values_clause expected"); }
    $self->{'tmp'}->{$key_list_partitioning_element}->{$key_list_values_clause} = $self->{'tmp'}->{$key_list_values_clause};
    delete $self->{'tmp'}->{$key_list_values_clause};
    unless ( $self->table_partition_description()  ) {  return $self->do_err("table_partition_description expected"); }
    $self->{'tmp'}->{$key_list_partitioning_element}->{$key_table_partition_description} = $self->{'tmp'}->{$key_table_partition_description};
    delete $self->{'tmp'}->{$key_table_partition_description};
  }
  return $match;
}

sub list_values_clause {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^VALUES//is  ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^\(//is  ) {
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^DEFAULT//is  ) {
        $self->{'tmp'}->{$key_list_values_clause}->{$key_default}->{'-empty'} = 1;
        $self->whitespace();
      }
      else {
        do {
          $self->whitespace();
          if ( $self->{'sql'} =~ s/^NULL//is  ) {
            ${$self->{'tmp'}->{$key_list_values_clause}->{$key_list_value}}[@{$self->{'tmp'}->{$key_list_values_clause}->{$key_list_value}}]->{$self->{'key'}->{'null'}} ->{'-empty'} = 1;
            $self->whitespace();
          }
          elsif ( $self->number() ) {
            ${$self->{'tmp'}->{$key_list_values_clause}->{$key_list_value}}[@{$self->{'tmp'}->{$key_list_values_clause}->{$key_list_value}}]->{$self->{'key'}->{'value'}}->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
            delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
            $self->whitespace();
          }
          else {
            return $self->do_err("value expected");
          }
        } while ( $self->{'sql'} =~ s/^,//is);
      }
      unless ( $self->{'sql'} =~ s/^\)//is  ) {  return $self->do_err("closing bracket expected in list_values_clause"); }
      $self->whitespace();
    }
    else {
      return $self->do_err("opening bracket expected");
    }
  }
  return $match;
}

sub row_movement_clause {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^(ENABLE|DISABLE)//is  ) {
    my $type = lc $1;
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^ROW//is  ) {
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^MOVEMENT//is  ) {
        $self->whitespace();
        $self->{'tmp'}->{$key_row_movement_clause}->{'-attributes'}->{$key_movement} = $type;
      }
      else {
        return $self->do_err("keyword 'MOVEMENT' expected");
      }
    }
    else {
      return $self->do_err("keyword 'ROW' expected");
    }
  }
  return $match;
}

sub parallel_clause {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^PARALLEL//is ) {
    $self->whitespace();
    if ( $self->number() ) {
      $match = 1;
      $self->{'tmp'}->{$key_parallel_clause}->{'-attributes'}->{$self->{'key'}->{'type'}} = 'compress';
      $self->{'tmp'}->{$key_parallel_clause}->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
      $self->{'tmp'}->{$key_parallel_clause}->{'-empty'} = 1;
      $self->whitespace();
    }
    else {
      return $self->do_err("number value for 'parallel' expected in parallel_clause");
    }
  }
  elsif ( $self->{'sql'} =~ s/^NOPARALLEL//is ) {
    $match = 1;
    $self->{'tmp'}->{$key_parallel_clause}->{'-attributes'}->{$self->{'key'}->{'type'}} = 'nocompress';
    $self->{'tmp'}->{$key_parallel_clause}->{'-empty'} = 1;
    $self->whitespace();
  }
  return $match;
}

sub enable_disable_clause {
  my $self = shift;
  my $match = 0;
  unless ( $self->{'sql'} =~ s/^(ENABLE|DISABLE)//is  ) { return 0; }
  $self->{'tmp'}->{$key_enable_disable_clause}->{'-attributes'}->{$self->{'key'}->{'type'}} = lc $1;
  $self->whitespace();
  if ( $self->{'sql'} =~ s/^(VALIDATE|NOVALIDATE)//is ) {
    $self->{'tmp'}->{$key_enable_disable_clause}->{'-attributes'}->{$key_validate} = lc $1;
    $self->whitespace();
  }
  if ( $self->{'sql'} =~ s/^UNIQUE//is ) {
    $self->whitespace();
    unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected in enable_disable_clause"); }
    $self->whitespace();
    $self->column_list();
    unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in enable_disable_vlause"); }
    $self->whitespace();
    $self->{'tmp'}->{$key_enable_disable_clause}->{$key_unique_enable_disable_clause}->{$self->{'key'}->{'column'}} = $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
    delete $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
  }
  elsif ( $self->{'sql'} =~ s/^PRIMARY//is ) {
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^KEY//is ) {
      $self->whitespace();
      $self->{'tmp'}->{$key_enable_disable_clause}->{$key_primary_key_enable_disable_clause}->{'-empty'} = 1;
    }
    else {
      return $self->do_err("keyword 'KEY' expected");
    }
  }
  elsif ( $self->{'sql'} =~ s/^CONSTRAINT//is ) {
    $self->whitespace();
    my $identifier;
    if ( $identifier = $self->identifier() ) {
      $self->{'tmp'}->{$key_enable_disable_clause}->{$key_constraint}->{'-attributes'}->{$self->{'key'}->{'name'}} =  $identifier;
    }
    else {
      return $self->do_err("identifier expected");
    }
  }
  else {
    return $self->do_err("enable_disable_clause expected");
  }
  if ( $self->using_index() ) {
    $self->{'tmp'}->{$key_enable_disable_clause}->{$key_using_index} = $self->{'tmp'}->{$key_using_index};
    delete $self->{'tmp'}->{$key_using_index};
  }
  if ( $self->{'sql'} =~ s/^CASCADE//is ) {
    $self->whitespace();
    $self->{'tmp'}->{$key_enable_disable_clause}->{$self->{'key'}->{'cascade'}}->{'-empty'} = 1;
  }
  if ( $self->{'sql'} =~ s/^(KEEP|DROP)//is ) {
    my $type = lc $1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^INDEX//is ) {
      $self->whitespace();
      $self->{'tmp'}->{$key_enable_disable_clause}->{'-attributes'}->{$self->{'key'}->{'type'}} = lc $1;
    }
    else {
      return $self->do_err("keyword 'INDEX' expected");
    }
  }
  return $match;
}

sub data_segment_compression {
  my $self = shift;
  my $match = 0;
  if ($self->{'sql'} =~ s/^(COMPRESS|NOCOMPRESS)//is ) {
    $match = 1;
    $self->{'tmp'}->{$key_data_segment_compression}->{'-attributes'}->{$self->{'key'}->{'value'}} = lc $1;
    $self->{'tmp'}->{$key_data_segment_compression}->{'-empty'} = 1;
    $self->whitespace();
  }
  return $match;
}

sub physical_attributes_clause {
  my $self = shift;
  my $match = 0;
  my $sub_match = 0;
  do {
    $sub_match = 0;
    if ($self->{'sql'} =~ s/^(MAXTRANS|INITRANS|PCTUSED|PCTFREE)//is ) {
      my $type = lc $1;
      $self->whitespace();
      if ( $self->number()  ) {
        $match = 1;
        $sub_match = 1;
        $self->{'tmp'}->{$key_physical_attributes_clause}->{$type}->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
        delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
        $self->{'tmp'}->{$key_physical_attributes_clause}->{$type}->{'-empty'} = 1;
        $self->whitespace();
      }
      else {
        return $self->do_err("value expected in physical_attributes_clause");
      }
    }
    elsif ($self->{'sql'} =~ m/^STORAGE/i ) {
      $self->whitespace();
      $match = 1;
      $sub_match = 1;
      if ( $self->storage_clause() ) {
        $self->{'tmp'}->{$key_physical_attributes_clause}->{$key_storage_clause} = $self->{'tmp'}->{$key_storage_clause};
        delete $self->{'tmp'}->{$key_storage_clause};
      }
    }
  } while ( $sub_match == 1 );
  return $match;
}

sub logging_clause {
  my $self = shift;
  my $match = 0;
  if ($self->{'sql'} =~ s/^(LOGGING|NOLOGGING)//is  ) {
    $match = 1;
    $self->{'tmp'}->{$key_logging_clause}->{'-attributes'}->{$key_logging} = lc $1;
    $self->{'tmp'}->{$key_logging_clause}->{'-empty'} = 1;
    $self->whitespace();
  }
  return $match;
}

sub storage_clause {
  my $self = shift;
  my $match = 0;
  my $type;
  if ( $self->{'sql'} =~ s/^STORAGE//is ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^\(//is ) {
      $self->whitespace();
      my $submatch = 0;
      do {
        $submatch = 0;
        $self->whitespace();
        if ( $self->{'sql'} =~ s/^(MINEXTENTS|PCTINCREASE|FREELISTS)//is ) {
          $type = lc $1;
          $self->whitespace();
          $submatch = 1;
          if ( $self->number() ) {
            ${$self->{'tmp'}->{$key_storage_clause}->{$type}}[@{$self->{'tmp'}->{$key_storage_clause}->{$type}}]->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
            delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
            ${$self->{'tmp'}->{$key_storage_clause}->{$type}}[-1]->{'-empty'} = 1;
            $self->whitespace();
          }
          else {
            return $self->do_err("value expected in storage_clause");
          }
        }
        elsif ($self->{'sql'} =~ s/^(INITIAL|NEXT)//is ) {
          $type = lc $1;
          $self->whitespace();
          $submatch = 1;
          if ( $self->number() ) {
            ${$self->{'tmp'}->{$key_storage_clause}->{$type}}[@{$self->{'tmp'}->{$key_storage_clause}->{$type}}]->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
            delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
            ${$self->{'tmp'}->{$key_storage_clause}->{$type}}[-1]->{'-empty'} = 1;
            $self->whitespace();
            if ( $self->{'sql'} =~ s/^(K|M)//is ) {
              ${$self->{'tmp'}->{$key_storage_clause}->{$type}}[-1]->{'-attributes'}->{'unit'} = lc $1;
              $self->whitespace();
            }
          }
          else {
            return $self->do_err("value expected in storage_clause");
          }
        }
        elsif ($self->{'sql'} =~ s/^MAXEXTENTS//is ) {
          $self->whitespace();
          $submatch = 1;
          if ( $self->{'sql'} =~ s/^unlimited//is ) {
            ${$self->{'tmp'}->{$key_storage_clause}->{$key_maxextents}}[@{$self->{'tmp'}->{$key_storage_clause}->{$key_maxextents}}] ->{'-attributes'}->{$self->{'key'}->{'value'}} = 'unlimited';
            ${$self->{'tmp'}->{$key_storage_clause}->{$key_maxextents}}[-1]->{'-empty'} = 1;
          }
          elsif ( $self->number() ) {
            ${$self->{'tmp'}->{$key_storage_clause}->{$key_maxextents}}[@{$self->{'tmp'}->{$key_storage_clause}->{$key_maxextents}}] ->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
            delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
            ${$self->{'tmp'}->{$key_storage_clause}->{$key_maxextents}}[-1]->{'-empty'} = 1;
          }
          else {
            return $self->do_err("value expected in storage_clause");
          }
        }
        elsif ($self->{'sql'} =~ s/^OPTIMAL//is ) {
          $self->whitespace();
          $submatch = 1;
          if ( $self->number() ) {
            ${$self->{'tmp'}->{$key_storage_clause}->{$key_optimal}}[@{$self->{'tmp'}->{$key_storage_clause}->{$key_optimal}}] ->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
            delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
            ${$self->{'tmp'}->{$key_storage_clause}->{$key_optimal}}[-1]->{'-empty'} = 1;
            $self->whitespace();
            if ( $self->{'sql'} =~ s/^(K|M)//is ) {
              ${$self->{'tmp'}->{$key_storage_clause}->{$key_optimal}}[-1]->{'-attributes'}->{'unit'} =  lc $1;
              $self->whitespace();
            }
          }
          elsif ( $self->{'sql'} =~ s/^NULL//is ) {
            ${$self->{'tmp'}->{$key_storage_clause}->{$key_optimal}}[@{$self->{'tmp'}->{$key_storage_clause}->{$key_optimal}}] ->{'-attributes'}->{$self->{'key'}->{'value'}} = 'null';
            ${$self->{'tmp'}->{$key_storage_clause}->{$key_optimal}}[-1]->{'-empty'} = 1;
            $self->whitespace();
          }
          else {
            return $self->do_err("value expected in storage_clause");
          }
        }
        elsif ($self->{'sql'} =~ s/^BUFFER_POOL//is ) {
          $self->whitespace();
          $submatch = 1;
          if ( $self->{'sql'} =~ s/^(KEEP|RECYCLE|DEFAULT)//is ) {
            ${$self->{'tmp'}->{$key_storage_clause}->{$key_buffer_pool}}[@{$self->{'tmp'}->{$key_storage_clause}->{$key_buffer_pool}}] ->{'-attributes'}->{$self->{'key'}->{'value'}} = lc $1;
            ${$self->{'tmp'}->{$key_storage_clause}->{$key_buffer_pool}}[-1]->{'-empty'} = 1;
          }
          else {
            return $self->do_err("value expected in storage_clause");
          }
        }
        elsif ($self->{'sql'} =~ s/^FREELIST//is ) {
          $self->whitespace();
          $submatch = 1;
          if ( $self->{'sql'} =~ s/^GROUPS//is ) {
            $self->whitespace();
            if ( $self->number() ) {
              ${$self->{'tmp'}->{$key_storage_clause}->{$key_freelist_groups}}[@{$self->{'tmp'}->{$key_storage_clause}->{$key_freelist_groups}}] ->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
              delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
              ${$self->{'tmp'}->{$key_storage_clause}->{$key_freelist_groups}}[-1]->{'-empty'} = 1;
            }
            else {
              return $self->do_err("value expected in storage_clause");
            }
          }
          else {
            return $self->do_err("keyword 'GROUPS' expected in storage_clause");
          }
        }
      } while ( $submatch == 1 );
      unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in storage_clause"); }
      $self->whitespace();
    }
    else {
      return $self->do_err("opening bracket expected in storage_clause");
    }
  }
  return $match;
}

sub key_compression {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^COMPRESS//is) {
    $self->whitespace();
    if ( $self->number() ) {
      $match = 1;
      $self->{'tmp'}->{$key_key_compression}->{'-attributes'}->{$self->{'key'}->{'type'}} = 'compress';
      $self->{'tmp'}->{$key_key_compression}->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
      $self->{'tmp'}->{$key_key_compression}->{'-empty'} = 1;
      $self->whitespace();
    }
    else {
      return $self->do_err("number value for 'compress' expected in key_compression");
    }
  }
  elsif ( $self->{'sql'} =~ s/^NOCOMPRESS//is ) {
    $match = 1;
    $self->{'tmp'}->{$key_key_compression}->{'-attributes'}->{$self->{'key'}->{'type'}} = 'nocompress';
    $self->{'tmp'}->{$key_key_compression}->{'-empty'} = 1;
    $self->whitespace();
  }
  return $match;
}

sub using_index {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^USING//is ) {
    $self->whitespace();
    $match = 1;
    if ( $self->{'sql'} =~ s/^INDEX//is ) {
      $self->whitespace();
      unless ( $self->using_index_internal() ) { return $self->do_err("using_index expected"); }
      ${$self->{'tmp'}->{$key_using_index}->{$key_using_index_element}}[@{$self->{'tmp'}->{$key_using_index}->{$key_using_index_element}}] = $self->{'tmp'}->{$key_using_index_internal};
      delete $self->{'tmp'}->{$key_using_index_internal};
      while (  $self->using_index_internal() ) {
        ${$self->{'tmp'}->{$key_using_index}->{$key_using_index_element}}[@{$self->{'tmp'}->{$key_using_index}->{$key_using_index_element}}] = $self->{'tmp'}->{$key_using_index_internal};
        delete $self->{'tmp'}->{$key_using_index_internal};
      }
    }
  }
  return $match;
}

sub using_index_internal {
  my $self = shift;
  my $constraint = shift;
  my $match = 0;
  my $identifier;
  if ( $self->{'sql'} =~ s/^(PCTFREE|INITRANS|MAXTRANS)//is ) {
    my $type = lc $1;
    $self->whitespace();
    if ( $self->number() ) {
      $self->{'tmp'}->{$key_using_index_internal}->{$type}->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
      $self->{'tmp'}->{$key_using_index_internal}->{$type}->{'-empty'} = 1;
      delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
      $match = 1;
    }
    else {
      return $self->do_err("number expected in using_index");
    }
  }
  elsif ( $self->{'sql'} =~ s/^TABLESPACE//is ) {
    $self->whitespace();
    if ( $identifier = $self->identifier() ) {
      $self->{'tmp'}->{$key_using_index_internal}->{$key_tablespace}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
      $self->{'tmp'}->{$key_using_index_internal}->{$key_tablespace}->{'-empty'} = 1;
      $match = 1;
    }
    else {
      return $self->do_err("tablespace expected in using_index");
    }
  }
  elsif ( $self->storage_clause() ) {
    $self->{'tmp'}->{$key_using_index_internal}->{$key_storage_clause} = $self->{'tmp'}->{$key_storage_clause};
    delete $self->{'tmp'}->{$key_storage_clause};
    $match = 1;
  }
  elsif ( $self->{'sql'} =~ s/^NOSORT//is ) {
    $self->{'tmp'}->{$key_using_index_internal}->{$key_nosort}->{'-attributes'}->{$self->{'key'}->{'value'}} = 'nosort';
    $self->{'tmp'}->{$key_using_index_internal}->{$key_nosort}->{'-empty'} = 1;
    $match = 1;
    $self->whitespace();
  }
  elsif ( $self->logging_clause() ) {
    $self->{'tmp'}->{$key_using_index_internal}->{$key_logging_clause} = $self->{'tmp'}->{$key_logging_clause};
    delete $self->{'tmp'}->{$key_logging_clause};
    $match = 1;
  }
  elsif ( $self->{'sql'} =~ s/^LOCAL//is ) {
    $self->{'tmp'}->{$key_using_index_internal}->{$key_local}->{'-attributes'}->{$self->{'key'}->{'value'}} = $key_local;
    $self->{'tmp'}->{$key_using_index_internal}->{$key_local}->{'-empty'} = 1;
    $match = 1;
    $self->whitespace();
  }
  else {
    if ( $identifier = $self->identifier() ) {
    $match = 1;
      if ( $self->{'sql'} =~ s/^\.//is  ) {
        $self->whitespace();
         $self->{'tmp'}->{$key_using_index_internal}->{$key_index_element}->{'-attributes'}->{$self->{'key'}->{'schema'}} = $identifier;
        unless ( $identifier = $self->identifier() ) { return $self->do_err("identifier expected"); }
        $self->{'tmp'}->{$key_using_index_internal}->{$key_index_element}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
      }
      else {
        $self->{'tmp'}->{$key_using_index_internal}->{$key_index_element}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
      }
      $self->{'tmp'}->{$key_using_index_internal}->{$key_index_element}->{'-empty'} = 1;
    }
  }
  return $match;
}


######################################################################
#
# END CREATE TABLE
#
######################################################################




######################################################################
#
# CREATE INDEX DBMS EXTENSION
#
######################################################################


sub dbms_table_index_clause {
  my $self = shift;
  my $match = 0;
  my $sub_match = 0;
  my $clause;
  do {
    $sub_match = 0;
    if ( $self->global_partitioned_index() ) {
      $match = 1;
      $sub_match = 1;
      ${$clause->{$key_global_partitioned_index}}[@{$clause->{$key_global_partitioned_index}}] = $self->{'tmp'}->{$key_global_partitioned_index};
      delete $self->{'tmp'}->{$key_global_partitioned_index};
    }
    elsif ( $self->local_partitioned_index() ) {
      $match = 1;
      $sub_match = 1;
      ${$clause->{$key_local_partitioned_index}}[@{$clause->{$key_local_partitioned_index}}] = $self->{'tmp'}->{$key_global_partitioned_index};
      delete $self->{'tmp'}->{$key_local_partitioned_index};
    }
    elsif ( $self->index_attributes() ) {
      $match = 1;
      $sub_match = 1;
      ${$clause->{$key_index_attributes}}[@{$clause->{$key_index_attributes}}] = $self->{'tmp'}->{$key_index_attributes};
      delete $self->{'tmp'}->{$key_index_attributes};
      $self->whitespace();
    }
  } while ($sub_match);
  $self->{'tmp'}->{$self->{'key'}->{'dbms_table_index_clause'}} = $clause;  
  return $match;
}

sub dbms_cluster_index_clause {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^CLUSTER//is ) {
    $match = 1;
    $self->whitespace();
    my $identifier;
    if ( $identifier = $self->identifier() ) {
      if ( $self->{'sql'} =~ s/^\.//is  ) {
        $self->whitespace();
        $self->{'tmp'}->{$self->{'key'}->{'index'}}->{$key_dbms_cluster_index_clause}->{'-attributes'}->{$self->{'key'}->{'schema'}} = $identifier;
        unless ( $identifier = $self->identifier() ) { return $self->do_err("identifier expected"); }
        $self->{'tmp'}->{$self->{'key'}->{'index'}}->{$key_dbms_cluster_index_clause}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
      }
      else {
        $self->{'tmp'}->{$self->{'key'}->{'index'}}->{$key_dbms_cluster_index_clause}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
      }
    }
    else {
      return $self->do_err("identifier expected");
    }
    unless ( $self->index_attributes() ) {
      return $self->do_err("Index attributes expected") ;
    }
    while ( $self->index_attributes() ) {
    }
    $self->{'tmp'}->{$self->{'key'}->{'index'}}->{$key_dbms_cluster_index_clause}->{$key_index_attributes} = $self->{'tmp'}->{$key_index_attributes};
    delete $self->{'tmp'}->{$key_index_attributes};
  }
  return $match;
}

sub dbms_bitmap_join_index_clause {
  my $self = shift;
  my $match = 0;
  return $match;
}

sub global_partitioned_index {
  my $self = shift;
  my $match = 0;
  if ( $self->{'sql'} =~ s/^GLOBAL//is ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^PARTITION//is ) {
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^BY//is ) {
        $self->whitespace();
        if ( $self->{'sql'} =~ s/^RANGE//is ) {
          $self->whitespace();
          unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected in out_of_line_constraint"); }
          $self->whitespace();
          $self->column_list();
          unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in out_of_line_constraint"); }
          $self->whitespace();
          $self->{'tmp'}->{$key_global_partitioned_index}->{$self->{'key'}->{'column'}} = $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
          delete $self->{'tmp'}->{$self->{'key'}->{'column_list'}};
          unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected in out_of_line_constraint"); }
          $self->whitespace();
          unless ( $self->global_partitioning_clause() ) {return $self->do_err("global_partitioning_clause expected in out_of_line_constraint"); }
          $self->{'tmp'}->{$key_global_partitioned_index}->{$key_global_partitioning_clause} = $self->{'tmp'}->{$key_global_partitioning_clause};
          delete $self->{'tmp'}->{$key_global_partitioning_clause};
          unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in out_of_line_constraint"); }
          $self->whitespace();
        }
        else {
          return $self->do_err("keyword 'RANGE' expected");
        }
      }
      else {
        return $self->do_err("keyword 'BY' expected");
      }
    }
    else {
      return $self->do_err("keyword 'PARTITION' expected");
    }
  }
  return $match;
}

sub local_partitioned_index {
  my $self = shift;
  my $match = 0;
  my $partitions;
  my $identifier;
  my $type = 'unknown';
  if ( $self->{'sql'} =~ s/^LOCAL//is ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^\(//is ) {
      $self->whitespace();
      # ok this is pretty complicated:
      # when beginning to parse, we do not know which type of partitioned_index we are looking at
      # since a valid entry in the list might even consist of the sole word PARTITION, we even might
      # not know after parsing the first, second, third... line.
      # Therefore we have a general parse routine, that determines the type every loop and if the types
      # match to the type-info we have collected so far, we append the value to the list $partitions,
      # if not, we throw an error message, because apparently the user must have mixed differenty
      # partitioned index types.
      do {
        $self->whitespace();
        # parse
        unless ( $self->local_partitioned_entry() ) { return $self->do_err("keyword 'PARTITION' expected"); }
        # check types
        if ( $type eq 'range_or_list' ) {
          unless ( $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'type'}} eq 'range_or_list' || $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'type'}} eq 'unknown' ) {
            return $self->do_err("error parsing local_partitioned_index");
          }
        }
        elsif ( $type eq 'hash' ) {
          unless ( $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'type'}} eq 'hash' || $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'type'}} eq 'unknown' ) {
            return $self->do_err("error parsing local_partitioned_index");
          }
        }
        else {
          if ( $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'type'}} ne 'unknown' ) {
            $type = $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'type'}};
          }
        }
        # append data
        if ( $self->{'tmp'}->{$key_local_partitioned_entry}->{$key_data} ) { # if there is data, there is also a name
          ${$partitions}[@{$partitions}]->{'-attributes'}->{$key_partition} = $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'name'}};
          ${$partitions}[-1]->{$key_tablespace}->{'-attributes'}->{$self->{'key'}->{'name'}} = $self->{'tmp'}->{$key_local_partitioned_entry}->{$key_data};
          ${$partitions}[-1]->{$key_tablespace}->{'-empty'} = 1;
        }
        elsif ( $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'name'}} ) {
          ${$partitions}[@{$partitions}]->{'-attributes'}->{$key_partition} = $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'name'}};
        }
        else {
          ${$partitions}[@{$partitions}]->{'-empty'} = 1;
        }
        # clean up
        delete $self->{'tmp'}->{$key_local_partitioned_entry};
      } while ($self->{'sql'} =~ s/^,//is );
      # finally store all in the correct place
      if ( $type eq 'unknown' || $type eq 'range_or_list' ) {
        # if we could not determine the type, we default to range_or_list
        $self->{'tmp'}->{$key_local_partitioned_index}->{$key_on_range_or_list_partitioned_table}->{$key_local_range_or_list_partition} = $partitions;
        $partitions = undef;
      }
      elsif ( $type eq 'hash' ) {
        $self->{'tmp'}->{$key_local_partitioned_index}->{$key_on_hash_partitioned_table}->{$key_local_hash_partition} = $partitions;
        $partitions = undef;
      }
      else {
        return $self->do_err("internal error in parser");
      }
      unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in out_of_line_constraint"); }
      $self->whitespace();
    }
    elsif ( $self->{'sql'} =~ s/^STORE//is ) {
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^IN//is ) {
        $self->whitespace();
        my $store_in;
        if ( $self->{'sql'} =~ s/^\(//is ) {
          $self->whitespace();
          if ( $self->{'sql'} =~ s/^\DEFAULT//is ) {
            ${$store_in}[@{$store_in}]->{$key_default}->{'-empty'} = 1;
          }
          else {
            do {
              $self->whitespace();
              unless ( $identifier = $self->identifier() ) { return $self->do_err("identifier expected"); }
              ${$store_in}[@{$store_in}]->{$key_tablespace}->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
              ${$store_in}[-1]->{$key_tablespace}->{'-empty'} = 1;
            } while ( $self->{'sql'} =~ s/^,//is );
          }
          unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in out_of_line_constraint"); }
          $self->whitespace();
          # now we see what type of partitioned index it is:
          if ( $self->{'sql'} =~ s/^\(//is ) {
            $self->whitespace();
            # comp partition
            $self->{'tmp'}->{$key_local_partitioned_index}->{$key_on_comp_partitioned_table}->{$key_store_in} = $store_in;
            do {
              $self->whitespace();
              unless ( $self->{'sql'} =~ s/^PARTITION//is ) { return $self->do_err("keyword 'PARTITION' expected"); }
              $self->whitespace();
              if ( ${$partitions}[@{$partitions}]->{'-attributes'}->{$key_partition} = $self->identifier() ) {
                while ( $self->segment_attributes_clause() ) {
                  ${${$partitions}[-1]->{$key_segment_attributes_clause}}[@{${$partitions}[-1]->{$key_segment_attributes_clause}}]  = $self->{'tmp'}->{$key_segment_attributes_clause};
                  delete $self->{'tmp'}->{$key_segment_attributes_clause};
                }
                # no subpartitions
              }
              else {
                ${$partitions}[@{$partitions}]->{'-empty'} = 1;
              }
            } while ( $self->{'sql'} =~ s/^,//is );
            unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in out_of_line_constraint"); }
            $self->whitespace();
          }
          else {
            # hash partition
            $self->{'tmp'}->{$key_local_partitioned_index}->{$key_on_hash_partitioned_table}->{$key_store_in} = $store_in;
          }
        }
        else {
          return $self->do_err("opening bracket expected");
        }
      }
      else {
        return $self->do_err("keyword 'IN' expected");
      }
    }
    else {
      return $self->do_err("unknown local_partitioned_index ");
    }
  }
  return $match;
}

sub index_attributes {
  my $self = shift;
  my $match = 0;
  if ( $self->physical_attributes_clause() ) {
    $match = 1;
    ${$self->{'tmp'}->{$key_index_attributes}->{$key_physical_attributes_clause}}[@{$self->{'tmp'}->{$key_index_attributes}->{$key_physical_attributes_clause}}] = $self->{'tmp'}->{$key_physical_attributes_clause};
    delete $self->{'tmp'}->{$key_physical_attributes_clause};
    $self->whitespace();
  }
  elsif ( $self->logging_clause() ) {
    $match = 1;
    ${$self->{'tmp'}->{$key_index_attributes}->{$key_logging_clause}}[@{$self->{'tmp'}->{$key_index_attributes}->{$key_logging_clause}}] = $self->{'tmp'}->{$key_logging_clause};
    delete $self->{'tmp'}->{$key_logging_clause};
    $self->whitespace();
  }
  elsif ( $self->key_compression() ) {
    $match = 1;
    ${$self->{'tmp'}->{$key_index_attributes}->{$key_key_compression}}[@{$self->{'tmp'}->{$key_index_attributes}->{$key_key_compression}}] = $self->{'tmp'}->{$key_key_compression};
    delete $self->{'tmp'}->{$key_key_compression};
    $self->whitespace();
  }
  elsif ( $self->parallel_clause() ) {
    $match = 1;
    ${$self->{'tmp'}->{$key_index_attributes}->{$key_parallel_clause}}[@{$self->{'tmp'}->{$key_index_attributes}->{$key_parallel_clause}}] = $self->{'tmp'}->{$key_parallel_clause};
    delete $self->{'tmp'}->{$key_parallel_clause};
    $self->whitespace();
  }
  elsif ( $self->{'sql'} =~ s/^TABLESPACE//is ) {
    $match = 1;
    $self->whitespace();
    my $identifier;
    if ( $identifier = $self->identifier() ) {
      ${$self->{'tmp'}->{$key_index_attributes}->{$key_tablespace}}[@{$self->{'tmp'}->{$key_index_attributes}->{$key_tablespace}}]->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
      ${$self->{'tmp'}->{$key_index_attributes}->{$key_tablespace}}[-1]->{'-empty'} = 1;
      $self->whitespace();
    }
    else {
      return $self->do_err("value for tablespace expected in index_attributes");
    }
  }
  elsif ( $self->{'sql'} =~ s/^ONLINE//is ) {
    $match = 1;
    ${$self->{'tmp'}->{$key_index_attributes}->{$key_online}}[@{$self->{'tmp'}->{$key_index_attributes}->{$key_online}}]->{'-empty'} = 1;
    $self->whitespace();
  }
  elsif ( $self->{'sql'} =~ s/^COMPUTE//is ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^STATISTICS//is ) {
      ${$self->{'tmp'}->{$key_index_attributes}->{$key_statistics}}[@{$self->{'tmp'}->{$key_index_attributes}->{$key_statistics}}] ->{'-attributes'}->{$key_compute} = 'compute';
      ${$self->{'tmp'}->{$key_index_attributes}->{$key_statistics}}[@{$self->{'tmp'}->{$key_index_attributes}->{$key_statistics}}]->{'-empty'} = 1;
      $self->whitespace();
    }
    else {
      return $self->do_err("keyword $key_statistics expected in index_attributes");
    }
  }
  elsif ( $self->{'sql'} =~ s/^(NOSORT|REVERSE)//is ) {
    $match = 1;
    ${$self->{'tmp'}->{$key_index_attributes}->{$key_index_sorting}}[@{$self->{'tmp'}->{$key_index_attributes}->{$key_index_sorting}}] ->{'-attributes'}->{$self->{'key'}->{'name'}} = lc $1;
    ${$self->{'tmp'}->{$key_index_attributes}->{$key_index_sorting}}[@{$self->{'tmp'}->{$key_index_attributes}->{$key_index_sorting}}]->{'-empty'} = 1;
    $self->whitespace();
  }
  return $match;
}

sub local_partitioned_entry {
  my $self = shift;
  my $match = 0;
  my $identifier;
  if ( $self->{'sql'} =~ s/^PARTITION//is ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'name'}} = $self->identifier() ) {
      if ( $self->segment_attributes_clause() ) {
        $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'type'}} = 'range_or_list';
        do {
          ${$self->{'tmp'}->{$key_local_partitioned_entry}->{$key_data}}[@{$self->{'tmp'}->{$key_local_partitioned_entry}->{$key_data}}] = $self->{'tmp'}->{$key_segment_attributes_clause};
          delete $self->{'tmp'}->{$key_segment_attributes_clause};
        } while ( $self->segment_attributes_clause() );
      }
      elsif ( $self->{'sql'} =~ s/^TABLESPACE//is ) {
        $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'type'}} = 'hash';
        $self->whitespace();
        unless ( $identifier = $self->identifier() ) {  return $self->do_err("identifier expected"); }
        $self->{'tmp'}->{$key_local_partitioned_entry}->{$key_data} = $identifier;
      }
      else {
        $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'type'}} = 'unknown';
      }
    }
    else {
      $self->{'tmp'}->{$key_local_partitioned_entry}->{$self->{'key'}->{'type'}} = 'unknown';
    }
  }
  return $match;
}


######################################################################
#
# END CREATE INDEX
#
######################################################################




######################################################################
#
# SELECT DBMS EXTENSION
#
######################################################################


sub dbms_for_update_clause {
  my $self = shift;
  my $submatch = 0;
  unless ( $self->{'sql'} =~ s/^FOR//is ) { return 0; }
  $self->whitespace();
  unless ( $self->{'sql'} =~ s/^UPDATE//is ) { return $self->do_err("keyword 'UPDATE' expected"); }
  $self->whitespace();
  if ( $self->{'sql'} =~ s/^OF//is ) {
    $self->whitespace();
    $submatch = 1;
    my $objects;
    my $identifier1;
    my $identifier2;
    my $identifier3;
    do {
      $self->whitespace();
      if ( $identifier1 = $self->identifier() ) {
        if ( $self->{'sql'} =~ s/^\.//is  ) {
          $self->whitespace();
          if ( $identifier2 = $self->identifier() ) {
            if ( $self->{'sql'} =~ s/^\.//is  ) {
              $self->whitespace();
              unless ( $identifier3 = $self->identifier() ) { return $self->do_err("identifier expected"); }
              ${$objects}[@{$objects}]->{'-attributes'}->{$self->{'key'}->{'schema'}} = $identifier1;
              ${$objects}[-1]->{'-attributes'}->{$self->{'key'}->{'table_view'}} = $identifier2;
              ${$objects}[-1]->{'-attributes'}->{$self->{'key'}->{'column'}} = $identifier3;
              ${$objects}[-1]->{'-attributes'}->{$self->{'key'}->{'id'}} = "object_".$self->{'object_id_counter'};
              $self->{'object_id_counter'}++;
            }
            else {
              ${$objects}[@{$objects}]->{'-attributes'}->{$self->{'key'}->{'table_view'}} = $identifier1;
              ${$objects}[-1]->{'-attributes'}->{$self->{'key'}->{'column'}} = $identifier2;
              ${$objects}[-1]->{'-attributes'}->{$self->{'key'}->{'id'}} = "object_".$self->{'object_id_counter'};
              $self->{'object_id_counter'}++;
            }
          }
          else {
            return $self->do_err("identifier expected");
          }
        }
        else {
          ${$objects}[@{$objects}]->{'-attributes'}->{$self->{'key'}->{'column'}} = $identifier1;
          ${$objects}[-1]->{'-attributes'}->{$self->{'key'}->{'id'}} = "object_".$self->{'object_id_counter'};
          $self->{'object_id_counter'}++;
        }
      }
      else {
        return $self->do_err("identifier expected");
      }
    } while ( $self->{'sql'} =~ s/^,//is );
    $self->{'tmp'}->{$self->{'key'}->{'for_update_clause'}}->{$self->{'key'}->{'object'}} = $objects;
  }
  if ( $self->{'sql'} =~ s/^NOWAIT//is ) {
    $submatch = 1;
    $self->whitespace();
    $self->{'tmp'}->{$self->{'key'}->{'for_update_clause'}}->{$key_nowait}->{'-empty'} = 1;
  }
  elsif ( $self->{'sql'} =~ s/^WAIT//is ) {
    $submatch = 1;
    $self->whitespace();
    $self->{'tmp'}->{$self->{'key'}->{'for_update_clause'}}->{$key_wait}->{'-empty'} = 1;
    if ( $self->number() ) {
      $self->{'tmp'}->{$self->{'key'}->{'for_update_clause'}}->{$key_wait}->{'-attributes'}->{$self->{'key'}->{'value'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
      delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
      $self->whitespace();
    }
    else {
      return $self->do_err("integer expected");
    }
  }
  unless ( $submatch ) {
    $self->{'tmp'}->{$self->{'key'}->{'for_update_clause'}}->{'-empty'} = 1;
  }
  return 1;
}

sub dbms_hint {
  my $self = shift;
  my $hints;
  my $submatch = 0;
  my $identifier;
  if ( $self->{'sql'} =~ s/^\/\*\+//is ) {
    $submatch = 1;
  }
  elsif ( $self->{'sql'} =~ s/^--\+//is ) {
    $submatch = 2;
  }
  else {
    $self->whitespace();
    return 0;
  }
  # note: no whitespaces in this part!
  while ( $self->{'sql'} =~ s/^\s*(\w+)\s*//is ) {
    my $hint_name = $1;
    if ( $hint_name eq 'index' ) {
      unless ( $self->{'sql'} =~ s/^\s*\(\s*//is ) { return $self->do_err("opening bracket expected"); }
      unless ( $identifier = $self->identifier() ) { return $self->do_err("identifier expected"); }
      ${$hints}[@{$hints}]->{$key_index_hint}->{'-attributes'}->{$self->{'key'}->{'table'}} = $identifier;
      while ( $identifier = $self->identifier() ) {
        ${${$hints}[-1]->{$key_index_hint}->{$key_index_name}}[@{${$hints}[-1]->{$key_index_hint}->{$key_index_name}}]->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
      }
      unless ( $self->{'sql'} =~ s/^\s*\)\s*//is ) { return $self->do_err("closing bracket expected in hint"); }
    }
    else {
      return 0;
    }
  }
  $self->{'tmp'}->{$self->{'key'}->{'subquery'}}->{$self->{'key'}->{'dbms_optimizer_hint'}} = $hints;
  if ( $submatch == 1 ) { # multiline
    unless ($self->{'sql'} =~ s/^\s*\*\///is  ) { return $self->do_err("'*/' expected"); }
  }
  return 1;
}


######################################################################
#
# END SELECT
#
######################################################################




######################################################################
#
# MISC STUBS
#
######################################################################


# ony a stub
sub insert {
  my $self = shift;
  if ( $self->{'sql'} =~ s/^INSERT.*?;//is ) {
    my $match = $&; # matched string
    while ( $match =~ s/\n// ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
  }
}

sub commit {
  my $self = shift;
  $self->{'sql'} =~ s/^COMMIT//is;
  $self->whitespace();
  unless ( $self->{'sql'} =~ s/^$self->{'statement_end'}//iso  ) { return $self->do_err("End of statement expected after commit") }
}

sub alter {
  my $self = shift;
  $self->{'sql'} =~ s/^ALTER//is;
  $self->whitespace();
  if ( $self->{'sql'} =~ s/^INDEX//is ) {
    $self->whitespace();
    $self->alter_index();
  }
  elsif ( $self->{'sql'} =~ s/^TABLE//is ) {
    $self->whitespace();
    $self->alter_table();
  }
  elsif ( $self->{'sql'} =~ s/^TRIGGER//is ) {
    $self->whitespace();
    $self->alter_trigger();
  }
  else {
    return $self->do_err("alter statement not implemented");
  }
}

sub alter_index {
  my $self = shift;
  my $alter;
  $self->whitespace();
  my $identifier;
  if ( $identifier = $self->identifier() ) {
    if ( $self->{'sql'} =~ s/^\.//is  ) {
      $self->whitespace();
      $alter->{'-attributes'}->{$self->{'key'}->{'schema'}} = $identifier;
      unless ( $identifier = $self->identifier() ) { return $self->do_err("tablename expected"); }
      $alter->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    }
    else {
      $alter->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    }
  }
  if ( $self->rebuild_clause() ) {
    $alter->{$key_rebuild_clause} = $self->{'tmp'}->{$key_rebuild_clause};
    delete $self->{'tmp'}->{$key_rebuild_clause};
  }
  else {
    return $self->do_err("unknown alter_index statement");
  }

  ${ $self->{'sqldata'}}[@{ $self->{'sqldata'}}]->{$key_alter_index} = $alter;
  return $self->do_err("End of statement expected in alter") unless ( $self->{'sql'} =~ s/^$self->{'statement_end'}//iso  );
}

sub alter_table {
  my $self = shift;
  my $alter;
  $self->whitespace();
  my $identifier;
  if ( $identifier = $self->identifier() ) {
    if ( $self->{'sql'} =~ s/^\.//is  ) {
      $self->whitespace();
      $alter->{'-attributes'}->{$self->{'key'}->{'schema'}} = $identifier;
      unless ( $identifier = $self->identifier() ) { return $self->do_err("tablename expected"); }
      $alter->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    }
    else {
      $alter->{'-attributes'}->{$self->{'key'}->{'name'}} = $identifier;
    }
  }
  if ( $self->move_table_clause() ) {
    $alter->{$key_move_table_clause} = $self->{'tmp'}->{$key_move_table_clause};
    delete $self->{'tmp'}->{$key_move_table_clause};
  }
  else {
    return $self->do_err("unknown alter_table statement");
  }
  ${ $self->{'sqldata'}}[@{ $self->{'sqldata'}}]->{$key_alter_table} = $alter;
  return $self->do_err("End of statement expected in alter") unless ( $self->{'sql'} =~ s/^$self->{'statement_end'}//iso  );
}

sub rebuild_clause {
  my $self = shift;
  my $match = 0;
  my $rebuild;
  if ( $self->{'sql'} =~ s/^REBUILD//is  ) {
    $match = 1;
    $self->whitespace();
    while ( $self->rebuild_element() ) {
      ${$rebuild->{$key_rebuild_element}}[@{$rebuild->{$key_rebuild_element}}] = $self->{'tmp'}->{$key_rebuild_element};
      delete $self->{'tmp'}->{$key_rebuild_element};
    }

    $self->{'tmp'}->{$key_rebuild_clause} = $rebuild;
  }
  return $match;
}

sub rebuild_element {
  my $self = shift;
  my $match = 0;
  my $element;
  if ( $self->parallel_clause() ) {
    $match = 1;
    $element->{$key_parallel_clause} = $self->{'tmp'}->{$key_parallel_clause};
    delete $self->{'tmp'}->{$key_parallel_clause};
  }
  elsif ( $self->physical_attributes_clause() ) {
    $match = 1;
    $element->{$key_physical_attributes_clause} = $self->{'tmp'}->{$key_physical_attributes_clause};
    delete $self->{'tmp'}->{$key_physical_attributes_clause};
  }
  elsif ( $self->key_compression() ) {
    $match = 1;
    $element->{$key_key_compression} = $self->{'tmp'}->{$key_key_compression};
    delete $self->{'tmp'}->{$key_key_compression};
  }
  elsif ( $self->logging_clause() ) {
    $match = 1;
    $element->{$key_logging_clause} = $self->{'tmp'}->{$key_logging_clause};
    delete $self->{'tmp'}->{$key_logging_clause};
  }
  $self->{'tmp'}->{$key_rebuild_element} = $element;
  return $match;

}

sub move_table_clause {
  my $self = shift;
  my $match = 0;
  my $move;
  if ( $self->{'sql'} =~ s/^MOVE//is  ) {
    $match = 1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^ONLINE//is  ) {
      $self->whitespace();
      $move->{'-attributes'}->{$key_online} = 'online';
    }
    if ( $self->segment_attributes_clause() ) {
      $move->{$key_segment_attributes_clause} = $self->{'tmp'}->{$key_segment_attributes_clause};
      delete $self->{'tmp'}->{$key_segment_attributes_clause};
    }
    if ( $self->data_segment_compression() ) {
      $move->{$key_data_segment_compression} = $self->{'tmp'}->{$key_data_segment_compression};
      delete $self->{'tmp'}->{$key_data_segment_compression};
    }
    while ( $self->lob_storage_clause() ) {
      ${$move->{$key_lob_storage_clause}}[@{$move->{$key_lob_storage_clause}}]   = $self->{'tmp'}->{$key_lob_storage_clause};
      delete $self->{'tmp'}->{$key_lob_storage_clause};
    }
    if ( $self->parallel_clause() ) {
      $move->{$key_parallel_clause} = $self->{'tmp'}->{$key_parallel_clause};
      delete $self->{'tmp'}->{$key_parallel_clause};
    }
    $self->{'tmp'}->{$key_move_table_clause} = $move;
  }
  return $match;
}

sub global_partitioning_clause {
  my $self = shift;
  my $match = 0;
  my $identifier;
  if ( $self->{'sql'} =~ s/^PARTITION//is  ) {
    $match = 1;
    $self->whitespace();
    if ( $identifier = $self->idientifier() ) {
      $self->{'tmp'}->{$key_global_partitioning_clause}->{'-attributes'}->{$key_partition} = $identifier;
    }
    if ( $self->{'sql'} =~ s/^VALUES//is  ) {
      $self->whitespace();
      if ( $self->{'sql'} =~ s/^LESS//is  ) {
        $self->whitespace();
        if ( $self->{'sql'} =~ s/^THAN//is  ) {
          $self->whitespace();
          unless ( $self->{'sql'} =~ s/^\(//is ) { return $self->do_err("opening bracket expected in out_of_line_constraint"); }
          $self->whitespace();
          do {
            $self->whitespace();
            unless ( $identifier = $self->identifier() ) { return $self->do_err("identifier expected"); }
            ${$self->{'tmp'}->{$key_global_partitioning_clause}->{$self->{'key'}->{'value'}}}[@{$self->{'tmp'}->{$key_global_partitioning_clause}->{$self->{'key'}->{'value'}}}] =  $identifier;
          } while ( $self->{'sql'} =~ s/^,//is );
          $self->whitespace();
          unless ( $self->{'sql'} =~ s/^\)//is ) { return $self->do_err("closing bracket expected in out_of_line_constraint"); }
          $self->whitespace();
        }
        else {
          return $self->do_err("keyword 'THAN' expected");
        }
      }
      else {
        return $self->do_err("keyword 'LESS' expected");
      }
    }
    else {
      return $self->do_err("keyword 'VALUES' expected");
    }
    if ( $self->segment_attributes_clause() ) {
      $self->{'tmp'}->{$key_global_partitioning_clause}->{$key_segment_attributes_clause} = $self->{'tmp'}->{$key_segment_attributes_clause};
      delete $self->{'tmp'}->{$key_segment_attributes_clause};
    }
  }
  return $match;
}


sub create_trigger {
  my $self = shift;
  # not implemented
  if ( $self->{'sql'} =~ s/^TRIGGER.*?END;//is ) {
    my $match = $&; # matched string
    while ( $match =~ s/\n// ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
    $self->{'sql'} =~ s/^\///is;
    $self->whitespace();
  }
}

# only a stub
sub alter_trigger {
  my $self = shift;
  if ( $self->{'sql'} =~ s/^.*?;//is ) {
    my $match = $&; # matched string
    while ( $match =~ s/\n// ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
  }
}


sub create_sequence {
  my $self = shift;
  # not implemented
  if ( $self->{'sql'} =~ s/^SEQUENCE .*?;//is ) {
    my $match = $&; # matched string
    while ( $match =~ s/\n// ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
  }
}


1;