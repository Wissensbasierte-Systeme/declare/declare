#!/usr/bin/perl -w
####################################################################
#
# Small shell frontend for the SquashML parser.
#
####################################################################


# following lines add the dir of this program to @INC so that the modules are found
use Cwd qw/cwd chdir abs_path/;
use File::Basename qw/dirname/;
use lib abs_path(dirname($0));

use strict;
use warnings;
use vars qw($VERSION);

$VERSION = '1.0';

use XMLWriter;
use TransactSQLParser;
use MySQLParser;
use OracleParser;


if ( @ARGV != 2 ) {
  help();
  exit(0);
}

my $sql;

my $dialect = $ARGV[0];
my $FILENAME = $ARGV[1];

$/ = '\0';
open(FILE, $FILENAME) || die "Cannot open file $FILENAME.\n";
$sql = <FILE>;
close FILE;

# parse
my $parser;

if ($dialect eq 'mysql') {
  $parser = MySQLParser->new();
}
elsif ($dialect eq 'oracle') {
  $parser = OracleParser->new();
}
elsif ($dialect eq 'transactsql') {
  $parser = TransactSQLParser->new();
}

$parser->parse($sql);
#$parser->finalize_schema();
my $parsetree = $parser->tree();
my $varlist = $parser->varlist();

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime(time);
my $date = sprintf "%4d-%02d-%02d %02d:%02d:%02d", $year+1900,$mon+1,$mday,$hour,$min,$sec;
# output
my $description = <<EOF;
*** SquashML parser info header ***
Generator:   squashml_parser.pl
Parsedate:   $date
Inputfile:   $FILENAME
EOF
my $dtd = dirname($0).'/squashml.dtd';
my $writer = XMLWriter->new($dtd, $varlist, 1);
print $writer->to_xml($parsetree, $description);



sub help {
  warn "Usage: squashml_parser.pl [mysql|oracle|transactsql] <sqlfile>.\n\n".
    "The XML output of the program is written to stdout.\n";
  exit(0);
}

