####################################################################
package TransactSQLParser;
####################################################################
#
# Parses TransactSQL code into SquashML.
#
####################################################################

use strict;
use warnings;
use vars qw(@ISA $VERSION);

use SquashMLParser;
@ISA = qw(SquashMLParser);

$VERSION = '1.0';


sub new {
  my $this = shift;
  my $class = ref($this) || $this;
  my $self = {};
  bless ($self, $class);
  $self->initialize();
  $self->{'tree'}->{'-attributes'}->{'sqldialect'} = 'transactsql';
  $self->{'tree'}->{'-attributes'}->{'version'} = $VERSION;
  return $self;
}

sub initialize {
  my $self = shift;
  $self->SquashMLParser::initialize();
  $self->{'statement_end'} = '';
  @{$self->{'RESERVED_WORDS'}} = qw(ADD EXCEPT PERCENT ALL EXEC PLAN 
ALTER EXECUTE PRECISION AND EXISTS PRIMARY ANY EXIT PRINT AS FETCH
PROC ASC FILE PROCEDURE AUTHORIZATION FILLFACTOR PUBLIC BACKUP FOR
RAISERROR BEGIN FOREIGN READ BETWEEN FREETEXT READTEXT BREAK
FREETEXTTABLE RECONFIGURE BROWSE FROM REFERENCES BULK FULL 
REPLICATION BY FUNCTION RESTORE CASCADE GOTO RESTRICT CASE GRANT
RETURN CHECK GROUP REVOKE CHECKPOINT HAVING RIGHT CLOSE HOLDLOCK
ROLLBACK CLUSTERED IDENTITY ROWCOUNT COALESCE IDENTITY_INSERT
ROWGUIDCOL COLLATE IDENTITYCOL RULE COLUMN IF SAVE COMMIT IN
SCHEMA COMPUTE INDEX SELECT CONSTRAINT INNER SESSION_USER CONTAINS
INSERT SET CONTAINSTABLE INTERSECT SETUSER CONTINUE INTO SHUTDOWN
CONVERT IS SOME CREATE JOIN STATISTICS CROSS KEY SYSTEM_USER
CURRENT KILL TABLE CURRENT_DATE LEFT TEXTSIZE CURRENT_TIME LIKE
THEN CURRENT_TIMESTAMP LINENO TO CURRENT_USER LOAD TOP CURSOR 
NATIONAL TRAN DATABASE NOCHECK TRANSACTION DBCC NONCLUSTERED
TRIGGER DEALLOCATE NOT TRUNCATE DECLARE NULL TSEQUAL DEFAULT
NULLIF UNION DELETE OF UNIQUE DENY OFF UPDATE DESC OFFSETS
UPDATETEXT DISK ON USE DISTINCT OPEN USER DISTRIBUTED
OPENDATASOURCE VALUES DOUBLE OPENQUERY VARYING DROP OPENROWSET
VIEW DUMMY OPENXML WAITFOR DUMP OPTION WHEN ELSE OR WHERE END
ORDER WHILE ERRLVL OUTER WITH ESCAPE OVER WRITETEXT);

  $self->{'key'}->{'dbms_identity'} = 'transactsql_identity';
  $self->{'key'}->{'dbms_create_table'} = 'transactsql_create_table';
  $self->{'key'}->{'filegroup'}  = 'filegroup';
  $self->{'key'}->{'inc'} = 'inc';
  $self->{'key'}->{'seed'} = 'seed';
}


sub dbms_statement {
  my $self = shift;
  if ( $self->{'sql'} =~ m/^IF/is ) {
    $self->if_sub();
  }
  elsif ( $self->{'sql'} =~ m/^GO/is ) {
    $self->go();
  }
  else {
     return 0;
  }

  return 1;
}


sub if_sub {
  my $self = shift;
  $self->{'sql'} =~ s/^IF//is;
  $self->whitespace();
  unless ( $self->{'sql'} =~ s/^EXISTS//is ) {
    return $self->do_err("Unknown IF statement.");
  }
  $self->whitespace();
  if ( $self->{'sql'} =~ s/^\(.*?\)//is ) {
    my $match = $&; # matched string
    while ( $match =~ s/\n// ) {
      $self->{'linecount'}++;
    }
    $self->whitespace();
  }
}


sub go {
  my $self = shift;
  $self->{'sql'} =~ s/^GO//is;
  $self->whitespace();
}



######################################################################
#
# CREATE TABLE DBMS EXTENSION
#
######################################################################

sub  dbms_create_table {
  my $self = shift;
  my $match = 0;
  my $filegroup;

  if ( $self->{'sql'} =~ s/^ON//is ) {
    $match = 1;
    $self->whitespace();
    unless ( $filegroup = $self->identifier() ) {
       return $self->do_err("identifier expected");
    }
    $self->{'tmp'}->{$self->{'key'}->{'dbms_create_table'}}->{$self->{'key'}->{'filegroup'}}->{'-empty'} = 1;
    $self->{'tmp'}->{$self->{'key'}->{'dbms_create_table'}}->{$self->{'key'}->{'filegroup'}}->{'-attributes'}->{$self->{'key'}->{'name'}} = $filegroup;
  }
  return $match;
}


sub dbms_identity {
  my $self = shift;
  my $match = 0;
  my $seed;
  my $inc;
  if ( $self->{'sql'} =~ s/^IDENTITY//is ) {
    $match = 1;
    $self->whitespace();
    $self->{'tmp'}->{$self->{'key'}->{'dbms_identity'}}->{'-empty'} = 1;
    if ( $self->{'sql'} =~ s/^\(//is ) {
       $self->whitespace();
       unless ( $self->number() ) {
         return $self->do_err("number expected");
       }
       $self->{'tmp'}->{$self->{'key'}->{'dbms_identity'}}->{'-attributes'}->{$self->{'key'}->{'seed'}} =  $self->{'tmp'}->{$self->{'key'}->{'num'}};
       delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
       if ( $self->{'sql'} =~ s/^,//is ) {
         $self->whitespace();
         unless ( $self->number() ) {
           return $self->do_err("number expected");
         }
         $self->{'tmp'}->{$self->{'key'}->{'dbms_identity'}}->{'-attributes'}->{$self->{'key'}->{'inc'}} = $self->{'tmp'}->{$self->{'key'}->{'num'}};
         delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
       }
       unless ( $self->{'sql'} =~ s/^\)//is ) {
         return $self->do_err("closing bracket expected");
       }
       $self->whitespace();
    }
  }
  return $match;
}


# we override idenifier
sub identifier {
  my $self = shift;
  if ( $self->{'sql'} =~ m/^$self->{'RESERVED_WORDS_REGEXP'}(\s|;|\)|\(|\.)/iso ) {
    return undef;
  }
  if ( $self->{'sql'} =~ s/^[a-z][a-z_0-9]*//is ) {
    my $val = $&;
    $self->whitespace();
    return uc $val;
  }
  elsif ( $self->{'sql'} =~ s/^\[?((?:&&)?[a-zA-Z][a-zA-Z_0-9]*)\]?//is ) {
    my $val = $1;
    $self->whitespace();
    return uc $val;
  }
  else {
   return undef;
  }
}


sub datatype {
  my $self = shift;
  my $type = "";
  my $quant = "";
  if ( $self->{'sql'} =~ m/^(NOT|NULL)/is ) { return undef; } # no type info suplied
  if ( $self->{'sql'} =~ s/^\[?(\w+)\]?//is ) {
    $type = uc $1;
    $self->whitespace();
    if ( $self->{'sql'} =~ s/^\(//is ) {
       $self->whitespace();
       unless ( $self->number() ) {
         return $self->do_err("number expected");
       }
       $quant = "(".$self->{'tmp'}->{$self->{'key'}->{'num'}};
       delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
       if ( $self->{'sql'} =~ s/^,//is ) {
         $self->whitespace();
         $quant .= ",";
         unless ( $self->number() ) {
           return $self->do_err("number expected");
         }
         $quant .= $self->{'tmp'}->{$self->{'key'}->{'num'}};
         delete $self->{'tmp'}->{$self->{'key'}->{'num'}};
       }
       unless ( $self->{'sql'} =~ s/^\)//is ) {
         return $self->do_err("closing bracket for datatype expected");
       }
       $self->whitespace();
       $quant .= ")";
    }
    return $type.$quant; # return datatype in uppercase letters
  }
  else {
   return undef;
  }
}

1;