

/******************************************************************/
/***                                                            ***/
/***          Squash:  Oracle Interface                         ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      oracle_result_atom/1.
:- multifile
      oracle_result_atom/1.


:- dislog_variable_set(oracle_connect, shell).


/*** interface ****************************************************/


/* oracle_execute(Sql_Statements) <-
      */

oracle_execute(Sql_Statements) :-
   oracle_execute(writeln, Sql_Statements).

oracle_execute(Write_Predicate, Sql_Statements) :-
   dislog_variable_get(home, DisLog_Directory),
   concat(DisLog_Directory, '/results/oracle_statements', File_1),
   predicate_to_file( File_1,
      ( Goal =.. [Write_Predicate, Sql_Statements],
        call(Goal) ) ),
   concat(DisLog_Directory, '/results/oracle_result', File_2),
   oracle_execute_file_to_file(File_1, File_2).


/* oracle_execute_file_to_file(File_1, File_2) <-
      */

oracle_execute_file_to_file(File_1, File_2) :-
   current_prolog_flag(unix, true),
   global_variable_get(squash, oracle_sqlplus, Sqlplus),
   concat( ['cat ', File_1, ' | ', Sqlplus,
      ' | grep -v ''^$'' > ',File_2], Command ),
   shell(Command, _).
% oracle_execute_file_to_file(File_1, File_2) :-
%    current_prolog_flag(windows, true),
%    concat(
%       ['C:/oracle/bin/oracle < ',File_1,' > ',File_2],
%       Command_1 ),
%    unix_command_to_windows_command(Command_1, Command_2),
%    concat('command.com /C ', Command_2, Command_3),
%    shell(Command_3).


/* oracle_select_execute(Sql_Command, Tuples) <-
      */

oracle_select_execute(Sql_Command, Tuples) :-
   dislog_variable_get(oracle_connect, shell),
   !,
   oracle_select_common(
      oracle_select_command_write,
      Sql_Command, Tuples ).


/* oracle_select_common(Sql_Statements, Tuples) <-
      */

oracle_select_common(Sql_Statements, Tuples) :-
   oracle_select_common(
      writeln,
      Sql_Statements, Tuples ).

oracle_select_common(Write_Predicate, Sql_Statements, Tuples) :-
   ( oracle_select_common_2(Write_Predicate, Sql_Statements, Tuples)
   ; Tuples = [] ).

oracle_select_common_2(Write_Predicate, Sql_Statements, Tuples) :-
   oracle_execute(Write_Predicate, Sql_Statements),
   style_check(-singleton),
   dislog_variable_get(home, DisLog_Directory),
   concat(DisLog_Directory, '/results/oracle_result', File),
   [File],
   style_check(+singleton),
   collect_arguments(oracle_result_atom,[_|Tuples]),
   retractall(oracle_result_atom(_)).


/* oracle_select_command_write(Sql_Command) <-
      */

oracle_select_command_write(Sql_Command) :-
   Select := Sql_Command^select,
   !,
   [Use] := Sql_Command^use,
   Select := Sql_Command^select,
   From := Sql_Command^from,
   writeln('set heading off'),
   writeln('set feedback off'),
   writeln('set termout off'),
   write('select ''oracle_result_atom(['' || chr(9) || '''),
   oracle_print_items(', ''|| chr(9) || '' ',Select),
   writeln(''' || chr(9) || '']).'''),
   writeln('from dual;'),
   writeln('select ''oracle_result_atom(['' || chr(9) || '),
   oracle_print_items('|| '','' || chr(9) ||  ',Select),
   writeln(' || chr(9) ||  '']).'''),
   concat(Use, '.', Database),
   write('from '), oracle_print_items(', ',From, Database), nl,
   call_or_true(
      ( [Where] := Sql_Command^where,
        write('where '), oracle_print_boolean(Where), nl ) ),
   call_or_true(
      ( Group_By := Sql_Command^group_by,
        write('group by '),
        oracle_print_items(', ',Group_By), write(' ') ) ),
   call_or_true(
      ( Order_By := Sql_Command^order_by,
        write('order by '),
        oracle_print_items(', ',Order_By), write(' ') ) ),
   writeln(';').
oracle_select_command_write(Sql_Command) :-
   Select := Sql_Command^select_distinct,
   !,
   [Use] := Sql_Command^use,
   From := Sql_Command^from,
   writeln('set heading off'),
   writeln('set feedback off'),
   writeln('set termout off'),
   write(
      'select distinct ''oracle_result_atom(['' || chr(9) || '''),
   oracle_print_items(', ''|| chr(9) || '', ',Select),
   writeln(''' || chr(9) || '']).'''),
   writeln('from dual;'),
   writeln(
      'select distinct ''oracle_result_atom(['' || chr(9) || '),
   oracle_print_items(', '' || chr(9) ||  '', ',Select),
   writeln(' || chr(9) ||  '']).'''),
   concat(Use, '.', Database),
   write('from '), oracle_print_items(', ',From, Database), nl,
   call_or_true(
      ( [Where] := Sql_Command^where,
        write('where '), oracle_print_boolean(Where), nl ) ),
   call_or_true(
      ( Group_By := Sql_Command^group_by,
        write('group by '), oracle_print_items(', ',Group_By) ) ),
   call_or_true(
      ( Order_By := Sql_Command^order_by,
        write('order by '), oracle_print_items(', ',Order_By) ) ),
   writeln(';').
oracle_select_command_write(Sql_Command) :-
   Select := Sql_Command^select_count_distinct,
   !,
   [Use] := Sql_Command^use,
   From := Sql_Command^from,
   writeln('set heading off'),
   writeln('set feedback off'),
   writeln('set termout off'),
   concat(['select ''oracle_result_atom(['' ',
      '|| chr(9) || ''count(*)'' || chr(9) || '']).''  '], T_1 ),
   write(T_1),
   writeln(' '),
   writeln('from dual;'),
   concat( ['select ''oracle_result_atom(['' ',
      '|| chr(9) || count(*)  || chr(9) ||  '']).'' from ',
      '( select distinct   '], T_2 ),
   writeln(T_2),
   oracle_print_items(', ',Select),
   writeln(' '),
   concat(Use, '.', Database),
   write('from '), oracle_print_items(', ',From, Database), nl,
   call_or_true(
      ( [Where] := Sql_Command^where,
        write('where '), oracle_print_boolean(Where), nl ) ),
   call_or_true(
      ( Group_By := Sql_Command^group_by,
        write('group by '), oracle_print_items(', ',Group_By) ) ),
   call_or_true(
      ( Order_By := Sql_Command^order_by,
        write('order by '), oracle_print_items(', ',Order_By) ) ),
   writeln(' );').


/*** implementation ***********************************************/


/* oracle_print_boolean(Condition) <-
      */

oracle_print_boolean(X and Y) :-
   oracle_print_boolean(X),
   write(' and '),
   oracle_print_boolean(Y).
oracle_print_boolean((X or Y)) :-
   write(' ( '),
   oracle_print_boolean(X),
   write(' or '),
   oracle_print_boolean(Y),
   write(' ) ').
oracle_print_boolean(A = B) :-
   oracle_print_item(A),
   write(' = '),
   oracle_print_item(B).
oracle_print_boolean(A like B) :-
   oracle_print_item(A),
   write(' like '),
   oracle_print_item(B).
oracle_print_boolean(A >= B) :-
   oracle_print_item(A),
   write(' >= '),
   oracle_print_item(B).
oracle_print_boolean(A =< B) :-
   oracle_print_item(A),
   write(' <= '),
   oracle_print_item(B).
oracle_print_boolean(A \= B) :-
   oracle_print_item(A),
   write(' <> '),
   oracle_print_item(B).


/* oracle_print_items(Separator, Items) <-
      */

oracle_print_items(Separator, Items) :-
   oracle_print_items(Separator, Items, '').

oracle_print_items(Separator, [X1, X2|Xs], Prefix) :-
   write(Prefix), oracle_print_item(X1), write(Separator),
   oracle_print_items(Separator, [X2|Xs], Prefix).
oracle_print_items(_, [X1], Prefix) :-
   write(Prefix), oracle_print_item(X1).
oracle_print_items(_, [], _).


/* oracle_print_item(Item) <-
      */

oracle_print_item(Item) :-
   nonvar(Item),
   Item =.. [Op, X],
   member(Op, [min, max, avg, sum, count,
      vsize, lower, upper]),
   !,
   write(Op), write('('),
   oracle_print_item(X),
   write(')').
oracle_print_item(Item) :-
   var(Item),
   !,
   write(Item).

oracle_print_item(X*Y) :-
   oracle_print_item(X),
   write('*'),
   oracle_print_item(Y).
oracle_print_item(X/Y) :-
   oracle_print_item(X),
   write('/'),
   oracle_print_item(Y).
oracle_print_item(O-Item) :-
   !,
   write_list([O,' ',Item]).
oracle_print_item(O^Item) :-
   !,
   write_list([O,'.',Item]).
oracle_print_item({{Item}}) :-
   !,
   write(''''''''', '),
   oracle_print_item(Item),
   write(', ''''''''').
oracle_print_item({Item}) :-
   !,
   write(''''),
   oracle_print_item(Item),
   write('''').
oracle_print_item(Item) :-
   write(Item).


/*** tests ********************************************************/


test(squash:squash_oracle_interface, oracle_select_execute) :-
   Select = [ use:[ 'USRSEARCH' ],
      select:[ count(*) ],
      from:[ 'TBLENZYMES' ] ],
   oracle_select_execute(Select, [[Count]]),
   writeln(Count).


/******************************************************************/


