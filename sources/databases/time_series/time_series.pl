

/******************************************************************/
/***                                                            ***/
/***          Declare:  Time Series                             ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_get(
      home, '/projects/KI_Nergy/data/', Path),
   dislog_variable_set(ki_nergy, Path).


/*** tests ********************************************************/


test(decreasing_parabolically, 1) :-
   Ys = [9, 9.5, 9.75, 9, 7, 4, 0],
%  Xs = [9, 10, 7, 3, 1.9, 1.8],
%  Zs = [3, 5, 7],
%  append([Xs, Zs, Xs], Ys),
   list_of_elements_to_relation(Ys, Series),
   decreasing_parabolically(N, 1, 6, Series, Subseries),
   writeln(N-Subseries),
   graph_plot_list('Test', Subseries).

test(decreasing_parabolically, 2) :-
%  File = 'data_001_000.csv',
   File = 'data_010_000.csv',
   dislog_variable_get(ki_nergy, File, Path),
   dread(csv(';'), Path, [_|Rows]),
   !,
   maplist( time_series_row_to_numbers, Rows, Series ),
   decreasing_parabolically(N, 2, 6, Series, Subseries),
   writeln(N-Subseries),
   graph_plot_list('Test', Subseries).

test(decreasing_parabolically, 3) :-
   File = 'data_010_000',
%  File = 'data_500_000',
   dislog_variable_get(ki_nergy, File, Path),
   concat(Path, '.csv', Path_1),
   concat(Path, '.pl', Path_2),
   write_list(user, ['<--- ', Path_1, '\n']),
   read_file_to_csv_row_facts(Path_1),
   time_series_rows_to_numbers,
   !,
   write_list(user, ['---> ', Path_2, '\n']),
   predicate_to_file( Path_2,
      listing(time_series_row/2) ).

test(decreasing_parabolically, 4) :-
   File = 'data_010_000.pl',
%  File = 'data_500_000.pl',
   dislog_variable_get(ki_nergy, File, Path),
   write_list(user, ['<--- ', Path, '\n']),
   reconsult(Path),
   test(time_series_rows, 1).

test(decreasing_parabolically, 5) :-
%  works on facts time_series_row(N, Xs)
%  A1 = 'Kessel RL',
   A2 = 'Kessel VL',
   time_series_attribute_to_number(A2, K2),
   time_series_peaks(K2, Peaks),
   nl,
   length(Peaks, N), writelnq(N).

test(decreasing_parabolically, 6) :-
   A1 = 'Kessel RL',
   A2 = 'Kessel VL',
   time_series_attribute_to_number(A1, K1),
   time_series_attribute_to_number(A2, K2),
   writeln(user, [A1=K1, A2=K2]),
   decreasing_parabolically(N, K1, 6, Subseries_1),
   \+ decreasing_parabolically(N, K2, 6, _),
   nth_subsequence_selected(N, 20, K1, Subseries_2),
   nth_subsequence_selected(N, 6,  K2, Subseries_3),
   nth_subsequence_selected(N, 20, K2, Subseries_4),
   time_series_plot_list(N,  6, A1, Subseries_1),
   time_series_plot_list(N, 20, A1, Subseries_2),
   time_series_plot_list(N,  6, A2, Subseries_3),
   time_series_plot_list(N, 20, A2, Subseries_4).

test(decreasing_parabolically, 7) :-
%  Attribute = 'WWT sek RL',     %  2
%  Attribute = 'WWT sek VL',     % 12
   Attribute = 'Kessel VL',      % 13
   time_series_attribute_to_number(Attribute, Column),
   N =  5,
   M = 45,
   time_series_n_peaks_in_m_minutes(Column, N, M, _Subseries).

test(decreasing_parabolically, 8) :-
%  Attribute = 'WWT sek RL',     %  2
%  Attribute = 'WWT sek VL',     % 12
   Attribute = 'Kessel VL',      % 13
   time_series_attribute_to_number(Attribute, Column),
   N =  5,
   M = 45,
   findall( Subseries,
      time_series_n_peaks_in_m_minutes(
         Column, N, M, Subseries),
      Xs ),
   length(Xs, Count),
   writeln(user, Count).

test(time_series_rows, 1) :-
   findall( N, time_series_row(N, _), Table ),
   length(Table, M),
   write_list(user, [M, ' rows in time series\n']).


/*** interface ****************************************************/


/* time_series_n_peaks_in_m_minutes(
         Column, Peaks, N, M, Subseries) <-
      */

time_series_n_peaks_in_m_minutes(Column, N, M, Subseries) :-
   time_series_peaks(Column, Peaks),
   nl(user),
   time_series_n_peaks_in_m_minutes(
      Column, Peaks, N, M, Subseries).

time_series_peaks(Column, Peaks) :-
   findall( N,
      ( decreasing_parabolically(N, Column, 6, _),
        ( 1 is N mod 100 -> write('+'), ttyflush
        ; true ) ),
      Peaks ).

time_series_n_peaks_in_m_minutes(
      Column, Peaks, N, M, Subseries) :-
   nth_subsequence(_, N, Peaks, Subseries_2),
   first(Subseries_2, I1),
   last(Subseries_2, I2),
   I2 - I1 < M,
   J1 <= maximum(I1 - 5, 2),
   J2 is I2 + 5,
   maplist( add(1), Subseries_2, Subseries),
   writeln(user, Subseries),
   time_series_attribute_to_number(Attribute, Column),
%  term_to_atom(Attribute:Subseries, Title),
%  concat([I1, ' to ', I2], Title),
   Title = Attribute,
   graph_plot_time_series_range(Title, Column, J1, J2).


/* decreasing_parabolically(
         Start, Column, Length, Series, Subseries) <-
      */

decreasing_parabolically(Start, Column, Length, Subseries) :-
   nth_subsequence(Start, Length, Rows),
   maplist( nth(Column),  Rows, Subseries),
   Subseries = [V1, V2|Vs],
   ( number(V1), number(V2),
     V1 =< V2
   ; fail ),
   decreasing_sequence([V2|Vs]).

decreasing_parabolically(
      Start, Column, Length, Series, Subseries) :-
   nth_subsequence(Start, Length, Series, Rows),
   maplist( nth(Column),  Rows, Subseries),
   Subseries = [V1, V2|Vs],
   ( number(V1), number(V2),
     V1 =< V2
   ; fail ),
   decreasing_sequence([V2|Vs]).


/* nth_subsequence_selected(
         Start, Length, Column, Subseries) <-
      */

nth_subsequence_selected(Start, Length, Column, Subseries) :-
   nth_subsequence(Start, Length, Rows),
   maplist( nth(Column),
      Rows, Subseries ).


/* nth_subsequence(Start, Length, Xs, Ys) <-
      Xs = [a_1,...,a_n],
      Ys = [a_k,...,a_l],
      k = Start, l = Start+Length-1 */

nth_subsequence(_, 0, []).
nth_subsequence(Start, Length, [Y|Ys]) :-
   Length > 0,
   time_series_row(Start, Y),
   Start > 0,
   Start_2 is Start + 1,
   Length_2 is Length - 1,
   nth_subsequence(Start_2, Length_2, Ys).

nth_subsequence(1, Length, Xs, Ys) :-
   first_n_elements(Length, Xs, Ys),
   length(Ys, Length).
nth_subsequence(Start, Length, [_|Xs], Subseries) :-
   nth_subsequence(Start_2, Length, Xs, Subseries),
   Start is Start_2 + 1.


/* decreasing_sequence(Xs) <-
      */

decreasing_sequence([X1, X2|Xs]) :-
   !,
   ( number(X1), number(X2),
     X1 > X2
   ; !, fail ),
   decreasing_sequence([X2|Xs]).
decreasing_sequence(_).


/* time_series_row_to_numbers(Row_1, Row_2) <-
      */

time_series_row_to_numbers([X|Xs], [X|Ys]) :-
   maplist( term_to_atom_time_series, Ys, Xs ).

term_to_atom_time_series('', '') :-
   !.
term_to_atom_time_series(X, Y) :-
   term_to_atom(X, Y).


/* time_series_rows_to_numbers <-
      */

time_series_rows_to_numbers :-
   retract_all(time_series_row(_, _)),
   time_series_rows_to_numbers_loop,
   nl.

time_series_rows_to_numbers_loop :-
   retract(csv_row(N, Tuple_1)),
   ( N > 0 ->
     time_series_row_to_numbers(Tuple_1, Tuple_2),
     assert(time_series_row(N, Tuple_2)),
     ( 0 is N mod 10000 -> write('*'), ttyflush
     ; true )
   ; assert(time_series_row(N, Tuple_1)) ),
   !,
   time_series_rows_to_numbers_loop.
time_series_rows_to_numbers_loop.


/* time_series_attribute_to_number(Attribute, N) <-
      */

time_series_attribute_to_number(Attribute, N) :-
   time_series_row(0, Attributes),
   nth(N, Attributes, Attribute).


/* time_series_plot_list(N, M, A, Subseries) <-
      */

time_series_plot_list(N, M, A, Subseries) :-
   concat([A, ' - ', M], Title), 
   writelnq(N-Subseries),
   graph_plot_list(Title, Subseries).


/* graph_plot_time_series_range(Title, Column, Start, End) <-
      */

graph_plot_time_series_range(Title, Column, Start, End) :-
   findall( Value,
      ( between(Start, End, L),
        time_series_row(L, Row),
        nth(Column, Row, Value) ),
      Values ),
   graph_plot_list(Title, Start, Values).


/* graph_plot_list(Title, Start, Values) <-
      */

graph_plot_list(Title, Start, Values) :-
   list_to_numbered_pairs(Start, Values, Pairs),
   writeln(user, Pairs),
   graph_plot(Title, Pairs).

list_to_numbered_pairs(Start, Values, Pairs) :-
   length(Values, N),
   I1 is Start,
   I2 is Start + N - 1,
   generate_interval(I1, I2, Is),
   ( foreach(I, Is), foreach(Value, Values),
     foreach(Pair, Pairs_2) do Pair = [I, Value] ),
   findall( [X, Y],
      ( member([X, Y], Pairs_2),
        Y \= '' ),
      Pairs ).

graph_plot_list(Title, Values) :-
   length(Values, N),
   generate_interval(1, N, Is),
   ( foreach(I, Is), foreach(Value, Values),
     foreach(Pair, Pairs) do Pair = [I, Value] ),
   writeln(user, Pairs),
   graph_plot(Title, Pairs).


/******************************************************************/


