

/******************************************************************/
/***                                                            ***/
/***          PDiaX:  GUI                                       ***/
/***                                                            ***/
/******************************************************************/


/*
:- use_module(library(pce)).
:- use_module(library(pce_style_item)).

:- ['sources/stock_tool/xpce_interfaces'].
:- ['sources/stock_tool/stock_elementary'].
*/

:- dynamic
      symptom_value/6.

symptom_value(A, B, C, D, E, F) :-
   value_type(A, B, C, D, E, F).


/*** interface ****************************************************/


/* user_dialog <-
      simple user-dialog without testing the validity
      of the result */ 

user_dialog(Identifier, Answer, Conficence_factor) :- 
%  first check if the answer already exists in the database
   pdiax_knowledge_base:user_dialog_answer(
      Identifier, Answer, Conficence_factor).

user_dialog(Identifier, Converted_answer, Conficence_factor) :-
%  check that fact doesn't exists already
   \+ pdiax_knowledge_base:user_dialog_answer(Identifier, _, _),

%  get user-question
   user_question(Identifier, Question, Explanation, Comment, 
      Value_identifier, Use, Cost, Conficence_factor),

%  check existence of value-type
   symptom_value(Value_identifier, Type,
         Lower_bound, Upper_bound, Unit, Range), 

%  get symptom-value-Type
   Symptom_value =  
      symptom_value(Value_identifier, Type,
         Lower_bound, Upper_bound, Unit, Range), 

   xpce_dialog(Question, Explanation, Comment, 
      Symptom_value, Use, Cost, Answer),
      
%  convert ja to true, falsch to false etc.
   convert_german_english(Answer, Converted_answer),   

%  store result
   assert(pdiax_knowledge_base:user_dialog_answer(
      Identifier, Converted_answer, Conficence_factor)).
   
   
/* xpce_dialog <-
      ask the user for an answer. */

xpce_dialog(Question, Explanation, Comment, 
      Symptom_value, Use, Cost, Answer) :-

   Symptom_value = symptom_value(_, Type,_, _ , _ ,_ ), 

   create_dlabel("Frage: ", Question, Label_question),
   create_dlabel("Erklärung: ", Explanation, Label_explanation),
   create_dlabel("Kommantar: ", Comment, Label_comment),
   create_dlabel("Nutzen: ", Use, Label_use),
   create_dlabel("Kosten: ", Cost, Label_cost),
   create_dlabel("Antworttyp: ", Type, Label_type),

   new(D, dialog('Benutzerfrage')),
   send(D, append, text(Label_question, center)),
   send(D, append, new(label)),

%  create the element for the user-answer and the given symptom_value
   create_answer_box(D, Type, Symptom_value, T_answer),

   send(D, append, text(Label_type)),
   send(D, append, new(label)),
   send(D, append, text(Label_explanation, center)),
   send(D, append, text(Label_comment, center)),
   send(D, append, text(Label_use, center)),
   send(D, append, text(Label_cost, center)),
   send(D, append, new(label)),
   send(D, append, button('Übernehmen',
      message(D, return, ok))),
   send(D, append, button('Abbrechen',
      message(D, return, cancel))),
   send(D, default_button, 'Übernehmen'),

%  get answer
   get(D, confirm, Return_value), 
   writeln(user, get_answer(Type, T_answer, Answer)),

%  evalute result
   ( ( Return_value = cancel,
       !, 
       send(D, destroy),
       fail )
   ; ( get_answer(Type, T_answer, Answer),
       send(D, destroy),
       pdiax_writef('Answer of the user-dialog: '),
       pdiax_writef(Answer) ) ).


/* create_dlabel(Label_head, Value, Atom_label) <-
      creates a label with the value as a atom. */

create_dlabel(Label_head, Value, Atom_label) :-
   \+ atomic(Value),
   append(Label_head, Value, Label_list),
   string_to_atom(Label_list, Atom_label). 
create_dlabel(Label_head, Value, Atom_label) :-
   atomic(Value),
   string_to_atom(Label_head, Atom_Head),
   concat(Atom_Head, Value, Atom_label). 


/* create_answer_box(D,Type,Symptom_value,T_Answer) <-
      creates an answer box with for the given question
      type. */

create_answer_box(D, boolean, _, _) :-
   dislog_variable_set(user_dialog_result, ja),
   send_choice_menu_gen(
      D, 'Antwort:', user_dialog_result, [ja, nein]).

create_answer_box(D, Type, _, T_Answer) :-
   member(Type, [string, time]),
   send(D, append, new(T_Answer, text_item('Antwort:', ''))).

create_answer_box(D, Type, Symptom_value, T_Answer) :-
   Symptom_value =
      symptom_value(_,_, Lower_bound, Upper_bound,_, _),
   member(Type, [integer, double]),
   send(D, append, new(T_Answer, text_item('Antwort:', ''))),
   create_dlabel(
      "Untergrenze: ", Lower_bound, Label_lower_bound),
   create_dlabel(
      "Obergrenze: ", Upper_bound, Label_upper_bound),
   send(D, append, text(Label_lower_bound)),
   send(D, append, text(Label_upper_bound)).

create_answer_box(D, multiple_choice, Symptom_value, _) :-
%  multiple_choice, preselect first item from range
   Symptom_value = symptom_value(_,_,_,_,_,Range),
   Range = [Head|_],
   dislog_variable_set(user_dialog_result, Head),
   send_choice_menu_gen(
      D, 'Antwort:', user_dialog_result, Range).

create_answer_box(D, single_choice, Symptom_value, _):-
%  single_choice, preselect first item from range
   Symptom_value = symptom_value(_,_,_,_,_, Range),
   Range = [Head|_],
   dislog_variable_set(user_dialog_result, Head),
   send_choice_menu_gen(
      D, 'Antwort:', user_dialog_result, Range).


/* get_answer(Type, _, Answer) <-
      returns the answer for the given value type */

get_answer(Type, _, Answer) :-
   member(Type, [boolean, single_choice, multiple_choice]),
   dislog_variable_get(user_dialog_result, Answer).

get_answer(Type, T_answer, Answer) :-
   member(Type, [string, integer, double, time]),
   get(T_answer, selection, Answer_string),
   string_to_atom(Answer_string, Answer).


/* convert_german_english(German,English) <-
      */

convert_german_english(nein, false).   
convert_german_english(falsch, false).   

convert_german_english(ja, true).   
convert_german_english(wahr, true).   

convert_german_english(Answer, Answer) :-
   \+ member(Answer, [ja, nein, wahr, falsch]).


/******************************************************************/


