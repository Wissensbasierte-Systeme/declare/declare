

/******************************************************************/
/***                                                            ***/
/***          PDiaX:  Tpe-Evidences                             ***/
/***                                                            ***/
/******************************************************************/


/*
:- module( pdiax_evidences, [
      aggregate_evidences/3, 
      calculate_evidence_of_conjunction_list/2, 
      calculate_new_conjunction_evidence/3,
      calculate_evidence_of_disjunction_list/2, 
      calculate_new_disjunction_evidence/3,
      combine_evidences/3,
      combine_evidences_of_list/2,
      default_evidence_of_true_fact/1,
      default_neutral_evidence/1,
      suspected_facts_evidence_threshold/1, 
      proven_facts_evidence_threshold/1 ] ).
*/


/* PDiaX:
      the evidences according Puppe96 and variations */


/* minimal evidence to be used for deduction of other evidences */

proven_facts_evidence_threshold(0.8).

suspected_facts_evidence_threshold(0.6).

default_evidence_of_true_fact(1.0).


/* default_neutral_evidence(E) <-
      default neutral evidence for conjunctions. */

default_neutral_evidence(1).


/*** interface ****************************************************/


/* combine_evidences(Evidence_1,Evidence_2,Evidence_3) <-
      combines two evidences without any restriction */

combine_evidences(Evidence_1,Evidence_2,Evidence_3) :-
   combine_evidences_(Evidence_1,Evidence_2,Evidence_3).


/* calculate_evidence_of_conjunction_list_(Es,E) <-
      calculates the overall evidence of a conjunction
      of evidences applying some restrictions.
      used for calculating the overall evidence
      of rule bodies etc. */

calculate_evidence_of_conjunction_list(E1, E2) :-
   calculate_evidence_of_conjunction_list_(E1, E2).

   
/* calculate_evidence_of_disjunction_list_(Es,E) <-
      calculates the overall evidence of a disjunction
      of evidences.
      used for calculating the overall evidence
      of multiple occurrences of a single fact. */

calculate_evidence_of_disjunction_list(Es, E) :-
   calculate_evidence_of_disjunction_list_(Es, E).


/*** implementation ***********************************************/


/* combine_evidences_(Evidence_1,Evidence_2,Evidence_3) <-
      combines two evidences without any restriction */

combine_evidences_(Evidence_1,Evidence_2,Evidence_3) :-
   Evidence_3 is Evidence_1 * Evidence_2.


/* combine_evidences_of_list(Es,E) <-
      */

combine_evidences_of_list([E], E).
combine_evidences_of_list([E|Rest], EOut) :-
   combine_evidences_of_list(Rest, ERest), 
   combine_evidences_(E, ERest, EOut).


/* calculate_evidence_of_conjunction_list_(Es,E) <-
      calculates the overall evidence of a conjunction
      of evidences applying some restrictions.
      used for calculating the overall evidence
      of rule bodies etc. */

calculate_evidence_of_conjunction_list_([E], E).
calculate_evidence_of_conjunction_list_([E|Rest], EOut) :-
   calculate_evidence_of_conjunction_list_(Rest, ERest), 
   calculate_new_conjunction_evidence(E, ERest, EOut).


/* calculate_new_conjunction_evidence(
         Facts_evidence, Old_evidence, E_out) <-
      case: first evidences is greater then threshold and
      second is greater then 0 (= the order is important)
      => calculate new evidence using Facts_evidence and 
      Old_evidence ... */

calculate_new_conjunction_evidence(
      Facts_evidence, Old_evidence, E_out) :-
   proven_facts_evidence_threshold(Threshold),
   Facts_evidence >= Threshold,
   Old_evidence >= 0,
   !,
   combine_evidences_(Facts_evidence, Old_evidence, E_out).
calculate_new_conjunction_evidence(_, _, 0).


/* calculate_evidence_of_disjunction_list_(Es,E) <-
      calculates the overall evidence of a disjunction
      of evidences.
      used for calculating the overall evidence
      of multiple occurrences of a single fact. */

calculate_evidence_of_disjunction_list_([E], E).
calculate_evidence_of_disjunction_list_([E|Rest], EOut) :-
   calculate_evidence_of_disjunction_list_(Rest, ERest), 
   calculate_new_disjunction_evidence(E, ERest, EOut).


/* calculate_new_disjunction_evidence(
         Facts_evidence, Old_evidence, E_out) <-
      => calculate new evidence using Facts_evidence and 
      Old_evidence ... */

calculate_new_disjunction_evidence(
      Facts_evidence, Old_evidence, E_out) :-
   E_out is Facts_evidence + Old_evidence.


/* aggregate_evidences(E1,E2,E3) <-
      aggregation of evidences E1 and E2 to E3:
      uses DISCRETE evidences. */

% cases: "sicher hergeleitet", "sicher ausgeschlossen"

aggregate_evidences(p7,_, 1000).
aggregate_evidences(_,p7, 1000).
aggregate_evidences(n7,_,-1000).
aggregate_evidences(_,n7,-1000).

% case: one evidence is p0

aggregate_evidences(p0,E2,E2).
aggregate_evidences(E1,p0,E1).

% usual cases

aggregate_evidences(p5,p5,p6).
aggregate_evidences(p4,p4,p5).
aggregate_evidences(p3,p3,p4).
aggregate_evidences(p2,p2,p3).
aggregate_evidences(p1,p1,p2).

aggregate_evidences(n5,n5,n6).
aggregate_evidences(n4,n4,n5).
aggregate_evidences(n3,n3,n4).
aggregate_evidences(n2,n2,n3).
aggregate_evidences(n1,n1,n2).


/* aggregate_evidences_2(E1,E2,E3) <-
      continuous evidences during calculation are mapped
      to discrete results. */

% cases: "sicher hergeleitet", "sicher ausgeschlossen"

aggregate_evidences_2(E1,_,1000) :-
   E1 >= 999,
   !. 
aggregate_evidences_2(_,E2,1000) :-
   E2 >= 999,
   !.
aggregate_evidences_2(E1,_,-1000) :-
   E1 =< -999,
   !. 
aggregate_evidences_2(_,E2,-1000) :-
   E2 =< -999,
   !.

% usual cases

aggregate_evidences_2(E1,E2,E3) :-
   E3 is E1 + E2.


/* aggregate_list_of_evidences_2(Evidences,Evidence) <-
      for Lists of evidences */

aggregate_list_of_evidences_2([E1],E1). 
aggregate_list_of_evidences_2([E1|Rest],EOut):- 
   aggregate_list_of_evidences_2(Rest, ERest), 
   aggregate_evidences_2(E1, ERest,EOut).


   
/* aggregate_evidences_3(+E1,+E2,-E3) <-
      Uses the underlying probabilities of rules and symptoms 
      directly as evidences.
      Calculates from these new continuous evidences
      and supplies a predicate to map them to continuous 
      results between -1 and 1
      (a kind of pseudo-probabilities but no real probabilities). */

% cases: "sicher hergeleitet", "sicher ausgeschlossen"

aggregate_evidences_3(E1,_,10) :-
   E1 >= 9.9,
   !. 
aggregate_evidences_3(_,E2,10) :-
   E2 >= 9.9,
   !.
aggregate_evidences_3(E1,_,-10) :-
   E1 =< -9.9,
   !. 
aggregate_evidences_3(_,E2,-10) :-
   E2 =< -9.9,
   !.

% usual cases

aggregate_evidences_3(E1,E2,E3) :-
   E3 is E1 + E2.


/* aggregate_list_of_evidences_3(+Evidences,-Evidence) <-
      for Lists of evidences */

aggregate_list_of_evidences_3([E1],E1). 
aggregate_list_of_evidences_3([E1|Rest],EOut):- 
   aggregate_list_of_evidences_3(Rest, ERest), 
   aggregate_evidences_3(E1, ERest, EOut).


/* evidences_to_continuous_probabilities(
         +Evidence, -Pseudo_Probability) <-
      Mapping from evidences to pseudo-evidences
      with the help of a modified Sigmoid function. */

evidences_to_continuous_probabilities(
      Evidence, Pseudo_Probability) :-
   Pseudo_Probability is
      2 * ( 1 / (1 + 1/exp(2*log(3) * Evidence ) ) - 0.5 ).


/* apply_predisposition(+Evidence, +Predisposition, Result) <- 
      apply a predisposition to a Evidence round_to_category 
      (according to Puppe96). */

apply_predisposition(E, 5, Result_Category) :- 
   Result is E * 1.1,
   round_to_category(Result, Result_Category).
apply_predisposition(E, 10, Result_Category) :- 
   Result is E * 1.2,
   round_to_category(Result, Result_Category).
apply_predisposition(E, 20, Result_Category) :- 
   Result is E * 1.4,
   round_to_category(Result, Result_Category).
apply_predisposition(E, 40, Result_Category) :- 
   Result is E * 1.8,
   round_to_category(Result, Result_Category).

apply_predisposition(E, 0, E).
apply_predisposition(E, Predisposition, Result_Category) :- 
   Predisposition < 0,
   Result is E + Predisposition,
   round_to_category(Result, Result_Category).
   

/* apply_predisposition2(+Evidence, +Predisposition, Result) <- 
      apply a predisposition to a Evidence
      using round_to_category2 */

apply_predisposition2(E, 5, Result_Category) :- 
   Result is E * 1.1,
   round_to_category2(Result, Result_Category).
apply_predisposition2(E, 10, Result_Category) :- 
   Result is E * 1.2,
   round_to_category2(Result, Result_Category).
apply_predisposition2(E, 20, Result_Category) :- 
   Result is E * 1.4,
   round_to_category2(Result, Result_Category).
apply_predisposition2(E, 40, Result_Category) :- 
   Result is E * 1.8,
   round_to_category2(Result, Result_Category).
   
apply_predisposition2(E, 0, E).
apply_predisposition2(E, Predisposition, Result_Category) :- 
   Predisposition < 0,
   Result is E + Predisposition,
   round_to_category2(Result, Result_Category).


/*** utilities ****************************************************/


/* evidences_to_results(+E, -Result) <-
      */

evidences_to_results(E, 'L�sung best�tigt') :-
   E >= 42.
evidences_to_results(E, 'L�sung verd�chtigt') :-
   E >= 10,
   E < 42.
evidences_to_results(E, 'L�sung unklar') :-
   E >= -41,
   E =< 9.
evidences_to_results(E, 'L�sung ausgeschlossen') :-
   E =< -42.


/* probabilities_to_evidences(+Probability, -Evidence) <-
      mapping probabilities to evidences */

probabilities_to_evidences(1, 1000).
probabilities_to_evidences(0,  0).
probabilities_to_evidences(-1,-1000).

probabilities_to_evidences(P, E) :-
   P > 0,
   evidence_to_probability_interval(E,[X,Y]),
   P > X,
   P =< Y.

probabilities_to_evidences(P, E) :-
   P < 0,
   evidence_to_probability_interval(F,[X,Y]),
   P > -Y,
   P =< -X,
   E is -F.

evidence_to_probability_interval( 80, [0.9, 1] ).
evidence_to_probability_interval( 40, [0.7, 0.9] ).
evidence_to_probability_interval( 20, [0.5, 0.7] ).
evidence_to_probability_interval( 10, [0.3, 0.5] ).
evidence_to_probability_interval(  5, [0.15, 0.31] ).
evidence_to_probability_interval(  2, [0, 0.15] ).


/* round_to_category(
         +Predisposition_Score, -Predisposition_Category) <-
      rounds a predisposition score
      off to the next lower category. */

round_to_category(1000, 1000).
round_to_category(0, 0).
round_to_category(-1000, -1000).

round_to_category(Score,Category) :-
   Score > 0,
   round_to_category_interval([Category,Y]),
   Score >= Category,
   Score =< Y.

round_to_category(Score,Category) :-
   Score < 0,
   round_to_category_interval([X,Y]),
   Score >= -Y,
   Score =< -X,
   Category is -X.

round_to_category_interval([80,999]).
round_to_category_interval([40,79]).
round_to_category_interval([20,39]).
round_to_category_interval([10,19]).
round_to_category_interval([5,9]).
round_to_category_interval([2,4]).
round_to_category_interval([0,1]).


/* round_to_category2(
         +Predisposition_score, -Predisposition_category) <-
      rounds a Predisposition_score/100 off to the next 
      lower category/100. */

round_to_category2(Score,Category) :-
   S is Score * 100,
   round_to_category(S,C),
   Category is C / 100.
   

/* evidence_categories_to_scrores(E,C) <-
      mapping between evidence categories and evidence scores. */

evidence_categories_to_scrores(80, p6).
evidence_categories_to_scrores(40, p5).
evidence_categories_to_scrores(20, p4).
evidence_categories_to_scrores(10, p3).
evidence_categories_to_scrores( 5, p2).
evidence_categories_to_scrores( 2, p1).
evidence_categories_to_scrores( 0, p0).
evidence_categories_to_scrores( 2, n1).
evidence_categories_to_scrores( 5, n2).
evidence_categories_to_scrores(10, n3).
evidence_categories_to_scrores(20, n4).
evidence_categories_to_scrores(40, n5).
evidence_categories_to_scrores(80, n6).
   

/******************************************************************/


