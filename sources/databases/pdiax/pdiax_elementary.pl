

/******************************************************************/
/***                                                            ***/
/***          PDiaX:  Elementary                                ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(
      pdiax_debug, on ).


/*** interface ****************************************************/


/* pdiax_writef(Argument1,Argument2) <-
      calls writef for its arguments
      if dislog_variable(pdiax_debug,on),
      otherwise it does nothing but returns true. */
 
pdiax_writef(Argument1, Argument2) :-
   dislog_variable_get(pdiax_debug,on),
   !,
   writef(Argument1, Argument2).
pdiax_writef(_,_).
pdiax_writef(Argument) :-
   dislog_variable_get(pdiax_debug,on),
   !,
   writef(Argument).
pdiax_writef(_).


/* pdiax_write <-
      the same as pdiax_writef but for write. */

pdiax_write(Argument) :-
   dislog_variable_get(pdiax_debug,on),
   !,
   write(Argument).
pdiax_write(_).


/* pdiax_writeln <-
      the same with ln and argument. */

pdiax_writeln(Argument) :-
   dislog_variable_get(pdiax_debug,on),
   !,
   writeln(Argument).
pdiax_writeln(_).

pdiax_writeln :-
   dislog_variable_get(pdiax_debug,on),
   !,
   nl.
pdiax_writeln.


/* pdiax_dportray(Mode,Program) <-
      calls dportray for its arguments
      if dislog_variable(pdiax_debug,on),
      otherwise it does nothing but returns true. */

pdiax_dportray(Mode,Program) :-
   dislog_variable_get(pdiax_debug,on),
   !,
   dportray(Mode,Program).
pdiax_dportray(_,_).


/* pdiax_start_timer <-
      calls start_timer for its arguments
      if dislog_variable(pdiax_debug,on),
      otherwise it does nothing but returns true. */

pdiax_start_timer(Arg) :-
   dislog_variable_get(pdiax_debug,on),
   !,
   start_timer(Arg).
pdiax_start_timer(_).


/* pdiax_stop_timer(Argument) <-
      calls stop_timer for its argument
      if dislog_variable(pdiax_debug,on),
      otherwise it does nothing but returns true. */

pdiax_stop_timer(Arg) :-
   dislog_variable_get(pdiax_debug,on),
   !,
   stop_timer(Arg).
pdiax_stop_timer(_).


/******************************************************************/


