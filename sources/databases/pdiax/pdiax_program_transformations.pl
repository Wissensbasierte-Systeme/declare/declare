

/******************************************************************/
/***                                                            ***/
/***          PDiaX:  Program Transformations                   ***/
/***                                                            ***/
/******************************************************************/


/*
:- module( pdiax_program_transformations, [
      coarse_predicate/2,
      coarse_literal/2, coarse_literals/2, 
      coarse_clause/2, coarse_program/2 ] ).

:- use_module(pdiax_elementary).
*/

:- op(600, fy, ~).


/*** interface ****************************************************/

 
/* coarse_program(+Program,-Result) <-
      */

coarse_program(Program_1,Program_2) :-
   maplist( coarse_clause,
      Program_1, Program_2 ).


/*** implementation ***********************************************/


/* coarse_predicates(Predicates, Result) <-
      */

coarse_predicates(Predicates, Result) :-
   maplist( coarse_predicate,
      Predicates, Result ).

   
/* coarse_predicate(+Predicate, -Result) <-
      */ 

/* case: predicate without arguments */
coarse_predicate(Predicate, Predicate_name) :-
   /* can't be replaced by Predicate =..[Predicate_name] : */
   Predicate =.. [Predicate_name],    
   !.
   
/* case: predicate with arguments - status:
   predicate not to be ignored */
coarse_predicate(Predicate, Result) :-
   ignore_predicate(Predicate_name), 
   Predicate =.. [Predicate_name|Potential_predicates], 
   \+ Potential_predicates = [], 
   coarse_predicates(Potential_predicates, Inner_result),
   Result =.. [Predicate_name|Inner_result],
   !.

/* case: predicate with arguments - status:
   predicate to be ignored */
coarse_predicate(Predicate, Result) :-
   Predicate =.. [Predicate_name|Potential_predicates], 
   \+ ignore_predicate(Predicate_name), 
   length(Potential_predicates, Number_of_arguments),
   concat_atom([Predicate_name, '_', Number_of_arguments],
      Result).
   
/* case: argument was no predicate */
coarse_predicate(Potential_predicate, Potential_predicate) :-
   \+ Potential_predicate=.. [_|_].
   

/* coarse_literals(Literals_1,Literals_2) <-
      */

coarse_literals(Literals_1,Literals_2) :-
   maplist( coarse_literal,
      Literals_1, Literals_2 ).


/* coarse_literal(+Clause, -Result) <-
      */

coarse_literal(Clause, -Coarsen_clause) :-
   Clause = -Predicate,
   coarse_predicate(Predicate, Coarsen_clause).
coarse_literal(Clause, ~Coarsen_clause) :-
   Clause = ~Predicate,
   coarse_predicate(Predicate, Coarsen_clause).
coarse_literal(Clause, Coarsen_clause) :-
   Clause = Predicate,
   coarse_predicate(Predicate, Coarsen_clause).


/* coarse_clause(+Clause, -Result) <-
      */

coarse_clause([], []).
coarse_clause(H1-P1-N1,H2-P2-N2) :- 
   coarse_literals(H1,H2),
   coarse_literals(P1,P2),
   coarse_literals(N1,N2).
coarse_clause(H1-P1,H2-P2) :- 
   coarse_literals(H1,H2),
   coarse_literals(P1,P2).
coarse_clause(H1,H2) :- 
   coarse_literals(H1,H2).


/* ignore_predicate(Predicate) <-
      list of predicates which should not be changed */

ignore_predicate(:).
 

/******************************************************************/


