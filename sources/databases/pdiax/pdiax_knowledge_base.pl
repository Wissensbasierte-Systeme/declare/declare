

/******************************************************************/
/***                                                            ***/
/***          PDiaX:  Knowledge Base                            ***/
/***                                                            ***/
/******************************************************************/


:- module( pdiax_knowledge_base, [
      user_question/8,
      user_dialog_answer/3,
      value_type/6, 
      diagnosis/5 ] ).

:- dynamic
      user_dialog_answer/3.


/*** interface ****************************************************/


/* user_question(Identifier, Question, Explanation, Comment, 
         Value_type, Use, Cost, Conficence_factor) <-
      */

user_question(power_failure_1,
   "Ist die Stromversorgung 1 fehlerhaft?", 
   "Erkl�rung der Frage", "Kommentar zur Frage", 
   power_failure_value_type_1, 10, 10, 1.0).
user_question(power_failure_2,
   "Ist die Stromversorgung 2 fehlerhaft (ja, nein)?", 
   "Erkl�rung der Frage", "Kommentar zur Frage", 
   power_failure_value_type_2, 10, 10, 1.0).
user_question(power_failure_3,
   "Frage nach Wahl?", 
   "Erkl�rung der Frage", "Kommentar zur Frage", 
   power_failure_value_type_3, 10, 10, 1.0).
user_question(power_failure_4,
   "Frage nach Wahl?", 
   "Erkl�rung der Frage", "Kommentar zur Frage", 
   power_failure_value_type_4, 10, 10, 1.0).
user_question(power_failure_5,
   "Wie hoch ist die Eingangsspannung?", 
   "Erkl�rung der Frage", "Kommentar zur Frage", 
   power_failure_value_type_5, 10, 10, 1.0).
user_question(power_failure_6,
   "Wie hoch ist die Eingangsspannung?", 
   "Erkl�rung der Frage", "Kommentar zur Frage", 
   power_failure_value_type_6, 10, 10, 1.0).
user_question(power_failure_7,
   "Ist die Stromversorgung fehlerhaft?", 
   "Erkl�rung der Frage", "Kommentar zur Frage", 
   power_failure_value_type_7, 10, 10, 1.0).


/* value_type(Identifier, Type,
         Lower_bound, Upper_bound, Unit, Range) <-
      */

value_type(power_failure_value_type_1,
   boolean, _, _, _, _).
value_type(power_failure_value_type_2,
   string, _, _, _, _).
value_type(power_failure_value_type_3,
   multiple_choice, _, _, _, ['Wahl 1', 'Wahl 2', 'Wahl 3']).
value_type(power_failure_value_type_4,
   single_choice, _, _, _, ['Wahl 1', 'Wahl 2', 'Wahl 3']).
value_type(power_failure_value_type_5,
   integer, -10, 10, _, _).
value_type(power_failure_value_type_6,
   double, -10.4, 10.8, _, _).
value_type(power_failure_value_type_7,
   time, _, _, _, _).



/* diagnosis(Identifier, Explanation, Comment, Evidence,
         Is_solution) <-
      */

diagnosis(diagnose_1, "Erkl�rung", "Kommentar", 1.0, true).


/******************************************************************/


