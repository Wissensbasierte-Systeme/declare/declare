

/******************************************************************/
/***                                                            ***/
/***          PDiaX:  Tpe-Delta-Operator                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tpe_delta_operator(Program,
         State_1,State_2,State_7, State_9) <-
      */

tpe_delta_operator(Program, State_1,State_2,State_7, State_9) :-
   pdiax_writeln,
   pdiax_writeln('==============================='),  
   pdiax_writeln('applying tpe_delta_operator on: '), 
   pdiax_writeln('==============================='),
   pdiax_writeln,
   pdiax_writeln('program:'),  
   pdiax_dportray(lp, Program), pdiax_writeln,
   pdiax_writeln('state 1:'), 
   pdiax_dportray(dhs, State_1), pdiax_writeln,
   pdiax_writeln('state 2:'), 
   pdiax_dportray(dhs, State_2), pdiax_writeln,
   pdiax_writeln('-----------------'), 

   tpe_delta_join(Program, State_1, State_2, State_3),
   pdiax_writeln('-----------------'), pdiax_writeln, 
   pdiax_writeln('result state of delta_join:'),  
   pdiax_dportray(dhs, State_3), pdiax_writeln,

   remove_structural_duplicates(State_3,State_4),
   pdiax_writeln('removed structural duplicates:'), 
   pdiax_dportray(dhs, State_4), pdiax_writeln,

%  discard the stored rule information to get a shorter result
%  remove_rule_information(State_4,State_5),
%  pdiax_writeln('removed information about rules:'), 
%  pdiax_writeln(State_5), 
%  pdiax_dportray(dhs, State_5), pdiax_writeln,

%  can be used without modification because of the stratification
%  state_union(State_1,State_5,State_6),
%  pdiax_writeln('union of states:'), 
%  pdiax_writeln(State_6), 
%  pdiax_dportray(dhs, State_6), pdiax_writeln,

%  can be used without modification because of the stratification
   state_union(State_1,State_4,State_5),
   pdiax_writeln('union of states:'), 
   pdiax_writeln(State_5), 
   pdiax_dportray(dhs, State_5), pdiax_writeln,
   
%  discard the stored rule information to get a shorter result
   remove_rule_information(State_5,State_6),
   pdiax_writeln('removed information about rules:'), 
   pdiax_writeln(State_6), 
   pdiax_dportray(dhs, State_6), pdiax_writeln,

   state_aggregate(State_6,State_7),
   pdiax_writeln('aggregated state:'), 
   pdiax_dportray(dhs, State_7), pdiax_writeln,

   pdiax_writeln('new state_1:'), 
   pdiax_dportray(dhs, State_7), pdiax_writeln,

   state_subtract(State_7,State_1,State_8),
   pdiax_writeln('result of subtraction: new state 2:'), 
   pdiax_dportray(dhs, State_8), pdiax_writeln,

   state_truncate(State_8,State_9),
   pdiax_writeln('truncated state:'), 
   pdiax_dportray(dhs, State_9).


/* tpe_delta_fixpoint(Program,
         State_1,State_2, Result_state) <-
      */

tpe_delta_fixpoint(Program, State_1,State_2, Result_state) :-
   tpe_delta_operator(
      Program, State_1, State_2, State_3, State_4),
   nl, writeln('next aggregated state:'),
   dportray(dhs, State_3), nl,
   ( ( State_4 = [],
       !,
       Result_state = State_1 )
   ; ( tpe_delta_fixpoint(
          Program,State_3,State_4, Result_state) ) ).



/* tpe_delta_join(Program, State1, State2, State3) <-
      delta_join with evidences and build-in-predicates. */

tpe_delta_join(Program, State1, State2, State3) :-
   ( ( current_num(join_mode,regular),
       Resolve_Predicate = delta_resolvable )
   ; ( current_num(join_mode,special),
       Resolve_Predicate = bi_delta_resolvable ) ),

   pdiax_write('tpe_delta_join:' ), pdiax_writeln, 
   Resolve_Goal =..
      [Resolve_Predicate, State1, State2, Body, _],

   ttyflush,

   pdiax_start_timer(tpe_delta_join),
   findall( Head,
      ( member(Rule, Program),
        decompose_rule(Rule, Head-Body, Rule_identifier),
        pdiax_writeln,
        pdiax_write('examing: ' ), 
        pdiax_dportray(lp, [Head-Body]),
        call(Resolve_Goal), 
        pdiax_writeln,pdiax_writeln,
        pdiax_write('initiated to: ' ),
        pdiax_dportray(lp, [Head-Body]),
        Head = [atom(_, Rule_identifier):_],
        maplist( remove_evidence,
           Body, Pruned_Body ),
        maplist( remove_evidence,
           Head, Pruned_Head ),
        list_to_ord_set(Pruned_Body,Ord_Body),
        ord_disjoint(Pruned_Head, Ord_Body) ),
      State3 ),
        
   pdiax_stop_timer(tpe_delta_join).


/*** implementation ***********************************************/


/* bi_delta_resolvable(State1,State2,Literals,Clause) <-
      */

bi_delta_resolvable(State1,State2,[L|Ls],Clause) :-
   append(Xs,[Y|Ys],[L|Ls]),
   append(Xs,Ys,Zs),
   pdiax_writef('\n%w%w',
      ['testing predicate for being not special: ', L]),
   \+ special_tpe_literal(Y), 
   member(Fact,State2), 
   pdiax_writef('\n%w%w', ['chosen fact: ', Fact]),
   pdiax_writef('\n%w%w', ['try to resolve with: ', Y]),
   pdiax_writef('\n%w%w', ['of clause: ', Clause1]),
   resolve(Y,Fact,Clause1),
   pdiax_writef('\n%w%w', ['resolved fact: ', Clause1]),
   bi_resolvable(State1,Zs,Clause2),
   ord_union(Clause1,Clause2,Clause).
bi_delta_resolvable(_,[],[],[]).


/* bi_resolvable(State,Literals,Clause) <-
      facts of the programm are only deduced at the beginning
      of the iteration
      - means state2 = [] otherwise with every iteration step
      the fact would be deduced again */

bi_resolvable(State,[L|Ls],Clause) :-
   pdiax_writef('\n%w%w\n', [
      'bi:testing predicate for being special: ', L]),
   special_tpe_literal(L),
   !, 
   pdiax_writef('calling predicate: '),
   pdiax_writeln(L),
   call(L),
   pdiax_writef('called successfully: '),
   bi_resolvable(State,Ls,Clause).
bi_resolvable(State,[L|Ls],Clause) :-
   pdiax_writef('%w%w%w', ['bi:testing base-literal',Fact,State]),
   member(Fact,State), 
   pdiax_writef('\n%w%w', ['bi:chosen fact: ', Fact]),
   resolve(L,Fact,Clause1),
   pdiax_writef('\n%w%w', ['resolved fact: ', Clause1]),
   bi_resolvable(State,Ls,Clause2),
   ord_union(Clause1,Clause2,Clause).
bi_resolvable(_,[],[]).

special_tpe_literal(aggr(_-_):_).


/* aggr(Head_evidence-Body_evidence):Rule_evidence <-
      calculates the evidence of the head of the clause */

aggr(Head_evidence-Body_evidence):Rule_evidence :-
   combine_evidences_of_list([Rule_evidence|Body_evidence], 
      Head_evidence).

   
/* user_question(
         +Identifier, -Answer, +Evidence_in, -Evidence_out) <-
      starts user-dialog for asking a question
      Evidence_in is the allover evidence of the terms of the 
      previous terms of the clause.
      Evidence_out is the evidence of the answer. */
   
user_question(Identifier, Answer):Answer_evidence :-
   user_dialog(Identifier, Answer, Answer_evidence).


/* delta_resolvable(State1,State2,Literals,Clause) <-
      no change in delta_resolvable. */

delta_resolvable(State1,State2,[L|Ls],Clause) :-
   append(Xs,[Y|Ys],[L|Ls]), append(Xs,Ys,Zs),
   member(Fact,State2), resolve(Y,Fact,Fact1),
   resolvable(State1,Zs,Clause1),
   ord_union(Fact1,Clause1,Clause).
delta_resolvable(_,_,[],[]).

resolvable(State,[L|Ls],Clause) :-
   member(Fact,State), resolve(L,Fact,Clause1),
   resolvable(State,Ls,Clause2),
   ord_union(Clause1,Clause2,Clause).
resolvable(_,[],[]).


/* remove_structural_duplicates(+List, ?Pruned) <-
      is true when Pruned is like List but with all 
      structurally duplicate elements removed.     */

remove_structural_duplicates([Head|Tail1], [Head|Tail2]) :-
   delete_structural_duplicates(Tail1, Head, Residue), 
   remove_structural_duplicates(Residue, Tail2).
remove_structural_duplicates([],[]).


/* delete_structural_duplicates(+List, +Element, ?Residue) <-
      is true when all structurally equal occurences of Element
      in List are removed and the result is Residue. */

delete_structural_duplicates([Head|Tail], Element, Rest) :-
   Head =@= Element,
   !,
   delete_structural_duplicates(Tail, Element, Rest).
delete_structural_duplicates(
      [Head|Tail], Element, [Head|Rest]) :-
   delete_structural_duplicates(Tail, Element, Rest).
delete_structural_duplicates([], _, []).
   

/* remove_rule_information(Xs1,Xs2) <-
      removes the information about the rule
      from the derived atoms */

remove_rule_information([Head1|Tail1],[Head2|Tail2]):-
   Head1 = [atom(Fact, _):P],
   Head2 = [atom(Fact, []):P],
   remove_rule_information(Tail1, Tail2).   
remove_rule_information([],[]).
   

/* state_aggregate(+State_1, -State_2) <-
      aggregates two states by aggregaton of their evidence. */

state_aggregate([X1|S1],[X2|S2]) :-
   fact_aggregate([X1|S1],X1,X2,S),
   state_aggregate(S,S2).
state_aggregate([],[]).


/* fact_aggregate <-
      aggregates the duplicates of a single fact,
      calculating a new evidence by using the evidences
      of the duplicates. */

fact_aggregate(List_of_facts, [Fact:_], 
      [Fact:Aggregated_evidences], List_of_remaining_facts) :-
   findall( Single_evidence,
      member([Fact:Single_evidence], List_of_facts),
      List_of_Evidences ),
   calculate_evidence_of_disjunction_list(
      List_of_Evidences, Aggregated_evidences),

%  get all other facts of the tail
   findall( [New_fact:New_evidence],
      ( member([New_fact:New_evidence], List_of_facts),
        \+ New_fact = Fact ),
      List_of_remaining_facts ).


/* state_truncate(+State_1, -State_2) <-
      Removes the weak facts from a given state.
      a fact is weak if it isn't at least suspected
      (<=> its evidence is smaller then the threshold
      specified by suspected_facts_evidence_threshold/1)
      in pdiax_evidences. */

state_truncate(State_1,State_2) :-
   suspected_facts_evidence_threshold(Threshold),
   state_truncate(Threshold,State_1,State_2).

state_truncate(Threshold,[[X:E]|S_1],[[X:E]|S_2]) :-
   E >= Threshold,
   state_truncate(Threshold,S_1,S_2).
state_truncate(Threshold,[[_:E]|S_1],S_2) :-
   E < Threshold,
   state_truncate(Threshold,S_1,S_2).
state_truncate(_,[],[]).


/* remove_evidence(Term_1,Term_2) <-
      converts a term Term_1 with or without evidence
      into a term Term_2 without evidence. */

remove_evidence(Head:_, Head).
remove_evidence(Term, Term):-
   \+ Term = _:_.


/* decompose_rule(Rule, Head-Body, Rule_identifier) <-
      decomposes a rule into its parts
      and creates a rule identifier which is the rule itself
      for non-facts and [] for facts. */

decompose_rule(Rule, Head-Body, Rule_identifier) :-
   ( ( Rule = Head-Body,
       copy_term(Head-Body, Rule_identifier) )
   ; ( \+ Rule = _-_,
       \+ Rule = _-_-_,
       Head = Rule,
       Body = [], 
       Rule_identifier = [] ) ).


/******************************************************************/


