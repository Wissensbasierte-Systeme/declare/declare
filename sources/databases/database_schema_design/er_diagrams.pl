

/******************************************************************/
/***                                                            ***/
/***         Database Design:  ER-Diagrams                      ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      entity/1,
      relationship/1.


:- dislog_variable_set(
      er_diagrams_example,
      'databases/er_diagrams/er_diagrams_kb' ).


/*** interface ****************************************************/


/* init_er_diagram <-
      */

init_er_diagram :-
   dislog_variable_get(example_path, Examples),
   dislog_variable_get(er_diagrams_example, File),
   concat(Examples, File, File_2),
   consult(File_2),
   dislog_consult(File_2, Er_Diagram),
   dabolish(current_er_diagram/1),
   assert(current_er_diagram(Er_Diagram)),
   !.


/* relationship_entities(
         Name, [Entities, Attributes, Cardinality]) <-
      */

relationship_entities(Name, Triple) :-
   relationship(Name, Rel),
   Triple := Rel^[entities, attributes, cardinality].


/* entity_relationships(Name, Attributes, P_Keys, F_Keys) <-
      */

entity_relationships(Name, Attributes, P_Keys, F_Keys) :-
   entity(Name, Entity),
   Attributes := Entity^attributes,
   P_Keys := Entity^primary_key,
   ( F_Keys := Entity^foreign_keys ->
     true
   ; F_Keys = [] ).


/* entity_name_to_primary_key(Entity_Name, Primary_Keys) <-
      */

entity_name_to_primary_key(Entity_Name, Primary_Keys) :-
   entity(Entity_Name, Entity),
   Primary_Keys := Entity^primary_key.


/* relationship_name_to_primary_key(
         Relationship_Name, P_Keys) <-
      */

relationship_name_to_primary_key(Relationship_Name, P_Keys) :-
   relationship(Relationship_Name, R),
   P_Keys := R^primary_key.


/* get_attribute_definition_from_entity(
         Entity_Name, Attribute_Name, [Attribute_Name, Type]) <-
      */

get_attribute_definition_from_entity(
      Entity_Name, Attribute_Name, [Attribute_Name, Type]) :-
   entity(Entity_Name, Entity),
   Attributes := Entity^attributes,
   member([Attribute_Name, Type], Attributes).


/* categorize_relationship([C|Cs], [E1, E2|Es], [X|Xs]) <-
      Categorizes a relationship based on the cardinality C
      between the different enities E1 and E2. 
      The result X is a list of lists with the elements
      [C, E1, E2]. */

categorize_relationship([C|Cs], [E1, E2|Es], [X|Xs]) :-
   X = [C, E1, E2],
   categorize_relationship(Cs, Es,Xs).
categorize_relationship([], [], []).



/* get_data_for_foreign_key_reference(E_Ns, New_As, PKs, FKs) <-
      */

get_data_for_foreign_key_reference(E_Ns, New_As, PKs, FKs) :-
   foreach(E_N, E_Ns), foreach(New_A, New_As),
   foreach(PK, PKs), foreach(FK, FKs) do
      entity_name_to_primary_key(E_N, PK),
      add_attributes_from_entity(E_N, PK, [New_A]),
      create_list_for_foreign_keys(E_N, PK, [FK]).
 
create_list_for_foreign_keys(E_name, PKs, FKs) :-
   foreach(PK, PKs), foreach(FK, FKs) do
      FK = [PK, E_name].

add_attributes_from_entity(Entityname, E_A, Sofar) :-
   maplist( get_attribute_definition_from_entity(Entityname),
      E_A, Sofar ).


/* create_sql_table_for_multivalued_attributes(
         Entityname, Attributes, Statement) <-
      */

create_sql_table_for_multivalued_attributes(
      Entityname, Attributes, Statement) :-
   concat([Entityname, '_MV'], Name),
   get_data_for_foreign_key_reference(
      [Entityname], As_2, Pks, Fks),
   flatten(Pks, Pk),
   union(Attributes, As_2, As),
   create_sql_table_from_entity(Name, As, Pk, Fks, Statement).


/* create_sql_table_from_relationship(Statement) <-
      */

create_sql_table_from_relationship(Statement) :-
   relationship_entities(Name, [Es, As, _]),
   get_data_for_foreign_key_reference(Es, As_2, Pks, Fks),
   maplist( entity_name_to_primary_key,
      Es, Pks ),
   flatten(Pks, Pk),
   union(As, As_2, As_3),
   create_sql_table_from_entity(Name, As_3, Pk, Fks, Statement).


/* create_sql_table_from_entity(Statement) <-
      */

create_sql_table_from_entity(Statement) :-
   entity_relationships(Name, As, Pk, Fks),
   create_sql_table_from_entity(Name, As, Pk, Fks, Statement).


/* create_sql_table_from_entity(Name, As, Pk, Fks, Statement) <-
      */

create_sql_table_from_entity(Name, As, Pk, Fks, Statement) :-
   concat(['CREATE TABLE ', Name, ' (\n'], Header),
   format_attributes(As, A),
   format_primary_key(Pk, P),
   format_foreign_keys(Fks, F),
   concat_with_separator([A, P, F], ',\n', APF),
   concat([Header, APF, ' );'], Statement).

format_attributes(As, Statement) :-
   ( foreach([A, T], As), foreach(Y, Ys) do
        concat(['   ', A, ' ', T], Y) ),
   concat_with_separator(Ys, ',\n', Statement).

format_primary_key(Pk, Statement) :-
   concat_atom(Pk, ', ', X),
   concat(['   PRIMARY KEY (', X,')'], Statement).

format_foreign_keys(Fks, Statement) :-
   ( foreach([Attribute, Table], Fks), foreach(Y, Ys) do
        concat([
           '   FOREIGN KEY (', Attribute,')\n',
           '   REFERENCES ', Table,' (', Attribute, ')'], Y) ),
   concat_with_separator(Ys, ',\n', Statement).


/* entity(Name, Entity) <-
      */

entity(Name, Entity) :-
   current_er_diagram(Er_Diagram), 
   member([entity(Entity)], Er_Diagram),
   Name := Entity^name.


/* weak_entity(Name, Entity) <-
      */

weak_entity(Name, Entity) :-
   current_er_diagram(Er_Diagram),
   member([weak_entity(Entity)], Er_Diagram),
   Name := Entity^name.


/* relationship(Name, Relationship) <-
      */

relationship(Name, Relationship) :-
   current_er_diagram(Er_Diagram),
   member([relationship(Relationship)], Er_Diagram),
   Name := Relationship^name.


/*** tests ********************************************************/


test(database_design, format_foreign_keys) :-
   format_foreign_keys([
      ['Lagerbestand', 'Lagerplatznr'],
      ['Produkt', 'Produktnr'] ], X),
   writeln(X).

test(database_design, multi) :-
   init_er_diagram,
   Attributes = [
      ['Posnr', 'INTEGER'],
      ['Lnr', 'INTEGER'],
      ['Produkttext', 'VARCHAR(25)'] ],
   create_sql_table_for_multivalued_attributes(
      'Lagerbestand', Attributes, X ),
   writeln(X).

test(database_design, get_foreign_key_data) :-
   get_data_for_foreign_key_reference(
      ['Abteilung'], New_As, _, FKs),
   writeln(New_As),
   writeln(FKs).

test(database_design, add_attributes) :-
   add_attributes_from_entity(
      'Abteilung',
      ['Abteilungsname', 'Standort'], X),
   writeln(X).


/******************************************************************/


