

/******************************************************************/
/***                                                            ***/
/***        Database Design:  FD-Algorithms for 3NF             ***/
/***                                                            ***/
/******************************************************************/


   
/*** interface ****************************************************/


/* relation_scheme_to_3nf_decomposition(R, D) <-
      A relation scheme R = [U, F] is decomposed
      into a lossless and independent 3NF-decomposition D. */

relation_scheme_to_3nf_decomposition([U, F], D) :-
   fd_set_to_basis_2(F, G),
   collect_left_elements_of_fd_list(F,L),
   create_tables_for_3nf_decomposition(L, G, R),
   guarantee_losslessness(U, G, R, D).


/*
test_relation_scheme_to_3nf_decomposition :-
   create_3nf_decomposition_from_relation_scheme(
      [k, n, s, z], [[k]-[s], [n]-[z, s]], D),
   writeln(D).
*/

test_relation_scheme_to_3nf_decomposition :-
   relation_scheme_to_3nf_decomposition(
      [[c, a, z], [[c, a]-[z], [z]-[c]]], D),
   writeln(D).


/* fd_set_reduction(...) <- 
      A given fd-set F is reduced under a attribute set X
      to a reduced fd-set R. */

fd_set_reduction([F|Fs], X, [R|Rs]) :-
   fd_set_reduction_loop(F, X, R),
   fd_set_reduction(Fs, X, Rs).
fd_set_reduction([_|Fs], X, R) :-
   fd_set_reduction(Fs, X, R).
fd_set_reduction([], _, []).

fd_set_reduction_loop(Y-Z, X, R) :-
   subset(Y, X),
   subset(Z, X),
   R = Y-Z.


test_fd_set_reduction :-
   fd_set_reduction(
      [[a, b]-[c], [c]-[a], [a]-[e]], [a, b, c], R),
   writeln(R).


collect_left_elements_of_fd_list([F-_|Fs], [F|Ys]) :-
   collect_left_elements_of_fd_list(Fs, Ys).
collect_left_elements_of_fd_list([], []).


collect_dependents(L, F, D) :-
   collect_dependents_loop(L, F, Y),
   flatten(Y, Y1),
   list_to_set(Y1, D).
  
collect_dependents_loop(L, [L-F|Fs], [F|Ds]) :-
   collect_dependents_loop(L, Fs, Ds).
collect_dependents_loop(L, [_|Fs], [_|Ds]) :-
   collect_dependents_loop(L, Fs, Ds).
collect_dependents_loop(_, [], []).

test_collect_dependents :-
   collect_dependents(
      [a], [[a]-[c, d], [a]-[c], [b]-[f]], X),
   writeln(X).


create_tables_for_3nf_decomposition([Y|Ys], G, [R|Rs]) :-
   collect_dependents(Y, G, D),
   union(Y, D, Z),
   fd_set_reduction(G, Z, Z1),
   R = [Z, Z1],
   create_tables_for_3nf_decomposition(Ys, G, Rs).
create_tables_for_3nf_decomposition([], _, []).

test_create_tables_for_3nf_decomposition :-
   create_tables_for_3nf_decomposition(
      [l, t], [[l, t]-[a], [l]-[a], [o]-[a]], R),
   writeln(R).


/* guarantee_losslessness(U, G, R, D) <- 
      On demand the 3nf relation set R is completed
      with a relation R_i = [[Key(U, G)], []]
      to provide losslessness of relation set. */

guarantee_losslessness(U, G, R, D) :-
   guarantee_losslessness_loop(U, G, R, R, D).

guarantee_losslessness_loop(U, G, R, [[R1|_]|Rs], D) :-
   not(fd_membership_2(R1-U, G)),
   !,
   guarantee_losslessness_loop(U, G, R, Rs, D).
guarantee_losslessness_loop(_, _, R, [_|_], R).
guarantee_losslessness_loop(U, G, R, _, D) :-
   get_key_from_relation_scheme([U, G],K),
   union(R, [K, []], D).
 

/*
test_guarantee_losslessness :-
   guarantee_losslessness(
      [c,a,z], [[c,a]-[z], [z]-[c]],
      [[[c,a,z], [[c,a]-[z], [z]-[c]]],
      [[z,c], [[z]-[c]]]], D),
   writeln(D).
*/

test_guarantee_losslessness :-
   guarantee_losslessness(
      [k,n,s,z], [[k]-[s], [n]-[z], [n]-[s]],
      [[[k,s], [[k]-[s]]], [[n,z,s], [[n]-[z], [n]-[s]]]], D),
   writeln(D).


/* fd_set_to_basis_2(F, G) <-
      */

fd_set_to_basis_2(F, G) :-
   fd_set_to_closure_f(F, H), nl,
   list_to_set(H, J),
   extract_redundant_fds(J, G).

test_fd_set_to_basis_2 :- 
   fd_set_to_basis_2(
      [[a,b]-[c], [c]-[a], [b,c]-[d], [a,c,d]-[b], [d]-[e,g],
       [b,e]-[c], [c,g]-[b,d], [c,e]-[a,g]], X),
   writeln(X).


/* fd_set_to_closure_f_2 <-
      */

fd_set_to_closure_f(F, G) :-
   fd_set_to_r_minimal_cover(F, F_l_minimal),
   fd_set_to_l_minimal_cover(F_l_minimal, G).

test_fd_set_to_closure_f :- 
   FD_s = [ [a, b]-[c], [b, e]-[i],
      [a]-[i], [i]-[f], [a, i, f]-[b, s] ],
   fd_set_to_closure_f(FD_s, X),
   writeln(X).


/* fd_set_to_closure_a_2 <-
      */

fd_set_to_closure_a_2(X, F, Y) :-
   step_through_dependency_list(X, F, Dependents),
   flatten(Dependents, New_x),
   [_|_] = New_x,
   not(subset(New_x, X)),
   append(X, New_x, New_X),
   list_to_set(New_X, New_X1),
   fd_set_to_closure_a_2(New_X1, F, Y).
fd_set_to_closure_a_2(X, _, X).

test_fd_set_to_closure_a_2 :-
   FD_s = [ [a, b]-[c], [b, e]-[i],
      [a]-[i], [i]-[f], [a, i, f]-[b, s] ],
   fd_set_to_closure_a_2([a,e], FD_s, X),
   writeln(X).


/* fd_membership_2(X-Y, F) <-
      */

fd_membership_2(X-Y, F) :-
   fd_set_to_closure_a_2(X, F, X_asterix),
   !,
   subset(Y, X_asterix).

test_fd_membership_2 :-
   fd_membership_2(
      [c, a, z]-[c, a, z], [ [c, a]-[z], [z]-[c] ]).


/*
test_fd_membership_2 :-
   fd_membership_2( [a]-[a, b],
      [[a,b]-[c], [b,e]-[i], [a]-[i], [i]-[f], [a,i]-[b,f]]).
*/


/* get_key_of_relation_scheme([V, F], K) <-
      gets a key K from a relation scheme R = [V, F]. */

get_key_from_relation_scheme([V, F], K) :-
   fd_set_to_l_minimal_cover([V-V], F, [K-V]).

test_get_key_from_relation_scheme :-
   get_key_from_relation_scheme(
      [ [c, e, b, d, a],
        [[a, b]-[c], [a]-[d], [d]-[e]] ], K),
   writeln(K).


/*** implementation ***********************************************/


extract_redundant_fds(F, G) :-
   extract_redundant_fds(F, F, G).

extract_redundant_fds([F|Fs], Old_F, New_F) :-
   delete(Old_F, F, Y),
   fd_membership_2(F, Y),
   extract_redundant_fds(Fs, Y, New_F).
extract_redundant_fds([_|Fs], Old_F, New_F) :-
   extract_redundant_fds(Fs, Old_F, New_F).
extract_redundant_fds([], X, X).


check_matching_of_left_side(X, F_left-F_right, Y) :-
   subset(F_left, X),
   Y = F_right.


step_through_dependency_list(X, [F|Fs], [New_X|New_Xs]) :-
  check_matching_of_left_side(X, F, New_X),
  step_through_dependency_list(X, Fs, New_Xs).
step_through_dependency_list(X, [_|Fs], New_Xs) :-
   step_through_dependency_list(X, Fs, New_Xs).
step_through_dependency_list(_, [], []).


/* fd_set_to_r_minimal_cover <-
      */

fd_set_to_r_minimal_cover(F, New_F) :-
   fd_set_to_r_minimal_cover_loop(F, Y),
   flatten(Y, New_F).


fd_set_to_r_minimal_cover_loop(
      [F_left-F_right|Fs], [New_F|New_Fs]) :-
   fd_set_r_partition_in_single_elements(F_left, F_right, New_F),
   fd_set_to_r_minimal_cover_loop(Fs, New_Fs).
fd_set_to_r_minimal_cover_loop([], []). 


fd_set_r_partition_in_single_elements(F1, [F2|F2s], [Y|Ys]) :-
    Y = F1-[F2],
    fd_set_r_partition_in_single_elements(F1, F2s, Ys).
fd_set_r_partition_in_single_elements(_, [], []).


/* fd_set_to_l_minimal_cover <-
      */


fd_set_to_l_minimal_cover(F, F_l_minimal) :-
   fd_set_to_l_minimal_cover(F, F, F_l_minimal).


fd_set_to_l_minimal_cover([F_left-F_right|Fs],
      F_copy, [F_l_min|F_l_mins]) :-
   fd_check_membership(
      F_left, F_left, F_right, F_copy, New_F_left),
   F_l_min = New_F_left-F_right,
   fd_set_to_l_minimal_cover(Fs, F_copy, F_l_mins).
fd_set_to_l_minimal_cover([_|Fs], F_copy, F_l_mins) :-
   fd_set_to_l_minimal_cover(Fs, F_copy, F_l_mins).
fd_set_to_l_minimal_cover([], _, []).


test_fd_set_l_minimal_cover :- 
   fd_set_to_l_minimal_cover(
      [[a,b]-[c], [b]-[i], [a]-[i], [i]-[f], [a,i,f]-[b,s]], X),
   writeln(X).

fd_check_membership(F_left, F_right, F_copy, New_F_left) :-
fd_check_membership(F_left, F_left, F_right, F_copy, New_F_left).


test_fd_check_membership :-
   fd_check_membership(
      [a, i, f], [a, i, f], [b, s],
      [ [a, b]-[c], [b, e]-[i],
        [a]-[i], [i]-[f], [a, i, f]-[b, s] ], X),
   writeln(X).

test(database_design:fd, '3NF_synthesis') :-
   FD_Set = [ [a, b]-[c, d], [a, c]-[d], [a, d]-[c] ],
   fd_set_to_basis_2(FD_Set, Basis),
   writeln_list(Basis).
   

fd_check_membership(
      [F_l|F_ls], F_left, F_right, F_copy, New_F_left) :-
   delete(F_left, F_l, Y),
   fd_membership_2(Y-F_right, F_copy),
   fd_check_membership(F_ls, Y, F_right, F_copy, New_F_left).
fd_check_membership(
      [_|F_ls], F_left, F_right, F_copy, New_F_left) :-
   fd_check_membership(
      F_ls, F_left, F_right, F_copy, New_F_left).
fd_check_membership([], F_left, _, _, F_left).


/******************************************************************/


