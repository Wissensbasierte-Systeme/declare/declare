

/******************************************************************/
/***                                                            ***/
/***       Database Design:  FD-Algorithms for BCNF             ***/
/***                                                            ***/
/******************************************************************/



/*** test cases ***************************************************/


test(database_design:fd, fd_set_to_closure_a) :-
   test_fd_set(F),
   X = [c, e],
   fd_set_to_closure_a(F, X, Y),
   writeln(X-Y).

test(database_design:fd, fd_membership) :-
   test_fd_set(F),
   fd_membership(F, [c, d]-[b]).

test(database_design:fd, fd_set_minimize) :-
   test_fd_set(F),
   fd_set_minimize(F, G),
   writeln(G).

test(database_design:fd, fd_set_to_basis) :-
   test_fd_set(F),
   fd_set_to_basis(F, G),
   writeln(G).

test(database_design:fd, fd_set_to_all_basis) :-
   test_fd_set(F),
   fd_set_to_all_basis(F, Gs),
   state_to_tree(Gs,Tree),
   dportray(tree,Tree).

test(database_design:fd, bcnf_decomposition) :-
   test_relation_scheme(U, F),
   relation_scheme_to_bcnf_decomposition([U, F], Us, Tree), 
   writeln(Us),
   dportray(tree, Tree).
%  decomposition_tree_to_picture(_, Tree).

test(database_design:fd, bcnf_decomposition_2) :-
   U = [a,b,c,d,e], F = [[a]-[e], [b, e]-[c]],
   relation_scheme_to_bcnf_decomposition([U, F], Us, Tree),
   writeln(Us),
   dportray(tree, Tree).
%  decomposition_tree_to_picture(_, Tree).

test(database_design:fd, bcnf_decomposition_3) :-
   U = [a, b, c, d, e],
   F = [ [b, c]-[d], [d]-[e], [a, c, d]-[b], [c, e]-[b, d] ],
   relation_scheme_to_bcnf_decomposition([U, F], Us, Tree),
   writeln(Us),
   dportray(tree, Tree).
%  decomposition_tree_to_picture(_, Tree).

test(database_design:fd, '3nf_decomposition') :-
   test_relation_scheme(U, F),
   relation_scheme_to_3nf_decomposition([U, F], Rs), 
   writeln_list(Rs).

test(database_design:fd, fd_projection) :-
   F = [[a]-[e],[b,e]-[c]], writeln('F' = F),
   V = [a,b,c,d], writeln('V' = V),
   fd_projection(F, V, G),
   fd_projection_partial(F, V, H),
   writeln(fd_projection('F', 'V', G)),
   writeln(fd_projection_partial('F', 'V', H)).


test_fd_set(F) :-
   F = [ [a, b]-[c], [c]-[a], [b, c]-[d], [a, c, d]-[b], 
      [d]-[e, g], [b, e]-[c], [c, g]-[b, d], [c, e]-[a, g] ].


test_relation_scheme(U, F) :-
   U = [a, k, l, o, t],
   F = [ [l, t]-[a], [l]-[o], [o]-[k] ].


/*** interface ****************************************************/


/* relation_scheme_to_bcnf_decomposition(R, Us,Tree) <-
      Given a relation scheme R = [U, F],
      a BCNF decomposition Us, i.e. a set of attribute sets,
      is constucted together with the corresponding
      decomposition tree Tree. */

relation_scheme_to_bcnf_decomposition([U, F], Us,Tree) :-
   writeln(U), writeln(F), star_line,
   fd_is_bcnf_violating([U, F], X-Y), writeln(X-Y),
%  writeln(bcnf_violating(X-Y)), wait,
   ord_union(X, Y, XY),
   ord_subtract(Y, X, Y_minus_X),
   ord_subtract(U, Y_minus_X, XZ),
   fd_projection(F, XY, F1),
   fd_projection(F, XZ, F2),
   relation_scheme_to_bcnf_decomposition([XY, F1], Us1,Tree1),
   relation_scheme_to_bcnf_decomposition([XZ, F2], Us2,Tree2),
   ord_union(Us1, Us2, Us),
   Atom =.. [' '|U],
   Node = [Atom],
   Tree = [Node|[Tree1,Tree2]].
relation_scheme_to_bcnf_decomposition([U,_], [U], [Node]) :-
   Atom =.. [' '|U],
   Node = [Atom].


/* fd_is_bcnf_violating(R, X-Y) <-
      Given a relation scheme R=[U, F] and an fd X->Y, the call 
      succeeds if the fd is BCNF-violating in R, i.e. if the fd is 
      non-trivial and X is no superkey.  This predicate can also be 
      used for generating BCNF-violating fds. */

fd_is_bcnf_violating([U, F], X-Y) :-
   member(X-Y, F),
   \+ fd_is_trivial(X-Y),
   \+ attribute_set_is_superkey([U, F], X).


/* fd_is_trivial(X-Y) <-
      Given an fd X->Y, the call succeeds if the fd is trivial,
      i.e. if Y is a subset of X. */

fd_is_trivial(X-Y) :-
   ord_subset(Y, X).


/* fd_set_to_all_basis(Fs,Set_of_Basis) <-
      Given a set Fs of fds, the set Set_of_Basis of sets of fds is 
      determined as the set of basis of Fs. */

fd_set_to_all_basis(Fs,Set_of_Basis) :-
   findall( Basis,
      fd_set_to_basis(Fs,Basis),
      Set_of_Basis ).


/* fd_set_to_basis(Fs, Basis) <-
      Basis is a basis of the fd set Fs. */

fd_set_to_basis(Fs, Basis) :-
   fd_set_minimize(Fs, Gs),
   fd_set_to_basis_loop(Gs, Basis).

fd_set_to_basis_loop(Fs, Gs) :-
   fd_set_to_basis_loop([], Fs, Hs),
   list_to_ord_set(Hs, Gs).

fd_set_to_basis_loop(_, [], []) :-
   !.
fd_set_to_basis_loop(Fs, Gs1, Gs2) :-
%  writeln(Fs),
%  writeln(Gs1),
   fd_all_redundants(Gs1, Rs1),
%  writeln(Rs1),
%  star_line,
   Rs1 \== [],
   !,
   ord_subtract(Rs1, Fs, Rs2), 
   append(Rs3, [F|_], Rs2),
   ord_del_element(Gs1, F, Gs3),
   ord_union(Rs3, Fs, Fs1),
   fd_set_to_basis_loop(Fs1, Gs3, Gs2).
fd_set_to_basis_loop(_, Gs, Gs).


/* fd_all_redundants(Fs, Gs) <-
      Given a set Fs of fds, the set Gs of all fds that are
      redundant in Fs is derived. */

fd_all_redundants(Fs, Gs) :-
   fd_all_redundants(Fs, Fs, Gs),
   !.

fd_all_redundants(Fs, [G|Gs1], [G|Gs2]) :-
   fd_is_redundant(Fs, G),
   !,
   fd_all_redundants(Fs, Gs1, Gs2).
fd_all_redundants(Fs, [_|Gs1], Gs2) :-
   fd_all_redundants(Fs, Gs1, Gs2).
fd_all_redundants(_, [], []).


/* fd_is_redundant(Fs, G) <-
      Given a set Fs of fds, the call succeeds if G is an fd in Fs
      that is redundant in Fs, i.e. it is implied by Fs \ {G}. */
      
fd_is_redundant(Fs, G) :-
   ord_del_element(Fs, G, Gs),
   fd_membership(Gs, G).


/* attribute_set_is_superkey(R, X) <-
      Given a relation scheme R=[U, F] and a set X of attributes,
      the call succeeds if X is a superkey of R. */
 
attribute_set_is_superkey([U, F], X) :-
   fd_membership(F, X-U).
 
 
/* derive_key_of_relation_scheme(R, K) <- 
      Given a relation scheme R = [U, F],
      a key K of R is derived. */ 
 
derive_key_of_relation_scheme([U, F], K) :-
   fd_left_minimize(F, U-U, K-U).
 

/* fd_set_minimize(Fs1, Fs2) <-
      The set Fs1 of fds is transformed to the set Fs2 of fds
      by first right-minimizing all the fds in Fs1, and
      then left-minimizing all the resulting fds. */

fd_set_minimize(Fs, Gs) :-
   fd_set_right_minimize(Fs, Hs),
   fd_set_left_minimize(Hs, Gs).


/* fd_set_right_minimize(Fs1, Fs2) <-
      The set Fs1 of fds is transformed to the set Fs2 of fds
      by right-minimizing all the fds in Fs1. */

fd_set_right_minimize(Fs, Gs) :-
   fd_set_right_minimize(Fs, [], L),
   ord_union(L, Gs).

fd_set_right_minimize([F1|Fs1],Sofar, Gs) :-
   fd_right_minimize(F1, Fs),
   fd_set_right_minimize(Fs1, [Fs|Sofar], Gs).
fd_set_right_minimize([], Gs, Gs).

fd_right_minimize(Fs, Gs) :-
   fd_right_minimize(Fs, [], L),
   list_to_ord_set(L, Gs).

fd_right_minimize(X-[A|As],Sofar, Gs) :-
   fd_right_minimize(X-As, [X-[A]|Sofar], Gs).
fd_right_minimize(_-[], Gs, Gs).


/* fd_set_left_minimize(Fs1, Fs2) <-
      The set Fs1 of fds is transformed to the set Fs2 of fds
      by left-minimizing all the fds in Fs1. */

fd_set_left_minimize(Fs1, Fs2) :-
   fd_set_left_minimize(Fs1, Fs1, L),
   list_to_ord_set(L, Fs2).

fd_set_left_minimize(Fs, [G1|Gs1], [G2|Gs2]) :-
   fd_left_minimize(Fs, G1, G2),
   fd_set_left_minimize(Fs, Gs1, Gs2).
fd_set_left_minimize(_, [], []).

fd_left_minimize(Fs, X-Y, Z-Y) :-
   member(A, X),
   ord_del_element(X,A, V),
   fd_membership(Fs, V-Y),
   !,
   fd_left_minimize(Fs, V-Y, Z-Y).
fd_left_minimize(_, X-Y, X-Y).


/* fd_membership(Fs, X-Y) <-
      Given a set Fs of fds and an fd X->Y, the call succeeds
      if X->Y is implied by Fs. */

fd_membership(Fs, X-Y) :-
   fd_set_to_closure_a(Fs, X, Z),
   ord_subset(Y, Z).


/* fd_set_to_closures(Fs, Gs1, Gs2) <-
      Given two sets Fs and Gs1 of fds.
      The resulting set Gs2 consists of all fds X->W, such that
      there exists an fd X->Y in Gs1 and
      W is the closure of the union of X and Y under Fs. */
  

fd_set_to_closures(Fs, [X-Y|Gs1], [X-W|Gs2]) :-
   ord_union(X, Y, V),
   fd_set_to_closure_a(Fs, V,W),
   fd_set_to_closures(Fs, Gs1, Gs2).
fd_set_to_closures(_, [], []).


/* fd_set_to_closure_a(Fs, X, Y) <-
      The attribute set Y is the closure of the attribute set X
      under the set Fs of fds. */

fd_set_to_closure_a(Fs, X, Y) :-
   list_to_ord_set(Fs, Gs),
   fd_set_to_closure_a_loop(Gs, X, Y),
   !.

fd_set_to_closure_a_loop(Fs, X, Z) :-
   member(V-W, Fs),
   ord_subset(V, X),
   !,
   ord_del_element(Fs, V-W, Gs),
   ord_union(X,W, Y),
%  write_list(user, [Y, '  derived by  ', V-W]), nl,
   fd_set_to_closure_a_loop(Gs, Y, Z).
fd_set_to_closure_a_loop(_, X, X).


/* fd_projection(F, V, G) <-
      */

fd_projection(F, V, G) :-
   findall( Fd,
      fd_set_to_closure_special(F, V, V, []-[]->Fd),
      H ),
   fd_set_to_basis(H, G).


/* fd_set_to_closure_special(F, V, W, Fd1->Fd2) <-
      */

fd_set_to_closure_special(_, V, _, X-Y->X-Z) :-
   ord_intersection(Y, V, Z), Z \= [].
fd_set_to_closure_special(F, V, W, Fd1->Fd2) :-
   fd_set_to_closure_next(F, V, W->U, Fd1->Fd3),
   fd_set_to_closure_special(F, V, U, Fd3->Fd2).

fd_set_to_closure_next(F, V, V1->V2, X1-Y1->X2-Y2) :-
   member(X-Y, F),
   ord_subset(X, V1), ord_union(V1, Y, V2),
   ord_intersection(X, V, Z), ord_union(X1, Z, X2),
   ord_union(Y1, Y, Y2),
   Y2 \= Y1.


/* fd_projection_partial(F, V, G) <- 
      The fd set F is partially projected onto the attribute set V.
      The resulting set G consists of all non-trivial fds X->Z,
      such that there exists an fd X->Y in F, X is a subset of V,
      and Z = (X_F^+ \ X) /\ V (intersection). */

fd_projection_partial(F, V, G) :-
   findall( X-Y,
      ( member(X-_, F),
        ord_subset(X, V),
        fd_set_to_closure_a(F, X, X_plus),
        ord_intersection(V, X_plus, V_X_plus),
        ord_subtract(V_X_plus, X, Y),
        Y \= [] ),
      G ),
   !.


/* decomposition_tree_to_picture(Picture, Tree) <-
      */

decomposition_tree_to_picture(Picture, Tree) :-
   decomposition_tree_to_binary_tree(Tree, Tree_2),
   binary_tree_to_picture_bfs(Picture, Tree_2).

decomposition_tree_to_binary_tree([], []) :-
   !.
decomposition_tree_to_binary_tree(Tree_1, Tree_2) :-
   decomposition_tree_parse(Tree_1, Root_1, L_1, R_1),
   decomposition_tree_root_to_atom(Root_1, Root_2),
   decomposition_tree_to_binary_tree(L_1, L_2),
   decomposition_tree_to_binary_tree(R_1, R_2),
   decomposition_tree_parse(Tree_2, Root_2, L_2, R_2).

decomposition_tree_root_to_atom([Term], Atom) :-
   Term =.. [' '|Xs],
   concat_with_separator(Xs, '', Atom).
%  concat_with_separator(Xs, '_', Atom).
%  concat(['{', A, '}'], Atom).

decomposition_tree_parse([Root, L, R], Root, L, R).
decomposition_tree_parse([Root], Root, [], []).

   
/******************************************************************/


