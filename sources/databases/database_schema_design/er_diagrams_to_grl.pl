

/******************************************************************/
/***                                                            ***/
/***         Database Design:  ER-Diagrams in GRL               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* grl_pp_er_diagram <-
      */

grl_pp_er_diagram :-
   findall( E,
      entity(E),
      Entities ),
   findall( R,
      relationship(R),
      Relationships ),
   grl_pp_er_diagram(Entities, Relationships).


/* grl_pp_er_diagram(Entities, Relationships) <-
      */

grl_pp_er_diagram(Entities, Relationships) :-
   lattice_file(File),
   write_list(user, ['---> ', File, '\n']),
   predicate_to_file(File,
      ( grl_pp_file_header(graph, [5-5, 10-120]),
        writeln('\n  /* list of entities */ \n'),
        checklist( grl_pp_entity,
           Entities ),
        writeln('\n  /* list of relationships */ \n'),
        checklist( grl_pp_relationship,
           Relationships ),
        grl_pp_file_tail ) ),
   edge.


/* grl_pp_relationship(Relationship) <-
      */

grl_pp_relationship(Relationship) :-
   Name := Relationship^name,
   Entities := Relationship^entities,
   write_list([
      '  node: { title: "', Name,
      '"   label: "', 'R: ', Name,
      '"   borderwidth: 3  }\n']),
   ( foreach(Entity, Entities), foreach(Edge, Edges) do
        Edge = [Name, Entity] ),
   grl_pp_graph_edges(Edges, 'thickness: 2').


/* grl_pp_entity(Entity) <-
      */

grl_pp_entity(Entity) :-
   Name := Entity^name,
   Attributes_Domains := Entity^attributes,
   maplist( nth1(1),
      Attributes_Domains, Attributes ),
   write_list([
      '  node: { title: "', Name,
      '"   label: "', 'E: ', Name,
      '"   borderwidth: 1  }\n']),
   ( foreach(Attribute, Attributes), foreach(Label, Labels) do
        Label = [Name, Attribute]-Attribute ),
   grl_pp_graph_nodes_labeled(Labels, 'borderwidth: 0'),
   ( foreach(Attribute, Attributes), foreach(Edge, Edges) do
        Edge = [Name, [Name, Attribute]] ),
   grl_pp_graph_edges(Edges, 'thickness: 1   arrowstyle: none').


/* grl_pp_er_diagram_around_entity(Entity_Name) <-
      */

grl_pp_er_diagram_around_entity(Entity_Name) :-
   related_entities(Entity_Name, Entities),
   related_relationships(Entity_Name, Relationships),
   grl_pp_er_diagram(Entities, Relationships).

related_entities(Entity_Name, [E_Description|E_Descriptions]) :-
   findall( E_Description,
      ( relationship(Rel),
        Es := Rel^entities,
        two_members(Entity_Name, Entity_Name_2, Es),
        entity(E_Description),
        Entity_Name_2 := E_Description^name ),
      E_Descriptions ),
   entity(E_Description),
   Entity_Name := E_Description^name.

related_relationships(Entity_Name, Relationships) :-
   findall( Rel,
      ( relationship(Rel),
        Entities := Rel^entities,
        member(Entity_Name, Entities) ),
      Relationships ).


/******************************************************************/


