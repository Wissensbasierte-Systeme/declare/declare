

/******************************************************************/
/***                                                            ***/
/***       Database Design:  FD-Algorithms                      ***/
/***                                                            ***/
/******************************************************************/



/*** test cases ***************************************************/


test(database_design:fd, decomposition_is_independent) :-
   F = [ [b, c]-[d], [d]-[e], [a, c, d]-[b], [c, e]-[b, d] ],
%  Us = [[a, b, c], [b, c, d], [b, c, e]],
%  Us = [[b, c, d], [c, d, e], [a, c, e]],
   Us = [[b, c, d], [d, e], [a, c, e]],
   ( decomposition_is_independent(F, Us) ->
     writeln('decomposition is independent')
   ; writeln('decomposition is dependent') ).


/*** interface ****************************************************/


/* decomposition_is_independent(F, Us) <-
      */

decomposition_is_independent(F, Us) :-
   maplist( fd_projection(F),
      Us, Fs ),
   ord_union(Fs, G),
   findall( X-[A],
      ( member(X-Y, F), member(A, Y),
        \+ fd_membership(G, X-[A]) ), 
      H ),
   writeln('the following fds are lost:'),
   writeln_list(H),
   H = [].


/******************************************************************/


