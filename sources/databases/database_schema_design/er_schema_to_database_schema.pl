

/******************************************************************/
/***                                                            ***/
/***          DDK:  ER-Mapping to Database Schema               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* er_schema_to_database_schema(Schema_1, Schema_2) <-
      */

test(database_schema_design, er_schema_to_database_schema) :-
   dislog_variable_get(xul_directory,
      'examples/test/v.xml', File),
   dread(xml, File, [Schema_1]),
   er_schema_to_database_schema(Schema_1, Schema_2),
   dwrite(xml, Schema_2),
   database_schema_xml_to_create_statements(
      Schema_2, Statements),
   concat_with_separator(Statements, ';\n', X),
   concat(X, ';', Statement),
   writeln(user, Statement).

er_schema_to_database_schema(Schema_1, Schema_2) :-
   er_schema_to_strong_schema(Schema_1, Schema),
   er_schema_to_database_tables(Schema, Items_A),
   er_schema_to_nm_tables(Schema, Items_B),
   append(Items_A, Items_B, Items),
   fn_item_parse(Schema, T:As:_),
   Schema_2 = T:As:Items.


/* er_schema_to_strong_schema(Schema_1, Schema_2) <-
      */

er_schema_to_strong_schema(Schema_1, Schema_2) :-
   findall( R,
      R := Schema_1/relationship,
      Rs ),
   findall( Item_2,
      ( Item_1 := Schema_1/table,
        er_table_to_strong_table(Schema_1, Item_1, Item_2) ),
      Items_2 ),
   append(Rs, Items_2, Es),
   fn_item_parse(Schema_1, T:As:_),
   Schema_2 = T:As:Es.


/*** implementation ***********************************************/


/* er_schema_to_database_tables(Schema, Items) <-
      */

er_schema_to_database_tables(Schema, Items) :-
   findall( Item_2,
      ( Item_1 := Schema/table,
        er_table_to_database_table(Schema, Item_1, Item_2) ),
      Items ).


/* er_table_to_database_table(Schema, Item_1, Item_2) <-
      */

er_table_to_database_table(Schema, Item_1, Item_2) :-
   Table := Item_1@name,
   fn_item_parse(Item_1, T:As:Es_1),
   er_table_to_fks(Schema, Table, Fks),
   maplist( fk_term_to_xml,
      Fks, Es_Fks ),
   maplist( er_table_fk_to_attributes_xml(Schema),
      Es_Fks, Xs ),
   append(Xs, As_Fks),
   er_table_to_inherited_fks(Schema, Item_1, I_Fks),
   append([Es_1, As_Fks, I_Fks, Es_Fks], Es_2),
   Item_2 = T:As:Es_2.

er_table_fk_to_attributes_xml(Schema, Fk, Attributes) :-
   findall( A,
      ( T_Ref := Fk/references@table,
        Item := Schema/table::[@name=T_Ref],
        Name := Fk/references/attribute@name,
        B_1 := Item/attribute::[@name=Name],
        er_table_to_database_table_fk_xml_to_attribute_name(
           Fk, Name, Name_2),
        fn_item_update_attribute_value(
           name:Name_2, B_1, B_2),
        fn_item_delete_attribute_value(
           is_nullable:_, B_2, A) ),
      Attributes ).

er_table_to_database_table_fk_xml_to_attribute_name(
      Fk, Name_1, Name_2) :-
   Item := Fk/references/nth_child::N,
   Name_1 := Item@name,
   Name_2 := Fk/nth_child::N@name.


/* er_table_to_fks(Schema, Table, Fks) <-
      */

er_table_to_fks(Schema, Table, Fks) :-
   findall( Bs->T:As,
      ( er_schema_relationship(Schema, strong, n:1, Table:T),
        Item := Schema/table::[@name=T]/primary_key,
        mysql_xml_findall_attributes(Item, As),
        concat(T, '__', Prefix),
        maplist( concat(Prefix),
           As, Bs ) ),
      Fks_2 ),
   er_table_fks_adapt(Fks_2, Fks).

er_table_fks_adapt(Fks_1, Fks_2) :-
   ddbase_aggregate( [As, list(Fk)],
      ( member(Fk, Fks_1),
        Fk = (As->_:_) ),
      Groups ),
   maplist( er_table_fks_improve_modify_group,
      Groups, Fkss_2 ),
   append(Fkss_2, Fks_2).

er_table_fks_improve_modify_group([_, Fks_1], Fks_2) :-
   length(Fks_1, N),
   N > 1,
   !,
   er_table_fks_improve_modify_fks(N, Fks_1, Fks_2).
er_table_fks_improve_modify_group([_, Fks], Fks).

er_table_fks_improve_modify_fks(N, Fks_1, Fks_2) :-
   for(I, 1, N),
   foreach(As_1->T:Bs, Fks_1), foreach(As_2->T:Bs, Fks_2) do
      ( foreach(A_1, As_1), foreach(A_2, As_2) do
           concat([A_1, '__', I], A_2) ).


/* er_table_to_inherited_fks(Schema, Item, I_Fks) <-
      */

er_table_to_inherited_fks(Schema, Item, I_Fks) :-
   As_1 := Item/inherited_key/content::'*',
   Table := Item@name,
   er_schema_relationship(
      Schema, identifying, n:1, Table:T),
   Pk := Schema/table::[@name=T]/primary_key/content::'*',
   References = references:[table:T]:Pk,
   append(As_1, [References], Es),
   I_Fks = [foreign_key:Es],
   !.
er_table_to_inherited_fks(_, _, []).


/* er_schema_to_nm_tables(Schema, Items) <-
      */

er_schema_to_nm_tables(Schema, Items) :-
   findall( Item,
      ( er_schema_relationship(
           Schema, strong, n:m, T_n:T_m, Item_r),
        er_relationship_to_nm_table(
           Schema, T_n:T_m, Item_r, Item) ),
      Items ).


/* er_relationship_to_nm_table(Schema, T_n:T_m, Item_r, Item) <-
      */

er_relationship_to_nm_table(Schema, T_n:T_m, Item_r, Item) :-
   Name := Item_r@name,
   findall( A,
      A := Item_r/attribute,
      As ),
   ( As_Pk_local := Item_r/primary_key/content::'*' ->
     true
   ; As_Pk_local = [] ),
   table_to_attributes_and_fk(
      Schema, T_n, '__1', Cs_n, Ds_n, Fk_n),
   table_to_attributes_and_fk(
      Schema, T_m, '__2', Cs_m, Ds_m, Fk_m),
   append([Ds_n, Ds_m, As_Pk_local], As_Pk),
   Pk = primary_key:As_Pk,
   append([Cs_n, Cs_m, As, [Pk, Fk_n, Fk_m]], Es),
   Item = table:[name:Name]:Es.

table_to_attributes_and_fk(Schema, Table, Q, Cs, Ds, Fk) :-
   er_table_to_primary_key_attributes(Schema, Table, As),
   table_xml_attributes_prune(As, Bs),
   table_xml_attributes_qualify(Q, As, Cs),
   table_xml_attributes_prune(Cs, Ds),
   append(Ds, [references:[table:Table]:Bs], Es),
   Fk = foreign_key:Es.

table_xml_attributes_prune(As_1, As_2) :-
   foreach(A1, As_1), foreach(A2, As_2) do
      ( Name := A1@name,
        A2 = attribute:[name:Name]:[] ).

table_xml_attributes_qualify(Suffix, As_1, As_2) :-
   foreach(A1, As_1), foreach(A2, As_2) do
      ( Name_1 := A1@name,
        concat(Name_1, Suffix, Name_2),
        fn_item_update_attribute_value(name:Name_2, A1, A2) ).


/* er_table_to_strong_table(Schema, Item_1, Item_2) <-
      */

er_table_to_strong_table(Schema, Item_1, Item_2) :-
   Table := Item_1@name,
   findall( Attribute,
      Attribute := Item_1/attribute,
      Attributes_Table ),
   er_table_to_identifying_attributes(Schema, Table, Attributes),
   ( foreach(Attribute, Attributes), foreach(A, Attributes_1) do
        Name := Attribute@name,
        A = attribute:[name:Name]:[] ),
   Attributes_2 := Item_1/primary_key/content::'*',
   append(Attributes_1, Attributes_2, Attributes_Pk),
   Pk = primary_key:Attributes_Pk,
   ( Attributes_1 = [] ->
     I_Keys = []
   ; I_Keys = [inherited_key:Attributes_1] ),
   append([Attributes, Attributes_Table, [Pk], I_Keys], Items),
   Item_2 = table:[name:Table]:Items,
   !.

er_table_to_identifying_attributes(Schema, Table, Attributes) :-
   er_table_to_identifying_chain(Schema, Table, Ts),
   reverse(Ts, Ts_2),
   findall( Attribute,
      ( member(T, Ts_2),
        Item := Schema/table::[@name=T],
        er_table_to_primary_key_attribute(Item, Name_1, A),
        concat([T, '__', Name_1], Name_2),
        fn_item_update_attribute_value(name:Name_2, A, Attribute) ),
      Attributes ).


/* er_table_to_identifying_chain(Schema, Table, Tables) <-
      */

er_table_to_identifying_chain(Schema, Table, [T|Ts]) :-
   weak := Schema/table::[@name=Table]@type,
   er_schema_relationship(Schema, identifying, n:1, Table:T),
   er_table_to_identifying_chain(Schema, T, Ts).
er_table_to_identifying_chain(_, _, []).


/* er_table_to_primary_key_attributes(Schema, Table, Attribtues) <-
      */

er_table_to_primary_key_attributes(Schema, Table, Attributes) :-
   Item := Schema/table::[@name=Table],
   findall( Attribute,
      er_table_to_primary_key_attribute(Item, _, Attribute),
      Attributes ).


/* er_table_to_primary_key_attribute(Item, Name, Attribute) <-
      */

er_table_to_primary_key_attribute(Item, Name, Attribute) :-
   Name := Item/primary_key/attribute@name,
   Attribute := Item/attribute::[@name=Name].


/* er_schema_relationship(
         Schema, Type, Functionality, Table_n:Table_1) <-
      */

er_schema_relationship(Schema, Type, F, T_n:T_1) :-
   Item_r := Schema/relationship,
   Type := Item_r@type,
   [T_n, 'n'] := Item_r/table-[@name, @type],
   [T_1, '1'] := Item_r/table-[@name, @type],
   F = n:1.
er_schema_relationship(Schema, Type, F, T_n:T_1) :-
   Item_r := Schema/relationship,
   Type := Item_r@type,
   let( Xs := Item_r/table ),
   n_members(2, [X_n, X_1], Xs),
   [T_n, '1'] := X_n-[@name, @type],
   [T_1, '1'] := X_1-[@name, @type],
   F = n:1.
er_schema_relationship(Schema, Type, F, T_n:T_m, Item_r) :-
   Item_r := Schema/relationship,
   Type := Item_r@type,
   [T_n, 'n'] := Item_r/table-[@name, @type],
   [T_m, 'm'] := Item_r/table-[@name, @type],
   F = n:m.


/* fn_item_update_attribute_value(A:V, Item_1, Item_2) <-
      */

fn_item_update_attribute_value(A:V, Item_1, Item_2) :-
   fn_item_parse(Item_1, T:As_1:Es),
   ( append(Xs, [A:_|Ys], As_1) ->
     append(Xs, [A:V|Ys], As_2)
   ; As_2 = [A:V|As_1] ),
   Item_2 = T:As_2:Es.
% fn_item_update_attribute_value(A:V, Item_1, Item_2) :-
%    Item_2 := Item_1*[@A:V],


/* fn_item_delete_attribute_value(A:V, Item_1, Item_2) <-
      */

fn_item_delete_attribute_value(A:V, Item_1, Item_2) :-
   fn_item_parse(Item_1, T:As_1:Es),
   ( append(Xs, [A:V|Ys], As_1) ->
     append(Xs, Ys, As_2)
   ; As_2 = As_1 ),
   Item_2 = T:As_2:Es.
  

/******************************************************************/


