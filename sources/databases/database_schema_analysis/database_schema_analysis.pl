

/******************************************************************/
/***                                                            ***/
/***       Databases:  Schema Analysis                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* database_schema_to_primary_keys(Pks) <-
      */

database_schema_to_primary_keys(Pks) :-
   stefan_issing_database_schema(Db_Schema),
   findall( Pk,
      Pk := Db_Schema^table^primary_key,
      Pks_2 ),
   sort(Pks_2, Pks),
   length(Pks, N),
   write_list(user, [N, ' primary keys\n']).


/* database_schema_supersets_to_picture(Table) <-
      */

database_schema_supersets_to_picture(Table) :-
   database_schema_analysis(Edges),
   reachable_edges(Table, Edges, Edges_2),
   length(Edges_2, N),
   write_list(user, [N, ' edges\n']),
   edges_to_picture(Edges_2).


/* database_schema_analysis_to_picture <-
      */

database_schema_analysis_to_picture :-
   database_schema_analysis(Edges),
   edges_to_ugraph(Edges, Graph_1),
   reduce(Graph_1, Graph_2),
   length(Graph_2, N),
   write_list(user, [N, ' connected components\n']),
   ugraph_to_vertices_edges(Graph_2, Vertices_2, Edges_2),
   maplist( edge_invert,
      Edges_2, Edges_3 ),
   vertices_edges_to_picture(Vertices_2, Edges_3).


/* database_schema_analysis(Edges) <-
      */

database_schema_analysis(Edges) :-
   stefan_issing_database_schema(tables:[]:Tables),
   findall( V-W,
      ( two_members(A, B, Tables),
        V := A@name,
        W := B@name,
        As := A^primary_key^content::'*',
        As \= [],
%       Bs := B^attributes^content::'*',
        Bs := B^primary_key^content::'*',
        subset(As, Bs) ),
%       writeln(user, V-W) ),
      Edges_2 ),
   sort(Edges_2, Edges),
   length(Edges, N),
   write_list(user, [N, ' edges\n']).


/******************************************************************/


