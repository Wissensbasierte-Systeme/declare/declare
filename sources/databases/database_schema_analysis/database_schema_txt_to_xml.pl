

/******************************************************************/
/***                                                            ***/
/***       Databases:  Schema Analysis                          ***/
/***                                                            ***/
/******************************************************************/


stefan_issing_directory(Directory) :-
   home_directory(Home),
   concat(Home,
      '/teaching/diplomarbeiten/Issing_Stefan/2005_04_21/',
      Directory).

stefan_issing_database_schema(Db_Schema) :-
   stefan_issing_directory(Directory),
   concat(Directory, 'db_schema.xml', File_1),
   dread(xml, File_1, [Db_Schema]).


/*** interface ****************************************************/


/* database_schema_text_to_xml(File_1, File_2) <-
      */

database_schema_text_to_xml :-
   stefan_issing_directory(Directory),
   concat(Directory, 'tabstr_koe.txt', File_1),
   concat(Directory, 'db_schema.xml', File_2),
   database_schema_text_to_xml(File_1, File_2).

database_schema_text_to_xml(File_1, File_2) :-
   read_file_to_string(File_1, S),
   name(N, S),
   name_split_at_position(["\r"], N, M),
   list_split_at_position([['\n']], M, Ms),
   writeln(user, Ms),
   maplist( relation_list_to_xml,
      Ms, Xmls ),
   Tables = tables:Xmls,
   dwrite(xml, File_2, Tables).

relation_list_to_xml(Xs, Ys) :-
   maplist( item_prune,
      Xs, [Z|Zs] ),
   findall( attribute:[name:A]:[],
      ( member(B, Zs),
        concat('_', A, B) ),
      Keys ),
   findall( attribute:[name:B]:[],
      ( member(B, Zs),
        \+ concat('_', A, B) ),
      Non_Keys ),
   append(Keys, Non_Keys, Attributes),
   Ys = table:[name:Z]:[attributes:Attributes, primary_key:Keys].

item_prune(X, Y) :-
   name(X, [10|Xs]),
   !,
   name(Y, Xs).
item_prune(X, X).


/******************************************************************/


