

/******************************************************************/
/***                                                            ***/
/***     SQL Schema Analysis and Transformation                 ***/
/***                                                            ***/
/******************************************************************/


% fn_path_split(Path, [_]).


/*** interface ****************************************************/


/* find_tables_without_primary_key(Ys) <-
      finds all tables without a primary key and returns them */

find_tables_without_primary_key(Ys) :-
   database_schema_load(X),
   findall( table_without_primary_key:[name:Name]:[],
      ( Tbl := X^_^create_table,
        Name := Tbl@name,
        % if at least one inline or out_of_line constraint is
        % present, the table has a primary key
        not( A := Tbl^_^out_of_line_primary_key_constraint ),
        not( A := Tbl^_^inline_primary_key_constraint ) ),
      Ys ).

% input_file('/tables_without_pk.xml').


/* find_unused_columns(UnusedCols) <-
      returns all columns that are not used in any select 
      statement */

find_unused_columns(UnusedCols) :-
   database_schema_load(Xml),
   find_all_defined_columns(Xml, DefinedCols),
   find_all_used_columns(Xml, UsedCols),
   !,
   subtract_fuzzy(DefinedCols, UsedCols, UnusedCols).

% input_file('/tables_unused_comlumns.xml').


/* find_unused_tables(UnusedTables) <-
      returns all tables that are not used
      in any select statement */

find_unused_tables(UnusedTables) :-
   database_schema_load(Xml),
   find_all_defined_tables(Xml, DefinedTables),
   find_all_used_tables(Xml, UsedTables),
   !,
   subtract_fuzzy(DefinedTables, UsedTables, UnusedTables).

% input_file('/tables_unused_tables.xml').


/* find_tables_without_fk_reference(UnreferencedTables) <-
      returns all tables that are not referenced by a foreign key
      in another table */

find_tables_without_fk_reference(UnreferencedTables) :-
   database_schema_load(Xml),
   find_all_defined_tables(Xml, DefinedTables),
   find_all_fk_referenced_tables(Xml, ReferencedTables),
   !,
   subtract_fuzzy(
      DefinedTables, ReferencedTables, UnreferencedTables).

% input_file('/tables_without_fk_reference.xml').


/* draw_join_tree <-
      draws a graphical representation of all joins in the XML file.
      TODO:
       - dont forget Oracle 8i joins!
       - output table_reference lists correctly -> findall */

draw_join_tree :-
   database_schema_load(Xml),
   draw_join_tree_condition(Xml).

draw_join_tree_condition(Xml) :-
   TblRef := Xml^_^from^table_reference,
   scan_table_reference(TblRef, Joins, Tables, Connections, root),
   ( foreach(J, Joins), foreach(N, JoinNodes) do
        N = J-rhombus-blue-medium ),
   ( foreach(T, Tables), foreach(N, TableNodes) do
        N = T-circle-grey-medium ),
   append(JoinNodes, TableNodes, Nodes),
   writeln_list([Nodes, Connections]),
   vertices_edges_to_picture_bfs(
      [root-'query'-circle-black-medium|Nodes], Connections).

input_file('/projects/Refactoring/Database_Schemas/join2.xml').


/*** implementation ***********************************************/


/* database_schema_load(Xml) <-
      */

database_schema_load(Xml) :-
   input_file(File),
   dislog_variable_get(home, DisLog),
   concat(DisLog, File, Path),
   dread(xml, Path, [Xml]).


/* scan_table_reference(Xml, Joins, Tables, Connections, RootNode) <-
      scans the table */

scan_table_reference(
      Xml, [JoinID-JoinNode|Joins], Tables,
      [RootNode-JoinID|Connections], RootNode) :-
   member(X, ['', natural_, cross_]),
   concat(X, join, Selector),
   Join := Xml^joined_table^Selector,
   !,
   Type := Join@type,
   concat([X, Type, '_join'], JoinNode),
   concat([RootNode, '_', JoinNode], JoinID),
   TblRef1 := Xml^joined_table^table_reference,
   TblRef2 := Xml^joined_table^Selector^table_reference,
   scan_table_reference(TblRef1, Joins1, Tables1, Cs1, JoinID),
   scan_table_reference(TblRef2, Joins2, Tables2, Cs2, JoinID),
   append(Joins1, Joins2, Joins),
   append(Tables1, Tables2, Tables),
   append(Cs1, Cs2, Connections).
scan_table_reference(
      Xml, [], [NodeID-Node], [RootNode-NodeID], RootNode) :-
   member(Selector, [only_table_reference, query_table_reference]),
   TblExpr := Xml^Selector^query_table_expression,
   !,
   Schema := TblExpr@schema,
   Object := TblExpr@object,
   concat([Schema, '.', Object], Node),
   concat([RootNode, '_', Node], NodeID).
scan_table_reference(_, [], [], [], _).

/* cross_join
Fix Oracle Syntax and Parser accordingly
*/


/*** implementation ***********************************************/


/* subtract_fuzzy(As, Bs, Cs) <-
      */

subtract_fuzzy(As, [], As).
subtract_fuzzy(As, [S|Ss], Rs) :-
   delete_fuzzy(As, S, Rts),
   !,
   subtract_fuzzy(Rts, Ss, Rs).
subtract_fuzzy(As, [_|Ss], Rs) :-
   subtract_fuzzy(As, Ss, Rs).

delete_fuzzy([Y|Ys], X, Ys) :-
   N := X@name,
   N := Y@name,
   ( S := X@schema,
     S := Y@schema
   ; '' := X@schema ).
delete_fuzzy([Y|Ys], X, [Y|Ys]) :- 
   delete_fuzzy(Ys, X, Ys).


/* find_all_used_columns(Xml, Columns) <-
      collects the schema.columns from the select lists
      and where clauses of all select statements in the xml. */

find_all_used_columns(Xml, Cols) :-
   findall( column:[name:Column, schema:Schema]:[],
      ( Select := Xml^select,
        ( ( SelectList := Select^_^select_list^_
               ^object::[@column=Column],
            ( Schema := SelectList@schema 
            ; Schema := '' ) )
          ; ( WhereClause := Select^_^where^_
                 ^object::[@column=Column],
              ( Schema := WhereClause@schema 
              ; Schema := '' ) ) ) ),
      Cols ).

find_all_defined_columns(Xml, Cols) :-
   findall( column:[name:Column, schema:Schema]:[],
      ( Table := Xml^_^create_table,
        Schema := Table@schema,
        Column := Table^_^column_def@name ),
      Cols ).

find_all_used_tables(Xml, Tables) :-
   findall( table:[name:Name,schema:Schema]:[],
      ( member(Selector, [
           only_table_reference, query_table_reference ]),
        Table := Xml^_^table_reference
           ^Selector^query_table_expression,
        Name := Table@object,
        Schema := Table@schema ),
      Tables ).

find_all_defined_tables(Xml, Tables) :-
   findall( table:[name:Name, schema:Schema]:[],
      ( Table := Xml^_^create_table,
        Schema := Table@schema,
        Name := Table@name ),
      Tables ).

find_all_fk_referenced_tables(Xml, ReferencedTables) :-
   findall( table:[name:Name, schema:Schema]:[],
      ( Ref := Xml^_^out_of_line_foreign_key_constraint
           ^references_clause,
        Schema := Ref@schema,
        Name := Ref@object ),
      ReferencedTables ).


/******************************************************************/


