

% a simple predicate for test the desired layout

basic_dialogs_layout_test:-
    
   new(Frame, frame_with_buffers(
      'DDK - Deductive Knowledgebase Management System')),
%  send(Frame, background, colour(white)),
   
   send(Frame, append, new(Dialog_Header, dialog)),
   send(Dialog_Header, append, new(Menu_Bar, menu_bar)),
   ddb_gui_send_pulldown_menus(Frame, Menu_Bar),
    
%  create the windows
   new(Picture, picture),
   send(Picture, size, size(500, 50)),
%  define the width for all editors
   new(DG1, rdg(dg1, Picture)),     
   new(W1, rd(DG1)),   
   
   new(Editor_2, view('File', size(15, 15))),
%  the width is calculated from the W1 view
   new(DG2, rdg(dg2, Editor_2)),    
   new(W2, rd(DG2)),   
   
   new(Editor, emacs_view(width:=15, height:=15)),
%  the width is calculated from the W2 view   
   new(DG3, rdg(dg3, Editor)),    
   new(W3, rd(DG3)),    
   
   new(EV4, emacs_view(width:=15, height:=15)),
%  the width is calculated from the W2 view   
   new(DG4, rdg(dg4, EV4)),    
   new(W4, rd(DG4)), 

%  new(Dialog_Header, dialog),
%  send(Dialog_Header, label(Dialog_Header_selection_tree)),
  
%  make shrimps of dialogs, windows   
   send(W1, ver_stretch(100)),
   send(W1, hor_stretch(100)),
   
   send(W2, ver_stretch(100)),
   send(W2, hor_stretch(100)),
      
   send(W3, ver_stretch(100)),
   send(W3, hor_stretch(100)),
   
   send(W4, ver_stretch(100)),
   send(W4, hor_stretch(100)),
   
%  layout order
%  start from the W1 window and build around the other Windows
%  in this case shrimp up the size of W1 and the Menu
%  bar(Dialog_Header) ist set at last
   send(W2, above,  W1),
   send(W3, above,  W1),
   send(W4, left,   W1),
   send(Dialog_Header, above,  W1),
   
   send(Frame, open).


