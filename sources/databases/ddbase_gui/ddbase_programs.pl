

/******************************************************************/
/***                                                            ***/
/***           Deductive KBMS:  Programs                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* program_file_to_program(File, Program) <-
      */

program_file_to_program(File, Program) :-
   dread(lp, File, Program).

program_file_to_program_3(File, Program) :-
   read_term_loop_parse(File, Rules),
%  writeln(File), writeln(Rules), wait,
   sublist( dislog_regular_rule,
      Rules, Rules_2 ),
   maplist( dislog_regular_rule_normalize,
      Rules_2, Program ),
   !.

dislog_regular_rule(Head-Operator-Body-_) :-
   Head-Operator-Body \= []-(:-)-[disjunctive],
   Head-Operator-Body \= []-(:-)-[prolog].

dislog_regular_rule_normalize(Head-_-Body-_, H-B-N) :-
   ( Head = [A],
     semicolon_structure_to_list(A, H)
   ; H = [] ),
   sublist( nmr_positive_atom,
      Body, B ),
   sublist( nmr_negative_atom,
      Body, N2 ),
   negate_literals(N2, N).


/* dislog_program_normalize(Rules_1, Rules_2) <-
      */

dislog_program_normalize(Rules_1, Rules_2) :-
   maplist( dislog_rule_normalize, Rules_1, Rules_2 ).

dislog_rule_normalize(Rule_1, Rule_2) :-
   parse_dislog_rule(Rule_1, H, Bs_1, Bs_2),
   sublist( nmr_positive_atom,
      Bs_1, Body_2 ),
   sublist( nmr_negative_atom,
      Bs_1, N ),
   negate_literals(N, N_2),
   ord_union(Bs_2, N_2, Neg_2),
   simplify_rule(H-Body_2-Neg_2, Rule_2).


/* nmr_positive_atom(A) <-
      */

nmr_positive_atom(A) :-
   \+ nmr_negative_atom(A).


/* nmr_negative_atom(A) <-
      */

nmr_negative_atom(~_).
nmr_negative_atom(\+_).
nmr_negative_atom(not(_)).


/* program_file_to_program_2(File, Program) <-
      */

program_file_to_program_2(File, Program) :-
   file_name_extension(_, 'pl', File),
   !,
   read_file_to_string(File, Xs),
   append([":- disjunctive.\n", Xs, "\n:- prolog.\n"], Ys),
   name(Name, Ys),
   concat(File, '.tmp', File_2),
   predicate_to_file(File_2, write(Name)),
   diconsult(File_2, Program),
   delete_file(File_2).
program_file_to_program_2(File, Program) :-
   catch( diconsult(File, Program), _, true ).
 

/******************************************************************/


