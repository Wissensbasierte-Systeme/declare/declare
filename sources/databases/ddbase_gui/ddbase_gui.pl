

/******************************************************************/
/***                                                            ***/
/***           Deductive KBMS:  GUI                             ***/
/***                                                            ***/
/******************************************************************/


:- Mode = semweb,
%  Mode = fnquery,
   dislog_variable_set(ontology_evaluation_mode, Mode).

:- [ddbase_resizable_ds].


/*** interface ****************************************************/


/* deductive_kbms_gui <-
      */

ddbase :-
   deductive_kbms_gui.

deductive_kbms_gui :-
   dislog_variable_set(ddb_gui_mode, together),
   Title =
      'DDBase - Deductive Database of Declare',
   new(Frame, frame_with_buffers(Title)),
%  send(Frame, background, colour(white)),    

   send(Frame, append, new(Dialog_Header, dialog)),
   send(Dialog_Header, append, new(Menu_Bar, menu_bar)),
   ddb_gui_send_pulldown_menus(Frame, Menu_Bar),
%  ddb_gui_send_toolbar(Dialog_Header),
   
%  new(Dialog_Header, dialog),
%  send(Dialog_Header, label(Dialog_Header_selection_tree)),

   create_the_kbms_hierarchy(Xml),
   Xml_2 := Xml * [@name:'Declare/examples'],
   alias_to_default_fn_triple(kbms_browser_config, Config),
   new(CDW, hierarchy_browser_ddk(Config, Xml_2)),

   new(EB, emacs_buffer('File')),
   send(EB, auto_save_mode, @off),
   send(EB, mode(prolog)),
   new(Editor, emacs_view(EB, 65, 20)),

   new(Editor_2, view('File', size(65, 10))),
   
   dislog_variable_get(fn_query_gui_font_size, Font_Size),
   send(Editor, font, font(screen, roman, Font_Size)),
   send(Editor_2, font, font(screen, roman, Font_Size)),
   
   new(Picture, picture),
   send(Picture, size, size(600, 200)),
   
   deductive_kbms_gui_create_dialog(
      'Graph',          W1, Picture),
   deductive_kbms_gui_create_dialog(
      'Evaluation',     W2, Editor_2),
   deductive_kbms_gui_create_dialog(
      'Program',        W3, Editor),
   deductive_kbms_gui_create_dialog(
      'Knowledge Base', W4, CDW),

   send(W2, above, W1),
   send(W3, above, W1),
   send(W4, left, W1),
   send(Dialog_Header, above, W1),
   
   dislog_variable_set(
      ddb_gui, [CDW, Editor, Editor_2, Picture]),
   send(Frame, open).

deductive_kbms_gui_create_dialog(Label, Dialog, Inner) :-
   new(Dialog_Group, rdg(Label, Inner)),
   new(Dialog, rd(Dialog_Group)),
%  make shrimps of dialog, window
   send(Dialog, ver_stretch(100)),
   send(Dialog, hor_stretch(100)).


deductive_kbms_gui_separate :-
%  dislog_variable_set(ddb_gui_mode, separate),
   program_files_to_html_with_table(Html),
   html_to_cms_doc_window('Disjunctive Databases',
      Html, size(600,300)).


/*** implementation ***********************************************/


/* ddb_gui_send_pulldown_menus(Frame, Menu_Bar) <-
      */

ddb_gui_send_pulldown_menus(Frame, Menu_Bar) :-
   send_pulldown_menu(Menu_Bar, 'File', [
      'FnQuery' - fn_query_gui_new,
      'Quit' - send(Frame, destroy) ]),
   send_pulldown_menu(Menu_Bar, 'Evaluate', [
      'Minimal Models'        - ddb_gui_evaluate(hi_minimal),
      'Perfect Model'         - ddb_gui_evaluate(perfect),
      'Stable Models'         - ddb_gui_evaluate(hi),
      'Semi-Stratified'       - ddb_gui_evaluate(stable_star),
      'Partial Stable Models' - ddb_gui_evaluate(hi_partial),
      'Well Founded Model'    - ddb_gui_evaluate(wfs),
      'Stable State'          - ddb_gui_evaluate(dhb),
      'Ontology Evaluation'   - ddb_gui_evaluate(ontology),
      'Hyperresolution'       - ddb_gui_evaluate(hyperresolution)
      ]),
   send_pulldown_menu(Menu_Bar, 'Transform', [
      'Magic Sets Transformation' -
         ddb_gui_evaluate(magic_sets_transformation),
      'Magic Sets Evaluation' -
         ddb_gui_evaluate(magic_sets_evaluation),
      'KNF Normalization' -
         ddb_gui_evaluate(knf_normalization),
      'KNF Evaluation' -
         ddb_gui_evaluate(knf_evaluation),
      'Predicate Dependency Graph' -
         ddb_gui_evaluate(prolog_program_to_graph_picture) ]),
   send_pulldown_menu(Menu_Bar, 'Help', []).


/* ddb_gui_evaluate(Mode) <-
      */

ddb_gui_evaluate(Mode) :-
   Mode = ontology,
   dislog_variable_get(ontology_evaluation_mode, semweb),
   !,
   dislog_variable_get(ddb_gui_current_file, Path),
   dislog_variable_get(output_path, Results),
   concat(Results, ddb_gui_evaluate_result, Path_2),
   owl_ontology_evaluation(Path, Program),
   predicate_to_file( Path_2,
      dportray(lp, Program) ),
   dislog_variable_get(ddb_gui, [_, _, Editor_2, _]),
   send(Editor_2, load(Path_2)).
ddb_gui_evaluate(Mode) :-
   Mode = ontology,
   dislog_variable_get(ontology_evaluation_mode, fnquery),
   !,
   dislog_variable_get(ddb_gui_current_file, Path),
   dislog_variable_get(source_path,
      'projects/ontologies/owl_ontology_to_rules_fnquery.fng',
      Fng_File),
   dislog_variable_get(output_path,
      ddb_gui_evaluate_result, Path_2),
   fn_select_for_ontology_evaluation(Fng_File, Path, Program),
   predicate_to_file( Path_2,
      dportray(lp, Program) ),
   dislog_variable_get(ddb_gui, [_, _, Editor_2, _]),
   send(Editor_2, load(Path_2)).

ddb_gui_evaluate(wfs) :-
   ddb_gui_input_program(Database),
   dislog_variable_get(output_path,
       ddb_gui_evaluate_result, Path_2),
   ( \+ ( member(Rule, Database),
          \+ parse_dislog_rule_save(Rule, [_], _, _) ),
     predicate_to_file( Path_2,
        gamma_iteration(dportray(Path_2, tu_pair), Database, _) )
   ; predicate_to_file( Path_2,
        writeln('The input was not a normal logic program !') ) ),
   dislog_variable_get(ddb_gui, [_, _, Editor_2, _]),
   send(Editor_2, load(Path_2)).

ddb_gui_evaluate(Mode) :-
   member(Mode, [hi, hi_partial, dhb, hi_minimal, stable_star]),
   ddb_gui_input_program(Database),
   !,
%  semantics(stable, Mode, Database, State),
   ( member(Mode, [hi, dhb]) ->
     program_to_stable_models_dlv(Database, State_2)
   ; Mode = stable_star ->
     dislog_rules_to_prolog_rules(Database, Prolog),
     evaluation_semi_stratified(Prolog, State_2),
     ( State_2 = [I] ->
       Title = 'Stable Model',
       xpce_display_herbrand_interpretation(Title, I)
     ; true )
   ; Mode = hi_minimal ->
     disjunctive_transformation(Database, Database_2),
     program_to_stable_models_dlv(Database_2, State_2)
   ; partial_stable_models(Database, State_2) ),
   ( member(Mode, [hi_partial, hi_minimal]) ->
     Mode_2 = hi
   ; Mode_2 = Mode ),
   ( member(Mode_2, [hi, stable_star]) ->
     State_3 = State_2
   ; Mode_2 = dhb,
     tree_based_boolean_dualization(State_2, State_3) ),
   maplist( sort,
      State_3, State ),
%  bar_line_2,
   change_hb_to_hs(Mode_2, Portray_Mode),
   dislog_variable_get(output_path,
      ddb_gui_evaluate_result, Path_2),
   ddb_gui_evaluate_first_line(Mode_2, State, Line),
   predicate_to_file( Path_2,
      ( write(Line),
        dportray(Portray_Mode, State) ) ),
   state_to_tree(State, Tree),
%  dportray(lp, State),
   !,
   set_tree_to_picture_dfs(Tree),
   dislog_variable_get(ddb_gui, [_, _, Editor_2, _]),
   send(Editor_2, load(Path_2)).
ddb_gui_evaluate(hyperresolution) :-
   ddb_gui_input_program_3(Database),
   dnlp_simplify(Database, Database_2),
   !,
   ( dislog_program_is_for_tp_iteration_prolog(Database_2) ->
%    dislog_program_is_definite(Database_2) ->
%    Goal = tp_iteration(Database_2, _)
     program_range_restrict(Database_2, Database_E),
     dislog_rules_to_prolog_rules(Database_E, Database_3),
     Goal = tp_iteration_prolog(Database_3, prolog_module, [], I)
   ; dislog_program_is_disjunctive(Database_2) ->
%    writeln(user, disjunctive),
     Goal = tps_fixpoint_iteration(Database_2, I)
   ; true, I = [] ),
   dislog_variable_get(output_path,
      ddb_gui_evaluate_result, Path_2),
   predicate_to_file( Path_2, Goal ),
   Title = 'Hyperresolution',
   xpce_display_herbrand_interpretation(Title, I),
   !,
   dislog_variable_get(ddb_gui, [_, _, Editor_2, _]),
   send(Editor_2, load(Path_2)).
ddb_gui_evaluate(perfect) :-
%  ddb_gui_input_program_2(Prolog),
   ddb_gui_input_program(Database),
   dislog_rules_to_prolog_rules(Database, Prolog),
   !,
   dislog_variable_get(output_path,
      ddb_gui_evaluate_result, Path_2),
   Goal = perfect_model_prolog(Prolog, [], I),
   predicate_to_file( Path_2, Goal ),
   Title = 'Perfect Model',
   xpce_display_herbrand_interpretation(Title, I),
   !,
   dislog_variable_get(ddb_gui, [_, _, Editor_2, _]),
   send(Editor_2, load(Path_2)).
ddb_gui_evaluate(magic_sets_transformation) :-
   ddb_gui_input_program(Database),
   dnlp_simplify(Database, Database_a),
   !,
   ( ddb_gui_program_extract_query(Database_a, Database_b, Query_1),
     disjunctive_transformation(Database_b, Database_1),
     magic_sets_transformation(Database_1, Query_1, Database_2, Query_2),
     ddb_gui_output_program([[query]-[Query_2]|Database_2])
   ; ddk_message(['magic sets transformation has failed']) ).
ddb_gui_evaluate(magic_sets_evaluation) :-
   ddb_gui_input_program(Database),
   dnlp_simplify(Database, Database_a),
   ddb_gui_program_extract_query(Database_a, Database_b, Query_1),
   !,
   Goal = ( disjunctive_transformation(Database_b, Database_1),
      magic_sets_evaluation(Database_1, Query_1, State) ),
   dislog_variable_get(output_path,
      ddb_gui_evaluate_result, Path_2),
   ( predicate_to_file( Path_2,
        ( Goal,
          write('Answers = '), dportray(dhs, State) ) )
   ; ddk_message(['magic sets evaluation has failed']) ),
   !,
   dislog_variable_get(ddb_gui, [_, _, Editor_2, _]),
   send(Editor_2, load(Path_2)).

ddb_gui_evaluate(knf_normalization) :-
   ddb_gui_input_program_cknf(Program_1),
   linear_sirup_to_knf(Program_1, Program_2),
   linear_sirup_to_as_graph_edges_to_picture(Program_1),
   ddb_gui_output_program_4(Program_2),
   dportray(lp_window, Program_2).
ddb_gui_evaluate(knf_evaluation) :-
   ddb_gui_evaluate(knf_normalization),
   dislog_variable_get(output_path, ddb_gui_file_output, Path),
   dread(pl, Path, Program_1),
   Goal = tp_iteration_prolog(Program_1, prolog_module, [], I),
   dislog_variable_get(output_path,
      ddb_gui_evaluate_result, Path_2),
   predicate_to_file( Path_2, Goal ),
   Title = 'Hyperresolution',
   xpce_display_herbrand_interpretation(Title, I),
   !,
   dislog_variable_get(ddb_gui, [_, _, Editor_2, _]),
   send(Editor_2, load(Path_2)).
ddb_gui_evaluate(prolog_program_to_graph_picture) :-
   ddb_gui_input_program_2(Database_1),
   !,
   copy_term(Database_1, Database),
   dislog_variable_get(ddb_gui, [_, _, _, Picture]),
   prolog_program_select_non_directives(Database, Database_2),
   prolog_program_to_rule_goal_graph_to_picture(Database_2, Picture).


/* ddb_gui_input_program..(Program) <-
      */

ddb_gui_input_program_2(Program) :-
   dislog_variable_get(output_path, ddb_gui_file_input, Path),
   dislog_variable_get(ddb_gui, [_, Editor, _, _]),
   send(Editor, save(Path)),
   dread(pl, Path, Program).

ddb_gui_input_program_3(Program) :-
   dislog_variable_get(output_path, ddb_gui_file_input, Path),
   dislog_variable_get(ddb_gui, [_, Editor, _, _]),
   send(Editor, save(Path)),
   program_file_to_program(Path, Program).

ddb_gui_input_program(Program) :-
   ddb_gui_input_program_3(Rules),
   dislog_program_normalize(Rules, Program).

ddb_gui_input_program_cknf(Program) :-
   dislog_variable_get(output_path, ddb_gui_file_input, Path),
   dislog_variable_get(ddb_gui, [_, Editor, _, _]),
   send(Editor, save(Path)),
   cknf_read_program(Path, Program).


/* ddb_gui_output_program(Program) <-
      */

ddb_gui_output_program(Program) :-
   dislog_variable_get(output_path, ddb_gui_file_output, Path),
   predicate_to_file( Path,
      dportray(lp_magic, Program) ),
   dislog_variable_get(ddb_gui, [_, _, Editor_2, _]),
   send(Editor_2, load(Path)).
 
ddb_gui_output_program_4(Program) :-
   dislog_variable_get(output_path, ddb_gui_file_output, Path),
   dportray(Path, lp_4, Program),
   read_file_to_name(Path, Name_1),
   name_exchange_sublist([[",", ", "], ["^", "_"]], Name_1, Name_2),
   name_exchange_sublist([[" \n", "\n"]], Name_2, Name_3),
   predicate_to_file( Path, write(Name_3) ),
   dislog_variable_get(ddb_gui, [_, _, Editor_2, _]),
   send(Editor_2, load(Path)).


/* ddb_gui_evaluate_first_line(Mode, State, Line) <-
      */

ddb_gui_evaluate_first_line(Mode, State, Line) :-
   length(State, N),
   ( member(Mode, [hi, stable_star]),
     X = model
   ; Mode = dhb,
     X = disjunction ),
   ( N = 1,
     Suffix = ''
   ; N \= 1,
     Suffix = 's' ),
   concat([N, ' ', X, Suffix, ':'], Line).
   

/* ddb_gui_program_extract_query(Program_1, Program_2, Query) <-
      */

ddb_gui_program_extract_query(Program_1, Program_2, Query) :-
   member(Rule, Program_1),
   parse_dislog_rule(Rule, [], [Query], []),
   !,
   delete(Program_1, Rule, Program_2).


/* fn_select_for_ontology_evaluation(File_S, File, Program) <-
      */

fn_select_for_ontology_evaluation(File_S, File, Program) :-
   fn_item_substitution_tmp_prepare(File_S, Predicate),
   dread(xml, File, Xml),
   last(Xml, Item),
   apply(Predicate, [Item, Program]).


/* create_the_kbms_hierarchy(Xml) <-
      */

create_the_kbms_hierarchy(Xml) :-
   dislog_variable_get(example_path, Example_Path),
   Patterns_1 = [
      'deductive',
      'nmr',
      'xml',
      'ontologies',
      'd3',
      'teaching' ],
   maplist( concat(Example_Path),
      Patterns_1, Patterns_2 ),
   maplist( file_system_to_xml,
      Patterns_2, Dirs_1 ),
   maplist( dir_path_to_name,
      Dirs_1, Dirs_2 ),
   Xml = dir:[name:Example_Path]:Dirs_2.

dir_path_to_name(Dir_1, Dir_2) :-
   Dir_1 = dir:[name:Path]:C,
   file_base_name(Path, Name),
   Dir_2 = dir:[name:Name]:C.
dir_path_to_name(Dir, Dir).


/* program_files_to_html_with_table(Html) <-
      */

program_files_to_html_with_table(Html) :-
   Patterns = [
      'deductive/*',
      'nmr/*',
      'xml/*',
      'ontologies/*',
      'd3/*',
      'teaching/*' ],
   current_num(join_mode, Join_Mode),
   maplist( show_dislog_data_dictionary(database),
      Patterns, Relations ),
   set_num(join_mode, Join_Mode),
   append(Relations, Relation),
   findall( Row,
      ( member([_, File, X, Y, Z], Relation),
        name_split_at_position(["/"], File, [A, B]),
        concat([
           'prolog:program_file_to_dependency_graph_xpce_command(''',
           File, ''')'], Call),
        concat([Y, ' / ', Z], YZ),
        Row = tr:[bgcolor:'#ffffff']:[
           td:[A], td:[a:[href:Call]:[B]],
%          td:[a:[href:Call]:[File]],
           td:[X], td:[YZ]] ),
      Rows ),
   rgb_to_colour_hex([14, 14, 15.99], Colour_header),
%  Header = tr:[bgcolor:'#eeeeee']:[
   Header = tr:[bgcolor:Colour_header]:[
      th:[align:left]:['Directory'],
      th:[align:left]:['File'],
      th:[align:left]:['Facts'],
      th:[align:left]:['Rules'] ],
   rgb_to_colour_hex([14.5, 14.5, 15.99], Colour_table),
   Table = table:[
%     border:1, bgcolor:'#f7f7f7', align:center]:[
      border:0, bgcolor:Colour_table,
      align:center, cellspacing:2]:[
      Header|Rows],
   Html = html:[
      body:[bgcolor:'#ffeecc']:[
         font:[face:'Times New Roman', size:'-5']:[Table] ] ].


/* program_file_to_dependency_graph_xpce_command(File) <-
      */

program_file_to_dependency_graph_xpce_command(File) :-
   file_name_extension(_, Extension, File),
   member(Extension, [xsl, xslt]),
   !,
   dislog_variable_get(ddb_gui, [_, Editor, Editor_2, Picture]),
   send(Editor_2, clear),
   dislog_variable_get(example_path, Examples),
   concat(Examples, File, Path),
   send(Editor, load(Path)),
   xslt_file_to_picture(Path, Picture),
   dislog_variable_set(ddb_gui_current_file, Path).
program_file_to_dependency_graph_xpce_command(File) :-
   file_name_extension(_, Extension, File),
   member(Extension, [xml, owl, gxl, html, htm]),
   !,
   dislog_variable_get(ddb_gui, [_, Editor, Editor_2, Picture]),
   send(Editor_2, clear),
   dislog_variable_get(example_path, Examples),
   concat(Examples, File, Path),
   send(Editor, load(Path)),
   dread(xml, Path, Xml),
   last(Xml, KB),
   fn_triple_to_schema_picture_2(KB, Picture),
   dislog_variable_set(ddb_gui_current_file, Path).
program_file_to_dependency_graph_xpce_command(File) :-
%  writeln(user, 1),
   dislog_variable_get(ddb_gui_mode, separate),
   dislog_variable_get(example_path, Examples),
   concat(Examples, File, Path),
   dislog_variable_get(ddb_gui, [_, _, _, Picture]),
   program_file_to_program(Path, Program),
   program_to_dependency_graph_xpce(Program, Picture),
   file_view(Path),
   dislog_variable_set(ddb_gui_current_file, Path).
program_file_to_dependency_graph_xpce_command(File) :-
   dislog_variable_get(ddb_gui_mode, together),
   program_file_to_dependency_graph_xpce_command_(File),
   program_file_to_dependency_graph_xpce_command_(File).

program_file_to_dependency_graph_xpce_command_(File) :-
   dislog_variable_get(ddb_gui, [_, Editor, Editor_2, Picture]),
   send(Editor_2, clear),
   dislog_variable_get(example_path, Examples),
   concat(Examples, File, Path),
   catch(
      program_file_to_dependency_graph_to_picture(Path, Picture),
      _, true ),
   send(Editor, load(Path)),
   dislog_variable_set(ddb_gui_current_file, Path).

program_file_to_dependency_graph_to_picture(Path, Picture) :-
   program_file_to_program(Path, Program_1),
   dislog_program_select_non_directives(Program_1, Program),
   dislog_program_without_dynamics(Program, Program_2),
   dislog_program_is_disjunctive(Program_2),
   !,
   program_to_dependency_graph_xpce(Program_2, Picture).
program_file_to_dependency_graph_to_picture(_, _) :-
   ddb_gui_evaluate(prolog_program_to_graph_picture).
program_file_to_dependency_graph_to_picture(Path, Picture) :-
   catch(dread(pl, Path, Program_1), _, Program_1 = []),
   prolog_program_select_non_directives(Program_1, Program),
   prolog_program_without_dynamics(Program, Program_2),
%  prolog_program_to_rule_goal_graph(Program, [Vertices, Edges]),
%  program_vertices_and_edges_to_picture(Vertices, Edges, Picture).
   prolog_rules_to_dislog_rules(Program_2, DisLog),
   program_to_dependency_graph_xpce(DisLog, Picture).


/* program_file_to_dependency_graph(Node) <-
      */

program_file_to_dependency_graph(Node) :-
   get(Node, xml, Xml),
   Path := Xml@path,
   dislog_variable_get(example_path, Examples),
   concat(Examples, Path_2, Path),
   program_file_to_dependency_graph_xpce_command(Path_2).


/******************************************************************/


