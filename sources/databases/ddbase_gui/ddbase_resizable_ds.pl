

:- pce_begin_class(rd, dialog,
      "Dialog whit only one resizable dialog group").

initialise(D, GD:rdg) :->
   send(D, send_super, initialise),
   send(D, ver_shrink(100)),
   send(D, hor_shrink(100)),
%  send(D, gap, size(15,15)),
   send(D, append, GD),
   send(D, resize_message, message(D, resize_d, @arg2)).
        
                
resize_d(D, Size:size) :->
   "Resize all graphical members to fit the dialog"::   
   get(D, graphicals, DGs),
%  there must be only one dialog group
%  else you have to calculate the appropiate size for each member            
   send(DGs, for_all, message(@arg1, resize_dg, Size) ).

:- pce_end_class.


:- pce_begin_class(rdg, dialog_group,
      "Dialog group whit resizable elements").

initialise(DG, Label:any, View:any) :->
   send(DG, send_super, initialise, Label),       
   send(DG, gap, size(10,10)),
   send(DG, append, View).      
                
resize_dg(DG, Size:size) :->
   "Resize all graphical members to fit the dialog"::      
   get(Size, height, Height),
   get(Size, width, Width),
   Tab_H is Height - 50,
%  distance would depend on defined gap size
   Tab_W is Width - 50,     
   get(DG, graphicals, Editor),    
   send(Editor, for_all,
      message(DG, set_geometry, @arg1, size(Tab_W, Tab_H))),
   send(DG, layout_dialog).
           
set_geometry(_GD, View, Size) :->   
   send(View,
      geometry(@default, @default, Size?width, Size?height)).
           
:- pce_end_class.


