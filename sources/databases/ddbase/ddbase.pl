

/******************************************************************/
/***                                                            ***/
/***          DDBase:  Deductive Database                       ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(ddbase, ddbase_database_load) :-
   ddbase_database_load(company, company).

test(ddbase, ddbase_call) :-
   ddbase_aggregate(
      [ SSN_2,
        list_to_comma_atom((PNO, Hours)),
        sum(Hours) ],
      ( ddbase_call( odbc(mysql),
           company:works_on(SSN_1, PNO, Hours) ),
        first_n_characters(4, SSN_1, SSN_2) ),
      Tuples ),
   Attributes = ['SSN', 'Workload', 'Hours'],
   xpce_display_table(Attributes, Tuples).


/*** interface ****************************************************/


/* ddbase_load(odbc(Connection), Database, Module) <-
      */

ddbase_load(odbc(Connection), Database, Module) :-
   ddbase_database_load(Connection, Database, Module).


/* ddbase_database_load(Database, Module) <-
      */

ddbase_database_load(Database, Module) :-
   dislog_variable_get(
      odbc_connection_data_dictionary, Connection),
   ddbase_database_load(Connection, Database, Module).

ddbase_database_load(Connection, Database, Module) :-
   dislog_variable_get(home,
      '/results/database_schema.pl', File),
   gensym(database_schema__, File_S),
   concat(['/results/', File_S, '.pl'], File_P),
   dislog_variable_get(home, File_P, Path_P),
   file_copy(File, Path_P),
   mysql_database_schema_to_ddbase_facts(
      Connection, Database, Path_P),
   ddbase_load(Path_P, Module),
   ddbase_load_tables(Module),
   file_remove(Path_P).


/* ddbase_database_ensure_loaded(Connection, Database, Module) <-
      */

ddbase_database_ensure_loaded(Connection, Database, Module) :-
   ( catch( call(
        ( Module:ddbase_schema(Table),
          Database := Table@database ) ), _, fail ) -> true
   ; catch( call(
        ddbase_database_load(Connection, Database, Module) ),
        _, fail ) ).


/* ddbase_load(File, Module) <-
      */

ddbase_load(File, Module) :-
   retractall(Module:ddbase_schema(_)),
   dynamic(Module:ddbase_schema/1),
   style_check(-singleton),
   Module:consult(File),
   style_check(+singleton),
   ddbase_drop_database(Module),
   ddbase_show_tables(Module).


/* ddbase_show_tables(Module) <-
      */

ddbase_show_tables(Module) :-
   dynamic(Module:ddbase_schema/1),
   findall( [Database, Table, N],
      ( Module:ddbase_schema(Schema),
        Table := Schema@name,
        Database := Schema@database,
        ddbase_facts_to_table(Module:Table/_, _, _, Rows),
        length(Rows, N) ),
      Rows ),
   concat('DDBase ', Module, Title),
   ( dislog_variable_get(ddbase_show_tables_xpce, yes) ->
     xpce_display_table(
        Title, ['Database', 'Table', 'Tuples'], Rows)
   ; true ).


/* ddbase_describe_table(Database:Table) <-
      */

ddbase_describe_table(Database:Table) :-
   mysql_relation_schema_to_xml(mysql, Database:Table, Xml),
   dwrite(xml, Xml).


/* ddbase_load_tables(Module) <-
      */

ddbase_load_tables(Module) :-
   findall( [Database, Table, N],
      ( Module:ddbase_schema(Schema),
        Table := Schema@name,
        Database := Schema@database,
        mysql_table_to_prolog_module(
           Database:Table, Module, Rs),
        length(Rs, N) ),
      Rows ),
   concat('DDBase ', Module, Title),
   ( dislog_variable_get(ddbase_show_tables_xpce, yes) ->
     xpce_display_table(
        Title, ['Database', 'Table', 'Tuples'], Rows)
   ; true ).


/* ddbase_facts_to_display(Mode, Module:Predicate/Arity) <-
      */

ddbase_facts_to_display(Mpa) :-
   ddbase_facts_to_display(xpce, Mpa).

ddbase_facts_to_display(Mode, M:P/A) :-
   ddbase_facts_to_table(M:P/A, Title, Attributes, Rows),
   concat([Mode, '_', display_table], Display),
   apply(Display, [Title, Attributes, Rows]).
ddbase_facts_to_display(Mode, P/A) :-
   ddbase_facts_to_display(Mode, user:P/A).
ddbase_facts_to_display(Mode, M) :-
   M \= _:_/_,
   M \= _/_,
   forall( ddbase_facts_to_display(Mode, M:_/_),
      true ).


/* ddbase_facts_to_table(M:P/A, Title, Attributes, Rows) <-
      */

ddbase_facts_to_table(M:P/A, Title, Attributes, Rows) :-
   ddbase_predicate_to_attributes(M:P/A, Attributes),
   collect_facts(M:P/A, Facts),
   ( foreach(Fact, Facts), foreach(Row, Rows) do
        Fact =.. [_|Xs],
        maplist( term_to_atom,
           Xs, Row ) ),
   term_to_atom(M:P/A, Title).

ddbase_predicate_to_attributes(M:P/A, Attributes) :-
   ( M:dynamic(ddbase_schema/1),
     M:ddbase_schema(Schema),
     P := Schema@name,
     Attributes := Schema/attributes/content::'*',
     length(Attributes, A)
   ; nonvar(P),
     nonvar(A),
     multify(' ', 15, Xs),
     concat(Xs, X),
     multify(X, A, Attributes) ).


/* ddbase_table_to_primary_key(Database:Table, Key) <-
      */

ddbase_table_to_primary_key(Database:Table, Key) :-
   mysql_table_to_primary_key(Database:Table, Key).


/* ddbase_value_to_table_and_attribute(
         Connection, Database, Value, Table, Attribute) <-
      */

ddbase_value_to_table_and_attribute(
      Connection, Database, Value, Table, Attribute) :-
   mysql_database_to_xml(Connection, Database, Xml),
   X := Xml/table,
   Table := X@name,
   Value := X/row@Attribute.


/* ddbase_insert(Module, Atom) <-
      */

ddbase_insert(Module:Atom) :-
   ddbase_insert(Module, Atom).

ddbase_insert(Module, Atom) :-
   ddbase_atom_make_dynamic(Module, Atom),
   Module:Atom,
   !,
   write_list(user,
      ['\nAtom already in database ', Module, '.\n']).
ddbase_insert(Module, Atom) :-
   assert(Module:Atom),
   ( ddbase_inconsistent_multiple(Module, Violations),
     \+ ([] := Violations/content::'*') ->
     retract(Module:Atom),
     writeln(user,
        '\nAtom not inserted, because of IC violation:\n'),
     dwrite(xml, Violations)
   ; writeln(user, '\nAtom successfully inserted.') ).


/* ddbase_delete(Module, Atom) <-
      */

ddbase_delete(Module:Atom) :-
   ddbase_delete(Module, Atom).

ddbase_delete(Module, Atom) :-
   ddbase_atom_make_dynamic(Module, Atom),
   \+ Module:Atom,
   !,
   write_list(user,
      ['\nAtom not in database ', Module, '.\n']).
ddbase_delete(Module, Atom) :-
   retract(Module:Atom),
   ( ddbase_inconsistent_multiple(Module, Violations),
     \+ ([] := Violations/content::'*'),
     !,
     assert(Module:Atom),
     writeln(user,
        '\nAtom not deleted, because of IC violation:\n'),
     dwrite(xml, Violations)
   ; writeln(user, '\nAtom successfully deleted.') ).


/* ddbase_drop_table(M:P/A) <-
      */

ddbase_drop_table(M:P/A) :-
   predicate_and_arity_to_atom(M:P/A, Atom),
   retract_all(Atom).


/* ddbase_drop_database(Module) <-
      */

ddbase_drop_database(Module) :-
   dynamic(Module:ddbase_schema/1),
   forall( Module:ddbase_schema(Schema),
      ( Table := Schema@name,
        Attributes := Schema/attributes/content::'*',
        length(Attributes, Arity),
        ddbase_drop_table(Module:Table/Arity) ) ).


/* ddbase_atom_make_dynamic(Module, Atom) <-
      */

ddbase_atom_make_dynamic(Module, Atom) :-
   functor(Atom, P, N),
   Module:dynamic(P/N).


/*** tests ********************************************************/


test(ddbase, ddbase_load) :-
   ddbase_load('examples/teaching/company/company.pl', c).
test(ddbase, ddbase_load_tables) :-
   ddbase_load_tables(c).
test(ddbase, ddbase_drop_database) :-
   ddbase_drop_database(c).
test(ddbase, ddbase_show_tables) :-
   ddbase_show_tables(c).
test(ddbase, ddbase_facts_to_display) :-
   ddbase_facts_to_display(c:works_on/3).
test(ddbase, ddbase_facts_to_display_all) :-
   ddbase_facts_to_display(c).
test(ddbase, ddbase_insert) :-
   ddbase_insert(c, works_on('111111111', 10, 3)),
   ddbase_insert(c, works_on('111111111', 10, 4)),
   ddbase_delete(c, works_on('111111111', 10, 3)).


/******************************************************************/


