

/******************************************************************/
/***                                                            ***/
/***       Declare:  DDQL - NLI, Optimizer                      ***/
/***                                                            ***/
/******************************************************************/


/*** tests ********************************************************/


:- style_check(-singleton).

test(fn_tuples, 1) :-
   Atom = [a:X, b:Y, c:Z],
   fn_tuple_project([b], Atom, Zs),
   writelnq(Atom),
   writelnq(Zs).
test(fn_tuples, 2) :-
   Atom_1 = employee:['SSN':X, 'SALARY':Y],
   Atom_2 = employee:['SSN':X, 'AGE':Y],
   Atom_3 = employee:['SSN':X, 'FNAME':'Borg'],
   Atoms_A = [Atom_1, Atom_2, Atom_3],
   ddql_fn_atoms_reduce(company, Atoms_A, Atoms_B),
   dwrite(lp, user, [Atoms_B-Atoms_A]).

:- style_check(+singleton).


/*** interface ****************************************************/


/* ddql_fn_atoms_reduce(Database, Atoms_1, Atoms_2) <-
      */

ddql_fn_atoms_reduce(Database, Atoms_1, Atoms_2) :-
   ddql_fn_atoms_reduce_(Database, Atoms_1, Atoms_3),
   !,
   ddql_fn_atoms_reduce(Database, Atoms_3, Atoms_2).
ddql_fn_atoms_reduce(_, Atoms, Atoms).

ddql_fn_atoms_reduce_(Database, Atoms_1, Atoms_2) :-
   append(As, [Atom_1|Atoms], Atoms_1),
   append(Bs, [Atom_2|Cs], Atoms),
   fn_atoms_aggree_on_key(Database, Atom_1, Atom_2, Atom_3),
   append([As, [Atom_3], Bs, Cs], Atoms_2).


/* ddql_fn_atoms_connect(Database, Atoms_1, Atoms_2) <-
      */

ddql_fn_atoms_connect(Database, Atoms, Atoms) :-
   append(_, [A1,A2|_], Atoms),
   A1 = Table:Tuple_1,
   A2 = Table:Tuple_2,
   ddbase_table_to_primary_key(Database:Table, Key),
   fn_tuple_project(Key, Tuple_1, T),
   fn_tuple_project(Key, Tuple_2, T).
ddql_fn_atoms_connect(Database, Atoms_1, Atoms_2) :-
   append(As, [A1,A2|Bs], Atoms_1),
   A1 = Table_1:Tuple_1,
   A2 = Table_2:Tuple_2,
%  Table_1 \= Table_2,
   mysql_connector(Table_1, Table_2, _),
   !,
   mysql_connector(Table_1, Table_2, Predicate),
   ddbase_table_to_primary_key(Database:Table_1, Key_1),
   ddbase_table_to_primary_key(Database:Table_2, Key_2),
   fn_tuple_project(Key_1, Tuple_1, T_1),
   fn_tuple_project(Key_2, Tuple_2, T_2),
   pair_lists(:, _, Values_1, T_1),
   pair_lists(:, _, Values_2, T_2),
   append(Values_1, Values_2, Values),
   A =.. [Predicate|Values],
   clause(A, C),
   append(As, [A1,C,A2|Bs], Atoms_2).
ddql_fn_atoms_connect(_, Atoms, Atoms).


/*** implementation ***********************************************/


/* fn_atoms_aggree_on_key(Database, Atom_1, Atom_2, Atom_3) <-
      */

fn_atoms_aggree_on_key(Database, Atom_1, Atom_2, Atom_3) :-
   Atom_1 = Table:Tuple_1,
   Atom_2 = Table:Tuple_2,
   ddbase_table_to_primary_key(Database:Table, Key),
   fn_tuple_project(Key, Tuple_1, Values_1),
   fn_tuple_project(Key, Tuple_2, Values_2),
   Values_1 = Values_2,
   fn_tuples_unify(Tuple_1, Tuple_2, Tuple_3),
   Atom_3 = Table:Tuple_3.


/* fn_tuple_project(Attributes, Tuple_1, Tuple_2) <-
      */

fn_tuple_project(As, [A:V|Tuple_1], [A:V|Tuple_2]) :-
   member(A, As),
   !,
   fn_tuple_project(As, Tuple_1, Tuple_2).
fn_tuple_project(As, [_|Tuple_1], Tuple_2) :-
   fn_tuple_project(As, Tuple_1, Tuple_2).
fn_tuple_project(_, [], []).


/* fn_tuples_unify(Tuple_1, Tuple_2, Tuple_3) <-
      */

fn_tuples_unify(Tuple_1, Tuple_2, Tuple_3) :-
   pair_lists(:, Attributes_1, _, Tuple_1),
   pair_lists(:, Attributes_2, _, Tuple_2),
   append(Attributes_1, Attributes_2, Attributes),
   sort(Attributes, Attributes_3),
   fn_tuples_unify(Attributes_3, Tuple_1, Tuple_2, Tuple_3).


/* fn_tuples_unify(Attributes, Tuple_1, Tuple_2, Tuple_3) <-
      */

fn_tuples_unify([A|As], Tuple_1, Tuple_2, [A:V|Tuple]) :-
   fn_tuples_to_unified_value(A, Tuple_1, Tuple_2, A:V),
   fn_tuples_unify(As, Tuple_1, Tuple_2, Tuple).
fn_tuples_unify([], _, _, []).

fn_tuples_to_unified_value(A, Tuple_1, Tuple_2, A:V) :-
   member(A:V, Tuple_1),
   member(A:V, Tuple_2).
fn_tuples_to_unified_value(A, Tuple_1, Tuple_2, A:V) :-
   member(A:V, Tuple_1),
   \+ member(A:_, Tuple_2).
fn_tuples_to_unified_value(A, Tuple_1, Tuple_2, A:V) :-
   member(A:V, Tuple_2),
   \+ member(A:_, Tuple_1).


/* fn_tuples_union(Tuple_1, Tuple_2, Tuple_3) <-
      */

fn_tuples_union(Tuple_1, Tuple_2, Tuple_3) :-
   append(Tuple_1, Tuple_2, Tuple),
   sort(Tuple, Tuple_3).


/******************************************************************/


