

/******************************************************************/
/***                                                            ***/
/***          DDBase:  Integrity Constraints                    ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      ddbase_inconsistent/2.

:- multifile
      ddbase_inconsistent/2.


/*** interface ****************************************************/


/* ddbase_inconsistent(Module) <-
      */

ddbase_inconsistent(Module) :-
   ddbase_inconsistent_multiple(Module, Violations),
   dwrite(xml, Violations).


/* ddbase_inconsistent_multiple(Module, Xml) <-
      */

ddbase_inconsistent_multiple(Module, Xml) :-
   findall( Violation,
      ddbase_inconsistent(Module, Violation),
      Violations_2 ),
   sort(Violations_2, Violations),
   Xml = violations:[module:Module]:Violations.


/* ddbase_inconsistent(Module, Violation) <-
      */

ddbase_inconsistent(Module, Violation) :-
   Module:dynamic(ddbase_schema/1),
   Module:ddbase_schema(Schema),
   Table := Schema@name,
   Database := Schema@database,
   Attributes := Schema/attributes/content::'*',
   Key := Schema/primary_key/content::'*',
   ddbase_tuples_with_the_same_key(
      Module, Table, Attributes, Key, Vs_1, Vs_2),
   pair_lists(:, Attributes, Vs_1, As_1),
   pair_lists(:, Attributes, Vs_2, As_2),
   sort([tuple:As_1:[], tuple:As_2:[]], Tuples),
   Violation = primary_key:[
      database:Database, table:Table ]:Tuples.

ddbase_inconsistent(Module, Violation) :-
   ddbase_schema_and_free_atom(Schema, Database,
      Module:Table:Attributes, Vs, Atom),
   Module_2 = Module,
   Foreign_Keys := Schema/foreign_keys/content::'*',
   ddbase_schema_to_foreign_key(Foreign_Keys, Database,
      A->Database_2:Table_2:A_2),
   nth_flexible(M, Attributes, A),
   nth_flexible(M, Vs, V),
   ddbase_schema_and_free_atom(_, Database_2,
      Module_2:Table_2:Attributes_2, Vs_2, Atom_2),
   nth_flexible(M_2, Attributes_2, A_2),
   nth_flexible(M_2, Vs_2, V),
   findall( row:Pairs:[],
      ( call(Atom),
        \+ call(Atom_2),
        ddbase_inconsistent_no_value_is_null(V),
        ddbase_combine_attributes_and_values(A, V, Pairs) ),
      Values ),
   Values \= [],
   ddbase_attributes_to_fn_items(A, As),
   ddbase_attributes_to_fn_items(A_2, As_2),
   Ref = references:[database:Database_2, table:Table_2]:As_2,
   append(As, [Ref|Values], Es),
   Violation = foreign_key:[database:Database, table:Table]:Es.

ddbase_inconsistent(Module, Violation) :-
   ddbase_schema_and_free_atom(Schema, Database,
      Module:Table:Attributes, Vs, Atom),
   Key := Schema/primary_key/content::'*',
   ( Not_Null := Schema/not_null/content::'*'
   ; \+ (_ := Schema/not_null/content::'*'),
     Not_Null = [] ),
   append(Key, Not_Null, Attributes_2),
   sort(Attributes_2, Attributes_3),
   nth_multiple(Numbers, Attributes, Attributes_3),
   nth_multiple(Numbers, Vs, Vs_2),
   findall( row:Pairs:[],
      ( call(Atom),
        member('$null$', Vs_2),
        pair_lists(:, Attributes_3, Vs_2, Pairs) ),
      Tuples ),
   Tuples \= [],
   Violation = not_null:[
      database:Database, table:Table ]:Tuples.


/*** implementation ***********************************************/


ddbase_inconsistent_no_value_is_null(V) :-
   is_list(V),
   \+ member('$null$', V).
ddbase_inconsistent_no_value_is_null(V) :-
   \+ is_list(V),
   V \= '$null$'.

ddbase_combine_attributes_and_values(A, V, Pairs) :-
   is_list(A),
   pair_lists(:, A, V, Pairs).
ddbase_combine_attributes_and_values(A, V, Pairs) :-
   \+ is_list(A),
   Pairs = [A:V].
        
ddbase_attributes_to_fn_items(A, As) :-
   is_list(A),
   findall( attribute:[name:Name]:[],
      member(Name, A),
      As ).
ddbase_attributes_to_fn_items(A, As) :-
   \+ is_list(A),
   As = [attribute:[name:A]:[]].


/* ddbase_tuples_with_the_same_key(
         Module, Table, Attributes, Key, Vs_1, Vs_2) <-
      */

ddbase_tuples_with_the_same_key(
      Module, Table, Attributes, Key, Vs_1, Vs_2) :-
   length(Attributes, N),
   predicate_to_free_atom(Module:Table/N, Vs_1, Atom_1),
   predicate_to_free_atom(Module:Table/N, Vs_2, Atom_2),
   call(Atom_1),
   call(Atom_2),
   Atom_1 \= Atom_2,
   nth_multiple(Numbers, Attributes, Key),
   checklist( nth_elements_are_equal(Vs_1, Vs_2),
      Numbers ).

nth_elements_are_equal(Xs, Ys, N) :-
   nth(N, Xs, X),
   nth(N, Ys, X).

predicate_to_free_atom(M:P/A, Vs, M:Atom) :-
   n_free_variables(A, Vs),
   Atom =.. [P|Vs],
   !.


/* ddbase_schema_to_foreign_key(Foreign_Keys, Database,
         A->Database_2:Table_2:A_2) <-
      */

ddbase_schema_to_foreign_key(Foreign_Keys, Database,
      A->Database_2:Table_2:A_2) :-
   ( member(A->Table_2:A_2, Foreign_Keys),
     \+ Table_2:A_2 = _:_:_,
     Database_2 = Database
   ; member(A->Database_2:Table_2:A_2, Foreign_Keys) ).


/* ddbase_schema_and_free_atom(Schema, Database,
         Module:Table:Attributes, Vs, Atom) <-
      */

ddbase_schema_and_free_atom(Schema, Database,
      Module:Table:Attributes, Vs, Atom) :-
   Module:dynamic(ddbase_schema/1),
   Module:ddbase_schema(Schema),
   Table := Schema@name,
   Database := Schema@database,
   Attributes := Schema/attributes/content::'*',
   length(Attributes, N),
   predicate_to_free_atom(Module:Table/N, Vs, Atom).


/* nth_flexible(N, Xs, X) <-
      */

nth_flexible(N, Xs, X) :-
   ( is_list(N)
   ; is_list(X) ),
   nth_multiple(N, Xs, X).
nth_flexible(N, Xs, X) :-
   \+ ( is_list(N)
   ; is_list(X) ),
   nth(N, Xs, X).

 
/*** tests ********************************************************/


test(ddbase, ddbase_inconsistent) :-
   ddbase_inconsistent(c).
test(ddbase, ddbase_fail) :-
   fail.


/******************************************************************/


