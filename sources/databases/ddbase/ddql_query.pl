

/******************************************************************/
/***                                                            ***/
/***       Declare:  DDQL - NLI, Queries                        ***/
/***                                                            ***/
/******************************************************************/


% :- dislog_variable_set(ddql_query_mode, mysql).
  :- dislog_variable_set(ddql_query_mode, ddbase).


/* ddql_test_query_key_words(N, Words) <-
      */

ddql_test_query_key_words(N, Words) :-
   Words_List = [
      [salary, of, 'Borg'],                       %  1
      [salary, of, employee],                     %  2
      [salary, of, employee, 'Borg'],             %  3
      [salary, of, employee, '111111111'],        %  4
      [sex, of, employee, 'Borg'],                %  5

      [salary, of, 'Headquarters'],               %  6   *
      [salary, of, 'Research'],                   %  7   *
      [salary, of, department],                   %  8
      [sum, of, salary, of, 'Research'],          %  9
      [sum, of, salary, of, department],          % 10   *

      [list, of, salary, of, department],         % 11   *
      [avg, of, salary, of, department],          % 12   *
      [employee,  is_supervisor_of, employee] ],  % 13
   nth(N, Words_List, Words).


/*** tests ********************************************************/


test(mysql_interface_ddql, ddql_query_google) :-
   ddql_query('list of salary of department').

test(mysql_interface_ddql, ddql_queries) :-
   ddbase_aggregate( [N, length(1)],
      test(mysql_interface_ddql, ddql_query(N)),
      Tuples ),
   writeln(user, Tuples),
   ddbase_aggregate( [average(L)],
      member([_, L], Tuples), Result ),
   writeln(user, Result).

test(mysql_interface_ddql, ddql_query(N)) :-
   ddql_test(N, Tuples),
   ( Tuples = [] -> writeln(user, 'empty table')
   ; xpce_display_table([], Tuples) ).

test(mysql_interface_ddql, ddql_query) :-
   ddbase_aggregate( [N, list(M)],
      ( ddql_test(N, Tuples),
        length(Tuples, M) ),
      Pairs ),
   xpce_display_table([], Pairs).

test(mysql_interface_ddql, ddql_query) :-
   Database = company, Connection = mysql, Module = Database,
   ddbase_database_ensure_loaded(Connection, Database, Module),
   mozilla_deep_speech_read_words(Words),
   star_line, writeln(Words), star_line,
   ddql_words_to_tuples(Module, Database, Words, Tuples),
   xpce_display_table([], Tuples).

test(mysql_interface_ddql, mozilla_deep_speech_read_words) :-
   mozilla_deep_speech_read_words(Words),
   star_line, writeln(Words), star_line.

test(mysql_interface_ddql, ddql_query_selli(N)) :-
   Database = selli, Connection = mysql, Module = Database,
   ddbase_database_ensure_loaded(Connection, Database, Module),
   Words_List = [
      ['Price', of, purchase, 'Number'],
      ['Number', of, purchase, 'Price'] ],
   nth(N, Words_List, Words),
   star_line, writeln(Words), star_line,
   ddql_words_to_tuples(Module, Database, Words, Tuples),
   ( Tuples = [] -> writeln(user, 'empty table')
   ; xpce_display_table([], Tuples) ).

test(mysql_interface_ddql, ddql_query_evu(N)) :-
   Database = evu, Connection = mysql, Module = Database,
   ddbase_database_ensure_loaded(Connection, Database, Module),
   Words_List = [
      [sem_id, of, semester] ],
   nth(N, Words_List, Words),
   star_line, writeln(Words), star_line,
   ddql_words_to_tuples(Module, Database, Words, Tuples),
   ( Tuples = [] -> writeln(user, 'empty table')
   ; xpce_display_table([], Tuples) ).


/*** interface ****************************************************/


/* ddql_test_query_evaluation <-
      */

ddql_test_query_evaluation :-
   findall( [N, M],
      ( ddql_test_query_key_words(N, Words),
        ddql_words_to_results(Words, Results),
        member((Goal, _), Results),
        call(Goal),
        Goal =.. List,
        last(List, Tuples),
        length(Tuples, M) ),
      Pairs ),
   xpce_display_table('DDQL', [], Pairs).


/* ddql_aggregation_words(Words) <-
      */

ddql_aggregation_words(Words) :-
   Words = [sum, avg, max, min, count, list].


/* ddql_aggregation_query(Words) <-
      */

ddql_aggregation_query(Words) :-
   ddql_aggregation_words(Aggregation_Words),
   first(Words, Word),
   member(Word, Aggregation_Words).
   

/* ddql_words_to_results(Words, Results) <-
      */

ddql_words_to_results(Words, Results) :-
   dabolish(ddql_result/1),
   Database = company, Connection = mysql, Module = Database,
   ddbase_database_ensure_loaded(Connection, Database, Module),
   findall( _,
      ddql_words_to_tuples(Module, Database, Words, _Tuples),
      _ ),
   findall( Result,
      ddql_result(Result),
      Results ),
   dabolish(ddql_result/1),
   star_line,
   writelnq_list(user, Results),
   star_line,
   ddql_results_to_html_display(Words, Results).


/* ddql_results_to_html_display(Words, Results) <-
      */

ddql_query(Word) :-
   name_split_at_position([" "], Word, Words),
   ddql_query_(Words).

ddql_query_(Words) :-
   ddql_words_to_results(Words, Results),
   ddql_results_to_html_display(Words, Results).

ddql_results_to_html_display(N) :-
   ddql_test_query_key_words(N, Words),
   ddql_words_to_results(Words, Results),
   ddql_results_to_html_display(Words, Results).

ddql_results_to_html_display(Words, Results) :-
   File = 'ddql_results.html',
   dislog_variable_get(home, DisLog),
   concat(['file://', DisLog, '/', File], Path),
   ( foreach(R, Results), foreach(I, Is) do
        ( R = (S, _), copy_term(S, S_),
          numbervars(S_, 0, _),
          ( S_ = call(ddbase_aggregate(T, _:G, _)) ->
            S  = call(ddbase_aggregate(_, _, Tuples)),
            H =.. [select|T], S__ = (H :- G),
            Command = '--'
          ; S_ = ddbase_select(_, _:S__, _),
            S  = ddbase_select(X, Y, Tuples),
            ddbase_select_to_statement(X, Y, Command) ),
          call(S), length(Tuples, N),
%         term_to_atom((nl, writelnq('SQL'(R)), R), R_),
          term_to_atom(R, R_),
          concat('prolog:', R_, Prolog),
          I = [font:[color:'#FF3333']:['Datalog*'],
               '&nbsp;', S__, '&nbsp;',
               a:[href:Prolog]:[N, ' tuples'], br:[],
               font:[color:'#FF3333']:[
                  'SQL', '&nbsp;', '&nbsp;', '&nbsp;'],
               '&nbsp;', Command, p:[]] ) ),
   append(Is, Items_1),
   names_append_with_separator(Words, ', ', Text),
   Items_2 = [
      hr:[],
      h1:[font:[color:'#3333FF']:['DDQL Query']],
      hr:[],
      h2:['key words:'], Text,
      h2:['results:'] ],
   Items_3 = [br:[], p:[], hr:[]],
   append([Items_2, Items_1, Items_3], Items),
   Html = html:Items,
   dwrite(html, File, Html),
   Title = 'DDQL',
   current_prolog_flag(version, Version_Prolog),
   ( Version_Prolog < 60000 -> Size = size(850, 400)
   ; gethostname(daniel)    -> Size = size(900, 420)
   ;                           Size = size(900, 540) ),
   file_to_cms_doc_window_no_prune(
      Title, Path, Size, desktop_link_handler).


/* ddql_test(N, Tuples) <-
      */

ddql_test(N, Tuples) :-
   Database = company, Connection = mysql, Module = Database,
   ddbase_database_ensure_loaded(Connection, Database, Module),
   ddql_test_query_key_words(N, Words),
   star_line, writeln(Words), star_line,
   ddql_words_to_tuples(Module, Database, Words, Tuples).


/* ddql_words_to_tuples(Module, Database, Words, Tuples) <-
      */

ddql_words_to_tuples(Module, Database, Words, Tuples) :-
   ( ddql_aggregation_query(Words) ->
     dislog_variable_set(ddql_query_mode, ddbase)
   ; dislog_variable_set(ddql_query_mode, mysql) ),
   dislog_variable_set(ddql_database, Database),
   question(Xml, Words, []),
   dwrite(xml, Xml),
   Xml = [question:Locations],
   ddql_locations_to_fn_atoms(Locations, Atoms_A),
   copy_term(Atoms_A, Atoms_1),
   numbervars(Atoms_1, 0, _),
%  writeln_list(user, ['before connect: ', Atoms_1]),
   star_line,
   ddql_fn_atoms_connect(Database, Atoms_A, Atoms_B),
   copy_term(Atoms_B, Atoms_2),
   numbervars(Atoms_2, 0, _),
%  writeln_list(user, ['after connect: ', Atoms_2]),
%  ddql_fn_atoms_reduce(Database, Atoms_B, Atoms),
   ddql_words_to_tuples_show_rule(Atoms_2-Atoms_1),
%  star_line,
   Atoms = Atoms_B,
   ddql_align_atoms(Atoms),
   ddql_atoms_to_tuples_(Module, Database, Atoms, Tuples).

ddql_atoms_to_tuples_(Module, Database, Atoms, Tuples) :-
   ddql_atoms_to_tuples(Module, Database, Atoms, Tuples),
   !.
ddql_atoms_to_tuples_(Module, Database, Atoms, Tuples) :-
   ddql_atoms_correct(Atoms, Atoms_2),   
   writelnq(user,
      ddql_atoms_to_tuples_(Module, Database, Atoms_2, Tuples)),
   ddql_atoms_to_tuples(Module, Database, Atoms_2, Tuples),
   !.


/* ddql_atoms_correct(Atoms_1, Atoms_2) <-
      */

ddql_atoms_correct(Atoms_1, Atoms_2) :-
   maplist( ddql_atom_correct, Atoms_1, Atoms_2).

ddql_atom_correct(Table:Pairs_1, Table:Pairs_2) :-
   maplist( ddql_pair_correct, Pairs_1, Pairs_2 ),
   wait,
   star_line,
   writelnq(user, ddql_atom_correct(Pairs_1->Pairs_2)),
   star_line,
   wait.


/* ddql_pair_correct(A:V, B:W) <-
      */

ddql_pair_correct(A:V, A:W) :-
   nonvar(V),
   V = {W},
   !.
ddql_pair_correct(A:V, A:V) :-
   V == {_},
   !.
ddql_pair_correct(A:V, A:V) :-
   nonvar(V),
   V = {_},
   !.
ddql_pair_correct(A:V, A:{V}) :-
   nonvar(V),
   !.
ddql_pair_correct(A:V, A:V).


/* ddql_words_to_tuples_show_rule(Rule) <-
      */

ddql_words_to_tuples_show_rule(Rule) :-
   writeln(user, 'ddql_connect'),
   nl(user),
   copy_term(Rule, Rule_2),
   numbervars(Rule_2, 0, _),
   Rule_2 = A-B,
   write_list_with_separators(writelnq, writeq, A),
   writeln(' <- '),
   write_list_with_separators(writelnq, writelnq, B).


/* ddql_atoms_to_tuples(Module, Database, Atoms, Tuples) <-
      */

ddql_atoms_to_tuples(Module, Database, Atoms, Tuples) :-
   Atoms = [ddbase_aggregate:Pairs|Atoms_2],
   !,
   ddql_aggregation_words(Words),
   member(Agg, Words),
   members([attribute:V, Agg:W], Pairs),
   ddbase_fn_atoms_to_datalog(
      odbc(mysql), Database, Atoms_2, Atoms_3 ),
   list_to_comma_structure(Atoms_3, Body),
   ( Agg = sum  -> P = sum_atoms
   ; Agg = avg  -> P = avg_atoms
   ; Agg = list -> P = list_for_ddql
   ; P = Agg ),
   A =.. [P, W],
   Goal = ddbase_aggregate( [V, A], Module:Body, Tuples ),
   star_line,
   writeln(user, ddbase_aggregate),
   star_line,
   dwrite(lp, user, [[goal], [Goal]]), star_line,
   assert( ddql_result(
      ( call(Goal),
        xpce_display_table('DDQL', [], Tuples) ) ) ),
   call(Goal).
ddql_atoms_to_tuples(Module, Database, Atoms, Tuples) :-
   term_to_variables(Atoms, Variables),
   Head =.. [select|Variables],
   list_to_comma_structure(Atoms, Body),
   Rule_fn = (Head :- Body),
%  star_line,
%  writeln(user, ddbase_select),
   assert( ddql_result(
      ( ddbase_select(Module, Database:Rule_fn, Tuples),
        xpce_display_table('DDQL', [], Tuples) ) ) ),
   ddbase_select(Module, Database:Rule_fn, Tuples),
   !.

list_for_ddql(Xs, X) :-
   maplist( term_to_atom, Ys, Xs ),
   names_append_with_separator(Ys, ', ', X).
%  list_to_comma_atom(Ys, X).

avg_atoms(Atoms, Average) :-
   maplist( atom_number, Atoms, Numbers ),
   avg(Numbers, Average).


/* mozilla_deep_speech_read_words(Words) <-
      */

mozilla_deep_speech_read_words(Words) :-
   dislog_variable_get(
      output_path, 'deep_speech_result.txt', Path),
   concat(['cd ~/soft/Mozilla_Deep_Speech/ds; ',
      'python3 ds-transcriber.py > ', Path, '; cd ~'],
      Command),
   catch( us(Command), _, true ),
   read_file_to_lines(Path, Lines),
   last(Lines, X), nl,
   term_to_atom(Words, X).


/******************************************************************/


