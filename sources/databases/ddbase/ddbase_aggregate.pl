

/******************************************************************/
/***                                                            ***/
/***          DDBase:  Aggregation                              ***/
/***                                                            ***/
/******************************************************************/


:- module_transparent
      ddbase_aggregate/3.


/*** tests ********************************************************/


test(ddbase_aggregate, ddbase_aggregate) :-
   ddbase_aggregate(
      [SSN, list_to_comma_atom((PNO, Hours)), sum(Hours)],
      ddbase_call(odbc, company:works_on(SSN, PNO, Hours)),
      Tuples_1 ),
   xpce_display_table(['SSN', 'Workload', 'Hours'], Tuples_1).
test(ddbase_aggregate, ddbase_aggregation) :-
   ddbase_aggregation(
      [SSN, list_to_comma_atom((PNO, Hours)), sum(Hours)],
      ddbase_call(odbc, company:works_on(SSN, PNO, Hours)),
      Tuple ),
   writeq(Tuple).
test(ddbase_aggregate, ddbase_aggregate(salary)) :-
   Connection = mysql, Database = company, Module = Database,
   ddbase_database_ensure_loaded(Connection, Database, Module),
   functor(A, employee, 10),
   ddbase_aggregate( [DNO, sum(Sal)],
      ( Module:A, A =.. Xs,
        nth(11, Xs, DNO), nth(9, Xs, S),
        atom_to_number(S, Sal) ),
      Tuples),
   xpce_display_table(['DNO', 'sum(Salary)'], Tuples).
test(ddbase_aggregate, teaching) :-
   Module = company,
   ddbase_load('examples/teaching/prolog/company.pl', Module),
   Template = [Pname, PNO, sum(Hs), length(Hs), average(Hs)],
   ddbase_aggregate( Template,
      ( Module:project(Pname, PNO, _, _),
        Module:works_on(_, PNO, Hours),
        atom_number_save(Hours, Hs) ),
      Rows ),
   Attributes = ['Pname', 'PNO', 'Hours', '#', 'Avg'],
   xpce_display_table(Attributes, Rows).

atom_number_save(Atom, Number) :-
   ( number(Atom) -> Number = Atom
   ; atom_number(Atom, Number) ).


/*** interface ****************************************************/


/* ddbase_count(X, Goal, N) <-
      */

ddbase_count(X, Goal, N) :-
   findall(X, Goal, Xs),
   length(Xs, N).
ddbase_count(N=(count(X:Goal))) :-
   ddbase_count(X, Goal, N).
   
   
/* aggregate(Template, Goal, Tuples) <-
      */

aggregate(Template, Goal, Tuples) :-
   ddbase_aggregate_template_to_template(Template, Template_2),
   bagof( Template_2,
      Goal,
      Table ),
   ddbase_aggregate_template_to_pattern(Template, Pattern),
   table_aggregate_to_table(Pattern, Table, Tuples).


/* ddbase_aggregate(Assignments, Goal) <-
      */

ddbase_aggregate(Assignments, Goal) :-
   pair_lists(=, Target, Template, Assignments),
   ddbase_aggregate(Template, Goal, Tuples),
   member(Target, Tuples).


/* ddbase_aggregation(Template, Goal, Tuple) <-
      */

ddbase_aggregation(Template, Goal, Tuple) :-
   ddbase_aggregate(Template, Goal, Tuples),
   member(Tuple, Tuples).


/* ddbase_aggregate_to_xpce(Template, Goal) <-
      */

ddbase_aggregate_to_xpce(Template, Goal) :-
   ddbase_aggregate(Template, Goal, Tuples),
   xpce_display_table([], Tuples).


/* ddbase_aggregate(Template, Goal, Tuples) <-
      */

ddbase_aggregate(Template, Goal, Tuples) :-
   ddbase_aggregate_template_to_template(Template, Template_2),
   findall( Template_2,
      Goal,
      Table ),
   ddbase_aggregate_template_to_pattern(Template, Pattern),
   table_aggregate_to_table(Pattern, Table, Tuples).


/*** implementation ***********************************************/


/* ddbase_aggregate_template_to_template(
         Template_1, Template_2) <-
      */ 

ddbase_aggregate_template_to_template([X|Xs], [X|Ys]) :-
   ( atomic(X)
   ; var(X) ),
   !,
   ddbase_aggregate_template_to_template(Xs, Ys).
ddbase_aggregate_template_to_template([X|Xs], [Y|Ys]) :-
   X =.. [_, Y],
   !,
   ddbase_aggregate_template_to_template(Xs, Ys).
ddbase_aggregate_template_to_template([], []).


/* ddbase_aggregate_template_to_pattern(Template, Pattern) <-
      */

ddbase_aggregate_template_to_pattern([X|Xs], [x|Ys]) :-
   ( atomic(X)
   ; var(X) ),
   !,
   ddbase_aggregate_template_to_pattern(Xs, Ys).
ddbase_aggregate_template_to_pattern([X|Xs], [Y|Ys]) :-
   X =.. [Y, _],
   !,
   ddbase_aggregate_template_to_pattern(Xs, Ys).
ddbase_aggregate_template_to_pattern([], []).


/* table_aggregate_to_table(Pattern, Table_1, Table_2) <-
      */

table_aggregate_to_table(Pattern, Table_1, Table_2) :-
   table_group_by_pattern(Pattern, Table_1, Tables),
   maplist( table_aggregate_to_tuple(Pattern),
      Tables, Table_2 ).


/* table_aggregate_to_tuple(Pattern, Table, Tuple) :-
      */

table_aggregate_to_tuple(Pattern, Table, Tuple) :-
   table_aggregate_to_tuple(1, Pattern, Table, Tuple).

table_aggregate_to_tuple(N, [x|Ps], Table, [X|Xs]) :-
   !,
   first(Table, T),
   nth(N, T, X),
   M is N + 1,
   table_aggregate_to_tuple(M, Ps, Table, Xs).
table_aggregate_to_tuple(N, [P|Ps], Table, [X|Xs]) :-
   ( foreach(T, Table), foreach(Y, Ys) do
        nth(N, T, Y) ),
   apply(P, [Ys, X]), 
   M is N + 1,
   table_aggregate_to_tuple(M, Ps, Table, Xs).
table_aggregate_to_tuple(_, [], _, []).


/* table_group_by_pattern(Pattern, Table, Tables) <-
      */

table_group_by_pattern(Pattern, Table, Tables) :-
   pattern_to_positions(1, Pattern, Positions),
   table_sort_by_positions(Positions, Table, Table_2),
   table_group_by_pattern(Pattern, Table_2, [], [], Tables_2),
   reverse(Tables_2, Tables).

table_group_by_pattern(Pattern,
      [T1,T2|Ts], Table, Tables_1, Tables_2) :-
   tuples_are_equal_wrt_pattern(Pattern, T1, T2),
   !,
   table_group_by_pattern(Pattern,
      [T2|Ts], [T1|Table], Tables_1, Tables_2).
table_group_by_pattern(Pattern,
      [T|Ts], Table, Tables_1, Tables_2) :-
   reverse([T|Table], Table_1),
   table_group_by_pattern(Pattern,
      Ts, [], [Table_1|Tables_1], Tables_2).
table_group_by_pattern(_, [], [], Tables_1, Tables_1) :-
   !.
table_group_by_pattern(_,
      [], Table, Tables_1, [Table|Tables_1]).


/* pattern_to_positions(N, Pattern, Positions) <-
      */

pattern_to_positions(N, [x|As], [N|Ns]) :-
   !,
   M is N + 1,
   pattern_to_positions(M, As, Ns).
pattern_to_positions(N, [_|As], Ns) :-
   M is N + 1,
   pattern_to_positions(M, As, Ns).
pattern_to_positions(_, [], []).


/* tuples_are_equal_wrt_pattern(Pattern, Xs1, Xs2) <-
      */

tuples_are_equal_wrt_pattern([x|Ps], [X1|Xs1], [X2|Xs2]) :-
   !,
   X1 = X2,
   tuples_are_equal_wrt_pattern(Ps, Xs1, Xs2).
tuples_are_equal_wrt_pattern([_|Ps], [_|Xs1], [_|Xs2]) :-
   tuples_are_equal_wrt_pattern(Ps, Xs1, Xs2).
tuples_are_equal_wrt_pattern([], [], []).


/* table_sort_by_positions(Positions, Table_1, Table_2) <-
      */

table_sort_by_positions(Positions, Table_1, Table_2) :-
   length(Table_1, N),
   generate_interval(1, N, I),
   pair_lists(-, I, Table_1, Pairs_1),
   maplist( pair_tag_for_sort(Positions),
      Pairs_1, Pairs_2 ),
   list_to_ord_set(Pairs_2, Pairs_3),
   maplist( triple_from_sort_untag,
      Pairs_3, Table_2 ).

pair_tag_for_sort(Positions, N-Tuple, Tag-N-Tuple) :-
   nth_multiple(Positions, Tuple, Tag).
   
triple_from_sort_untag(_-_-Tuple, Tuple).


/******************************************************************/


