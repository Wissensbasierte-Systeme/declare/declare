

/******************************************************************/
/***                                                            ***/
/***       Declare:  DDQL - NLI, Parser                         ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(mysql_interface_ddql, ddql_parser(N)) :-
   Words_List = [
      [salary, of, 'Borg'],
      [sum, of, salary, of, 'Research'],
      [avg, of, salary, of, department] ],
   nth(N, Words_List, Words),
   ddql_word_list_annotate(Words, Xml),
   dwrite(xml, Xml).


/*** interface ****************************************************/


/* ddql_word_list_annotate(Words, Xml) <-
      */

ddql_word_list_annotate(Words, Xml) :-
   question(Xml, Words, []).

question ==> aggregation, of, sql_attribute, of, table.
question ==> aggregation, of, sql_attribute.
question ==> sql_attribute, of, table.
question ==> table, is_supervisor_of, table.

sql_attribute(Xml) -->
   [Attribute],
   { ( characters_lower_to_capital(Attribute, B) -> A = B
     ; A = Attribute ),
     dislog_variable_get(ddql_database, Database),
     ddql_entity_annotate(mysql, Database, A, Location),
     Xml = [Location] }.
aggregation([Agg]) -->
   [Agg],
   { member(Agg, [sum, avg, max, min, count, list]) }.
of([of]) --> ['of'].
is_supervisor_of([]) --> ['is_supervisor_of'].
table(Xml) -->
   { dislog_variable_get(ddql_database, Database) },
   ( [X],
     { \+ ddql_entity_annotate(mysql, Database, X, _),
       Xml = [X] }
   ; [Table, X],
     { ddql_entity_annotate(mysql, Database, Table, T),
       ddql_entity_annotate(mysql, Database, X, Location),
       Xml = [T, Location] }
   ; [X],
     { ddql_entity_annotate(mysql, Database, X, Location),
       Xml = [Location] } ).


/*** implementation ***********************************************/


/* ddql_locations_to_fn_atoms(Locations, Atoms) <-
      */

ddql_locations_to_fn_atoms([Agg, of|Locations], [Atom|Atoms]) :-
   first(Locations, (A_1=_)),
   ddql_locations_to_fn_atoms(Locations, Atoms),
   first(Atoms, _:Pairs_1),
   last(Atoms, Table_2:Pairs_2),
   mysql_table_to_primary_key(company:Table_2, [A_2]),
   member(A_1:V_1, Pairs_1),
   member(A_2:V_2, Pairs_2),
   Atom = ddbase_aggregate:[attribute:V_2, Agg:V_1].

ddql_locations_to_fn_atoms([avg, of|Locations], [Atom|Atoms]) :-
   first(Locations, (A_1=_)),
   ddql_locations_to_fn_atoms(Locations, Atoms),
   first(Atoms, _:Pairs_1),
   last(Atoms, Table_2:Pairs_2),
   mysql_table_to_primary_key(company:Table_2, [A_2]),
   member(A_1:V_1, Pairs_1),
   member(A_2:V_2, Pairs_2),
   Atom = ddbase_aggregate:[attribute:V_2, avg:V_1].

ddql_locations_to_fn_atoms(Locations, Atoms) :-
   \+ ( Locations = [Agg|_],
        member(Agg, [sum, avg, max, min, count, list]) ),
   findall( Atom,
      ( member(Location, Locations),
        ddql_location_to_fn_atom(Location, Atom) ),
      Atoms ).

ddql_location_to_fn_atom(Location, Atom) :-
   Location = (V=Database/Table/row(@Attribute)),
   mysql_table_to_primary_key(Database:Table, [A]),
   n_free_variables(2, [K, V]),
   ( dislog_variable_get(ddql_query_mode, mysql) ->
     Atom = Table:[A:K, Attribute:{V}]
   ; Atom = Table:[A:K, Attribute:V] ).
ddql_location_to_fn_atom(Location, Atom) :-
   Location = (Attribute=Database/Table/attribute),
   mysql_table_to_primary_key(Database:Table, [A]),
   n_free_variables(2, [K, V]),
   Atom = Table:[A:K, Attribute:V].
ddql_location_to_fn_atom(Location, Atom) :-
   Location = (Table=Database/Table@name),
   mysql_table_to_primary_key(Database:Table, [A]),
   n_free_variables(1, [K]),
   Atom = Table:[A:K].


/* ddql_align_atoms(Atoms) <-
      */

ddql_align_atoms(Atoms) :-
   ddbase_aggregate( [Table, Key],
      ( member(Atom, Atoms),
        Atom = Table:[Key:_|_] ),
      Tuples ),
   ddql_align_atoms(Tuples, Atoms).

ddql_align_atoms(Ts, Atoms) :-
   length(Ts, N),
   n_free_variables(N, Vs),
   ( foreach(T, Ts), foreach(V, Vs), foreach(A, As) do
        ( T = [Table, Key], A = Table:[Key:V] ) ),
   ddql_terms_unify(As, Atoms).

ddql_terms_unify(As, [Atom|Atoms]) :-
   Atom = Table:[Key:V|_],
   member(Table:[Key:V], As),
   ddql_terms_unify(As, Atoms).
ddql_terms_unify(_, []).


/*
ddql_align_atoms(Atoms) :-
   ddbase_aggregate( [Table, Key, list(V)],
      ( member(Atom, Atoms),
        Atom = Table:[Key:V|_] ),
      Tuples ),
   ddql_terms_unify(Tuples).

ddql_terms_unify([Tuple|Tuples]) :-
   Tuple = [_, _, Terms],
   terms_unify(Terms),
   ddql_terms_unify(Tuples).
ddql_terms_unify([]).

ddql_terms_unify_(Tuples) :-
   ( foreach(Tuple, Tuples) do
        ( Tuple = [_, _, Terms],
          terms_unify(Terms) ) ).
*/


/******************************************************************/


