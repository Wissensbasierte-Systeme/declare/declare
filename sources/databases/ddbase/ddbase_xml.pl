

/******************************************************************/
/***                                                            ***/
/***          DDBase:  Schema                                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* mysql_table_to_primary_key(Database:Table, Key) <-
      */

mysql_table_to_primary_key(Database:Table, Key) :-
   mysql_database_schema_to_xml(Database, Schema),
   T := Schema/table,
   Table := T@name,
   findall( Attribute,
      Attribute := T/primary_key/attribute@name,
      Key ).


/* mysql_table_to_display(Database:Table) <-
      */

mysql_table_to_display(Database:Table) :-
   mysql_table_select(Database:Table, Tuples),
   mysql_database_schema_to_xml(Database, Xml),
   Schema := Xml/table::[@name=Table],
   mysql_xml_findall_attributes(Schema, Attributes),
%  term_to_atom(Database:Table, Title),
   Title = Table,
   xpce_display_table(Title, Attributes, Tuples).


/* mysql_database_schema_to_ddbase_facts(Database, File) <-
      */

mysql_database_schema_to_ddbase_facts(Database, File) :-
   dislog_variable_get(
      odbc_connection_data_dictionary, Connection),
   mysql_database_schema_to_ddbase_facts(Connection, Database, File).

mysql_database_schema_to_ddbase_facts(Connection, Database, File) :-
   mysql_database_schema_to_xml(Connection, Database, Xml),
   findall( Schema,
      ( Item := Xml/table,
        mysql_table_schema_xml_to_fact(Database, Item, Schema) ),
      Schemas ),
   predicate_to_file( File,
      ( foreach(Schema, Schemas) do
           writeq(Schema), writeln('.\n') ) ).

mysql_table_schema_xml_to_fact(Database, Item, Schema) :-
   Table := Item@name,
   mysql_xml_findall_attributes(Item, Attributes),
   findall( Pk_Attribute,
      Pk_Attribute := Item/primary_key/attribute@name,
      Pk_Attributes ),
   findall( Not_Null_Attribute,
      Not_Null_Attribute :=
         Item/attribute::[@is_nullable='NO']@name,
      Not_Null_Attributes ),
   findall( Fk,
      ( I := Item/foreign_key,
        fk_xml_to_term(I, Fk) ),
      Fks ),
   Schema = ddbase_schema( table:[name:Table, database:Database]:[
      attributes:Attributes,
      primary_key:Pk_Attributes,
      not_null:Not_Null_Attributes,
      foreign_keys:Fks ] ),
   !.


/* fk_xml_to_term(Item, As1->T:As2) <-
      */

fk_xml_to_term(Item, As1->T:As2) :-
   mysql_xml_findall_attributes(Item, As1),
   References := Item/references::[@table=T],
   mysql_xml_findall_attributes(References, As2).


/* fk_term_to_xml(As1->T:As2, Item) <-
      */

fk_term_to_xml(As1->T:As2, Item) :-
   maplist( mysql_attribute_to_xml,
      As1, Is1 ),
   maplist( mysql_attribute_to_xml,
      As2, Is2 ),
   References = references:[table:T]:Is2,
   append(Is1, [References], Is),
   Item = foreign_key:Is.

mysql_attribute_to_xml(A, Item) :-
   Item = attribute:[name:A]:[].


/* fk_term_to_string(As1->T:As2, String) <-
      */

fk_term_to_string(As1->T:As2, String) :-
   names_append_with_separator(As1, ', ', A1),
   names_append_with_separator(As2, ', ', A2),
   concat(['foreign key (', A1, ') references ',
      T, ' (', A2, ')'], String).


/* mysql_xml_findall_attributes(Xml, Attributes) <-
      */

mysql_xml_findall_attributes(Xml, Attributes) :-
   findall( Attribute,
      Attribute := Xml/attribute@name,
      Attributes ).


/******************************************************************/


