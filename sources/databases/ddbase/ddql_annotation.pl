

/******************************************************************/
/***                                                            ***/
/***       Declare:  DDQL - NLI, Annotation                     ***/
/***                                                            ***/
/******************************************************************/


% :- dislog_variable_set(ddbase_show_tables_xpce, no).
  :- dislog_variable_set(ddbase_show_tables_xpce, yes).


/*** tests ********************************************************/


test(mysql_interface_ddql, value_to_table_and_attribute) :-
   Connection = mysql, Database = company,
   Value = 'Research',
   ddbase_value_to_table_and_attribute(
      Connection, Database, Value, Table, Attribute),
   writelnq(Value = Table/Attribute).

test(mysql_interface_ddql, ddql_entity_annotate) :-
   Connection = mysql,
   ( ddql_entity_annotate(Connection, company,
        'department', Location)
   ; ddql_entity_annotate(Connection, company,
        'ESSN', Location)
   ; ddql_entity_annotate(Connection, company,
        'Borg', Location) ),
   writelnq(Location).

test(mysql_interface_ddql, ddql_value_find) :-
   Connection = mysql, Database = company, Module = Database,
   Value = '55000.00',
   ddql_value_find(Connection, Module, Database, Value).

test(mysql_interface_ddql, ddbase_fn_to_sql) :-
   Connection = mysql, Database = company, Module = Database,
   ddbase_database_ensure_loaded(Connection, Database, Module),
   Rule_fn = ( select(Dname, Ssn, Lname, Salary) :-
      employee:['SSN':Ssn, 'LNAME':Lname, 'SALARY':Salary],
      Module:works_for(Dno, Ssn),
      department:['DNAME':Dname, 'DNUMBER':Dno] ),
   ddbase_select(Module, Database:Rule_fn, Tuples),
   Attributes = ['Dname', 'Ssn', 'Lname', 'Salary'],
   ( dislog_variable_get(ddbase_show_tables_xpce, yes) ->
     xpce_display_table(Attributes, Tuples)
   ; true ).

:- style_check(-singleton).

test(mysql_interface_ddql, aggregation(1)) :-
   Connection = mysql, Database = company, Module = c,
   ddbase_database_ensure_loaded(Connection, Database, Module),
   Tuples <= ddbase_aggregate( [X1, sum_atoms(X4)],
      ( Module:employee(X5, X6, X3, X2, X7, X8, X9, X4, X10, X11),
        works_for_ddql(X20, X2),
        Module:department(X1, X20, X21, X22) ) ),
   Attributes = ['Dname', 'Sum'],
   ( dislog_variable_get(ddbase_show_tables_xpce, yes) ->
     xpce_display_table(Attributes, Tuples)
   ; true ).

:- style_check(+singleton).


mysql_connector(employee, department, employee_works_for_department).
mysql_connector(employee, employee, is_supervisor_of).
mysql_connector(employee, employee, works_for).

employee_works_for_department(E, D) :-
   employee:['SSN':E, 'DNO':D].

works_for(D, E) :-
   employee:['SSN':E, 'DNO':D].

is_supervisor_of(E1, E2) :-
   employee:['SSN':E1, 'SUPERSSN':E2].

works_for_ddql(D, E) :-
   Atom_1 = employee:['SSN':E, 'DNO':D],
   ddbase_fn_atoms_to_datalog(
      odbc(mysql), company, [Atom_1], [Atom_2]),
   call(c:Atom_2).


/*** interface ****************************************************/


/* ddql_entity_annotate(Connection, Database, Entity, Location) <-
      */

ddql_entity_annotate(Connection, Database, Value, Location) :-
   ddbase_value_to_table_and_attribute(
      Connection, Database, Value, Table, Attribute),
   Location = (Value = Database/Table/row(@Attribute)).
ddql_entity_annotate(Connection, Database, Table, Location) :-
   mysql_database_to_xml(Connection, Database, Xml),
   Table := Xml/table@name,
   Location = (Table = Database/Table@name).
ddql_entity_annotate(Connection, Database, Attribute, Location) :-
   mysql_database_to_xml(Connection, Database, Xml),
   findall( L,
      ( _ := Xml/table::[@name=Table]/row@Attribute,
        L = (Attribute = Database/Table/attribute) ),
      Ls_1 ),
   sort(Ls_1, Ls_2),
   member(Location, Ls_2).


/* ddql_value_find(Connection, Module, Database, Value) <-
      */

ddql_value_find(Connection, Module, Database, Value) :-
   ddbase_database_ensure_loaded(Connection, Database, Module),
   ddbase_value_to_table_and_attribute(
      Connection, Database, Value, Table, Attribute),
   writelnq(Value = Table/Attribute),
   ddbase_table_to_primary_key_pairs(
      Database:Table, Variables, Pairs),
   G1 = Table:[Attribute:V|Pairs],
   G2 =.. [works_for, D|Variables],
   Rule_fn = ( (D, G1) :- (G1, c:G2, V = {Value}) ),
   ddbase_select(Module, Database:Rule_fn, Tuples),
   ( dislog_variable_get(ddbase_show_tables_xpce, yes) ->
     xpce_display_table([], Tuples)
   ; true ),
   writeq(Tuples),
   !.

ddbase_table_to_primary_key_pairs(
      Database:Table, Variables, Pairs) :-
   mysql_table_to_primary_key(Database:Table, Key),
   length(Key, N),
   n_free_variables(N, Variables),
   pair_lists(':', Key, Variables, Pairs).


/******************************************************************/


