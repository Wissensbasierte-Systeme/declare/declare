

/******************************************************************/
/***                                                            ***/
/***          DDBase:  Deductive Database Mediator              ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      ddbase_call/3.


/*** interface ****************************************************/


/* ddbase_query(odbc(Connection), Database:Rule) <-
      */

ddbase_query(odbc(Connection), Database:Rule) :-
   datalog_rule_to_sql(Connection, Database:Rule, Sql),
   arg(1, Rule, Head),
   ddbase_query(odbc(mysql), Sql, Tuple),
%  Head =.. [_|Tuple].
   free_variables(Head, Vs),
   ( Vs \= [] ->
     Tuple = Vs
   ; true ).


/* ddbase_query(odbc(Connection), Sql_Command, Tuple) <-
      */

ddbase_query(odbc(Connection), Sql_Command, Tuple) :-
   mysql_select_execute_odbc(Connection, Sql_Command, Tuples),
   member(Tuple, Tuples).


/* dcall(Type, Source, Tuple) <-
      */

dcall(Type, Source, Tuple) :-
   ddbase_call(Type, Source, Tuple).


/* ddbase_call(odbc(Connection), Database:Atom) <-
      */

ddbase_call(odbc, Database:Atom) :-
   ddbase_call(odbc(mysql), Database:Atom).

ddbase_call(odbc(Connection), Database:Atom) :-
   Atom =.. [Table|Vs],
   mysql_relation_schema_to_attributes(
      Connection, Database:Table, As),
   findall( A:V,
      ( nth(N, Vs, V),
        ground(V),
        nth(N, As, A) ),
      Pairs_Where ),
%  mysql_table_select(
%     Connection, Database:Table, Pairs_Where, Tuples),
%  member(Vs, Tuples).
   ddbase_call_to_tuple(
      odbc(Connection), Database:Table, Pairs_Where, Vs).


/* ddbase_call(
         odbc(Connection), Database:Table, Pairs_Where, Pairs) <-
      */

ddbase_call(
      odbc(Connection), Database:Table, Pairs_Where, Pairs) :-
   mysql_relation_schema_to_attributes(
      Connection, Database:Table, As),
   ddbase_call_to_tuple(
      odbc(Connection), Database:Table, Pairs_Where, Vs),
   pair_lists(:, As, Vs, Pairs).


/* ddbase_call(Type, Source, Tuple) <-
      */

ddbase_call(odbc, Database:Table, Tuple) :-
   !,
   ddbase_call(odbc(mysql), Database:Table, Tuple).

ddbase_call(odbc(Connection), Database:Table, Tuple) :-
   ddbase_call_to_tuple(
      odbc(Connection), Database:Table, [], Tuple).


/* ddbase_call_to_tuple(
         odbc(Connection), Database:Table, Pairs_Where, Tuple) <-
      */

ddbase_call_to_tuple(
      odbc(Connection), Database:Table, Pairs_Where, Tuple) :-
   mysql_use_database(Connection, Database),
   mysql_pairs_where_to_where_condition(Pairs_Where, Condition),
   concat(['select * from ', Table, Condition, ';'], Statement),
   ddbase_odbc_table_to_prolog_types(Database:Table, Types),
%  writeq(user, Types),
   !,
   odbc_query(Connection, Statement, Row, Types),
   Row =.. [row|Tuple].

ddbase_call(csv, File, Tuple) :-
   !,
   ddbase_call(csv(:), File, Tuple).

ddbase_call(csv(Separator), File, Tuple) :-
   dread(csv(Separator), File, Tuples),
   !,
   member(Tuple, Tuples).


/* ddbase_odbc_table_to_prolog_types(Database:Table, Types) <-
      */

ddbase_odbc_table_to_prolog_types(Database:Table, Types) :-
   mysql_database_schema_to_xml(Database, Xml),
   findall( T,
      ( X := Xml/table::[@name=Table]/attribute@type,
        ddbase_type_odbc_to_prolog(X, T) ),
      Ts ),
   Types = [types(Ts)].

ddbase_type_odbc_to_prolog(X, float) :-
   concat(decimal, _, X),
   !.
ddbase_type_odbc_to_prolog(_, default).


/* ddbase_connect(Source, Module:Table) <-
      */

ddbase_connect(Source, Module:Table) :-
   ddbase_connect(Source, Module, Module:Table).

ddbase_connect(Source, Module, Database:Table) :-
   ddbase_connect_arity(Source, Database:Table, N),
   n_free_variables(N, Tuple),
   Goal =.. [Table|Tuple],
   assert(Module:Goal :- ddbase_call(Source, Database:Goal)),
   !.

ddbase_connect_arity(odbc(Connection), Database:Table, N) :-
   mysql_use_database(Connection, Database),
   concat([ 'select * from ', Table, ';'], Statement),
   odbc_query(Connection, Statement, Row, []),
   Row =.. [row|Xs],
   length(Xs, N).

ddbase_connect_arity(csv(File), _:_, N) :-
   dread(csv, File, Tuples),
   member(Xs, Tuples),
   length(Xs, N).


/* ddbase_import(Source, Module:Table) <-
      */

ddbase_import(odbc(Connection), Database:Table) :-
   mysql_use_database(Connection, Database),
   concat([ 'select * from ', Table, ';'], Statement),
   forall( odbc_query(Connection, Statement, Row, []),
      ( Row =.. [row|Tuple],
        Goal =.. [Table|Tuple],
        assert(Database:Goal) ) ).

ddbase_import(csv(File), Module:Table) :-
   dread(csv, File, Tuples),
   forall( member(Tuple, Tuples),
      ( Goal =.. [Table|Tuple],
        assert(Module:Goal) ) ).


/* ddbase_abolish(Source, Module:Table) <-
      */

ddbase_abolish(Source, Module:Table) :-
   ddbase_connect_arity(Source, Module:Table, N),
   abolish(Module:Table/N).


/******************************************************************/


