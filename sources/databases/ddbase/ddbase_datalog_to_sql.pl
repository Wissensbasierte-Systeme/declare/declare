

/******************************************************************/
/***                                                            ***/
/***          DDBase:  Datalog to SQL                           ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(datalog_to_sql, 1) :-
   Rule = (
      work_on_same_project(X,Y,P) :-
         works_on(X,P,_), works_on(Y,P,_), X \= Y, P < 3 ),
   datalog_rule_to_sql(mysql, company:Rule, Sql),
   dwrite(xml, user, sql:Sql),
   setof( [X2, Y2, P],
      X1^Y1^( ddbase_query(odbc(mysql), Sql, [X1, Y1, P]),
        sort([X1, Y1], [X2, Y2]) ),
      Tuples ),
   xpce_display_table([], Tuples).

test(datalog_to_sql, 2) :-
   Rule = (
      work_on_same_project(X,Y,P) :-
         works_on(X,P,_), works_on(Y,P,_), X \= Y, P < 3 ),
   findall( Rule,
      ddbase_query(odbc(mysql), company:Rule),
      Rules ),
   star_line,
   dwrite(pl, user, Rules).


/*** interface ****************************************************/


/* datalog_rule_to_sql(Connection, Database:Rule, Sql) <-
      */

datalog_rule_to_sql(Connection, Database:Rule, Sql) :-
   Rule = (Head :- Body),
   comma_structure_to_list(Body, Atoms_1),
   mysql_database_schema_to_xml(Connection, Database, Schema),
   ( foreach(Atom_1, Atoms_1), foreach(Q:Ws, Atoms_2) do
        Atom_1 =.. [Q|Ws] ),
   datalog_atoms_to_froms(Schema, Atoms_2, Atoms, Froms),
   datalog_head_to_selects(
      Schema, Head, Atoms, Froms, Selects),
   datalog_atoms_to_conditions(
      Schema, Atoms, Froms, Conditions),
   list_to_functor_structure(and, Conditions, Condition),
   Sql = [
      use:[Database],
      select:Selects,
      from:Froms,
      where:[Condition] ].


/*** implementation ***********************************************/


/* datalog_head_to_selects(
         Schema, Atom, Atoms, Froms, Selects) <-
      */

datalog_head_to_selects(Schema, Head, Atoms, Froms, Selects) :-
%  Head = [P|Vs],
   ( free_variables(Head, Vs),
     Vs \= [] ->
     true
   ; Vs = ['0'] ),
   findall( Select,
      ( member(V, Vs),
        ( var(V) ->
          database_variable_to_select(
             Schema, V, Atoms, Froms, Select)
        ; Select = V ) ),
      Selects ).

database_variable_to_select(Schema, V, Atoms, Froms, Select) :-
   member(P:Vs, Atoms),
   nth(N, Vs, W),
   V == W,
   database_schema_to_table_attribute(Schema, Froms, P^N, A),
   Select = P^A,
   !.


/* datalog_atoms_to_froms(Schema, Atoms_1, Atoms_2, Froms) <-
      */

datalog_atoms_to_froms(Schema, Atoms_1, Atoms_2, Froms) :-
   sublist( database_schema_table_of_atom_exists(Schema, []),
      Atoms_1, Atoms_3 ), 
   ( foreach(P:Vs, Atoms_3), foreach(Q:Vs, Atoms_2a),
     foreach(From, Froms) do
        ( n_ordered_members(2, [P:_,P:_], Atoms_3) ->
          concat(P, '__', P__), gensym(P__, Q),
          From = P-Q
        ; Q = P, 
          From = P ) ),
   sublist( database_schema_table_of_atom_not_exists(Schema, []),
      Atoms_1, Atoms_2b ),
   append(Atoms_2a, Atoms_2b, Atoms_2).


/* datalog_atoms_to_conditions(Schema, Atoms, Froms, Conditions) <-
      */

datalog_atoms_to_conditions(Schema, Atoms, Froms, Conditions) :-
   datalog_atoms_to_conditions_equality(
      Schema, Atoms, Froms, Cs_1),
   datalog_atoms_to_conditions_selection(
      Schema, Atoms, Froms, Cs_2),
   datalog_atoms_to_conditions_built_in(
      Schema, Atoms, Froms, Cs_3),
   append([Cs_2, Cs_1, Cs_3], Conditions).

datalog_atoms_to_conditions_equality(
      Schema, Atoms, Froms, Conditions) :-
   findall( P^A = Q^B,
      ( n_ordered_members(2, [P:Vs,Q:Ws], Atoms),
        nth(N, Vs, V), \+ ground(V),
        nth(M, Ws, W), \+ ground(W),
        V == W,
        database_schema_to_table_attribute(
           Schema, Froms, P^N, A),
        database_schema_to_table_attribute(
           Schema, Froms, Q^M, B) ),
      Conditions ).

datalog_atoms_to_conditions_selection(
      Schema, Atoms, Froms, Conditions) :-
   findall( P^A = V,
      ( member(P:Vs, Atoms),
        nth(N, Vs, V),
        ground(V),
        database_schema_to_table_attribute(
           Schema, Froms, P^N, A) ),
      Conditions ).

datalog_atoms_to_conditions_built_in(
      Schema, Atoms, Froms, Conditions) :-
   findall( Condition,
      ( member(P:Vs, Atoms),
        \+ database_schema_table_of_atom_exists(
             Schema, Froms, P:Vs),
        datalog_arguments_to_sql_arguments(
           Schema, Atoms, Froms, Vs, Ws),
        Condition =.. [P|Ws] ),
      Conditions ).

datalog_arguments_to_sql_arguments(
      Schema, Atoms, Froms, Xs, Args) :-
   findall( Arg,
      ( member(X, Xs),
        datalog_argument_to_sql_argument(
           Schema, Atoms, Froms, X, Arg) ),
      Args ).

datalog_argument_to_sql_argument(Schema, Atoms, Froms, X, Arg) :-
   ( ground(X) ->
     Arg = X
   ; member(P:Vs, Atoms),
     nth(N, Vs, V),
     V == X ->
     database_schema_to_table_attribute(
        Schema, Froms, P^N, A),
     Arg = P^A ).


/* database_schema_table_of_atom_not_exists(Schema, Froms, Atom) <-
      */

database_schema_table_of_atom_not_exists(Schema, Froms, Atom) :-
   \+ database_schema_table_of_atom_exists(Schema, Froms, Atom).
   

/* database_schema_table_of_atom_exists(Schema, Froms, Atom) <-
      */

database_schema_table_of_atom_exists(Schema, Froms, Atom) :-
   Atom = P:_,
   database_schema_table_exists(Schema, Froms, P).


/* database_schema_table_exists(Schema, Froms, P) <-
      */

database_schema_table_exists(Schema, Froms, P) :-
   ( member(Table-P, Froms)
   ; Table = P ),
   _ := Schema/table::[@name=Table].


/* database_schema_to_table_attribute(Schema, Froms, P^N, A) <-
      */

database_schema_to_table_attribute(Schema, P^N, A) :-
   database_schema_to_table_attribute(Schema, [], P^N, A).

database_schema_to_table_attribute(Schema, Froms, P^N, A) :-
   ( member(Table-P, Froms)
   ; Table = P ),
   Xml := Schema/table::[@name=Table],
   findall( Attribute,
      Attribute := Xml/attribute@name,
      Attributes ),
   nth(N, Attributes, A).


/******************************************************************/


