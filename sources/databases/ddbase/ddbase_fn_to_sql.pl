

/******************************************************************/
/***                                                            ***/
/***          DDBase:  FN to SQL                                ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(ddbase_fn_to_sql, ddbase_fn_atoms_to_datalog) :-
   Fn_Atoms = [
      works_on:['ESSN':X, 'PNO':P], 
      works_on:['ESSN':Y, 'PNO':P],
      X \= Y, P < 3 ],
   ddbase_fn_atoms_to_datalog(
      odbc(mysql), company, Fn_Atoms, Atoms),
   list_to_comma_structure(Atoms, Body),
   dwrite(pl, user, [query :- Body]).

test(ddbase_fn_to_sql, ddbase_fn_atoms_to_datalog_mixed) :-
   Fn_Atoms = [
      works_on(X, P, _),
      works_on:['ESSN':Y, 'PNO':P],
      X \= Y, P < 3 ],
   ddbase_fn_atoms_to_datalog(
      odbc(mysql), company, Fn_Atoms, Atoms),
   list_to_comma_structure(Atoms, Body),
   dwrite(pl, user, [query :- Body]).

test(ddbase_fn_to_sql, ddbase_fn_rule_to_datalog) :-
   Rule_fn = (
      work_on_same_project:[ssn_1:X, ssn_2:Y, pno:P] :-
         works_on:['ESSN':X, 'PNO':P],
         works_on:['ESSN':Y, 'PNO':P],
         X \= Y, P < 3 ),
   ddbase_fn_rule_to_datalog(odbc(mysql), company:Rule_fn, _:Rule_dl),
   dwrite(pl, user, [Rule_dl]).

test(ddbase_fn_to_sql, ddbase_query_fn(1)) :-
   Rule_fn = (
      work_on_same_project:[ssn_1:X, ssn_2:Y, pno:P] :-
         works_on:['ESSN':X, 'PNO':P],
         works_on:['ESSN':Y, 'PNO':P],
         X \= Y, P < 3 ),
   findall( Rule_fn,
      ddbase_query_fn(odbc(mysql), company:Rule_fn),
      Rules ),
   star_line,
   dwrite(pl, user, Rules),
   findall( work_on_same_project:[ssn_1:X, ssn_2:Y, pno:P]:[],
      ddbase_query_fn(odbc(mysql), company:Rule_fn),
      Es ),
   dwrite(xml, table:Es).

test(ddbase_fn_to_sql, ddbase_query_fn(2)) :-
   T = [X, Y, P],
   Rule = ( T :-
      works_on:['ESSN':X, 'PNO':P],
      works_on(Y, P, _), X \= Y, P < 3 ),
   findall( T,
      ddbase_query_fn(odbc(mysql), company:Rule),
      Tuples ),
   writeln_list(Tuples).

test(ddbase_fn_to_sql, ddbase_query_fn_stock) :-
   Database = stock, Connection = mysql, Module = Database,
   ddbase_database_ensure_loaded(Connection, Database, Module),
   Z = {'Oce'},
   Rule_fn = (
      result:[a:Y, b:Y] :-
         number_name:[wkn:Y, name:Z] ),
   findall( Rule_fn,
      ddbase_query_fn(odbc(mysql), Database:Rule_fn),
      Rules ),
   star_line,
   dwrite(pl, user, Rules).


/*** interface ****************************************************/


/* ddbase_query_fn(odbc(Connection), Database:Rule_fn) <-
      */

ddbase_query_fn(odbc(Connection), Database:Rule_fn) :-
   ddbase_fn_rule_to_datalog(
      odbc(Connection), Database:Rule_fn, Database:Rule_dl),
   ddbase_query(odbc(Connection), Database:Rule_dl).


/* ddbase_select(Module, Database:Rule_fn, Tuples) <-
      */

ddbase_select(Module, Database:Rule_fn, Tuples) :-
   ddbase_fn_rule_to_tuples(Module, Database:Rule_fn, Tuples).


/* ddbase_fn_rule_to_tuples(
         Connection, Module, Database:Rule_fn, Tuples) <-
      */

ddbase_fn_rule_to_tuples(Module, Database:Rule_fn, Tuples) :-
   dislog_variable_get(
      odbc_connection_data_dictionary, Connection),
   ddbase_fn_rule_to_tuples(
      Connection, Module, Database:Rule_fn, Tuples).

ddbase_fn_rule_to_tuples(
      Connection, Module, Database:Rule_fn, Tuples) :-
   star_line,
   writeln(user, ddbase_select),
   nl(user),
%  dwrite(pl, user, [Rule_fn]),
   ddbase_fn_rule_to_datalog(
      ddbase(Module), Database:Rule_fn, _:Rule_dl),
   dwrite(pl, user, [Rule_dl]),
   nl(user),
   datalog_rule_to_sql(Connection, Database:Rule_dl, Sql),
   mysql_select_command_to_name_1(Sql, Use_Command),
   writeln(user, Use_Command),
   odbc_query(mysql, Use_Command),
%  mysql_use_database(mysql, Database),
   mysql_select_command_to_name_2(Sql, Statement),
   select_statement_writeln(Statement),
   star_line,
   odbc_query_to_tuples(mysql, Statement, Tuples),
   !.


/* ddbase_select_to_statement(Module, Database:Rule_fn, Command) <-
      */

ddbase_select_to_statement(Module, Database:Rule_fn, Command) :-
   ddbase_fn_rule_to_tuples_to_statement(
      Module, Database:Rule_fn, Command).

ddbase_fn_rule_to_tuples_to_statement(Module, Database:Rule_fn, Command) :-
   dislog_variable_get(
      odbc_connection_data_dictionary, Connection),
   ddbase_fn_rule_to_tuples_to_statement(
      Connection, Module, Database:Rule_fn, Command).

ddbase_fn_rule_to_tuples_to_statement(
      Connection, Module, Database:Rule_fn, Command) :-
   star_line,
   ddbase_fn_rule_to_datalog(
      ddbase(Module), Database:Rule_fn, _:Rule_dl),
   dwrite(pl, user, [Rule_dl]),
   datalog_rule_to_sql(Connection, Database:Rule_dl, Sql),
   mysql_select_command_to_name_1(Sql, Use_Command),
   mysql_select_command_to_name_2(Sql, Statement),
   concat(Use_Command, Statement, Command).


/* select_statement_writeln(Statement) <-
      */

select_statement_writeln(Statement) :-
   Substitution = [
      ["from", "\nfrom"], ["where", "\nwhere"],
      ["and", "\nand"] ],
   name_exchange_sublist(Substitution, Statement, Statement_2),
   writeln(user, Statement_2).


/* ddbase_fn_rule_to_datalog(
         Type, Database:Rule_fn, Database:Rule_dl) <-
      */

ddbase_fn_rule_to_datalog(
      Type, Database:Rule_fn, Database:Rule_dl) :-
   Rule_fn = (Head_1 :- Body_1),
   ddbase_fn_conjunction_to_datalog(
      Type, Database, Body_1, Body_2),
%  Head_1 = P:As,
%  maplist( attribute_value_pair_to_value,
%     As, Xs ),
%  Head_2 =.. [P|Xs],
   Head_2 = Head_1,
   Rule_dl = (Head_2 :- Body_2).

attribute_value_pair_to_value(_:V, V).


/*** implementation ***********************************************/


/* ddbase_fn_conjunction_to_datalog(Type, Database, C1, C2) <-
      */

ddbase_fn_conjunction_to_datalog(Type, Database, C1, C2) :-
   comma_structure_to_list(C1, Atoms_1),
   ddbase_fn_atoms_expand(Type, Database, Atoms_1, Atoms),
   ddbase_fn_atoms_to_datalog(Type, Database, Atoms, Atoms_2),
   list_to_comma_structure(Atoms_2, C2).


/* ddbase_fn_atoms_expand(Type, Database, Atoms_1, Atoms_2) <-
      */

ddbase_fn_atoms_expand(Type, Database, Atoms_1, Atoms_2) :-
   maplist( ddbase_fn_atom_expand(Type, Database),
      Atoms_1, Atomss ),
   append(Atomss, Atoms_2).

ddbase_fn_atom_expand(Type, Database, c:Atom, Atoms) :-
   Atom =.. [P|_],
   \+ ( Type = odbc(Connection) ->
        mysql_relation_schema_to_attributes(
           Connection, Database:P, _)
      ; Type = ddbase(Module),
        Module:ddbase_schema(Table),
        [Database, P] := Table@[database, name] ),
   clause(Atom, C),
   comma_structure_to_list(C, Atoms).
ddbase_fn_atom_expand(_, _, Atom, [Atom]).


/* ddbase_fn_atoms_to_datalog(Type, Database, Fn_Atoms, Atoms) <-
      */

ddbase_fn_atoms_to_datalog(Type, Database, Fn_Atoms, Atoms) :-
   maplist( ddbase_fn_atom_to_datalog(Type, Database),
      Fn_Atoms, Atoms ).

ddbase_fn_atom_to_datalog(Type, Database, P:As, Atom) :-
   is_list(As),
   !,
   ( Type = odbc(Connection) ->
     mysql_relation_schema_to_attributes(
        Connection, Database:P, Attributes)
   ; Type = ddbase(Module),
     Module:ddbase_schema(Table),
     [Database, P] := Table@[database, name],
     ( Attributes := Table/attributes/content::'*' -> true
     ; findall( A, A := Table/attribute@name, Attributes ) ) ),
   maplist( ddbase_attribute_to_argument(As),
      Attributes, Xs ),
   Atom =.. [P|Xs].
%  ( P = M:Predicate ->
%    A =.. [Predicate|Xs],
%    Atom = M:A
%  ; A =.. [P|Xs],
%    Atom = Module:A ).
ddbase_fn_atom_to_datalog(_, _, Atom, Atom).

ddbase_attribute_to_argument(As, A, X) :-
   ( append(_, [A:X|As_2], As) ->
     ( member(A:_, As_2) ->
       ddbase_attribute_to_argument(As_2, A, X)
     ; true )
   ; X = _ ),
   !.
ddbase_attribute_to_argument_(As, A, X) :-
   ( member(A:V, As) ->
     X = V
   ; X = _ ).


/******************************************************************/


