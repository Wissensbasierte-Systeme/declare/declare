

/******************************************************************/
/***                                                            ***/
/***           Data Mining: Weka                                ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(weka_association_rules, all) :-
   forall( weka_association_rules(Table_Name, _Rules),
      ( star_line, writeln(user, Table_Name), star_line ) ).

test(weka_wrapper, abc) :-
   Table_Name = abc,
   Attributes = [a, b, c],
   Rows = [[a,b,c],[a,b,-],[-,b,c]],
   weka_association_rules(Table_Name, Attributes, Rows, Rules),
   dwrite(xml, Rules),
   association_rules_xml_select_non_minus(Rules, Rules_2),
   star_line,
   dwrite(xml, Rules_2).
test(weka_wrapper, pc_printer) :-
   Table_Name = printer,
   Transactions = [
      [printer, paper, pc, toner],
      [pc, scanner],
      [printer, paper, toner],
      [printer, pc],
      [printer, paper, pc, scanner, toner] ],
   transactions_to_table(Transactions, Attributes, Rows),
   weka_association_rules(Table_Name, Attributes, Rows, Rules),
   weka_association_rules_transform(Rules, Rules_1),
   dwrite(xml, Rules_1),
   association_rules_xml_select_non_minus(Rules_1, Rules_2),
   star_line,
   dwrite(xml, Rules_2).


/******************************************************************/


