

/******************************************************************/
/***                                                            ***/
/***           Association Rules: Redundancy                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


redundant_rule(Id_1, Id_2) :-
   association_rule(Id_1, Antecedent1, Consequent1, _, _),
   association_rule(Id_2, Antecedent2, Consequent2, _, 1),
   Id_1 =\= Id_2,
   subset(Antecedent2, Antecedent1),
   subset(Consequent1, Consequent2).

redundant_generic_basis_rule(Id_1, Id_2) :-
   association_rule(Id_1, Antecedent1, Consequent1, Sup, Conf),
   association_rule(Id_2, Antecedent2, Consequent2, Sup, Conf),
   Id_1 =\= Id_2,
   subset(Antecedent2, Antecedent1),
   subset(Consequent1, Consequent2).

redundant_and(Id_1, Id_2) :-
   redundant_generic_basis_rule(Id_1, Id_2),
   redundant_rule(Id_1, Id_2).

redundant_or(Id_1, Id_2) :-
   ( redundant_generic_basis_rule(Id_1, Id_2)
   ; redundant_rule(Id_1, Id_2) ).


/* subsumed_rule(Id_1, Id_2) <-
      association rule Id_1 is subsumed by Id_2. */

subsumed_rule(Id_1, Id_2) :-
   association_rule(Id_1, Ant1, Cons1, Sup1, Conf1),
   association_rule(Id_2, Ant2, Cons2, Sup2, Conf2),
   Id_1 =\= Id_2,
   subset(Ant2, Ant1), subset(Cons1, Cons2),
   Sup1 =< Sup2, Conf1 =< Conf2.

maximal_rule(Id_1) :-
   association_rule(Id_1, _, _, _, _),
   not(subsumed_rule(Id_1, _)).


prune_redundant(NameofStreamFile) :-
   open(NameofStreamFile, write, Stream),
   writeln(Stream, 'non-redundant association rules:'),
   writeln(Stream, ''),
   maximal_association_rule_id(MaxRule),
   ( for(I, 1, MaxRule) do
        ( not(redundant_rule(I, _)) ->
          write_association_rule(Stream, I)
        ; true ) ),
   close(Stream).

prune_redundant_no_Stream :-
   maximal_association_rule_id(MaxRule),
   ( for(I, 1, MaxRule) do
        ( not(redundant_rule(I, _)) ->
          write_association_rule(I)
        ; true ) ).

find_all_maximal_rule :-
   maximal_association_rule_id(MaxRule),
   ( for(I, 1, MaxRule) do
        ( not(subsumed_rule(I, _)) ->
          write_association_rule(I)
        ; true ) ).

maximal_non_redundant_rule :-
   maximal_association_rule_id(MaxRule),
   ( for(I, 1, MaxRule) do
        ( not(redundant_rule(I, _)), not(subsumed_rule(I, _)) ->
          write_association_rule(I)
        ; true ) ).


/* maximal_association_rule_id(MaxRule) <-
      */

maximal_association_rule_id(MaxRule) :-
   aggregate_all( max(ID),
      association_rule(ID, _, _, _, _),
      MaxRule ).


/* write_association_rule(Stream, I) <-
      */

write_association_rule(I) :-
   write_association_rule(user, I).

write_association_rule(Stream, I) :-
   association_rule(I, Ant, Cons, Sup, Conf),
   write_list(Stream, [ I, ': ', Ant, ' => ', Cons, ',',
      ' sup'=Sup, ', conf'=Conf ]),
   nl(Stream).


/******************************************************************/


