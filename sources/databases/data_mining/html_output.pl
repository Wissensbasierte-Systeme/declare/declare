

/******************************************************************/
/***                                                            ***/
/***            Data Mining: HTML Ouput                         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* association_rules_with_info_to_html(
         Freq:Conf, Attributes, Rules, Html) <-
      */

association_rules_with_info_to_html(
      Freq:Conf, Attributes, Rules) :-
   association_rules_with_info_to_html(
      Freq:Conf, Attributes, Rules, Html),
   html_to_display(Html, size(500, 200)).
   
association_rules_with_info_to_html(
      Freq:Conf, Attributes, Rules, Html) :-
   length(Attributes, N),
   M is N - 2,
   attributes_to_table_header_for_association_rule_table(
      Attributes, Header),
   maplist( association_rule_with_info_to_row(M),
      Rules, Rows ),
   concat(['Association Rules (F=', Freq, ', C=', Conf, ')'], Title),
   Html = html:[
      head:[title:[Title]],
      body:[table:[border:1]:[Header|Rows]] ].

attributes_to_table_header_for_association_rule_table(
      Attributes, Header) :-
   ( foreach(A, Attributes), foreach(Th, Ths) do
        Th = th:[A] ),
   Header = tr:Ths.

association_rule_with_info_to_row(N, F:C:(As-->Bs), Row) :-
   generate_interval(1, N, Ms),
   findall( Td,
      ( member(M, Ms),
        association_rule_to_nth_td(M, As-->Bs, Td) ),
      Tds ),
   round(C*100, 0, C2),
   concat(C2, '%', C3),
   Row = tr:[td:[F], td:[C3]|Tds].

association_rule_to_nth_td(N, As-->_, Td) :-
   member(N:V, As),
   !,
   Td = td:[]:[font:[color:'#0000dd']:[V]].
association_rule_to_nth_td(N, _-->Bs, Td) :-
   member(N:V, Bs),
   !,
   Td = td:[]:[font:[color:'#dd0000']:[V]].
association_rule_to_nth_td(N, As-->Bs, Td) :-
   no_singleton_variable(V),
   \+ member(N:V, As),
   \+ member(N:V, Bs),
   !,
   Td = td:[]:[''].


/******************************************************************/


