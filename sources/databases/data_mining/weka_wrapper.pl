

/******************************************************************/
/***                                                            ***/
/***           Data Mining: Weka Wrapper                        ***/
/***                                                            ***/
/******************************************************************/


:- module( weka_wrapper, [
      weka_association_lists_to_rules/2,
      weka_association_rules/2,
      weka_association_rules/3,
      weka_association_rules/4,
      weka_association_rules/5,
      weka_association_rules_transform/2 ] ).


/*** interface ****************************************************/


/* weka_association_lists_to_rules(Rows, Rules) <-
      */

weka_association_lists_to_rules(Rows, Rules) :-
   ( foreach(Row, Rows), foreach(Tuple, Table) do
        pair_lists(:, _, Tuple, Row) ),
   Rows = [Pairs|_],
   pair_lists(:, Attributes, _, Pairs),
   weka_rows_to_association_rules(Attributes, Table, Rules).

weka_rows_to_association_rules(Attributes, Rows, Rules) :-
%  Options = [                    'C':0.50],
%  Options = [          'N':4000, 'C':0.10],
   Options = ['M':0.02, 'N':4000, 'C':0.05],
   weka_association_rules(
      Options, 'relation', Attributes, Rows, Rules_2),
   weka_association_rules_transform(Rules_2, Rules).


/* weka_association_rules(Options, Table_Name, Rules) <-
      */

weka_association_rules(Table_Name, Rules) :-
   weka_association_rules([], Table_Name, Rules).

weka_association_rules(Options, Table_Name, Rules) :-
   data_mining_table_2(Table_Name, Attributes, Rows),
%  writeln(user, table(1, Table_Name)), wait,
   weka_association_rules(
      Options, Table_Name, Attributes, Rows, Rules_2),
   weka_association_rules_transform(Rules_2, Rules),
   dwrite(xml, Rules).
%  writeln(user, table(2, Table_Name)), wait.

weka_association_rules(Table_Name, Attributes, Rows, Rules) :-
   weka_association_rules(
      [], Table_Name, Attributes, Rows, Rules).


/* weka_association_rules(
         Options, Table_Name, Attributes, Rows, Rules) <-
      */

weka_association_rules(
      Options, Table_Name, Attributes, Rows, Rules) :-
   table_rows_prune_for_weka(Rows, Rows_2),
   dislog_variable_get(output_path, 'weka_input.arff', File_In),
   table_to_weka_file(Table_Name, Attributes, Rows_2, File_In),
   dislog_variable_get(output_path, 'weka_output', File_Out),
   weka_associations_file(Options, File_In, File_Out),
   weka_file_to_rules(File_Out, Rules),
   !.


/* weka_association_rules_transform(Rules_1, Rules_2) <-
      */

weka_association_rules_transform(Rules_1, Rules_2) :-
   fn_item_parse(Rules_1, rules:[]:Rs_1),
   maplist( weka_association_rule_transform,
      Rs_1, Rs_2),
   Rules_2 = rules:Rs_2,
   !.

weka_association_rule_transform(Rule_1, Rule_2) :-
   fn_item_parse(Rule_1, rule:[]:[Number|Es]),
   fn_item_parse(Number, number:[]:[N]),
   Rule_2 = rule:[number:N]:Es.


/*** implementation ***********************************************/


/* weka_associations_file(Options, File_In, File_Out) <-
      */

weka_associations_file(File_In, File_Out) :-
   weka_associations_file([], File_In, File_Out).

weka_associations_file(Options, File_In, File_Out) :-
   home_directory('/soft/Weka/weka-3-6-0', Weka_Directory),
   concat(Weka_Directory, '/data/weka_input.arff', Weka_Input),
   concat(Weka_Directory, '/data/weka_output', Weka_Output),
   concat(['cp ', File_In, ' ', Weka_Input], Cp_Command_1),
   writeln(user, Cp_Command_1),
   us(Cp_Command_1),
%  Options = ['M':0.02, 'N':4000, 'C':0.05],
   weka_options_list_to_string(Options, String),
   concat([ 'cd ', Weka_Directory, '; ',
      'java -cp ./weka.jar weka.associations.Apriori -t ',
      Weka_Input, ' ', String, ' > ', Weka_Output ], Command),
   writeln(user, Command),
   us(Command),
   concat(['cp ', Weka_Output, ' ', File_Out], Cp_Command_2),
   writeln(user, Cp_Command_2),
   us(Cp_Command_2).

weka_options_list_to_string(Options, String) :-
   ( foreach(A:V, Options), foreach(S, Strings) do
        concat(['-', A, ' ', V], S) ),
   names_append_with_separator(Strings, ' ', String).


/* table_to_weka_file(Table_Name, Attributes, Rows, File) <-
      */

table_to_weka_file(Table_Name, Attributes, Rows, File) :-
%  writelnq(table_to_weka_file(Table_Name, Attributes, Rows, File)),
   findall( A-Vs,
      ( nth(N, Attributes, A),
        setof( V,
           Row^( member(Row, Rows),
             nth(N, Row, V) ),
           Vs ) ),
      Pairs ),
%  writeln(Pairs),
   predicate_to_file( File,
      ( write('@relation '),
        writeq(Table_Name),
        writeln('.symbolic'), nl,
        ( foreach(A-Vs, Pairs) do
             write('@attribute '),
             writeq(A), write(' {'),
             writeq_list_with_comma(Vs),
             writeln('}') ),
        nl,
        writeln('@data'),
        ( foreach(Row, Rows) do
             writeq_list_with_comma(Row), nl ) ) ).


table_rows_prune_for_weka(Rows_1, Rows_2) :-
   maplist( table_row_prune_for_weka,
      Rows_1, Rows_2 ).

table_row_prune_for_weka(Row_1, Row_2) :-
   foreach(V1, Row_1), foreach(V2, Row_2) do
      name_exchange_sublist([
         [">= ", "greater_equal_"], 
         ["< ", "smaller_"],
         [" ", "_"] ], V1, V2).


/******************************************************************/


