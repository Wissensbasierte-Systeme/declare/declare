

/******************************************************************/
/***                                                            ***/
/***           Data Mining: Association Rules                   ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(weka_file_to_rules, 1) :-
   weka_file_to_rules('results/weka_output', rules:Rules), nl,
   ( foreach(Rule, Rules) do
        xml_rule_to_implication(Rule, Implication),
        writeq(Implication), nl ), nl.


/*** interface ****************************************************/


/* xml_rules_to_implications(Rules, Implications) <-
      */

xml_rules_to_implications(Rules, Implications) :-
   maplist( xml_rule_to_implication,
      Rules, Implications ).


/* xml_rule_to_implication(Rule, F:C:(Bs --> As)) <-
      */

xml_rule_to_implication(Rule, F:C:(Bs --> As)) :-
   formula_to_findings(head, Rule, As_),
   list_to_ord_set(As_, As),
   formula_to_findings(body, Rule, Bs_),
   list_to_ord_set(Bs_, Bs),
%  writeq(Rule:F:C:(Bs --> As)),
   [F] := Rule/head/number/content::'*',
   C := Rule/confidence/content::'*'.

formula_to_findings(Mode, Formula, Atoms) :-
   findall( A:V,
      ( Atom := Formula/Mode/finding,
        A := Atom@attribute,
        V := Atom@value ),
      Atoms ).


/* association_rules_xml_select_non_minus(Xml_1, Xml_2) <-
      */

association_rules_xml_select_non_minus(Xml_1, Xml_2) :-
   fn_item_parse(Xml_1, rules:_:Rules_1),
   association_rules_select_non_minus(Rules_1, Rules_2),
   Xml_2 = rules:Rules_2.

association_rules_select_non_minus(Rules_1, Rules_2) :-
   findall( Rule,
      ( member(Rule, Rules_1),
        \+ (- := Rule/head/finding@value),
        \+ (- := Rule/body/finding@value) ),
      Rules_2 ).


/* association_rules_xml_to_facts(Xml, Facts) <-
      */

association_rules_xml_to_facts(Xml, Facts) :-
   findall( Fact,
      ( Rule := Xml/rule,
        association_rule_xml_to_fact(Rule, Fact) ),
      Facts ).

association_rule_xml_to_fact(Xml, Fact) :-
%  N_ := Xml@number, atom_number(N_, N),
   [N_] := Xml/number/content::'*', atom_number(N_, N),
   findall( Av,
      ( F := Xml/body/finding, finding_to_av(F, Av) ),
      B ),
   findall( Av,
      ( F := Xml/head/finding, finding_to_av(F, Av) ),
      H ),
%  _:[N1] := Xml/body/number,
   _:[N2_] := Xml/head/number, atom_number(N2_, N2),
   _:C_ := Xml/confidence, atom_number(C_, C),
   Fact = association_rule(N, B, H, N2, C).

finding_to_av(Finding, A:V) :-
   A := Finding@attribute,
   V := Finding@value.


/******************************************************************/


