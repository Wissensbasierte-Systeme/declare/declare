

/******************************************************************/
/***                                                            ***/
/***           Data Mining: Weka Parser                         ***/
/***                                                            ***/
/******************************************************************/


:- module( weka_parser, [
      weka_file_to_rules/2,
      weka_output_file_to_rules_xml_file/2 ] ).


/*** interface ****************************************************/


/* weka_output_file_to_rules_xml_file(File_Out, File_Rules) <-
      */

weka_output_file_to_rules_xml_file(File_Out, File_Rules) :-
   weka_file_to_rules(File_Out, Rules),
   dwrite(xml, File_Rules, Rules).


/* weka_file_to_rules(File, Rules) <-
      */

weka_file_to_rules :-
   dislog_variable_get(output_path, 'weka_output', File),
   weka_file_to_rules(File, Rules),
   dwrite(xml, Rules).

weka_file_to_rules(File, Rules) :-
   read_file_to_name(File, Name_1),
   name_start_after_position(["Best rules found:"], Name_1, Name_2),
   name_cut_at_position(["=== Evaluation ==="], Name_2, Name_3),
   name_exchange_elements(["\n "], Name_3, Name_4),
   atom_chars(Name_4, Characters),
   rules([Rules], Characters, []),
   !.


/*** implementation ***********************************************/


rules ==>
   sequence('*', rule),
   blanks.

rule ==>
   blanks,
   numbering,
   blanks,
   body,
   rule_arrow, 
%  { write(' ==> ') },
   head,
%  { writeln('.') },
   blanks,
   confidence.

body ==>
   sequence('*', finding), number.

head ==>
   sequence('*', finding), number.

rule_arrow([]) -->
   [' ', '=', '=', '>', ' '].

numbering([number:[N]]) -->
   integer_number(N),
   ['.'].

number([number:[N]]) -->
   integer_number(N).

decimal_number(Decimal) -->
   integer_number(A),
   ['.'],
   integer_number(B),
   { concat([A, '.', B], Decimal) }.

integer_number(Number) -->
   sequence('**', digit, S),
   { findall( M, member([digit:[M]], S), Ms ), concat(Ms, Number) }.

digit ==> [A],
   { atom_codes(A, [N]), between(48, 57, N) }.

finding([finding:[attribute:A, value:V]:[]]) -->
   sequence('*', weka_in_attribute, Xs), ['='],
   sequence('*', weka_in_attribute, Ys), [' '],
   { atom_codes(A, Xs), atom_codes(V, Ys) }.
%  { atom_codes(A, Xs), atom_codes(V, Ys), write([A=V]) }.

weka_in_attribute(A) --> [A],
   { atom_codes(A, [N]),
     ( member(N, [32, 45, 95])
     ; member(N, [36, 47])
     ; between(48, 57, N)
     ; between(65, 90, N)
     ; between(97, 122, N) ) }.

confidence([confidence:N]) -->
   [c, o, n, f, ':', '('],
   ( integer_number(N)
   ; decimal_number(N)
   ; ['N', a, 'N'], { N = 'NaN' } ),
   [')'].

blanks([]) -->
   sequence('**', blank, _).

blank ==>
   [' '].


/******************************************************************/


