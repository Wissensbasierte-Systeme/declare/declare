

/******************************************************************/
/***                                                            ***/
/***            Data Mining: Decision Trees                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* data_mining_table_to_decision_tree(Table, Ns, Trees) <-
      */

data_mining_table_to_decision_tree(
      Table, Ns, [[N, Value]|Trees]) :-
   table_to_entropy(5, Table, Entropy),
   Entropy \= 0,
   data_mining_table_to_gain(Table, Ns, [N, Value]),
   data_mining_table_split([N, Value], Table, Tables_2),
   remove_empty_sets(Tables_2, Tables),
   Tables_2 \= [Table],
%  delete(Ns, N, Ms),
   Ms = Ns,
   ( foreach(T, Tables), foreach(Tree, Trees) do
        data_mining_table_to_decision_tree(T, Ms, Tree) ).
data_mining_table_to_decision_tree(
      Table, _, [['*', L, Entropy, Table]]) :-
   table_to_entropy(5, Table, Entropy),
   length(Table, L).


/* data_mining_table_split([N, Value], Table, Tables) <-
      */

data_mining_table_split([N, '--'], Table, Tables) :-
   !,
   projection(N, Table, Values_2),
   sort(Values_2, Values),
   ( foreach(V, Values), foreach(T, Tables) do
        findall( Row,
           ( member(Row, Table),
             nth(N, Row, V) ),
           T ) ).
data_mining_table_split([N, Value], Table, Tables) :-
   findall( Row,
      ( member(Row, Table),
        nth(N, Row, V),
        V < Value ),
      T1 ),
   findall( Row,
      ( member(Row, Table),
        nth(N, Row, V),
        V >= Value ),
      T2 ),
   Tables = [T1, T2].


/* data_mining_table_to_gain(Rows, Ns, [N, Value]) <-
      */

data_mining_table_to_gain(Rows, Ns, [N, Value]) :-
   data_mining_table_to_entropies(Rows, Ns, Triples),
   member([N, Value, _], Triples).


/* data_mining_table_to_entropies(Rows, Ns, Triples) <-
      */

data_mining_table_to_entropies_xpce(Rows, Ns, Triples) :-
   data_mining_table_to_entropies(Rows, Ns, Triples),
   xpce_display_table(['N', 'Value', 'Entropy_5^N'], Triples).

data_mining_table_to_entropies(Rows, Ns, Triples) :-
   findall( [N, Value, Entropy],
      ( member(N, Ns),
        ( member(N, [2, 3]),
          table_to_entropy(N, 5, Rows, Entropy),
          Value = '--'
        ; N = 4,
          data_mining_table_to_entropy(
             Rows, N, Value, Entropy) ) ),
      Triples_2 ),
   table_sort_by_positions([3], Triples_2, Triples).

data_mining_table_to_entropy(Rows, N, Value, Entropy) :-
   projection(N, Rows, Values_2),
   sort(Values_2, [_|Values]),
   member(Value, Values),
   maplist( data_mining_row_transform(credit, Value),
      Rows, Rows_2 ),
   table_to_entropy(N, 5, Rows_2, Entropy).


/******************************************************************/


