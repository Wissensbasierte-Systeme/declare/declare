

/******************************************************************/
/***                                                            ***/
/***           Data Mining: DSDK                                ***/
/***                                                            ***/
/******************************************************************/


:- module( dsdk, [
      dsdk_constraint/2,
      dsdk_constraints/1 ] ).


:- discontiguous
      dsdk_constraint/2,
      abnormality/2.


/*** facts ********************************************************/


weight(age, 4).
weight(car_color, 1).

normal(t=36.5-37.2).

abnormality(t='>39', 5).

similarity(color=white, color=gray, 0.5).
similarity(color=white, color=black, 0).

ordinal_attribute(age).
ordinal_attribute(car_size).
ordinal_attribute(liver_size).

attribute_partition(inner_organs, [
   [fatty_liver, liver_cirrhosis],
   [renal_failure, nephritis] ]).

domain(liver_size, [1,2,3,4,5,6]).

aggregate_attribute(liver_size).


/*** rules ********************************************************/


/* attribute(A) <-
      */

attribute(A) :-
   ( ordinal_attribute(A)
   ; aggregate_attribute(A)
   ; domain(A, _)
   ; weight(A, _)
   ; similarity(A=_, _, _)
   ; similarity(_, A=_, _)
   ; attribute_partition(_, Xs),
     member(X, Xs), member(A, X)
   ; normal(A=_)
   ; abnormality(A=_, _) ).


/* abnormality(A=V, N) <-
      */

abnormality(A=W, N) :-
   normal(A=V),
   ordinal_attribute(A),
   abnormal_distance(A=V, A=W, N).
abnormality(A=V, 1) :-
   normal(A=V),
   \+ ordinal_attribute(A).

abnormal_distance(A=V, A=W, N) :-
   domain(A, Vs),
   !,
   nth1(N1, Vs, V),
   nth1(N2, Vs, W),
   N is 1 + abs(N1 - N2).
abnormal_distance(A=V, A=V, 1).


/*** dsdk constraints *********************************************/


/* facts */

dsdk_constraint(exclude(attribute), car_color).
dsdk_constraint(include(attribute), age).

dsdk_constraint(exclude(attribute_pair),
   [car_color, car_size]).
dsdk_constraint(include(attribute_pair),
   [age, car_size]).

dsdk_constraint(exclude(feature), age=30-50).

dsdk_constraint(aggregated_values,
   age = [ [0-40,40-50], [50-70,70-100] ]).


/* attribute */

dsdk_constraint(exclude(attribute), A) :-
   weight(A, N), N =< 1.

dsdk_constraint(include(attribute), A) :-
   weight(A, N), N >= 1.
dsdk_constraint(include(attribute), A) :-
   weight(A, N), N >= 2,
   abnormality(A=_, 5).


/* attribute_pair */

dsdk_constraint(exclude(attribute_pair), [A1, A2]) :-
   attribute_partition(_, P),
   member(As_1, P), member(As_2, P), As_1 \= As_2,
   member(A1, As_1), member(A2, As_2).


/* feature */

dsdk_constraint(exclude(feature), A=V) :-
   normal(A=V).
dsdk_constraint(exclude(feature), A=V) :-
   abnormality(A=V, N), N =< 1.


/* error */

dsdk_constraint(error(include_exclude(X)), Y) :-
   dsdk_constraint(include(X), Y),
   dsdk_constraint(exclude(X), Y).


/* dsdk_constraint(aggregated_values, A=Values) <-
      */

dsdk_constraint(aggregated_values, A=Values) :-
   attribute(A),
   findall( Vs,
      abnormality_aggregate(A=Vs, _),
      Values ).

abnormality_aggregate(A=Vs, N) :-
   setof( V,
      abnormality(A=V, N),
      Vs ).

dsdk_constraint(aggregated_values_2, A=Values) :-
   attribute(A),
   findall( Vs,
      abnormality_aggregate_2(A=Vs),
      Values ).

abnormality_aggregate_2(A=Vs) :-
   setof( V,
      abnormality(A=V, _),
      Vs ).

dsdk_constraint(aggregated_values, A=Values) :-
   ordinal_attribute(A), aggregate_attribute(A),
   domain(A, Vs),
   findall( Ws,
      similar_sub_sequence(A, 0.5, Vs, Ws),
      Values ).

similar_sub_sequence(A, X, Vs, Ws) :-
   append(Us, _, Vs),
   similar_ending_sequence(A, X, 1, Us, Ws).

similar_ending_sequence(A, Min, X, [U|Us], Ws) :-
   similar_ending_sequence(A, Min, Y, Us, Vs),
   ( Ws = Vs
   ; first(Vs, V),
     similarity(A:U, A:V, Z),
     X is Z * Y,
     X >= Min,
     Ws = [U|Vs] ).
similar_ending_sequence(_, _, 1, [U], [U]).

dsdk_constraint(aggregated_values, A=Values) :-
   no_singleton_variable(V),
   ordinal_attribute(A), aggregate_attribute(A),
   domain(A, Vs),
   ( normal(A=V),
     append(Vs_1, [V|Vs_2], Vs)
   ; \+ normal(A=V),
     Vs_1 = Vs, Vs_2 = Vs ),
   findall( Ws,
      ( starting_sequence(Vs_1, Ws)
      ; ending_sequence(Vs_2, Ws) ),
      Values ).

starting_sequence(Vs, Ws) :-
   append(Ws, _, Vs), Ws \= [].

ending_sequence(Vs, Ws) :-
   append(_, Ws, Vs), Ws \= [].


/* dsdk_constraints(Xs) <-
      computes all dsdk constraints. */

dsdk_constraints(Xs) :-
   findall( Type-Constraint,
      dsdk_constraint(Type, Constraint),
      Xs ),
   writeln_list(Xs).


/******************************************************************/


