

/******************************************************************/
/***                                                            ***/
/***            Data Mining: Classification                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* table_to_entropy(N, Goal, Rows, Entropy) <-
      Entropy = entropy_Goal^N(Rows). */

table_to_entropy(N, Goal, Rows, Entropy) :-
   distribution_of_nth_attribute(N, Rows, Multiset),
   length(Rows, M),
   ( foreach(V:K, Multiset), foreach(Y, Ys) do
        P = K/M,
        table_select_wrt_nth_attribute(N, V, Rows, Rows_2),
        table_to_entropy(Goal, Rows_2, E),
        Y = E * P,
        Y_eval is Y,
        writeln(user, ((value:V):(Y_eval is E * P))) ),
   writeln(user, Ys),
   ( foreach(Y, Ys), foreach(Z, Zs) do
       Z is Y ),
   add(Zs, Entropy).


/* table_to_entropy(N, Rows, Entropy) <-
      Entropy = entropy_N(Rows). */

table_to_entropy(N, Rows, Entropy) :-
   distribution_of_nth_attribute(N, Rows, Multiset),
   length(Rows, M),
   ( foreach(_:K, Multiset), foreach(Y, Ys) do
        P is K/M,
        log(2, P, L),
        Y is - P * L ),
   ( foreach(_:K, Multiset), foreach(Z, Zs) do
        Z = - K/M * log_2(K/M) ),
   writeln(user, Zs),
   add(Ys, Entropy).

distribution_of_nth_attribute(N, Rows, Multiset) :-
   maplist( nth(N),
      Rows, Values ),
   list_to_multiset(Values, Multiset).


/* table_select_wrt_nth_attribute(
         N, Value, Table_1, Table_2) <-
      */

table_select_wrt_nth_attribute(N, Value, Table_1, Table_2) :-
   findall( Row,
      ( member(Row, Table_1),
        nth(N, Row, Value) ),
      Table_2 ),
   !.


/* entropy(Probabilities, Entropy) <-
      */

entropy(Probabilities, Entropy) :-
   ( foreach(Probability, Probabilities), foreach(E, Entropies) do
        entropy_epsilon(Probability, E) ),
   add(Entropies, Entropy).


/* entropy(N, Numbers, Entropy) <-
      */

entropy(N, Numbers, Entropy) :-
   maplist( multiply(1/N),
      Numbers, Probabilities ),
   call(entropy, Probabilities, Entropy).


/* entropy_epsilon(Probability, Entropy) <-
      */

entropy_epsilon(Probability, Entropy) :-
   log(2, Probability, Log),
   Entropy is - Probability * Log.


/******************************************************************/


