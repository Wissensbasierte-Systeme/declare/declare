

/******************************************************************/
/***                                                            ***/
/***           Data Mining: Association Rules                   ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(tuples_to_association_rules, 1) :-
%  Tuples = [[a:1, b:2, c:3], [a:1, b:2, c:4], [a:2, b:2, c:5]],
   Tuples = [
      [a:1, b:1, c:3], 
      [a:1, b:1, c:4],
%     [a:2, b:1, c:5],
%     [a:2, b:2, c:6] ],
      [a:2, b:1, c:3],
      [a:2, b:2, c:5] ],
   tuples_to_association_rules(Tuples, Implications),
   writeln('Implications'),
   writelnq_list(Implications),
   star_line,
%  association_rules_search(
%     [b:'1', c:'4'], Implications, Implications_2),
   association_rules_search(
      _:_:([b:'1']-->[c:'4']), [b:'1', c:'4'],
      Implications, Implications_2),
   star_line,
   writeln('Implications_2'),
   writelnq_list(Implications_2),
   star_line.

test(tuples_to_association_rules, 2) :-
   Tuples = [
      [a:1, b:1, c:3],
      [a:1, b:1, c:4],
      [a:2, b:1, c:3],
      [a:2, b:2, c:5] ],
   tuples_to_association_rules(Tuples, Rules_1),
   member(Rule, Rules_1),
   association_rules_search_split(Rules_1, Rule, Rules_2),
   star_line,
   writeln(Rule),
   writeln_list(Rules_2).


/*** interface ****************************************************/


/* association_rules_search_split(Rules_1, Rule, Rules_2) <-
      */

association_rules_search_split(Rules_1, Rule, Rules_2) :-
   findall( Rule_1,
      ( member(Rule_1, Rules_1),
        Rule = _:_:(As-->Bs),
        Rule_1 = _:_:(As_1-->Bs_1),
        Rule \= Rule_1,
        ord_subset(As_1, As),
        ord_subset(Bs_1, Bs),
        length(Bs, K), K > 1 ),
      Rules_2 ),
   length(Rules_2, N),
   N > 0.


/* association_rules_search(Pairs, Rules_1, Rules_2) <-
      */

association_rules_search(Pairs, Rules_1, Rules_2) :-
   findall( Rule,
      ( member(Rule, Rules_1),
        Rule = _F:_C:(As-->Bs),
        ord_union(As, Bs, Cs),
        ord_subset(Pairs, Cs) ),
      Rules_2 ).

association_rules_search(
      _:_:(As-->Bs), Pairs, Rules_1, Rules_2) :-
   findall( K-Rule,
      ( member(Rule, Rules_1),
        Rule = _:_:(As_1-->Bs_1),
        ord_union(As_1, Bs_1, Cs_1),
        ord_subset(Pairs, Cs_1),
        ord_subtract(As, As_1, As_2),
        ord_subtract(Bs, Bs_1, Bs_2),
        length(As_2, N),
        length(Bs_2, M),
        ord_subtract(As_1, As, As_3),
        ord_subtract(Bs_1, Bs, Bs_3),
        length(As_3, N_3),
        length(Bs_3, M_3),
        K is 3*N + 3*M + N_3 + M_3,
        writelnq(K is 3*N + 3*M + N_3 + M_3) ),
      Rules ),
   sort(Rules, Rules_2).


/* tuples_to_association_rules(Tuples, Implications) <-
      */

tuples_to_association_rules(Tuples, Implications) :-
   weka_association_lists_to_rules(Tuples, Xml),
   dwrite(xml, Xml),
   star_line,
   Xml = rules:Rules,
   xml_rules_to_implications(Rules, Implications),
   association_rule_subsumes_others(Implications, Subsumptions),
   writeln('Subsumptions'),
   writelnq_list(Subsumptions),
   ddbase_aggregate( [N, length(Rule)],
      member(N-Rule, Subsumptions),
      Aggregation ),
   writeln(Aggregation),
   star_line,
   association_rule_subsumes_others_aggregate(
      Subsumptions, Subsumptions_Agg),
   writeln('Subsumptions_Agg'),
   writelnq_list(Subsumptions_Agg),
   star_line,
   association_rules_subsumes_maximal(Implications, Maximal),
   writeln('Maximal'),
   writelnq_list(Maximal),
   star_line.


/* association_rule_subsumes_2(Rule_2, Rule_1) <-
      */

association_rule_subsumes_2(Rule_2, Rule_1) :-
   Rule_1 = Sup_1:Conf_1:(Ant_1-->Cons_1),
   Rule_2 = Sup_2:Conf_2:(Ant_2-->Cons_2),
   subset(Ant_2, Ant_1),
   subset(Cons_1, Cons_2),
   maplist( atom_number,
      [Sup_1, Conf_1, Sup_2, Conf_2],
      [S_1, C_1, S_2, C_2] ),
   S_1 =< S_2, C_1 =< C_2.


/* association_rule_subsumes_others(Rules, Subsumptions) <-
      */

association_rule_subsumes_others(Rules, Subsumptions) :-
   findall( N-Rule,
      ( member(Rule, Rules),
        association_rule_subsumes_others(Rule, Rules, Rules_2),
        length(Rules_2, N) ),
      Subsumptions ).

association_rule_subsumes_others(Rule, Rules_1, Rules_2) :-
   findall( Rule_1,
      ( member(Rule_1, Rules_1),
        association_rule_subsumes_2(Rule, Rule_1) ),
      Rules_2 ).


/* association_rule_subsumes_others_aggregate(
         Subsumptions, Subsumptions_Agg) <-
      */

association_rule_subsumes_others_aggregate(
      Subsumptions, Subsumptions_Agg) :-
   ddbase_aggregate( [N, length(Rule), list(Rule)],
      member(N:Rule, Subsumptions),
      Subsumptions_Agg ).


/* association_rules_subsumes_maximal(Rules_1, Rules_2) <-
      */

association_rules_subsumes_maximal(Rules_1, Rules_2) :-
   findall( Rule_1,
      ( member(Rule_1, Rules_1),
        \+ ( member(Rule_2, Rules_1),
             Rule_1 \= Rule_2,
             association_rule_subsumes_2(Rule_2, Rule_1) ) ),
      Rules_2 ).


/******************************************************************/


