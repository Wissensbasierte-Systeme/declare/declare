

/******************************************************************/
/***                                                            ***/
/***            Data Mining: Demos                              ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      data_mining_row_transform/3,
      demo_dm/2.


/*** interface ****************************************************/


demo(data_mining, entropy) :-
   demo_dm(data_mining, entropy(credit, 5)).

demo_dm(data_mining, entropy(Table_Name, Target)) :-
   data_mining_table_2(Table_Name, Attributes, Table),
   ( for(N, 1, Target) do
        nth(N, Attributes, A),
        nth(Target, Attributes, Z),
        bar_line(user),
        writeln(user, A),
        table_to_entropy(N, Table, E1),
        write_list(user, [
           'entropy_', A, '(T) = ', E1, '\n']),
        table_to_entropy(N, Target, Table, E2),
        write_list(user, [
           'entropy_', Z, '^', A, '(T) = ', E2, '\n']) ).


demo(data_mining, frequent_itemsets) :-
   Mins = [2, 3, 4, 5, 6, 7, 8],
   ( foreach(Min, Mins) do
        bar_line_3,
        writeln('Min'=Min),
        bar_line_3,
        a_priori_algorithm_2(credit, Min, _) ).

a_priori_algorithm_2(Table_Name, Min, Max_Sets) :-
   data_mining_table_to_transactions(Table_Name, _, Transactions),
   a_priori_algorithm(Transactions, Min, Max_Sets).


demo(data_mining, association_rules) :-
   Pairs = [2:0.8],
%  Pairs = [6:0.8],
%  Pairs = [8:0.9],
%  Pairs = [ 2:0.8, 6:0.8, 8:0.9 ],
   ( foreach(Freq:Conf, Pairs) do
        association_rules_2(credit, Freq, Conf, _) ).

demo(data_mining, association_rules) :-
   Freq = 2, Conf = 0.8, 
   demo_dm(data_mining, association_rules(Freq, Conf)).

demo_dm(data_mining, association_rules(Freq, Conf)) :-
   association_rules_2(credit, Freq, Conf, _).

association_rules_2(Name, Freq, Conf, Rules) :-
   data_mining_table_to_transactions(Name, As, Transactions),
   measure( association_rules(Transactions, Freq, Conf, Rules),
      Time ), pp_time(Time), writeln(' sec.'),
   Attributes = ['Frequency', 'Confidence'|As],
   association_rules_with_info_to_html(
      Freq:Conf, Attributes, Rules).


/*** implementation ***********************************************/


/* data_mining_table_to_transactions(
         Table_Name, Attributes, Transactions) <-
      */

data_mining_table_to_transactions(
      Table_Name, Attributes, Transactions) :-
   data_mining_table_2(Table_Name, Attributes, Rows),
   table_to_transactions(Rows, Transactions).


/* data_mining_table_2(Table_Name, Attributes, Rows) <-
      */

data_mining_table_2(Table_Name, Attributes, Rows) :-
   data_mining_table(Table_Name, Attributes, Rows_2),
   data_mining_table_transform(Table_Name, Rows_2, Rows).


/* data_mining_table_transform(Table_Name, Rows_1, Rows_2) <-
      */

data_mining_table_transform(Table_Name, Rows_1, Rows_2) :-
   maplist( data_mining_row_transform(Table_Name),
      Rows_1, Rows_2 ).

data_mining_row_transform(soccer, Vs, Vs).

data_mining_row_transform(credit, Vs, Ws) :-
   S = 30,
   data_mining_row_transform(credit, S, Vs, Ws).

data_mining_row_transform(credit, S, Vs, Ws) :-
   append(Xs, [Y|Ys], Vs),
   length(Xs, 3),
   concat('< ', S, Smaller),
   concat('>= ', S, Greater),
   ( Y < S,
     Z = Smaller
   ; Y >= S,
     Z = Greater ),
   append(Xs, [Z|Ys], Ws).

data_mining_row_transform(employee, Tuple_1, Tuple_2) :-
   Tuple_1 = [Fname, Minit, Lname,
      Ssn, Bdate_1, Address_1, Sex, Salary_1, Superssn, Dno],
   term_to_atom(Year_1-_-_, Bdate_1),
   term_to_atom(Salary, Salary_1),
   round_value_for_data_mining(10, Year_1, Year_2),
   round_value_for_data_mining(10000, Salary, Salary_2),
   name_split_at_position([", "], Address_1, Address),
   last(Address, Address_2),
%  Address_2 = Address_1,
   Tuple_2 = [Fname, Minit, Lname,
      Ssn, Year_2, Address_2, Sex, Salary_2, Superssn, Dno].

data_mining_row_transform(employee_2, Tuple, Tuple).

round_value_for_data_mining(Factor, Value_1, Value_2) :-
   round(Value_1/Factor, 0, Value),
   Value_2 is Value * Factor.

 
/* data_mining_table(Table_Name, Attributes, Rows) <-
      */

data_mining_table(credit, Attributes, Rows) :-
   Attributes = [
      'Id', 'Married', 'PrevDef', 'Income', 'Def' ],
   Rows = [
      ['C1', yes, no, 50, no],
      ['C2', yes, no, 100, no],
      ['C3', no, yes, 135, yes],
      ['C4', yes, no, 125, no],
      ['C5', yes, no, 50, no],
      ['C6', no, no, 30, no],
      ['C7', yes, yes, 10, no],
      ['C8', yes, no, 10, yes],
      ['C9', yes, no, 75, no],
      ['C10', yes, yes, 45, no],
      ['C11', yes, no, 60, yes],
      ['C12', no, yes, 120, yes],
      ['C13', yes, yes, 20, no],
      ['C14', no, no, 15, no],
      ['C15', no, no, 60, no],
      ['C16', yes, no, 15, yes],
      ['C17', yes, no, 35, no],
      ['C18', no, yes, 160, yes],
      ['C19', yes, no, 40, no],
      ['C20', yes, no, 30, no] ].

data_mining_table(employee, Attributes, Rows) :-
   Attributes = [
      'FNAME', 'MINIT', 'LNAME', 'SSN', 'BDATE',
      'ADDRESS', 'SEX', 'SALARY', 'SUPERSSN', 'DNO' ],
   Rows = [
      ['John', 'B', 'Smith', '123456789', '1955-01-09',
       '731 Fondren, Houston, TX', 'M', 30000,
       '333445555', 5],
      ['Franklin', 'T', 'Wong', '333445555', '1945-12-08',
       '638 Voss, Houston, TX', 'M', 40000,
       '888665555', 5],
      ['Alicia', 'J', 'Zelaya', '999887777', '1958-07-19',
       '3321 Castle, Spring, TX', 'F', 25000,
       '987654321', 4],
      ['Jennifer', 'S', 'Wallace', '987654321', '1931-06-20',
       '291 Berry, Bellaire, TX', 'F', 43000,
       '888665555', 4],
      ['Ramesh', 'K', 'Narayan', '666884444', '1952-09-15',
       '975 Fire Oak, Humble, TX', 'M', 38000,
       '333445555', 5],
      ['Joyce', 'A', 'English', '453453453', '1962-07-31',
       '5631 Rice, Houston, TX', 'F', 25000,
       '333445555', 5],
      ['Ahmad', 'V', 'Jabbar', '987987987', '1959-03-29',
       '980 Dallas, Houston, TX', 'M', 25000,
       '987654321', 4],
      ['James', 'E', 'Borg', '888665555', '1927-11-10',
       '450 Stone, Houston, TX', 'M', 55000,
       '$null$', 1] ].

data_mining_table(employee_short, Attributes, Rows) :-
   Attributes = [
      'LNAME', 'SSN', 'BDATE',
      'SEX', 'SALARY', 'SUPERSSN', 'DNO' ],
   Rows = [
      ['Smith', '123456789', '1955-01-09', 'M', 30000, '333445555', 5],
      ['Wong', '333445555', '1945-12-08', 'M', 40000, '888665555', 5],
      ['Zelaya', '999887777', '1958-07-19', 'F', 25000, '987654321', 4],
      ['Wallace', '987654321', '1931-06-20', 'F', 43000, '888665555', 4],
      ['Narayan', '666884444', '1952-09-15', 'M', 38000, '333445555', 5],
      ['English', '453453453', '1962-07-31', 'F', 25000, '333445555', 5],
      ['Jabbar', '987987987', '1959-03-29', 'M', 25000, '987654321', 4],
      ['Borg', '888665555', '1927-11-10', 'M', 55000, '$null$', 1] ].

data_mining_table(employee_2, Attributes, Rows) :-
   Attributes = [
      'SSN', 'BDATE', 'SEX', 'SALARY', 'SUPERSSN', 'DNO' ],
   Rows = [
       [111, 1955, 'M', 30000, 333, 5],
       [222, 1959, 'F', 40000, 666, 4],
       [333, 1945, 'M', 50000, 777, 5],
       [444, 1962, 'F', 30000, 333, 5],
       [555, 1952, 'M', 30000, 333, 5],
       [666, 1931, 'F', 50000, 777, 4],
       [777, 1927, 'M', 60000, 000, 1],
       [888, 1958, 'F', 40000, 666, 4] ].

data_mining_table(employee_3, Attributes, Rows) :-
   Attributes = [
      'SSN', 'BDATE', 'SEX', 'SALARY', 'SUPERSSN', 'DNO1', 'DNO2', 'DNO3' ],
   sqrt(2, S),
   Rows = [
       [111111111, 1927, 'M', 60000, 000000000, S, 0, 0],
       [222222222, 1945, 'M', 50000, 111111111, 0, 0, S],
       [333333333, 1931, 'F', 50000, 111111111, 0, S, 0],
       [444444444, 1955, 'M', 30000, 222222222, 0, 0, S],
       [555555555, 1952, 'M', 30000, 222222222, 0, 0, S],
       [666666666, 1962, 'F', 30000, 222222222, 0, 0, S],
       [777777777, 1958, 'F', 40000, 333333333, 0, S, 0],
       [888888888, 1959, 'F', 40000, 333333333, 0, S, 0] ].

data_mining_table(soccer, Attributes, Rows) :-
   Attributes = [
      'Year', 'Champion', 'C_Region', 'Organizer', 'O_Region' ],
   Rows = [
       [1970, 'Brasilien', 'Amerika', 'Mexiko', 'Amerika'],
       [1974, 'Deutschland', 'Europa', 'Deutschland', 'Europa'],
       [1978, 'Argentinien', 'Amerika', 'Argentinien', 'Amerika'],
       [1982, 'Italien', 'Europa', 'Spanien', 'Europa'],
       [1986, 'Argentinien', 'Amerika', 'Mexiko', 'Amerika'],
       [1990, 'Deutschland', 'Europa', 'Italien', 'Europa'],
       [1994, 'Brasilien', 'Amerika', 'USA', 'Amerika'],
       [1998, 'Frankreich', 'Europa', 'Frankreich', 'Europa'],
       [2002, 'Brasilien', 'Amerika', 'Japan/Suedkorea', 'Asien'],
       [2006, 'Italien', 'Europa', 'Deutschland', 'Europa' ] ].

data_mining_table(age_grade, Attributes, Rows) :-
   Attributes = [
      'Id', 'Age', 'Grade' ],
   Rows = [
      ['S1', 17, 3.9], ['S2', 17, 3.5], ['S3', 18, 3.1],
      ['S4', 20, 3.0], ['S5', 23, 3.5], ['S6', 26, 3.6] ].


/* data_mining_test_clustering(Table_Name, Clusters) <-
      */

data_mining_test_clustering(credit, Clusters) :-
   Clusters = [
      [ ['C1', yes, no, 50, no],
        ['C5', yes, no, 50, no],
        ['C17', yes, no, 35, no],
        ['C19', yes, no, 40, no],
        ['C20', yes, no, 30, no],

        ['C2', yes, no, 100, no],
        ['C4', yes, no, 125, no],
        ['C9', yes, no, 75, no] ],

      [ ['C8', yes, no, 10, yes],
        ['C16', yes, no, 15, yes] ],

      [ ['C11', yes, no, 60, yes] ],

      [ ['C7', yes, yes, 10, no],
        ['C10', yes, yes, 45, no],
        ['C13', yes, yes, 20, no] ],

      [ ['C6', no, no, 30, no],
        ['C14', no, no, 15, no],

        ['C15', no, no, 60, no] ],

      [ ['C3', no, yes, 135, yes],
        ['C12', no, yes, 120, yes],
        ['C18', no, yes, 160, yes] ] ].


/* data_mining_transactions(Transactions) <-
      */

data_mining_transactions(Transactions) :-
   Transactions_2 = [
      [printer, paper, pc, toner],
      [pc, scanner],
      [printer, paper, toner],
      [printer, pc],
      [printer, paper, pc, scanner, toner] ],
   maplist( sort,
      Transactions_2, Transactions ).


/******************************************************************/


