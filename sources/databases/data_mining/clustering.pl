

/******************************************************************/
/***                                                            ***/
/***           Data Mining: Clustering                          ***/
/***                                                            ***/
/******************************************************************/


/*** configuration ************************************************/


/* k_means_distance(Point_1, Point_2, Distance) <-
      */

k_means_distance(Xs, Ys, Distance) :-
   ( foreach(X, Xs), foreach(Y, Ys), foreach(Z, Zs) do
        ( number(X), number(Y), Z is (X-Y)^2
        ; X = V:_, Y = V:_, Z = 0
        ; X = _:A, Y = _:B, Z is A*B
        ; X = '--', Z is 0
        ; Y = '--', Z is 0 ) ),
   add(Zs, Z),
   Distance is sqrt(Z),
   !.


/* points_to_center(Points, Center) <-
      */

points_to_center(Points, Center) :-
   ( dislog_variable_get(k_means_center, centroid),
     points_to_centroid(Points, Center)
   ; dislog_variable_get(k_means_center, center_of_gravity),
     points_to_center_of_gravity(Points, Center) ).


/*** interface ****************************************************/


/* k_means_clustering_to_xpce_table(
         Type, Table, K, Ms, Weights) <-
      */

k_means_clustering_to_xpce_table(Type, Table, K, Ms, Weights) :-
   dislog_variable_set(k_means_center, Type),
   data_mining_table(Table, Attributes, Rows),
   extend_tuples_by_numbers(Rows, Rows_2),
   k_means_apply_weights(Weights, Rows_2, Points),
   k_means_clustering_to_xpce_table(
      K, Ms, ['#'|Attributes], Points),
   !.


/* k_means_clustering_to_xpce_table(K, Ms, Attributes, Points) <-
      */

k_means_clustering_to_xpce_table(K, Ms, Attributes, Points) :-
   initial_cluster_centers(K, Ms, Points, Centers),
   k_means_clustering(K, Centers, Points, Clusters),
   clusters_to_xpce_table(Attributes, Clusters).


/* k_means_clustering(K, Centers, Points, Clusters) <-
      */

k_means_clustering(_K, Centers, Points, Clusters) :-
   clusters_rearrange(Centers, [Points], Clusters_Initial),
%  ( length(Clusters_Initial, K)
%  ; writeln(user, 'not enough clusters'),
%    true ),
   k_means_clustering_intelligent(Clusters_Initial, Clusters).


/* k_means_clustering_intelligent(Clusters_1, Clusters_2) <-
      */

k_means_clustering_intelligent(Clusters_1, Clusters_2) :-
   k_means_clustering(Clusters_1, Clusters_A),
   cluster_exchange(Clusters_A, Clusters_B),
   ( Clusters_1 = Clusters_B ->
     Clusters_2 = Clusters_B
   ; k_means_clustering_intelligent(Clusters_B, Clusters_2) ).


/* k_means_clustering(Clusters_1, Clusters_2) <-
      */

k_means_clustering(Clusters_1, Clusters_2) :-
   k_means_clustering_step(Clusters_1, Clusters),
   ( Clusters_1 = Clusters ->
     Clusters_2 = Clusters
   ; k_means_clustering(Clusters, Clusters_2) ).

k_means_clustering_step(Clusters_1, Clusters_2) :-
   clusters_to_cost(Clusters_1, Cost),
   write_list(user, ['cost: ', Cost, '\n']),
   show_clusters(Clusters_1),
   maplist( points_to_center,
      Clusters_1, Centers ),
   show_centers(Centers),
   star_line,
   clusters_rearrange(Centers, Clusters_1, Clusters_A),
   sort(Clusters_A, Clusters_B),
   length(Clusters_1, N),
   length(Clusters_B, N),
   clusters_to_cost(Clusters_B, Cost_B),
   ( Cost_B =< Cost ->
     Clusters_2 = Clusters_B
   ; Clusters_2 = Clusters_1 ).


/* cluster_exchange(Clusters_1, Clusters_2) <-
      */

cluster_exchange(Clusters_1, Clusters_2) :-
   two_members(Clusters_1, Cs_1, A, Cs_2, B, Cs_3),
   ( [Cluster_1, Cluster_2] = [A, B],
     [Cluster_1a, Cluster_2a] = [C, D]
   ; [Cluster_2, Cluster_1] = [A, B],
     [Cluster_2a, Cluster_1a] = [C, D] ),
   append(Ps_1, [Point|Ps_2], Cluster_1),
   append(Ps_1, Ps_2, Cluster_1a),
   distance_of_clusters([Point], Cluster_1a, Dist_1),
   distance_of_clusters([Point], Cluster_2, Dist_2),
   Dist_2 < Dist_1,
   write(user, 'exchange: '),
   show_point(Point), nl(user),
   show_cluster(Cluster_1), show_cluster(Cluster_2),
   star_line,
   sort([Point|Cluster_2], Cluster_2a),
   two_members(Clusters_2, Cs_1, C, Cs_2, D, Cs_3).
cluster_exchange(Clusters, Clusters).

two_members(Xs, Xs_1, A, Xs_2, B, Xs_3) :-
   append(Xs_1, [A|Ys], Xs),
   append(Xs_2, [B|Xs_3], Ys).


/*** implementation ***********************************************/


/* show_centers(Centers) <-
      */

show_centers(Centers) :-
   writeln(user, 'centers:'),
   ( foreach(Point, Centers) do
        ( dislog_variable_get(k_means_center, centroid),
          Point = [Id:_|_],
          write_list(user, ['   ', Id, '\n'])
        ; dislog_variable_get(k_means_center, center_of_gravity),
          write_list(user, ['   ', Point, '\n']) ) ),
   !.


/* show_clusters(Clusters) <-
      */

show_clusters(Clusters) :-
%  writeln_list(user, ['clusters:'|Clusters]),
   writeln(user, 'clusters:'),
   maplist( show_cluster,
      Clusters ).

show_cluster(Points) :-
   write(user, '   '),
   ( foreach(Point, Points) do
        show_point(Point) ),
   nl(user).

show_point([Id:_|_]) :-
   write_list(user, [Id, ' ']).


/* clusters_to_xpce_table(Attributes, Clusters) <-
      */

clusters_to_xpce_table(Attributes, Clusters) :-
   clusters_to_rows(Clusters, Rows),
   xpce_display_table([''|Attributes], Rows).

clusters_to_rows(Clusters, Rows) :-
   findall( [N|Row],
      ( nth(N, Clusters, Cluster),
        member(Row, Cluster) ),
      Rows_2 ),
   sort(Rows_2, Rows).


/* initial_cluster_centers(K, Type, Points, Centers) <-
      */

initial_cluster_centers(K, Type, Points, Centers) :-
   length(Points, N),
   initial_cluster_center_indexes(K, Type, N, Ms),
   nth_multiple(Ms, Points, Centers),
   star_line,
   show_centers(Centers),
   star_line,
   not( ( member(C1, Centers), member(C2, Centers), C1 \= C2,
      k_means_distance(C1, C2, Distance),
      0 is sign(Distance) ) ),
   !.

initial_cluster_center_indexes(K, first, _, Ms) :-
   generate_interval(1, K, Ms).
initial_cluster_center_indexes(K, randoms, N, Ms) :-
   repeat,
      randoms(K, N, Ms),
   \+ member(0, Ms).
initial_cluster_center_indexes(_, Ms, _, Ms).


/* clusters_rearrange(Centers, Clusters_1, Clusters_2) <-
      */

clusters_rearrange(Centers, Clusters_1, Clusters_2) :-
   findall( Point-Center,
      ( member(Cluster, Clusters_1),
        member(Point, Cluster),
        points_and_point_to_nearest_neighbour(
           Centers, Point, Center) ),
      Pairs ),
   findall( Cluster,
      ( member(Center, Centers),
        findall( Point,
           member(Point-Center, Pairs),
           Cluster_2 ),
        Cluster_2 \= [],
        sort(Cluster_2, Cluster) ),
      Clusters_2 ).

points_and_point_to_nearest_neighbour(Points, Point, Neighbour) :-
   ( foreach(P, Points), foreach(Dist-P, Xs) do
        k_means_distance(P, Point, Dist) ),
   sort(Xs, [_-Neighbour|_]).


/* clusters_to_cost(Clusters, Cost) <-
      */

clusters_to_cost(Clusters, Cost) :-
   findall( C,
      ( member(Cluster, Clusters),
        append(_, [Point_1|Points], Cluster),
        member(Point_2, Points),
        k_means_distance(Point_1, Point_2, C) ),
      Cs ),
   add(Cs, Cost).


/* distance_of_clusters(Clusters, Distance) <-
      */

distance_of_clusters(Clusters, Distance) :-
   findall( D,
      ( append(_, [C1|Cs], Clusters),
        member(C2, Cs),
        distance_of_clusters(C1, C2, D) ),
      Ds ),
   add(Ds, Distance).
   

/* distance_of_clusters(Points_1, Points_2, Distance) <-
      */

distance_of_clusters(Points_1, Points_2, Distance) :-
   findall( D,
      ( member(Point_1, Points_1),
        member(Point_2, Points_2),
        k_means_distance(Point_1, Point_2, D) ),
      Ds ),
   add(Ds, Distance).


/* points_to_centroid(Points, Centroid) <-
      */

points_to_centroid(Points, Centroid) :-
   ( foreach(Point, Points), foreach(Distance-Point, Pairs) do
        points_and_point_to_distance(Points, Point, Distance) ),
   sort(Pairs, [_-Centroid|_]).

points_and_point_to_distance(Points, Point, Distance) :-
   maplist( k_means_distance(Point),
      Points, Distances ),
   add(Distances, Distance).


/* points_to_center_of_gravity(Points, Center) <-
      */

points_to_center_of_gravity(Points, Center) :-
   findall( X,
      ( maplist( nth(_),
           Points, Values_1 ),
        sublist( number,
           Values_1, Values_2 ),
        average(Values_2, X) ),
      Center ),
   !.


/* k_means_apply_weights(Weights, Points_1, Points_2) <-
      */

k_means_apply_weights(Weights, Points_1, Points_2) :-
   maplist( vectors_multiply_(Weights),
      Points_1, Points_2 ).

vectors_multiply_(Weights, [X|Xs], [X|Ys]) :-
   vectors_multiply_(Weights, Xs, Ys).

vectors_multiply_([W|Ws], [X|Xs], [Y|Ys]) :-
   ( number(X) ->
     Y is W * X
   ; Y = X:W ),
   !,
   vectors_multiply_(Ws, Xs, Ys).
vectors_multiply_([], [], []).


/* extend_tuples_by_numbers(Points_1, Points_2) <-
      */

extend_tuples_by_numbers(Points_1, Points_2) :-
   extend_tuples_by_numbers(1, Points_1, Points_2).

extend_tuples_by_numbers(N, [P|Ps], [Q|Qs]) :-
   atom_number(Id, N),
   Q = [Id|P],
   M is N + 1,
   extend_tuples_by_numbers(M, Ps, Qs).
extend_tuples_by_numbers(_, [], []).


/******************************************************************/


/* point_to_reals(Point, Reals) <-
      */

point_to_reals(Point, Point) :-
   !.

point_to_reals([center|Xs], [0|Xs]) :-
   !.
point_to_reals(Point, Reals) :-
   maplist( value_to_real,
      Point, Xs ),
   flatten(Xs, Reals).

value_to_real(X, Y) :-
   member(X-Y, [yes-1, no-0, 'M'-0, 'F'-2]),
   !.
% value_to_real(X, Y) :-
%    sqrt(2, S),
%    member(X-Y, [1-[S,0,0], 4-[0,S,0], 5-[0,0,S]]),
%    !.
value_to_real(X, Y) :-
   number(X),
   1900 < X, X < 2000,
   !,
   Y is (X - 1900) / 10.
value_to_real(X, Y) :-
   number(X),
   10 < X, X < 200,
   !,
   Y is X / 80.
value_to_real(X, Y) :-
   number(X),
   10000 < X, X < 100000,
   !,
   Y is X / 10000.
value_to_real(X, 0) :-
   number(X),
   X > 100000000,
   !.
value_to_real(X, X) :-
   number(X),
   !.
value_to_real(_, 0).


/*** tests ********************************************************/


test(clustering, credit(Clusters)) :-
   data_mining_table(credit, Attributes, _),
   data_mining_test_clustering(credit, Clusters_T),
   !,
   k_means_clustering(Clusters_T, Clusters),
   clusters_to_xpce_table(Attributes, Clusters).

test(clustering, credit:1) :-
   k_means_clustering_to_xpce_table(
      centroid, credit, 3, randoms, [0,1,1,1,1,1]).
test(clustering, age_grade:1) :-
   k_means_clustering_to_xpce_table(
      centroid, age_grade, 3, randoms, [0,0,1,1]).
test(clustering, age_grade:2) :-
   k_means_clustering_to_xpce_table(
      center_of_gravity, age_grade, 3, randoms, [0,0,1,1]).
test(clustering, employee:1) :-
   k_means_clustering_to_xpce_table(
      centroid, employee_short, 3, randoms,
      [0,0,0,0,0,0.01,0,100000]).
test(clustering, employee:2) :-
   k_means_clustering_to_xpce_table(
      centroid, employee_short, 3, [2,4,5],
      [0,0,0,0,0,0.001,0,100]).
test(clustering, employee:3) :-
   k_means_clustering_to_xpce_table(
      centroid, employee, 3, randoms,
      [0,0,0,0,0,0,0,100000,0.01,0,100000]).


/******************************************************************/


