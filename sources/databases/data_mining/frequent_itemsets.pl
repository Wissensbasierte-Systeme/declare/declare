

/******************************************************************/
/***                                                            ***/
/***            Data Mining: Frequent Itemsets                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* association_rule_to_frequency_and_confidence(
         Table, Rule, F, C) <-
      */

association_rule_to_frequency_and_confidence(
      Transactions, Rule, F:C:Rule) :-
   Rule = (As-->Bs),
   itemset_to_frequency(Transactions, As, X),
   ord_union(As, Bs, Cs),
   itemset_to_frequency(Transactions, Cs, F),
   round(F/X, 2, C).


/* association_rules(Transactions, Freq, Conf, Rules) <-
      */

association_rules(Transactions, Freq, Conf, Rules) :-
   a_priori_algorithm(Transactions, Freq, Max_Sets),
   findall( Rule,
      ( member(Set, Max_Sets),
        itemset_to_association_rule(
           Transactions, Freq, Conf, Set, Rule) ),
      Rules_2 ),
   sort(Rules_2, Rules_3),
   writeln('association rules'),
   writeln_list(Rules_3),
   association_rules_prune(Rules_3, Rules),
   writeln('maximal association rules'),
   writeln_list(Rules).


/* a_priori_algorithm(Transactions, Min, Max_Sets) <-
      */

a_priori_algorithm(Transactions, Min, Max_Sets) :-
   transactions_to_one_itemsets(Transactions, One_Sets),
   combine_itemsets_loop(
      Transactions, Min, Max_Sets, One_Sets, [[]], []),
   ( foreach(X, Max_Sets), foreach(Y, Ys) do
        itemset_to_frequency(Transactions, X, F),
        Y = F:X ),
   sort(Ys, Max_Sets_with_Frequency),
   writeln('maximal itemsets'),
   writeln_list(Max_Sets_with_Frequency).


/* combine_itemsets_loop(
         Ts, Min, Max_Sets, One_Sets, Sets_1, Sets_2) <-
      */

combine_itemsets_loop(_, _, [], _, [], []) :-
   !.
combine_itemsets_loop(
      Ts, Min, Max_Sets, One_Sets, Sets_1, Sets_2) :-
   length(Sets_1, N),
   first(Sets_1, Set),
   length(Set, M), 
   write_list([N, ' itemsets of the length ', M, '\n']),
   write_list(Sets_1), nl,
   combine_itemsets(Ts, Min, One_Sets, Sets_1, Sets_3),
   sort(Sets_3, Sets_4),
   findall( X,
      ( member(X, Sets_1),
        \+ ( member(Y, Sets_4), ord_subset(X, Y) ) ),
      Max_Sets_1 ),
   combine_itemsets_loop(
      Ts, Min, Max_Sets_2, One_Sets, Sets_4, Sets_2),
   append(Max_Sets_1, Max_Sets_2, Max_Sets_3),
   sort(Max_Sets_3, Max_Sets).
   
combine_itemsets(
      Ts, Min, One_Item_Sets, Item_Sets_1, Item_Sets_2) :-
   findall( Ys,
      ( member(Xs, Item_Sets_1),
        member([X], One_Item_Sets),
        \+ member(X, Xs),
        sort([X|Xs], Ys),
        itemset_to_frequency(Ts, Ys, F),
        F >= Min ),
      Item_Sets_2 ).


/* transactions_to_one_itemsets(Transactions, One_Sets) <-
      */

transactions_to_one_itemsets(Transactions, One_Sets) :-
   append(Transactions, Xs),
   sort(Xs, Ys),
   elements_to_lists(Ys, One_Sets).


/* itemset_to_frequency(Transactions, Itemset, Frequency) <-
      */

itemset_to_frequency(Transactions, Itemset, Frequency) :-
   ddbase_aggregate( [length(Transaction)],
      ( member(Transaction, Transactions),
        subset(Itemset, Transaction) ),
      [[Frequency]] ).

itemset_to_frequency_(Transactions, Itemset, Frequency) :-
   findall( Transaction,
      ( member(Transaction, Transactions),
        ord_subset(Itemset, Transaction) ),
      Xs ),
   length(Xs, Frequency).


/* association_rule_to_frequency(
         Transactions, As-->Bs,  Frequency) <-
      */

association_rule_to_frequency(Transactions, As-->Bs,  Frequency) :-
   append(As, Bs, Cs),
   sort(Cs, Ds),
   itemset_to_frequency(Transactions, Ds, Frequency).


/* association_rule_to_confidence(
         Transactions, As-->Bs, Confidence) <-
      */

association_rule_to_confidence(Transactions, As-->Bs, Conf) :-
   itemset_to_frequency(Transactions, As, F_As),
   append(As, Bs, Cs),
   sort(Cs, Ds),
   itemset_to_frequency(Transactions, Ds, F_Ds),
   Conf is F_Ds / F_As.


/* itemset_to_association_rule(
         Ts, Freq, Conf, Set, F:C:(As-->Bs)) <-
      */

itemset_to_association_rule(Ts, Freq, Conf, Set, F:C:(As-->Bs)) :-
   itemset_to_association_rule(Set, As-->Bs),
   association_rule_to_frequency(Ts, As-->Bs, F),
   F >= Freq, 
   association_rule_to_confidence(Ts, As-->Bs, C),
   C >= Conf.


/* itemset_to_association_rule(Set, As-->Bs) <-
      */

itemset_to_association_rule(Set, As-->Bs) :-
   generate_subsets_2(Set, Sets_1),
   member(As, Sets_1),
   ord_subtract(Set, As, Cs),
   generate_subsets(Cs, Sets_2),
   member(Bs, Sets_2).

generate_subsets_2(Set, [[]|Sets]) :-
   generate_subsets(Set, Sets).


/* table_to_transactions(Table, Transactions) <-
      */

table_to_transactions(Table, Transactions) :-
   foreach(Row, Table), foreach(Transaction, Transactions) do
      findall( N:Value,
         nth(N, Row, Value),
         Transaction ).


/* transactions_to_table(Transactions, Attributes, Rows) <-
      */

transactions_to_table(Transactions, Attributes, Rows) :-
   append(Transactions, List),
   list_to_ord_set(List, Attributes),
   ( foreach(T, Transactions), foreach(R, Rows) do
        ( foreach(A, Attributes), foreach(V, R) do
             ( member(A, T) -> V = A
             ; V = '-' ) ) ).


/******************************************************************/


