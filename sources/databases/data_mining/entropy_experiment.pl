

/******************************************************************/
/***                                                            ***/
/***            Data Mining: Entropy Experiment                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* entropy_experiment(N*M, [I,J]) <-
      */

entropy_experiment(N*M, [I,J]) :-
   forall( matrix_generate(N*M, [I,J], Matrix),
      ( matrix_to_entropies(Matrix, Entropy, Entropy_s),
        ( Entropy_s > Entropy ->
          Diff is Entropy_s - Entropy,
          Example = (Matrix:Diff=Entropy -> Entropy_s),
          writeln(user, Example),
          assert(entropy_difference_found(Example))
        ; true ) ) ).

matrix_to_entropies(Matrix, Entropy, Entropy_s) :-
   maplist( sum,
      Matrix, Ms ),
   sum(Ms, N),
   entropy(N, Ms, Entropy),
   findall( X,
      ( matrix_to_colomn(_, Matrix, Column),
        sum(Column, S),
        entropy(S, Column, E),
        X is S/N * E ),
      Xs ),
   sum(Xs, Entropy_s).


/******************************************************************/


