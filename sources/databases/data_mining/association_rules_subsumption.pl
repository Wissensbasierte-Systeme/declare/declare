

/******************************************************************/
/***                                                            ***/
/***        Data Mining: Association Rules - Subsumption        ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/



/*** interface ****************************************************/


/* association_rules_prune(Rules_1, Rules_2) <-
      */

association_rules_prune(Rules_1, Rules_2) :-
   findall( Rule,
      ( member(Rule, Rules_1),
        \+ association_rules_subsume(Rules_1, Rule) ),
      Rules_2 ).


/* association_rules_subsume(Rules, Rule) <-
      */

association_rules_subsume(Rules, Rule) :-
   member(Rule_2, Rules),
   Rule_2 \= Rule,
   association_rule_subsumes(Rule_2, Rule).


/* association_rule_subsumes(Rule_1, Rule_2) <-
      */

% association_rule_subsumes(_, _) :-
%    !,
%    fail.
association_rule_subsumes(Rule_1, Rule_2) :-
   Rule_1 = F1:C1:(As1-->Bs1),
   Rule_2 = F2:C2:(As2-->Bs2),
   ord_subset(As1, As2),
   ord_subset(Bs2, Bs1),
   F1 >= F2,
   C1 >= C2.
%  C1 is 1.


/******************************************************************/


