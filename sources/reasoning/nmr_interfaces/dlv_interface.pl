

/******************************************************************/
/***                                                            ***/
/***         DisLog:  Interface to DLV                          ***/
/***                                                            ***/
/******************************************************************/


:- op(700, xfx, '!=').


/*** interface ****************************************************/


/* program_to_definite_consequences_dlv(Program, State) <-
      */

program_to_definite_consequences_dlv(Program, State) :-
   program_to_stable_models_dlv(Program, Stable_Models),
   length(Stable_Models, N),
   write_list(['stable models: ', N]), nl,
   tree_based_boolean_dualization(Stable_Models, State_2),
   list_to_ord_set(State_2, State).


/* prolog_program_to_stable_models_dlv(Prolog, Stable_Models) <-
      */

prolog_program_to_stable_models_dlv(Prolog, Stable_Models) :-
   prolog_rules_to_dislog_rules(Prolog, DisLog),
   program_to_stable_models_dlv(DisLog, Stable_Models).


/* program_to_stable_models_dlv(Program, Stable_Models) <-
      */

program_to_stable_models_dlv(Program, Stable_Models) :-
   member(Rule, Program),
   parse_dislog_rule(Rule, [], [], []),
   !,
   Stable_Models = [].
program_to_stable_models_dlv(Program, Stable_Models) :-
   dislog_file_get(home,'/results/dlv_program', Path_I),
   dislog_file_get(home,'/results/dlv_result', Path_O),
   dislog_program_to_dlv_pragram(Program, Program_Dlv),
   write_dislog_program_to_dlv_program_file(
      Program_Dlv, Path_I),
   program_to_stable_models_dlv_file(Path_I, Path_O),
   dlv_file_to_stable_models(Path_O, Stable_Models).

dlv_file_to_stable_models(Path_O, Stable_Models) :-
   dlv_file_add_dot_at_end_of_all_lines(Path_O),
   read_models_from_file(Path_O, Stable_Models_Dlv),
   dislog_program_to_dlv_pragram(
      Stable_Models, Stable_Models_Dlv).

dlv_file_add_dot_at_end_of_all_lines(Path) :-
   !,
   dislog_variable_get(source_path,
      'reasoning/nmr_interfaces/add_dot_at_end_of_all_lines', C),
   concat(['cat ', C, ' | vi ', Path], Command),
   us(Command).
dlv_file_add_dot_at_end_of_all_lines(Path) :-
   read_file_to_string(Path, String_1),
   list_exchange_sublist([["\n",".\n"]], String_1, String_2),
   name(Name, String_2),
   predicate_to_file( Path,
      write(Name) ).

program_to_stable_models_dlv_file(Path_I, Path_O) :-
   home_directory(Home),
   concat([Home, '/soft/dlv/dlv.i386-linux-elf-gnulibc2.bin ',
      '-silent '], C1),
   concat(Path_I, ' > ', C2),
   concat([C1, C2, Path_O], Command),
   write('calling dlv ... '),
%  measure_until_measurable(us(Command)),
   write_list(['\n', C1, '\n   ', C2, '\n   ', Path_O, '\n']),
   measure(us(Command)),
   nl.

dislog_file_get(X, Y, Value) :-
   dislog_variable_get(X, Z),
   name_append(Z, Y, Value).


/* extended_program_to_stable_models_dlv(Program, Models) <-
      */

extended_program_to_stable_models_dlv(Program, Models) :-
   program_to_stable_models_dlv(Program, Models_2),
   maplist( dlv_model_to_extended_model,
      Models_2, Models ),
   !.

dlv_model_to_extended_model(Model_1, Model_2) :-
   maplist( dlv_atom_to_extended_atom,
      Model_1, Model_2 ).

dlv_atom_to_extended_atom(Atom_1, ~Atom_2) :-
   Atom_1 =.. [P1|As],
   concat(not_, P2, P1),
   !,
   Atom_2 =.. [P2|As].
dlv_atom_to_extended_atom(Atom, Atom).


/*** implementation ***********************************************/


/* write_dislog_program_to_dlv_program_file(Program, File) <-
      */

write_dislog_program_to_dlv_program_file(Program, File) :-
   predicate_to_file( File,
      write_dislog_program_to_dlv_program(Program) ).

write_dislog_program_to_dlv_program(Program) :-
   maplist( copy_term,
      Program, Program_2 ),
   checklist( write_dislog_rule_to_dlv_rule,
      Program_2 ).


/* write_dislog_rule_to_dlv_rule(Rule) <-
      */

write_dislog_rule_to_dlv_rule(Rule) :-
   parse_dislog_rule(Rule, As, Bs, Cs),
   listvars(Rule,1),
   write_list_with_separators(
      write_atom_with_v, write_atom, As ),
   ( Bs = [], Cs = [],
     writeln('.')
   ; write_dislog_body_to_dlv_body(Bs, Cs) ).

write_dislog_body_to_dlv_body(Bs, Cs) :-
   writeln(' :-'),
   ( Bs \= [] ->
     tab(3),
     write_list_with_separators(
        write_atom_with_comma, write_atom,
        Bs ),
     ( Cs = [] ->
       writeln('.')
     ; writeln(',') )
   ; true ),
   ( Cs \= [] ->
     smodels_not_body(Cs, Cs_2),
     tab(3),
     write_list_with_separators(
        write_atom_with_comma, write_atom,
        Cs_2 ),
     writeln('.')
   ; true ).

write_atom_with_v(Atom) :-
   write_extended_atom(Atom), write(' v ').

write_atom_with_comma(Atom) :-
   write_extended_atom(Atom), write(',').

write_atom_with_point(Atom) :-
   write_extended_atom(Atom), write('.').

write_atom(Atom) :-
   write_extended_atom(Atom).

write_extended_atom(~A) :-
   write_list(['not_', A]).
write_extended_atom(A) :-
   write(A).


/* read_models_from_file(File, Models) <-
      */

read_models_from_file(File, Models) :-
   read_terms_from_file(File, Terms),
   !,
   maplist( set_structure_to_list,
      Terms, Models ).
   
read_terms_from_file(File, Terms) :-
   see(File),
   read_terms_from_file(Terms),
   seen.

read_terms_from_file(Terms) :-
   read(Term),
   ( Term = end_of_file,
     Terms = []
   ; read_terms_from_file(Terms_2),
     Terms = [Term|Terms_2] ).


/* set_structure_to_list(Set_Structure, List) <-
      */

set_structure_to_list({X,Xs},[X|Ys]) :-
   comma_structure_to_ord_set(Xs,Ys).
set_structure_to_list({X},[X]).
set_structure_to_list({},[]).


/* dislog_program_to_dlv_pragram(Program_1, Program_2) <-
      */

dislog_program_to_dlv_pragram(Program_1, Program_2) :-
   maplist( dislog_rule_to_dlv,
      Program_1, Program_2 ),
   !.

dislog_rule_to_dlv(Rule_1, Rule_2) :-
   var(Rule_2),
   !,
   parse_dislog_rule(Rule_1, As1, Bs1, Cs1),
%  ( As1 = [A1], A1 = (_;_) ->
%    semicolon_structure_to_list(A1, As3)
%  ; As3 = As1 ), 
   dislog_atoms_to_dlv_atoms(As1, As2),
   dislog_atoms_to_dlv_atoms(Bs1, Bs2),
   dislog_atoms_to_dlv_atoms(Cs1, Cs2),
   Rule_2 = As2-Bs2-Cs2.
dislog_rule_to_dlv(Rule_1, Rule_2) :-
   var(Rule_1),
   parse_dislog_rule(Rule_2, As2, Bs2, Cs2),
   dislog_atoms_to_dlv_atoms(As1, As2),
   dislog_atoms_to_dlv_atoms(Bs1, Bs2),
   dislog_atoms_to_dlv_atoms(Cs1, Cs2),
   simplify_rule(As1-Bs1-Cs1, Rule_1).


/* dislog_atoms_to_dlv_atoms(As1, As2) <-
      */

dislog_atoms_to_dlv_atoms(As1, As2) :-
   maplist( dislog_atom_to_dlv_atom,
      As1, As2 ).

dislog_atom_to_dlv_atom(~A1, ~A2) :-
   !,
   dislog_atom_to_dlv_atom(A1, A2).
dislog_atom_to_dlv_atom(-A1, -A2) :-
   !,
   dislog_atom_to_dlv_atom(A1, A2).
dislog_atom_to_dlv_atom(A1, A2) :-
   var(A2), A1 = X1-Y1,
   !,
   dislog_atom_to_dlv_atom(X1, X2),
   dislog_atom_to_dlv_atom(Y1, Y2),
   A2 = minus(X2, Y2).
dislog_atom_to_dlv_atom(A1, A2) :-
   var(A2),
   !,
   A1 =.. [P|Ts1],
   maplist( dislog_argument_to_dlv_argument,
      Ts1, Ts2 ),
   A2 =.. [P|Ts2].
dislog_atom_to_dlv_atom(A1, A2) :-
   var(A1), A2 = minus(X2, Y2),
   !,
   dislog_atom_to_dlv_atom(X1, X2),
   dislog_atom_to_dlv_atom(Y1, Y2),
   A1 = X1-Y1.
dislog_atom_to_dlv_atom(A1, A2) :-
   var(A1),
   A2 =.. [P|Ts2],
   maplist( dislog_argument_to_dlv_argument,
      Ts1, Ts2 ),
   A1 =.. [P|Ts1].


/* dislog_argument_to_dlv_argument(X, Y) <-
      */

dislog_argument_to_dlv_argument(X, Y) :-
   var(Y),
   atomic(X),
   !,
   ( concat(constant_, X, Z)
   ; X = Z ),
   Substitution = [
      [" ", "_"], ["-", "_"], ["/", "_"],
      [".", "_"], [":", "_"] ],
   name_exchange_sublist(Substitution, Z, Y).
dislog_argument_to_dlv_argument(X, Y) :-
   var(X),
   atomic(Y),
   !,
   ( concat(constant_, X, Y)
   ; X = Y ).
dislog_argument_to_dlv_argument(X, X).


/*** tests ********************************************************/


test(nmr:dislog_dlv, 1) :-
   dconsult('nmr/path_blocked/pb1', Program),
   program_to_stable_models_dlv(Program, Models),
   dportray(chs, Models).
test(nmr:dislog_dlv, 2) :-
   Program = [[a,c]-[b], [b,d]],
   dportray(lp, Program),
   program_to_stable_models_dlv(Program, Models),
   write('models ='), dportray(chs, Models),
   program_to_definite_consequences_dlv(Program, State),
   write('state ='), dportray(dhs, State).


/******************************************************************/


