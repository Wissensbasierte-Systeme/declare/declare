

/******************************************************************/
/***                                                            ***/
/***           DDBase: Semi-Stratified Evaluation               ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


/* evaluation_semi_stratified(Prolog, Models) <-
      */

evaluation_semi_stratified :-
   File = 'examples/nmr/graph_problems/graph_col_aggregate',
   dread(pl, File, Prolog), 
   evaluation_semi_stratified(Prolog, Models),
   state_to_tree(Models, Tree),
   !,
   set_tree_to_picture_dfs(Tree, _Picture).

evaluation_semi_stratified(Prolog, Models) :-
   gensym(evaluation_semi_stratified_, Module),
   writeln(user, 'Module'=Module),
   prolog_program_dynamize_predicate_symbols(Module, Prolog),
%  prolog_program_execute_dynamics(Prolog, Module, Prolog_2),
   prolog_program_ddbase_connect(Prolog, Module),
   prolog_program_ddbase_connect_rules(Prolog, Rules_c),
   prolog_program_stratify_rules(Prolog, Rules_s),
   prolog_program_without_ddbase_connect(Prolog, Prolog_2),
   prolog_program_without_stratify(Prolog_2, Prolog_3),
   append([Prolog_3, Rules_c, Rules_s], Prolog_4),
   prolog_rules_to_partition_no_stratification(
      Prolog_4, Partition),
   star_line,
   writeln(user, 'Partition'),
   star_line,
   writeln_list(user, Partition),
   append([Prolog_3, Rules_c], Prolog_5),
   prolog_program_semi_stratify(Prolog_5, [], Partition, Strata),
   star_line,
   writeln(user, 'Strata'),
   star_line,
   maplist( dwrite(pl, user), Strata ),
   evaluation_semi_stratified(Module, Strata, [], Models).

evaluation_semi_stratified(Module, [P|Ps], I, Models) :-
   ( ( prolog_program_is_indefinite(P)
     ; \+ prolog_program_is_stratisfiable(P) ) ->
     append(P, I, P_),
     prolog_program_to_stable_models_dlv(P_, Ms1)
   ; tp_iteration_prolog(P, Module, I, M),
     Ms1 = [M] ),
   ( foreach(M1, Ms1), foreach(Ms2, Mss2) do
        evaluation_semi_stratified(Module, Ps, M1, Ms2) ),
   append(Mss2, Ms2),
   Models = Ms2,
   !.
evaluation_semi_stratified(_, [], I, [I]).


/******************************************************************/


