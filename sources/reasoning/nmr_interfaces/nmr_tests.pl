

/******************************************************************/
/***                                                            ***/
/***          DisLog:  NMR Tests                                ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


/* test(nmr_manual:states, Y) <-
      */

test(nmr_manual:states, state_prune) :-
   state_prune([[a,b], [a,b], [b,c]], State_2),
   !,
   State_2 = [[a,b], [b,c]].

test(nmr_manual:states, state_subsumes) :-
   state_subsumes([[a], [b,c]], [[a,b], [b,c,d]]).

test(nmr_manual:states, state_subtract) :-
   state_subtract([[a,b], [b,d]], [[a],[b,c]], State_3),
   !,
   State_3 = [[b,d]].

test(nmr_manual:states, state_can) :-
   state_can([[a], [a,b], [a,b,c], [b,d]], State_2),
   !,
   State_2 = [[a], [b,d]].

test(nmr_manual:states, state_disjunction) :-
   state_disjunction([[a], [b]], [[b,c], [b,d]], State_3),
   !,
   State_3 = [[b,c], [b,d], [a,b,c], [a,b,d]].
 
test(nmr_manual:states, state_resolve) :-
   state_resolve([[a,b,d], [a,b,e], [a,c]], b, State_2),
   !,
   State_2 = [[a,d], [a,e]].
 
test(nmr_manual:states, state_reduce) :-
   state_reduce([[a,b,d], [a,b,e], [a,c]], b, State_2),
   !,
   State_2 = [[a,c]].


/* test(nmr_manual:states_and_coins, Y) <-
      */

test(nmr_manual:states_and_coins, state_to_coin) :-
   state_to_coin([[a,b], [b,c], [d]], Coin),
   !,
   Coin = [[a,c,d], [b,d]].

test(nmr_manual:states_and_coins, coin_to_state) :-
   coin_to_state([[a,c,d], [b,d]], State),
   !,
   State = [[a,b], [b,c], [d]].


/* test(nmr_manual:state_trees, Y) <-
      */

test(nmr_manual:state_trees, state_to_tree) :-
   State = [[a,b,d], [a,b,e], [a,c]],
   state_to_tree(State, Tree),
   !,
   Tree = [[a],[[c]],[[b],[[e]],[[d]]]].

test(nmr_manual:state_trees, tree_to_state) :-
   Tree = [[a], [[b], [[d]], [[e]]], [[c]]],
   tree_to_state(Tree, State),
   !,
   State = [[a,b,d], [a,b,e], [a,c]].
 
test(nmr_manual:state_trees, tree_subsumes_clause) :-
   Tree = [[a], [[b], [[d]], [[e]]], [[c]]],
   Clause = [a,b,c],
   tree_subsumes_clause(Tree, Clause),
   !.

test(nmr_manual:state_trees, tree_subsumes_state) :-
   Tree = [[a], [[b], [[d]], [[e]]], [[c]]],
   State = [[a,b,d,e], [a,b,c]],
   tree_subsumes_state(Tree, State),
   !.

test(nmr_manual:state_trees, state_tree_subtract) :-
   State_1 = [[a,d], [a,b,c]],
   Tree = [[a], [[b], [[d]], [[e]]], [[c]]],
   state_tree_subtract(State_1, Tree, State_2),
   !,
   State_2 = [[a,d]].

test(nmr_manual:state_trees, tree_state_subtract) :-
   Tree_1 = [[a], [[d]], [[b,c]]],
   State = [[a,b,d], [a,b,e], [a,c]],
   tree_state_subtract(Tree_1, State, Tree_2),
   !,
   Tree_2 = [[a,d]].

test(nmr_manual:state_trees, tree_resolve) :-
   Tree_1 = [[a], [[b], [[d]], [[e]]], [[c]]],
   tree_resolve(Tree_1, b, Tree_2),
   !,
   Tree_2 = [[a], [[], [[d]], [[e]]]].

test(nmr_manual:state_trees, tree_reduce) :-
   Tree_1 = [[a], [[b], [[d]], [[e]]], [[c]]],
   tree_reduce(Tree_1, b, Tree_2),
   !,
   Tree_2 = [[a], [[c]]].
 

/* test(nmr_manual:tps_operator, Y) <-
      */

test(nmr_manual:tps_operator, tps_operator) :-
   Program = [[c]-[a], [a,d]-[c]],
   State_1 = [[a,b,c], [a,e], [b,d]],
   tps_operator(Program, State_1, State_2),
   !,
   State_2 = [[a,b,d], [b,c], [c,e]].

test(nmr_manual:tps_operator, tps_operator_dc) :-
   Program = [[c]-[a],[a,d]-[c]],
   State_1 = [[a,b,c], [a,e], [b,d]],
   tps_operator_dc(Program, State_1, State_2),
   !,
   State_2 = [[b,c], [c,e]].
 
test(nmr_manual:tps_operator, tps_delta_operator) :-
   Program = [[a]-[b,c]],
   State_1 = [[b,d], [c,e], [d,g]],
   State_2 = [[b,f], [c,g]],
   tps_delta_operator(Program, State_1, State_2, State_3),
   !,
   State_3 = [[a,d,g], [a,e,f]].
 
test(nmr_manual:tps_operator, tps_delta_operator_dc) :-
   Program = [[a]-[b,c]],
   State_1 = [[b,d], [c,e], [d,g]],
   State_2 = [[b,f], [c,g]],
   tps_delta_operator_dc(Program, State_1, State_2, State_3),
   !,
   State_3 = [[a,e,f]].
 
test(nmr_manual:tps_operator, tree_tps_operator) :-
   Program = [[c]-[a], [a,d]-[c]],
   Tree_1 = [[], [[b,d]], [[a], [[e]], [[b,c]]]],
   tree_tps_operator(Program, Tree_1, Tree_2),
   !,
   Tree_2 = [[], [[c,e]], [[b], [[c]], [[a,d]]]].

test(nmr_manual:tps_operator, tree_tps_operator_dc) :-
   Program = [[c]-[a], [a,d]-[c]],
   Tree_1 = [[], [[b,d]], [[a], [[e]], [[b,c]]]],
   tree_tps_operator_dc(Program, Tree_1, Tree_2),
   !,
   Tree_2 = [[c], [[e]], [[b]]].
 
test(nmr_manual:tps_operator, tree_tps_delta_operator) :-
   Program = [[a]-[b,c]],
   Tree_1 = [[], [[c,e]], [[d], [[g]], [[b]]]],
   Tree_2 = [[], [[c,g]], [[b,f]]],
   tree_tps_delta_operator(Program, Tree_1, Tree_2, Tree_3),
   !,
   Tree_3 = [[a], [[e,f]], [[d,g]]].

test(nmr_manual:tps_operator, tree_tps_delta_operator_dc) :-
   Program = [[a]-[b,c]],
   Tree_1 = [[], [[c,e]], [[d], [[g]], [[b]]]],
   Tree_2 = [[], [[c,g]], [[b,f]]],
   tree_tps_delta_operator_dc(Program, Tree_1, Tree_2, Tree_3),
   !,
   Tree_3 = [[a,e,f]].
 
test(nmr_manual:tps_operator, tps_fixpoint) :-
   Program = [[c]-[a], [a,d]-[c]],
   State_1 = [[a,b,c], [a,e]],
   tps_fixpoint(Program, State_1, State_2),
   !,
   State_2 = [[a,b,d], [a,e], [b,c], [c,e]].
 
/*
test(nmr_manual:tps_operator, tps_fixpoint_cc) :-
   Program = [[c]-[a], [a,d]-[c]],
   State_1 = [[a,b,c], [a,e]],
   tps_fixpoint_cc(Program, State_1, State_2),
   !,
   State_2 = [[a,b,d], [a,e], [b,c], [c,e]].
*/

test(nmr_manual:tps_operator, tree_tps_fixpoint) :-
   Program = [[c]-[a], [a,d]-[c]],
   Tree_1 = [[a], [[b,c]], [[e]]],
   tree_tps_fixpoint(Program, Tree_1, Tree_2),
   !,
   Tree_2 = [[], [[b], [[a,d]], [[c]]], [[e], [[c]], [[a]]]].

test(nmr_manual:tps_operator, tps_fixpoint_iteration) :-
   Program = [[c]-[a], [a,d]-[c], [a,b,c], [a,e]],
   tps_fixpoint_iteration(Program, State),
   !,
   State = [[a,b,d], [a,e], [b,c], [c,e]].
 

/* test(nmr_manual:tpi_operator, Y) <-
      */

test(nmr_manual:tpi_operator, tree_tpi_operator) :-
   Program = [[p(X,Y)]-[a(X,Z),p(Z,Y)]],
   Tree_1 = [[p(b,d),p(c,d)], [[a(a,b)]], [[a(a,c)]]],
   tree_tpi_operator(Program, Tree_1, Tree_2),
   !,
   Tree_2 = [[p(b,d),p(c,d)], [[a(a,b),p(a,d)]], [[a(a,c),p(a,d)]]].

test(nmr_manual:tpi_operator, tree_tpi_operator_ground) :-
   Program = [
      [p(a,d)]-[a(a,b),p(b,d)],
      [p(a,d)]-[a(a,c),p(c,d)] ],
   Tree_1 = [[p(b,d),p(c,d)], [[a(a,b)]], [[a(a,c)]]],
   tree_tpi_operator_ground(Program, Tree_1, Tree_2),
   !,
   Tree_2 = [[p(b,d),p(c,d)], [[a(a,b),p(a,d)]], [[a(a,c),p(a,d)]]].

test(nmr_manual:tpi_operator, tree_tpi_delta_operator) :-
   Program = [[d,e]-[a,c], [b,d,f]-[a,c,e]],
   Tree_1 = [[c], [[b]]],
   Delta_1 = [[c], [[a], [[d]], [[e]]]],
   tree_tpi_delta_operator(Program, Tree_1, Delta_1, Tree_2, Delta_2),
   !,
   Tree_2 = [[c], [[b]], [[a,d]]],
   Delta_2 = [[a,c,e,f]].
 
test(nmr_manual:tpi_operator, tree_tpi_fixpoint) :-
   Program = [[d,e]-[a,c], [b,d,f]-[a,c,e]],
   Tree_1 = [[c], [[a]], [[b]]],
   tree_tpi_fixpoint(Program, Tree_1, Tree_2),
   !,
   Tree_2 = [[c], [[b]], [[a,d]], [[a,e,f]]].

test(nmr_manual:tpi_operator, tpi_fixpoint_iteration) :-
   Program = [[c]-[a], [a,d]-[c], [a,b,c], [a,e]],
   tpi_fixpoint_iteration(Program, Models),
   !,
   Models = [[a,c], [b,e], [c,d,e]].


/* test(nmr_manual:program_transformations, Y) <-
      */

test(nmr_manual:program_transformations, partial_evaluation) :-
   Program_1 = [
      [tc(X,Y)]-[arc(X,Y)]-[blocked(X,Y)],
      [tc(X,Z)]-[tc(X,Y),arc(Y,Z)]-[blocked(Y,Z)],
      [arc(a,a),arc(a,b)],
      [blocked(b,b)] ],
   partial_evaluation(Program_1, Program_2),
   !,
   Program_2 = [
      [tc(a,a),tc(a,b)]-[]-[blocked(a,a),blocked(a,b)],
      [arc(a,b),tc(a,a)]-[]-[blocked(a,a)],
      [arc(a,a),tc(a,b)]-[]-[blocked(a,b)],
      [arc(a,a),arc(a,b)]-[]-[],
      [blocked(b,b)]-[]-[] ].
 
test(nmr_manual:program_transformations, gl_transformation_non_ground) :-
   Program_1 = [
      [tc(X,Y)]-[arc(X,Y)]-[blocked(X,Y)],
      [tc(X,Z)]-[tc(X,Y),arc(Y,Z)]-[blocked(Y,Z)],
      [arc(a,a),arc(a,b)],
      [blocked(b,b)] ],
   I = [arc(a,b), blocked(a,a), blocked(b,a), blocked(b,b)],
   gl_transformation_non_ground(Program_1, I, Program_2),
   !,
   Program_2 = [
      [tc(a,b)]-[arc(a,b)],[tc(a,b)]-[tc(a,a),arc(a,b)],
      [tc(b,b)]-[tc(b,a),arc(a,b)],
      [arc(a,a),arc(a,b)],
      [blocked(b,b)] ].
 
test(nmr_manual:program_transformations, gl_transformation) :-
   Program_1 = [
      [p(a)]-[]-[q(a)], [p(b)]-[]-[q(b)], [q(a),q(b)] ],
   I = [p(c),q(a)],
   gl_transformation(Program_1, I, Program_2),
   !,
   Program_2 = [[p(b)], [q(a),q(b)]].
 
test(nmr_manual:program_transformations, evidential_transformation) :-
   Program_1 = [[a,b]-[c]-[d], [d]-[]-[a]],
   evidential_transformation(Program_1, Program_2),
   !,
   Program_2 = [ [a,b,'E' d]-[c], [d,'E' a] ].

test(nmr_manual:program_transformations, stratify_program) :-
   Program = [[p]-[]-[a], [q]-[]-[b], [a,b]],
   stratify_program(Program, Programs),
   !,
   Programs = [ [[a,b]], [[q]-[]-[b]], [[p]-[]-[a]] ].

test(nmr_manual:program_transformations, tu_transformation) :-
   Program_1 = [[a,b]-[c]-[d], [d]-[]-[a]],
   tu_transformation(Program_1, Program_2),
   !,
   Program_2 = [
      [u(X)]-[t(X)],
      [t(a),t(b)]-[t(c)]-[u(d)], [u(a),u(b)]-[u(c)]-[t(d)],
      [t(d)]-[]-[u(a)], [u(d)]-[]-[t(a)] ].
 

/* test(nmr_manual:minimal_models, Y) <-
      */

test(nmr_manual:minimal_models, minimal_models) :-
   Program = [[c]-[a], [a,d]-[c], [a,b,c], [a,e]],
   minimal_models(Program, Minimal_Models),
   !,
   Minimal_Models = [[a,c], [b,e], [c,d,e]].

test(nmr_manual:minimal_models, minimal_model_state) :-
   Program = [[c]-[a], [a,d]-[c], [a,b,c], [a,e]],
   minimal_model_state(Program, Minimal_Model_State),
   !,
   Minimal_Model_State = [[a,b,d], [a,e], [b,c], [c,e]].

test(nmr_manual:minimal_models, minimal_model_semantics_s) :-
   Program = [[c]-[a], [a,d]-[c], [a,b,c], [a,e]],
   minimal_model_semantics_s(Program, Minimal_State),
   !,
   Minimal_State = [
      [a,b,d], [a,d,~c], [a,e], [b,c], [b,d,~e], [c,e],
      [c,~a], [c,~d], [d,~c,~e], [e,~b], [e,~d], [~a,~b], [~a,~d],
      [~a,~e], [~b,~c], [~b,~d] ].
 
test(nmr_manual:minimal_models, minimal_model_semantics_i) :-
   Program = [[c]-[a], [a,d]-[c], [a,b,c], [a,e]],
   minimal_model_semantics_i(Program, Minimal_State),
   !,
   Minimal_State = [
      [a,b,d], [a,d,~c], [a,e], [b,c], [b,d,~e], [c,e],
      [c,~a], [c,~d], [d,~c,~e], [e,~b], [e,~d], [~a,~b], [~a,~d],
      [~a,~e], [~b,~c], [~b,~d]].


/* test(nmr_manual:perfect_models, Y) <-
      */

test(nmr_manual:perfect_models, perfect_models) :-
   perfect_models([[p]-[]-[a], [p]-[]-[b], [a,b]], Perfect_Models),
   !,
   Perfect_Models = [[a,p], [b,p]].

test(nmr_manual:perfect_models, perfect_model_state) :-
   Program = [[p]-[]-[a], [p]-[]-[b], [a,b]],
   perfect_model_state(Program, Perfect_Model_State),
   !,
   Perfect_Model_State = [[a,b], [p]].
 
test(nmr_manual:perfect_models, perfect_model_semantics) :-
   Program = [[p]-[]-[a], [p]-[]-[b], [a,b]],
   perfect_model_semantics(Program, Perfect_State),
   !,
   Perfect_State = [[a,b], [p], [~a,~b]].


/* test(nmr_manual:stable_models, Y) <-
      */

test(nmr_manual:stable_models, stable_models) :-
   Program = [
      [win(X)]-[move(X,Y)]-[win(Y)],
      [move(a,b)], [move(b,c)] ],
   stable_models(Program, Stable_Models),
   !,
   Stable_Models = [ [win(b),move(a,b),move(b,c)] ].

test(nmr_manual:stable_models, stable_models_normal) :-
   Program = [
      [win(X)]-[move(X,Y)]-[win(Y)],
      [move(a,b)], [move(b,c)], [move(c,d)], [move(d,a)] ],
   stable_models_normal(Program, Stable_Models),
   !,
   Stable_Models = [
      [win(a),win(c),move(a,b),move(b,c),move(c,d),move(d,a)],
      [win(b),win(d),move(a,b),move(b,c),move(c,d),move(d,a)] ].

test(nmr_manual:stable_models, stable_models_normal_e) :-
   Program = [
      [win(X)]-[move(X,Y)]-[win(Y)],
      [move(a,b)], [move(b,c)], [move(c,d)], [move(d,a)] ],
   stable_models_normal_e(Program, [win(a)], Stable_Models),
   !,
   Stable_Models = [
      [win(a),win(c),move(a,b),move(b,c),move(c,d),move(d,a)] ].
 
test(nmr_manual:stable_models, stable_model_state) :-
   Program = [
      [win(X)]-[move(X,Y)]-[win(Y)],
      [move(a,b)], [move(b,c)] ],
   stable_model_state(Program, Stable_Model_State),
   !,
   Stable_Model_State = [[win(b)], [move(a,b)], [move(b,c)]].
 
test(nmr_manual:stable_models, stable_model_semantics) :-
   Program = [
      [win(X)]-[move(X,Y)]-[win(Y)],
      [move(a,b)], [move(b,c)] ],
   stable_model_semantics(Program, Stable_State),
   !,
   Stable_State = [
      [win(b)], [~win(a)], [~win(c)],
      [~move(a,a)], [~move(a,c)], [~move(b,a)], [~move(b,b)],
      [~move(c,a)], [~move(c,b)], [~move(c,c)],
      [move(a,b)], [move(b,c)] ].

test(nmr_manual:stable_models, partial_stable_models) :-
   Program = [
      [win(X)]-[move(X,Y)]-[win(Y)],
      [move(a,b)], [move(b,c)] ],
   partial_stable_models(Program, Partial_Stable_Models),
   !,
   Partial_Stable_Models = [
      [win(b),~win(a),~win(c),
       ~move(a,a),~move(a,c),~move(b,a),~move(b,b),
       ~move(c,a),~move(c,b),~move(c,c),
       move(a,b),move(b,c)] ].

test(nmr_manual:stable_models, evidential_stable_models) :-
   Program = [
      [a,b,c],
      [a]-[]-[b], [b]-[]-[c], [c]-[]-[a] ],
   evidential_stable_models(Program, Models),
   !,
   Models = [
      [t(a),'E' t(c)],
      [t(b),'E' t(a)],
      [t(c),'E' t(b)] ].

test(nmr_manual:stable_models, partial_evidential_stable_models) :-
   Program = [
      [a,b,c],
      [a]-[]-[b], [b]-[]-[c], [c]-[]-[a] ],
   partial_evidential_stable_models(Program, Models),
   !,
   Models = [
      [t(a),u(b),'E' u(c)],
      [t(b),u(c),'E' u(a)],
      [t(c),u(a),'E' u(b)] ].
 

/* test(nmr_manual:cwa, Y) <-
      */

test(nmr_manual:cwa, state_to_egcwa) :-
   State = [[a,b,d], [b,c], [c,d]],
   state_to_egcwa(State, Egcwa),
   !,
   Egcwa = [[a,b], [a,d], [b,c,d]].

test(nmr_manual:cwa, state_to_gcwa) :-
   State = [[a,b], [a,b,c]],
   state_to_gcwa(State, Gcwa),
   !,
   Gcwa = [[c]].

test(nmr_manual:cwa, state_to_sn_sets) :-
   State = [[a,b,d], [b,c], [c,d]],
   state_to_sn_sets(State, Supports),
   !,
   Supports = [
      [[a],[b,d]], [[b],[a,d],[c]],
      [[c],[b],[d]], [[d],[a,b],[c]] ].

test(nmr_manual:cwa, snd_snd_product) :-
   State = [[a,b], [b,c]],
   Supports_1 = [[[a],[b]], [[b],[a],[c]]],
   Supports_2 = [[[c],[b]]],
   snd_snd_product(
      State, Supports_1, Supports_2, Supports_3, Egcwa),
   !,
   Supports_3 = [[[a,c], [b]]],
   Egcwa = [[b,c]].

test(nmr_manual:cwa, egcwa_operator) :-
   Program = [[c]-[a], [a,d]-[c], [a,b,c], [a,e]],
   egcwa_operator(Program, Con_Egcwa),
   !,
   Con_Egcwa = [[a,b], [a,d], [a,e], [b,c], [b,d]].

test(nmr_manual:cwa, gcwa_operator) :-
   Program = [[c]-[a], [a,c]-[d], [d,c]],
   gcwa_operator(Program, Gcwa),
   !,
   Gcwa = [[a], [d]].
 

/* test(nmr_manual:wfs, Y) <-
      */

test(nmr_manual:wfs, sd_operator) :-
   Program = [
      [p]-[q,t], [q]-[]-[a], [t]-[]-[b], [a,b],
      [e]-[p]-[f], [f]-[]-[e] ],
   State_Pair_1 = [[], []],
   sd_operator(Program, State_Pair_1, State_Pair_2),
   !,
   State_Pair_2 = [[[a,b]], []].

test(nmr_manual:wfs, se_operator) :-
   Program = [
      [p]-[q,t], [q]-[]-[a], [t]-[]-[b], [a,b],
      [e]-[p]-[f], [f]-[]-[e] ],
   State_Pair_1 = [[[a,b]], []],
   se_operator(Program, State_Pair_1, State_Pair_2),
   !,
   State_Pair_2 = [
      [[a,b], [a,q], [b,t], [e,f]],
      [[a,q], [b,t], [e,f], [p], [q,t]] ].

test(nmr_manual:wfs, sed_operator) :-
   Program = [
      [p]-[q,t], [q]-[]-[a], [t]-[]-[b], [a,b],
      [e]-[p]-[f], [f]-[]-[e] ],
   State_Pair_1 = [[], []],
   sed_operator(Program, State_Pair_1, State_Pair_2),
   !,
   State_Pair_2 = [
      [[a,b], [a,q], [b,t], [e,f]],
      [[a,q], [b,t], [e,f], [p], [q,t]] ].

test(nmr_manual:wfs, sd_fixpoint) :-
   Program = [
      [p]-[q,t], [q]-[]-[a], [t]-[]-[b], [a,b],
      [e]-[p]-[f], [f]-[]-[e] ],
   State_Pair_1 = [[], []],
   sd_fixpoint(Program, State_Pair_1, State_Pair_2),
   !,
   State_Pair_2 = [[[a,b]], []].

test(nmr_manual:wfs, se_fixpoint) :-
   Program = [
      [p]-[q,t], [q]-[]-[a], [t]-[]-[b], [a,b],
      [e]-[p]-[f], [f]-[]-[e] ],
   State_Pair_1 = [[[a,b]], []],
   se_fixpoint(Program, State_Pair_1, State_Pair_2),
   !,
   State_Pair_2 = [
      [[a,b], [a,q], [b,t], [e,f]],
      [[a,q], [b,t], [e,f], [p], [q,t]] ].

test(nmr_manual:wfs, sed_fixpoint) :-
   Program = [
      [p]-[q,t], [q]-[]-[a], [t]-[]-[b], [a,b],
      [e]-[p]-[f], [f]-[]-[e] ],
   State_Pair_1 = [[], []],
   sed_fixpoint(Program, State_Pair_1, State_Pair_2),
   !,
   State_Pair_2 = [
      [[a,b], [a,q], [b,t], [f]],
      [[a,q], [b,t] ,[e], [p], [q,t]] ].

test(nmr_manual:wfs, dwfs_operator) :-
   Program = [
      [p]-[q,t], [q]-[]-[a], [t]-[]-[b], [a,b],
      [e]-[p]-[f], [f]-[]-[e] ],
   dwfs_operator(Program, State_Pair),
   !,
   State_Pair = [[[a,b]], []].
 
test(nmr_manual:wfs, gdwfs_operator) :-
   Program = [
      [p]-[q,t], [q]-[]-[a], [t]-[]-[b], [a,b],
      [e]-[p]-[f], [f]-[]-[e] ],
   gdwfs_operator(Program, State_Pair),
   !,
   State_Pair = [
      [[a,b], [a,q], [b,t], [f]],
      [[a,q], [b,t], [e], [p], [q,t]] ].

test(nmr_manual:wfs, sb_operator) :-
   Program = [
      [p]-[q,t], [q]-[]-[a], [t]-[]-[b], [a,b],
      [e]-[p]-[f], [f]-[]-[e] ],
   State_1 = [],
   sb_operator(Program, State_1, State_2),
   !,
   State_2 = [
      [a,b], [~a,~b], [~a,~q], [~b,~t], [~e], [~p], [~q,~t] ].
 
test(nmr_manual:wfs, sb_fixpoint) :-
   Program = [
      [p]-[q,t], [q]-[]-[a], [t]-[]-[b], [a,b],
      [e]-[p]-[f], [f]-[]-[e] ],
   State_1 = [],
   sb_fixpoint(Program, State_1, State_2),
   !,
   State_2 = [
      [a,b], [a,q], [a,~t], [b,t], [b,~q], [f],
      [q,t], [q,~b], [t,~a],
      [~a,~b], [~a,~q], [~b,~t], [~e], [~p], [~q,~t] ].

test(nmr_manual:wfs, sb_fixpoint_tree) :-
   Program = [[p]-[]-[a], [p]-[]-[b], [a,b]],
   Tree_1 = [],
   sb_fixpoint_tree(Program, Tree_1, Tree_2),
   !,
   Tree_2 = [[], [[p]], [[~a,~b]], [[a,b]]].

test(nmr_manual:wfs, bdwfs_operator) :-
   Program = [[p]-[]-[a], [p]-[]-[b], [a,b]],
   bdwfs_operator(Program, State),
   !,
   State = [[a,b], [p], [~a,~b]].

test(nmr_manual:wfs, tsb_fixpoint) :-
   Program = [
      [p]-[]-[a], [p]-[]-[b], [a,b] ],
   State_1 = [[~a,~b]],
   tsb_fixpoint(Program, State_1, State_2),
   !,
   State_2 = [[a,b], [p], [~a,~b]].
 
test(nmr_manual:wfs, fsb_fixpoint) :-
   Program = [
      [p]-[]-[a], [p]-[]-[b], [a,b] ],
   State_1 = [[a,b]],
   fsb_fixpoint(Program, State_1, State_2),
   !,
   State_2 = [[~a,~b]].


/* test(nmr_manual:dportray, Y) <-
      */

test(nmr_manual, dportray(dhs)) :-
   dportray(dhs, [[a,b,d], [a,b,e], [a,c]]).

test(nmr_manual, dportray(chs)) :-
   dportray(chs, [[a,b,d], [a,b,e], [a,c]]).

test(nmr_manual, dportray(ghs)) :-
   dportray(ghs, [[c,~d], [a,b], [~e,~f]]).

test(nmr_manual, dportray(lp)) :-
   dportray(lp, [[p]-[q,t], [a,b], [e]-[p]-[f], [f]-[]-[e]]).

test(nmr_manual, dportray(sp)) :-
   dportray(sp, [[[a,b], [a,q]], [[b,t], [p]]]).

test(nmr_manual, dportray(st)) :-
   dportray(st, [[[a,b], [a,q]], [[e], [f]], [[b,t], [p]]]).
 
test(nmr_manual, dportray(tree)) :-
   dportray(tree, [[a,b], [[c], [[d]], [[e]]], [[f]]]).
 
/*
test(nmr_manual, dportray(edge(state))) :-
   dportray(edge(state),
      [[a,b,d], [a,b,e], [a,c]]). 

test(nmr_manual, dportray(edge(tree)))) :-
   dportray(edge(tree),
      [[a], [[c]], [[b], [[e]], [[d]]]]).

test(nmr_manual, dportray(edge(graph)))) :-
   dportray(edge(graph),
      [[a,b], [b,c], [b,d], [d,c]]).

test(nmr_manual, dportray(edge(labeled_graph))) :-
   dportray(edge(labeled_graph),
      [[a,b,1], [b,c,2], [b,d,0], [d,c,8]]).
 
test(nmr_manual, dportray(edge(dependency_graph))) :-
   dportray(edge(dependency_graph),
      [ [a,b,c,d], [[a,b]], [[a,c], [b,c]], [[a,d], [b,d]] ]).
*/
  

/* test(nmr_manual:input_output, Y) <-
      */

test(nmr_manual, dconsult) :-
%  File = 'nmr/path_disjunctive/path_abcd',
   File = 'examples/nmr/path_disjunctive/path_abcd',
%  dconsult(File, Program),
   dread(lp, File, Program),
   !,
   Program = [
      [arc(a,b),arc(a,c)], [arc(b,d)], [arc(c,d)],
      [path(X,Y)]-[arc(X,Z),path(Z,Y)],
      [path(X,Y)]-[arc(X,Y)] ].

test(nmr_manual, dsave) :-
   Program = [
      [arc(a,b),arc(a,c)], [arc(b,d)], [arc(c,d)],
      [path(X,Y)]-[arc(X,Y)],
      [path(X,Y)]-[arc(X,Z),path(Z,Y)] ],
   File = 'nmr/path_disjunctive/path_abcd_2',
   dsave(Program, File).

 
/******************************************************************/


