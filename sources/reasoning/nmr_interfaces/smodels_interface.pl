

/******************************************************************/
/***                                                            ***/
/***         DisLog:  Interface to Smodels                      ***/
/***                                                            ***/
/******************************************************************/


:- op(700, xfx, '!=').


/*** interface ****************************************************/


/* program_to_definite_consequences_smodels(Program, State) <-
      */

program_to_definite_consequences_smodels(Program, State) :-
   program_to_stable_models_smodels(Program, Stable_Models),
   length(Stable_Models, N),
   write_list(['stable models: ', N]), nl,
   tree_based_boolean_dualization(Stable_Models, State_2),
   list_to_ord_set(State_2, State).


/* program_to_stable_models_smodels(Program, Stable_Models) <-
      */

program_to_stable_models_smodels(Program, Stable_Models) :-
   dislog_variable_get(smodels_in, File_I),
   dislog_variable_get(smodels_out, File_O),
   dislog_variable_get(home, DisLog),
   concat([DisLog, '/results/', File_I], Path_I), 
   concat([DisLog, '/results/', File_O], Path_O), 
   dislog_program_to_thing_facts(Program, Facts_2),
   list_to_ord_set(Facts_2, Facts),
   append(Facts, Program, Program_2),
   dsave_for_smodels(Program_2, Path_I),
   dislog_variable_get(smodels_type(stable), [Type, _]),
   name_append([
      'lparse < ',Path_I,' | smodels ',Type,' > ',Path_O ],
      Command ),
   us(Command),
   parse_stable_models_file(Path_O, Stable_Models_smodels),
   maplist( stable_model_smodels_prune,
      Stable_Models_smodels, Stable_Models_2 ),
   dislog_program_to_dlv_pragram(Stable_Models, Stable_Models_2).

stable_model_smodels_prune(I1, I2) :-
   findall( Atom,
      ( member(Atom, I1),
        \+ functor(Atom, 'thing_smodels', _) ),
      I2 ).


/* dsave_for_smodels(Program, File) <-
      */

dsave_for_smodels(Program, File) :-
   dislog_program_to_smodels_program(Program, Program_Smodels),
   switch(F, File),
   dportray(lp, Program_Smodels), nl,
   switch(F).


/* dislog_program_to_smodels_program(Program_1, Program_2) <-
      */

dislog_program_to_smodels_program(Program_1, Program_2) :-
   findall( H-P,
      ( member(Rule, Program_1),
        parse_dislog_rule(Rule, H1, P1, N1),
        dislog_atoms_to_dlv_atoms(H1, H2),
        ( H2 = [] -> H = [false]
        ; H = H2 ),
        free_variables(Rule, Variables),
        free_variables_to_thing_atoms(Variables, As),
        dislog_atoms_to_dlv_atoms(P1, P2),
        dislog_atoms_to_dlv_atoms(N1, N),
        smodels_not_body(N, N2),
        append([As, P2, N2], P) ),
      Program_2 ).

dislog_program_to_thing_facts(Program, Facts) :-
   findall( Fact,
      ( member(Rule, Program),
        parse_dislog_rule(Rule, H, P, N),
        append([H, P, N], Atoms),
        member(Atom, Atoms),
        dislog_atom_to_thing_fact(Atom, Fact) ),
      Facts ).

dislog_atom_to_thing_fact(Atom, [thing_smodels(X)]) :-
   Atom =.. [_|Arguments],
   member(X, Arguments),
   constant(X).

free_variables_to_thing_atoms(Variables, Atoms) :-
   foreach(V, Variables), foreach(A, Atoms) do
      A = thing_smodels(V).

smodels_not_body([X=Y|Ls_1], [X'!='Y|Ls_2]) :-
   !,
   smodels_not_body(Ls_1, Ls_2).
smodels_not_body([A_1|Ls_1], [A_2|Ls_2]) :-
   !,
   A_1 =.. [P_1|Args],
   name_append('not ', P_1, P_2),
   A_2 =.. [P_2|Args],
   smodels_not_body(Ls_1, Ls_2).
smodels_not_body([], []).


/* parse_stable_models_file(File, Models) <-
      Parser for output files of Smodels. */

parse_stable_models_file(File, Models) :-
   read_file_into_one_line(File, Line),
   parse_stable_models(Models, Line).

parse_stable_models(Models, Line) :-
   parse_stable_models(Models, Line,_).


/*** implementation ***********************************************/


/* read_file_into_one_line(File, All_Lines) <-
      */

read_file_into_one_line(File, All_Lines) :-
   read_file(File, Lines),
   append(Lines, All_Lines).

read_file(File, Lines) :-
   see(File),
   read_lines_of_file(Lines),
   seen,
   !.

read_lines_of_file(Lines) :-
   read_line_of_file(L),
   ( ( L = [],
       Lines = [] )
   ; ( read_lines_of_file(Ls),
       Lines = [L|Ls] ) ).

read_line_of_file(Names) :-
   read_line_of_file([], Line_Reverse),
   reverse(Line_Reverse, Line),
   string_to_names(Line, Names),
   !.

read_line_of_file(Sofar, Line) :-
   get0(C),
   ( ( C == 10, Line = Sofar )
   ; ( C == -1, Line = Sofar )
   ; ( C \== 10, read_line_of_file([C|Sofar], Line) ) ).


/* parse_stable_models(Models, Line_1, Line_2) <-
      */

parse_stable_models(Models) -->
   starting_line,
   parse_stable_models_2(Models).

parse_stable_models_2([M|Ms]) -->
   parse_stable_model(M),
   { M = []
   ; ( last_element(M, A), A \= 'FalseDuration' ) },
   list_of_all_except(['S', 'D'], _),
   parse_stable_models_2(Ms).
parse_stable_models_2([M]) -->
   parse_stable_model(N),
   { append(M, ['FalseDuration'], N) }.

parse_stable_model(Model) -->
   { string_to_names("Stable Model:", Names) },
   Names,
   !,
   atoms(Model),
   !.


starting_line -->
   answer_line.
starting_line -->
   false_line.

answer_line -->
   list_of_all_except(['A'], _),
   { string_to_names("Answer:", Names) },
   Names,
   list_of_all_except(['S', 'F'], _).
   
false_line -->
   list_of_all_except(['F'], _),
   { string_to_names("False", Names) },
   Names.
   
end_line -->
   { string_to_names("Duration", Names) },
   Names.


atoms([A|As]) -->
   atom(A),
   { A \= 'Answer', A \= 'FalseDuration' },
   atoms(As).
atoms([]) -->
   atom(A),
   { A = 'Answer' },
   list_of_all_except(['S', 'D'], _).
atoms([A]) -->
   atom(A),
   { A = 'FalseDuration' }.

atom(Atom) -->
   [' '],
   atom(Atom),
   !.
atom(Atom) -->
   term(Atom).

term(Term) -->
   identifier(F),
   ['('],
   terms(Ts),
   { Term =.. [F|Ts] },
   [')'].
term(Term) -->
   identifier(Term).

identifier(Id) -->
   { name(NewLine, [10]) },
   list_of_all_except(['(',')',',',' ',':',NewLine], List),
   { name_append(List, Id) },
   !.

terms([T|Ts]) -->
   term(T),
   [','],
   terms(Ts).  
terms([T]) -->
   term(T).  


/******************************************************************/


