

/******************************************************************/
/***                                                            ***/
/***         DisLog:  WWW Interface                             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* computation of models */

mosaic_min_mod(Program,File) :-
   dnlp_to_dlp(Program,Disjunctive_Program),
   mm_operator(Disjunctive_Program,Minimal_Models),
   mosaic_pp_chs(File,Minimal_Models).

mosaic_stab_mod(Program,File) :-
   stable_models_operator(Program,Stable_Models),
   mosaic_pp_chs(File,Stable_Models).

mosaic_poss_mod(Program,File) :-
   possible_models_operator(Program,Possible_Models),
   mosaic_pp_chs(File,Possible_Models).


/* computation of disjunctive model states */

mosaic_min_ms(Program,File) :-
   dnlp_to_dlp(Program,Disjunctive_Program),
   minimal_model_state_operator(Disjunctive_Program,State),
   mosaic_pp_dhs(File,'',State).

mosaic_stab_ms(Program,File) :-
   stable_model_state_operator(Program,State),
   mosaic_pp_dhs(File,'',State).

mosaic_poss_ms(Program,File) :-
   possible_model_state_operator(Program,State),
   mosaic_pp_dhs(File,'',State).


/* computation of general model states */

mosaic_min_sem(Program,File) :-
   min_sem(Program,State),
   mosaic_pp_ghs(File,'',State).

mosaic_stab_sem(Program,File) :- 
   stable_model_state_general(Program,State),
   mosaic_pp_ghs(File,'',State).

mosaic_poss_sem(Program,File) :- 
   possible_model_state_general_i(Program,State),
   mosaic_pp_ghs(File,'',State).


/* computation of negative model states */

mosaic_egcwa(Program,File) :-
   dnlp_to_dlp(Program,Disjunctive_Program),
   minimal_model_state_negative_s(Disjunctive_Program,State),
   mosaic_pp_dhs(File,'',State).

mosaic_stab_egcwa(Program,File) :-
   stable_model_state_negative_i(Program,State),
   mosaic_pp_dhs(File,'',State).

mosaic_poss_egcwa(Program,File) :-
   possible_model_state_negative(Program,State),
   mosaic_pp_dhs(File,'',State).


/* well-founded semantics */

mosaic_wfs(Program,File) :-
   ( member([_|[_|_]]-_-_,Program)
   ; member([_|[_|_]]-_,Program)
   ; member([_|[_|_]],Program) ),
   !,
   tell(File),
   writeln('  This logic program is disjunctive.  '),
   writeln('  The well-founded semantics cannot be applied.  '),
   told.
mosaic_wfs(Program,File) :-
   mosaic_bdwfs(Program,File).

mosaic_dwfs(Program,File) :-
   dwfs_operator(Program,[T,F]),
   negate_state(F,N),
   ord_union(T,N,State1),
   binary_resolve_fixpoint(State1,State2), 
   mosaic_pp_ghs(File,'',State2).

mosaic_gdwfs(Program,File) :-
   gdwfs_operator(Program,[T,F]),
   negate_state(F,N),
   ord_union(T,N,State1),
   binary_resolve_fixpoint(State1,State2), 
   mosaic_pp_ghs(File,'',State2).

mosaic_bdwfs(Program,File) :-
   bdwfs_operator(Program,State),
   mosaic_pp_ghs(File,'',State).

mosaic_sdwfs(Program,File) :-
   sdwfs_operator(Program,State),
   mosaic_pp_ghs(File,'',State).


mosaic_dconsult(Filename,Program) :-
   clear_current_program,
   [Filename],
   current_program(Program),
   clear_current_program_2, !.


mosaic_pp_sp(T,F) :-
   negate_state(F,N),
   append(T,N,State),
   mosaic_pp_ghs('',State).
 

mosaic_pp_ghs(File,Name,State) :-
   tell(File),
   mosaic_pp_ghs(Name,State), nl,
   told.

mosaic_pp_dhs(File,Name,State) :-
   tell(File),
   mosaic_pp_dhs(Name,State), nl,
   told.
 
mosaic_pp_chs(File,Models) :-
   tell(File),
   mosaic_pp_chs(Models), nl,
   told.


mosaic_pp_ghs(_,Clauses) :-
   partition_general_herbrand_state(Clauses,Pos,Neg,Mixed), 
   write(' { '), mosaic_pp_dhs_loop(4,Pos), nl, 
   write('   '), mosaic_pp_dhs_loop(4,Neg), nl,   
   write('   '), mosaic_pp_dhs_loop(4,Mixed), write(' } '), nl. 
 
mosaic_pp_dhs(_,State) :-
   write(' { '), mosaic_pp_dhs_loop(4,State), write(' } ').

mosaic_pp_dhs_loop(I,State) :-
   mosaic_pp_dhs_loop(I,I,State).

mosaic_pp_dhs_loop(I,J,[C1,C2|Cs]) :-
   !, 
   pp_d_fact(C1), write(' '), 
   K is J - 1, 
   ( ( K = 0, !,
       writeln(' <p>'), write('   '), mosaic_pp_dhs_loop(I,I,[C2|Cs]) );
     mosaic_pp_dhs_loop(I,K,[C2|Cs]) ).
mosaic_pp_dhs_loop(_,_,[C]) :-
   !,
   pp_d_fact(C).
mosaic_pp_dhs_loop(_,_,[]).


mosaic_pp_chs(State) :-
   write(' { '), mosaic_pp_chs_loop(1,State), write(' } ').
 
mosaic_pp_chs_loop(I,State) :-
   mosaic_pp_chs_loop(I,I,State).
 
mosaic_pp_chs_loop(I,J,[C1,C2|Cs]) :-
   !,
   pp_c_fact(C1), write(' '),
   K is J - 1,
   ( ( K = 0, !,
       writeln(' <p>'), write('   '), mosaic_pp_chs_loop(I,I,[C2|Cs]) );
     mosaic_pp_chs_loop(I,K,[C2|Cs]) ).
mosaic_pp_chs_loop(_,_,[C]) :-
   !,
   pp_c_fact(C).
mosaic_pp_chs_loop(_,_,[]).
 

/******************************************************************/


