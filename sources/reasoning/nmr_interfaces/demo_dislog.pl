

/******************************************************************/
/***                                                            ***/
/***          DisLog:  NMR Demos                                ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      demo/2.


/*** interface ****************************************************/


/* reading programs from the database */

demo(nmr, 1) :-
   dconsult('minker/minker2', Program),
   dportray(lp, Program).

demo(nmr, 2) :-
   lp('path_blocked/pb1').


/* computing various semantics */

demo(nmr, semantics(3, X)) :-
   semantics(X, hi, 'minker/minker2').

demo(nmr, semantics(4, X)) :-
   semantics(X, ghb, 'minker/minker2').

demo(nmr, semantics(5, Y)) :-
   semantics(minimal, Y, 'path/path_abcd').

demo(nmr, 6) :-
   stable_models('przymusinski/przymusinski_1').

demo(nmr, 7) :-
   stable_models('classical_examples/tweety').

demo(nmr, 8) :-
   stable_models('pq_examples/p_if_not_p'),
   bar_line_2,
   partial_stable_models('pq_examples/p_if_not_p'),
   bar_line_2,
   lp('pq_examples/p_if_not_p').


/* program transformations */

demo(nmr, 9) :-
   partial_evaluation('minker/minker2').

demo(nmr, 10) :-
   partial_evaluation('path_blocked/pb1').


/* minimal model semantics */

demo(nmr, tps) :-
   dconsult('path/path_abcd', Program),
   tps_fixpoint_iteration(Program, State),
   bar_line_2, write('Minimal Model State ='),
   dportray(dhs,State),
   bar_line_2, write('Program ='),
   dportray(lp, Program).

demo(nmr, tpi) :-
   dconsult('path/path_abcd',Program),
   tpi_fixpoint_iteration(Program, Models),
   bar_line_2, write('Minimal Herbrand Models ='),
   dportray(chs, Models),
   bar_line_2, write('Program ='),
   dportray(lp, Program).

demoi(nmr, tpi_2) :-
   dc('elementary/abcd1', Program),
   tree_tpi_fixpoint_2(Program, [[]], _).


/******************************************************************/


