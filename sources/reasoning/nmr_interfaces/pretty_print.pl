

/******************************************************************/
/***                                                            ***/
/***             DisLog :  Pretty-Printer                       ***/
/***                                                            ***/
/******************************************************************/


% :- multifile
%       dportray/2.


/*** interface ****************************************************/


/* dportray(File, Type, Object) <-
      pretty prints a DisLog object Object of the type Type
      into the file File. */

dportray(user, Type, Object) :-
   !,
   dportray(Type, Object).
dportray(File, Type, Object) :-
   switch(File_2, File),
   dportray(Type, Object),
   switch(File_2),
   !.


/* dportray(Type, Object) <-
      pretty prints a DisLog object Object of the type Type. */

dportray2(dhs, State) :-
   pp_dhs(State), nl.
dportray2(chs, State) :-
   pp_chs(State), nl.

dportray(sets, Sets) :-
   nl, write(' { '), pp_sets(Sets), write(' } '), nl.

dportray(dhs, State) :-
   nl, pp_dhs(State), nl.
dportray(nhs, State) :-
%  negate_state(State, Negated_State),
   nl, pp_dhs(State), nl.
dportray(chs, State) :-
   nl, pp_chs(State), nl.
dportray(ghs, State) :-
   partition_general_herbrand_state(State, Pos, Neg, Mixed),
   nl,
   write(' { '), pp_dhs_loop(Pos), nl,
   write('   '), pp_dhs_loop(Neg), nl,
   write('   '), pp_dhs_loop(Mixed), write(' } '), nl.


/* dportray(prolog, Program) <-
      */

dportray(prolog, Program) :-
   sublist( dynamic_rule,
      Program, Dynamics ),
   subtract(Program, Dynamics, Program_2),
   prolog_rules_to_dislog_rules(Program_2, DisLog_Program),
   checklist( dportray(dynamic),
      Dynamics ),
   dportray(lp_xml, DisLog_Program).
%  star_line.

dynamic_rule(Rule) :-
   Rule = (:- dynamic _).

dportray(dynamic, Rule) :-
   Rule = (:- dynamic C),
   comma_structure_to_list(C, Xs),
   writeln(':- dynamic'), write('      '),
   list_split_into_sublists_of_size(2, Xs, Ys),
   delete_last_element(Ys, Y, Zs),
   checklist(
      write_list_with_separators(
         writeq_comma, writeq_comma_nl_tab_six),
      Zs ),
   write_list_with_separators(writeq_comma, writeq, Y),
   writeln('.\n').

writeq_comma(X) :-
   writeq(X), write(', ').

writeq_comma_nl_tab_six(X) :-
   writeq(X), write(',\n      ').
 

/* dportray(lp, Program) <-
      */

dportray(lp, Program) :-
   checklist( dportray(rule),
      Program ).
dportray(rule, Rule) :-
   copy_term(Rule, Rule_2),
   pp_rule(Rule_2).
dportray(lp_2, Program) :-
   maplist( copy_term,
      Program, Program_2 ),
   checklist( dportray(rule_2),
      Program_2 ).
dportray(lp_4, Program) :-
   checklist( dportray(rule_4),
      Program ).
dportray(lp_xml, Program) :-
   findall( As-Bs,
      ( member(Rule, Program),
        parse_dislog_rule(Rule, As, Bs1, Bs2),
        negate_atoms(Bs2, Bs3),
        append(Bs1, Bs3, Bs) ),
      Program_2 ),
   program_to_xml(Program_2, Xml),
   xml_to_prolog:xml_rules_to_prolog(Xml),
   !.

dportray(lps, Programs) :-
   maplist( maplist(copy_term),
      Programs, Programs_2 ),
   pp_programs(Programs_2).


dportray(tree,Tree) :-
   nl, pp_tree(Tree).

dportray(sp, [T, F]) :-
   nl,
   write(' T ='), pp_dhs(T), nl,
   write(' F ='), pp_chs(F), nl.

dportray(st,[T, U, F]) :-
   nl,
   write(' T ='), pp_dhs(T), nl,
   write(' U ='), pp_uhs(U), nl,
   write(' F ='), pp_chs(F), nl.

dportray(stsp, [T, U, F]) :-
   state_triple_to_state_pair([T, U, F], [T2, F2]), 
   dportray(sp, [T2, F2]).

dportray(hi, As) :-
   write(' { '),
   write_conjunction(As),
   writeln(' }').

dportray(tu_pair, [T, U]) :-
   write(' T = { '),
   write_conjunction(T),
   writeln(' }'),
   write(' U = { '),
   write_conjunction(U),
   writeln(' }').


/* dportray(rule_2, Rule) <-
      */

dportray(rule_2, Rule) :-
   parse_dislog_rule_save(Rule, As, [], []),
   !,
   numbervars(Rule, 23, _),
   dportray(disjunction_2, As),
   writeln('.').
dportray(rule_2, Rule) :-
   parse_dislog_rule(Rule, As, Xs, Ys),
   numbervars(Rule, 23, _),
   ( As = [],
     write(':- ')
   ; dportray(disjunction_2, As),
     writeln(' :-') ),
   ( Xs = []
   ; ( ( As = []
       ; tab(3) ),
       dportray(conjunction_2, Xs),
       ( Ys = []
       ; writeln(',') ) ) ),
   ( Ys = []
   ; ( tab(3),
       negate_general_clause(Ys, Zs),
       dportray(conjunction_2, Zs) ) ),
   writeln('.').
dportray(rule_4, Rule) :-
   parse_dislog_rule(Rule, [A], Xs, []),
   ( Xs = [] ->
     write(A), writeln('.')
   ; write(A), writeln(' :-'),
     append(Ys, [Y], Xs),
     ( foreach(B, Ys) do
          ( tab(3), write(B), writeln(',') ) ),
     tab(3), write(Y), writeln('.') ).

dportray(disjunction_2, []) :-
   !.
dportray(disjunction_2, Xs) :-
   list_to_semicolon_structure(Xs, Cs),
   write_term(Cs, [numbervars(true), quoted(true)]).

dportray(conjunction_2, []) :-
   !.
dportray(conjunction_2, Xs) :-
   list_to_comma_structure(Xs, Cs),
   write_term(Cs, [numbervars(true), quoted(true)]).


/* dportray(edge(X), Y) <-
      */

dportray(edge(state), State) :-
   grl_pp_state_lattice(small, State).

dportray(edge(tree), Tree) :-
   grl_pp_tree_lattice(small, Tree).
dportray(edge(trees), Trees) :-
   grl_pp_trees(Trees).
dportray(edge(graph), Edges) :-
   grl_pp_graph(Edges).
dportray(edge(labeled_graph), Edges) :-
   grl_pp_labeled_graph(Edges).
dportray(edge(dependency_graph),
      [Nodes, E_Edges, P_Edges, N_Edges]) :-
   grl_pp_dependency_graph(Nodes, E_Edges, P_Edges, N_Edges).


/*** implementation ***********************************************/


/* pp_relation(Tuples) <-
      */

pp_relation([T|Ts]) :-
   write('    '), pp_tuple(T), nl,
   pp_relation(Ts).
pp_relation([]).

pp_tuple([A]) :-
   !,
   dislog_format_3(A).
pp_tuple([A|As]) :-
   dislog_format_2(A),
   pp_tuple(As).
pp_tuple([]).


/* pp_numbers([I1, ..., In]) <-
      writes '                         I1 - ... - In'. */

pp_numbers_2(File_2, Numbers) :-
   switch(File_1, File_2),
   pp_numbers(25, Numbers),
   switch(File_1).
 
pp_numbers(Numbers) :-
   pp_numbers(25, Numbers).

pp_numbers(_, []) :-
   nl.
pp_numbers(Tab, [H|T]) :-
   tab(Tab), write(H),
   pp_numbers_loop(T).

pp_numbers_loop([H|T]) :-
   write(' - '), write(H),
   pp_numbers_loop(T).
pp_numbers_loop([]) :-
   nl.


pp_d_facts :-
   assert(d_fact(0)),
   fail.
pp_d_facts :-
   d_fact(A),
   pp_d_fact(A), nl,
   fail.
pp_d_facts :-
   retract(d_fact(0)).
 
pp_d_clauses :-
   assert(d_clause(0, 0)),
   fail.
pp_d_clauses :-
   d_clause(A, B),
   pp_d_clause(A, B), nl,
   fail.
pp_d_clauses :-
   retract(d_clause(0, 0)).
 
pp_dn_clauses :-
   assert(dn_clause(0, 0, 0)),
   fail.
pp_dn_clauses :-
   dn_clause(A, B, C),
   pp_dn_clause(A, B, C), nl,
   fail.
pp_dn_clauses :-
   retract(dn_clause(0, 0, 0)).

pp_d_fact(0) :-
   fail.
pp_d_fact(Clause) :-
   listvars(Clause, 1),
   write_disjunction(Clause), write('.').


pp_sets([Set]) :-
   !, 
   pp_set(Set).
pp_sets([Set|Sets]) :-
   pp_set(Set), write(', '),
   pp_sets(Sets).
pp_sets([]).

pp_set(Set) :-
   write('{'), write_conjunction(Set), write('}').

pp_c_fact(0) :-
   fail.
pp_c_fact(Clause) :-
   write_conjunction(Clause), write('.').


/* pp_program(Rules) <-
      */

pp_program(Rules) :-
   checklist( pp_rule,
      Rules ).


/* pp_rule(Rule) <-
      */

pp_rule(Head-Body1-Body2) :-
   !,
   pp_dn_clause(Head, Body1, Body2), nl.
pp_rule(Head-Body) :-
   !,
   pp_d_clause(Head, Body), nl.
pp_rule(Head) :-
   !,
   pp_d_fact(Head), nl.


pp_programs([Prog|Progs]) :-
   ( ( length(Prog,1),
       Prog = [Rule],
       parse_dislog_rule(Rule, H, P, N),
       nl, write(' { '), pp_dn_clause(H, P, N), write(' } ') )
   ; ( nl, write(' { '),
       pp_program_2(Prog) ) ),
   pp_programs(Progs).
pp_programs([]) :-
   nl.
 
pp_program_2([Rule|Rules]) :-
   parse_dislog_rule(Rule, H, P, N),
   pp_dn_clause(H, P, N),
   ( length(Rules,0)
   ; ( nl, write('   ') ) ),
   pp_program_2(Rules).
pp_program_2([]) :-
   write(' } ').

pp_d_clause(0, 0) :-
   fail.
pp_d_clause(Head, []) :-
   !,
   listvars(d_fact(Head), 1),
   pp_d_fact(Head).
pp_d_clause(Head, Body) :-
   listvars(d_clause(Head, Body), 1),
   write_disjunction(Head), write(' :- '),
   write_atoms(Body), write('.').

pp_dn_clause(A, B, C) :-
   listvars(dn_clause(A, B, C), 1),
   pp_dn_clause_2(A, B, C).

pp_dn_clause_2(0, 0, 0) :-
   fail.
pp_dn_clause_2(A, [], []) :-
   !,
   pp_d_fact(A).
pp_dn_clause_2(A, B, []) :-
   !,
   pp_d_clause(A, B).
pp_dn_clause_2(A, [], C) :-
   !,
   write_disjunction(A), write(' :- '),
   write_neg_atoms(C), write('.').
pp_dn_clause_2(A, B, C) :-
   write_disjunction(A), write(' :- '),
   write_atoms(B), write(','), write_neg_atoms(C), write('.').


/* pp_slf_rule(Rule) <-
      Pretty-prints the SLF rule Rule = Head-Body_I-Body_E with the
      intensional body Body_I and the extensional body Body_E. */

pp_slf_rule(Head-Body_I-Body_E) :-
   write_atoms(Head), writeln(' :-'),
   tab(3), write_atoms(Body_I), writeln(','),
   tab(3), write_atoms(Body_E), writeln('.').


/* write_atoms(As) <-
      */

writeq_atoms(Xs) :-
   list_to_comma_structure(Xs, C),
   writeq(C).

xxx_write_atoms(As) :-
   !,
   maplist( term_to_atom,
      As, Bs ),
   concat_atom(Bs, ',', B),
   write(B).

write_atoms([A1, A2|As]) :-
   !,
   write_list([A1, ',']),
   write_atoms([A2|As]).
write_atoms([A]) :-
   !,
   write(A).
write_atoms([]).


/* write_disjunction(As) <-
      */

writeq_disjunction(Xs) :-
   writeq_atoms(Xs).

xxx_write_disjunction(As) :-
   !,
   maplist( term_to_atom,
      As, Bs ),
   concat_atom(Bs, ';', B),
   write(B).

write_disjunction([A1, A2|As]) :-
   !,
   write(A1), write(';'),
   write_disjunction([A2|As]).
write_disjunction([A]) :-
   !,
   write(A).
write_disjunction([]).

 
/* write_conjunction(Conjunction) <-
      */

write_conjunction(Conjunction) :-
   write_atoms(Conjunction).


write_neg_atoms([H1, H2|T]) :-
   !,
   write_list(['~', H1, ',']), 
   write_neg_atoms([H2|T]).
write_neg_atoms([H]) :-
   !,
   write_list(['~', H]).
write_neg_atoms([]).


write_neg_atoms_disjunction([H1, H2|T]) :-
   !,
   write_list(['~', H1, ';']), 
   write_neg_atoms_disjunction([H2|T]).
write_neg_atoms_disjunction([H]) :-
   !,
   write_list(['~', H]).
write_neg_atoms_disjunction([]).


pp_delta_d_facts :-
   current_num(tps_mode, 5),
   current_num(trace_level, 4),
   !,
   delta_d_fact_tree(Tree), 
   tree_to_state(Tree, State), 
   tab(25),
   write('Delta = { '), pp_dhs_loop(State), writeln(' } ').
pp_delta_d_facts.

pp_dhs(File, Cs) :-
   switch(File2, File),
   pp_dhs(Cs),
   switch(File2).

pp_dhs_new(File, State) :-
   switch(File2, File),
   pp_dhs_new_loop(State),
   switch(File2).
   
pp_dhs_new_loop([C|Cs]) :-
   pp_d_fact(C), nl,
   pp_dhs_new_loop(Cs).
pp_dhs_new_loop([]).
 
pp_chs(File, Cs) :-
   switch(File2, File),
   pp_chs(Cs),
   switch(File2).
 
pp_hb(Atoms) :-
   writeln('herbrand base'),
   list_of_elements_to_relation(Atoms, HB_P),
   write('  HB ='), pp_uhs(HB_P).

pp_dhs(Cs) :-
   var(Cs),
   !,
   write(' DHB ').
pp_dhs(Cs) :-
   current_num(trace_level, 1),
   !,
   write(' { '), pp_dhs_loop(Cs), write(' } ').
pp_dhs(Cs) :-
   length(Cs, I), I > 20,
   !,
   write(' { '), write(I), write(' clauses } ').
pp_dhs(Cs) :-
   write(' { '), pp_dhs_loop(Cs), write(' } ').

pp_dhs_loop([C1, C2|Cs]) :-
   !,
   pp_d_fact(C1), write(' '), pp_dhs_loop([C2|Cs]).
pp_dhs_loop([C]) :-
   !,
   pp_d_fact(C).
pp_dhs_loop([]).

pp_dhs_loop_2([C1, C2|Cs]) :-
   !,
   pp_d_fact(C1), nl, pp_dhs_loop_2([C2|Cs]).
pp_dhs_loop_2([C]) :-
   !,
   pp_d_fact(C).
pp_dhs_loop_2([]).

pp_chs(Cs) :-
   var(Cs),
   !,
   write(' CHB ').
pp_chs(Cs) :-
   current_num(trace_level, 1),
   !,
   write(' { '), pp_chs_loop(Cs), write(' } ').
pp_chs(Cs) :-
   length(Cs,I), I > 20,
   !,
   write(' { '), write(I), write(' clauses } ').
pp_chs(Cs) :-
   write(' { '), pp_chs_loop(Cs), write(' } ').

pp_chs_loop([C1, C2|Cs]) :-
   !,
%  pp_c_fact(C1), write(' '), pp_chs_loop([C2|Cs]).
   pp_c_fact(C1), nl, write('   '), pp_chs_loop([C2|Cs]).
pp_chs_loop([C]) :-
   !,
   pp_c_fact(C).
pp_chs_loop([]).

pp_uhs(Atoms) :-
   var(Atoms),
   !,
   write(' HB ').
pp_uhs(Atoms) :-
   write(' { '), pp_dhs_loop(Atoms), write(' } ').

pp_mhs(Mixed_Clauses) :-
   var(Mixed_Clauses),
   !,
   write(' MHB ').
pp_mhs(Mixed_Clauses) :-
   write(' { '), pp_mhs_loop(Mixed_Clauses), write(' } ').

pp_mhs_loop([[C1, C2], [C3, C4]|Mixed_Clauses]) :-
   !,
   write_mixed_clause([C1, C2]), write(' '),
   pp_mhs_loop([[C3, C4]|Mixed_Clauses]).
pp_mhs_loop([[C1, C2]]) :-
   write_mixed_clause([C1, C2]).
pp_mhs_loop([]).
   
write_mixed_clause([C1, []]) :-
   !,
   write_disjunction(C1), write('.').
write_mixed_clause([C1, C2]) :-
   write_disjunction(C1), write(';'), 
   write_neg_atoms_disjunction(C2), write('.').


pp_msp(Msp) :-
   writeln('minimal model state'),
   write('  Ms ='), pp_dhs(Msp), nl.

pp_mm(Mm) :-
   writeln('minimal models'),
   write('  Mm ='), pp_chs(Mm), nl.

pp_sp(T, F) :-
   writeln('state pair <T,F>'),
   write('  T ='), pp_dhs(T), nl,
   write('  F ='), pp_chs(F), nl.

pp_st(T, U, F) :-
   writeln('state triple <T,U,F>'),
   write('  T ='), pp_dhs(T), nl,
   write('  U ='), pp_uhs(U), nl,
   write('  F ='), pp_chs(F), nl.

pp_gcwa(Gcwa) :-
   writeln('generalized closed-world-assumption'),
   write('  Gcwa ='), pp_uhs(Gcwa), nl.

pp_egcwa(Egcwa) :-
   writeln('extended generalized closed-world-assumption'),
   write('  Egcwa ='), pp_chs(Egcwa), nl.

pp_dddb(Mixed_Clauses) :-
   writeln('disjunctive deductive database'),
   write('  DDDB ='), pp_mhs(Mixed_Clauses), nl.

pp_ghs(Clauses) :-
   pp_ghs('disjunctive deductive database',Clauses).
pp_ghs(String,Clauses) :-
   partition_general_herbrand_state(Clauses,Pos,Neg,Mixed),
   writeln(String),
   write('  DDDB ='), write(' { '), pp_dhs_loop(Pos), nl,
   write('           '), pp_dhs_loop(Neg), nl,
   write('           '), pp_dhs_loop(Mixed), write(' } '), nl.


/* ppt <-
      pretty printing of d_fact_tree and delta_d_fact_tree. */

ppt :-
   d_fact_tree(Tree),
   !,
   pp_tree(Tree).
ppt.
 
ppdt :-
   delta_d_fact_tree(Tree),
   !,
   pp_tree(Tree).
ppdt.

pp_delta_d_fact_tree(File) :-
   switch(File2, File),
   delta_d_fact_tree(Tree),
   pp_tree(Tree),
   switch(File2). 
 
pp_d_fact_tree(File) :-
   switch(File2, File),
   d_fact_tree(Tree),
   pp_tree(Tree),
   switch(File2). 
 
pp_tree(Tree) :-
   pp_tree(Tree,0).
 
pp_tree(Tree, I) :-
   var(Tree),
   !,
   tab(I), writeln('tree full').
pp_tree([H|T], I) :-
   !,
   J is I+3, tab(I), writeln(H),
   ppx_tree(T, J).
pp_tree([], I) :-
   tab(I), writeln('tree empty').
 
ppx_tree([H|T], J) :-
   pp_tree(H, J),
   ppx_tree(T, J).
ppx_tree([],_).
 

/* abbreviations for listings
      */

liall :-
   lif, lidf, lir,
   listing(herbrand_base/1),
   list, licst, lisp,
   limsp,
   ligcwa, liegcwa.

lp :-
   nl, pp_program, nl.
ldnp :-
   nl, pp_dn_clauses, nl.
ldp :-
   nl, pp_d_clauses, pp_facts, nl.
lr :-
   nl, pp_rules, nl.
lf :-
   nl, pp_facts, nl.
lhb :-
   nl, pp_hb, nl.
lsp :-
   nl, pp_sp, nl.
lst :-
   nl, pp_st, nl.
lgcwa :-
   nl, pp_gcwa, nl.
legcwa :-
   nl, pp_egcwa, nl.
ldddb :-
   nl, pp_dddb, nl.

pp_program :-
   pp_rules, pp_facts.
pp_rules :-
   pp_dn_clauses, pp_d_clauses.
pp_facts :-
   pp_d_facts.
pp_hb :-
   herbrand_base(Atoms),
   pp_hb(Atoms).
pp_msp :-
   minimal_model_state(State),
   pp_msp(State).
pp_ms :-
   collect_derived_d_facts(State),
   pp_msp(State).
pp_sp :-
   state_pair(T, F),
   pp_sp(T, F).
pp_st :-
   state_triple(T, U, F),
   pp_st(T, U, F).
pp_gcwa :-
   gcwa(Gcwa),
   pp_gcwa(Gcwa).
pp_egcwa :-
   egcwa(Egcwa),
   pp_egcwa(Egcwa).
pp_dddb :-
   dddb(Database),
   pp_dddb(Database).


lip :-
   lir,
   lif.
lif :- 
   listing(d_fact), listing(n_fact).
lidf :- 
   listing(delta_d_fact).
lir :- 
   listing(dn_clause), listing(d_clause).
list :-
   listing(state_triple/3).
licst :-
   listing(current_state_triple/3).
lisp :-
   listing(state_pair/2).
limsp :-
   listing(minimal_model_state/1).
ligcwa :-
   listing(gcwa/1).
liegcwa :-
   listing(egcwa/1).

litf :-
   d_fact_tree(Tree),
   findall( Atom,
      ( tree_to_d_fact(Tree, Fact),
        Atom =.. [tree_fact, Fact] ), 
      Atoms ),
   write_atoms(Atoms).
lidtf :-
   delta_d_fact_tree(Tree),
   findall( Atom,
      ( tree_to_d_fact(Tree, Fact),
        Atom =.. [delta_tree_fact, Fact] ),
      Atoms ),
   write_atoms(Atoms).


/******************************************************************/


