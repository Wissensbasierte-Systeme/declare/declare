

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Interface to DisLog Files                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* diinsert(State, File) <-
      */

diinsert(State, File) :-
   diconsult(File, State_2),
   list_to_ord_set(State_2, State_3),
   list_to_ord_set(State, State_4),
   state_union(State_3, State_4, State_5),
   disave(State_5, File).


/* dsave(Program, Filename),
   dstore(Program, Filename) <-
      Stores the DNLP into the file Filename
      in the example directory examples. */
 
dsave(Program, Filename) :-
   dstore(Program, Filename).

dstore(Program, Filename) :-
   dis_ex_extend_filename(Filename, Pathname),
   distore(Program, Pathname).

disave(Program, Pathname) :-
   distore(Program, Pathname).

distore(Program, Pathname) :-
   switch(File, Pathname),
   writeln(':- disjunctive.'),
   dportray(lp,Program), nl,
   writeln(':- prolog.'),
   switch(File).

disave_2(State, File_Name) :-
   writeln('---> output file: '),
   writeln(File_Name),
   disave(State, File_Name).

disave(Write_Predicate, State, File) :-
   switch(F, File),
   writeln(':- disjunctive.'), nl,
   checklist( Write_Predicate,
      State ),
   nl, writeln(':- prolog.'),
   told,
   switch(F).


/* dconsult(Filename, Program) <-
      Consults an example DNLP Program from the file Filename 
      in the example directory examples. */

dc(Filename, Program) :-
   dconsult(Filename, Program).
dconsult(Filename, Program) :-
   dis_ex_extend_filename(Filename, Pathname),
   diconsult(Pathname, Program).

dc(Filename) :-
   dconsult(Filename).
dconsult(Filename) :-
   dis_ex_extend_filename(Filename, Pathname),
   diconsult(Pathname).


/* diconsult(Pathname, Program) <-
      */

diconsult(Pathname, Program) :-
   diconsult(Pathname),
   current_program(Program),
   clear_current_program_2,
   !.

diconsult(Pathname) :-
   clear_current_program,
   [Pathname].


/* dcons(Filename, Program) <-
      */

dcons(Filename, Program) :-
   dcons(Filename),
   current_program(Program),
   clear_current_program_2,
   !.

dcons(Filename) :-
   dis_ex_extend_filename(Filename, Pathname),
   clear_current_program,
   assert('dis$read'),
   [Pathname],
   retractall('dis$read').


/* dislog_consult(Filename, Program) <-
     */

dislog_consult(File, Program) :-
   dislog_variable_get(home, DisLog_Home),
   name_append(DisLog_Home, '/results/ddb_temp', F),
   concat_name_and_file(':- disjunctive. ', File, F),
   concat_file_and_name(F, ' :- prolog.', F),
   dconsult(F, Program).


/* dislog_portray(Program, File) <-
      */

dislog_portray(Program, File) :-
   dislog_variable_get(home, DisLog_Home),
   name_append(DisLog_Home, '/results/ddb_temp', F),
   dportray(F, lp, Program),
   program_file_to_xml(F, Xml),
   xml_to_prolog([Xml], File).


/* clear_current_program <-
      Clears the currently aserted DNLP,
      i.e. retracts all atoms for d_fact, delta_d_fact,
      d_clause, dn_clause, as well as herbrand_base. */

clear_current_program :-
   clear_current_program_2,
   retractall(herbrand_base(_)).

clear_current_program_2 :-
   abolish(d_fact, 1),
   abolish(delta_d_fact, 1),
   abolish(d_clause, 2),
   abolish(dn_clause, 3).
 

/* dis_ex_extend_filename(Filename, Pathname) <-
      Based on the value Path/ of the DisLog variable example_path,
      a full path Pathname = 'Path/Filename' is generated. */

dis_ex_extend_filename(Filename, Filename) :-
   absolute_path(Filename),
   !.
dis_ex_extend_filename(Filename, Pathname) :-
   dislog_variable(example_path, Path),
   concat_atoms([Path, Filename], Pathname).


/******************************************************************/


