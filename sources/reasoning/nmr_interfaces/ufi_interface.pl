

/******************************************************************/
/***                                                            ***/
/***         DisLog:   User Friendly Interface                  ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      semantics/3,
      semantics/4.
:- dynamic
      semantics/3,
      semantics/4.


/*** interface ****************************************************/


/* semantics(Type, Mode, Database_Name) <-
      the semantics Type is applied to the database with the
      name Database_Name, the result type is Mode. */

evaluate(Type, Mode, Database) :-
   semantics(Type, Mode, Database).

semantics(Type, Mode, Database_Name) :-
   dconsult(Database_Name, Database),
   semantics(Type, Mode, Database, State),
   bar_line_2,
   write('Semantics ='),
   change_hb_to_hs(Mode, Portray_Mode),
   dportray(user, Portray_Mode, State).

change_hb_to_hs(dhb, dhs).
change_hb_to_hs(nhb, nhs).
change_hb_to_hs(ghb, ghs).
change_hb_to_hs(hi, chs).
change_hb_to_hs(stable_star, chs).


/* Minimal Model Semantics */

semantics(minimal, dhb, Database, State) :-
   hidden( (
      disjunctive_transformation(Database, Database_2),
      minimal_model_state(Database_2, State) ) ).
semantics(minimal, nhb, Database, State) :-
   hidden( (
      disjunctive_transformation(Database, Database_2),
      minimal_model_state_negative_s(Database_2, State) ) ).
semantics(minimal, ghb, Database, State) :-
   hidden( (
      disjunctive_transformation(Database, Database_2),
      minimal_model_state_general(Database_2, State) ) ).
semantics(minimal, hi, Database, Models) :-
   hidden( (
      disjunctive_transformation(Database, Database_2),
      minimal_models(Database_2, Models) ) ).


/* Perfect Model Semantics */

semantics(perfect, dhb, Database, State) :-
   hidden( (
      locally_stratify_test(Database),
      perfect_model_state(Database, State) ) ).
semantics(perfect, nhb, Database, State) :-
   hidden( (
      locally_stratify_test(Database),
      perfect_model_state_negative_i(Database, State) ) ).
semantics(perfect, ghb, Database, State) :-
   hidden( (
      locally_stratify_test(Database),
      perfect_model_state_general(Database, State) ) ).
semantics(perfect, hi, Database, Models) :-
   hidden( ( 
      locally_stratify_test(Database),
      perfect_models_operator_ls(Database, Models) ) ).


/* Stable Model Semantics */

semantics(stable, dhb, Database, State) :-
   hidden( stable_model_state_operator(Database, State) ).
semantics(stable, nhb, Database, State) :-
   hidden( stable_model_state_negative_s(Database, State) ).
semantics(stable, ghb, Database, State) :-
   hidden( stable_model_state_general(Database, State) ).
semantics(stable, hi, Database, Models) :-
   hidden( stable_models_operator(Database, Models) ).


/* Possible Model Semantics */

semantics(possible, dhb, Database, State) :-
   hidden( possible_model_state_operator(Database, State) ).
semantics(possible, nhb, Database, State) :-
   hidden( possible_model_state_negative(Database, State) ).
semantics(possible, ghb, Database, State) :-
   hidden( possible_model_state_general_i(Database, State) ).
semantics(possible, hi, Database, Models) :-
   hidden( possible_models_operator(Database, Models) ).


/* Well-Founded Semantics */

semantics(dwfs, ghb, Database, State) :-
   hidden( (
      dwfs_operator(Database, [T_S, F_S]),
      negate_state(F_S, N_S), ord_union(T_S, N_S, State) ) ).
semantics(dwfs, hi, Database, Models) :-
   hidden( (
      dwfs_operator(Database, [T_S, F_S]),
      negate_state(F_S, N_S), ord_union(T_S, N_S, State),
      minimal_models_operator_g_state(State, Models) ) ).
semantics(gdwfs, ghb, Database, State) :-
   hidden( (
      gdwfs_operator(Database, [T_S, F_S]),
      negate_state(F_S, N_S), ord_union(T_S, N_S, State) ) ).
semantics(gdwfs, hi, Database, Models) :-
   hidden( (
      gdwfs_operator(Database, [T_S, F_S]),
      negate_state(F_S, N_S), ord_union(T_S, N_S, State),
      minimal_models_operator_g_state(State, Models) ) ).
semantics(bdwfs, ghb, Database, State) :-
   hidden( 
      bdwfs_operator(Database, State) ).
semantics(bdwfs, hi, Database, Models) :-
   hidden( (
      bdwfs_operator(Database, State),
      minimal_models_operator_g_state(State, Models) ) ).


/* semantics <-
      */

semantics :-
   show_dislog_data_dictionary(semantics).

show_dislog_data_dictionary(semantics) :-
   show_dislog_data_dictionary(semantics, [_, _]).

show_dislog_data_dictionary(semantics, [Type, Mode]) :-
   findall( [semantics, Type, Mode],
      ( Head =.. [semantics, Type, Mode, _, _],
        clause(Head, _) ),
      Entries ),
   format("~`*t~`*t~30|~n", []),
   format("~`*t DisLog Data Dictionary ~`*t~30|~n", []),
   format("~`*t~`*t~30|~n", []),
   pp_relation(Entries),
   !.


/* databases(Pattern) <-
      */

databases :-
   show_dislog_data_dictionary(database,'*').
databases(Pattern) :-
   show_dislog_data_dictionary(database, Pattern).

show_dislog_data_dictionary(database, Pattern) :-
   show_dislog_data_dictionary(database, Pattern, Relation),
   writeln(user, Relation),
   pp_relation(Relation).

show_dislog_data_dictionary(database, Pattern, Relation) :-
   dislog_variable_get(example_path, Example_Path),
   unix_directory_contains_files(
      Example_Path, Pattern, Database_Names),
   maplist( analyse_database,
      Database_Names, Relation ),
%  format("~`*t~`*t~30|~n", []),
   star_line,
   format("~`*t DisLog Data Dictionary ~`*t~30|~n", []),
%  format("~`*t~`*t~30|~n", []),
   star_line.


/* unix_directory_contains_files(Path, Pattern, Files) <-
      */

unix_directory_contains_files(Path, Pattern, Files) :-
   dislog_variable_get(output_path, Results),
   concat(['ls -d -F ', Path, Pattern,
      ' > ', Results, 'file_data'], Command_1 ),
   unix(system(Command_1)),
   writeln(user, Command_1),
   concat(['echo "eof" > ', Results, eof], Command_2),
   unix(system(Command_2)),
   concat(['cat ', Results, 'file_data ',
      Results, 'eof > ', Results, filenames], Command_3),
   unix(system(Command_3)),
   concat(Results, filenames, Filename_File),
   file_name_file_to_files(Filename_File, Files).
   
file_name_file_to_files(Filename_File, Files) :-
   read_file_to_string(Filename_File, String),
   name(Name, String),
   name_split_at_position(["\n"], Name, Files_2),
   findall( File,
      ( member(File_2, Files_2),
        \+ concat(_, '/', File_2),
        \+ concat(_, 'eof', File_2),
        name_start_after_position(["DisLog/examples/"],
            File_2, File) ),
      Files ).


/* analyse_database(File, [database, File, N3, N2, N1]) <-
      */

analyse_database(File, [database, File, 0, 0, 0]) :-
   member(Ending, [xml, owl, gxl]),
   file_has_ending(Ending, File),
   !.
analyse_database(File, [database, File, N3, N2, N1]) :-
   dconsult(File, Database),
   analyse_database_group_rules(Database, Groups),
   maplist( length,
      Groups, [N1, N2, N3] ).

analyse_database_group_rules(
      Database, [Dn_Rules, D_Rules, D_Facts]) :-
   analyse_db_group_rules(Database, Dn_Rules, D_Rules, D_Facts),
   !.

analyse_db_group_rules(
      [H-B1-B2|Rs], [H-B1-B2|Dn_Rules], D_Rules, D_Facts) :-
   analyse_db_group_rules(Rs, Dn_Rules, D_Rules, D_Facts).
analyse_db_group_rules(
      [H-B1|Rs], Dn_Rules, [H-B1|D_Rules], D_Facts) :-
   analyse_db_group_rules(Rs, Dn_Rules, D_Rules, D_Facts).
analyse_db_group_rules([H|Rs], Dn_Rules, D_Rules, [H|D_Facts]) :-
   analyse_db_group_rules(Rs, Dn_Rules, D_Rules, D_Facts).
analyse_db_group_rules([], [], [], []).


/******************************************************************/


