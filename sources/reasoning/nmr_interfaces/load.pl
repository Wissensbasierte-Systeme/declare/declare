
/***************************************************************************/
/*              Module for consulting disjunctive programs                 */
/*                                                                         */
/* Author:      Stephan Blocher                                            */
/* Last Change: April 28, 1994                                             */
/*                                                                         */
/* Copyright (C) 1994, Stephan Blocher. All rights reserved.               */
/***************************************************************************/

/* ====================== revision control =================================
   $Id: load.pl,v 0.3 1994/03/10 10:56:11 blocher Exp blocher $

   $Log: load.pl,v $
% Revision 0.3  1994/03/10  10:56:11  blocher
% Presentation version. Delivered on march, 10.
%
========================================================================= */

/* -------------------------------------------------------------------------

Structure of disjunctive Programs:

   The first line (of code) must contain the command
	:- disjunctive.
   to switch the "consult" from reading PROLOG-programs to disj. programs.
   When reading this command, the flag "dis$read" is set.
   The last line must contain the command
	:- prolog.	(Note: this will not be translated to a dn_clause.)
   to unset the "dis$read"-flag. This is necessary for reconsulting PROLOG
   modules. (end_of_file seems not to work)

   In the middle part disjunctive clauses of the form
	D_1 ";" ... ";" D_n "."		                        (facts)
   or
        D_1 ";" ... ";" D_n ":-" K_1 "," ... "," K_m "."	(rules)
   or 
                            ":-" K_1 "," ... "," K_m "."        (negative)
   are expected.
   The form of the literals D_i and K_i determins, which kind of logic
   program is given. When reading the program, the flag "dis_status(X)"
   is set, where X may take one of the following values:
     1) none 	 - no clauses read
     2) positive - only positive Literals were read
     3)	normal   - all D_i are positive, some K_j are negative
     4)	general  - there are some negative head-literals 
		   or strong negation somewhere

   The clauses are stored in "dn_clause"-facts:
	D_1 ";" ... ";" D_n ":-" K_1 "," ... "," K_m "."
   is transformed to
	dn_clause([D_1,...,D_n], [A_1,...,A_x], [B_1,...,B_y]).
   where the A_i's are all positive K_j's and 
         the B_i's are atoms from negative  K_j = ~B_i.
   (Clearly, the disjunctive facts are dn_clause([D_1,...,D_n], [], []).)

Negation:
   Two negation operators are parsed:
	~A is for not(A) with default negation
	-L is for not(L) with (strong) negation
   where A is an atom and L literal (of the form A or -A).
   Possible locations for the negation-operators:
	-L in the head, when L is an ATOM
        -L in the body
	~A in the body	(these literals 

Examples:
   in: anc(X,Y) :- par(X,Z), anc(Z,Y).
  out: dn_clause([anc(X,Y)], [par(X,Z), anc(Z,Y)], []).

   in: a :- b, ~c, d, ~e.
  out: dn_clause([a], [b,d], [c,e]).

   in: a;b :- d, e, ~f.
  out: dn_clause([a,b],[d,e],[f]).

   in: a;-b :- c, d, ~ -e, ~f.
  out: dn_clause([a,-b], [c,d], [-e,f]).

   in: :- c, d, ~ -e, ~f.
  out: dn_clause([], [c,d], [-e,f]).

Attention:
  Leave blank space between operators - and ~ when using in combination.
  Don't use declarations like ":- multifile ..." in the disj. part of your
  program (between ":- disjunctive" and ":- prolog").

Extension:
  1) Command  
	:- use_prolog_builtins.
     is recognized and the flag dis_use_prolog_builtins is set.
  2) Command
	:- use_interpreted.
     is recognized and the flag dis_use_interpreted is set.

Interface:
  dlisting - lists all facts dn_clause(...) (pretty print)
  dis_status(X) - none/positive/normal/general
  dis_use_prolog_builtins - true, if the special command was given
  dis_use_interpreted - ...

last change: (28.04.94)
  - rules like  ":- ..." for negative conjunctions were read as 
    declarations; now rules like ":- ..." are conversed to dn_clause([],...)
    ( So: NO DECLARATIONS in disjunctive programs )
  - some predicates are abolished and redefined as dynamic:
	d_fact/1,delta_d_fact/1,d_clause/2,dn_clause/3
  - end-of-file resets input mode to prolog

------------------------------------------------------------------------- */
   

:- dynamic 
      'dis$read'/0,
      dis_status/1, dis_trace/0,
      dis_use_prolog_builtins/0, dis_use_interpreted/0.

% /* negation operator */
% :- op(900,fy,~).

/* initial status: */
:- assert(dis_status(none)).

/************************* program reading ********************************/

do_term_expansion(end_of_file,_) :- !,
	retractall('dis$read'),
        ( dis_trace -> print('command prolog'), nl
                   ; true),
	fail.
do_term_expansion((:-Com),Clauses) :-
	expand_command(Com,Clauses).

do_term_expansion(Clause,Clauses) :-
	( Clause = (Head :- Body) -> true; Head = Clause, Body = true ),
	'dis$read',
 	make_disj(Head,Body,Clauses)
        .
make_disj(Head,Body,Clauses) :- 
	(  Head==fail
	-> Conclusions=[]
	;  disj_to_list(Head,Conclusions)
	),
	conj_to_posneg(Body,PosPremises,NegPremises),
	New = (dn_clause(Conclusions,PosPremises,NegPremises):-true), 
	( dis_trace -> 
	   print_dn_clause(Conclusions,PosPremises,NegPremises); true 
        ),
	set_dis_status(Conclusions,PosPremises,NegPremises),
	Clauses=[ New ].

expand_command(Body,[ (dn_clause([],PosPremises,NegPremises):-true) ]) :-
	'dis$read', 
	Body \== prolog,
	conj_to_posneg(Body,PosPremises,NegPremises).
expand_command(disjunctive,
	[(:- dynamic d_fact/1,delta_d_fact/1,d_clause/2,dn_clause/3)]
			) :-
	retractall('dis$read'),
	assert('dis$read'),
        abolish(user:d_fact, 1),
        abolish(user:delta_d_fact, 1),
        abolish(user:d_clause, 2),
        abolish(user:dn_clause, 3),
	'dis_trace' -> (print('command disjunctive'), nl)
                       ; true.
expand_command(prolog,[]) :-
          retractall('dis$read'),
          'dis_trace' -> (print('command prolog'), nl)
                       ; true.

expand_command(use_prolog_builtins,[]) :-
          retractall('dis_use_prolog_builtins'),
	  assert('dis_use_prolog_builtins'),
          'dis_trace' -> (print('command use_prolog_builtin'), nl)
                       ; true.

expand_command(use_interpreted,[]) :-
          retractall('dis_use_interpreted'),
	  assert('dis_use_interpreted'),
          'dis_trace' -> (print('command use_interpreted'), nl)
                       ; true.

/* set dis_status to correct values for new program-clause */
set_dis_status(Head,Pos,Neg) :-
	get_status(Head,SH), get_status(Pos,SP), get_status(Neg,SN),
	set_dis_statuspos(SH),
	set_dis_statuspos(SP),
	( Neg==[] -> true
	  ; set_dis_statusneg(SN)
	).

get_status([],positive).
get_status([Lit|Rest],S) :-
	get_status(Rest,S1),
	chk_term_status(Lit,S2),
	max_status(S1,S2,S).

/* check status of a literal */
chk_term_status(-(_), general) :- !.
chk_term_status(~(-(_)), general) :- !.
chk_term_status(~(_), normal) :- !.
chk_term_status(_, positive).

/* ordering on the possible stati */
max_status(X,X,X).
max_status(positive,X,X) :- X \== positive.
max_status(normal,positive,normal).
max_status(normal,general,general).
max_status(general,_,general).

set_dis_statuspos(_) :- dis_status(none),
	retractall(dis_status(_)), assert(dis_status(positive)), fail.
set_dis_statuspos(positive).
set_dis_statuspos(normal) :- retractall(dis_status(_)),
	assert(dis_status(general)).
set_dis_statuspos(general) :- retractall(dis_status(_)),
        assert(dis_status(general)).
set_dis_statusneg(positive) :- 
	dis_status(general) -> true
	; retractall(dis_status(_)), assert(dis_status(normal)).
set_dis_statusneg(general) :-
	retractall(dis_status(_)), assert(dis_status(general)).

/* transform conjunction  A,B,... to  [A,B,...] */
conj_to_list(Term,List) :-
	conj_to_list(Term,List,[]).
conj_to_list(Term,List0,List) :-
	( Term = (T1,T2) ->
	  conj_to_list(T1,List0,List1),
	  conj_to_list(T2,List1,List)
        ; Term == true ->
	  List0 = List
        ; List0 = [Term|List]
        ).

/* extension to the previous predicate: positive literals get into
   the list PosList and negative literals ~L get as L into NegList */
conj_to_posneg(Term,PosList,NegList) :-
	conj_to_posneg(Term,PosList,[],NegList,[]).
conj_to_posneg(Term,PosList0,PosList,NegList0,NegList) :-
	( Term = (T1,T2) ->
	  conj_to_posneg(T1,PosList0,PosList1,NegList0,NegList1),
	  conj_to_posneg(T2,PosList1,PosList,NegList1,NegList)
        ; Term == true ->
	  PosList0 = PosList, NegList0 = NegList
	; Term = ~(Term1) ->
	  PosList0 = PosList, NegList0 = [Term1|NegList]
        ; PosList0 = [Term|PosList], NegList0 = NegList
        ).

/* same thing for disjunctions */
disj_to_list(Term,List) :-
	disj_to_list(Term,List,[]).
disj_to_list(Term,List0,List) :-
	( Term = (T1;T2) ->
	  disj_to_list(T1,List0,List1),
	  disj_to_list(T2,List1,List)
        ; Term == true ->
	  List0 = List
        ; List0 = [Term|List]
        ).

/* dual predicates */
list_to_conj([],true).
list_to_conj([Lit|List],G0) :-
	( List == [] ->
	  G0 = Lit
        ; G0 = (Lit,G),
	  list_to_conj(List,G)
        ).

posneg_to_conj([],[],true) :- !.
posneg_to_conj([Atom|List],Neg,G0) :-
	( List == [] ->
          (Neg==[] -> G0 = Atom
	   ; G0 = (Atom,G), posneg_to_conj(List,Neg,G)
          )
        ; G0 = (Atom,G),
          posneg_to_conj(List,Neg,G)
        ).
posneg_to_conj([],[Atom|List],G0) :-
        ( List == [] ->
          G0 = ~(Atom)
        ; G0 = ( ~(Atom),G),
          posneg_to_conj([],List,G)
        ).

neg_to_conj([],true).
neg_to_conj([Lit|List],G0) :-
	( List == [] ->
	  G0 = ~(Lit)
        ; G0 = (~(Lit),G),
	  neg_to_conj(List,G)
        ).

list_to_disj([],true).
list_to_disj([Lit|List],G0) :-
	( List == [] ->
	  G0 = Lit
        ; G0 = (Lit;G),
	  list_to_disj(List,G)
        ).

/* ----------------- end of loading routines --------------------------- */

/* DisLog tracing:
   xtrace: turns SLG trace on, which prints out tables at various 
           points
   xnotrace: turns off SLG trace
*/
xtrace :- 
    ( dis_trace -> 
      true 
    ; assert(dis_trace)
    ).
xnotrace :- 
    ( dis_trace -> 
      retractall(dis_trace) 
    ; true
    ).

/********************** print routines *************************************/

dlisting :- \+ dis_status(none),
	dn_clause(A,B,C), print_dn_clause(A,B,C), fail.
dlisting :- \+ dis_status(none).

/* reconsulting is not possible, even dabolish won't change this
dabolish :- \+ dis_status(none),
	retract( (dn_clause(_,_,_):-true) ) , fail
	% , dynamic(dn_clause/3),
	% multifile(dn_clause/3).
	.
dabolish :- \+ dis_status(none),
	retractall(dis_status(_)),
	assert(dis_status(none)).
*/
print_dn_clause(X,Y,Z) :- 
	list_to_disj(X,Head),
	posneg_to_conj(Y,Z,Body),
	print(Head), 
	(Body==true -> true
          ;( format(" :- ",[]),
             print(Body)
           )
        ),
	format(".",[]), nl.

/*********************** turn on user:term_expansion ***********************/

:- assertz((
	term_expansion(X,Y) :-
	        do_term_expansion(X,Y)
	    )).

/***************************************************************************/


