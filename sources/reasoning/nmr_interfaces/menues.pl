


/******************************************************************/
/***                                                            ***/
/***           DisLog:  NMR Menues                              ***/
/***                                                            ***/
/******************************************************************/


m :-
   dislog_ufi_menue.

dislog_ufi_menue :-
   dotted_line,
   writeln_list(user,[
      ' ',
      '     DisLog  -  Non-Monotonic Reasoning  ']),
   dotted_line,
   writeln_list(user,[
      ' ',
      '     semantics <-  ',
      '        lists the available semantics  ',
      '     semantics(?Semantics,?Result_Type,+Database) <-  ',
      '        applies Semantics to Database  ',
      '        and yields a result of the type Result_Type  ',
%     '        it is possible to browse through all available',
%     '        semantics and result types for a fixed database  ',
      ' ',
      '     databases, databases(+Pattern) <-  ',
      '        lists the available databases  ',
      '        that match the regular expression Pattern  ',
      ' ',
      '     list_database(+Database) <-  ',
      '        show the example file Database  ',
      '     edit_database(+Database) <-  ',
      '        edit the example file Database  ']),
%     '     show_library_manual <-  ',
%     '        show the DisLog library manual  ',
   dotted_line,
   write(user,'                                             '),
   writeln(user,'     m <- show this menue  ').

old_menue_2 :-
   writeln(user,' '),
   writeln(user,'                            DisLog-Operators '),
   dotted_line,
   writeln(user,' '),
   write(user,'    mms_operator(+Dlp,-State)        '),
   writeln(user,'  tps_fixpoint_iteration(+Dlp,-State)  '),
   write(user,'    mm_operator(+Dlp,-Min_Models)    '),
   writeln(user,'  egcwa_operator(+Dlp,-State)      '),
   write(user,'    cmms_operator(+Dnlp,-State)      '),
   writeln(user,' '),
   writeln(user,' '),
   write(user,'    sm_operator(+Dnlp,-Stable_Models)'),
   writeln(user,'  sms_operator(+Dnlp,-Stable_State)'),
   write(user,'    csms_operator(+Dnlp,-State)      '),
   writeln(user,' '),
   writeln(user,' '),
   write(user,'    dwfs_operator(+Dnlp,-State_Pair) '),
   writeln(user,'  gdwfs_operator(+Dnlp,-State_Pair)'),
   writeln(user,'    dwfs_baral_operator(+Dnlp,-State) '),
   writeln(user,' '),
   write(user,'    edit_db(+File)                   '),
   writeln(user,'  menue   -  show this menue '),
   dotted_line.

old_menue_1 :-
   writeln(user,' '),
   bar_line,
   writeln(user,' '),
   writeln(user,'                         DisLog:   Main-Menue              '),
   dotted_line,
   writeln(user,' '),
   write(user,'    gdwfs   -  generalized dwfs      '),
   writeln(user,'  dwfs    -  disjunctive wfs      '),
   write(user,'    bdwfs   -  Baral''s dfws         '),
   writeln(user,' '),
   write(user,'    ms      -  minimal model state   '),
   writeln(user,'  egcwa   -  closed world assumption '),
   write(user,'    tps_fix -  Tps-fixpoint          '),
   writeln(user,'  tps     -  single Tps-iteration '),
   write(user,'    gnaf_us -  dual generalized naf  '),
   writeln(user,'  gnaf_fs -  generalized naf      '),
   writeln(user,' '),
   write(user,'    lp      -  pretty print program  '),
   writeln(user,'  lf,lr   -  pretty print facts,rules '),
   write(user,'    lip     -  listing of program    '),
   writeln(user,'  lif,lir -  listing of facts,rules '),
   write(user,'    dc      -  consult example       '),
   writeln(user,'  lex     -  list example files     '),
   write(user,'    dl      -  grl-display lattice   '),
   writeln(user,'  menue   -  show this menue '),
   writeln(user,' '),
   bar_line,
   writeln(user,' ').


tps_modes :-
   writeln(user,' '),
   writeln(user,'     -------------------------------------------------- '),
   writeln(user,'                      Tps-Modes                         '),
   writeln(user,'     .................................................. '),
   writeln(user,'       1     -  naive evaluation '),
   writeln(user,'       2     -  delta-iteration '),
   writeln(user,'       3     -  tree-based delta-iteration '),
   writeln(user,'       4     -  tree-delta-iteration with mst '),
   writeln(user,'       5     -  tree-delta-iteration '),
   writeln(user,'       6     -  tree-delta-iteration '),
   writeln(user,'       tpsm   -  show this menue '),
   writeln(user,'     -------------------------------------------------- '),
   writeln(user,' ').
 

lop :-
   writeln(user,' '),
   writeln(user,'DisLog-Commands'),
   writeln(user,'mm   sm    cmms   egcwa   dwfs    bdwfs     gnaf_us'),
   writeln(user,'ms   sms   csms   gcwa    gdwfs   gnaf_fs ').


/******************************************************************/


