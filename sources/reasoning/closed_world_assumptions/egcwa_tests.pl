

/* Operator_test(File) <-
      tests the correctness of an egcwa-operator Operator
      w.r.t. the operator egcwa_operator. */

egcwa_operator_i_test(File) :-
   dconsult(File,State),
   measure(egcwa_operator(State,Egcwa1),T1),
   measure(egcwa_operator_i(State,Egcwa2),T2),
   R is T2/T1, pp_times([T1,T2,R]),
   !,
   Egcwa1 = Egcwa2.

binary_state_to_egcwa_graph_test(File) :-
   dconsult(File,State),
   measure(binary_state_to_egcwa_graph(State,Egcwa1),T1),
   measure(egcwa_operator(State,Egcwa2),T2),
   ord_subtract(Egcwa1,Egcwa2,S1), write(S1), write(' - '),
   ord_subtract(Egcwa2,Egcwa1,S2), writeln(S2),
   R is T2/T1, pp_times([T1,T2,R]),
   !,
   Egcwa1 = Egcwa2.

binary_state_to_egcwa_tree_test(File) :-
   dconsult(File,State),
   measure(binary_state_to_egcwa_tree(State,Egcwa1),T1),
   measure(egcwa_operator(State,Egcwa2),T2),
   R is T2/T1, pp_times([T1,T2,R]),
   !,
   Egcwa1 = Egcwa2.


/* count_atoms_models_egcwa(File) <-
      for the disjunctive Herbrand state State given by File,
      the following is counted:
       - the number of atoms,
       - the number of minimal Herbrand models,
       - the number of minimal elements in the extended generalized
         closed-world-assumption. */

count_atoms_models_egcwa(File) :-
   dconsult(File,State), ord_union(State,List),
   list_to_ord_set(List,Atoms),              length(Atoms,N1),
   mm_operator(State,Models),                length(Models,N2),
%  binary_state_to_egcwa_tree(State,Egcwa),  length(Egcwa,N3),
   binary_state_to_egcwa_graph(State,Egcwa), length(Egcwa,N3),
   pp_numbers([N1,N2,N3]).



egcwa_testing(File) :-
   dc(File,Program1),
   dnlp_to_dlp(Program1,Program),
   mms_operator(Program,State),
   measure( egcwa_testing_1(State,Egcwa1),T1 ),
   dportray(chs,Egcwa1),
   measure( tree_based_boolean_dualization(State,Models),T2 ),
   measure( egcwa_testing_2(Models,Egcwa2),T3 ),
   dportray(chs,Egcwa2),
   ord_subtract(Egcwa1,Egcwa2,Diff),
   dportray(chs,Diff),
   R is T3/T1, nl,
   write('state_to_models   '), pp_times([T2]),
   write('state_to_egcwa    '), pp_times([T1]),
   write('models_to_egcwa   '), pp_times([T3]),
   write('improvement       '), pp_times([R]), nl,
   length(Egcwa1,N1),
   length(Egcwa2,N2),
   length(Diff,N3),
   length(State,N),
   length(Models,M),
   pp_numbers([N1,N2,N3]),
   pp_numbers([N,M]).

egcwa_testing_1(File) :-
   dc(File,Program1),
   dnlp_to_dlp(Program1,Program),
   mms_operator(Program,State),
   measure( egcwa_testing_1(State,Egcwa1),T1 ),
   dportray(chs,Egcwa1),
   pp_times([T1]).

egcwa_testing_1(State,Egcwa) :-
   egcwa_operator(State,Egcwa).
egcwa_testing_2(Models,Egcwa) :-
   hidden( tree_based_coin_to_egcwa(Models,Egcwa) ).


compute_logarithms :-
%  Lengths = [3.21,3.08,3.18,2.52,2.21,1.92,1.77,1.80,1.90,2.00],
   Lengths = [5.57,7.20,10.10,10.90,9.56,7.83,6.67,6.00],
%  Lengths = [7.33,10.35,18.47,22.11,23.06,22.18,20.15,17.61,13.15,2.00],
   logarithms(Lengths,Logs),
   pp_numbers(Logs).

logarithms([X|Xs],[Y|Ys]) :-
   Y is 10 * log(X),
   logarithms(Xs,Ys).
logarithms([],[]).


