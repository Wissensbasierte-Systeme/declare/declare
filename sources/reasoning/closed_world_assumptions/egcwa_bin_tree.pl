

/******************************************************************/
/***                                                            ***/
/***         DisLog :  Egcwa-Operator for Binary Trees          ***/
/***                                                            ***/
/******************************************************************/
 


/*** interface ****************************************************/


/* binary_state_to_egcwa_tree(State,Egcwa) <-
      computes the extended generalized closed-world-assumption Egcwa
      of an acyclic, binary disjunctive Herbrand state State. */

binary_state_to_egcwa_tree(File) :-
   dconsult(File,State),
   measure(binary_state_to_egcwa_tree(State,_),Time),
   writeln(Time).

binary_state_to_egcwa_tree(State,Egcwa) :-
   prepare_analysis,
   binary_state_to_utree(State,Tree),
   binary_tree_to_egcwa(Tree,Egcwa),
   nl, write('  Egcwa ='), pp_chs(Egcwa), nl,
   nl, perform_analysis.


/*** implementation ***********************************************/


/* binary_tree_to_egcwa(Tree,Egcwa) <-
      computes the extended generalized closed-world-assumption Egcwa
      of an acyclic, binary disjunctive Herbrand state represented by
      the binary tree Tree. */

binary_tree_to_egcwa(Tree,Egcwa) :-
   findall( Clause,
            ( binary_tree_to_egcwa(free,Tree,Nodes),
              list_to_ord_set(Nodes,Clause) ),
            Clauses ),
   list_to_ord_set(Clauses,Egcwa).

/* binary_tree_to_egcwa(Mode,Tree,Clause) <-
      the mode Mode directs the search for finding a critical
      vertex set Clause in Tree. */

binary_tree_to_egcwa(free,[Node,Tree],Nodes) :-
   ( ( Tree = [Node2|_],
       Nodes = [Node,Node2] );
     ( tree_down(3,[Node,Tree],[Node2|Trees2]),
       binary_trees_to_egcwa_1(non_free,Trees2,[],Nodes2),
       Nodes = [Node,Node2|Nodes2] ) ).
binary_tree_to_egcwa(free,[Node|Trees],Nodes) :-
   length(Trees,I), I > 1,
   ( ( binary_trees_to_egcwa_1(non_free,Trees,[],Nodes2),
       Nodes = [Node|Nodes2] );
     ( member([Node2|Trees2],Trees),
       binary_trees_to_egcwa_1(non_free,Trees2,[],Nodes2),
       Nodes = [Node,Node2|Nodes2] ) ).
binary_tree_to_egcwa(free,[_|Trees],Nodes) :-
   member(Tree,Trees),
   binary_tree_to_egcwa(down,Tree,Nodes).
binary_tree_to_egcwa(free,Tree,[Node1,Node2|Nodes]) :-
   tree_down(1,Tree,[Node1|Trees1]),
   tree_down(1,Tree,Tree2), [Node1|Trees1] \== Tree2,
   tree_down(1,Tree2,[Node2|Trees2]),
   binary_trees_to_egcwa_1(non_free,Trees1,[],Nodes1),
   binary_trees_to_egcwa_1(non_free,Trees2,[],Nodes2),
   append(Nodes1,Nodes2,Nodes).
binary_tree_to_egcwa(down,Tree,[Node1,Node2|Nodes]) :-
   tree_down(1,Tree,[Node1|Trees1]),
   tree_down(1,Tree,Tree2), [Node1|Trees1] \== Tree2,
   tree_down(1,Tree2,[Node2|Trees2]),
   binary_trees_to_egcwa_1(non_free,Trees1,[],Nodes1),
   binary_trees_to_egcwa_1(non_free,Trees2,[],Nodes2),
   append(Nodes1,Nodes2,Nodes).
binary_tree_to_egcwa(down,[_|Trees],Nodes) :-
   member(Tree,Trees),
   binary_tree_to_egcwa(down,Tree,Nodes).
binary_tree_to_egcwa(down,[Node|Trees],[Node,Node2|Nodes2]) :-
   member([Node2,Tree2],Trees),
   binary_tree_to_egcwa_1(non_free,Tree2,Nodes2).
binary_tree_to_egcwa(down,[Node|Trees],[Node,Node2|Nodes2]) :-
   member([Node2|Trees2],Trees),
   binary_trees_to_egcwa_1(non_free,Trees2,[],Nodes2).
 
binary_tree_to_egcwa_1(non_free,[],[]).
binary_tree_to_egcwa_1(non_free,Tree,[Node]) :-
   Tree = [Node|Trees],
   Trees \== [].
binary_tree_to_egcwa_1(non_free,Tree,[Node2|Nodes3]) :-
   tree_down(2,Tree,[Node2|Trees3]),
   binary_trees_to_egcwa_1(non_free,Trees3,[],Nodes3).
 
binary_trees_to_egcwa_1(non_free,[Tree|Trees],Sofar,Nodes) :-
   binary_tree_to_egcwa_1(non_free,Tree,Nodes1),
   append(Nodes1,Sofar,New_Sofar),
   binary_trees_to_egcwa_1(non_free,Trees,New_Sofar,Nodes).
binary_trees_to_egcwa_1(non_free,[],Nodes,Nodes).

tree_down(0,Tree,Tree) :-
   !.
tree_down(I,[_|Trees],Tree) :-
   member(Tree2,Trees),
   J is I - 1,
   tree_down(J,Tree2,Tree).


/* binary_state_to_utree(State,Tree) <-
      transforms an acyclic, binary state State to an acyclic
      ugraph Tree. */

binary_state_to_utree(State,Tree) :-
   binary_state_to_ugraph(State,Graph),
   vertices(Graph,Vertices), member(Vertex,Vertices), !,
   ugraph_to_tree(Graph,Vertex,Tree),
   dportray(tree,Tree).
 

/* state_to_egcwa_characteristics(State) <-
      determines some characteristics of the extended generalized
      closed-world assumption of the disjunctive Herbrand state State. */

state_to_egcwa_characteristics(State) :-
   egcwa_operator(State,Egcwa),
   egcwa_to_characteristics(Egcwa,Characteristics),
   dportray(chs,Egcwa),
   writeln(Characteristics).
 
egcwa_to_characteristics([C|Cs],[Ch|Chs]) :-
   clause_to_int_list(C,L),
   list_to_ord_set(L,O),
   ord_set_to_characteristic(O,Ch),
   egcwa_to_characteristics(Cs,Chs).
egcwa_to_characteristics([],[]).
 
ord_set_to_characteristic([N,M|Ns],[K|Ks]) :-
  K is M - N,
  ord_set_to_characteristic([M|Ns],Ks).
ord_set_to_characteristic([_],[]).
ord_set_to_characteristic([],[*]).
 
clause_to_int_list([A|As],[N|Ns]) :-
   name(A,[_|Digits]),
   name(N,Digits),
   clause_to_int_list(As,Ns).
clause_to_int_list([],[]).

/******************************************************************/


