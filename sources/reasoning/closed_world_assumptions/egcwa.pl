

/******************************************************************/
/***                                                            ***/
/***             DisLog :  Egcwa                                ***/
/***                                                            ***/
/******************************************************************/


:- dynamic 
      egcwa/1.


/*** interface ****************************************************/


/* gcwa_operator(Program,Gcwa) <-
      for a given DLP Program the generalized closed-world-assumption
      Gcwa is determined. */

gcwa_operator(Program,Gcwa) :-
   minimal_model_state(Program,State),
   state_to_gcwa(Program,State,Gcwa). 


/* state_to_gcwa(Program,State,Gcwa) <-
      Given a DNLP Program and a canonical disjunctive Herbrand 
      state State, the set Gcwa of all atoms that are false in all 
      minimal Herbrand models of State is computed. */

state_to_gcwa(Program,State,Gcwa) :-
   state_or_coin_to_gcwa(Program,State,Gcwa).


/* egcwa_operator(Program,Egcwa) <-
      for a given DLP Program the extended generalized 
      closed-world-assumption Egcwa is computed. */

egcwa_operator(Program,Gcwa_Egcwa) :-
   minimal_model_state(Program,State),
   state_to_gcwa_egcwa(Program,State,Gcwa_Egcwa), !.

state_to_gcwa_egcwa(Program,State,Gcwa_Egcwa) :-
   state_to_gcwa(Program,State,Gcwa),
   state_to_egcwa(State,Egcwa),
   ord_union(Gcwa,Egcwa,Gcwa_Egcwa), !.

egcwa_operator_i(Program,Gcwa_Egcwa) :-
   minimal_models_operator(Program,Models),
   tree_based_coin_to_gcwa_egcwa(Program,Models,Gcwa_Egcwa), !.

tree_based_coin_to_gcwa_egcwa(Program,Coin,Gcwa_Egcwa) :-
   coin_to_gcwa(Program,Coin,Gcwa),
   tree_based_coin_to_egcwa(Coin,Egcwa),
   ord_union(Gcwa,Egcwa,Gcwa_Egcwa), !.

 
/* state_to_egcwa(State,Egcwa) <-
      for the d-state State the extended generalized
      closed-world-assumption Egcwa is computed. */

state_to_egcwa(State,Egcwa) :-
   retractall(d_fact(_)),
   assert_arguments(d_fact,State),
   egcwa,
   retract(egcwa(Egcwa)),
   retractall(d_fact(_)).

egcwa :-
   hidden( 1, (
      prepare_analysis,
      init_state_triple([],_,[]),
      msp_to_egcwa,
      retractall(state_triple(_,_,_)) ) ),
   nl, legcwa,
   perform_analysis.


msp_to_egcwa :-
   ( retract(egcwa(_))
   ; true ),
   collect_arguments(d_fact,Minimal_Model_State),
   msp_to_egcwa(Minimal_Model_State,Egcwa),
   assert(egcwa(Egcwa)),
   legcwa.

msp_to_egcwa(Minimal_Model_State,Egcwa) :-
   announcement('model state to egcwa'),
   start_timer(msp_to_egcwa),
   pp_msp(Minimal_Model_State), nl,
   assert(minimal_model_state(Minimal_Model_State)),
   state_to_sn_sets2(Minimal_Model_State,Support_Sets),
   analysed_sn_sets_to_egcwa(Support_Sets,Egcwa_1),
   list_to_ord_set(Egcwa_1,Egcwa),
   retract(minimal_model_state(_)),
   write('total time   '), 
   stop_timer(msp_to_egcwa).  


/* state_to_gcwa(State,Gcwa) <-
      computes the generalized closed-world-assumption Gcwa of the
      not necessarily canonized d-state State. */

state_to_gcwa(State,Gcwa) :-
   state_to_atoms(State,Atoms),
   state_can(State,State2),
   state_to_atoms(State2,Atoms2),
   ord_subtract(Atoms,Atoms2,N_Fact),
   list_of_elements_to_relation(N_Fact,Gcwa).


msp_to_gcwa :-
   collect_arguments(d_fact,Minimal_Model_State),
   msp_to_gcwa(Minimal_Model_State,_).
 
msp_to_gcwa(Minimal_Model_State) :-
   msp_to_gcwa(Minimal_Model_State,_).
 
msp_to_gcwa(Minimal_Model_State,Gcwa) :-
   announcement('model state to gcwa'),
   abolish(gcwa/1),
   state_to_atoms(Minimal_Model_State,Atoms),
   compute_herbrand_base,
   herbrand_base(Herbrand_Base),
   ord_subtract(Herbrand_Base,Atoms,N_Fact),
   list_of_elements_to_relation(N_Fact,Gcwa),
   assert(gcwa(Gcwa)),
   lgcwa.

 
egcwa_slinf :-
   collect_arguments(d_fact,MSp),
   assert(minimal_model_state(MSp)),
   get_example_sli(0),
   compute_all_sn(SN),
   sn_sum_up(SN,CSN),
   remove_empty_list_sn(CSN,CESN),
   basic_sn_to_egcwa(CESN,Egcwa),
   assert(egcwa(Egcwa)),
   retract(minimal_model_state(_)),
   legcwa.


/* abbreviations */

mms(Filename) :-
   dconsult(Filename,Program),
   minimal_model_semantics_msp_egcwa(Program,State1-State2),
   nl, pp_sp(State1,State2).


/*** implementation **************************************************/


/* analysed_sn_sets_to_egcwa(Mode,SN,Egcwa) <-
      the Egcwa for the set SN of sn-sets is computed, depending on 
      the current egcwa_mode Mode,
      for Mode = 0, the basic method is used,
      for Mode > 0, the divide and conquer method splits SN into
      subsets of size Mode, for which finally the basic method is 
      used. */

analysed_sn_sets_to_egcwa(SN,Egcwa) :-
   hidden( 5, (
      analyse_snd_sets(SN),
      statistics_about_sn_sets(SN),
      length_order_minimal_model_state,
%     msp_to_d_fact_tree,
      init_timer(product_of_list),
      sn_sets_to_egcwa(SN,Egcwa),
      write('disjunctions '), ttyflush,
      close_timer(product_of_list) ) ),
   snd_evaluation.

sn_sets_to_egcwa(SN,Egcwa) :-
   current_num(egcwa_mode,0),
   !,
   basic_sn_to_egcwa(SN,Egcwa).
sn_sets_to_egcwa(SN,Egcwa) :-
   current_num(egcwa_mode,Mode),
   dc_sn_to_egcwa(Mode,SN,Egcwa).

 
/* basic_sn_to_egcwa(SN,Egcwa) <-
      given an asserted model state MSp and sn-sets SN,
      the Egcwa is computed. */

basic_sn_to_egcwa(SN,Egcwa) :-
   length(SN,I),
   writeln('sn sets                  '), 
   pp_numbers([I]),
   basic_sn_sets_to_egcwa(I,SN,SN,[],Egcwa).
 
 
/* basic_sn_sets_to_egcwa(I,SN,SND,Egcwa_1,Egcwa_2) <-
      computes an egcwa_iteration, the iteration stops,
      when the snd-set SND is empty, or when the iteration depth is
      greater than the length of the sn-set SN. */
 
basic_sn_sets_to_egcwa(I,_,SND_1,Egcwa,Egcwa) :-
   ( I = 1
   ; SND_1 = [] ),
   !.
basic_sn_sets_to_egcwa(I,SN,SND_1,Egcwa_1,Egcwa_3) :-
   relevant_snd_sn_product(SND_1,SN,SND_2,Egcwa_1,Egcwa_2),
%  relevant_snd_snd_product(SND_1,SN,SND_2,Egcwa_1,Egcwa_2),
   analyse_snd_sets(SND_2),
   basic_sn_sets_to_egcwa(I-1,SN,SND_2,Egcwa_2,Egcwa_3).


/* basic_sn_sets_to_snd_sets_and_egcwa(I,SN,SND,Egcwa) <-
      computes from SN the set SND of all non-empty snd-sets
      and the corresponding egcwa Egcwa. */
 
basic_sn_sets_to_snd_sets_and_egcwa(I,SN,SND,Egcwa) :-
   writeln('sn sets                  '),
   length(SN,I), pp_numbers([I]),
   basic_sn_to_snd_egcwa(I,SN,SN,SN,Sofar,[],Egcwa),
   append(Sofar,SND).
 
basic_sn_to_snd_egcwa(I,_,SND_1,Sofar,Sofar,Egcwa,Egcwa) :-
   ( I = 1
   ; SND_1 = [] ),
   !.
basic_sn_to_snd_egcwa(I,SN,SND_1,Sofar_1,Sofar_3,Egcwa_1,Egcwa_3) :-
   relevant_snd_sn_product(SND_1,SN,SND_2,Egcwa_1,Egcwa_2),
   analyse_snd_sets(SND_2),
   basic_sn_to_snd_egcwa(I-1,SN,SND_2,
      [SND_2|Sofar_1],Sofar_3,Egcwa_2,Egcwa_3).
  

/* relevant_snd_sn_product(SND_1,SN_2,SND_3,Egcwa_1,Egcwa_2) <-
      the product SND_3 of an snd-set SND_1 and an sn-set SN_2
      is created and purified w.r.t. Egcwa_1,
      the new conjunctions C, where SN(C) is empty, are added to
      Egcwa_1 yielding Egcwa_2. */

relevant_snd_sn_product(SND_1,SN_2,SND_3,Egcwa_1,Egcwa_3) :-
   relevant_snd_sn_product(SND_1,SN_2,[],SND_3,Egcwa_1,Egcwa_3).

relevant_snd_sn_product([S1|SND_1],SN_2,Sofar,SND,Egcwa_1,Egcwa_3) :-
   relevant_sn_sets(SN_2,S1,Relevant_SN_2),
   snd_snd_product_loop(S1,Relevant_SN_2,SND_3,Egcwa_1,Egcwa_2),
   append(SND_3,Sofar,New_Sofar),
   relevant_snd_sn_product(SND_1,SN_2,New_Sofar,SND,Egcwa_2,Egcwa_3).
relevant_snd_sn_product([],_,SND,SND,Egcwa,Egcwa).


/* relevant_sn_sets(SN,[C|SN(C)],Relevant_SN) <-
      for a set SN of sn-sets and the last atom Atom in the clause C,
      Relevant_SN is the subset of SN containing all sn-sets [[A]|Cs]
      such that Atom @< A. */

relevant_sn_sets(SN,[C|_],Relevant_SN) :-
   last_element(C,Atom),
   relevant_sn_sets_loop(SN,Atom,Relevant_SN).
  
relevant_sn_sets_loop([],_,[]).
relevant_sn_sets_loop([[[A]|Cs]|SN],Atom,[[[A]|Cs]|Relevant_SN]) :-
   Atom @< A,
   !,
   relevant_sn_sets_loop(SN,Atom,Relevant_SN).
relevant_sn_sets_loop([_|SN],Atom,Relevant_SN) :-
   relevant_sn_sets_loop(SN,Atom,Relevant_SN). 


/* relevant_snd_snd_product(SND_1,SN_2,SND_3,Egcwa_1,Egcwa_2) <-
      the product SND_3 of an snd-set SND_1 with itself
      is created and purified w.r.t. Egcwa_1,
      relevant sn-pairs are determined based on SN_2,
      the new conjunctions C, where SN(C) is empty, are added to
      Egcwa_1 yielding Egcwa_2. */
 
relevant_snd_snd_product(SND_1,SN_2,SND_3,Egcwa_1,Egcwa_3) :-
   list_to_ord_set(SND_1,SND),
   relevant_snd_snd_product(SND,SND,SN_2,[],SND_3,Egcwa_1,Egcwa_3).

relevant_snd_snd_product([S1|SND_1],SND_2,SN_2,Sofar,SND,Egcwa_1,Egcwa_3) :-
   relevant_snd_sets(SN_2,S1,SND_2,Relevant_SN_2),
   snd_snd_product_loop(S1,Relevant_SN_2,SND_3,Egcwa_1,Egcwa_2),
   append(SND_3,Sofar,New_Sofar),
   relevant_snd_snd_product(SND_1,SND_2,SN_2,New_Sofar,SND,Egcwa_2,Egcwa_3).
relevant_snd_snd_product([],_,_,SND,SND,Egcwa,Egcwa).

relevant_snd_sets(SN,[C|_],SND,Relevant_SN) :-
   last_element(C,Atom),
   relevant_snd_sets_loop(SN,C,Atom,SND,Relevant_SN).
   
relevant_snd_sets_loop([],_,_,_,[]).
relevant_snd_sets_loop([[[A]|Cs]|SN],C,Atom,SND,[S1|Relevant_SN]) :-
   Atom @< A,
   !,
   write('relevant_snd             '), ttyflush,   
   ord_subtract(C,[Atom],C1),
   ord_union(C1,[A],C2),
   get_min_snd(C2,[[A]|Cs],SND,S1),
   length(Cs,I1), length(S1,I2), I3 is I2 - 1,
   write(I1), write(' - '), writeln(I3),
   relevant_snd_sets_loop(SN,C,Atom,SND,Relevant_SN).
relevant_snd_sets_loop([_|SN],C,Atom,SND,Relevant_SN) :-
   relevant_snd_sets_loop(SN,C,Atom,SND,Relevant_SN).

get_min_snd(C,S1,SND,S) :-
   get_snd_set(C,SND,S),
   length(S1,I1),
   length(S,I2),
   I1 > I2,
   !.
get_min_snd(_,S,_,S).

get_snd_set(C,[[C|Cs]|_],[C|Cs]) :-
   !.
get_snd_set(C,[[C1|_]|SND],S) :-
   C1 @< C,
   !,
   get_snd_set(C,SND,S).
get_snd_set(_,_,_) :-
   fail.
 

/* dc_sn_sets_to_egcwa(J,SN,Egcwa) <-
      given an asserted model state MSp and sn-sets SN,
      the Egcwa is computed, the split boundary is given by J. */

dc_sn_to_egcwa(J,SN,Egcwa) :-
   divide_and_conquer_sn_to_snd_egcwa(J,SN,_,Egcwa).


/* divide_and_conquer_sn_to_snd_egcwa(J,SN,SND,Egcwa) <-
      given an asserted model state MSp and sn-sets SN,
      the non-empty snd-sets SND and the Egcwa are computed,
      the split boundary is given by J. */

divide_and_conquer_sn_to_snd_egcwa(J,SN,SND,Egcwa) :-
   length(SN,I), I =< J,
   !,
   nl, write('computing  '), projected_writeln(SN), nl,
   basic_sn_sets_to_snd_sets_and_egcwa(I,SN,SND,Egcwa).
divide_and_conquer_sn_to_snd_egcwa(J,SN,SND_4,Egcwa_4) :-
   middle_split(SN,SN_1,SN_2),
   divide_and_conquer_sn_to_snd_egcwa(J,SN_1,SND_1,Egcwa_1),
   divide_and_conquer_sn_to_snd_egcwa(J,SN_2,SND_2,Egcwa_2),
   nl, write('merging  '), projected_write(SN_1),
   write('  and  '), projected_writeln(SN_2), nl,
   snd_snd_product(SND_1,SND_2,SND_3,Egcwa_3),
   reduce_n_facts_by_state_triple(Egcwa_3,Reduced_Delta_Egcwa_3),
%  state_can(Reduced_Delta_Egcwa_3,Purified_Egcwa_3,_),
   ord_union([SND_1,SND_2,SND_3],SND_123),
   sn_sort_by_length(SND_123,SND_4),
   ord_union([Egcwa_1,Egcwa_2,Reduced_Delta_Egcwa_3],Egcwa_4),
   analyse_snd_sets(SND_4).


projected_writeln(L) :-
   projected_write(L), nl.

projected_write([[H|_]|T]) :-
   write(H),
   projected_write(T).
projected_write([]).


statistics_about_sn_sets([[C|Cs]|SN]) :-
   length(C,I), add_num([I,snnumber],1),
   length(Cs,J), add_num([I,sncard],J),
   statistics_about_sn_sets(SN).
statistics_about_sn_sets([]).
 
snd_evaluation :-
%  listing(current_num),
   writeln('statistics about the support-for-negation sets '), nl,
   start_timer(analysis),
   snd_evaluation(1),
   close_num([snnumber],N),
   close_num([j3],J3), close_num([k3],K3),
   close_num([i1],I1), close_num([i2],I2),
   close_num([i3],I3), close_num([i4],I4),
   pp_numbers(4,[o,N]),
   tab(13), 
   ( ( N = 0, not_a_number(Nan),
       pp_averages([Nan,Nan,Nan,Nan,Nan,Nan]) )
   ; pp_averages([J3/N,K3/N,I1/N,I2/N,I3/N,I4/N]) ),
   close_num([snd_reduce],T1), close_num([product],T2),
   close_num([prune],T3),      close_num([delta],T4),
   close_num([purify],T5),     close_num([doss],T6),
   nl,
   write('reductions   '), pp_time(T1), writeln(' sec.   '),
   write('products     '), pp_time(T2), writeln(' sec.   '),
   write('prunings     '), pp_time(T3), writeln(' sec.   '),
   write('deltas       '), pp_time(T4), writeln(' sec.   '),
   write('purifies     '), pp_time(T5), writeln(' sec.   '),
%  write('------------ '), pp_time(T6), writeln(' sec.   '),
   write('sum          '), pp_time(T6), writeln(' sec.   '),
   write('analysis     '), stop_timer(analysis),
   !.

snd_evaluation(I) :-
   current_num([I,snnumber],Num), Num > 0,
%  current_num([I,snnumber],_),
   close_num([I,snnumber],N),    add_num([snnumber],N),
   close_num([I,sncard],N1),     
   close_num([I,snd_reduce],T1), add_num([snd_reduce],T1),
   close_num([I,product],T2),    add_num([product],T2),
   close_num([I,prune],T3),      add_num([prune],T3),
   close_num([I,delta],T4),      add_num([delta],T4),
   close_num([I,purify],T5),     add_num([purify],T5),
   close_num([I,j3],J3),         add_num([j3],J3),
   close_num([I,k3],K3),         add_num([k3],K3),
   close_num([I,i1],I1),         add_num([i1],I1),
   close_num([I,i2],I2),         add_num([i2],I2),
   close_num([I,i3],I3),         add_num([i3],I3),
   close_num([I,i4],I4),         add_num([i4],I4),
   XJ3 is J3 / N, XK3 is K3 / N,
   XI1 is I1 / N, XI2 is I2 / N, XI3 is I3 / N, XI4 is I4 / N,
   X0 is N1 / N, X1 is T1 / N, X2 is T2 / N, 
   X3 is T3 / N, X4 is T4 / N, X5 is T5 / N,
   pp_numbers(4,[I,N]),
   snd_evaluation_if(I,[X0,XJ3,XK3,XI1,XI2,XI3,XI4,X1,X2,X3,X4,X5]),
   J is I + 1,
   snd_evaluation(J).
snd_evaluation(_).

snd_evaluation_if(1,[X|_]) :-
   !,
   tab(13), pp_averages([X]).
snd_evaluation_if(_,[_,XJ3,XK3,XI1,XI2,XI3,XI4,X1,X2,X3,X4,X5]) :-
   tab(13), pp_averages([XJ3,XK3,XI1,XI2,XI3,XI4]),
   tab(13), pp_times([X1,X2,X3,X4,X5]).

  
/******************************************************************/


