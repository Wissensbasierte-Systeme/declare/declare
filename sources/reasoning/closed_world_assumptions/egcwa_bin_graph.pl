

/******************************************************************/
/***                                                            ***/
/***         DisLog :  Egcwa-Operator for Binary Graphs, I      ***/
/***                                                            ***/
/******************************************************************/
 


/*** interface ****************************************************/


/* binary_state_to_egcwa_graph(State,Egcwa) <-
      computes the extended generalized closed-world-assumption Egcwa
      of a binary disjunctive Herbrand state State. */

binary_state_to_egcwa_graph(File) :-
   dconsult(File,State),
   hidden( 1, (
      select_binary_disjunctions(State,State2),
      binary_state_to_egcwa_graph(State2,Egcwa) ) ),
   dportray(chs,Egcwa).


select_binary_disjunctions([[A,B]|Cs1],[[A,B]|Cs2]) :-
   !,
   select_binary_disjunctions(Cs1,Cs2).
select_binary_disjunctions([_|Cs1],Cs2) :-
   select_binary_disjunctions(Cs1,Cs2).
select_binary_disjunctions([],[]).


binary_state_to_egcwa_graph(State,Egcwa) :-
   prepare_analysis,
   binary_state_to_ugraph(State,Graph),
   vertices(Graph,Nodes),
   findall( A-X-Y,
            ( member(A,Nodes),
              four_sequences_ge(Graph,A-X),
              two_sequences_ge(Graph,A-Y) ),
            List ),
   list_to_ord_set(List,Seq),
%  Status1 = [[]-Nodes,[],Graph,Seq],
   Status1 = [[]-Nodes,[],[],Seq], assert(graph_ge(Graph)),
   writeln(Status1), writeln(user,'status computed'),
%  member(Node1,Nodes), 
   !,
   switch(File,'/dev/null'),
   findall( Marked,
            ( find_init_ge(Status1,_,Status2),
              expand_status_ge(Status2,[Marked-_|_]),
              write(user,'.'), ttyflush ),
            Egcwa_List1 ),
   switch(File),
%  nl, write('  Egcwa_List1 ='), pp_chs(Egcwa_List1), nl, 
   list_to_ord_set(Egcwa_List1,Egcwa_List2),
%  nl, write('  Egcwa_List2 ='), pp_chs(Egcwa_List2), nl, 
   ugraph_to_stars(Graph,Stars),
   state_subtract(Egcwa_List2,Stars,Egcwa_List3),
   state_subtract(Stars,Egcwa_List3,Stars2),
   state_can(Egcwa_List3,Egcwa_List4),
   ord_union(Stars2,Egcwa_List4,Egcwa),
   length(Egcwa_List1,L1), length(Egcwa_List2,L2), 
   length(Egcwa_List3,L3), length(Egcwa_List4,L4),
   length(Egcwa,L), 
   pp_numbers([L1,L2,L3,L4,L]),
   write('updates      '), close_timer(update_state_ge),
   write('groups       '), close_timer(group_four_sequences),
   retract(graph_ge(Graph)),
   nl, write('  Egcwa ='), pp_chs(Egcwa), nl, 
   nl, perform_analysis.


/*** implementation ***********************************************/


xxx_find_init_ge(Status1,A,A,Status3) :-
   mark_node_ge(Status1,A,Status2),
   init_state_ge(Status2,A,Status3).
xxx_find_init_ge(Status1,A,C,Status3) :-
   non_mark_nodes_ge(Status1,[A],Status2),
%  Status1 = [_-Markable,_,G,_|_],
   Status1 = [_-Markable,_,[],_|_], graph_ge(G),
   neighbours(A,G,Bs), ord_intersection(Bs,Markable,Mbs), member(B,Mbs),
   find_init_ge(Status2,B,C,Status3).

find_init_ge(Status1,A,Status4) :-
   Status1 = [_-Markable|_],
   append(X,[A|_],Markable),
   non_mark_nodes_ge(Status1,X,Status2),
   mark_node_ge(Status2,A,Status3),
   init_state_ge(Status3,A,Status4).


step_ge(Status1,A,Status4) :-
   pick_sequence_i_ge(Status1,[A,B],Status2),
   step_i_ge(Status2,[A,B],Status3),
   next_step_ge(Status3,Status4).
step_ge(Status1,A,Status4) :-
   pick_sequence_ii_ge(Status1,[A,B,C,D],Status2),
   step_ii_ge(Status2,[A,B,C,D],Status3),
   next_step_ge(Status3,Status4).
step_ge(Status1,A,Status4) :-
   pick_sequence_iii_ge(Status1,[A,B,C,D],Status2),
   step_iii_ge(Status2,[A,B,C,D],Status3),
   next_step_ge(Status3,Status4).

next_step_ge(Status,Status) :-
   inconsistent_ge(Status), !.
next_step_ge(Status1,Status2) :-
   marked_node_ge(Status1,A),
   step_ge(Status1,A,Status2).

xxx_writeln_status(_) :-
   !.
writeln_status([MM,State,_,Seq|_]) :-
   writeln(MM),
   writeln(State),
   writeln(Seq).

step_i_ge(Status1,[_,B],Status3) :-
% step_i_ge(Status1,[A,B],Status3) :-
%  write('*** step_i_ge  '), writeln([A,B]), writeln_status(Status1),
   non_marked_neighbours_ge(Status1,B,Neighbours),
   update_state_i_ge(Status1,Neighbours,[B],Status2),
   mark_node_ge(Status2,B,Status3).
%  writeln('new status'), writeln_status(Status3).

step_ii_ge(Status1,[A,B,C,D],Status4) :-
%  write('*** step_ii_ge  '), writeln([A,B,C,D]), writeln_status(Status1),
   Status1 = [M1-M2,S,G,Seq1|T],
   remove_reverse_ii_ge(Seq1,A-[[B,C,D]],Seq2),
   non_marked_neighbours_ge(Status1,D,Neighbours),
   update_state_ii_ge([M1-M2,S,G,Seq2|T],Neighbours,[B,C,D],Status2), 
   mark_node_ge(Status2,D,Status3),
   non_mark_nodes_ge(Status3,[B,C],Status4).
%  writeln('new status'), writeln_status(Status4).

step_ii_prime_ge(Status1,[A,B,C,D],Status4) :-
%  write('*** step_ii_ge  '), writeln([A,B,C,D]), writeln_status(Status1),
   Status1 = [M1-M2,S,G,Seq1|T],
   remove_reverse_ii_ge(Seq1,A-[[B,C,D]],Seq2),
   non_marked_neighbours_ge(Status1,D,Neighbours),
   update_state_ii_ge([M1-M2,S,G,Seq2|T],Neighbours,[B,C,D],Status2), 
   mark_node_ge(Status2,D,Status4).
%  writeln('new status'), writeln_status(Status4).

step_iii_ge(Status1,[A,B,C,D],Status3) :- 
%  write('*** step_iii_ge  '), writeln([A,B,C,D]), writeln_status(Status1),
   Status1 = [M1-M2,S,G,Seq1|T],
   remove_reverse_ii_ge(Seq1,A-[[B,C,D]],Seq2),
   update_state_iii_ge([M1-M2,S,G,Seq2|T],[B,C],Status3).
%  non_mark_nodes_ge(Status2,[B,C],Status3),
%  writeln('new status'), writeln_status(Status3).


four_sequences_ge(Graph,A-X) :- 
   findall( [B,C,D],
            ( neighbours(A,Graph,Bs), member(B,Bs),
              neighbours(B,Graph,Cs), member(C,Cs), C \== A,
              neighbours(C,Graph,Ds), member(D,Ds), D \== B, D \== A ),
            X ), !.

two_sequences_ge(Graph,A-Y) :-
   findall( B,
            ( neighbours(A,Graph,Bs), member(B,Bs) ),
            Y ), !.

pick_sequence_i_ge([M1-M2,S,G,Seq1|T],[A,B],[M1-M2,S,G,Seq4|T]) :-
   pick_pick_ge(M1,Seq1,A-X-Y1,Seq2), !, append(_,[B|Y2],Y1), member(B,M2),
   remove_if_possible_i_ge(Seq2,[B,A],Seq3),
   ord_add_element(Seq3,A-X-Y2,Seq4).

remove_if_possible_i_ge(Seq1,[B,A],Seq3) :-
   member(B-V-W1,Seq1), !, ord_del_element(Seq1,B-V-W1,Seq2),
   ord_del_element(W1,A,W2),
   ord_add_element(Seq2,B-V-W2,Seq3).
remove_if_possible_i_ge(Seq,_,Seq).

pick_sequence_ii_ge([M1-M2,S,G,Seq1|T],[A,B,C,D],[M1-M2,S,G,Seq4|T]) :-
   pick_pick_ge(M1,Seq1,A-X1-_,Seq2), !, append(_,[[B,C,D]|X2],X1),
%  check_status_ge([M1-M2,S,G,Seq1|T],[B,C]),
%  \+ member(B,M1), \+ member(C,M1), 
   member(D,M2),
   remove_if_possible_ii_ge(Seq2,[D,C,B,A],Seq3),
   ord_add_element(Seq3,A-X2-[],Seq4).
   
pick_pick_ge(_,[A-X-Y|Seq],A-X-Y,Seq) :-
   !.
pick_pick_ge(M1,[B-_-_|Seq1],A-X-Y,Seq2) :-
   member(B,M1), !,
   pick_pick_ge(M1,Seq1,A-X-Y,Seq2).
pick_pick_ge(M1,[B-U-V|Seq1],A-X-Y,[B-U-V|Seq2]) :-
   pick_pick_ge(M1,Seq1,A-X-Y,Seq2).

remove_if_possible_ii_ge(Seq1,[D,C,B,A],Seq3) :-
   member(D-V1-W,Seq1), !, ord_del_element(Seq1,D-V1-W,Seq2),
   ord_del_element(V1,[C,B,A],V2),  
   ord_add_element(Seq2,D-V2-W,Seq3).
remove_if_possible_ii_ge(Seq,_,Seq).

pick_sequence_iii_ge([M1-M2,S,G,Seq1|T],[A,B,C,D],[M1-M2,S,G,Seq4|T]) :-
   pick_pick_ge(M1,Seq1,A-X1-_,Seq2), !, append(_,[[B,C,D]|X2],X1),
%  check_status_ge([M1-M2,S,G,Seq1|T],[B,C]),
%  \+ member(B,M1), \+ member(C,M1), 
   member(D,M1),
   remove_if_possible_ii_ge(Seq2,[D,C,B,A],Seq3),
   ord_add_element(Seq3,A-X2-[],Seq4).

check_status_ge([M1-M2,S,G,Seq1|T],Nodes) :-
   ord_intersect(Nodes,M1), !,
   write(Nodes), write('   '), writeln_status([M1-M2,S,G,Seq1|T]).
check_status_ge(_,_).

remove_complete_sequences_ge([MM,S,G,Seq1|T],Nodes,[MM,S,G,Seq2|T]) :-
   remove_complete_sequences_loop_ge(Seq1,Nodes,Seq2), !.

remove_complete_sequences_loop_ge(Seq1,[N|Ns],Seq3) :-
   member(N-X-Y,Seq1), !, ord_del_element(Seq1,N-X-Y,Seq2),
   remove_complete_sequences_loop_ge(Seq2,Ns,Seq3).
remove_complete_sequences_loop_ge(Seq1,[_|Ns],Seq2) :-
   remove_complete_sequences_loop_ge(Seq1,Ns,Seq2).
remove_complete_sequences_loop_ge(Seq1,[],Seq1).
 
remove_sequences_ge([MM,S,G,Seq1|T],Node,[MM,S,G,Seq2|T]) :-
   remove_sequences_loop_ge(Seq1,Node,Seq2), !.

remove_sequences_loop_ge([A-X1-Y1|Seq1],Node,[A-X2-Y2|Seq2]) :-
   remove_four_sequences_ge(X1,Node,X2),
   remove_two_sequences_ge(Y1,Node,Y2),
   remove_sequences_loop_ge(Seq1,Node,Seq2).
remove_sequences_loop_ge([],_,[]).
   
remove_two_sequences_ge([B|Y1],Node,Y2) :-
   Node = B, !,
   remove_two_sequences_ge(Y1,Node,Y2).
remove_two_sequences_ge([B|Y1],Node,[B|Y2]) :-
   remove_two_sequences_ge(Y1,Node,Y2).
remove_two_sequences_ge([],_,[]).

remove_four_sequences_ge([[B,C,_]|X1],Node,X2) :-
   ( Node = B ; Node = C ), !,
   remove_four_sequences_ge(X1,Node,X2).
remove_four_sequences_ge([[B,C,D]|X1],Node,[[B,C,D]|X2]) :-
   remove_four_sequences_ge(X1,Node,X2).
remove_four_sequences_ge([],_,[]).

remove_four_sequences_grouped_ge([BC1-D|X1],Node,[BC2-D|X2]) :-
   remove_four_sequences_loop_save_ge(BC1,Node,BC2),
   BC2 \== [], !,
   remove_four_sequences_grouped_ge(X1,Node,X2).
remove_four_sequences_grouped_ge([_|X1],Node,X2) :-
   remove_four_sequences_grouped_ge(X1,Node,X2).
remove_four_sequences_grouped_ge([],_,[]).

remove_four_sequences_loop_save_ge(BC1,Node,BC2) :-
   remove_four_sequences_loop_ge(BC1,Node,BC2), !.

remove_four_sequences_loop_ge([[B,C]|BC1],Node,BC2) :-
   ( Node = B ; Node = C ), !,
   remove_four_sequences_loop_ge(BC1,Node,BC2).
remove_four_sequences_loop_ge([[B,C]|BC1],Node,[[B,C]|BC2]) :-
   remove_four_sequences_loop_ge(BC1,Node,BC2).
remove_four_sequences_loop_ge([],_,[]).

inconsistent_ge([_,State|_]) :-
   member([],State).

marked_node_ge([Marked-_|_],Node) :-
   member(Node,Marked).

markable_node_ge([_-Markable|_],Node) :-
   member(Node,Markable).

% non_marked_neighbours_ge([Marked-_,_,Graph|_],Node,Nodes) :-
non_marked_neighbours_ge([Marked-_,_,[]|_],Node,Nodes) :-
   graph_ge(Graph),
   neighbours(Node,Graph,Neighbours),
   ord_subtract(Neighbours,Marked,Nodes).

mark_node_ge([Marked1-Markable1|T],Node,Status2) :- 
   ord_add_element(Marked1,Node,Marked2),
   ord_del_element(Markable1,Node,Markable2),
%  Status2 = [Marked2-Markable2|T].
   remove_sequences_ge([Marked2-Markable2|T],Node,Status2).

non_mark_nodes_ge([Marked-Markable1|T],Nodes,Status2) :-
   ord_subtract(Markable1,Nodes,Markable2),
%  Status2 = [Marked-Markable2|T].
   remove_complete_sequences_ge([Marked-Markable2|T],Nodes,Status2).
 
update_state_i_ge([MM,State1|T],Clause,[B],[MM,State4|T]) :-
   start_timer(update_state_ge),
   state_resolve(State1,B,State2), state_reduce(State1,B,State3), 
   ord_union([State2,State3,[Clause]],State4),
   add_timer(update_state_ge), !.

update_state_ii_ge([MM,State1|T],Clause,[B,C,D],[MM,State8|T]) :-  
   start_timer(update_state_ge),
   state_resolve(State1,D,State2), state_reduce(State1,D,State3), 
   ord_union(State2,State3,State4),
   ord_add_element(State4,Clause,State5),
   state_doubly_resolve(State5,[B,C],State6),
%  state_subtract(State6,State5,State7),
   ord_union(State5,State6,State8),
   add_timer(update_state_ge), !.

update_state_iii_ge([MM,State1|T],[B,C],[MM,State4|T]) :-
   start_timer(update_state_ge),
   state_doubly_resolve(State1,[B,C],State2),
%  state_subtract(State2,State1,State3),
   ord_union(State1,State2,State4),
   add_timer(update_state_ge), !.

state_doubly_resolve(State1,[A,B],State7) :-
   state_resolve(State1,A,State2), 
   state_reduce(State2,B,State3),
   state_resolve(State1,B,State4), 
   state_reduce(State4,A,State5),
   state_disjunction_basic(State3,State5,[],State6),
   list_to_ord_set(State6,State7).

state_prune_can(State1,State3) :-
   list_to_ord_set(State1,State2),
   state_can(State2,State3).

% init_state_ge([MM,State1,Graph,Seq|T],A,[MM,State2,Graph,Seq|T]) :-
init_state_ge([MM,State1,G,Seq|T],A,[MM,State2,G,Seq|T]) :-
   graph_ge(Graph),
   neighbours(A,Graph,Clause),
   ord_add_element(State1,Clause,State2).


binary_state_to_ugraph(State,Graph) :-
   ord_union(State,Nodes),
   binary_state_to_uedges(State,Edges),
   vertices_edges_to_ugraph(Nodes,Edges,Graph).
   
binary_state_to_uedges([[A,B]|Cs],[A-B,B-A|Edges]) :-
   binary_state_to_uedges(Cs,Edges).
binary_state_to_uedges([],[]).


ugraph_to_stars(Graph,State) :-
   vertices(Graph,Vertices),
   findall( Star,
            ( member(Vertex,Vertices),
              neighbours(Vertex,Graph,Neighbours),
              ord_add_element(Neighbours,Vertex,Star) ),
            Stars1 ),
   list_to_ord_set(Stars1,Stars2),
   state_can(Stars2,State).

/******************************************************************/



/******************************************************************/
/***     Module :  Egcwa-Operator for Binary Graphs, II         ***/
/******************************************************************/


expand_status_ge(Status1,Status2) :-
   Status1 = [M1-M2,S,G,Seq1|T],
   member(A-X-Y,Seq1), member(A,M1), !,
   ord_del_element(Seq1,A-X-Y,Seq2),
   group_four_sequences(X,X2),
%  write('#### '), writeln(A-X2-Y),
   expand_node_ge([M1-M2,S,G,Seq2|T],A-X2-Y,Status2).

expand_node_ge(Status1,A-X1-Y1,Status3) :-
%  write('expand_node'), writeln(A-X1-Y1),
   Status1 = [M1-M2,S,G,Seq1|T],
   append(Y3,[B|Y2],Y1), member(B,M2),
   ord_subtract(M2,Y3,M3), ord_intersection(M2,Y3,Nodes),
   remove_complete_sequences_loop_ge(Seq1,Nodes,Seq2),
   remove_reverse_i_ge(Seq2,A-[B|Y3],Seq3),
   step_i_ge([M1-M3,S,G,Seq3|T],[A,B],Status2),
   remove_four_sequences_grouped_ge(X1,B,X2),
   next_expand_node_ge(Status2,A-X2-Y2,Status3).
expand_node_ge(Status1,A-X-Y,Status3) :-
%  write('expand_node'), writeln(A-X-[]),
   Y \== [],
   Status1 = [M1-M2,S,G,Seq1|T],
   ord_subtract(M2,Y,M3), ord_intersection(M2,Y,Nodes),
   remove_complete_sequences_loop_ge(Seq1,Nodes,Seq2),
   remove_reverse_i_ge(Seq2,A-Y,Seq3), 
   expand_node_ge([M1-M3,S,G,Seq3|T],A-X-[],Status3).

expand_node_ge(Status1,A-[BC-D|X]-[],Status3) :-
   marked_node_ge(Status1,D), !,
   expand_node_iii_loop_ge(Status1,A-BC-D,Status2),
   next_expand_node_ge(Status2,A-X-[],Status3).
expand_node_ge(Status1,A-[BC-D|X]-[],Status3) :-
   markable_node_ge(Status1,D),
   expand_node_ii_loop_ge(Status1,A-BC-D,Status2),
   next_expand_node_ge(Status2,A-X-[],Status3).
   
expand_node_ge(Status1,A-[BC-D|X]-[],Status3) :-
   Status1 = [M1-M2,S,G,Seq1|T],
   ord_subtract(M2,[D],M3), ord_intersection(M2,[D],Nodes),
   remove_complete_sequences_loop_ge(Seq1,Nodes,Seq2),
   remove_reverse_ii_loop_ge(Seq2,A-BC-D,Seq3),
   expand_node_ge([M1-M3,S,G,Seq3|T],A-X-[],Status3). 
expand_node_ge(Status1,_-[]-[],Status2) :-
   expand_status_ge(Status1,Status2).

next_expand_node_ge(Status,_,Status) :-
   inconsistent_ge(Status), !.
next_expand_node_ge(Status1,A-X-Y,Status2) :-
   expand_node_ge(Status1,A-X-Y,Status2).


expand_node_ii_loop_ge(Status1,A-[[B,C]]-D,Status3) :-
   !,
   step_ii_ge(Status1,[A,B,C,D],Status2),
   next_expand_node_iii_loop_ge(Status2,A-[]-D,Status3).
expand_node_ii_loop_ge(Status1,A-[[B,C]|BC]-D,Status3) :-
   step_ii_prime_ge(Status1,[A,B,C,D],Status2),
   next_expand_node_iii_loop_ge(Status2,A-BC-D,Status3).

expand_node_iii_loop_ge(Status1,A-[[B,C]|BC]-D,Status3) :-
   step_iii_ge(Status1,[A,B,C,D],Status2),
   next_expand_node_iii_loop_ge(Status2,A-BC-D,Status3).
expand_node_iii_loop_ge(Status,_-[]-_,Status).

next_expand_node_iii_loop_ge(Status,_,Status) :-
   inconsistent_ge(Status), !.
next_expand_node_iii_loop_ge(Status1,A-BC-D,Status2) :-
   expand_node_iii_loop_ge(Status1,A-BC-D,Status2).

remove_reverse_i_ge(Seq1,A-[B|Y3],Seq3) :-
   remove_if_possible_i_ge(Seq1,[B,A],Seq2),
   remove_reverse_i_ge(Seq2,A-Y3,Seq3).
remove_reverse_i_ge(Seq,_-[],Seq).

remove_reverse_ii_ge(Seq1,A-[[B,C,D]|X3],Seq3) :-
   remove_if_possible_ii_ge(Seq1,[D,C,B,A],Seq2),
   remove_reverse_ii_ge(Seq2,A-X3,Seq3).
remove_reverse_ii_ge(Seq,_-[],Seq).

remove_reverse_ii_loop_ge(Seq1,A-[[B,C]|BC]-D,Seq3) :-
   remove_if_possible_ii_ge(Seq1,[D,C,B,A],Seq2),
   remove_reverse_ii_loop_ge(Seq2,A-BC-D,Seq3).
remove_reverse_ii_loop_ge(Seq,_-[]-_,Seq).
   
group_four_sequences(X1,X2) :-
   start_timer(group_four_sequences),
   findall( D,
            member([_,_,D],X1),
            D_List ),
   list_to_ord_set(D_List,Ds),
   findall( BC-D,
            ( member(D,Ds),
              findall( [B,C],
                       member([B,C,D],X1),
                       BC ) ),
            X2 ),
   add_timer(group_four_sequences),
   !.

/******************************************************************/


