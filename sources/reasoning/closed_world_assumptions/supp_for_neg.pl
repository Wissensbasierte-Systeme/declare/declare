

/******************************************************************/
/***                                                            ***/
/***           Module:  Support for Negation Sets               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* snd_snd_product(State,Supports1,Supports2,Supports3,Egcwa) <-
      the state disjunctions of all reduced sn--sets 
      in Supports1 with reduced sn--sets in Supports2 form 
      the derived reduced sn--sets in Supports3, which are 
      reduced w.r.t. State, empty reduced sn--sets are omitted,
      Egcwa is the set of all minimal conjunctions E, 
      whose reduced sn--set SN(E) is empty. */

snd_snd_product(State,Supports1,Supports2,Supports3,Egcwa) :-
   assert(state_triple(State,_,[])),
   assert(minimal_model_state(State)),
   snd_snd_product(Supports1,Supports2,[],Supports3,[],Egcwa),
   retract(state_triple(State,_,[])),
   retract(minimal_model_state(State)).

 
/* snd_snd_product(SND_1,SND_2,SND_3,Egcwa) <- 
      the set disjunction of all snd-sets in SND_1 with all snd-sets 
      of SND_2 is computed as SND_3, 
      new sn-sets [C3|SN(C3)], where SN(C3) is empty, are omitted,
      Egcwa is the set of all minimal C3, where SN(C3) is empty. */
 
snd_snd_product(SND_1,SND_2,SND_3,Egcwa) :-
   snd_snd_product(SND_1,SND_2,[],SND_3,[],Egcwa).

 
/* snd_snd_product(SND_1,SND_2,Sofar,SND_3,Egcwa_1,Egcwa_2) <-
      the set disjunction of all snd-sets in SND_1 with all snd-sets
      of SND_2 is computed and added to Sofar yielding SND_3,
      new sn-sets [C3|SN(C3)], where C3 is subsumed by Egcwa_1, are
      removed from SND_3, all C3, where SN(C3) is empty, are added to
      Egcwa_1 yielding Egcwa_2. */
 
snd_snd_product([S1|SND_1],SND_2,Sofar,SND,Egcwa_1,Egcwa_3) :-
   snd_snd_product_loop(S1,SND_2,SND_3,Egcwa_1,Egcwa_2),
   append(SND_3,Sofar,New_Sofar),
   !,
   snd_snd_product(SND_1,SND_2,New_Sofar,SND,Egcwa_2,Egcwa_3).
snd_snd_product([],_,SND,SND,Egcwa,Egcwa).
 
snd_snd_product_loop([C1|Cs1],[[C2|Cs2]|SND_2],SND_3,Egcwa_1,Egcwa_2) :-
   ord_union(C1,C2,C3),
   ( state_subsumes(Egcwa_1,[C3])
   ; state_triple_subsumes_n_fact(C3) ),
   !,
   writeln('subsumed     '),
   length(C1,L1), length(C2,L2), length(C3,L3),
   length(Cs1,J1), length(Cs2,J2), J3 is J1 * J2,
   pp_numbers([L1,L2,L3]),
   pp_numbers([J1,J2,J3]),
   snd_snd_product_loop([C1|Cs1],SND_2,SND_3,Egcwa_1,Egcwa_2).
snd_snd_product_loop(S1,[S2|SND_2],[[C3|Cs3]|SND_3],Egcwa_1,Egcwa_2) :-
   sn_sn_product(S1,S2,[C3|Cs3]),
   Cs3 \== [],
   !,
   snd_snd_product_loop(S1,SND_2,SND_3,Egcwa_1,Egcwa_2).
snd_snd_product_loop([C1|Cs1],[[C2|_]|SND_2],SND_3,Egcwa_1,Egcwa_2) :-
   ord_union(C1,C2,C3),
   !,
   snd_snd_product_loop([C1|Cs1],SND_2,SND_3,[C3|Egcwa_1],Egcwa_2).
snd_snd_product_loop(_,[],[],Egcwa_1,Egcwa_1).
 

/* sn_sn_product([C1|Cs1],[C2|Cs2],[C3|Cs3]) <-
      Cs3 is equivalent to the reduced sn-set for C3 = C1 ^ C2. */

sn_sn_product([C1|Cs1],[C2|Cs2],[C3|Purified_Cs3]) :-
   ddk_time(T0),
   ord_union(C1,C2,C3),
   length(C1,L1), length(C2,L2), length(C3,L3), add_num([L3,snnumber],1),
   length(Cs1,J1), length(Cs2,J2), J3 is J1 * J2,
   ddk_time(T1),
   snd_reduce([C1|Cs1],[C2|Cs2],[C1|S1],[C2|S2]),
%  S1 = Cs1, S2 = Cs2, 
   length(S1,K1), length(S2,K2), K3 is K1 * K2,
   ddk_time(T2),
   state_disjunction_optimized(S1,S2,S3), length(S3,I1),
   ddk_time(T3),
   state_prune(S3,Pruned_S3,I2),
   ddk_time(T4),
   minimal_model_state(Msp),
   state_subtract(Pruned_S3,Msp,Cs3), 
   length(Cs3,I3),
%  ordered_minimal_model_state(MSp),
%  ordered_state_subtract(Pruned_S3,Msp,Cs3), 
%  tree_compute_delta_facts(Pruned_S3,Cs3,I3),
   ddk_time(T5),
%  state_can(Cs3,Purified_Cs3,I4),
   Purified_Cs3 = Cs3, length(Purified_Cs3,I4),
   ddk_time(T6),
   add_num([L3,sncard],I4),
   T_snd_reduce is ( T2 - T1 ) / 1000, add_num([L3,snd_reduce],T_snd_reduce),
   T_product is ( T3 - T2 ) / 1000, add_num([L3,product],T_product),
   T_prune is ( T4 - T3 ) / 1000, add_num([L3,prune],T_prune),
   T_delta is ( T5 - T4 ) / 1000, add_num([L3,delta],T_delta),
   T_purify is ( T6 - T5 ) / 1000, add_num([L3,purify],T_purify),
   pp_numbers([L1,L2,L3]),
   pp_numbers([J1,J2,J3]),
   pp_numbers([K1,K2,K3]),
   pp_numbers([I1,I2,I3,I4]),
   add_num([L3,j3],J3), add_num([L3,k3],K3), 
   add_num([L3,i1],I1), add_num([L3,i2],I2),
   add_num([L3,i3],I3), add_num([L3,i4],I4),
   ddk_time(T7),
   T_doss is ( T7 - T0 ) / 1000, add_num([doss],T_doss).


/* snd_reduce([C1|Cs1],[C2|Cs2],[C1|Pruned_Cs1],[C2|Pruned_Cs2]) <-
      removes from Cs1 all clauses which are not disjoint from C2,
      removes from Cs2 all clauses which are not disjoint from C1. */

snd_reduce([C1|Cs1],[C2|Cs2],[C1|Pruned_Cs1],[C2|Pruned_Cs2]) :-
   write('snd_reduce   '),
   start_timer(snd_reduce),
   state_reduce2(Cs1,C2,Pruned_Cs1),
   state_reduce2(Cs2,C1,Pruned_Cs2),
   stop_timer(snd_reduce).


/*** implementation **************************************************/


/* state_to_sn_sets(State,Supports) <-
      Supports is the list of all lists [[A]|SN(A)] 
      for the atoms A in the disjunctive Herbrand state State. */

state_to_sn_sets(State,Supports) :-
   state_to_atoms(State,Atoms),
   state_to_sn_sets(State,Atoms,Supports).

state_to_sn_sets(_,[],[]).
state_to_sn_sets(State,[A|As],[[[A]|Cs]|Supports]) :-
   state_resolve(State,A,Cs),
   state_to_sn_sets(State,As,Supports).


/* state_to_sn_sets2(State,Supports) <-
      Supports is the list of all lists [[A]|SN(A)]
      for those A in the disjunctive Herbrand state State,
      where SN(A) doesn't contain the empty clause. */

state_to_sn_sets2(State,Supports) :-
   state_to_atoms(State,Atoms),
   state_to_sn_sets2(State,Atoms,Supports).

state_to_sn_sets2(_,[],[]).
state_to_sn_sets2(State,[A|As],[[[A]|Cs]|Supports]) :-
   state_resolve(State,A,Cs),
   \+ member([],Cs),
   !,
   state_to_sn_sets2(State,As,Supports).
state_to_sn_sets2(State,[_|As],Supports) :-   
   state_to_sn_sets2(State,As,Supports).


/* sn_insert_length([C1|Cs1],SND_2,SND_3) <-
     inserts the snd-set [C1|Cs1] in the set SND_2 of snd-sets
     yielding the new set SND_3 of snd-sets. */

sn_insert_length([C1|Cs1],[[C2|Cs2]|SND_2],SND_3) :-
  length(C1,I1), length(Cs1,I2),
  length(C2,J1), length(Cs2,J2),
  I1 =< J1, I2 =< J2,
  !,
  append([[C1|Cs1],[C2|Cs2]],SND_2,SND_3).
sn_insert_length([C1|Cs1],[[C2|Cs2]|SND_2],[[C2|Cs2]|SND_3]) :-
  sn_insert_length([C1|Cs1],SND_2,SND_3).
sn_insert_length([C1|Cs1],[],[[C1|Cs1]]).

sn_sort_by_length(L1,L2) :-
  sn_sort_by_length(L1,[],L2).
sn_sort_by_length([H|T],List,Erglist) :-
  sn_insert_length(H,List,List1),
  sn_sort_by_length(T,List1,Erglist).
sn_sort_by_length([],L,L).

sn_sort_by_length_check([]).
sn_sort_by_length_check([_|[]]).
sn_sort_by_length_check([[H1|_]|[[H2|T2]|T]]) :-
  length(H1,I), length(H2,J),
  I =< J,
  sn_sort_by_length_check([[H2|T2]|T]).


/* remove_empty_list_sn(Supports_1,Supports_2) <-
      the list Supports_2 is the the list of all sn_sets [C|Cs] 
      in Supports_1, such that Cs does not contain the empty clause. */
 
remove_empty_list_sn([[_|Cs]|Supports_1],Supports_2) :-
   member([],Cs),
   !,
   remove_empty_list_sn(Supports_1,Supports_2).
remove_empty_list_sn([Support|Supports_1],[Support|Supports_2]) :-
   remove_empty_list_sn(Supports_1,Supports_2).
remove_empty_list_sn([],[]).
 
 
/* sn_delete(UCSN,RemainUCSN,Collector,subSN,SN) <-
      collects all unmarked clauses for the Atom
      collector. ReamainUCSN is the remaining
      uncollected SN-List. */
 
sn_delete([],[],_,L,L).
sn_delete([[H1|T1]|Tail],RT,H2,L1,L3) :-
   H1 == H2, !,
   ord_union(L1,T1,L2),
   sn_delete(Tail,RT,H2,L2,L3).
sn_delete([X|Tail],[X|RT],H2,L1,L2) :-
   sn_delete(Tail,RT,H2,L1,L2).
 
 
/* sn_sum_up(UCSN-Sets,SN-Sets) <-
      sums up the postive unmarked clauses for each literal (creating the
      support-for-negation-sets).
      example:
      UCSN-Sets  = [[a,[f]],[a,[e]],[c,[]],[e,[f]],[f,[e]],[d,[]],[g,[]],
                    [q,[d]],[q,[e,f]]]
      -> SN-Sets = [[a,[f],[e]],[c,[]],[e,[f]],[f,[e]],[d,[]],
                    [g,[]],[q,[d],[e,f]]]. */
 
sn_sum_up([],[]).
sn_sum_up([[X|T]|Tail],Liste) :-
   sn_delete([[X|T]|Tail],L1,X,[],SN),
   sn_sum_up(L1,L2),
   ord_union([[X|SN]],L2,Liste).


length_order_minimal_model_state :-
   minimal_model_state(MSp),
   abolish(ordered_minimal_model_state,1),
   length_order_2(MSp,O_MSp),
   aggregate_on_first_component(O_MSp,OO_MSp),
   o_tree_transform_o_msp(OO_MSp,O_Tree),
%  project_on_second_components(O_MSp,OO_MSp),
   project_on_second_components(O_Tree,OO_Tree),
   assert(ordered_minimal_model_state(OO_Tree)).
%  assert(ordered_minimal_model_state(OO_MSp)).

project_on_second_components([[_,Y]|Pairs],[Y|Ys]) :-
   project_on_second_components(Pairs,Ys).
project_on_second_components([],[]).

aggregate_on_first_component([[I,C]|Pairs],Agg) :-
   aggregate_on_first_component(Pairs,[I,[C]],[],Agg1),
   reverse(Agg1,Agg).
aggregate_on_first_component([],[]).

aggregate_on_first_component([[I,C]|Pairs],[I,Cs],Sofar,Agg) :-
   !,
   aggregate_on_first_component(Pairs,[I,[C|Cs]],Sofar,Agg).
aggregate_on_first_component([[I,C]|Pairs],[J,Cs],Sofar,Agg) :-
   aggregate_on_first_component(Pairs,[I,[C]],[[J,Cs]|Sofar],Agg).
aggregate_on_first_component([],[J,Cs],Sofar,[[J,Cs]|Sofar]).

o_tree_transform_o_msp([[I,D]|IDs],[[I,Tree]|ITrees]) :-
   tree_clauses_insert([],Tree,D),
   o_tree_transform_o_msp(IDs,ITrees).
o_tree_transform_o_msp([],[]).

analyse_snd_sets(SND) :-
   writeln('snd overlap            '),
   delete_first_elements(SND,Set_of_Sets),
   state_overlap_ratio(Set_of_Sets,Or),
   write('                       '),
   write('  o_s = '), format("~2f",Or), writeln(' ').

delete_first_elements([[_|Cs]|Sets],[Cs|Sets1]) :-
   delete_first_elements(Sets,Sets1).
delete_first_elements([],[]).

 
/******************************************************************/


