

disjunction_of_smr_sets([State1,State2],State3) :-
   disjunction_of_smr_sets(State1,State2,State3).  
disjunction_of_smr_sets([State1|States1],State3) :-
   disjunction_of_smr_sets(States1,State2),   
   disjunction_of_smr_sets(State1,State2,State3). 
disjunction_of_smr_sets([],[[]]).


disjunction_of_smr_sets([C1|State1],[C2|State2],[C3|State3]) :-
   state_hyperremove(State1,C2,State4), 
   state_hyperremove(State2,C1,State5),
   ord_union(State4,State5,State6),
   state_prune(State6,State7),
   state_can(State7,State3),
   ord_union(C1,C2,C3).


state_to_sm_set(State,Atom,Sm_Set) :-
   state_resolve(State,Atom,Resolvent),
   tree_based_boolean_dualization(Resolvent,Sm_Set).

% for all atoms, cf. supp_for_neg

% state_to_sm_sets(State,Sm_Sets) :-


state_hyperremove(State1,Clause,State2) :-
   state_hyperresolve(State1,Clause,Resolvent),
   state_hyperreduce(State1,Clause,Reduction),
   ord_union(Resolvent,Reduction,State3),
   state_prune(State3,State4),
   state_can(State4,State2).

state_remove(State1,Atom,State2) :-
   state_resolve(State1,Atom,Resolvent),
   state_reduce(State1,Atom,Reduction),
   state_subtract(Reduction,Resolvent,Diff),
   ord_union(Resolvent,Diff,State2).
   

state_hyperresolve(State1,Clause,State2) :-
   state_hyperresolve_loop(State1,Clause,States),
   state_disjunction(States,State2).

state_hyperresolve_loop(State,[A|As],[Res|States]) :-
   state_resolve(State,A,Res),
   state_hyperresolve_loop(State,As,States).
state_hyperresolve_loop(_,[],[]).


state_hyperreduce(State1,[A|As],State3) :-
   state_reduce(State1,A,State2),
   state_hyperreduce(State2,As,State3).
state_hyperreduce(State,[],State).


