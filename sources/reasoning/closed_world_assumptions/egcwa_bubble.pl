

/******************************************************************/
/***                                                            ***/
/***         DisLog :  Garlands and Bubbles                     ***/
/***                                                            ***/
/******************************************************************/
 


/*** interface ****************************************************/


stgt(State) :-
   state_to_garland_tree(State,Garland_Tree),
   dportray(tree,Garland_Tree).


/* state_to_garland_tree(State,Garland_Tree) <-
      */

state_to_garland_tree(State,Garland_Tree) :-
   state_to_garlands(State,Garlands),
   garlands_to_tree(Garlands,Garland_Tree).


/* garlands_to_tree(Garlands,Tree) <-
      from the garlandlist Garlands is built a tree Tree */

garlands_to_tree(Garlands,Tree) :-
   garland_tree_clauses_lists_insert([],Tree,Garlands).


/* state_to_garlands(State,Garlands) <-
      for the state State is Garlands the list of all garlands,
      the list is of the form
      [[bubble1|Allgarlands1],[bubble2|Allgarlands2],...] */

state_to_garlands(State,Garlands) :-
   get_single_garland_loop(State,Garlandlist1),
   delete_problematic_bubbles(Garlandlist1,Garlands2),
   garland_list_to_garlands(Garlands2,Garlands).


/*** implementation ***********************************************/


/* get_single_garland_loop(State,Garlands) <-
      foreach bubble in the state Sate there ist a list of all
      possible garlands in Garlands */

get_single_garland_loop(State,Garlands) :-
   get_single_garland_loop(State,State,[],Garlands).
get_single_garland_loop(State,[D|Ds],Sofar,Ws) :-
   get_single_garland(State,D,W),
   get_single_garland_loop(State,Ds,[[D|W]|Sofar],Ws).
get_single_garland_loop(_,[],Sofar,Sofar).


/* get_single_garland(State,Clause,W) <-
      W ist the list of all sets, from which we can choose one
      element, to get a garland for the bubble Clause in the
      state State */

get_single_garland(State,Clause,W) :-
   get_single_garland(State,Clause,Clause,[],W).
get_single_garland(State,Clause,[A|As],Sofar,W) :-
   get_edges(State,Clause,A,E),
   get_single_garland(State,Clause,As,[E|Sofar],W).
get_single_garland(_,_,[],Sofar,Sofar).


/* get_edges(State,D,A,E3) <-
      E3 ist the set of all disjunctions of the state State, which
      contain the Literal A, and which are not in the disjunction D */

get_edges(State,D,A,E3) :-
   get_edges_loop(State,A,E1),
   ord_union(E1,E2),
   ord_subtract(E2,D,E3).


/* get_edges_loop(State,Atom,List) <-
      List ist the set of all disjunction of the state State which
      contain then Literal Atom */

get_edges_loop(State,Atom,List) :-
   get_edges_loop(State,Atom,[],List).
get_edges_loop([D|Ds],A,Sofar,Ds2) :-
   member(A,D),!,
   get_edges_loop(Ds,A,[D|Sofar],Ds2).
get_edges_loop([_|Ds],A,Sofar,Ds2) :-
   get_edges_loop(Ds,A,Sofar,Ds2).
get_edges_loop([],_,Sofar,Sofar).


/* delete_problematic_bubbles(G1,G2) <-
      G2 ist the list of all those elements in G1, wich do not
      contain the empty list */

delete_problematic_bubbles(L,R) :-
   delete_problematic_bubbles(L,[],R).
delete_problematic_bubbles([H|T],Sofar,R) :-
   member([],H),
   !,
   delete_problematic_bubbles(T,Sofar,R).
delete_problematic_bubbles([H|T],Sofar,R) :-
   delete_problematic_bubbles(T,[H|Sofar],R).
delete_problematic_bubbles([],Sofar,Sofar).


/* garland_list_to_garlands(Garlist,Garlands) <-
      the list Garlist which represent the information for the
      are multiplied to get the explizit garlands Garlands */

garland_list_to_garlands([],[]).
garland_list_to_garlands([[D|G]|Gl],[[D|G2]|G3]) :-
   garland_list_to_garlands(Gl,G3),
   cartesian_product(G,G2).


/* garland_tree_clauses_insert(Tree1,Tree3,List) <-
      All the garlands in List were asserted in in the Tree1,
      yielding Tree2 */

garland_tree_clauses_lists_insert(Tree1,Tree3,[[B|List]|Lists]) :-
   garland_tree_clauses_insert(Tree1,Tree2,[B],List),
   garland_tree_clauses_lists_insert(Tree2,Tree3,Lists).
garland_tree_clauses_lists_insert(Tree,Tree,[]).


garland_tree_clauses_insert(Tree1,Tree2,B,[H|T]) :-
   garland_tree_clause_insert(Tree1,Tree3,B,H),
   garland_tree_clauses_insert(Tree3,Tree2,B,T).
garland_tree_clauses_insert(Tree,Tree,_,[]).

   
garland_tree_clause_insert([],[Clause,[Bubble]],Bubble,Clause) :-
   !.
garland_tree_clause_insert(Tree,New_Tree,Bubble,Clause) :-
   best_intersection(Tree,Clause,Sequence,_),
   garland_tree_clause_insert(Tree,New_Tree,Bubble,Clause,Sequence),
   !.
garland_tree_clause_insert([N|Ts],[N,[Clause1|[[B]]]|Ts],B,Clause,[]) :-
   ord_subset(N,Clause),
   !,
   ord_subtract(Clause,N,Clause1).
garland_tree_clause_insert([N|Ts],[N1,[Clause1|[[B]]],[N2|Ts]],B,Clause,[]) :-
   !,
   ord_intersection(N,Clause,N1),
   ord_subtract(Clause,N,Clause1),
   ord_subtract(N,Clause,N2).
garland_tree_clause_insert([N|Ts],[N|New_Ts],Bubble,Clause,S) :-
   ord_subtract(Clause,N,Clause1), 
   garland_trees_clause_insert(Ts,New_Ts,Bubble,Clause1,S).
garland_trees_clause_insert([Tree|Trees],[New_Tree|Trees],Bubble,
			    Clause,[I|S]) :-
   I = 1,
   !,
   garland_tree_clause_insert(Tree,New_Tree,Bubble,Clause,S).
garland_trees_clause_insert([Tree|Trees],[Tree|New_Trees],Bubble,
			    Clause,[I|S]) :-
   II is I - 1,
   garland_trees_clause_insert(Trees,New_Trees,Bubble,Clause,[II|S]).


/******************************************************************/


