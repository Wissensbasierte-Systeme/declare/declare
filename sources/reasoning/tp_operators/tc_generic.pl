

/******************************************************************/
/***                                                            ***/
/***           DDK:  Generic Transitive Closure                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* transitive_closure(Predicate, X, Y) <-
      */

transitive_closure(Predicate, X, Y) :-
   findall(  [arc(U, V)],
      call(Predicate, U, V),
      Facts ),
   tc_program_generic(tc, arc, Tc_Rules),
   append(Facts, Tc_Rules, Tc_Program),
   ( ( ground(X)
     ; ground(Y) ) ->
     magic_sets_evaluation(Tc_Program, tc(X, Y), Interpretation),
     member([tc(X, Y)], Interpretation)
   ; tp_iteration(Tc_Program, Interpretation),
     member(tc(X, Y), Interpretation) ).


/* transitive_closure_2(Edges, Tc_Edges) <-
      */

transitive_closure_2(Edges, Tc_Edges) :-
   transitive_closure_edges(Edges, Tc_Edges).


/* transitive_closure_edges(Edges, Tc_Edges) <-
      */

transitive_closure_edges(Edges, Tc_Edges) :-
   ( foreach(Edge, Edges), foreach(Fact, Facts) do
        Edge = X-Y, Fact = [arc(X, Y)] ),
   tc_program_generic(tc, arc, Tc_Rules),
   append(Facts, Tc_Rules, Tc_Program),
   tp_iteration(Tc_Program, Interpretation),
   findall( X-Y,
      member(tc(X, Y), Interpretation),
      Tc_Edges ).


/* tc_program_generic(+Tc, +Arc, -Program) <-
      */

tc_program_generic(Tc, Arc, Program) :-
   Tc_xy =.. [Tc, X, Y], Arc_xy =.. [Arc, X, Y],
   Arc_xz =.. [Arc, X, Z], Tc_zy =.. [Tc, Z, Y],
   Program = [
      [Tc_xy]-[Arc_xy],
      [Tc_xy]-[Arc_xz, Tc_zy] ].


/* tc_program_generic(Type, Tc, Arc, Program) <-
      */

tc_program_generic(Tc, Arc, Program) :-
   tc_program_generic(r, Tc, Arc, Program).

tc_program_generic(r, Tc, Arc, Program) :-
   Tc_xy =..  [Tc, X, Y],
   Arc_xy =.. [Arc, X, Y],
   Arc_xz =.. [Arc, X, Z], 
   Tc_zy =..  [Tc, Z, Y],
   Program = [
      [Tc_xy]-[Arc_xy],
      [Tc_xy]-[Arc_xz, Tc_zy] ].

tc_program_generic(l, Tc, Arc, Program) :-
   Tc_xy =..  [Tc, X, Y],
   Arc_xy =.. [Arc, X, Y],
   Tc_xz =..  [Tc, X, Z],
   Arc_zy =.. [Arc, Z, Y],
   Program = [
      [Tc_xy]-[Arc_xy],
      [Tc_xy]-[Tc_xz, Arc_zy] ].

tc_program_generic(lr, Tc, Arc, Program) :-
   Tc_xy =..  [Tc, X, Y],
   Arc_xy =.. [Arc, X, Y],
   Tc_xz =..  [Tc, X, Z],
   Tc_zy =..  [Tc, Z, Y],
   Program = [
      [Tc_xy]-[Arc_xy],
      [Tc_xy]-[Tc_xz, Tc_zy] ].


/******************************************************************/


