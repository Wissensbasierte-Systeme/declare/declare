

/******************************************************************/
/***                                                            ***/
/***     Tp-Operator:  Mixed Bottom-Up/Top-Down Evaluation      ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(tp_operator, gen_1) :-
   Program = [
      (a :- (b;c), writeln('ok')),
      (b :- assert(d)),
      (c :- fail),
      (d :- d) ],
   tp_iteration_prolog(Program, prolog_module, [], I),
   writeln(user, I).


/*** interface ****************************************************/


/* tp_iteration_prolog_file(File_Program, Module, I1, I2) <-
      */

tp_iteration_prolog_file(File_Program, Module, I1, I2) :-
   prolog_consult(File_Program, Program),
   tp_iteration_prolog(Program, Module, I1, I2).


/* tp_iteration_prolog_muliple(Programs, Module, I1, I2) <-
      */

tp_iteration_prolog_muliple([Program|Programs], Module, I1, I2) :-
   predicate_to_file( user, star_line ),
   writeln(user, 'Stratum\n'), ttyflush,
   predicate_to_file( user, dwrite(pl, user, Program) ),
   writeln('Tp Iteration:'), ttyflush,
   tp_iteration_prolog(Program, Module, I1, I3),
   ord_subtract(I3, I1, I4),
   write('I_* ='), dportray(hi, I4),
   predicate_to_file( user, write('+ ') ),
   tp_iteration_prolog_number_of_facts(I4),
   predicate_to_file( user, write('= ') ),
   tp_iteration_prolog_number_of_facts(I3),
   nl, ttyflush,
   tp_iteration_prolog_muliple(Programs, Module, I3, I2).
tp_iteration_prolog_muliple([], _, I, I) :-
   predicate_to_file( user, star_line ).

tp_iteration_prolog_number_of_facts(Interpretation) :-
   length(Interpretation, N),
   predicate_to_file( user,
      ( write_list([N, ' ', facts]), nl ) ).


/* tp_iteration_prolog(Program, Module, I1, I2) <-
      */

tp_iteration_prolog(Program, Module, I1, I2) :-
   assert_facts(Module, I1),
   prolog_program_execute_dynamics(Program, Module, Program_2),
   tp_iteration_prolog_(Program_2, Module, I1, I2),
   ( foreach(A, I2) do ( retract(Module:A) ; true ) ).
%  prolog_program_to_head_predicates(I2, Ps_2),
%  ( foreach(P, Ps_2) do
%       Module:abolish(P) ).

tp_iteration_prolog_(Program, Module, I1, I2) :-
   tp_iteration_prolog_(1, Program, Module, I1, I2).

tp_iteration_prolog_(N, Program, Module, I1, I2) :-
   tp_operator_prolog(Program, Module, Facts),
   list_to_ord_set(Facts, Facts_2),
   ord_subtract(Facts_2, I1, Facts_3),
   write_list(['I_', N, ' =']),
   dportray(hi, Facts_3),
   sublist( fact_is_not_in_module(Module),
      Facts_2, Facts_4 ),
   assert_facts(Module, Facts_4),
   ( ( Facts_3 = [],
       !,
       I2 = I1 )
   ; ord_union(I1, Facts_3, I3),
     M is N + 1,
     tp_iteration_prolog_(M, Program, Module, I3, I2) ).

fact_is_not_in_module(Module, Fact) :-
   \+ clause(Module:Fact, true).


/* tp_operator_prolog(Program, Module, Facts) <-
      */

tp_operator_prolog(Program, Module, Facts) :-
   findall( A,
      ( member(Rule, Program),
        parse_prolog_rule(Rule, A :- B),
        Module:call(B) ),
      Facts ),
%  writeln(user, tp(Program, Module, Facts)),
   tp_operator_prolog_message(Facts).

tp_operator_prolog_message(_) :-
   !.
tp_operator_prolog_message(Facts) :-
   length(Facts, N),
   write_list(user, ['T_P: ', N, ' * ']).


/* parse_prolog_rule(Rule_1, Rule_2) <-
      */

parse_prolog_rule(A :- B, A :- B) :-
   !.
parse_prolog_rule(:- B, false :- B) :-
   !.
parse_prolog_rule(A, A :- true).


/* prolog_program_execute_dynamics(
         Program_1, Module, Program_2) <-
      */

prolog_program_execute_dynamics(Program_1, Module, Program_2) :-
   findall( Rule,
      ( member(Rule, Program_1),
        ( Rule = (:- dynamic(Ps)),
          Module:dynamic(Ps),
          fail
        ; Rule \= (:- dynamic(_)) ) ),
      Program_2 ),
   prolog_program_to_head_predicates(Program_2, Ps),
   ( foreach(P, Ps) do
        Module:dynamic(P) ).


/* prolog_program_without_dynamics(Program_1, Program_2) <-
      */

prolog_program_without_dynamics(Program_1, Program_2) :-
   findall( Rule,
      ( member(Rule, Program_1),
        Rule \= (:- dynamic(_)) ),
      Program_2 ).


prolog_program_without_ddbase_connect(Program_1, Program_2) :-
   findall( Rule,
      ( member(Rule, Program_1),
        Rule \= (:- ddbase_connect(_, _)),
        Rule \= (:- ddbase_connect_(_, _)) ),
      Program_2 ).

prolog_program_without_stratify(Program_1, Program_2) :-
   findall( Rule,
      ( member(Rule, Program_1),
        Rule \= (:- stratify _) ),
      Program_2 ).


/* dislog_program_without_dynamics(Program_1, Program_2) <-
      */

dislog_program_without_dynamics(Program_1, Program_2) :-
   findall( Rule,
      ( member(Rule, Program_1),
        Rule \= []-[dynamic(_)] ),
      Program_2 ).


/* prolog_program_to_head_predicates(Program, Predicates) <-
      */

prolog_program_to_head_predicates(Program, Predicates) :-
   findall( P/N,
      ( member(Rule, Program),
        parse_prolog_rule(Rule, A :- _),
        functor(A, P, N),
        \+ member(P/N, [false/0, ;/2]) ),
      Predicates ).


/******************************************************************/


