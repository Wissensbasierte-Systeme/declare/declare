

/******************************************************************/
/***                                                            ***/
/***     Declare:  Hres-Iteration                               ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


/* hres(+Program, +Int_1, -Int_2) <-
      */

hres(Program, Int_1, Int_2) :-
   findall( A,
      ( member(Rule, Program),
        parse_rule(Rule, [A]-Body),
        members_1(Body, Int_1) ),
      Int_2 ).


/* hres_iteration(+Program, -Interpretation) <-
      */

hres_iteration(Program, Interpretation) :-
   hres_iteration(Program, [], Interpretation).

hres_iteration(Program, Int_1, Int_2) :-
   writeln(Int_1),
   hres(Program, Int_1, Int_3),
   sort(Int_3, Int_4),
   Int_4 \= Int_1,
   !,
   hres_iteration(Program, Int_4, Int_2).
hres_iteration(_, Int, Int).


/*** implementation ***********************************************/


/* members_...(Xs, Ys) <-
      */

members_1(Xs, Ys) :-
   foreach(X, Xs) do member(X, Ys).

members_2([X|Xs], Ys) :-
   member(X, Ys),
   members_2(Xs, Ys).
members_2([], _).


/******************************************************************/


