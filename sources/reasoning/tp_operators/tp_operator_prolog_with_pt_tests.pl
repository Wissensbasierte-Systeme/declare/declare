

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Proof Trees - Tests                   ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(proof_trees, 1) :-
   dislog_variable_get(example_path,
      'deductive/transitive_closure/tc_arc.pl', File),
   dread(lp, File, Program),
   tp_iteration_dislog_with_proof_trees(
      Program, module, [], Atoms),
   ( foreach(tree(_, Tree), Atoms) do dwrite(xml, Tree) ),
   star_line.

test(proof_trees, 2) :-
   Program = [
      r = [tc(X, Y)]-[arc(X, Z), tc(Z, Y)],
      e = [tc(X, Y)]-[arc(X, Y)],
      f1 = [arc(a, b)],
      f2 = [arc(b, c)],
      f3 = [arc(c, d)] ],
   Query = _,
   tp_iteration_dislog_with_proof_trees(Program, Query).

test(proof_trees, '2adf') :-
   Program = [
      r = [anc(X, Y)]-[par(X, Z), anc(Z, Y)],
      e = [anc(X, Y)]-[par(X, Y)],
      f1 = [par(a, d)],
      f2 = [par(d, f)] ],
   Query = anc(a, f),
   tp_iteration_dislog_with_proof_trees(Program, Query).

test(proof_trees, 'Muenchen') :-
   Program = [
      r = [route(X, Y, L)]-[street(X, Z, N), route(Z, Y, M), prolog:(L is N+M)],
      e = [route(X, Y, N)]-[street(X, Y, N)],
      f1 = [street('KT', 'W�', 15)],
      f2 = [street('W�', 'M�', 280)] ],
   Query = _,
   tp_iteration_dislog_with_proof_trees(Program, Query).

test(proof_trees, Mode) :-
   member(Mode, [l, r, lr]),
   tc_program_generic(Mode, anc, par, Rules_1),
   pair_lists(=, [e, r], Rules_1, Rules_2),
   Facts = [
      f1 = [par(a1, a2)],
      f2 = [par(a2, a3)],
      f3 = [par(a3, a4)],
      f4 = [par(a4, a5)] ],
   append(Rules_2, Facts, Program),
   Query = anc(a1, a5),
   tp_iteration_dislog_with_proof_trees(Program, Query).

test(proof_trees, 'l2') :-
   Program = [
      r2 = [anc(X, Y)]-[anc(X, Z1), par(Z1, Z2), par(Z2, Y)],
      e1 = [anc(X, Y)]-[par(X, Z), par(Z, Y)],
      e = [anc(X, Y)]-[par(X, Y)],
      f1 = [par(a1, a2)],
      f2 = [par(a2, a3)],
      f3 = [par(a3, a4)],
      f4 = [par(a4, a5)] ],
   Query = anc(a1, a5),
   tp_iteration_dislog_with_proof_trees(Program, Query).

test(proof_trees, 3) :-
   Program = [
      r = [p(X, Y)]-[up(X, X1), p(X1, Y1), down(Y1, Y)],
      e = [p(X, Y)]-[flat(X, Y)],
      f1 = [up(a1, a2)],
      f2 = [up(a2, a3)],
      f3 = [flat(a3, b3)],
      f4 = [down(b3, b2)],
      f5 = [down(b2, b1)] ],
   Query = p(a1, b1),
   tp_iteration_dislog_with_proof_trees(Program, Query).

test(proof_trees, 4) :-
   Program = [
      r = [p(X, Y)]-[up(X, X1), p(Y1, X1), down(Y1, Y)],
      e = [p(X, Y)]-[flat(X, Y)],
      f1 = [up(a1, a2)],
      f2 = [down(a3, a2)],
      f3 = [flat(a3, b3)],
      f4 = [up(b2, b3)],
      f5 = [down(b2, b1)] ],
   Query = p(a1, b1),
   tp_iteration_dislog_with_proof_trees(Program, Query).

test(proof_trees, '42') :-
   Program = [
      r2 = [p(X, Y)]-[
         up(X, X1), down(X2, X1), p(X2, Y2),
         up(Y1, Y2), down(Y1, Y)],
      e1 = [p(X, Y)]-[up(X, X1), flat(Y1, X1), down(Y1, Y)],
      e = [p(X, Y)]-[flat(X, Y)],
      f1 = [up(a1, a2)],
      f2 = [down(a3, a2)],
      f3 = [flat(a3, b3)],
      f4 = [up(b2, b3)],
      f5 = [down(b2, b1)] ],
   Query = p(a1, b1),
   tp_iteration_dislog_with_proof_trees(Program, Query).
test(proof_trees, 5) :-
   dislog_variable_get(example_path,
       'datalogs/append_support.pl', File_1),
   [File_1],
   dislog_variable_get(example_path,
       'datalogs/append.pl', File_2),
   dread(lp, File_2, Program),
   Query = _,
   tp_iteration_dislog_with_proof_trees(Program, Query).

test(proof_trees, 6) :-
   Program = [
%     r = (tc(X, Y, C) :-
%        ( arc(X, Z), tc(Z, Y, B), C is 1+B
%        ; arc(X, Y), {C = 1} ) ),
      r = (tc(X, Y, C) :- arc(X, Z), tc(Z, Y, B), C is 1+B),
      e = (tc(X, Y, C) :- arc(X, Y), {C = 1} ),
      f1 = arc(a, b),
      f2 = arc(b, c),
      f3 = arc(c, a) ],
   tp_iteration_prolog_with_proof_trees(
      Program, module, [], Atoms),
   xml_proof_atoms_to_picture(Atoms),
   dwrite(pl, user, Program).

test(proof_trees, 7) :-
   Program = [
      r = (tc(X, Y) :- arc(X, Z), tc(Z, Y)),
      e = (tc(X, Y) :- arc(X, Y)),
      f1 = arc(a, b),
      f2 = arc(b, c) ],
   tp_iteration_prolog_with_proof_trees(
      Program, module, [], Atoms),
   xml_proof_atoms_to_picture(Atoms),
   dwrite(pl, user, Program).

test(proof_trees, 8) :-
%  dislog_variable_get(example_path, 'd3/Travel.xml', File),
%  dislog_variable_get(example_path, 'd3/SonoConsult/SonoConsult.xml', File),
   dislog_variable_get(example_path, 'd3/test_kb.xml', File),
   dread(xml, File, [KB]),
   assert(d3_knowledge_base(KB)),
   d3_knowledge_base_to_rules(KB, Rules_1),
   dislog_rules_to_prolog_rules(Rules_1, Rules_2),
   Module = module,
   Goal = d3_finding(indicated_question, _),
   R = ( Goal :- Module:clause(tree(Goal, _), true) ),
   assert(R),
   star_line,
   tp_iteration_prolog_with_proof_trees(
      Rules_2, Module, [], Atoms),
   retract_all(Module:R),
   star_line,
   xml_proof_atoms_to_picture_d3(Atoms),
   star_line,
   dwrite(pl, user, Rules_2),
   abolish(d3_finding/2),
   retract_all(d3_knowledge_base(_)).


/******************************************************************/


