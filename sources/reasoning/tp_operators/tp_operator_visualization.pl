

/******************************************************************/
/***                                                            ***/
/***       Teaching:  Visualization of the Tp-Operator          ***/
/***                                                            ***/
/******************************************************************/
        

             
/*** tests ********************************************************/


test(tp_iteration_visualization, Mode) :-
   member(Mode, [l, r, lr]),
   tp_iteration_visualization(Mode, yes, 10).


/*** interface ****************************************************/


/* tp_iteration_visualization(Mode, Magic, Max) <-
      */

tp_iteration_visualization(Mode, Magic, Max) :-
   tc_program_generic(Mode, tc, arc, Program),
   findall( [arc(N, M)],
      ( between(1, Max, N),
        \+ N is Max//2,
        M is N + 1 ),
      Int ),
   append(Program, Int, Program_2),
   ( Magic = yes,
     magic_sets_transformation(Program_2, tc(1, _), Program_3)
   ; Magic = no,
     Program_3 = Program_2 ),
   tp_operator_picture_create(Picture),
   send(Picture, size, size(500, 500)),
   ( for(I, 1, Max+1) do
        tp_point_to_picture(Picture, yellow, I-I) ),
   tp_iteration_visualization(Picture, Program_3, [], _),
   !.


/*** implementation ***********************************************/


/* tp_iteration_visualization(Picture, Program, I1, I2) <-
      */

tp_iteration_visualization(Picture, Program, I1, I2) :-
   ( foreach(A, I1) do
        ( ( A = tc(N, M)
          ; A = aug(tc, [b, f], N, M) ) ->
          tp_point_to_picture(Picture, blue, N-M)
        ; true ) ),
   send(Picture, synchronise),
   wait_seconds(1),
   write(I1), nl,
   tp_operator(Program, I1, I3),
   list_to_ord_set(I3, I4),
   ( ( I4 = I1,
       !,
       I2 = I1 )
   ; tp_iteration_visualization(Picture, Program, I4, I2) ).


/* tp_operator_picture_create(Picture) <-
      */

tp_operator_picture_create(Picture) :-
   send(new(Picture, picture('tc')), open).


/* tp_point_to_picture(Picture, Colour, N-M) <-
      */

tp_point_to_picture(Picture, Colour, N-M) :-
   X is 10 * N,
   Y is 500 - 10 * M,
   point_to_picture(Picture, Colour, X-Y).

point_to_picture(Picture, Colour, X-Y) :-
   send(Picture, display,
      new(Point, ellipse(10, 10)), point(X, Y)),
   send(Point, fill_pattern, colour(Colour)).


/******************************************************************/


