

/******************************************************************/
/***                                                            ***/
/***          Teaching:  Tp-Operator                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tp_operator(Program, Interpretation_1, Interpretation_2) <-
      */

tp_operator(Program, Interpretation_1, Interpretation_2) :-
   findall( A,
      ( member(Rule, Program),
        parse_rule(Rule, [A]-Body),
        tp_resolve_literals(Body, Interpretation_1) ),
      Interpretation_2 ).

parse_rule(Head-Body-Negative, Head-Body-Negative) :-
   !.
parse_rule(Head-Body, Head-Body) :-
   !.
parse_rule(Head, Head-[]).


/* tp_resolve_literal(Atom, Atoms) <-
      */

tp_resolve_literal(Atom, _) :-
   Atom = prolog:Goal,
   !,
   call(Goal).
tp_resolve_literal(Atom, Atoms) :-
   Atom = ~(A),
   !,
   \+ member(A, Atoms).
tp_resolve_literal(Atom, Atoms) :-
   member(Atom, Atoms).


/* tp_resolve_literals(Xs, Ys) <-
      */

% tp_resolve_literals(Xs, Ys) :-
%    ( foreach(X, Xs) do tp_resolve_literal(X, Ys) ).

tp_resolve_literals([X|Xs], Ys) :-
   tp_resolve_literal(X, Ys),
   tp_resolve_literals(Xs, Ys).
tp_resolve_literals([], _).

subset_generate([X|Xs], Ys) :-
   member(X, Ys),
   subset_generate(Xs, Ys).
subset_generate([], _).


/* tp_iteration(Program, Interpretation) <-
      */

tp_iteration(Program, Interpretation) :-
   tp_iteration(Program, [], Interpretation).

tp_iteration(Program, I1, I2) :-
%  writeln(I1),
   tp_operator(Program, I1, I3),
   list_to_ord_set(I3, I4),
   ord_subtract(I4, I1, I),
   writeln(I),
   ( ( I4 = I1,
       !,
       I2 = I1 )
   ; tp_iteration(Program, I4, I2) ).


/*** tests ********************************************************/


test(teaching:nmr, tp_iteration) :-
   Program = [
      [tc(X, Y)]-[arc(X, Y)],
      [tc(X, Y)]-[arc(X, Z), tc(Z,Y)],
      [arc(a, b)], [arc(b, c)], [arc(c, d)] ],
   tp_iteration(Program, _).

test(teaching:nmr, tp_iteration_quadratic) :-
   Program = [
      [tc(X, Y)]-[arc(X, Y)],
      [tc(X, Y)]-[tc(X, Z), tc(Z,Y)],
%     [arc(d, e)], [arc(e, f)], [arc(f, g)],
      [arc(a, b)], [arc(b, c)], [arc(c, d)] ],
   tp_iteration(Program, _).

test(teaching:nmr, tp_iteration_path_length) :-
   Program = [
      [tc(X, Y, L)]-[arc(X, Y, L)],
      [tc(X, Y, L)]-[arc(X, Z, N), tc(Z,Y, M), prolog:(L is N+M)],
      [arc(a, b, 1)], [arc(b, c, 2)], [arc(c, d, 3)] ],
   tp_iteration(Program, _).


/******************************************************************/

 
