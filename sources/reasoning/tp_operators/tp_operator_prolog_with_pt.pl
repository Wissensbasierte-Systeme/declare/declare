

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Proof Trees                           ***/
/***                                                            ***/
/******************************************************************/


% :- dynamic
%       module:tree/2.


/*** interface ****************************************************/


/* tp_iteration_prolog_with_proof_trees(
         Program, Module, I1, I2) <-
      */

tp_iteration_prolog_with_proof_trees(Program, Module, I1, I2) :-
   ( pair_lists(=, _, Program_1, Program) -> true
   ; Program_1 = Program ),
   prolog_program_execute_dynamics(Program_1, Module, _),
   module_prepare_for_proof_trees(Module, R),
   writeq(Program),
   star_line,
   prolog_program_expand_with_proof_trees(Program, Program_2),
   dwrite(pl, user, Program_2),
   tp_iteration_prolog(Program_2, Module, I1, I2),
   retract_all(Module:R).

module_prepare_for_proof_trees(Module, Rule) :-
   Rule = ( tree(Goal, Tree) :-
      call(Goal),
      Tree = node:[key:Goal]:[] ),
   assert(Module:Rule).


/* tp_iteration_with_proof_trees(Program, Query) <-
      */

tp_iteration_with_proof_trees(Program, Query) :-
   tp_iteration_dislog_with_proof_trees(Program, Query).


/* tp_iteration_dislog_with_proof_trees(Program, Query) <-
      */

tp_iteration_with_proof_trees(Program, Query) :-
   tp_iteration_dislog_with_proof_trees(Program, Query).

tp_iteration_dislog_with_proof_trees(Program, Query) :-
   Rule = ( tree(prolog:Goal, Tree) :-
      call(Goal),
      Tree = node:[key:Goal]:[] ),
   assert(module:Rule),
   listing(tree),
   tp_iteration_dislog_with_proof_trees(
      Program, module, [], Atoms),
   xml_proof_atoms_to_picture(Atoms, Query),
   star_line,
   retractall(module:tree(_, _)),
   !.


/* tp_iteration_dislog_with_proof_trees(
         Program, Module, I1, I2) <-
      */

tp_iteration_dislog_with_proof_trees(Program, Module, I1, I2) :-
   dislog_program_expand_with_proof_trees(Program, Program_1),
   dislog_rules_to_prolog_rules(Program_1, Program_2),
%  expansion
   star_line,
%  writeln(user, original), nl(user),
   writeln_list(user, Program),
   star_line,
%  writeln(user, expanded), nl(user),
   dwrite(pl, user, Program_2),
   star_line,
   tp_iteration_prolog(Program_2, Module, I1, I2),
   star_line.


/* xml_proof_atoms_to_picture(Atoms, Query) <-
      */

xml_proof_atoms_to_picture(Atoms) :-
   xml_proof_atoms_to_picture(Atoms, _).

xml_proof_atoms_to_picture(Atoms, Query) :-
   forall( member(tree(Query, Tree_1), Atoms),
      (
%       dwrite(xml, Tree_1),
        xml_proof_tree_simplify(Tree_1, Tree_2),
        xml_proof_tree_to_picture_tp_pt(Tree_2) ) ).

xml_proof_tree_to_picture_tp_pt(Item) :-
   xml_proof_tree_to_vertices_and_edges(
      Item, Vertices, Edges),
%  vertices_edges_to_picture_bfs(Vertices, Edges).
   Config =
   config:[]:[
      gxl_layout:[
         x_start:70, y_start:70,
%        x_step:50,  y_step:50, y_variance:10]:[]],
         x_step:110, y_step:60, y_variance:10]:[]],
   vertices_edges_to_picture(dfs, Config, Vertices, Edges, _).

xml_proof_atoms_to_picture_d3(Atoms) :-
   ( foreach(tree(_, Tree_1), Atoms) do
        dwrite(xml, Tree_1),
        xml_proof_tree_simplify(Tree_1, Tree_2),
        xml_proof_tree_simplify(d3, Tree_2, Tree_3),
        xml_proof_tree_to_picture(Tree_3) ).


/******************************************************************/


