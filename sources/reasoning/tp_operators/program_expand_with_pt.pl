

/******************************************************************/
/***                                                            ***/
/***             DDK:  Proof Trees                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* prolog_programs_expand_with_proof_trees(
         Programs_1, Programs_2) <-
      */

prolog_programs_expand_with_proof_trees(
      Programs_1, Programs_2) :-
   ( foreach(Rules_1, Programs_1),
     foreach(Rules_2, Programs_2) do
        maplist( prolog_rule_expand_with_proof_trees,
           Rules_1, Rules_2 ) ),
   close_num(rule_).


/* prolog_program_expand_with_proof_trees(Rules_1, Rules_2) <-
      */

prolog_program_expand_with_proof_trees(Rules_1, Rules_2) :-
   maplist( prolog_rule_expand_with_proof_trees,
      Rules_1, Rules_2 ),
   close_num(rule_).


/* prolog_rule_expand_with_proof_trees(Id=Rule_1, Rule_2) <-
      */

prolog_rule_expand_with_proof_trees(Id=Rule_1, Rule_2) :-
   !,
   parse_prolog_rule(Rule_1, A1 :- B1),
   ( B1 = true ->
     Es = [],
     B2 = true
   ; formula_expand_with_proof_trees(B1, B2, T),
     Es = [T] ),
   A2 = tree(A1, node:[key:A1, rule:Id]:Es),
   ( B2 = true ->
     Rule_2 = (A2 :- proof_tree_atom_test(A2))
   ; Rule_2 = (A2 :- (B2, proof_tree_atom_test(A2))) ).
prolog_rule_expand_with_proof_trees(Rule_1, Rule_2) :-
   get_num(rule_, Id),
   prolog_rule_expand_with_proof_trees(Id=Rule_1, Rule_2).


/* proof_tree_atom_test(Atom) <-
      */

proof_tree_atom_test(Atom) :-
   Atom = tree(A, node:_:[T]),
   A = tc(X, Y, C1),
   !,
   \+ ( tc(X, Y, C2) := T/descendant::node@key, C1 >= C2 ).
proof_tree_atom_test(_).


/* dislog_program_expand_with_proof_trees(Rules_1, Rules_2) <-
      */

dislog_program_expand_with_proof_trees(Rules_1, Rules_2) :-
   maplist( dislog_rule_expand_with_proof_trees,
      Rules_1, Rules_2 ).

dislog_rule_expand_with_proof_trees(Id=Rule_1, Rule_2) :-
   !,
   parse_dislog_rule(Rule_1, [A]-As-[]),
   pair_lists(tree, [A|As], [T|Ts], [B|Bs]),
   C = dislog_rule_proof_trees_combine([A:T]-[Id|Ts]),
   append(Bs, [C], Cs),
   Rule_2 = [B]-Cs.
dislog_rule_expand_with_proof_trees(Rule_1, Rule_2) :-
   get_num(rule_, Id),
   dislog_rule_expand_with_proof_trees(Id=Rule_1, Rule_2).


/* dislog_rule_proof_trees_combine([A:T]-[Id|Ts]) <-
      */

dislog_rule_proof_trees_combine([A:T]-[Id|Ts]) :-
   sublist( \=(node:[]),
      Ts, Ts_2 ),
   T = node:[key:A, rule:Id]:Ts_2,
   \+ (A := T/descendant::node@key).


/*** implementation ***********************************************/


/* formula_expand_with_proof_trees(X1, X2, T) <-
      */

formula_expand_with_proof_trees(X1, X2, T) :-
   X1 = not(X),
   !,
   formula_expand_with_proof_trees(X, Y, _),
   X2 = (not(Y), T = []).
formula_expand_with_proof_trees(X1, X2, T) :-
   X1 = (\+ X),
   !,
   formula_expand_with_proof_trees(X, Y, _),
   X2 = (\+ Y, T = node:[]).
formula_expand_with_proof_trees(X1, X2, T) :-
   X1 = ddbase_aggregate(X, G1, Xs),
   !,
   formula_expand_with_proof_trees(G1, G2, _),
   X2 = (ddbase_aggregate(X, G2, Xs), T = node:[]).
formula_expand_with_proof_trees(X1, X2, T) :-
   X1 = {X},
   !,
   X2 = (X, T = node:[]).
formula_expand_with_proof_trees(X1, X2, T) :-
   X1 = !,
   !,
   X2 = (!, T = node:[]).
formula_expand_with_proof_trees(X1, X2, T) :-
   X1 = (_,_),
   !,
   comma_structure_to_list(X1, Xs1),
   !,
   ( foreach(A, Xs1), foreach(B, Xs2), foreach(C, Vs) do
        formula_expand_with_proof_trees(A, B, C) ),
   list_to_comma_structure(Xs2, X),
   Sublist = sublist( \=(node:[]), Vs, Vs_2 ),
   If_Then_Else =
      ( Vs_2 = [Tree] ->
        T = Tree
      ; T = node:[key:and]:Vs_2 ),
   X2 = (X, Sublist, If_Then_Else),
   !.
formula_expand_with_proof_trees(X1, X2, T) :-
   X1 = (_;_),
   !,
   semicolon_structure_to_list(X1, Xs1),
%  ( foreach(X, Xs1), foreach(Y, Xs2) do
%       formula_expand_with_proof_trees(X, Y, T) ),
   maplist( formula_expand_with_proof_trees_(T),
      Xs1, Xs2 ),
   list_to_semicolon_structure(Xs2, X2),
   !.
formula_expand_with_proof_trees(X1, X2, T) :-
   X2 = tree(X1, T).

formula_expand_with_proof_trees_(T, X1, X2) :-
   formula_expand_with_proof_trees(X1, X2, T).

 
/******************************************************************/


