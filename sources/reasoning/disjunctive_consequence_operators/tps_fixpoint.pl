

/******************************************************************/
/***                                                            ***/
/***   DisLog : Tps-Fixpoint-Iteration                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* minimal_model_state(Program, State) <-
      the minimal model state State of a DLP Program 
      is computed based on clause trees. */

minimal_model_state_operator(Program, State) :-
   state_tree_mms_operator(Program, State).
minimal_model_state(Program, State) :-
   state_tree_mms_operator(Program, State).

state_tree_mms_operator(Program, State) :-
   tree_tps_fixpoint(Program, [], Tree),
   tree_to_state(Tree, State).


/* tree_mms_operator(Program, Tree) <-
      a clause tree Tree representing the minimal model state 
      of a DLP Program is computed based on clause trees. */

tree_mms_operator(Program, Tree) :-
   tree_tps_fixpoint(Program, [], Tree).


/* mms_operator(Program, State) <-
      the minimal model state State of a DLP Program 
      is computed. */

mms_operator(Program, State) :-
   tps_fixpoint(Program, [], State).


/* state_tree_tps_fixpoint(Program, State_1, State_2) <-
      the T_ps fixpoint iteration for the DLP Program
      starting with a clause tree for the d-state State_1
      yields a clause tree for the d-state State_2. */

state_tree_tps_fixpoint(Program, State_1, State_2) :-
   state_to_tree(State_1, Tree_1),
   tree_tps_fixpoint(Program, Tree_1, Tree_2),
   tree_to_state(Tree_2, State_2).


/* tree_tps_fixpoint(Program, Tree_1, Tree_2) <-
      the T_ps fixpoint iteration for the DLP Program
      starting with the clause tree Tree_1
      yields the clause tree Tree_2. */

tree_tps_fixpoint(Program, Tree_1, Tree_2) :-
   hidden( 1, (
      dlp_to_d_clauses(Program),
      tree_tps_fixpoint(Tree_1, Tree_2),
      retractall(d_clause(_, _)) ) ),
   !.

tree_tps_fixpoint(Tree_1, Tree_2) :-
   hidden( 1, (
      dislog_flag_switch(tps_mode, Mode, 5),
      assert(d_fact_tree(Tree_1)),
      assert(delta_d_fact_tree(Tree_1)),
      tps_fixpoint,
      retract(d_fact_tree(Tree_2)),
      retract(delta_d_fact_tree(_)),
      dislog_flag_set(tps_mode, Mode) ) ),
   !.


/* tree_tps_fixpoint_iteration(Program) <-
      The tree based T_ps fixpoint iteration for the DLP Program 
      is executed; after each iteration the actual d-state is
      pretty-printed. */

tree_tps_fixpoint_iteration(Program) :-
   tree_tps_fixpoint_iteration(Program, _).

tree_tps_fixpoint_iteration(Program, Tree) :-
   announcement('tree_tps_fixpoint'), nl,
   tree_tps_fixpoint_iteration(1, Program, [], Tree),
   tree_to_state(Tree, State),
   writeln('least fixpoint:'),
   write('MS'), write(' ='), dportray2(dhs, State), nl,
   !.

tree_tps_fixpoint_iteration(N, Program, Tree_1, Tree_5) :-
   write_list(['tps_iteration ', N, ':\n']),
   hidden( (
      tree_tps_operator_dc(Program, Tree_1, Tree_2),
      tree_to_state(Tree_2, State_2) ) ),
   dportray2(dhs, State_2), nl,
   hidden( (
      tree_union(Tree_1, Tree_2, Tree_3),
      tree_can(Tree_3, Tree_4) ) ),
   M is N + 1,
   ( ( Tree_2 = [], Tree_5 = Tree_4 );
     tree_tps_fixpoint_iteration(M, Program, Tree_4, Tree_5) ).


/* tps_fixpoint_iteration(Program) <-
      the T_ps fixpoint iteration for the DLP Program is
      executed, after each iteration the actual d-state is
      pretty-printed. */

tps_fixpoint_iteration(Program) :-
   tps_fixpoint_iteration(Program,_).

tps_fixpoint_iteration(Program, State) :-
%  announcement('tps_fixpoint'), nl,
   tps_fixpoint_iteration(1, Program, [], State),
   write_list(['S_m =']),
   dportray2(dhs, State),
%  nl,
   !.

tps_fixpoint_iteration(N, Program, State_1, State_5) :-
   write_list(['S_', N, ' =']),
   hidden( 
      tps_operator_dc(Program, State_1, State_2) ),
   dportray2(dhs, State_2),
%  nl,
   hidden( (
      ord_union(State_1, State_2, State_3),
      state_can(State_3, State_4) ) ),
   M is N + 1,
   ( ( State_2 = [], State_5 = State_4 );
     tps_fixpoint_iteration(M, Program, State_4, State_5) ).


/* tps_fixpoint(Program, State_1, State_2) <-
      the T_ps fixpoint iteration for the DLP Program starting 
      with the d-state State_1 yields the d-state State_2. */

tps_fixpoint(Program, State_1, State_2) :-
   hidden( 1, (
      dlp_to_d_clauses(Program),
      tps_fixpoint(State_1, State_2),
      dabolish(d_clause/2) ) ),
   !.


tps_fixpoint(State_1, State_2) :-
   hidden( 1, (
      dhs_to_d_facts(State_1),
      initialize_tps_fixpoint,
      tps_fixpoint,
      collect_arguments(d_fact, State),
      state_prune(State, State_2),
      dabolish(d_fact/1) ) ),
   !.


/*** implementation ***********************************************/


/* tps_fixpoint <-
      Basic predicate for computing the least fixpoint of the
      T_ps-operator of a DLP,
      the DLP is assumed to be asserted as d_clause and d_fact 
      atoms, all d_fact atoms are also asserted as delta_d_fact 
      atoms. */

tps_fixpoint :-
   ( ( dislog_flag_get(tps_mode, I),
       var(I), !,
       announcement('tps_fixpoint') )
   ; ( dislog_flag_get(tps_mode, I),
       I =< 2, !,
       announcement('tps_fixpoint') )
   ; announcement('tree_tps_fixpoint') ), 
   nl,
   assert(delta_d_fact(dummy)),
   repeat,
      tps_single_iteration(N),
   N == 0,
   retract(current_num(tps_single_iteration, _)).


/* tps_single_iteration(N) :-
      a single execution of the disjunctive T_ps-operator
      yields N new d_facts. */

tps_single_iteration(N) :-
   get_num(tps_single_iteration, I),
   write_list(['tps_iteration_', I, ':\n']),
   tps_single_iteration(N, _),
   !.


/* tps_single_iteration(N, I) <-
      a single execution of the disjunctive T_ps-operator
      yields N new d_facts, deletes I old subsumed facts,
      depending on the tps_mode:
       1 - naive evaluation
       2 - clause-based sne (semi-naive evaluation)
       3 - tree-based sne using minimal spanning trees
       4 - tree-based sne using insertion heuristic
       5 - join-tree-based sne using insertion heuristic
       6 - join-tree-based improved sne using insertion heuristic */

tps_single_iteration(N, I) :-
   dislog_flag_get(tps_mode, 1),
   start_timer(tps_single_iteration),
   slow_join(Facts, M),
   slow_compute_delta_facts(Facts, N, I),
   pp_numbers([M, N, I]),
   write('time         '), ttyflush,
   stop_timer(tps_single_iteration).

tps_single_iteration(N, I) :-
   dislog_flag_get(tps_mode, Mode),
   tps_mode_to_predicates(Mode, [
      Join_Predicate, Compute_Delta_Facts_Predicate,
      Purify_Facts_Predicate, Purify_D_Facts_Predicate,
      Update_D_Facts_Predicate]),
   start_timer(tps_single_iteration),
   apply( Join_Predicate, [Facts, M] ),
   state_prune(Facts,Pruned_Facts,K),
   apply( Compute_Delta_Facts_Predicate,
      [Pruned_Facts, Delta_Facts, J] ),
   apply( Purify_Facts_Predicate,
      [Delta_Facts, Purified_Delta_Facts, N] ),
   apply( Purify_D_Facts_Predicate, [Purified_Delta_Facts, I] ),
   apply( Update_D_Facts_Predicate, [Purified_Delta_Facts] ),
   pp_numbers([M, K, J, N, I]),
   write('time         '), ttyflush,
   stop_timer(tps_single_iteration).

tps_mode_to_predicates(Mode, [ Join_Predicate,
      Compute_Delta_Facts_Predicate,
      Purify_Facts_Predicate, Purify_D_Facts_Predicate,
      Update_D_Facts_Predicate ]) :-
   ( ( Mode = 2 ->
       Join_Predicate =                delta_join,
       Compute_Delta_Facts_Predicate = compute_delta_facts,
       Purify_Facts_Predicate =        state_can,
       Purify_D_Facts_Predicate =      purify_d_facts,
       Update_D_Facts_Predicate =      update_d_facts )
   ; ( Mode = 3 ->
       Join_Predicate =                tree_based_join,
       Compute_Delta_Facts_Predicate = tree_compute_delta_facts,
       Purify_Facts_Predicate =        state_can,
       Purify_D_Facts_Predicate =      purify_d_fact_tree,
       Update_D_Facts_Predicate =      update_d_fact_trees )
   ; ( Mode = 4 ->
       Join_Predicate =                tree_join,
       Compute_Delta_Facts_Predicate = tree_compute_delta_facts,
       Purify_Facts_Predicate =        state_can,
       Purify_D_Facts_Predicate =      purify_d_fact_tree,
       Update_D_Facts_Predicate =      old_update_d_fact_trees )
   ; ( Mode = 5 ->
       Join_Predicate =                tree_join,
       Compute_Delta_Facts_Predicate = tree_compute_delta_facts,
       Purify_Facts_Predicate =        state_can_3,
       Purify_D_Facts_Predicate =      purify_d_fact_tree,
       Update_D_Facts_Predicate =      update_d_fact_trees )
   ; ( Mode = '5m' ->
       Join_Predicate =                tree_join,
       Compute_Delta_Facts_Predicate = meta_tree_compute_delta_facts,
       Purify_Facts_Predicate =        state_can_3,
       Purify_D_Facts_Predicate =      purify_d_fact_tree,
       Update_D_Facts_Predicate =      update_d_fact_trees )
   ; ( Mode = 6 ->
       Join_Predicate =                new_tree_join,
       Compute_Delta_Facts_Predicate = tree_compute_delta_facts,
       Purify_Facts_Predicate =        state_can_3,
       Purify_D_Facts_Predicate =      purify_d_fact_tree,
       Update_D_Facts_Predicate =      new_update_d_fact_trees ) ).


/* initialize_tps_fixpoint <-
      initialization of a T_ps-Delta fixpoint iteration,
      given a DLP, which is asserted as d_clause and d_fact atoms,
      asserts all d_fact atoms of the DLP as ordered d_fact atoms
      and as delta_d_fact atoms for the first iteration. */
 
initialize_tps_fixpoint :-
   announcement(12, 'tps_initialization'),
   initialize_d_fact,
%  initialize_d_clause,
   special_initialize.
 

/* initialize_d_fact <-
      replaces all asserted d_fact atoms by ordered d_fact atoms. */

initialize_d_fact :-
   assertz(d_fact([[]])),
   initialize_d_fact_loop.
 
initialize_d_fact_loop :-
   retract_d_fact(Fact),
   list_to_ord_set(Fact, New_Fact),
   assert(d_fact(New_Fact)), 
   assert(delta_d_fact(New_Fact)),
   initialize_d_fact_loop.
initialize_d_fact_loop.

retract_d_fact(Fact) :-
   retract(d_fact(Fact)),
   !,
   Fact \== [[]].


/* initialize_d_clause <-
      replaces all asserted d_clause atoms by ordered d_clause 
      atoms. */

initialize_d_clause :-
   assertz(d_clause([[]], [[]])),
   initialize_d_clause_loop.

initialize_d_clause_loop :-
   retract_d_clause(Head, Body),
   list_to_ord_set(Head, New_Head), 
   list_to_ord_set(Body, New_Body),
   assert(d_clause(New_Head, New_Body)),
   initialize_d_clause_loop.
initialize_d_clause_loop.

retract_d_clause(Head, Body) :-
   retract(d_clause(Head, Body)),
   !,
   ( Head \== [[]]; 
     Body \== [[]] ).


/* special_initialize <-
      executes the necessary initializations which are special
      to the different values for the DisLog flag tps_mode. */

special_initialize :-
   dislog_flag_get(tps_mode, Mode),
   ( Mode = 1 ->
     true
   ; Mode = 2 ->
     assert(delta_d_fact(dummy))
   ; Mode = 3 ->
     assert(delta_d_fact(dummy)),
     init_d_fact_trees
   ; Mode = 4 ->
     old_init_d_fact_trees
   ; Mode = 5 ->
     init_d_fact_trees
   ; Mode = 6 ->
     new_init_d_fact_trees ).


/* tree_mode <-
      Sets the current value of the DisLog flag tps_mode to 5,
      which yields tree based T_ps computations. */

tree_mode :-
   dislog_flag_set(tps_mode, 5),
   write_list([
      '{DisLog will evaluate the T_ps-operator ',
      'based on clause trees}', '\n', 'tps_mode = 5', '\n' ]).

 
/*** tests ********************************************************/


test(tps_fixpoint, join_mode_special) :-
   dislog_flags,
   dislog_flag_switch(join_mode, Mode, special),
   dislog_variable_get(example_path, Examples),
   concat(Examples, 'evaluable_predicates/test', File),
   dconsult(File, Program),
   tps_fixpoint(Program, [], State),
   dportray(dhs, State),
   dislog_flag_set(join_mode, Mode).


/******************************************************************/


