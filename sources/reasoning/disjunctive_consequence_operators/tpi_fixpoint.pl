

/******************************************************************/
/***                                                            ***/
/***          DisLog :  Tpi-Operator - Delta-Iteration          ***/
/***                                                            ***/
/******************************************************************/


:-  multifile
      special_literal/1.


/*** tests ********************************************************/


test(tree_tpi_fixpoint_iteration, non_delta) :-
   Program = [[e,c]-[b], [f,d]-[b], [b], []-[e,d]],
   tree_tpi_fixpoint_iteration_non_delta(Program, [[]], _).


/*** interface ****************************************************/


/* tree_tpi_mm_operator(Program, Tree) <-
      computes a minimal model tree Tree for the DLP Program. */
 
tree_tpi_mm_operator(Program, Tree) :-
   tree_tpi_fixpoint(Program, [[]], Tree).
 
tree_tpi_mm_operator_2(Program, Tree) :-
   tree_tpi_fixpoint_2(Program, [[]], Tree).
 
 
tpi_fixpoint_iteration(Program) :-
   tree_tpi_fixpoint_iteration(Program).

tpi_fixpoint_iteration(Program, Models) :-
   tree_tpi_fixpoint_iteration(Program, Model_Tree),
   tree_to_state(Model_Tree, Models).


/* tree_tpi_fixpoint_iteration_non_delta(Program, Tree_1, Tree_2) <-
      */

tree_tpi_fixpoint_iteration_non_delta(Program, Tree_1, Tree_2) :-
   tree_tpi_operator(Program, Tree_1, Tree_3),
   write_list(user, ['Tree = ', Tree_1, '\n']),
   set_tree_to_picture_bfs(Tree_1),
   tree_to_state(Tree_1, State_1),
   writeln(user, 'Coin = '), dportray2(chs, State_1),
   ( Tree_3 = Tree_1 ->
     Tree_2 = Tree_1
   ; tree_tpi_fixpoint_iteration_non_delta(
        Program, Tree_3, Tree_2) ),
   !.
    

/* tree_tpi_fixpoint_iteration(Program) <-
      the T_pi fixpoint iteration for the DLP Program is executed,
      after each iteration the actual c-state is pretty-printed. */

tree_tpi_fixpoint_iteration(Program) :-
   tree_tpi_fixpoint_iteration(Program,_).

tree_tpi_fixpoint_iteration(Program, Tree) :-
   announcement('tpi_fixpoint'), nl,
   tree_tpi_fixpoint_iteration(1, Program, [], [[]], Tree, _),
   tree_to_state(Tree, State),
%  star_line,
   writeln('tpi_fixpoint:'),
   write('MM '), write(' ='), dportray2(chs, State), nl,
   !.

tree_tpi_fixpoint_iteration(N,Program, Tree_1, Delta1, Tree_3, Delta3) :-
   write('tpi_iteration '), write(N), write(':'),
   hidden( 1, (
      tree_tpi_delta_operator(Program, Tree_1,Delta1, Tree_2,Delta2),
      tree_to_state(Tree_2, State_2),
      tree_to_state(Delta2,DeltaState_2) ) ),
   write('M_'), write(N), write(' ='), dportray2(chs, State_2),
   write('D_'), write(N), write(' ='), dportray2(chs,DeltaState_2), nl,
   M is N + 1,
   ( ( Delta2 = [], Tree_3 = Tree_2 )
   ; tree_tpi_fixpoint_iteration(M,Program, Tree_2,Delta2, Tree_3,Delta3) ).


/* tree_tpi_fixpoint(Program, Tree_1, Tree_2) <-
      given a DLP Program and a model tree Tree_1,
      the model tree Tree_2 is the result of a T_pi fixpoint
      iteration for Program starting with Tree_1. */

tree_tpi_fixpoint(Program, Tree_1, Tree_2) :-
   hidden( 1, (
      assert(d_fact_tree([])), 
      assert(delta_fact_tree(Tree_1)),
      repeat, 
         tree_tpi_delta_operator(Program), 
      delta_fact_tree([]),
      retract(delta_fact_tree([])), 
      retract(d_fact_tree(Tree_2)) ) ).

tree_tpi_fixpoint_2(Program, Tree) :-
   tree_tpi_fixpoint_2(Program,[[]], Tree).

tree_tpi_fixpoint_2(Program, Tree_1, Tree_2) :-
   assert(d_fact_tree([])),
   assert(delta_fact_tree(Tree_1)),
   assert(saved_trees([[['T','D'],[],[[]]]])),
   repeat,
      tree_tpi_delta_operator(Program), 
      save_tree,
   delta_fact_tree([]),
   retract(delta_fact_tree([])),
   retract(d_fact_tree(Tree_2)),
   retract(saved_trees(Trees)),
   writeln(user, Trees),
   dportray(edge(trees), Trees).
      

/* tree_tpi_delta_operator(Program) <-
      */

tree_tpi_delta_operator(Program) :- 
   retract(d_fact_tree(Tree_1)), 
   retract(delta_fact_tree(Delta1)), 
   tree_tpi_delta_operator(Program, Tree_1,Delta1, Tree_2,Delta2),
   assert(d_fact_tree(Tree_2)),
   dspy_tree(Tree_2),
   assert(delta_fact_tree(Delta2)),
   dspy_tree(Delta2),
   !. 


/* save_tree <-
      */

save_tree :-
   retract(saved_trees(Trees)),
   d_fact_tree(Tree_1), 
   delta_fact_tree(Tree_2),
   append(Trees,[[['T','D'], Tree_1, Tree_2]], New_Trees),
   assert(saved_trees(New_Trees)),
   !.


/* tree_tpi_delta_operator(Program, Tree_1, Tree_2, Tree_3, Tree4) <-
      given a DLP Program and two model trees Tree_1, Tree_2,
      the tpi-operator applied to Tree_2 yields a new model tree New,
      the part of New which was already in Tree_2 is added to Tree_1
      yielding Tree_3, 
      from the really new part of New the model tree Tree_3 is 
      subtracted, the result is canonized yielding Tree4. */
 
tree_tpi_delta_operator(Program, Tree_1,Delta1, Tree_2,Delta2) :-
   tree_tpi_join_non_ground(Program,Delta1, New_Tree),
   dspy_tree(New_Tree),
   tree_prune([], New_Tree, New_Tree_2),
   tree_simplify_2(New_Tree_2, New_Tree_3),
   dspy_tree(New_Tree_3),
   tree_can_2(New_Tree_3, New),
   dspy_tree(New),
   tree_tree_difference(New,Delta1, New2),
   dspy_tree(New2),
   tree_tree_difference(New, New2, New1),
   dspy_tree(New1),
   tree_simplify_3(New1, New4),
   dspy_tree(New4),
   tree_union(Tree_1, New4, Tree_2),
   dspy_tree(Tree_2),
   tree_tree_subtract(New2, Tree_2, New3),
   dspy_tree(New3),
   tree_can(New3,Delta2).

tree_tpi_operator(Program, Tree_1, Tree_2) :-
   tree_tpi_join_non_ground(Program, Tree_1, Tree_2).

tree_tpi_operator_ground(Program, Tree_1, Tree_2) :-
   tree_tpi_join_ground(Program, Tree_1, Tree_2).


/* tree_tpi_join_non_ground(Program, Tree_1, Tree_2) <-
      the DLP Program applied to the model tree Tree_1 yields 
      the model tree Tree_2. */
 
tree_tpi_join_non_ground(Program, Tree_1, Tree_2) :-
   tree_tpi_join_2(Program, Tree_1,Program_2),    
   tree_tpi_join_ground(Program_2, Tree_1, Tree_2).
 
 
/* tree_tpi_join_2(Program_1, Tree,Program_2) <-
      The ground DNLP Program_2 is constructed as the set of all 
      ground instances of rules of the DNLP Program_1 that resolve 
      with the model tree Tree. */

tree_tpi_join_2(Program_1, Tree,Program_2) :-                
   dnlp_to_dn_clauses(Program_1),
   assert(delta_d_fact_tree(Tree)),
   tree_tpi_join_2(Program_2),
   retractall(dn_clause(_, _,_)),
   retract(delta_d_fact_tree(Tree)).
 
tree_tpi_join_2(Program) :-
   write('tree_join    '), ttyflush,
   start_timer(tree_tpi_join),
   delta_d_fact_tree(Tree),
   findall( Ord_Head-Ord_Pos_Body-Ord_Neg_Body,
      ( dn_clause(Head,Pos_Body,Neg_Body),
        tpi_resolve(Tree,Pos_Body,_),
        list_to_ord_set(Head,Ord_Head),
        list_to_ord_set(Pos_Body,Ord_Pos_Body),
        list_to_ord_set(Neg_Body,Ord_Neg_Body) ),
      Program_1 ),
   list_to_ord_set(Program_1,Program),
   stop_timer(tree_tpi_join).


/* tpi_resolve(Tree_1, Clause, Tree_2) <-
      Generates all ground instances of a clause Clause 
      which are supersumed by the clause tree Tree_1 
      and the supersuming subtrees Tree_2 of Tree_1. */

tpi_resolve([],[],[[]]) :-
   !.
tpi_resolve(Tree_1,[], Tree_1) :-
   !.
tpi_resolve(Tree_1, Clause, Tree_2) :-
   tree_supersumes_clause(Tree_1, Clause, Tree_2).


/* tree_tpi_join_ground(Program, Tree_1, Tree_2) <-
      the ground DLP Program applied to the model tree Tree_1
      yields the model tree Tree_2. */

tree_tpi_join_ground(Program_1,[Node], Tree) :-
   !,
   reduce_program(Program_1,Node,Program_2),
   select_facts(Program_2, State),
   state_to_model_tree(State, Model_Tree),
   attach_tree(Node, Model_Tree, Tree).
%  cross_product_to_trees(State, Trees),
%  Tree = [Node|Trees].
tree_tpi_join_ground(Program_1,[Node|Trees_1], Tree) :-
   reduce_program(Program_1,Node,Program_2),
   trees_tpi_join_ground(Program_2, Trees_1, Trees_2),
   attach_trees(Node, Trees_2, Tree).
tree_tpi_join_ground(_,[],[]).

trees_tpi_join_ground(Program, [Tree|Trees], New_Trees) :-
   tree_tpi_join_ground(Program, Tree, Tree_2),
   trees_tpi_join_ground(Program, Trees, Trees_2),
   ( ( Tree_2 == [],
       !,
       New_Trees = Trees_2 )
   ; New_Trees = [Tree_2|Trees_2] ).
trees_tpi_join_ground(_, [], []).


/* reduce_program(Program_1, Node, Program_2) <-
      From the ground DNLP Program_1 a new ground DNLP Program_2 
      is constructed, which consists of all rules
      Head-New_Pos_Body-Neg_Body, such that there is a rule 
      Head-Pos_Body-Neg_Body in Program_1, where Head and Node
      are disjoint, and Neg_Body and Node are disjoint, and
      New_Pos_Body is the set difference of Pos_Body and Node. */

reduce_program(Program_1, Node, Program_2) :-
   reduce_program(Program_1, Node, [], Program_2).

reduce_program([Rule|Rules], Node, Program_1, Program_3) :-
   dislog_flag_get(join_mode, Join_Mode),
   parse_dislog_rule(Rule, Head, Pos_Body, Neg_Body),
   ( ( ord_disjoint(Head, Node),
       ord_disjoint(Node, Neg_Body),
       special_literals_are_false(Join_Mode, Neg_Body),
%      !,
       ord_subtract(Pos_Body, Node, New_Pos_Body_2),
       subtract_special_literals(
          Join_Mode, New_Pos_Body_2, New_Pos_Body),
       simplify_rule(Head-New_Pos_Body-Neg_Body, New_Rule),
       Program_2 = [New_Rule|Program_1] )
   ; Program_2 = Program_1 ),
   reduce_program(Rules, Node, Program_2, Program_3).
reduce_program([], _, Program, Program).


/* special_literals_are_false(Join_Mode, Atoms) <-
      */

special_literals_are_false(special, Atoms) :-
   !,
   special_literals_are_false(Atoms).
special_literals_are_false(_, _).
   
special_literals_are_false([A|As]) :-
   special_literal(A),
   !,
   not(A),
   special_literals_are_false(As).
special_literals_are_false([_|As]) :-
   special_literals_are_false(As).
special_literals_are_false([]).


/* subtract_special_literals(Join_Mode, Ls1, Ls2) <-
      */

subtract_special_literals(special, Ls1, Ls2) :-
   !,
   subtract_special_literals(Ls1, Ls2).
subtract_special_literals(_, Ls, Ls).

subtract_special_literals([], []).
subtract_special_literals([L|Ls1], Ls2) :-
   special_literal(L),
%  !,
   call(L),
   subtract_special_literals(Ls1, Ls2).
subtract_special_literals([L|Ls1], [L|Ls2]) :-
   \+ special_literal(L),
%  base_literal(L),
   subtract_special_literals(Ls1, Ls2).


/* special_literal(L) <-
      */

special_literal(L) :-
   functor(L, #, _).


/* attach_tree(Node, Tree, New_Tree) <-
      the clause tree Tree is attached to the node Node,
      which yields a new clause tree New_Tree 
      for the d-state Node v state(Tree). */

attach_tree(_, [], []) :-
   !.
attach_tree(Node, [[]], [Node]) :-
   !.
attach_tree(Node_1, [Node_2|Trees], [Node|Trees]) :-
   ord_union(Node_1, Node_2, Node).


/* attach_trees(Node, Trees, New_Tree) <-
      The clause trees Trees are attached to the node Node, which 
      yields a new clause tree New_Tree for the d-state 
      Node v state(Trees), 
      where state([T1,...,Tk]) = state(T1) u ... u state(Tk). */

attach_trees(_, [], []) :-
   !.
attach_trees(Node, [Tree], New_Tree) :-
   !,
   attach_tree(Node, Tree, New_Tree).
attach_trees(Node, Trees, [Node|Trees]).


/* cross_product_to_trees(State, Trees) <-
      constructs a set Trees of model trees, such that 
      the model tree [[]|Trees] represents a c-state which is
      equivalent to the conjunction over the d-state State. */

cross_product_to_trees([C|Cs], Trees) :-
   cross_product_to_trees(Cs, Trees_1),
   cross_product_of_clause_and_trees(C, Trees_1, Trees).
cross_product_to_trees([],[]).

cross_product_of_clause_and_trees([A|As], Ts, [[[A]|Ts]|New_Ts]) :-
   cross_product_of_clause_and_trees(As, Ts, New_Ts).
cross_product_of_clause_and_trees([], _, []).


/* select_facts(Program, State) <-
      Given a ground DNLP Program,
      State is the d-state formed out of the facts in Program. */

select_facts(Program, State) :-
   select_facts(Program, [], State).

select_facts([Rule|Rules], Acc, State) :-
   parse_dislog_rule(Rule, Head, Pos_Body,_),
   ( ( Pos_Body == [],
       NewAcc = [Head|Acc] )
   ; NewAcc = Acc ),
   select_facts(Rules, NewAcc, State).
select_facts([], State, State).


/* tree_tpi_resolve(Tree_1, Head-Body, Tree_2) <-
      applies the disjunctive rule Head-Body to the model tree
      Tree_1, and yields a new extended model tree Tree_2. */

tree_tpi_resolve([Node], Head-[], [Node|Trees]) :-
   !,
   list_of_elements_to_relation(Head, List),
   list_of_elements_to_relation(List, Trees).
tree_tpi_resolve([Node], _, [Node]) :-
   !.
tree_tpi_resolve([Node|Trees_1], Head-Body_1, [Node|Trees_2]) :-
   setdifference_non_ground(Body_1, Node, Body_2),
   trees_tpi_resolve(Trees_1, Head-Body_2, Trees_2).
tree_tpi_resolve([], _, []).

trees_tpi_resolve([Tree_1|Trees_1], Head-Body, [Tree_2|Trees_2]) :-
   tree_tpi_resolve(Tree_1, Head-Body, Tree_2),
   trees_tpi_resolve(Trees_1, Head-Body, Trees_2).
trees_tpi_resolve([], _, []).


/* tree_tpi_join(Program, Tree, New_Clauses, Extendable_Clauses) <-
      The conjunctions of the clause tree Tree are extended w.r.t. 
      the DLP Program.  Those conjunctions which could be extended
      are given by Extendable_Clauses, their extensions are given 
      by New_Clauses. */

tree_tpi_join(Program, Tree, New_Clauses, Extendable_Clauses) :-
   dlp_to_d_clauses(Program),
   assert(delta_d_fact_tree(Tree)),
   tree_tpi_join(New_Clauses, Extendable_Clauses),
   retractall(d_clause(_, _)),
   retract(delta_d_fact_tree(Tree)).

tree_tpi_join(New_Clauses, Extendable_Clauses, M) :-
   tree_tpi_join(New_Clauses, Extendable_Clauses),
   length(New_Clauses, M).

tree_tpi_join(New_Clauses, Extendable_Clauses) :-
   write('tree_join    '), ttyflush,
   start_timer(tree_tpi_join),
   delta_d_fact_tree(Tree),
   findall( Clause_3,
      ( d_clause(Head, Body),
        tpi_resolve(Tree, Body, Tree_2),
        write(user,'.'), ttyflush,
        list_to_ord_set(Head, Head_2),
        tree_to_state(Tree_2, State_2),
        member(Clause_2, State_2),
        ord_disjoint(Head_2, Clause_2),
        assert(extended_clause(Clause_2)),
        member(Atom, Head),
        ord_union([Atom], Clause_2, Clause_3) ),
      Clauses ),
   list_to_ord_set(Clauses, New_Clauses),
   collect_arguments(extended_clause, Extendable_Clauses_List),
   list_to_ord_set(Extendable_Clauses_List, Extendable_Clauses),
   retractall(extended_clause(_)),
   stop_timer(tree_tpi_join).


/* tpi_fixpoint <-
      */

pi_fixpoint :-
   d_facts_to_d_clauses,
   prepare_analysis,
   assert(d_fact_tree([])),
   assert(delta_d_fact_tree([])),
   tpi_fixpoint(1),
   retract(d_fact_tree(Tree)),
   retract(delta_d_fact_tree([])),
   pp_tree(Tree), nl,
   retract(current_num(tpi_single_iteration,_)),
   perform_analysis.
 
tpi_fixpoint(0) :-
   !.
tpi_fixpoint(_) :-
   tpi_operator(N),
   tpi_fixpoint(N).

 
/* tpi_operator(N) <-
      */

tpi_operator(N) :-
   get_num(tpi_single_iteration,I),
   write('tpi_iteration_'), write(I), writeln(':'),
   tpi_operator(N,_),
   !.

tpi_operator(N,I) :-
   start_timer(tree_tpi_delta_operator),
   tree_tpi_join(Facts, Extended_Facts, M),
   state_prune(Facts, Pruned_Facts, K),
   tpi_purify_d_fact_tree(Extended_Facts),
   tree_compute_delta_facts(Pruned_Facts, Delta_Facts, J),
   state_can_3(Delta_Facts, Purified_Delta_Facts, N),
   purify_d_fact_tree(Purified_Delta_Facts, I),
   update_d_fact_trees(Purified_Delta_Facts),
   pp_numbers([M, K, J, N, I]),
   write('time         '), ttyflush,
   stop_timer(tree_tpi_delta_operator).

tpi_purify_d_fact_tree(Extended_Facts) :-
   retract(d_fact_tree(Tree_1)),
   tree_state_subtract(Tree_1, Extended_Facts, Tree_2),
   assert(d_fact_tree(Tree_2)).


/*** tests ********************************************************/


test(tpi_fixpoint, join_mode_special) :-
   test_tpi_fixpoint_join_mode_special(
      'evaluable_predicates/test'),
   test_tpi_fixpoint_join_mode_special(
      'pdiax/example1').

test_tpi_fixpoint_join_mode_special(File) :-
   dislog_flags,
   dislog_flag_switch(join_mode, Mode, special),
   dislog_variable_get(example_path, Examples),
   concat(Examples, File, Path),
   dconsult(Path, Program),
   tree_tpi_fixpoint_iteration(Program, Tree),
   pp_tree(Tree), nl,
   dislog_flag_set(join_mode, Mode).


/******************************************************************/


