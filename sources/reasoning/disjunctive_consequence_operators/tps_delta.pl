

/******************************************************************/
/***                                                            ***/
/***          DisLog :  Tps-Delta-Operator                      ***/
/***                                                            ***/
/******************************************************************/
 


:- dynamic
      d_clause/2, dn_clause/3,
      delta_d_fact/1, 
      length_d_fact_pair/2,
      base_literal/1.

:- multifile
      d_clause/2, dn_clause/3.


/*** interface ****************************************************/


/* tps_operator(Program, State_1, State_2) <-
      for a DLP Program the T_ps-operator applied to the d-state
      State_1 yields the d-state State_2. */

tps_operator(Program, State_1, State_2) :-
   tps_delta_operator(Program, State_1, State_1, State_2).


/* tps_operator_dc(Program, State_1, State_2) <-
      for a DLP Program the T_ps-operator applied to the d-state
      State_1 followed by delta computation w.r.t. State_1 and
      canonization yields the d-state State_2. */

tps_operator_dc(Program, State_1, State_2) :-
   tps_delta_operator_dc(Program, State_1, State_1, State_2).


/* tps_delta_operator(Program, State_1, State_2, State_3) <-
      for a DLP Program the T_ps-Delta-operator applied to the 
      d-states State_1 and State_2 yields the d-state State_3. */

tps_delta_operator(Program, State_1, State_2, State_3) :-
   hidden( 1, (
      delta_join(Program, State_1, State_2, State),
      state_prune(State, State_3) ) ),
   !.
 

/* tps_delta_operator_dc(Program, State_1, State_2, State_3) <-
      for a DLP Program the T_ps-Delta-operator applied to the 
      d-states State_1 and State_2 followed by delta computation 
      w.r.t. State_1 and canonization yields the d-state State_3. */

tps_delta_operator_dc(Program, State_1, State_2, State_3) :-
   hidden( 1, (
      delta_join(Program, State_1, State_2, State),
      state_prune(State, Pruned_State),
      state_subtract(Pruned_State, State_1, Delta_State),
      state_can(Delta_State, State_3) ) ),
   !.


/* tree_tps_operator(Program, Tree_1, Tree_2) <-
      for a DLP Program the T_ps-operator applied to the clause
      tree Tree_1 yields the clause tree Tree_2. */

tree_tps_operator(Program, Tree_1, Tree_2) :-
   tree_tps_delta_operator(Program, Tree_1, Tree_1, Tree_2).
 

/* tree_tps_operator_dc(Program, Tree_1, Tree_2) <-
      for a DLP Program the T_ps-operator applied to the clause
      tree Tree_1 followed by delta computation w.r.t. Tree_1 and
      canonization yields the clause tree Tree_2. */

tree_tps_operator_dc(Program, Tree_1, Tree_2) :-
   tree_tps_delta_operator_dc(Program, Tree_1, Tree_1, Tree_2).


/* tree_tps_delta_operator(Program, Tree_1, Tree_2, Tree_3) <-
      for a DLP Program the T_ps-Delta-operator applied to 
      the clause trees Tree_1 and Tree_2 yields the clause tree 
      Tree_2. */

tree_tps_delta_operator(Program, Tree_1, Tree_2, Tree_3) :-
   hidden( 1, (
      tree_join(Program, Tree_1, Tree_2, State),
      state_prune(State, Pruned_State),
      state_to_tree(Pruned_State, Tree_3) ) ).


/* tree_tps_delta_operator_dc(Program, Tree_1, Tree_2, Tree_3) <-
      for a DLP Program the T_ps-Delta-operator applied to
      the clause trees Tree_1 and Tree_2 followed by delta 
      computation w.r.t. Tree_1 and canonization yields the 
      clause tree Tree_3. */

tree_tps_delta_operator_dc(Program, Tree_1, Tree_2, Tree_3) :-
   hidden( 1, (
      tree_join(Program, Tree_1, Tree_2, State),
      state_prune(State, Pruned_State),
      state_tree_subtract(Pruned_State, Tree_1, Delta_State),
      state_can(Delta_State, State_3),
      state_to_tree(State_3, Tree_3) ) ).


/*** implementation ***********************************************/


/* delta_join(State_1, State_2, State_3) <-
      for a DLP asserted as d_fact and d_clause atoms the
      T_ps-Delta-operator applied to the d-states State_1 and
      State_2 yields the d-state State_3 as a multiset, where 
      duplicates are not eliminated yet. */

delta_join(Program, State_1, State_2, State_3) :-
   dlp_to_d_clauses(Program),
   dhs_to_d_facts(State_1),
   dhs_to_delta_d_facts(State_2),
   assert(delta_d_fact(dummy)),
   delta_join(State_3,_),
   dabolish(d_fact/1), 
   dabolish(delta_d_fact/1),
   dabolish(d_clause/2).

delta_join(Facts, M) :-
   ( ( current_num(join_mode,regular),
%      writeln(user,regular),
       Resolve_Predicate = delta_resolvable )
   ; ( current_num(join_mode,special),
%      writeln(user,special),
       Resolve_Predicate = bi_delta_resolvable ) ),
   write('delta join   '), ttyflush,
   start_timer(delta_join),
   findall( Pruned_Fact,
      ( d_clause(Head,Body),
        apply(Resolve_Predicate, [Body, Clause]),
        head_ord_union(Head, Clause,Fact),
        remove_duplicates(Fact, Pruned_Fact),
        ord_disjoint(Pruned_Fact,Body) ),
      Facts ),
   length(Facts, M),
   stop_timer(delta_join).


/* slow_join(Facts, M) <-
      for a DLP asserted as d_fact and d_clause atoms the
      T_ps-operator applied to the empty d-state yields the d-state 
      Facts as a multiset of size M, where duplicates are not 
      eliminated yet. */

slow_join(Facts, M) :-
   write('join         '), ttyflush,
   start_timer(join),
   findall( Pruned_Fact,
      ( d_clause(Head,Body),
        resolvable(Body, Clause),
        append(Head, Clause,Fact),
        remove_duplicates(Fact, Pruned_Fact) ),
      Facts ),
   length(Facts, M),
   stop_timer(join).

 
/* head_ord_union(Head, Clause,Fact) <-
      orders the instantiated head Head of a d_clause and unions it 
      with an ordered resolvent Clause to a derived d_fact Fact. */

head_ord_union(Head, Clause,Fact) :-
%  list_to_ord_set(Clause,Ord_Clause),
   list_to_ord_set(Head,Ord_Head),
   ord_union(Ord_Head, Clause,Fact).


/* delta_resolvable([L1,...,Ln], Clause) <-
      finds at least one delta_d_fact and d_facts  Li' v Ci'
      where all the literals Li simultaneously can be unified with
      the literals Li' and sets  Clause = C1' v ... v Cn'
      To gain efficiency [L1,...,Ln] is broken into two parts by
      append and the first literal of the second part is tested
      whether it is resolvable with a delta_d_fact. The case that
      the body is resolved by old d_facts only is therefore avoided.
      After the delta test for the chosen Literal is successful the
      remaining parts are tested by the ordinary resolvable
      predicate; this is possible because derived facts are
      asserted as delta_d_facts as well as as d_facts.  */

delta_resolvable([L|Ls], Clause) :-
   append(Xs,[Y|Ys],[L|Ls]), append(Xs,Ys,Zs),
   delta_d_fact(Fact), resolve(Y,Fact,Fact_1),
   resolvable(Zs, Clause_1),
   ord_union(Fact_1, Clause_1, Clause).
delta_resolvable([],[]).


/* resolvable([L1,...,Ln], Clause) <-
      finds d_facts  Li' v Ci'  where all the literals Li
      simultaneously can be unified with the literals Li'
      and sets  Clause = C1' v ... v Cn' */
 
resolvable([L|Ls], Clause) :-
   d_fact(Fact), resolve(L,Fact, Clause_1),
   resolvable(Ls, Clause_2),
   ord_union(Clause_1, Clause_2, Clause).
resolvable([],[]).
 
resolve(Literal,[Literal|Ls],Ls).
resolve(Literal,[L|Ls],[L|Ls1]) :-
   resolve(Literal,Ls,Ls1).


/* delta_resolvable([L1,...,Ln]) <-
      finds at least one delta_d_fact and d_facts  Li' where all the 
      literals Li simultaneously can be unified with the literals Li'. */

delta_resolvable([L|Ls]) :-
   append(Xs,[Y|Ys],[L|Ls]), append(Xs,Ys,Zs),
   delta_d_fact([Y]),
   resolvable(Zs).
delta_resolvable([]).

resolvable([L|Ls]) :-
   d_fact([L]),
   resolvable(Ls).
resolvable([]).
 
us_delta_resolvable([L|Ls]) :-
   append(Xs,[Y|Ys],[L|Ls]), append(Xs,Ys,Zs),
   delta_d_fact([Y]),
   us_resolvable(Zs).
us_delta_resolvable([]).

us_resolvable([L|Ls]) :-
   ( u_fact([L])
   ; d_fact([L]) ),
   us_resolvable(Ls).
us_resolvable([]).


/* bi_delta_resolvable([L1,...,Ln], Clause) <-
      finds at least one delta_d_fact and d_facts  Li' v Ci' 
      where all the literals Li simultaneously can be unified
      with the literals Li' and sets  Clause = C1' v ... v Cn'
      To gain efficiency [L1,...,Ln] is broken into two parts by
      append and the first literal of the second part is tested
      whether it is resolvable with a delta_d_fact. The case that
      the body is resolved by old d_facts only is therefore avoided.
      After the delta test for the chosen Literal is successful the
      remaining parts are tested by the ordinary resolvable
      predicate; this is possible because derived facts are
      asserted as delta_d_facts as well as as d_facts.  */
 
bi_delta_resolvable([L|Ls], Clause) :-
   append(Xs, [Y|Ys], [L|Ls]), append(Xs, Ys, Zs),
   base_literal(Y),
   delta_d_fact(Fact), resolve(Y, Fact, Clause_1),
   bi_resolvable(Zs, Clause_2),
   ord_union(Clause_1, Clause_2, Clause).
%  ord_disjoint(Clause_1, Clause_2).
bi_delta_resolvable([], []).
 
 
/* bi_resolvable([L1,...,Ln], Clause) <-
      finds d_facts  Li' v Ci'  where all the literals Li
      simultaneously can be unified with the literals Li'
      and sets  Clause = C1' v ... v Cn' */
 
bi_resolvable([L|Ls], Clause) :-
   \+ base_literal(L),
   !,
   call(L),
   bi_resolvable(Ls, Clause).
bi_resolvable([L|Ls], Clause) :-
   d_fact(Fact), resolve(L, Fact, Clause_1),
   bi_resolvable(Ls, Clause_2),
   ord_union(Clause_1, Clause_2, Clause).
bi_resolvable([], []).



/* purify_d_facts(Facts, I) <-
      all asserted d_facts which are subsumed by Facts
      are retracted,
      I counts the retracted d_facts. */

purify_d_facts(Facts, I) :-
   write('purify d_f   '), ttyflush,
   start_timer(d_fact_purifying),
   retract_implied_d_facts_loop(Facts, I),
   stop_timer(d_fact_purifying).


/* update_d_facts(Purified_Delta_Facts) <-
      all clauses in Purified_Delta_Facts are asserted as
      d_facts and as delta_d_facts. */

update_d_facts(Purified_Delta_Facts) :-
   write('fact update  '), ttyflush,
   start_timer(update_d_facts),
   assert_arguments(d_fact, Purified_Delta_Facts),
   assert_arguments(delta_d_fact, Purified_Delta_Facts),
   stop_timer(update_d_facts).


/* remove_subsumed_delta_facts(Purified_Facts) <-
      reduces the asserted delta_d_facts to their canonical form,
      which again is asserted as delta_d_facts.  */
 
remove_subsumed_delta_d_facts(Purified_Facts) :-
   collect_arguments(delta_d_fact, Facts),
   abolish(delta_d_fact, 1),
   state_prune(Facts, Pruned_Facts),
   state_can(Pruned_Facts, Purified_Facts),
   assert_arguments(delta_d_fact, Purified_Facts).

 
/* tree_compute_delta_facts(Facts, Delta_Facts, J) <-
      the pruned list Facts of lately derived facts is checked 
      for facts not subsumed by the current d_fact_tree,
      Delta_Facts is the resulting list of J facts.  */

tree_compute_delta_facts(Pruned_Facts, Delta_Facts, J) :-
   write('tree delta   '), ttyflush,
   start_timer(tree_compute_delta_facts),
   d_fact_tree(Tree),
   abolish(delta_d_fact, 1),
   tree_delta_facts(Tree, Pruned_Facts, J),
   collect_arguments(delta_d_fact, Delta_Facts),
   abolish(delta_d_fact,1),
   stop_timer(tree_compute_delta_facts).

tree_delta_facts(Tree, [Fact|Facts], J) :-
   tree_subsumes_clause(Tree, Fact),
   !, 
   tree_delta_facts(Tree, Facts, J).
tree_delta_facts(Tree, [Fact|Facts], J) :-
   assert(delta_d_fact(Fact)),
   tree_delta_facts(Tree, Facts, M),
   J is M + 1.
tree_delta_facts(_, [], 0).


meta_tree_compute_delta_facts(Pruned_Facts, Delta_Facts, J) :-
   write('delta        '), ttyflush,
   start_timer(tree_compute_delta_facts),
   d_fact_tree(Tree),
   abolish(delta_d_fact, 1),
   meta_tree_delta_facts(Tree, Pruned_Facts, J),
   collect_arguments(delta_d_fact, Delta_Facts),
   abolish(delta_d_fact, 1),
   stop_timer(tree_compute_delta_facts).

meta_tree_delta_facts(Tree, [Fact|Facts], J) :-
   tree_meta_subsumes_clause(Tree, Fact),
   !,
   meta_tree_delta_facts(Tree, Facts, J).
meta_tree_delta_facts(Tree, [Fact|Facts], J) :-
   assert(delta_d_fact(Fact)),
   meta_tree_delta_facts(Tree, Facts, M),
   J is M + 1.
meta_tree_delta_facts(_, [], 0).



/* compute_delta_facts(Facts, Delta_Facts, J) <-
      the pruned list Facts of lately derived facts is checked
      for facts not subsumed by the current d_facts,
      Delta_Facts is the resulting list of J facts.  */ 
 
compute_delta_facts(Facts, Delta_Facts, J) :-
   write('delta        '), ttyflush,
   start_timer(compute_delta_facts),
   abolish(delta_d_fact, 1),
   assert(d_fact(dummy)),
   delta_facts(Facts, J),
   collect_arguments(delta_d_fact, Delta_Facts),
   abolish(delta_d_fact, 1),
   retract(d_fact(dummy)),
   stop_timer(compute_delta_facts).


/* delta_facts(Facts, J) <-
      asserts all the new facts in Facts, their number is J.  */

delta_facts([Fact|Facts], J) :-
   d_fact(Fact_1), ord_subset(Fact_1, Fact), 
   !,
   delta_facts(Facts, J).
delta_facts([Fact|Facts], J) :-
   assert(delta_d_fact(Fact)),
   delta_facts(Facts, M),
   J is M + 1.
delta_facts([], 0).


/* slow_compute_delta_facts(Facts, N, I) <-
      asserts all the new facts in Facts, their number is N,
      retracts old facts subsumed by new facts, their number is I */

slow_compute_delta_facts([Fact|Facts], N, I) :-
   write('delta        '), ttyflush,
   start_timer(slow_compute_delta_facts),
   assert(d_fact(dummy)),
   slow_delta_facts([Fact|Facts], N, I),
   retract(d_fact(dummy)),
   stop_timer(slow_compute_delta_facts).

slow_delta_facts([Fact|Facts], N, I) :-
   d_fact(Fact_1), subset(Fact_1, Fact),
   !,
   slow_delta_facts(Facts, N, I).
slow_delta_facts([Fact|Facts], N, I) :-
   d_fact(Fact_1), subset_not_eq(Fact, Fact_1),
   !,
   retract(d_fact(Fact_1)), assert(d_fact(Fact)),
   slow_delta_facts(Facts, M, J),
   N is M + 1,
   I is J + 1.
slow_delta_facts([Fact|Facts], N, I) :-
   assert(d_fact(Fact)),
   slow_delta_facts(Facts, M, I),
   N is M + 1.
slow_delta_facts([], 0, 0).
 

/* ordered_state_subtract(State_1, Tree, State_3) <-
      computes in State_3 all facts from State_1 which are not subsumed by 
      some clause tree in Tree. */

ordered_state_subtract(State_1, State_2, State_3) :-
   write('o delta hs   '), ttyflush,
   start_timer(state_subtract),
   ordered_state_subtract_loop(State_1, State_2, State_3),
   stop_timer(state_subtract).

ordered_state_subtract_loop([], _, []).
ordered_state_subtract_loop([C1|Cs1], Cs, Cs2) :-
   ordered_subsumes(Cs, C1),
   !,
   ordered_state_subtract_loop(Cs1, Cs, Cs2).
ordered_state_subtract_loop([C1|Cs1], Cs, [C1|Cs2]) :-
   ordered_state_subtract_loop(Cs1, Cs, Cs2).

ordered_subsumes(Cs, C1) :-
   member(Tree, Cs), tree_subsumes_clause(Tree, C1).
%  length(C1, M),
%  ordered_subsumes_loop(Cs, [M, C1]).
   
ordered_subsumes_loop([[N, _]|_], [M, _]) :-
   N > M,
   !,
   fail.
ordered_subsumes_loop([[_, Cs]|_], [_, C1]) :-
   tree_subsumes_clause(Cs, C1).
%  member(C2, Cs), ord_subset(C2, C1).
ordered_subsumes_loop([_|Cs], [M, C1]) :-
   ordered_subsumes_loop(Cs, [M, C1]).
 
 
/* retract_implied_d_facts(Fact, K) <-
      retracts all d_facts which are supersets of Fact, their number 
      is K it uses the auxilliary predicate retract_idf */

retract_implied_d_facts_loop([Fact|Facts], K) :-
   retract_implied_d_facts(Fact, K1),
   retract_implied_d_facts_loop(Facts, K2),
   K is K1 + K2. 
retract_implied_d_facts_loop([], 0).

retract_implied_d_facts(Fact, K) :-
   retract_idf(Fact, 0, K).

retract_idf(Fact, I, J) :-
   d_fact(Fact_1), ord_subset(Fact, Fact_1),
   !,
   retract(d_fact(Fact_1)),
   M is I + 1,
   retract_idf(Fact, M, J).
retract_idf(_, K, K).


/* collect_derived_d_facts(State) <-
      Collects the derived disjunctions to the d-state State,
      given the current DisLog tps_mode I,
      if I =< 2, then the asserted d_fact atoms form State,
      if I >= 3, then the asserted d_fact_tree forms State. */
      
collect_derived_d_facts(State) :-
   current_num(tps_mode, I),
   I =< 2,
   collect_arguments(d_fact, State_1),
   list_to_ord_set(State_1, State).
collect_derived_d_facts(State) :-
   current_num(tps_mode, I),
   I >= 3,
   d_fact_tree(Tree), tree_to_state(Tree, State_1),
   list_to_ord_set(State_1, State).
 

/******************************************************************/


