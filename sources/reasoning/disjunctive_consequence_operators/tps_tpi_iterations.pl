

/******************************************************************/
/***                                                            ***/
/***          DisLog :  Tps/Tpi-Iterations                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tpi_ordinal_powers_file(File, I) <-
      */

tpi_ordinal_powers_file(File, I) :-
   dconsult(File, Program),
   tpi_ordinal_powers(Program, I).

tpi_ordinal_powers(Program, I) :-
   tpi_ordinal_powers(Program, 1, I, [], [[]], _, _).

tpi_ordinal_powers(Program, I, I,
      Tree_1, Delta_1, Tree_2, Delta_2) :-
   !,
   tpi_ordinal_powers_single_step(Program, I,
      Tree_1, Delta_1, Tree_2, Delta_2).
tpi_ordinal_powers(Program, I, J,
      Tree_1, Delta_1, Tree_2, Delta_2) :-
   tpi_ordinal_powers_single_step(Program, I,
      Tree_1, Delta_1, Tree_3, Delta_3),
   K is I + 1,
   tpi_ordinal_powers(Program, K, J,
      Tree_3, Delta_3, Tree_2, Delta_2).

tpi_ordinal_powers_single_step(Program, I,
      Tree_1, Delta_1, Tree_2, Delta_2) :-
   hidden( 2, (
      tree_tpi_delta_operator(Program,
         Tree_1, Delta_1, Tree_2, Delta_2),
      tree_to_state(Tree_2, State_2),
      tree_to_state(Delta_2, Delta_State_2) ) ),
   write_list(['M_', I, ' =']),
   dportray2(chs, State_2),
   write_list(['D_', I, ' =']),
   dportray2(chs, Delta_State_2).


/* tps_ordinal_powers_file(File, I) <-
      */

tps_ordinal_powers_file(File, I) :-
   dconsult(File, Program),
   tps_ordinal_powers(Program, I).


/* tps_ordinal_powers(Program, I, State) <-
      */

tps_ordinal_powers(Program, I) :-
   tps_ordinal_powers(Program, I, _).

tps_ordinal_powers(Program, I, State) :-
   tps_ordinal_powers(Program, 1, I, [], State).

tps_ordinal_powers(Program, I, I, State_1, State_2) :-
   !,
   tps_operator_dc_for_powers(Program, I, State_1, State_2).
tps_ordinal_powers(Program, I, J, State_1, State_2) :-
   tps_operator_dc_for_powers(Program, I, State_1, State),
   K is I + 1,
   tps_ordinal_powers(Program, K, J, State, State_2).

tps_operator_dc_for_powers(Program, I, State_1, State_2) :-
   hidden( 2, (
      tps_operator_dc(Program, State_1, State_A),
      length_order(State_A, State_B),
      state_union(State_1, State_B, State_2) ) ),
   J is I - 1,
   write_list(user, ['Tps^', I, ' \\ Tps^', J, ' =']),
   dportray(dhs, State_B).


/*** tests ********************************************************/


test(nmr:tps_iterations, 1) :-
   tps_ordinal_powers_file('teaching/pf_or_qf.pl', 5).
test(nmr:tpi_iterations, 1) :-
   tpi_ordinal_powers_file('teaching/pf_or_qf.pl', 3).
 

/******************************************************************/


