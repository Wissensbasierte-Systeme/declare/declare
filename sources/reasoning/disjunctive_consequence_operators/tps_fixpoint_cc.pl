

/******************************************************************/
/***                                                            ***/
/***          DisLog :  Tps-Fixpoint-Iteration in C++           ***/
/***                                                            ***/
/******************************************************************/
 

:- multifile 
      tps_iteration_ps/2.
:- dynamic 
      tps_iteration_ps/2.


/*** interface ****************************************************/


tps_operator_dc_ps(Program,State1,State2) :-
   distore(Program,'results/program'),
   distore(State1,'results/input'),
   concat(['tools/tps_operator_dc_ps results/program ',
       'results/input results/output > /dev/null '], Command),
   ( unix(system(Command))
   ; true ),
%  ( unix(system('tools/tps_operator_dc_ps results/program
%       results/input results/output > /dev/null '))
%  ; true ),
   get_tps_ps_result('results/output',State),
   select_facts(Program,Program_Facts),
   ord_union([Program_Facts,State1],State3),
   state_subtract(State3,State,State4),
   ord_union(State4,State,State2).

tps_fixpoint_cc(Program,Minimal_Model_State) :-
   select_facts(Program,State),
   tps_fixpoint_ps(Program,State,Minimal_Model_State).

tps_fixpoint_cc(Program,State1,State2) :-
   tps_fixpoint_ps(Program,State1,State2).

tps_fixpoint_ps(Filename) :-
   dconsult(Filename,Program),
   prepare_analysis,
   tps_fixpoint_ps(Program,[],State),
   write(' Ms ='), pp_dhs(State), nl,
   length(State,N),
   write(N), writeln(' disjunctions'),
   perform_analysis.   

tps_fixpoint_ps(Program,State) :-
   distore(Program,'results/program'),
   ( unix(system('tools/tps_fixpoint_ps results/program \
        results/output'))
   ; true ),
   get_tps_ps_result('results/output',State).
tps_fixpoint_ps(Program,State1,State2) :-
   distore(Program,'results/program'),
   distore(State1,'results/input'),
   ( unix(system('tools/tps_fixpoint_ps results/program \
        results/input results/output > /dev/null '))
   ; true ),
   get_tps_ps_result('results/output',State2).


/*** implementation ***********************************************/


dislog_tps(Program_File,State_File) :-
   dconsult(Program_File,Program),
   tree_tps_fixpoint(Program,[],Tree),
   tree_to_state(Tree,State),
   pp_dhs_new(State_File,State).     


get_tps_ps_result(File,State) :-
   [File],
   findall( Clause,
      ( retract(tps_iteration_ps(_,Clauses)),
        member(Clause,Clauses) ),
      State2 ),
   state_sort(State2,State).


tps_operator_dc_compare :-
   prepare_analysis,
   unix(system('tps_operator_dc_ps examples/program \
      examples/input output')),
   get_tps_ps_result(output,State1),
   perform_analysis,
   dportray(dhs,State1),
   prepare_analysis,
   dconsult(program,Program),
   dconsult(input,State2),
   state_to_tree(State2,Tree2),
   tree_tps_operator_dc(Program,Tree2,Tree3),
   tree_to_state(Tree3,State3),
   perform_analysis,
   dportray(dhs,State3),
   state_compare(State1,State3).


tps_fixpoint_compare(Filename) :-
   dconsult(Filename,Program),
   tps_fixpoint_compare(Program,[],_).

tps_fixpoint_compare(Program,State1,State2) :-
   prepare_analysis,
   tps_fixpoint_ps(Program,State1,State2),
   perform_analysis,
   dportray(dhs,State2),
   prepare_analysis,
   state_to_tree(State1,Tree1),
   tree_tps_fixpoint(Program,Tree1,Tree2),
   tree_to_state(Tree2,State3),
   perform_analysis,
   dportray(dhs,State3),
   state_compare(State2,State3).

state_compare(State,State) :-
   !,
   writeln('the states are equal').  
state_compare(_,_) :-
   writeln('the states are different').  


/******************************************************************/


