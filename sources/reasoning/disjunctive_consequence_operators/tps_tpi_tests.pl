

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Tests for Tps vs. Tpi                    ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(tpi_tps, 1) :-
   dislog_variable_get(example_path, Examples),
   concat(Examples, 'nmr/path_disjunctive/path_abcd', File),
   tpi_tps_test(File).
 
tpi_tps_test(File) :-
%  dconsult(File, Program_1),
   dread(lp, File, Program_1),
   dnlp_to_dlp(Program_1, Program),
   dwrite(lp, Program),
   measure(
      tree_tpi_fixpoint(Program, [[]], Model_Tree_1), T1 ),
   measure(
      model_tree_to_clause_tree(Model_Tree_1, Clause_Tree_1), T2 ),
   measure(
      tree_tps_fixpoint(Program, [], Clause_Tree_2), T3 ),
   measure(
      model_tree_to_clause_tree(Clause_Tree_2, Model_Tree_2), T4 ),
   T5 is T1 + T2, T6 is T3 + T4, nl, 
   write('program_to_models           '), pp_times([T1]),
   write('program_to_state_to_models  '), pp_times([T6, T3, T4]),
   write('program_to_state            '), pp_times([T3]),
   write('program_to_models_to_state  '), pp_times([T5, T1, T2]),
   tree_to_state(Clause_Tree_1, State_1),
   tree_to_state(Clause_Tree_2, State_2), 
   tree_to_state(Model_Tree_1, Models_1),
   tree_to_state(Model_Tree_2, Models_2),
   dportray(dhs, State_1),
   dportray(chs, Models_1),
   length(State_1, N), length(Models_1, M), pp_numbers([N, M]),
   !,
   State_1 = State_2, Models_1 = Models_2.
   

/******************************************************************/


