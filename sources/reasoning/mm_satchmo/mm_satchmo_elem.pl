

/******************************************************************/
/***                                                            ***/
/***             Module:  MM SATCHMO - Elementary               ***/
/***                                                            ***/
/******************************************************************/


% :- op(1190, xfx, --->).


/*** interface ****************************************************/
 

minimal_models_satchmo_loop :-
   minimal_models_satchmo_loop(y).

minimal_models_satchmo_loop(Answer) :-
   mm_satchmo(true),
   display_constraint(Answer),
   fail.
minimal_models_satchmo_loop(_).


collect_models_satchmo(Models) :-
   get_actual_satchmo_program_id(Program_Id),
   findall( M,
      ( get_satchmo_clause(Program_Id,(B ---> false)),
        comma_structure_to_ord_set(B,M) ),
      Models ).


make_predicates_dynamic([[Pred,Arity]|Preds_and_Arities]) :-
   Atom =.. [/,Pred,Arity],
   dynamic(Atom),
%  dynamic(Pred/Arity),
   make_predicates_dynamic(Preds_and_Arities).
make_predicates_dynamic([]).



/*** implementation ***********************************************/

   
mm_satchmo_special_test(C) :-
   dislog_flag_get(satchmo_test_yes_or_no,yes),
   !,
   do_mm_satchmo_special_test(C).
mm_satchmo_special_test(_).


% first call: mm_satchmo(true,C).

mm_satchmo(C1) :-
   violated_instances(Set),
   !,
   mm_satisfy_all(Set,C1,C2),
   mm_satchmo(C2).
mm_satchmo(C) :-
   mm_satchmo_special_test(C),
   asserta_satchmo_clause((C ---> false)).

violated_instances(Disjunctions) :-
   dislog_flag_get(mm_satchmo,1),
   !,
   findall( Head,
      violated_instance(Head),
      Disjunctions ),
   !,
   \+ ( Disjunctions = [] ).
violated_instances(Disjunctions) :-
   dislog_flag_get(mm_satchmo,2),
   !,
   violated_instance(Head),
   Disjunctions = [Head].
 
mm_satisfy_all([],C,C).
mm_satisfy_all([C|Cs],C1,C3) :-
   mm_satisfy(C,A),
   and_merge(A,C1,C2),
   mm_satisfy_all(Cs,C2,C3).
 
mm_satisfy(Disjunction,Atom) :-
   cs_component(Atom,Postfix,Disjunction),
   \+ (Atom = false),
   assume_satchmo(Atom),
   assume_neg(Postfix).


% from cs-satchmo

cs_component(Atom,Disjunction,(Atom;Disjunction)).
cs_component(Atom,Disjunction,(_;Disjunction_Tail)) :-
   !,
   cs_component(Atom,Disjunction,Disjunction_Tail).
cs_component(Atom,false,Atom).


% auxilliary

display_constraint(Answer) :-
   Answer == y,
   !,
   display_constraint.
display_constraint(_).
 
display_constraint :-
   once(get_satchmo_clause(B ---> false)),
   retract(counter(N1)),
   N2 is N1 + 1,
   assert(counter(N2)),
   write(N2),
   write(': '),
   writeln(B).


% from fair-satchmo
 
violated_instance(Head) :-
   get_satchmo_clause((Body ---> Head)),
   call(Body),
   \+ call(Head).
 

assume_satchmo_clause(Rule) :-
   asserta_satchmo_clause(Rule).
assume_satchmo_clause(Rule) :-
   once(retract_satchmo_clause(Rule)).
 

assume_satchmo_atom(Atom) :-
   asserta(Atom).
assume_satchmo_atom(Atom) :-
   once(retract(Atom)),
   fail.


get_satchmo_clause(Rule) :-
   get_actual_satchmo_program_id(Program_Id),
   get_satchmo_clause(Program_Id,Rule).

get_satchmo_clause(Program_Id,Rule) :-
   ( ob_clause(Program_Id,Rule)
   ; s_clause(Program_Id,Rule) ).
 

asserta_satchmo_clause(Rule) :-
   get_actual_satchmo_program_id(Program_Id),
   asserta(s_clause(Program_Id,Rule)),
   !.
 
retract_satchmo_clause(Rule) :-
   get_actual_satchmo_program_id(Program_Id),
   retract(s_clause(Program_Id,Rule)),
   !.
 
 
assert_satchmo_program(Program) :-
   get_actual_satchmo_program_id(Program_Id),
   assert_satchmo_program(Program_Id,Program).
 
assert_satchmo_program(Program_Id,[R|Rs]) :-
   asserta(s_clause(Program_Id,R)),
   assert_satchmo_program(Program_Id,Rs).
assert_satchmo_program(_,[]).


assert_satchmo_program(X_Clause,Program_Id,[R|Rs]) :-
   Goal =.. [X_Clause,Program_Id,R],
   asserta(Goal),
   assert_satchmo_program(X_Clause,Program_Id,Rs).
assert_satchmo_program(_,_,[]).

 
retract_satchmo_program(X_Clause,Program_Id) :-
   Goal =.. [X_Clause,Program_Id,_],
   retract(Goal),
   fail.
retract_satchmo_program(_,_).

 
get_actual_satchmo_program_id(Program_Id) :-
   dislog_flag_get(mm_satchmo_p_id,Program_Id).
 
 
/******************************************************************/

