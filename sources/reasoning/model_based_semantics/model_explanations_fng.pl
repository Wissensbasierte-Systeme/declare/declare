

models:Es --->
   html:[head:[], body:[models, ol:Es]].
model:Es --->
   li:['model', ul:Es].
atom:[name:Atom]:Es --->
   li:['atom=', font:[color:'#ee3333']:[Atom]|Es_2] :-
   ( Es = [],
     Es_2 = []
   ; Es_2 = [ul:Es] ).
rule:[name:Rule]:[E] ---> E :-
   parse_dislog_rule(Rule, _, _, []).
rule:[name:Rule]:Es --->
   li:['rule=', font:[color:'#3333ee']:[Rule], ul:Es] :-
   Es \= [].
rule:[name:Rule]:_ --->
   li:['rule=', font:[color:'#3333ee']:[Rule]].


