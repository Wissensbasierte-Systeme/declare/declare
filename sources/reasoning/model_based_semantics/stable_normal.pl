

/******************************************************************/
/***                                                            ***/
/***         NMR:  Stable Models - for NLP's                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* stable_models_normal(+Program,-Stable_Models) <-
      The set Stable_Models of all stable models of the normal 
      logic program Program is computed. */

stable_models_normal(Program,Stable_Models) :-
   stable_models_operator_semi_strat_with_folding(
      Program,Stable_Models).  

/* stable_models_normal_e(+Program,+E,-Stable_Models) <-
      The set Models of all E-stable models of the normal logic 
      program Program is computed for a set E of ground atoms.
      E-stable models are stable models of Programm that 
      contain E.  */

stable_models_normal_e(Program,E,Stable_Models) :-
   stable_models_operator_e(Program,E,Stable_Models).

 
/* stable_models_operator_sim(+Program,-Models) <-
      The set Models of all stable models of a normal logic program 
      Program is computed by applying stable_models_operator 
      to the simplification of Program. */

stable_models_operator_sim(Program,Models) :-
   simplify_program_gamma_2(Program,Program1),
%  writeln(user,Program1), wait,
   stable_models_operator(Program1,Models),
   !.


/* stable_models_operator_semi_strat(+Program,-Models) <-
      The set Models of all stable models of a normal logic program 
      Program is computed.
      For the simplification of Program, a semi-stratification is 
      computed.  Then the stable models are computed by successively
      evaluating the semi-strata.  Depending on whether a 
      semi-stratum is stratified or not, the tpi_operator or the
      branching algorithm stabalg_branch is applied to it. */

stable_models_operator_semi_strat(Program,Models) :-
   simplify_program_gamma_2(Program,Program1),
   semi_stratified_evaluation(Program1,Models),
   !.


/* stable_models_operator_semi_strat_with_folding(+Program,-Models) <-
      The set Models of all stable models of a normal logic program
      Program is computed by a semi-stratified evaluation (see above).
      Here, the simplified program is further optimized by factoring 
      out same negative body atoms of ground rules to reduce the 
      number of ground rules with non empty negative body.  */

stable_models_operator_semi_strat_with_folding(Program,Models) :-
   simplify_program_gamma_2(Program,Program1),
   modified_ground_folding_program(Program1,Program2),
%  writeln(user,'after modified_ground_folding_program'), wait,
   semi_stratified_evaluation(Program2,Models),
   !.


/* stable_models_operator_branch(+Program,-Models) <-
      The set Models of all stable models of the normal logic program
      Program is computed by a branching algorithm.
      The branching is based on the Program rules with non-empty 
      negative body, hence the algorithm stabalg_branch is a 
      rule-based branching algorithm.  */

stable_models_operator_branch(Program,Models) :-
   simplify_program_gamma_2(Program,Program1),
   stabalg_branch(Program1,Models),
   !.


/* stable_models_operator_branch_with_folding(+Program,-Models) <-
      The set Models of all stable models of the normal logic program
      Program is computed by a branching algorithm with folding.
      This branching algorithm has got the additional property, that 
      a ground folding step is applied to the simplified program.  */

stable_models_operator_branch_with_folding(Program,Models) :-
   simplify_program_gamma_2(Program,Program1),
   modified_ground_folding_program(Program1,Program2),
   stabalg_branch(Program2,Models),
   !.


/* stable_models_operator_e(+Program,+E,-Models) <-
      The set Models of all E-stable models of the normal logic program
      Program is computed for a set E of ground atoms.
      E-stable models are stable models of Programm that contain E.  */

stable_models_operator_e(Program,E,Models) :-
   simplify_program_gamma_2(Program,Program1),
   e_stable_models_computation(Program1,E,Models).
  

/* gamma_operator_ground(Program,I1,I2) <-
      The operator Gamma_P applied to the normal logic program
      Program and the Herbrand interpretation I1 yields the
      Herbrand interpretation I2. */
 
gamma_operator_ground(Program,I1,I2) :-
   gl_transformation(Program,I1,Program2),
   minimal_model_operator_definite_2(Program2,I2).
%  simplify_program(Program2,Program3),
%  tps_fixpoint_cc(Program3,State),
%  ord_union(State,I2).

gamma_operator_ground_version_2(Program,I1,I2) :-
   gl_transformation(Program,I1,Program2),
   minimal_model_operator_definite_1(Program2,I2).

gamma_2_operator_ground(Program,I1,I2) :-
   gamma_operator_ground(Program,[],I1),
   gamma_operator_ground(Program,I1,I2).
 
gamma_2_operator_ground_version_2(Program,I1,I2) :-
   gamma_operator_ground_version_2(Program,[],I1),
   gamma_operator_ground_version_2(Program,I1,I2).
 
gamma_4_operator_ground(Program,I3,I4) :-
   gamma_operator_ground(Program,[],I1),
   gamma_operator_ground(Program,I1,I2),
   gamma_operator_ground(Program,I2,I3),
   gamma_operator_ground(Program,I3,I4).
 

/* simplify_program_gamma_2(Program1,Program2) <-
      The logic program Program1 is simplified using the operator
      Gamma_P to the logic program Program2. */

simplify_program_gamma_2(Program1,Program2) :-
   simplify_program_gamma_2(Program1,_,Program2).

simplify_program_gamma_2(Program1,GrdP,Program2) :-
%  writeln(user,simplify_program_gamma_2),
   ground_and_standardize_program(Program1,GrdP),
   gamma_2_operator_ground(GrdP,I_Upper,I_Lower),
   simplify_program_2(GrdP,I_Lower,I_Upper,Program),
   list_to_ord_set(Program,Program2).

simplify_program_gamma_4(Program1,Program2) :-
   simplify_program_gamma_4(Program1,_,Program2).

simplify_program_gamma_4(Program1,GrdP,Program2) :-
   ground_and_standardize_program(Program1,GrdP),
   gamma_4_operator_ground(GrdP,I_Upper,I_Lower),
   simplify_program_2(GrdP,I_Lower,I_Upper,Program),
   list_to_ord_set(Program,Program2).

simplify_program_gamma_omega(Program1,Program2) :-
   ground_and_standardize_program(Program1,Ground_Program1),
   simplify_program_gamma_omega([],Ground_Program1,Program),
   list_to_ord_set(Program,Program2).

simplify_program_gamma_omega(Program1,Program2,P_sim) :-
   ( ( Program1 \== Program2,
       gamma_2_operator_ground_version_2(Program2,I_Upper,I_Lower),
       simplify_program_2(Program2,I_Lower,I_Upper,Program3),
       simplify_program_gamma_omega(Program2,Program3,P_sim) )
   ; P_sim = Program1 ).
 

/*** implementation ***********************************************/


/* stabalg_branch(Program,I_plus,I_minus,Models) <-
      Given a normal logic program Program, the set Models of 
      all stable models of Program is computed using a 
      sophisticated backtracking algorithm.
      The two disjoint sets I_plus and I_minus of atoms denote
      the atoms that have been set to true and false,
      respectively, so far. */

stabalg_branch(Program,Models) :-
   stabalg_branch(Program,[],[],Models).

stabalg_branch(Program,I_plus,I_minus,Models) :-
   stabalg_branch(Program,I_plus,I_minus),
   setof2( I, retract(stable_model_stabalg_branch(I)), Models ).

stabalg_branch(Program,I_plus,I_minus) :-
   transitive_eval_of_def_rules(Program,I_plus,I_new,Program_2),
   ord_union(I_plus,I_new,I_plus_2),
   ord_disjoint(I_plus_2,I_minus),
   extension_of_interpretation_possible_with(Program_2,I_plus_2,Rules),
   ( ( Rules = [],
       assert(stable_model_stabalg_branch(I_plus_2)) )
   ; stabalg_branch_create_branches(Rules,Program_2,I_plus_2,I_minus) ).
stabalg_branch(_,_,_).


extension_of_interpretation_possible_with(Program,I_plus,Rules) :- 
   setof2( Head-PosBody-NegBody, 
      ( member(Head-PosBody-NegBody,Program),
        ord_subset(PosBody,I_plus), 
        ord_disjoint(NegBody,I_plus) ),
      Rules ), !.

extension_of_interpretation_possible_with(Program,I_plus,I_minus,Rules) :-
   setof2( Head-PosBody-NegBody,
      ( member(Head-PosBody-NegBody,Program),
        \+ ord_subset(Head,I_plus),
        \+ ord_subset(Head,I_minus),
        ord_subset(PosBody,I_plus), 
        ord_disjoint(NegBody,I_plus) ),
      Rules ), !.

extension_of_interpretation_impossible(Program,I_plus) :-
   extension_of_interpretation_possible_with(Program,I_plus,Rules),
   Rules = [].


stabalg_branch_create_branches([R|Rs],Program,I_plus,I_minus) :-
   stabalg_branch_create_branch(R,Program,I_plus,I_minus),
   stabalg_branch_create_branches(Rs,Program,I_plus,I_minus).
stabalg_branch_create_branches([],_,_,_).

stabalg_branch_create_branch(Rule,Program,I_plus,I_minus) :-
   parse_dislog_rule(Rule,Head,_,NegBody),
   list_to_ord_set(Head,OrdHead),
   list_to_ord_set(NegBody,OrdNegBody),
   ord_union(OrdHead,I_plus,I_plus_2),
   ord_union(OrdNegBody,I_minus,I_minus_2),
   ord_disjoint(OrdHead,I_minus_2),
   ord_disjoint(I_plus_2,I_minus_2),
%  ( delete(Program,Rule,Program_1); Program_1 = [] ),
%  eliminate_rules(Program_1,I_plus_2,I_minus_2,Program_2),
   delete(Program,Rule,Program_1),
   simplify_program_1_a(Program_1,I_plus_2,I_minus_2,Program_2),
   stabalg_branch(Program_2,I_plus_2,I_minus_2).
stabalg_branch_create_branch(_,_,_,_).


transitive_eval_of_def_rules(Program1,I_plus,I_new,Program2) :-
   setof2( H-B1,
      R^( member(R,Program1),
          parse_dislog_rule(R,H,B1,B2), B2 = [] ),
      Program ),
   Program \== [],
   !,
   setof2( [A]-[], 
      member(A,I_plus), 
      Facts ),
   append(Facts,Program,List1), 
   list_to_ord_set(List1,Definite_Program),
   minimal_model_operator_definite_2(Definite_Program,I_new),
   setof2( H-B1-[],
      ( member(H-B1,Program), 
        ord_subset(B1,I_new) ),
      List2 ),
   list_to_ord_set(List2,Evaluated_Rules),
   ord_subtract(Program1,Evaluated_Rules,Program2).
transitive_eval_of_def_rules(Program,_,[],Program).

eliminate_rules(Program_1,I_plus,I_minus,Program_2) :-
   setof2( Head-PosBody-NegBody,
      L^( member(Head-PosBody-NegBody,Program_1),
          member(L,NegBody), member(L,I_plus) ),
      Delete_List_1 ),
   list_to_ord_set(Program_1,Ordered_Program_1),
   list_to_ord_set(Delete_List_1,Delete_Program_1),
   ord_subtract(Ordered_Program_1,Delete_Program_1,Program_3),
   setof2( Head-PosBody-NegBody,
      L^( member(Head-PosBody-NegBody,Program_1),
          member(L,PosBody), member(L,I_minus) ),
      Delete_List_2 ),
   list_to_ord_set(Delete_List_2,Delete_Program_2),
   ord_subtract(Program_3,Delete_Program_2,Program_2), 
   !.

perform_definite_rules(Cs,P,NewCs,NewFacts,NewP) :-
   setof2( A-B-[], member(A-B-[],Cs), DelRules ),
   ord_subtract(Cs,DelRules,NewCs),
   findall( A, member([A]-_-_,DelRules), NewFacts1 ),
   list_to_ord_set(NewFacts1,NewFacts),
   list_to_ord_set(P,OrdP),
   ord_subtract(OrdP,DelRules,NewP).


/* perfect_model_operator_normal(Program,Perfect_Model) <-
      The perfect model Perfect_Model of the normal logic program 
      Program is computed. */

perfect_model_operator_normal(Program,Perfect_Model) :-
   ground_transformation_rr(Program,Program1),
   tp_fixpoint_normal(Program1,[],_,Perfect_Model),
   !.

tp_fixpoint_normal(Program,I0,I1,Model) :-
   gl_transformation(Program,I0,Program1),
   minimal_model_operator_definite_1(Program1,I1),
   gl_transformation(Program,I1,Program2),
   minimal_model_operator_definite_1(Program2,I2),
   ( ( ord_seteq(I1,I2), Model = I1 )
   ; ( simplify_program_2(Program,I2,I1,Program3),
       tp_fixpoint_normal(Program3,I2,_,Model) ) ).


/* minimal_model_operator_definite_1(+Program,-Model) <-
      The minimal Herbrand model Model of a definite logic program
      Program is computed. */

minimal_model_operator_definite_1(Program,Model) :-
   setof( A, member([A],Program), Facts ),
   setof2( As-Bs, member(As-Bs,Program), Rules ),
   tp_fixpoint(Rules,Facts,Model),
   !.
minimal_model_operator_definite_1(_,[]).


/* tp_fixpoint(Rules,Facts,Model) <-
      The minimal Herbrand model Model of a definite logic program
      consisting of the set Rules of rules and the set Facts of
      facts is computed. */

tp_fixpoint(Rules,Facts,Model) :-
   tp_single_iteration(Rules,Facts,DerivedFacts,FiredRules),
   !,
   ord_union(Facts,DerivedFacts,_,New),
   ord_union(Facts,New,NewFacts),
   ord_subtract(Rules,FiredRules,NewRules),
   tp_fixpoint(NewRules,NewFacts,Model).
tp_fixpoint(_,Model,Model).

tp_single_iteration(Rules,Facts,DerivedFacts,FiredRules) :-
   setof( A-[A]-Bs, 
      ( member([A]-Bs,Rules), ord_subset(Bs,Facts) ),
      AtomsRules ),
   setof( R, member(_-R,AtomsRules), FiredRules ),
   setof( A, member(A-_,AtomsRules), DerivedFacts ).


/* minimal_model_operator_definite_2(+Program,-Model) <-
      The minimal Herbrand model Model of a definite logic program
      Program is computed. */

minimal_model_operator_definite_2(Program,Model) :-
%  writeln(user,Program), wait,
   minimal_model_state(Program,State),
%  findall(Atom,member([Atom],State),Model).
   append(State,Model).


/* semi_stratified_evaluation(Program,Stable_Models) <-
      Given a normal logic program Program, a semi-stratification 
      Semi_Strata is computed, and subsequently the set Stable_Models 
      of all stable models of Program is computed w.r.t. Semi_Strata. */

semi_stratified_evaluation(Program,Stable_Models) :-
   semi_stratify_normal_program(Program,Semi_Strata),
   semi_stratified_evaluation(Semi_Strata,[[]],Stable_Models).

semi_stratified_evaluation([G|Gs],Is,M) :-
   ( G = ['stratified'];
     ( ( G = [TagValue|Rules],
         TagValue == stratified,
         fixpoint_of_stratum_iteration(Is,Rules,[],Is1) )
     ; ( G = [_|Rules],
         branch_and_bound_iteration(Is,Rules,[],Is1) ) ) ),
   semi_stratified_evaluation(Gs,Is1,M).
semi_stratified_evaluation([],M,M).

branch_and_bound_iteration([I|Is],Program,Acc,NewIs) :-
   stabalg_branch(Program,I,[],Ints),
   append(Acc,Ints,NewAcc),
   branch_and_bound_iteration(Is,Program,NewAcc,NewIs).
branch_and_bound_iteration([],_,Acc,Acc).

fixpoint_of_stratum_iteration([I|Is],P,Acc,NewIs) :-
   ( setof( [H]-[], member(H,I), FactRules ); FactRules = [] ),
   ord_union(P,FactRules,NewP),
%  minimal_model_operator_definite_2(NewP,M),
   perfect_models_operator(NewP,[M]),
%  fixpoint_of_stratum(P,I,M),
   ( ( M \== [], NewAcc = [M|Acc] )
   ; NewAcc = Acc ),
   fixpoint_of_stratum_iteration(Is,P,NewAcc,NewIs).
fixpoint_of_stratum_iteration([],_,Acc,Acc).

fixpoint_of_stratum(P,I0,M) :-
   setof2( As-Bs-Ds,
      ( member(As-Bs-Ds,P), Bs == [], Ds == [] ),
      StdFacts ),
   ord_subtract(P,StdFacts,Rules),
   setof2( Fact, member([Fact]-Bs-Ds,StdFacts), Facts1 ),
   ord_union(Facts1,I0,Facts),
   fixpoint_of_stratum_iterate(Rules,Facts,M).
fixpoint_of_stratum(_,_,[]).
 
fixpoint_of_stratum_iterate(Rules,Facts,M) :-
   tp_single_iteration_strat(Rules,Facts,DerivedFacts,FiredRules),
   !,
   ord_union(Facts,DerivedFacts,_,New),
   ord_union(Facts,New,NewFacts),
   ord_subtract(Rules,FiredRules,NewRules),
   fixpoint_of_stratum_iterate(NewRules,NewFacts,M).
fixpoint_of_stratum_iterate(_,M,M).
 
tp_single_iteration_strat(Rules,Facts,DerivedFacts,FiredRules) :-
   setof( A-[A]-Bs-Ds,
      ( member([A]-Bs-Ds,Rules),
        ord_subset(Bs,Facts), ord_disjoint(Ds,Facts) ),
      AtomsRules ),
   setof( R, member(_-R,AtomsRules), FiredRules ),
   setof( A, member(A-_,AtomsRules), DerivedFacts ).


/* e_stable_models_computation(Program,E,Models) <-
      For the normal logic program Program and the set E of ground atoms
      all stable models of Program that satisfy E are computed. */

e_stable_models_computation(Program,E,Models) :-
   e_resolution_derivation(Program,E,Models1),
   writeln(Models1),
   e_stable_models_computation_iteration(Models1,Program,[],Models).

e_stable_models_computation_iteration([I_plus-I_minus|Pairs],P,Acc,Models) :-
   simplify_program_1(P,I_plus,I_minus,P_sim),
%  stable_models_operator(P_sim,Is),
   stabalg_branch(P_sim,I_plus,I_minus,Is),
   lists_to_ord_sets(Is,OrdIs),
   append(Acc,OrdIs,NewAcc),
   e_stable_models_computation_iteration(Pairs,P,NewAcc,Models).
e_stable_models_computation_iteration([],_,Acc,Models) :-
   setof2( I, member(I,Acc), Models ).

e_resolution_derivation(Program,E,Models) :-
   e_resolution_derivation(E,Program,[],[]),
   setof2( I-J, retract(e_resolution_derivation_stable_pair(I-J)), Models ).

e_resolution_derivation(Es,P,I_plus,I_minus) :-
   member(E,Es), delete(Es,E,NewEs),
   setof2( [H]-Bs-Ds, 
      ( member([H]-Bs-Ds,P), H == E ),
      ResolveRules ),
   e_resolve_and_resolve(ResolveRules,NewEs,P,I_plus,I_minus).
e_resolution_derivation(_,_,I_plus,I_minus) :-
   assert(e_resolution_derivation_stable_pair(I_plus-I_minus)).

e_resolve_and_resolve([R|Rs],Es,P,I_plus,I_minus) :-
   R = H-Bs-Ds,
   ord_union(I_plus,H,NewI_plus), 
   list_to_ord_set(Ds,OrdDs),
   ord_union(I_minus,OrdDs,NewI_minus),
   ord_disjoint(NewI_plus,NewI_minus),
   list_to_ord_set(Bs,OrdBs),
   ord_union(Es,OrdBs,NewEs),
   delete(P,R,NewP1),
%  eliminate_rules(NewP1,NewI_plus,NewI_minus,NewP),
   simplify_program_1_a(NewP1,NewI_plus,NewI_minus,NewP),
   e_resolution_derivation(NewEs,NewP,NewI_plus,NewI_minus),
   e_resolve_and_resolve(Rs,Es,P,I_plus,I_minus).
e_resolve_and_resolve([_|Rs],Es,P,I_plus,I_minus) :-
   e_resolve_and_resolve(Rs,Es,P,I_plus,I_minus).
e_resolve_and_resolve([],_,_,_,_).


/* simplify_program_1(Program1,I_plus,I_minus,Program2) <-
      The normal logic program Program1 is simplified w.r.t. the
      Herbrand interpretations I_plus and I_minus to the
      definite logic program Program2. */

simplify_program_1(Program1,I_plus,I_minus,Program2) :-
   simplify_program_1_a(Program1,I_plus,I_minus,Program),
   simplify_program_1_b(Program,I_plus,I_minus,Program2).

simplify_program_1_a(Program1,I_plus,I_minus,Program2) :-
   simplify_program_1_a(Program1,I_plus,I_minus,[],Program3),
   list_to_ord_set(Program3,Program2).

simplify_program_1_a([Rule|Rules],I_plus,I_minus,Acc,P_sim) :-
   parse_dislog_rule(Rule,_,PosBody,NegBody),
   ( ( ( ord_intersect(PosBody,I_minus);
         ord_intersect(NegBody,I_plus) ),
       NewAcc = Acc )
   ; NewAcc = [Rule|Acc] ),
%  ( ( B^( ( member(B,PosBody), member(B,I_minus) ) ),
%      NewAcc = Acc )
%  ; ( ( D^( ( member(D,NegBody), member(D,I_plus) ) ),
%        NewAcc = Acc )
%    ; NewAcc = [Rule|Acc] ) ),
   simplify_program_1_a(Rules,I_plus,I_minus,NewAcc,P_sim).
simplify_program_1_a([],_,_,P_sim,P_sim).

simplify_program_1_b(Program1,I_plus,I_minus,Program2) :-
   simplify_program_1_b(Program1,I_plus,I_minus,[],Program2).

simplify_program_1_b([Rule|Rules],I_plus,I_minus,Acc,P_sim) :-
   parse_dislog_rule(Rule,Head,PosBody,NegBody),
   ord_subtract(PosBody,I_plus,NewPosBody),
   ord_subtract(NegBody,I_minus,NewNegBody),
   NewAcc = [Head-NewPosBody-NewNegBody|Acc],
   simplify_program_1_b(Rules,I_plus,I_minus,NewAcc,P_sim).
simplify_program_1_b([],_,_,P_sim,P_sim).


/* simplify_program_2(Program1,I_Lower,I_Upper,Program2) <-
      The normal logic ground program Program1 is simplified w.r.t. 
      the Herbrand interpretations I_Lower and I_Upper to the
      definite logic program Program2 using the rules (S1)-(S5). */

simplify_program_2(Program1,I_Lower,I_Upper,Program2) :-
   simplify_program_2_a(Program1,I_Lower,I_Upper,Program),
   simplify_program_2_b(Program,I_Lower,I_Upper,Program2).
   
simplify_program_2_a(Program1,I_Lower,I_Upper,Program) :-
   simplify_program_2_a(Program1,I_Lower,I_Upper,[],Program).

simplify_program_2_a([Rule|Rules],I_Lower,I_Upper,Acc,P_sim) :-
   parse_dislog_rule(Rule,Head,PosBody,NegBody),
%  ( ( Head = [A], non_member(A,I_Upper),
%      NewAcc = Acc )
%  ; ( ( B^( ( member(B,PosBody), non_member(B,I_Upper) ) ),
%        NewAcc = Acc )
%    ; ( ( D^( ( member(D,NegBody), member(D,I_Lower) ) ),
%          NewAcc = Acc )
%      ; NewAcc = [Rule|Acc] ) ) ),
   ( ( Head = [A], non_member(A,I_Upper),
       NewAcc = Acc )
   ; ( ( ( ( member(B,PosBody), non_member(B,I_Upper) ) ),
         NewAcc = Acc )
     ; ( ( ( ( member(D,NegBody), member(D,I_Lower) ) ),
           NewAcc = Acc )
       ; NewAcc = [Rule|Acc] ) ) ),
   simplify_program_2_a(Rules,I_Lower,I_Upper,NewAcc,P_sim).
simplify_program_2_a([],_,_,P_sim,P_sim).

   
simplify_program_2_b(Program,I_Lower,I_Upper,Program2) :-
   simplify_program_2_b(Program,I_Lower,I_Upper,[],Program2).

simplify_program_2_b([Rule|Rules],I_Lower,I_Upper,Acc,P_sim) :-
   parse_dislog_rule(Rule,Head,PosBody,NegBody),
   ord_subtract(PosBody,I_Lower,NewPosBody),
   ord_intersection(NegBody,I_Upper,NewNegBody),
   NewAcc = [Head-NewPosBody-NewNegBody|Acc],
   simplify_program_2_b(Rules,I_Lower,I_Upper,NewAcc,P_sim).
simplify_program_2_b([],_,_,P_sim,P_sim).


/* modified_ground_folding_program(Program1,Program2) <-
      The normal logic program Program1 is folded 
      to the normal logic program Program2. */

modified_ground_folding_program(Program1,Program2) :-
   mgf_join_rules(Program1,Program1,L),
   modified_ground_folding_program(L,[],Program),
   findall( A-Bs-Ds,
      ( member(A-Bs-Ds,Program1), Ds == [] ),
      Rules ),
   append(Program,Rules,Program2).

modified_ground_folding_program([L|Ls],Acc,FP) :-
   ( ( L = [A-Bs-Ds],
       NewAcc = [A-Bs-Ds|Acc] )
   ; ( modified_ground_folding_step(L,Res),
       append(Acc,Res,NewAcc) ) ),
   modified_ground_folding_program(Ls,NewAcc,FP).
modified_ground_folding_program([],X,X).

modified_ground_folding_step(L,Res) :-
   member(R,L),
   R = _-_-Ds,
   Chi =.. [mgf_dummy|Ds],
   Rho = [Chi]-[]-Ds,
   modified_ground_folding_step(L,Chi,[],Res1),
   Res = [Rho|Res1].

modified_ground_folding_step([R|Rs],Chi,Acc,Res) :- 
   R = A-Bs-_,
   NewAcc = [A-[Chi|Bs]-[]|Acc],
   modified_ground_folding_step(Rs,Chi,NewAcc,Res).
modified_ground_folding_step([],_,X,X).

mgf_join_rules(Program1,Program2,Program3) :-
   mgf_join_rules(Program1,Program2,[],Program3).

mgf_join_rules([R|Rs],P,Acc,Res) :-
   mgf_join_rules_1(P,R,NewP,Res1),
   ( ( Res1 \== [], NewAcc = [Res1|Acc] ); NewAcc = Acc ),
   mgf_join_rules(Rs,NewP,NewAcc,Res).
mgf_join_rules([],_,X,X).

mgf_join_rules_1(P,R,NewP,Res) :-
   R = _-_-Ds,
   ( ( Ds \== [],
       setof( A-Bs-Ds1,
          ( member(A-Bs-Ds1,P), Ds1 == Ds ),
          Res) )
   ; Res = [] ),
   ord_subtract(P,[R|Res],NewP).


/* stable_models_filter(Program,Models,Stable_Models) <- 
      For a normal logic program Program and a coin Models,
      those interpretations in Models that are stable models of 
      Program are retrieved in the coin Stable_Models. */

stable_models_filter(Program,Models,Stable_Models) :-
   stable_models_filter(Program,Models,[],Stable_Models).

stable_models_filter(Program,[Model|Models],Acc,Stable_Models) :-
   ( ( stable_model_test(Program,Model), 
       NewAcc = [Model|Acc] )
   ; NewAcc = Acc ),
   stable_models_filter(Program,Models,NewAcc,Stable_Models).
stable_models_filter(_,[],Stable_Models,Stable_Models).

stable_model_test(Program,Model) :-
   gl_transformation(Program,Model,Program1),
   minimal_model_operator_definite_1(Program1,Int),
   list_to_ord_set(Model,Ord_Model),
   !,
   ord_seteq(Int,Ord_Model).


/******************************************************************/


