

/******************************************************************/
/***                                                            ***/
/***         NMR:  Explanations for Models                      ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(model_explanations, 1) :-
   Coin = [[a,c], [b,c]],
   Program = [[c]-[]-[a], [c]-[]-[b], [a,b]],
%  Coin = [[a,b]],
%  Program = [[a]-[b], [b]-[a], [a,b]],
   explain_coin(Coin, Program, Explanations),
   writeln_list(user, Explanations).
test(model_explanations, 2) :-
   Semantics = stable,
   Program = [[c]-[]-[a], [c]-[]-[b], [a,b]],
   explain_semantics(Semantics, hi, Program).


/*** interface ****************************************************/


/* Minimal Models:
      The semantics of a disjunctive logic program without 
      negation is given by its set of minimal models.
      Minimal models are interpretations of the program.
   Stable Models:
      The semantics of a disjunctive logic program with
      negation can be defined by its set of stable models.
      Stable models are interpretations of the program.
   Coin:
      A set of interpretations is called a Coin, 
      short for collection of interpretations. */


explain_minimal_models(Database_Id) :-
   dread(lp, Database_Id, Program),
   explain_semantics(minimal, hi, Program).

explain_stable_models(Database_Id) :-
   dread(lp, Database_Id, Program),
   explain_semantics(stable, hi, Program).


/* explain_semantics(Semantics, hi, Program, Explanations) <-
      */

explain_semantics(Semantics, hi, Program) :-
   explain_semantics(Semantics, hi, Program, Explanations),
   show_explanations(Explanations),
   show_explanations_xml(Explanations).

explain_semantics(Semantics, hi, Program, Explanations) :-
   semantics(Semantics, hi, Program, Coin),
   length(Coin, N),
   nl,
   write_list(['There are ',N,' ',Semantics,' models given by:']),
   dportray(chs, Coin), 
   nl,
   writeln('The explanations are given by:'),
   explain_coin(Coin, Program, Explanations).


/* show_explanations_xml(Explanations) <-
      */

show_explanations_xml(Explanations) :-
   explanations_to_xml(Explanations, Xml),
   star_line,
   html_display_explanations_xml(Xml).


/* html_display_explanations_xml(Xml) <-
      */

html_display_explanations_xml(Xml) :-
   dwrite(xml, Xml),
   concat('sources/reasoning/model_based_semantics/',
      'model_explanations_fng.pl', File_Fng),
   fn_transform_fn_item_fng(File_Fng, Xml, Html),
%  dwrite(xml, Html),
%  html_to_display(Html).
   html_explanations_www_open_url(Html).


/* html_explanations_www_open_url(Html) <-
      */

html_explanations_www_open_url(Html) :-
   dislog_variable_get(output_path, Results),
   concat(Results, 'd3_explanations.html', Path),
   nl, write_list(user, ['---> ', Path]), nl,
   dwrite(xml, Path, html:[]:[Html]),
   www_open_url(Path).


/* show_explanations(Explanations) <-
      */

show_explanations(Explanations) :-
   checklist( show_explanation,
      Explanations ).

show_explanation(Explanation) :-
   star_line,
   show_supports(Explanation).

show_supports(Supports) :-
   checklist( writeln,
      Supports ).


/* explain_coin(Coin, Program, Supports) <-
      */

explain_coin(Coin, Program, Supports) :-
   coin_to_tree(Coin, Tree),
   hidden( tree_tpi_join_2(Program, Tree, Program_2) ),
   maplist( construct_exclusive_supports(Program_2),
      Coin, Supports ).


explain_coin_2(Program,[I|Is],[I-P|Is_Ps]) :-
   explain_interpretation(Program,I,P),
   explain_coin_2(Program,Is,Is_Ps).
explain_coin_2(_,[],[]).

explain_interpretation(Program_1,Interpretation,Program_3) :-
   tree_tpi_join_2(Program_1,[Interpretation],Program_2),
   select_applicable_rules(Program_2,Interpretation,Program_3).


coin_to_tree(Coin,Tree) :-
   state_to_tree(Coin,Tree).

ord_element(Element,Set) :-
   ord_subset([Element],Set).


/*** implementation ***********************************************/


/* select_applicable_rules(Rules1, I, Rules2) <-
      */

select_applicable_rules([Rule|Rules1],I,[Rule|Rules2]) :-
   parse_dislog_rule_save(Rule,_,Pos_Body,Neg_Body),
   ord_subset(Pos_Body,I),
   ord_disjoint(Neg_Body,I),
   !,
   select_applicable_rules(Rules1,I,Rules2).
select_applicable_rules([_|Rules1],I,Rules2) :-
   select_applicable_rules(Rules1,I,Rules2).
select_applicable_rules([],_,[]).


/* construct_exclusive_supports(Program,Interpretation,Proof) <-
      */

construct_exclusive_supports(Program,Interpretation,Proof) :-
   construct_exclusive_supports(Interpretation,[],Interpretation,
      Program,[],Reverse_Proof),
   reverse(Reverse_Proof,Proof).

construct_exclusive_supports(_,_,[],_,Proof,Proof) :-
   !.
construct_exclusive_supports(I_Global,I_Sofar,I_Rest,
      Program,Proof_Sofar,Proof) :-
   member(Rule,Program),
   parse_dislog_rule(Rule,Head,Pos_Body,Neg_Body),
   ord_subset(Pos_Body,I_Sofar),
   ord_disjoint(Neg_Body,I_Global),
   member(Atom,I_Rest),
   clause_is_exclusive_support(Head,Atom,I_Global),
   Program_2 = Program,
   ord_add_element(I_Sofar,Atom,I_Sofar_2),
   ord_del_element(I_Rest,Atom,I_Rest_2),
   simplify_rule(Rule,Rule_2),
   New_Proof_Sofar = [Atom-is_supported_by-Rule_2|Proof_Sofar],
   construct_exclusive_supports(I_Global,I_Sofar_2,I_Rest_2,
      Program_2,New_Proof_Sofar,Proof).

clause_is_exclusive_support(Clause,Atom,Interpretation) :-
   member(Atom,Clause),
   ord_element(Atom,Interpretation),
   ord_del_element(Clause,Atom,Clause_2),
   ord_del_element(Interpretation,Atom,Interpretation_2),
   ord_disjoint(Clause_2,Interpretation_2).


/* explanations_to_xml(Explanations, Xml) <-
      */

explanations_to_xml(Explanations, Xml) :-
   maplist( explanation_to_xml,
      Explanations, Xmls ),
   Xml = models:Xmls.

explanation_to_xml(Explanation, Xml) :-
   explanation_to_leading_atoms(Explanation, Atoms),
   maplist( explanation_for_atom(Explanation),
      Atoms, Xmls ),
   Xml = model:Xmls.

explanation_to_leading_atoms(Explanation, Atoms) :-
   findall( Atom,
      ( member(E, Explanation),
        E = Atom-is_supported_by-_,
        \+ ( member(F, Explanation),
             F = _-is_supported_by-Rule,
             parse_dislog_rule(Rule, _, As, _),
             member(Atom, As) ) ),
      Atoms ).

explanation_for_atom(Explanation, Atom, Xml) :-
   findall( X,
      ( member(E, Explanation),
        E = Atom-is_supported_by-Rule,
        parse_dislog_rule(Rule, _, Atoms, _),
        maplist( explanation_for_atom(Explanation),
           Atoms, Xs ),
        X = rule:[name:Rule]:Xs ),
      Xmls ),
   Xml = atom:[name:Atom]:Xmls.


/* used_rules_to_explanations_xml(Rules, Xml) <-
      */

used_rules_to_explanations_xml(Rules, Xml) :-
   used_rules_to_leading_atoms(Rules, Atoms),
   maplist( used_rules_to_explanation_for_atom(Rules),
      Atoms, Xmls ),
   Xml = models:[model:Xmls].

used_rules_to_leading_atoms(Rules, Atoms) :-
   findall( Atom,
      ( member(R, Rules),
        parse_dislog_rule(R, [Atom], _, _),
        \+ ( member(S, Rules),
             parse_dislog_rule(S, _, As, _),
             member(Atom, As) ) ),
      Atoms ).

used_rules_to_explanation_for_atom(Rules, Atom, Xml) :-
   findall( X,
      ( member(Rule, Rules),
        parse_dislog_rule(Rule, [Atom], Atoms, _),
        maplist( used_rules_to_explanation_for_atom(Rules),
           Atoms, Xs ),
        X = rule:[name:Rule]:Xs ),
      Xmls ),
   Xml = atom:[name:Atom]:Xmls.


/******************************************************************/


