

/******************************************************************/
/***                                                            ***/
/***         NMR:  Partial Stable Models - for DNLP's           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* partial_stable_models(Program,Partial_Stable_Models) <-
      Given a DNLP Program, the set Partial_Stable_Models
      of all partial stable models of Program is computed. */

partial_stable_models(Database_Id) :-
   hidden( (
      dconsult(Database_Id,Program),
      partial_stable_models(Program,Partial_Stable_Models) ) ),
   bar_line_2, write('Partial Stable Models ='),
   dportray(chs,Partial_Stable_Models).

partial_stable_models(Program,Partial_Stable_Models) :-
   partial_stable_models_pe_nml_1(Program,Partial_Stable_Models).


partial_stable_models_pe_nml_1_state(Database_Id) :-
   hidden( (
      dconsult(Database_Id,Program),
      partial_stable_models_pe_nml_1(Program,Partial_Stable_Models),
      state_operator_disjunctive(Partial_Stable_Models,State1),
      state_remove_tautologies(State1,State) ) ),
   dportray(ghs,State).

partial_stable_models_pe_nml_1(Program,Partial_Stable_Models) :-
   tu_transformation(Program,Program2),
   stable_models_pe_nml_1(Program2,Stable_Models),
%  stable_models_pe_nml_clark(Program2,Stable_Models),
%  stable_models_operator(Program2,Stable_Models),
   dnlp_to_herbrand_base(Program,HB),
   coin_tu_annotated_to_partial(HB,
      Stable_Models,Partial_Stable_Models).
%  Partial_Stable_Models = Stable_Models.


/******************************************************************/


