

/******************************************************************/
/***                                                            ***/
/***         NMR:  Explanations for Models                      ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(nmr:explain_semantics_xpce, minimal(ancestor)) :-
   concat('deductive/transitive_closure/ancestor/',
      'ancestor_windsors_1.pl', Path),
   dislog_variable_get(example_path, Path, File),
   dread(lp, File, Program),
   explain_semantics_xpce(minimal, hi, Program).
test(nmr:explain_semantics_xpce, stable(Database)) :-
   member(Database, [
%     'nmr/classical_examples/fly_not_ab',
      'nmr/classical_examples/tweety',
      'nmr/minker/minker_2' ]),
   dislog_variable_get(example_path, Database, File),
   dread(lp, File, Program),
   explain_semantics_xpce(stable, hi, Program).
test(nmr:explain_semantics_xpce, 3) :-
   Explanations = [
      a-is_supported_by-([a]-[b]-[c]),
      b-is_supported_by-([b]) ],
   show_explanations_xpce([Explanations]).


/*** interface ****************************************************/


/* explain_semantics_xpce(Semantics, hi, Program) <-
      */

explain_semantics_xpce(Semantics, hi, Program) :-
   explain_semantics(Semantics, hi, Program, Explanations),
   star_line,
   writelnq_list(Explanations),
   star_line,
   !,
   show_explanations_xpce(Explanations),
   !.


/* show_explanations_xpce(Explanations) <-
      */

show_explanations_xpce(Explanations) :-
%  star_line,
%  writeln_list(user, Explanations),
   checklist( show_explanation_xpce,
      Explanations ),
   !.

show_explanation_xpce(Explanation) :-
   findall( V,
      ( member(A-is_supported_by-Rule, Explanation),
        parse_dislog_rule(Rule, _, [], []),
        term_to_atom(A, V) ),
      Vertices_1 ),
   model_explanation_to_edges(Explanation, Edges),
%  writeln(user, Edges),
   edges_to_vertices(Edges, Vertices_2),
   append(Vertices_1, Vertices_2, Vertices_3),
   sort(Vertices_3, Vertices),
%  writeq(vertices=Vertices), nl,
%  writeq(edges=Edges), nl,
   predicate_to_file( show_explanation_xpce,
      ( star_line, writeln('nodes'), star_line,
        maplist( writelnq, Vertices ),
        star_line, writeln('edges'), star_line,
        maplist( writelnq, Edges ) ) ),
   vertices_clingo_prune(Vertices, Vertices_4),
   edges_clingo_prune(Edges, Edges_4),
   ( Vertices_4 = [], Edges_4 = []
   ; vertices_edges_to_picture_bfs(Vertices_4, Edges_4) ).
%  ; vertices_edges_to_picture(dfs, Vertices_4, Edges_4, _) ).
%  ; vertices_and_edges_to_picture(xpce, Vertices_4, Edges_4) ).


/* model_explanation_to_edges(Explanation, Edges) <-
      */

model_explanation_to_edges(Explanation, Edges) :-
   findall( A-R,
      ( member(V-is_supported_by-Rule, Explanation),
        parse_dislog_rule(Rule, _, P, N),
        ( P \= []
        ; N \= [] ),
        term_to_atom(V, A),
        rule_to_atom(Rule, R) ),
      Edges_1 ),
   findall( R-W,
      ( member(_-is_supported_by-Rule, Explanation),
        parse_dislog_rule(Rule, _, Bs, _),
        rule_to_atom(Rule, R),
        member(B, Bs),
        term_to_atom(B, W) ),
      Edges_2 ),
   findall( Edge_3,
      ( member(_-is_supported_by-Rule, Explanation),
        parse_dislog_rule(Rule, _, _, Cs),
        rule_to_atom(Rule, R),
        Count = (_=(#(count('{}'(_:C))))),
        member(Count, Cs),
        term_to_atom(Count, W),
        E = R-W,
        explanation_edges_for_count(Explanation, W, C, Es),
        member(Edge_3, [E|Es]) ),
      Edges_3 ),
   findall( R-W,
      ( member(_-is_supported_by-Rule, Explanation),
        parse_dislog_rule(Rule, _, _, Cs),
        rule_to_atom(Rule, R),
        member(not(C), Cs),
        term_to_atom('not_'(C), W) ),
      Edges_4 ),
   append([Edges_1, Edges_2, Edges_3, Edges_4], Edges_5),
   sort(Edges_5, Edges).

explanation_edges_for_count(Explanation, W, C, Es) :-
   findall( F,
      ( member(C-is_supported_by-_, Explanation),
        term_to_atom(C, V),
        F = W-V ),
      Es ).


/* edges_clingo_prune(Es_1, Es_2) <-
      */

edges_clingo_prune(Es_1, Es_2) :-
   maplist( edge_clingo_prune, Es_1, Es_2 ).

edge_clingo_prune(V1-V2, W1-W2) :-
   vertex_clingo_prune(V1, W1),
   vertex_clingo_prune(V2, W2).


/* vertices_clingo_prune(Vs_1, Vs_2) <-
      */

vertices_clingo_prune(Vs_1, Vs_2) :-
   maplist( vertex_clingo_prune, Vs_1, Vs_2 ).

vertex_clingo_prune(V1, V2) :-
   Substitution = [ ["{","({"], ["}","})"] ],
   name_exchange_sublist(Substitution, V1, V2).


/******************************************************************/


