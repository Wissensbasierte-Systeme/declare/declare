

/******************************************************************/
/***                                                            ***/
/***       NMR:  Perfect Model for Prolog                       ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(perfect_model_prolog, ddbase_aggregate) :-
   Datalog = [
      (a(S=Y) :- ddbase_aggregate(
             [sum(X),list(X)], b(X), [[S,L]]),
          list_to_functor_structure(+,L,Y)),
      b(2), b(3)],
   perfect_model_prolog(Datalog, [], Model),
   dportray(hi, Model).

test(perfect_model_prolog, d3_and_ddbase) :-
   dislog_variable_get(example_path,
      'd3/Diverse/test_kb.xml', File_KB),
   dread(xml, File_KB, [KB]),
   assert(d3_knowledge_base(KB)),
   dislog_variable_get(example_path,
      'd3/Diverse/test_rules.pl', File_PL),
   prolog_consult(File_PL, Program),
   perfect_model_prolog(Program, [], M),
   dabolish(d3_finding/2),
   retract(d3_knowledge_base(KB)),
   star_line,
   dwrite(pl, user, M).

list_to_sum_for_test(Xs, X) :-
   list_to_functor_structure(+, Xs, X).


/*** interface ****************************************************/


/* perfect_model_prolog_files(
         Datalog_Files, Prolog_Files, Model) <-
      */

perfect_model_prolog_files(Datalog_Files, Prolog_Files, Model) :-
   maplist( prolog_consult,
      Datalog_Files, Datalogs ),
   append(Datalogs, Datalog),
   maplist( prolog_consult,
      Prolog_Files, Prologs ),
   append(Prologs, Prolog),
   perfect_model_prolog(Datalog, Prolog, Model).


/* perfect_model_prolog(Datalog, Prolog, Model) <-
      */

perfect_model_prolog(Datalog, Prolog, Model) :-
   gensym(perfect_models_prolog_, Module),
   writeln(user, 'Module'=Module),
   prolog_program_dynamize_predicate_symbols(Module, Datalog),
   perfect_model_prolog(Datalog, Prolog, Module, Model).

perfect_model_prolog(Datalog, Prolog, Module, Model) :-
   assert_rules(Module, Prolog),
   prolog_program_ddbase_connect(Datalog, Module),
   prolog_program_ddbase_connect_rules(Datalog, Rules_c),
   prolog_program_stratify_rules(Datalog, Rules_s),
   prolog_program_without_ddbase_connect(Datalog, Datalog_2),
   prolog_program_without_stratify(Datalog_2, Datalog_3),
   append([Datalog_3, Rules_c, Rules_s], Datalog_4),
   prolog_rules_to_partition(Datalog_4, Partition),
   predicate_to_file(user, star_line),
   writeln(user, 'Partition'),
   predicate_to_file(user, star_line),
   writeln_list(user, Partition),
   append([Datalog_3, Rules_c], Datalog_5),
   prolog_program_stratify(Datalog_5, Prolog, Partition, Datalogs),
   dislog_variable_get(output_path,
      'stratified_datalog_program.tmp', File),
   predicate_to_file( File,
      checklist( dportray(prolog),
         Datalogs ) ),
%  predicate_to_file(user, star_line),
%  writeln(user, 'Strata'),
%  predicate_to_file(user, star_line),
%  predicate_to_file(user,
%     maplist( dwrite(pl, user), Datalogs )),
   tp_iteration_prolog_muliple(Datalogs, Module, [], Model).


/* prolog_program_ddbase_connect(Prolog, Module) <-
      */

prolog_program_ddbase_connect(Prolog, Module) :-
   findall( Goal,
      ( member(Rule, Prolog),
        ( Rule = (:- ddbase_connect(
             odbc(mysql), Database:Table))
        ; Rule = (:- ddbase_connect_(
             odbc(mysql), Database:Table)) ),
        Goal = ddbase_connect(
           odbc(mysql), Module, Database:Table) ),
      Directives ),
   maplist( call, Directives ).

prolog_program_ddbase_connect_rules(Prolog, Rules) :-
   findall( Rule_2,
      ( member(Rule, Prolog),
        Source = odbc(mysql),
        Rule = (:- ddbase_connect_(Source, Database:Table)),
        once(ddbase_connect_arity(Source, Database:Table, N)),
        functor(Atom, Table, N), 
        Rule_2 = (Atom :- Atom) ),
      Rules ).


/* prolog_program_stratify_rules(Prolog, Rules) <-
      */

prolog_program_stratify_rules(Prolog, Rules) :-
   findall( Rule_2,
      ( member(Rule, Prolog),
        Rule = (:- stratify Comparison),
        comma_structure_to_list(Comparison, Precedences),
        member(Precedence, Precedences),
        Precedence =.. [Op, P1/N1, P2/N2],
        functor(A1, P1, N1),
        functor(A2, P2, N2),
        ( Op = <, Rule_2 = (A2 :- not(A1))
        ; Op = =<, Rule_2 = (A2 :- A1) ) ),
      Rules ).


/* dislog_program_select_non_directives(Datalog_1, Datalog_2) <-
      */

dislog_program_select_non_directives(Datalog_1, Datalog_2) :-
   findall( Rule,
      ( member(Rule, Datalog_1),
        Rule \= []-_ ),
      Datalog_2 ).
%  dwrite(lp, user, Datalog_2).


/* prolog_program_select_non_directives(Prolog_1, Prolog_2) <-
      */

prolog_program_select_non_directives(Prolog_1, Prolog_2) :-
   findall( Rule,
      ( member(Rule, Prolog_1),
        Rule \= (:- _) ),
      Prolog_2 ).
%  dwrite(pl, user, Prolog_2).


/******************************************************************/


