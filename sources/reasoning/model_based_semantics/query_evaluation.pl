

/******************************************************************/
/***                                                            ***/
/***         NMR:  Disjunctive Query Evaluation                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* disjunctive_query(Program, Query) <-
      */

disjunctive_query(Program,Query) :-
   dualize_disjunctive_rules(Program,Program1),
   list_of_elements_to_relation(Query, Rules),
   ord_union(Rules,Program1,Program2),
   dportray(lp,Program2),
%  tps_fixpoint_iteration(Program2,State),
   tps_fixpoint(Program2, [], State),
%  tps_fixpoint_cc(Program2,State),
%  tree_tpi_fixpoint(Program2, [[]],Models),
   !,
   nl, writeln(user,State), nl,
%  nl, writeln(user,Models), nl,
   State = [[]].
%  Models = [].


/*** implementation ***********************************************/


/* dualize_disjunctive_rules(Rs1, Rs5) <-
      */

dualize_disjunctive_rules(Rs1, Rs5) :-
   dualize_disjunctive_rules(Rs1, [], Rs2),
   list_to_ord_set(Rs2, Rs3),
   dnlp_to_constants(Rs1, Cs),
   create_constant_facts(Cs, Rs4),
   ord_union(Rs3, Rs4, Rs5). 


/* create_constant_facts(+Cs, -Rs) <-
      */

create_constant_facts([C|Cs], [R|Rs]) :-
   R = [constant(C)],
   create_constant_facts(Cs, Rs).
create_constant_facts([], []).

dualize_disjunctive_rules([R|Rs], Sofar, Rules) :-
   dualize_disjunctive_rule(R, R1),
   dualize_disjunctive_rules(Rs, [R1|Sofar], Rules).
dualize_disjunctive_rules([], Rules, Rules).

dualize_disjunctive_rule(R1, R2) :-
   parse_dislog_rule(R1, H1, B, _), 
   term_to_variables(H1, V1),
   term_to_variables(B, V2),
   ord_subtract(V2, V1, V3),
   create_constant_atoms(V3, As),
   ord_union(H1, As, H2),
   R2 = B-H2.
   
create_constant_atoms([V|Vs], [A|As]) :-
   A = constant(V),
   create_constant_atoms(Vs,As).
create_constant_atoms([], []).

dualize_disjunctive_program([H-B|Rs1], [B-H|Rs2]) :-
   !,
   dualize_disjunctive_program(Rs1, Rs2).
dualize_disjunctive_program([H|Rs1], [[]-H|Rs2]) :-
   dualize_disjunctive_program(Rs1, Rs2).
dualize_disjunctive_program([], []).

dnlp_to_constants(Program, Constants) :-
   dnlp_to_dn_clauses(Program),
   get_program_atoms(Atoms),
   get_constants_from_clause(Atoms, Constants),
   retractall(dn_clause(_, _, _)).

term_to_variables(X, [X]) :-
   var(X).
term_to_variables(X, []) :-
   nonvar(X), constant(X).
term_to_variables(X, Vs) :-
   nonvar(X), 
   compound(X),
   functor(X, _, N), 
   term_to_variables(N, X, Vs). 

term_to_variables(N, X, Vs) :-
   term_to_variables(N, X, [], Vs1),
   ord_union(Vs1, Vs).

term_to_variables(N, X, Vs1, Vs3) :-
   N > 0, arg(N, X, Arg),
   term_to_variables(Arg, Vs2),
   M is N - 1,
   term_to_variables(M, X, [Vs2|Vs1], Vs3).
term_to_variables(0, _, Vs, Vs).

not_occurs_in(X, Y) :-
   var(Y), X \== Y.
not_occurs_in(_, Y) :-
   nonvar(Y), constant(Y).
not_occurs_in(X, Y) :-
   nonvar(Y), 
   compound(Y),
   functor(Y, _, N),
   not_occurs_in(N, X, Y).

not_occurs_in(N, X, Y) :-
   N > 0, arg(N, Y, Arg),
   not_occurs_in(X, Arg),
   M is N - 1,
   not_occurs_in(M, X, Y).
not_occurs_in(0,_,_).

constant(X) :-
   atomic(X).


/******************************************************************/


test(disjunctive_query, tps_fixpoint) :-
%  Query = [p(c,f),p(d,g)],
   Query = [p(a,1),p(a,d),p(a,e)],
%  Query = [p(a,a),p(a,e)],
%  Query = [p(a,e),p(a,g),p(c,f),p(h,h)],
   dconsult('path/path_nm', Program),
   measure(
      disjunctive_query(Program, Query), T1 ),
   measure(
      ( tps_fixpoint(Program, [], State),
        dportray(dhs,State), !,
        member(Query,State) ), T2 ),
   pp_times([T1, T2]).


/******************************************************************/


