 

/******************************************************************/
/***                                                            ***/
/***         NMR:   Evidential Stable Models                    ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


/* evidential_stable_models(Program,ES_Models) <-
      Given a DNLP Program, the set ES_Models of all
      evidential stable models is computed. */

evidential_stable_models(Database_Id) :-
   evidential_stable_models_id(2,Database_Id).

evidential_stable_models(Program,ES_Models) :-
   evidential_stable_models(2,Program,ES_Models).


/* partial_evidential_stable_models(Program,PES_Models) <-
      Given a DNLP Program, the set PES_Models of all 
      partial evidential stable models is computed. */

partial_evidential_stable_models(Database_Id) :-
   evidential_stable_models_id(3,Database_Id).

partial_evidential_stable_models(Program,PES_Models) :-
   evidential_stable_models(3,Program,PES_Models).


evidential_stable_models_id(V_Mode,Database_Id) :-
   measure(
      ( dconsult(Database_Id,Program),
        dportray(lp,Program),
        star_line,
        evidential_stable_models(V_Mode,Program,ES_Models),
        star_line,
        dportray(chs,ES_Models),
        nl ) ).


/* evidential_stable_models(V_Mode,MM_Mode,Program,ES_Models) <-
      Given a DNLP Program, the set ES_Models of all
      V_Mode-valued evidential stable models is computed.
      MM_Mode determines whether or not minimal model constraints
      are used. */

esm_mm_on :-
   dislog_flag_set(evidential_stable_mode, a).
esm_mm_off :-
   dislog_flag_set(evidential_stable_mode, b).

evidential_stable_models(V_Mode, Program, ES_Models) :-
   dislog_flag_get(evidential_stable_mode, MM_Mode),
   evidential_stable_models(V_Mode, MM_Mode, Program, ES_Models).

evidential_stable_models(V_Mode,MM_Mode,Program,ES_Models) :-
   transform_program_for_evidential_stable_models(V_Mode,MM_Mode,
      Program,EA_Program),
   minimal_models_operator(EA_Program,Coin_1),
   dportray(chs,Coin_1),
   evidential_tu_constraints(E_Constraints_tu),
   determine_e_tu_violations(E_Constraints_tu,Coin_1,Is_and_Vs_1),
   coin_can_wrt_violations(Is_and_Vs_1,Is_and_Vs_2),
   coin_etu_vs_to_partial(Is_and_Vs_2,Coin_2),
   list_to_ord_set(Coin_2,ES_Models).


/*** implementation ***********************************************/


transform_program_for_evidential_stable_models(
      V_Mode,MM_Mode,Program,EA_Program) :-
   evidential_truth_annotate_transformation(
      V_Mode,Program,EA_Program_1),
   program_to_evidential_minimal_model_constraints(
      V_Mode,MM_Mode,Program,EA_Program_2),
   append(EA_Program_1,EA_Program_2,EA_Program).


program_to_evidential_minimal_model_constraints(
      V_Mode,a,Program,MM_Constraints) :-
   truth_annotate_transformation(V_Mode,Program,A_Program),
   program_to_evidential_minimal_model_constraints(
      A_Program,MM_Constraints),
   dportray(lp,MM_Constraints).
program_to_evidential_minimal_model_constraints(_,b,_,[]).


program_to_evidential_minimal_model_constraints(
      Program,MM_Constraints) :-
   disjunctive_transformation(Program,Program_dlp),
   gcwa_operator(Program_dlp,Gcwa),  
   egcwa_operator(Program_dlp,Egcwa), 
   ord_union(Gcwa,Egcwa,Conjunctive_State), 
%  cc_semantics(minimal,nhb,Program_dlp,N_State),
%  negate_state(N_State,Conjunctive_State),
   maplist( atoms_to_evidential_ic,
      Conjunctive_State, MM_Constraints ).
  
atoms_to_evidential_ic(Atoms,[]-E_Atoms) :-
   clause_to_evidential_clause(Atoms,E_Atoms).
 
   
old_coin_etu_vs_to_partial(Is_and_Vs_2,Models_2) :-
   findall( K,
      ( member(I-Vs,Is_and_Vs_2),
        split_evidential_clause(I,J-_),
        append(J,[violations(Vs)],K) ),
      Models_2 ).

coin_etu_vs_to_partial(Coin1,Coin2) :-
   maplist( interpretation_etu_vs_to_partial,
      Coin1, Coin2 ).

interpretation_etu_vs_to_partial(I1-Vs1,I3) :-
   findall( t(A), member(t(A),I1), I1_t ),
   findall( u(A),
      ( member(u(A),I1),
        \+ member(t(A),I1) ),
      I1_u ),
   ord_union(I1_t,I1_u,I2),
   findall( 'E'(t(A)), member('E'(t(A)),Vs1), Vs1_t ),
   findall( 'E'(u(A)), 
      ( member('E'(u(A)),Vs1),
        \+ member('E'(t(A)),Vs1) ),
      Vs1_u ),
   ord_union(Vs1_t,Vs1_u,Vs2),
   append(I2,Vs2,I3),
   !.

determine_e_tu_violations(Constraints,Coin1,Coin2) :-
   maplist( determine_e_tu_violations_for_int(Constraints),
      Coin1, Coin2 ).

determine_e_tu_violations_for_int(Constraints,I,I-Violations) :-
   findall( X,
      ( member([]-[X]-[Y],Constraints),
        member(X,I),
        \+ ord_element(Y,I) ),
      Violations_2 ),
   list_to_ord_set(Violations_2,Violations).

coin_can_wrt_violations(Is_and_Vs_1,Is_and_Vs_2) :-
   coin_can_wrt_ordering(e_tu_violation_ordering,
      Is_and_Vs_1,Is_and_Vs_2).

e_tu_violation_ordering(_-Vs1,_-Vs2) :-
   Vs1 \== Vs2,
   ord_subset(Vs1,Vs2).


evidential_tu_rules([ ['E'(u(A))]-[u(A)], ['E'(t(A))]-[t(A)],
   ['E'(u(A))]-['E'(t(A))]]).
evidential_tu_constraints([
   []-['E'(t(A))]-[t(A)], []-['E'(u(A))]-[u(A)] ]).


new_semantics_stable(Program,Minimal_Models) :-
   dportray(lp,Program),
   transform_program_t(Program,Program2),
   dportray(lp,Program2),
   stable_models_pe_nml_1(Program2,Minimal_Models),
   dportray(chs,Minimal_Models).

new_semantics(Program,Minimal_Models) :-
   dportray(lp,Program),
   transform_program_t_dis(Program,Program2),
   dportray(lp,Program2),
   minimal_models(Program2,Models),
   dportray(chs,Models),
   coin_can_wrt_ordering(tu_knowledge_smaller,
      Models,Minimal_Models),
   dnlp_to_herbrand_base(Program,Herbrand_Base),
   coin_tu_annotated_to_partial(Herbrand_Base,
      Minimal_Models,Partial_Minimal_Models),
   dportray(chs,Partial_Minimal_Models).


interpretation_smaller_than(Int_1,Int_2) :-
   \+ tu_violation(Int_1,Int_2).

tu_violation(Int_1,Int_2) :-
   tu_violation(_,_,Int_1,Int_2).
 
tu_violation(t,Atom,Int_1,Int_2) :-
   tu_truth_value(Int_1,Atom,t),
   tu_truth_value(Int_2,Atom,f).
tu_violation(u,Atom,Int_1,Int_2) :-
   tu_truth_value(Int_1,Atom,u),
   \+ tu_truth_value(Int_2,Atom,u).
 

test_tu_violation_1(V,Atom) :-
   tu_violation(V,Atom,
      [t(a),u(a),u(c)], 
      [t(a),u(a),u(c)]).
test_tu_violation_2(V,Atom) :-
   tu_violation(V,Atom,
      [t(a),u(a),u(c)], 
      [t(a),u(a),t(c),u(c)]).


transform_program_t(Program1,Program2) :-
   annotate_program_t(Program1,Program3),
   A =.. [u,X], B =.. [t,X],
   Program2 = [[A]-[B]|Program3].

transform_program_t_dis(Program1,Program2) :-
   annotate_program_t(Program1,Program3),
   disjunctive_transformation(Program3,Program4),
   A =.. [u,X], B =.. [t,X],
   Program2 = [[A]-[B]|Program4].


/******************************************************************/
 

