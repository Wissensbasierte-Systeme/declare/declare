

/******************************************************************/
/***                                                            ***/
/***             NMR:  Minimal Models                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* minimal_models(Program,Minimal_Models) <-
      Computes the set Minimal_Models of all minimal Herbrand
      models for a DLP Program. */

minimal_models(Program,Minimal_Models) :-
   minimal_models_operator(Program,Minimal_Models).

minimal_models_operator(Program,Minimal_Models) :-
   disjunctive_transformation(Program,Program_2),
   tree_tpi_mm_operator(Program_2,Minimal_Model_Tree),
   tree_to_state(Minimal_Model_Tree,Minimal_Models),
   !.

minimal_models_2(Program,Minimal_Models) :-
   minimal_models_operator_2(Program,Minimal_Models).

minimal_models_operator_2(Program,Minimal_Models) :-
   tree_tpi_mm_operator_2(Program,Minimal_Model_Tree),
   tree_to_state(Minimal_Model_Tree,Minimal_Models),
   !.


/* minimal_models_operator_g_state(State,Models) <-
      Computes the set Models of all minimal Herbrand models
      of a general Herbrand state State. */

minimal_models_operator_g_state(State,Models) :-
   g_state_to_dlp(State,Program),
   minimal_models_operator(Program,Models),
   !.


/* minimal_models_operator_edlp(Program,Models) <-
      Given an EDLP Program, the set Models of all consistent 
      minimal models is computed. */
 
minimal_models_operator_edlp(Program,Models) :-
   minimal_models_operator(Program,Minimal_Models),
   state_remove_tautologies(Minimal_Models,Models).


/* minimal_model_state_general(Program,General_State) <-
      Given a DNLP Program, the set General_State of all general
      disjunctions that are true in all minimal Herbrand models of 
      Program is computed. */
 
minimal_model_state_general(Program,General_State) :-
   dnlp_to_dlp(Program,Program_2),
   minimal_models_operator(Program_2,Minimal_Models),
   state_operator_general(Program_2,Minimal_Models,General_State).


/* minimal_model_state_negative_s(Program,Negative_State) <-
      Given a DNLP Program, the set Negative_State of all negative
      disjunctions that are true in all minimal Herbrand models of 
      Program is computed. */
 
minimal_model_state_negative_s(Program,Negative_State) :-
   dnlp_to_dlp(Program,Program_2),
   gcwa_operator(Program_2,Gcwa),
   egcwa_operator(Program_2,Egcwa),
   ord_union(Gcwa,Egcwa,Conjunctive_State),
   negate_state(Conjunctive_State,Negative_State), !.


/* minimal_model_semantics_i(Program,General_State) <-
      Given a DNLP Program, the set General_State of all general
      disjunctions that are true in all minimal Herbrand models of
      Program is computed. */
 
minimal_model_semantics_i(Program,General_State) :-
   minimal_model_state_general(Program,General_State).

cmms_operator(Program,General_State) :-
   minimal_model_state_general(Program,General_State).
 

/* minimal_model_semantics_s(Program,General_State) <-
      Given a DNLP Program, the set General_State of all general
      disjunctions that are true in all minimal Herbrand models of 
      Program is computed by binary resolution from the minimal model 
      state and the extended generalized closed-world-assumption. */

minimal_model_semantics_s(Program,General_State) :-
   minimal_model_semantics_msp_egcwa(Program,State1-State2),
   negate_state(State2,State3),
   state_to_tree(State1,Tree1), state_to_tree(State3,Tree2),
   tree_union(Tree1,Tree2,Tree3),
   tree_binary_resolve_fixpoint(Tree3,Tree),
   tree_to_state(Tree,General_State).

min_sem(Program,General_State) :-
   minimal_model_semantics_s(Program,General_State).


/* minimal_model_semantics_msp_egcwa(Program,Msp-Egcwa) <-
      Given a DNLP Program, the state pair consisting out of
      the minimal model state Msp and the extended generalized 
      closed-world-assumption Egcwa of Program are computed. */

minimal_model_semantics_msp_egcwa(Program,State1-State2) :-
   dnlp_to_dlp(Program,Program2),
   minimal_model_state(Program2,State1),
   state_to_gcwa_egcwa(Program2,State1,State2).


/* tree_tpi_mms_operator(Program,Tree) <-
      for a given DLP Program a clause tree Tree representing
      the minimal model state of Program is computed. */

tree_tpi_mms_operator(Program,Tree) :-
   tree_tpi_mm_operator(Program,Model_Tree), nl, nl,
   model_tree_to_clause_tree(Model_Tree,Tree).


/* tree_tps_mm_operator(Program,Model_Tree) <- 
      for a given DLP Program a model tree Model_Tree representing
      the set of minimal models of Program is computed. */ 
 
tree_tps_mm_operator(Program,Model_Tree) :-
   tree_tps_minimal_models(Program,Minimal_Models),      
   state_to_tree(Minimal_Models,Model_Tree).


/* tps_mm_operator(Program,Minimal_Models),
      for the DLP Program the set Minimal_Models of all minimal 
      models is computed. */

tps_mm_operator(Program,Minimal_Models) :-
   tps_minimal_models(Program,Minimal_Models).


/* tree_mm_operator(Program,Minimal_Models) <-
      for the DLP Program the set Minimal_Models of all minimal 
      models is computed. */

tree_mm_operator(Program,Minimal_Models) :-
   tree_tps_minimal_models(Program,Minimal_Models).


/* mm_operator(Program,Minimal_Models) <-
      for the DLP Program the set Minimal_Models of all minimal 
      models is computed. */

mm_operator(Program,Minimal_Models) :-
   tps_minimal_models(Program,Minimal_Models).


/* tree_tps_minimal_models(Program,Minimal_Models) <-
      for the DLP Program the set Minimal_Models of all minimal 
      models is computed. */

tree_tps_minimal_models(Program,Minimal_Models) :-
   tree_tps_fixpoint(Program,[],Minimal_Model_Tree),
   tree_to_state(Minimal_Model_Tree,Minimal_Model_State),
   tree_based_boolean_dualization(Minimal_Model_State,Minimal_Models).


/* tps_minimal_models(Program,Minimal_Models) <-
      for the DLP Program the set Minimal_Models of all minimal 
      models is computed. */

tps_minimal_models(Program,Minimal_Models) :-
   tps_fixpoint(Program,[],Minimal_Model_State),
   tree_based_boolean_dualization(Minimal_Model_State,Minimal_Models).


/* abbreviations */

consistent_mm(File) :-
   hidden( (
      dconsult(File,Program),
      minimal_models_operator_edlp(Program,Models) ) ),
   nl, writeln('consistent minimal models'),
   write('  Mm ='), pp_chs(Models), nl.

cmms(File) :-
   hidden( (
      dconsult(File,Program),
      minimal_model_state_general(Program,State) ) ),
   nl, pp_ghs('completed minimal model state',State).
 
mm(File) :-
   min(File).
 
min(File) :-
   hidden( (
      dconsult(File,Program),
      dnlp_to_dlp(Program,Program2),
      minimal_models_operator(Program2,Minimal_Models) ) ),
   nl, writeln('minimal models'),
   write('  Mm ='), pp_chs(Minimal_Models), nl.


/******************************************************************/

 
