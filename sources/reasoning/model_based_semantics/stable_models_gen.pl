

/******************************************************************/
/***                                                            ***/
/***         NMR:   Stable Models for Teaching                  ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(
      stable_models_computation_mode, optimized).


/*** interface ****************************************************/


/* stable_models_computation(Datalog, Coin) <-
      */

stable_models_computation(Datalog, Coin) :-
   stable_models_computation(optimized, Datalog, Coin).

stable_models_computation(Mode, Datalog, Coin) :-
   member(Mode, [optimized, regular]),
   dislog_variable_set(
      stable_models_computation_mode, Mode),
   findall( I,
      ( stable_models_iteration(Datalog, [[], []], I),
        partial_herbrand_interpretation_is_consistent(I) ),
      Coin_2 ),
   sort(Coin_2, Coin),
   stable_models_edges_to_picture(Coin).


/* stable_models_iteration(Datalog, I1, I2) <-
      */

stable_models_iteration(Datalog, I1, I2) :-
   Module = stable_models_computation,
   stable_models_close_interpretation(Datalog, Module, [], I1, I3),
   stable_models_iteration_(Datalog, I3, I2).

stable_models_iteration_(Datalog, I1, I2) :-
   stable_models_step(Datalog, I1, I3),
   ( I3 = I1,
     I2 = I3
   ; I3 \= I1,
     assert(stable_models_edge(I1, I3)),
     ( partial_herbrand_interpretation_is_inconsistent(I3),
       I2 = I3
     ; partial_herbrand_interpretation_is_consistent(I3),
       stable_models_iteration_(Datalog, I3, I2) ) ).


/* stable_models_step(Datalog, I1, I2) <-
      */

stable_models_step(Datalog, I1, I2) :-
   no_singleton_variable(Hypothesis),
   Module = stable_models_computation,
   stable_models_find_hypotheses(Datalog, I1, Hypotheses),
   ( member(Hypothesis, Hypotheses),
     write(user, '.'), ttyflush,
     stable_models_close_interpretation(
        Datalog, Module, Hypothesis, I1, I2)
   ; \+ member(Hypothesis, Hypotheses),
     I2 = I1 ).
 

/* stable_models_find_hypotheses(Program, I, Hypotheses) <-
      */

stable_models_find_hypotheses(Program, I, Hypotheses) :-
   I = [T, F],
   findall( Cs2,
      ( member(Rule, Program),
        parse_dislog_rule(Rule, [A], Bs, Cs), Cs \= [],
        subset_generate(Bs, T),
        sort(Cs, Cs2), \+ ord_subset(Cs2, F),
        ord_disjoint(Cs2, T),
        \+ member(A, T) ),
      Hypotheses ).


/* stable_models_close_interpretation(
         Datalog, Module, Hypothesis, I1, I2) <-
      */

stable_models_close_interpretation(
      Datalog, Module, Hypothesis, I1, I2) :-
   ( foreach(D, Datalog), foreach(D_2, Datalog_2) do
        parse_dislog_rule(D, As, Bs, Cs),
        ( foreach(C, Cs), foreach(N, Ns) do
             N = not_(C) ),
        append(Bs, Ns, Bs_2),
        parse_dislog_rule(D_2, As, Bs_2, []) ),
   dislog_rules_to_prolog_rules(Datalog_2, Prolog),
   stable_models_close_interpretation_prolog(
      Prolog, Module, Hypothesis, I1, I2).

stable_models_close_interpretation_prolog(
      Prolog, Module, Hypothesis, [T1, F1], [T2, F2]) :-
   ord_union(Hypothesis, F1, F2),
   ( dislog_variable_get(
        stable_models_computation_mode, optimized) ->
     Mod = gamma_operator
   ; Mod = Module ),
   Mod:assert(not_(A) :- member(A, F2)),
   ( dislog_variable_get(
        stable_models_computation_mode, optimized) ->
     ord_union(Prolog, T1, Prolog_2),
     gamma_iteration_prolog(Prolog_2, Mod, [T2, _])
   ; tp_iteration_prolog(Prolog, Mod, T1, T2) ),
   Mod:retract(not_(A) :- member(A, F2)).

stable_models_assert_hypothesis(Hypothesis, Module) :-
   foreach(A, Hypothesis) do
      Module:assert(not_(A)).

stable_models_retract_hypothesis(Hypothesis, Module) :-
   foreach(A, Hypothesis) do
      Module:retract(not_(A)).


/* stable_models_edges_to_picture(Coin) <-
      */

stable_models_edges_to_picture(Coin) :-
   findall( I-J,
      retract(stable_models_edge(I, J)),
      Edges ),
   findall( V,
      ( member(E, Edges),
        edge_to_vertex(E, I),
        partial_herbrand_interpretation_to_vertex(I, V) ),
      Vertices_2 ),
   maplist( partial_herbrand_interpretation_to_vertex,
      Coin, Vertices_3 ),
   ord_union(Vertices_2, Vertices_3, Vertices),
   remove_duplicates(Vertices, Vertices_1),
   ( foreach(I-J, Edges), foreach(V-W, Edges_1) do
        partial_herbrand_interpretation_to_name(I, V),
        partial_herbrand_interpretation_to_name(J, W) ),
   vertices_edges_to_picture_bfs(Vertices_1, Edges_1),
   !.

partial_herbrand_interpretation_to_name(I, Name) :-
   partial_herbrand_interpretation_to_literals(I, Ls),
   maplist( term_to_atom,
      Ls, Atoms ),
   names_append_with_separator(Atoms, ' ', Name).

partial_herbrand_interpretation_to_vertex(X, V) :-
   partial_herbrand_interpretation_to_name(X, N),
   ( partial_herbrand_interpretation_is_inconsistent(X),
     V = N-N-box-red-small
   ; V = N-N-circle-blue-small ),
   !.


/*** tests ********************************************************/


test(stable_models_teaching_all, all) :-
   forall( test(stable_models_teaching, X),
      writeln(user, X) ).

test(stable_models_teaching, N-Mode) :-
   stable_test_program(N, Datalog),
   member(Mode, [regular, optimized]),
   stable_models_computation(Mode, Datalog, _).

stable_test_program(N, Datalog) :-
   ( N = 1, Datalog = [
        [a]-[]-[b], [b]-[]-[c] ]
   ; N = 2, Datalog = [
        [a]-[]-[b], [b]-[]-[a] ]
   ; N = 3, Datalog = [
        [a]-[]-[b], [b]-[]-[c], [c]-[]-[a] ]
   ; N = 4, Datalog = [
        [a]-[]-[b], [b]-[]-[a], [c]-[]-[a,b], [d]-[]-[a] ]
   ; N = 5, Datalog = [
        [w(X)]-[m(X,Y)]-[w(Y)],
        [m(a,b)], [m(b,a)] ]
   ; N = 6, Datalog = [
        [c(1)],
        [a(X)]-[c(X)]-[b(X)],
        [b(X)]-[c(X)]-[a(X)],
        [d(X)]-[a(X)],
        [d(X)]-[b(X)] ]
   ; N = 7, Datalog = [
        [b]-[]-[a], [c]-[]-[b], [d]-[]-[e], [e]-[]-[d] ]
   ; N = 8, Datalog = [
        [b]-[]-[a], [c]-[]-[b], [d]-[b]-[e], [e]-[b]-[d] ] ).


/******************************************************************/


