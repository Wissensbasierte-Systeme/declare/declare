

/******************************************************************/
/***                                                            ***/
/***         NMR:  Stable Models - for DNLP's                   ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      stable_models/3.


/*** interface ****************************************************/


/* stable_models(Program,Stable_Models) <-
      The set Stable_Models of the stable models of a DNLP
      Program is computed. */
 
stable_models(Database_Id) :-
   dconsult(Database_Id,Program),
   stable_models(Program,Stable_Models),
   dportray(chs,Stable_Models).

stable_models(Program,Stable_Models) :-
   stable_models(default,Program,Stable_Models).

stable_models(default,Program,Stable_Models) :-
   stable_models_default(Program,Stable_Models).
stable_models(pe_nml_1,Program,Stable_Models) :-
   stable_models_pe_nml_1(Program,Stable_Models).
stable_models(pe_nml_2,Program,Stable_Models) :-
   stable_models_pe_nml_2(Program,Stable_Models).
stable_models(pe_nml_clark,Program,Stable_Models) :-
   stable_models_pe_nml_clark(Program,Stable_Models).

stable_models_default(Program,Stable_Models) :-
   ( ( stratify_test(Program),
       !,
       perfect_models_operator(Program,Stable_Models) )
   ; stable_models_pe_nml_1(Program,Stable_Models) ).

stable_models_db(Mode,Database_Id) :-
   hidden( (
      dconsult(Database_Id,Program),
      stable_models(Mode,Program,Stable_Models) ) ),
   bar_line_2,
   write('Stable Models ='),
   dportray(chs,Stable_Models).


partial_stable_models(pe_1_n_state,Program,N_State) :-
   partial_stable_models_pe_1_n_state(Program,_,N_State).


/* partial_stable_models_pe_1_n_state(Program,Program5,N_State) <-
      Given a DNLP Program,
      the negative Herbrand state N_State is computed by
       - partial evaluation, 
       - program simplification, and
       - generalized negation-as-failure. */

partial_stable_models_pe_1_n_state(Database_Id) :-
   hidden( (
      dconsult(Database_Id,Program),
      partial_stable_models_pe_1_n_state(Program,Program3,N_State) ) ),
   dportray(nhs,N_State),
   dportray(lp,Program3).

partial_stable_models_pe_1_n_state(Program,Program5,N_State) :-
      measure( 
   partial_evaluation(Program,Program2), Time1 ),
      write_length(Program2),
      measure( 
   simplify_program_3(Program2,Program3), Time2 ),
      write_length(Program3),
      measure( 
   simplify_program_gamma_pbe_dis(Program3,Program4), Time3 ),
      write_length(Program4),
      measure( 
   simplify_program_pbe_can(Program4,Program5), Time4 ),
      write_length(Program5),
      measure( (
   select_facts_dnlp(Program5,G_State),
   gnaf_fs_operator(Program5,G_State,[],N_State) ), Time5 ),
      write_length(N_State),
   writeln([Time1,Time2,Time3,Time4,Time5]).


/* stable_models_pe_nml_1(Program,Stable_Models) <-
      The set Stable_Models of the stable models of a DNLP
      Program is computed based on 
       - partial evaluation, 
       - normal transformation,
       - program simplification w.r.t. the well-founded semantics,
         and 
       - minimal models computation for states. */

stable_models_pe_nml_1(Program,Stable_Models) :-
      measure( 
   partial_evaluation(Program,Program2), Time1 ),
      write_length(Program2),
   normal_transformation(Program2,Program3),
      write_length(Program3),
      measure( (
   select_facts_dnlp(Program2,State2),
   simplify_program_3(Program3,State2,Program3a) ), Time1a ),
      write_length(Program3a),
%     dportray(lp,Program3a),
      measure(
   simplify_program_gamma_2_pbe(Program3a,Program4), Time2 ),
%     dportray(lp,Program4),
   simplify_program_pbe_can(Program4,Program4a),
      write_length(Program4a),
   disjunctive_transformation(Program4a,Program5),
      write_length(Program5),
   simplify_program(Program5,State5),
      write_length(State5),
      measure( (
   state_prune(State5,State5a),
   state_can(State5,State6) ), Time3 ),
      write_length(State5a),
      write_length(State6),
      measure( 
   tree_based_state_to_coin(State6,Minimal_Models), Time4 ),
      write_length(Minimal_Models),
      measure(
   stable_models_filter(Program4a,Minimal_Models,Stable_Models),
      Time5 ),
      write_length(Stable_Models),
   pp_times([Time1,Time1a,Time2,Time3,Time4,Time5]),
   !.


/* stable_models_pe_nml_clark(Program,Stable_Models) <-
      The set Stable_Models of the stable models of a DNLP
      Program is computed based on
       - partial evaluation,
       - normal transformation,
       - program simplification w.r.t. the well-founded semantics,
       - clark completion, and
       - minimal models computation for states. */

stable_models_pe_nml_clark(Program,Stable_Models) :-
      measure(
   partial_evaluation(Program,Program2), Time1 ),
      write_length(Program2),
   normal_transformation(Program2,Program3),
      write_length(Program3),
      measure( (
   select_facts_dnlp(Program2,State2),
   simplify_program_3(Program3,State2,Program3a) ), Time1a ),
      write_length(Program3a),
      measure( 
   simplify_program_gamma_2_pbe(Program3a,Program4), Time2 ),
   simplify_program_pbe_can(Program4,Program4a),
      write_length(Program4a),
   disjunctive_transformation(Program4a,Program5),
      write_length(Program5),
   simplify_program(Program5,State5), 
      write_length(State5), 
      measure( (
   state_prune(State5,State5a),
   state_can(State5,State6) ), Time3 ),
      write_length(State5a),
      write_length(State6),
      measure(
   clark_complete_pbe_program(Program4a,Completion), Time4 ),
      write_length(Completion), 
   dportray(lp,Program4a), dportray(lp,Completion),
      measure(
   gnaf_fs_completion(Program4a,Completion4a), Time4a ),
   dportray(lp,Completion4a),
%  Time4a = 0, Completion4a = Completion,
   ord_union(State6,Completion4a,Program6),
      measure(
   minimal_models_operator(Program6,Stable_Models), Time5 ),
      write_length(Stable_Models),
   pp_times([Time1,Time1a,Time2,Time3,Time4,Time4a,Time5]),
   !.

% write_length(_) :-
%    !.
write_length(X) :-
   length(X,I), 
   writeln(I).


/* stable_models_pe_nml_2(Program,Stable_Models) <-
      The set Stable_Models of the stable models of a DNLP
      Program is computed based on 
       - partial evaluation, 
       - normal transformation,
       - program simplification w.r.t. the well-founded semantics, 
         and 
       - stable models computation for normal logic programs. */

stable_models_pe_nml_2(Program,Stable_Models) :-
   partial_evaluation(Program,Program2),
   normal_transformation(Program2,Program3),
   stable_models_operator_sim(Program3,Stable_Models),
   !.


/******************************************************************/


