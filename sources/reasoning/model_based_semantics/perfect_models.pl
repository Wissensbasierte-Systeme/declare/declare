

/******************************************************************/
/***                                                            ***/
/***         NMR:  Perfect Models                               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


perf(File) :-
   dconsult(File,Program),
   hidden(perfect_models_operator(Program,Models)),
   nl, writeln('perfect models'),
   write('  Pm ='), pp_chs(Models), nl.

perf_ls(File) :-
   dconsult(File,Program),
   hidden(perfect_models_operator_ls(Program,Models)),
   nl, writeln('perfect models'),
   write('  Pm ='), pp_chs(Models), nl.


perfect_model_semantics(Program,State) :-
   perfect_model_state_general(Program,State).

perfect_model_state_general(Program,State) :-
   perfect_models_operator(Program,Models),
   state_operator_general_2(Program,Models,State).


perfect_model_state(Program,State) :-
   perfect_models(Program,Models),
   tree_based_boolean_dualization(Models,State),
   !.

perfect_models(Program,Models) :-
   perfect_models_operator(Program,Models).



perfect_models_operator(Program,Models) :-
   perfect_model_tree_operator(Program,Model_Tree),
   tree_to_state(Model_Tree,Models),
   !.

perfect_model_tree_operator(Program,Model_Tree) :-
   stratify_program(Program,Programs),
   tree_iterated_tpi_fixpoint(Programs,[[]],Model_Tree).


perfect_models_operator_ls(Program,Models) :-
   perfect_model_tree_operator_ls(Program,Model_Tree),
   tree_to_state(Model_Tree,Models),
   !.

perfect_model_tree_operator_ls(Program,Model_Tree) :-
   locally_stratify_program(Program,Programs),
   tree_iterated_tpi_fixpoint(Programs,[[]],Model_Tree).


tree_iterated_tpi_fixpoint([Program|Programs],Tree1,Tree3) :-
   tree_tpi_fixpoint(Program,Tree1,Tree2),
   tree_to_state(Tree2,State2),
   dportray(chs,State2),
   tree_iterated_tpi_fixpoint(Programs,Tree2,Tree3).
tree_iterated_tpi_fixpoint([],Tree,Tree).


perfect_model_state_negative_i(Program,Negative_State) :- 
   perfect_models_operator_ls(Program,Perfect_Models), 
   state_operator_negative_i(
      Program,Perfect_Models,Negative_State).


/******************************************************************/


