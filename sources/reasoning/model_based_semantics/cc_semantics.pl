

/******************************************************************/ 
/***                                                            ***/
/***             DisLog :  CC-Semantics                         ***/ 
/***                                                            ***/ 
/******************************************************************/ 
  
  
:- multifile 
      cc_semantics/3, 
      cc_semantics/4.
:- dynamic 
      cc_semantics/4.


/*** interface ****************************************************/


/* cc_semantics(Type,Mode,Database_Name) <-
      the semantics Type is applied to the database with the
      name Database_Name, the result type is Mode. */

cc_semantics(Type,Mode,Database_Name) :-
   dconsult(Database_Name,Database),
   cc_semantics(Type,Mode,Database,State),
   bar_line_2,
   write('Semantics ='),
   change_hb_to_hs(Mode,Portray_Mode),
   dportray(user,Portray_Mode,State).

cc_semantics(Semantics,Result_Type,Program,State) :-
   ( Result_Type == dhb
   ; Result_Type == hi ),
   !,
   distore(Program,'results/program'),
   concat([
      'tools/cc_semantics ',Semantics,' ',Result_Type,' ',
      'results/program results/output > /dev/null'],
      Command ),
   ( unix(system(Command))
   ; true ),
   diconsult('results/output',List),
   state_sort(List,State).

cc_semantics(Semantics,nhb,Program,State) :-
   distore(Program,'results/program'),
   concat([
      'tools/cc_semantics ',Semantics,' dhb ',
      'results/program results/output > /dev/null; ',
      'cp results/output results/program; ',
      'tools/cc_semantics ',Semantics,' nhb ',
      'results/program results/output > /dev/null'],
      Command ),
   ( unix(system(Command))
   ; true ),
   diconsult('results/output',List),
   state_sort(List,Disjunctive_State),
   negate_state(Disjunctive_State,State).


/******************************************************************/


