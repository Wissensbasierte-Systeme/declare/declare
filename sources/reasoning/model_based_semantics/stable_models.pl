

/******************************************************************/
/***                                                            ***/
/***         NMR:   Stable Models                               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* stable_models(Mode,Program,Stable_Models) <-
      Given a DNLP Program, the set Stable_Models of all
      stable models of Program is computed
      based on a technique indicated by Mode. */

stable_models(evidential_1,Program,Stable_Models) :-
   stable_models_operator_1(Program,Stable_Models).
stable_models(evidential_2,Program,Stable_Models) :-
   stable_models_operator_2(Program,Stable_Models).
stable_models(evidential_tps,Program,Stable_Models) :-
   tps_stable_models_operator(Program,Stable_Models).


/* stable_models_operator(Program,Stable_Models) <-
      Given a DNLP Program, the set Stable_Models of all
      stable models of Program is computed
      based on the technique of evidential transformation. */

stable_models_operator(Program,Stable_Models) :-
   ( ( stratify_test(Program),
       !,
       perfect_models_operator(Program,Stable_Models) )
   ; stable_models_operator_1(Program,Stable_Models) ).

stable_models_operator_1(Program,Stable_Models) :-
   evidential_transformation(Program,Program2),
   minimal_models_operator(Program2,Minimal_Models),
   evidential_state_can(Minimal_Models,State1),
   evidential_state_reduce(State1,State2),
   evidential_state_to_state(State2,Stable_Models).

stable_models_operator_2(Program,Stable_Models) :-
   evidential_transformation(Program,Program2),
   minimal_models_operator_2(Program2,Minimal_Models),
   evidential_state_can(Minimal_Models,State1),
   evidential_state_reduce(State1,State2),
   evidential_state_to_state(State2,Stable_Models).

tps_stable_models_operator(Program,Stable_Models) :-
   evidential_transformation(Program,Program2),
   tree_tps_fixpoint(Program2,[],Tree),
   tree_to_dual_state(Tree,Minimal_Models),
   evidential_state_can(Minimal_Models,State1),
   evidential_state_reduce(State1,State2),
   evidential_state_to_state(State2,Stable_Models).

sm_operator(Program,Stable_Models) :-
   stable_models_operator(Program,Stable_Models).


/* stable_models_operator_ednlp(Program,Models) <-
      Given an EDNLP Program, the set Models of all consistent
      stable models is computed. */

stable_models_operator_ednlp(Program,Models) :-
   stable_models_operator(Program,Stable_Models),
   state_remove_tautologies(Stable_Models,Models).


/* stable_model_state_operator(Program,Stable_Model_State) <-
      Given a DNLP Program, the set Stable_Model_State of all
      positive disjunctions that are true in all stable models 
      of Program is computed. */

stable_model_state_operator(Program,Stable_Model_State) :-
   stable_models_operator(Program,Stable_Models),
   tree_based_boolean_dualization(Stable_Models,Stable_Model_State).

sms_operator(Program,Stable_Model_State) :-
   stable_model_state_operator(Program,Stable_Model_State).

stable_model_state(Program,Stable_Model_State) :-
   stable_model_state_operator(Program,Stable_Model_State).


/* stable_model_state_negative_i(Program,Negative_State) <-
      Given a DNLP Program, the set Negative_State of all negative 
      disjunctions that are true in all stable models of Program
      is computed. */

stable_model_state_negative_i(Program,State) :-
   stable_models_operator(Program,Stable_Models),
   state_operator_negative_i(Program,Stable_Models,State).

stable_model_state_negative_s(Program,Negative_State) :-
   stable_models_operator(Program,Models),
   state_operator_negative_s(Program,Models,Negative_State).


/* stable_model_state_general(Program,General_State) <-
      Given a DNLP Program, the set General_State of all general
      disjunctions that are true in all stable models of Program
      is computed. */
 
stable_model_state_general(Program,General_State) :-
   stable_models_operator(Program,Stable_Models),
   state_operator_general_2(Program,Stable_Models,General_State).
 
csms_operator(Program,General_State) :-
   stable_model_state_general(Program,General_State).
 
stable_model_semantics(Program,General_State) :-
   stable_model_state_general(Program,General_State).
 


/*** implementation ***********************************************/


/* evidential_state_to_state(State1,State2) <-
      a state State1 of evidential clauses is transformed to a
      conventional state by removing all evidential atoms 
      E A from the clauses. */

evidential_state_to_state([C1|Cs1],[C2|Cs2]) :-
   !,
   evidential_clause_to_clause(C1,C2),
   evidential_state_to_state(Cs1,Cs2).
evidential_state_to_state([],[]).


/* evidential_clause_to_clause(Clause1,Clause2) <-
      a given evidential clause Clause1 is transformed to a
      conventional clause Clause2 by removing all evidential atoms
      E A from Clause1. */

evidential_clause_to_clause(['E'(_)|As],New_As) :-
   !,
   evidential_clause_to_clause(As,New_As).
evidential_clause_to_clause([A|As],[A|New_As]) :-
   !,
   evidential_clause_to_clause(As,New_As).
evidential_clause_to_clause([],[]).


/* evidential_state_reduce(State1,State2) <- 
      given a state State1, the state State2 will contain all
      clauses C of State1 which are evidentially correct. */

evidential_state_reduce([Clause|Cs1],[Clause|Cs2]) :-
   evidential_clause_correct(Clause),
   !,
   evidential_state_reduce(Cs1,Cs2).
evidential_state_reduce([_|Cs1],Cs2) :-
   !,
   evidential_state_reduce(Cs1,Cs2).
evidential_state_reduce([],[]).


/* evidential_clause_correct(Clause) :-
      an evidential clause Clause is correct if for each its evidential 
      atoms E A it also contains the conventional atom A. */

evidential_clause_correct(Clause) :-
   evidential_clause_correct(Clause,Clause).

evidential_clause_correct(['E'(A)|As],Clause) :-
   !,
   member(A,Clause),
   evidential_clause_correct(As,Clause).
evidential_clause_correct([_|As],Clause) :-
   !,
   evidential_clause_correct(As,Clause).
evidential_clause_correct([],_).


/* evidential_state_can(State1,State2) <-
      given a state State1, the state State2 will contain all
      clauses of State1 which are not subsumed evidentially by 
      some other clause of State1. */

evidential_state_can(State1,State2) :-
   evidential_state_can(State1,State1,State2).

evidential_state_can(State,[C|Cs],New_Cs) :-
   member(Clause,State),
   Clause \== C,
   evidential_clause_subsumes_clause(Clause,C),
   !, 
   evidential_state_can(State,Cs,New_Cs).
evidential_state_can(State,[C|Cs],[C|New_Cs]) :-
   evidential_state_can(State,Cs,New_Cs).
evidential_state_can(_,[],[]).


/* evidential_state_subsumes_state(State1,State2) <-
      a state State1 evidentially subsumes another state State2
      if each clause of State2 is evidentially subsumed by some
      clause of State1. */

evidential_state_subsumes_state(State,[C|Cs]) :-
   evidential_state_subsumes_clause(State,C),
   evidential_state_subsumes_state(State,Cs).
evidential_state_subsumes_state(_,[]).


/* evidential_state_subsumes_clause(State,Clause) <-
      a state State evidentially subsumes a clause Clause
      if Clause is evidentially subsumed by some clause of State1. */

evidential_state_subsumes_clause(State,Clause) :-
   member(C,State),
   evidential_clause_subsumes_clause(C,Clause).


/* evidential_clause_subsumes_clause(Clause1,Clause2) <-
      a clause Clause1 evidentially subsumes another clause Clause2
      if all conventional atoms A of Clause1 also occur in Clause2
      and for all evidential atoms E A of Clause1 either A or E A or
      both occur in Clause2. */

evidential_clause_subsumes_clause(['E'(A)|As],Clause) :-
   !,
   ( member(A,Clause); member('E'(A),Clause) ),
   evidential_clause_subsumes_clause(As,Clause).
evidential_clause_subsumes_clause([A|As],Clause) :-
   member(A,Clause),
   evidential_clause_subsumes_clause(As,Clause).
evidential_clause_subsumes_clause([],_).


/******************************************************************/


