

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Meta Disjunctive WFS - MDWFS             ***/
/***                                                            ***/
/******************************************************************/


/* mdwfs_compute_basic_interpretation <-
      */

mdwfs_compute_basic_interpretation :-
   dislog_variable_get(wfs_basic_module,File_B),
   dislog_variable_get(wfs_basic_interpretation,File_I),
   dconsult(File_B,Program_B),
   tpi_fixpoint_iteration(Program_B,[I]),
   dsave([I],File_I).


/* dnlp_to_wfs_basic_interpretation <-
      */

dnlp_to_wfs_basic_interpretation(File) :-
   dconsult(File,P),
   dnlp_to_wfs_basic_interpretation(P,I),
   dislog_variable_get(wfs_basic_interpretation,File_I),
   dsave(I,File_I).

dnlp_to_wfs_basic_interpretation(Program,I) :-
   dnlp_to_herbrand_ordering(Program,
      Atom_Definition,Smaller_Definition),
   dislog_variable_get(wfs_basic_module,File_B),
   dconsult(File_B,Program_B),
   semi_stratify_program(Program_B,Programs),
   ord_union(Atom_Definition,Smaller_Definition,P_Init),
   dportray(lp,P_Init), wait,
   iterative_tpi_fixpoint_iteration([P_Init|Programs],[],I).
   
iterative_tpi_fixpoint_iteration([P|Ps],I,K) :-
   dportray(lp,P),
   tps_fixpoint_special(P,I,J),
%  writeln(user,tpi_fixpoint_iteration(Q,[As])),
%  writeln(user,As), wait,
   iterative_tpi_fixpoint_iteration(Ps,J,K).
iterative_tpi_fixpoint_iteration([],I,I).


tps_fixpoint_special([Rule],I,J) :-
   parse_dislog_rule_save(Rule,[A],Body,_),
   A = binary_resolvent(_,_,_),
   list_to_comma_structure(Body,Body_2),
   assert_in_prolog(I),
   assert(':-'(A,Body_2)),
   findall( [A], A, K ),
   append(K,I,J),
   retract(':-'(A,Body_2)),
   retractall(conjunction(_,_,_)),
   retractall(extract(_,_,_)),
   retractall(complementary_literal(_,_)).
tps_fixpoint_special(P,I,J) :-
   tps_fixpoint(P,I,J).
  
assert_in_prolog([[A]|S]) :-
   ( A = conjunction(_,_,_)
   ; A = extract(_,_,_)
   ; A = complementary_literal(_,_) ),
   assert(A),
   assert_in_prolog(S).
assert_in_prolog([_|S]) :-
   assert_in_prolog(S).
assert_in_prolog([]).

 
/* dnlp_to_herbrand_ordering(
         Program,Atom_Definition,Smaller_Definition) <-
      */

dnlp_to_herbrand_ordering(Program,
      Atom_Definition,Smaller_Definition) :-
   dnlp_to_herbrand_base(Program,Herbrand_Base),
   atoms_to_atom_definition(Herbrand_Base,Atom_Definition),
   atoms_to_smaller_definition(Herbrand_Base,Smaller_Definition).

atoms_to_smaller_definition([A1,A2|As],[[smaller(A1,A2)]|Ss]) :-
   atoms_to_smaller_definition([A2|As],Ss).
atoms_to_smaller_definition(_,[]).

atoms_to_atom_definition(Atoms,Atom_Definition) :-
   maplist( atom_to_atom_fact,
      Atoms, Atom_Definition ).
   
atom_to_atom_fact(A,[atom(A)]).


/******************************************************************/


