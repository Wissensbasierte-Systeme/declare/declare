

/******************************************************************/
/***                                                            ***/
/***        Teaching:  Gamma-Operator for Prolog                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gamma_iteration_prolog(Program, Module, [True, Undef]) <-
      */

gamma_iteration_prolog(Program, Module, [True, Undef]) :-
   gamma_iteration_prolog(
      dportray(tu_pair), Program, Module, [True, Undef] ).


/* gamma_iteration_prolog(
         Show, Program, Module, [True, Undef]) <-
      */

gamma_iteration_prolog(
      Show, Program, Module, [True, Undef]) :-
   set_num(gamma_iteration, 0),
   gamma_operator_prolog(Program, Module, [], I),
   gamma_iteration_prolog(Show, Program, Module,
      [[], I], [True, Undef]).

gamma_iteration_prolog(
      Show, Program, Module, [I1, J1], [I2, J2]) :-
   get_num(gamma_iteration, N),
   writeln(N),
   call(Show, [I1, J1]),
   gamma_operator_prolog(Program, Module, J1, I3),
   list_to_ord_set(I3, I4),
   gamma_operator_prolog(Program, Module, I4, J3),
   list_to_ord_set(J3, J4),
   ( ( [I4, J4] = [I1, J1],
       !,
       [I2, J2] = [I1, J1] )
   ; gamma_iteration_prolog(
        Show, Program, Module, [I4, J4], [I2, J2]) ).


/* gamma_operator_prolog(Program, Module, T1, T2) <-
      */

gamma_operator_prolog(Program, Module, T1, T2) :-
   Module:assert(not_(A) :- \+ member(A, T1)),
   tp_iteration_prolog(Program, Module, [], T2),
   Module:retract(not_(A) :- \+ member(A, T1)).


/******************************************************************/


