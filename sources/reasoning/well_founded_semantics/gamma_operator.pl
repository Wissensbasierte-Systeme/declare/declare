

/******************************************************************/
/***                                                            ***/
/***          Teaching:  Gamma-Operator                         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gamma_iteration(Program, [True, Undef]) <-
      */

gamma_iteration(Program, [True, Undef]) :-
   gamma_iteration(
      dportray(tu_pair), Program, [True, Undef] ).


/* gamma_iteration(Show, Program, [True, Undef]) <-
      */

gamma_iteration(Show, Program, [True, Undef]) :-
   set_num(gamma_iteration, 0),
   gamma_operator(Program, [], I),
   gamma_iteration(Show, Program, [[], I], [True, Undef]).

gamma_iteration(Show, Program, [I1, J1], [I2, J2]) :-
   get_num(gamma_iteration, N),
   writeln(N),
   call(Show, [I1, J1]),
   gamma_operator(Program, J1, I3),
   list_to_ord_set(I3, I4),
   gamma_operator(Program, I4, J3),
   list_to_ord_set(J3, J4),
   ( ( [I4, J4] = [I1, J1],
       !,
       [I2, J2] = [I1, J1] )
   ; gamma_iteration(Show, Program, [I4, J4], [I2, J2]) ).


/* gamma_operator(Program, I1, I2) <-
      */

gamma_operator(Program, I1, I2) :-
   tp_iteration_normal(Program, I1, [], I2).

tp_iteration_normal(Program, True, I1, I2) :-
   tp_operator_normal(Program, True, I1, I3),
   list_to_ord_set(I3, I4),
   ( ( I4 = I1,
       !,
       I2 = I1 )
   ; tp_iteration_normal(Program, True, I4, I2) ).


/* tp_operator_normal(Program, True, Int_1, Int_2) <-
      */

tp_operator_normal(Program, True, Int_1, Int_2) :-
   findall( A,
      ( member(Rule, Program),
        ( parse_rule(Rule, [A]-Body-Negative)
        ; parse_rule(Rule, [A]-Body),
          Negative = [] ),
        subset_generate(Body, Int_1),
        \+ intersects(Negative, True) ),
      Int_2 ).


/*** tests ********************************************************/


test(gamma_iteration, 1) :-
   File = 'examples/well_founded_semantics/counter',
   dislog_consult(File, P),
   gamma_iteration(dportray(tu_pair), P, _).

test(gamma_iteration, 2) :-
   File = 'examples/well_founded_semantics/counter',
   dislog_consult(File, P),
   gamma_iteration(show_counter_interpretation_pair, P, _).

show_counter_interpretation_pair([I, J]) :-
   filter_counter_interpretation(last, [p], I, I2),
   filter_counter_interpretation(all, [enp], J, J2),
   dportray(tu_pair, [I2, J2]).

filter_counter_interpretation(Where, Ps, As, Ds) :-
   findall( B,
      ( member(A, As),
        A =.. [P|Xs],
        member(P, Ps),
        maplist( term_to_term_depth,
           Xs, Ys ),
        B =.. [P|Ys] ),
      Bs ),
   sort(Bs, Cs),
   ( ( Where = all
     ; Cs = [] ),
     Ds = Cs
   ; member(Where, [first, last]),
     apply(Where, [Cs, C]),
     Ds = [C] ).


/******************************************************************/


