

/******************************************************************/
/***                                                            ***/
/***          DisLog:  Meta Disjunctive WFS - MDWFS             ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variables_set([
      smodels_in - smodels_in,
      smodels_out - smodels_out,
      wfs_logic_module_file(2) -
         'wfs_meta/wfs_logic_module',
      wfs_logic_module_file(3) -
         'wfs_meta/wfs_logic_module_partial',
      wfs_logic_module_file(4) -
         'wfs_meta/wfs_logic_module_ground',
      wfs_basic_module -
         'wfs_meta/wfs_basic_module',
      wfs_basic_interpretation -
         'wfs_meta/wfs_basic_interpretation',
      smodels_type(wfs) - ['-w',2],
      smodels_type(stable) - ['0',2],
      smodels_type(stable_three) - ['0',3] ]).


/*** interface ****************************************************/


/* mdwfs_dislog_program_smodels_evaluate(Semantics,File,State) <-
      */

mdwfs_dislog_program_smodels_evaluate(Semantics,File,State) :-
   dislog_variable_get(smodels_in,File_I),
   dislog_variable_get(smodels_out,File_O),
   dis_ex_extend_filename(File_I,Path_I),
   dis_ex_extend_filename(File_O,Path_O),
   dislog_variable_get(smodels_type(Semantics),[Type,Mode]),
   mdwfs_dislog_file_to_smodels_input_file(Mode,File,File_I),
   mdwfs_smodels_evaluation(Type,Path_I,Path_O),
   mdwfs_smodels_output_file_to_dislog_state(Mode,Path_O,State).


/* mdwfs_smodels_evaluation(Type,Path_I,Path_O) <-
      */

mdwfs_smodels_evaluation(Type,Path_I,Path_O) :-
   name_append([
      'pparse < ',Path_I,' | smodels ',Type,' > ',Path_O ],
      Command ),
   us(Command).


/* mdwfs_dislog_file_to_smodels_input_file(Mode,File_1,File_2) <-
      */

mdwfs_dislog_file_to_smodels_input_file(Mode,File_1,File_2) :-
   dconsult(File_1,Program_1),
   cd_transformation(Mode,Program_1,Program_Given),
%  dportray(lp,Program_Given),
   dislog_variable_get( wfs_logic_module_file(Mode),
      Wfs_Logic_Module_File ),
   dconsult(Wfs_Logic_Module_File,Program_Logic),
%  dportray(lp,Program_Logic),
   append(Program_Given,Program_Logic,Program_All),
   partially_ground_program(Program_All,Program_2),
%  dportray(lp,Program_2),
   !,
   dsave_for_smodels(Program_2,File_2).


/* smodels_output_file_to_dislog_state(Mode,File,State) <-
      */

mdwfs_smodels_output_file_to_dislog_state(Mode,File,State) :-
   parse_stable_models_file(File,Models),
   maplist( list_to_ord_set,
      Models, Models_2 ),
   ord_intersection(Models_2,Model),
   maplist( mdwfs_smodel_to_state(Mode),
      [Model|Models_2], [State|States_2] ),
   checklist( dportray(dhs),
      States_2 ),
   dportray(dhs,State).


/* partially_ground_program(Program_1,Program_2) <-
      */

partially_ground_program :-
   dislog_variable_get(wfs_logic_module_file(2),File_I),
   dislog_variable_get(wfs_logic_module_file(4),File_G),
   dconsult(File_I,Program_L),
   partially_ground_program(Program_L,Program_G),
   dsave(Program_G,File_G).

partially_ground_program(Program_1,Program_2) :-
   dislog_variable_get(wfs_basic_interpretation,File_I),
   dconsult(File_I,[I]),
   Activation = [
      activate(d_complement,negative) ],
%     activate(c_complement,negative) ],
   append(Activation,I,J),
   partially_ground_program(J,Program_1,Program_2).

partially_ground_program(I,Program_1,Program_2) :-
   partially_ground_program( I,
      [ activate,
        clause, atomic_clause, positive_clause,
        empty_clause, literal,
        complementary_clause, binary_resolvent,
%       merge,
        subsumes, conjunction ],
      Program_1, Program_3 ),
   dislog_program_flatten_arguments(Program_3,Program_2).


/*** implementation ***********************************************/


/* partially_ground_program(
         Atoms,Predicates,Program_1,Program_2) <-
      */

partially_ground_program(Atoms,Predicates,Program_1,Program_2) :-
   maplist( partially_ground_rule(Atoms,Predicates),
      Program_1, Programs_3 ),
   append(Programs_3,Program_3),
   simplify_program_pgp(Program_3,Program_2).

partially_ground_rule(Atoms,Predicates,Rule,Rules) :-
   parse_dislog_rule(Rule,H1,B1,N1),
   findall( H1-B2-N1,
      partially_ground_atoms(Atoms,Predicates,B1,B2),
      Rules ).

partially_ground_atoms(Atoms,Predicates,As1,As2) :-
   maplist( partially_ground_atom(Atoms,Predicates),
      As1, List ),
   append(List,As2).
   
partially_ground_atom(Atoms,Predicates,A,[]) :-
   A =.. [P|_],
   member(P,Predicates),
   !,
   member(A,Atoms).
partially_ground_atom(_,_,A,[A]).


/* simplify_program_pgp(Program_1,Program_2) <-
      */

simplify_program_pgp(Program_1,Program_2) :-
   sublist( non_trivial_rule,
      Program_1, Program_2 ).

non_trivial_rule(Rule) :-
   parse_dislog_rule(Rule,H1,B1,_),
   ord_disjoint(H1,B1).



/* dislog_program_flatten_arguments(Program_1,Program_2) <-
      */

dislog_program_flatten_arguments(Program_1,Program_2) :-
   maplist( dislog_rule_flatten_arguments,
      Program_1, Program_2 ).

dislog_rule_flatten_arguments(Rule_1,Rule_2) :-
   parse_dislog_rule(Rule_1,H1,B1,N1),
   dislog_clause_flatten_arguments(H1,H2),
   dislog_clause_flatten_arguments(B1,B2),
   dislog_clause_flatten_arguments(N1,N2),
   Rule_2 = H2-B2-N2.

dislog_clause_flatten_arguments(Clause_1,Clause_2) :-
   maplist( dislog_atom_flatten_arguments,
      Clause_1, Clause_2 ).

dislog_atom_flatten_arguments(t(Atom_1),t(Atom_2)) :-
   Atom_1 =.. [Functor|Terms],
   mdwfs_terms_to_atoms(Terms,Atoms),
   Atom_2 =.. [Functor|Atoms].
dislog_atom_flatten_arguments(u(Atom_1),u(Atom_2)) :-
   Atom_1 =.. [Functor|Terms],
   mdwfs_terms_to_atoms(Terms,Atoms),
   Atom_2 =.. [Functor|Atoms].
dislog_atom_flatten_arguments(Atom_1,Atom_2) :-
   Atom_1 =.. [Functor|Terms],
   mdwfs_terms_to_atoms(Terms,Atoms),
   Atom_2 =.. [Functor|Atoms].


/* mdwfs_smodel_to_state(Mode,Model,State) <-
      */

mdwfs_smodel_to_state(2,Model,State) :-
   findall( C,
      ( member(dis(Term),Model),
        mdwfs_term_to_clause(Term,Clause),
        list_to_ord_set(Clause,C) ),
      State_2 ),
   state_can(State_2,State).

mdwfs_smodel_to_state(3,Model,State) :-
   findall( C,
      ( member(t(dis(Term)),Model),
        mdwfs_term_to_clause(Term,Clause),
        list_to_ord_set(Clause,C) ),
      State_1 ),
   state_can(State_1,State).

mdwfs_smodel_to_state(3,Model,State) :-
   findall( C,
      ( member(t(dis(Term)),Model),
        mdwfs_term_to_clause(Term,Clause),
        list_to_ord_set(Clause,C) ),
      State_1 ),
   state_can(State_1,State_1a),
   findall( [t(C)],
      member(C,State_1a),
      State_1b ),
   findall( C,
      ( member(u(dis(Term)),Model),
        mdwfs_term_to_clause(Term,Clause),
        list_to_ord_set(Clause,C) ),
      State_2 ),
   state_subtract(State_2,State_1,State_2a),
   state_can(State_2a,State_2b),
   findall( [u(C)],
      member(C,State_2b),
      State_2c ),
   state_union(State_1b,State_2c,State).


mdwfs_terms_to_atoms(Terms,Atoms) :-
   maplist( mdwfs_clause_to_term,
      Terms, Atoms ).

mdwfs_clause_to_term(Clause,Term) :-
   maplist( mdwfs_smodels_pack_literal,
      Clause, Clause_2 ),
   Term =.. [l|Clause_2].

mdwfs_term_to_clause(Term,Clause) :-
   Term =.. [l|Clause_2],
   maplist( mdwfs_smodels_pack_literal,
      Clause, Clause_2 ).
        

mdwfs_smodels_pack_literal(~A,n(A)) :-
   !.
mdwfs_smodels_pack_literal(A,A).


/******************************************************************/


