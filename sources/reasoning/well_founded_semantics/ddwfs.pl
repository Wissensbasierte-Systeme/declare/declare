

/******************************************************************/
/***                                                            ***/
/***          DisLog:  DD-WFS                                   ***/
/***                                                            ***/
/******************************************************************/


sdwfs_pe_operator(Database_Id) :-
   dconsult(Database_Id,Program),
   hidden( sdwfs_pe_operator(Program,State) ),
   dportray(ghs,State).

sdwfs_pe_operator(Program,State) :-
   partial_evaluation_cc(Program,Pbe_Program_1),
   simplify_program_3(Pbe_Program_1,Pbe_Program_2),
   sdwfs_operator(Pbe_Program_2,State).


bdwfs_pe_operator(Database_Id) :-
   dconsult(Database_Id,Program),
   bdwfs_pe_operator(Program,State),
   dportray(ghs,State).

bdwfs_pe_operator(Program,State) :-
   partial_evaluation_cc(Program,Pbe_Program),
   bdwfs_operator(Pbe_Program,State).


pbe_program_to_egcwa(Pbe_Program,State,Egcwa) :-
   state_to_tree(State,Tree_S),
   assemble_gsn_sets(Pbe_Program,Tree_S,Gsn_Sets),
   init_state_triple([],_,[]),
   assert(minimal_model_state(State)),
   analysed_sn_sets_to_egcwa(Gsn_Sets,Egcwa).

assemble_gsn_sets(Pbe_Program,Tree_S,Gsn_Sets) :-
   findall( _,
      ( member(Head-[]-Body_2,Pbe_Program),
        member(Atom,Head),
        resolve(Atom,Head,Clause_1),
        list_to_ord_set(Clause_1,Clause_2),
        ord_union(Clause_2,Body_2,Clause),
        \+ tree_subsumes_clause(Tree_S,Clause),
        assert(support_pair(Atom,Clause)) ), _ ),
   findall( Atom,
      support_pair(Atom,_), List ),
   list_to_ord_set(List,Atoms),
   findall( [[Atom]|Gsn],
      ( member(Atom,Atoms),
        collect_all_support_pairs_of_atom(Atom,Gsn) ),
      Gsn_Sets ).
        
collect_all_support_pairs_of_atom(Atom,Gsn) :-
   findall( Clause,
      retract(support_pair(Atom,Clause)), List ),
   list_to_ord_set(List,Gsn),
   !.


/*** tests ********************************************************/


test(nmr:ddwfs,1) :-
   Database_Id = 'path_blocked/pb1',
%  state_to_tree([[a,b]],Tree_S),
   state_to_tree([],Tree_S),
   dconsult(Database_Id,Program),
   partial_evaluation(Program,Pbe_Program),
   assemble_gsn_sets(Pbe_Program,Tree_S,Gsn_Sets),
   writeln(user,Gsn_Sets).

test(nmr:ddwfs,2) :-
   Database_Id = 'path_blocked/pb1',
%  State = [[a,b]],
   State = [
      [p(a,b),p(a,c)],
      [p(b,d)],[p(c,d)],[p(f,a)],[p(d,e)],
      [b(d,e),b(f,a)] ],
   dconsult(Database_Id,Program),
   partial_evaluation(Program,Pbe_Program),
   pbe_program_to_egcwa(Pbe_Program,State,Egcwa),
   writeln(user,Egcwa).


/******************************************************************/


