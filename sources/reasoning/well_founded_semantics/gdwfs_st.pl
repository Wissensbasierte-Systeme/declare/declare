

/******************************************************************/
/***                                                            ***/
/***             DisLog :  GDWFS-Operator                       ***/
/***                                                            ***/
/******************************************************************/
 
/*** This module works with state triples. ***/


:- dynamic 
      u_fact/1.


/*** interface ****************************************************/


gdwfs_operator(Program,State_Pair) :-
   sed_fixpoint(Program,[[],[]],State_Pair).

sed_fixpoint(Program,State_Pair1,State_Pair2) :-
   measure_hidden( 1, (
      dnlp_to_dn_clauses(Program),
      assert_dn_clause_to_d_fact_d_clause,
      assert(delta_d_fact(dummy)),
      state_pair_to_state_triple(State_Pair1,[T1,U1,F1]),
      retractall(state_triple(_,_,_)),
      assert(state_triple(T1,U1,F1)),
      sed_fixpoint,
      retract(state_triple(T2,U2,F2)),
      dabolish_all([dn_clause/3,d_clause/2,d_fact/1]),
      state_triple_to_state_pair([T2,U2,F2],State_Pair2),
      dabolish(herbrand_base/1) ) ),
   !.


dwfs_operator(Program,State_Pair) :-
   sd_fixpoint(Program,[[],[]],State_Pair).
 
sd_fixpoint(Program,State_Pair1,State_Pair2) :-
   measure_hidden( 1, (
      dnlp_to_dn_clauses(Program),
      assert_dn_clause_to_d_fact_d_clause,
      assert(delta_d_fact(dummy)),
      state_pair_to_state_triple(State_Pair1,[T1,U1,F1]),
      retractall(state_triple(_,_,_)),
      assert(state_triple(T1,U1,F1)),
      sd_fixpoint,
      retract(state_triple(T2,U2,F2)),
      dabolish_all([dn_clause/3,d_clause/2,d_fact/1]),
      state_triple_to_state_pair([T2,U2,F2],State_Pair2),
      dabolish(herbrand_base/1) ) ),
   !.


sed_operator(Program,State_Pair1,State_Pair3) :-
   sd_operator(Program,State_Pair1,State_Pair2),
   se_operator(Program,State_Pair2,State_Pair3).

se_operator(Program,State_Pair1,State_Pair2) :-
   measure_hidden( 1, (
      dnlp_to_dn_clauses(Program),
      state_pair_to_state_triple(State_Pair1,[T1,U1,F1]),
      retractall(state_triple(_,_,_)),
      assert(state_triple(T1,U1,F1)),
      se_operator,
      retract(state_triple(T2,U2,F2)),
      dabolish_all([dn_clause/3,d_clause/2,d_fact/1]),
      dabolish(herbrand_base/1),
      state_triple_to_state_pair([T2,U2,F2],State_Pair2) ) ),
   !.

sd_operator(Program,State_Pair1,State_Pair2) :-
   measure_hidden( 1, (
      dnlp_to_dn_clauses(Program),
      sd_operator_sp(State_Pair1,State_Pair2),
      dabolish_all([dn_clause/3,d_clause/2,d_fact/1]),
      dabolish(herbrand_base/1) ) ),
   !.


dnlp_logical_implication(Program,G_State1,G_State2) :-
   dnlp_to_edlp(Program,Program2),
   state_to_tree(G_State1,G_Tree1),
   tree_edlp_logical_implication(Program2,G_Tree1,G_Tree2),
   tree_to_state(G_Tree2,G_State2),
   dportray(dhs,G_State2),
   !.

tree_edlp_logical_implication(Program,G_Tree1,G_Tree4) :-
   tree_tps_fixpoint(Program,G_Tree1,G_Tree2),
   tree_binary_resolve_fixpoint(G_Tree2,G_Tree3),
   ( ( \+ tree_subsumes_tree(G_Tree1,G_Tree3),
          tree_edlp_logical_implication(Program,G_Tree3,G_Tree4) );
     G_Tree3 = G_Tree4 ).


sed_test(Program,State_Pair1,State_Pair4) :-
   sd_operator(Program,State_Pair1,State_Pair2),
   dportray(sp,State_Pair2),
%  state_pair_inconsistent_test(State_Pair2),
%  dnlp_and_state_pair_inconsistent_test(Program,State_Pair2),
   se_operator(Program,State_Pair2,State_Pair3),
   dportray(sp,State_Pair3),
%  state_pair_inconsistent_test(State_Pair3),
%  dnlp_and_state_pair_inconsistent_test(Program,State_Pair3),
   ( ( \+ State_Pair1 = State_Pair3,
          sed_test(Program,State_Pair3,State_Pair4) );
     State_Pair4 = State_Pair3 ).


/*** implementation ***********************************************/


/* gdwfs <-
      fixpoint computation for the disjunctive Sed-operator */


gdwfs(File) :-
   dconsult(File),
   ( ( current_num(sd_mode,1), nl,
     basic_sed_fixpoint(_) )
   ; ( current_num(sd_mode,2), nl,
     gdwfs ) ).

gdwfs :-
   measure_hidden( 1, ( 
      dwfs_initialize,
      sed_fixpoint,
      state_triple_to_state_pair ) ), 
   nl, lsp.


sed_fixpoint :-
   announcement(10,'sed_fixpoint'),
   repeat,
      state_triple(T_S,U_S,F_S),
      sed_single_iteration,
   equivalent_state_triples(T_S,U_S,F_S).

sed_single_iteration :-   
   announcement(11,'--'),
   sd_operator,
   se_operator.


se_operator :-
   announcement(10,'se_operator'),
   disjunctive_transformation,
   special_initialize,
   tps_fixpoint, nl, add_tsed_to_state_triple,
   reduce_state_triple,
   msp_to_gcwa,  add_gcwa_to_state_triple,
   msp_to_egcwa, add_egcwa_to_state_triple,
   reduce_state_triple,
   !.

add_tsed_to_state_triple :-
   announcement(12,'adding minimal model state'),
   retract(state_triple(T_S,U_S,F_S)),
   collect_arguments(d_fact,Facts_1),
   list_to_ord_set(Facts_1,Facts_2),
   ord_union(T_S,Facts_2,Facts_3),
   state_can(Facts_3,New_T_S),
   assert(state_triple(New_T_S,U_S,F_S)),
   lst.

add_egcwa_to_state_triple :-
   announcement(12,'adding egcwa'),
   retract(egcwa(Egcwa)),
   add_false_to_state_triple(Egcwa).

add_gcwa_to_state_triple :-
   announcement(12,'adding gcwa'),
   retract(gcwa(Gcwa)),
   add_false_to_state_triple(Gcwa).

add_false_to_state_triple(False) :-
   retract(state_triple(T_S,U_S,F_S)),
   ord_union(F_S,False,New_F_S),
   ord_subtract(U_S,False,New_U_S),
   state_can(New_F_S,Purified_New_F_S),
   assert(state_triple(T_S,New_U_S,Purified_New_F_S)),
   lst.


/* dwfs <-
      fixpoint computation for the disjunctive sd-operator */

dwfs(File) :-
   dconsult(File),
   dwfs.

dwfs :-
   measure_hidden( 1, ( 
      dwfs_initialize,
      sd_fixpoint,
      state_triple_to_state_pair ) ),
   nl, lsp.


sd_fixpoint :-
   announcement('sd_fixpoint'),
   repeat,
      state_triple(T_S,U_S,F_S),
      sd_operator,
   equivalent_state_triples(T_S,U_S,F_S).


gdwfs_iteration(File) :-
   dconsult(File,Program),
   sed_fixpoint_iteration(Program,[[],[]],_).

sed_fixpoint_iteration(Program,State_Pair1,State_Pair2) :-
   measure_hidden( 1, (
      dnlp_to_dn_clauses(Program),
      assert_dn_clause_to_d_fact_d_clause,
      assert(delta_d_fact(dummy)),
      state_pair_to_state_triple(State_Pair1,[T1,U1,F1]),
      retractall(state_triple(_,_,_)),
      assert(state_triple(T1,U1,F1)),
      announcement(10,'sed_fixpoint'),
      repeat,
         state_triple(T_S,U_S,F_S),
         sd_operator,
         state_triple(T_d,U_d,F_d), dportray(user,stsp,[T_d,U_d,F_d]),
         se_operator,
         state_triple(T_e,U_e,F_e), dportray(user,stsp,[T_e,U_e,F_e]),
      equivalent_state_triples(T_S,U_S,F_S),
      retract(state_triple(T2,U2,F2)),
      dabolish_all([dn_clause/3,d_clause/2,d_fact/1]),
      state_triple_to_state_pair([T2,U2,F2],State_Pair2),
      dabolish(herbrand_base/1) ) ),
   !.


se_fixpoint(Program,State_Pair1,State_Pair2) :-
   se_operator(Program,State_Pair1,State_Pair2).


/* sd_operator_sp(State_Pair_1,State_Pair_2) <-
      the Sd-operator applied to State_Pair_1 yields State_Pair_2. */

sd_operator_sp(State_Pair_1,State_Pair_2) :-
   state_pair_to_state_triple(State_Pair_1,State_Triple_1),
   sd_operator_st(State_Triple_1,State_Triple_2),
   state_triple_to_state_pair(State_Triple_2,State_Pair_2).


/* sd_operator_st(State_Triple_1,State_Triple_2) <-
      the Sd-operator applied to State_Triple_1 yields State_Triple_2. */

sd_operator_st([T_1,U_1,F_1],[T_2,U_2,F_2]) :-
   initialize_tps_fixpoint,
   abolish(state_triple/3),
   assert(state_triple(T_1,U_1,F_1)),
   assert_dn_clause_to_d_fact_d_clause,
   assert(delta_d_fact(dummy)),
   sd_operator,
   retract(state_triple(T_2,U_2,F_2)).


/* sd_operator <-
      fixpoint computation 
      for the disjunctive tsd- and fsd-operator */

sd_operator :-
   announcement(10,'sd_operator'),
   tsd_omega, 
   fsd_omega,
   update_state_triple,
   lst,
   !.

tsd_omega :-
   announcement('tsd_omega'),
   state_triple(T_S,_,_),
   assign_relation(delta_d_fact,T_S), assign_relation(d_fact,T_S),
   repeat,    
      tsd_single_iteration(N),
   N == 0,
   set_num(single_iteration,0),
   collect_arguments(d_fact,T_omega_List),
   list_to_ord_set(T_omega_List,T_omega),
   assert(tsd_omega_facts(T_omega)),
   assign_relation(delta_d_fact,[]), assign_relation(d_fact,[]),
   !.

fsd_omega :- 
   announcement('usd_omega'),
   state_triple(_,U_S,_), 
   ( ( var(U_S), !, U = [] )
   ; U = U_S ),
   assign_relation(delta_d_fact,U), 
   assign_relation(u_fact,U),
   assign_relation(d_fact,[]),
   repeat,     
      fsd_single_iteration(N), 
   N == 0, 
   set_num(single_iteration,0), 
   collect_arguments(d_fact,U_omega_List),
   list_to_ord_set(U_omega_List,U_omega),
   assert(fsd_omega_facts(U_omega)),
   assign_relation(delta_d_fact,[]), assign_relation(d_fact,[]),
   assign_relation(u_fact,[]),
   !. 


tsd_single_iteration(N) :-
   get_num(single_iteration,I),
   write('tsd_iteration_'), write(I), writeln(':'),
   tsd_single_iteration(N,_),
   swi_listing(current_state_triple/3),
   !.

fsd_single_iteration(N) :-
   get_num(single_iteration,I),
   write('usd_iteration_'), write(I), writeln(':'),
   fsd_single_iteration(N,_),
   swi_listing(current_state_triple/3),
   !.
 

/* tsd_single_iteration(N,I),
   fsd_single_iteration(N,I) <-
      a single execution of the disjunctive normal consequence operators,
      yields N new d_facts, deletes I old subsumed facts */

tsd_single_iteration(N,I) :-
   start_timer(single_iteration),
   tsd_join(D_Facts,M),
   state_prune(D_Facts,Pruned_Facts,K),
   compute_delta_facts(Pruned_Facts,Delta_Facts,J),
   state_can(Delta_Facts,Purified_Delta_Facts,N),
   purify_d_facts(Purified_Delta_Facts,I),
   update_d_facts(Purified_Delta_Facts),
   pp_numbers([M,K,J,N,I]),
   write('time         '), ttyflush,
   stop_timer(single_iteration).
   
fsd_single_iteration(N,0) :-
   start_timer(single_iteration),
   fsd_join(N_Facts,M),
   state_prune(N_Facts,Pruned_Facts,K),
   compute_delta_facts(Pruned_Facts,Delta_N_Facts,N),
   update_d_facts(Delta_N_Facts),
   pp_numbers([M,K,N,N,0]),
   write('time         '), ttyflush,
   stop_timer(single_iteration).


/* dwfs_initialize <-
      initialization for dwfs_omega. */

dwfs_initialize :-
   initialize_tps_fixpoint,
   init_state_triple([],_,[]),
   assert_dn_clause_to_d_fact_d_clause,
   assert(delta_d_fact(dummy)).

assert_dn_clause_to_d_fact_d_clause :-
   assert(':-'(d_fact(Fact),dn_clause(Fact,[],[]))),
   assert(':-'(d_clause(Head,Body),dn_clause(Head,Body,[]))).


tsd_join(Facts,M) :-
   write('tsd join     '), ttyflush,
   start_timer(tsd_join),
   state_triple(T_S,U_S,F_S),
   assert(d_fact(dummy)), assert(delta_d_fact(dummy)),
   findall( Pruned_Fact,
      ( dn_clause(Head,Body1,Body2),
        delta_resolvable(Body1,Clause),
        state_triple_implies_conj_neg(Body2,T_S,U_S,F_S),
        head_ord_union(Head,Clause,Fact),
        remove_duplicates(Fact,Pruned_Fact) ),
      Facts ),
   retract(d_fact(dummy)), retract(delta_d_fact(dummy)),
   length(Facts,M),
   stop_timer(tsd_join).
 
fsd_join(Facts,M) :-
   write('usd join     '), ttyflush,
   start_timer(fsd_join),
   state_triple(T_S,U_S,F_S),
   assert(u_fact(dummy)), assert(d_fact(dummy)), 
   assert(delta_d_fact(dummy)),
   findall( [Atom],
      ( dn_clause(Head,Body1,Body2),
        us_delta_resolvable(Body1),
        \+ state_subsumes(F_S,[Body1]),
        \+ state_subsumes(T_S,[Body2]),
        member(Atom,Head),
        unknown_wrt_state_triple([Atom],T_S,F_S,U_S) ),
      Facts ),
   retract(u_fact(dummy)), retract(d_fact(dummy)), 
   retract(delta_d_fact(dummy)),
   length(Facts,M),
   stop_timer(fsd_join).

unknown_wrt_state_triple([Atom],T_S,F_S,U_S) :-
   \+ member([Atom],T_S),
   \+ member([Atom],F_S),
   save_member([Atom],U_S).

save_member(_,Set) :-
   var(Set),
   !.
save_member(Element,Set) :-
   member(Element,Set).
 

swi_listing(current_state_triple/3) :-
   clause(current_state_triple(_,_,_),_),
   !,
   listing(current_state_triple/3).
swi_listing(current_state_triple/3) :-
   !.
swi_listing(X) :-
   listing(X), 
   !.
swi_listing(_).


init_state_triple(T,U,F) :-
   abolish(state_triple/3),
   assert(state_triple(T,U,F)).

init_current_state_triple :-
   state_triple(T_S,_,F_S),
   assert(current_state_triple(T_S,[],F_S)).

 
update_state_triple :-
   retract(state_triple(T_S,U_S,F_S)),
   retract(tsd_omega_facts(Current_T_S)),
   union_d_facts(T_S,Current_T_S,New_T_S),
   retract(fsd_omega_facts(Current_U_S)),
   us_intersection(U_S,Current_U_S,New_U_S),
   assert(state_triple(New_T_S,New_U_S,F_S)).

union_d_facts(Facts_1,Facts_2,Facts_6) :-
   ord_union(Facts_1,Facts_2,Facts_3),
   list_to_ord_set(Facts_3,Facts_4),
   state_can(Facts_4,Facts_5,_),
   list_to_ord_set(Facts_5,Facts_6).
 
update_current_state_triple(Delta_N_Facts) :-
   retract(current_state_triple(T_S,U_S,F_S)),
   ord_union(Delta_N_Facts,U_S,New_U_S),
   retract(delta_n_facts(_)),
   assert(delta_n_facts(Delta_N_Facts)),
   assert(current_state_triple(T_S,New_U_S,F_S)).

% update_delta_n_facts(Delta_N_Facts) :-
%    retract(delta_n_facts(_)),
%    assert(delta_n_facts(Delta_N_Facts)).


equivalent_state_triples(T_S,U_S,F_S) :-
   state_triple(New_T_S,New_U_S,New_F_S),
   equivalent_state_triples([T_S,U_S,F_S],[New_T_S,New_U_S,New_F_S]).
 
equivalent_state_triples([T_S,U_S,F_S],[New_T_S,New_U_S,New_F_S]) :-
   T_S == New_T_S,
   U_S == New_U_S,
   F_S == New_F_S.


reduce_state_triple :-
   announcement('reduce state triple'),
   start_timer(reduce_state_triple),
   retract(state_triple(T_S,U_S,F_S)),
   reduce_d_facts(T_S,New_T_S,T_S,U_S,F_S),
   reduce_n_facts(F_S,New_F_S,T_S,U_S,F_S),
   ord_subtract(U_S,New_T_S,New_U_S),
   assert(state_triple(New_T_S,New_U_S,New_F_S)),
   write('             '), ttyflush,
   stop_timer(reduce_state_triple),
   lst.


/* reduce_d_facts(Facts,New_Facts,T_S,U_S,F_S) <-
      computes the canonical set New_Facts by removing all false 
      Atoms from the facts in Facts. */

reduce_d_facts(Facts,New_Facts,T_S,U_S,F_S) :-
   compute_reduced_d_facts(Facts,Reduced_Facts_1,T_S,U_S,F_S),
   list_to_ord_set(Reduced_Facts_1,Reduced_Facts_2),
   state_can(Reduced_Facts_2,Reduced_Facts,_),
   state_subtract(Facts,Reduced_Facts,Facts_1),
   ord_union(Reduced_Facts,Facts_1,New_Facts).

compute_reduced_d_facts([C1|Cs1],[C2|Cs2],T_S,U_S,F_S) :-
   reduce_d_fact(C1,C2,T_S,U_S,F_S),
   C1 \== C2,
   !,
   compute_reduced_d_facts(Cs1,Cs2,T_S,U_S,F_S).
compute_reduced_d_facts([_|Cs1],Cs2,T_S,U_S,F_S) :-
   compute_reduced_d_facts(Cs1,Cs2,T_S,U_S,F_S).
compute_reduced_d_facts([],[],_,_,_).


/* reduce_d_fact(Fact,Reduced_Fact,T_S,U_S,F_S) <-
      computes the fact Reduced_Fact by removing all all false Atoms 
      from the fact Fact. */

reduce_d_fact([Atom|Atoms],Non_False_Atoms,T_S,U_S,F_S) :-
   state_triple_implies_conj_neg([Atom],T_S,U_S,F_S),
   !,
   reduce_d_fact(Atoms,Non_False_Atoms,T_S,U_S,F_S).
reduce_d_fact([Atom|Atoms],[Atom|Non_False_Atoms],T_S,U_S,F_S) :-
   reduce_d_fact(Atoms,Non_False_Atoms,T_S,U_S,F_S).
reduce_d_fact([],[],_,_,_).


/* reduce_n_facts_by_state_triple(Facts,New_Facts) <-
      computes the canonical set New_Facts by replacing n_facts
      containing false atoms to the false atoms. */
      
reduce_n_facts_by_state_triple(Facts,New_Facts) :-
   state_triple(T_S,U_S,F_S),
   reduce_n_facts(Facts,New_Facts,T_S,U_S,F_S).


/* reduce_n_facts(Facts,New_Facts,T_S,U_S,F_S) <-
      computes the canonical set New_Facts by replacing n_facts
      containing false atoms to the false atoms. */

reduce_n_facts(Facts,New_Facts,T_S,U_S,F_S) :-
   ord_union(Facts,Atoms),
   compute_false_atoms(Atoms,False_Atoms,T_S,U_S,F_S),
   list_of_elements_to_relation(False_Atoms,False_Facts),
   state_subtract(Facts,False_Facts,Facts_1),
   ord_union(False_Facts,Facts_1,New_Facts).


/* compute_false_atoms(Atoms,False_Atoms,T_S,U_S,F_S) <-
      False_Atoms contains all the false atoms in Atoms. */

compute_false_atoms([Atom|Atoms],[Atom|False_Atoms],T_S,U_S,F_S) :-
   state_triple_implies_conj_neg([Atom],T_S,U_S,F_S),    
   !,
   compute_false_atoms(Atoms,False_Atoms,T_S,U_S,F_S).
compute_false_atoms([_|Atoms],False_Atoms,T_S,U_S,F_S) :-
   compute_false_atoms(Atoms,False_Atoms,T_S,U_S,F_S).
compute_false_atoms([],[],_,_,_).   
 

/* state_triple_implies_conj_neg([A1,...,An],T_S,U_S,F_S) <-
      the state triple <T_S,U_S,F_S> implies the
      conjunction of the negations of the atoms A1,...,An. */
  
state_triple_implies_conj_neg([Atom|Atoms],T_S,U_S,F_S) :-
   member([Atom],F_S),
   !,
   state_triple_implies_conj_neg(Atoms,T_S,U_S,F_S).
state_triple_implies_conj_neg([Atom|Atoms],T_S,U_S,F_S) :-
   \+ member([Atom],T_S),
   \+ save_member([Atom],U_S),
   !,
   state_triple_implies_conj_neg(Atoms,T_S,U_S,F_S).
state_triple_implies_conj_neg([],_,_,_).

              
/* state_triple_subsumes_n_fact(Clause) <-
      the currently asserted state triple <T_S,U_S,F_S>
      subsumes the n_fact Clause. */

state_triple_subsumes_n_fact(Clause) :-
   state_triple(T_S,U_S,F_S),
   state_triple_implies_n_fact(Clause,T_S,U_S,F_S).


/* state_triple_implies_n_fact([A1,...,An],T_S,U_S,F_S) <-
      the state triple <T_S,U_S,F_S> implies the
      disjunction of the negations of the atoms A1,...,An. */

state_triple_implies_n_fact(Clause,_,_,F_S) :-
   state_subsumes(F_S,[Clause]),
   !.
state_triple_implies_n_fact(Clause,T_S,U_S,_) :-
   member(Atom,Clause),
   \+ member([Atom],T_S),
   \+ save_member([Atom],U_S).


/* state_triple_implies_n_fact_gen([A1,...,An],T_S,U_S,F_S) <-
      generates the disjunctions of the negations of atoms A1,...,An
      implied by the state triple <T_S,U_S,F_S>. */

state_triple_implies_n_fact_gen(Clause,_,_,F_S) :-
   member(Clause,F_S).
state_triple_implies_n_fact_gen([Atom],T_S,U_S,_) :-
   herbrand_base(HB), member(Atom,HB),
   \+ member([Atom],T_S),
   \+ save_member([Atom],U_S).


/* compute_delta_n_facts(N_Facts,Delta_N_Facts,N) <-
      Delta_N_Facts becomes the set difference with subsumption of the 
      set N_Facts and the set of actually asserted n_facts,
      the computed facts are asserted as N new n_facts. */

compute_delta_n_facts([],[],0) :-
   !.
compute_delta_n_facts(N_Facts,Delta_N_Facts,N1) :-
   assert(n_fact(dummy)),
   compute_delta_n_facts_loop(N_Facts,Delta_N_Facts,N1),
   retract(n_fact(dummy)).

compute_delta_n_facts_loop([N_Fact|N_Facts],Delta_N_Facts,N1) :-
   n_fact(N_Fact1), ord_subset(N_Fact,N_Fact1),
   !,
   compute_delta_n_facts_loop(N_Facts,Delta_N_Facts,N1).
compute_delta_n_facts_loop([N_Fact|N_Facts],[N_Fact|Delta_N_Facts],N2) :-
   assert(n_fact(N_Fact)),
   compute_delta_n_facts_loop(N_Facts,Delta_N_Facts,N1),
   N2 is N1 + 1.
compute_delta_n_facts_loop([],[],0).
 

/* us_intersection(U_S,U_Facts,New_U_S) <- 
      determines the set intersection New_U_S of U_S and U_Facts. */

us_intersection(U_S,U_Facts,U_Facts) :-
   var(U_S),
   !.
us_intersection(U_S,U_Facts,New_U_S) :-
   ord_intersection(U_Facts,U_S,New_U_S).


n_facts_from_s_t_to_d_facts :-
   state_triple(T_S,U_S,F_S),
   member(N_Fact,F_S),
   state_triple_implies_n_fact(N_Fact,T_S,U_S,F_S),
   list_of_elements_to_relation(N_Fact,D_Fact),
   assert(d_fact(D_Fact)),
   fail.
n_facts_from_s_t_to_d_facts.
   
prune_by_atomary_negations(Clauses1,Clauses2) :-
   compute_atomary_negations_of_state_triple(Atoms),
   prune_by_atomary_negations_loop(Atoms,Clauses1,Clauses2).
   
prune_by_atomary_negations_loop(Atoms,[C1|Cs1],[C2|Cs2]) :-
   ord_subtract(C1,Atoms,C2),
   prune_by_atomary_negations_loop(Atoms,Cs1,Cs2).
   
compute_atomary_negations_of_state_triple(Atoms) :-
   state_triple(_,U_S,F_S),
   findall( Atom,
      state_triple_implies_conj_neg([Atom],U_S,F_S),
      Atoms ).

state_pair_to_state_triple([[],[]],[[],_,[]]) :-
   !.
state_pair_to_state_triple([T_S,F_S],[T_S,U_S,New_F_S]) :-
   herbrand_base(HB),
   !,
   list_of_elements_to_relation(HB,X_1),
   ord_subtract(X_1,T_S,X_2),
   ord_subtract(X_2,F_S,U_S),
   ord_subtract(HB,U_S,X_3),
   ord_subtract(F_S,X_3,New_F_S).
state_pair_to_state_triple(State_Pair,State_Triple) :-
   compute_herbrand_base,
   state_pair_to_state_triple(State_Pair,State_Triple).

state_triple_to_state_pair([T_S,U_S,F_S],[T_S,New_F_S]) :-
   herbrand_base(_),
   !,
   findall( N_Fact,
      state_triple_implies_n_fact_gen(N_Fact,T_S,U_S,F_S),
      N_Facts_1 ),
   list_to_ord_set(N_Facts_1,N_Facts_2),
   state_can(N_Facts_2,N_Facts_3,_),
   list_to_ord_set(N_Facts_3,New_F_S).
state_triple_to_state_pair(State_Triple,State_Pair) :-
   compute_herbrand_base,
   state_triple_to_state_pair(State_Triple,State_Pair).
 
state_triple_to_state_pair :-
   abolish(state_pair/2),
   state_triple(T_S,U_S,F_S),
   state_triple_to_state_pair([T_S,U_S,F_S],[T_S,New_F_S]),
   assert(state_pair(T_S,New_F_S)),
   lsp.


/******************************************************************/

