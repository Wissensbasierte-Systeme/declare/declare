

/******************************************************************/
/***     Module :  Approximating the Stable Model Semantics     ***/
/******************************************************************/


/*** interface ****************************************************/


/* dwfs_seipel_operator(Filename) <-
      for the DNLP in file Filename a subset of the stable model 
      state is computed and printed. */ 

sdwfs(Filename) :-
   dwfs_seipel_operator(Filename).

dwfs_seipel_operator(Filename) :-
   measure_hidden( 1, (
      dconsult(Filename,Program),
      dwfs_seipel_operator(Program,State) ) ),
   pp_ghs(State), nl.


/* dwfs_seipel_operator(Program,State) <-
      given a DNLP Program, this operator yields a subset State
      of the stable model state of Program. */

sdwfs_operator(Program,State) :-
   dwfs_seipel_operator(Program,State).

dwfs_seipel_operator(Program,State) :-
   gnaf_seipels_fixpoint(Program,[],State).
 
 
/* gnaf_seipels_fixpoint(Program,State1,State2) <-
      given a DNLP Program,
      the omega-power of the gnaf_seipel-operator applied to the
      g-state State1 yields the g-state State2. */
 
gnaf_seipels_fixpoint(Program,State1,State2) :-
   state_to_tree(State1,Tree1),
   gnaf_seipels_fixpoint_tree(Program,Tree1,Tree2),
   tree_to_state(Tree2,State2).

 
/* gnaf_seipels_fixpoint_tree(Program,Tree1,Tree2) <-
      given a DNLP Program,
      the omega-power of the gnaf_seipel-operator applied to the
      g-state state(Tree1) yields the g-state state(Tree2). */
 
gnaf_seipels_fixpoint_tree(Program,Tree1,Tree2) :-
   dnlp_to_dn_clauses(Program),
   gnaf_seipels_fixpoint_tree(Tree1,Tree2),
   dabolish_all([dn_clause/3,d_clause/2,d_fact/1]),
   !.

gnaf_seipels_fixpoint_tree(Tree1,Tree3) :-
   announcement(11,'--'),
   gnaf_seipels_operator_tree(Tree1,Tree2),
   ( ( \+ tree_subsumes_tree(Tree1,Tree2),
       gnaf_seipels_fixpoint_tree(Tree2,Tree3) );
     Tree3 = Tree2 ).

gnaf_seipels_operator_tree(Tree1,Tree7) :-
   announcement(10,'barals_operator'), dspy(pp_tree(Tree1)),
   tree_ts_fixpoint(Tree1,Tree2), dspy(pp_tree(Tree2)),
   tree_merge(Tree1,Tree2,Tree3), dspy(pp_tree(Tree3)),
   tree_binary_resolve_fixpoint(Tree3,Tree4), dspy(pp_tree(Tree4)),
   tree_merge(Tree3,Tree4,Tree5), dspy(pp_tree(Tree5)),
   gnaf_fs_operator_tree(Tree5,Tree6), dspy(pp_tree(Tree6)),
   tree_merge(Tree5,Tree6,Tree7).


/* gnaf_fs_operator_tree(G_Tree,N_Tree) <-
      the generalized negation-as-failure operator F_S for the 
      g-state S = state(G_Tree) applied to the empty negative 
      Herbrand state yields an n-state N_State and 
      N_Tree = tree(N_State). */

gnaf_fs_operator_tree(G_Tree,N_Tree) :-
   program_to_herbrand_base(Herbrand_Base),
   assert(herbrand_base(Herbrand_Base)),
%  negate_atoms(Herbrand_Base,Clause),
%  gnaf_us_operator_tree(G_Tree,[Clause],Filter_N_Tree),
%  dualize_max_clause_tree(Filter_N_Tree,N_Tree),
   gnaf_fs_operator_tree(G_Tree,[],N_Tree).
%  gnaf_fs_operator_tree_diagnosis(G_Tree,N_Tree).


/*** implementation ***********************************************/


/* gnaf_fs_operator_tree_diagnosis(G_Tree,N_Tree) <-
      shows all clauses represented by the clause trees G_Tree, N_Tree, 
      which are not true in all stable models given by the asserted atom 
      stable_models(Stable_Models). */

gnaf_fs_operator_tree_diagnosis(G_Tree,N_Tree) :-
   switch(File,user),
%  dportray(tree,G_Tree), dportray(tree,N_Tree),
   tree_to_state(G_Tree,G_State), tree_to_state(N_Tree,N_State),
   dportray(ghs,G_State), dportray(ghs,N_State),
   stable_models(Stable_Models),
   state_models_subtract(G_State,Stable_Models,G_Violations),
   state_models_subtract(N_State,Stable_Models,N_Violations),
   writeln(user,'*** Violations in G_Tree ***'),
   dportray(ghs,G_Violations),
   writeln(user,'*** Violations in N_Tree ***'),
   dportray(ghs,N_Violations), wait,
   switch(File).


/* test_dwfs_seipel_operator(File) <-
      tests if the dwfs_seipel_operator applied to the
      DNLP Program in the file File is consistent with the
      stable model semantics. */

test_dwfs_seipel_operator(pb1) :-
   !,
   stable_models_of_example_pb1(Stable_Models),
   assert(stable_models(Stable_Models)),
   dwfs_seipel_operator(pb1),
   retract(stable_models(Stable_Models)).
test_dwfs_seipel_operator(File) :-
   dconsult(File,Program),
   stable_models_operator(Program,Stable_Models),
   assert(stable_models(Stable_Models)),
   dwfs_seipel_operator(File),
   retract(stable_models(Stable_Models)).


/* stable_models_of_example_pb1(Stable_Models) <-
      Stable_Models are the stable models of example 'pb1'. */

stable_models_of_example_pb1([
   [b(d,e),p(a,b),p(a,d),p(b,d),p(c,d),p(d,e),p(f,a),p(f,b),p(f,d)],
   [b(d,e),p(a,c),p(a,d),p(b,d),p(c,d),p(d,e),p(f,a),p(f,c),p(f,d)],
   [b(f,a),p(a,b),p(a,d),p(a,e),p(b,d),p(b,e),p(c,d),p(c,e),p(d,e),p(f,a)],
   [b(f,a),p(a,c),p(a,d),p(a,e),p(b,d),p(b,e),p(c,d),p(c,e),p(d,e),p(f,a)]]).


/******************************************************************/

