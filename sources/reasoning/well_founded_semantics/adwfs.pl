

/******************************************************************/
/***                                                            ***/
/***             DisLog: Alternating Disjunctive WFS            ***/
/***                                                            ***/
/******************************************************************/


adwfss(Database_Id) :-
   dconsult(Database_Id,Program),
   adwfs_operator_s(Program,State),
   writeln(user,' '),
   dportray(user,ghs,State).

adwfs(Database_Id) :-
   dconsult(Database_Id,Program),
   adwfs_operator(Program,Partial_Coin),
   writeln(user,' '), !,
   writeln(user,Partial_Coin).

adwfs_operator_s(Program,General_State) :-
   adwfs_operator(Program,Partial_Coin),
   split_partial_coin(Partial_Coin,Coin_1,Coin_2),
   state_operator_disjunctive(Coin_1,Positive_State),
   state_operator_negative_i(Program,Coin_2,Negative_State),
   ord_union(Positive_State,Negative_State,General_State).
   
split_partial_coin([I1-I2|Is],[I1|Is1],[I2|Is2]) :-
   split_partial_coin(Is,Is1,Is2).
split_partial_coin([],[],[]).

adwfs_operator(Program,Coin) :-
   gamma_fixpoint(Program,Coin).

gamma_fixpoint(Program,Coin) :-
   dnlp_to_herbrand_base(Program,HB),
   gamma_fixpoint(Program,[[]-HB],Coin).
 
gamma_fixpoint(Program,Coin_1,Coin_3) :-
   gamma_iteration_up_down(Program,Coin_1,Coin_2),
   ( ( Coin_1 = Coin_2,
       Coin_3 = Coin_2 )
   ; gamma_fixpoint(Program,Coin_2,Coin_3) ).

gamma_iteration_up_down(Program,Coin_1,Coin_3) :-
   gamma_partial_coin(up,Program,Coin_1,Coin_2),
   gamma_partial_coin(down,Program,Coin_2,Coin_3),
   writeln(user,Coin_2),
   writeln(user,Coin_3).


/* gamma_up_partial_coin(Program,Coin_1,Coin_2) <-
      */

gamma_partial_coin(Mode,Program,P_Coin_1,P_Coin_2) :-
   gamma_partial_coin(Mode,Program,P_Coin_1,[],Partial_Coins),
   ord_union(Partial_Coins,P_Coin_2).

gamma_partial_coin(Mode,Program,[I|Is],Sofar,Partial_Coins) :-
   gamma_partial_int(Mode,Program,I,Partial_Coin_1),
   gamma_partial_coin(Mode,Program,Is,
      [Partial_Coin_1|Sofar],Partial_Coins).
gamma_partial_coin(_,_,[],Partial_Coins,Partial_Coins).

gamma_partial_int(up,Program,I1-I2,Partial_Coin) :-
   gl_transformation(Program,I1,Program2),
   partial_interpretation_to_constraints(Program2,I1-I2,Constraints),
   ord_union(Program2,Constraints,Program3),
   minimal_models_operator(Program3,Coin),
   coin_plus_lower_bound(I1,Coin,Partial_Coin).
gamma_partial_int(down,Program,I1-I2,Partial_Coin) :-
   gl_transformation(Program,I2,Program2),
   partial_interpretation_to_constraints(Program2,I1-I2,Constraints),
   ord_union(Program2,Constraints,Program3),
   minimal_models_operator(Program3,Coin),
   coin_plus_upper_bound(Coin,I2,Partial_Coin).
   
coin_plus_lower_bound(I1,[I2|Is2],[I1-I2|Partial_Coin]) :-
   coin_plus_lower_bound(I1,Is2,Partial_Coin).
coin_plus_lower_bound(_,[],[]).

coin_plus_upper_bound([I1|Is1],I2,[I1-I2|Partial_Coin]) :-
   coin_plus_upper_bound(Is1,I2,Partial_Coin).
coin_plus_upper_bound([],_,[]).


partial_interpretation_to_constraints(Program,I1-I2,Constraints) :-
   dnlp_to_herbrand_base(Program,HB),
   ord_subtract(HB,I2,I3),
   interpretation_to_atomary_denials(I3,Forbidden),
   list_of_elements_to_relation(I1,Required),
   ord_union(Required,Forbidden,Constraints).

interpretation_to_atomary_denials([A|As],[[]-[A]|Ds]) :-
   interpretation_to_atomary_denials(As,Ds).
interpretation_to_atomary_denials([],[]).


/******************************************************************/

