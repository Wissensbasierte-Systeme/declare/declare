

/******************************************************************/
/***                                                            ***/
/***             DisLog :  Baral's Semantics                    ***/
/***                                                            ***/
/******************************************************************/
 
 

/*** interface ****************************************************/


bdwfs(Filename) :-
   dconsult(Filename,Program),
   measure_hidden( 1, (
      announcement('barals_fixpoint'),
      bdwfs_operator(Program,State2) ) ),
   pp_ghs(State2), nl.


bdwfs_operator(Program,State) :-
   barals_fixpoint(Program,[],State).


/* barals_fixpoint(State1,State2) <-
      the omega-power of the Baral-operator applied to the 
      g-state State1 yields the g-state State2. */

sb_fixpoint(Program,State1,State2) :- 
   barals_fixpoint(Program,State1,State2).

barals_fixpoint(Program,State1,State2) :- 
   prepare_analysis,  
   hidden( 1, (
      dnlp_to_dn_clauses(Program),  
      barals_fixpoint(State1,State2),
      dabolish_all([dn_clause/3,d_clause/2,d_fact/1]) ) ),
   pp_ghs(State2), nl,
   perform_analysis,
   !.  

barals_fixpoint(State1,State2) :-
   state_to_tree(State1,Tree1),
   barals_fixpoint_tree(Tree1,Tree2),
   tree_to_state(Tree2,State2).


sb_fixpoint_tree(Program,Tree1,Tree2) :-
   barals_fixpoint_tree(Program,Tree1,Tree2).

barals_fixpoint_tree(Program,Tree1,Tree2) :-
   prepare_analysis,
   hidden( 1, (
      dnlp_to_dn_clauses(Program),
      barals_fixpoint_tree(Tree1,Tree2),
      dabolish_all([dn_clause/3,d_clause/2,d_fact/1]) ) ),
   nl,
   perform_analysis,
   !.

sb_operator(Program,State1,State2) :-
   state_to_tree(State1,Tree1),
   sb_operator_tree(Program,Tree1,Tree2),
   tree_to_state(Tree2,State2).

sb_operator_tree(Program,Tree1,Tree2) :- 
   prepare_analysis, 
   hidden( 1, (
      dnlp_to_dn_clauses(Program), 
      barals_operator_tree(Tree1,Tree2), 
      dabolish_all([dn_clause/3,d_clause/2,d_fact/1]) ) ),
   nl,  
   perform_analysis, 
   !. 


tsb_fixpoint(Program,State1,State2) :-
   state_to_tree(State1,Tree1),
   tsb_fixpoint_tree(Program,Tree1,Tree2),
   tree_to_state(Tree2,State2).

tsb_fixpoint_tree(Program,Tree1,Tree5) :-
   prepare_analysis, 
   hidden( 1, (
      dnlp_to_dn_clauses(Program), 
      tree_ts_fixpoint(Tree1,Tree2),
      tree_merge(Tree1,Tree2,Tree3),
      tree_binary_resolve_fixpoint(Tree3,Tree4),
      tree_merge(Tree3,Tree4,Tree5),
      dabolish_all([dn_clause/3,d_clause/2,d_fact/1]) ) ),
   nl,  
   perform_analysis, 
   !. 

fsb_fixpoint(Program,State1,State2) :-
   state_to_tree(State1,Tree1),
   fsb_fixpoint_tree(Program,Tree1,Tree2),
   tree_to_state(Tree2,State2).

fsb_fixpoint_tree(Program,Tree1,Tree2) :-
   prepare_analysis,
   hidden( 1, (
      dnlp_to_dn_clauses(Program),
      gnaf_fs_omega_tree(Tree1,Tree2),
      dabolish_all([dn_clause/3,d_clause/2,d_fact/1]) ) ),
   nl,
   perform_analysis,
   !.


/* barals_fixpoint_tree(Tree1,Tree3) <-
      the omega-power of the Baral-operator applied to the 
      g-state State1 = state(Tree1) yields the g-state State3,
      and Tree3 = tree(State3). */

barals_fixpoint_tree(Tree1,Tree3) :-
   announcement(11,'--'),
   barals_operator_tree(Tree1,Tree2),
   ( ( \+ tree_subsumes_tree(Tree1,Tree2),
       barals_fixpoint_tree(Tree2,Tree3) );
     Tree3 = Tree2 ).


barals_operator_tree(Tree1,Tree7) :-
   announcement(10,'barals_operator'), dspy(pp_tree(Tree1)),
%  tree_to_state_and_print_2(Tree1),
   tree_ts_fixpoint(Tree1,Tree2), dspy(pp_tree(Tree2)),
   tree_merge(Tree1,Tree2,Tree3), dspy(pp_tree(Tree3)),
%  tree_to_state_and_print(Tree3),
   tree_binary_resolve_fixpoint(Tree3,Tree4), dspy(pp_tree(Tree4)),
   tree_merge(Tree3,Tree4,Tree5), dspy(pp_tree(Tree5)),
%  tree_to_state_and_print(Tree5),
   gnaf_fs_omega_tree(Tree5,Tree6), dspy(pp_tree(Tree6)),
   tree_merge(Tree5,Tree6,Tree7).
%  tree_to_state_and_print(Tree7).

tree_to_state_and_print(Tree) :-
   tree_to_state(Tree,State),
   switch(File,user),
   nl, writeln('~~~~~~~~~~~~~~~~~~~'),
   pp_dhs(State),
   nl, writeln('~~~~~~~~~~~~~~~~~~~'),
   pp_tree(Tree),
   switch(File).

tree_to_state_and_print_2(Tree) :-
   tree_to_state(Tree,State),
   switch(File,user),
   nl, writeln('~~~~~~~~~~~~~~~~~~~'),
   pp_ghs(State), 
   nl, writeln('~~~~~~~~~~~~~~~~~~~'),
   switch(File).


/* tree_ts_fixpoint(Tree1,Tree2) <-
      the clause tree Tree2 is derived form the clause tree Tree1
      by general hyperresolution with the actual dn_clauses. */

tree_ts_fixpoint(Tree1,Tree3) :-
   retractall(d_clause(_,_)),
   dn_clauses_to_ed_clauses,
%  tree_to_state(Tree1,State1),
%  tps_fixpoint(State1,State2),
%  state_to_tree(State2,Tree2),
   tree_tps_fixpoint(Tree1,Tree2),
   tree_remove_tautologies(Tree2,Tree3),
   retractall(d_clause(_,_)).


/* binary_resolve_fixpoint(State1,State2) <-
      the g-state State2 is derived from the g-state State1 by
      repeated binary resolution of clauses in State1. */

binary_resolve_fixpoint(State1,State2) :-
   state_to_tree(State1,Tree1),
   tree_binary_resolve_fixpoint(Tree1,Tree2),
   tree_to_state(Tree2,State2).


/* tree_binary_resolve_fixpoint(Tree1,Tree2) <-
      the clause tree Tree2 is derived from the clause tree Tree1
      by repeated binary resolution of clauses in state(Tree1),
      this is achieved by general hyperresolution with rules
      d_clause([],[L,~L]) for all complementary literals L and ~L. */

tree_binary_resolve_fixpoint(Tree1,Tree3) :-
   retractall(d_clause(_,_)),
   tree_to_atoms(Tree1,Literals1),
   generate_contrapositive_d_clauses(Literals1),
%  tree_to_state(Tree1,State1),
%  tps_fixpoint(State1,State2),
%  state_to_tree(State2,Tree2),
   tree_tps_fixpoint(Tree1,Tree2),
   tree_remove_tautologies(Tree2,Tree3),
   retractall(d_clause([],_)).


/* generate_contrapositive_d_clauses(Literals) <-
      for each literal L in Literals a rule d_clause([],[L,~L]) 
      is asserted. */

generate_contrapositive_d_clauses([L|Ls]) :-
   negate_literal(L,N),
   list_to_ord_set([L,N],Body),
   assert(d_clause([],Body)),
   generate_contrapositive_d_clauses(Ls).
generate_contrapositive_d_clauses([]).


/******************************************************************/


