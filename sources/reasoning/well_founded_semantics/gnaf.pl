

/******************************************************************/
/***                                                            ***/
/***          DisLog :  Generalized Negation as Failure         ***/
/***                                                            ***/
/******************************************************************/
 

:- dynamic 
      supersumed_clause/1,
      support_pair/2, 
      delta_support_set/2, support_set/2,
      d_fact_tree_S/1, d_fact_tree_s/1, delta_d_fact_tree_s/1,
      global_extenders/1,
      actual_state/1, accumulated_state/1.


/*** interface ****************************************************/



/* maximal_unfounded_set(Program,G_State,N_State) <-
      the maximal unfounded set of the DNLP Program w.r.t. 
      the general Herbrand state G_State is given by 
      the negative Herbrand state N_State. */

maximal_unfounded_set(Program,G_State,N_State) :-
   dnlp_to_dn_clauses(Program),
   gnaf_fs_omega(G_State,N_State),
   dabolish_all([dn_clause/3,d_clause/2,d_fact/1]).

   
/* gnaf_fs_omega_tree(G_Tree,N_Tree) <-
      the operator F_S-omega for the g-state S = state(G_Tree) 
      applied to the negative Herbrand base NHB_P 
      yields an n-state N_State and N_Tree = tree(N_State). */

gnaf_fs_omega_tree(G_Tree,N_Tree) :-  
   announcement('gnaf_fs_omega_tree'),
   prepare_analysis,
   gnaf_us_omega_tree(G_Tree,[],Tree_s),
   dualize_max_clause_tree(Tree_s,N_Tree),
   debug_writeln(bdwfs,user,' '),
   debug_write(bdwfs,user,'  S" ='), gnaf_us_operator_tree_pp_tree(N_Tree),
   perform_analysis.
   
debug_writeln(bdwfs,user,_) :-
   !.
debug_write(bdwfs,user,_) :-
   !.


/* gnaf_fs_omega(G_State,N_State) <-  
      the operator F_S-omega for the g-state S = G_State 
      applied to the negative Herbrand base NHB_P 
      yields the n-state N_State. */

gnaf_fs_omega(G_State,N_State) :-  
   gnaf_fs_omega(G_State,_,N_State).


/* gnaf_fs_omega(G_State,N_State1,N_State2) <-  
      given a g-state S = G_State and a n-state N_State1,
      the operator F_S-omega applied to N_State1 yields the 
      n-state N_State2. */

gnaf_fs_fixpoint(Program,G_State,N_State) :-
   prepare_analysis,
   hidden( 1, dnlp_to_dn_clauses(Program) ),
   gnaf_fs_omega(G_State,N_State),
   dabolish_all([dn_clause/3,d_clause/2,d_fact/1]),
   perform_analysis,
   !.

gnaf_fs_omega(G_State,N_State1,N_State2) :-  
   hidden( 1, (
      prepare_analysis,
      announcement('gnaf_fs_omega'),
      state_to_tree(G_State,G_Tree),
      state_to_tree(N_State1,N_Tree),
      dualize_min_clause_tree(N_Tree,Filter_N_Tree),
      gnaf_us_omega_tree(G_Tree,Filter_N_Tree,Tree_s),
      dualize_max_clause_tree(Tree_s,Dual_Tree_s),
      tree_to_state(Dual_Tree_s,N_State2) ) ),
   nl, writeln('negative Herbrand state'),
   write('  N  ='), pp_dhs(N_State2), nl, nl,
   perform_analysis.


/* gnaf_us_omega_tree(G_State,Filter_N_Tree,Tree_s) <-  
      the operator U_S-omega for the g-state S = state(Tree1) 
      applied to the n-state N_State1 = state(Filter_N_Tree) 
      yields the n-state N_State2, and Tree_s = tree(N_State2). */

gnaf_us_omega_tree(G_Tree,Filter_N_Tree,Tree_s) :-
   announcement('gnaf_us_omega_tree'),
   assert(d_fact_tree_S(G_Tree)),
   assert(d_fact_tree_s([])),
   assert(delta_d_fact_tree_s(Filter_N_Tree)),
   repeat,
      gnaf_us_operator_tree, 
   retract(delta_d_fact_tree_s([])),
   retract(d_fact_tree_s(Tree_s)),
   retract(d_fact_tree_S(G_Tree)).

gnaf_us_omega_tree(G_Tree,Tree_s) :- 
   gnaf_us_omega_tree(G_Tree,[],Tree_s). 

gnaf_us_operator_tree :- 
   d_fact_tree_S(G_Tree),
   retract(d_fact_tree_s(Tree)),
   retract(delta_d_fact_tree_s(Delta_Tree)),
%  gnaf_us_operator_tree_pp_tree(G_Tree),
%  gnaf_us_operator_tree_pp_tree(Tree),
   gnaf_us_operator_tree(G_Tree,Delta_Tree,Derived_Tree), 
   tree_to_state(Derived_Tree,Derived_State),
   write('  D  ='), pp_dhs(Derived_State), nl,
   state_tree_subtract(Derived_State,G_Tree,New_Derived_State),  
   state_max_tree_subtract(New_Derived_State,Tree,New_Delta_State),  
   write('  D  ='), pp_dhs(New_Delta_State), nl,
   max_tree_clauses_insert(Tree,New_Tree,New_Delta_State),
   state_to_max_tree(New_Delta_State,New_Delta_Tree),
   assert(d_fact_tree_s(New_Tree)),
   assert(delta_d_fact_tree_s(New_Delta_Tree)),
   star_line,
   write('  D  ='), pp_dhs(New_Delta_State), nl,
   !.

gnaf_us_operator_tree_pp_tree(Tree) :-
   var(Tree), !,
   debug_writeln(bdwfs,user,' NHB').
gnaf_us_operator_tree_pp_tree(Tree) :-
   tree_to_state(Tree,State),
   pp_dhs(bdwfs,user,State), debug_writeln(bdwfs,user,' ').
   
pp_dhs(bdwfs,user,_) :-
   !.


/* gnaf_fs_operator(State_S,State1,State2) <-
      the F_S-operator for the g-state S = State_S applied to 
      the n-state State1 yields the n-state State2. */

gnaf_fs_operator(Program,State_S,State1,State2) :-
   dnlp_to_dn_clauses(Program),
   hidden( gnaf_fs_operator(State_S,State1,State2) ),
   dabolish_all([dn_clause/3,d_clause/2,d_fact/1]).

gnaf_fs_operator(State_S,State1,State2) :-
   announcement('gnaf_fs_operator'),
   state_to_tree(State_S,G_Tree),
   state_to_tree(State1,Tree1),
   gnaf_fs_operator_tree(G_Tree,Tree1,Tree2),
   tree_to_state(Tree2,State2).

gnaf_fs_operator_tree(G_Tree,Tree1,Tree2) :-
   dualize_min_clause_tree(Tree1,Filter_N_Tree1),
   gnaf_us_operator_tree(G_Tree,Filter_N_Tree1,Filter_N_Tree2),
   dualize_max_clause_tree(Filter_N_Tree2,Tree2).


/* gnaf_us_operator_tree(G_Tree,Filter_N_Tree1,Filter_N_Tree2) <-
      given a clause tree G_Tree for a g-state G_State 
      and a clause tree Filter_N_Tree1 for a n-state N_State1,
      a clause tree Filter_N_Tree2 for the n-state N_State2 
      derived by applying the us-operator to G_State and N_State1
      is returned. */

gnaf_us_operator_tree(G_Tree,Filter_N_Tree1,Filter_N_Tree2) :-
   announcement('gnaf_us_operator_tree'),
   debug_writeln(bdwfs,user,' '),
   debug_write(bdwfs,user,'  S  ='), gnaf_us_operator_tree_pp_tree(G_Tree),
   debug_write(bdwfs,user,'  s  ='), gnaf_us_operator_tree_pp_tree(Filter_N_Tree1),
   gnaf_us_operator_tree_to_state(G_Tree,Filter_N_Tree1,Filter_N_State2),
   state_to_max_tree(Filter_N_State2,Filter_N_Tree2),
   debug_write(bdwfs,user,'  s" ='), gnaf_us_operator_tree_pp_tree(Filter_N_Tree2),
   gnaf_us_operator_tree_protocol(G_Tree,Filter_N_Tree1,Filter_N_Tree2).
   
gnaf_us_operator_tree_protocol(G_Tree,Filter_N_Tree1,Filter_N_Tree2) :-
   switch(File,'results/gnaf_us_operator_prot'),
   star_line,
   tree_to_state(G_Tree,State),
   pp_tree(G_Tree), pp_ghs(State),
   tree_to_state(Filter_N_Tree1,State1),
   pp_tree(Filter_N_Tree1), pp_dhs(State1),
   tree_to_state(Filter_N_Tree2,State2),
   pp_tree(Filter_N_Tree2), pp_dhs(State2),
   switch(File).


/* gnaf_us_operator(G_Tree,Filter_N_State1,Filter_N_State2) <-
      given a g-state G_State and a n-state N_State1,
      the n-state N_State2 derived by applying the us-operator to 
      G_State and N_State1 is returned. */

gnaf_us_operator(G_Tree,Filter_N_State1,Filter_N_State2) :-
   announcement('gnaf_us_operator'),
   state_to_max_tree(Filter_N_State1,Filter_N_Tree1),
   gnaf_us_operator_tree_to_state(G_Tree,Filter_N_Tree1,Filter_N_State2).


/* gnaf_us_operator_tree_to_state(G_Tree,Filter_N_Tree1,Filter_N_State2) <-
      given a clause tree G_Tree for a g-state G_State 
      and a clause tree Filter_N_Tree1 for a n-state Filter_N_State1,
      the n-state Filter_N_State2 derived by applying the us-operator to 
      G_State and Filter_N_State1 is returned. */

gnaf_us_operator_tree_to_state(G_Tree,Filter_N_Tree1,Filter_N_State2) :-
   assert(d_fact_tree_S(G_Tree)),
   assert(delta_d_fact_tree_s(Filter_N_Tree1)),
   init_gnaf_us_operator,
   repeat,
      extend_actual_state,
   actual_state([]),
   accumulated_state(Filter_State1),
%  writeln(user,Filter_State1), 
   extend_accumulated_state_and_negate(Filter_State1,Filter_N_State2),
   post_process_gnaf_us_operator,
   retract(delta_d_fact_tree_s(Filter_N_Tree1)),
   retract(d_fact_tree_S(G_Tree)).


extend_accumulated_state_and_negate(State,N_State) :-
   retract(global_extenders(Atoms)),
   negate_atoms(Atoms,Negated_Atoms),
   negate_state(State,N_State_1),
   state_disjunction_gnaf([Negated_Atoms],N_State_1,N_State).

state_disjunction_gnaf([[]],[],[]) :-
   !.
state_disjunction_gnaf([C],[],[C]) :-
   !.
state_disjunction_gnaf([C],N_State_1,N_State) :-
   state_disjunction([C],N_State_1,N_State).


/* init_gnaf_us_operator <-
      Computes the non-empty gsn-sets for atoms in the Herbrand base
      w.r.t. the asserted clause trees Tree_S and Delta_Tree_s.

    - global_extenders(Atoms):
      the set Atoms of all atoms A whose gsn-set contains the 
      empty clause - represented by the pair []-[] - is asserted 
      as global_extenders(Atoms), since these atoms will occur in 
      all disjunctions constructed by the us_operator,
    - actual_state(Atoms): 
      the set Atoms of all the other atoms is asserted as 
      actual_state(Atoms), 
    - support_set([Atom],Gsn),
    - delta_support_set([Atom],Gsn):
      the gsn-sets Gsn for the atoms Atom in the actual state
      are asserted as support_set([Atom],Gsn) and as 
      delta_support_set([Atom],Gsn), 
      they will be used for constructing gsn-sets for 
      disjunctions. */

init_gnaf_us_operator :-
%  write(user,'init_gnaf_us '), ttyflush,
%  start_timer(init_gnaf_us_operator),
   d_fact_tree_S(Tree_S),
   delta_d_fact_tree_s(Delta_Tree_s),
   init_gnaf_us_operator(Tree_S,Delta_Tree_s),
   switch(File,user),
%  stop_timer(init_gnaf_us_operator),
   switch(File).
%  pfe_dump.

pfe_dump :-
   support_set([p(f,e)],Gsn), !,
   write(user,'support_set([p(f,e]) = '),
   writeln(user,Gsn).
pfe_dump :-
   write(user,'support_set([p(f,e]) = '),
   writeln(user,'[]').

init_gnaf_us_operator(Tree_S,Delta_Tree_s) :-
   dn_clause(H,B1,B2),
   negate_atoms(B1,N_B1),
   tree_supersumes_clause(Delta_Tree_s,N_B1),
   list_to_ord_set(H,Head), list_to_ord_set(B2,Body2),
   list_to_ord_set(N_B1,E),   
   member(Atom,Head),
   resolve(Atom,Head,Clause1),
   list_to_ord_set(Clause1,Clause2),
   ord_union(Clause2,Body2,C),
   ord_union(C,E,Clause),
   \+ tautological_clause(Clause),
   \+ tree_subsumes_clause(Tree_S,Clause),
   assert(support_pair(Atom,C-E)),
   fail.
init_gnaf_us_operator(_,_) :-
   findall( Atom,
      support_pair(Atom,[]-[]), Extenders1 ),
   list_to_ord_set(Extenders1,Extenders2),
   retract_support_pairs(Extenders2),
   assert(global_extenders(Extenders2)),
%  writeln(user,'*** global_extenders ***'),
%  writeln(user,Extenders2),
%  writeln(user,'*** global_extenders ***'),
   findall( [Atom],
      support_pair(Atom,_), List ),
   list_to_ord_set(List,State),
   assert(actual_state(State)),
   findall( _,
      ( member([Atom],State),
        group_support_pairs(Atom,Gsn),
        assert(delta_support_set([Atom],Gsn)),
        assert(support_set([Atom],Gsn)) ), _ ),
   retractall(support_pair(_,_)),
   assert(accumulated_state([])).
 
group_support_pairs(Atom,Gsn) :-
   findall( C-E,
      support_pair(Atom,C-E), List ),
   list_to_ord_set(List,Gsn),
   !.

retract_support_pairs([A|As]) :-
   retractall(support_pair(A,_)),
   retract_support_pairs(As).
retract_support_pairs([]).


post_process_gnaf_us_operator :- 
   retractall(support_set(_,_)),
   retractall(delta_support_set(_,_)),
   retract(actual_state(_)),
   retract(accumulated_state(_)).


old_post_process_gnaf_us_operator :-
   write('pp_gnaf_us   '), ttyflush, nl,
   retractall(support_pair(_,_,_)),
   retract(d_fact_tree_s(Tree_s)),
   retract(delta_d_fact_tree_s(_)),
   retract(actual_state(_)),
   retract(accumulated_state(State)),
   negate_state(State,Negated_State),
%  state_to_max_tree(Negated_State,Max_Tree),
%  dualize_max_clause_tree(Max_Tree,Min_Tree),
%  pp_tree(Min_Tree),
   state_max_tree_subtract(Negated_State,Tree_s,Delta_s),
   max_tree_clauses_insert(Tree_s,New_Tree_s,Delta_s),
   max_tree_clauses_insert([],New_Delta_Tree_s,Delta_s),
   assert(d_fact_tree_s(New_Tree_s)), 
   assert(delta_d_fact_tree_s(New_Delta_Tree_s)).


/* abbreviations */

gnaf_fs_omega :-
   gnaf_fs_omega([],_).

gnaf_fs_omega(File) :-
   dconsult(File),
   gnaf_fs_omega([],_).
 
gnaf_fs(File) :-
   lp(File),
   d_facts_to_tree(G_Tree),
   gnaf_us_omega_tree(G_Tree,[],Tree_s),
   tree_to_state(Tree_s,State_s),
   state_to_max_tree(State_s,Max_Tree_s),
   dualize_max_clause_tree(Max_Tree_s,Dual_Tree_s),
   pp_tree(Dual_Tree_s),
   !.
 
gnaf_us(File) :-
   lp(File),
   d_facts_to_tree(G_Tree),
   gnaf_us_omega_tree(G_Tree,[],Tree_s),
   pp_tree(Tree_s),
   !.


/*** implementation ***********************************************/


/* extend_actual_state <-
      The actual conjunctive Herbrand state is extended.
      The non-extendable conjunctions are added to the accumulated
      state, the extended conjunctions form the new actual state, 
      their gsn-sets are asserted. */

extend_actual_state :-
   retract(actual_state(State1)),
   debug_write(bdwfs,user,'      '), pp_chs(bdwfs,user,State1), 
   debug_writeln(bdwfs,user,' '),
%  pp_dual_state(State1),
   extend_state(State1,State2,State3),
   assert(actual_state(State3)),
   retract(accumulated_state(State4)),
   state_supertract(State4,State2,State5),
   ord_union(State5,State2,State),
   assert(accumulated_state(State)),
   write('accumulated state '), pp_dhs(State), nl.
   
pp_chs(bdwfs,user,_) :-
   !.

pp_dual_state(State) :-
   write(user,'       '), 
   tree_based_coin_to_egcwa(State,Dual_State),
   writeln(user,' '), 
   write(user,'      '), pp_chs(user,Dual_State), writeln(user,' ').


/* extend_state(State1,State2,State3) <-
      The conjunctive Herbrand state State1 is extended, such that 
      the conjunctions are extended by one atom each.
      All non-extendable conjunctions form State2, the extended 
      conjunctions Ex_Conj form State3, their gsn-sets Gsn are 
      asserted as delta_support_set(Ex_Conj,Gsn). */

extend_state(State1,State2,State3) :-
%  write('extend state '), ttyflush,
%  write(user,'extend state '), ttyflush,
   pp_dhs(State1),
%  nl,
%  start_timer(extend_state),
   extend_state_loop(State1,[],State3),
   state_supertract(State1,State3,State2),
   switch(File,user),
%  stop_timer(extend_state),
   switch(File),
%  write('extended state   '), pp_dhs(State3), nl,
   !.

extend_state_loop([C|Cs],Sofar,Extended_State) :-
   extend_conjunction(C,State),
   extend_state_loop(Cs,[State|Sofar],Extended_State).
extend_state_loop([],Sofar,Extended_State) :-
   ord_union(Sofar,Extended_State).

extend_conjunction(Conjunction,State) :-
   d_fact_tree_S(Tree_S),
   delta_d_fact_tree_s(Tree_s),
   findall( Extended_Conjunction,
      ( Conjunction = [A|_],
%       negate_general_clause(Conjunction,Disjunction),
%       \+ tree_subsumes_clause(Tree_S,Disjunction),
        support_set([Atom],_),
        Atom @< A,
        gsn_product(Tree_S,Tree_s,Conjunction,Atom),
        ord_union([Atom],Conjunction,Extended_Conjunction) ), List ),
   list_to_ord_set(List,State).
   

/* gsn_product(Tree_S,Tree_s,Conj,Atom) <-
      From the asserted gsn-sets for Atom and Conj,
      delta_support_set(Conj,G1) and support_set([Atom],G2), 
      the gsn-set G3 for the extended conjunction E = Atom ^ Conj  
      is computed and asserted as delta_support_set(E,G3). */
 
gsn_product(Tree_S,Tree_s,Conj,Atom) :-
   delta_support_set(Conj,G1), support_set([Atom],G2),
   findall( C-E,
      ( member(C1-E1,G1), member(C2-E2,G2),
%       ord_disjoint([Atom],C1), ord_disjoint(Conj,C2),
        ord_union(E1,E2,E),
        tree_supersumes_clause(Tree_s,E),
        ord_union(C1,C2,C), ord_union(C,E,Clause),
        \+ tautological_clause(Clause),
        \+ tree_subsumes_clause(Tree_S,Clause) ),
      S1 ),
   !,
   S1 \== [], length(S1,I1),
   list_to_ord_set(S1,S2), length(S2,I2),
   remove_tautological_dn_facts(S2,S3), length(S3,I3),
   canonize_dn_facts(S3,G3), length(G3,I4),
   pp_numbers([I1,I2,I3,I4]),
   ord_union([Atom],Conj,Atom_Conj),
   assert(delta_support_set(Atom_Conj,G3)).


/* dualize_max_clause_tree(Tree1,Tree2) <-
      Given a clause tree Tree1 representing an impanded n-state State1,
      the dual clause tree Tree2 representing the dual, expanded n-state
      State2 = NHB_P \ State1 is computed. */

dualize_max_clause_tree(Tree1,[]) :-
   var(Tree1), !.
dualize_max_clause_tree([],Tree) :-
   !,
   program_to_herbrand_base(Herbrand_Base),
   ( ( Herbrand_Base = [], !,
       Tree = [] )
   ; ( negate_atoms(Herbrand_Base,Literals),
       list_of_elements_to_relation(Literals,Nodes),
       list_of_elements_to_relation(Nodes,Trees),
       Tree = [[]|Trees] ) ).
dualize_max_clause_tree(Tree1,Tree4) :-
   announcement('dualize_max_clause_tree'),
   tree_to_atoms(Tree1,Literals1),
   program_to_herbrand_base(Herbrand_Base),
   negate_atoms(Herbrand_Base,Literals2),
   ord_subtract(Literals2,Literals1,Literals3),
   tree_to_state(Tree1,State1),
   tree_based_coin_to_egcwa(State1,State2),
   state_to_tree(State2,Tree2),
   list_of_elements_to_relation(Literals3,State3),
   state_to_tree(State3,Tree3),
   tree_merge(Tree3,Tree2,Tree4).


/* old_dualize_max_clause_tree(Tree1,Tree2) <-
      Given a clause tree Tree1 representing an impanded n-state State1,
      the dual clause tree Tree2 representing the dual, expanded n-state
      State2 = NHB_P \ State1 is computed. */

old_dualize_max_clause_tree(Tree,[]) :-
   var(Tree), !.
old_dualize_max_clause_tree(Tree,Dual_Tree) :-
   announcement('dualize_max_clause_tree'),
%  switch(File,'results/dummy_data'),
   program_to_herbrand_base(Herbrand_Base),
   negate_atoms(Herbrand_Base,Literals),
   pp_hb(Literals), nl,
   non_supersumed_clauses(Tree,Literals,State), 
%  switch(File),
   write('  D  ='), pp_dhs(State), nl,
   state_to_tree(State,Dual_Tree).

non_supersumed_clauses(Tree,Literals,State) :-
   extend_supersumed_clauses(Tree,Literals,[[]],[],State).
 
extend_supersumed_clauses(_,_,[],State,State) :-
   !.
extend_supersumed_clauses(Tree,Literals,State1,Sofar,State4) :-
   extend_supersumed_clauses_base(Tree,Literals,State1,State2,State3),
   state_subtract(State2,Sofar,Delta_State2),
   ord_union(Delta_State2,Sofar,New_Sofar),
   state_subtract(State3,Sofar,Delta_State3),
   extend_supersumed_clauses(Tree,Literals,Delta_State3,New_Sofar,State4).

extend_supersumed_clauses_base(Tree,Literals,State1,State3,State4) :-
   write('extending '), pp_dhs(State1), nl,
   extend_state_by_literals(Literals,State1,State2),
   tree_supersumes_partition(Tree,State2,State3,State4).

extend_state_by_literals(Literals,State1,State2) :-
   findall( [Relevant_Literal|Clause],
      ( member(Clause,State1),
        relevant_literal(Literals,Relevant_Literal,Clause) ),
      State2 ).

relevant_literal(Literals,Relevant_Literal,[L|_]) :-
   member(Relevant_Literal,Literals),
   Relevant_Literal @< L.
relevant_literal(Literals,Relevant_Literal,[]) :-
   member(Relevant_Literal,Literals).
   
extend_clause_by_literals(Literals,Clause,State) :-
   findall( [Relevant_Literal|Clause],
      relevant_literal(Literals,Relevant_Literal,Clause),
      State ).


/* dualize_min_clause_tree(Tree1,Tree2) <-
      Given a clause tree Tree1 representing an expanded n-state State1,
      the dual clause tree Tree2 representing the dual, impanded n-state
      State2 = NHB_P \ State1 is computed. */

dualize_min_clause_tree(Tree,[]) :-
   var(Tree),
   announcement('dualize_min_clause_tree'),
   !.
% dualize_min_clause_tree([],_) :-
%    announcement('dualize_min_clause_tree'),
%    !.
dualize_min_clause_tree([],[Literals]) :-
   announcement('dualize_min_clause_tree'),
   program_to_herbrand_base(Herbrand_Base),
   negate_atoms(Herbrand_Base,Literals),
   !.
dualize_min_clause_tree(Tree,Dual_Tree) :-
   announcement('dualize_min_clause_tree'),
   program_to_herbrand_base(Herbrand_Base),
   negate_atoms(Herbrand_Base,Literals),
   tree_to_atoms(Tree,Tree_Literals),
   non_subsumed_clauses(Tree,Tree_Literals,State2),
   ord_subtract(Literals,Tree_Literals,Literals2), 
   state_disjunction([Literals2],State2,State),
   write('  D  ='), pp_dhs(State), nl,
   state_to_max_tree(State,Dual_Tree).


/* non_subsumed_clauses(Tree,Literals,State) <-
      Given a clause tree Tree and a set of literals Literals,
      State is computed as the set of maximal clauses, which are 
      not subsumed by the clause tree Tree, over Literals. */

non_subsumed_clauses(Tree,Literals,State) :-
   extend_non_subsumed_clauses(Tree,Literals,[[]],State).


/* extend_non_subsumed_clauses(Tree,Literals,State1,State2) <-
      extends all the clauses in State1 by relevant literals in Literals,
      State2 will contain the maximal extended clauses, which are not 
      subsumed by the clause tree Tree. */

extend_non_subsumed_clauses(Tree,Literals,State1,State2) :-
   extend_non_subsumed_clauses(Tree,Literals,State1,[],State2).

extend_non_subsumed_clauses(_,_,[],State,State) :-
   !.
extend_non_subsumed_clauses(Tree,Literals,State1,Sofar,State4) :-
   extend_non_subsumed_clauses_base(Tree,Literals,State1,State2,State3),
   ord_union(State3,Sofar,New_Sofar),
   extend_non_subsumed_clauses(Tree,Literals,State2,New_Sofar,State4).
 

/* extend_non_subsumed_clauses_base(Tree,Literals,State1,State2,State3) <-
      extends all clauses in State1 by one relevant literal in Literals,
      State2 will contain those extended clauses, which are not subsumed
      by the clause tree Tree, State3 will contain all clauses from 
      State1, which have no extension in State2. */

extend_non_subsumed_clauses_base(Tree,Literals,State1,State2,State3) :-
   write('extending '), pp_dhs(State1), nl,
   extend_state_by_literals(Literals,State1,State),
   tree_subsumes_partition(Tree,State,State2,_),
   state_supertract(State1,State2,State3).


/* old_extend_non_subsumed_clauses_base(Tree,Literals,State1,State2,State3) <-
      extends all clauses in State1 by one relevant literal in Literals,
      State2 will contain those extended clauses, which are not subsumed
      by the clause tree Tree, State3 will contain all clauses from 
      State1, which have no extension in State2. */

old_extend_non_subsumed_clauses_base(Tree,Literals,State1,State2,State3) :-
   write('extending '), pp_dhs(State1), nl, 
   extend_nsc_base(Tree,Literals,State1,[],State2,[],State3),
   write('result2   '), pp_dhs(State2), nl,
   write('result3   '), pp_dhs(State3), nl.

extend_nsc_base(Tree,Literals,[C|Cs],Sofar1,State1,Sofar2,State2) :-
   extend_clause_by_literals(Literals,C,Clauses),
   tree_subsumes_partition(Tree,Clauses,S,_),
   ( ( S = [], !,
       extend_nsc_base(Tree,Literals,Cs,[C|Sofar1],State1,Sofar2,State2) )
   ; extend_nsc_base(Tree,Literals,Cs,Sofar1,State1,[S|Sofar2],State2) ).
extend_nsc_base(_,_,[],Sofar1,State1,Sofar2,State2) :-
   list_to_ord_set(Sofar1,State1),
   ord_union(Sofar2,State2).


/* tree_subsumes_partition(Tree,State,State1,State2) <-
      Given a clause tree Tree and a state State,
      State1 contains all clauses from State, which are not subsumed
      by Tree, State2 contains the remaining clauses from State, 
      i.e. those which are subsumed by Tree. */

tree_subsumes_partition(Tree,State,State1,State2) :-
   tree_subsumes_partition(Tree,State,[],State1,[],State2).
 
tree_subsumes_partition(Tree,[C|Cs],Sofar1,State1,Sofar2,State2) :-
   tree_subsumes_clause(Tree,C),
   !,
   tree_subsumes_partition(Tree,Cs,Sofar1,State1,[C|Sofar2],State2).
tree_subsumes_partition(Tree,[C|Cs],Sofar1,State1,Sofar2,State2) :-
   tree_subsumes_partition(Tree,Cs,[C|Sofar1],State1,Sofar2,State2).
tree_subsumes_partition(_,[],Sofar1,State1,Sofar2,State2) :-
   list_to_ord_set(Sofar1,State1),
   list_to_ord_set(Sofar2,State2).


/* tree_supersumes_partition(Tree,State,State1,State2) <-
      Given a clause tree Tree and a state State,
      State1 contains all clauses from State, which are not supersumed
      by Tree, State2 contains the remaining clauses from State,
      i.e. those which are supersumed by Tree. */
 
tree_supersumes_partition(Tree,State1,State2,State3) :-
   tree_supersumes_partition(Tree,State1,[],State2,[],State3).
 
tree_supersumes_partition(Tree,[C|Cs],Sofar1,State1,Sofar2,State2) :-
   tree_supersumes_clause(Tree,C),
   !,
   tree_supersumes_partition(Tree,Cs,Sofar1,State1,[C|Sofar2],State2).
tree_supersumes_partition(Tree,[C|Cs],Sofar1,State1,Sofar2,State2) :-
   tree_supersumes_partition(Tree,Cs,[C|Sofar1],State1,Sofar2,State2).
tree_supersumes_partition(_,[],Sofar1,State1,Sofar2,State2) :-
   list_to_ord_set(Sofar1,State1),
   list_to_ord_set(Sofar2,State2).
 

/******************************************************************/


