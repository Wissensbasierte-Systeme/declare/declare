

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Static Semantics                      ***/
/***                                                            ***/
/******************************************************************/


semantics(static,dhb,DisLog_Program,State) :-
   transform_dnlp_to_static(DisLog_Program,Static_Program),
   hidden( ( 
      static_models_operator(Static_Program,Static_Models),
      tree_based_boolean_dualization(Static_Models,State) ) ).
semantics(static,nhb,DisLog_Program,State) :-
   transform_dnlp_to_static(DisLog_Program,Static_Program),
   hidden( (
      static_models_operator(Static_Program,Static_Models),
      state_operator_negative_s(
         DisLog_Program,Static_Models,State) ) ).
semantics(static,ghb,DisLog_Program,State) :-
   transform_dnlp_to_static(DisLog_Program,Static_Program),
   hidden( (
      static_models_operator(Static_Program,Static_Models),
      state_operator_general_2(
         DisLog_Program,Static_Models,State) ) ).
semantics(static,hi,DisLog_Program,Static_Models) :-
   transform_dnlp_to_static(DisLog_Program,Static_Program),
   hidden( 
      static_models_operator(Static_Program,Static_Models) ).


transform_dnlp_to_static(DisLog_Program,Static_Program) :-
   transform_dnlp_to_static_loop(
      DisLog_Program,Static_Program), !.

transform_dnlp_to_static_loop([],[]).
transform_dnlp_to_static_loop([R1|Rs1],[R2|Rs2]) :-
   transform_dnlp_rule_to_static(R1,R2),
   transform_dnlp_to_static_loop(Rs1,Rs2).

transform_dnlp_rule_to_static(H-P1-P2,Rule) :-
   static_negate_list(P2,SP2),
   Rule =.. [rule,H,P1,SP2].
transform_dnlp_rule_to_static(H-P1,Rule) :-
   Rule =.. [rule,H,P1,[]].
transform_dnlp_rule_to_static(H,Rule) :-
   Rule =.. [rule,H,[],[]].


static_negate_list([],[]).
static_negate_list([F1|Fs1],[F2|Fs2]) :-
   static_negate_formula(F1,F2),
   static_negate_list(Fs1,Fs2).

static_negate_formula(Formula,Static_Formula) :-
   ( Formula = []
   ; Formula = [_|_] ),
   !,
   Static_Formula =.. [not|[Formula]].
static_negate_formula(Formula,Static_Formula) :-
   Static_Formula =.. [not,[Formula]].


/******************************************************************/


