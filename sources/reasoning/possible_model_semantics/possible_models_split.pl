

/******************************************************************/
/***             Module :  Possible Models SI                   ***/
/******************************************************************/


/* in this Module, since the Rules are all definite ones, they
   are denoted [Head|Body] rather than Head-Body */


/*** interface ****************************************************/


possible_models_algorithm(1,Program,Possible_Models) :-
   pm_algorithm1(Program,Possible_Models).


pm_algorithm1(Program,Possible_Models) :-
   positive_possible_models_old(Program,Possible_Models),
   !.


/* positive_possible_models_old(Program,Possible_Models) <-
      First the ground program parts of Program are generated.
      All valid combinations of collections of facts Fs1
      and Rules Rs1 are used to model the Interpretations Model
      in Models by the findall. 
      Interpretations, which violate integrity constraints ICs are 
      mapped to [] and removed from Ms leaving Possible_Models. */

positive_possible_models_old(Program,Possible_Models) :-
   compute_ground_program_parts(Program,
      Facts,Sets2_of_Fs,
      Rules,Sets2_of_Rs,
      ICs),
   reduce_ics(ICs,Facts,ICs1),
   findall( Model,
      ( collect_facts(Sets2_of_Fs,[],Facts_1),
        reduce_ics(ICs1,Facts_1,ICs2),
        ord_union(Facts_1,Facts,Facts_2),
        collect_rules(Sets2_of_Rs,Rules,Rules_1),
        generate_possible_model(Facts_2,Rules_1,ICs2,Model) ),
      Models ),
   list_to_ord_set(Models,Possible_Models_1),
   remove_leading_empty_sets(Possible_Models_1,Possible_Models).
positive_possible_models_old(_,[]).


/* generate_possible_model(I,Rs,ICs,M) <-
      initializes model/3 by making IC a special kind of rule */

generate_possible_model(I,Rs,ICs,M) :-
   ics_to_no_rules(ICs,No_Rules),
   append(Rs,No_Rules,P),
   generate_possible_model(I,P,M).

/* generate_possible_model(I,P,M) <-
      P generates I1 from I (and is reduced to P1 by removing
      obsolete Rules). If I = I1, the Model M is ready,
      otherwise computation continues with I1 and P1. */

generate_possible_model(I,P,M) :- 
   generate_possible_model(P,I,[],I1,P1),
   ( ( ( I == I1 
       ; I1 == [] ), 
       !,
       M = I1 )
   ; ( reverse(P1,P2),
       generate_possible_model(I1,P2,M) ) ).


/*** implementation ***********************************************/


generate_possible_model([[Head|_]|P],I_Akku,P_Akku,I,P1) :-
   member(Head,I_Akku), 
   !,
   generate_possible_model(P,I_Akku,P_Akku,I,P1).
generate_possible_model([[Head|Body]|P],I_Akku,P_Akku,I,P1) :-
   ord_subtract(Body,I_Akku,Body1),
   ( ( Body1 == [],
       !,
       ( ( Head == no,
           !,
           I = [] )
       ; ( ord_add_element(I_Akku,Head,I_Akku1),
           generate_possible_model(P,I_Akku1,P_Akku,I,P1) ) ) )
   ; generate_possible_model(P,I_Akku,[[Head|Body1]|P_Akku],I,P1) ).
generate_possible_model([],I,P1,I,P1).

remove_leading_empty_sets([[]|X],Y) :-
   remove_leading_empty_sets(X,Y).
remove_leading_empty_sets(X,X).

normal_positiv_models_old1(P,Possible_Ms) :-
   epistemic_transformation2(P,EP),
   positive_possible_models_old(EP,Possible_Ms1),
   objc(Possible_Ms1,Possible_Ms).

collect_facts([Set_of_Facts|Sets],Fact_Akku,Facts) :-
   member(Facts1,Set_of_Facts),
   ord_union(Facts1,Fact_Akku,Fact_Akku2),
   collect_facts(Sets,Fact_Akku2,Facts).
collect_facts([],Facts,Facts).

collect_rules([Set_of_Rules|Sets],Rule_Akku,Rules) :-
   member(Rules1,Set_of_Rules),
   ord_union(Rules1,Rule_Akku,Rule_Akku2),
   collect_rules(Sets,Rule_Akku2,Rules).
collect_rules([],Rules,Rules).

satisfies_ics(Facts,ICs) :-
   member(Fact,Facts),
   member(IC,ICs),
   ord_subset(IC,Fact),
   !,
   fail.


reduce_ics(ICs,Fs,ICs1) :-
   reduce_ics(ICs,Fs,[],ICs1).

reduce_ics([IC|ICs],Fs,Akku,ICs1) :-
   ord_subtract(IC,Fs,IC1),
   ( ( IC1 == [], 
       !, 
       fail )
   ; reduce_ics(ICs,Fs,[IC1|Akku],ICs1) ).
reduce_ics([],_,ICs,ICs).


howmany([],1). 
howmany([[]|_],0).
howmany([[_]|X],Y) :-
   howmany(X,Y).
howmany([[_,_]|X],Y) :- 
   howmany(X,Z),
   Y is 3 * Z.
howmany([[_,_,_]|X],Y) :- 
   howmany(X,Z),
   Y is 7 * Z.
howmany([[_|A]|X],Y) :- 
   howmany([A|X],Z),
   Y is 2 * Z.


ics_to_no_rules([IC|ICs],[[no|IC]|No_Rules]) :-
   ics_to_no_rules(ICs,No_Rules).   
ics_to_no_rules([],[]).


/******************************************************************/


