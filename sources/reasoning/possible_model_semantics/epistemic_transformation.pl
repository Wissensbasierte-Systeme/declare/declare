

/******************************************************************/
/***                                                            ***/
/***             DisLog :  Epistemic Transformation             ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


/* epistemic_transformation(Program,Transformed_Program) <-
      Program is transformed to Transformed_Program by the
      epistemic transformation. */

epistemic_transformation(Program,Transformed_Program) :-
   epistemic_transformation(Program,0,[],Transformed_Program).


/* epistemic_transformation(Program,Index,T-Program1,T-Program2 <-
      Transforming the first Rule of Program yields TR,
      transforming the rest of Program yields T-Program0,
      appending TR, T-Program0 and T-Program1 yields T-Program2.
      T-Program0 is computed during recursive calls of the same 
      predicate. */

epistemic_transformation([H-B1-[]|P],Free_Index,Akku,TP) :-
%  !,
%  write('1'), ttyflush,
   epistemic_transformation(P,Free_Index,[H-B1|Akku],TP).
epistemic_transformation([[]-B1-B2|P],Free_Index,Akku,TP) :-
%  !,
%  write('2!'), ttyflush,
   epistemic_transform_rule(B1,B2,TR),
   epistemic_transformation(P,Free_Index,[TR|Akku],TP).
epistemic_transformation([H-B1-B2|P],Free_Index,Akku,TP) :-
%  !,
%  write('3!!'),ttyflush,
   dislog_flag_get(epistemic_transformation,ET),
   epistemic_transform_rule(ET,H,B1,B2,Free_Index,Next_Free_Index,TRs),
%  pp_bad_program(TRs),
%  wait,
   append(TRs,Akku,Akku1),
   epistemic_transformation(P,Next_Free_Index,Akku1,TP).
epistemic_transformation([H-B1|P],Free_Index,Akku,TP) :-
%  !,
%  write('4'),ttyflush,
   epistemic_transformation(P,Free_Index,[H-B1|Akku],TP).
epistemic_transformation([H|P],Free_Index,Akku,TP) :-
%  !,
%  write('5'),ttyflush,
   epistemic_transformation(P,Free_Index,[H|Akku],TP).
epistemic_transformation([],_,TP,TP).


/* objc(Coin1,Coin2) <-
      Interpretations which are not canonical are removed from Coin1.
      Atoms not belonging to the Herbrand Base are removed from every
      remaining Interpretation yielding Coin2. */

objc([I|Coin],[Herbrand_I|Herbrand_Coin]) :-
   canonical(I),
   cut_herbrand_base(I,Herbrand_I),
   objc(Coin,Herbrand_Coin).
objc([_|Coin],Herbrand_Coin) :-
   objc(Coin,Herbrand_Coin).
objc([],[]).


/* epistemic_transform_rule(B1,B2,TR) <-
      the rule with empty head and positive and normal Bodies B1 and B2
      is transsformed to the Rule TR. */

epistemic_transform_rule(B1,B2,B2_believed-B1) :-
   generate_believed_atoms(B2,B2_believed).


/* epistemic_transform_rule(TF,H,B1,B2,Free_Index,Next_Free_Index,TRs) <-
      the rule consisting of head H and positive and normal Bodies B1 and B2
      is transformed to the Rules TRs using epistemic transforamtion
      (Version TF). Free_Index denotes an index not used yet, during
      computation it is updated to Next_Free_Index. */

/* Version 1 */

epistemic_transform_rule(1,H,B1,B2,Free_Index,Next_Free_Index,TRs) :-
   generate_lambda_atoms(H,B1,Free_Index,Next_Free_Index,H_lambda),
   generate_believed_atoms(B2,B2_believed),
   epistemic_gen_clause3(H_lambda,B1,B2_believed,C3),
   epistemic_gen_clauses4(H,H_lambda,Cs4),
   epistemic_gen_clauses5(H_lambda,B2,Cs5),
   epistemic_gen_clauses6(H_lambda,H,H_lambda,Cs6),
   append([C3|Cs4],Cs5,Cs345),
   append(Cs345,Cs6,TRs).

epistemic_gen_clause3(H_lambda,B1,B2_believed,H-B1) :-
   append(H_lambda,B2_believed,H).

epistemic_gen_clauses4([H0|H],[HL0|H_lambda],[[H0]-[HL0]|Cs4]) :-
   epistemic_gen_clauses4(H,H_lambda,Cs4).
epistemic_gen_clauses4([],[],[]).
epistemic_gen_clauses4(_,_,_) :- error.

epistemic_gen_clauses5([HL0|H_lambda],B2,Cs5) :-
   epistemic_gen_clauses5b(HL0,B2,Cs5b),
   epistemic_gen_clauses5(H_lambda,B2,Cs5a),
   append(Cs5b,Cs5a,Cs5).
epistemic_gen_clauses5([],_,[]).

epistemic_gen_clauses5b(HL0,[B0|B2],[[]-[HL0,B0]|Cs5b]) :-
   epistemic_gen_clauses5b(HL0,B2,Cs5b).
epistemic_gen_clauses5b(_,[],[]).

epistemic_gen_clauses6([HL10|H_Lambda1],[H0|H],H_Lambda2,Cs6) :-
   epistemic_gen_clauses6b(HL10,H0,H_Lambda2,Cs6b),
   epistemic_gen_clauses6(H_Lambda1,H,H_Lambda2,Cs6a),
   append(Cs6a,Cs6b,Cs6).
epistemic_gen_clauses6([],[],_,[]).

epistemic_gen_clauses6b(HL10,H0,[HL10|H_Lambda2],Cs6b) :-
   epistemic_gen_clauses6b(HL10,H0,H_Lambda2,Cs6b).
epistemic_gen_clauses6b(HL10,H0,[HL20|H_Lambda2],[[HL10]-[H0,HL20]|Cs6b]) :-
   epistemic_gen_clauses6b(HL10,H0,H_Lambda2,Cs6b).
epistemic_gen_clauses6b(_,_,[],[]).


/* Version 2 */

epistemic_transform_rule(2,H,B1,B2,Free_Index,Next_Free_Index,[C3,C4|Cs5]) :-
   generate_lambda_atom(B1,Free_Index,Next_Free_Index,H_lambda),
   generate_believed_atoms(B2,B2_believed),
   epistemic_gen_clause3([H_lambda],B1,B2_believed,C3),
   epistemic2_gen_clause4(H,H_lambda,C4),
   epistemic_gen_clauses5b(H_lambda,B2,Cs5).

epistemic2_gen_clause4(H,H_lambda,H-[H_lambda]).


/* Version 3 */

epistemic_transform_rule(3,H,B1,B2,Free_Index,
      Next_Free_Index,[C3,C5A,C4,C4A|Cs5]) :-
   generate_lambda_atom(B1,Free_Index,Next_Free_Index,H_lambda),
   generate_not_lambda_atom(H_lambda,Not_H_lambda),
   generate_believed_atoms(B2,B2_believed),
   epistemic_gen_clause3([H_lambda,Not_H_lambda],B1,B2_believed,C3),
   epistemic2_gen_clause4(H,H_lambda,C4),
   epistemic3_gen_clause4A(Not_H_lambda,B2_believed,C4A),
   epistemic_gen_clauses5b(H_lambda,B2,Cs5),
   epistemic3_gen_clause5A(H_lambda,Not_H_lambda,C5A).

epistemic3_gen_clause5A(H_lambda,Not_H_lambda,[]-[H_lambda,Not_H_lambda]).

epistemic3_gen_clause4A(Not_H_lambda,B2_believed,B2_believed-[Not_H_lambda]).


/* Version 4 */

epistemic_transform_rule(4,H,B1,B2,Free_Index,Next_Free_Index,TRs) :-
   generate_lambda_atom(B1,Free_Index,Free_Index_2,H_lambda),
   generate_not_lambda_atom(H_lambda,Not_H_lambda),
   generate_not_lambda_atoms(B2,Free_Index_2,Next_Free_Index,B2_lambda),
   generate_believed_atoms(B2,B2_believed),
   epistemic_gen_clause3([H_lambda,Not_H_lambda],B1,B2_believed,C3),
   epistemic2_gen_clause4(H,H_lambda,C4),
   epistemic4_gen_clause4A(Not_H_lambda,B2_lambda,C4A),
   epistemic4_gen_clauses4B(B2_believed,B2_lambda,Cs4B),
   epistemic4_gen_clauses4C(B2_lambda,Cs4C),
   epistemic_gen_clauses5b(H_lambda,B2,Cs5),
   epistemic3_gen_clause5A(H_lambda,Not_H_lambda,C5A),
   append([C4,C4A|Cs4B],Cs4C,Cs4),
   append([C3|Cs4],[C5A|Cs5],TRs).

epistemic4_gen_clause4A(Not_H_lambda,B2_lambda,B2_lambda-[Not_H_lambda]).

epistemic4_gen_clauses4B([B2B0|B2_believed],[B2L0|B2_lambda],
      [[B2B0]-[B2L0]|Cs4B]) :-
   epistemic4_gen_clauses4B(B2_believed,B2_lambda,Cs4B).
epistemic4_gen_clauses4B([],[],[]).

epistemic4_gen_clauses4C([B2L0|B2_lambda],Cs4C) :-
   epistemic4_gen_clauses4Cb(B2L0,B2_lambda,Cs4Cb),
   epistemic4_gen_clauses4C(B2_lambda,Cs4Ca),
   append(Cs4Cb,Cs4Ca,Cs4C).
epistemic4_gen_clauses4C([],[]).

epistemic4_gen_clauses4Cb(B2L0,[B2L1|B2_lambda],[[]-[B2L0,B2L1]|Cs4Cb]) :-
   epistemic4_gen_clauses4Cb(B2L0,B2_lambda,Cs4Cb).
epistemic4_gen_clauses4Cb(_,[],[]).


/* Version 5 */

epistemic_transform_rule(5,H,B1,B2,Free_Index,Next_Free_Index,TRs) :-
   generate_lambda_atom(B1,Free_Index,Free_Index_2,H_lambda),
   generate_not_lambda_atom(H_lambda,Not_H_lambda),
   generate_not_lambda_atoms(B2,Free_Index_2,Next_Free_Index,B2_lambda),
   generate_believed_atoms(B2,B2_believed),
   epistemic_gen_clause3([H_lambda,Not_H_lambda],B1,B2_believed,C3),
   epistemic2_gen_clause4(H,H_lambda,C4),
   epistemic4_gen_clause4A(Not_H_lambda,B2_lambda,C4A),
   epistemic4_gen_clauses4B(B2_believed,B2_lambda,Cs4B),
   epistemic4_gen_clauses4C(B2_lambda,Cs4C),
   epistemic_gen_clauses5b(H_lambda,B2,Cs5b),
   epistemic5_gen_clauses5B(H_lambda,B2_believed,Cs5B),
   epistemic3_gen_clause5A(H_lambda,Not_H_lambda,C5A),
   append([C4,C4A|Cs4B],Cs4C,Cs4),
   append([C5A|Cs5b],Cs5B,Cs5),
   append([C3|Cs4],Cs5,TRs).

epistemic5_gen_clauses5B(H_lambda,B2_believed,Cs5B) :-
   epistemic_gen_clauses5b(H_lambda,B2_believed,Cs5B).


/* Version 6 */

epistemic_transform_rule(6,H,B1,B2,Free_Index,Next_Free_Index,TRs) :-
   generate_lambda_atom(B1,Free_Index,Free_Index_2,H_lambda),
   generate_not_lambda_atom(H_lambda,Not_H_lambda),
   generate_not_lambda_atoms(B2,Free_Index_2,Next_Free_Index,B2_lambda),
   generate_believed_atoms(B2,B2_believed),
   epistemic_gen_clause3([H_lambda,Not_H_lambda],B1,B2_believed,C3),
   epistemic2_gen_clause4(H,H_lambda,C4),
   epistemic4_gen_clause4A(Not_H_lambda,B2_lambda,C4A),
   epistemic4_gen_clauses4B(B2_believed,B2_lambda,Cs4B),
   epistemic4_gen_clauses4C(B2_lambda,Cs4C),
   epistemic_gen_clauses5b(H_lambda,B2,Cs5b),
   epistemic5_gen_clauses5B(H_lambda,B2_believed,Cs5B),
   epistemic3_gen_clause5A(H_lambda,Not_H_lambda,C5A),
   epistemic6_gen_clauses7(B2_believed,B2_lambda,Cs7),
   append([C4,C4A|Cs4B],Cs4C,Cs4),
   append([C5A|Cs5b],Cs5B,Cs5),
   append([C3|Cs4],Cs5,Cs345),
   append(Cs345,Cs7,TRs).

epistemic6_gen_clauses7(B2_believed,B2_lambda,Cs7) :-
   epistemic6_gen_clauses7(B2_believed,[],B2_lambda,Cs7).

epistemic6_gen_clauses7([B2B0|B2_believed],B2_lambda1,[B2L20|B2_lambda2],Cs7) :-
   append(B2_lambda1,B2_lambda2,B2_lambda),
   epistemic6_gen_clauses7b(B2B0,B2_lambda,Cs7b),
   epistemic6_gen_clauses7(B2_believed,[B2L20|B2_lambda1],B2_lambda2,Cs7a),
   append(Cs7b,Cs7a,Cs7).
epistemic6_gen_clauses7([],_,_,[]).

epistemic6_gen_clauses7b(B2B0,[B2L0|B2_lambda],[[]-[B2L0,B2B0]|Cs7b]) :-
   epistemic6_gen_clauses7b(B2B0,B2_lambda,Cs7b).
epistemic6_gen_clauses7b(_,[],[]).


/*** implementation ***********************************************/


var_filter([Atom|Atom_List],Var_List) :-
   var_filter(Atom,1,Var_List1),
   list_to_ord_set(Var_List1,Var_List2),
   var_filter(Atom_List,Var_List3),
   ord_union(Var_List3,Var_List2,Var_List).
var_filter([],[]).

var_filter(Atom,I,[Var|Var_List]) :-
   arg(I,Atom,Var),
   var(Var),
   II is I + 1,
   var_filter(Atom,II,Var_List).
var_filter(Atom,I,Var_List) :-
   arg(I,Atom,_),
   II is I + 1,
   var_filter(Atom,II,Var_List).
var_filter(_,_,[]).

generate_lambda_atoms(H,B1,Free_Index,Next_Free_Index,H_Lambda) :-
   length(H,L_H),
   Next_Free_Index is Free_Index + L_H,
   var_filter(B1,B1_Var_List),
   generate_lambda_atoms(H,B1_Var_List,Free_Index,H_Lambda).

generate_lambda_atoms([H0|H],Lambda_Args,Free_Index,[HL0|H_Lambda]) :-
   H0 =.. [H0_Predicate|_],
   HL =.. [H0_Predicate|Lambda_Args],
   concatenate(lambda,Free_Index,Lambda_Prefix),
   prefixed_predicate(Lambda_Prefix,HL,HL0),
   Next_Free_Index is Free_Index + 1,
   generate_lambda_atoms(H,Lambda_Args,Next_Free_Index,H_Lambda).
generate_lambda_atoms([],_,_,[]).


/* belongs to Version 2 */

generate_lambda_atom(B1,Free_Index,Next_Free_Index,H_Lambda) :-
   Next_Free_Index is Free_Index + 1,
   var_filter(B1,B1_Var_List),
   generate_lambda_atom(B1_Var_List,Free_Index,H_Lambda).

generate_lambda_atom(Lambda_Args,Free_Index,H_Lambda) :-
   concatenate(lambda,Free_Index,Lambda_Predicate),
   H_Lambda =.. [Lambda_Predicate|Lambda_Args].


/* belongs to Version 3 */

generate_not_lambda_atom(H_Lambda,Not_H_Lambda) :-
   prefixed_predicate(not_,H_Lambda,Not_H_Lambda).


/* belongs to Version 4 */

generate_not_lambda_atoms(B2,Free_Index,Next_Free_Index,B2_Lambda) :-
   length(B2,L_B2),
   Next_Free_Index is Free_Index + L_B2,
   generate_not_lambda_atoms(B2,Free_Index,B2_Lambda).

generate_not_lambda_atoms([B20|B2],Free_Index,[B2L0|B2_Lambda]) :-
   concatenate(lambda,Free_Index,Not_Lambda_Prefix),
   prefixed_predicate(Not_Lambda_Prefix,B20,B2L0),
   Next_Free_Index is Free_Index + 1,
   generate_not_lambda_atoms(B2,Next_Free_Index,B2_Lambda).
generate_not_lambda_atoms([],_,[]).

generate_believed_atoms([B20|B2],[BB20|B2_believed]) :-
   prefixed_predicate(believed,B20,BB20),
   generate_believed_atoms(B2,B2_believed).
generate_believed_atoms([],[]).


/* belongs to objc */

canonical(I) :-
   canonical(I,I).

canonical([Believed_Atom|Atoms],I) :-
   believed_atom(Believed_Atom,Atom),
   !,
   member(Atom,I),
   canonical(Atoms,I).
canonical([_|Atoms],I) :-
   canonical(Atoms,I).
canonical([],_).

believed_atom(Believed_Atom,Atom) :-
   prefixed_predicate(believed,Atom,Believed_Atom).

cut_herbrand_base([Not_Herbrand_Atom|I],Herbrand_I) :-
   not_herbrand_prefix(Not_Herbrand_Prefix),
   %prefixed_predicate(Not_Herbrand_Prefix,_,Not_Herbrand_Atom),
   Not_Herbrand_Atom =.. [Not_Herbrand_Predicate|_],
   concatenate(Not_Herbrand_Prefix,_,Not_Herbrand_Predicate),
   cut_herbrand_base(I,Herbrand_I).
cut_herbrand_base([Atom|I],[Atom|Herbrand_I]) :-
   cut_herbrand_base(I,Herbrand_I).
cut_herbrand_base([],[]).

not_herbrand_prefix(not_lambda).
not_herbrand_prefix(lambda).
not_herbrand_prefix(believed).

concatenate(String1,String2,String3) :-
   ( nonvar(String3) ; ( nonvar(String1) , nonvar(String2) ) ),
   ( var(String1) ; name(String1,String1_as_List) ),
   ( var(String2) ; name(String2,String2_as_List) ),
   ( var(String3) ; name(String3,String3_as_List) ),
   !,
   append(String1_as_List,String2_as_List,String3_as_List),
   name(String1,String1_as_List),
   name(String2,String2_as_List),
   name(String3,String3_as_List).

prefixed_predicate(Prefix,Predicate,Prefixed_Predicate) :-
   nonvar(Predicate),
   Predicate =.. [Name|Args],
   concatenate(Prefix,Name,Prefixed_Name),
   Prefixed_Predicate =.. [Prefixed_Name|Args].
prefixed_predicate(Prefix,Predicate,Prefixed_Predicate) :-
   nonvar(Prefixed_Predicate),
   Prefixed_Predicate =.. [Prefixed_Name|Args],
   concatenate(Prefix,Name,Prefixed_Name),
   Predicate =.. [Name|Args].


/******************************************************************/


