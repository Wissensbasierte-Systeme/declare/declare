

/******************************************************************/
/***                                                            ***/
/***         DisLog :  Ground_Transformation                    ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


/* compute_ground_program_parts(P,Definite_Facts,SetsofFacts,
         Definite_Rules,SetsofRules,Ground_ICs) <-
      From P the ground_program_parts are computed by
      sorting P in Facts, Rules and ICs,
      formatting of Facts to Delta,
      instantiating Rules to Ground_Rules,
      sorting and transforming Facts and Ground_Rules
      to Definite_Facts and -_Rules and
         SetsofFacts and -Rules. */

compute_ground_program_parts(P,Definite_Facts,SetsofFacts,
      Definite_Rules,SetsofRules,Ground_ICs) :-
   %program_rfi_filter(P,Facts,Rules,ICs),
   program_rfi_filter(P,Rules,Facts,ICs),
   retract_controller,
   set_controller(P),
   controller(Controller),
   facts_to_pargs(Facts,[],P_Args),
   facts_to_controlled_facts(P_Args,Controller,Delta),
   init_fest(Controller,Fest),
   dlp_to_d_clauses(Rules),
   iteration(Delta,Fest,Ground_Rules,Fest2),
   dlp_to_d_clauses(ICs),
   iteration(Fest2,Ground_ICs),
   facts_to_definite_and_sets_of_facts(Facts,Definite_Facts,
      SetsofFacts),
   rules_to_definite_and_sets_of_rules(Ground_Rules,
      Definite_Rules,SetsofRules).


/*** implementation ***********************************************/


retract_controller :-
   assertz(controller(end)),
   retract(controller(X)),
   X == end.

facts_to_definite_and_sets_of_facts(Facts,DefFacts,DisFacts) :-
   sort_facts(Facts,[],[],DefFacts,DisFacts).

sort_facts([[A]|Facts],DefFact_Akku,DisFact_Akku,DefFacts,DisFacts) :-
   sort_facts(Facts,[A|DefFact_Akku],DisFact_Akku,DefFacts,DisFacts).
sort_facts([Fact|Facts],DefFact_Akku,DisFact_Akku,DefFacts,DisFacts) :-
   disfact_to_set_of_deffacts(Fact,SetofDefFacts),
   sort_facts(Facts,DefFact_Akku,[SetofDefFacts|DisFact_Akku],
      DefFacts,DisFacts).
sort_facts([],DefFact_Akku,DisFact_Akku,DefFacts,DisFacts) :-
   list_to_ord_set(DefFact_Akku,DefFacts),
   list_to_ord_set(DisFact_Akku,DisFacts).

disfact_to_set_of_deffacts(Fact,SetofDefFacts) :- 
   findall(PartFact,
      ( sublist(PartFact,Fact),
        PartFact \== [] ),
           SetofDefFacts).

rules_to_definite_and_sets_of_rules(Rules,DefRules,DisRules) :-
   sort_rules(Rules,[],[],DefRules,DisRules).

sort_rules([[A]-B|Rules],DefRule_Akku,DisRule_Akku,DefRules,DisRules) :-
   sort_rules(Rules,[[A|B]|DefRule_Akku],DisRule_Akku,DefRules,DisRules).
sort_rules([Rule|Rules],DefRule_Akku,DisRule_Akku,DefRules,DisRules) :-
   disrule_to_defrules(Rule,Rules1),
   disfact_to_set_of_deffacts(Rules1,SetofDefRules), %missbraucht!
   sort_rules(Rules,DefRule_Akku,[SetofDefRules|DisRule_Akku],DefRules,DisRules).
sort_rules([],DefRule_Akku,DisRule_Akku,DefRules,DisRules) :-
   list_to_ord_set(DefRule_Akku,DefRules),
   list_to_ord_set(DisRule_Akku,DisRules).

disrule_to_defrules(Head-Body,Rules) :-
   disrule_to_defrules(Head,Body,Rules).

disrule_to_defrules([H|Head],Body,[[H|Body]|Rules]) :-
   disrule_to_defrules(Head,Body,Rules).
disrule_to_defrules([],_,[]).

disrule_to_set_of_defrules(Rules,SetofDefRules) :- 
   findall(PartRules,
      ( sublist(PartRules,Rules),
        PartRules \== [] ),
           SetofDefRules).

iteration(Fest,Ground_ICs) :-
   controller(C),
   findall( Body, %IC: denoted just by body!!
      ( d_clause(Body,[]),
        body_controller(Body,Body_Controller,Controlled_Body),
        may_delta_resolve(Body_Controller,C,Controlled_Body,Fest) ),
            Ground_ICs).

set_controller(Rules) :- %Rules include Facts here
   get_all_predicates(Rules,Predicates),
   list_to_ord_set(Predicates,Controller),
   assert(controller(Controller)).

init_fest([_|C],[[]|Fest]) :-
   init_fest(C,Fest).
init_fest([],[]).

get_all_predicates([H-B|Rules],Predicates) :-
   append(H,B,HB),
   list_to_ord_set(HB,HB1),
   get_predicates2(HB1,Ps1),
   get_all_predicates(Rules,Ps2),
   ord_union(Ps1,Ps2,Predicates).
get_all_predicates([H|Rules],Predicates) :-
   list_to_ord_set(H,H1),
   get_predicates2(H1,Ps1),
   get_all_predicates(Rules,Ps2),
   ord_union(Ps1,Ps2,Predicates).
get_all_predicates([],[]).

get_predicates2([H|T],[P|Ps]) :-
   H =.. [P|_],
   get_predicates2(T,Ps).
get_predicates2([],[]).
   
iteration(Delta,Fest,Ground_Rules,Fest2) :-
   add_controlled_facts(Fest,Delta,Fest1),
   controller(C),
   findall( Ground_Head-Ground_Body,
      ( d_clause(Head,Body),
        body_controller(Body,Body_Controller,Controlled_Body),
        must_delta_resolve(Body_Controller,C,Controlled_Body,Delta,Fest,Fest1),
        list_to_ord_set(Head,Ground_Head),
        list_to_ord_set(Body,Ground_Body) ),
            Ground_Rules1), 
   select_heads(Ground_Rules1,[],Facts),
   facts_to_controlled_facts(Facts,C,Controlled_Facts),
   subtract_controlled_facts(Controlled_Facts,Fest1,Delta1),
   ( (empty(Delta1), !,
      Ground_Rules = Ground_Rules1,
      Fest2 = Fest1)
   ; (iteration(Delta1,Fest1,Ground_Rules2,Fest2),
      ord_union(Ground_Rules1,Ground_Rules2,Ground_Rules) ) ).

empty([[]|A]) :-
   empty(A).
empty([]).

body_controller(Body,Body_C,C_Body) :-
   !,
   %list_to_ord_set(Body,S_Body),
   univ_predicates(Body,P_Body),
   list_to_ord_set(P_Body,P_S_Body),
   body_controller1(P_S_Body,Body_C,C_Body).

body_controller1([[P|Args]|Body],[P|Body_C],[Args|C_Body]) :-
   body_controller1(Body,Body_C,C_Body).
body_controller1([],[],[]).

must_delta_resolve([P1|BC],[P2|FC],Body,[_|D],[_|F],[_|DF]) :-
   P1 \== P2,
   !,
   must_delta_resolve([P1|BC],FC,Body,D,F,DF).
must_delta_resolve([_],_,[B],[D1|_],_) :-
   !,
   member(B,D1).
must_delta_resolve([_|BC],FC,[B|Body],[D1|_],_,DF) :-
   member(B,D1),
   may_delta_resolve(BC,FC,Body,DF).
must_delta_resolve([_|BC],FC,[B|Body],D,[F1|F],DF) :-
   member(B,F1),
   must_delta_resolve(BC,FC,Body,D,[F1|F],DF).

may_delta_resolve([P1|BC],[P2|FC],Body,[_|DF]) :-
   P1 \== P2,
   !,
   may_delta_resolve([P1|BC],FC,Body,DF).
may_delta_resolve([_|BC],FC,[B|Body],[DF1|DF]) :-
   member(B,DF1),
   may_delta_resolve(BC,FC,Body,[DF1|DF]).
may_delta_resolve([],_,[],_).

select_heads([Head-_|Ground_Rules],Head_Akku,Heads) :-
   head_to_univ_split_heads(Head,P_Arg_Head),
   ord_union(Head_Akku,P_Arg_Head,Head_Akku1),
   select_heads(Ground_Rules,Head_Akku1,Heads).
select_heads([],Heads,Heads).

head_to_univ_split_heads([H|Head],[P_Arg|P_Args]) :-
   H =.. P_Arg,
   head_to_univ_split_heads(Head,P_Args).
head_to_univ_split_heads([],[]).

facts_to_controlled_facts(P_Args,Controller,Controlled_Facts) :-
   %facts_to_pargs(Facts,[],P_Args),
   list_to_ord_set(P_Args,S_P_Args), %Sch... library!
   facts_to_controlled_facts(S_P_Args,Controller,[],[],Controlled_Facts).

facts_to_controlled_facts([[P|Args]|P_Args],[P|C],SL_Akku,L_Akku,CFs) :-
   facts_to_controlled_facts(P_Args,[P|C],[Args|SL_Akku],L_Akku,CFs).
facts_to_controlled_facts(P_Args,[_|C],SL_Akku,L_Akku,CFs) :-
   reverse(SL_Akku,SubList),
   facts_to_controlled_facts(P_Args,C,[],[SubList|L_Akku],CFs).
%facts_to_controlled_facts([],[],SL_Akku,L_Akku,CFs) :-
%   reverse(SL_Akku,SubList),
%   reverse([SubList|L_Akku],CFs).
facts_to_controlled_facts([],[],[],L_Akku,CFs) :-
   reverse(L_Akku,CFs).

facts_to_pargs([Fact|Facts],Akku,P_Args) :-
   fact_to_pargs(Fact,[],Pargs1),
   ord_union(Akku,Pargs1,Akku1),
   facts_to_pargs(Facts,Akku1,P_Args).
facts_to_pargs([],P_Args,P_Args).

fact_to_pargs([F|FF],Akku,P_Args) :-
   F =.. P_Arg,
   fact_to_pargs(FF,[P_Arg|Akku],P_Args).
fact_to_pargs([],Akku,P_Args) :-
   list_to_ord_set(Akku,P_Args).

add_controlled_facts([SL1|L1],[SL2|L2],[SL|L]) :-
   ord_union(SL1,SL2,SL),
   add_controlled_facts(L1,L2,L).
add_controlled_facts([],[],[]).

subtract_controlled_facts([SL1|L1],[SL2|L2],[SL|L]) :-
   ord_subtract(SL1,SL2,SL),
   subtract_controlled_facts(L1,L2,L).
subtract_controlled_facts([],[],[]).

facts_to_definite_and_sets_of_facts(Fs,DeFs,SetFs) :-
   facts_to_definite_and_sets_of_facts(Fs,[],[],DeFs,SetFs).

facts_to_definite_and_sets_of_facts([[P]|Fs],F_Akku,Set_Akku,DeFs,SetFs) :-
   facts_to_definite_and_sets_of_facts(Fs,[P|F_Akku],Set_Akku,DeFs,SetFs).
facts_to_definite_and_sets_of_facts([F|Fs],F_Akku,Set_Akku,DeFs,SetFs) :-
   fact_to_set_of_facts(F,SoF),
   facts_to_definite_and_sets_of_facts(Fs,F_Akku,[SoF|Set_Akku],DeFs,SetFs).
facts_to_definite_and_sets_of_facts([],DeFs,SetFs,DeFs,SetFs).

%program_rfi_filter(A,B,C,D) :-
   %program_filter(A,[],[],[],[B,C,D]).

%from ground

univ_predicates([H|T],[PH|PT]) :-
   H =.. PH,
   univ_predicates(T,PT).
univ_predicates([],[]).


/******************************************************************/
