

/******************************************************************/
/***             Module :  Possible Models Tests                ***/
/******************************************************************/



/*** interface ****************************************************/


possible_models_algorithm_test(Database_Name) :-
   dconsult(Database_Name,Database),
   findall( X-Possible_Models,
      possible_models_algorithm(X,Database,Possible_Models),
      All_Sets_of_Possible_Models ),
   !,
   writeln(All_Sets_of_Possible_Models),
   findall( X-Y,
      ( member(X-PM_X,All_Sets_of_Possible_Models),
        member(Y-PM_Y,All_Sets_of_Possible_Models),
        PM_X == PM_Y ),
      Graph_Edges ),
   findall( Z,
      ( member(Z-_,Graph_Edges)
      ; member(_-Z,Graph_Edges) ),
      Graph_Vertices ),
   vertices_edges_to_ugraph(Graph_Vertices,Graph_Edges,Graph),
   reduce(Graph,Reduced_Graph),
   writeln('Equals:'),
   writeln(Reduced_Graph).


/* pm_test(P_File,Data_File,Algorithm,
         Transformation,Possible_Models) <-
      (See below, usage)
      Merging Raw_P and data from P_File and Data_File yields
      program P. 
      The set Possible_Models is computed using algorithm 
      #Alogrithm and transformation #Transformation. */

pm_test(P_File,Data_File,Algorithm,
      Transformation,Possible_Models) :-
   dconsult(P_File,Raw_P),
   dconsult(Data_File,Rest_P),
   append(Raw_P,Rest_P,P),
   pm_algorithm_suffix(Algorithm,Algorithm_Suffix),
   pm_test_epistemic_transformation(Transformation,
      P,Transformed_P),
   pm_algorithm(Algorithm_Suffix,Transformed_P,Possible_Ms_TP),
   pm_test_objc(Transformation,Possible_Ms_TP,Possible_Ms_List),
   list_to_ord_set(Possible_Ms_List,Possible_Models).

pm_test_time(P_File,Data_File,Algorithm,
      Transformation,Possible_Models) :-
   start_timer('possible_models'),
   pm_test(P_File,Data_File,Algorithm,
      Transformation,Possible_Models),
   stop_timer(possible_models).

pm_test :-
   writeln('usage: '), nl,
   write('  pm_test(P_File,Data_File,Algorithm,'),
   writeln('Transformation,Possible_Ms)'), nl,
   writeln('  P_File - Program-File in dislog/sources'), nl,
   writeln('  Data_File - Data-File in dislog/sources'), nl,
   writeln('  Algorithm - Suffix has to be member of'), nl,
   writeln('     [0, 1, 2, 2i, 2d, 2di, 2id,   '),
   writeln('      2a, 2ia, 2da, 2dia, 2ida   '),
   writeln('      2as, 2ias, 2das, 2dias, 2idas, 3]   '), nl,
   writeln('     0 - PM as stable models of split-ground-programs'),
   writeln('     1 - PM as stable models of split-ground-programs'),
   writeln('          (= 0 with improved and adapted algorithms)'),
   writeln('     2 - PM with Sakama & Inoues algorithm'),
   writeln('          i  - with initialization'),
   writeln('          d  - with delta-iteration'),
   writeln('          a  - with single-atom-case-splitting'),
   writeln('          as - with subset-case-splitting'),
   writeln('     3 - PM tree-based'), nl,
   write('  Transformation - epistemische transformation,'),
   writeln(' has to be member of'), nl,
   writeln('     [0, 1, 2, 3, 4, 5, 6]'),
   writeln('          0 - no transformation'), nl,
   writeln('  Possible_Ms - Result').


/*** implementation ***********************************************/


pm_algorithm_suffix(Algorithm,0) :-
   concatenate(_,old,Algorithm).
pm_algorithm_suffix(Algorithm,3) :-
   concatenate(_,ree,Algorithm).
pm_algorithm_suffix(Algorithm,Algorithm_Suffix) :-
   concatenate(Algorithm_Prefix,as,Algorithm),
   dislog_flag_set(pm_algorithm2_generate,atoms),
   pm_algorithm_suffix(Algorithm_Prefix,Algorithm_Suffix).
pm_algorithm_suffix(Algorithm,Algorithm_Suffix) :-
   member(Algorithm_Suffix,[0,1,2,'2i','2d','2id','2di',3]),
   concatenate(_,Algorithm_Suffix,Algorithm).
pm_algorithm_suffix(Algorithm,Algorithm_Suffix) :-
   ( concatenate(Algorithm_Prefix,a,Algorithm)
   ; Algorithm_Prefix = Algorithm ),
   dislog_flag_set(pm_algorithm2_generate,atom),
   pm_algorithm_suffix(Algorithm_Prefix,Algorithm_Suffix).


pm_algorithm(0,P,Possible_Ms) :-
   possible_models_operator(P,Possible_Ms).
pm_algorithm(1,P,Possible_Ms) :-
   pm_algorithm1(P,Possible_Ms).
pm_algorithm('2di',P,Possible_Ms) :-
   pm_algorithm2id(P,Possible_Ms).
pm_algorithm('2id',P,Possible_Ms) :-
   pm_algorithm2id(P,Possible_Ms).
pm_algorithm('2d',P,Possible_Ms) :-
   pm_algorithm2i(P,Possible_Ms).
pm_algorithm('2i',P,Possible_Ms) :-
   pm_algorithm2i(P,Possible_Ms).
pm_algorithm(2,P,Possible_Ms) :-
   pm_algorithm2(P,Possible_Ms).
pm_algorithm(3,P,Possible_Ms) :-
   pm_algorithm3(P,Possible_Ms).


pm_test_epistemic_transformation(0,P,P). 
pm_test_epistemic_transformation(Transformation,P,TP) :- 
   number(Transformation), 
   Transformation < 7, 
   Transformation > 0, 
   dislog_flag_set(epistemic_transformation,Transformation), 
   epistemic_transformation(P,TP). 
pm_test_epistemic_transformation(_,P,TP) :-
   writeln('Using default transformation: 1'), 
   pm_test_epistemic_transformation(1,P,TP). 


pm_test_objc(0,Possible_Ms,Possible_Ms).
pm_test_objc(_,Coin,Possible_Ms) :-
   objc(Coin,Possible_Ms).


/******************************************************************/


