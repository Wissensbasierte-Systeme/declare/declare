

/******************************************************************/
/***         Module:   Possible Models                          ***/
/******************************************************************/


:- multifile possible_models_algorithm/3.


/*** interface ****************************************************/


/* possible_model_state_operator(Program,State) <-
      Given a DNLP Program, the set State of all positive
      disjunctions that are true in all possible models of 
      Program is computed. */

possible_model_state_operator(Program,State) :-
   possible_models_operator(Program,Possible_Models),
   tree_based_boolean_dualization(Possible_Models,State).

negative_possible_model_state_operator(Program,State) :-
   possible_gcwa_egcwa_operator(Program,State).

possible_gcwa_egcwa_operator(Program,State) :-
   possible_models_operator(Program,Possible_Models),
   tree_based_mmp_to_egcwa(Possible_Models,Egcwa_State),
   dnlp_to_herbrand_base(Program,Herbrand_Base),
   ord_union(Possible_Models,Atoms),
   ord_subtract(Herbrand_Base,Atoms,Gcwa_Atoms),
   list_of_elements_to_relation(Gcwa_Atoms,Gcwa_State),
   ord_union(Gcwa_State,Egcwa_State,State).

completed_possible_model_state_operator(Program,State) :-
   possible_models_operator(Program,Possible_Models),
   dnlp_to_herbrand_base(Program,Herbrand_Base),
   complete_models(Herbrand_Base,Possible_Models,Completions),
   tree_based_boolean_dualization(Completions,Dual_State),
   state_remove_tautologies(Dual_State,State).

possible_models_operator(Program,Possible_Models) :-
   hidden( ground_transformation(Program,Program2) ),
   dnlp_to_split_nlps(Program2,Split_Programs), 
   length(Split_Programs,L),
   writeln(L),
   possible_models_operator_loop(Split_Programs,List),
   ord_union(List,Possible_Models),
   !.


/* possible_models_operator(Program,Possible_Models) <-
      Given a DNLP Program, the set Possible_Models of all possible
      models of Program is computed. */

possible_models_operator_loop([Program|Programs],[Stable_Models|List]) :-
   hidden( sm_operator(Program,Stable_Models) ),
   possible_models_operator_loop(Programs,List).
possible_models_operator_loop([],[]).

/******************************************************************/
