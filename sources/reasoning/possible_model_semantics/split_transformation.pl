

/******************************************************************/
/***         Module:   Split Transformation                     ***/
/******************************************************************/


/*** interface ****************************************************/


/* dnlp_to_split_nlps(Program,Split_Programs) <-
      Given a DNLP Program, the set Split_Programs of all of its
      split programs is computed.  Each program in Split_Programs
      is an NLP. 
      program_split_rules_subsets avoids multiple computations 
      of subsets */

dnlp_to_split_nlps(Program,Split_Programs) :-
   program_split_rules_subsets(Program,Split_Rules_Subsets),
   split_rules_subsets_split_programs(Split_Rules_Subsets,[[]],Split_Programs).

program_split_rules_subsets([Rule|Rules],[SRSs|SRsSs]) :-
   rule_to_split_rules(Rule,SRSs),
   program_split_rules_subsets(Rules,SRsSs).
program_split_rules_subsets([],[]).

split_rules_subsets_split_programs([SRSs|SRsSs],SPs_sofar,SPs) :-
   findall( New_Split_Program,
            ( member(Split_Program,SPs_sofar),
              member(SRS,SRSs),
              ord_union(Split_Program,SRS,New_Split_Program) ),
            New_Split_Programs
          ),
   split_rules_subsets_split_programs(SRsSs,New_Split_Programs,SPs).
%split_rules_subsets_split_programs([],Split_Programs,Split_Programs).
split_rules_subsets_split_programs([],Split_Programs,Ord_Split_Programs) :-
   list_to_ord_set(Split_Programs,Ord_Split_Programs),
   length(Ord_Split_Programs,OSPL),
   write(OSPL),
   writeln(' Split_Programs fertig').

/*** implementation ***********************************************/

rule_to_split_rules([]-Body1-Body2,[[[]-Body1-Body2]]).
rule_to_split_rules([]-Body1,[[[]-Body1]]).

rule_to_split_rules(Head-Body1-Body2,Split_Rules_Subsets) :-
   rule_to_split_rules(Head,Body1,Body2,Split_Rules),
   generate_subsets(Split_Rules,Split_Rules_Subsets).
rule_to_split_rules(Head-Body,Split_Rules_Subsets) :-
   rule_to_split_rules(Head,Body,Split_Rules),
   generate_subsets(Split_Rules,Split_Rules_Subsets).
rule_to_split_rules(Head,Split_Rules_Subsets) :-
   head_to_split_heads(Head,Split_Rules),
   generate_subsets(Split_Rules,Split_Rules_Subsets).

head_to_split_heads(Head,Split_Heads) :-
   findall( [Split_Head],
            member(Split_Head,Head),
            Split_Heads
          ).

rule_to_split_rules(Head,Body1,Body2,Rules) :-
   findall( [Split_Head]-Body1-Body2,
            member(Split_Head,Head),
            Rules
          ).
rule_to_split_rules(Head,Body,Rules) :-
   findall( [Split_Head]-Body,
            member(Split_Head,Head),
            Rules
          ).

generate_subsets(Set,Subsets) :-
   findall( Subset,
            (sublist(Subset,Set), Subset \== []),
            Subsets
          ).

/******************************************************************/
