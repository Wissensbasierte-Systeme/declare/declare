


/******************************************************************/
/***             Module :  Possible Models Tpi                  ***/
/******************************************************************/


/* to understand the predicates in this module, it is usefull
   to lookup the original predicates from the given modules.
   %P denotes changes with regard to the supressed subsumption. */


/*** interface ****************************************************/


possible_models_algorithm(3,Program,Possible_Models) :-
   pm_algorithm3(Program,Possible_Models).


pm_algorithm3(Program,Possible_Models) :-
   program_rfi_filter(Program,Rules,_,ICs),
   p_minimal_models(Program,Coin1),
   pm_remove_interpretations(Coin1,ICs,Coin2),
   pm_mu_operator(Rules,Coin2,Possible_Models),
   !.


/* from minimal_models */


/* p_minimal_models(Program,Minimal_Models) <-
      tree_based PM-computation of Program yields Coin1.
      Applying pm_remove_interpretations and
      pm_mu_operator to Coin1 yields Minimal_Models. */

p_minimal_models(Program,Minimal_Models) :-
   abolish(d_fact_tree,1),
   abolish(delta_d_fact_tree,1),
%  program_rfi_filter(Program,Rs,_,ICs),
%  p_minimal_models_operator(Program,Coin1),
   p_minimal_models_operator(Program,Minimal_Models).
%  pm_remove_interpretations(Coin1,ICs,Coin2),
%  pm_mu_operator(Rs,Coin2,Minimal_Models).

p_minimal_models_operator(Program,Minimal_Models) :-
   p_tree_tpi_fixpoint(Program,[[]],Minimal_Model_Tree),
   tree_to_state(Minimal_Model_Tree,Minimal_Models), 
   !.


/*  from tpi_fixpoint */


/* p_tree_tpi_fixpoint(Program,Tree1,Tree2) <-
      given a DLP Program and a model tree Tree1,
      the model tree Tree2 is the result of a T_pi fixpoint
      iteration for Program starting with Tree1. */
 
p_tree_tpi_fixpoint(Program,Tree1,Tree2) :-
   hidden( 1, (
      assert(d_fact_tree([])), 
      assert(delta_fact_tree(Tree1)),
      repeat, 
         p_tree_tpi_delta_operator(Program), 
      delta_fact_tree([]),
      retract(delta_fact_tree([])), 
      retract(d_fact_tree(Tree2)) ) ).

p_tree_tpi_delta_operator(Program) :- 
   retract(d_fact_tree(Tree1)), 
   retract(delta_fact_tree(Delta1)), 
   p_tree_tpi_delta_operator(Program,Tree1,Delta1,Tree2,Delta2),
   assert(d_fact_tree(Tree2)),      dspy_tree(Tree2),
   assert(delta_fact_tree(Delta2)), dspy_tree(Delta2),
   !. 


/* tree_tpi_delta_operator(Program,Tree1,Tree2,Tree3,Tree4) <-
      given a DLP Program and two model trees Tree1, Tree2,
      the tpi-operator applied to Tree2 yields a new model tree New,
      the part of New which was already in Tree2 is added to Tree1
      yielding Tree3,
      from the really new part of New the model tree Tree3 is
      subtracted, the result is canonized yielding Tree4. */

p_tree_tpi_delta_operator(Program,Tree1,Delta1,Tree2,Delta2) :-
%  xy_tree_to_state(Delta1,Deltas1),
%  xy_tree_to_state(Tree1,State1),
   p_tree_tpi_join_non_ground(Program,Delta1,New_Tree),
%  dspy_tree(New_Tree),
%  xy_tree_to_state(New_Tree,New_State),
   tree_prune([],New_Tree,New_Tree2),
%  dspy_tree(New_Tree2),
%  xy_tree_to_state(New_Tree2,New_State2),
%  ( New_State2 == New_State 
%  ; writeln('New_State2 != New_State') ),
   p_tree_simplify_2(New_Tree2,New_Tree3), %P<
%  dspy_tree(New_Tree3),
%  xy_tree_to_state(New_Tree3,New_State3),
%  ( New_State3 == New_State2 
%  ; writeln('New_State3 != New_State2') ),
   p_tree_can_2(New_Tree3,New),
%  dspy_tree(New),
%  xy_tree_to_state(New,News),
%  ( New_State3 == News; writeln('New_State3 != News') ),
   tree_tree_difference(New,Delta1,New2), 
%  dspy_tree(New2),
%  xy_tree_to_state(New2,News2),
%  ( ord_disjoint(Deltas1,News2) 
%  ; writeln('Deltas1,News2 not disjoint') ),
%  ( ord_union(News2,Deltas1,News) 
%  ; writeln('News2+Deltas1 != News') ),
%  writeln([News2,Deltas1,News]),
%  ( ord_union(News2,Deltas1,News) 
%  ; ( writeln('News2+Deltas1 != News'), 
%      states_out([News2,Deltas1,News]) ) ),
   tree_tree_difference(New,New2,New1),   
%  dspy_tree(New1),
%  xy_tree_to_state(New1,News1),
%  ( ord_disjoint(News2,News1) 
%  ; writeln('News2,News1 not disjoint') ), 
%  ( ord_union(News1,News2,News) 
%  ; writeln('News1+News2 != News') ),
   tree_simplify_3(New1,New4),            
%  dspy_tree(New4),
%  xy_tree_to_state(New4,News4),
%  ( News4 == News1 
%  ; writeln('News4 != News1') ),
   tree_union(Tree1,New4,Tree2),          
%  dspy_tree(Tree2),
%  xy_tree_to_state(Tree2,State2),
%  ( ord_union(State1,News4,State2) 
%  ; writeln('State1+News4 != State2') ),
%  tree_tree_subtract(New2,Tree2,New3),   dspy_tree(New3),
   tree_tree_difference(New2,Tree2,New3),   
%  dspy_tree(New3),
%  xy_tree_to_state(New3,News3),
%  ( ord_disjoint(State2,News3) 
%  ; writeln('State2,News3 not disjoint') ),
%  ( ord_union(News3,State2,News2) 
%  ; writeln('News3+State2 != News2') ),
%  tree_can(New3,Delta2).
   Delta2 = New3.


/*** implementation ***********************************************/


p_tree_tpi_join_non_ground(Program,Tree1,Tree2) :-
   p_tree_tpi_join_2(Program,Tree1,Program2), %P fuer positiv
   p_tree_tpi_join_ground(Program2,Tree1,Tree2).

p_tree_tpi_join_2(Program1,Tree,Program2) :-
   dlp_to_d_clauses(Program1),
   assert(delta_d_fact_tree(Tree)),
   p_tree_tpi_join_2(Program2),
   retractall(d_clause(_,_)),
   retract(delta_d_fact_tree(Tree)).

p_tree_tpi_join_2(Program) :-
   write('tree_join    '), ttyflush,
   start_timer(tree_tpi_join),
   delta_d_fact_tree(Tree),
   findall( Ord_Head-Ord_Body,
      ( d_clause(Head,Body),
        tpi_resolve(Tree,Body,_),
        list_to_ord_set(Head,Ord_Head),
        list_to_ord_set(Body,Ord_Body) ),
      Program1 ),
   list_to_ord_set(Program1,Program),
   stop_timer(tree_tpi_join).

p_tree_tpi_join_ground(Program1, [Node], Tree) :-
   !,
   p_reduce_program(Program1, Node, Program2),
   p_select_facts(Program2, State),
   ( State == [[]], Tree = []
   ; p_d_facts_to_state(State, State2),
     p_state_to_tree(State2, Model_Tree),
     attach_tree(Node, Model_Tree, Tree) ).
p_tree_tpi_join_ground(Program1, [Node|Trees1], Tree) :-
   p_reduce_program(Program1, Node, Program2),
   p_trees_tpi_join_ground(Program2, Trees1, Trees2),
   attach_trees(Node, Trees2, Tree).
p_tree_tpi_join_ground(_, [], []).

p_trees_tpi_join_ground(Program,[Tree|Trees],New_Trees) :-
   p_tree_tpi_join_ground(Program,Tree,Tree2),
   p_trees_tpi_join_ground(Program,Trees,Trees2),
   ( ( Tree2 == [], 
       !, 
       New_Trees = Trees2 )
   ; ( New_Trees = [Tree2|Trees2] ) ).
p_trees_tpi_join_ground(_,[],[]).

p_reduce_program([[]-Body|Program1],Node,[[]-New_Body|Program2]) :-
   ord_subtract(Body,Node,New_Body),
   p_reduce_program(Program1,Node,Program2).
p_reduce_program([[[]|Head]-Body|Program1],Node,Program2) :-
   !,
   ord_subtract(Head,Node,New_Head),
   ( ( New_Head == [],
       Program2 = Program3 )
   ; Program2 = [[[]|New_Head]-New_Body|Program3] ),
   ord_subtract(Body,Node,New_Body),
   p_reduce_program(Program1,Node,Program3).
p_reduce_program([Head-Body|Program1],Node,Program2) :-
   !,
   ord_subtract(Head,Node,New_Head),
   ( ( New_Head == Head,
       Program2 = [New_Head-New_Body|Program3] )
   ; ( ( New_Head == [] ,
         Program2 = Program3 )
     ; Program2 = [[[]|New_Head]-New_Body|Program3] ) ),
   ord_subtract(Body,Node,New_Body),
   p_reduce_program(Program1,Node,Program3).
   p_reduce_program([],_,[]).

p_state_to_tree(State,Model_Tree) :- %P+
   p_tree_clauses_insert([],Model_Tree,State).

p_select_facts([[]-[]|_],[[]]).
p_select_facts([Head-[]|Program],[Head|State]) :-
   !,
   p_select_facts(Program,State).
p_select_facts([_|Program],State) :-
   p_select_facts(Program,State).
p_select_facts([],[]).


/* from clause_trees */


p_tree_simplify_2([Node|Trees1],[Node|Trees2]) :-
   p_remove_empty_nodes_2(Trees1,Trees2).
p_tree_simplify_2([],[]).

p_remove_empty_nodes_2([[[]|Trees1]|Trees2],Trees4) :-
   Trees1 \== [], %P+
   !,
   append(Trees1,Trees2,Trees3),
   p_remove_empty_nodes_2(Trees3,Trees4).
p_remove_empty_nodes_2([[Node|Trees1]|Trees2],
      [[Node|Trees3]|Trees4]) :-
   p_remove_empty_nodes_2(Trees1,Trees3),
   p_remove_empty_nodes_2(Trees2,Trees4).
p_remove_empty_nodes_2([],[]).

p_tree_can_2(Tree1,Tree2) :-
   tree_to_state(Tree1,State1),
%  state_can(State1,State2),
   State1 = State2, %P<
   p_state_to_tree(State2,Tree2).

p_tree_clauses_insert(Tree1,Tree3,[Clause|Clauses]) :-
   p_tree_clause_insert(Tree1,Tree2,Clause),
   p_tree_clauses_insert(Tree2,Tree3,Clauses).
p_tree_clauses_insert(Tree,Tree,[]).

p_tree_clause_insert([],[Clause],Clause) :-
   !.
p_tree_clause_insert(Tree,New_Tree,Clause) :-
   best_intersection(Tree,Clause,Sequence,_),
   p_tree_clause_insert(Tree,New_Tree,Clause,Clause,Sequence),
   !.
p_tree_clause_insert([H],[H,[[]],[Clause1]],Clause,_,[]) :- %P!
   ord_subset(H,Clause),
   !,
   ord_subtract(Clause,H,Clause1).
p_tree_clause_insert([H|Ts],[Clause,[[]],[H1|Ts]],Clause,_,[]) :- %P!
   ord_subset(Clause,H),
   !,
   ord_subtract(H,Clause,H1).
p_tree_clause_insert([H|Ts],[H,[Clause1]|Ts],Clause,_,[]) :- %P=
   ord_subset(H,Clause),
   !,
   ord_subtract(Clause,H,Clause1).
p_tree_clause_insert([H|Ts],[Node,[Clause1],[H1|Ts]],Clause,_,[]) :- %P=
   !,
   ord_intersection(H,Clause,Node),
   ord_subtract(Clause,H,Clause1),
   ord_subtract(H,Clause,H1).
p_tree_clause_insert([H|Ts],[H|New_Ts],Clause,Full_Clause,S) :- %P=
   ord_subtract(Clause,H,Clause1),
   p_trees_clause_insert(Ts,New_Ts,Clause1,Full_Clause,S).

p_trees_clause_insert([Tree|Trees],[New_Tree|Trees],C1,C2,[I|S]) :-
   I = 1,
   !,
   p_tree_clause_insert(Tree,New_Tree,C1,C2,S).
p_trees_clause_insert([Tree|Trees],[Tree|New_Trees],C1,C2,[I|S]) :-
   II is I - 1,
   p_trees_clause_insert(Trees,New_Trees,C1,C2,[II|S]).


/* from dualization */


p_clause_tree_to_model_tree(Tree1,Tree2) :-
   p_tree_to_models(Tree1,State1),
   state_prune(State1,State2),
   /* state_can(State2,State3), violates possible models */
   State2 = State3,            /* replacement */
   p_state_to_tree(State3,Tree2).

p_state_to_model_tree(State,Model_Tree) :-
   p_state_to_tree(State,Clause_Tree),
   p_clause_tree_to_model_tree(Clause_Tree,Model_Tree).

p_tree_to_models(Tree,Minimal_Models) :-
   write('tree_models  '), ttyflush,
   start_timer(tree_to_models),
   p_tree_to_c_facts(Tree,Minimal_Models),
   stop_timer(tree_to_models).

p_tree_to_c_facts([],[[]]) :-
   !.
p_tree_to_c_facts(Tree,C_Facts1) :-
   findall( C_Fact,
      p_tree_to_c_fact(Tree,C_Fact),
      C_Facts ),
   list_to_ord_set(C_Facts,C_Facts1). 

p_tree_to_c_fact([Clause|Trees],Subclause) :-
   p_xy(Clause,Subclause1),
   list_to_ord_set(Subclause1,Ordset_Subclause1), 
   p_trees_to_c_fact(Trees,Subclause2),
   ord_union(Ordset_Subclause1,Subclause2,Subclause),
   Subclause \== [].

p_trees_to_c_fact([Tree|Trees],C_Fact) :-
   p_tree_to_c_fact(Tree,C_Fact_1),
   p_trees_to_c_fact(Trees,C_Fact_2),
   ord_union(C_Fact_1,C_Fact_2,C_Fact).
p_trees_to_c_fact([Tree],C_Fact) :-
   p_tree_to_c_fact(Tree,C_Fact).
p_trees_to_c_fact([],[]).

p_xy([_|Clause],Subclause) :-
   p_xy(Clause,Subclause).
p_xy([H|Clause],[H|Subclause]) :-
   p_xy(Clause,Subclause).
p_xy([],[]).


/* p_d_facts_to_state */


/* p_d_facts_to_state(Facts,State) <-
      State represents the possible models representing the
      disjunctive facts Facts.
      Using facts_to_lists_of_possible_facts all subsets of
      every Fact are only created once. */

p_d_facts_to_state(Facts,State) :-
   facts_to_lists_of_possible_facts(Facts,L_P_Facts),
   lists_of_possible_facts_to_state(L_P_Facts,State).

facts_to_lists_of_possible_facts([Fact|Facts],[P_Facts|L_P_Facts]) :-
   fact_to_possible_facts(Fact,P_Facts),
   facts_to_lists_of_possible_facts(Facts,L_P_Facts).
facts_to_lists_of_possible_facts([],[]).

fact_to_possible_facts([[]|Fact],P_Facts) :-
   findall( P_Fact,
      sublist(P_Fact,Fact),
      P_Facts1),
   list_to_ord_set(P_Facts1,P_Facts).
fact_to_possible_facts(Fact,P_Facts) :-
   findall( P_Fact,
      sublist(P_Fact,Fact),
      P_Facts1),
   list_to_ord_set(P_Facts1,P_Facts2),
   ord_del_element(P_Facts2,[],P_Facts).

lists_of_possible_facts_to_state([State|L_P_Facts],State2) :-
   lists_of_possible_facts_to_state(State,L_P_Facts,State2).

lists_of_possible_facts_to_state(State,[P_Facts|L_P_Facts],State2) :-
   possible_facts_to_state(State,P_Facts,State1),
   lists_of_possible_facts_to_state(State1,L_P_Facts,State2).
lists_of_possible_facts_to_state(State,[],State).

possible_facts_to_state(State,P_Facts,State2) :-
   findall( Clause,
      ( member(Clause1,State),
        member(Clause2,P_Facts),
        ord_union(Clause1,Clause2,Clause) ),
      State1),
   list_to_ord_set(State1,State2).


/******************************************************************/


