

/******************************************************************/
/***                                                            ***/
/***        DentaLog: Knowledge Base to Decision Tree           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dentalog_question_to_decision_tree(KB, Qid, Xml) <-
      */

dentalog_knowledge_base_to_decision_tree :-
   dread(xml, 'examples/dentalog/dentalog_kb.xml', [KB]),
   findall( Qid,
      Qid := KB^'InitQASets'^'QContainer'@'ID',
      Qids ),
   maplist( dentalog_question_to_decision_tree(KB),
      Qids, Xmls ),
   Xml = decision_tree:Xmls,
   dwrite(xml, 'dentalog', Xml).
   
dentalog_question_to_decision_tree(KB, Qid, Xml) :-
   Question := KB^'Questions'^'Question'::[@'ID'=Qid],
   [Q_Text] := Question^'Text'^content::'*',
   write_list(user, [Qid, '.']), ttyflush,
   !,
   findall( Item,
      ( Answer := Question^'Answers'^'Answer',
        [A_Text] := Answer^'Text'^content::'*',
        Value := Answer@'ID',
        dentalog_answer_id_to_answer_content(KB, Value, Content),
        Item := answer:['ID':Value, text:A_Text]:Content ),
      Items ),
   Xml = question:['ID':Qid, text:Q_Text]:Items.
dentalog_question_to_xml_tree(_, _, to_be_done:[]).

dentalog_answer_id_to_answer_content(KB, Value, Content) :-
   findall( Xml,
      ( Qid_2 := KB^'KnowledgeSlices'^'KnowledgeSlice'::[
           ^'Action'@type='ActionIndication',
           ^'Condition'@value=Value]^'Action'^'TargetQASets'
           ^'QASet'@'ID',
        dentalog_question_to_decision_tree(KB, Qid_2, Xml)
      ; Did := KB^'KnowledgeSlices'^'KnowledgeSlice'::[
           ^'Action'@type='ActionHeuristicPS',
           ^'Condition'@value=Value]^'Action'^'Diagnosis'@'ID',
        concat('P_', Diag, Did),
        Xml = diagnosis:[text:Diag]:[] ),
      Content ),
   Content \= [],
   !.
dentalog_answer_id_to_answer_content(_, _, []).


/******************************************************************/


