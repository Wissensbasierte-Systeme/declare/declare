

/******************************************************************/
/***                                                            ***/
/***         DentaLog: Tests                                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


test(dentalog:d3_findings_to_rule_goal_edges, Findings) :-
   dread(xml, 'examples/dentalog/dentalog_kb.xml', [KB]),
   Findings = ['P_Gelenkflaeche beschaedigt':'P6'],
   d3_knowledge_base_to_knowledge_slices(KB),
   d3_findings_to_rule_goal_edges(Findings, Edges),
   d3_rule_goal_edges_to_picture(Edges).

test(dentalog:d3_file_and_diagnosis_to_reaching_rule_edges,
      Diagnosis) :-
   dread(xml, 'examples/dentalog/dentalog_kb.xml', [KB]),
   Diagnosis = 'P_Gelenkflaeche beschaedigt',
   d3_knowledge_base_to_reaching_rule_edges(KB, Diagnosis, Edges),
   d3_rule_goal_edges_to_picture(Edges).

test(dentalog, d3_knowledge_base_to_display) :-
   dread(xml, 'examples/dentalog/dentalog_kb.xml', [KB]),
   d3_knowledge_base_to_display(KB).

test(dentalog:d3_file_to_rule_goal_edges, 'Dentalog'(Findings)) :-
   Findings = ['P_Gelenkflaeche beschaedigt':'P6'],
   File = 'examples/dentalog/dentalog_kb.xml',
   d3_file_and_findings_to_rule_goal_edges(
      File, Findings, Edges),
   writeq(user, Edges),
   d3_rule_goal_edges_to_picture(Edges).

test(dentalog:d3_file_to_diagnosis_environment, Diagnosis) :-
   Diagnosis = 'P_Gelenkflaeche beschaedigt',
   File = 'examples/dentalog/dentalog_kb.xml',
   d3_file_to_diagnosis_environment(File, Diagnosis).

test(dentalog:d3_file_and_diagnosis_to_reaching_rule_edges,
      Diagnosis) :-
   Diagnosis = 'P_Gelenkflaeche beschaedigt',
   File = 'examples/dentalog/dentalog_kb.xml',
   d3_file_and_diagnosis_to_knowledge_base(File, Diagnosis, KB),
   d3_knowledge_base_to_reaching_rule_edges(KB, Diagnosis, Edges),
   d3_rule_goal_edges_to_picture(Edges).

test(dentalog:all_edges, 1) :-
   dread(xml, 'examples/dentalog/dentalog_kb.xml', [KB]),
   d3_knowledge_base_to_rule_edges(finding, KB, Edges_1),
   d3_knowledge_base_to_question_indication_edges(KB, Edges_2),
   append(Edges_1, Edges_2, Edges),
   d3_rule_goal_edges_to_picture(Edges).


/******************************************************************/


