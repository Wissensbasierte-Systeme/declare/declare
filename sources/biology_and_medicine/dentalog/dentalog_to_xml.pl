

/******************************************************************/
/***                                                            ***/
/***         DentaLog: Knowledge Base to XML                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dentalog_file_to_xml <-
      */

dentalog_file_to_xml :-
   dislog_variable_get(example_path, Examples),
   concat(Examples, 'dentalog/dentalog_kb', File),
   dentalog_file_to_xml(File, Xml),
   concat(File, '.xml', File_2),
   dwrite(xml, File_2, Xml).

dentalog_file_to_xml(Xml) :-
   dislog_variable_get(example_path, Examples),
   concat(Examples, 'dentalog/dentalog_kb', File),
   dentalog_file_to_xml(File, Xml).

dentalog_file_to_xml(File, Xml) :-
   dconsult(File, Program_2),
   sort(Program_2, Program),
   dentalog_program_to_xml(Program, Xml).


/* dentalog_program_to_xml(Program, Xml) <-
      */

dentalog_program_to_xml(Program, Xml) :-
   dentalog_program_to_questions(Program, Q_Xml),
   dentalog_program_to_diagnoses(Program, D_Xml),
   dentalog_program_to_init_qa_sets(Q_Xml, Program, I_Xml),
   dentalog_program_to_rule_indications(
      Q_Xml, Program, Rind_Xmls),
   dentalog_program_to_diagnosis_rules(
      Q_Xml, Program, Rfb_Xmls),
   dentalog_program_to_value_rules(Q_Xml, Program, V_Slices),
   set_num(dentalog_question, 0),
   set_num(dentalog_knowledge_slice, 0),
   append([Rind_Xmls, Rfb_Xmls, V_Slices], Slices),
   Xml = 'KnowledgeBase':[
      I_Xml, 'QContainers':[], Q_Xml, D_Xml,
      'KnowledgeSlices':Slices ].


/* dentalog_program_to_questions(Program, Xml) <-
      */

dentalog_program_to_questions(Program, Xml) :-
   findall( Question,
      ( member([possible_observations(Q, Xs)], Program),
        get_num(dentalog_question, N),
        concat('Mf', N, Qid),
        dentalog_fact_to_answers(Qid, Xs, Answers),
        Question = 'Question':['ID':Qid, type:'OC']:[
           'Text':[Q], 'Answers':Answers ] ),
      Questions_1 ),
   findall( Q,
      ( member(
           [next_examination(Q)]-[examination(_, _)], Program),
        \+ member([possible_observations(Q, _)], Program) ),
      Qs ),
   sort(Qs, Qs_2),
   ( foreach(Q, Qs_2), foreach(Question, Questions_2) do
        get_num(dentalog_question, N),
        concat('Mf', N, Qid),
        Question = 'Question':['ID':Qid, type:'OC']:[
           'Text':[Q], 'Answers':[] ] ),
   append(Questions_1, Questions_2, Questions),
   Xml = 'Questions':Questions.

dentalog_fact_to_answers(Qid, Xs, Answers) :-
   foreach(X, Xs), foreach(Answer, Answers) do
      X = N-Text,
      concat([Qid, a, N], Aid),
      Answer = 'Answer':[
         'ID':Aid, type:'AnswerChoice']:[
         'Text':[Text]].


/* dentalog_program_to_diagnoses(Program, Xml) <-
      */

dentalog_program_to_diagnoses(Program, Xml) :-
   findall( Diagnosis,
      ( member(
           [diagnosis(D)]-[examination(_, _)], Program),
        concat('P_', D, Did),
        Diagnosis = 'Diagnosis':['ID':Did]:['Text':[D]] ),
      Diagnoses ),
   Xml = 'Diagnoses':Diagnoses.


/* dentalog_program_to_init_qa_sets(Q_Xml, Program, Xml) <-
      */

dentalog_program_to_init_qa_sets(Q_Xml, Program, Xml) :-
   findall( QContainer,
      ( member([first_examination(Q)], Program),
        dentalog_question_to_qid(Q_Xml, Q, Qid),
        QContainer = 'QContainer':['ID':Qid]:[] ),
      QContainers ),
   Xml = 'InitQASets':QContainers.


/* dentalog_program_to_rule_indications(Q_Xml, Program, Slices) <-
      */

dentalog_program_to_rule_indications(Q_Xml, Program, Slices) :-
   findall( Slice,
      ( member(
           [next_examination(Q2)]-[examination(Q, A)], Program),
        dentalog_question_to_qid(Q_Xml, Q, Qid),
        dentalog_question_to_qid(Q_Xml, Q2, Qid_2),
        concat([Qid, 'a', A], Aid),
        get_num(dentalog_knowledge_slice, N),
        concat('RIND', N, Rid),
        Slice = 'KnowledgeSlice':[
           'ID':Rid, type:'RuleComplex' ]:[
           'Action':[type:'ActionIndication']:[
              'TargetQASets':['QASet':['ID':Qid_2]:[]] ],
           'Condition':[ 
              type:equal, 'ID':Qid, value:Aid]:[] ] ),
      Slices ).

dentalog_question_to_qid(Q_Xml, Q, Qid) :-
   Qid := Q_Xml^'Question'::[^'Text'^content::'*'=[Q]]@'ID'.


/* dentalog_program_to_diagnosis_rules(Q_Xml, Program, Slices) <-
      */

dentalog_program_to_diagnosis_rules(Q_Xml, Program, Slices) :-
   findall( Slice,
      ( member(
           [diagnosis(D)]-[examination(Q, A)], Program),
        dentalog_question_to_qid(Q_Xml, Q, Qid),
        concat([Qid, 'a', A], Aid),
        get_num(dentalog_knowledge_slice, N),
        concat('Rfb', N, Rid),
        concat('P_', D, Did),
        Slice = 'KnowledgeSlice':[
           'ID':Rid, type:'RuleComplex' ]:[
           'Action':[type:'ActionHeuristicPS']:[
              'Score':[value:'P6']:[],
              'Diagnosis':['ID':Did]:[] ],
           'Condition':[
              type:equal, 'ID':Qid, value:Aid]:[] ] ),
      Slices ).


/* dentalog_program_to_value_rules(Q_Xml, Program, Slices) <-
      */

dentalog_program_to_value_rules(Q_Xml, Program, Slices) :-
   findall( Slice,
      ( member([possible_observations(Q, Xs)], Program),
        dentalog_question_to_qid(Q_Xml, Q, Qid),
        member(M-_, Xs),
        concat([Qid, a, M], Aid),
        get_num(dentalog_knowledge_slice, N),
        concat('RADD', N, Rid),
        Slice = 'KnowledgeSlice':[
           'ID':Rid, type:'RuleComplex' ]:[
           'Action':[type:'ActionAddValue']:[
              'Question':['ID':Qid]:[],
              'Values':[
                 'Value':[type:'answerChoice', 'ID':Aid]:[] ] ],
           'Condition':[
              type:equal, 'ID':Qid, value:Aid]:[] ] ),
      Slices ).


/******************************************************************/


