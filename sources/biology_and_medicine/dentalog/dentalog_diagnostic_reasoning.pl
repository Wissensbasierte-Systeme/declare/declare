

/******************************************************************/
/***                                                            ***/
/***         DentaLog: Diagnostic Reasoning                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dentalog_diagnostic_reasoning <-
      */

dentalog_diagnostic_reasoning :-
   dentalog_knowledge_base(DentaLog),
   dislog_variable_get(output_path, Results),
   concat(Results, 'dentalog_kb.xml', File),
   dwrite(xml, File, DentaLog),
   d3_diagnostic_reasoning(DentaLog).


/* dentalog_knowledge_base(DentaLog) <-
      */

dentalog_knowledge_base(DentaLog) :-
   dentalog_file_to_xml(DentaLog).


/******************************************************************/


