

/******************************************************************/
/***                                                            ***/
/***         DentaLog: Knowledge Base to Rule/Goal Graph        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dentalog_knowledge_base_to_rule_goal_graph <-
      */

dentalog_knowledge_base_to_rule_goal_graph :-
   dread(xml, 'examples/dentalog/dentalog_kb.xml', [KB]),
   d3_knowledge_base_to_environment(KB, Es),
   d3_rule_goal_edges_to_picture(Es).


/******************************************************************/


