

/******************************************************************/
/***                                                            ***/
/***          Pathway Analysis:  Model Generation               ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(kegg_analysis, kegg_pathway_to_models_ds) :-
   test(kegg_analysis, kegg_pathway_to_models_ds(3)).

test(kegg_analysis, kegg_pathway_to_models_ds(N)) :-
   kegg_pathway_input_and_edges(N, Input, Edges),
   kegg_pathway_to_models_ds(Input, Edges, Models),
   kegg_pathway_models_show_tree(Models).


/*** interface ****************************************************/


/* kegg_pathway_to_models_ds(Input, Edges, Models_1, Models_2) <-
      */

kegg_pathway_to_models_ds(Input, Edges, Models) :-
   kegg_pathway_to_models_ds(Input, Edges, _, Models, _, _).

kegg_pathway_to_models_ds(
      Input, Edges, Models_1, Models_2, Time_1, Time_2) :-
   measure(
      pathway_to_blocks_ds(Input, Edges, Models_1),
      Time_1 ),
   star_line,
   write_list(['Model Generation: ', Time_1, ' seconds.']),
   dportray(chs, Models_1),
   !,
   pathway_input_and_edges_to_reach_rules(Input, Edges, Rules),
   measure(
      kegg_pathway_models_select_admissible(
         Input, Rules, Models_1, Models_2),
      Time_2 ),
   star_line,
   write_list(['Model Selection: ', Time_2, ' seconds.']),
   dportray(chs, Models_2),
   star_line.


/* pathway_to_blocks_ds(Input, Edges, Models) <-
      */

pathway_to_blocks_ds(Input, Edges, Models) :-
   pathway_edges_to_program_ds(Edges, Rules),
   list_of_elements_to_relation(Input, Facts),
   append(Facts, Rules, Program_1),
   star_line, dportray(lp, Program_1), star_line,
%  partial_evaluation(Program_1, Program_2),
%  pathway_to_block_change_reach_rules(Program_2, Program_3),
%  star_line, dportray(lp, Program_3), star_line,
   program_to_stable_models_dlv(Program_1, Ms),
   maplist( pathway_to_block_model_prune,
      Ms, Models ).


/* pathway_to_block_change_reach_rules(Rules_1, Rules_2) <-
      */

pathway_to_block_change_reach_rules(Rules_1, Rules_2) :-
   findall( Rule_2,
      ( member(Rule_1, Rules_1),
        parse_dislog_rule(Rule_1, Head_1-Body_1-Body),
        pathway_to_block_remove_reach_atoms(Head_1, As, Head_2),
        union(Body_1, As, Body_2),
        Rule_2 = Head_2-Body_2-Body ),
      Rules_2 ).

pathway_to_block_remove_reach_atoms(As_1, As_2, As_3) :-
   sort(As_1, Bs),
   findall( B,
      ( member(B, Bs),
        functor(B, reach, 1) ),
      Cs ),
   findall( not_reach(X),
      ( member(B, Bs),
        B = reach(X) ),
      As_2 ),
   subtract(Bs, Cs, As_3).


/* kegg_pathway_models_select_admissible(
         Input, Rules, Models_1, Models_2) <-
      */

kegg_pathway_models_select_admissible(
      Input, Rules, Models_1, Models_2) :-
   sublist( kegg_pathway_model_test(Input, Rules),
      Models_1, Models_2 ).


/* kegg_pathway_model_test(Input, Rules, Model) <-
      */

kegg_pathway_model_test(Input, Rules, Model) :-
   kegg_pathway_model_to_reach(Rules, Model, Model_2),
   \+ ( member(target(V), Input),
        \+ member(reach(V), Model_2) ).

kegg_pathway_model_to_reach(Rules, Model_1, Model_2) :-
   findall( [block(V)],
      ( member(block(X), Model_1),
%       atom_to_number(X, V),
        V = X ),
      Blocks ),
   append(Rules, Blocks, Program),
   !,
   dislog_variable_get(output_path,
      'tmp_kegg_pathway_model_test_file', File),
   predicate_to_file( File,
      tp_iteration(Program, Model_2) ),
%  tp_iteration(Program, Model_2),
%  writeq(tp_iteration(Program, Model_2)),
   !.


/* pathway_input_and_edges_to_reach_rules(Input, Edges, Rules) <-
      */

pathway_input_and_edges_to_reach_rules :-
   kegg_pathway_input_and_edges(Input, Edges),
   pathway_input_and_edges_to_reach_rules(Input, Edges, Rules),
   dwrite(lp, Rules).

pathway_input_and_edges_to_reach_rules(Input, Edges, Rules) :-
   pathway_edges_to_reach_rules(Edges, Rules_1),
   pathway_input_to_reach_rules(Input, Rules_2),
   append(Rules_1, Rules_2, Rules).

pathway_input_to_reach_rules(Input, Rules) :-
   findall( [reach(V)],
      member(source(V), Input),
      Rules ).

pathway_edges_to_reach_rules(Edges, Rules) :-
   maplist( pathway_edge_to_reach_rule,
      Edges, Rules ).

pathway_edge_to_reach_rule(edge(Vs, W, Es), Rule) :-
   kegg_values_to_atoms(block, Es, Bs),
   kegg_values_to_atoms(~, Bs, As_1),
   kegg_values_to_atoms(reach, Vs, As_2),
   append(As_1, As_2, As),
   Rule = [reach(W)]-As.


/******************************************************************/


