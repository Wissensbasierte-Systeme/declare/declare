

/******************************************************************/
/***                                                            ***/
/***          Pathway Analysis:  Model Generation               ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(kegg_analysis, kegg_pathway_to_models(N)) :-
   kegg_pathway_input_and_edges(N, Input, Edges),
   kegg_pathway_to_models(Input, Edges, Models, Time),
   dportray(chs, Models),
   write_list(user, [Time, ' seconds.\n']),
   star_line,
   kegg_pathway_models_show_tree(Models).


/*** interface ****************************************************/


/* kegg_pathway_to_models(Input, Edges, Models) <-
      */

kegg_pathway_to_models(Input, Edges, Models, Time) :-
   kegg_measure(
      kegg_pathway_to_models(Input, Edges, Models),
      Time ).

kegg_pathway_to_models(Input, Edges, Models) :-
   pathway_to_blocks_iteration(Input, Edges, [], [], Models),
   !.


/* pathway_to_blocks_iteration(
         Input, Edges, Blocked, Models_1, Models_2) <-
      */

pathway_to_blocks_iteration(
      Input, Edges, Blocked, Models_1, Models_2) :-
   pathway_to_blocks(Input, Edges, Blocked, Models),
   ( Models = [] ->
     Models_2 = Models_1
   ; kegg_models_project_on_blocked(Models, Blocked_1),
     append(Blocked, Blocked_1, Blocked_2),
     append(Models_1, Models, Models_3),
     pathway_to_blocks_iteration(
        Input, Edges, Blocked_2, Models_3, Models_2) ).


/* pathway_to_blocks(Input, Edges, Models) <-
      */

pathway_to_blocks(Input, Edges, Blocked, Models) :-
   pathway_edges_to_program(Edges, Blocked, Rules),
   list_of_elements_to_relation(Input, Facts),
   append(Facts, Rules, Program_1),
   star_line, dportray(lp, Program_1), star_line,
   kegg_program_to_stable_models_dlv(Program_1, Ms),
   maplist( pathway_to_block_model_prune,
      Ms, Models ).


/* kegg_measure(Goal, Time) <-
      */

kegg_measure(Goal, Time) :-
   statistics(cputime, Time_1),
   call(Goal),
   statistics(cputime, Time_2),
   Time is Time_2 - Time_1.


/*** implementation ***********************************************/


/* kegg_program_to_stable_models_dlv(Program, Stable_Models) <-
      */

kegg_program_to_stable_models_dlv(Program, Stable_Models) :-
   member(Rule, Program),
   parse_dislog_rule(Rule, [], [], []),
   !,
   Stable_Models = [].
kegg_program_to_stable_models_dlv(Program, Stable_Models) :-
   dislog_variable_get(output_path, 'dlv_program', Path_I),
   dislog_variable_get(output_path, 'dlv_result', Path_O),
   dislog_program_to_dlv_pragram(Program, Program_Dlv),
   dlv_kegg_program_to_file(Program_Dlv, Path_I),
   program_to_stable_models_dlv_file(Path_I, Path_O),
   dlv_kegg_file_to_stable_models(Path_O, Stable_Models).


/******************************************************************/


