

/******************************************************************/
/***                                                            ***/
/***          Pathway Analysis:  Examples                       ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* kegg_pathway_input_and_edges(N, Input, Edges) <-
      */

kegg_pathway_input_and_edges(N, Input, Edges) :-
   ( N = 1,
     Input = [
        source(d), source(e), source(f), source(g), source(j),
        target(a), not_reach(b) ],
     Edges = [
        edge([j],    c, ['3']),
        edge([d],    c, ['1']),
        edge([c],    a, ['4']),
        edge([h, i], b, ['4']),
        edge([g],    i, ['2']),
        edge([e],    h, ['1']),
        edge([f],    h, ['2', '5']) ]
   ; N = 2,
     Input = [
        source(a),
        target(g), not_reach(e) ],
     Edges = [
        edge([a],    b, ['1']),
        edge([b],    c, ['2']),
        edge([b],    d, ['3']),
        edge([c, d], e, ['4']),
        edge([b],    f, ['5']),
        edge([f],    g, ['6']),
        edge([g],    h, ['7']),
        edge([h],    f, ['8']) ]
   ; N = 3,
     Input = [
        source(a), target(d), not_reach(z) ],
     Edges = [
        edge([a],    b, ['0']),
        edge([x],    d, ['1']),
        edge([b],    c, ['2']),
        edge([b],    v, ['110']),
        edge([b],    w, ['111']),
        edge([c],    r, ['114']),
        edge([r, v], u, ['115']),
        edge([u],    v, ['112']),
        edge([w],    u, ['113']),
        edge([u],    z, ['200']),
        edge([z],    x, ['5']),
        edge([x],    e, ['6']),
        edge([x],    k, ['11']),
        edge([k],    x, ['12']),
        edge([c],    x, ['3']) ]
   ; N = 4,
     Input = [
        source(a),
%       target(c),
        not_reach(d) ],
     Edges = [
        edge([a],    b, ['1']),
        edge([b],    c, ['2']),
        edge([c],    b, ['3']),
        edge([b],    d, ['4']) ]
   ; N = 5,
     Input = [
        source(a),
        target(c),
        not_reach(d) ],
     Edges = [
%       edge([c],    g, [8]),
%       edge([g],    c, [9]),
        edge([a],    b, ['1']),
        edge([b],    c, ['2']),
        edge([b],    d, ['3']),
        edge([e],    f, ['6']),
        edge([f],    e, ['7']),
        edge([d],    e, ['4']),
        edge([e],    d, ['5']) ]
   ; N = 6,
     kegg_map_file_to_edges(
        '00010',
        'C00022', 'C00267',
        Edges, Input)
   ; N = 7,
     kegg_map_file_to_edges(
        '00230',
        ['C06194'], ['C00575'], ['C01762'],
        Edges, Input) ).
 

/******************************************************************/


