

/******************************************************************/
/***                                                            ***/
/***          Pathway Analysis:  Edges to Program               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* pathway_edges_to_program(Edges, Rules) <-
      */

pathway_edges_to_program(Edges, Rules) :-
   pathway_edges_to_program(Edges, [], Rules_2),
   sort(Rules_2, Rules).

pathway_edges_to_program(Edges, Blocked, Rules) :-
   pathway_edges_to_vertex_facts(Edges, Facts),
   pathway_edges_to_not_reach_rules(Edges, Rules_1),
   pathway_edges_to_rules(Edges, Rules_2),
   Rules_3 = [
      [reach(V)]-[source(V)],
      []-[not_reach(V), target(V)],
      []-[not_reach(V), source(V)],
%     []-[target(V)]-[reach(V)],
%     []-[source(V)]-[reach(V)],
      []-[not_reach(V), reach(V)],
      [not_reach(V)]-[vertex(V)]-[reach(V)],
      [' :~ block(V). [1:1]'] ],
   ( foreach(Body, Blocked), foreach(Rule_4, Rules_4) do
        Rule_4 = []-Body ),
   append([Facts, Rules_1, Rules_2, Rules_3, Rules_4], Rules) .

pathway_edges_to_program_modified(Edges, Blocked, Rules) :-
   pathway_edges_to_vertex_facts(Edges, Facts),
   pathway_edges_to_not_reach_rules(Edges, Rules_1),
   pathway_edges_to_rules(Edges, Rules_2),
   Rules_3 = [
      [reach(V)]-[source(V)],
%     []-[not_reach(V), target(V)],
%     []-[not_reach(V), source(V)],
      []-[target(V)]-[reach(V)],
      []-[source(V)]-[reach(V)],
      []-[not_reach(V), reach(V)],
%     [not_reach(V)]-[vertex(V)]-[reach(V)],
      [' :~ block(V). [1:1]'] ],
   ( foreach(Body, Blocked), foreach(Rule_4, Rules_4) do
        Rule_4 = []-Body ),
   append([Facts, Rules_1, Rules_2, Rules_3, Rules_4], Rules) .


/* pathway_edges_to_program_ds(Edges, Rules) <-
      */

pathway_edges_to_program_ds(Edges, Rules) :-
   pathway_edges_to_vertex_facts(Edges, Facts),
   pathway_edges_to_not_reach_rules(Edges, Rules_1),
   pathway_edges_to_rules(Edges, Rules_2),
   Rules_3 = [
%     [reach(V)]-[source(V)],
%     [not_reach(V)]-[vertex(V)]-[reach(V)],
      []-[not_reach(V), target(V)],
      []-[not_reach(V), source(V)] ],
   append([Facts, Rules_1, Rules_2, Rules_3], Rules) .


/*** implementation ***********************************************/


/* pathway_edges_to_vertex_facts(Edges, Facts) <-
      */

pathway_edges_to_vertex_facts(Edges, Facts) :-
   setof( [vertex(V)],
      Vs^W^Es^(
         member(edge(Vs, W, Es), Edges),
         ( member(V, Vs)
         ; V = W ) ),
      Facts ).


/* pathway_edges_to_not_reach_rules(Edges, Rules) <-
      */

pathway_edges_to_not_reach_rules(Edges, Rules) :-
   setof( W,
      Vs^Es^member(edge(Vs, W, Es), Edges),
      Ws ),
   maplist( pathway_edges_to_not_reach_rules(Edges),
      Ws, Rss ),
   append(Rss, Rules).

% pathway_edges_to_not_reach_rules(_, _, []) :-
%    !.
pathway_edges_to_not_reach_rules(Edges, W, Rules) :-
   findall( As,
      ( member(edge(Vs, W, Es), Edges),
        kegg_values_to_atoms(block, Es, As_1),
        kegg_values_to_atoms(not_reach, Vs, As_2),
        append(As_1, As_2, As) ),
      State ),
   minimal_models(State, Models),
   ( foreach(Model, Models), foreach(Rule, Rules) do
        Rule = [not_reach(W)]-Model-[source(W)] ).


/* pathway_edges_to_rules(Edges, Rules) <-
      */

pathway_edges_to_rules(Edges, Rules) :-
   maplist( pathway_edge_to_rules,
      Edges, Rss ),
   append(Rss, Rules).

pathway_edge_to_rules(edge(Vs, W, Es), Rules) :-
   kegg_values_to_atoms(block, Es, As_1),
   kegg_values_to_atoms(not_reach, Vs, As_2),
   append(As_1, As_2, As),
   Rule_1 = As-[not_reach(W)],
   kegg_values_to_atoms(reach, Vs, Bs),
   Rule_2 = [reach(W)]-Bs-As_1,
   Rules = [Rule_1, Rule_2].


/******************************************************************/


