

/******************************************************************/
/***                                                            ***/
/***          Pathway Analysis:  Elementary                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* kegg_pathway_models_show_tree(Models) <-
      */

kegg_pathway_models_show_tree(Models) :-
   maplist( kegg_pathway_model_project_on_block,
      Models, Models_2 ),
   state_to_tree(Models_2, Tree),
%  dportray(tree, Tree),
   set_tree_to_picture_bfs(Tree),
   !.


/* kegg_values_to_atoms(Predicate, Values, Atoms) <-
      */

kegg_values_to_atoms(Predicate, Values, Atoms) :-
   foreach(Value, Values), foreach(Atom, Atoms) do
      Atom =.. [Predicate, Value].


/* kegg_pathway_to_models_normalize(Models_1, Models_2) <-
      */

kegg_pathway_to_models_normalize(Models_1, Models_2) :-
   ( foreach(Model_1, Models_1), foreach(Model_A, Models_A) do
        kegg_pathway_model_project_on_block(Model_1, Model),
         sort(Model, Model_A) ),
   sort(Models_A, Models_B),
   !,
   Models_2 = Models_B.


/* pathway_to_block_model_prune(Model_1, Model_2) <-
      */

pathway_to_block_model_prune(Model_1, Model_2) :-
   findall( A,
      ( member(A, Model_1),
        functor(A, F, 1),
        not(member(F, [source, target, vertex])) ),
      Model_2 ).


/* kegg_models_project_on_blocked(Models_1, Models_2) <-
      */

kegg_models_project_on_blocked(Models_1, Models_2) :-
   coin_project_on_predicate(block/1, Models_1, Models_2).


/* kegg_pathway_model_project_on_block(Model, Enzymes) <-
      */

kegg_pathway_model_project_on_block(Model, Enzymes) :-
   findall( Enzyme,
      member(block(Enzyme), Model),
      Enzymes ).


/* kegg_dlv_models_file_to_models(File, Models) <-
      */

kegg_dlv_models_file_to_models(File, Models) :-
   read_models_from_file(File, Models_dlv),
   dislog_program_to_dlv_pragram(Models, Models_dlv).


/******************************************************************/


