

/******************************************************************/
/***                                                            ***/
/***        Pathway Analysis:  Comparison of Methods            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* kegg_pathway_input_and_edges_to_models(Input, Edges) <-
      */

kegg_pathway_input_and_edges_to_models(N) :-
   kegg_pathway_input_and_edges(N, Input, Edges),
   kegg_pathway_input_and_edges_to_models(Input, Edges).

kegg_pathway_input_and_edges_to_models(Input, Edges) :-
   kegg_pathway_to_models(Input, Edges, Models, _),
   ( foreach(Model, Models) do
        pathway_input_and_edges_to_reach_rules(
           Input, Edges, Rules),
        kegg_pathway_model_to_reach(Rules, Model, I),
        visualize_model_with_kegg(I) ).


/* kegg_pathway_to_models_compare(Tuples) <-
      */

kegg_pathway_to_models_compare :-
   kegg_pathway_to_models_compare(Tuples),
   Attributes = ['N', '#1', '#2',
      'time(M1)', 'time(M2)', 'M1=M2'],
   xpce_display_table(Attributes, Tuples).

kegg_pathway_to_models_compare(Tuples) :-
   findall( [N|Tuple],
      ( kegg_pathway_input_and_edges(N, Input, Edges),
        kegg_pathway_to_models_compare(Input, Edges, Tuple) ),
      Tuples ).


/* kegg_pathway_to_models_compare(Input, Edges, Tuple) <-
      */

kegg_pathway_to_models_compare(Input, Edges, Tuple) :-
   kegg_pathway_to_models(Input, Edges, Models_A, Time),
   kegg_pathway_to_models_ds(
      Input, Edges, Models_C, Models_B, Time_1, Time_2),
   length(Models_A, I_A),
   length(Models_B, I_B),
   length(Models_C, I_C),
   ( kegg_pathway_to_models_normalize(Models_A, Models),
     kegg_pathway_to_models_normalize(Models_B, Models) ->
     C = yes
   ; C = no ),
   Time_12 is Time_1 + Time_2,
   Tuple = [I_A, I_B <- I_C,
      Time, Time_12 = Time_1 + Time_2, C].


/******************************************************************/


