

/******************************************************************/
/***                                                            ***/
/***       Pathway Analysis:  Kegg Map to Edges                 ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_get(home,
      '/projects/kegg_analysis/', Path),
   dislog_variable_set(kegg_analysis, Path),
   concat('ftp://ftp.genome.jp/pub/kegg/xml/kgml/',
      'metabolic/ec/ec', Url),
   dislog_variable_set(kegg_ftp_server, Url).


/*** interface ****************************************************/


/* kegg_map_file_to_edges(Map, Edges) <-
      */

kegg_map_file_to_edges(
      Map, Sources, Targets, Notreachs, Edges, Input) :-
   kegg_map_file_to_edges(Map, Edges, _),
   kegg_values_to_atoms(source, Sources, As_1),
   kegg_values_to_atoms(target, Targets, As_2),
   kegg_values_to_atoms(not_reach, Notreachs, As_3),
   append([As_1, As_2, As_3], Input).

kegg_map_file_to_edges(Map, Target, Notreach, Edges, Input) :-
   kegg_map_file_to_edges(Map, Edges, Sources),
   Input = [target(Target), not_reach(Notreach)|Sources].

kegg_map_file_to_edges(Map, Edges, Sources) :-
   dislog_variable_get(kegg_analysis, 'Maps/', Path),
   concat([Path, Map, '.xml'], Path_2),
   dread(xml, Path_2, [Kegg]),
%  kegg_maps_check(Edges_2),
   kegg_to_edges(Kegg, Edges, Sources).
%  append(Edges_1, Edges_2, Edges).


% currently not used

kegg_maps_check(Kegg, Edges) :-
   findall( Relation,
      Relation := Kegg/relation::[@type = maplink],
      Relations ),
   findall( Map,
      ( member(R1, Relations),
        member(R2, Relations),
        A1 := R1@entry1,
        A1 := R2@entry2,
        Entry := Kegg/entry::[@id = A1],
        Name := Entry@name,
        concat('path:eco', Map, Name) ),
      Maps_1 ),
   list_to_set(Maps_1, Maps_2),
   dislog_variable_get(kegg_analysis, 'Maps/', Path),
   findall( Edges_1,
      ( member(M, Maps_2),
        concat([Path, M, '.xml'], File),
        dread(xml, File, [Kegg_2]),
        kegg_to_edges(Kegg_2, Edges_1, _Sources_1) ),
      Edges ).


/* kegg_to_edges(Kegg, Edges, Sources) <-
      */

kegg_to_edges(Kegg, Edges, Sources) :-
   findall( edge(SNames, PName, Enzyme_Names),
      ( Reaction := Kegg/reaction,
        components_to_names(Reaction, SNames, PName),
        Reaction_Name := Reaction@name,
        enzyme_to_names(Kegg, Reaction_Name, Enzyme_Namess),
        member(Enzyme_Names, Enzyme_Namess) ),
      Edges ),
   calculate_sources(Kegg, Edges, Sources).


/* enzyme_to_names(Kegg, Reaction_Name, Enzyme_Namess) <-
      */

enzyme_to_names(Kegg, Reaction_Name, Enzyme_Namess) :-
   findall( Enzyme_Names,
      ( Name := Kegg/entry::[@reaction = Reaction_Name]@name,
        name_split_at_position([" "], Name, Names),
        findall( N,
           ( member(N1,Names),
%            ( concat('ec:', N, N1)
%            ; concat('ko:', N, N1) )
             Substitution = [
                [".", "_"], [":", "_"] ],
             name_exchange_sublist(Substitution, N1, N) ),
           Enzyme_Names ) ),
      Enzyme_Namess ).


/* components_to_names(Reaction, SNames, PName) <-
      */

components_to_names(Reaction, SNames, PName) :-
   findall( Substrate,
      Substrate := Reaction/substrate,
      Substrates ),
   substrates_to_names(Substrates, SNames),
   Name := Reaction/product@name,
   concat('cpd:', PName, Name).
components_to_names(Reaction, SNames, PName) :-
   'reversible' := Reaction@type,
   findall( Substrate,
      Substrate := Reaction/product,
      Substrates ),
   substrates_to_names(Substrates, SNames),
   Name := Reaction/substrate@name,
   concat('cpd:', PName, Name).


/* substrates_to_names(Substrates, Names) <-
      */

substrates_to_names(Substrates, Names) :-
   foreach(Substrate, Substrates), foreach(Name, Names) do
      Name_2 := Substrate@name,
      concat('cpd:', Name, Name_2).


/* calculate_sources(Kegg, Edges, Sources) <-
      */

calculate_sources(Kegg, Edges, Sources) :-
   findall( Entry,
      ( Relation := Kegg/relation::[@type = maplink],
        A1 := Relation/subtype@value,
        Entry := Kegg/entry::[@id = A1]@name ),
      Entries_1 ),
   list_to_set(Entries_1, Entries_2),
   findall( source(V),
      ( member(edge([V], _, _), Edges),
        not(member(edge(_, V, _), Edges) ),
        concat('cpd:', V, E),
        not(member(E, Entries_2)) ),
      Sources_2 ),
   list_to_set(Sources_2, Sources).


/* load_map(Map, Path) <-
      */

load_map(Map, Path) :-
   dislog_variable_get(kegg_ftp_server, Url),
   concat(['wget ', Url, Map, '.xml', ' -O', Path ], Command),
   shell(Command).


/* visualize_with_kegg(Url, Index, Models) <-
      */

visualize_with_kegg(Index, Models) :-
   nth(Index, Models, Model),
   visualize_model_with_kegg(Model).

visualize_model_with_kegg(Model) :-
   Url = 'http://www.kegg.jp/kegg-bin/show_pathway?ec00010',
   visualize_model_with_kegg(Url, Model).

visualize_model_with_kegg(Url, Model) :-
   findall( String_2,
      ( member(Atom, Model),
        ( Atom = block(X),
          concat(ec_, Y, X)
        ; Atom = reach(Y) ),
	string_to_atom(String_1, Y),
        name_exchange_sublist(
           [["_", "."]], String_1, String_2) ),
      Strings ),
   names_append_with_separator(Strings, '+', String),
   concat(['firefox ', Url, '+', String], Command),
   shell(Command).


/******************************************************************/


