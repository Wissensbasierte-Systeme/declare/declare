

/******************************************************************/
/***                                                            ***/
/***        Pathway Analysis:  Dlv File Handling                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dlv_kegg_file_to_stable_models(Path_O, Stable_Models) <-
      */

dlv_kegg_file_to_stable_models(Path_O, Stable_Models) :-
%  dlv_file_add_dot_at_end_of_all_lines(Path_O),
   read_kegg_models_from_file(Path_O, Stable_Models_Dlv),
%  read_kegg_models_from_file_2(Path_O, Stable_Models_Dlv),
   dislog_program_to_dlv_pragram(
      Stable_Models, Stable_Models_Dlv).

read_kegg_models_from_file(File, Models) :-
   concat(File, '2', File_2),
   read_linewise(3, File, File_2),
   read_terms_from_file(File, Terms),
   !,
   maplist( set_structure_to_list,
      Terms, Models ).

read_kegg_models_from_file_2(File, Models) :-
   concat(File, 'temp', Path),
   read_linewise(1, File, Path),
   read_models_from_file(File, Models).


/* dlv_kegg_program_to_file(Program_Dlv, File) <-
      */

dlv_kegg_program_to_file(Program_Dlv, File) :-
   write_dislog_program_to_dlv_program_file(
      Program_Dlv, File),
   concat(File, '_temp', File_temp),
   read_linewise(2, File, File_temp).


/*** implementation ***********************************************/


/* read_linewise(File_1, File_2) <-
      */

read_linewise(Index, File_1, File_2):-
   open(File_1, read, Stream_r, [buffer(line)]),
   open(File_2, append, Stream_w),
   read_one_line_in_loop(Index, Stream_r, Stream_w),
   concat(['mv ', File_2, ' ', File_1], Command),
   shell(Command).

read_one_line_in_loop(1, Stream_r, Stream_w):-
   read_line_to_codes(Stream_r, C),
   append(C, ".\n", NewC),
   write_codes(Stream_w, NewC),
   ( at_end_of_stream(Stream_r) ->
     close(Stream_r),
     close(Stream_w)
   ; read_one_line_in_loop(1, Stream_r, Stream_w) ).

read_one_line_in_loop(2, Stream_r, Stream_w) :-
   read_line_to_codes(Stream_r, Codes_1),
   ( append(X, "].", Codes_1) ->
     append(X, "]", Codes_2)
   ; append(Codes_1, "\n", Codes_2) ),
   write_codes(Stream_w, Codes_2),
   ( at_end_of_stream(Stream_r) ->
     close(Stream_r),
     close(Stream_w)
   ; read_one_line_in_loop(2, Stream_r, Stream_w) ).

read_one_line_in_loop(3, Stream_r, Stream_w):-
   read_line_to_codes(Stream_r, C),
   read_one_line_in_loop(Stream_w, C),
   ( at_end_of_stream(Stream_r) ->
     close(Stream_r),
     close(Stream_w)
   ; read_one_line_in_loop(3, Stream_r, Stream_w) ).

read_one_line_in_loop(4, Stream_r, Stream_w):-
   read_line_to_codes(Stream_r, C),
   append(C, "\n", NewC),
   write_codes(Stream_w, NewC),
   ( at_end_of_stream(Stream_r) ->
     close(Stream_r),
     close(Stream_w)
   ; read_one_line_in_loop(4, Stream_r, Stream_w) ).

read_one_line_in_loop(Stream, Codes) :-
   [C] = "{",
   ( append(_, [C|Cs], Codes) ->
     append([C|Cs], ".\n", Codes_2),
     write_codes(Stream, Codes_2)
   ; true ).

write_codes(Stream, Codes) :-
   string_to_list(String, Codes),
   write(Stream, String).


/* copy_model_from_result_to_temp_result(File) <-
      */

copy_model_from_result_to_temp_result(File) :-
   dislog_variable_get(output_path, 'dlv_result', Path_O),
   open(Path_O, read, Stream_r),
   open(File, append, Stream_w),
   read_one_line_in_loop(4, Stream_r, Stream_w).


/* replace_result_with_temp_result <-
      */

replace_result_with_temp_result :-
   dislog_variable_get(output_path, 'dlv_result', Path),
   concat(['mv ', Path, '_temp ', Path], Command),
   shell(Command).


/* kegg_append_blocked_to_temp_result <-
      */

kegg_append_blocked_to_temp_result :-
   dislog_variable_get(output_path, 'dlv_result_tmp', File),
   copy_model_from_result_to_temp_result(File).


/******************************************************************/


