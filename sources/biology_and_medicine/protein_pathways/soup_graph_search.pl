

/******************************************************************/
/***                                                            ***/
/***             Genetics: Search for Soups                     ***/
/***                                                            ***/
/******************************************************************/


:- module( protein_pathway_graph, [
        soup_node/2,
        soup_edge/2,
        protein_pathway_graph/2,
        protein_pathway_graph/3 ] ).

:- dynamic
      soup_node/2,
      soup_edge/2.

:-  multifile
      soup_node/2,
      soup_edge/2.


/*** interface ****************************************************/


/* protein_pathway_graph(File_Proteins, File_Actions, Graph) <-
      */

protein_pathway_graph(File_Proteins, File_Actions) :-
   dabolish(soup_node/2),
   dabolish(soup_edge/2),
   protein_pathway_graph_common(
      File_Proteins, File_Actions, Dir, S, Soup_2, Actions),
   protein_pathway_graph_(Soup_2, Actions),
   !,
   soup_graph_to_display,
   soup_graph_to_file(Dir, S).

protein_pathway_graph(File_Proteins, File_Actions, Graph) :-
   protein_pathway_graph_common(
      File_Proteins, File_Actions, _, S, Soup_2, Actions),
   protein_pathway_graph_(Soup_2, Actions, Graph),
   !,
   soup_graph_to_display(Graph),
   dwrite(xml, S, Graph).

protein_pathway_graph_common(
      File_Proteins, File_Actions, Dir, S, Soup_2, Actions) :-
   dislog_variable_get(source_path, Sources),
   concat(Sources, 'projects/protein_states/XML/', Dir),
   concat(Dir, File_Proteins, Ps),
   concat(Dir, File_Actions, As),
   concat(Dir, 'graph.xml', S),
   dread(xml, Ps, [Soup]),
   Soup_2 := Soup*[@id:0],
   dread(xml, As, [Actions]).


/* protein_pathway_graph_(Soup, Actions, Graph) <-
      */

protein_pathway_graph_(Soup, Actions) :-
   forall( protein_state_search(Soup, Actions, EndSoup),
      ( N := EndSoup@id,
        assert(soup_node(N, EndSoup)) ) ),
   retractall(user:current_num(current_soup_number, _)).

protein_pathway_graph_(Soup, Actions, Graph) :-
   findall( EndSoup,
      protein_state_search(Soup, Actions, EndSoup),
      Soups ),
   findall( edge:[from:X, to:Y]:[],
      soup_edge(X, Y),
      Edges ),
   retractall(user:current_num(current_soup_number, _)),
   retractall(soup_edge(_, _)),
   Graph = graph:[nodes:Soups, edges:Edges].


/* soup_graph_to_file(Dir, File) <-
      */

soup_graph_to_file(Dir, File) :-
   findall( Node,
      ( soup_node(N, Xml),
        number_to_two_digits(N, M),
        concat([Dir, 'nodes/', M, '.xml'], F),
        Node = node:[id:N, file:F]:[],
        dwrite(xml, F, Xml) ),
      Nodes ),
   findall( edge:[from:V, to:W]:[],
      soup_edge(V, W),
      Edges ),
   write_list(user, ['---> ', File, '\n']),
   Graph = graph:[nodes:Nodes, edges:Edges],
   dwrite(xml, File, Graph).


/*** implementation ***********************************************/


/* protein_state_search(Soup_1, Actions_1, Soup_2) <-
      done if no new actions. */

protein_state_search(Soup_1, Actions_1, Soup_2) :-
%  announce_soup('process', Soup_1),
   ( Soup_2 = Soup_1
   ; early_non_soup_node_subsumes_test(Soup_1),
     find_next_situation_(Soup_1, Actions_1, Soup, Actions),
     protein_state_search(Soup, Actions, Soup_2) ).

announce_soup(Message, Soup) :-
   ID := Soup@id,
   write_list(user, [Message, ' ', ID, ': ']).

early_non_soup_node_subsumes_test(Soup) :-
   M := Soup@id,
   \+ soup_node_subsumes(_, M).


/* find_next_situation_(Soup_1, Actions_1, Soup_2, Actions_2) <-
      */

find_next_situation_(Soup_1, Actions_1, Soup_2, Actions_2) :-
   find_next_soup_(Soup_1, Actions_1, Soup_2, Actions),
   find_next_actions_(Soup_2, Actions, Actions_2).


/* find_next_soup_(Soup_1, Actions_1, Soup_2, Actions_2) <-
      */

find_next_soup_(Soup_1, Actions_1, Soup_2, Actions_2) :-
   Action := Actions_1^(action::[@id=Id, ^status@state=todo]),
%  dwrite(xml, Action),
%  write(user, action(Id)),
   ID_1 := Soup_1@id,
   write_list(user, [ID_1, ' * ']),
   Type := Action@type,
   SubType := Action@subtype,
   Perform =.. [SubType, Soup_1, Action, Soup],
   once(call(Type:Perform)),
   Actions_2 := Actions_1 * [^action::[@id=Id]^status@state:done],
%  Actions_2 := Actions_1 * [^action^status@state:done],
   remember_search_edge_(Soup_1, Soup, Soup_2).

remember_search_edge_(Soup_1, Soup, Soup_2) :-
   M := Soup_1@id,
   soup_to_number_(Soup, N),
   Soup_2 := Soup * [@id:N],
   assert(soup_edge(M, N)),
   write(user, M-->N),
   !.


/* find_next_actions_(Soup, Actions, Actions_New) <-
      */

find_next_actions_(Soup, Actions, Actions_New) :-
   findall( Action,
      ( Protein := Soup^proteins^protein,
        protein:check_action(Protein, Action),
%       check for circularity
        \+ performed(Action, Soup),
        \+ action_already_found(Actions, Action) ),
      Candidates_2 ),
   sort(Candidates_2, Candidates),
   add_candidate_actions(Candidates, Actions, Actions_New).

action_already_found(Actions, Action) :-
   Action_2 := Action * [@id:_],
   Action_2 := Actions^action.

add_candidate_actions(Candidates, Actions, Actions_New) :-
%  writeln(user, add_candidate_actions),
   length(Candidates, L),
   write_list(user, [' (', L, ' new actions)\n']),
   generate_interval(1, L, Ns),
   action_count_(Actions, Count),
   pair_lists(Ns, Candidates, Pairs),
   ( foreach([N, Action_1], Pairs), foreach(Action_2, Actions_2) do
        Count_2 is Count + N,
        Action_2 := Action_1 * [@id:Count_2] ),
   Actions_New := Actions <+> Actions_2,
   !.

action_count_(Actions, 0) :-
   \+ (_ := Actions^action),
   !.
action_count_(Actions, Count) :-
   findall( N,
      ( ID := Actions^action@id,
%       atom_chars(ID, Cs),
%       number_chars(N, Cs),
        term_to_atom(N, ID) ),
      Counts ),
   sort(Counts, Counts_Sorted),
   last(Counts_Sorted, Count),
   !.

performed(Action, Soup) :-
   TestAction := Soup^pathway^step^action,
%  dwrite(xml, Action),
   action:identical(Action, TestAction).


/* soup_to_number_(Soup, N) <-
      */

soup_to_number_(_, N) :-
   get_num(current_soup_number, N).
%  get_num(current_soup_number, M).
%  term_to_atom(M, N).


/******************************************************************/


