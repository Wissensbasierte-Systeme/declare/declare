

/******************************************************************/
/***                                                            ***/
/***             Protein State Search: Tests                    ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(genetics:pathway, 1) :-
   dislog_variable_get(source_path, Sources),
   concat(Sources, 'projects/protein_states/XML/', Dir),
   concat(Dir, 'proteins.xml', Ps),
%  concat(Dir, 'start.xml', As),
   concat(Dir, 'actions.xml', As),
   dread(xml, Ps, [Soup]), 
   dread(xml, As, [Actions]),
   Action := Actions^action, 
   Proteins = [ligand, receptor], 
   maplist( pathway:get_protein_state_(Soup),
      Proteins, Protein_States ), 
   Soup_2 := Soup <+>
      [^pathway^step:[count:1]:[Action,proteins:[]:[]]], 
   Soup_3 := Soup_2 <+>
      [^pathway^step::[@count=1]^proteins:[]:[Protein_States]],
   Pathway := Soup_3^pathway, 
   dwrite(xml, Pathway).


/******************************************************************/


