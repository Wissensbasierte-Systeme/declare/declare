

/******************************************************************/
/***                                                            ***/
/***          Genetics: Visualization of Soup Graphs            ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(soup_graph_mode, simplified).


/*** interface ****************************************************/


/* soup_graph_to_display(Graph) <-
      */

soup_graph_to_display :-
   ( dislog_variable_get(soup_graph_mode, simplified) ->
     soup_graph_simplify
   ; dislog_variable_get(soup_graph_mode, regular) ),
   soup_graph_to_vertices_edges(Vertices, Edges),
   vertices_edges_to_gxl(Vertices, Edges, Gxl),
   gxl_tree_layout(bfs, ['0'], Gxl, Gxl_2),
%  gxl_to_picture(Gxl_2).
   gxl_to_xpce:gxl_to_picture(Gxl_2).

soup_graph_to_display(Graph) :-
   ( dislog_variable_get(soup_graph_mode, simplified) ->
     soup_graph_simplify(Graph, Graph_2)
   ; dislog_variable_get(soup_graph_mode, regular),
     Graph_2 = Graph ),
   dabolish(soup_graph/1),
   dwrite(xml, 'results/proteins.xml', Graph_2),
   assert(soup_graph(Graph_2)),
   soup_graph_to_vertices_edges(Graph_2, Vertices, Edges),
   vertices_edges_to_gxl(Vertices, Edges, Gxl),
%  vertices_edges_to_gxl_graph(Vertices, Edges, Gxl),
   gxl_tree_layout(bfs, ['0'], Gxl, Gxl_2),
%  gxl_to_picture(Gxl_2).
   gxl_to_xpce:gxl_to_picture(Gxl_2).


/* soup_graph_to_vertices_edges(Graph, Vertices, Edges) <-
      */

soup_graph_to_vertices_edges(Vertices, Edges) :-
   findall( Vertex,
      soup_node(Vertex, _),
      Vertices ),
   findall( V-W,
      soup_edge(V, W),
      Edges ).

soup_graph_to_vertices_edges(Graph, Vertices, Edges) :-
   findall( Vertex,
      Vertex := Graph^nodes^node@id,
      Vertices ),
   findall( V-W,
      _ := Graph^edges^edge::[@from=V, @to=W],
      Edges ).

double_term_to_atom(Term, Atom) :-
   term_to_atom(T, Atom),
   term_to_atom(Term, T).


/* xpce_mouse_click(middle, Node_Atom) <-
      */

xpce_mouse_click(middle, Node_Atom) :-
   atom_to_term(Node_Atom, Node_Term, _),
%  writeln(user, Node_Atom),
   N := Node_Term@id,
   atom_to_term(N, M, _),
%  writeln(user, M),
   soup_edit(M).


/* soup_edit(N) <-
      */

soup_edit(N) :-
   soup_node(N, Node),
   soup_edit_common(N, Node).
soup_edit(N) :-
   soup_graph(Graph),
   Node := Graph^nodes^node::[@id=N],
   soup_edit_common(N, Node).

soup_edit_common(N, Node) :-
   Proteins_1 := Node^proteins^content::'*',
   findall( Protein,
      ( Step := Node^pathway^step,
        step_to_protein(Step, Protein) ),
      Proteins_2 ),
   append(Proteins_1, Proteins_2, Proteins),
   Xml = proteins:[]:Proteins,
   soup_xml_to_html(Xml, N, Table),
   dwrite(xml, 'results/soup.xml', Node),
%  edit('results/soup.xml'),
   dislog_variable_get(output_path, Results),
   concat(Results, 'proteins.html', File),
   predicate_to_file( File,
      field_notation_to_xml(Table) ),
   html_file_to_display(File).

step_to_protein(Step, Protein) :-
   Count := Step@count,
   Action := Step^action,
   ( Count = 1 ->
     Name = pathway
   ; Name = '' ),
   Protein = protein:[name:Name, sequence:step]:[
      action_states:[state:[location:Count]:[actions:[Action]]]].

step_to_protein(Step, Protein) :-
   Protein := Step^proteins^protein.


/******************************************************************/


