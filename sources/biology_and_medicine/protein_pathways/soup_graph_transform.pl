

/******************************************************************/
/***                                                            ***/
/***         Genetics: Simplification of Soup Graphs            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* soup_graph_simplify(Graph_1, Graph_2) <-
      */

soup_graph_simplify :-
   soup_nodes_non_redundant(Pairs),
   findall( soup_node(Id, Node_2),
      ( soup_node(Id, Node_1),
        Proteins := Node_1^proteins,
        member(Id-Ids, Pairs),
        soup_graph_collect_pathways([Id|Ids], Steps),
        Node_2 = node:[id:Id]:[
           Proteins, pathway:Steps] ),
      Nodes_2 ),
   findall( soup_edge(F2, T2),
      ( soup_edge(F1, T1),
        node_id_normalize(Pairs, F1, F2),
        node_id_normalize(Pairs, T1, T2) ),
      Edges ),
   sort(Edges, Edges_2),
   dabolish(soup_node/2),
   dabolish(soup_edge/2),
   assert_all(Nodes_2),
   assert_all(Edges_2).

soup_graph_simplify(Graph_1, Graph_2) :-
   soup_nodes_non_redundant(Graph_1, Pairs),
   findall( Node_2,
      ( Proteins := Graph_1^nodes^node::[@id=Id]^proteins,
        member(Id-Ids, Pairs),
        soup_graph_collect_pathways(Graph_1, [Id|Ids], Steps),
        Node_2 = node:[id:Id]:[
           Proteins, pathway:Steps] ),
      Nodes_2 ),
   findall( edge:[from:F2, to:T2]:[],
      ( _ := Graph_1^edges^edge::[@from=F1, @to=T1],
        node_id_normalize(Pairs, F1, F2),
        node_id_normalize(Pairs, T1, T2) ),
      Edges ),
   sort(Edges, Edges_2),
   Graph_2 = graph:[]:[nodes:Nodes_2, edges:Edges_2].


/*** implementation ***********************************************/


/* soup_graph_collect_pathways(Graph, Ids, Steps) <-
      */

soup_graph_collect_pathways(Ids, Steps) :-
   maplist( soup_graph_to_pathway,
      Ids, S ),
   append(S, Steps).

soup_graph_collect_pathways(Graph, Ids, Steps) :-
   maplist( soup_graph_to_pathway(Graph),
      Ids, S ),
   append(S, Steps).

soup_graph_to_pathway(Id, Steps) :-
   soup_node(Id, Node),
   Steps := Node^pathway^content::'*'.

soup_graph_to_pathway(Graph, Id, Steps) :-
   Steps := Graph^nodes^node::[@id=Id]^pathway^content::'*'.

 
/* node_id_normalize(Pairs, X, Y) <-
      */

node_id_normalize(Pairs, X, Y) :-
   member(Y-Xs, Pairs),
   member(X, Xs),
   !.
node_id_normalize(_, X, X).


/* soup_nodes_non_redundant(Graph, Pairs) <-
      */

soup_nodes_non_redundant(Pairs) :-
   soup_nodes_redundant(Redundant),
   findall( Id-Ids,
      ( soup_node(Id, _),
        \+ member(Id, Redundant),
        soup_node_to_subsumed_nodes(Id, Ids) ),
      Pairs_2 ),
   sort(Pairs_2, Pairs),
   soup_nodes_non_redundant_message(Redundant, Pairs).

soup_nodes_non_redundant(Graph, Pairs) :-
   soup_nodes_redundant(Graph, Redundant),
   findall( Id-Ids,
      ( Id := Graph^nodes^node@id,
        \+ member(Id, Redundant),
        soup_node_to_subsumed_nodes(Graph, Id, Ids) ),
      Pairs_2 ),
   sort(Pairs_2, Pairs),
   soup_nodes_non_redundant_message(Redundant, Pairs).

soup_nodes_non_redundant_message(Redundant, Pairs) :-
   star_line,
   findall( Id-Ids,
      ( member(Id-Ids, Pairs),
        Ids \= [] ),
      Contraction ),
   writeln(user, contraction = Contraction),
   length(Redundant, Red),
   length(Pairs, Non_Red),
   round(Non_Red / (Non_Red + Red), 2, X),
   writeln(user, factor = X).


/* soup_node_to_subsumed_nodes(Graph, N, Ms) <-
      */

soup_node_to_subsumed_nodes(N, Ms) :-
   findall( M,
      soup_node_subsumes(N, M),
      Ms ).

soup_node_to_subsumed_nodes(Graph, N, Ms) :-
   findall( M,
      soup_node_subsumes(Graph, N, M),
      Ms ).


/* soup_nodes_redundant(Graph, Ids) <-
      */

soup_nodes_redundant(Ids) :-
   findall( Id,
      ( soup_node(Id, _),
        soup_node_subsumes_once(Id) ),
      Ids_2 ),
   sort(Ids_2, Ids).
   
soup_nodes_redundant(Graph, Ids) :-
   findall( Id,
      ( Id := Graph^nodes^node@id,
        soup_node_subsumes_once(Graph, Id) ),
      Ids_2 ),
   sort(Ids_2, Ids).
   
soup_node_subsumes_once(M) :-
   soup_node_subsumes(_, M),
   !.

soup_node_subsumes_once(Graph, M) :-
   soup_node_subsumes(Graph, _, M),
   !.


/* soup_node_subsumes(Graph, N, M) <-
      */

soup_node_subsumes(N, M) :-
   soup_node(N, Node_1),
   soup_node(M, Node_2),
   N < M,
   soup_nodes_are_equivalent(Node_1, Node_2).

soup_node_subsumes(Graph, N, M) :-
   Node_1 := Graph^nodes^node::[@id=N],
   Node_2 := Graph^nodes^node::[@id=M],
   N < M,
   soup_nodes_are_equivalent(Node_1, Node_2).

soup_nodes_are_equivalent(Node_1, Node_2) :-
   soup_node_normalize(Node_1, N1),
   soup_node_normalize(Node_2, N2),
   !,
   N1 = N2.


/* soup_node_normalize(Node, Normal_Node) <-
      */

soup_node_normalize(Node, Normal_Node) :-
   findall( State,
      ( S := Node^proteins^protein^current_state^state,
        soup_protein_current_state_normalize(S, State) ),
      States ),
   Normal_Node = node:States,
   !.

soup_protein_current_state_normalize(State_1, State_2) :-
%  writeln('State_1' = State_1),
   L := State_1@location,
   Ms := State_1^modifications,
   fn_item_parse(Ms, modifications:_:Xs),
   sort(Xs, Ys),
   Is := State_1^interactions,
   fn_item_parse(Is, interactions:_:Us),
   sort(Us, Vs),
   State_2 = state:[location:L]:[
      modifications:Ys, interactions:Vs],
%  writeln('State_2' = State_2),
   !.


/******************************************************************/


