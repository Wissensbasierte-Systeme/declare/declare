

/******************************************************************/
/***                                                            ***/
/***             Genetics: Soups in HTML                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* soup_xml_to_html(Xml, N, Table) <-
      */

soup_xml_to_html(Xml, N, Table) :-
   concat('Node #', N, Title),
   findall( Html,
      ( Protein := Xml^protein,
        protein_to_html(Protein, Html) ),
      Htmls ),
   Rows = [tr:[]:[td:['BGCOLOR':white]:N] | Htmls],
   Table =
      html:[]:[
         head:[]:[title:Title],
         body:[]:[
            table:[
               'BORDER':1, cellspacing:1,
               'BGCOLOR':white]:Rows]].


/*** implementation ***********************************************/


/* protein_to_html(Protein, Rows) <-
      */

protein_to_html(Protein, Rows) :-
%  writeln(user, protein_to_html),
   Name := Protein@name,
   ( ( Sequence := Protein@sequence,
       ! )
   ; Sequence = '*' ),
   findall( Html,
      ( ( State := Protein^current_state^state
        ; State := Protein^state ),
        state_to_html('#dddddd', State, Html) ),
      Htmls_current ),
   append(Htmls_current, Rows_current),
   findall( Html,
      ( State := Protein^action_states^state,
        state_to_html('#9999ff', State, Html) ),
      Htmls_action ),
   append(Htmls_action, Rows_action),
   findall( Html,
      ( State := Protein^forbidden_states^state,
        state_to_html('#ff8888', State, Html) ),
      Htmls_forbidden ),
   append(Htmls_forbidden, Rows_forbidden),
   append([Rows_current, Rows_action, Rows_forbidden], Rows_all),
   Rows = [tr:[]:[
      td:['BGCOLOR':'white']:Name,
      td:['BGCOLOR':'white']:Sequence]|Rows_all].

state_to_html(Color, State, Html) :-
   findall( Modification_Html,
      ( Modification := State^modifications^modification,
        modification_to_html(Modification, Modification_Html) ),
      Modification_Htmls ),
   findall( Interaction_Html,
      ( Interaction := State^interactions^interaction,
        interaction_to_html(Interaction, Interaction_Html) ),
      Interaction_Htmls ),
   findall( Action_Html,
      ( Action := State^actions^action,
        action_to_html(Action, Action_Html) ),
      Action_Htmls ),
   append(
      [Modification_Htmls, Interaction_Htmls, Action_Htmls],
      State_Html ),
   ( ( Location := State@location,
       ! )
   ; Location = '*' ),
   Html = [tr:[]:[
      td:'', td:['BGCOLOR':Color]:Location]|State_Html].

modification_to_html(Modification, Html) :-
   Position := Modification@position,
   Type := Modification@type,
   Html = tr:[
      td:'', td:'', td:modification, td:Position, td:Type].

interaction_to_html(Interaction, Html) :-
   Protein := Interaction@protein,
   Html = tr:[]:[
      td:'', td:'', td:interaction, td:Protein].

action_to_html(Action, Html) :-
   Type := Action@type,
   Subtype := Action@subtype,
   !,
   findall( Parameter,
      ( Parameter := Action^protein@name
      ; Parameter := Action^parameter@location
      ; Parameter := Action^parameter@protein
      ; Parameter := Action^status@state ),
      Parameters_2 ),
   ( list_to_comma_structure(Parameters_2, Parameters_3),
     term_to_atom(Parameters_3, Parameters)
   ; Parameters = '' ),
   Html = tr:[]:[
      td:'', td:'',
      td:action, td:Type, td:Subtype, td:Parameters].
action_to_html(action:[]:[], Html) :-
   Html = tr:[]:[
      td:'', td:'', td:action].


/*** tests ********************************************************/


test(genetics, action_to_html, 1) :-
   Action =
      action:[type:interaction, subtype:binding]:[
         protein:[name:jak]:[],
         parameter:[location:cytoplasma]:[],
         parameter:[protein:receptor, position:2]:[],
         status:[state:todo]:[] ],
   action_to_html(Action, Html),
   dwrite(xml, Html).


/******************************************************************/


