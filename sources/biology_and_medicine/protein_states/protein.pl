

:- module(protein, []).


/* get_location(Protein, Soup, Loc) <-
      */

get_location(Protein, Soup, Loc) :-
   Loc := Soup^proteins^protein::[@name=Protein]
      ^current_state^state@location.


/* set_location(Protein, Soup_1, Loc, Soup_2) <-
      */

/*
set_location(Protein, Soup_1, Loc, Soup_2) :-
   protein:get_interactions(Protein, Soup_1, Int),
   move_bound_(Soup_1, Loc, Int, [], Soup),
   protein:set_location_xml(Protein, Soup, Loc, Soup_2).
*/

set_location(Protein, Soup_1, Loc, Soup_2) :-
   move_bound_(Soup_1, Loc, [Protein], [], Soup_2).

set_location_xml(Protein, Soup_1, Loc, Soup_2) :-
   Soup_2 := Soup_1 * [
      ^proteins^protein::[@name=Protein]
      ^current_state^state@location:Loc].

move_bound_(Soup, _, [], _, Soup).
%  dwrite(xml, Soup), nl.

% what about proteins bound to the interactor??
move_bound_(Soup, Loc, [Protein|Proteins], Moved, EndSoup) :-
   protein:get_location(Protein, Soup, CurrentLocation),
   localisation:allowed_move(CurrentLocation, Loc),
   protein:set_location_xml(Protein, Soup, Loc, NewSoup),
   protein:get_interactions(Protein, Soup, Int),
   subtract(Int, Moved, NewMovers),
   flatten([Proteins|NewMovers], NewMoversList),
   move_bound_(NewSoup, Loc, NewMoversList, [Protein|Moved], EndSoup).


/* get_interactions(Protein, Soup, Int) <-
      */

get_interactions(Protein,Soup,Int) :-
   State := Soup^proteins
      ^protein::[@name=Protein]^current_state^state,
   state:get_interactions(State, Int).


/* add_interaction(Soup_1, [Protein_1, Protein_2], Soup_2) <-
      assure reciprocity of interactions. */

/* homodimer */
add_interaction(Soup_1, [Protein_1, Protein_1], Soup_2) :-
   protein:add_interaction(Protein_1, Soup_1, Protein_1, Soup_2).

add_interaction(Soup_1, [Protein_1, Protein_2], Soup_2) :-
   protein:add_interaction(Protein_1, Soup_1, Protein_2, Soup),
   protein:add_interaction(Protein_2, Soup, Protein_1, Soup_2).


/* add_interaction(Protein, Soup, Int, NewSoup) <-
      */

add_interaction(Protein, Soup, Int, NewSoup) :-
   NewSoup := Soup <+> [
      ^proteins^protein::[@name=Protein]^current_state^state
      ^interactions^interaction:[protein:Int]:[] ].

/* delete_interaction(Soup_1, [Protein_1, Protein_2], Soup_2) <-
      break up an interaction
      */

delete_interaction(Soup_1, [Protein_1, Protein_2], Soup_2) :-
   protein:delete_interaction(Protein_1, Soup_1, Protein_2, Soup),
   protein:delete_interaction(Protein_2, Soup, Protein_1, Soup_2).

/* delete_interaction(Protein, Soup_1, Int, Soup_2) <-
      can I make this an internal function?
     */

delete_interaction(Protein, Soup_1, Int, Soup_2) :-
   Soup_2 := Soup_1 <-> [
      ^proteins^protein::[@name=Protein]^current_state^state
      ^interactions^interaction::[@protein=Int]].

/* get_modification(Protein, Soup_1, Position, ModAA) <-
     */

get_modification(Protein, Soup_1, Position, ModAA) :-
   _ := Soup_1^proteins^protein::[@name=Protein]^current_state^state
      ^modifications^modification::[@position=Position, @type=ModAA].


/* add_modification(Protein, Soup_1, ModAA, Position, Soup_2) <-
     */

add_modification(Protein, Soup_1, ModAA, Position, Soup_2) :-
   Soup_2 := Soup_1 <+> [
      ^proteins^protein::[@name=Protein]^current_state^state
      ^modifications^modification:[position:Position, type:ModAA]:[] ].

/* delete_modification(Protein, Soup_1, ModAA, Position, Soup_2) <-
     */

delete_modification(Protein, Soup_1, ModAA, Position, Soup_2) :-
   Soup_2 := Soup_1 <-> [
      ^proteins^protein::[@name=Protein]^current_state^state
      ^modifications^modification::[@position=Position, @type=ModAA]].

/* get_sequence(Protein, Soup, Sequence) <-
     */

get_sequence(Protein, Soup, Sequence) :-
   Sequence := Soup^proteins^protein::[@name=Protein]@sequence.

/* get_sequence_position(Protein, Soup, Position, AA) <-
     */

get_sequence_position(Protein, Soup, PositionText, AA) :-
   get_sequence(Protein, Soup, Sequence),
   term_to_atom(Position, PositionText),
   StringPos is Position - 1,
   string_to_atom(String,Sequence),
   sub_string(String,StringPos,1,_,StringAA),
   string_to_atom(StringAA,AA).


/* check_action(Protein, Action) <-
      at the moment, only one action is deliverd
      -> we are doing depth first searches. */

check_action(Protein, Action) :-
   CurrentState := Protein^current_state^state,
   ActionState := Protein^action_states^state,
%  CurrentState has to be more specific than ActionState
   state:protein_state_subsumes(CurrentState, ActionState),
   Action := ActionState^actions^action.


/* forbidden_state(Proteins, Soup) <-
      check whether any of the Proteins is in a forbidden state */

forbidden_state(Proteins, Soup) :-
   member(Protein, Proteins),
   forbidden_state_protein(Protein,Soup),
%  if there is a forbidden state, stop search
   !.

forbidden_state_protein(ProteinName, Soup) :-
   Protein := Soup^proteins^protein::[@name=ProteinName],
   CurrentState := Protein^current_state^state,
   ForbiddenState := Protein^forbidden_states^state,
   state:protein_state_subsumes(CurrentState, ForbiddenState).
%  state:forbidden_subsumes(CurrentState, ForbiddenState).
   

