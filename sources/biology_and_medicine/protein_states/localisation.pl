

:- module(localisation, []).


/* localisation(Localisation) <-
      Here we can define the logic of localisation (ontology)
      allowed localisations. */

localisation(localisation).
localisation(intra).
localisation(extra).
localisation(membrane).
localisation(transmembrane).
localisation(cytoplasma).
localisation(nucleus).
localisation(mitochondrion).
localisation(nucleolus).


/* isa(X, Y) <-
      hirarchy of the localisation. */

isa(localisation, intra).
isa(localisation, extra).
isa(localisation, membrane).
isa(transmembrane, membrane).
isa(intra, cytoplasma).
isa(intra, nucleus).
isa(intra, mitochondrion).
isa(nuclear, nucleolus).


/* contains(X, Y) <-
      */

contains(transmembrane, cytoplasma).
contains(transmembrane, extra).
contains(transmembrane, membrane).


/* allowed_move(From, To) <-
      */

allowed_move(From, To) :-
   ( allowed(From, To)
   ; allowed(To, From) ).


/* allowed(X, Y) <-
      allowed_moves
      This should become a rule, not a fact collection. */

allowed(intra, extra).
allowed(cytoplasma, nucleus).
allowed(X, X).


/* allowed_interaction(Loc1, Loc2, LocNew) <-
      checks wether an interaction between proteins
      in Loc1 and Loc2 is permitted.
      LocNew is the most specific localisation of both */

allowed_interaction(Loc, Loc, Loc, Loc) :-
   localisation(Loc).

allowed_interaction(Loc1, Loc2, Loc1_2, Loc2_2) :-
   localisation(Loc1),
   localisation(Loc2),
   specifies(Loc1, Loc2, Loc),
   Loc1_2 = Loc,
   Loc2_2 = Loc.

/* following clause allows interactions between tm and other proteins
       */
allowed_interaction(Loc1, Loc2, Loc1, Loc2) :-
   ( contains(Loc1, Loc2)
   ; contains(Loc2, Loc1) ).


/* specifies(L1, L2) <-
      */

specifies(L1, L2) :-
   specifies(L1, L2, L2).


/* specifies(L1, L2, L) <-
      */

% localisations are identical
specifies(L, L, L).

% L2 is more specific than L1
specifies(L1, L2, L2) :-
   isa(L1, L2),
   !. 
specifies(L1, L2, L2) :-
   isa(L1, L),
   specifies(L, L2, L2).

% L1 is more specific than L2
specifies(L1, L2, L1) :-
   specifies(L2, L1, L1).


