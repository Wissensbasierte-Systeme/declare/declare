

:- module(moving, []).


/* moving(Soup_1, Action, Soup_2) <-
      */

moving(Soup_1, Action, Soup_2) :-
   Protein := Action^protein@name,
   NewLocation := Action^parameter@location,
%  writeln('moving'),
   protein:get_location(Protein, Soup_1, CurrentLocation),
   localisation:allowed_move(CurrentLocation, NewLocation),
%  check all bound proteins, move if possible
   protein:set_location(Protein, Soup_1, NewLocation, Soup_A),
   protein:get_interactions(Protein, Soup_1, Int),
   \+ protein:forbidden_state([Protein|Int], Soup_A),
   pathway:add(Soup_A, Action, [Protein|Int], Soup_2).


