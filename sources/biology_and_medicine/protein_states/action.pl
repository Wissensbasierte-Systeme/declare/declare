

:- module(action, []).


/* identical(Action_1, Action_2) <-
      */

identical(Action_1, Action_2) :-
   fn_get_identical(@type, Action_1, Action_2, Type),
   fn_get_identical(@subtype, Action_1, Action_2, SubType),
   fn_get_identical(^protein@name, Action_1, Action_2),
   identical(Type, SubType, Action_1, Action_2).

identical(moving, moving, Action_1, Action_2) :-
   fn_get_identical(^parameter@location, Action_1, Action_2).

identical(interaction, binding, Action_1, Action_2) :-
   fn_get_identical(^parameter@protein, Action_1, Action_2).


