

:- module(pathway, []).


/* get_pathway(Soup, Pathway) <-
      */

get_pathway(Soup, Pathway) :-
   Pathway := Soup^pathway.


/* add(Soup_1, Action, Proteins, Soup_2) <-
      */

add(Soup_1, Action, Proteins, Soup_2) :-
   step_count_(Soup_1, OldCount),
   Count is OldCount + 1,
   Soup := Soup_1 <+> [^pathway^step:[count:Count]:[Action]],
%  for each Protein in Proteins, the current state has to be added
   maplist( get_protein_state_(Soup_1),
      Proteins, Protein_States ),
%  write(Protein_States),
   Soup_2 := Soup <+>
      [^pathway^step::[@count=Count]^proteins:[]:[Protein_States]].

get_protein_state_(Soup, Protein, Protein_State) :-
   State :=
      Soup^proteins^protein::[@name=Protein]^current_state^state,
   Protein_State = protein:[name:Protein]:[State].


/* step_count_(Soup, Count) <-
      Get the maximal currently used step count;
      0, if no step exists. */

step_count_(Soup, 0) :-
   \+ (_ := Soup^pathway^step).
step_count_(Soup, Count) :-
   findall( Count,
      ( C1 := Soup^pathway^step@count,
%       atom_chars(C1, C2),
%       number_chars(Count, C2),
        term_to_atom(Count, C1) ),
      Counts ),
   sort(Counts, Counts_Sorted),
   last(Counts_Sorted, Count),
   !.


