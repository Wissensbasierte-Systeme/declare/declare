

:- module(interaction, []).


/* binding(Soup_1, Action, Soup_2) <-
      */

binding(Soup_1, Action, Soup_2) :-
   Protein_1 := Action^protein@name,
   Protein_2 := Action^parameter@protein,
   Proteins = [Protein_1, Protein_2],
   unbound(Soup_1, Proteins),
   matching_location_(Soup_1, Proteins, Soup_A),
   protein:add_interaction(Soup_A, Proteins, Soup_B),
   !,
   \+ protein:forbidden_state(Proteins, Soup_B),
   pathway:add(Soup_B, Action, Proteins, Soup_2).


/* dissociate(Soup_1,Action,Soup_2) <-
   two proteins which are bound dissociate
      */

dissociate(Soup_1,Action,Soup_2) :-
   Protein_1 := Action^protein@name,
   Protein_2 := Action^parameter@protein,
   Proteins = [Protein_1, Protein_2],
   bound(Soup_1, Proteins),
   protein:delete_interaction(Soup_1, Proteins, Soup_A),
   !,
   \+ protein:forbidden_state(Proteins, Soup_A),
   pathway:add(Soup_A, Action, Proteins, Soup_2).


/* unbound(Soup, [Protein_1, Protein_2]) <-
      */

unbound(Soup, Proteins) :-
   \+ bound(Soup,Proteins).

/* bound(Soup, [Protein_1, Protein_2]) <-
   check, whether two proteins are interacting
      */

bound(Soup, [Protein_1, Protein_2]) :-
   protein:get_interactions(Protein_1, Soup, Int1),
   protein:get_interactions(Protein_2, Soup, Int2),
   bound_list_(Protein_1, Int2),
   bound_list_(Protein_2, Int1).


bound_list_(Protein, Int) :-
   memberchk(Protein, Int).


/* matching_location_(Soup_A, [Protein_1, Protein_2], Soup_B) <-
      Only proteins with matching locations can bind.
      Update location information if possible. */

matching_location_(Soup_A, [Protein_1, Protein_2], Soup_B) :-
   protein:get_location(Protein_1, Soup_A, Loc_1),
   protein:get_location(Protein_2, Soup_A, Loc_2),
   localisation:allowed_interaction(Loc_1, Loc_2, Loc_1_A, Loc_2_A),
   protein:set_location_xml(Protein_1, Soup_A, Loc_1_A, Soup),
   protein:set_location_xml(Protein_2, Soup, Loc_2_A, Soup_B).


