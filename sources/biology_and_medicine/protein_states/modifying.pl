
:- module(modifying, []).

/* modification(AA, Type, ModAA) <-
     list of all accepted modifications
     */

modification('T', phosphorylation, 'Tp').
modification('S', phosphorylation, 'Sp').
modification('Y', phosphorylation, 'Yp').

/* phosphorylation(Soup_1, Action, Soup_2) <-
      */

phosphorylation(Soup_1, Action, Soup_2) :-
   Protein_1 := Action^protein@name,
   Protein_2 := Action^parameter@protein,
   Proteins = [Protein_1, Protein_2],
   interaction:unbound(Soup_1, Proteins),
   interaction(Soup_1, Proteins, binding, Soup_A),
   phosphorylation_(Soup_A, Action, Soup_B),
   interaction(Soup_B, Proteins, dissociate, Soup_2).

phosphorylation(Soup_1, Action, Soup_2) :-
   Protein_1 := Action^protein@name,
   Protein_2 := Action^parameter@protein,
   Proteins = [Protein_1, Protein_2],
   interaction:bound(Soup_1, Proteins),
   phosphorylation_(Soup_1, Action, Soup_2).


/* phosphorylation_(Soup_1, Action, Soup_2) <-
      here, both proteins are bound
      one site cannot be phosphorylated multiple times
      */

phosphorylation_(Soup_1, Action, Soup_2) :-
   Protein_1 := Action^protein@name,
   Protein_2 := Action^parameter@protein,
   unknown := Action^parameter@position,
   !,
   \+ protein:get_modification(Protein_2, Soup_1, unknown, phosphorylated),
   protein:add_modification(Protein_2, Soup_1, phosphorylated, unknown, Soup_A),
   !,
   \+ protein:forbidden_state([Protein_1, Protein_2], Soup_A),
   pathway:add(Soup_A, Action, [Protein_1, Protein_2], Soup_2).

phosphorylation_(Soup_1, Action, Soup_2) :-
   Protein_1 := Action^protein@name,
   Protein_2 := Action^parameter@protein,
   Position := Action^parameter@position,
   protein:get_sequence_position(Protein_2, Soup_1, Position, AA),
   modification(AA, phosphorylation, ModAA),
   \+ protein:get_modification(Protein_2, Soup_1, Position, ModAA),
   protein:add_modification(Protein_2, Soup_1, ModAA, Position, Soup_A),
   !,
   \+ protein:forbidden_state([Protein_1, Protein_2], Soup_A),
   pathway:add(Soup_A, Action, [Protein_1, Protein_2], Soup_2).


/* dephosphorylation(Soup_1, Action, Soup_2) <-
      */

dephosphorylation(Soup_1, Action, Soup_2) :-
   Protein_1 := Action^protein@name,
   Protein_2 := Action^parameter@protein,
   Proteins = [Protein_1, Protein_2],
   interaction:unbound(Soup_1, Proteins),
   interaction(Soup_1, Proteins, binding, Soup_A),
   dephosphorylation_(Soup_A, Action, Soup_B),
   interaction(Soup_B, Proteins, dissociate, Soup_2).

dephosphorylation(Soup_1, Action, Soup_2) :-
   Protein_1 := Action^protein@name,
   Protein_2 := Action^parameter@protein,
   Proteins = [Protein_1, Protein_2],
   interaction:bound(Soup_1, Proteins),
   phosphorylation_(Soup_1, Action, Soup_2).

/* dephosphorylation_(Soup_1, Action, Soup_2) <-
      here, both proteins are bound
      */

dephosphorylation_(Soup_1, Action, Soup_2) :-
   Protein_1 := Action^protein@name,
   Protein_2 := Action^parameter@protein,
   Position := Action^parameter@position,
   protein:get_modification(Protein_2, Soup_1, Position, ModAA),
   modification(_, phosphorylation, ModAA),
   protein:delete_modification(Protein_2, Soup_1, ModAA, Position, Soup_A),
   !,
   \+ protein:forbidden_state([Protein_1, Protein_2], Soup_A),
   pathway:add(Soup_A, Action, [Protein_1, Protein_2], Soup_2).


/* interaction(Soup_1, Proteins, Subtype, Soup_2) <-
      build the action XML structure and call interaction:Subtype
      */

interaction(Soup_1, [Protein_1, Protein_2], Subtype, Soup_2) :-
   Protein = protein:[name:Protein_1]:[],
   Parameter = parameter:[protein:Protein_2]:[],
   Status = status:[state:todo]:[],
   Action = action:[type:interaction, subtype:Subtype]:[Protein, Parameter, Status],
   Perform =.. [Subtype, Soup_1, Action, Soup_2],
   call(interaction:Perform).


/* check_unknown_modification(Mod, SpecMod) <-
      */

check_unknown_modification([unknown, phosphorylated], SpecModList) :-
   member([_Position, ModAA], SpecModList),
   modification(_AA, phosphorylation, ModAA).

