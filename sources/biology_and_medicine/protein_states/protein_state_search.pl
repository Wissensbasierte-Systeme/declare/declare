

/******************************************************************/
/***                                                            ***/
/***             Genetics: Search for Soups                     ***/
/***                                                            ***/
/******************************************************************/


:- module(protein_state_search, [
        pss/0, pss/1,
        protein_state_search_graph/0,
        protein_state_search_graph/1 ]).


/*** interface ****************************************************/


pss :-
   protein_state_search_graph.

pss(N) :-
   protein_state_search_graph(N).


/* protein_state_search_graph <-
      */

protein_state_search_graph :-
   protein_state_search_graph(1).

protein_state_search_graph(N) :-
   nth(N, [
      'jak_stat_proteins.xml' - 'actions.xml',
      'proteins.xml' - 'actions.xml',
      'proteins.xml' - 'actions2.xml',
      'proteins_forbidden.xml' - 'actions2.xml' ],
      X-Y),
%  protein_pathway_graph(X, Y, _).
   protein_pathway_graph(X, Y).


/******************************************************************/


