

:- module(state, []).


/* get_interactions(State, Interactions) <-
      */

get_interactions(State, Interactions) :-
   findall( Int,
      Int := State^interactions^interaction@protein,
      Interactions ).


/* get_modifications(State, Modifications) <-
      Modifications is a list of Pos, ModAA tuples */

get_modifications(State, Modifications) :-
   findall([Position, ModAA],
       _ :=  State^modifications^modification::[
          @position=Position, @type=ModAA],
       Modifications ).


/* protein_state_subsumes(SpecState, GeneralState) <-
      */

protein_state_subsumes(SpecState, GeneralState) :-
   subsumes_localisation_(SpecState, GeneralState),
   !,
   subsumes_interactions_(SpecState, GeneralState),
   subsumes_modifications_(SpecState, GeneralState).


/* subsumes_'feature'_(SpecState, GeneralState) <-
      Two options:
       - both states need identical features
       - one is a specification of the other */

subsumes_localisation_(SpecState, GeneralState) :-
   SpecLoc := SpecState@location,
   GeneralLoc := GeneralState@location,
   localisation:specifies(SpecLoc, GeneralLoc).

subsumes_interactions_(SpecState, GeneralState) :-
   identical := GeneralState^interactions@mode,
   !,
   get_interactions(SpecState, SpecInt),
   get_interactions(GeneralState, GeneralInt),
   subset(GeneralInt, SpecInt),
   subset(SpecInt, GeneralInt).

subsumes_interactions_(SpecState, GeneralState) :-
   get_interactions(SpecState, SpecInt),
   get_interactions(GeneralState, GeneralInt),
   subset(GeneralInt, SpecInt).

subsumes_modifications_(SpecState, GeneralState) :-
   identical := GeneralState^modifications@mode,
   !,
   get_modifications(SpecState, SpecMod),
   get_modifications(GeneralState, GeneralMod),
   subset(GeneralMod, SpecMod),
   subset(SpecMod, GeneralMod).

subsumes_modifications_(SpecState, GeneralState) :-
   get_modifications(SpecState, SpecMod),
   get_modifications(GeneralState, GeneralMod),
   subset(GeneralMod, SpecMod).

%subsumes_modifications_(SpecState, GeneralState) :-
%   get_modifications(SpecState, SpecMod),
%   get_modifications(GeneralState, GeneralMod),
%   no_position_subset_(GeneralMod, SpecMod).
%
%/* no_position_subset(GeneralMod, SpecMod) <-
%      */
%
%no_position_subset_(GeneralMod, SpecMod) :-
%   maplist(
%      (   member( SpecMod)
%        ;  modifying:check_unknown_modification(SpecMod)  
%      ), 
%      GeneralMod ).


