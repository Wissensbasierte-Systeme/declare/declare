

/******************************************************************/
/***                                                            ***/
/***             Genetics: ITS Analysis                         ***/
/***                                                            ***/
/******************************************************************/


:- module( its_analysis, [
      its_analysis_prepare/0,
      bracket_structure_to_chart_xpce/1,
      bracket_structure_distances_to_file/2,
      bracket_structure_distance/3,
      bracket_structure_distance/4 ] ).

 
:- dynamic
      bracket_structure/1,
      file_line_fact_1.


:- home_directory(Home),
   concat(Home, '/research/projects/Bioinformatik/ITS/',
      Directory),
   dislog_variables_set([
      its_chart_mode - compact,
      genetics_its_directory - Directory ]).


/*** interface ****************************************************/


/* its_analysis_prepare <-
      */

its_analysis_prepare :-
   dislog_variable_get(genetics_its_directory, Directory),
   concat(Directory, 'ITS2.html', File),
   its_read_file_to_bracket_structure_facts(File),
   dislog_stock_advisor(set_environment),
   bracket_structure_facts_to_charts.


/* bracket_structure_to_chart_xpce(N) <-
      */

bracket_structure_to_chart_xpce(N) :-
   bracket_structure_to_pairs(0.1, N, Pairs),
   concat('ITS:  ', N, Name),
   Size = [800,200], Origin = [5,5],
   Y_Unit = '', XY_Labels = [1,-2], Factor = 23,
   Chart = [ Name, colour(green), 5,
      [], [[1,10], [2,20], [3,30], [4,40]],
      [1,2,3,4], [5,10,15,20],
      Pairs ],
   stock_chart_to_xpce_stand_alone(
      Size, Origin, Y_Unit, XY_Labels, Factor, Chart ).


/* bracket_structure_distances_to_file(N, File) <-
      */

bracket_structure_distances_to_file(N, File) :-
   findall( Difference-M,
      ( bracket_structure_distance(N, M, Difference),
        write(user, '.'), ttyflush ),
      Distances_2 ),
   sort(Distances_2, Distances),
   predicate_to_file( File,
      writeln_list(Distances) ).


/* bracket_structure_distance(N, M, Distance) <-
      */

bracket_structure_distance(N, M, Distance) :-
   dislog_variable_get(its_chart_mode, Mode),
   bracket_structure_chart(N, Chart_1),
   bracket_structure_chart(M, Chart_2),
   chart_distance(Mode, Chart_1, Chart_2, Distance).

bracket_structure_distance(Mode, N, M, Distance) :-
   member(Mode, [compact, regular]),
   bracket_structure(N, S1),
   bracket_structure_to_chart(Mode, S1, Chart_1),
   bracket_structure(M, S2),
   bracket_structure_to_chart(Mode, S2, Chart_2),
   chart_distance(Mode, Chart_1, Chart_2, Distance).


/*** implementation ***********************************************/


/* bracket_structure_to_pairs(Factor, N, Pairs) <-
      */

bracket_structure_to_pairs(Factor, N, Pairs) :-
   bracket_structure(N, S),
   bracket_structure_to_pairs(S, Pairs_2),
   maplist( minus_pair_multiply(Factor),
      Pairs_2, Pairs ),
   !.

bracket_structure_to_pairs(S, Pairs) :-
   bracket_structure_to_chart(regular, S, Chart),
   length(Chart, L),
   generate_interval(1, L, Is),
   pair_lists_2(Is, Chart, Pairs).

minus_pair_multiply(Factor, X1-Y1, X2-Y2) :-
   X2 is X1 * Factor,
   Y2 is Y1 * Factor.


/* bracket_structure(N, Structure) <-
      */

bracket_structure(N, Structure) :-
   findall( X,
      bracket_structure(X),
      Xs ),
   nth(N, Xs, Structure).


/* bracket_structure_facts_to_charts <-
      */

bracket_structure_facts_to_charts :-
   retractall(bracket_structure_chart(_, _)),
   dislog_variable_get(its_chart_mode, Mode),
   forall_save( bracket_structure(N, S),
      ( bracket_structure_to_chart(Mode, S, Chart),
        assert(bracket_structure_chart(N, Chart)) ) ).


/* its_read_file_to_bracket_structure_facts(File) <-
      */

its_read_file_to_bracket_structure_facts(File) :-
   open(File, read, Stream),
   read_stream_to_line_facts(Stream, []),
   close(Stream),
   findall( Line,
      retract(file_line_fact(Line)),
      Lines ),
   forall_save( nth(N, Lines, Structure),
      ( modulo(N, 4, 0),
        assertz(bracket_structure(Structure)) ) ).


/* read_stream_to_line_facts(Stream, Sofar) <-
      */

read_stream_to_line_facts(Stream, Sofar) :-
   get0(Stream, C),
   read_stream_to_line_facts_loop(Stream, [C|Sofar]).

read_stream_to_line_facts_loop(Stream, [10|Sofar]) :-
   !,
   reverse(Sofar, Sofar_Rev),
   name(Name, Sofar_Rev),
   assertz(file_line_fact(Name)),
   read_stream_to_line_facts(Stream, []).
read_stream_to_line_facts_loop(_, [-1|Sofar]) :-
   !,
   reverse(Sofar, Sofar_Rev),
   name(Name, Sofar_Rev),
   assertz(file_line_fact(Name)).
read_stream_to_line_facts_loop(Stream, [C|Sofar]) :-
   read_stream_to_line_facts(Stream, [C|Sofar]).


/* forall_save(Goal_1, Goal_2) <-
      */

forall_save(Goal_1, Goal_2) :-
   findall( _,
      ( call(Goal_1),
        call(Goal_2) ),
      _ ).


/******************************************************************/


