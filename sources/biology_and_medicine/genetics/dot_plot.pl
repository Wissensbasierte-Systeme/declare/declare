

/******************************************************************/
/***                                                            ***/
/***             Genetics: Dotplot                              ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      dot/2.

:- multifile
      dot/2.


:- home_directory(Home),
   concat(Home, '/research/projects/Bioinformatik/Dandekar/',
      Directory),
   dislog_variable_set(bio_db, Directory).

   
/*** tests ********************************************************/


test(genetics:dot_plot_to_picture, 1) :-
   dislog_variable_get(bio_db, Directory),
%  concat(Directory, 'annot_300.data', Path),
   concat(Directory, 'annot1.data', Path),
   test(genetics:dot_plot_to_picture, Path, Path,2).

test(genetics:dot_plot_to_picture, File_1, File_2 ,Stretch) :-
   read_file_to_string(File_1, String_1),
   read_file_to_string(File_2, String_2),
   maplist( ascii_to_character,
      String_1, Characters_1 ),
   maplist( ascii_to_character,
      String_2, Characters_2 ),
   dot_plot_to_picture(Stretch, Characters_1, Characters_2).

ascii_to_character(A, C) :-
   name(C, [A]).


/*** interface ****************************************************/


/* dot_plot_to_picture(Stretch, Xs, Ys) <-
      */

dot_plot_to_picture(Stretch, Xs, Ys) :-
   dot_plot(Xs, Ys, Ps),
   new(Dialog, dialog('Dot Plot')),
%  send(Dialog, append, new(Quit, button(quit,
%     message(Dialog, destroy)))),
%  send(Quit, alignment, left),
%  send(Dialog, append, new(Make, button(make,
%     message(@prolog, make)))),
%  send(Make, alignment, left),
   send(Dialog, append,
      new(Picture, picture), below),
   send(Picture, size, size(2000, 2000)),
   send(Dialog, size, size(500, 500)),
   send(Dialog, open),
   send(Dialog, scrollbars, both),
%  send(Dialog, background, colour('#efd')),
%  send(Picture, background, colour('#efd')),
   rgb_to_colour([15,15,15.99], Colour),
   send(Dialog, background, Colour),
   send(Picture, background, Colour),
   dot_plot_send_frame(Picture, Stretch, Xs, Ys),
   dot_plot_send_x_marks(Picture, Stretch, Ys),
   dot_plot_send_y_marks(Picture, Stretch, Xs),
   checklist( send_dots_to_picture(Picture, Stretch),
      Ps ).


/* send_dots_to_picture(Picture, Stretch, Ps) <-
      */

send_dots_to_picture(Picture, Stretch, Ps) :-
   checklist( send_dot_to_picture(Picture, Stretch),
      Ps ).


/* send_dot_to_picture(Picture, Stretch, [X, Y]) <-
      */

send_dot_to_picture(Picture, Stretch, [X, Y]) :-
   A is Stretch * ( X - 1 ) + 20,
   B is Stretch * ( Y - 1 ) + 20,
   send(Picture, display, line(Stretch, Stretch), point(B, A)).


/* dot_plot_send_frame(Picture, Stretch, Xs, Ys) <-
      */

dot_plot_send_frame(Picture, Stretch, Xs, Ys) :-
   length(Xs, N),
   length(Ys, M),
   X1 is 20,
   X2 is Stretch * M + 20,
   Y1 is 20,
   Y2 is Stretch * N + 20,
   A is Stretch * M,
   B is Stretch * N,
   send(Picture, display, line(A,0), point(X1, Y1)),
   send(Picture, display, line(A,0), point(X1, Y2)),
   send(Picture, display, line(0,B), point(X1, Y1)),
   send(Picture, display, line(0,B), point(X2, Y1)).


/* dot_plot_send_x_marks(Picture, Stretch, [Y|Ys]) <-
      */

dot_plot_send_x_marks(Picture, Stretch, Ys) :-
   dot_plot_send_x_marks(Picture, Stretch, 1, Ys).

dot_plot_send_x_marks(Picture, Stretch, I, [Y|Ys]) :-
   A is 10,
   B is Stretch * ( I - 0.5 ) + 20,
   A1 is A - 5,
   B1 is B - 1,
   send(Picture, display, new(Text, text(Y)), point(B1, A1)),
   send(Text, font, font(times, bold, 10)),
   I1 is I + 1,
   dot_plot_send_x_marks(Picture, Stretch, I1, Ys).
dot_plot_send_x_marks(_, _, _, []).


/* dot_plot_send_y_marks(Picture, Stretch, Ys) <-
      */

dot_plot_send_y_marks(Picture, Stretch, Ys) :-
   dot_plot_send_y_marks(Picture, Stretch, 1, Ys).

dot_plot_send_y_marks(Picture, Stretch, I, [X|Xs]) :-
   A is 10,
   B is Stretch * ( I - 0.5 ) + 20,
   A1 is A - 3,
   B1 is B - 4,
   send(Picture, display, new(Text, text(X)), point(A1, B1)),
   send(Text, font, font(times, bold, 10)),
   I1 is I + 1,
   dot_plot_send_y_marks(Picture, Stretch, I1, Xs).
dot_plot_send_y_marks(_, _, _, []).


/* dot_plot(Xs, Ys, Ps) <-
      */

dot_plot(Xs, Ys, Ps) :-
   dot_plot(1, Xs, Ys, Ps_1),
%  predicate_to_file( a,
%     writeln(Ps_1) ),
   dot_plot(1, Ys, Xs, Ps_2),
%  predicate_to_file( b,
%     writeln(Ps_2) ),
%  dot(a, Ps_1),
%  dot(b, Ps_2),
   maplist( reverse_pairs,
      Ps_2, Ps_3 ),
   append(Ps_1, Ps_3, Ps),
   !.

reverse_pairs(Ps_1, Ps_2) :-
   maplist( reverse_pair,
      Ps_1, Ps_2 ).

reverse_pair([X, Y], [Y, X]).

dot_plot(I, Xs, Ys, Ps) :-
   measure(
      dot_plot_acc(I, Xs, Ys, Ps), T ),
   M is T * 1000,
   writeln(user, M).

dot_plot_acc(I, [X|Xs], Ys, [Ps_1|Ps_2]) :-
   dot_plot_sub([I, 1], [X|Xs], Ys, Ps_1),
   write(user,'.'), ttyflush,
   K is I + 1,
   dot_plot_acc(K, Xs, Ys, Ps_2).
dot_plot_acc(_, [], _, []).

dot_plot_sub([I, J], [X|Xs], [X|Ys], [[I, J]|Ps]) :-
   !,
   I1 is I + 1,
   J1 is J + 1,
   dot_plot_sub([I1, J1], Xs, Ys, Ps).
dot_plot_sub([I, J], [_|Xs], [_|Ys], Ps) :-
   I1 is I + 1,
   J1 is J + 1,
   dot_plot_sub([I1, J1], Xs, Ys, Ps).
dot_plot_sub(_, [], _, []) :-
   !.
dot_plot_sub(_, _, [], []).


/******************************************************************/


