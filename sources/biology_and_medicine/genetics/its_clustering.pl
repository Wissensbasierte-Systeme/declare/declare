

/******************************************************************/
/***                                                            ***/
/***             Genetics: ITS Clustering                       ***/
/***                                                            ***/
/******************************************************************/


:- module( its_clustering, [
      bracket_structures_to_clusters/2,
      bracket_structure_assign/2,
      bracket_cluster_distance/3,
      bracket_cluster_diameter/2,
      distance_to_bracket_cluster/3,
      bracket_clusters/1,
      bracket_cluster_with_diameter/2,
      bracket_cluster_pairs_with_distance/2 ] ).


:- dynamic
      bracket_cluster/1.


/*** interface ****************************************************/


/* bracket_clusters(Clusters_with_Diameters) <-
      */

bracket_clusters(Clusters_with_Diameters) :-
   findall( Diameter-Is,
      bracket_cluster_with_diameter(Is, Diameter),
      Xs ),
   sort(Xs, Clusters_with_Diameters),
   writeln_list(Clusters_with_Diameters).


/* bracket_cluster_with_diameter(Is, Diameter) <-
      */

bracket_cluster_with_diameter(Is, Diameter) :-
   bracket_cluster(Is),
   bracket_cluster_diameter(Is, Diameter).


/* bracket_cluster_pairs_with_distance(Distance, Pairs) <-
      */

bracket_cluster_pairs_with_distance(Distance, Pairs) :-
   findall( Is-Js,
      ( bracket_cluster(Is),
        Is = [_],
        writeln(user, Is),
        bracket_cluster(Js),
        Is \= Js,
        bracket_cluster_distance(Is, Js, D),
        D < Distance,
        write_list(user, [' --> ', Js, '\n']) ),
      Pairs ).


/* bracket_structures_to_clusters(Diameter, N) <-
      */

bracket_structures_to_clusters(Diameter, N) :-
   retractall(bracket_cluster(_)),
   ( for(I, 1, N) do
        bracket_structure_assign(Diameter, I) ).

bracket_structure_assign(Diameter, I) :-
   bracket_cluster(Js),
%  distance_to_bracket_cluster(Js, I, D),
%  D < Diameter,
   distance_to_bracket_cluster_ok(Diameter, Js, I),
   !,
   retract(bracket_cluster(Js)),
   append(Js, [I], Is),
   assert(bracket_cluster(Is)),
   write(user, Is), ttyflush.
bracket_structure_assign(_, I) :-
   assert(bracket_cluster([I])),
   write(user, [I]), ttyflush.

distance_to_bracket_cluster_ok(Diameter, [J|Js], I) :-
   bracket_structure_distance(I, J, D),
   D < Diameter,
   !,
   distance_to_bracket_cluster_ok(Diameter, Js, I).
distance_to_bracket_cluster_ok(_, [], _).


/* bracket_cluster_distance(Is, Js, Distance) <-
      */

bracket_cluster_distance(Is, Js, Distance) :-
   findall( D,
      ( member(I, Is),
        member(J, Js),
        bracket_structure_distance(I, J, D) ),
      Ds ),
   minimum(Ds, Distance).


/* bracket_cluster_diameter(Is, Diameter) <-
      */

bracket_cluster_diameter(Is, Diameter) :-
   findall( D,
      ( append(_, [I|_], Is),
        member(J, Is),
        bracket_structure_distance(I, J, D) ),
      Ds ),
   maximum(Ds, Diameter).


/* distance_to_bracket_cluster(Is, J, Distance) <-
      */

distance_to_bracket_cluster(Is, J, Distance) :-
   findall( D,
      ( member(I, Is),
        bracket_structure_distance(I, J, D) ),
      Ds ),
   maximum(Ds, Distance).


/******************************************************************/


