

/******************************************************************/
/***                                                            ***/
/***             Genetics: Charts                               ***/
/***                                                            ***/
/******************************************************************/


:- module( its_charts, [
      bracket_structure_to_chart/3,
      chart_distance/4 ] ).


/*** interface ****************************************************/


/* bracket_structure_to_chart(Mode, S, Chart) <-
      */

bracket_structure_to_chart(Mode, S, Chart) :-
   name(S, Xs_1),
   maplist( bracket_number_to_delta_number,
      Xs_1, Xs_2 ),
   ( Mode = regular,
     delta_numbers_to_chart(0, Xs_2, Chart)
   ; Mode = compact, 
     list_to_run_length_encoding(Xs_2, Chart) ),
   !.

bracket_number_to_delta_number(X, Y) :-
   member([X, Y], [[46, 0], [40, 1], [41, -1]]).


/* delta_numbers_to_chart(Start, Deltas, Chart) <-
      */

delta_numbers_to_chart(X, [A|As], [B|Bs]) :-
   B is X + A,
   delta_numbers_to_chart(B, As, Bs).
delta_numbers_to_chart(_, [], []).


/* chart_distance(Mode, Chart_1, Chart_2, Distance) <-
      */

chart_distance(regular, Chart_1, Chart_2, Distance) :-
   lists_fill_to_equal_length(Chart_1, Chart_2, Xs, Ys),
   euklidian_distance(Xs, Ys, Distance),
   !.
chart_distance(compact, Chart_1, Chart_2, Distance) :-
   chart_distance_for_rle(Chart_1, Chart_2, Distance),
   !.


/* chart_distance_for_rle(Chart_1, Chart_2, Distance) <-
      Chart_1 and Chart_2 are charts in run level encoding */

chart_distance_for_rle(Chart_1, Chart_2, Distance) :-
   chart_distance_for_rle(0, 0, Chart_1, Chart_2, D),
   sqrt(D, Distance).

chart_distance_for_rle(Sofar, Level, [X:N|Xs], [Y:M|Ys], D) :-
   minimum(N, M, K),
   L is K * (K + 1),
   Z is X - Y,
   Sofar2 is Sofar + K * Level^2 +
      Level * Z * L + Z^2 * L * (2*K + 1) / 6,
   Level2 is Level + K * Z,
   ( K >= N,
     Xs2 = Xs
   ; I is N - K,
     Xs2 = [X:I|Xs] ),
   ( K >= M,
     Ys2 = Ys
   ; J is M - K,
     Ys2 = [Y:J|Ys] ),
   !,
   chart_distance_for_rle(Sofar2, Level2, Xs2, Ys2, D).
chart_distance_for_rle(Sofar, Level, [], [Y:M|Ys], D) :-
   !,
   chart_distance_for_rle(Sofar, Level, [0:M], [Y:M|Ys], D).
chart_distance_for_rle(Sofar, Level, [X:N|Xs], [], D) :-
   !,
   chart_distance_for_rle(Sofar, Level, [X:N|Xs], [0:N], D).
chart_distance_for_rle(D, _, [], [], D) :-
   !.


/* list_to_run_length_encoding(Xs, Ys) <-
      */

list_to_run_length_encoding([X|Xs], Ys) :-
   list_to_run_length_encoding(X:1, Xs, Ys).
list_to_run_length_encoding([], []).

list_to_run_length_encoding(X:N, [X|Xs], Ys) :-
   !,
   M is N + 1,
   list_to_run_length_encoding(X:M, Xs, Ys).
list_to_run_length_encoding(X:N, [Z|Xs], [X:N|Ys]) :-
   list_to_run_length_encoding(Z:1, Xs, Ys).
list_to_run_length_encoding(X:N, [], [X:N]).


/* lists_fill_to_equal_length(Xs1, Xs2, Ys1, Ys2) <-
      */

lists_fill_to_equal_length(Xs1, Xs2, Ys1, Ys2) :-
   length(Xs1, N1),
   length(Xs2, N2),
   ( N1 > N2,
     M is N1 - N2,
     multify(0, M, Xs),
     append(Xs2, Xs, Ys2),
     Ys1 = Xs1
   ; N1 = N2,
     Ys1 = Xs1,
     Ys2 = Xs2
   ; lists_fill_to_equal_length(Xs2, Xs1, Ys2, Ys1) ),
   !.


/******************************************************************/


