

test(genetics:append_fast, N) :-
   once(
      ( N = 5,
        generate_interval(1,N,I),
        measure(
           append_fast(I,I,_), T1 ),
        measure(
           my_append(I,I,_), T2 ),
        M1 is T1 * 1000,
        M2 is T2 * 1000,
        writeln(user,M1:M2),
        generate_copies(N,I,Is),
        measure(
           append(Is,_), T3 ),
        M3 is T3 * 1000,
        writeln(user,M3) ) ).


append_fast(X,Y,Z) :-
%  reverse(X,R),
   append_fast_rev(X,Y,Z),
   !.
   
append_fast_rev([X|Xs],Y,Z) :-
   append_fast_rev(Xs,[X|Y],Z).
append_fast_rev([],Y,Y).

my_append([X|Xs],Ys,[X|Zs]) :-
   my_append(Xs,Ys,Zs).
my_append([],Ys,Ys).


generate_copies(0,_,[]).
generate_copies(N,X,[X|Xs]) :-
   M is N - 1,
   generate_copies(M,X,Xs).


