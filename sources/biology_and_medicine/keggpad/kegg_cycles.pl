

/******************************************************************/
/***                                                            ***/
/***        KEGG Pathways: Graph Analysis - Mark Cycles         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* kegg_pathway_to_cycles(
         Gene_Colour, Arrow_Colour, Hide_Expose) <-
      */

kegg_pathway_to_cycles(
      Gene_Colour, Arrow_Colour, Hide_Expose) :-
%  kegg_data(Kegg),
   dislog_variable_get(keggfile, Kegg),
   kegg_pathway_to_cycles(Kegg, Cycles_2),
   sort(Cycles_2, Cycles),
%  writeln_list(Cycles),
   !,
%  trace,
   ( foreach(V-W, Cycles) do
        ( ( kgmlreaction(gene, Id, _, V, W, _)
          ; kgmlreaction(gene, Id, _, W, V, _) ),
%         writeln(Id-V-W),
          get(@Id, member, box, Box),
          send(Box, fill_pattern, colour(Gene_Colour)),
          concat([Id, V, W], RId),
          send(@RId, Hide_Expose),
          send(@RId, colour, colour(Arrow_Colour)) ) ).


/*** implementation ***********************************************/


/* kegg_pathway_to_cycles(Kegg, Cycles) <-
      */

kegg_pathway_to_cycles(Kegg, Cycles) :-
   findall( V-W,
      kegg_pathway_to_cycle(Kegg, V-W),
      Cycles ).

kegg_pathway_to_cycle(Kegg, V-W) :-
   kegg_pathway_to_edgesb(Kegg, Edges),
   edges_to_ugraph(Edges, Ugraph),
%  writeln('UGraph'),
%  writeln(Ugraph),
   transitive_closure(Ugraph, Closure),
%  writeln('Closure'),
%  writeln(Closure),
   member(V-W, Edges),
   \+ member(W-V, Edges),
   member(W-Vs, Closure),
   member(V, Vs).


/* kegg_pathway_to_edges(Kegg, Edges) <-
      */

kegg_pathway_to_edgesb(Kegg, Edges) :-
   findall( V-W,
      ( kgmlentry(gene, _, _, Reaction, _, _, _, _),
	R := Kegg/reaction::[@name=Reaction],
	U := R@type,
        ( V := R/substrate@name,
          W := R/product@name
        ; U == 'reversible',
	  W := R/substrate@name,
          V := R/product@name ) ),
      Edges ).


/******************************************************************/


