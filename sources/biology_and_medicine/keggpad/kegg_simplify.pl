

/******************************************************************/
/***                                                            ***/
/***     KEGG Pathways: Graph Analysis - Simplify Graph         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* kegg_file_to_edges(Gene_Active, Arrow_Colour, HideExpose) <-
      */

kegg_file_to_edges(Gene_Active, Arrow_Colour, HideExpose) :-
   dislog_variable_get(keggfile, File),
   kegg_file_to_edges(File, Edges),
%  edges_to_picture(Edges),
   setof( U-V-W-Id1-Id2,
      edges_to_chain(Edges, U-V-W-Id1-Id2),
      Chains ),
   kp_parse_ids(Chains, [], X),
   ( foreach( Id, X) do (
%       get(@Id, member, box, Box),
%       send(Box, fill_pattern(colour(ghost_white))),
        send(@Id, active, Gene_Active),
        send(@Id, HideExpose),
        kgmlreaction(gene, Id, _, S, P,_),
        concat([Id, S, P], RId),
%       send(@RId,pen,2),
        send(@RId, colour(Arrow_Colour)) ) ).
%  writeln_list(user, Chains).


/* kegg_file_to_edges(File, Edges) <-
      */

kegg_file_to_edges(File, Sorted) :-
%  dread(xml, File, [X]),
   findall( Edge,
      ( kgmlentry(gene,Id,_,R,_,_,_,_),
        Reaction := File/reaction::[@name=R],
        kegg_reaction_to_edge(Reaction, Id, Edge) ),
      Edges ),   
   sort(Edges, Sorted).


kegg_reaction_to_edge(Reaction, Id, V-W-Id) :-
   ( [V,W] := Reaction-[substrate@name, product@name]
   ; (reversible := Reaction@type,
     [W,V] := Reaction-[substrate@name, product@name]) ).


edges_to_chain(Edges, U-V-W-Id1-Id2) :-
   member(U-V-Id1, Edges),
   member(V-W-Id2, Edges),
   U \= W,
   edges_to_chain_check(Edges, U-V-W-Id1-Id2).

%  edges_to_chain_check(Edges, 'cpd:C00015'-'cpd:C00105'-'cpd:C00299').


edges_to_chain_check(Edges, U-V-W-_-_) :-
   \+ (member(X-V-_, Edges), X \= U, X \= W),
   \+ (member(V-Y-_, Edges), Y \= W, Y \= U).

%  X \= Y

%  \+ (member(X-V-Id1, Edges), X \= U, X \= W),
%  \+ (member(V-Y-Id2, Edges), Y \= W, Y \= U).  X \= Y

kp_parse_ids([],List,Ids) :- 
   Ids = List.

kp_parse_ids(Chains,List,Ids) :-
   Chains = [H|T],
   H = _-_-_-Id1-Id2,
   add_element_to_list(Id1, List, NewList),
   add_element_to_list(Id2, NewList, NewList2),
   kp_parse_ids(T, NewList2, Ids).


/******************************************************************/


