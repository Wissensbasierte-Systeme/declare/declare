

/*********************************************************************/
/***                                                               ***/
/***      KEGG:  Additional Predicates (SWI-Extension)             ***/
/***                                                               ***/
/*********************************************************************/



/**** list operations ************************************************/


/* delete_first_of_list(+List, +Int, -List) <-
      deletes first Int elements from a list. */

delete_first_of_list(L,0,X) :- 
   L = X, 
   !.
delete_first_of_list(L,1,X) :- 
   [_|X] = L, 
   !.
delete_first_of_list(L,I,X) :- 
   length(L,Length), 
   ( I > Length -> 
     X = []
   ; [_|NewL] = L, 
     NewI is I-1, 
     delete_first_of_list(NewL,NewI, X) ). 


/* select_end_of_list(+List, +Int, -List) <-
      selects the last Int elements from a list. */

select_end_of_list(L,I,X) :- 
   length(L, Length), 
   NewI is Length-I, 
   delete_first_of_list(L,NewI,X).


/* select_begin_of_list(+List, +Int, -List) <-
      selects the first Int elements from a list. */

select_begin_of_list(L,I,X) :- 
   length(L, Length), 
   NewI is Length-I, 
   select_end_of_list(L,NewI,EndX), 
   append(X,EndX,L),
   !.

add_element_to_list(Element, [], [Element]).
add_element_to_list(Element, [Head|Tail], [Head|X]):-
   add_element_to_list(Element, Tail, X).


substring(S,Subs) :-
%  prefix
   append(_,Rest,S),
%  suffix
   append(Subs,_,Rest).
%  length(Prefix,N).


/**** atom operations ************************************************/


/* smallchar_to_bigchar(+Atom, -Atom) <-
      converts Atom of small letters to big letters. */

smallchar_to_bigchar(Small,Big) :-
   atom_codes(Small,SmallList),
   translate_code_list(SmallList,32,BigList),
   atom_codes(Big,BigList).

translate_code_list([],_,[]).

translate_code_list([H|T],Offset,L) :-
   NewH is H - Offset,   
   append([NewH],NewL,L),
   translate_code_list(T,Offset,NewL).
%  32 small to big

first_id_of_idstring(IDString, First) :-
   atomic_list_concat(Namelist,' ',IDString),
   Namelist = [First|_].

i_of_list(L,0,X) :-
   [X|_] = L,
   !.
i_of_list(L,I,X) :-
   [_|NewL] = L,
   NewI is I-1,
   i_of_list(NewL,NewI,X).


/* printkegg <-
      postscript printing. */

printkegg :-
%  forall(kegg_ko(Id,_._._),
%  atom_chars(8, L), atomic_list_concat(L,Y),
%  get(@Y, member(box), Box),
%  new(Area, area(0,0, 1100,800)),
   get(@keggpad, member, workspace, WorkSpace),
   get(WorkSpace, postscript, PS),
   get(PS, value, Value),
   postscript_patch(Value,NewValue),
   /*writeln(NewValue),*/
   /*working_directory(CWD, CWD),*/
   kgmlinfo(Pathway_Id, _, _),
   /*atomic_list_concat([CWD,Pathway_Id,'.ps'],Filename),*/
   atomic_list_concat([Pathway_Id,'.ps'],Filename),
   writeln(Filename),
   kegg_variable_get(tmp, Filename, FileWithPath),
   /*concat(CWD,'keggpad_postscript.ps', Filename),*/
   new(F, file(FileWithPath)),
   send(F, open, write),
   send(F, append, NewValue),
   send(F, close),
   /*get(F, name, File),*/
   atomic_list_concat(['kghostview ', FileWithPath], Command),
   shell(Command, Back),
   
   atomic_list_concat([ 'The file ', FileWithPath,
      ' has been saved, but could not be opened,',
      ' because KGhostView is not installed yet!'], Message),
   ( Back == 127 ->
     kp_info_message(Message)
   ; ! ).

postscript_patch(File, NewFile) :-
   name(File, L),
   replace_with_dot(L,_, Lnew),
   name(NewFile, Lnew).

replace_with_dot([],_,[]).

replace_with_dot([H|T],Offset,L) :-
   ( H == 44 ->
     NewH is H+2
   ; NewH is H ),   
   append([NewH],NewL,L),
   replace_with_dot(T,Offset,NewL).

/*
print_message(Message) :- 
   new(Md, dialog('Info')), 
   send(Md, append, button(Message, message(Md, destroy))), 
   send(Md, open_centered), 
   send(Md, open).
*/


/* save_sequence(File, Sequence) <-
      save to file. */

save_sequence(File, Sequence) :-
   kegg_variable_get(tmp, File, FileWithPath),
   open(FileWithPath,write,Stream),
   write(Stream, Sequence),
   close(Stream).

/*
simp_closure([H1|T1], [H2|T2], L3) :-
   L3 =
*/
  

/*********************************************************************/


