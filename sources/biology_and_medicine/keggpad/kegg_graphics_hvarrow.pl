

/*********************************************************************/
/***                                                               ***/
/***      KEGGPAD:  Graphics - Drawing the h_v_arrows              ***/
/***                                                               ***/
/*********************************************************************/


:- module( kegg_graphics_hvarrow, [
      draw_h_v_arrow/7 ] ).


/*** interface *******************************************************/


draw_h_v_arrow(Window, RId, R, Obj1, Obj2, Obj3, Dir) :-
   get(@Obj1, absolute_x, X1),
   get(@Obj1, absolute_y, Y1),
   get(@Obj2, absolute_x, X2),
   get(@Obj2, absolute_y, Y2),
   get(@Obj3, absolute_x, X3),
   get(@Obj3, absolute_y, Y3),
   get(@Obj1, height, H1),
   get(@Obj1, width, W1),
   get(@Obj2, height, H2),
   get(@Obj2, width, W2),
%  get(@Obj3, height, H3),
%  get(@Obj3, width, W3),
%  ( RId == '15cpd:C00337cpd:C00438' -> trace
%  ; notrace ),
   Z is Y2+2, 
%  writeln(Y2-Y3-Z),
   ( ( Y2 == Y3 ; Z == Y3 ) ->
       ( ( (Y2+H2) > Y1 , Y2 < (Y1+H1) ) ->
         draw_within_height(Window, RId, R, X2, Y2, X3, Dir ) 
       ; draw_vhv(Window, RId, R, X1, Y1, X2, Y2, X3, Y3, Dir ) )
   ; ! ),
   ( abs(X2 - X3) < 4 ->
       ( ( X2+W2 > X1 , X2 < X1+W1 ) ->
	 draw_within_width(Window, RId, R, X2, Y2, Y3, Dir) 
       ; draw_hvh(Window, RId, R, X1, Y1, X2, Y2, X3, Y3, Dir) )
   ; ! ),
   ( ( Y2 =\= Y3 , abs(X2 - X3) > 3 ) -> 
       ( ( Y2 > Y1-H1/2, Y3 > Y1-H1/2
         ; Y2 < Y1+H1/2, Y3 < Y1+H1/2 ) ->
%        draw_other(RId)
         draw_other2(Window, RId, R, X1, Y1, X2, Y2, X3, Y3,Dir)
       ; draw_other(Window, RId, R, X1, Y1, X2, Y2, X3, Y3,Dir) )
   ; ! ).


draw_within_height(Window, RId, R, X2, Y2, X3,Dir) :-
%  writeln(Dir),
   ( Dir == reversible -> 
     ArrowDir = both
   ; ArrowDir = second ),
   ( X2 < X3 -> 
     ( Off1 is 8, Off2 is 0 )
   ; ( Off1 is 0, Off2 is 8 ) ),
   send(Window, display, 
      new(@RId, line( X2+Off1, Y2+4, X3+Off2, Y2+4, ArrowDir ))),
%  send(Window, display, new(@RId, line(0,0,(X3-X2)-16,0,both))),
%  writeln(RId),
   send(@RId, hide),
   add_mouse_events(RId, R).

draw_within_width(Window, RId, R, X2, Y2, Y3,Dir) :-
   ( Dir == reversible -> 
     ArrowDir = both
   ; ArrowDir = second ),
   ( Y2 < Y3 -> 
     ( Off1 is 8, Off2 is 0 )
   ; ( Off1 is 0, Off2 is 8 ) ),
   send(Window, display, 
      new(@RId, line( X2+4, Y2+Off1, X2+4, Y3+Off2, ArrowDir ))),
   send(@RId, hide),
   add_mouse_events(RId, R).

draw_vhv(Window, RId, R, X1, Y1, X2, Y2, X3, _,Dir) :- /*Y3*/ 
   new(@RId, device),
   ( Dir == reversible -> 
     ArrowDir = first
   ; ArrowDir = none ), 
%  writeln('Stufe01'),
   ( Y2 > Y1 -> 
     YOff1 is -8
   ; YOff1 is 0 ),
%  writeln('Stufe02'),
   ( X2 > X1 -> 
     ( Off1 is 4, Off2 is 0 )
   ; ( Off1 is 0, Off2 is 4 ) ),
   Off is random(3),
%  writeln('Stufe1'),
%  concat(RId, '1', L1),
   send(@RId, display, 
      new(@L1, line( 0+1+Off, YOff1, 0+1+Off, Y1-Y2, ArrowDir ))),
   send(@RId, display, 
      new(@L2, line( 0+1+Off, Y1-Y2, X3-X2-1+Off, Y1-Y2, none ))),
   send(@RId, display, 
      new(@L3, line( X3-X2-1+Off, Y1-Y2, X3-X2-1+Off, YOff1, second ))),
%  writeln('Stufe2'),
   add_mouse_events(L1, R), 
   add_mouse_events(L2, R), 
   add_mouse_events(L3, R),
%  send(@RId, hide), send(L2, hide), send(L3, hide),
%  send(@RId, colour, colour(whitesmoke)),
   ( X3 > X2 -> 
     Ox is X2+3
   ; Ox is X3+3 ),
   ( Y2 > Y1 ->
     Oy is Y1+8
   ; Oy is Y2+8 ), 
   send(Window, display, @RId, point(Ox,Oy)),
   send(@RId, hide).
%  send(Window, display, new(line(X2+4, Y2+Off1, X2+4, Y3+Off2, none))),
%  send(Window, display, new(line(X2+4, Y2+Off1, X2+4, Y3+Off2, second))).

draw_hvh(Window, RId, R, X1, _, X2, Y2, _, Y3,Dir) :- /*Y1, X3*/
   new(@RId, device),
   ( Dir == reversible -> 
     ArrowDir = first
   ; ArrowDir = none ),
   ( X2 > X1 -> 
     XOff is -23
   ; XOff is -14 ),
   send(@RId, display, 
      new(@L1, line( 0, 0, X1-X2-XOff, 0, ArrowDir ))),
   send(@RId, display, 
      new(@L2, line( X1-X2-XOff, 0, X1-X2-XOff, Y3-Y2, none ))),
   send(@RId, display, 
      new(@L3, line( X1-X2-XOff, Y3-Y2, 0, Y3-Y2, second ))),
   add_mouse_events(L1, R),
   add_mouse_events(L2, R),
   add_mouse_events(L3, R),
%  send(@RId, hide), send(L2, hide), send(L3, hide),
%  send(@RId, colour, colour(blue)),
   ( X2 > X1 -> 
     Ox is X1+23
   ; Ox is X2+9 ),
   ( Y3 > Y2 -> 
     Oy is Y2+2
   ; Oy is Y3+2 ),
   send(Window, display, @RId, point(Ox,Oy)),
   send(@RId, hide).

draw_other(Window, RId, R, X1, Y1, X2, Y2, X3, Y3,Dir) :- 
   no_singleton_variables([Off1, Off2]),
   new(@RId, device),
   ( Dir == reversible ->
     ArrowDir = first
   ; ArrowDir = none ),
   ( Y2 > Y1 ->
     ( YOff1 is -8, YOff2 is 0 )
   ; ( YOff1 is 0, YOff2 is -8 ) ),
   ( X2 > X1 ->
     ( Off1 is 4, Off2 is 0 )
   ; ( Off1 is 0, Off2 is 4 ) ),
%  Off is random(4),
   send(@RId, display, 
      new(@L1, line( 0, 0+YOff1, 0, Y1-Y2, ArrowDir ))),
   send(@RId, display, 
      new(@L2, line( 0, Y1-Y2, X3-X2, Y1-Y2, none ))),
   send(@RId, display, 
      new(@L3, line( X3-X2, Y1-Y2, X3-X2, (Y3-Y1)+(Y1-Y2)+YOff2, second ))),
   add_mouse_events(L1, R),
   add_mouse_events(L2, R),
   add_mouse_events(L3, R),
%  send(@RId, hide), send(L2, hide), send(L3, hide),
%  send(@RId, colour, colour(red)),
   ( X3 > X2 ->
     Ox is X2+3
   ; Ox is X3+3 ),
   ( Y3 > Y2 ->
     Oy is Y2+8
   ; Oy is Y3+8 ), 
   send(Window, display, @RId, point(Ox,Oy)),
   send(@RId, hide).

draw_other2(Window, RId, R, X1, Y1, X2, Y2, X3, Y3,Dir) :- 
   new(@RId, device),
   ( Dir == reversible ->
     ArrowDir = first
   ; ArrowDir = none ),
   ( Y2 > Y1+8 -> 
     ( YOff1 is 0, YOff2 is 0 )
   ; ( YOff1 is -8, YOff2 is 0 ) ),
   ( X2 > X1 -> 
     ( Off1 is 4, Off2 is 0 )
   ; ( Off1 is 0, Off2 is 4 ) ),
%  Off is random(4),
   ( Y2 > Y1+8 ->
     YY is Y1-Y2
   ; YY is Y1-Y2 ),
   send(@RId, display, 
      new(@L1, line( 0, 0+YOff1, 0, YY, ArrowDir ))),
   send(@RId, display, 
      new(@L2, line( 0, YY, X3-X2, YY, none ))),
   send(@RId, display, 
      new(@L3, line( X3-X2, YY, X3-X2, (Y3-Y1)+(YY)-YOff2, second ))),
   add_mouse_events(L1, R),
   add_mouse_events(L2, R),
   add_mouse_events(L3, R),
%  send(@RId, hide), send(L2, hide), send(L3, hide),
%  send(@RId, colour, colour(blue)),
%  send(@L1, colour, colour(red)),
   ( X3 > X2 -> 
     Ox is X2+3
   ; Ox is X3+3 ),
   ( Y2 > Y1+8 ->
     Oy is Y1+1
   ; ( Y3 > Y2 -> 
       Oy is Y2+8
     ; Oy is Y3+8 ) ), 
   send(Window, display, @RId, point(Ox,Oy)),
   send(@RId, hide).


/*********************************************************************/


