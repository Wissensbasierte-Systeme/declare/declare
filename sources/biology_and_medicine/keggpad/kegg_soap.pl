

/*********************************************************************/
/***                                                               ***/
/***      KEGG:  API Connector                                     ***/
/***                                                               ***/
/*********************************************************************/


:- use_module(library(http/http_open)).
:- use_module(library(http/http_client)).


% connect_to_keggapi
% adds the KEGG API predicates with its original names
% to the prolog system
% be careful of possible name conflicts


param(0,'Out)').
param(1,'In1,Out)').
param(2,'In1,In2,Out)').
param(3,'In1,In2,In3,Out)').
param(4,'In1,In2,In3,In4,Out)').
param(5,'In1,In2,In3,In4,In5,Out)').

param2(0,'[],Out)').
param2(1,'[In1],Out)').
param2(2,'[In1,In2],Out)').
param2(3,'[In1,In2,In3],Out)').
param2(4,'[In1,In2,In3,In4],Out)').
param2(5,'[In1,In2,In3,In4,In5],Out)').


/*** interface *******************************************************/


connect_to_keggapi(Wsdl_File) :-
   retractall(partline(_,_,_,_)),
   dread(xml, Wsdl_File, [Wsdl]),
   forall( Q := Wsdl/message,
      ( R := Q@name,
        ( concat(KeggFunction, 'Request', R)
        ; concat(_, 'Response', R) ),
        ( KeggFunction \= '' -> 
           ( add_soap_facts(Q, KeggFunction, List), 
             length(List, Length), 
%            Arity is Length+1, 
%            write('predicate '),
%            write(KeggFunction),
%            write('/'), 
%            write(Arity), 
%            writeln(' ...added'),
             add_kegg_predicate(KeggFunction, Length) )
        ; ! ) ) ).

add_soap_facts(Q, KeggFunction, List) :-
   dislog_variable_set(counter, 1),
   findall( PartLine, 
      ( PartLine := Q/part,
        Name := PartLine@name,
        Type := PartLine@type,
        dislog_variable_get(counter, Counter),
        assert(partline(KeggFunction, Name, Type, Counter)),
        NewCounter is Counter+1,
        dislog_variable_set(counter, NewCounter) ), 
        List ).

add_kegg_predicate(KeggFunction, Length) :-   
   VarList = ['In1'=_, 'In2'=_, 'In3'=_, 'In4'=_, 'In5'=_],
   param(Length, Parm), 
   param2(Length, Parm2),
   select_begin_of_list(VarList, Length, SelectedVarList),
   add_element_to_list('Out'=_, SelectedVarList, BindingList),
%  Term
   concat('(', Parm, ParamStr),
   concat(KeggFunction, ParamStr, StringL),
%  Term2
   concat('call_keggapi(', KeggFunction, SubStr2),
   concat(',', Parm2, ParamStr2),
   concat(SubStr2, ParamStr2, StringR),
%  Term and Term2: string_to_term
   string_to_atom(StringL, Atom), 
   atom_to_term(Atom, Term, BindingList),
   string_to_atom(StringR, Atom2),
   atom_to_term(Atom2, Term2, BindingList),
   assert(Term :- Term2).


% call_keggapi
% wrapper-facade predicate for the via connect_to_keggapi
% added KEGG API methods
% calls the KEGG API via method name as first parameter,
% In-Parameter as list of method parameters.
% Reply as return value of the called API method

call_keggapi(Method, In_Parameter, Reply) :-
   retractall(soapparam(_,_)),
   dislog_variable_set(counter, 1),
   add_soap_pmfacts(In_Parameter),
   make_soap_message(Method),
   kegg_variable_get(tmp, 'soap_request.xml', RequestFile),
   send_soap_message('http://soap.genome.jp/keggapi/request_v6.2.cgi',
      RequestFile, SoapAnswer),
   kegg_variable_get(tmp, 'soap_answer.xml', AnswerFile),
   dwrite(xml, AnswerFile, SoapAnswer),
   dread(xml, AnswerFile, [Doc]),
   get_return_of_soap_answer(Doc, Reply).
%  dwrite(xml, Return).

add_soap_pmfacts(In_Parameter) :-
   ( foreach(Parameter, In_Parameter) do 
        ( dislog_variable_get(counter, Counter),
          assert(soapparam(Counter, Parameter)),
          NewCounter is Counter+1,
          dislog_variable_set(counter, NewCounter) ) ).

make_soap_message(Method) :-
   kegg_variable_get(xml, 'soap_call.xml', SoapcallDefaultFile),
   dread(xml, SoapcallDefaultFile,[Soap]), 
   concat('ns1:',Method,Tag), 
   U := Soap/nth_child::1 <+> [/'SOAP-ENV:Body'/Tag:[
      'xmlns:ns1':'SOAP/KEGG',
      'SOAP-ENV:encodingStyle':
         'http://schemas.xmlsoap.org/soap/encoding/']:[]], 
   dislog_variable_set(soapcall, U),
   forall( partline(Method,Name,Type,Index),
      ( soapparam(Index,Value),
   add_soap_pmline(Tag, Name, Type, Value) ) ),
   dislog_variable_get(soapcall, W),
   kegg_variable_get(tmp, 'soap_request.xml', RequestFile),
   dwrite(xml, RequestFile, W).

add_soap_pmline(Tag, Name, Type, Value) :-
   dislog_variable_get(soapcall, U),
   V := U <+> [/'SOAP-ENV:Body'/Tag/Name:['xsi:type':Type]:[]],
   W := V * [/'SOAP-ENV:Body'/Tag/Name:Value],
   dislog_variable_set(soapcall, W).

send_soap_message(URL, SoapMessage, SoapAnswer) :-
   http_post(URL, file('text/xml', SoapMessage), SoapAnswer, []).

get_return_of_soap_answer(Doc, Return) :-
   R := Doc/nth_child::1, 
   S := R/nth_child::1,
   Return := S/nth_child::1.


/* soap reihenfolge korrekt auswerten:
   vorher anzahl ermitteln, danach gezielt via

   dread(xml,'KEGG.wsdl',[K]),
   S := K/message::[@name='get_linkdb_by_entryRequest'],
   R := S-nth(3, /part).
*/

/* beispiele fnquery

   dread(xml,'KEGG.wsdl',[K]),
   S := K/message::[@name='get_genes_by_ko_classRequest'],
   R := S-nth(2, /part), dwrite(xml,R).

<part name="org" type="xsd:string"/>
*/


/*********************************************************************/


