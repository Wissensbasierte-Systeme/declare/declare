

/*********************************************************************/
/***                                                               ***/
/***      KEGGPAD:  Graphics                                       ***/
/***                                                               ***/
/*********************************************************************/


:- use_module(library(www_browser)).
:- use_module(kegg_graphics_hvarrow, [draw_h_v_arrow/7]).

element_offset(100,80).


/*** interface *******************************************************/


kp_draw_title(Keggfile, Window) :-
   get_keggpathway_infos(Keggfile,Name,Org),
   organisms(Org, Organism),
   concat_atom([Name,' of "', Organism, '"'], Text), 
   send(@pathwayname, value, Text),
   send(Window, display, @pathwayname, point(5,5)), 
   send(@pathwayname, colour, colour(gray15)),
   send(@pathwayname, font, font(helvetica, bold, 14)).

draw_genes(Window) :-
   forall( kgmlentry(gene,Id,_,_,Link,Label,CX,CY),
      ( correct_coordinates(labeled_box, CX, CY, Xc, Yc),
        draw_device(labeled_box, Id, Label, darkseagreen2),
        element_offset(OX,OY),
        send(Window, display, @Id, point(Xc-OX, Yc-OY)),
        send(@Id, cursor, hand1),
        send(@Id, help_message, tag, Label),
        add_clickgestures(Id, Link, mark_gene_ortho),
        send(@Id, displayed, @on) ) ).

draw_orthologs(Window) :-
   forall( kgmlentry(ortholog,Id,Name,_,Link,Label,CX,CY),
      ( correct_coordinates(labeled_box, CX, CY, Xc, Yc),
        draw_device(labeled_box, Id, Label, whitesmoke),
        element_offset(OX,OY),
        send(Window, display, @Id, point(Xc-OX, Yc-OY)),
        send(@Id, cursor, hand1),
        send(@Id, help_message, tag, Name),
        add_clickgestures(Id, Link, mark_gene_ortho),
        send(@Id, displayed, @off) ) ).

draw_compounds(Window) :-
   forall( kgmlentry(compound,Id,_,_,Link,Label,CX,CY),
      ( correct_coordinates(labeled_circle, CX, CY, Xc, Yc),
        draw_device(labeled_circle, Id, Label, peachpuff4),
        element_offset(OX,OY),
        send(Window, display, @Id, point(Xc-OX, Yc-OY)),
        send(@Id, cursor, hand1),
        send(@Id, help_message, tag, Label),
        add_clickgestures(Id, Link, mark_cpd),
        send(@Id, displayed, @on) ) ).
   
add_clickgestures(Id, Link, Command) :-
   new( CGleft, click_gesture( left, '', single, 
      and( message(@prolog, writeln, @arg1),
      message(@prolog, www_open_url, Link),
      message(@prolog, nl) ) ) ), 
   new( CGright, click_gesture( right, '', single, 
      and( message(@prolog, writeln, @arg1),
      message(@prolog, Command, Id),
      message(@prolog, nl) ) ) ),
   send(@Id, recogniser, CGleft),
   send(@Id, recogniser, CGright).
   

draw_device(labeled_box, LabelBox, Label,Color) :-
   new(@LabelBox, device),
%  writeln(@LabelBox),
   send(@LabelBox, display, new(B, box(45,15))),
   send(B, fill_pattern(colour(Color))),
   string_length(Label, Length),
   ( Length > 5 ->
     ( substring(Label, 1, 5, ShortLabel),
       send(@LabelBox, display, 
          new(T, text(ShortLabel)), point(2,0)) )
     ; send(@LabelBox, display, 
          new(T, text(Label)), point(2,0)) ),
   kegg_variable_get(pictures, 'b_flag2.xpm', PictureFile),
%  working_directory(Directory,_),
%  concat(Directory,'b_flag2.xpm',Imagepath),
   send(@LabelBox, display, 
      new(Bm2, flagblue(PictureFile)), point(32,0)),
   send(Bm2, displayed, @off).
%  send(T, center, B?center).

draw_device(labeled_circle, Compound, Label, Colour) :-
   new(@Compound, device),
%  writeln(@LabelBox),
   send(@Compound, display, 
      new(C, circle(9))),
   send(C, fill_pattern(colour(Colour))),
   send(@Compound, display, 
      new(T, text(Label)), point(8,11)),
   send(T, colour, colour(Colour)),
   send(T, font, font(helvetica, normal, 10)).


mark_gene_ortho(Id) :-
   get(@Id, member(box), B),
   send(B, fill_pattern(colour(red))).

mark_cpd(Id) :-
   get(@Id, member(text), TI), 
   send(TI, colour, colour(red)),
   get(@Id, member(circle), C), 
   send(C, fill_pattern(colour(red))).

mark_arrow(Id) :-
   send(@Id, colour, colour(red)).


draw_arrows(Window, Type) :-
   ( Type == gene ->
     retractall(kegg_active_cpd(_))
   ; ! ),
   forall( kgmlreaction(Type,Id1,Reaction,C1,C2,Dir),
      ( kgmlentry(Type,Id1,_,_,_,_,CX,CY),
        chose_it(CX,CY,C1,Id2),
        chose_it(CX,CY,C2,Id3),
%       write(Id1), write(Id2), write(Id3), write(C1), writeln(C2),
        concat(Id1,C1,Idtemp),
        concat(Idtemp,C2,RId),
        ( Type == gene ->
          ( assert(kegg_active_cpd(Id2)),
            assert(kegg_active_cpd(Id3)) )
        ; ! ),
%       writeln(RId),
        draw_h_v_arrow(Window, RId, Reaction, Id1, Id2, Id3, Dir) ) ).

get_all_active_cpds(Ids) :-
   findall( Id,
      kegg_active_cpd(Id),
      Ids ).

add_mouse_events(Id, Reaction) :-
   ( Reaction \= '@off' -> 
     ( send(@Id, cursor, hand1),
       send(@Id, help_message, tag, Reaction),
       concat('http://www.kegg.jp/dbget-bin/www_bget?',Reaction,Link),
       add_clickgestures(Id, Link, mark_arrow) )
   ; ! ).

correct_coordinates(labeled_box, X, Y, Xc, Yc) :- 
   atom_number(X, Xn), 
   atom_number(Y, Yn), 
   Xc is Xn - 22, 
   Yc is Yn - 7.

correct_coordinates(labeled_circle, X, Y, Xc, Yc) :- 
   atom_number(X, Xn), 
   atom_number(Y, Yn), 
   Xc is Xn - 4, 
   Yc is Yn - 4.


kp_freeallobj(X) :- 
   X =:= 0, 
   !.
kp_freeallobj(X) :- 
   atom_chars(X, L), 
   atomic_list_concat(L,Y), 
   free(@Y), 
   Newx is X -1, 
   kp_freeallobj(Newx).

kp_free_arrows(Type) :-
   forall( kgmlreaction(Type,Id1,_,C1,C2,_),
      ( concat(Id1,C1,Idtemp),
        concat(Idtemp,C2,RId),
%       writeln(RId),
        free(@RId) ) ).


hide_genes :-
   forall( kgmlentry(gene,Id,_,_,_,_,_,_), 
      send(@Id, displayed, @off) ),
   menubar_swap_itemvalues(graph, genes, 'Hide', 'Show', off),
   set_toolbar_button('1', off),
   set_toolbar_button_active('2',off),
   set_menubar_item(graph, compounds, 'Hide', off).

show_genes :-
   forall( kgmlentry(gene,Id,_,_,_,_,_,_), 
      send(@Id, displayed, @on) ),
   menubar_swap_itemvalues(graph, genes, 'Hide', 'Show', on),
   set_toolbar_button('1', on),
   set_toolbar_button_active('2',on),
   set_menubar_item(graph, compounds, 'Hide', on).

hide_compounds :-
   get_all_active_cpds(Ids),
   forall( ( kgmlentry(compound,Id,_,_,_,_,_,_), member(Id,Ids) ),
      send(@Id, displayed, @off) ),
   menubar_swap_itemvalues(graph, compounds, 'Hide', 'Show', off),
   set_toolbar_button('2', off),
   set_toolbar_button_active('1',off),
   set_menubar_item(graph, genes, 'Hide', off).

show_compounds :-
   get_all_active_cpds(Ids),
   forall( ( kgmlentry(compound,Id,_,_,_,_,_,_), member(Id,Ids) ), 
      send(@Id, displayed, @on) ),
   menubar_swap_itemvalues(graph, compounds, 'Hide', 'Show', on),
   set_toolbar_button('2', on),
   set_toolbar_button_active('1',on),
   set_menubar_item(graph, genes, 'Hide', on).


hide_ref_compounds :-
   get_all_active_cpds(Ids),
   forall( ( kgmlentry(compound,Id,_,_,_,_,_,_),
      \+ member(Id,Ids) ), 
      send(@Id, displayed, @off) ),
   menubar_swap_itemvalues(graph, compounds, 'Show All', 'Only Organism', on),
   set_toolbar_button('7', off).

show_ref_compounds :-
   get_all_active_cpds(Ids),
   forall( ( kgmlentry(compound,Id,_,_,_,_,_,_),
      \+ member(Id,Ids) ), 
      send(@Id, displayed, @on) ),
   menubar_swap_itemvalues(graph, compounds, 'Show All', 'Only Organism', off),
   set_toolbar_button('7', on).

show_cycles :-
   kegg_pathway_to_cycles(tomato,red,expose),
   menubar_swap_itemvalues(operations, cycles, 'Mark', 'Unmark', off),
   set_toolbar_button('4', on).
   

hide_cycles :-
   kegg_pathway_to_cycles(darkseagreen2,black,hide),
   menubar_swap_itemvalues(operations, cycles, 'Unmark', 'Mark', off),
   set_toolbar_button('4', off).

show_simplify :-
   kegg_file_to_edges(@off, royalblue, hide),
   menubar_swap_itemvalues(graph, simplify, 'Show', 'Hide', off),
   set_toolbar_button('5', on),
   set_toolbar_button_active('4',off),
   set_menubar_item(operations, cycles, 'Mark', off).
   

hide_simplify :-
   kegg_file_to_edges(@on, black, expose),
   menubar_swap_itemvalues(graph, simplify, 'Show', 'Hide', on),
   set_toolbar_button('5', off),
   set_toolbar_button_active('4',on),
   set_menubar_item(operations, cycles, 'Mark', on).

hide_kos :-
   forall( kgmlentry(ortholog,Id,_,_,_,_,_,_), 
      send(@Id, displayed, @off) ),
   hide_ref_compounds,
   kp_free_arrows(ortholog),
   retractall(kgmlreaction(ortholog,_,_,_,_,_)),
   get(@keggpad, member, vertdial, X), 
   get(X, member, operations, Y), 
   get(Y, member, 7, Z),
   kegg_toolbar:keggpadbutton('7',_,_, Default_Label, _, _),    
   send(Z, selected, @off),
   send(Z, help_message, tag, Default_Label),
   menubar_swap_itemvalues(graph, ortholog, 'Show', 'Hide', on),
   set_toolbar_button('6', off).


show_kos :-
   menubar_swap_itemvalues(graph, ortholog, 'Show', 'Hide', off),
   set_toolbar_button('6', on),
   forall( kgmlentry(ortholog,Id,_,_,_,_,_,_), 
      send(@Id, displayed, @on) ),
   show_ref_compounds,
   dislog_variable_get(keggfile, Keggfile),
   get_reactions(Keggfile, ortholog),
   get(@keggpad, member, workspace, WS), 
   draw_arrows(WS, ortholog),
   get(@keggpad, member, vertdial, X), 
   get(X, member, operations, Y), 
   get(Y, member, 7, Z),
   kegg_toolbar:keggpadbutton('7',_,_, _, Pressed_Label, _),    
   send(Z, selected, @on),
   send(Z, help_message, tag, Pressed_Label).

flag_org :-
   menubar_swap_itemvalues(operations, organisms, 'Flag', 'Unflag', off),
   set_toolbar_button('3', on),
   dislog_variable_set(flagorganism, true),
   get(@keggpad,member,dialog,D),
   get(D, member, st_org1, Textfield),
   send(Textfield, value, ''),

   get(@orgbrowser, show, Browser_Open),
   ( Browser_Open == @off ->
     flag_message
   ; ! ),
   get(@orgbrowser, selection, Sel),
   chain_list(Sel,DictList),
%  send(Textfield, value, 'Alles neu'),
   ( foreach(Dict2,DictList) do (
   get(Dict2, label, OrgDef2),
   organisms(Org2, OrgDef2),
   get(Textfield, value, STValue),
   concat(STValue, ' ', STValue2),
   concat(STValue2, Org2, STValue3),
   send(Textfield, value, STValue3) ) ),
   findall( _,
      ( kgmlentry(gene,Id,Name,_,_,_,_,_),
        first_id_of_idstring(Name,First),
        writeln(First),
        concat('Connecting via KEGG API, querying ', First, Status),
        writeln(Status),
        send(@keggpad, report, progress, Status),
        dislog_variable_get(flagorganism, Flag1),
        Flag1 == true,
   
        get_ko_by_gene_r(First,KO),
        get_ko_table_by_ko(KO,Doc),
   
        member(Dict,DictList),
        get(Dict, label, OrgDef),
        organisms(Org, OrgDef),
%       Org = hsa,

%       check_org_by_gene(Org,First),
        check_org_by_ko_table(Org,Doc),
 
%       !,
%       dislog_variable_get(flagorganism, Flag2),
%       Flag2 == true,

        get(@Id, member(flagblue), Bm),
        writeln(Bm),
        send(Bm, displayed, @on),
        get(@Id, help_message, tag, HM), 
        get(HM, value, V),
        organisms(Org,OrgDef),
        concat(V,'\nl Found: ', Vneu),
        concat(Vneu, OrgDef, Vneu2),
        send(@Id, help_message, tag, Vneu2) ),
      _ ),
%  ( Flag == false ->
%    unflag_org
%  ; true ),
   send(@keggpad, report, progress,
      'Ready - all organisms are compared and marked!').

unflag_org :-
   dislog_variable_set(flagorganism, false),
   forall( kgmlentry(gene, Id, _, _, _, _, _, _),
      ( get(@Id, member(flagblue), Bm),
        send(Bm, displayed, @off) ) ),
   id_to_name(gene),
   menubar_swap_itemvalues(operations, organisms, 'Flag', 'Unflag', on),
   set_toolbar_button('3', off).
   

flag_org_browser :-
%  new(@orgbrowser, browser),
   send(@orgbrowser, multiple_selection, @on),
   forall( organisms(_,Def), 
      send(@orgbrowser, append, Def) ), 
   send(@orgbrowser, open_centered),
   send(@orgbrowser, size, size(50, 50)), /*???*/
   send(@orgbrowser, open).

flag_message :- /*KeggPad*/
%  new(Md, dialog('Info')), 
%  send(Md, append,
%     button('Please choose the compare organisms first! - CLOSE',
%     message(Md, destroy))), 
%  send(Md, open_centered), 
%  send(Md, open),
   kp_info_message('Please choose the compare organisms first!'),
   get(@keggpad, member, vertdial, X), 
   get(X, member, operations, Y), 
   get(Y, member, 3, Z),
   kegg_toolbar:keggpadbutton('3',_,_, Default_Label, _, _),    
   send(Z, selected, @off),
   send(Z, help_message, tag, Default_Label).

get_ko_by_gene_r(Gene,KO) :-
   get_ko_by_gene(Gene, Reply),
   KO := Reply/descendant::item/content::'*'.

get_ko_table_by_ko(KO,Doc) :-
   bget(KO,Reply),
   [Doc] := Reply/content::'*',
   !.

check_org_by_ko_table(Org,Doc) :-
   smallchar_to_bigchar(Org,BigOrg),
   concat(BigOrg,': ',Atomsearch),
   sub_atom(Doc,_,_,_,Atomsearch).

   
:- pce_begin_class(flagblue, bitmap).
:- pce_end_class(flagblue).


/*********************************************************************/


