

/*********************************************************************/
/***                                                               ***/
/***      KEGGPAD:  Functions                                      ***/
/***                                                               ***/
/*********************************************************************/


/*** part of n organisms *********************************************/


check_org_by_gene(Org,Gene) :-
   get_ko_by_gene(Gene, Reply),
   KO := Reply/descendant::item/content::'*',
   check_org_by_ko(Org,KO).
	
check_org_by_ko(Org,KO) :-
   /*concat('ko:',KO,BgetSearch),
   writeln(BgetSearch),*/
   bget(KO,Reply),
   [Doc] := Reply/content::'*',!,
   smallchar_to_bigchar(Org,BigOrg),
   concat(BigOrg,': ',Atomsearch),
   sub_atom(Doc,_,_,_,Atomsearch),
   writeln('Found').


/*** part of n organisms *********************************************/
	

/* id_to_name(Type) <-
      get the names of compounds, genes and label them. */

id_to_name(Type) :-
   findall( H1,
      ( kgmlentry(Type,Id,Name,_,_,_,_,_),
        atomic_list_concat(Namelist,' ',Name),
        Namelist = [H1|T1]),
        List ),
%  writeln_list(List),
   sort(List, Sorted),
   atomic_list_concat(Sorted,' ',String),
   btit(String, Reply),
   [U] := Reply/content::'*', 
   !,
   Sorted = [H|T],
   send_new_label(U, Type, T, H),
%  writeln('here'),
   forall( kgmlentry(Type,Id,Name,_,_,_,_,_),
      ( /*get_name_from_reply(Name,U,Label),*/
	atomic_list_concat(Namelist,' ',Name),
	Namelist = [H1|T1],
	entrylabel(Type,H1,Label),
	send(@Id,help_message, tag, Label) ) ).

send_new_label(U, Type, [], A) :- 
   sub_atom(U,R,S,Z,A),
   Start is R+S+1,
   Laenge is Z-1,
   sub_atom(U,Start,Laenge,_,Label),
   assert(entrylabel(Type,A,Label)).
send_new_label(U, Type, List, A) :-
   List = [H|T],
   send_it(U, A, H),
   send_new_label(U, Type, T, H).

send_it(U,A,H) :-
   sub_atom(U,R,S,_,A),
   sub_atom(U,R2,_,_,H),
   Start is R+S+1,
   Laenge is R2-Start-1,
   sub_atom(U,Start,Laenge,_,Label),
   assert(entrylabel(_,A,Label)).


% get the names of compounds, genes,
% (recations) and label them


/* chose_it(X, Y, Compound, RId) <-
      get the id of the right compounds for one reaction. */

chose_it(X, Y, Compound, RId) :-
   findall( [Id, Val],
      ( kgmlentry(compound, Id, Compound, _, _, _, X2, Y2), 
        atom_number(X, Xnum),
        atom_number(X2, X2num),
        atom_number(Y, Ynum),
        atom_number(Y2, Y2num),
        Val is (abs(Xnum - X2num) + abs(Ynum - Y2num)) ),
      ValList ),
   chlist(ValList, _, 1500, RId).

chlist([],Id,_,X) :- 
   X=Id, 
   !.
chlist(List,Id,Vgl,X) :-
   List = [H|T],
   H = [Idt,Val],
%  H = [Idt|[Val]],
   ( (Val > Vgl) -> 
     chlist(T,Id,Vgl,X)
   ; chlist(T,Idt,Val,X) ).


% get the id of the right compounds for one reaction

% Auswertung Reactions mit Berücksichtigung 1+/1+

r1(_,_,_,_,[],[],_,_).

r1(Type,Id,R,Dir,[],L2,A1,A2) :- 
   L2 = [H2|T2], 
%  writeln(A1-H2-Dir),
   assert(kgmlreaction(Type,Id,R,A1,H2,Dir)), 
%  write('A'), writeln(Id),
   r1(Type,Id,R,Dir,[],T2,A1,A2). 

r1(Type,Id,R,Dir,L1,[],A1,A2) :- 
   L1 = [H1|T1], 
%  writeln(H1-A2-Dir),
   assert(kgmlreaction(Type,Id,R,H1,A2,Dir)), 
%  write('B'), writeln(Id),
   r1(Type,Id,R,Dir,T1,[],A1,A2).

r1(Type,Id,R,Dir,L1,L2,_,_) :-
   L1 = [H1|T1],
   L2 = [H2|T2],
%  writeln(H1-H2-Dir),
   assert(kgmlreaction(Type,Id,R,H1,H2,Dir)),
%  write('C'),writeln(Id),
%  writeln(L1), writeln(L2),
   r1(Type,Id,R,Dir,T1,T2,H1,H2).

% Auswertung Reactions mit Berücksichtigung 1+/1+

assert_organisms :- 
   retractall(organisms(_,_)),
   list_organisms(Reply), 
   forall( U := Reply/item, 
      ( [V] := U/entry_id/content::'*', 
        [W] := U/definition/content::'*', 
        assert(organisms(V,W)) ) ).

assert_pathways(Org) :- 
   retractall(kegg_pathway(_,_)),
   list_pathways(Org, Reply), 
   forall( U := Reply/item, 
      ( [V] := U/entry_id/content::'*', 
	[W] := U/definition/content::'*', 
	assert(kegg_pathway(V,W)) ) ).


/*********************************************************************/


