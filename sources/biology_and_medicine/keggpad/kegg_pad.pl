

/*********************************************************************/
/***                                                               ***/
/***      KEGGPAD:  Variables                                      ***/
/***                                                               ***/
/*********************************************************************/


kegg_variable(project, Path) :-
   dislog_variable_get(home, '/projects/keggpad/', Path).

kegg_variable(sources, Path) :-
   dislog_variable_get(home, '/sources/projects/keggpad/', Path).

kegg_variable(examples, Path) :-
   kegg_variable_get(project, 'examples/', Path).
kegg_variable(pictures, Path) :-
   kegg_variable_get(project, 'pictures/', Path).
kegg_variable(tmp, Path) :-
   kegg_variable_get(project, 'tmp/', Path).
kegg_variable(xml, Path) :-
   kegg_variable_get(project, 'xml/', Path).


kegg_variable_get(Variable, Extension, Value) :-
   kegg_variable_get(Variable, V),
   concat(V, Extension, Value).

kegg_variable_get(Variable, Value) :-
   kegg_variable(Variable, Value).


/*********************************************************************/


