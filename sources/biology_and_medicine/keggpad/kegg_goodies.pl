

/*********************************************************************/
/***                                                               ***/
/***    KEGGPAD:  Implementation of Resize Graph Function (beta)   ***/
/***                                                               ***/
/*********************************************************************/



/*** interface *******************************************************/


pfeil :-
   Id = '1250didel',
   Id2 = '1250dialog',
   Id3 = '1250box',
   new(@Id, device),
   send(@Id, display, new(L, line(0, 0, 0, 50, none))),
   send(@Id, display, new(L2, line(0, 50, 70, 50, none))),
   send(@Id, display, new(L3, line(70, 50, 70, 0, none))),
   new(@Id2, dialog),
   send(@Id2, display, @Id, point(50,50)),
   send(@Id2, display, new(@Id3,box(70,20)), point(50,50)),
   send(@Id3, fill_pattern(colour(aquamarine))),
   send(@Id3, hide),
   send(@Id, display, text('123456789')),
   send(@Id2, display, box(10,10), point(40,40)),
   send(@Id2, display, line(50,45,120,45)),
   send(@Id2, append, button(plus,message(@prolog,resize_pfeil))),
   send(@Id2, append, button(minus,message(@prolog,resize_pfeilz))),
   send(@Id2,open),
   writeln(Id2).
%  resize, color

resize_pfeil :-
   Id = '1250didel',
   Id2 = '1250dialog',
   Id3 = '1250box',
   get(@Id, absolute_x, XId),
   get(@Id, absolute_y, YId),writeln(XId-YId),
   get(@Id3, absolute_x, XId3),
   get(@Id3, absolute_y, YId3), writeln(XId3-YId3),
   get(@Id, member, text, Text),
   get(Text, font, N), writeln(N),
   get(N, points, N2), writeln(N2),
   send(Text, font, font(helvetica, normal, N2*1.25)),
   
   new(P, point(2,2)),
   send(@Id, resize, 1.25, 1.25, point(XId,YId)),
%  send(@Text, resize, 1.25),
   send(@Id3, resize, 1.25),
   send(@Id, move, point(1.25*XId,1.25*YId)),
   send(@Id3, move, point(1.25*XId3,1.25*YId3)).
%  send(@Id, absolute_x, 2*XId), F is 2*XId, writeln(F),
%  send(@Id, absolute_y, 2*YId), G is 2*YId, writeln(G),
%  send(@Id3, absolute_x, 2*XId3), H is 2*XId3, writeln(H),
%  send(@Id3, absolute_y, 2*YId3), I is 2*YId3, writeln(I).


resize_pfeilz :-
   Id = '1250didel',
   Id2 = '1250dialog',
   Id3 = '1250box',
   get(@Id, absolute_x, XId),
   get(@Id, absolute_y, YId),writeln(XId-YId),
   get(@Id3, absolute_x, XId3),
   get(@Id3, absolute_y, YId3), writeln(XId3-YId3),
   get(@Id, member(text), Text),
   get(Text, font, N), writeln(N),
   get(N, points, N2), writeln(N2),
   send(Text, font, font(helvetica, normal, N2*0.8)),
   new(P, point(2,2)),
   send(@Id, resize, 0.8),
   send(@Id3, resize, 0.8),
   send(@Id, move, point(XId/1.25,YId/1.25)),
   send(@Id3, move, point(XId3/1.25,YId3/1.25)).
%  send(@Id, absolute_x, 2*XId),F is 2*XId, writeln(F),
%  send(@Id, absolute_y, 2*YId),G is 2*YId, writeln(G),
%  send(@Id3, absolute_x, 2*XId3),H is 2*XId3, writeln(H),
%  send(@Id3, absolute_y, 2*YId3),I is 2*YId3, writeln(I).

/*
Id = '1250didel',
Id2 = '1250dialog',
Id3 = '1250box', free(@Id), free(@Id2), free(@Id3).   */

resize_all :-
   resize_genes(gene),
   resize_genes(compound),
   resize_arr.

resize_allz :-
   resize_genesz(gene),
   resize_genesz(compound),
   resize_arrz.

resize_genes(Type) :-
   forall( kgmlentry(Type, Id, _, _, _, _, _, _),
      (
%        Id = '1250didel',
%        Id2 = '1250dialog',
%        Id3 = '1250box',
         get(@Id, absolute_x, XId),
         get(@Id, absolute_y, YId),writeln(XId-YId),
         get(@Id, member, text, Text),
         get(Text, font, N), writeln(N),
         get(N, points, N2), writeln(N2),
         send(Text, font, font(helvetica, normal, N2*1.25)),
         new(P, point(2,2)),
         send(@Id, resize, 1.25),
%        send(@Text, resize, 1.25),
         send(@Id, move, point(1.25*XId,1.25*YId)) ) ).
%  send(@Id, absolute_x, 2*XId), F is 2*XId, writeln(F),
%  send(@Id, absolute_y, 2*YId), G is 2*YId, writeln(G),
%  send(@Id3, absolute_x, 2*XId3), H is 2*XId3, writeln(H),
%  send(@Id3, absolute_y, 2*YId3), I is 2*YId3, writeln(I).

resize_genesz(Type) :-
   forall( kgmlentry(Type, Id, _, _, _, _, _, _),
      (
%       Id = '1250didel',
%       Id2 = '1250dialog',
%       Id3 = '1250box',
        get(@Id, absolute_x, XId),
        get(@Id, absolute_y, YId),writeln(XId-YId),
        get(@Id, member, text, Text),
        get(Text, font, N), writeln(N),
        get(N, points, N2), writeln(N2),
        send(Text, font, font(helvetica, normal, N2*0.8)),
        new(P, point(2,2)),
        send(@Id, resize, 0.8),
%       send(@Text, resize, 1.25),
        send(@Id, move, point(XId/1.25,YId/1.25)) ) ).
%  send(@Id, absolute_x, 2*XId), F is 2*XId, writeln(F),
%  send(@Id, absolute_y, 2*YId), G is 2*YId, writeln(G),
%  send(@Id3, absolute_x, 2*XId3), H is 2*XId3, writeln(H),
%  send(@Id3, absolute_y, 2*YId3), I is 2*YId3, writeln(I).

resize_arr :-
   dislog_variable_get(wks, Keggpad),
   get(Keggpad, member, myp_wks, WS),
   forall( kgmlreaction(gene, Id1, Reaction, C1, C2, _),
      ( kgmlentry(gene, Id1, _, _, _, _, CX, CY),
        chose_it(CX,CY,C1,Id2),
        chose_it(CX,CY,C2,Id3),
        concat(Id1,C1,Idtemp),
        concat(Idtemp,C2,Id),
        get(@Id, absolute_x, XId),
        get(@Id, absolute_y, YId),writeln(XId-YId),
        concat(XId,YId,KORD),
        concat(Id,a,Ida),
        free(@Ida),
        draw_device(labeled_circle, Ida, KORD, red),
%       send(WS, display, @Ida, point(XId, YId)),
        new(P, point(2,2)),
        send(@Id, resize, 1.25, 1.25, point(XId,YId)),
        send(@Id, move, point(1.25*XId,1.25*YId))
%       send(@Text, resize, 1.25)
      ) ).

resize_arrz :-
   forall( kgmlreaction(gene, Id1, Reaction, C1, C2, _),
      ( kgmlentry(gene, Id1, _, _, _, _, CX, CY),
        chose_it(CX,CY,C1,Id2),
        chose_it(CX,CY,C2,Id3),
        concat(Id1,C1,Idtemp),
        concat(Idtemp,C2,Id),
        get(@Id, absolute_x, XId),
        get(@Id, absolute_y, YId), writeln(XId-YId),
        new(P, point(2,2)),
        send(@Id, resize, 0.8, 0.8, point(XId,YId)),
%       send(@Text, resize, 1.25),
        send(@Id, move, point(XId/1.25,YId/1.25)) ) ).


/*********************************************************************/


