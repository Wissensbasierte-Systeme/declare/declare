

/******************************************************************/
/***                                                            ***/
/***      KEGGPAD:  Loading and Starting the Application        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


keggpad_gui :-
   connect_to_keggapi,
   keggpad.
 
connect_to_keggapi :- 
   kegg_variable_get(xml, 'KEGG.wsdl', File),
   connect_to_keggapi(File). 


/******************************************************************/


