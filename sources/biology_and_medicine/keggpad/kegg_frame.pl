

/*********************************************************************/
/***                                                               ***/
/***      KEGGPAD:  GUI                                            ***/
/***                                                               ***/
/*********************************************************************/


:- use_module( kegg_toolbar, [
      keggpad_toolbar_create/2 ] ).


:- pce_autoload(report_dialog, library(pce_report)).
% :- pce_autoload(tool_bar, library(toolbar)).
:- pce_autoload(finder, library(find_file)).
:- pce_global(@finder, new(finder)).
:- pce_global(@keggpad, new(keggpad_gui)).
:- pce_global(@pathwayname, new(text(''))).
:- pce_global(@orgbrowser, new(browser('Organism Browser'))).


keggpad_windowsize(1100, 800).


/*** interface *******************************************************/


:- pce_begin_class(keggpad_gui, frame).


initialise(KeggPad) :->
   send_super(KeggPad, initialise, 'KeggPAD'),
   keggpad_windowsize(X_Dim, Y_Dim),
   send(KeggPad, append, 
      new(TopDialog, dialog)),
   send(TopDialog, size, size(X_Dim, 50)),
   send(TopDialog, append, 
      new(menu_bar)),
%  send(D, append, 
%     new(Label,label(compare)), right),
%  send(Label, value, 'Comp.Organisms:'),
   send(TopDialog, append, 
      new(label(fill)), right),
%  working_directory(Directory,_),
%  concat(Directory,'b_flag2.xpm',Imagepath1),
   kegg_variable_get(pictures, 'b_flag2.xpm', Imagepath1),
   send(TopDialog, display, 
      new(StarIcon, bitmap(Imagepath1)), point(370,10)),
   send(StarIcon, cursor, hand1),
      new( ClickGestureLeft, click_gesture( left, '', single, 
      and( message(@prolog, writeln, @arg1),
      message(@prolog, flag_org_browser),
      message(@prolog, nl) ) ) ), 
   send(StarIcon, recogniser, ClickGestureLeft),
   send(TopDialog, display, 
      new(st_org1('')), point(398,12)),
   send(KeggPad, fill_menu_bar),
   send(
      new(Workspace, workspace), below, TopDialog),
   send(Workspace, scrollbars, both),
   send(Workspace, hor_stretch, 25),
%  workspace_3DLook(W),
   send(KeggPad, append, 
      new(VerticalDialog, vertdial)),
   send(VerticalDialog, left, Workspace),
   send(VerticalDialog, size, size(65,Y_Dim)),
   keggpad_toolbar_create(VerticalDialog, vertical),
%  concat(Directory, 'kegg_n.gif', Imagepath2),
   kegg_variable_get(pictures, 'kegg_n.gif', Imagepath2),
   send(VerticalDialog, display, 
      new(bitmap(Imagepath2)), point(7,6)),
   new(Gap, size(10,52)),
   send(VerticalDialog, gap, Gap),
   send(
      new(report_dialog), below, Workspace).


fill_menu_bar(KeggPad) :->
   get(KeggPad, member, dialog, TopDialog),
   get(TopDialog, member, menu_bar, MenuBar),
   new(GraphNotLoaded,
      not(message(KeggPad?get_workspace?graphicals, empty))),
   writeln(GraphNotLoaded),
%  get(KeggPad, member, workspace, WorkSpace),
%  get(KeggPad, member, workspace, WS),
   send_list( MenuBar, append, [
      new(File, popup(file)), 
      new(Operations, popup(operations)), 
      new(Graph, popup(graph)),
      new(Help, popup(help)),
      new(Compare, popup('Comp.Organisms:')) ] ),
   send_list( File, append, [
      menu_item('load KGML File',
         message(KeggPad, load)),
      menu_item('save KGML File',
         message(@prolog,write_orgbrowser), condition := 2<1),
      menu_item('download Assistant',
         message(@prolog, get_pathway_window), end_group := @on),
      menu_item(print,
         message(@prolog, printkegg),
         end_group := @on, condition := GraphNotLoaded),
      menu_item(quit_KeggPAD_GUI, 
         and( message(@prolog, kp_free_arrows, gene),
              message(KeggPad, quit) ), condition := GraphNotLoaded ),
      menu_item(quit_SWI_Prolog, message(KeggPad, quit_all) ) ] ),
   send_list( Operations, append, [
      menu_item('Search',
         message(KeggPad, search_for_element),
         condition := GraphNotLoaded),
      new(Cycles, popup(cycles)),
      new(Organisms, popup(organisms)),
      menu_item('Elementary Modes',
         message(@prolog, kp_info_message, 'Not implemented!'),
         condition := 2<1) ] ),
   send(Cycles?context, condition, GraphNotLoaded),
   send(Organisms?context, condition, GraphNotLoaded),
   send_list( Graph, append, [
      new(Genes, popup(genes)),
      new(Compounds, popup(compounds)),
      new(Ortholog, popup(ortholog)),
      new(Simplify, popup(simplify)) ] ),
   send(Help, append, menu_item(about, 
      message(@prolog, kp_info_message,
         'KeggPAD V0.xx, Uwe Weber, 2010'))),
   send(Compare, append,
      menu_item('Organism Browser',
         message(@prolog, flag_org_browser))),

   send(Genes?context, condition, GraphNotLoaded),
   send(Compounds?context, condition, GraphNotLoaded),
   send(Ortholog?context, condition, GraphNotLoaded),
   send(Simplify?context, condition, GraphNotLoaded),
%  send(Ortholog?context, condition, GraphNotLoaded),
   send_list( Genes, append, [
      menu_item('Hide', message(@prolog, hide_genes)),
      menu_item('Show', message(@prolog, show_genes)) ] ),
   set_menubar_item(graph, genes, 'Show', off),
   send_list( Compounds, append, [
      menu_item('Hide', message(@prolog, hide_compounds)),
      menu_item('Show', message(@prolog, show_compounds)),
      menu_item('Show All', message(@prolog, show_ref_compounds)),
      menu_item('Only Organism',
         message(@prolog, hide_ref_compounds)) ] ),
   set_menubar_item(graph, compounds, 'Show', off),
   set_menubar_item(graph, compounds, 'Only Organism', off),
   send(Ortholog, end_group, @on),
   send_list( Ortholog, append, [
      menu_item('Show', message(@prolog, show_kos)),
      menu_item('Hide', message(@prolog, hide_kos)) ] ),
   set_menubar_item(graph, ortholog, 'Hide', off),
   send_list( Simplify, append, [
      menu_item('Show', message(@prolog, show_simplify)),
      menu_item('Hide', message(@prolog, hide_simplify)) ] ),
   set_menubar_item(graph, simplify, 'Hide', off),
   send_list( Cycles, append, [
      menu_item('Mark', message(@prolog, show_cycles)),
      menu_item('Unmark', message(@prolog, hide_cycles)) ] ),
   set_menubar_item(operations, cycles, 'Unmark', off),
   send(Organisms, end_group, @on),
   send_list( Organisms, append, [
      menu_item('Flag', message(@prolog, flag_org)),
      menu_item('Unflag', message(@prolog, unflag_org)) ] ),
   set_menubar_item(operations, organisms, 'Unflag', off).

get_menubar(KeggPad, MenuBar) :<-
   get(KeggPad, member, dialog, TopDialog), 
   get(TopDialog, member, menu_bar, MenuBar).

get_toolbar(KeggPad, ToolBar) :<-
   get(KeggPad, member, vertdial, VerticalDialog),
   get(VerticalDialog, member, operations, ToolBar).

get_workspace(KeggPad, Workspace) :<-
   get(KeggPad, member, workspace, Workspace).

/*
little_message(_) :-> 
   new(MessageDialog, dialog('Info')), 
   send(MessageDialog, append,
      button('KeggPAD V0.xx, Uwe Weber, 2010',
         message(MessageDialog, destroy))), 
   send(MessageDialog, open_centered), 
   send(MessageDialog, open).
*/

load(KeggPad) :->
   kegg_variable_get(examples, Path),
   chdir(Path),
   get(@finder, file, @on, xml, File),
   ( check_for_kgmldata(File) ->
     true
   ; kp_info_message('The file does not include valid KGML data!'),
     false ),
   kp_freeallobj(300),
   get(@pathwayname, value, Value),
   ( Value \= '' ->
     ( kp_free_arrows(gene),
       send(@pathwayname, value, '') )
   ; writeln('Status') ),
   get(KeggPad, member, workspace, Workspace),
   send(KeggPad, report, progress, 'Loading file ...'),
   send(@prolog, kp_load_and_display, KeggPad, File, Workspace),
   send(KeggPad, report, done, done).

/*
print_wsheet(KeggPad) :->
   get(KeggPad, member, workspace, Window),
   printkegg(Window).
*/

quit(KeggPad) :->
   free(KeggPad).

quit_all(KeggPad) :->
   free(KeggPad),
   halt.

search_for_element(_) :->
%  KeggPad
   kp_get_element_idnr(Id_Number, compound), 
%  dislog_variable_get(keggfile, Keggfile),
%  R := Keggfile/descendant::entry::[@id='1'], U := R@id, writeln(U),
%  searching for compounds
   concat('cpd:',Id_Number, Compound_Id),
   forall( kgmlentry(compound, Entry_Id, Compound_Id, _,_,_,_,_), 
      ( get(@Entry_Id, member(text), TextItem), 
        send(TextItem, colour, colour(red)), 
        get(@Entry_Id, member(circle), Circle), 
        send(Circle, fill_pattern(colour(red))) )).

:- pce_end_class(keggpad_gui).


:- pce_begin_class(workspace, picture).
:- pce_end_class(workspace).

:- pce_begin_class(vertdial, dialog).
:- pce_end_class(vertdial).

:- pce_begin_class(st_org1, text).
:- pce_end_class(st_org1).

% :- pce_begin_class(topdialog, dialog).
% :- pce_end_class(topdialog).


set_toolbar_button(Number,Value) :-
   set_toolbar_button(Number, Value, on).
   

set_toolbar_button(Number,Value,Active) :-
   get(@keggpad?get_toolbar, member, Number, Button),
   kegg_toolbar:keggpadbutton(
      Number, _, _, Default_Label, Pressed_Label, _),
   ( Value == on ->
     send(Button, help_message, tag, Pressed_Label)
   ; send(Button, help_message, tag, Default_Label) ),
   send(Button, selected, Value),
   send(Button, Active).

set_toolbar_button_active(Number,Active) :-
   get(@keggpad?get_toolbar, member, Number, Button),
   send(Button, Active).

set_menubar_item(PopUp1, PopUp2, MenuItem, Value) :-
   get(@keggpad?get_menubar, member, PopUp1, H1Menu),
   ( PopUp2 \= none ->
     ( get(H1Menu, member, PopUp2, H2Menu),
       get(H2Menu, popup, X),
       get(X, member, MenuItem, UMenu) )
   ; get(H1Menu, member, MenuItem, UMenu) ),
   send(UMenu, Value).

menubar_swap_itemvalues(
      PopUp1, PopUp2, MenuItem1, MenuItem2, Value) :-
   set_menubar_item(PopUp1, PopUp2, MenuItem1, Value),
   ( Value == off ->
     NotValue = on
   ; NotValue = off ),
   set_menubar_item(PopUp1, PopUp2, MenuItem2, NotValue). 

kp_info_message(MessageText) :-
   new(MessageDialog, dialog('Info')), 
   send(MessageDialog, append, new(Label, label(test))),
   send(Label, value, MessageText),
   send(MessageDialog, append,
      button(ok, message(MessageDialog, destroy))), 
   send(MessageDialog, open_centered), 
   send(MessageDialog, open).
 

/* kp_get_element_idnr(Id_Number, Type) <-
      for searching an element. */

kp_get_element_idnr(Id_Number, Type) :-
   concat('Search for ', Type, UserDialogName),
   new(UserDialog, dialog(UserDialogName)), 
   atomic_list_concat(['enter_',Type,'_Number'], TextItemValue),
   send( UserDialog, append, 
      new(TextItem, text_item(TextItemValue)) ), 
   send(UserDialog, append, button(ok,
      message(UserDialog, return, TextItem?selection))), 
   send(UserDialog, open_centered), 
   get(UserDialog, confirm, Answer), 
   send(UserDialog, destroy), 
   Id_Number = Answer.


/* keggpad <-
      */

keggpad :-
   send(@keggpad, open_centered),
   get(@keggpad, member, dialog, TopDialog),
   send(TopDialog, active, @off),
   get(@keggpad, member, vertdial, VerticalDialog),
   send(VerticalDialog, active, @off),
   send(@keggpad, open),
   send(@keggpad, report, progress,
      'Loading organisms data from KEGG ...'),
   assert_organisms,
   send(TopDialog, active, @on),
   send(@keggpad, report, progress,
      'Ready ... Load your file via File>Load KGML file now.').

kp_load_and_display(KeggPad, File, WS) :- 
   get_kgmlfile(File, Keggfile),
  
   dislog_variable_set(keggfile, Keggfile), 
   dislog_variable_set(wks, KeggPad), 
   kp_draw_title(Keggfile, WS), 
   send(KeggPad, report, progress, 'Calculating graph ...'), 
%  dislog_variable_set(data, Data), the kgml data
   get_all_keggelements(Keggfile), 
   draw_genes(WS), 
   draw_compounds(WS), 
   get_reactions(Keggfile, gene),
   draw_arrows(WS, gene), 
   hide_ref_compounds,
%  get_reactions(Keggfile, ortholog),
   draw_orthologs(WS),
   send(KeggPad, report, progress,
      'Calculating graph ... dissolving the KEGG-IDs via KEGG API ...'),
   id_to_name(gene),
   id_to_name(compound),
%  writeln('vor ortholog'),
%  id_to_name(ortholog),
%  writeln('nach ortholog'),
   get(KeggPad, member, vertdial, VD),
   send(VD, active, @on).
%  writeln('load and display').

workspace_3DLook(W) :-
   keggpad_windowsize(X_Dim, Y_Dim),
   send(W, display, new(L1, line(X_Dim,0)), point(0,0)),
   send(W, display, new(L2, line(0,Y_Dim)), point(0,0)),
   send(L1, colour, colour(black)),
   send(L2, colour, colour(black)).


/*** download assistant **********************************************/


/* get_pathway_window <-
      */

get_pathway_window :-
   new(D, dialog('Download Assistant')),
   send(D, append, new(Dd1, menu('Choose Organism', cycle))),
   send(D, append, new(Dd2, menu('Choose Pathway', cycle))),
   atomic_list_concat(['Note: \nl',
      '1. Choose the desired organism \nl',
      '2. Search for the specific pathways \nl',
      '3. Choose the desired pathway \nl',
      '4. Download the selected pathway/KGML file'], InfoText),
   send(Dd1, help_message, tag, InfoText),
   send(Dd2, help_message, tag, InfoText),
   findall( Orgsort, 
      organisms(_,Orgsort),
      Orglist ),
   msort(Orglist,OrglistSorted),
%  writeln_list(OrglistSorted),
   ( foreach( OrgDef, OrglistSorted ) do
        ( organisms(Org,OrgDef),
          atom_concat(OrgDef,' - ',Org1),
          atom_concat(Org1,Org,Org2),
          send(Dd1, append, Org2),
          send(Dd1, value_width, 700),
          send(Dd2, value_width, 700) ) ),
   send(Dd2, append, 'Choose Pathway'),
   new(B, button(download_selected_pathway)),
   send(B, help_message, tag, InfoText),
   new(C2, and(
      message(@prolog, open_pathwayurl_by_pathwayid, Dd1, Dd2)
      /*message(@prolog, send, B, active, @off)*/ ) ),
   send(B, message, C2),
   new(S, button(search_for_specific_pathways)),
   send(S, help_message, tag, InfoText),
   new(C, and( message(@prolog, search_pathways_by_org, Dd1, Dd2),
      message(@prolog, send, B, active, @on))),
   send(S, message, C),
   send(D, append, S),
   send(D, append, B),
   send(B, active, @off),
   send(D, append, button(close_download_assistant, message(D, destroy))),
   send(D, open_centered),
   send(D, open).

open_pathwayurl_by_pathwayid(DropDown1,DropDown2) :-
   get(DropDown1, selection, X),
   sub_atom(X,_,3,0,Org),
   get(DropDown2, selection, Y),
   sub_atom(Y,_,8,0,Pathway_Id),
   concat('/pub/kegg/xml/kgml/metabolic/organisms/',Org,Path),
   concat(Pathway_Id,'.xml', File),
   kgml_download_via_ftp('ftp.genome.jp', Path, File).
%  atomic_list_concat([
%     'ftp://ftp.genome.jp/pub/kegg/xml/kgml/metabolic/organisms/',
%     Org,'/',Pathway_Id,'.xml'], '', Link),
%  www_open_url(Link).

search_pathways_by_org(DropDown1, DropDown2) :-
   get(DropDown1, selection, X),
%  get(X, value, Value),
%  writeln(X),
   sub_atom(X,_,3,0,Org),
   assert_pathways(Org),
   findall( Pathsort, 
      kegg_pathway(_,Pathsort),
   Pathlist ),
   msort(Pathlist,PathlistSorted),
%  writeln_list(OrglistSorted),
   send(DropDown2, clear),
   ( foreach( PathDef, PathlistSorted) do
        ( kegg_pathway(Pathway,PathDef),
          atom_concat(PathDef,' - ',Path1),
          atom_concat(Path1,Pathway,Path2),
          send(DropDown2, append, Path2) ) ).

/* download of kgml files via ftp,
   uses the ftp command of the operating system */

/* kgml_download_via_ftp(Server, Path, File) <-
      for anonymous login. */

kgml_download_via_ftp(Server, Path, File) :-
   kgml_download_via_ftp(
     Server, Path, File, 'anonymous', 'mail@prolog').

kgml_download_via_ftp(Server, Path, File, Username, Password) :-
   concat('open ', Server, FTP_Command0),
   atomic_list_concat([ 'user ', Username, ' ', Password],
      FTP_Command02),
   concat('cd ', Path, FTP_Command1),
   concat('get ', File, FTP_Command2),
   kegg_variable_get(tmp, 'ftp_call.txt', CallFile),
   open(CallFile,write,Stream),
   writeln(Stream, FTP_Command0),
   writeln(Stream, FTP_Command02),
   writeln(Stream, FTP_Command1),
   writeln(Stream, FTP_Command2),
   close(Stream),
%  working_directory(CWD, CWD),
%  atomic_list_concat(['ftp -n < ',CWD,'ftp_call.txt'], FTP_Call),
   atomic_list_concat(['ftp -n < ',CallFile], FTP_Call), 
   kegg_variable_get(examples, WorkingPath),
   chdir(WorkingPath),
   shell(FTP_Call,_),
   kp_info_message('Download Completed!').
   

/*********************************************************************/


