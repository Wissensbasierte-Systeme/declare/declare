

/*********************************************************************/
/***                                                               ***/
/***     KEGGPAD:  ToolBar                                         ***/
/***                                                               ***/
/*********************************************************************/


:- module( kegg_toolbar, [
      keggpad_toolbar_create/2 ] ).


/*** interface *******************************************************/


keggpad_toolbar_create(Dialog, Orientation) :- 
   send(Dialog, append, menu(operations, toggle,
      message(@prolog, select_and_call_operation, Dialog))),
   get(Dialog, member, operations, Toolbar),
   send(Toolbar, show_label, @off), 
   send(Toolbar, layout, Orientation), 
   send(Toolbar, format, left),
   kegg_variable_get(xml, 'kegg_toolbar_config.xml', ConfigFile),
   dread(xml,ConfigFile, [TB_Config]),
   retractall(keggpadbutton(_,_,_,_,_,_)),
%  working_directory(Directory,_),
   !,
   forall( Button := TB_Config/button,
      ( Index := Button@index,
   ImageFile := Button@path,
   Value := Button@value,
   Default_Label := Button/labels@default,
   Pressed_Label := Button/labels@pressed,
   Cmd_Off := Button/commands@off,
   Cmd_On := Button/commands@on,
   ( Value == '@off' ->
     Balloon = Default_Label
   ; Balloon = Pressed_Label ),
   kegg_variable_get(pictures, ImageFile, Imagepath),
%  concat(Directory,ImageFile,Imagepath),
   create_toolbar_button(Toolbar, Index, Imagepath, Balloon, Value),
   assert(keggpadbutton(Index,Cmd_Off, Cmd_On,
           Default_Label, Pressed_Label, Value)) ) ).

create_toolbar_button(Menu, Index, Imagepath, Balloon, Value) :-
   new(Image, image),
   send(Image, load, Imagepath),
   send(Menu, append, new(Button,
      menu_item(value := Index, label := Image))),
   send(Button, selected, Value),
   send(Button, help_message, tag, Balloon).
   

select_and_call_operation(Dialog) :- 
   get(Dialog, member, operations, Toolbar),
   get(Toolbar, item_from_event, Eventitem),
   get(Eventitem, value, Index),
   get(Eventitem, selected, Pressvalue),
   keggpadbutton(Index,Cmd_Off,Cmd_On, Default_Label, Pressed_Label,_),
   ( Pressvalue == @off ->
     ( change_balloon(Eventitem, Default_Label), Cmd_On )
   ; ( change_balloon(Eventitem, Pressed_Label), Cmd_Off ) ).

change_balloon(Item, Newballoon) :-
   send(Item, help_message, tag, Newballoon).


/*********************************************************************/


