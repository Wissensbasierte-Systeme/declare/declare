

/*********************************************************************/
/***                                                               ***/
/***      KEGG API:  Connector & KEGGPAD - Examples & Demos        ***/
/***                                                               ***/
/*********************************************************************/



/*** interface *******************************************************/


get_amino_by_gene(Gene) :- 
   concat('-f -n a ',Gene,Search), 
   bget(Search, Reply), 
   R := Reply/content::'*', 
   writeln(R).

get_dna_by_gene(Gene) :- 
   concat('-f -n n ',Gene,Search), 
   bget(Search, Reply), 
   R := Reply/content::'*', 
   writeln(R), 
   concat(Gene,'.fasta', File), 
   save_sequence(File, R). 

assert_linkdb :- 
   get_linkdb_between_databases('eco', 'pathway', 1, all, Reply), 
   forall( U := Reply/item, 
      ( [V] := U/entry_id1/content::'*', 
        [W] := U/entry_id2/content::'*', 
        assert(linkdb(V,W)), 
        writeln(V-W) ) ).

compound_in_pathway(Compound,Pathway) :-
   get_compounds_by_pathway(Pathway,Reply),
   !,
   findall( R,
      [R] := Reply/descendant::item/content::'*',
      LC ), 
   writeln_list(LC),
   memberchk(Compound, LC).


/* dbget_demo <-
      dbget for swiss-prot. */

dbget_demo :-
   bget('sp:104K_THEAN', Reply), 
   save_sequence('sp_test.txt', Reply).


/* draw_demo(Label, YBox) <-
      Demo: Drawing arrows parallel to the axes. */

draw_demo(Label, YBox) :-
   new(Picture, picture('DEMO')),  
   send(Picture, size, size(300,150)),
   send(Picture, display, 
      new(@Box, box(20,15)), point(150,YBox)),
   send(Picture, display, 
      new(@Circle1, circle(10)), point(50,50)),
   send(Picture, display, 
      new(@Circle2, circle(10)), point(250,150)),
   draw_h_v_arrow(Picture, h_v_arrow, Label,
      Box, Circle1, Circle2, 'reversible'),
   send(Picture, open).


/* adjust_flowvalue_demo(Arrowline, Value) <-
      Demo: Adjust arrow strength. */

adjust_flowvalue_demo(Arrowline, Value) :-
   send(Arrowline, pen, Value),
   new(Arrow, arrow(Value+6, Value*3+6)),
   get(Arrowline, arrows, Status),
   ( ( Status == first
     ; Status == both ) ->
     send(Arrowline, first_arrow, Arrow)
   ; ! ),
   ( ( Status == second
     ; Status == both ) ->
     send(Arrowline, second_arrow, Arrow)
   ; ! ).


/*********************************************************************/


