

/*********************************************************************/
/***                                                               ***/
/***      KEGGPAD:  Local Data Access                              ***/
/***                                                               ***/
/*********************************************************************/



/*** interface *******************************************************/


/* get_kgmlfile(Filename, Kgmlfile) <-
      load the KGML file. */

get_kgmlfile(Filename, Kgmlfile) :-
   dread(xml, Filename, [Kgmlfile]).

check_for_kgmldata(Filename) :-
   dread(xml, Filename, [Kgmlfile]),
   R := Kgmlfile/descendant::entry,
   _ := R@type.


/* get_keggpathway_infos(Kgmlfile, Pathway_Name, Org_Name) <-
      get the names of pathway and organism(abbr). */

get_keggpathway_infos(Kgmlfile, Pathway_Name, Org_Name) :-
   retractall(kgmlinfo(_,_)),
   Pathway_Name := Kgmlfile@title, 
   Org_Name := Kgmlfile@org,
   Pathway_Id := Kgmlfile@name,
   assert(kgmlinfo(Pathway_Id, Pathway_Name, Org_Name)).


/* get_all_keggelements(Keggfile) :-
      get entry element data. */

get_all_keggelements(Keggfile) :-
   retractall(kgmlentry(_, _, _, _, _, _, _, _)),
   findall( Id,
      ( R := Keggfile/descendant::entry,
	Type := R@type,
	Id := R@id,
	Name := R@name,
        ( ( Type = 'gene'; Type = 'ortholog') -> 
            Reaction := R@reaction
          ; Reaction = ''),
	Link := R@link,
	Label := R/graphics@name,
	CX := R/graphics@x,
	CY := R/graphics@y,
	assert( kgmlentry(
           Type, Id, Name, Reaction, Link, Label, CX, CY ) ) ),
      _ ).


/* get_reactions(Keggfile, Type) <-
      get reaction data. */

get_reactions(Keggfile, Type) :-
   ( Type == gene ->
     retractall(kgmlreaction(_, _, _, _, _, _))
   ; ! ),
   forall( kgmlentry(Type, Id, _, Reaction, _, _, _, _),
      ( R := Keggfile/reaction::[@name=Reaction],
        !,
	Dir := R@type,
	findall(S, S := R/substrate@name, SList),
	findall(P, P := R/product@name, PList),
%       writeln(SList),
%       writeln(PList),
        ( ( SList \= [] , PList \= [] ) ->
	  r1(Type, Id, Reaction, Dir, SList, PList, _, _)
        ; ! ) ) ).


/*********************************************************************/


