

/******************************************************************/
/***                                                            ***/
/***          KEGG Pathways: Graph Analysis                     ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(kegg_analysis, kegg_file_to_edges_ds) :-
   home_directory(Home),
   concat([ Home, '/teaching/diplomarbeiten/',
      'Weber_Uwe/2009_10_22/aac00010.xml' ], File),
   kegg_file_to_edges_ds(File, Edges),
   edges_to_picture_bfs(Edges),
   setof( Chain,
      edges_to_chain_ds(Edges, Chain),
      Chains ),
   writeln_list(user, Chains).


/*** interface ****************************************************/


/* kegg_file_to_edges_ds(File, Edges) <-
      */

kegg_file_to_edges_ds(File, Edges) :-
   dread(xml, File, [X]),
   findall( Edge,
      ( Reaction := X/reaction,
        kegg_reaction_to_edge_ds(Reaction, Edge) ),
      Edges ).

kegg_reaction_to_edge_ds(Reaction, V-W) :-
   ( [V,W] := Reaction-[substrate@name, product@name]
   ; reversible := Reaction@type,
     [W,V] := Reaction-[substrate@name, product@name] ).


/* edges_to_chain_ds(Edges, U-V-W) <-
      */

edges_to_chain_ds(Edges, U-V-W) :-
   member(U-V, Edges),
   member(V-W, Edges),
   edges_to_chain_check_ds(Edges, U-V-W).

edges_to_chain_check_ds(Edges, U-V-W) :-
   \+ (member(X-V, Edges), X \= U),
   \+ (member(V-X, Edges), X \= W).


/******************************************************************/


