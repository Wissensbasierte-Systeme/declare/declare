


/******************************************************************/
/***                                                            ***/
/***          Kinase Activation:  Main                          ***/
/***                                                            ***/
/******************************************************************/



/*** data *********************************************************/


blood_data(Tuples) :-
   Tuples = [
      [a,b,1], [c,b,2], [b,d,3], [g,d,3], [e,d,4], [f,d,4] ].

blood_active(Active_Kinases) :-
%  Active_Kinases = [a, b].
   Active_Kinases = [].

blood_should_be_active(Active_Kinases) :-
   Active_Kinases = [d, f].


/*** interface ****************************************************/


blood_data_inference :-
   blood_data_logic_program(Program),
   bar_line, dportray(lp, Program), bar_line, wait,
   tp_iteration(Program, I),
   bar_line, dportray(hi, I), bar_line,
   findall( K,
      ( member(active(K), I),
        \+ member([active(K)], Program) ),
      Ks ),
   writeln_list(Ks).

blood_data_abduction :-
   blood_data_inverse_program(Inverse_Program),
   bar_line, dportray(lp, Inverse_Program), bar_line, wait,
   blood_should_be_active(Active_Kinases),
   findall( [active(K)],
      member(K, Active_Kinases),
      Facts ),
   append(Facts, Inverse_Program, Program),
   tpi_fixpoint_iteration(Program, Models),
   bar_line, dportray(chs, Models), bar_line,
   state_to_tree(Models, Tree),
   set_tree_to_picture_bfs(Tree).


/*** implementation ***********************************************/


blood_data_inverse_program(Inverse_Program) :-
   blood_data_logic_program([Rule|Rules_1]),
   Rule = [active(Y, P)]-[active(X), activates(X, Y, P)],
   findall( [active(Y, P)]-[active(X)],
      member([activates(X, Y, P)], Rules_1),
      Rules_2 ),
   append(Rules_2, Rules_1, Rules_3),
   dportray(lp, Rules_3),
   findall( Rule_2,
      blood_data_inverse_rule(Rules_3, Rule_2),
      Inverse_Program ).

blood_data_inverse_rule(Program, Rule) :-
   setof( Body,
      member(Head-Body, Program),
      Coin ),
   boolean_dualization(Coin, State),
   member(Clause, State),
   Rule = Clause-Head.


blood_data_logic_program(Program) :-
   blood_data_position_rules(Rules),
   blood_data_activation_facts(Facts),
   blood_data_active_facts(Activated),
   Rule = [active(K2, P)]-
      [active(K1), activates(K1, K2, P)],
   append([[Rule|Rules], Facts, Activated], Program).

blood_data_activation_facts(Facts) :-
   blood_data(Ts),
   findall( [activates(X, Y, P)],
      member([X, Y, P], Ts),
      Facts ).

blood_data_position_rules(Rules) :-
   blood_data(Ts),
   findall( [active(Y)]-Body,
      ( blood_data_to_kinase_positions(Ts, Y-Ps),
        kinase_positions_to_body(Y-Ps, Body) ),
      Rules ).

blood_data_active_facts(Facts) :-
   blood_active(Ks),
   findall( [active(K)],
      member(K, Ks),
      Facts ).

blood_data_to_kinase_positions(Ts, Y-Ps) :-
   setof( P,
      X^member([X, Y, P], Ts),
      Ps ).

kinase_positions_to_body(Y-Ps, Body) :-
   findall( active(Y, P),
      member(P, Ps),
      Body ).

/*

active(X1, X2) :- active(X3),activates(X3, X1, X2).
active(b) :- active(b, 1),active(b, 2).
active(d) :- active(d, 3),active(d, 4).
activates(a, b, 1).
activates(c, b, 2).
activates(b, d, 3).
activates(g, d, 3).
activates(e, d, 4).
activates(f, d, 4).

*/


/******************************************************************/


