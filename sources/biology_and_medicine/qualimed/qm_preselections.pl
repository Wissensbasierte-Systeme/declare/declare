

/******************************************************************/
/***                                                            ***/
/***             QualiMed:  Preselections                       ***/
/***                                                            ***/
/******************************************************************/


/* qm_preselections <-
      temporary predicate */

qm_preselections([]).


/* qm_compute_preselection <-
      */

qm_compute_preselection(D,D_Name,P) :-
   get(D,members,Members),
   list_to_chain(P,P_C),
   send(Members,for_some,
      message(@prolog,
         qm_compute_item_preselection,D_Name,@arg1,P_C)).

qm_compute_item_preselection(D_Name,Item,P_C) :-
   get(Item,name,I_Name),
   qdb_preselection(D_Name,I_Name,qm_preselection_tps_fixpoint),
   chain_to_list(P_C,P1),
   dconsult('orthopaedie/preselections',P2),
   append(P1,P2,P),   
   Pred =.. [qm_preselection_tps_fixpoint,D_Name,I_Name,P,X],
   call(Pred),
   X = [Y],
   send(Item,selection,Y).
qm_compute_item_preselection(D_Name,Item,P_C) :-
   get(Item,name,I_Name),
   qdb_preselection(D_Name,I_Name,qm_preselection_date),
   chain_to_list(P_C,P1),
   dconsult('orthopaedie/preselections',P2),
   append(P1,P2,P),   
   Pred =.. [qm_preselection_date,D_Name,I_Name,P,X],
   call(Pred),
   X = [Y],
   send(Item,selection,Y).
qm_compute_item_preselection(D_Name,Item,_) :-
   get(Item,name,I_Name),
   qdb_preselection(
      D_Name,I_Name,qmp_current_entity(Dialog,D_Item)),
   qdb_current_entity(E),
   qdb_current_keys(K),
   qdb_key_selection(E,K,P),
   remove_keys(P,P1),
   get_dialog_entry(Dialog,D_Item,P1,Y),
   send(Item,selection,Y).

qm_preselection_tps_fixpoint(D_Name,I_Name,P,Preselection) :-
   tps_fixpoint_iteration(P,S),
   findall( Y,
      ( X =.. [D_Name,I_Name,Y],
        member([X],S) ),
      Preselection ).

qm_preselection_date(_,_,_,[Date]) :-
   new(D,date),
   get(D,print_name,X),
   object(X,Date).


/******************************************************************/


