

/******************************************************************/
/***                                                            ***/
/***             QualiMed:  Warnings                            ***/
/***                                                            ***/
/******************************************************************/


/* qm_warning(A,B) <-
      */

qm_warning(incomplete,X) :-
   new(Dialog,dialog(warnung)),
   name_append('Der Eintrag im Feld "',X,Y),
   name_append(Y,'" darf nicht leer sein',Z),
   send(Dialog,append,label(X,Z)),
   send(Dialog,append,button(ok,message(Dialog,destroy))),
   send(Dialog,open).

qm_warning(existence_dependency,[A,B]) :-
   new(Dialog,dialog(warnung)),
   name_append('Die Daten vom Typ ',A,Y),
   name_append(Y,' sind abhaengig von Daten des Typs ',Z),
   name_append(Z,B,X),
   send(Dialog,append,label(exdep1,X)),
   send(Dialog,append,label(exdep2,
      'Sie muessen erst einen entsprechenden Datentyp laden.')),
   send(Dialog,append,button(ok,message(Dialog,destroy))),
   send(Dialog,open).


/******************************************************************/


