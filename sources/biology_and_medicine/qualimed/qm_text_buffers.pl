

/******************************************************************/
/***                                                            ***/
/***             QualiMed:  Text Buffers                        ***/
/***                                                            ***/
/******************************************************************/


/* chain_to_text_buffer(E_Name,C,Text_Buffer) <-
      */

chain_to_text_buffer(E_Name,C,Text_Buffer) :-
   new(C_A,char_array(E_Name)),
   get(C_A,upcase,E),
   send(Text_Buffer,append,E),
   name(A,[10]),
   send(Text_Buffer,append,A),
   get(C,head,H),
   tuples_to_text_buffer(C,H,Text_Buffer).

/* not last tuple, subdialog */

tuples_to_text_buffer(C,H,Text_Buffer) :-
   get(C,next,H,Next),
   get(H,first,A_Name),
   get(H,second,Value),
   send(Value,instance_of,chain),
   get(Value,head,Head),
   send(Head,instance_of,tuple),
   send(Text_Buffer,append,A_Name),
   name(A,[10]),
   send(Text_Buffer,append,A),
   tuples_to_text_buffer(Value,Head,Text_Buffer),
   tuples_to_text_buffer(C,Next,Text_Buffer).

/* last tuple, subdialog */

tuples_to_text_buffer(_,H,Text_Buffer) :-
   get(H,first,A_Name),
   get(H,second,Value),
   send(Value,instance_of,chain),
   get(Value,head,Head),
   send(Head,instance_of,tuple),
   send(Text_Buffer,append,A_Name),
   name(A,[10]),
   send(Text_Buffer,append,A),
   tuples_to_text_buffer(Value,Head,Text_Buffer).

/* not last tuple, multi value */

tuples_to_text_buffer(C,H,Text_Buffer) :-
   get(C,next,H,Next),
   get(H,first,A_Name),
   get(H,second,Value),
   send(Value,instance_of,chain),
   get(Value,head,Head),
   send(Text_Buffer,append,A_Name),
   send(Text_Buffer,append,': '),
   multi_value_to_text_buffer(Value,Head,Text_Buffer),
   name(A,[10]),
   send(Text_Buffer,append,A),
   tuples_to_text_buffer(C,Next,Text_Buffer).

/* not last tuple, multi value (empty) */

tuples_to_text_buffer(C,H,Text_Buffer) :-
   get(C,next,H,Next),
   get(H,first,A_Name),
   get(H,second,Value),
   send(Value,instance_of,chain),
   send(Text_Buffer,append,A_Name),
   send(Text_Buffer,append,': '),
   name(A,[10]),
   send(Text_Buffer,append,A),
   tuples_to_text_buffer(C,Next,Text_Buffer).

/* last tuple, multi value */

tuples_to_text_buffer(_,H,Text_Buffer) :-
   get(H,first,A_Name),
   get(H,second,Value),
   send(Value,instance_of,chain),
   get(Value,head,Head),
   send(Text_Buffer,append,A_Name),
   send(Text_Buffer,append,': '),
   multi_value_to_text_buffer(Value,Head,Text_Buffer).

/* last tuple, multi value (empty) */

tuples_to_text_buffer(_,H,Text_Buffer) :-
   get(H,first,A_Name),
   get(H,second,Value),
   send(Value,instance_of,chain),
   send(Text_Buffer,append,A_Name),
   send(Text_Buffer,append,': ').

/* not last tuple, single value (not filled in) */

tuples_to_text_buffer(C,H,Text_Buffer) :-
   get(C,next,H,Next),
   get(H,first,A_Name),
   get(H,second,@default),
   send(Text_Buffer,append,A_Name),
   send(Text_Buffer,append,': '),
   send(Text_Buffer,append,'k. A.'),
   name(A,[10]),
   send(Text_Buffer,append,A),
   tuples_to_text_buffer(C,Next,Text_Buffer).

/* not last tuple, single value */

tuples_to_text_buffer(C,H,Text_Buffer) :-
   get(C,next,H,Next),
   get(H,first,A_Name),
   get(H,second,Value),
   send(Text_Buffer,append,A_Name),
   send(Text_Buffer,append,': '),
   send(Text_Buffer,append,Value),
   name(A,[10]),
   send(Text_Buffer,append,A),
   tuples_to_text_buffer(C,Next,Text_Buffer).

/* last tuple, sigle value (not filled in) */

tuples_to_text_buffer(_,H,Text_Buffer) :-
   get(H,first,A_Name),
   get(H,second,@default),
   send(Text_Buffer,append,A_Name),
   send(Text_Buffer,append,': '),
   send(Text_Buffer,append,'k. A.'),
   name(A,[10]),
   send(Text_Buffer,append,A).

/* last tuple, sigle value */

tuples_to_text_buffer(_,H,Text_Buffer) :-
   get(H,first,A_Name),
   get(H,second,Value),
   send(Text_Buffer,append,A_Name),
   send(Text_Buffer,append,': '),
   send(Text_Buffer,append,Value),
   name(A,[10]),
   send(Text_Buffer,append,A).


/* multi_value_to_text_buffer(C,Head,Text_Buffer) <-
      */

multi_value_to_text_buffer(_,@default,_).
multi_value_to_text_buffer(C,Head,Text_Buffer) :-
   send(Text_Buffer,append,Head),
   get(C,next,Head,Next),
   send(Text_Buffer,append,', '),
   multi_value_to_text_buffer(C,Next,Text_Buffer).
multi_value_to_text_buffer(_,Head,Text_Buffer) :-
   send(Text_Buffer,append,Head).


/* program_to_text_buffer(Program,Text_Buffer) <-
      */

program_to_text_buffer([[Atom]|Rest],Text_Buffer) :-
   Atom =.. [_,_,_,Name,Value],
   new(C,char_array(Name)),
   get(C,capitalise,Name1),
   send(Text_Buffer,append,' ',5),
   send(Text_Buffer,append,Name1),
   send(Text_Buffer,append,': '),
   send(Text_Buffer,append,Value),
   name(A,[10]),
   send(Text_Buffer,append,A),
   program_to_text_buffer(Rest,Text_Buffer).


/* args_to_text_buffer(Args,Text_Buffer) <-
      */

args_to_text_buffer([Arg|Rest],Text_Buffer) :-
   is_list(Arg),
   !,
   args_to_text_buffer(Arg,Text_Buffer),
   args_to_text_buffer(Rest,Text_Buffer).
args_to_text_buffer([Arg|Rest],Text_Buffer) :-
   send(Text_Buffer,append,Arg),
   send(Text_Buffer,append,' '),
   args_to_text_buffer(Rest,Text_Buffer).
args_to_text_buffer([],_).


/******************************************************************/


