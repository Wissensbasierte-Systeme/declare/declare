

/******************************************************************/
/***                                                            ***/
/***             QualiMed:  Database Operations                 ***/
/***                                                            ***/
/******************************************************************/


/* qm_entity_to_filename(E_Name,Key,Filename) <-
      */

qm_entity_to_filename(E_Name,Key,Filename) :-
   qualimed_variable(e_r_model,ER),
   name_append(['medicine/qualimed/',ER,'/',E_Name,Key],Filename).


/*** interface ****************************************************/


/* qm_create_entity(+Entity) <-
      startet die Reihe der Eingabedialoge f�r +Entity */

/* case 1: ED relationship for New_Entity
      and correct current_entity */

qm_create_entity(@nil).
       
qm_create_entity(E_Name) :-
   qm_start_entity(E_Name),
%  qualimed_variable(current_entity,C_Entity),
   findall( [R,Dep_Entity],
      qm_relationship_type(R,Dep_Entity,E_Name,[1,1],_),
      Ex_Dep ),
   qm_get_ex_dep_keys(Ex_Dep,Ex_Dep_Keys),
%  qm_relationship_type(R,C_Entity,E_Name,[1,1],_),
   !,
%  qualimed_variable(current_key,C_Key),
   qm_create_entity_key(E_Name,New_Key),
   qualimed_variable_set(active_entity,E_Name),
   qualimed_variable_set(active_key,New_Key),
%  qm_create_relationship_key(R,R_Key),
   make_dialog_chain(E_Name,C),
   qualimed_variable(dialog_state,State),
   findall( Tuple,
      ( member([preselection(Name,Value)],State),
        Tuple = [Name,Value] ),
      Tuple_List ),
   tuples_to_chain(Tuple_List,C),
   qm_dialog_input(E_Name,C,Answer),
   ( Answer = @nil
   ; ( Answer = @on,
       qm_save_relationship_list(Ex_Dep_Keys,New_Key),
%      qm_save_relationship(R,R_Key,C_Key,New_Key),
       qm_save_entity(E_Name,New_Key,C),
       qm_load_entity(E_Name,New_Key),
       qm_compute_next_entity(Next_Entity),
       qm_create_entity(Next_Entity) )
     ; Answer = @off ).

qm_create_entity(E_Name) :-
   qualimed_variable(current_entity,C_Entity),
   findall( [R,Dep_Entity],
      qm_relationship_type(R,Dep_Entity,E_Name,[1,1],_),
      Ex_Dep ),
   qm_get_ex_dep_keys(Ex_Dep,Ex_Dep_Keys),
%  qm_relationship_type(R,C_Entity,E_Name,[1,1],_),
   !,
   qualimed_variable(current_key,C_Key),
   qm_create_entity_key(E_Name,New_Key),
   qualimed_variable_set(active_entity,E_Name),
   qualimed_variable_set(active_key,New_Key),
%  qm_compute_preselections(Pre_Prog),
%  append(C_Prog,Pre_Prog,P),
%  qm_create_relationship_key(R,R_Key),
%  tps_fixpoint_iteration(P,State),
   make_dialog_chain(E_Name,C),
   qualimed_variable(dialog_state,State),
   findall( Tuple,
      ( member([preselection(Name,Value)],State),
        Tuple = [Name,Value] ),
      Tuple_List ),
   tuples_to_chain(Tuple_List,C),
   qm_dialog_input(E_Name,C,Answer),
   ( Answer = @nil
   ; ( Answer = @on,
       qm_save_relationship_list(Ex_Dep_Keys,New_Key),
%      qm_save_relationship(R,R_Key,C_Key,New_Key),
       qm_save_entity(E_Name,New_Key,C),
       qm_load_entity(E_Name,New_Key),
       qm_compute_next_entity(Next_Entity),
       qm_load_entity(C_Entity,C_Key),
       qm_create_entity(Next_Entity) )
     ; Answer = @off ).
   
/* case 2: ED relationship for New_Entity
      and wrong current_entity */

qm_create_entity(New_Entity) :-
   qualimed_variable(current_entity,C_Entity),
   qm_relationship_type(_,P_Entity,New_Entity,[1,1],_),
   C_Entity \= P_Entity,
   !,
   qm_warning(existence_dependency,[C_Entity,New_Entity]).

/* case 3: ED relationship for New_Entity
      and no current_entity */

qm_create_entity(New_Entity) :-
   qualimed_variable(current_entity,@nil),
   qm_relationship_type(_,C_Entity,New_Entity,[1,1],_),
   !,
   qm_warning(existence_dependency,[C_Entity,New_Entity]).

/* case 4: no ED relationship */

qm_create_entity(E_Name) :-
   qm_create_entity_key(E_Name,New_Key),
   qualimed_variable_set(active_entity,E_Name),
   qualimed_variable_set(active_key,New_Key),
   make_dialog_chain(E_Name,C),
   qualimed_variable(dialog_state,State),
   findall( Tuple,
      ( member([preselection(Name,Value)],State),
        Tuple = [Name,Value] ),
      Tuple_List ),
   tuples_to_chain(Tuple_List,C),
   write_ln(bla),
   qm_dialog_input(E_Name,C,Answer),
   ( Answer = @nil
   ; ( Answer = @on,
%      qm_create_entity_key(E_Name,New_Key),
       qm_save_entity(E_Name,New_Key,C),
       qm_load_entity(E_Name,New_Key),
       qm_compute_next_entity(Next_Entity),
       qm_create_entity(Next_Entity) )
%      qm_create_entity(@nil) )
     ; Answer = @off ).


/* qm_get_ex_dep_keys <-
      */
  
qm_get_ex_dep_keys([],_) :- 
   !,
   fail.
qm_get_ex_dep_keys([[R,C_Entity]],[[R,C_Entity,C_Key]]) :-
   qualimed_variable(current_entity,C_Entity),
   qualimed_variable(current_key,C_Key).
qm_get_ex_dep_keys([H|T],[K_H|K_T]) :-
   qm_get_all_ex_dep_keys([H|T],[K_H|K_T]).

qm_get_all_ex_dep_keys([],[]).
qm_get_all_ex_dep_keys([[R,E]|T],[[R,E,K]|K_T]) :-   
   qm_ask_user_for_key(R,E,K),
   qm_get_all_ex_dep_keys(T,K_T).

qm_ask_user_for_key(R,E,K) :-
   qualimed_variable(current_dataset,DB),
   findall( Key,
      member([qm_entity(E,Key,_)],DB),
      Key_List ),
   list_to_chain(Key_List,C),
   new(Dialog,dialog(R)),
   new(Browser,browser),
   send(Browser,members,C),
   qm_change_keys_for_exdep(Browser,E),
   send(Dialog,append,Browser),
   send(Dialog,append,new(_,button(ok,
      message(Dialog,return,Browser?selection?key)))),
   send(Dialog,open),
   get(Dialog,confirm_centered,K),
   send(Dialog,destroy).
   

/* qm_save_relationship_list <-
      */

qm_save_relationship_list([[R,_,K]|T],New_Key) :-
   qm_create_relationship_key(R,R_Key),
   qm_save_relationship(R,R_Key,K,New_Key),
   qm_save_relationship_list(T,New_Key).
qm_save_relationship_list([],_).


/* qm_create_file_entity(E) <-
      */

qm_create_file_entity(E) :-
   qm_file_entity(E),
   qm_close_entity,
   qm_check_rules([[next_entity(E)],[user_creates_entity]]),
   qm_create_entity(E).


/* qm_create_start_entity(E) <-
      */

qm_create_start_entity(_) :-
   qualimed_variable(current_entity,@nil),
   !,
   fail.
qm_create_start_entity(E) :-
   qm_check_rules([[next_entity(E)],[user_creates_entity]]),
   qm_create_entity(E).


/* qm_compute_next_entity(-Next_Entity) <-
      */

/*
qm_compute_next_entity(Next_Entity) :-
   qualimed_variable(mode,attribute_mode),
   dconsult('qualimed/next_entity_am',P),
   qualimed_variable(current_entity,E),
   qualimed_variable(current_key,K),
   qualimed_variable(current_dataset,DB),
   append([[qm_current_entity(E)],[qm_current_key(K)]|DB],P,Prog),
   tpi_fixpoint_iteration(Prog,Coin),
   coin_to_state(Coin,State),
   member([next_entity(Next_Entity)],State).
qm_compute_next_entity(Next_Entity) :-
   qualimed_variable(mode,entity_mode),
   dconsult('qualimed/next_entity_em',P),
%  qm_current_entity(E),
%  qm_current_key(K),
   qualimed_variable(current_dataset,DB),
%  append([[qm_current_entity(E)],[qm_current_key(K)]|DB],P,Prog),
   append(P,DB,Prog),
   list_to_ord_set(Prog,Ord_Prog),
   dislog_flag_set(join_mode,special),
   tpi_fixpoint_iteration(Ord_Prog,Coin),
   dislog_flag_set(join_mode,regular),
   coin_to_state(Coin,State),
   member([next_entity(Next_Entity)],State).
qm_compute_next_entity(Next_Entity) :-
   qualimed_variable(mode,attribute_list_mode),
   dconsult('qualimed/next_entity_al',P),
   qualimed_variable(current_entity,E),
   qualimed_variable(current_key,K),
   qualimed_variable(current_dataset,DB),
   append([[qm_current_entity(E)],[qm_current_key(K)]|DB],P,Prog),
   tpi_fixpoint_iteration(Prog,Coin),
   coin_to_state(Coin,State),
   member([next_entity(Next_Entity)],State).
*/

qm_compute_next_entity(Next_Entity) :-
   qualimed_variable(dialog_state,State),
   member([next_entity(Next_Entity)],State),
   !.
qm_compute_next_entity(@nil).


/* qm_save_entity(+Entity,+Key,+Selections) <-
      save Entity in file and in the current set of facts. */

qm_save_entity(E,Key,Selections) :-
   qm_file_entity(E),
   qualimed_variable(mode,attribute_list_mode),
   qm_selections_to_program(E,Key,Selections,Prog),
   qm_close_entity,
   qm_entity_to_filename(E,Key,F),
   qualimed_variable(current_dataset,DB),
   qm_append_attributes(Prog,DB,P),
   list_to_ord_set(P,P1),
   qualimed_variable_set(current_dataset,P1),
   qm_get_key_attributes(E,Prog,Key_Attributes),
   qm_append_keys_to_keylist(E,Key,Key_Attributes),
   qm_save_database(P1,F).
qm_save_entity(E,Key,Selections) :-
   qm_file_entity(E),
   ( qualimed_variable(mode,entity_mode)
   ; qualimed_variable(mode,attribute_mode) ),
   qm_selections_to_program(E,Key,Selections,Prog),
   qm_close_entity,
   qm_entity_to_filename(E,Key,F),
   list_to_ord_set(Prog,P1),
   qualimed_variable_set(current_dataset,P1),
   qm_get_key_attributes(E,Key,Prog,Key_Attributes),
   qm_append_keys_to_keylist(E,Key,Key_Attributes),
   qm_save_database(P1,F).
qm_save_entity(E,Key,Selections) :-
   qualimed_variable(mode,attribute_list_mode),
   qm_selections_to_program(E,Key,Selections,Prog),
   qualimed_variable(current_dataset,DB),
   qm_append_attributes(Prog,DB,P),
   list_to_ord_set(P,P1),
   qualimed_variable_set(current_dataset,P1),
   qm_save_database(P1).
qm_save_entity(E,Key,Selections) :-
   ( qualimed_variable(mode,entity_mode)
   ; qualimed_variable(mode,attribute_mode) ),
   qm_selections_to_program(E,Key,Selections,Prog),
   qualimed_variable(current_dataset,DB),
   append(DB,Prog,P),
   list_to_ord_set(P,P1),
   qualimed_variable_set(current_dataset,P1),
   qm_save_database(P1).


/* qm_get_key_attributes(E,Key,Prog,K_Att) <-
      */

qm_get_key_attributes(E,Key,Prog,K_Att) :-
   qm_keys(E,Att_List),
   member([qm_entity(E,Key,Attributes)],Prog),
   qm_get_keylist(Att_List,Attributes,K_Att).

qm_get_keylist([],_,[]).
qm_get_keylist([H|T],Attributes,[A|B]) :-
   member([H,A],Attributes),
   qm_get_keylist(T,Attributes,B).


/* qm_append_keys_to_keylist(E,Key,Key_Attributes) <-
      */

qm_append_keys_to_keylist(E,Key,Key_Attributes) :-
   list_name_append(Key_Attributes,Label),
   qualimed_variable(current_keylist,Key_List),
   qualimed_variable_set(
      current_keylist,[[qm_keys(E,Key,Label)]|Key_List]),
   qm_save_keylist.


/* qm_save_keylist <-
      */

qm_save_keylist :-
   qualimed_variable(current_keylist,Key_List),
   qualimed_variable(e_r_model,ER_Model),
   name_append(['qualimed/',ER_Model,'/keylist'],File),
   qdb_store(Key_List,File).


/* qm_load_keylist <-
      */

qm_load_keylist :-
   qualimed_variable(e_r_model, ER_Model),
   name_append(['medicine/qualimed/', ER_Model, '/keylist'], File),
   qdb_load(File, Key_List),
   qualimed_variable_set(current_keylist, Key_List).


/* qm_append_attributes <-
      */

qm_append_attributes([],DB,DB).
qm_append_attributes([H|T],DB,P) :-
   H = [qm_entity(_,_)],
   qm_append_attributes(T,[H|DB],P).
qm_append_attributes([H|T],DB,P) :-
   H = [qm_attribute(E_Name,A_Name,Key,Value)],
   X = [qm_attribute(E_Name,A_Name,A_List)],
   member(X,DB),
   subtract(DB,[X],DB2),
   qm_append_attributes(T,[[qm_attribute(E_Name,A_Name,
      [[Key,Value]|A_List])]|DB2],P).
qm_append_attributes([H|T],DB,P) :-
   H = [qm_attribute(E_Name,A_Name,Key,Value)],
   qm_append_attributes(T,[[qm_attribute(E_Name,A_Name,
      [[Key,Value]])]|DB],P).

/*
qm_save_database(P) :-
   qualimed_variable(current_entity,E),
   qm_file_entity(E),
   !,
   qualimed_variable(current_key,K),
   qm_entity_to_filename(E,K,File),
   qdb_store(P,File).
*/

qm_save_database(P,File) :-
   qualimed_variable_set(current_file,File),
   qdb_store(P,File).

qm_save_database(P) :-
   qualimed_variable(current_file,File),
   qdb_store(P,File).


/*
qm_set_current_database(P) :-
   retractall(qm_current_database(_)),
   assert(qm_current_database(P)).
*/


/* qm_selections_to_program(+E,+Key,+Selections,-Prog) <-
      wandelt eine XPCE-Chain, die Attribute zur Enitity E
      mit Schl�ssel Key enth�lt, in ein DisLog Programm um */
  
qm_selections_to_program(
      E,Key,Selections,[[qm_entity(E,Key)]|S_Prog]) :-
   qm_long_selection(E,Key,Selections,S_Prog).

qm_selections_to_program(R,R_K,K1,K2,Selections,
      [[qm_relationship(R,R_K,K1,K2)]|S_Prog]) :-
   qm_long_selection(R,R_K,Selections,S_Prog).

/*
qm_long_selection(E,K,C,[[S]]) :-
   qm_mode(entity_mode),
   chain_to_list(C,L),
   S =.. [E,K|L].
*/

qm_long_selection(E,K,C,[[S]]) :-
   qualimed_variable(mode,entity_mode),
   chain_to_list(C,L),
   S =.. [qm_entity,E,K,L].

qm_long_selection(E,K,C,S) :-
   ( qualimed_variable(mode,attribute_mode)
   ; qualimed_variable(mode,attribute_list_mode) ),
   chain_to_list(C,L),
   qm_tuples_to_attributes(E,K,L,S).

qm_tuples_to_attributes(_,_,[],[]).
qm_tuples_to_attributes(E,K,[[_,V]|L],S) :-
   V = [[_,_]|_],
   qm_tuples_to_attributes(E,K,V,S1),
   qm_tuples_to_attributes(E,K,L,S2),
   append(S1,S2,S).
qm_tuples_to_attributes(
      E,K,[[A,V]|L],[[qm_attribute(E,A,K,V)]|S]) :-
   qm_tuples_to_attributes(E,K,L,S). 


/* qm_program_to_selections(+E,+P,-C) <-
      liefert eine XPCE-Chain f�r die Entity E,
      die Attribute mit den Werten aus P enth�lt */ 

qm_program_to_selections(E,[[Fact]],C) :-
   qualimed_variable(mode,entity_mode),
   Fact =.. [qm_entity,E,_,P],
   make_dialog_chain(E,C),
   get(C,head,H),
   values_to_chain(P,C,H).

qm_program_to_selections(_,[],_) :-
   ( qualimed_variable(mode,attribute_list_mode)
   ; qualimed_variable(mode,attribute_mode) ).
qm_program_to_selections(E,P,C) :-
   ( qualimed_variable(mode,attribute_list_mode)
   ; qualimed_variable(mode,attribute_mode) ),
   make_dialog_chain(E,C),
   qm_program_values_to_chain(P,C).

qm_program_values_to_chain([H|T],C) :-
   get(C,head,Head),
   H = [qm_attribute(_,A_Name,_,Value)],
   set_tuple_value(C,Head,A_Name,Value),
   qm_program_values_to_chain(T,C).
qm_program_values_to_chain([],_).


/* qm_save_relationship(+R_Name,+R_Key,+Key1,+Key2,+S) <-
      sichert Relationship */

/*
qm_save_relationship(R,R_Key,Key1,Key2,S) :-
   qm_selections_to_program(R,R_Key,Key1,Key2,S,Prog),
   qm_current_database(DB),
   append(DB,Prog,P),
   list_to_ord_set(P,P1),
   qm_set_current_database(P1),
   qm_save_database(P1).

qm_save_relationship(R,R_Key,Key1,Key2) :-
   qm_current_database(DB),
   append(DB,[[qm_relationship(R,R_Key,Key1,Key2)]],P),
   list_to_ord_set(P,P1),
   qm_set_current_database(P1),
   qm_save_database(P1).
*/

qm_save_relationship(R,R_Key,Key1,Key2,S) :-
   qm_selections_to_program(R,R_Key,Key1,Key2,S,Prog),
   qualimed_variable(current_dataset,DB),
   append(DB,Prog,P),
   list_to_ord_set(P,P1),
   qualimed_variable_set(current_dataset,P1),
   qm_save_database(P1).

qm_save_relationship(R,R_Key,Key1,Key2) :-
   qualimed_variable(current_dataset,DB),
   append(DB,[[qm_relationship(R,R_Key,Key1,Key2)]],P),
   list_to_ord_set(P,P1),
   qualimed_variable_set(current_dataset,P1),
   qm_save_database(P1).


/* qm_get_keys(+Entity,+E_Program,-Keys) <-
      liefert Schl�ssel zu +Entity mit Dialogfolge +E_Program */  

qm_get_keys(_,[],[]).
qm_get_keys(E,P,K) :-
   qm_keys(E,Key_Names),
   get_key(Key_Names,P,K),
   !.
qm_get_keys(E,_,[K]) :-
   create_key(E,K).


/* qdb_save(+E,+P,+K) <-
      speichert Entity E mit Dialogfolge P und Schl�sseln +K */

qdb_save(_,[],_).
qdb_save(E,P,K) :-
   qm_file(E,F),
   qdb_load(F,P2),
   qm_entity(E,[D|_]),
   member([X],P2),
   X =.. [D,K|_],
   write_ln('Schluessel existiert bereits'),
   edit_input([],P,Result),
   qm_get_keys(E,Result,K2),
   qdb_save(E,Result,K2).
 
qdb_save(E,P,K) :-
   insert_keys(P,K,P1),
   qm_file(E,F),
   qdb_load(F,P2),
   append(P1,P2,P3),
   qdb_store(P3,F).


/* qdb_store(+P,+F) <-
      speichert Programm +P in Dislog-Syntax in File +F */

qdb_store(P,F) :-
   dsave_atomic(P,F).
   
/* qdb_load(+F,-P) <-
      liest File +F ein und legt das enthaltene Programm
      in Dislog-Syntax in der Variable -P ab */

qdb_load(F,P) :-
   dconsult(F,P).


/* qm_edit_entity(+E_Name,+Keys) <-
      */

qm_edit_entity :-
   qualimed_variable(current_entity,E),
   qualimed_variable(current_key,K),
   qm_edit_entity(E,K).

qm_edit_entity(@nil,_).
qm_edit_entity(E_Name,Key) :-
   qm_file_entity(E_Name),
   qualimed_variable(mode,attribute_list_mode),
   !,
   qm_key_selection(E_Name,Key,P),
   qm_program_to_selections(E_Name,P,C),
   qm_dialog_input(E_Name,C,Answer),
   ( Answer = @nil
   ; Answer = @off
   ; ( Answer = @on,
       qm_remove_entity(E_Name,Key),
       qm_selections_to_program(E_Name,Key,C,Prog),
       qualimed_variable(current_dataset,DB),
       qm_append_attributes(Prog,DB,DB2),
       list_to_ord_set(DB2,DB3),
       qm_save_database(DB3),
%      qm_save_entity(E_Name,Key,C),        
       qm_load_entity(E_Name,Key) ) ).
qm_edit_entity(E_Name,Key) :-
   qm_file_entity(E_Name),
   !,
   qm_key_selection(E_Name,Key,P),
   qm_program_to_selections(E_Name,P,C),
   qm_dialog_input(E_Name,C,Answer),
   ( Answer = @nil
   ; Answer = @off
   ; ( Answer = @on,
       qm_remove_entity(E_Name,Key),
       qm_selections_to_program(E_Name,Key,C,Prog),
       qualimed_variable(current_dataset,DB),
       append(DB,[[qm_entity(E_Name,Key)]|Prog],DB2),
       list_to_ord_set(DB2,DB3),
       qm_save_database(DB3),
       qm_load_entity(E_Name,Key),
       qm_update_keylist(E_Name,Key) ) ).
qm_edit_entity(E_Name,Key) :-
   qm_key_selection(E_Name,Key,P),
   qm_program_to_selections(E_Name,P,C),
   qm_dialog_input(E_Name,C,Answer),
   ( Answer = @nil
   ; Answer = @off
   ; ( Answer = @on,
       qm_remove_entity(E_Name,Key),
       qm_save_entity(E_Name,Key,C),
       qm_load_entity(E_Name,Key) ) ).


/* qm_update_keylist(E,K) <-
      */

qm_update_keylist(E,K) :-
   qualimed_variable(current_dataset,DB),
   qm_get_key_attributes(E,K,DB,A),
   qualimed_variable(current_keylist,K_L),
   subtract(K_L,[[qm_keys(E,K,_)]],New_K_L),
   qualimed_variable_set(current_keylist,New_K_L),   
   qm_append_keys_to_keylist(E,K,A).


/* qm_save_result(E_Name,Keys,Result) <-
      */

qm_save_result(_,_,[]) :-
   !.
qm_save_result(E_Name,Keys,Result) :-
   qm_get_keys(E_Name,Result,New_Keys),
   qm_remove_entity(E_Name,Keys),
   qdb_save(E_Name,Result,New_Keys),
   qm_update_keys(E_Name,Keys,New_Keys),
   qm_load_entity(E_Name,New_Keys).

qm_update_keys(_,K,K) :-
   !.
qm_update_keys(E_Name,Keys1,Keys2) :-
   findall( X,
      qm_entity_is_related(E_Name,Keys1,X),
      R_List ),
   qm_update_rel_list(R_List,Keys1,Keys2).

qm_update_rel_list([R|R_List],Keys1,Keys2) :-
   qm_update_relationship(R,Keys1,Keys2),
   qm_update_rel_list(R_List,Keys1,Keys2).
qm_update_rel_list([],_,_).

qm_update_relationship([R_Name,[K1,K2]],Keys1,Keys2) :-
   qm_file(R_Name,File),
   qm_relationship(R_Name,Dialogs,_,_,_,_),
   qdb_load(File,P),
   qdb_key_selection(P,[K1,K2],Dialogs,S),
   subtract(P,S,New_P),
   remove_keys(S,S2),
   ( ( Keys1 = K1,
       insert_keys(S2,[Keys2,K2],S3) )
   ; ( Keys1 = K2,
       insert_keys(S2,[K1,Keys2],S3) ) ),
   append(New_P,S3,Updated_P),
   qdb_store(Updated_P,File).
   

/* qm_close_entity <-
      */

/*
qm_close_entity :-
   send(@view,clear),
   send(@qdb_browser1,clear),
   send(@qdb_browser2,clear),
   retract(qdb_current_entity(_)),
   assert(qdb_current_entity(@nil)),
   retract(qdb_current_keys(_)),
   assert(qdb_current_keys(@nil)).
*/

qm_close_entity :-
   send(@view,clear),
   send(@qdb_browser1,clear),
   send(@qdb_browser2,clear),
   qualimed_variable_set(current_entity,@nil),
   qualimed_variable_set(current_key,@nil),
   qualimed_variable_set(current_dataset,[]),
   qualimed_variable_set(current_file,@nil).


/* qm_delete_entity */

qm_delete_entity :-
   qualimed_variable(current_entity,Entity_Name),
   qualimed_variable(current_key,Key),
   qm_delete_entity(Entity_Name,Key),
   qm_close_entity.

qm_delete_entity(@nil,_).
qm_delete_entity(Entity_Name,Keys) :-
   qm_file_entity(Entity_Name),
   qm_delete_file(Entity_Name,Keys),
   qualimed_variable(current_keylist,L),
   subtract(L,[[qm_keys(Entity_Name,Keys,_)]],New_L),
   qualimed_variable_set(current_keylist,New_L),
   qm_save_keylist,
   !.
qm_delete_entity(Entity_Name,Keys) :-
   qm_delete_ed_entities(Entity_Name,Keys),
   qm_delete_all_relationships(Entity_Name,Keys),
   qm_remove_entity(Entity_Name,Keys).

qm_delete_ed_entities(Entity_Name,Keys) :-
   findall( X,
      qdb_ed_selection(_,Entity_Name,Keys,X),
      E_List ),
   qm_delete_entity_list(E_List).

qm_delete_entity_list([A|B]) :-
   A = [E_Name,Keys],
   qm_delete_entity(E_Name,Keys),
   qm_delete_entity_list(B).
qm_delete_entity_list([]).

qdb_ed_selection(
      Rel_Name,Entity_Name,Key,[Related_E,Related_Key]) :-
   qm_relationship_type(Rel_Name,Entity_Name,Related_E,[1,1],_),
   qualimed_variable(current_dataset,DB),
   member([qm_relationship(Rel_Name,_,Key,Related_Key)],DB).


/* qm_delete_file(E_Name,Keys) <-
      */

qm_delete_file(E_Name,Keys) :-
   name_append(E_Name,Keys,Filename),
   qualimed_variable(current_directory,Dir),
   name_append(Dir,Filename,Path),
   send(file(Path),remove).


/* qm_remove_entity(+E_Name,+Key) <-
      */

qm_remove_entity(E_Name,Key) :-
   qualimed_variable(mode,entity_mode),
   qm_file_entity(E_Name),
   qualimed_variable(current_dataset,DB),
   Entity =.. [qm_entity,E_Name,Key,_],
   subtract(DB,[[qm_entity(E_Name,Key)],[Entity]],New_DB),
   qualimed_variable_set(current_dataset,New_DB),
   qm_save_database(New_DB),
   qualimed_variable(current_keylist,K_List),
   subtract(K_List,[[qm_keys(E_Name,Key,_)]],New_K_List),
   qualimed_variable_set(current_keylist,New_K_List),
   qm_save_keylist.
qm_remove_entity(E_Name,Key) :-
   qualimed_variable(mode,entity_mode),
   qualimed_variable(current_dataset,DB),
   Entity =.. [qm_entity,E_Name,Key,_],
   subtract(DB,[[qm_entity(E_Name,Key)],[Entity]],New_DB),
   qualimed_variable_set(current_dataset,New_DB),
   qm_save_database(New_DB).
qm_remove_entity(E_Name,Key) :-
   qualimed_variable(mode,attribute_mode),
   qualimed_variable(current_dataset,DB),
   X = [qm_attribute(E_Name,_,Key,_)],
   findall( X,
      member(X,DB),
      A_List ),
   subtract(DB,[[qm_entity(E_Name,Key)]|A_List],New_DB),
   qualimed_variable_set(current_dataset,New_DB),
   qm_save_database(New_DB).
qm_remove_entity(E_Name,Key) :-
   qualimed_variable(mode,attribute_list_mode),
   qualimed_variable(current_dataset,DB),
   X = [qm_attribute(E_Name,_,_)],
   findall( X,
      member(X,DB),
      A_List ),
   subtract(DB,[[qm_entity(E_Name,Key)]|A_List],New_DB),
   qm_remove_tuples(A_List,Key,New_List),
   append(New_DB,New_List,DB2),
   qualimed_variable_set(current_dataset,DB2),
   qm_save_database(DB2).

qm_remove_tuples([[qm_attribute(E,A,L)]|T1],K,
      [[qm_attribute(E,A,New_L)]|T2]) :-
   subtract(L,[[K,_]],New_L),
   qm_remove_tuples(T1,K,T2).
qm_remove_tuples([],_,[]).

 
/* qm_delete_all_relationships(Entity_Name,Keys) <-
      */

qm_delete_all_relationships(Entity_Name,Keys) :-  
   findall( X,
      qm_entity_is_related(Entity_Name,Keys,X),
      R_List ),
   qm_delete_relationship_list(R_List).

qm_delete_relationship_list([A|B]) :-
   A = [R_Name,Keys],
   qm_delete_relationship(R_Name,Keys),
   qm_delete_relationship_list(B).
qm_delete_relationship_list([]).

qm_entity_is_related(A,B,C) :-
   qm_entity_is_related_1(A,B,C).
qm_entity_is_related(A,B,C) :-
   qm_entity_is_related_2(A,B,C).

qm_entity_is_related_1(Entity_Name,Key,P) :-
   qm_relationship_type(Rel_Name,Entity_Name,_,_,_),
   qualimed_variable(current_dataset,DB),
   member([qm_relationship(Rel_Name,K,Key,_)],DB),
   P = [Rel_Name,K].

qm_entity_is_related_2(Entity_Name,Key,P) :-
   qm_relationship_type(Rel_Name,_,Entity_Name,_,_),
   qualimed_variable(current_dataset,DB),
   member([qm_relationship(Rel_Name,K,_,Key)],DB),
   P = [Rel_Name,K].


/* qm_delete_relationship(R_Name,Key) <-
      */

qm_delete_relationship(R_Name,Key) :-
   qualimed_variable(current_dataset,DB),
   X =.. [R_Name,Key],
   subtract(DB,[[qm_relationship(R_Name,Key,_,_)],[X]],New_DB),
   qualimed_variable_set(current_dataset,New_DB),
   qm_save_database(New_DB).


/* qm_open_entity(+Chain) <-
      */

qm_open_entity(Chain) :-
   chain_to_list(Chain,List),
   X =.. [qm_load_entity|List],
   call(X).


/* qm_load_entity(+Entity_Name) <-
      */

qm_load_entity(E_Name) :-
   qm_file_entity(E_Name),
   !,
   qualimed_variable(current_directory,Dir),
   name_append('./examples/',Dir,Dir2),
   get(directory(Dir2),files,F),
   chain_to_list(F,L),
   findall( X,
      ( member(Filename,L),
        name(Filename,Filename_L),
        name(E_Name,E_Name_L),
        append(E_Name_L,X_L,Filename_L),
        name(X,X_L) ),
      Key_List ),   
   list_to_chain(Key_List,C),
   new(Dialog,dialog(laden)),
   new(Browser,browser),
   send(Browser,members,C),
   qm_change_keys(E_Name,Browser),
   send(Dialog,append,Browser),
   send(Browser,open_message, and(
      message(@prolog,qm_load_entity,E_Name,Browser?selection?key),
      message(Dialog,destroy))),
   send(Dialog,append,new(_,button(abbrechen,
      message(Dialog,destroy)))),
   send(Dialog,append,new(_,button(laden, and(
      message(@prolog,qm_load_entity,E_Name,Browser?selection?key),
      message(Dialog,destroy))))),
   send(Dialog,open).


/* qm_change_keys(E_Name,Browser) <-
      */

qm_change_keys(E_Name,Browser) :-
   get(Browser,members,Members),
   qm_change_keys_2(E_Name,Members).

qm_change_keys_2(E,M) :-
   send(M,for_some,message(@prolog,qm_change_label,@arg1,E)).

qm_change_label(Item,E) :-
   qualimed_variable(current_keylist,L),
   get(Item,key,Name),
   member([qm_keys(E,Name,Label)],L),
   send(Item,label,Label).

/*
qm_load_entity(Entity_Name) :-
   qm_key_list(Entity_Name,Key_List),
   key_list_to_browser(Key_List,Browser),   
   new(Dialog,dialog(laden)),
   send(Dialog,append,Browser),
   send(Browser,open_message, and(
      message(@prolog,qm_load_entity,Entity_Name,Browser?selection?key),
      message(Dialog,destroy))),
   send(Dialog,append,new(_,button(abbrechen,
      message(Dialog,destroy)))),
   send(Dialog,append,new(_,button(laden, and(
      message(@prolog,qm_load_entity,Entity_Name,Browser?selection?key),
      message(Dialog,destroy))))),
   send(Dialog,open).
*/
   

/* qm_load_entity(+Entity_Name,+Key) <-
      */

/*
qm_load_entity(E_Name,Key) :-
   qm_key_selection(E_Name,Key,P),
   qm_program_to_selections(E_Name,P,C),
   retract(qm_current_entity(_)),
   assert(qm_current_entity(E_Name)),
   retract(qm_current_key(_)),
   assert(qm_current_key(Key)),
   qm_show_report(E_Name,C),
   qm_load_relationships.
*/   

qm_load_entity(E_Name,Key) :-
   qm_key_selection(E_Name,Key,P),
   qm_program_to_selections(E_Name,P,C),
   qualimed_variable_set(current_entity,E_Name),
   qualimed_variable_set(current_key,Key),
   qm_show_report(E_Name,C),
   qm_load_relationships.


/* qm_key_selection(E_Name,Key,P) <-
      */

qm_key_selection(E_Name,Key,P) :-
   qm_file_entity(E_Name),
   qualimed_variable(mode,attribute_mode),
   qm_open_file(E_Name,Key),
   qualimed_variable(current_dataset,DB),
   findall( [X],
      ( member([X],DB),
        X =.. [qm_attribute,E_Name,_,Key,_] ),
      P ).
qm_key_selection(E_Name,Key,P) :-
   qm_file_entity(E_Name),
   qualimed_variable(mode,entity_mode),
   qm_open_file(E_Name,Key),
   qualimed_variable(current_dataset,DB),
   findall( [X],
      ( member([X],DB),
        X =.. [qm_entity,E_Name,Key,_] ),
      P ).
qm_key_selection(E_Name,Key,P) :-
   qm_file_entity(E_Name),
   qualimed_variable(mode,attribute_list_mode),
   qm_open_file(E_Name,Key),
   qualimed_variable(current_dataset,DB),
   findall( [qm_attribute(E_Name,Attr,Key,Value)],
      ( member([X],DB),
        X =.. [qm_attribute,E_Name,Attr,L],
        member([Key,Value],L)),
      P ).
qm_key_selection(E_Name,Key,P) :-
   qualimed_variable(mode,attribute_mode),
   qualimed_variable(current_dataset,DB),
   findall( [X],
      ( member([X],DB),
        X =.. [qm_attribute,E_Name,_,Key,_] ),
      P ).
qm_key_selection(E_Name,Key,P) :-
   qualimed_variable(mode,entity_mode),
   qualimed_variable(current_dataset,DB),
   findall( [X],
      ( member([X],DB),
        X =.. [qm_entity,E_Name,Key,_] ),
      P ).
qm_key_selection(E_Name,Key,P) :-
   qualimed_variable(mode,attribute_list_mode),
   qualimed_variable(current_dataset,DB),
   findall( [qm_attribute(E_Name,Attr,Key,Value)],
      ( member([X],DB),
        X =.. [qm_attribute,E_Name,Attr,L],
        member([Key,Value],L) ),
      P ).


/* qm_open_file(E_Name,Key) <-
      */

qm_open_file(E_Name,Key) :-
   qm_entity_to_filename(E_Name,Key,File),
   qualimed_variable(current_file,File).
qm_open_file(E_Name,Key) :-
   qm_entity_to_filename(E_Name,Key,File),
   qdb_load(File,P),
   qualimed_variable_set(current_file,File),
   qualimed_variable_set(current_dataset,P).


/* qm_set_qm_mode <-
      */

/* 
qm_set_qm_mode(X) :-
   retractall(qm_mode(_)),
   assert(qm_mode(X)),
   qm_current_file(F),
   qdb_load(F,P),
   qm_set_current_database(P).
*/

qm_set_qm_mode :-
   make_dialog(D,set_mode),
   send(D,open),
   get(D,confirm_centered,X),
   send(D,destroy),
   qm_set_qm_mode(X).

qm_set_qm_mode(@nil).
qm_set_qm_mode(X) :-
   qualimed_variable_set(mode,X),
%  name_append('./examples/qualimed/',X,D),
%  name_append(D,'/',Dir),
%  qualimed_variable_set(current_directory,Dir),
   qm_close_entity.


/* qm_load_relationships <-
      zum laden aller Relationships der aktuellen Entity
      in den Browser */

qm_load_relationships :-
   send(@qdb_browser1,clear),
   send(@qdb_browser2,clear),
   qualimed_variable(current_entity,Entity_Name),
   qualimed_variable(current_key,Key),
   qm_load_relationships_1(Entity_Name,Key),
   qm_change_keys(@qdb_browser1),
   qm_load_relationships_2(Entity_Name,Key),
   qm_change_keys(@qdb_browser2).

qm_load_relationships_1(Entity_Name,Keys) :- 
   qm_get_relationship_1_list(Entity_Name,Keys,Rel_List),
   qm_relationship_list_to_browser(Rel_List,@qdb_browser1).

qm_load_relationships_2(Entity_Name,Keys) :-
   qm_get_relationship_2_list(Entity_Name,Keys,Rel_List),
   qm_relationship_list_to_browser(Rel_List,@qdb_browser2).

qm_get_relationship_1_list(Entity_Name,Keys,Rel_List) :-
   findall( X,
      qm_rel_1_selection(_,Entity_Name,Keys,X),
      Rel_List ).

qm_get_relationship_2_list(Entity_Name,Keys,Rel_List) :-
   findall( X,
      qm_rel_2_selection(_,Entity_Name,Keys,X),
      Rel_List ).

qm_rel_1_selection(Rel_Name,Entity_Name,Keys1,P) :-
   qm_relationship_type(Rel_Name,Entity_Name,Related_E,_,_),
   qualimed_variable(current_dataset,DB),
   member([X],DB),
   X =.. [qm_relationship,Rel_Name,_,Keys1,Keys2],
   P = [Related_E,Keys2].

qm_rel_2_selection(Rel_Name,Entity_Name,Keys1,P) :-
   qm_relationship_type(Rel_Name,Related_E,Entity_Name,_,_),
   qualimed_variable(current_dataset,DB),
   member([X],DB),
   X =.. [qm_relationship,Rel_Name,_,Keys2,Keys1],
   P = [Related_E,Keys2].


/* qm_change_keys(Browser) <-
      */

qm_change_keys(Browser) :-
   get(Browser,members,M),
   send(M,for_some,message(@prolog,qm_change_label,@arg1)).

qm_change_label(Dict_Item) :-
   get(Dict_Item?key,head,E),
   get(Dict_Item?key,tail,K),
   qualimed_variable(current_dataset,DB),
   qm_get_key_attributes(E,K,DB,A_List),
   list_name_append([E|A_List],Label),
   send(Dict_Item,label,Label).
   
   
/* qm_change_keys_for_exdep(Browser,E) <-
      */

qm_change_keys_for_exdep(Browser,E) :-
   get(Browser,members,M),
   send(M,for_some,
      message(@prolog,qm_change_label_for_exdep,@arg1,E)).
   
qm_change_label_for_exdep(Dict_Item,E) :-
   get(Dict_Item,key,K),
   qualimed_variable(current_dataset,DB),
   qm_get_key_attributes(E,K,DB,A_List),
   list_name_append([E|A_List],Label),
   send(Dict_Item,label,Label).


/* qm_relationship_list_to_browser(Relationship,Browser) <-
      */

qm_relationship_list_to_browser([A|B],Browser) :-
%  A = [E,K],
%  qm_get_print_keys(E,K,P_K),
%  List = [E,K|P_K],
   key_to_dict_item(A,D),
   send(Browser,append,D),
   qm_relationship_list_to_browser(B,Browser).
qm_relationship_list_to_browser([],_).

qm_get_print_keys(E_Name,Key,P_Keys) :-
   qm_keys(E_Name,Item_Names),
   qm_key_selection(E_Name,Key,P),
   findall( Y,
      ( member([qm_attribute(_,_,I,Y)],P),
        member(I,Item_Names) ),
      P_Keys ).


/* qm_key_list(E_Name,Key_List) <-
      */

qm_key_list(E_Name,Key_List) :-
   qm_current_database(DB),
   findall( X,
      member([qm_entity(E_Name,X)],DB),
      Key_List ).


/* qm_show_report(E,C) <-
      */

qm_show_report(E,C) :-
   send(@view?editor?text_buffer,clear),
   chain_to_text_buffer(E,C,@view?editor?text_buffer).


/* qm_create_relationship(Rel_Name) <-
      */

qm_create_relationship(Rel_Name) :-
   qm_current_entity_key(X),
   qm_create_relationship(Rel_Name,X).

qm_create_relationship(Rel_Name,Key1) :-
   qm_relationship(Rel_Name,_,Ent2_Name,_,_), 
   create_key(Ent2_Name,Key2),
   start_input(Rel_Name,[Rel_Name],[Key1,Key2]),
   qm_create_entity(Ent2_Name,Key2).

qm_create_entity_key(E_Name,Key) :-
   qm_file_entity(E_Name),
   qualimed_variable(current_directory,Dir),
   qualimed_variable(key_head,Y),
   name_append('examples/',Dir,Dir2),
   get(directory(Dir2),files,Files),
   chain_to_list(Files,List),
   findall( X,
      ( member(Filename,List),
        name_append(E_Name,X,Filename)),
      [] ),
   !,
   name(Y,L1),
   name(1,L2),
   append(L1,L2,L),
   name(Key,L).
qm_create_entity_key(E_Name,Key) :-
   qm_file_entity(E_Name),
   qualimed_variable(current_directory,Dir),
   qualimed_variable(key_head,Y),
   name_append('examples/',Dir,Dir2),
   get(directory(Dir2),files,Files),
   chain_to_list(Files,L),
   findall( Number,
      ( member(Filename,L),
        name_append(E_Name,X,Filename),
        name_append(Y,Number,X)),
      Key_List ),
   !,
   list_maximum(Key_List,Key1),
%  sort(Key_List,L1),
%  reverse(L1,[Key1|_]),
   qm_create_successor_key(Key1,Key).
qm_create_entity_key(E_Name,Key) :-
   qualimed_variable(current_dataset,P),
   findall( X,
      member([qm_entity(E_Name,X)],P),
      [] ),
   qualimed_variable(key_head,X),
   name(X,L1),
   name(1,L2),
   append(L1,L2,L),
   name(Key,L).
qm_create_entity_key(E_Name,Key) :-
   qualimed_variable(current_dataset,P),
   qualimed_variable(key_head,Y),
   findall( Number,
      ( member([qm_entity(E_Name,X)],P),
        name_append(Y,Number,X) ),
      Key_List ),
   list_maximum(Key_List,Key1),
%  sort(Key_List,L1),
%  reverse(L1,[Key1|_]),
   qm_create_successor_key(Key1,Key).


/* qm_create_relationship_key(R_Name,Key) <-
      */

qm_create_relationship_key(R_Name,Key) :-
   qualimed_variable(current_dataset,P),
   findall( X,
      member([qm_relationship(R_Name,X,_,_)],P),
      [] ),
   qualimed_variable(key_head,X),
   name(X,L1),
   name(1,L2),
   append(L1,L2,L),
   name(Key,L).

qm_create_relationship_key(R_Name,Key) :-
   qualimed_variable(current_dataset,P),
   qualimed_variable(key_head,Y),
   findall( Number,
      ( member([qm_relationship(R_Name,X,_,_)],P),
        name_append(Y,Number,X) ),
      Key_List ),
   list_maximum(Key_List,Key1),
%  sort(Key_List,L1),
%  reverse(L1,[Key1|_]),
   qm_create_successor_key(Key1,Key).

qm_create_successor_key(X,Key) :-
   qualimed_variable(key_head,K),
   Y is X + 1,
   name_append(K,Y,Key).


qm_remove_key_head(Key,Rest) :-
   name(Key,[_,_,_,_,_,_|R]),
   name(Rest,R).

qm_insert_key_head(N,Key) :-
   qualimed_variable(key_head,X),
   name(X,L1),
   name(N,L2),
   append(L1,L2,L),
   name(Key,L).

% qm_key_head('klh01_').


/*
qm_key_number(E_Name,D_Name,I_Name,Key_Number) :-
   qm_file(E_Name,File),
   dconsult(File,P),
   remove_keys(P,P1),
   findall( X,
      get_dialog_entry(D_Name,I_Name,P1,X),
      Number_List ),
   get(_),
   list_maximum(Number_List,Max),
   Key_Number is Max + 1.
*/


/* qm_check_rules(P) <-
      */

/*
qm_check_rules(P) :-
   qualimed_variable(current_dataset,DB),
   append(P,DB,Facts),
   dconsult('qualimed/next_entity_em',Prog),
   append(Facts,Prog,P2),
   list_to_ord_set(P2,Program),
   dislog_flag_set(join_mode,special),
   tpi_fixpoint_iteration(Program,Coin),
   dislog_flag_set(join_mode,regular),
   coin_to_state(Coin,State),
   qualimed_variable_set(dialog_state,State).
*/

qm_check_rules(P) :-
   qualimed_variable(current_dataset,DB),
   append(P,DB,Facts),
   qualimed_variable(knowledge_base,K),
   append(Facts,K,P2),
   list_to_ord_set(P2,Program),
   dislog_flag_set(join_mode,special),
   tpi_fixpoint_iteration(Program,Coin),
   dislog_flag_set(join_mode,regular),
   coin_to_state(Coin,State),
   qualimed_variable_set(dialog_state,State).


/* qm_evaluate_dialog_state(Dialog,Chain,Answer) <-
      */

qm_evaluate_dialog_state(D,Chain,Answer) :-
%  Answer = @on,
   get_selections(D,Chain),
   qualimed_variable(active_entity,E),
   qualimed_variable(active_key,K),
   qm_selections_to_program(E,K,Chain,Prog),
   qm_check_rules(Prog),
   qualimed_variable(dialog_state,State),
   member([warning(A,B)],State),
   !,
   qm_warning(A,B),
   get(D,confirm_centered,Answ),
   qm_evaluate_answer(Answ,D,Chain,Answer).
qm_evaluate_dialog_state(D,C,@on) :-
   get_selections(D,C).


/* qm_dialog_input(E_Name,Chain,Answer) <-
      */

qm_dialog_input(E_Name,Chain,Answer) :-
   make_dialog(D1,E_Name),
   fill_dialog(D1,Chain),
   make_dialog(D2,buttons),
   send(D2,below,D1),
   send(D1,open),
   get(D2,confirm_centered,Answ),
   qm_evaluate_answer(Answ,D1,Chain,Answer),
   send(D1,destroy).


/* qm_evaluate_answer(Answ,D,Chain,Answer) <-
      */

qm_evaluate_answer(@nil,_,_,@nil).
%  send(D,destroy).
qm_evaluate_answer(@off,D,Chain,@off) :-
   get_selections(D,Chain).
%  send(D,destroy).
qm_evaluate_answer(@on,D,Chain,Answer) :-
%  Answer = @on,
%  get_selections(D,Chain),
%  qm_selections_to_program(Chain,Prog),
%  qm_check_rules(Prog),
   qm_evaluate_dialog_state(D,Chain,Answer).
%  send(D,destroy).
qm_evaluate_answer(Answ,D,Chain,Answer) :-
   get_subchain(Chain,Answ,Subchain),
   qm_dialog_input(Answ,Subchain,Subanswer),
   qm_evaluate_subanswer(Subanswer,D,Chain,Answer).


/* qm_evaluate_subanswer(Subanswer,D,Chain,Answer) <-
     */

qm_evaluate_subanswer(@nil,D,_,@nil) :-
   send(D,destroy).
qm_evaluate_subanswer(@off,D,Chain,@off) :-
   get_selections(D,Chain),
   send(D,destroy).
qm_evaluate_subanswer(@on,D,Chain,Answer) :-
   get(D,confirm,A),
   qm_evaluate_answer(A,D,Chain,Answer).


/******************************************************************/


