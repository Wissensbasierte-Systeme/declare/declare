

/******************************************************************/
/***                                                            ***/
/***            QualiMed:  Handling of Chains                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* get_subchain(Chain, Name, Subchain) <-
      */

get_subchain(Chain, Name, Subchain) :-
   get(Chain, head, Head),
   is_subchain(Chain, Head, Name, Subchain).


/* is_subchain(Chain, Head, Name, Subchain) <-
      */

is_subchain(_, Head, Name, Subchain) :-
   get(Head, first, Name),
   get(Head, second, Subchain).
is_subchain(Chain, Head, Name, Subchain) :-
   get(Chain, next, Head, Next),
   is_subchain(Chain, Next, Name, Subchain).


/* make_dialog_chain(Dialog, Chain) <-
      */

/*
make_dialog_chain(D_Name, Chain) :-
   new(Chain, chain),
   qm_dialog(D_Name, Part_List),
   append_parts(Chain, Part_List).
*/

make_dialog_chain(D_Name, Chain) :-
   new(Chain, chain),
   dialog(D_Name, [_, parts:=[_|Part_List]|_]),
   append_parts(Chain, Part_List).

append_parts(_, []).
append_parts(Chain, [_:=label(_,_)|T]) :-
   !,
   append_parts(Chain, T).
append_parts(Chain, [_:=button(Name)|T]) :-
   !,
   make_dialog_chain(Name, C2),
   new(Tuple, tuple(Name, C2)),
   send(Chain, append, Tuple),
   append_parts(Chain, T).
append_parts(Chain, [_:=H|T]) :-
   H =.. [_, Name|_],
   new(Tuple, tuple(Name, @default)),
   send(Chain, append, Tuple),
   append_parts(Chain,T).

/*
append_parts(_, []).
append_parts(Chain, [H|T]) :-
   H =.. [label|_],
   append_parts(Chain, T).
append_parts(Chain, [H|T]) :-
   H =.. [text_item, Name|_],
   new(Tuple, tuple(Name, @default)),
   send(Chain, append, Tuple),
   append_parts(Chain, T).
append_parts(Chain, [H|T]) :-
   H =.. [slider, Name|_],
   new(Tuple, tuple(Name, @default)),
   send(Chain, append, Tuple),
   append_parts(Chain, T).
append_parts(Chain, [H|T]) :-
   H =.. [cycle, Name|_],
   new(Tuple,tuple(Name,@default)),
   send(Chain,append,Tuple),
   append_parts(Chain,T).
append_parts(Chain,[H|T]) :-
   H =.. [toggle,Name|_],
   new(Tuple,tuple(Name,@default)),
   send(Chain,append,Tuple),
   append_parts(Chain,T).
append_parts(Chain,[H|T]) :-
   H =.. [choice,Name|_],
   new(Tuple,tuple(Name,@default)),
   send(Chain,append,Tuple),
   append_parts(Chain,T).
append_parts(Chain,[H|T]) :-
   H =.. [button,_,Name|_],
   make_dialog_chain(Name,C2),
   new(Tuple,tuple(Name,C2)),
   send(Chain,append,Tuple),
   append_parts(Chain,T).
*/


/* fill_dialog(Dialog, Chain) <-
      */

fill_dialog(_, Chain) :-
   send(Chain, empty).
fill_dialog(D, Chain) :-
   get(D, members, M),
   get(M, head, I),
   get(Chain, head, H),
   fill_dialog_items(Chain, H, M, I).

fill_dialog_items(Chain, T, M, I) :-
   get(Chain, tail, T),
   send(I, instance_of, label),
   get(M, next, I, J),
   fill_dialog_items(Chain, T, M, J).
   
fill_dialog_items(Chain,T,_,I) :-
   get(Chain,tail,T),
   send(I,instance_of,button).
   
fill_dialog_items(Chain,T,_,_) :-
   get(Chain,tail,T),
   get(T,second,@default).

fill_dialog_items(Chain,T,_,I) :-
   get(Chain,tail,T),
   get(T,second,Value),
   send(I,selection,Value).

fill_dialog_items(Chain,T,M,I) :-
   send(I,instance_of,label),
   get(M,next,I,J),
   fill_dialog_items(Chain,T,M,J).
   
fill_dialog_items(Chain,T,M,I) :-
   send(I,instance_of,button),
   get(M,next,I,J),
   get(Chain,next,T,S),
   fill_dialog_items(Chain,S,M,J).
   
fill_dialog_items(Chain,T,M,I) :-
   get(T,second,@default),
   get(Chain,next,T,S),
   get(M,next,I,J),
   fill_dialog_items(Chain,S,M,J).

fill_dialog_items(Chain,T,M,I) :-
   get(T,second,Value),
   send(I,selection,Value),
   get(Chain,next,T,S),
   get(M,next,I,J),
   fill_dialog_items(Chain,S,M,J).


/* get_selections(+Dialog,-List) <-
      liefert die +Dialog _eintr�ge in -List */

get_selections(D,Chain) :-
   get(D,members,M),
   get(M,head,I),
   get(Chain,head,H),
   get_member_selections(M,I,Chain,H),
   !.

get_member_selections(_,I,Chain,H) :-
   send(I,instance_of,button),
   get(Chain,tail,H).

get_member_selections(M,I,Chain,H) :-
   send(I,instance_of,button),
   get(Chain,next,H,Next_Tuple),
   get(M,next,I,Next_Item),
   get_member_selections(M,Next_Item,Chain,Next_Tuple).

get_member_selections(_,I,Chain,H) :-
   send(I,instance_of,label),
   get(Chain,tail,H).

get_member_selections(M,I,Chain,H) :-
   send(I,instance_of,label),
   get(M,next,I,Next_Item),
   get_member_selections(M,Next_Item,Chain,H).


/* Dialog-Items f�r mehrwertiges Attribut
   werden als Liste uebergeben */

get_member_selections(_,I,Chain,H) :-
   get(Chain,tail,H),
   get(I,selection,S),
   send(H,second,S).

get_member_selections(M,I,Chain,H) :-
   get(I,selection,S),
   send(H,second,S),
   get(M,next,I,Next_Item),
   get(Chain,next,H,Next_Tuple),
   get_member_selections(M,Next_Item,Chain,Next_Tuple).


/* set_tuple_value <-
      */

set_tuple_value(_,Tuple,A_Name,Value) :-
   get(Tuple,first,A_Name),
   is_list(Value),
   list_to_chain(Value,Chain),
   send(Tuple,second,Chain).
set_tuple_value(_,Tuple,A_Name,Value) :-
   get(Tuple,first,A_Name),
   send(Tuple,second,Value).
set_tuple_value(_,Tuple,A_Name,Value) :-
   get(Tuple,second,Chain),
   send(Chain,instance_of,chain),
   get(Chain,head,H),
   send(H,instance_of,tuple),
   set_tuple_value(Chain,H,A_Name,Value).
set_tuple_value(C,T,A,V) :-
   get(C,next,T,N),
   set_tuple_value(C,N,A,V).


/* values_to_chain <-
      */

values_to_chain([[_,L]|Rest],C,Tuple) :-
   get(C,next,Tuple,Next),
   L = [[_,_]|_],
   get(Tuple,second,Subchain),
   get(Subchain,head,H),
   values_to_chain(L,Subchain,H),
   values_to_chain(Rest,C,Next).

values_to_chain([[_,L]],_,Tuple) :-
   L = [[_,_]|_],
   get(Tuple,second,Subchain),
   get(Subchain,head,H),
   values_to_chain(L,Subchain,H).
   
values_to_chain([[_,L]|Rest],C,Tuple) :-
   get(C,next,Tuple,Next),
   is_list(L),
   list_to_chain(L,S_Chain),
   send(Tuple,second,S_Chain),
   values_to_chain(Rest,C,Next).

values_to_chain([[_,L]],_,Tuple) :-
   is_list(L),
   list_to_chain(L,S_Chain),
   send(Tuple,second,S_Chain).

values_to_chain([[_,V]|Rest],C,Tuple) :-
   get(C,next,Tuple,Next),
   send(Tuple,second,V),
   values_to_chain(Rest,C,Next).

values_to_chain([[_,V]],_,Tuple) :-
   send(Tuple,second,V).


/* tuples_to_chain(Tuple,Chain) <-
      */

tuples_to_chain([],_).
tuples_to_chain([[N,V]|Rest],C) :-
   send(C,for_some,message(@prolog,set_value,@arg1,N,V)),
   tuples_to_chain(Rest,C).

set_value(Tuple,Name,Value) :-
   get(Tuple,first,Name),
   send(Tuple,second,Value).


/* list_to_chain(L,C) <-
      */

list_to_chain(L,C) :-
   new(C,chain),
   list_to_chain2(L,C).

list_to_chain2([],_).
list_to_chain2([A|B],C) :-
   is_list(A),
   !,
   list_to_chain(A,C1),
   send(C,append,C1),
   list_to_chain2(B,C).
list_to_chain2([A|B],C) :-
   A =.. [_,_|_],
   !,
   pred_to_chain(A,A_Chain),
   send(C,append,A_Chain),
   list_to_chain2(B,C).
list_to_chain2([A|B],C) :-
   send(C,append,A),
   list_to_chain2(B,C).


/* chain_to_list(C,L) <-
      */

chain_to_list(C,[]) :-
   send(C,empty),
   !.

/*
chain_to_list(C,[H1|L]) :-
   get(C,head,H),
   send(H,instance_of,chain),
   get(H,head,pce_pred),
   !,
   chain_to_pred(H,H1),
   send(C,delete_head),
   chain_to_list(C,L). 
*/

chain_to_list(C,L) :-
   get(C,head,H),
   chain_to_list(C,H,L).

chain_to_list(C,H,[H1]) :-
   get(C,tail,H),
   send(H,instance_of,chain),
   get(H,head,pce_pred),
   !,
   chain_to_pred(H,H1). 

chain_to_list(C,H,[H1|L]) :-
   send(H,instance_of,chain),
   get(H,head,pce_pred),
   !,
   chain_to_pred(H,H1),
   get(C,next,H,Next),
   chain_to_list(C,Next,L). 

chain_to_list(C,H,[[F,L]]) :-
   get(C,tail,H),
   send(H,instance_of,tuple),
   get(H,first,F),
   get(H,second,S),
   send(S,instance_of,chain),
   chain_to_list(S,L),
   !. 

chain_to_list(C,H,[[F,S1]|L]) :-
   send(H,instance_of,tuple),
   get(H,first,F),
   get(H,second,S),
   send(S,instance_of,chain),
   chain_to_list(S,S1),
   !,
   get(C,next,H,Next),
   chain_to_list(C,Next,L). 

chain_to_list(C,H,[[F,S]]) :-
   get(C,tail,H),
   send(H,instance_of,tuple),
   get(H,first,F),
   get(H,second,S),
   !. 

chain_to_list(C,H,[[F,S]|L]) :-
   send(H,instance_of,tuple),
   get(H,first,F),
   get(H,second,S),
   !,
   get(C,next,H,Next),
   chain_to_list(C,Next,L). 

chain_to_list(C,H,[H1]) :-
   get(C,tail,H),
   send(H,instance_of,chain),
   !,
   chain_to_list(H,H1).

chain_to_list(C,H,[H1|L]) :-
   send(H,instance_of,chain),
   !,
   chain_to_list(H,H1),
   get(C,next,H,Next),
   chain_to_list(C,Next,L).

chain_to_list(C,H,[H]) :-
   get(C,tail,H).
   
chain_to_list(C,H,[H|L]) :-
   get(C,next,H,Next),
   chain_to_list(C,Next,L).

/* 
chain_to_list(C,[H1|L]) :-
   send(H,instance_of,chain),
   !,
   chain_to_list(H,H1),
   send(C,delete_head),
   chain_to_list(C,L). 

chain_to_list(C,[H|L]) :- 
   get(C,head,H),
   send(C,delete_head),
   chain_to_list(C,L).
*/


/* pred_to_chain(Pred, Chain) <-
      */

pred_to_chain(Pred, Chain) :-
   Pred =.. X,
   list_to_chain([pce_pred|X], Chain).


/* chain_to_pred(Chain, Pred) <-
      */

chain_to_pred(Chain, Pred) :-
   chain_to_list(Chain, [pce_pred|List]),
   Pred =.. List.


/******************************************************************/


