

/******************************************************************/
/***                                                            ***/
/***             QualiMed:  Elementary Predicates               ***/
/***                                                            ***/
/******************************************************************/


/* dsave_atomic(Program,Filename) <-
      save Program with atoms with capital letters
      to Filename. */

dsave_atomic(Program,Filename) :-
   dstore_atomic(Program,Filename).

dstore_atomic(Program,Filename) :-
   dis_ex_extend_filename(Filename,Pathname),
   distore_atomic(Program,Pathname).

distore_atomic(Program,CompleteFilename) :-
   switch(File,CompleteFilename),
   writeln(':- disjunctive.'),
   dportray_atomic(lp,Program), nl,
   writeln(':- prolog.'),
   switch(File).

dportray_atomic(lp,Program) :-
   maplist( copy_term,
      Program, Program1 ),
   pp_program_atomic(Program1).

pp_program_atomic([Head|Rules]) :-
   !,
   nl, pp_d_fact_atomic(Head),
   pp_program_atomic(Rules).
pp_program_atomic([]) :-
   nl.

pp_d_fact_atomic(0) :-
   fail.
pp_d_fact_atomic(Clause) :-
   write_atomic_disjunction(Clause),
   write('.').

write_atomic_disjunction([H1,H2|T]) :-
   !,
   write_atomic_literal(H1),
   write(';'),
   write_atomic_disjunction([H2|T]).
write_atomic_disjunction([H]) :-
   !,
   write_atomic_literal(H).
write_atomic_disjunction([]).

write_atomic_literal(Lit) :-
   Lit =.. [P|Args],
   write(P),
   write('('),
   write_atomic_args(Args),
   write(')').


/* write_atomic_args(Args) <-
      */

write_atomic_args([H1,H2|T]) :-  
   is_list(H1),
   !,
   write('['),
   write_atomic_args(H1),
   write(']'),
   write(','),
   write_atomic_args([H2|T]).
write_atomic_args([H1,H2|T]) :-
   integer(H1),
   !,
   write(H1),
   write(','),
   write_atomic_args([H2|T]).
write_atomic_args([H1,H2|T]) :-
   !,
   put(39),
   write(H1),
   put(39),
   write(','),
   write_atomic_args([H2|T]).
write_atomic_args([H]) :-
   is_list(H),
   !,
   write('['),
   write_atomic_args(H),
   write(']').
write_atomic_args([H]) :-
   integer(H),
   !,
   write(H).
write_atomic_args([H]) :-
   !,
   put(39),
   write(H),
   put(39).
write_atomic_args([]).


/* list_maximum(Xs,Max) <-
      */

list_maximum([A|B],X) :-
   list_maximum(B,Y),
   ( ( A > Y,
       X = A,
       ! )
   ; X = Y ).
list_maximum([],0).


/* key_list_to_browser(Key_List,Browser) <-
      */

key_list_to_browser(Key_List,Browser) :-
   new(Dict,dict),
   key_list_to_dict(Key_List,Dict),
   new(Browser,list_browser(Dict,30,15)).

key_list_to_dict([Keys],Dict) :-
   key_to_dict_item(Keys,Dict_Item),
   send(Dict,append,Dict_Item).
key_list_to_dict([Keys|Rest],Dict) :-
   key_to_dict_item(Keys,Dict_Item),
   send(Dict,append,Dict_Item),
   key_list_to_dict(Rest,Dict).

key_to_dict_item(Keys,Dict_Item) :-
   Keys = [E,K|_],
   list_to_chain([E,K],C_Keys),
   list_to_name(Keys,Name),
   new(Dict_Item,dict_item(C_Keys,Name)).
key_to_dict_item(Keys,Dict_Item) :-
   atomic(Keys),
   new(Dict_Item,dict_item(Keys,Keys)).


/* get_key <-
      */

get_key([[D,Key_Name]|Rest],P,[K|Keys]) :-
   get_dialog_entry(D,Key_Name,P,K),
   get_key(Rest,P,Keys).
get_key([],_,[]).

get_dialog_entry(D,K_Name,P,K) :-
   make_dialog(Dialog,D),
   get(Dialog,members,M),
   get_item_index(M,K_Name,I),
   member([X],P),
   X =.. [D|_],
   arg(I,X,K).

get_item_index(M,K,I) :-
   get(M,head,H),
   ( send(H,instance_of,label)
   ; send(H,instance_of,button) ),
   !,
   send(M,delete_head),
   get_item_index(M,K,I).
get_item_index(M,K,1) :-
   get(M,head,H),
   get(H,name,K),
   !.
get_item_index(M,K,I) :-
   send(M,delete_head),
   get_item_index(M,K,J),
   I is J + 1.


/* relations_to_browser(Relations,Browser) <-
      */

relations_to_browser(Relations,Browser) :-
   new(Dict,dict),
   append_relations(Relations,Dict),
   new(Browser,list_browser(Dict)).

append_relations([A|B],Dict) :-
   relation_to_dict_item(A,I),
   send(Dict,append,I),
   append_relations(B,Dict).
append_relations([],_).

relation_to_dict_item(Relation,Dict_Item) :-
   Relation =.. [Rel_Name|[Own_Key|[Rel_Key|Rest]]],
   list_to_chain([Rel_Name|[Own_Key|[Rel_Key|Rest]]],Chain),
   create_label_for_dict_item(Rel_Name,Rel_Key,Label),
   new(Dict_Item,dict_item(Chain,Label)).

create_label_for_dict_item(Rel_Name,Rel_Key,Name) :-
   name(Rel_Name,X),
   name(Rel_Key,Y),
   append(X,[32|Y],Z),
   name(Name,Z).


/******************************************************************/


