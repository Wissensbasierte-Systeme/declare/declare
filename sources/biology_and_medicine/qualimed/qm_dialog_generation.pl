

/******************************************************************/
/***                                                            ***/
/***             QualiMed:  Generation of Dialogs               ***/
/***                                                            ***/
/******************************************************************/


/* qm_create_dialogs <-
      */

qm_create_dialogs :-
   dconsult('qualimed/qm_dialog_config',P),
   checklist( qm_assert_rule,
      P ).

qm_assert_rule([R]) :-
   R =.. [qm_dialog,D_Name,L],
   qm_create_dialog_lists(
      Dialog,L,P_List,M_List,L_List,B_List),
   retractall(dialog(D_Name,_)),
   assert(dialog(D_Name,[
      object := Dialog,
      parts := [Dialog := dialog(D_Name)|P_List],
      modifications := M_List,
      layout := L_List,
      behaviour := B_List ])).


/* qm_assert_dialogs <-
      */

qm_assert_dialogs :-
   dconsult('qualimed/qm_dialog_config',P),
   checklist( qm_assert_dialog_fact,
      P ).

qm_assert_dialog_fact([H]) :-
   H =.. [qm_dialog,D_Name,_],
   retractall(qm_dialog(D_Name,_)),
   assert(H).


/* qm_create_dialog_lists(Dialog,List) <-
      for first argument. */

qm_create_dialog_lists(Dialog, [H|T],
      [X := H|P_List],
      [X := [ length := 30 ] | M_List],
      L_List, B_List ) :-
   H =.. [text_item,_],
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, [H|T],
      [X := text_item(Label)|P_List],
      [X := [ length := Length ] | M_List],
      L_List, B_List ) :-
   H =.. [text_item,Label,Length],
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, [H|T],
      [X := menu(Label,choice)|P_List],
      [X := [append := I_List]|M_List],
      L_List, B_List ) :-
   H =.. [choice,Label,Menu_Items],
   qm_create_item_list(Menu_Items,I_List),
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, [H|T],
      [X := menu(Label,toggle)|P_List],
      [X := [append := I_List]|M_List],
      L_List, B_List ) :-
   H =.. [toggle,Label,Menu_Items],
   qm_create_item_list(Menu_Items,I_List),
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, [H|T],
      [X := menu(Label,cycle)|P_List],
      [X := [append := I_List]|M_List],
      L_List, B_List ) :-
   H =.. [cycle,Label,Menu_Items],
   qm_create_item_list(Menu_Items,I_List),
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, [H|T],
      [X := slider(Label,Low,High,Sel)|P_List],
      M_List, L_List, B_List ) :-
   H =.. [slider,Label,[Low,High,Sel]],
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, [H|T],
      [X := label(Label,String)|P_List],
      M_List, L_List, B_List ) :-
   H =.. [label,Label,String],
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, [H|T],
      [X := button(Label)|P_List],
      M_List, L_List,
      [X := [ message :=
                 message(Dialog, return, Answer)]|B_List]) :-
   H =.. [button,Label,Answer],
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).


/* qm_create_dialog_lists <-
      for subsequent arguments. */

qm_create_dialog_lists(_,_,_,[],[],[],[],[]).

qm_create_dialog_lists(Dialog, Below, _, [H|T],
      [X := text_item(Label)|P_List],
      [X := [ length := 30 ] | M_List],
      [L|L_List], B_List ) :-
   H =.. [text_item,Label,below],
   L =.. [below,X,Below],
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, Right, [H|T],
      [X := text_item(Label)|P_List],
      [X := [ length := 30 ] | M_List],
      [L|L_List], B_List ) :-
   H =.. [text_item,Label,right],
   L =.. [right,X,Right],
   qm_create_dialog_lists(
      Dialog,Below,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, _, [H|T],
      [X := text_item(Label)|P_List],
      [X := [ length := Length ] | M_List],
      [L|L_List], B_List ) :-
   H =.. [text_item,Label,Length,below],
   L =.. [below,X,Below],
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, Right, [H|T],
      [X := text_item(Label)|P_List],
      [X := [ length := Length ] | M_List],
      [L|L_List], B_List ) :-
   H =.. [text_item,Label,Length,right],
   L =.. [right,X,Right],
   qm_create_dialog_lists(
      Dialog,Below,X,T,P_List,M_List,L_List,B_List).
   
qm_create_dialog_lists(Dialog, Below, _, [H|T],
      [X := menu(Label,choice)|P_List],
      [X := [append := I_List]|M_List],
      [L|L_List], B_List ) :-
   H =.. [choice,Label,Menu_Items,below],
   L =.. [below,X,Below],
   qm_create_item_list(Menu_Items,I_List),
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, Right, [H|T],
      [X := menu(Label,choice)|P_List],
      [X := [append := I_List]|M_List],
      [L|L_List], B_List ) :-
   H =.. [choice,Label,Menu_Items,right],
   L =.. [right,X,Right],
   qm_create_item_list(Menu_Items,I_List),
   qm_create_dialog_lists(
      Dialog,Below,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, _, [H|T],
      [X := menu(Label,toggle)|P_List],
      [X := [append := I_List]|M_List],
      [L|L_List], B_List ) :-
   H =.. [toggle,Label,Menu_Items,below],
   L =.. [below,X,Below],
   qm_create_item_list(Menu_Items,I_List),
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, Right, [H|T],
      [X := menu(Label,toggle)|P_List],
      [X := [append := I_List]|M_List],
      [L|L_List], B_List ) :-
   H =.. [toggle,Label,Menu_Items,right],
   L =.. [right,X,Right],
   qm_create_item_list(Menu_Items,I_List),
   qm_create_dialog_lists(
      Dialog,Below,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, _, [H|T],
      [X := menu(Label,cycle)|P_List],
      [X := [append := I_List]|M_List],
      [L|L_List], B_List ) :-
   H =.. [cycle,Label,Menu_Items,below],
   L =.. [below,X,Below],
   qm_create_item_list(Menu_Items,I_List),
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, Right, [H|T],
      [X := menu(Label,cycle)|P_List],
      [X := [append := I_List]|M_List],
      [L|L_List], B_List ) :-
   H =.. [cycle,Label,Menu_Items,right],
   L =.. [right,X,Right],
   qm_create_item_list(Menu_Items,I_List),
   qm_create_dialog_lists(
      Dialog,Below,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, _, [H|T],
      [X := slider(Label,Low,High,Sel)|P_List],
      M_List, [L|L_List], B_List ) :-
   H =.. [slider,Label,[Low,High,Sel],below],
   L =.. [below,X,Below],
   qm_create_dialog_lists(Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, Right, [H|T],
      [X := slider(Label,Low,High,Sel)|P_List],
      M_List, [L|L_List], B_List ) :-
   H =.. [slider,Label,[Low,High,Sel],right],
   L =.. [right,X,Right],
   qm_create_dialog_lists(
      Dialog,Below,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, _, [H|T],
      [X := label(Label,String)|P_List],
      M_List, [L|L_List], B_List ) :-
   H =.. [label,Label,String,below],
   L =.. [below,X,Below],
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, Right, [H|T],
      [X := label(Label,String)|P_List],
      M_List, [L|L_List], B_List ) :-
   H =.. [label,Label,String,right],
   L =.. [right,X,Right],
   qm_create_dialog_lists(
      Dialog,Below,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, _, [H|T],
      [X := button(Label)|P_List],
      M_List, [L|L_List],
      [X := [ message :=
                 message(Dialog, return, Answer)]|B_List] ) :-
   H =.. [button,Label,Answer,below],
   L =.. [below,X,Below],
   qm_create_dialog_lists(
      Dialog,X,X,T,P_List,M_List,L_List,B_List).

qm_create_dialog_lists(Dialog, Below, Right, [H|T],
      [X := button(Label)|P_List],
      M_List, [L|L_List],
      [X := [ message :=
                 message(Dialog, return, Answer)]|B_List] ) :-
   H =.. [button,Label,Answer,right],
   L =.. [right,X,Right],
   qm_create_dialog_lists(
      Dialog,Below,X,T,P_List,M_List,L_List,B_List).

qm_create_item_list([H|T],
      [menu_item(H,@default,H,@off,@nil)|I_List]) :-
   qm_create_item_list(T,I_List).
qm_create_item_list([],[]).


/******************************************************************/


