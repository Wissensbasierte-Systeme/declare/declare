

/******************************************************************/
/***                                                            ***/
/***          Qualimed:  Database Interface                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


dconsult(qualimed,Filename,List) :-
   qualimed_variable(example_path,Ex_Path),
   concat_atom([Ex_Path,Filename],Absolute_Path),
   dconsult(Absolute_Path,List).
   

qm_entity_types(E) :-
   dconsult(qualimed,'e_r_model.dl',L),
   findall( [qm_entity_type(X)],
      member([qm_entity_type(X)],L),
      E ).


qm_relationship_types(R) :-
   dconsult(qualimed,'e_r_model.dl',L),
   findall( [qm_relationship_type(A,B,C,D,E)],
      member([qm_relationship_type(A,B,C,D,E)],L),
      R ).


qm_name_data_type(Entity,I,M,D) :- 
   dconsult(qualimed,'qm_dialogs.dl',DL),
   qm_dialog_description_to_attribute_description_list(
      Entity,DL,I,M,D).


qm_rel_owner(Entity_Type,Member_List) :-
   qm_relationship_types(R),
   findall( [X,Y],
      member([qm_relationship_type(X,Entity_Type,Y,_,_)],R),
      Member_List ).

qm_rel_member(Entity_Type,Owner_List) :-
   qm_relationship_types(R),
   findall( [X,Y],
      member([qm_relationship_type(X,Y,Entity_Type,_,_)],R),
      Owner_List ).


qm_create_table_statement(Entity,Create_Table_Statement) :-
   dconsult(qualimed,'qm_dialogs.dl',DL),
   qm_dialog_description_to_attribute_description_list(
      Entity,DL,_,_,D),
   db_description_to_create_table_statement(
      Entity,D,Create_Table_Statement).


/*** implementation ***********************************************/


/* qm_dialog_description_to_attribute_description_list <-
      */

qm_dialog_description_to_attribute_description_list(
      Entity_Type,Dialog_List,Items,Modifications,Desc_List) :-
   member([dialog(Entity_Type,[object:=_,parts:=[_|Items],
      modifications:=Modifications,layout:=_])],Dialog_List),
   dialog_items_to_attr_descriptions(
      Items,Modifications,Desc_List).


/* dialog_items_to_attr_descriptions(
         Items,Modifications,Desc_List) <-
      */

dialog_items_to_attr_descriptions(
      Items,Modifications,Desc_List) :-
   dialog_items_to_attr_descriptions(
      Items,Modifications,[],Desc_List).

dialog_items_to_attr_descriptions(
      [X:=Atom|Items],Modifications1,List1,List3) :-
   Atom =.. [slider,Attr_Name|_],
   append(List1,[[[Attr_Name,integer]]],List2),
   delete(Modifications1,X:=_Item_Mod_List,Modifications2),
   dialog_items_to_attr_descriptions(
      Items,Modifications2,List2,List3).
dialog_items_to_attr_descriptions(
      [X:=Atom|Items],Modifications1,List1,List3) :-
   Atom =.. [text_item,Attr_Name|_],
   member(X:=Item_Mod_List,Modifications1),
   delete(Modifications1,X:=Item_Mod_List,Modifications2),
   member(length:=Length,Item_Mod_List),
   append(List1,[[[Attr_Name,varchar,Length]]],List2),
   dialog_items_to_attr_descriptions(
      Items,Modifications2,List2,List3).
dialog_items_to_attr_descriptions(
      [X:=Atom|Items],Modifications1,List1,List3) :-
   Atom =.. [menu,_,toggle], 
   member(X:=Item_Mod_List,Modifications1),
   delete(Modifications1,X:=Item_Mod_List,Modifications2),
   member(append:=Toggle_Value_List,Item_Mod_List),
   get_toggle_attribute_list(Toggle_Value_List,Attribute_List),
   append(List1,[Attribute_List],List2),
   dialog_items_to_attr_descriptions(
      Items,Modifications2,List2,List3).
dialog_items_to_attr_descriptions(
      [X:=Atom|Items],Modifications1,List1,List3) :-
   Atom =.. [menu,Attr_Name,_],
%  member(X:=Item_Mod_List,Modifications1),
   delete(Modifications1,X:=_Item_Mod_List,Modifications2),
   append(List1,[[[Attr_Name,varchar,50]]],List2),
   dialog_items_to_attr_descriptions(
      Items,Modifications2,List2,List3).
dialog_items_to_attr_descriptions(
      [_|Items],Modifications,List1,List2) :-
   dialog_items_to_attr_descriptions(
      Items,Modifications,List1,List2).
dialog_items_to_attr_descriptions([],_,List,List).



/* get_toggle_attribute_list(Toggle_Value_List,Attribute_List) <-
      */

get_toggle_attribute_list(Toggle_Value_List,Attribute_List) :-
   get_toggle_attribute_list(Toggle_Value_List,[],Attribute_List).

get_toggle_attribute_list([Value_Description|Values],List1,List3) :-
%  write_ln(Value_Description),
   Value_Description =.. [menu_item,Value|_],
   append(List1,[[Value,boolean]],List2),
   get_toggle_attribute_list(Values,List2,List3).
get_toggle_attribute_list([],List,List).


/* db_description_to_create_table_statement(
      Entity,D,Create_Table_Statement) <-
         */

db_description_to_create_table_statement(
      Entity,Descr,Create_Table_Statement) :-
   attr_description_to_data_definition(Descr,Def),
   concat_atom(['CREATE TABLE',Entity,'(',Def,')'],' ',
      Create_Table_Statement).

attr_description_to_data_definition(Descr,Def) :-
   attr_description_to_data_definition(Descr,[],Def_List1),
   append(['key Varchar(10)'],Def_List1,Def_List2),
   write_ln(Def_List2),
   concat_atom(Def_List2,', ',Def).

attr_description_to_data_definition(
      [Description|Descriptions],Def1,Def3) :-
   item_def_list(Description,Def_List),
   append(Def1,Def_List,Def2),
   attr_description_to_data_definition(Descriptions,Def2,Def3).
attr_description_to_data_definition([],Def,Def).


item_def_list(Item_List,Def_List) :-
   maplist(item_definition_to_sql_definition,Item_List,Def_List).

item_definition_to_sql_definition([Value,boolean],String) :-
   name(H,[39]),
   concat_atom([H,Value,H,' Boolean'],String).
item_definition_to_sql_definition([Value,integer],String) :-
   name(H,[39]),
   concat_atom([H,Value,H,' Integer'],String).
item_definition_to_sql_definition([Value,varchar,Length],String) :-
   name(H,[39]),
   concat_atom([H,Value,H,' Varchar(',Length,')'],String).


dialog_descriptions(Description_List) :-
   ent(Entity_Types),
%  length(Entity_Types,L),
%  write_ln(L),
   dialog_descriptions_rec(Entity_Types,Description_List).

dialog_descriptions_rec(Entity_Types,Description_List) :-
   dialog_descriptions_rec(Entity_Types,[],Description_List).

dialog_descriptions_rec([],List,List).
dialog_descriptions_rec([Entity_Type|Entity_Types],List1,List3) :-
   dialog_descr(Entity_Type,Items,Modifications),
   write_ln(Entity_Type), write_ln(Items),
   write_ln(Modifications), write_ln(' '),
%  length(Entity_Types,L),write(' '),write_ln(L),
%  write_ln(' '),
   append(List1,[[Entity_Type,Items,Modifications]],List2),
   dialog_descriptions_rec(Entity_Types,List2,List3).


/*****************************************************************/


