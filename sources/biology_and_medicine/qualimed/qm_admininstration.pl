

/******************************************************************/
/***                                                            ***/
/***             QualiMed:  Administration                      ***/
/***                                                            ***/
/******************************************************************/


/*

writes data into 'examples/medicine/qualimed/orthopaedie'

stammdaten
 - allgemeinbogen
    - allgemein_anamnese
    - allgemein_befund
    - allgemein_bewegungsbefund
       - schulter
       - ellbogen
       - untere_extremitaet
       - huefte
    - allgemein_neurologie
    - allgemein_diagnose
 - allgemeinbogen
    - allgemein_anamnese
    - allgemein_befund
    - allgemein_bewegungsbefund
       - schulter
       - ellbogen
       - untere_extremitaet
       - huefte
    - allgemein_neurologie
    - allgemein_diagnose
spezialbogen_knie
 - kindliche_knie_anam
 - kindliche_knie_klin_diag
 - kindliche_knie_diagn_ther

*/

subtrees_of(State,Node,Subtrees) :-
   findall( [Y,Subtree],
      ( next_entity(State,Node,Y),
        subtrees_of(State,Y,Subtree) ),
      Subtrees ).

next_entity(State,X,Y) :-
   tps_fixpoint_iteration(
      [[qualimed_variable(active_entity,X)]|State],
      State_2 ),
   !,
   dportray(dhs,State_2),
   writeln_list(State_2),
   member([next_entity(Y)],State_2).


/******************************************************************/


