

/******************************************************************/
/***                                                            ***/
/***          Qualimed:  Variables                              ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      qualimed_variable/2.

:- multifile
      qualimed_variable/2.


qualimed_variable(e_r_model_file, File) :-
   qualimed_variable(e_r_model, Model_Name),
   name_append(['examples/medicine/qualimed/',
      Model_Name, '/e_r_model'], File).

qualimed_variable(dialog_model_file, File) :-
   qualimed_variable(e_r_model, Model_Name),
   name_append(['examples/medicine/qualimed/',
      Model_Name, '/qm_dialogs.pl'], File).

qualimed_variable(knowledge_base_file, File) :-
   qualimed_variable(e_r_model, Model_Name),
   name_append(['medicine/qualimed/',
      Model_Name, '/knowledge_base'], File).

qualimed_variable(
   example_path, 'medicine/qualimed/db_interface_test/' ).
qualimed_variable(
   keylist, 'medicine/qualimed/e_r_models/keylist' ).
qualimed_variable(
   e_r_models_directory, 'examples/medicine/qualimed/e_r_models' ).
qualimed_variable(
   e_r_model, orthopaedie ).
qualimed_variable(
   current_entity, @nil ).
qualimed_variable(
   current_key, @nil ).
qualimed_variable(
   current_dataset, @nil ).
qualimed_variable(
   current_directory, 'medicine/qualimed/orthopaedie/' ).
qualimed_variable(
   file_table, @nil ).
qualimed_variable(
   mode, @nil ).
qualimed_variable(
   key_head, 'klh01_' ).


qualimed_variable_set(Variable_Name,Value) :-
   retractall(qualimed_variable(Variable_Name,_)),
   assert(qualimed_variable(Variable_Name,Value)).

qualimed_variables :-
   qualimed_variable(X,Y),
   write_list([X,' = ',Y]), nl,
   fail.
qualimed_variables.


/******************************************************************/


