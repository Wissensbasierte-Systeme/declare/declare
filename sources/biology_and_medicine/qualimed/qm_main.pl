

/******************************************************************/
/***                                                            ***/
/***             QualiMed:  Main Window                         ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      dialog/2.


/*** interface ****************************************************/


qm :-
   qualimed.

qualimed :-
   new(@view,view),
   new(@qdb_browser1,browser),
   new(@qdb_browser2,browser),
   new(@neu_pop,popup(neu)),
   new(@load_pop,popup(laden)),
   qm_load_models,
   qm_set_qm_mode(entity_mode),
   qm_load_keylist,
   new(@frame, frame('QualiMed - Datenbank-Browser')),
   send(@frame, append, new(MBD, dialog)),
   send(@qdb_browser1,open_message,
      message(@prolog,qm_open_entity,@arg1?key)),
   send(@qdb_browser2,open_message,
      message(@prolog,qm_open_entity,@arg1?key)),
   send(MBD, append, new(MB1, menu_bar)),
   send_pulldown_menu(MB1,'Datei',[
      'Datenbank laden' - true,
      'Drucken' - qm_print_view,
      'Beenden' - quit_qualimed ]),
   send(MB1,append,new(Bearbeiten, popup(bearbeiten))),
   send(Bearbeiten,append,@neu_pop),
   send(Bearbeiten,append,@load_pop),
   send(Bearbeiten,append,new(_,menu_item(editieren,
      message(@prolog,qm_edit_entity)))),
   send(Bearbeiten,append,new(_,menu_item(loeschen,
      message(@prolog,qm_delete_entity)))),
   send(Bearbeiten,append,new(_,menu_item(schliessen,
      message(@prolog,qm_close_entity)))),
   send_pulldown_menu(MB1,'Optionen',[
      'ER-Modell bearbeiten' - qm_edit_er_model,
      'Dialogmodell bearbeiten' - qm_edit_dialog_model,
      'Wissensbasis bearbeiten' - qm_edit_knowledge_base,
      'Modelle laden' - qm_load_models ]),
   send(MB1, append, new(_, popup(dislog))),
   send(MB1, append, new(_, popup(hilfe))),
   send(@view,below,MBD),
   new(Weiter,label(w,'weiter zu:')),
   send(new(W_D,dialog),append,Weiter),
   send(W_D,below,@view),
   send(@qdb_browser1,below,W_D),
   new(Zurueck,label(z,'zur�ck zu:')),
   send(new(Z_D,dialog),append,Zurueck),
   send(Z_D,below,@qdb_browser1),
   send(@qdb_browser2,below,Z_D),
   send(@frame, open).


quit_qualimed :-
   free(@neu_pop),
   free(@load_pop),
   send(@frame,destroy).


/* qm_load_er_model_2 <-
      */

qm_load_er_model_2 :-
   qualimed_variable(e_r_model_file,File),
   consult(File).


/* qm_load_dialog_model <-
      */

qm_load_dialog_model :-
   qualimed_variable(dialog_model_file,File),
   consult(File).


/* qm_load_knowledge_base <-
      */

qm_load_knowledge_base :-
   qualimed_variable(knowledge_base_file, File),
   dconsult(File, P),
   qualimed_variable_set(knowledge_base, P).


/* qm_load_models <-
      */

qm_load_models :-
   qm_load_er_model_2,
   qm_load_dialog_model,
   qm_load_knowledge_base,
   qm_update_menu.


/* qm_update_menu <-
      */

qm_update_menu :-
   send(@neu_pop, clear),
   qm_append_file_entities_to_popup(@neu_pop,
      qm_create_file_entity ),
   qm_append_start_entities_to_popup(@neu_pop,
      qm_create_start_entity ),
   send(@load_pop, clear),
   qm_append_file_entities_to_popup(@load_pop,
      qm_load_entity ).   


/* qm_print_view <-
      */

qm_print_view :-
   new(F,file(druck)),
   send(@view,save,F),
   shell(qm_lpr).


/* qm_append_start_entities_to_popup(Popup,Message) <-
      */

qm_append_start_entities_to_popup(Popup,Message) :-
%  findall( X,
%     qm_entity_type(X),
%     E_List ),
   findall( X,
      qm_start_entity(X),
      E_List ),
   qm_entity_list_to_popup(E_List,Popup,Message).


/* qm_append_file_entities_to_popup(Popup,Message) <-
      */

qm_append_file_entities_to_popup(Popup,Message) :-
%  findall( X,
%     qm_entity_type(X),
%     E_List ),
   findall( X,
      qm_file_entity(X),
      E_List ),
   qm_entity_list_to_popup(E_List,Popup,Message).

qm_entity_list_to_popup([H|T],Popup,Message) :-
   send(Popup,append,new(_,menu_item(H,
      message(@prolog,Message,H)))),
   qm_entity_list_to_popup(T,Popup,Message).
qm_entity_list_to_popup([],_,_).


/* qm_edit_er_model <-
      */

qm_edit_er_model :-
   qualimed_variable(e_r_model_file,File),
   emacs(File).


/* qm_edit_dialog_model <-
      */

qm_edit_dialog_model :-
   manpce,
%  send_man_manual(dialog_editor, @manual),
%  send(new(dia_editor), open),
   qualimed_variable(dialog_model_file,File),
   emacs(File).


/* qm_edit_knowledge_base <-
      */

qm_edit_knowledge_base :-
   qualimed_variable(knowledge_base_file,File),
   emacs(File).
   

/* qm_load_database <-
      */

qm_load_database :-
   qualimed_variable(e_r_models_directory,Directory),
   get(directory(Directory),files,F),
   chain_to_list(F,L),
   findall( X,
      ( member(Filename,L),
        name(Filename,Filename_L),
        name(e_r_model,E_Name_L),
        append(E_Name_L,X_L,Filename_L),
        name(X,X_L) ),
      Key_List ),   
   list_to_chain(Key_List,C),
   new(Dialog,dialog(laden)),
   new(Browser,browser),
   send(Browser,members,C),
   qualimed_variable(current_keylist,KL),
   qualimed_variable(keylist,Keylist_File),
   dconsult(Keylist_File,KL2),
   qualimed_variable_set(current_keylist,KL2),
   qm_change_keys(e_r_model,Browser),
   qualimed_variable_set(current_keylist,KL),
%  qm_change_keys(e_r_model,Browser),
   send(Dialog,append,Browser),
   send(Browser,open_message, and(
      message(@prolog,qm_load_database,Browser?selection?label),
      message(Dialog,destroy))),
   send(Dialog,append,new(_,button(abbrechen,
      message(Dialog,destroy)))),
   send(Dialog,append,new(_,button(laden, and(
      message(@prolog,qm_load_database,Browser?selection?label),
      message(Dialog,destroy))))),
   send(Dialog,open).

qm_load_database(DB_Name) :-
   qm_close_entity,
   qualimed_variable_set(e_r_model, DB_Name),
   name_append(['qualimed/',DB_Name,'/'],Dir),
   qualimed_variable_set(current_directory, Dir),
   qualimed_variable_set(current_entity, @nil),
   qualimed_variable_set(current_key, @nil),
   qualimed_variable_set(current_dataset, @nil),
   qm_load_models,
   qm_load_keylist.


/* load_er_model <-
      */

load_er_model :-
   qualimed_variable(e_r_models_directory,Directory),
   get(directory(Directory),files,F),
   chain_to_list(F,L),
   findall( X,
      ( member(Filename,L),
        name(Filename,Filename_L),
   name(e_r_model,E_Name_L),
   append(E_Name_L,X_L,Filename_L),
   name(X,X_L) ),
      Key_List),   
   list_to_chain(Key_List,C),
   new(Dialog,dialog(laden)),
   new(Browser,browser),
   send(Browser,members,C),
%  qualimed_variable(current_keylist,KL),
%  dconsult('/examples/qualimed/e_r_models/keylist',KL2),
%  qualimed_variable_set(current_keylist,KL2),
   qm_change_keys(e_r_model,Browser),
%  qualimed_variable_set(current_keylist,KL),
   send(Dialog,append,Browser),
   send(Browser,open_message, and(
      message(@prolog,load_er_model,
         Browser?selection?key,Browser?selection?label),
      message(Dialog,destroy))),
   send(Dialog,append,new(_,button(abbrechen,
      message(Dialog,destroy)))),
   send(Dialog,append,new(_,button(laden, and(
      message(@prolog,load_er_model,
         Browser?selection?key,Browser?selection?label),
      message(Dialog,destroy))))),
   send(Dialog,open).

load_er_model(Key,Label) :-
   qm_close_entity,
   qualimed_variable_set(
      current_directory,'qualimed/e_r_models/'),
   qualimed_variable_set(e_r_model,e_r_models),
   qm_entity_to_filename(e_r_model,Key,F),
   qdb_load(F,P),
   qualimed_variable_set(e_r_model,Label),
   name_append(['qualimed/',Label,'/'],Dir),
   qualimed_variable_set(
      current_directory,Dir),
   qm_assert_er_model(P).
   

/* qm_assert_er_model(P) <-
      */

qm_assert_er_model(P) :-
   qm_assert_entities(P),
   qm_assert_relationships(P),
   qm_assert_start_entities(P),
   qm_assert_file_entities(P),
   qm_assert_keys(P).

qm_assert_entities(P) :-
   retractall(qm_entity_type(_)),
   findall( [Name],
      ( member([qm_entity(entity_type,_,A)],P),
        member([name,Name],A) ),
      E_List ),
   assert_arguments_generic(qm_entity_type,E_List).

qm_assert_relationships(P) :-
   retractall(qm_relationship_type(_,_,_,_,_)),
   findall( [R_Name,Q,Z,[Min_Q,Max_Q],[Min_Z,Max_Z]],
      ( member([qm_entity(relationship_type,Key,A)],P),
        member([name,R_Name],A),
        member([min_quell_entity,Min_Q],A),
        member([max_quell_entity,Max_Q],A),
        member([min_ziel_entity,Min_Z],A),
        member([max_ziel_entity,Max_Z],A),
        member([qm_relationship(quell_entity,_,Key,Q_Key)],P),
        member([qm_entity(entity_type,Q_Key,A_1)],P),
        member([name,Q],A_1),
        member([qm_relationship(ziel_entity,_,Key,Z_Key)],P),
        member([qm_entity(entity_type,Z_Key,A_2)],P),
        member([name,Z],A_2)),
      R_List ),
   assert_arguments_generic(qm_relationship_type,R_List).

qm_assert_start_entities(P) :-
   retractall(qm_start_entity(_)),
   findall( [Start_E],
      ( member([qm_entity(entity_type,_,A_1)],P),
        member([optionen,O],A_1),
        member(start_entity,O),
        member([name,Start_E],A_1)),
      R_List),
   assert_arguments_generic(qm_start_entity,R_List).

qm_assert_file_entities(P) :-
   retractall(qm_file_entity(_)),
   findall( [File_E],
      ( member([qm_entity(entity_type,_,A_1)],P),
        member([optionen,O],A_1),
        member(file_entity,O),
        member([name,File_E],A_1)),
      R_List ),
   assert_arguments_generic(qm_file_entity,R_List).

qm_assert_keys(P) :-
   retractall(qm_keys(_,_)),
   findall( [E,Key_List],
      ( member([qm_entity(entity_type,E_K,A_1)],P),
        member([name,E],A_1),
        findall( K,
           ( member(
                [qm_relationship(schluesselattribut,_,E_K,A_K)],P),
             member([qm_entity(attribute,A_K,[[name,K]|_])],P)),
           Key_List ),
        Key_List \= [] ),
      R_List),
   assert_arguments_generic(qm_keys,R_List).


/* qm_get_current_relationships(R) <-
      */

qm_get_current_relationships(R) :-
   qdb_current_entity(E),
   qdb_current_keys(K),
   findall( X,
      qm_relationship(X,E,_,_,_,_),
      Rel_Names ),
   qm_collect_relationships(K,Rel_Names,R).

qm_collect_relationships(Keys,[A|B],Relationships) :-
   qm_file(A,File),
   dconsult(File,P),
   findall( X,
      ( member([X],P),
        X =.. [A|[Keys|_]] ),
      R1 ),
   qm_collect_relationships(Keys,B,R2),
   append(R1,R2,Relationships).
qm_collect_relationships(_,[],[]).


/* qm_relationships_of_entity(Entity,Relationships) <-
      */

qm_relationships_of_entity(Entity,Relationships) :-
   Entity =.. [Entity_Name|[Key|_]],
   findall( X,
      qm_relationship(X,Entity_Name,_,_,_,_),
      R ),
   qm_collect_relationships(Key,R,Relationships).


/******************************************************************/


