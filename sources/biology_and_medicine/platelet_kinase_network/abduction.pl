


/******************************************************************/
/***                                                            ***/
/***          Platelet Kinase Network:  Abduction               ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(
      platelet_kinase_network_activation_mode, at_least_one).
%     platelet_kinase_network_activation_mode, all).


/*** data *********************************************************/


platelet_kinase_network_data(Tuples) :-
   Tuples = [
      [a,b,1], [c,b,2], [b,d,3], [g,d,3], [e,d,4], [f,d,4] ].

/*
platelet_kinase_network_data(Tuples) :-
   dread(xls, 'PlateletKinaseNetwork.txt', Tuples).
*/

kinase_is_active(Active_Kinases) :-
%  Active_Kinases = [a, b].
   Active_Kinases = [].

kinase_should_be_active(Active_Kinases) :-
%  Active_Kinases = [d, f].
%  Active_Kinases = ['ZYX'].
   Active_Kinases = ['MINK1'].


/*** interface ****************************************************/


/* platelet_kinase_network_inference <-
      */

platelet_kinase_network_inference :-
   platelet_kinase_network_logic_program(Program),
   bar_line, dportray(lp, Program), bar_line, wait,
   tp_iteration(Program, I),
   bar_line, dportray(hi, I), bar_line,
   findall( K,
      ( member(active(K), I),
        \+ member([active(K)], Program) ),
      Ks ),
   writeln_list(Ks).


/* platelet_kinase_network_abduction(Active_Kinases) <-
      */

platelet_kinase_network_abduction :-
   kinase_should_be_active(Active_Kinases),
   platelet_kinase_network_abduction(Active_Kinases).

platelet_kinase_network_abduction(Active_Kinases) :-
   platelet_kinase_network_inverse_program(Inverse_Program),
   bar_line, writeln('abductive program:'),
   dportray(lp, Inverse_Program), bar_line, wait,
   findall( [active(K)],
      member(K, Active_Kinases),
      Facts ),
   append(Facts, Inverse_Program, Program),
%  tpi_fixpoint_iteration(Program, Models),
   program_to_stable_models_dlv(Program, Models_2),
   platelet_kinase_network_models_prune(Models_2, Models),
   bar_line, dportray(chs, Models), bar_line,
   length(Models, N), write_list(user, [N, ' models\n']),
   assert(platelet_kinase_network_models(Models)),
   state_to_tree(Models, Tree),
   predicate_to_file( platelet_kinase_network_abduction_tree,
      dportray(tree, Tree) ),
   ( N =< 20 ->
     set_tree_to_picture_bfs(Tree)
   ; true ).


/* platelet_kinase_network_models_to_tree <-
      */

platelet_kinase_network_models_to_tree :-
   platelet_kinase_network_models(Models),
   state_to_tree(Models, Tree),
   dportray(tree, Tree).

platelet_kinase_network_models_prune(Models_1, Models_2) :-
   maplist( platelet_kinase_network_model_prune,
      Models_1, Models_2 ).

platelet_kinase_network_model_prune(Model_1, Model_2) :-
   findall( K,
      member(active(K), Model_1),
      Model_a ),
   findall( K:P,
      member(active_position(K, P), Model_1),
      Model_b ),
   append(Model_a, Model_b, Model_2).


/*** implementation ***********************************************/


/* platelet_kinase_network_inverse_program(Inverse_Program) <-
      */

platelet_kinase_network_inverse_program(Inverse_Program) :-
   platelet_kinase_network_logic_program([Rule|Rules_1]),
   Rule = [active_position(Y, P)]-[active(X), activates(X, Y, P)],
   findall( [active_position(Y, P)]-[active(X)],
      member([activates(X, Y, P)], Rules_1),
      Rules_2 ),
   append(Rules_2, Rules_1, Rules_3),
   dportray(lp, Rules_3),
   findall( Rule_2,
      platelet_kinase_network_inverse_rule(Rules_3, Rule_2),
      Inverse_Program ).

platelet_kinase_network_inverse_rule(Program, Rule) :-
   setof( Body,
      member(Head-Body, Program),
      Coin ),
   boolean_dualization(Coin, State),
   member(Clause, State),
   Rule = Clause-Head.


/* platelet_kinase_network_logic_program(Program) <-
      */

platelet_kinase_network_logic_program(Program) :-
   platelet_kinase_network_position_rules(Rules),
   platelet_kinase_network_activation_facts(Facts),
   platelet_kinase_network_active_facts(Activated),
   Rule = [active_position(K2, P)]-
      [active(K1), activates(K1, K2, P)],
   append([[Rule|Rules], Facts, Activated], Program).

platelet_kinase_network_activation_facts(Facts) :-
   platelet_kinase_network_data(Tuples),
   findall( [activates(X, Y, P)],
      member([X, Y, P], Tuples),
      Facts ).

platelet_kinase_network_position_rules(Rules) :-
   platelet_kinase_network_data(Tuples),
   platelet_kinase_network_data_to_position_rules(Tuples, Rules).

platelet_kinase_network_data_to_position_rules(Tuples, Rules) :-
   dislog_variable_get(
      platelet_kinase_network_activation_mode, all),
   findall( [active(Y)]-Body,
      ( platelet_kinase_network_to_kinase_positions(Tuples, Y-Ps),
        kinase_positions_to_body(Y-Ps, Body) ),
      Rules ).
platelet_kinase_network_data_to_position_rules(Tuples, Rules) :-
   dislog_variable_get(
      platelet_kinase_network_activation_mode, at_least_one),
   findall( [active(Y)]-[active_position(Y, P)],
      member([_, Y, P], Tuples),
      Rules ).

platelet_kinase_network_active_facts(Facts) :-
   kinase_is_active(Ks),
   findall( [active(K)],
      member(K, Ks),
      Facts ).

platelet_kinase_network_to_kinase_positions(Tuples, Y-Ps) :-
   setof( P,
      X^member([X, Y, P], Tuples),
      Ps ).

kinase_positions_to_body(Y-Ps, Body) :-
   findall( active_position(Y, P),
      member(P, Ps),
      Body ).

/*
active_position(X1, X2) :- active(X3),activates(X3, X1, X2).
active(b) :- active_position(b, 1),active_position(b, 2).
active(d) :- active_position(d, 3),active_position(d, 4).
activates(a, b, 1).
activates(c, b, 2).
activates(b, d, 3).
activates(g, d, 3).
activates(e, d, 4).
activates(f, d, 4).
*/


/******************************************************************/


