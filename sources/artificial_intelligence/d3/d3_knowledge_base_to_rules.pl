

/******************************************************************/
/***                                                            ***/
/***       D3:  Knowledge Base to Rules                         ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(d3_view_rules_in_editor, yes).


/*** interface ****************************************************/


/* d3_knowledge_base_to_rules(KB, Rules) <-
      */

d3_knowledge_base_to_rules(KB, Rules) :-
   findall( Rule,
      ( QContainer := KB/'QContainers'/'QContainer',
        d3_q_container_to_rule(QContainer, Rule) ),
      Rules_1 ),
   findall( Rule,
      ( Slice := KB/'KnowledgeSlices'/'KnowledgeSlice',
        d3_knowledge_slice_to_rule(Slice, Rule) ),
      Rules_2 ),
   findall( Rule,
      ( Id := KB/'InitQASets'/'QContainer'@'ID',
        Rule = [d3_finding(indicated_question, Id)]-[] ),
      Rules_3 ),
   append([Rules_1, Rules_2, Rules_3], Rules).


/* d3_knowledge_base_and_rule_ids_to_rules(KB, Rids, Rules) <-
      */

d3_knowledge_base_and_rule_ids_to_rules(KB, Rids, Rules) :-
   findall( Rule,
      ( Slice := KB/'KnowledgeSlices'
           /'KnowledgeSlice'::[@'ID'=Id, member(Id, Rids)],
        d3_knowledge_slice_to_rule(Slice, Rule) ),
      Rules ).


/* d3_q_container_to_rule(QContainer, Rule) <-
      */

d3_q_container_to_rule(QContainer, Rule) :-
   X := QContainer@'ID',
   Y := QContainer/'Children'/'Child'@'ID',
   Rule =
      [d3_finding(indicated_question, Y)]-
         [d3_finding(indicated_question, X)].


/* d3_knowledge_slices_to_rules(Finding, Rules) <-
      */

d3_knowledge_slices_to_rules(Finding, Rules) :-
   findall( Rule,
      ( d3_knowledge_slice(Finding, Slice),
        d3_knowledge_slice_to_rule(Slice, Rule) ),
      Rules ).


/* d3_knowledge_slice_to_rule(Slice, Rule) <-
      */

d3_knowledge_slice_to_rule(Slice, Rule) :-
   d3_knowledge_slice_to_head_finding(Slice, A:V),
   Condition := Slice/'Condition',
   d3_condition_to_goal(Condition, Goal),
   Rule = [d3_finding(A, V)]-[Goal].

d3_knowledge_slice_to_rule(Slice, Rule) :-
   Id := Slice/'Action'::[
      @type=Type,
      member(Type, ['ActionIndication', 'ActionNextQASet']) ]
      /'TargetQASets'/'QASet'@'ID',
   Condition := Slice/'Condition',
   d3_condition_to_goal(Condition, Goal),
   Rule = [d3_finding(indicated_question, Id)]-[Goal].


/* d3_condition_to_goal(Condition, Goal) <-
      */

d3_condition_to_goal(Condition, Goal) :-
   Type := Condition@type,
   member(Type, [and, or]),
   Conditions := Condition/content::'*',
   maplist( d3_condition_to_goal,
      Conditions, Goals ),
   ( Type = and,
     list_to_comma_structure(Goals, Goal)
   ; Type = or,
     list_to_semicolon_structure(Goals, Goal) ).
d3_condition_to_goal(Condition, Goal) :-
   Type := Condition@type,
   member(Type, ['MofN']),
   [Min, Max] := Condition@[min, max],
   maplist( term_to_atom,
      I, [Min, Max] ),
   C := Condition/content::'*',
   d3_cardinality_constraint_to_goal(interval, C-I, Goal).
d3_condition_to_goal(Condition, Goal) :-
   Type := Condition@type,
   member(Type, [not]),
   C := Condition/'Condition',
   d3_condition_to_goal(C, G),
   Goal = not(G).
d3_condition_to_goal(Condition, Goal) :-
   Type := Condition@type,
   Id := Condition@'ID',
   d3_condition_to_value_for_epce(Type, Condition, Value),
   Goal = d3_condition(Type, Id, Value).


/* d3_cardinality_constraint_to_goal(Mode, Constraint, Goal) <-
      */

d3_cardinality_constraint_to_goal(Mode, Constraint, Goal) :-
   cardinality_constraint_to_dlp(Mode, Constraint, Program),
   maplist( d3_dislog_rule_to_goal,
      Program, Goals ),
   list_to_comma_structure(Goals, Goal).

d3_dislog_rule_to_goal(Rule, Goal) :-
   parse_dislog_rule(Rule, As, Bs, _),
   maplist( d3_condition_to_goal,
      As, Goals_As ),
   maplist( d3_condition_to_goal,
      Bs, Goals_Bs ),
   ( Goals_Bs = [],
     list_to_semicolon_structure(Goals_As, Goal)
   ; list_to_comma_structure(Goals_Bs, Goal_Bs),
     list_to_semicolon_structure([Goal_Bs|Goals_As], Goal) ).


/* possibly_view_rules_in_editor(Title, Rules) <-
      */

possibly_view_rules_in_editor(Title, Rules) :-
   dislog_variable_get(d3_view_rules_in_editor, yes),
   dportray(lp, Rules),
   view_rules_in_editor(Title, Rules).
possibly_view_rules_in_editor(_, _).


/* view_rules_in_editor(Title, Rules) <-
      */

view_rules_in_editor(Title, Rules) :-
   dislog_variable_get(output_path, Results),
   concat(Results, 'rules_tmp.pl', Path),
   predicate_to_file(Path, dportray(lp_xml, Rules)),
   new(Frame, frame(Title)),
   new(Editor, view('File', size(100, 40))),
   send(Frame, append, Editor),
   send(Frame, open),
   send(Editor, load(Path)),
   !.


/******************************************************************/


