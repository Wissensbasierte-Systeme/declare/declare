

/******************************************************************/
/***                                                            ***/
/***       D3:  Rule/Goal Graphs                                ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(d3:d3_rule_goal_graphs, general) :-
   d3_rule_goal_edges_to_picture([
      'P181'-'Rfb12', 'Rfb12'-not_node_1,
      not_node_1-and_node_1,
      and_node_1-'MofN_node_0_1_0815',
      'MofN_node_0_1_0815'-'Msi280=a1',
      'MofN_node_0_1_0815'-('Msi280':'Msi280a3'),
      and_node_1-or_node_1,
      or_node_1-('Mf14':a1), or_node_1-('Msi280'=a2) ]).


/*** interface ****************************************************/


/* d3_rule_goal_edges_to_picture(Edges) <-
      */

d3_rule_goal_edges_to_picture(Edges) :-
   maplist( d3_finding_edge_to_edge,
      Edges, Edges_2 ),

   edges_to_vertices(Edges_2, Vertices),
%  writeq(user, Vertices), wait,
   maplist( d3_rule_goal_node_classify,
      Vertices, Vertices_1 ),
%  writeq(user, Vertices_1), nl(user), wait, !,
   ( foreach(A-B-C-D-E, Vertices_1),
     foreach(A-B-C-D-E-(0,0), Vertices_2) do
        true ),
%  writeq(user, Vertices_2), nl(user), !,
   vertices_and_edges_to_picture(bfs, Vertices_2, Edges_2, _).

/*
   graph_edges_to_picture(d3_rule_goal_node_classify, Edges_2).
*/
/*
   gxl_layout_variable_switch(tree_offset_x, X_Offset, 75),
   graph_edges_to_picture_bfs(
      d3_rule_goal_node_classify, Edges_2, Picture),
   visur_popups:color_all_links(Picture, black),
   gxl_layout_variable_set(tree_offset_x, X_Offset).
*/


/* d3_finding_edge_to_edge(X1-Y1, X2-Y2) <-
      */

d3_finding_edge_to_edge(X1-Y1, X2-Y2) :-
   d3_finding_to_node(X1, X2),
   d3_finding_to_node(Y1, Y2).

d3_finding_to_node(X, Y) :-
   d3_finding_to_equality(X, Z),
   Z =.. [Op, A, V],
   concat([A, Op, V], Y).
d3_finding_to_node(X, X).


/* d3_finding_to_equality(Finding, A=V) <-
      */

d3_finding_to_equality(Finding, A=V) :-
   ( Finding = (A:V2)
   ; Finding = (A=V2) ),
   concat(A, V, V2),
   !.
d3_finding_to_equality(Finding, A=V) :-
   ( Finding = (A:V)
   ; Finding = (A=V) ).


/* d3_rule_goal_node_classify(Node_1, Node_2) <-
      */

d3_rule_goal_node_classify(Node, Node-and-circle-grey-small) :-
   concat(and, _, Node),
   !.
d3_rule_goal_node_classify(Node, Node-or-triangle-white-small) :-
   concat(or, _, Node),
   !.
d3_rule_goal_node_classify(Node, Node-not-rhombus-orange-small) :-
   concat(not, _, Node),
   !.
d3_rule_goal_node_classify(Node, Node-Label-circle-white-small) :-
   concat('MofN_', K1_K2_, Node),
   name_split_at_position(["_"], K1_K2_, [_, K1, K2|_]),
   concat(['MofN(', K1, ',', K2, ')'], Label),
   !.
d3_rule_goal_node_classify(Node_1, Node_2) :-
   d3_vertex_classify(Node_1, Node_2).


/* d3_vertex_classify(Vertex, Classified_Vertex) <-
      */

d3_vertex_classify(Vertex, Vertex-Vertex-triangle-blue-small) :-
   concat('R', _, Vertex),
   !.
d3_vertex_classify(Vertex, Vertex-Vertex-File-''-(20, 20)) :-
   concat('P', _, Vertex),
   not(concat('Prop', _, Vertex)),
   !,
   dislog_variable_get(image_directory, Directory),
   concat(Directory, 'diagnosis_orange.gif', File).
d3_vertex_classify(Vertex, Vertex-Vertex-File-''-(20, 20)) :-
   concat('M', _, Vertex),
   !,
   dislog_variable_get(image_directory, Directory),
   concat(Directory, 'single.gif', File).
d3_vertex_classify(Vertex, Vertex-Vertex-box-green-small) :-
   concat('Q', _, Vertex),
   !.
d3_vertex_classify(Vertex, Vertex-Vertex-circle-grey-small) :-
   !.

d3_vertex_classify_previous(Vertex, Classified_Vertex) :-
   d3_vertex_classify(Vertex, Id-Label-Symbol-Color-Size),
   Classified_Vertex = Id-[Label]-Symbol-Color-Size.


/******************************************************************/


