

/******************************************************************/
/***                                                            ***/
/***       D3:  Question Dialogs                                ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(d3_dialog, Type) :-
   member(Type, ['OC', 'MC']),
   Question = 'Question':['ID':'Q1', type:Type]:[
      'Text':['Test'],
      'Answers':[
         'Answer':['ID':1, type:'AnswerChoice']:[
            'Text':['Antwort 1'] ],
         'Answer':['ID':2, type:'AnswerChoice']:[
            'Text':['Antwort 2'] ] ] ],
   d3_dialog(Question, Answer),
   writeln(user, 'Answer'=Answer).


/*** interface ****************************************************/


/* d3_dialog_for_file_and_id(File, Qid, Answer) <-
      */

d3_dialog_for_file_and_id(File, Qid, Answer) :-
   dread(xml, File, [KB]),
   d3_dialog_for_id(KB, Qid, Answer).


/* d3_dialog_for_id(Qid, Answer) <-
      */

d3_dialog_for_id(Qid, Answer) :-
   d3_knowledge_base(KB),
   d3_dialog_for_id(KB, Qid, Answer).


/* d3_dialog_for_id(KB, Qid, Answer) <-
      */

d3_dialog_for_id(KB, Qid, Answer) :-
   Question := KB/'Questions'/'Question'::[@'ID'=Qid],
   d3_dialog(Question, Answer),
   write_list(user, ['answer = ', Answer, '\n']).

d3_dialog(Question, Answer) :-
   Type := Question@type,
   ( Type = 'OC' ->
     Title = 'One Choice'
   ; Type = 'MC',
     Title = 'Multiple Choice' ),
   new(Dialog, dialog(Title)),
%  send(Dialog, background, gainsboro),
%  rgb_to_colour([14,14,15.99], Colour),
   rgb_to_colour([15,15,15], Colour),
   send(Dialog, background, Colour),
   d3_dialog_buttons(Dialog, Question, QA_Pairs),
   send(Dialog, default_button, 'Accept'),
   get(Dialog, confirm, Answer_Text),
   d3_dialog_answer_text_to_answer(
      Type, QA_Pairs, Answer_Text, Answer),
   send(Dialog, destroy),
   !.

d3_dialog_answer_text_to_answer(
      'OC', QA_Pairs, Answer_Text, Answer) :-
   ( Answer_Text \== @nil ->
     member([Answer, Answer_Text], QA_Pairs)
   ; Answer = 0 ).
d3_dialog_answer_text_to_answer(
      'MC', QA_Pairs, Chain, Answers) :-
   ( Chain \== @nil ->
     chain_to_list(Chain, List),
     findall( Answer,
        ( member(Answer_Text, List),
          member([Answer, Answer_Text], QA_Pairs) ),
        Answers )
     ; Answers = [] ).

d3_dialog_buttons(Dialog, Question, QA_Pairs) :-
   send_list(Dialog, append, [
      new(Q, text_item('Question')),
      new(Text, text_item('Text')),
      new(Answers, menu('Answers')),
      button('Accept',
         message(Dialog, return, Answers?selection)),
      button('Cancel',
         message(Dialog, return, @nil)) ]),
   Type := Question@type,
   ( Type = 'MC' ->
     send(Answers, multiple_selection, multiple := @on)
   ; Type = 'OC' ),
   d3_send_question_text(Question, Q, Text),
   d3_send_answer_texts_for_question(
      Question, Answers, QA_Pairs).

d3_send_question_text(Question, Q, Text) :-
   Question_Texts := Question/'Text'/content::'*',
   Question_Id := Question@'ID',
   send(Q, selection, Question_Id),
   concat(Question_Texts, Question_Text),
   send(Text, selection, Question_Text).

d3_send_answer_texts_for_question(
      Question, Answers, QA_Pairs) :-
   send(Answers, on_image, 'on_marked.bm'),
   send(Answers, off_image, 'off_marked.bm'),
   findall( [Id, Answer_Text],
      ( Answer := Question/'Answers'/'Answer',
        Answer_Texts := Answer/'Text'/content::'*',
        concat(Answer_Texts, Answer_Text),
        Id := Answer@'ID' ),
      QA_Pairs ),
   checklist( d3_id_answer_pair_to_dialog(Answers),
      QA_Pairs ),
   send(Answers, layout(orientation := vertical)).
%  send(Answers, format(alignment := center)).
 
d3_id_answer_pair_to_dialog(Answers, [_, Answer_Text]) :-
   send(Answers, append, Answer_Text).


/******************************************************************/


