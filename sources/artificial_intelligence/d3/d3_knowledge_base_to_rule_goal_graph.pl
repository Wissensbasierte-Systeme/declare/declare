

/******************************************************************/
/***                                                            ***/
/***       D3:  Knowledge Base to Rule/Goal Graph               ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(d3:d3_file_to_rule_goal_edges, 'EchoDOC'(Findings)) :-
   ( Findings = ['P306':'P7']
   ; Findings = ['P126':'P6']
   ; Findings = ['P200':'P7'] ),
   File = 'examples/d3/EchoDOC.xml',
   d3_file_and_findings_to_rule_goal_edges(
      File, Findings, Edges),
   d3_rule_goal_edges_to_picture(Edges).

test(d3:d3_file_to_rule_goal_edges, 'SonoConsult'(Findings)) :-
   ( Findings = ['P181':'P6']
   ; Findings = ['P181':'P6', 'P1398':'P6']
   ; Findings = ['P169':'P6'] ),
   File = 'examples/d3/SonoConsult.xml',
   d3_file_and_findings_to_rule_goal_edges(
      File, Findings, Edges),
   d3_rule_goal_edges_to_picture(Edges).

test(d3:d3_findings_to_rule_goal_edges, Findings) :-
   dread(xml, 'examples/d3/SonoConsult.xml', [KB]),
   Findings = ['P822':'P3'],
   d3_knowledge_base_to_knowledge_slices(KB),
   d3_findings_to_rule_goal_edges(Findings, Edges),
   d3_rule_goal_edges_to_picture(Edges).

test(d3:d3_knowledge_slice_to_rule_goal_edges, Finding-Id) :-
   dread(xml, 'examples/d3/SonoConsult.xml', [KB]),
   member(Finding:Id, [('P822':'P3'):'Rfb1957']),
   d3_knowledge_base_to_knowledge_slices(KB),
   d3_finding_and_knowledge_slice_id_to_edges(
      Finding, Id, Edges),
   d3_rule_goal_edges_to_picture(Edges).


/*** interface ****************************************************/


/* d3_finding_and_knowledge_slice_id_to_edges(
         Finding, Id, Edges) <-
      */

d3_finding_and_knowledge_slice_id_to_edges(
      Finding, Id, Edges) :-
   d3_knowledge_slice(Finding, Knowledge_Slice),
   Id := Knowledge_Slice@'ID',
   d3_knowledge_slice_to_edges(Knowledge_Slice, Edges).


/* d3_file_and_findings_to_rule_goal_edges(
         File, Findings, Edges) <-
      */

d3_file_and_findings_to_rule_goal_edges(
      File, Findings, Edges) :-
   maplist( d3_file_and_finding_to_knowledge_slices(File),
      Findings, List_1 ),
   append_and_sort(List_1, Slices),
   d3_knowledge_slices_to_edges(Slices, Edges).


/* d3_findings_to_rule_goal_edges(Findings, Edges) <-
      */

d3_findings_to_rule_goal_edges(Findings, Edges) :-
   findall( Slice,
      ( member(Finding, Findings),
        d3_knowledge_slice(Finding, Slice) ),
      Knowledge_Slices ),
   d3_knowledge_slices_to_edges(Knowledge_Slices, Edges).


/* d3_knowledge_slices_to_edges(Knowledge_Slices, Edges) <-
      */

d3_knowledge_slices_to_edges(Knowledge_Slices, Edges) :-
   maplist( d3_knowledge_slice_to_edges,
      Knowledge_Slices, List ),
   append_and_sort(List, Edges).


/* d3_knowledge_slice_to_edges(Slice, Edges) <-
      */

d3_knowledge_slice_to_edges(Slice, [N1-N2|Edges]) :-
   d3_knowledge_slice_to_head_finding(Slice, A:V),
   d3_triple_to_node([A, =, V], N1),
   N2 := Slice@'ID',
   Condition := Slice^'Condition',
   d3_condition_to_edges(N2, Condition, Edges),
   !.
d3_knowledge_slice_to_edges(_, []).


/* d3_condition_to_edges(Node, Condition, Edges) <-
      */

d3_condition_to_edges(Node, Condition, [Node-Node_2|Edges]) :-
   Type := Condition@type,
   member(Type, [and, or, not, 'MofN']),
   !,
   d3_condition_to_node(Condition, Node_2),
   findall( Cond,
      Cond := Condition^'Condition',
      Conditions ),
   maplist( d3_condition_to_edges(Node_2),
      Conditions, List ),
   append_and_sort(List, Edges).
d3_condition_to_edges(Node, Condition, [Node-Node_2]) :-
   Type := Condition@type,
   member(Type, [equal, numEqual, numGreater, 'DState']),
   !,
   Attribute := Condition@'ID',
   d3_condition_to_value_for_epce(Type, Condition, Value),
   d3_condition_type_to_comparator(Type, Comparator),
   d3_triple_to_node([Attribute, Comparator, Value], Node_2).


/* d3_condition_to_node(Condition, Node) <-
      */

d3_condition_to_node(Condition, Node) :-
   Type := Condition@type,
   member(Type, [and, or, not]),
   concat([Type, '_node_'], X),
   gensym(X, Node).
d3_condition_to_node(Condition, Node) :-
   Type := Condition@type,
   Type = 'MofN', 
   [Min, Max] := Condition@[min, max],
   concat([Type, '_node_', Min, '_', Max, '_'], X),
   gensym(X, Node).


/* d3_triple_to_node([X, Operator, Y], Node) <-
      */

d3_triple_to_node([X, Operator, Y], Node) :-
   concat(X, Z, Y),
   !,
   concat([X, Operator, Z], Node).
d3_triple_to_node([X, Operator, Y], Node) :-
   concat([X, Operator, Y], Node).


/******************************************************************/


