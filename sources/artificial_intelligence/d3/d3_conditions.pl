

/******************************************************************/
/***                                                            ***/
/***       D3:  Conditions                                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* d3_condition(Type, Id, Value) <-
      */

d3_condition(Type, Id, Value) :-
   d3_finding(Id, _),
   !,
   d3_finding(Id, V),
   atom_to_number(Value, Val),
   d3_condition_type_to_comparator(Type, Comparator),
   call(Comparator, V, Val).
d3_condition(Type, Id, Value) :-
   !,
   d3_finding(indicated_question, Id),
   d3_dialog_for_id(Id, Answer),
   ( is_list(Answer) ->
     Vs = Answer
   ; Vs = [Answer] ),
   ( foreach(V, Vs) do assert(d3_finding(Id, V)) ),
   !,
   d3_condition(Type, Id, Value).


/******************************************************************/


