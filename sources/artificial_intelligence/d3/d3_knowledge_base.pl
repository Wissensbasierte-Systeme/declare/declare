

/******************************************************************/
/***                                                            ***/
/***       D3:  Knowledge Base                                  ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      d3_knowledge_slice/2.


/*** interface ****************************************************/


/* d3_knowledge_base_to_knowledge_slices(KB) <-
      */

d3_knowledge_base_to_knowledge_slices(KB) :-
   retractall(d3_knowledge_slice(_, _)),
   d3_knowledge_base_to_knowledge_slices(KB, Slices),
   checklist( d3_knowledge_slice_assert,
      Slices ).


/* d3_knowledge_base_to_knowledge_slices(KB, Slices) <-
      */

d3_knowledge_base_to_knowledge_slices(KB, Slices) :-
   findall( Slice,
      Slice := KB^'KnowledgeSlices'^'KnowledgeSlice',
      Slices ).


/* d3_knowledge_slice_assert(Slice) <-
      */

d3_knowledge_slice_assert(Slice) :-
   d3_knowledge_slice_to_head_finding(Slice, Finding),
   !,
   assert(d3_knowledge_slice(Finding, Slice)).
d3_knowledge_slice_assert(_).


/* d3_knowledge_slice_to_head_finding(Slice, A:V) <-
      */

d3_knowledge_slice_to_head_finding(Slice, A:V) :-
   Action := Slice^'Action',
   ( [A, V] := Action^['Question'@'ID', 'Values'^'Value'@'ID']
   ; [A, V] := Action^['Diagnosis'@'ID', 'Score'@value] ).


/* d3_knowledge_slice_to_body_finding(Slice, A:V) <-
      */

d3_knowledge_slice_to_body_finding(Slice, A:V) :-
   ( Condition := Slice^'Condition'
   ; Condition := Slice^_^'Condition' ),
   [A, V] := Condition@['ID', value].


/* d3_knowledge_base_to_diagnosis_findings(KB, Findings) <-
      */

d3_knowledge_base_to_diagnosis_findings(KB, Findings) :-
   findall( A:V,
      [A, V] := KB^'KnowledgeSlices'^'KnowledgeSlice'
         ^'Action'^['Diagnosis'@'ID', 'Score'@value],
      Findings_2 ),
   sort(Findings_2, Findings),
   length(Findings, N),
   writeln(user, N).


/* d3_condition_to_value_for_epce(Type, Condition, Value) <-
      */

d3_condition_to_value_for_epce(Type, Condition, Value) :-
   member(Type, [equal, numEqual, numGreater]),
   Value := Condition@value.
d3_condition_to_value_for_epce('DState', Condition, Value) :-
   V := Condition@value,
   ( V = established,
     Value = 'P8'
   ; V = excluded,
     Value = 'N8' ).


/* d3_condition_type_to_comparator(Type, Comparator) <-
      */

d3_condition_type_to_comparator(Type, Comparator) :-
   member([Type, Comparator], [
      [in, in],
      [equal, =], [numEqual, =], ['DState', =],
      [numGreater, >],
      [numLess, <], [numSmaller, <] ]),
   !.


/******************************************************************/


