

/******************************************************************/
/***                                                            ***/
/***       D3:  Knowledge Base to Edges                         ***/
/***                                                            ***/
/******************************************************************/
   


/*** interface ****************************************************/


/* d3_knowledge_base_to_reachable_objects(
         KB, Prefix, Sources, Targets) <-
      */

d3_knowledge_base_to_reachable_objects(
      KB, Prefix, Sources, Targets) :-
   writeln(user, 'd3_knowledge_base_to_rule_edges ...'),
   d3_knowledge_base_to_rule_edges(finding, KB, Edges_2),
   writeln(user, 'reachable_edges_multiple ...'),
   reachable_edges_multiple(Sources, Edges_2, Edges),
   length(Edges, N),
   ( N < 80,
     d3_rule_goal_edges_to_picture(Edges)
   ; writeln(user, Edges) ),
   write_list(user, [N, ' edges\n']),
   d3_edges_to_special_nodes(Prefix, Edges, Targets),
   length(Targets, M),
   write_list(user, [M, ' objects\n']).


/* d3_knowledge_base_to_finding_edges(KB, Edges) <-
      */
   
d3_knowledge_base_to_finding_edges(KB, Edges) :-
   findall( Y-X,
      ( Slice := KB^'KnowledgeSlices'^'KnowledgeSlice',
        d3_knowledge_slice_to_labeled_rule_edge(
           finding, Slice, X-_-Y) ),
      Edges ).


/* d3_knowledge_base_to_reaching_rule_edges(KB, Target, Edges) <-
      */

d3_knowledge_base_to_reaching_rule_edges(KB, Target, Edges) :-
   ( Target = _:_,
     Mode = finding
   ; Target \= _:_,
     Mode = regular ),
   d3_knowledge_base_to_reaching_rule_edges(
      Mode, KB, Target, Edges).

d3_knowledge_base_to_reaching_rule_edges(
      Mode, KB, Target, Edges) :-
   d3_knowledge_base_to_rule_edges(Mode, KB, Edges_2),
   reaching_edges(Target, Edges_2, Edges).


/* d3_knowledge_base_to_rule_edges(Mode, KB, Edges) <-
      Mode can be 'regular' or 'finding'. */

d3_knowledge_base_to_rule_edges(Mode, KB, Edges) :-
   findall( Rule_Edge,
      ( Slice := KB^'KnowledgeSlices'^'KnowledgeSlice',
        d3_knowledge_slice_to_labeled_rule_edge(
           Mode, Slice, Edge),
        edge_with_label_split(Edge, Rule_Edge) ),
      Edges ).


/* d3_knowledge_slice_to_labeled_rule_edge(Mode, Slice, Edge) <-
      */

d3_knowledge_slice_to_labeled_rule_edge(regular, Slice, Edge) :-
   'RuleComplex' := Slice@type,
   Rid := Slice@'ID',
   ( Cid := Slice^'Condition'@'ID'
   ; Cid := Slice^_^'Condition'@'ID' ),
   member(Selector, ['Diagnosis', 'Question']),
   Aid := Slice^'Action'^Selector@'ID',
   Edge = Cid-Rid-Aid.
d3_knowledge_slice_to_labeled_rule_edge(finding, Slice, Edge) :-
   'RuleComplex' := Slice@type,
   Rid := Slice@'ID',
   d3_knowledge_slice_to_body_finding(Slice, A:B),
   d3_knowledge_slice_to_head_finding(Slice, C:D),
   Edge = (A:B)-Rid-(C:D).


/* d3_knowledge_base_to_question_indication_edges(KB, Edges) <-
      */

d3_knowledge_base_to_question_indication_edges(KB, Edges) :-
   findall( Edge,
      ( d3_knowledge_base_to_question_indication_edge(KB, E),
        edge_with_label_split(E, Edge) ),
      Edges ).

d3_knowledge_base_to_question_indication_edge(KB, Cid-Rid-Aid) :-
   Slice := KB^'KnowledgeSlices'^'KnowledgeSlice',
   'RuleComplex' := Slice@type,
%  write(user, '.'), ttyflush,
   ( Cid := Slice^'Condition'@'ID'
   ; Cid := Slice^_^'Condition'@'ID' ),
   Rid := Slice@'ID',
   Aid := Slice^'Action'^'TargetQASets'^'QASet'@'ID'.


/* d3_knowledge_base_to_question_edges(KB, Question, Edges) <-
      */

d3_knowledge_base_to_question_edges(KB, Question, Edges) :-
   findall( Q1-Q2,
      Q2 := KB^_^'QContainer'::[@'ID'=Q1]
         ^'Children'^'Child'@'ID',
      Edges_2 ),
   list_to_ord_set(Edges_2, Edges_3),
   reachable_edges(Question, Edges_3, Edges),
   writeln(user, Edges).


/* d3_knowledge_base_to_neighbours_of_question(KB, Edges) <-
      */

d3_knowledge_base_to_neighbours_of_question(
      KB, Question, Edges) :-
   findall( Edge,
      ( d3_knowledge_base_to_neighbour_of_question(KB, E),
        edge_is_incident_with_node(E, Question),
        edge_with_label_split(E, Edge) ),
      Edges ).

d3_knowledge_base_to_neighbour_of_question(KB, Cid-Rid-Aid) :-
   Slice := KB^'KnowledgeSlices'^'KnowledgeSlice',
   'RuleComplex' := Slice@type,
%  write(user, '.'), ttyflush,
   ( Cid := Slice^'Condition'@'ID'
   ; Cid := Slice^_^'Condition'@'ID' ),
   Rid := Slice@'ID',
   ( Aid := Slice^'Action'^'Question'@'ID'
   ; Aid := Slice^'Action'^_@'ID' ).


/* edge_is_incident_with_node(Edge_with_Label, Node) <-
      */

edge_is_incident_with_node(U-_-_, U).
edge_is_incident_with_node(_-_-V, V).


/* edge_with_label_split(Edge_with_Label, Edge) <-
      */

edge_with_label_split(U-L-_, U-L).
edge_with_label_split(_-L-V, L-V).


/* d3_knowledge_base_to_question_container_edges(KB, Edges) <-
      */

d3_knowledge_base_to_question_container_edges(KB, Edges) :-
   findall( X-Y,
      Y := KB^'QContainers'^'QContainer'::[@'ID'=X]
         ^'Children'^'Child'@'ID',
      Edges ).


/* d3_knowledge_base_to_question_child_edges(KB, Edges) <-
      */

d3_knowledge_base_to_question_child_edges(KB, Edges) :-
   findall( X-Y,
      Y := KB^'Questions'^'Question'::[@'ID'=X]
         ^'Children'^'Child'@'ID',
      Edges ).


/* d3_edges_to_special_nodes(Prefix, Edges, Nodes) <-
      */

d3_edges_to_special_nodes(Prefix, Edges, Nodes) :-
   edges_to_vertices(Edges, Nodes_2),
   findall( Node,
      ( member(Node, Nodes_2),
        d3_node_has_prefix(Prefix, Node) ),
      Nodes ).

d3_node_has_prefix(Prefix, X:_) :-
   !,
   concat(Prefix, _, X).
d3_node_has_prefix(Prefix, X) :-
   !,
   concat(Prefix, _, X).


/*** tests ********************************************************/


test(d3:d3_knowledge_base_to_reaching_rule_edges, 'P181') :-
   dread(xml, 'examples/d3/SonoConsult.xml', [KB]),
   d3_knowledge_base_to_reaching_rule_edges(
      KB, 'P181', Edges_1),
   d3_rule_goal_edges_to_picture(Edges_1),
   d3_knowledge_base_to_reaching_rule_edges(
      KB, 'P181':'P3', Edges_2),
   d3_rule_goal_edges_to_picture(Edges_2).

test(d3:d3_knowledge_base_to_reachable_objects, Sources) :-
   dread(xml, 'examples/d3/SonoConsult.xml', [KB]),
   Sources = [
      'Mf2975':'Mf2975a1', 'Mf2975':'Mf2975a2', 'RADD1701'],
   d3_knowledge_base_to_reachable_objects(
      KB, 'P', Sources, Targets),
   writeln(user, Targets).


/******************************************************************/


