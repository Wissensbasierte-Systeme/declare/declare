

/******************************************************************/
/***                                                            ***/
/***       D3:  Statistics                                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* d3_knowledge_base_to_condition_statistics <-
      */

d3_knowledge_base_to_condition_statistics :-
   d3_knowledge_base_to_condition_statistics(Multiset),
   d3_multiset_to_bar_chart(Multiset, Distribution),
   dislog_bar_chart('Conditions', Distribution, 20),
   findall( [K, M],
      member(K:M, Multiset),
      Rows ),
   xpce_display_table(_, _, 'Conditions',
      ['Conditions', 'Amount'], Rows).


/* d3_knowledge_base_to_condition_statistics(Multiset) <-
      */

d3_knowledge_base_to_condition_statistics(Multiset) :-
   d3_knowledge_base_read(KB),
   d3_knowledge_base_to_condition_statistics(KB, Numbers),
   list_to_multiset(Numbers, Multiset).

d3_knowledge_base_to_condition_statistics(KB, Numbers) :-
   findall( N,
      ( Slice := KB^_^'KnowledgeSlice',
        d3_knowledge_slice_to_conditions(Slice, Conditions),
        write(user, '.'), ttyflush,
        length(Conditions, M),
        N is M // 5,
%       N = M,
        d3_knowledge_slice_check(Slice, N) ),
      Numbers ).

d3_knowledge_slice_check(Slice, N) :-
   N > 100,
   assert(d3_knowledge_slice(Slice)),
   !.
d3_knowledge_slice_check(_, _).

d3_knowledge_slice_to_conditions(Slice, Conditions) :-
   findall( Condition,
      ( ( Condition := Slice^'Condition'
        ; Condition := Slice^_^'Condition' ),
        _ := Condition@'ID',
        _ := Condition@value ),
      Conditions ),
   !.


/* d3_multiset_to_bar_chart(Multiset, Distribution) <-
      */

d3_multiset_to_bar_chart(Multiset, Distribution) :-
   length(Multiset, N),
   d3_bar_chart_colours(N, Colours),
   pair_lists(Multiset, Colours, Pairs),
   findall( [K, Colour]-M,
      member([K:M, Colour], Pairs),
      Distribution ).

d3_bar_chart_colours(N, Colours) :-
   dislog_variable_get(dislog_unit_colours, Cs),
   length(Cs, M),
   K is 1 + N // M,
   multify(Cs, K, List),
   append(List, Cs_2),
   first_n_elements(N, Cs_2, Colours).


/******************************************************************/


