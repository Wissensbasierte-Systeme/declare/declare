

/******************************************************************/
/***                                                            ***/
/***       D3:  Case Study                                      ***/
/***                                                            ***/
/******************************************************************/



/*** files ********************************************************/


case_study_directory(d3, knowledge_base, Directory) :-
   home_directory(Home),
   concat(Home, '/research/projects/D3/', Directory).

case_study_file(d3, knowledge_base, Path) :-
   case_study_directory(d3, knowledge_base, Directory),
%  File = 'rheuma_wb.test',
%  File = 'rheuma_wb.xml',
%  File = 'SonoConsult_P181.xml',
   File = 'SonoConsult.xml',
   concat(Directory, File, Path).


/*** interface ****************************************************/


/* case_study(d3, Mode) <-
      */

case_study(d3, Mode) :-
   d3_knowledge_base_read(KB),
   case_study(d3, KB, Mode).

case_study(d3, KB, Mode) :-
   case_study(d3, KB, Mode, Edges),
   case_study_directory(d3, knowledge_base, Directory),
   concat(Directory, 'd3_graph.gxl', File),
   graph_edges_to_gxl_file(Edges, File).

case_study(d3, KB, schema_edges_extended) :-
%  fn_triple_to_schema_picture(KB).
   fn_triple_to_schema_picture_2(KB),
   !.
case_study(d3, KB, knowledge_base_view) :-
   d3_knowledge_base_view(KB, KB_2),
   case_study_file(d3, knowledge_base, Path),
   concat(Path, '.compact', Path_2),
   write_list(user, ['---> ', Path_2]), nl,
   dwrite(xml, Path_2, KB_2),
   !.


/* case_study(d3, KB, Mode, Edges) <-
      */

case_study(d3, KB, rule_edges, Edges) :-
   d3_knowledge_base_to_reaching_rule_edges(KB, 'P181', Edges),
   graph_edges_to_picture_bfs_inverted(d3_vertex_classify, Edges).
case_study(d3, KB, neighbours_of_question, Edges) :-
   d3_knowledge_base_to_neighbours_of_question(KB, 'Msi250', Edges),
   graph_edges_to_picture(d3_vertex_classify, Edges).
case_study(d3, KB, question_edges, Edges) :-
   d3_knowledge_base_to_question_edges(KB, 'Qcl600', Edges),
   graph_edges_to_picture_bfs(d3_vertex_classify, Edges).
case_study(d3, KB, schema_edges, Edges) :-
   d3_knowledge_base_to_schema_edges(KB, Edges),
   graph_edges_to_picture_bfs(gxl_vertex_classify, Edges).
   

/* d3_knowledge_base_read(Knowledge_Base) <-
      */

d3_knowledge_base_read(Knowledge_Base) :-
   case_study_file(d3, knowledge_base, Path),
   write_list(user, ['<--- ', Path]), nl,
   dread(xml, Path, [Knowledge_Base]).


/*** implementation ***********************************************/


/* d3_knowledge_base_view(KB_1, KB_2) <-
      */

d3_knowledge_base_view(KB_1, KB_2) :-
   Rule = _-(equal:['ID':_, value:_]:[])-d3_shorten_value_by_id,
   S_1 = [ (X:As:Es)-('Condition':[type:X|As]:Es), Rule,
      _-(_)-d3_or_condition_to_in_condition ],
   S_2 = [ Rule ],
   fn_transform_elements(S_2, [KB_1], [KB_3]),
   fn_transform_elements_fixpoint(S_1, [KB_3], [KB_2]).


/* d3_shorten_value_by_id(Equal_Element_1, Equal_Element_2) <-
      */

d3_shorten_value_by_id(
      equal:['ID':Id, value:V1]:[],
      equal:['ID':Id, value:V2]:[] ) :-
   concat(Id, V2, V1).


/* d3_or_condition_to_in_condition(Or, In) <-
      */

d3_or_condition_to_in_condition(Or, In) :-
   fn_item_parse(Or, or:[]:Equals),
   Id := Or/equal@'ID',
   maplist( d3_equal_to_element(Id),
      Equals, Elements ),
   In = in:['ID':Id]:Elements.

d3_equal_to_element(Id, Equal, Element) :-
   Equal = equal:['ID':Id, value:V]:[],
   Element = element:[value:V]:[].


/* d3_knowledge_base_view_to_display(File) <-
      */

d3_knowledge_base_view_to_display(File) :-
   dread(xml, File, [KB_1]),
   d3_knowledge_base_view_2(KB_1, KB_2),
   d3_knowledge_base_to_display(KB_2).


/* d3_knowledge_base_view_2(KB_1, KB_2) <-
      */

d3_knowledge_base_view_2(KB_1, KB_2) :-
   Fng = [
      (X ---> Y :-
          d3_shorten_value_by_id_2(X, Y)),
      (X ---> Y :-
          d3_or_condition_to_in_condition_2(X, Y)) ],
   fn_transform([fng_program(Fng), item], KB_1, KB_2).
  
d3_shorten_value_by_id_2(
      'Condition':[type:equal, 'ID':Id, value:V1]:[],
      'Condition':[type:equal, 'ID':Id, value:V2]:[] ) :-
   concat(Id, V2, V1).

d3_or_condition_to_in_condition_2(Or, In) :-
   Or = 'Condition':[type:or]:Equals,
   Id := Or/'Condition'::[@type=equal]@'ID',
   maplist( d3_equal_condition_to_element(Id),
      Equals, Elements ),
   In = 'Condition':[type:in, 'ID':Id]:Elements.

d3_equal_condition_to_element(Id, Equal, Element) :-
   Equal = 'Condition':[type:equal, 'ID':Id, value:V]:[],
   Element = element:[value:V]:[].


/* d3_knowledge_base_to_schema_edges(KB, Edges) <-
      */

d3_knowledge_base_to_schema_edges(KB, Edges) :-
   fn_triple_to_schema_edges(KB, Edges),
   writeln(user, Edges).


/******************************************************************/


