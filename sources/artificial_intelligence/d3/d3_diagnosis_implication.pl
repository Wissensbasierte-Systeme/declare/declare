

/******************************************************************/
/***                                                            ***/
/***       D3:  Implication of Diagnosis Findings               ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(d3_subsumes, model).

:- dynamic
      d3_explanation_tree/1.


/*** tests ********************************************************/


test(d3:d3_diagnosis_finding_tree, 'EchoDOC'(Type)) :-
   member(Type, [model, value]),
   File = 'examples/d3/EchoDOC.xml',
   d3_file_to_explanation_tree(File, Tree),
   dislog_variable_switch(d3_subsumes, X, Type),
   d3_diagnosis_finding_tree_to_subsumes_edges(Tree, Edges),
   dislog_variable_set(d3_subsumes, X),
   d3_rule_goal_edges_to_picture(Edges).

d3_file_to_explanation_tree(_, Tree) :-
   d3_explanation_tree(Tree),
   !.
d3_file_to_explanation_tree(File, Tree) :-
   dread(xml, File, [KB]),
   d3_knowledge_base_to_explanations(KB, Tree),
   assert(d3_explanation_tree(Tree)).


/*** interface ****************************************************/


/* d3_diagnosis_finding_to_explanations(Finding, Tree) <-
      */

d3_diagnosis_finding_to_explanations(Finding, Tree) :-
   d3_explanation_tree([[]|Trees]),
   member(Tree, Trees),
   Tree = [[Finding]|_].


/* d3_diagnosis_finding_tree_to_subsumes_edges(Tree, Edges) <-
      */

d3_diagnosis_finding_tree_to_subsumes_edges(Tree, Edges) :-
   findall( X1-X2,
      d3_diagnosis_finding_subsumes(Tree, X1, X2),
      Edges_1 ),
   edges_to_transitive_reduction(Edges_1, Edges_2),
   maplist( d3_edge_to_atom_edge,
      Edges_2, Edges ),
   !.


/* d3_diagnosis_finding_subsumes(Tree, X1, X2) <-
      */

d3_diagnosis_finding_subsumes(Tree, X1, X2) :-
   Tree = [[]|Trees],
   two_members([[X1]|Ts1], [[X2]|Ts2], Trees),
   ( dislog_variable_get(d3_subsumes, model),
     tree_subsumes_tree([[]|Ts2], [[]|Ts1])
   ; dislog_variable_get(d3_subsumes, value),
     d3_tree_subsumes_tree([[]|Ts2], [[]|Ts1]) ).
   
 
/* d3_edge_to_atom_edge(Vs-Ws, V-W) <-
      */

d3_edge_to_atom_edge(Vs-Ws, V-W) :-
   d3_equality_findings_concat(Vs, V),
   d3_equality_findings_concat(Ws, W).

d3_equality_findings_concat([A=V, X|Xs], Y) :-
   d3_equality_findings_concat([X|Xs], Z),
   concat([A, '=', V, ', ', Z], Y).
d3_equality_findings_concat([A=V], Y) :-
   concat([A, '=', V], Y).
d3_equality_findings_concat([], '').


/*** implementation ***********************************************/


/* d3_tree_subsumes_tree(Tree_1, Tree_2) <-
      */

d3_tree_subsumes_tree(Tree_1, Tree_2) :-
   tree_to_state(Tree_1, Models_1),
   tree_to_state(Tree_2, Models_2),
   forall( member(Model_2, Models_2),
      ( member(Model_1, Models_1),
        model_subsumes_model(Model_1, Model_2) ) ). 

   
/* model_subsumes_model(Xs, Ys) <-
      */

model_subsumes_model([X|Xs], Ys) :-
   ( member(X, Ys)
   ; member(Y, Ys),
     d3_finding_subsumes_finding(Y, X) ),
   !,
   model_subsumes_model(Xs, Ys).
model_subsumes_model([], _).


/* d3_finding_subsumes_finding(A=V1, A=V2) <-
      */

d3_finding_subsumes_finding(A=V1, A=V2) :-
   member(A, [
      'Msi155', 'Msi241', 'Msi272', 'Msi78',
      'Mf108', 'Mf166', 'Mf181', 'Mf272' ]),
   d3_value_subsumes_value(V1, V2).

d3_value_subsumes_value(V1, V2) :-
   concat(a, W1, V1), term_to_atom(U1, W1),
   concat(a, W2, V2), term_to_atom(U2, W2),
   U1 > U2.


/* two_members(X1, X2, Xs) <-
      */

two_members(X1, X2, Xs) :-
   append(_, [X1|Ys], Xs),
   member(X2, Ys).
two_members(X1, X2, Xs) :-
   append(_, [X2|Ys], Xs),
   member(X1, Ys).


/******************************************************************/


