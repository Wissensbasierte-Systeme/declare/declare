

models:Es --->
   html:[head:[], body:Es].
model:Es ---> ul:Es.
atom:[name:Atom]:Es ---> li:[Html|Es_2] :-
   d3_atom_to_html(Atom, Html),
   ( Es = [],
     Es_2 = []
   ; Es_2 = [ul:Es] ).
rule:[name:Rule]:[E] ---> E :-
   atom_to_term(Rule, Rule_2, _),
   parse_dislog_rule(Rule_2, _, _, []).
rule:[name:Rule]:Es --->
   li:['rule=', font:[color:'#3333ee']:[Rule], ul:Es] :-
   Es \= [].
rule:[name:Rule]:_ --->
   li:['rule=', font:[color:'#3333ee']:[Rule]].


