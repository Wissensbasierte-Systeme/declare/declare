

/******************************************************************/
/***                                                            ***/
/***       D3:  Diagnostic Reasoning                            ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      d3_finding/2, d3_rule/1, d3_knowledge_base/1.

:- multifile
      d3_finding/2.


/*** tests ********************************************************/


test(d3_diagnostic_reasoning_pl, 1) :-
%  File = 'd3/Travel.xml',
   File = 'd3/SonoConsult/SonoConsult.xml',
   dislog_variable_get(example_path, File, Path),
   dread(xml, Path, [KB]),
   d3_diagnostic_reasoning_pl(KB).


/*** interface ****************************************************/


/* d3_diagnostic_reasoning_pl(KB) <-
      */

d3_diagnostic_reasoning_pl(KB) :-
   assert(d3_knowledge_base(KB)),
   d3_knowledge_base_to_rules(KB, Rules_1),
   dislog_rules_to_prolog_rules(Rules_1, Rules_2),
   dwrite(pl, user, Rules_2),
   (dynamic d3_finding/2),
   R = ( d3_finding(indicated_question, Id) :-
      module:clause(d3_finding(indicated_question, Id), true) ),
   assert(R),
   tp_iteration_prolog(Rules_2, module, [], Atoms),
   retract_facts(d3_finding/2, Answers),
   append(Atoms, Answers, Findings),
   d3_findings_to_html(Findings, Html_1),
   fn_item_prune_lists(Html_1, Html_2),
   html_to_display('D3 Results', Html_2, size(300, 400)),
   view_rules_in_editor('Rules', Rules_1),
   abolish(d3_finding/2),
   retract_all(d3_knowledge_base(_)).


/* d3_diagnostic_reasoning <-
      */

d3_diagnostic_reasoning_with_file(File) :-
   dread(xml, File, [KB]),
   d3_diagnostic_reasoning(KB).

d3_diagnostic_reasoning :-
   writeln(user, 'loading D3 knowledge base ... '),
   d3_knowledge_base_read(KB),
   writeln(user, 'done'),
   assert(d3_finding('Mf280', 100)),
   d3_diagnostic_reasoning(KB).

d3_diagnostic_reasoning(KB) :-
   assert(d3_knowledge_base(KB)),
   d3_diagnostic_reasoning(KB, Findings, Rules),
   writeq(user, Findings), wait,
   d3_findings_to_html(Findings, Html_1),
   fn_item_prune_lists(Html_1, Html_2),
   !,
   html_to_display('D3 Results', Html_2, size(300, 400)),
   view_rules_in_editor('Rules', Rules),
   !,
%  d3_findings_and_rules_to_explanations_xml(Findings, Rules),
   retract_all(d3_knowledge_base(_)).


/* d3_diagnostic_reasoning(KB, Findings, Rules) <-
      */

d3_diagnostic_reasoning(KB, Findings, Rules) :-
   d3_diagnostic_reasoning_before(KB, Rules_2),
   repeat,
      d3_single_diagnostic_iteration(Rules_2, Delta_Findings),
   Delta_Findings = [],
   d3_diagnostic_reasoning_after(Findings, Rules),
   !.

d3_diagnostic_reasoning_before(KB, Rules) :-
%  write(user, 'constructing indicated questions ...'), ttyflush,
%  forall( Id := KB/'InitQASets'/'QContainer'@'ID',
%     assert(d3_finding(indicated_question, Id)) ),
%  writeln(user, 'done'),
   write(user, 'constructing rules ... '), ttyflush,
   d3_knowledge_base_to_rules(KB, Rules),
   writeln(user, 'done'),
   d3_diagnostic_reasoning_before_rules_to_file(Rules).

d3_diagnostic_reasoning_before_rules_to_file(Rules) :-
   dislog_variable_get(output_path, 'd3_all_rules.pl', Path),
   d3_diagnostic_rules_to_file(Rules, Path).

d3_diagnostic_reasoning_after(Findings, Rules) :-
   collect_facts(d3_finding/2, Findings),
   collect_arguments(d3_rule, Rules),
   retract_all(d3_finding(_, _)),
   retract_all(d3_rule(_)).


/* d3_diagnostic_rules_to_file(Rules, File) <-
      */

d3_diagnostic_rules_to_file(Rules, File) :-
   write_list(user, ['writing rules to file ', File, ' ... ']),
   ttyflush,
   ( foreach(Rule, Rules), foreach(Rule_2, Rules_2) do
        ( Rule = As-[B] -> comma_structure_to_list(B, Bs)
        ; Rule = As-[] -> Bs = []
        ; Rule = As -> Bs = [] ),
        Rule_2 = As-Bs ),
   dportray(File, lp_xml, Rules_2),
   writeln(user, 'done').


/* d3_single_diagnostic_iteration(Rules, Delta_Findings) <-
      */

d3_single_diagnostic_iteration(Rules, Delta_Findings) :-
   findall( Finding,
      ( member([Finding]-[Goal], Rules),
        call(Goal),
        \+ call(Finding),
        assert(Finding),
        assert(d3_rule([Finding]-[Goal])) ),
      Delta_Findings ).


/* d3_findings_to_html(Prefix, Findings, Htmls) <-
      */

d3_findings_to_html(Findings, Html) :-
   d3_findings_to_html('M', Findings, Htmls_M),
   d3_findings_to_html('P', Findings, Htmls_P),
   d3_findings_to_html('i', Findings, Htmls_i),
   ( Htmls_M = [] -> Ul_M = '' ; Ul_M = ul:Htmls_M ),
   ( Htmls_P = [] -> Ul_P = '' ; Ul_P = ul:Htmls_P ),
   ( Htmls_i = [] -> Ul_i = '' ; Ul_i = ul:Htmls_i ),
   Html = html:[
      h2:[font:[color:'#990000']:['Diagnoses:']],
%     font:[color:'#990000', size:2]:[b:['Diagnoses:']],
      Ul_P,
      h2:[font:[color:'#000099']:['Answers:']],
%     font:[color:'#000099', size:2]:[b:['Answers:']],
      Ul_M,
      h2:[font:[color:'#009900']:['Indications:']],
%     font:[color:'#009900', size:2]:[b:['Indications:']],
      Ul_i ].

d3_findings_to_html(Prefix, Findings, Htmls) :-
   findall( Html,
      ( member(Finding, Findings),
        d3_finding_to_html(Prefix, Finding, Html) ),
      Htmls ).


/* d3_finding_to_html(Prefix, Finding, Html) <-
      */

d3_finding_to_html(Prefix, d3_finding(Question, Value), Html) :-
   Prefix = 'M',
   concat(Prefix, _, Question),
   ( concat(Question, V, Value)
   ; V = Value ),
   concat([Question, ' = ', V], Equality),
   d3_attribute_to_image(Question, Image),
   Html = li:[ img:[
      src:Image, width:'16',
      height:'16', align:'center' ]:[Equality] ],
   !.
d3_finding_to_html(Prefix, d3_finding(Diagnosis, Score), Html) :-
   Prefix = 'P',
   concat(Prefix, _, Diagnosis),
   d3_knowledge_base(KB),
   Texts := KB/'Diagnoses'
      /'Diagnosis'::[@'ID'=Diagnosis]/'Text'^content::'*',
   concat(Texts, Text),
   d3_score_to_diagnosis_image(Score, Image),
   Html = li:[ img:[
      src:Image, width:'16',
      height:'16', align:'center' ]:[
      Text, ' / ', Diagnosis, em:[]:[' with '], Score ] ],
   !.
d3_finding_to_html(
      Prefix, d3_finding(Indication, Question), Html) :-
   Prefix = 'i',
   concat(Prefix, _, Indication),
   d3_attribute_to_image(Question, Image),
   Html = li:[ img:[
      src:Image, width:'16',
      height:'16', align:'center' ]:[Question] ],
   !.


/* d3_findings_to_indication_links(Findings, Rules) <-
      */

d3_findings_to_indication_links(Findings, Rules) :-   
   findall( Rule,
      ( member(d3_finding(indicated_question, Q), Findings),
        member(d3_finding(Q, A), Findings),
        Rule = [d3_condition(equal, Q, A)]-
           [d3_finding(indicated_question, Q)] ),
      Rules ).


/******************************************************************/


