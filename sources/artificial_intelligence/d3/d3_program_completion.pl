

/******************************************************************/
/***                                                            ***/
/***       Completion:  Definite, Generalized Programs          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* definite_program_to_completed_program(Rules_1, Rules_2) <-
      */

definite_program_to_completed_program(Rules_1, Rules_2) :-
   definite_rules_to_grouped_rules(Rules_1, Rules_a),
   rules_to_normalized_rules([',', ';', not], Rules_a, Rules_b),
   rules_invert(Rules_b, Rules_3),
   rules_to_normalized_rules([';', ',', not], Rules_1, Rules_4),
   append(Rules_4, Rules_3, Rules_2).


/* definite_rules_to_grouped_rules(Rules_1, Rules_2) <-
      */

definite_rules_to_grouped_rules(Rules_1, Rules_2) :-
   findall( Atom,
      ( member(Rule, Rules_1),
        parse_dislog_rule(Rule, [Atom], _, _) ),
      Atoms_2 ),
   sort(Atoms_2, Atoms),
   maplist( atom_to_definition(Rules_1),
      Atoms, Rules_2 ).

atom_to_definition(Rules, Atom, Rule) :-
   findall( B,
      ( member(R, Rules),
        parse_dislog_rule(R, [Atom], Body, _),
        list_to_comma_structure(Body, B) ),
      Bs ),
   list_to_semicolon_structure(Bs, Atom_2),
   Rule = [Atom]-[Atom_2].


/* rules_to_normalized_rules(Operators, Rules_1, Rules_2) <-
      */

rules_to_normalized_rules(Operators, Rules_1, Rules_2) :-
   maplist( rule_to_normalized_rules(Operators),
      Rules_1, Programs ),
   append(Programs, Rules_2).

rule_to_normalized_rules(Operators, Rule, Rules) :-
   parse_dislog_rule(Rule, Head, Body, []),
   list_to_comma_structure(Body, Formula),
   formula_to_state(Operators, Formula, State),
   findall( Head-Cs,
      member(Cs, State),
      Rules ).


/* rules_invert(Rules_1, Rules_2) <-
      */

rules_invert(Rules_1, Rules_2) :-
   maplist( rule_invert,
      Rules_1, Rules_2 ).

rule_invert(Rule_1, Rule_2) :-
   parse_dislog_rule(Rule_1, Head, Body, []),
   Rule_2 = Body-Head.


/* formula_to_state([X, Y, N], Formula, State) <-
      */

formula_to_state([X, Y, N], Formula, State) :-
   Formula =.. [X|Formulas],
   !,
   maplist( formula_to_state([X, Y, N]),
      Formulas, States ),
   ord_union(States, State).
formula_to_state([X, Y, N], Formula, State) :-
   Formula =.. [Y|Formulas],
   !,
   maplist( formula_to_state([X, Y, N]),
      Formulas, States ),
   state_disjunction(States, State_2),
   sort(State_2, State).
formula_to_state([X, Y, N], Formula, State) :-
   Formula =.. [N, Formula_2],
   !,
   formula_to_state([Y, X, N], Formula_2, State_2),
   negate_state(State_2, State_3),
   maplist( sort,
      State_3, State_4 ),
   sort(State_4, State).
formula_to_state([_, _, _], Atom, [[Atom]]).


/*** tests ********************************************************/


test(d3:program_completion, 1) :-
   Program_1 = [[a]-[b,c], [a]-[d;not(e)]],
   definite_program_to_completed_program(Program_1, Program_2),
   !,
   Program_2 = [
      [a]-[b, c], [a]-[d], [a]-[~e],
      [b, d, ~e]-[a], [c, d, ~e]-[a] ].

test(d3:formula_to_state, 1) :-
   Formula = (not((a; not(e))); (c, d)),
   formula_to_state([',', ';', not], Formula, State),
   !,
   State = [[c, e], [c, ~a], [d, e], [d, ~a]].

test(d3:formula_to_state, 2) :-
   Formula = not((b,(c;f))),
   formula_to_state([',', ';', not], Formula, State),
   !,
   State = [[~b, ~c], [~b, ~f]].

 
/******************************************************************/


