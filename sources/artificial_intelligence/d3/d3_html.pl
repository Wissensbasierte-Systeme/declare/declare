

/******************************************************************/
/***                                                            ***/
/***       D3:  Case Study with HTML                            ***/
/***                                                            ***/
/******************************************************************/


/*
d3_score_to_diagnosis_image('P6', File) :-
   dislog_variable_get(image_directory, Directory),
   concat(Directory, 'diagnosis_p6.gif', File).

d3_attribute_to_image('N6', File) :-
   dislog_variable_get(image_directory, Directory),
   concat(Directory, 'diagnosis_n6.gif', File).
*/

d3_score_to_diagnosis_image(_, File) :-
   dislog_variable_get(image_directory, Directory),
   concat(Directory, 'diagnosis_orange.gif', File).

d3_attribute_to_image(_, File) :-
   dislog_variable_get(image_directory, Directory),
   concat(Directory, 'single.gif', File).


/*** interface ****************************************************/


/* d3_knowledge_base_derivation_tree_to_html(KB, Diagnosis) <-
      */

d3_knowledge_base_derivation_tree_to_html(Diagnosis) :-
   d3_knowledge_base_read(KB),
   d3_knowledge_base_derivation_tree_to_html(KB, Diagnosis).

d3_knowledge_base_derivation_tree_to_html(KB, Diagnosis) :-
   d3_knowledge_base_to_reaching_rule_edges(
      KB, Diagnosis, Edges),
   edges_to_vertices(Edges, Nodes),
   findall( Html,
      ( Rule := KB^_^'KnowledgeSlice',
        'RuleComplex' := Rule@type,
        Id := Rule@'ID',
        member(Id, Nodes),
        d3_rule_to_html(Rule, Html) ),
      Htmls ),
   case_study_file(d3, knowledge_base, Path),
   concat(Path, '.extract.html', Path_2),
   nl, write_list(user, ['---> ', Path_2]), nl,
   dwrite(xml, Path_2, html:[]:Htmls),
   www_open_url(Path_2).


/* d3_knowledge_base_to_display <-
      */

d3_knowledge_base_to_display :-
   d3_knowledge_base_read(KB),
   d3_knowledge_base_to_display(KB).

d3_knowledge_base_to_display(KB) :-
   d3_knowledge_base_to_html(KB, Html),
   html_to_display(
      'D3 Knowledge Base', Html, size(400, 275)).


/* d3_knowledge_base_to_html(KB) <-
      */

d3_knowledge_base_to_html :-
   d3_knowledge_base_read(KB),
   d3_knowledge_base_to_html(KB, Html),
   case_study_file(d3, knowledge_base, Path),
   concat(Path, '.html', Path_2),
   write_list(user, ['---> ', Path_2]), nl,
   dwrite(xml, Path_2, Html).


/* d3_knowledge_base_to_html(KB, Html) <-
      */

d3_knowledge_base_to_html(KB, Html) :-
   findall( Html,
      ( Rule := KB^_^'KnowledgeSlice',
        'RuleComplex' := Rule@type,
        d3_rule_to_html(Rule, Html) ),
      Htmls ),
   Html = html:[
      h2:[font:[color:'#009900']:['Rules:']]|Htmls].

d3_rule_to_html(Rule, Html) :-
   writeln(user, Rule),
   Id := Rule@'ID',
   Condition := Rule^'Condition',
   T := Rule^'Action'@type,
   concat(['(', T, ')'], Type),
   d3_condition_to_html(Condition, Html_2),
   Header = [ '&nbsp; &nbsp;',
      font:[color:'#000099']:[ 'Rule',
         b:[]:[Id], Type], '<br>' ],
   If = [ '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
      em:[]:['if &nbsp;'] ],
   d3_rule_to_then(Rule, Then),
   append(
      [Header, If, Html_2, ['<br>'], Then, ['<p>']], Html),
   writeln(user, Html),
   !.

d3_rule_to_then(Rule, Then) :-
   Diagnosis := Rule^'Action'^'Diagnosis'@'ID',
   Score := Rule^'Action'^'Score'@value,
   d3_score_to_diagnosis_image(Score, Image),
   Then = [ '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
      em:[]:['then &nbsp;'],
      img:[
         src:Image, width:'16',
         height:'16', align:'center' ]:[
         Diagnosis, em:[]:[' with '], Score ] ].
d3_rule_to_then(Rule, Then) :-
   Question := Rule^'Action'^'Question'@'ID',
   findall( Value,
      ( V := Rule^'Action'^'Values'^'Value'@'ID',
        concat(Question, Value, V) ),
      Values ),
   Values = [Value],
   concat([Question, ' = ', Value], Equality),
   d3_attribute_to_image(Question, Image),
   Then = [ '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
      em:[]:['then &nbsp;'],
      img:[
         src:Image, width:'16',
         height:'16', align:'center' ]:[
         Equality ] ].
d3_rule_to_then(Rule, Then) :-
   Type := Rule^'Action'@type,
   member(Type, ['ActionIndication', 'ActionContraIndication']),
   findall( Qid,
      Qid := Rule^'Action'^'TargetQASets'^'QASet'@'ID',
      Qids ),
   length(Qids, N),
   ( N > 1,
     Is_or_Are = are
   ; Is_or_Are = is ),
   ( Type = 'ActionIndication',
     Indicated_or_Contra = 'indicated'
   ; Indicated_or_Contra = 'contra-indicated' ),
   iterate_list(name_append_with_comma, '', Qids, Qid),
   d3_attribute_to_image(Qid, Image),
   Then = [ '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
      em:[]:['then &nbsp;'],
      img:[
         src:Image, width:'16',
         height:'16', align:'center' ]:[
         Qid ],
      em:[]:['&nbsp; ', Is_or_Are, ' ', Indicated_or_Contra] ].


/* d3_condition_to_html(Condition, Html) <-
      */

d3_condition_to_html(Condition, Html) :-
   in := Condition@type,
   !,
   findall( V,
      V := Condition/element@value,
      Values ),
   iterate_list(name_append_with_comma, '', Values, Value),
   Attribute := Condition@'ID',
   d3_attribute_to_image(Attribute, Image),
   ( concat(Attribute, Value, V)
   ; Value = V ),
%  d3_condition_type_to_comparator_html(in, Comparator),
   Comparator = ' &isin; ',
   concat([Attribute, Comparator, '{ ', Value, ' }'], Comparison),
   Html = [ img:[
      src:Image, width:'16',
%     height:'16', align:'absmiddle' ]:[Comparison].
      height:'16', align:'center' ]:[Comparison] ].
d3_condition_to_html(Condition, Html) :-
   Type := Condition@type,
   member(Type, [and, or]),
   !,
   findall( C,
      C := Condition^'Condition',
      Conditions ),
   maplist( d3_condition_to_html,
      Conditions, Htmls ),
   d3_htmls_to_html(Type, Htmls, Html).
d3_condition_to_html(Condition, Html) :-
   Type := Condition@type,
   member(Type, [equal, numEqual, numGreater]),
   !,
   Attribute := Condition@'ID',
   V := Condition@value,
   d3_attribute_to_image(Attribute, Image),
   ( concat(Attribute, Value, V)
   ; Value = V ),
   d3_condition_type_to_comparator_html(Type, Comparator),
   concat([Attribute, Comparator, Value], Comparison),
   Html = [ img:[
      src:Image, width:'16',
%     height:'16', align:'absmiddle' ]:[Comparison].
      height:'16', align:'center' ]:[Comparison] ].

d3_condition_type_to_comparator_html(Type, Comparator) :-
   d3_condition_type_to_comparator(Type, C),
   concat([' ', C, ' '], Comparator).


d3_htmls_to_html(Operator, Xs, Html) :-
   d3_htmls_to_html_sub(Operator, Xs, Hs),
   append(['(&nbsp; '|Hs], [' &nbsp;)'], Html).

d3_htmls_to_html_sub(Operator, [X1, X2|Xs], [X1, Op|Hs]) :-
   concat(['&nbsp; ', Operator, ' &nbsp;'], Operator_2),
   Op = em:[]:[Operator_2],
   d3_htmls_to_html_sub(Operator, [X2|Xs], Hs).
d3_htmls_to_html_sub(_, [X], [X]).


/* d3_knowledge_base_inspector <-
      */
   
d3_knowledge_base_inspector :-
   dislog_variable_get(source_path, Sources),
   concat(['file://', Sources,
      'artificial_intelligence/d3/d3_inspector.html'], Path),
   Size = size(450, 235),
   file_to_cms_doc_window(
      'DDK', Path, Size, d3_knowledge_base_link_handler).

d3_knowledge_base_link_handler(Atom) :-
   term_to_atom(Goal, Atom),
   call(Goal).


/*** tests ********************************************************/


test(ontologies:d3, d3_to_html) :-
   d3_condition_to_html(
      'Condition':[
         type:equal, 'ID':'M146', value:'M146a33']:[],
      Html),
   dwrite(xml, user, Html).

test(ontologies:d3, html_file_to_display) :-
   File = '/research/projects/D3/SonoConsult_P181.xml.html',
   home_directory(File, Path),
   html_file_to_display(Path).


/******************************************************************/


