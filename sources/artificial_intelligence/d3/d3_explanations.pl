

/******************************************************************/
/***                                                            ***/
/***       D3:  Diagnostic Reasoning - Explanations             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* d3_findings_and_rules_to_explanations_xml(Findings, Rules) <-
      */

d3_findings_and_rules_to_explanations_xml(Findings, Rules) :-
   d3_findings_to_indication_links(Findings, Rules_2),
   append([Rules, Rules_2], Rules_3),
   view_rules_in_editor('Rules 3', Rules_3),
   used_rules_to_explanations_xml(Rules_3, Xml),
   star_line,
   html_display_d3_explanations_xml(Xml).
%  explain_semantics(minimal, hi, Rules_3).


/* html_display_d3_explanations_xml(Xml) <-
      */

html_display_d3_explanations_xml(Xml) :-
   dwrite(xml, Xml),
   dislog_variable_get(source_path,
      'artificial_intelligence/d3/d3_explanations_fng.pl',
      File_Fng),
   fn_transform_fn_item_fng(File_Fng, Xml, Html),
%  dwrite(xml, Html),
%  html_to_display(Html).
   html_explanations_www_open_url(Html).


/* d3_atom_to_html(Atom, Html) <-
      */

d3_atom_to_html(Atom, Html) :-
   atom_to_term(Atom, Term, _),
   d3_term_to_html(Term, Html),
   !.

d3_term_to_html((T1,T2), Html) :-
   d3_term_to_html(T1, Html_1),
   d3_term_to_html(T2, Html_2),
   Html = ul:[li:[and], li:[Html_1], li:[Html_2]].
d3_term_to_html((T1;T2), Html) :-
   d3_term_to_html(T1, Html_1),
   d3_term_to_html(T2, Html_2),
   Html = ul:[li:[or], li:[Html_1], li:[Html_2]].
d3_term_to_html(not(T), Html) :-
   d3_term_to_html(T, Html_),
   Html = p:['not(', Html_, ')'].

d3_term_to_html(Term, Html) :-
   ( Term = d3_condition(Type, Qid, V)
   ; Term = d3_finding(Qid, V),
     Type = equal ),
   d3_attribute_to_image(Qid, Image),
   ( concat(Qid, Value, V)
   ; Value = V ),
   d3_condition_type_to_comparator_html(Type, Comparator),
   d3_knowledge_base(KB),
   [Text] := KB^'Questions'^'Question'::[@'ID'=Qid]
      ^'Answers'^'Answer'::[@'ID'=V]^'Text'^content::'*',
   concat([Qid, Comparator, Value, ': ', Text], Comparison),
   Html = img:[ src:Image, width:'16',
      height:'16', align:'center' ]:[Comparison].
d3_term_to_html(Term, Html) :-
   Term = d3_finding(indicated_question, Qid),
   !,
   d3_knowledge_base(KB),
   [Text] := KB^'Questions'^'Question'::[@'ID'=Qid]
      ^'Text'^content::'*',
   d3_attribute_to_image(Qid, Image),
   concat([Qid, ': ', Text], Qid_Text),
   Html = img:[ src:Image, width:'16',
      height:'16', align:'center' ]:[Qid_Text].
d3_term_to_html(Term, Html) :-
   Term = d3_finding(Diagnosis, Score),
   concat('P_', Diagnosis_2, Diagnosis),
   d3_score_to_diagnosis_image(Score, Image),
   Html = img:[
      src:Image, width:'16', height:'16', align:'center' ]:[
      Diagnosis_2, em:[]:[' with '], Score ].


/******************************************************************/


