

/******************************************************************/
/***                                                            ***/
/***       DDK:  Question Dialogs                               ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(ddk_dialog, Type) :-
   member(Type, ['OC', 'MC']),
   Question = 'Question':['ID':'Q1', type:Type]:[
      'Text':['Test'],
      'Answers':[
         'Answer':['ID':1, type:'AnswerChoice']:[
            'Text':['Antwort 1'] ],
         'Answer':['ID':2, type:'AnswerChoice']:[
            'Text':['Antwort 2'] ] ] ],
   ddk_dialog(Question, Answer),
   writeln(user, 'Answer'=Answer).


/*** interface ****************************************************/


/* ddk_dialog(Question, Answer) <-
      */

ddk_dialog(Question, Answer) :-
   fn_item_parse(Question, _:As:_),
   member(type:Type, As),
   ( Type = 'OC' ->
     Title = 'One Choice'
   ; Type = 'MC',
     Title = 'Multiple Choice' ),
   new(Dialog, dialog(Title)),
%  send(Dialog, background, gainsboro),
%  rgb_to_colour([14,14,15.99], Colour),
   rgb_to_colour([15,15,15], Colour),
   send(Dialog, background, Colour),
   ddk_dialog_buttons(Dialog, Question, QA_Pairs),
   send(Dialog, default_button, 'Accept'),
   get(Dialog, confirm, Answer_Text),
   ddk_dialog_answer_text_to_answer(
      Type, QA_Pairs, Answer_Text, Answer),
   send(Dialog, destroy),
   !.

ddk_dialog_answer_text_to_answer(
      'OC', QA_Pairs, Answer_Text, Answer) :-
   ( Answer_Text \== @nil ->
     member([Answer, Answer_Text], QA_Pairs)
   ; Answer = 0 ).
ddk_dialog_answer_text_to_answer(
      'MC', QA_Pairs, Chain, Answers) :-
   ( Chain \== @nil ->
     chain_to_list(Chain, List),
     findall( Answer,
        ( member(Answer_Text, List),
          member([Answer, Answer_Text], QA_Pairs) ),
        Answers )
     ; Answers = [] ).

ddk_dialog_buttons(Dialog, Question, QA_Pairs) :-
   send_list(Dialog, append, [
      new(Q, text_item('Question')),
      new(Text, text_item('Text')),
      new(Answers, menu('Answers')),
      button('Accept',
         message(Dialog, return, Answers?selection)),
      button('Cancel',
         message(Dialog, return, @nil)) ]),
   fn_item_parse(Question, _:As:_),
   member(type:Type, As),
   ( Type = 'MC' ->
     send(Answers, multiple_selection, multiple := @on)
   ; Type = 'OC' ),
   ddk_send_question_text(Question, Q, Text),
   ddk_send_answer_texts_for_question(
      Question, Answers, QA_Pairs).

ddk_send_question_text(Question, Q, Text) :-
   fn_item_parse(Question, _:As:Es),
   member('Text':Question_Texts, Es),
   member('ID':Question_Id, As),
   send(Q, selection, Question_Id),
   concat(Question_Texts, Question_Text),
   send(Text, selection, Question_Text).

ddk_send_answer_texts_for_question(
      Question, Answers, QA_Pairs) :-
   fn_item_parse(Question, _:_:Es),
   member('Answers':Xs, Es),
   send(Answers, on_image, 'on_marked.bm'),
   send(Answers, off_image, 'off_marked.bm'),
   findall( [Id, Answer_Text],
      ( member('Answer':As:Answer, Xs),
        member('Text':Answer_Texts, Answer),
        concat(Answer_Texts, Answer_Text),
        member('ID':Id, As) ),
      QA_Pairs ),
   checklist( ddk_id_answer_pair_to_dialog(Answers),
      QA_Pairs ),
   send(Answers, layout(orientation := vertical)).
%  send(Answers, format(alignment := center)).
 
ddk_id_answer_pair_to_dialog(Answers, [_, Answer_Text]) :-
   send(Answers, append, Answer_Text).


/******************************************************************/


