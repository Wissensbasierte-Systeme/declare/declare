

/******************************************************************/
/***                                                            ***/
/***       D3:  Abduction of Diagnoses                          ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(d3:d3_abduction, 'EchoDOC'(Finding)) :-
   member(Finding, ['P200':'P7']),
   File = 'examples/d3/EchoDOC/EchoDOC.xml',
   d3_file_and_finding_to_explanations(File, Finding).

test(d3:d3_abduction, 'SonoConsult'(Finding)) :-
   member(Finding, ['P181':'P6', 'P169':'P6', 'P632':'P3']),
   File = 'examples/d3/SonoConsult/SonoConsult.xml',
   d3_file_and_finding_to_explanations(File, Finding).

test(d3:d3_program_and_finding_to_explanations, Finding) :-
   dread(xml, 'examples/d3/SonoConsult/SonoConsult.xml', [KB]),
   Finding = 'M498':'M498a2',
   d3_knowledge_base_and_finding_to_explanations(
      KB, Finding, Explanations),
   dportray(chs, Explanations).


/*** interface ****************************************************/


/* d3_file_and_finding_to_explanations(File, A:V) <-
      */

d3_file_and_finding_to_explanations(File, A:V) :-
   d3_file_and_diagnosis_to_knowledge_base(File, A, KB),
   d3_knowledge_base_and_finding_to_explanations(KB, A:V).


/* d3_knowledge_base_and_finding_to_explanations(
         KB, Finding) <-
      */

d3_knowledge_base_and_finding_to_explanations(KB, Finding) :-
%  d3_knowledge_base_and_finding_to_explanations(
%     KB, Finding, Models),
%  maplist( d3_explanation_prune,
%     Models, Explanations ),
   Explanations <=
      maplist( d3_explanation_prune,
         d3_knowledge_base_and_finding_to_explanations(
            KB, Finding) ),
   dportray(chs, Explanations),
   state_to_tree(Explanations, [Findings|Trees]),
   Tree = [[Finding|Findings]|Trees],
   dportray(tree, Tree),
   set_tree_to_picture_bfs(Tree),
   !.


/* d3_knowledge_base_and_finding_to_explanations(
         KB, A:V, Models) <-
      */

d3_knowledge_base_and_finding_to_explanations(
      KB, A:V, Models) :-
%  d3_knowledge_base_to_relevant_rules(KB, A, Program),
%  d3_program_and_finding_to_explanations(
%     Program, A:V, Models_2),
%  sublist( conjunction_consistent,
%     Models_2, Models ).
   Models <=
      sublist( conjunction_consistent,
         d3_program_and_finding_to_explanations(
            d3_knowledge_base_to_relevant_rules(KB, A),
            A:V) ).


/* d3_knowledge_base_and_finding_to_explanations(
         KB, Finding, Explanations) <-
      */

d3_knowledge_base_and_finding_to_explanations(
      KB, Finding, Explanations) :-
   d3_knowledge_base_to_knowledge_slices(KB),
   d3_knowledge_slices_to_rules(Finding, Program),
   d3_program_and_finding_to_explanations(
      Program, Finding, Models),
   !,
   maplist( d3_explanation_prune,
      Models, Explanations ).


/* d3_program_and_finding_to_explanations(
         Program, A:V, Models) <-
      */

d3_program_and_finding_to_explanations(Program, A:V, Models) :-
   definite_program_to_completed_program(Program, Rules),
   Rule = [d3_finding(X, Y)]-[d3_condition('equal', X, Y)],
   Rules_2 = [[d3_finding(A, V)], Rule|Rules],
   possibly_view_rules_in_editor(A, Rules_2),
   minimal_models(Rules_2, Models).
%  extended_program_to_stable_models_dlv(Rules_2, Models).


/* d3_explanation_prune(Model, Explanation) <-
      */

d3_explanation_prune(Model, Explanation) :-
   findall( Literal_2,
      ( member(Literal_1, Model),
        d3_condition_prune(Literal_1, Literal_2) ),
      Explanation ).

d3_condition_prune(~Atom, ~E) :-
   !,
   d3_condition_prune(Atom, E).
d3_condition_prune(d3_condition(equal, X, Y), E) :-
   !,
   concat(X, Z, Y),
   E = (X = Z).
d3_condition_prune(d3_condition(numEqual, X, Y), E) :-
   !,
   E = (X = Y).
d3_condition_prune(d3_condition(numGreater, X, Y), E) :-
   !,
   E = (X > Y).
d3_condition_prune(d3_condition(numLess, X, Y), E) :-
   !,
   E = (X < Y).
d3_condition_prune(d3_condition(Op, X, Y), E) :-
   E =.. [Op, X, Y].


/******************************************************************/


