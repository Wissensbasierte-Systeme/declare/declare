

/******************************************************************/
/***                                                            ***/
/***       D3:  Diagnostic Reasoning                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* d3_diagnostic_reasoning_prolog <-
      */

d3_diagnostic_reasoning_prolog :-
   d3_knowledge_base_read(KB),
%  dentalog_knowledge_base(KB),
   d3_diagnostic_reasoning_prolog(KB).

d3_diagnostic_reasoning_prolog(KB) :-
   dislog_variable_get(
      output_path, 'd3_all_rules.pl', Rule_File),
   dislog_variable_get(
      source_path, 'projects/d3/d3_conditions.pl', Module_File),
   d3_diagnostic_knowledge_base_to_rule_file(KB, Rule_File),
   I1 = [d3_finding('Mf280', 100)],
%  I1 = [],

   d3_module:consult(Module_File),
   assert(d3_knowledge_base(KB)),
   tp_iteration_prolog_file(Rule_File, d3_module, I1, I2),
   retract(d3_knowledge_base(KB)),

   d3_findings_to_html(I2, Html),
   html_to_display('D3 Results', Html, size(300, 275)).


/* d3_diagnostic_knowledge_base_to_rule_file(KB, File) <-
      */

d3_diagnostic_knowledge_base_to_rule_file(KB, File) :-
   write(user, 'constructing rules ... '), ttyflush,
   findall( [d3_finding(indicated_question, Id)]-[true],
      Id := KB^'InitQASets'^'QContainer'@'ID',
      Rules_Q ),
   d3_knowledge_base_to_rules(KB, Rules_R),
   writeln(user, 'done'),
   Rule = [d3_finding(X, Y)]-[d3_finding(X, Y)],
%  Rule = [d3_finding(X, Y)]-
%     [(d3_finding(X, Y), writeln(user, d3_finding(X, Y)))],
   append([Rules_Q, Rules_R, [Rule]], Rules),
   d3_diagnostic_rules_to_file(Rules, File).


/******************************************************************/


