

/******************************************************************/
/***                                                            ***/
/***       D3:  Knowledge Base Slicing                          ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(d3:d3_knowledge_base_slicing, Diagnosis) :-
   Diagnosis = 'P181',
   home_directory(Home),
   concat(Home, '/research/projects/D3/', D3_Directory),
   concat(D3_Directory, 'SonoConsult.xml', File_1),
   concat(D3_Directory, 'SonoConsult.xml.extract', File_2),
   dread(xml, File_1, [KB_1]),
   d3_knowledge_base_slicing(KB_1, Diagnosis, KB_2),
   dwrite(xml, File_2, KB_2).

test(d3:d3_file_to_diagnosis_environment, Diagnosis) :-
   Diagnosis = 'P181',
   File = 'examples/d3/SonoConsult.xml',
   d3_file_to_diagnosis_environment(File, Diagnosis).

test(d3:d3_file_and_diagnosis_to_reaching_rule_edges, Diagnosis) :-
   Diagnosis = 'P181',
   File = 'examples/d3/SonoConsult.xml',
   d3_file_and_diagnosis_to_knowledge_base(File, Diagnosis, KB),
   d3_knowledge_base_to_reaching_rule_edges(KB, Diagnosis, Edges),
   d3_rule_goal_edges_to_picture(Edges).


/*** interface ****************************************************/


/* d3_file_and_finding_to_knowledge_slices(File, A:V, Slices) <-
      */

d3_file_and_finding_to_knowledge_slices(File, A:V, Slices) :-
   d3_file_and_diagnosis_to_knowledge_base(File, A, KB),
   d3_knowledge_base_to_reaching_rule_edges(KB, A:V, Es),
   d3_edges_to_special_nodes('R', Es, Rids),
   d3_knowledge_base_and_ids_to_knowledge_slices(
      KB, Rids, Slices).


/* d3_knowledge_base_and_ids_to_knowledge_slices(
         KB, Ids, Knowledge_Slices) <-
      */

d3_knowledge_base_and_ids_to_knowledge_slices(
      KB, Ids, Knowledge_Slices) :-
   findall( Knowledge_Slice,
      ( Knowledge_Slice := KB^'KnowledgeSlices'
           ^'KnowledgeSlice'::[@'ID'=Id],
        member(Id, Ids) ),
      Knowledge_Slices ).


/* d3_file_and_diagnosis_to_knowledge_base(
         File, Diagnosis, KB) <-
      */

d3_file_and_diagnosis_to_knowledge_base(File, Diagnosis, KB) :-
   file_name_extension(F, X, File),
   concat([F, '_', Diagnosis, '.', X], File_2),
   file_exists(File_2),
   !,
   dread(xml, File_2, [KB]).
d3_file_and_diagnosis_to_knowledge_base(File, Diagnosis, KB) :-
   dread(xml, File, [KB_2]),
   d3_knowledge_base_slicing(KB_2, Diagnosis, KB),
   file_name_extension(F, X, File),
   concat([F, '_', Diagnosis, '.', X], File_2),
   dwrite(xml, File_2, KB).


/* d3_knowledge_base_slicing(KB_1, Diagnosis, KB_2) <-
      */

d3_knowledge_base_slicing(KB_1, Diagnosis, KB_2) :-
   d3_knowledge_base_to_diagnosis_environment(
      KB_1, Diagnosis, Edges),
   d3_edges_to_special_nodes('Q', Edges, Qs_2),
   findall( Id,
      Id := KB_1^'InitQASets'^'QContainer'@'ID',
      Qs_3 ),
   append_and_sort([Qs_3, Qs_2], Qs),
   d3_edges_to_special_nodes('M', Edges, Ms),
   d3_edges_to_special_nodes('R', Edges, Rs),
   findall( QContainer,
      QContainer := KB_1^'QContainers'^'QContainer'::[
         @'ID'=Id, member(Id, Qs)],
      QContainers ),
   findall( Question,
      Question := KB_1^'Questions'^'Question'::[
         @'ID'=Id, member(Id, Ms)],
      Questions ),
   findall( KnowledgeSlice,
      KnowledgeSlice := KB_1^'KnowledgeSlices'
         ^'KnowledgeSlice'::[@'ID'=Id, member(Id, Rs)],
      KnowledgeSlices_1 ),
   findall( KnowledgeSlice,
      KnowledgeSlice := KB_1^'KnowledgeSlices'
         ^'KnowledgeSlice'::[
            ^'Action'^'TargetQASets'^'QASet'@'ID'=Id,
            member(Id, Qs)],
      KnowledgeSlices_2_a ),
   sort(KnowledgeSlices_2_a, KnowledgeSlices_2),
   append(KnowledgeSlices_1, KnowledgeSlices_2, KnowledgeSlices),
   d3_knowledge_base_to_init_qa_sets(KB_1, InitQASets),
   KB_2 = 'KnowledgeBase':[
      InitQASets,
      'QContainers':QContainers,
      'Questions':Questions,
      'KnowledgeSlices':KnowledgeSlices ].


/* d3_knowledge_base_to_init_qa_sets(KB, InitQASets) <-
      */

d3_knowledge_base_to_init_qa_sets(KB, InitQASets) :-
   InitQASets := KB^'InitQASets',
   !.
d3_knowledge_base_to_init_qa_sets(_, InitQASets) :-
   InitQASets = 'InitQASets':[].


/* d3_file_to_diagnosis_environment(File, Diagnosis) <-
      */

d3_file_to_diagnosis_environment(File, Diagnosis) :-
   d3_file_and_diagnosis_to_knowledge_base(File, Diagnosis, KB),
   d3_knowledge_base_to_diagnosis_environment(
      KB, Diagnosis, Edges),
   writeln(user, Edges),
   d3_rule_goal_edges_to_picture(Edges).


/* d3_file_and_diagnosis_to_rules_mult(File, Diagnoses, Rules) <-
      */

d3_file_and_diagnosis_to_rules_mult(File, Diagnoses, Rules) :-
   maplist( d3_file_and_diagnosis_to_rules(File),
      Diagnoses, List ),
   append_and_sort(List, Rules).


/* d3_file_and_diagnosis_to_rules(File, Diagnosis, Rules) <-
      */

d3_file_and_diagnosis_to_rules(File, Diagnosis, Rules) :-
   d3_file_and_diagnosis_to_knowledge_base(File, Diagnosis, KB),
   d3_knowledge_base_to_rule_edges(regular, KB, Edges),
   d3_edges_to_special_nodes('R', Edges, Rids),
   d3_knowledge_base_and_rule_ids_to_rules(KB, Rids, Rules).


/* d3_knowledge_base_to_relevant_rules(KB, Diagnosis, Rules) <-
      */

d3_knowledge_base_to_relevant_rules(KB, Diagnosis) :-
   d3_knowledge_base_to_relevant_rules(KB, Diagnosis, Rules),
   concat('Rules for ', Diagnosis, Title),
   view_rules_in_editor(Title, Rules).

d3_knowledge_base_to_relevant_rules(KB, Diagnosis, Rules) :-
   d3_knowledge_base_to_diagnosis_environment(
      KB, Diagnosis, Edges),
   d3_edges_to_special_nodes('R', Edges, Rids),
   d3_knowledge_base_and_rule_ids_to_rules(KB, Rids, Rules).


/* d3_knowledge_base_to_diagnosis_environment(
         KB, Diagnosis, Edges) <-
      */

d3_knowledge_base_to_diagnosis_environment(
      KB, Diagnosis, Edges) :-
   d3_knowledge_base_to_reaching_rule_edges(
      KB, Diagnosis, Rule_Edges),
   d3_knowledge_base_to_environment(KB, Rule_Edges, Edges).

d3_knowledge_base_to_environment(KB, Edges) :-
   d3_knowledge_base_to_rule_edges(regular, KB, Rule_Edges), 
   d3_knowledge_base_to_environment(KB, Rule_Edges, Edges).

d3_knowledge_base_to_environment(KB, Rule_Edges, Edges) :-
   d3_knowledge_base_to_question_container_edges(KB, Q_Edges),
   d3_knowledge_base_to_question_child_edges(KB, QC_Edges),
   d3_knowledge_base_to_question_indication_edges(KB, QI_Edges),
   writeln(user, QI_Edges),
%  QI_Edges = [],
   append([Q_Edges, QC_Edges, QI_Edges], QCI_Edges),
   edges_to_vertices(Rule_Edges, Vertices),
   sublist( d3_question_id,
      Vertices, Question_Ids ),
   reaching_edges_multiple(Question_Ids, QCI_Edges, Edges_2),
   append_and_sort([Rule_Edges, Edges_2], Edges).

d3_question_id(Qid) :-
   concat('M', _, Qid).


/******************************************************************/


