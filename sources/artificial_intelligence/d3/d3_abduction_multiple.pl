

/******************************************************************/
/***                                                            ***/
/***       D3:  Knowledge Base Abduction Multiple               ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      d3_finding_explanation/2.


/*** test *********************************************************/


test(d3:d3_file_to_explanations, 'EchoDOC') :-
   File = 'examples/d3/EchoDOC/EchoDOC.xml',
   dread(xml, File, [KB]),
   d3_knowledge_base_to_explanations(KB, Tree),
   predicate_to_file( 'results/d3_all_explanations_tree',
      dportray(tree, Tree) ),
   dportray(tree, Tree).

test(d3:d3_file_and_diagnosis_to_explanations, Diagnosis) :-
   File = 'examples/d3/SonoConsult/SonoConsult.xml',
   member(Diagnosis, ['P181', 'P169', 'P822']),
   d3_file_and_diagnosis_to_knowledge_base(
      File, Diagnosis, KB),
   d3_knowledge_base_to_explanations(KB, Tree),
   dportray(tree, Tree).


/*** interface ****************************************************/


/* d3_knowledge_base_to_explanations(KB, Tree) <-
      */

d3_knowledge_base_to_explanations(KB, Tree) :-
   retractall(d3_finding_explanation(_, _)),
   d3_knowledge_base_to_knowledge_slices(KB),
   d3_knowledge_base_to_finding_edges(KB, Edges),
   edges_to_ugraph(Edges, Graph),
   top_sort(Graph, Vertices),
   reverse(Vertices, Findings),
   dislog_variable_switch(d3_view_rules_in_editor, Mode, no),
   hidden(
      checklist( d3_finding_to_explanations,
         Findings ) ),
   dislog_variable_set(d3_view_rules_in_editor, Mode),
   findall( [[A=V], Tree],
      ( d3_finding_explanation(A=V, Tree),
        concat('P', _, A) ),
      Trees ),
   Tree_2 = [[]|Trees],
   set_tree_prune(Tree_2, Tree),
   !.


/* d3_finding_to_explanations(Finding) <-
      */

d3_finding_to_explanations(Finding) :-
   d3_finding_to_explanations(Finding, _).

d3_finding_to_explanations(Finding, Tree) :-
   d3_finding_to_equality(Finding, Node),
   write(user, Node), ttyflush,
   d3_finding_to_direct_explanations(Finding, Explanations),
   write(user, ' .'), ttyflush,
   ord_union(Explanations, Findings),
   coin_to_tree(Explanations, Tree_2),
   write(user, '.'), ttyflush,
   findall( [F]-T,
      ( member(F, Findings),
        d3_finding_explanation(F, T) ),
      Resolvents ),
   write(user, '.'), ttyflush,
   set_tree_resolvents_sort(Resolvents, Resolvents_2),
/*
   writeln_list(user, Resolvents_2),
   nth(5, Resolvents_2, _-R), tree_to_state(R, S),
   predicate_to_file( d3_abduction_multiple_tmp,
      ( dportray(tree, R),
        dportray(chs, S) ) ),
   writeq(Tree_2),
   dportray(tree, Tree_2),
   length(Resolvents, N), writeln(user, N),
   ttyflush,
*/
   set_tree_resolve(Resolvents_2, Tree_2, Tree_3),
   set_tree_minimize(Tree_3, Tree),
   writeln(user, '.'), ttyflush,
   !,
   ( Explanations = []
   ; assert(d3_finding_explanation(Node, Tree)) ).


/* d3_finding_to_direct_explanations(Finding, Explanations) <-
      */

d3_finding_to_direct_explanations(Finding, Explanations) :-
   findall( Rule,
      ( d3_knowledge_slice(Finding, Slice),
        d3_knowledge_slice_to_rule(Slice, Rule) ),
      Program ),
   elements_to_lists(Program, Programs),
   ( foreach(P, Programs), foreach(Ms, List_of_Models) do
        d3_program_and_finding_to_explanations(
           P, Finding, Ms) ),
   append_and_sort(List_of_Models, Models_2),
   state_can(Models_2, Models),
   maplist( d3_explanation_prune,
       Models, Explanations ).


/******************************************************************/


