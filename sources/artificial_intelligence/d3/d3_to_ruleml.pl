

/******************************************************************/
/***                                                            ***/
/***       D3:  Knowledge Base to RuleML                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


d3_file_to_ruleml :-
   File_1 = 'examples/d3/SonoConsult_P181.xml',
   File_2 = 'examples/d3/SonoConsult_P181.ruleml',
   d3_file_to_ruleml(File_1, File_2).


/*** implementation ***********************************************/


/* d3_file_to_ruleml(File_1, File_2) <-
      */

d3_file_to_ruleml(File_1, File_2) :-
   File_S = 'sources/projects/d3/d3_pl_xml_to_ruleml.fng',
   concat(File_1, '.pl', File_3),
   concat(File_3, '.xml', File_4),
   d3_file_to_prolog_file(File_1, File_3),
   program_file_to_xml(File_3, Xml),
   dwrite(xml, File_4, Xml),
   dread(xml, File_4, [Item_1]),
   fn_transform_fn_item_fng(File_S, Item_1, Item_2),
   dwrite(xml, File_2, Item_2).
 
   
/* d3_file_to_prolog_file(File_1, File_2) <-
      */

d3_file_to_prolog_file(File_1, File_2) :-
   dread(xml, File_1, [KB]),
   d3_knowledge_base_to_rules(KB, Rules),
   predicate_to_file( File_2,
      dportray(lp_2, Rules) ).


/******************************************************************/


