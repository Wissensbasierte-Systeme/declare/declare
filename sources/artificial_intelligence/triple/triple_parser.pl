

/******************************************************************/
/***                                                            ***/
/***       Triple:  Parser                                      ***/
/***                                                            ***/
/******************************************************************/


:- module( triple_parser, [
      parse/3 ] ).

:- dynamic
      op_term/4, un_op/4, bin_op/4.

:- discontiguous
      op_term/4, un_op/4, bin_op/4.


/*** interface ****************************************************/


% program

parse(program:Cs) -->
   sequence('*', triple_clause, Cs).


% clause

triple_clause(clause:['FORALL':Ts, T]) -->
   forall_quantifier(Ts), term(T), ['.'],
   { triple_write_trace(user, clause:['FORALL':Ts, T]), ! }.
triple_clause(C) -->
   triple_clause_block(C),
   { triple_write_trace(user, C), ! }.
triple_clause(clause:[T]) -->
   term(T), ['.'],
   { triple_write_trace(user, clause:[T]), ! }.

triple_write_trace(X, _) :-
   write(X, '.'), ttyflush.


% block

triple_clause_block(block:[Block]) -->
   simple_triple_clause_block(Block).
triple_clause_block(block:[at:[op:'@']:[Loc], Block]) -->
   ['@'], struct_term(Loc),
   simple_triple_clause_block(Block).
triple_clause_block(block:['FORALL':Ts, at:[op:'@']:[Loc], Block]) -->
   forall_quantifier(Ts), ['@'], struct_term(Loc),
   simple_triple_clause_block(Block).

simple_triple_clause_block(Cs) -->
   ['{'], sequence('*', triple_clause, Cs), ['}'].

forall_quantifier(Ts) -->
   ['FORALL'], id_term_seq(',', Ts).


% term

term(Op:[T]) -->
   un_op(1200, Op), formula(T).
term(term:[op:Op2]:[Op1:[T1], T2]) -->
   un_op(1200, Op1), formula(T1),
   bin_op(1200, Op2), formula(T2).
term(T) -->
   formula(T).
term(term:[op:Op]:[T1, T2]) -->
   formula(T1), bin_op(1200, Op), formula(T2).


% formula

formula(T) -->
   op_term(1100, T).

op_term(X, T) -->
   { member(X, [900, 1000, 1100]) },
   ['('], formula(T), [')'].

op_term(1100, T) -->
   op_term(1100, 1000, T).

op_term(1000, T) -->
   op_term(1000, 900, T).

op_term(900, Op:[T]) -->
   un_op(900, Op), op_term(900, T).
op_term(900, quant:[Op:Ts, T]) -->
   quant_op(900, Op), id_term_seq(',', Ts),
   op_term(900, T).
op_term(900, T) -->
   op_term(900, 700, T).

op_term(700, T) -->
   op_term(700, 680, T).
op_term(680, T) -->
   op_term(680, 661, T).
op_term(661, T) -->
   op_term(661, 500, T).

op_term(X, Y, Z) -->
   op_term(Y, T1),
   ( { Z = T1 }
   ; bin_op(X, Op),
     op_term(X, Y, T2),
     { Z = term:[op:Op]:[T1, T2] } ). 

op_term(500, T) -->
   op_term(400, T).
op_term(500, Op:[T]) -->
   un_op(500, Op), op_term(400, T).
op_term(500, Op:[T1, T2]) -->
   op_term(400, T1),
   bin_op(500, Op), op_term(500, T2).
op_term(500, Op2:[Op1:[T1], T2]) -->
   un_op(500, Op1), op_term(400, T1),
   bin_op(500, Op2), op_term(500, T2).

op_term(400, T) -->
   struct_term(T).
op_term(400, Op:[T1, T2]) -->
   struct_term(T1),
   bin_op(400, Op), op_term(400, T2).


struct_term(T) -->
   unit_term(X),
   sequence('*', arg_list_or_sb_arg_list, Xs),
   { Xs \= [], T = atom:[X|Xs]
   ; Xs == [], T = X }.

arg_list_or_sb_arg_list(X) -->
   ( arg_list(X)
   ; sb_arg_list(X) ).

unit_term(X) -->
   ( id_term(X)
   ; path_expression(X)
   ; integer(X) ).
unit_term(unit_term:[op:'()']:[X]) -->
   ['('], term(X), [')'].
unit_term(unit_term:[op:'[]']:[X]) -->
   ['<'], term(X), ['>'].

path_expression(path:[T|Ts]) -->
   id_term(T),
   id_term_seq('.', Ts).

id_term(T) -->
   ( integer(T)
   ; symbol(T)
   ; variable(T) ).
id_term(id:[op:':']:[T, X]) -->
   ( symbol(T)
   ; variable(T) ),
   [':'], id_term(X).

integer(integer:[X]) -->
   [X],
   { atomic(X), integer(X), ! }.
symbol(symbol:[X]) -->
   [X],
   { atomic(X),
     name(X, [Y|_]),
     ( Y = 39
     ; 97 =< Y, Y =< 122 ),
     ! }.
variable(variable:[X]) -->
   [X],
   { atomic(X), \+ member(X, ['[', ']', '(', ')']) }.


% sequences

arg_list(list:[op:'()']:Ts) -->
   ['('], term_seq(Ts), [')'].

sb_arg_list(list:[op:'[]']:Ts) -->
   ['['], term_seq(Ts), [']'].

term_seq([T]) -->
   op_term(900, T).
term_seq([T|Ts]) -->
   op_term(900, T), [','], term_seq(Ts).

id_term_seq(Separator, [T|Ts]) -->
   id_term(T), [Separator], id_term_seq(Separator, Ts).
id_term_seq(_, [T]) -->
   id_term(T).

id_term_seq(Seq) -->
   sequence('+', id_term, Seq).


% operators

un_op(1200, X) -->
   [X], { member(X, ['<-']) }.
bin_op(1200, X) -->
   [X], { member(X, ['<-', 'EQUIV', ':=']) }.
bin_op(1100, X) -->
   [X], { member(X, [';', 'OR']) }.
bin_op(1000, X) -->
   [X], { member(X, [',', 'AND']) }.
quant_op(900, X) -->
   [X], { member(X, ['FORALL', 'EXISTS']) }.
un_op(900, X) -->
   [X], { member(X, ['NOT', 'NEG', 'EXTERNAL']) }.
bin_op(900, X) -->
   [X], { member(X, ['->']) }.
bin_op(700, X) -->
   [X], { member(X, ['>=', '=', 'IS', '<', '>', '=<']) }.
bin_op(680, X) -->
   [X], { member(X, ['@']) }.
bin_op(661, X) -->
   [X], { member(X, ['']) }.
un_op(500, X) -->
   [X], { member(X, ['+', '-']) }.
bin_op(500, X) -->
   [X], { member(X, ['+', '-']) }.
bin_op(400, X) -->
   [X], { member(X, ['*', 'BY', 'INTERSECT', 'UNION', 'DIFF']) }.


/******************************************************************/


