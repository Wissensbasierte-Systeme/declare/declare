

/******************************************************************/
/***                                                            ***/
/***       Triple:  Translator - XML to Prolog                  ***/
/***                                                            ***/
/******************************************************************/


:- module( triple_xml_to_prolog, [
      triple_xml_to_prolog/0,
      triple_xml_to_prolog/2 ] ).

:- dynamic
      triple_abbreviation/2.


/*** interface ****************************************************/


/* triple_xml_to_prolog(File_1, File_2) <-
      */

triple_xml_to_prolog :-
   dislog_variable_get(example_path, Examples),
%  concat(Examples, 'triple/reasoner.xml', File_1),
%  concat(Examples, 'triple/reasoner.pl', File_2),
   concat(Examples, 'triple/personalization_reasoner_smart.xml', File_1),
   concat(Examples, 'triple/personalization_reasoner_smart.pl', File_2),
   triple_xml_to_prolog(File_1, File_2).

triple_xml_to_prolog(File_1, File_2) :-
   dread(xml, File_1, [Xml_1]),
   findall( triple_xml_to_prolog:triple_abbreviation(X, Y),
      ( T := Xml_1^clause^term::[@op=':='],
        [X] := T-nth(1)^symbol^content::'*',
        [Y] := T-nth(2)^symbol^content::'*',
        write_list(user, [X, ' := ', Y, '.\n']) ),
      Triple_Abbreviations ),
   assert_facts(Triple_Abbreviations),
   fn_item_transform(triple_xml_to_prolog:table, Xml_1, Rules),
   retractall(triple_xml_to_prolog:triple_abbreviation(_,_)),
   R1 = []-[discontiguous(statement/4)],
   R2 = [triple_exists_quantifier('_')],
   dislog_variable_get(output_path, Results),
   concat(Results, 'triple_xml_to_prolog.tmp', File),
   dportray(File, lp, [R1, R2|Rules]),
   program_file_to_xml(File, Xml_2),
   xml_to_prolog([Xml_2], File_2).


/*** implementation ***********************************************/


/* table(X, Y) <-
      */

table(X, Y) :-
   call(X ---> Y).


program:[]:Blocks ---> Rules :-
   findall( Rule,
      ( member(block:Rs, Blocks),
        member(Rule, Rs) ),
      Rules ).

block:[]:[at:[op:'@']:[L]|Rs_1] ---> block:Rs_2 :-
   findall( [A]-[B],
      ( member([H]-[B], Rs_1),
        concat_for_triple_2(H, L, A) ),
      Rs_2 ).

clause:[]:[forall:[]:_, R] ---> R.
clause:[]:[S] ---> [R] :-
   S =.. [_|Us],
   list_to_comma_structure(Us, C),
   comma_structure_to_list(C, [L|Xs]),
   writeln(user, L),
   findall( Y,
      ( member(X, Xs),
        concat_for_triple(X, L, Y) ),
      Ys ),
   list_to_comma_structure(Ys, R),
   writeln(user,  R).

atom:[]:[O, list:[op:'[]']:Tos] ---> Statement :-
   findall( statement(O, P, V),
      member(to(P, V), Tos),
      Statements ),
   list_to_comma_structure(Statements, Statement),
   writeln(user, Statement).

atom:[]:[O, list:[op:'()']:Es] ---> Atom :-
   Atom =.. [O|Es].

not:[]:[A] ---> not(A).

term:[op:'->']:[P, V] ---> to(P, V).
term:[op:'<-']:[H, B] ---> [H]-[B].
term:[op:'OR']:[X, Y] ---> (X;Y).
term:[op:'AND']:[X, Y] ---> (X,Y).
term:[op:'@']:[F, L] ---> A :-
   concat_for_triple(F, L, A).

% quant:[]:[exists:[]:_, F] ---> F.
quant:[]:[exists:[]:Xs, F] ---> (triple_exists_quantifier(Xs), F).

% id:[op:':']:[X1, Y1] ---> (X2:Y2) :-
%    resolve_triple_abbreviation(X1, X2),
%    resolve_triple_abbreviation(Y1, Y2).
id:[op:':']:[_, Y1] ---> Y2 :-
   resolve_triple_abbreviation(Y1, Y2).

variable:[]:['Learned'] ---> '\'Learned\'' :-
   !.
variable:[]:[V] ---> V.
symbol:[]:[S] ---> S.

T:As:Es ---> T:As:Es.


/* concat_for_triple(Atom_1, L, Atom_2) <-
      */

% concat_for_triple(Atom, _, Atom) :-
%    !.
concat_for_triple(Atom_1, L, Atom_2) :-
   Atom_1 =.. [P1|Args],
%  concat(P1, '___at', P2),
   P2 = P1, 
   Atom_2 =.. [P2, L|Args].

concat_for_triple_2(Atom, _, Atom).


/* resolve_triple_abbreviation(X, Y) <-
      */

resolve_triple_abbreviation(X, X) :-
   var(X),
   !.
resolve_triple_abbreviation(X, Y) :-
   triple_abbreviation(X, Y),
   !.
resolve_triple_abbreviation(X, X).


/******************************************************************/


