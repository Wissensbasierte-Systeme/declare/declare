

/******************************************************************/
/***                                                            ***/
/***       Triple:  Analysis                                    ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      concept/2, statement/4.

:- dynamic
      concept/2, statement/4.


/*** interface ****************************************************/


/* reasoner_triple_pose_query <-
      */

reasoner_triple_pose_query :-
   reasoner_triple_pose_query('Dietmar', 1, thislearningState).

reasoner_triple_pose_query(User, Page, For) :-
   retract_all(statement(query, _, _, _)),
   assert(statement(query, a, type, 'Query')),
   assert(statement(query, a, aboutUser, User)),
   assert(statement(query, a, currentPage, Page)),
   assert(statement(query, a, queryFor, For)).


/* prepare_for_triple_reasoning <-
      */

prepare_for_triple_reasoning :-
   dislog_variable_get(example_path, Examples),
   concat(Examples, 'triple/', Triple),
%  concat(Triple, 'reasoner.pl', Reasoner),
   concat(Triple, 'personalization_reasoner_smart.pl', Reasoner),
   concat(Triple, 'java_ontology.rdf', O),
   concat(Triple, 'sun_java_tutorial.rdf', T),
   retractall(concept(_, _)),
   retractall(statement(_, _, _, _)),
   consult(Reasoner),
   rdf_ontology_to_facts(java_ontology, O),
   rdf_ontology_to_facts(sun_tutorial, T).

rdf_ontology_to_facts(Domain, File) :-
   rdf_load(File),
   rdf_to_concepts(Domain, Concepts),
   assert_facts(Concepts),
   rdf_to_statements(Domain, Statements),
   assert_facts(Statements),
   rdf_unload(File).


/* rdf_to_concepts(Domain, Concepts) <-
      */

rdf_to_concepts(Domain, Concepts) :-
   findall( concept(Domain, Concept),
      ( rdf_subject(Subject),
        rdf_resource_to_triple_resource(Subject, Concept) ),
      Concepts ).


/* rdf_to_statements(Domain, Statements) <-
      */

rdf_to_statements(Domain, Statements) :-
   findall( Statement,
      ( rdf(R1, P1, O1),
        rdf_to_statement(Domain, rdf(R1, P1, O1), Statement) ),
      Statements ).

rdf_to_statement(Domain,
      rdf(R1, P1, O1), statement(Domain, R2, P2, O2)) :-
   rdf_resource_to_triple_resource(R1, R2),
   rdf_resource_to_triple_resource(P1, P2),
   rdf_resource_to_triple_resource(O1, O2).


/* rdf_resource_to_triple_resource(R, Id) <-
      */

rdf_resource_to_triple_resource(R, Id) :-
   atomic(R),
   name_split_at_position(["#"], R, [X, Y]),
   !,
   concat(X, '#', NS),
   O = Y,
   simplify_resource_identifier(NS:O, Id).
rdf_resource_to_triple_resource(literal(Id), Id) :-
   !.
rdf_resource_to_triple_resource(Id, Id).
   
% simplify_resource_identifier(NS:O, NS:O).
simplify_resource_identifier(_:O, O).


/******************************************************************/


