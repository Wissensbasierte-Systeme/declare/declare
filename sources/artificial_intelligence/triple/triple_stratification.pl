

/******************************************************************/
/***                                                            ***/
/***       Triple:  Pattern Stratification                      ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      concept/1, statement/3.


/*** examples *****************************************************/


/* triple_example_file_pattern_stratify <-
      */

triple_example_file_pattern_stratify :-
   dislog_variable(example_path, Examples),
   concat(Examples, 'triple/reasoner.pl', File_1),
   concat(File_1, '.strat', File_2),
   prolog_file_pattern_stratify(File_1, File_2).


/* triple_example_file_to_predicate_dependency_graph <-
      */

triple_example_file_to_predicate_dependency_graph :-
   dislog_variable(example_path, Examples),
   concat(Examples, 'triple/reasoner.triple.xml', File),
   triple_program_xml_to_predicate_dependency_graph(File).


/*** interface ****************************************************/


/* prolog_file_pattern_stratify(File_1, File_2) <-
      */

prolog_file_pattern_stratify(File_1, File_2) :-
   prolog_file_to_pattern_partition(File_1, Partition),
   partition_program_file_to_file(Partition, File_1, File_2).


/* prolog_file_to_pattern_partition(File, Partition) <-
      Partition is a list of lists of patterns. */

prolog_file_to_pattern_partition(File, Partition) :-
   prolog_program_coarsen_for_rgg(File, Prog_1),
   program_to_pattern_program(Prog_1, Prog_2),
   pattern_program_to_rule_goal_graph_xpce(Prog_2, Prog_3),
   !,
   stratify_program(Prog_3, Partition, _).


/* partition_program_file_to_file(Partition, File_1, File_2) <-
      */

partition_program_file_to_file(Partition, File_1, File_2) :-
   program_file_to_xml(File_1, Program_Xml),
   partition_program_xml_to_xml(
      Partition, Program_Xml, Strata_Xml),
   program_strata_xml_to_file(Strata_Xml, File_2).


/* triple_program_xml_to_predicate_dependency_graph(File) <-
      */

triple_program_xml_to_predicate_dependency_graph(File) :-
   triple_program_xml_to_predicate_dependency_graph(
      File, Edges),
   graph_edges_to_picture(Edges),
   edges_to_ugraph(Edges, Graph),
   reduce(Graph, Reduced_Graph),
   writeln(user, Reduced_Graph),
   top_sort(Reduced_Graph, Components),
   writeln(user, Components).


/*** implementation ***********************************************/


/* prolog_program_coarsen_for_rgg(File, Rules) <-
      */

prolog_program_coarsen_for_rgg(File, Rules) :-
   read_term_loop_parse(File, Rules_2),
   findall( Rule,
      ( member(Rule_2, Rules_2),
        prolog_rule_coarsen_for_rgg(Rule_2, Rule) ),
      Rules ).

prolog_rule_coarsen_for_rgg([F]-(:-)-Fs-_, Head-Pos-Neg) :-
   formula_to_literals(F, Head),
   maplist( formula_to_literals,
      Fs, List_Ls2),
   append(List_Ls2, Ls2),
   sublist( positive_literal,
      Ls2, Pos ),
   sublist( negative_literal,
      Ls2, Ls3 ),
   negate_general_clause(Ls3, Neg).

positive_literal(A) :-
   \+ A =.. [~|_].

negative_literal(~_).


/* formula_to_literals(Formula, Literals) <-
      */

formula_to_literals(Formula, Literals) :-
   Formula =.. [Junctor, F1, F2],
   member(Junctor, [';', ',']),
   !,
   formula_to_literals(F1, Ls1),
   formula_to_literals(F2, Ls2),
   append(Ls1, Ls2, Literals).
formula_to_literals(Formula, Literals) :-
   Formula =.. [not, F],
   !,
   formula_to_literals(F, Ls),
   findall( ~A,
      member(A, Ls),
      Literals ).
formula_to_literals(Atom, [Atom]).


/* program_to_pattern_program(Program_1, Program_2) <-
      */

program_to_pattern_program(Program_1, Program_2) :-
   maplist( rule_to_pattern_rule,
      Program_1, Program_2 ).

rule_to_pattern_rule(Rule_1, Rule_2) :-
   parse_dislog_rule(Rule_1, As1, Bs1, Cs1),
   atoms_to_pattern_atoms(As1, As2),
   atoms_to_pattern_atoms(Bs1, Bs2),
   atoms_to_pattern_atoms(Cs1, Cs2),
   parse_dislog_rule(Rule_2, As2, Bs2, Cs2).

atoms_to_pattern_atoms(Atoms_1, Atoms_2) :-
   maplist( atom_to_pattern_atom,
      Atoms_1, Atoms_2 ).


/* atom_to_pattern_atom(Atom_1, Atom_2) <-
      */
   
atom_to_pattern_atom(Atom_1, Atom_2) :-
   Atom_1 =.. [P|As_1],
   maplist( argument_to_pattern_argument,
      As_1, As_2 ),
   Term =.. [P|As_2],
   term_to_atom(Term, Atom_2).
   
argument_to_pattern_argument(X, '?') :-
   var(X),
   !.
argument_to_pattern_argument(X, Y) :-
   is_list(X),
   !,
   maplist( argument_to_pattern_argument,
      X, Y ).
argument_to_pattern_argument(X, X).


/* pattern_program_to_rule_goal_graph_xpce(Prog_1, Prog_2) <-
      */

pattern_program_to_rule_goal_graph_xpce(Prog_1, Prog_2) :-
   dislog_program_to_rule_goal_graph(Prog_1, [Vs, Es_1]),
   vertices_to_more_general_edges_and_rules(Vs, Es_2, Prog),
   append(Es_1, Es_2, Es),
%  vertices_edges_to_picture_bfs(Vs, Es),
   vertices_edges_to_picture(Vs, Es),
   append(Prog_1, Prog, Prog_2).

vertices_to_more_general_edges_and_rules(
      Vertices, Edges, Rules) :-
   findall( V-W-blue,
      ( member(V-V-_-_-_, Vertices),
        member(W-W-_-_-_, Vertices), V \= W,
        term_to_atom(A, V),
        term_to_atom(B, W),
        atom_is_more_general_than_atom(B, A) ),
      Edges ),
   findall( [V]-[W],
      member(V-W-blue, Edges),
      Rules ).


/* atom_is_more_general_than_atom(Atom_1, Atom_2) <-
      */

atom_is_more_general_than_atom(Atom_1, Atom_2) :-
   atom_to_atom_with_underscores(Atom_1, Atom),
   !,
   Atom = Atom_2.

atom_to_atom_with_underscores(Atom_1, Atom_2) :-
   Atom_1 =.. [P|As1],
   maplist( question_mark_argument_to_underscore,
      As1, As2 ),
   Atom_2 =.. [P|As2].

question_mark_argument_to_underscore('?', _) :-
   !.
question_mark_argument_to_underscore(X, X).


/* partition_program_xml_to_xml(Partition,
         Program_Xml, List_Rules_Xml) <-
      */

partition_program_xml_to_xml(Partition,
      Program_Xml, List_Rules_Xml) :-
   maplist( program_to_program_wrt_partition(Program_Xml),
      Partition, List_Rules_Xml ).

program_to_program_wrt_partition(
      Program_Xml, Atoms, Rules_Xml) :-
   writeln(user, Atoms),
   findall( Rule,
      ( Rule := Program_Xml^rule,
        A := Rule^head^atom,
        xml_atom_to_prolog_atom(A, B),
        atom_to_pattern_atom(B, C),
        member(C, Atoms) ),
      Rules_Xml ).


/* program_strata_xml_to_file(Strata_Xml, File) <-
      */

program_strata_xml_to_file(Strata_Xml, File) :-
   write_list(user, ['---> ', File, '\n']),
   predicate_to_file( File,
      ( checklist( xml_rules_to_prolog_with_starline,
           Strata_Xml ),
        star_line ) ).

xml_rules_to_prolog_with_starline([]) :-
   !.
xml_rules_to_prolog_with_starline(Rules_Xml) :-
   star_line,
   xml_to_prolog:xml_rules_to_prolog(Rules_Xml).


/* triple_program_xml_to_predicate_dependency_graph(
         File, Edges) <-
      */

triple_program_xml_to_predicate_dependency_graph(
      File, Edges) :-
   dread(xml, File, [Xml]),
   findall( V-W,
      ( Rule := Xml^_^term::[@op='<-'],
        A1 := Rule-nth(1)^atom,
        A2 := Rule-nth(2)^_,
        triple_xml_term_to_predicate(A1, V),
        triple_xml_term_to_predicate(A2, W) ),
      Edges_2 ),
   sort(Edges_2, Edges).

triple_xml_term_to_predicate(Atom, Predicate) :-
   fn_item_parse(Atom, atom:_:_),
   '[]' := Atom^list@op,
   !,
   Predicate = statement.
triple_xml_term_to_predicate(Atom, Predicate) :-
   fn_item_parse(Atom, atom:_:_),
   '()' := Atom^list@op,
   !,
   [Predicate] := Atom^symbol^content::'*'.
triple_xml_term_to_predicate(Formula, Predicate) :-
   Atom := Formula^_^atom,
   triple_xml_term_to_predicate(Atom, Predicate).


/******************************************************************/


