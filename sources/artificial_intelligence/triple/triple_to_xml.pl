

/******************************************************************/
/***                                                            ***/
/***       Triple:  Translator - Triple to XML                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* triple_file_to_xml(File, Xml) <-
      */

triple_file_to_xml :-
   dislog_variable(example_path, Examples),
%  concat(Examples, 'triple/reasoner.triple', File),
%  concat(Examples, 'triple/reasoner.xml', File_3),
   concat(Examples, 'triple/personalization_reasoner_smart.triple', File),
   concat(Examples, 'triple/personalization_reasoner_smart.xml', File_3),
   concat(File, '.xml', File_2),
   triple_file_to_xml(File, Xml),
   dwrite(xml, File_2, Xml),
   dread(xml, File_2, [Xml_2]),
   triple_transform_xml_item(Xml_2, Xml_3),
   dwrite(xml, File_3, Xml_3).

triple_file_to_xml(File, Xml) :-
   triple_file_to_tokens(File, Names),
   triple_parser:parse(Xml, Names, []).


/* triple_file_to_tokens(File, Tokens) <-
      */

triple_file_to_tokens(File, Tokens) :-
   read_file_to_string(File, String_1),
   list_eliminate_prolog_comments(String_1, String_2),
   name(N2, String_2),
   name_exchange_sublist([
      [[9], " "], ["->", " -> "],
      ["{", " { "], ["}", " } "],
      ["(", " ( "], [")", " ) "],
      ["[", " [ "], ["]", " ] "],
      [":", " : "], [".\n", " . "], [". ", " . "],
      [",", " , "], ["@", " @ "]], N2, N3),
   name_split_at_position(["\n", " "], N3, Ns3),
   list_remove_elements([''], Ns3, Ns4),
   list_exchange_sublist([
      [[':', '='], [':=']],
      ['\'http', ':', _]-concat_brackets ], Ns4, Tokens).

concat_brackets(Xs, [Y]) :-
   concat(Xs, Y).


/*** implementation ***********************************************/


/* name_eliminate_prolog_comments(Name_1, Name_2) <-
      */

name_eliminate_prolog_comments(Name_1, Name_2) :-
   name(Name_1, List_1),
   list_eliminate_prolog_comments(List_1, List_2),
   name(Name_2, List_2).

list_eliminate_prolog_comments(Xs_1, Xs_2) :-
   append("/*", Xs_3, Xs_1),
   !,
   list_eliminate_in_prolog_comment(Xs_3, Xs_2).
list_eliminate_prolog_comments([X|Xs1], [X|Xs2]) :-
   list_eliminate_prolog_comments(Xs1, Xs2).
list_eliminate_prolog_comments([], []).
   
list_eliminate_in_prolog_comment(Xs_1, Xs_2) :-
   append("*/", Xs_3, Xs_1),
   !,
   list_eliminate_prolog_comments(Xs_3, Xs_2).
list_eliminate_in_prolog_comment([_|Xs_1], Xs_2) :-
   list_eliminate_in_prolog_comment(Xs_1, Xs_2).


/******************************************************************/


