

/******************************************************************/
/***                                                            ***/
/***       Triple:  Translator - XML to XML                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* triple_transform_xml_item(Item_1, Item_2) <-
      */

triple_transform_xml_item(Item_1, Item_2) :-
   fn_item_parse(Item_1, block:As:Es_1),
   !,
   write(user, [block]), ttyflush,
   Loc := Item_1^at^id,
   maplist( triple_transform_xml_item(Loc),
      Es_1, Es_2 ),
   Item_2 = block:As:Es_2.
triple_transform_xml_item(Item_1, Item_2) :-
   fn_item_parse(Item_1, T:As:Es_1),
   !,
   write(user, [T]), ttyflush,
   maplist( triple_transform_xml_item,
      Es_1, Es_2 ),
   Item_2 = T:As:Es_2.
triple_transform_xml_item(Item, Item).


/* triple_transform_xml_item(Loc, Item_1, Item_2) <-
      */

triple_transform_xml_item(_, Item, Item) :-
   fn_item_parse(Item, term:[op:'@']:_),
%  write(user, [term]), ttyflush,
   !.
triple_transform_xml_item(Loc, Item_1, Item_2) :-
   fn_item_parse(Item_1, atom:_:_),
%  write(user, [atom]), ttyflush,
   !,
   Item_2 = term:[op:'@']:[Item_1, Loc].
triple_transform_xml_item(Loc, Item_1, Item_2) :-
   fn_item_parse(Item_1, T:As:Es_1),
%  write(user, [T]), ttyflush,
   !,
   maplist( triple_transform_xml_item(Loc),
      Es_1, Es_2 ),
   Item_2 = T:As:Es_2.
triple_transform_xml_item(_, Item, Item).


/******************************************************************/


