

/******************************************************************/
/***                                                            ***/
/***       SWRL:  Refactoring                                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* swrl_anomaly(Type, Anomaly) <-
      */

swrl_anomaly(Anomaly) :-
   swrl_anomaly(_, Anomaly),
   dwrite(xml, Anomaly).
   
swrl_anomaly(implication_of_subclass, Anomaly) :-
   ruleml_rule(Rule),
   C1 := Rule^'_body'^atom,
   C2 := Rule^'_head'^atom,
   swrl_isa(C1, C2),
   Anomaly =
      swrl_anomaly:[type:implication_of_subclass]:[
         isa:[C1, C2], Rule ].

swrl_anomaly(implication_of_equivalence, Anomaly) :- 
   ruleml_rule(Rule),
   C1 := Rule^'_body'^atom,
   C2 := Rule^'_head'^atom,
   swrl_equiv(C1, C2),
   Anomaly =
      swrl_anomaly:[type:implication_of_equivalence]:[
         equiv:[C1, C2], Rule ].

swrl_anomaly(equivalence_in_antecedent, Anomaly) :-
   ruleml_rule(Rule),
   C1 := Rule^'_body'^_^atom,
   C2 := Rule^'_body'^_^atom,
   C1 \= C2,
   swrl_equiv(C1, C2),
   Anomaly =
      swrl_anomaly:[type:equivalence_in_antecedent]:[
         isa:[C1, C2], Rule ].


/*** implementation ***********************************************/


/* swrl_atom_to_arity(Atom, N) <-
      */

swrl_atom_to_arity(Atom, N) :-
   Xs := Atom^content::'*',
   !,
   length(Xs, N).
   

/* swrl_isa(C1, C2) <-
      */

swrl_isa(C1, C2) :-
   rule_ml_atom_to_prolog_atom(C1, A1),
   rule_ml_atom_to_prolog_atom(C2, A2),
   isa(A1, A2).


/* swrl_equiv(C1, C2) <-
      */

swrl_equiv(C1, C2) :-
   rule_ml_atom_to_prolog_atom(C1, A1),
   rule_ml_atom_to_prolog_atom(C2, A2),
   equiv(A1, A2).


/* rule_ml_atom_to_prolog_atom(Atom_Swrl, Atom_Prolog) <-
      */

rule_ml_atom_to_prolog_atom(Atom_Swrl, Atom_Prolog) :-
   [P] := Atom_Swrl^'_opr'^rel^content::'*',
   findall( X,
      [X] := Atom_Swrl^ind^content::'*',
      Xs ),
   Atom_Prolog =.. [P|Xs].


/* ruleml_rule(Rule) <-
      */

ruleml_rule(Rule) :-
   ruleml_data(Xml),
   Rule := Xml^imp.

ruleml_data(Xml) :-
   dread(xml, 'examples/d3/SonoConsult_P181.ruleml', [Xml]).


/*** data *********************************************************/


isa(
   d3_finding(indicated_question, 'Q000'),
   d3_finding(indicated_question, 'Qcl49') ).
equiv(
   d3_finding(indicated_question, 'Q000'),
   d3_finding(indicated_question, 'Q483') ).
equiv(
   d3_condition(numEqual, 'Mf280', '126'),
   d3_condition(numGreater, 'Mf280', '126') ).


/******************************************************************/


