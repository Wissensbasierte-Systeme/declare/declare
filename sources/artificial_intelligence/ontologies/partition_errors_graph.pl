

/******************************************************************/
/***                                                            ***/
/***       Ontologies:  Partition Error Graphs                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


partition_errors_to_picture(Errors) :-
   findall( Vertex,
      ( member(X1->[X2, X3], Errors),
        member(Vertex, [X1, X2, X3]) ),
      Vertices_2 ),
   sort(Vertices_2, Vertices),
   findall( X-Y-red,
      ( member(_->[X2, X3], Errors),
        ( X-Y = X2-X3
        ; X-Y = X3-X2 ) ),
      Edges_Disjoint ),
   findall( X1-X-blue,
      ( member(X1->[X2, X3], Errors),
        member(X, [X2, X3]) ),
      Edges_Subclass ),
   findall( X1-X2-green,
      subClassOf_minimal(Vertices, X1, X2),
      Edges_subClassOf_minimal ),
%  writeln(user, Edges_subClassOf_minimal),
   append([Edges_Disjoint, Edges_Subclass,
      Edges_subClassOf_minimal], Edges),
   vertices_edges_to_picture(Vertices, Edges).

subClassOf_minimal(Vertices, C1, C2) :-
   member(C1, Vertices),
   member(C2, Vertices),
   C1 \= C2,
   ontology_subClassOf(C1, C2),
   \+ ( isa(C1, C3),
        member(C3, Vertices),
        ontology_subClassOf(C3, C2) ).


/*** tests ********************************************************/


test(ontologies, partition_errors) :-
   Errors = [
      'P1024'->['P169', 'P574'],
      'P1025'->['P169', 'P574'] ],
   partition_errors_to_picture(Errors).


/******************************************************************/


