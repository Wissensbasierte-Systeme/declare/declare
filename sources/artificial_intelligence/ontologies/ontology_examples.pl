

/******************************************************************/
/***                                                            ***/
/***       Ontologies:  Examples                                ***/
/***                                                            ***/
/******************************************************************/


/* owl_current_ontology(Ontology) <-
      */

% owl_current_ontology('Examples/printer.owl').
% owl_current_ontology('Examples/printer_invalid.owl').
% owl_current_ontology('Examples/printer_res.owl').
% owl_current_ontology('Examples/printer_ext_invalid.owl').

% owl_current_ontology('Examples/teaching.owl').
% owl_current_ontology('Examples/quantifiers.owl').

% owl_current_ontology('Examples/MediConsult.owl').
% owl_current_ontology('Examples/SonoConsult_Diagnoses.owl').

  owl_current_ontology('Rewerse/ResearcherOntology.owl').


/* owl_current_ontology_path(Path) <-
      */

owl_current_ontology_path(Path) :-
   dislog_variable_get(home, DisLog),
   concat([DisLog, '/projects/Ontologies/'], Path).
   

/******************************************************************/


