

/******************************************************************/
/***                                                            ***/
/***       SWRL:  Family Ontology                               ***/
/***                                                            ***/
/******************************************************************/


family_ontology(Swrl) :-
   dislog_variable_get(home, DisLog),
   concat(DisLog,
      '/projects/Ontologies/Protege/family.swrl.owl', Path),
   dread(xml, Path, [Swrl]).


/*** implementation ***********************************************/


/* swrl_to_prolog_rules(Swrl, Rules) <-
      */

swrl_to_prolog_rules(Rules) :-
   family_ontology(Swrl),
   swrl_to_prolog_rules(Swrl, Rules),
   dportray(lp_gui, Rules).

swrl_to_prolog_rules(Swrl, Rules) :-
   findall( Rule,
      swrl_to_prolog_rule(Swrl, Rule),
      Rules ).

swrl_to_prolog_rule(Swrl, Head-Body) :-
   Rule := Swrl^'swrl:Imp',
   H := Rule^'swrl:head',
   swrl_to_prolog_atoms(H, Head),
   B := Rule^'swrl:body',
   swrl_to_prolog_atoms(B, Body).

swrl_to_prolog_atoms(Swrl, Atoms) :-
   findall( A,
      ( A := Swrl^_^'swrl:IndividualPropertyAtom'
      ; A := Swrl^_^'swrl:ClassAtom' ),
      As ),
   maplist( swrl_atom_to_prolog_atom,
      As, Atoms ).


/* swrl_atom_to_prolog_atom(Swrl, Prolog) <-
      */

swrl_atom_to_prolog_atom(Swrl, Prolog) :-
   ( Pred := Swrl^'swrl:propertyPredicate'@'rdf:resource'
   ; Pred := Swrl^'swrl:classPredicate'@'rdf:resource' ),
   rdf_resource_short(Pred, P),
   Xs := Swrl^content::'*',
   findall( A,
      ( member(T:As:[], Xs),
        concat('swrl:argument', _, T),
        Arg := (T:As:[])@'rdf:resource',
        rdf_resource_short(Arg, A) ),
      Arguments ),
   Prolog =.. [P|Arguments].


/******************************************************************/


