

/******************************************************************/
/***                                                            ***/
/***       Ontologies:  Owl to Rules                            ***/
/***                                                            ***/
/******************************************************************/


class(C) :-
   isa(C1, C2),
   member(C, [C1, C2]).

subClassOf(C1, C2) :-
   rdf_subject(Class_1),
   rdf_has(Class_1, rdfs:subClassOf, Class_2)
   \+ owl_restriction(Class_1),
   \+ owl_restriction(Class_2),
   Class_1 \= Class_2,
   rdf_resource_short(Class_1, C1),
   rdf_resource_short(Class_2, C2).

isa(C1, C2) :-
   rdf(Class_1, rdfs:subClassOf, Class_2),
   \+ owl_restriction(Class_1),
   \+ owl_restriction(Class_2),
   Class_1 \= Class_2,
   rdf_resource_short(Class_1, C1),
   rdf_resource_short(Class_2, C2).

disjointWith(C1, C2) :-
   rdf_subject(Class_1),
   rdf(Class_1, owl:disjointWith, Class_2),
   \+ owl_restriction(Class_1),
   \+ owl_restriction(Class_2), 
   Class_1 \= Class_2,
   rdf_resource_short(Class_1, C1),
   rdf_resource_short(Class_2, C2).

property(P, S, O) :-
   rdf(Subject, Predicate, Object),
   rdf_subject(Predicate),
   rdf_resource_short(Subject, S),
   rdf_resource_short(Predicate, P),
   rdf_resource_short(Object, O).

instanceOf(R, C) :-
   rdf_subject(Resource),
   rdf_has(Resource, rdf:type, Class),
   rdf_resource_short(Class, C),
   class(C),
   rdf_resource_short(Resource, R).


/******************************************************************/


