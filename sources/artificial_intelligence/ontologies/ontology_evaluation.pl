

/******************************************************************/
/***                                                            ***/
/***       Ontologies:  Evaluation                              ***/
/***                                                            ***/
/******************************************************************/


/* sport_ontology(Mode) <-
      */

sport_ontology(Mode) :-
   member(Mode, [load, unload]),
   dislog_variable_get(example_path, E),
   concat(E, 'smelly_owls/Sportauswahl.owl', Path),
   concat(rdf_, Mode, Predicate),
   apply(Predicate, [Path]).


/*** interface ****************************************************/


/* owl_ontology_report(File) <-
      */

owl_ontology_report :-
   dislog_variable_get(example_path, E),
%  concat(E, 'smelly_owls/Sportauswahl.owl', File),
   concat(E, 'ontologies/Sprache2.owl', File),
   owl_ontology_report(File).

owl_ontology_report(File) :-
   rdf_load(File),
   owl_ontology_to_program(Rules),
   dportray(lp_gui, Rules),
   rdf_unload(File).


/* owl_ontology_evaluation(File) <-
      */

owl_ontology_evaluation :-
   dislog_variable_get(example_path, Ex),
   concat(Ex, 'smelly_owls/Sportauswahl.owl', File),
   owl_ontology_evaluation(File, _).

owl_ontology_evaluation(File, Rules) :-
   rdf_load(File),
   owl_ontology_evaluation_program(Rules_1),
   owl_ontology_to_program(Rules_2),
%  owl_ontology_to_rules(class, Facts_1),
%  owl_ontology_to_rules(isa, Facts_2),
%  owl_ontology_to_rules(disjointWith, Facts_3),
%  append([Facts_1, Facts_2, Facts_3, Rules_1], Program),
   append(Rules_2, Rules_1, Program),
   perfect_models(Program, [Model]),
   list_of_elements_to_relation(Model, Facts_4),
   append(Rules_2, Facts_4, Rules_c),
   sort(Rules_c, Rules),
   ontology_rules_visualize(Rules),
   dportray(lp_gui, Facts_4),
   rdf_unload(File).


/* dportray(lp_gui, Rules) <-
      */

dportray(lp_gui, Rules) :-
   dportray(lp_gui(File), Rules),
   file_view_for_gui(File).

dportray(lp_gui(File), Rules) :-
   dislog_variable_get(output_path, Results),
   concat(Results, logic_program, File),
   predicate_to_file( File,
      dportray(lp_xml, Rules) ).


/* owl_ontology_evaluation_program(Program) <-
      */

owl_ontology_evaluation_program([
   [C=C]-[class(C)],
   [instanceOf(X, C2)]-[instanceOf(X, C1), subClassOf(C1, C2)],
   [subClassOf(C1, C2)]-[isa(C1, C2)],
   [subClassOf(C1, C2)]-[isa(C1, C), subClassOf(C, C2)],
   [circularity(C1, C2)]-[isa(C1, C2), subClassOf(C2, C1)],
   [partition_error(C, C1, C2)]-
      [disjointWith(C1, C2), subClassOf(C, C1), subClassOf(C, C2)],
   [incompleteness(C, C1, C2, C3)]-
      [isa(C1, C), isa(C2, C), isa(C3, C), disjointWith(C1, C2)]-
      [disjointWith(C2, C3), C1=C2, C2=C3, C1=C3],
   [redundant_isa(C1->C2->C3)]-
      [isa(C1, C3), subClassOf(C3, C2), isa(C1, C2)]-
      [subClassOf(C2, C3)]]).


/* owl_class_hierarchy_to_picture <-
      */

owl_class_hierarchy_to_picture :-
   owl_ontology_to_rules(isa, Facts),
   ( foreach([isa(X, Y)], Facts), foreach(X-Y, Edges) do
        true ),
   edges_to_picture(Edges).
%  edges_to_transitive_reduction(Edges, Edges_2),
%  findall( X-Y,
%     member([X]-[Y], Edges_2),
%     Edges_3 ),
%  edges_to_picture(Edges_2).


/* ontology_rules_visualize(Rules) <-
      */

ontology_rules_visualize(Rules) :-
   findall( C1-C2,
      member([isa(C1, C2)], Rules),
      Edges_2 ),
   findall( X-C-blue,
      ( ( member([X:C], Rules)
        ; member([instanceOf(X, C)], Rules) ),
        \+ X = (_, _) ),
      Edges_3 ),
   findall( C1-C2-orange,
      ( member([disjointWith(C1, C2)], Rules)
      ; member([disjointWith(C2, C1)], Rules) ),
      Edges_4 ),
%  Edges_4 = [],
   findall( C1-C2-green,
      member([partition_error(_, C1, C2)], Rules),
      Edges_5 ),
   append([Edges_2, Edges_3, Edges_4, Edges_5], Edges),
   edges_to_picture(Edges).


/******************************************************************/


