

/******************************************************************/
/***                                                            ***/
/***       SWRL:  Sumo Ontology                                 ***/
/***                                                            ***/
/******************************************************************/


sumo_ontology(Sumo) :-
   dislog_variable_get(home, DisLog),
   concat(DisLog,
      '/projects/Ontologies/Protege/SUMO.xml', Path),
   dread(xml, Path, [Sumo]).


/*** interface ****************************************************/


/* sumo_to_prolog_rules(Sumo, Rules) <-
      */

sumo_to_prolog_rules(Rules) :-
   sumo_ontology(Sumo),
   sumo_to_prolog_rules(Sumo, Rules),
   dportray(lp_gui, Rules).

sumo_to_prolog_rules(Sumo, Rules) :-
   findall( Rule,
      sumo_to_prolog_rule(Sumo, Rule),
      Rules ).

sumo_to_prolog_rule(Sumo, Head-Body) :-
   [H, B] := Sumo^implies^content::'*',
   sumo_to_prolog_atoms(H, Head),
   sumo_to_prolog_atoms(B, Body).

sumo_to_prolog_atoms(Sumo, Atoms) :-
   findall( A,
      ( A := Sumo,
        _ := Sumo^predicate
      ; A := Sumo^clause::[^predicate=_]
      ; A := Sumo^_^clause::[^predicate=_] ),
      As ),
   maplist( sumo_atom_to_prolog_atom,
      As, Atoms ).


/* sumo_atom_to_prolog_atom(Sumo, Prolog) <-
      */

sumo_atom_to_prolog_atom(Sumo, Prolog) :-
%  star_line,
%  writeln(user, Sumo),
%  star_line,
   ( P := Sumo^predicate@value
   ; P := Sumo^function@value ),
   !,
   findall( A,
      ( [B] := Sumo^argument^content::'*',
        var_name_simplify(B, A)
      ; B := Sumo^clause,
        sumo_atom_to_prolog_atom(B, A) ),
      As ),
   Prolog =.. [P|As].
sumo_atom_to_prolog_atom(Sumo, Sumo).

var_name_simplify(var:[name:X]:[], v(X)) :-
   !.
var_name_simplify(X, X).


/******************************************************************/


