

/******************************************************************/
/***                                                            ***/
/***       Triple20:  Front End                                 ***/
/***                                                            ***/
/******************************************************************/


file_search_path(triple20, Triple20) :-
   dislog_variable_get(home, DisLog),
   concat(DisLog, '/library/triple20', Triple20).

% :- use_module(triple20(load)).

:- file_search_path(triple20, Triple20),
   concat(Triple20, '/load.pl', Load),
   [Load].


/*** interface ****************************************************/


/* rdfs_explorer <-
      */

rdfs_explorer :-
   new(X, rdfs_explorer),
   send(X, open).


/******************************************************************/


