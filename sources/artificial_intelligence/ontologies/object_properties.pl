

/******************************************************************/
/***                                                            ***/
/***       Ontologies:  Visualization of Properties             ***/
/***                                                            ***/
/******************************************************************/


 
/*** interface ****************************************************/


/* owl_to_property_graphs(Owl) <-
      */

owl_to_property_graphs(Owl) :-
   owl_to_property_graph(Owl, 'owl:ObjectProperty'),
   owl_to_property_graph(Owl, 'owl:DatatypeProperty').

owl_to_property_graph(Owl, Tag) :-
   findall( Property,
      owl_to_property(Owl, Tag, Property),
      Properties ),
   writeln_list(user, Properties),
   length(Properties, N),
   writeln(user, N),
   maplist( object_property_to_vertices,
      Properties, List_Vertices ),
   append(List_Vertices, Vertices_2 ),
   sort(Vertices_2, Vertices),
   maplist( object_property_to_edges,
      Properties, List_Edges ),
   append(List_Edges, Edges ),
   vertices_edges_to_picture(Vertices, Edges).

 
/*** implementation ***********************************************/


/* owl_to_property(Owl, Tag, Property) <-
      Tag can be
       - 'owl:ObjectProperty' or
       - 'owl:DatatypeProperty' */

owl_to_property(Owl, Tag, Property) :-
   P := Owl^Tag::[ member(A, ['rdf:about', 'rdf:ID']),
      @A=N, owl_reference_to_id(N, Name) ], 
   rdfs_domain_or_range_to_classes(P, 'rdfs:domain', Cs_1),
   rdfs_domain_or_range_to_classes(P, 'rdfs:range', Cs_2),
   Property = Cs_1-Name-Cs_2.

rdfs_domain_or_range_to_classes(Rdfs, Tag, Classes) :-
   findall( Class,
      ( ( C := Rdfs^Tag^'owl:Class'^'owl:unionOf'::[
             @'rdf:parseType'='Collection' ]
             ^'owl:Class'@'rdf:about'
        ; C := Rdfs^Tag@'rdf:resource' ),
        owl_reference_to_id(C, Class) ),
      Classes ).


/* object_property_to_vertices(Property, Vertices) <-
      */

object_property_to_vertices(Property, Vertices) :-
   Property = Cs_1-Name-Cs_2,
   append(Cs_1, Cs_2, Cs),
   Vertices = [Name-Name-rhombus-blue-medium|Cs].


/* object_property_to_edges(Property, Edges) <-
      */

object_property_to_edges(Property, Edges) :-
   Property = Cs_1-Name-Cs_2,
   findall( V-Name,
      member(V, Cs_1),
      Edges_1 ),
   findall( Name-W,
      member(W, Cs_2),
      Edges_2 ),
   append(Edges_1, Edges_2, Edges).


/******************************************************************/


