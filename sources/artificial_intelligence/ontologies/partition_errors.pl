

/******************************************************************/
/***                                                            ***/
/***       Ontologies:  Partition Errors                        ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      isa/2,
      disjointWith/2,
      partition_error_found/2.


/*** interface ****************************************************/


/* partition_errors_to_picture <-
      */

partition_errors_to_picture :-
   read_ontology(Owl),
   ontology_to_partition_errors_program(assert, Owl),
   partition_errors_in_prolog,
   partition_errors_minimal_in_prolog.


/* ontology_to_partition_errors_program(Mode, Owl) <-
      */

ontology_to_partition_errors_program(assert, Owl) :-
   retractall(disjointWith(_, _)),
   retractall(isa(_, _)),
   forall(
      owl_to_rules_of_type(
         disjointWith, Owl, [[A]]),
      assert(A) ),
   forall(
      owl_to_rules_of_type(
         subClassOf, Owl, [[subClassOf(X, Y)]]),
      assert(isa(X, Y)) ).
ontology_to_partition_errors_program(consult, Owl) :-
   retractall(disjointWith(_, _)),
   retractall(isa(_, _)),
   findall( Rule,
      owl_to_rules_of_type(
         disjointWith, Owl, [Rule]),
      Rules_d ),
   findall( [isa(X, Y)],
      owl_to_rules_of_type(
         subClassOf, Owl, [[subClassOf(X, Y)]]),
      Rules_i ),
   append(Rules_d, Rules_i, Rules_2),
%  dislog_program_to_dlv_pragram(Rules_2, Rules),
   Rules = Rules_2,
   File = 'results/disjointWith_isa',
   predicate_to_file( File,
      dportray(lp_3, Rules) ),
   consult(File).


/* partition_errors_in_prolog <-
      */

partition_errors_in_prolog_with_consult :-
   consult('results/disjointWith_isa'),
   partition_errors_in_prolog.

partition_errors_in_prolog :-
   measure(
      findall( partition_error_found(X1, Xs),
         ( ontology_partition_error(X1, X2, X3),
           sort([X2, X3], Xs) ),
         Partition_Errors_2 ),
      Time ),
   length(Partition_Errors_2, N),
   sort(Partition_Errors_2, Partition_Errors),
   length(Partition_Errors, M),
   write_list(user, [Time, ' seconds']), nl,
   write_list(user, [N, ' partition errors \n',
      M, ' different partition errors']), nl,
   assert_facts(Partition_Errors).


/* partition_errors_minimal_in_prolog <-
      */

partition_errors_minimal_in_prolog :-
   findall( E,
      partition_error_minimal(E),
      Es_2 ),
   sort(Es_2, Es_3),
   length(Es_3, N),
   write_list(user, [N, ' minimal partition errors']), nl,
   class_partition_errors_nice(Es_3, Es_3n),
   writeln_list('results/partition_errors_minimal', Es_3n),
   findall( E,
      partition_error_very_minimal(Es_3, E),
      Es ),
   length(Es, M),
   write_list(user, [M, ' very minimal partition errors']), nl,
   class_partition_errors_nice(Es, Es_n),
   writeln_list('results/partition_errors_very_minimal', Es_n),
   partition_errors_to_picture(Es_n).


/* partition_error_minimal(X->Xs) <-
      */

partition_error_minimal(X->Xs) :-
   partition_error_found(X, Xs),
   \+ ( partition_error_found(Y, Ys),
        smaller_partition_error_isa(Y->Ys, X->Xs) ).


/* partition_error_very_minimal(Es, X->Xs) <-
      */

partition_error_very_minimal(Es, X->Xs) :-
   member(X->Xs, Es),
   \+ ( member(Y->Ys, Es),
        smaller_partition_error_subClassOf(X->Xs, Y->Ys) ).


/* greater_partition_error(X->Xs, Y->Ys) <-
      */

greater_partition_error(X->Xs, Y->Ys) :-
   partition_error_found(Y, Ys),
   smaller_partition_error_subClassOf(X->Xs, Y->Ys).
   

/* smaller_partition_error_subClassOf(X->Xs, Y->Ys) <-
      */

smaller_partition_error_subClassOf(X1->[X2, X3], Y1->[Y2, Y3]) :-
   ( ontology_subClassOf(Y1, X1)
   ; ontology_subClassOf(X2, Y2)
   ; ontology_subClassOf(X2, Y3)
   ; ontology_subClassOf(X3, Y2)
   ; ontology_subClassOf(X3, Y3) ).


/* smaller_partition_error_isa(X->Xs, Y->Ys) <-
      */

smaller_partition_error_isa(X1->[X2, X3], Y1->[Y2, Y3]) :-
   ( isa(Y1, X1)
   ; isa(X2, Y2)
   ; isa(X2, Y3)
   ; isa(X3, Y2)
   ; isa(X3, Y3) ).


/* disjointWith_all(Class, Classes) <-
      */

disjointWith_all(Class, Classes) :-
   findall( C,
      ( disjointWith(Class, C)
      ; disjointWith(C, Class) ),
      Classes_2 ),
   sort(Classes_2, Classes).


/* class_partition_errors_nice(Es_1, Es_2) <-
      */

class_partition_errors_nice(Es, Es) :-
   !.
class_partition_errors_nice(Es_1, Es_2) :-
   maplist( class_partition_error_nice,
      Es_1, Es_2 ).

class_partition_error_nice(X->Xs, Y->Ys) :-
   maplist( name_start_after_position(["constant_"]),
      [X|Xs], [Y|Ys] ).


/* ontology_partition_error(X1, X2, X3) <-
      */

ontology_partition_error(X1, X2, X3) :-
   ontology_subClassOf(X1, X2), X1 \= X2,
   disjointWith(X2, X3), X2 \= X3,
   ontology_subClassOf(X1, X3), X1 \= X3.

ontology_subClassOf(X1, X2) :-
   isa(X1, X2).
ontology_subClassOf(X1, X2) :-
   isa(X1, X3),
   ontology_subClassOf(X3, X2).


/******************************************************************/


