

/******************************************************************/
/***                                                            ***/
/***       Ontologies:  subClassOf Graph                        ***/
/***                                                            ***/
/******************************************************************/


/* owl_to_subClassOf_graph <-
      */

owl_to_subClassOf_graph :-
   read_ontology(Owl),
   owl_to_nmr_program(Owl, Program),
   findall( C1-C2,
      member([subClassOf(C1, C2)], Program),
      Edges ),
   graph_edges_to_picture(Edges).


/******************************************************************/


