

/******************************************************************/
/***                                                            ***/
/***       Ontologies:  Owl to Rules                            ***/
/***                                                            ***/
/******************************************************************/


:- file_search_path(library, Lib),
   concat(Lib, '/semweb', Path),
   assert(file_search_path(semweb, Path)).

:- use_module(library('triple20/owl')).

:- use_module(semweb(rdf_db)).
:- use_module(semweb(rdfs)).
:- use_module(semweb(rdf_edit)).


/*** interface ****************************************************/


/* owl_ontology_to_program(Rules) <-
      */

owl_ontology_to_program(Rules) :-
   maplist( owl_ontology_to_rules,
      [ instanceOf, class,
        isa,
%       subClassOf,
        disjointWith,
        property, restrictions ], Programs ),
   append(Programs, Rules).


/* owl_ontology_to_rules(Type, Rules) <-
      */

owl_ontology_to_rules(Type) :-
   writeln(user, owl_ontology_to_rules(Type)),
   owl_ontology_to_rules(Type, Rules),
   dportray(lp, Rules).

owl_ontology_to_rules(class, Facts) :-
   owl_ontology_to_rules(isa, As),
   findall( [class(C)],
      ( member([isa(C1, C2)], As),
        member(C, [C1, C2]) ),
      Facts_2 ),
   sort(Facts_2, Facts).

owl_ontology_to_rules(Predicate, Facts) :-
   member(Predicate, [subClassOf, isa]),
   findall( [Atom],
      ( rdf_subject(Class_1),
        ( Predicate = subClassOf,
%         rdfs_subclass_of(Class_1, Class_2),
          rdf_has(Class_1, rdfs:subClassOf, Class_2)
        ; Predicate = isa,
          rdf(Class_1, rdfs:subClassOf, Class_2) ),
        \+ owl_restriction(Class_1),
        \+ owl_restriction(Class_2),
        Class_1 \= Class_2,
        rdf_resource_short(Class_1, C1),
        rdf_resource_short(Class_2, C2),
        Atom =.. [Predicate, C1, C2] ),
      Facts_2 ),
   list_to_ord_set(Facts_2, Facts).

owl_ontology_to_rules(disjointWith, Facts) :-
   findall( [disjointWith(C1, C2)],
      ( rdf_subject(Class_1),
        rdf(Class_1, owl:disjointWith, Class_2),
        \+ owl_restriction(Class_1),
        \+ owl_restriction(Class_2), 
        Class_1 \= Class_2,
        rdf_resource_short(Class_1, C1),
        rdf_resource_short(Class_2, C2) ),
      Facts_2 ),
   list_to_ord_set(Facts_2, Facts).

owl_ontology_to_rules(property, Facts) :-
   findall( [property(P, S, O)],
      ( rdf(Subject, Predicate, Object),
        rdf_subject(Predicate),
        rdf_resource_short(Subject, S),
        rdf_resource_short(Predicate, P),
        rdf_resource_short(Object, O) ),
      Facts ).

owl_ontology_to_rules(instanceOf, Facts) :-
   owl_ontology_to_rules(class, Classes),
   findall( [instanceOf(R, C)],
      ( rdf_subject(Resource),
        rdf_has(Resource, rdf:type, Class),
%       owl_individual_of(Resource, Class),
%       rdf_subject(Class),
        rdf_resource_short(Class, C),
        member([class(C)], Classes),
        rdf_resource_short(Resource, R) ),
      Individuals ),
   list_to_ord_set(Individuals, Facts).

owl_ontology_to_rules(restrictions, Restrictions) :-
   findall( [restriction(Domain_and_Name, P, Res)]-[Satisfiers],
      ( owl_restriction(Resource, Predicate, Restriction),
        rdf_resource_short(Resource, Domain, Name),
        concat([Domain, '#', Name], Domain_and_Name),
        rdf_resource_short(Predicate, P),
        owl_restriction_shorten(Restriction, Res),
        owl_restriction_to_satisfiers(Resource, Satisfiers) ),
      Restrictions ).

owl_restriction_shorten(Restriction_1, Restriction_2) :-
   Restriction_1 = restriction(File_1, Type_1),
   rdf_resource_short(File_1, File_2),
   Type_1 =.. [T, File_3],
   rdf_resource_short(File_3, File_4),
   Type_2 =.. [T, File_4],
   Restriction_2 = restriction(File_2, Type_2).

owl_restriction_to_satisfiers(Restriction, Satisfiers) :-
   findall( R,
      ( rdf_subject(Resource),
        owl:owl_satisfies_restriction(Resource, Restriction),
        rdf_resource_short(Resource, R) ),
      Satisfiers ).


/* owl_restriction(Resource, Predicate, Restriction) <-
      */

owl_restriction(Resource, Predicate, Restriction) :-
   rdf(Resource, Predicate,
      'http://www.w3.org/2002/07/owl#Restriction'),
   owl_restriction(Resource, Restriction).


/* owl_restriction(Resource) <-
      */

owl_restriction(Resource) :-
   !,
   owl_restriction(Resource, _).
owl_restriction(Resource) :-
   rdf(Resource, _, 'http://www.w3.org/2002/07/owl#Restriction').


/* rdf_resource_short(X, Domain, Y) <-
      */

% rdf_resource_short(X, X) :-
%    !.
rdf_resource_short(X, X) :-
   X = literal(_),
   !.
rdf_resource_short(X, Y) :-
%  rdf_split_url(_, Y, X).
   rdf_resource_short(X, _, Y).

rdf_resource_short(X, D, Y) :-
   name_split_at_position(["#"], X, [Domain, Y]),
   name_split_at_position(["/"], Domain, Ys),
   last(Ys, D).


/* rdf_strip(Class_1, Type, Class_2) <-
      */

rdf_strip(Class_1, Type, Class_2) :-
   rdf(C_1, T, C_2),
   rdf_class_strip(C_1, Class_1),
   rdf_class_strip(T, Type),
   rdf_class_strip(C_2, Class_2).

rdf_class_strip(Class_1, Class_2) :-
   atomic(Class_1),
   name_split_at_position(["#", "/"], Class_1, Xs),
   last(Xs, Class_2).
rdf_class_strip(Class, Class) :-
   \+ atomic(Class).


/******************************************************************/


