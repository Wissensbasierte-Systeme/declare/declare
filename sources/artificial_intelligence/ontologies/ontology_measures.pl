

/******************************************************************/
/***                                                            ***/
/***       Ontologies:  Measures                                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* owl_to_adjacent_classes(Mode, [Min, Max]:Avg) <-
      can be called for Mode = subclasses, superclasses. */

owl_to_adjacent_classes(Mode, [Min, Max]:Avg) :-
   member(Mode, [subclasses, superclasses]),
   concat(owl_to_, Mode, Predicate),
   apply(Predicate, [Xs]),
   dislog_variable_get(output_path, Path),
   concat([Path, 'owl_', Mode], File),
   owl_to_adjacent_classes_sort_by_length(Xs, File),
   owl_to_adjacent_classes_aggregate(Xs, [Min, Max]:Avg).

owl_to_adjacent_classes_sort_by_length(Xs, File) :-
   findall( N-Class-Ys,
      ( member(Class-Ys, Xs),
        length(Ys, N) ),
      Zs_2 ),
   sort(Zs_2, Zs_3),
   reverse(Zs_3, Zs),
   write_list(user, ['<--- ', File]), nl(user),
   writeln_list(File, Zs).

owl_to_adjacent_classes_aggregate(Xs, [Min, Max]:Avg) :-
   findall( N,
      ( member(_Class-Superclasses, Xs),
        length(Superclasses, N) ),
      Ns ),
   minimum(Ns, Min),
   maximum(Ns, Max),
   average(Ns, Avg).


/*** implementation ***********************************************/


/* owl_to_subclasses(Classes_Subclasses) <-
      */

owl_to_subclasses(Classes_Subclasses) :-
   read_ontology(Owl),
   owl_to_subclasses(Owl, Classes_Subclasses).
   
owl_to_subclasses(Owl, Classes_Subclasses) :-
   owl_to_classes(Owl, Classes),
   findall( Class_1-Class_2,
      owl_ontology_subclass_of(Owl, Class_1, Class_2),
      Pairs ),
   findall( Class-Subclasses,
      ( member(Class, Classes),
        owl_class_to_subclasses(Pairs, Class, Subclasses) ),
      Classes_Subclasses ).

owl_class_to_subclasses(Pairs, Class, Subclasses) :-
   findall( Subclass,
      member(Subclass-Class, Pairs),
      Subclasses ).


/* owl_to_superclasses(Classes_Superclasses) <-
      */

owl_to_superclasses(Classes_Superclasses) :-
   read_ontology(Owl),
   owl_to_superclasses(Owl, Classes_Superclasses).

owl_to_superclasses(Owl, Classes_Superclasses) :-
   findall( Class-Superclasses,
      ( C := Owl^'owl:Class',
        Class := C@'rdf:ID',
        owl_class_to_superclasses(C, Superclasses) ),
      Classes_Superclasses ).

owl_class_to_superclasses(C, Superclasses) :-
   findall( Superclass,
      ( S := C^'rdfs:subClassOf'@'rdf:resource',
        owl_reference_to_id(S, Superclass) ),
      Superclasses ).


/******************************************************************/


