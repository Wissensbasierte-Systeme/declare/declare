

/******************************************************************/
/***                                                            ***/
/***       Ontologies:  Evaluation with FnQuery                 ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      ontology_test/2,
      owl_to_rules_of_type/3, owl_restriction_to_rule/3.


/* read_ontology(Ontology, Owl) <-
      */

read_ontology(Owl) :-
   owl_current_ontology(Ontology),
   read_ontology(Ontology, Owl),
   !.

read_ontology(Ontology, Owl) :-
   owl_current_ontology_path(Path),
   concat(Path, Ontology, File),
   write_list(['<--- ', File]), nl, nl,
   dread(xml, File, [Owl]).
%  pillow_read(xml, File, Xml),
%  Owl_2 := (xml:Xml)^'rdf:RDF',
%  pillow_fn_item_remove_just_blanks(Owl_2, Owl).


/*** interface ****************************************************/


/* xml_file_to_schema_graph(File) <-
      */

xml_file_to_schema_graph(File) :-
   dread(xml, File, [Xml]),
   fn_triple_to_schema_picture_2(Xml).


/* owl_file_to_schema_graph <-
      */

owl_file_to_schema_graph :-
   read_ontology(Ontology),
   fn_triple_to_schema_picture_2(Ontology).


/* owl_files_evaluate(Times) <-
      */

owl_files_evaluate(Times) :-
   maplist( owl_file_evaluate,
      [ 'printer.owl', 'printer_invalid.owl', 'printer_res.owl',
        'teaching.owl', 'quantifiers.owl' ],
      Times ).


/* owl_file_evaluate <-
      */

owl_file_evaluate :-
   owl_current_ontology(Ontology),
   owl_file_evaluate(Ontology, _).

owl_file_evaluate(Ontology, [Time_1, Time_2]) :-
   read_ontology(Ontology, Owl),
   owl_to_program(Owl, Program),
   nmr_program_to_state(Program, _, [Time_1, Time_2]).

owl_file_evaluate_pe :-
   owl_current_ontology(Ontology),
   owl_file_evaluate_pe(Ontology, _).

owl_file_evaluate_pe(Ontology, [[Time_1, Time_2], T2, T3]) :-
   read_ontology(Ontology, Owl),
   owl_to_program(Owl, Program),
   nmr_program_to_state(Program, State, [Time_1, Time_2]),
   nmr_program_to_state_pe(Program, State_pe, [T2, T3]),
   ( State = State_pe
   ; writeln(user, 'error: different states') ).


/* nmr_program_to_state(Program, State, [Time_1, Time_2]) <-
      */

nmr_program_to_state(Program, State, [Time_1, Time_2]) :-
   star_line,
   dportray(owl_to_nmr_Program, lp_2, Program),
   nmr_program_to_state(dlv, Program, State, Time_1),
   nmr_program_to_state(smodels, Program, State_2, Time_2),
   !,
%  state_subtract(State, State_2, S),
%  state_subtract(State_2, State, S2),
%  dportray(lp, S),
%  bar_line,
%  dportray(lp, S2),
   State = State_2.

nmr_program_to_state(Tool, Program, State, Time) :-
   concat(program_to_definite_consequences_, Tool, Predicate),
   measure(
      apply(Predicate, [Program, State]), Time ),
   dportray(owl_to_nmr_State, lp_2, State),
   star_line,
%  dportray(lp_2, State),
   write_list([Tool, ' ', Time, ' sec.']), nl,
   star_line.


/* nmr_program_to_state_pe(Program, State, [T2, T3]) <-
      */

nmr_program_to_state_pe(Program, State, [T2, T3]) :- 
   measure(
      partial_evaluation(Program, Program_pe), T2 ),
   measure(
      program_to_definite_consequences_dlv(
         Program_pe, State), T3 ),
   dportray(owl_to_nmr_Program_pe, lp_2, Program_pe),
   star_line,
   write_list(['PE           ', T2, ' sec.']), nl,
   write_list(['dlv with PE  ', T3, ' sec.']), nl.


/* owl_to_program(Owl, Program) <-
      */

owl_to_program(Owl, Program) :-
   Types = [ general_axioms, instance, property,
      disjointWith, intersectionOf, subClassOf,
      hasValue, allValuesFrom, someValuesFrom ],
   maplist( owl_to_all_rules_of_type(Owl),
      Types, Programs ),
   append(Programs, Program).

owl_to_all_rules_of_type(Owl, Type, Program) :-
   findall( Rules,
      owl_to_rules_of_type(Type, Owl, Rules),
      Programs ),
   append(Programs, Program).


/* owl_ontology_test(Type, Owl, Errors) <-
      */

owl_ontology_test(Type, Errors) :-
   read_ontology(Owl),
   owl_ontology_test(Type, Owl, Errors).

owl_ontology_test(circularity, Owl, Errors) :-
   findall( C1-C2,
      owl_ontology_subclass_of(Owl, C1, C2),
      Edges_1 ),
   transitive_closure_for_edges(Edges_1, Edges_2),
%  writeln(user, Edges_2),
   findall( C,
      ( member(C-D, Edges_2),
        member(D-C, Edges_2) ),
      Errors ).

owl_ontology_test(partition_error, Owl, Errors) :-
   findall( Class-Class_1-Class_2,
      ( owl_disjoint_with(Owl, Class_1, Class_2),
        owl_transitive_subclass_of(Owl, Class, Class_1),
        owl_transitive_subclass_of(Owl, Class, Class_2) ),
      Errors ).

owl_ontology_test(redundancy, Owl, Errors) :-
   findall( Class_1->Set->Class_2,
      ( owl_ontology_subclass_of(Owl, Class_1, Class_a),
        owl_ontology_subclass_of(Owl, Class_1, Class_b),
        Class_a \= Class_b,
        owl_transitive_subclass_of_sym(Owl, Class_a, Class_2),
        owl_transitive_subclass_of_sym(Owl, Class_b, Class_2),
        list_to_ord_set([Class_a, Class_b], Set) ),
      Errors_2 ),
   list_to_ord_set(Errors_2, Errors).

owl_transitive_subclass_of_sym(Owl, Class_1, Class_2) :-
   owl_transitive_subclass_of(Owl, Class_1, Class_2).
owl_transitive_subclass_of_sym(_, Class, Class).


/* owl_teaching_example <-
      */

owl_teaching_example :-
   diconsult('projects/Ontologies/teaching.dl', Program),
   program_to_definite_consequences_dlv(Program, State),
   dportray(lp, State).


/*** implementation ***********************************************/


/* owl_transitive_subclass_of(Owl, Class_1, Class_2) <-
      */

owl_transitive_subclass_of(Owl, Class_1, Class_2) :-
   findall( C1-C2,
      owl_ontology_subclass_of(Owl, C1, C2),
      Edges_1 ),
   transitive_closure_for_edges(Edges_1, Edges_2),
   !,
   member(Class_1-Class_2, Edges_2).


/* owl_ontology_subclass_of(Owl, Class_1, Class_2) <-
      */

owl_ontology_subclass_of(Owl, Class_1, Class_2) :-
   Class := Owl^'owl:Class'::[@'rdf:ID'=Class_1]
      ^'rdfs:subClassOf'@'rdf:resource',
   ( owl_reference_to_id(Class, Class_2) -> true
   ; Class_2 = Class ).


/* owl_disjoint_with(Owl, Class_1, Class_2) <-
      */

owl_disjoint_with(Owl, Class_1, Class_2) :-
   C2 := Owl^'owl:Class'::[@'rdf:about'=C1]
      ^'owl:disjointWith'@'rdf:resource',
   owl_reference_to_id(C1, Class_1),
   owl_reference_to_id(C2, Class_2).
owl_disjoint_with(Owl, Class_1, Class_2) :-
   C2 := Owl^'owl:Class'::[@'rdf:ID'=Class_1]
      ^'owl:disjointWith'@'rdf:resource',
   owl_reference_to_id(C2, Class_2).


/* owl_to_classes(Owl, Classes) <-
      */

owl_to_classes(Owl, Classes) :-
   findall( Class,
      Class := Owl^'owl:Class'@'rdf:ID',
      Classes_2 ),
   sort(Classes_2, Classes).


/* owl_to_rules_of_type(Type, Owl, Program) <-
      */

owl_to_rules_of_type(general_axioms, _, Program) :-
   Program = [
      [instanceOf(X, C1)]-
         [instanceOf(X, C2), subClassOf(C2, C1)],
      [subClassOf(X, Y)]-
         [subClassOf(X, Z), subClassOf(Z, Y)],
      [subClassOf_ic_error(X, C1, C2)]-
         [subClassOf_ic(C1, C2), instanceOf(X, C1)]-
         [instanceOf(X, C2)],
      [partition_error(C, C1, C2)]-
         [disjointWith(C1, C2), C1 '!=' C2,
          subClassOf(C, C1), C '!=' C1,
          subClassOf(C, C2), C '!=' C2] ].

owl_to_rules_of_type(instance, Owl, [Rule]) :-
   owl_to_classes(Owl, Classes),
   member(Class, Classes),
   X := Owl^Class@'rdf:ID',
   Rule = [instanceOf(X, Class)].
owl_to_rules_of_type(instance, _, [Rule]) :-
   Rule = [instanceOf(X, 'Thing')]-[instanceOf(X, _)].

owl_to_rules_of_type(property, Owl, [Rule]) :-
   owl_to_classes(Owl, Classes),
   member(Class, Classes),
   Y1 := Owl^Class::[@'rdf:ID'=X]^Property@'rdf:resource',
   atomic(Property),
   owl_reference_to_id(Y1, Y),
   Rule = [property(Property, X, Y)].
owl_to_rules_of_type(property, Owl, [Rule]) :-
   Y := Owl^'rdf:Description'::[@'rdf:about'=X]
      ^Property@'rdf:resource',
   Property \= 'rdf:type',
   Rule = [property(Property, X, Y)].

owl_to_rules_of_type(disjointWith, Owl, [Rule]) :-
   owl_disjoint_with(Owl, Class_1, Class_2),
   Rule = [disjointWith(Class_1, Class_2)].

owl_to_rules_of_type(hasValue, Owl, [Rule]) :-
   member(S, ['rdfs:subClassOf', 'owl:subClassOf']),
   R := Owl^'owl:Class'::[@'rdf:ID'=Class_1]^S^'owl:Restriction',
   owl_restriction_parse(hasValue, R, Property=Value),
   Rule = [property(Property, X, Value)]-[instanceOf(X, Class_1)].

owl_to_rules_of_type(intersectionOf, Owl, Program) :-
   I := Owl^'owl:Class'::[@'rdf:ID'=Class]^'owl:intersectionOf',
   owl_translate(I, Class_I, Rules),
   Program = [
      [instanceOf(X, Class)]-[instanceOf(X, Class_I)] | Rules].

owl_to_rules_of_type(subClassOf, Owl, [Rule]) :-
   owl_ontology_subclass_of(Owl, Class_1, Class_2),
   Rule =
      [subClassOf(Class_1, Class_2)].
owl_to_rules_of_type(subClassOf, Owl, Program) :-
   member(S, ['rdfs:subClassOf', 'owl:subClassOf']),
   member(A, ['rdf:ID', 'rdf:about']),
   I := Owl^'owl:Class'::[@A=C]^S,
   owl_reference_to_id(C, Class),
   \+ (_ := I@'rdf:resource'),
   owl_translate(I, Class_I, Rules),
   Program = [
      [subClassOf_ic(Class, Class_I)] | Rules ].


/* owl_translate(Owl, Class_I, Rules) <-
      */

owl_translate(Owl, Class_I, Rules) :-
   Owl_2 := Owl^'owl:Class'^'owl:intersectionOf',
   !,
   owl_translate(Owl_2, Class_I, Rules).
owl_translate(Owl, Class_I, Rules) :-
   fn_item_parse(Owl, 'owl:intersectionOf':_:_),
   gensym(intersectionOf_restriction_, Class_I),
   owl_translate_collection(Owl, Class_I, Rules).
owl_translate(Owl, Class_I, Rules) :-
   ( fn_item_parse(Owl, 'rdfs:subClassOf':_:_)
   ; fn_item_parse(Owl, 'owl:subClassOf':_:_) ),
   gensym(subClassOf_restriction_, Class_I),
   owl_translate_collection(Owl, Class_I, Rules).

owl_translate_collection(Owl, Class_I, Rules) :-
   findall( Class,
      ( ( C := Owl^'owl:Class'@'rdf:about'
        ; C := Owl^'owl:Class'@'rdf:ID' ),
        owl_reference_to_id(C, Class) ),
      Classes_1 ),
   ( member(Superclass, Classes_1)
   ; Superclass = 'Thing' ),
   findall( restriction(Type, Superclass, Result),
      ( Restriction := Owl^'owl:Restriction',
        member(Type, [hasValue, allValuesFrom, someValuesFrom]),
        owl_restriction_parse(Type, Restriction, Result) ),
      Restrictions ),
   ( foreach(Restriction, Restrictions),
     foreach(Class, Classes_2),
     foreach(Program, Programs) do
        owl_restriction_to_program(Restriction, Class, Program) ),
   append(Classes_1, Classes_2, Classes),
   maplist( owl_class_to_property_atom(X),
      Classes, As ),
   Rule = [instanceOf(X, Class_I)]-As,
   append([[Rule]|Programs], Rules),
   !.

owl_class_to_property_atom(X, Class, A) :-
   A = instanceOf(X, Class).


/* owl_restriction_to_program(Restriction, Name, Program) <-
      */

owl_restriction_to_program(Restriction, Q, Program) :-
   Restriction = restriction(allValuesFrom, Superclass, R-C),
   concat_atom([Superclass, all, R, C], '_', Q),
   concat_atom([not, Q], '_', Not_Q),
   Program = [
      [instanceOf(X, Q)]-
         [instanceOf(X, Superclass)]-
         [instanceOf(X, Not_Q)],
      [instanceOf(X, Not_Q)]-
         [instanceOf(X, Superclass), property(R, X, Y)]-
         [instanceOf(Y, C)] ],
   !.
owl_restriction_to_program(Restriction, Q, Program) :-
   Restriction = restriction(someValuesFrom, _, R-C),
   concat_atom([some, R, C], '_', Q),
   Program = [
      [instanceOf(X, Q)]-
         [property(R, X, Y), instanceOf(Y, C)] ],
   !.
owl_restriction_to_program(Restriction, Q, Program) :-
   Restriction = restriction(hasValue, Superclass, Property=Value),
   gensym(hasValue_restriction_, Q),
%  concat_atom([hasValue, Property, Value], '_', Q),
   Program = [
      [instanceOf(X, Q)]-
         [instanceOf(X, Superclass),
          property(Property, X, Value)] ],
   !.


/* owl_restriction_parse(Type, Restriction, Result) <-
      */

owl_restriction_parse(hasValue, Restriction, Property=Value) :-
   ( P := Restriction^'owl:onProperty'@'rdf:resource'
   ; P := Restriction^'owl:onProperty'
        ^'owl:FunctionalProperty'@'rdf:about' ),
   ( ( Type = 'xsd:string'
     ; Type = 'xsd:integer' ),
     Value := Restriction^'owl:hasValue'^Type@'rdf:value'
   ; V := Restriction^'owl:hasValue'@'rdf:resource',
     owl_reference_to_id(V, Value) ),
   owl_reference_to_id(P, Property).

owl_restriction_parse(Type, Restriction, Role-Concept) :-
   member(Type, [allValuesFrom, someValuesFrom]),
   concat('owl:', Type, Selector),
   R := Restriction^'owl:onProperty'@'rdf:resource',
   owl_reference_to_id(R, Role),
   C := Restriction^Selector@'rdf:resource',
   owl_reference_to_id(C, Concept).

owl_restriction_parse(Type, Restriction,
      Property-Which_Cardinality:Value) :-
   member(Type, [onProperty]),
   Property := Restriction
      ^'owl:onProperty'^'owl:ObjectProperty'@'rdf:ID',
   member(T, ['owl:minCardinality', 'owl:owl:maxCardinality']),
   [Value] := Restriction
      ^T^content::'*',
   concat('owl:', Which_Cardinality, T).


/* owl_reference_to_id(Reference, Id) <-
      */

owl_reference_to_id(Reference, Id) :-
   concat('#', Id, Reference),
   !.
owl_reference_to_id(Id, Id).


/* transitive_closure_for_edges(Edges_1, Edges_2) <-
      */

transitive_closure_for_edges(Edges_1, Edges_2) :-
   edges_to_vertices(Edges_1, Vertices_1),
   vertices_edges_to_ugraph(Vertices_1, Edges_1, Graph_1),
   transitive_closure(Graph_1, Graph_2),
   edges(Graph_2, Edges_2).


/******************************************************************/


test(ontologies, 'mia_onto.owl') :-
   read_ontology('Examples/mia_onto.owl', Owl),
   owl_to_program(Owl, Program),
   dportray(owl_to_nmr_Program, lp_2, Program),
   nmr_program_to_state(dlv, Program, State, Time),
   dportray(owl_dlv_state, lp, State),
   write_list([Time, ' sec.']), nl.


/******************************************************************/


