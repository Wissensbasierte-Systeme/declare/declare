

/******************************************************************/
/***                                                            ***/
/***       SWRL Ontologies:  Evaluation                         ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      swrl_to_rule/3.
:- multifile
      swrl_file_evaluation/1,
      swrl_to_rule/3.


/*** interface ****************************************************/


/* swrl_file_evaluation(classes) <-
      */

swrl_file_evaluation(classes) :-
   File = 'examples/ontologies/family_2.owl',
   swrl_file_class_evaluation(File, _).


/* swrl_file_class_evaluation(File, Rules) <-
      */

swrl_file_class_evaluation(File, Rules) :-
   dread(xml, File, [Swrl]),
   swrl_class_evaluation(Swrl, Rules),
   dportray(lp_window, Rules).

dportray(lp_window, Program) :-
   dislog_variable_get(output_path, Path),
%  concat(Path, 'swrl_evaluation.tmp', File_Out),
   concat(Path, 'logic_program.tmp', File_Out),
   predicate_to_file( File_Out,
      dportray(lp_xml, Program) ),
   file_view_for_gui(File_Out).


/* swrl_class_evaluation(Swrl, Rules) <-
      */

swrl_class_evaluation(Swrl, Rules) :-
   findall( Rule,
      ( swrl_to_rule(Type, Swrl, Rule),
        Type \= rule ),
      Rules_2 ),
   swrl_ontology_class_evaluation(Rules_2, Rules).


/* swrl_ontology_class_evaluation(Rules_1, Rules_2) <-
      */

swrl_ontology_class_evaluation(Rules_1, Rules_2) :-
   dportray(lp, Rules_1),
   writeq(user, Rules_1),
   swrl_ontology_class_evaluation_program(Rules_a),
   append(Rules_1, Rules_a, Rules_b),
   perfect_models(Rules_b, [Model]),
   list_of_elements_to_relation(Model, Facts),
   append(Rules_1, Facts, Rules_c),
   sort(Rules_c, Rules_2).


/* swrl_ontology_class_evaluation_program(Program) <-
      */

swrl_ontology_class_evaluation_program([
   [C=C]-[class(C)],
   [isa(C1, C2)]-[sub_class(C1, C2)],
   [isa(C1, C3)]-[isa(C1, C2), sub_class(C2, C3)],

   [circularity_of_taxonomy(C1, C2)]-
      [isa(C1, C2), sub_class(C2, C1)],
   [partition_error(C, C1, C2)]-
      [incompatible(C1, C2), isa(C, C1), isa(C, C2)],
   [incompleteness(C, C1, C2, C3)]-
      [sub_class(C1, C), sub_class(C2, C), sub_class(C3, C),
       incompatible(C1, C2)]-
      [incompatible(C2, C3), C1=C2, C2=C3, C1=C3],
   [redundant_isa(C1->C2->C3)]-
      [isa(C1, C3), sub_class(C3, C2), isa(C1, C2)]-
      [sub_class(C2, C3)],
   [lonely_disjoint_class(C, C1, C2, C3)]-
      [incompatible(C, C1), sub_class(C2, C),
       sub_class(C3, C1), incompatible(C2, C3)]-
      [C=C1, C1=C2, C2=C3, C=C2, C1=C3],
   [inconsistent_taxonomy(C1, C2)]-
      [isa(C1, C2), incompatible(C1, C2)],
   [inconsistent_instance(I, (C1, C2))]-
      [instance(I, C1), instance(I, C2), incompatible(C1, C2)]
]).
      

/******************************************************************/


