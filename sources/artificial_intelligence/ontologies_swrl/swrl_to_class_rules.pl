

/******************************************************************/
/***                                                            ***/
/***       SWRL Ontologies:  SWRL to Class Rules                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* owl_to_sub_id(Owl_1, Tag, Owl_2, Value) <-
      */

owl_to_sub_id(Owl_1, Tag, Owl_2, Value) :-
   ( Owl_2 := Owl_1^Tag::[@'rdf:ID'=V]
   ; Owl_2 := Owl_1^Tag::[@'rdf:about'=V] ),
   owl_reference_to_id(V, Value).


/* owl_to_self_id(Owl, Value) <-
      */

owl_to_self_id(Owl, Value) :-
   ( V := Owl@'rdf:resource'
   ; V := Owl@'rdf:ID'
   ; V := Owl@'rdf:about' ),
   owl_reference_to_id(V, Value).


/* swrl_to_rule(Type, Owl, Rule) <-
      */

swrl_to_rule(class, Owl, [class(C)]) :-
   owl_to_sub_id(Owl, 'owl:Class', _, C).

swrl_to_rule(property, Owl, [Atom]) :-
   propert_predicate_to_owl_tag(Type, Tag),
   owl_to_sub_id(Owl, Tag, _, C),
   Atom =.. [Type, C].

swrl_to_rule(sub_class, Owl, [sub_class(C1, C2)]) :-
   owl_to_sub_id(Owl, 'owl:Class', Owl_2, C1),
   S := Owl_2/'rdfs:subClassOf',
   ( owl_to_self_id(S, C2)
   ; owl_to_sub_id(S, 'owl:Class', _, C2) ).

swrl_to_rule(P, Owl, [Atom]) :-
   ( P = disjoint, T = 'owl:disjointWith'
   ; P = complement, T = 'owl:complementOf' ),
   owl_to_sub_id(Owl, 'owl:Class', Owl_2, C1),
   S := Owl_2/descendant::T,
   ( owl_to_self_id(S, C2)
   ; owl_to_sub_id(S, 'owl:Class', _, C2) ),
   Atom =.. [P, C1, C2].

swrl_to_rule(equivalent_class, Owl,
      [equivalent_class(C1, C2)]) :-
   owl_to_sub_id(Owl, 'owl:Class', Owl_2, C1),
   owl_to_sub_id(Owl_2, 'owl:equivalentClass', _, C2).

swrl_to_rule(instance, Owl, [instance(I, C)]) :-
   Owl_2 := Owl/descendant::C,
   \+ name_contains_name(C, ':'),
   owl_to_self_id(Owl_2, I).

swrl_to_rule(restriction, Swrl,[restriction(Type, C, Result)]) :-
   Class := Swrl/descendant::'owl:Class'::[@'rdf:about'=C1],
   owl_reference_to_id(C1, C),
   Restriction := Class/descendant::'owl:Restriction',
   member(Type, [
      hasValue, allValuesFrom, someValuesFrom, onProperty ]),
   owl_restriction_parse(Type, Restriction, Result).


/* propert_predicate_to_owl_tag(Predicate, Tag) <-
      */

propert_predicate_to_owl_tag(Predicate, Tag) :-
   Tags = [
      object_property - 'ObjectProperty',
      datatype_property - 'DatatypeProperty',
      transitive_property - 'TransitiveProperty',
      symmetric_property - 'SymmetricProperty',
      functional_property - 'FunctionalProperty',
      inverse_functional_property -
         'InverseFunctionalProperty' ],
   member(Predicate-T, Tags),
   concat('owl:', T, Tag).

   
/******************************************************************/


