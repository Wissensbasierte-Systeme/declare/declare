

/******************************************************************/
/***                                                            ***/
/***       SWRL Ontologies:  SWRL to Rule Rules                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* swrl_to_rule(Type, Swrl, Rule) <-
      */

swrl_to_rule(rule_with_variables, Swrl, [rule(A-Bs)]) :-
   Rule := Swrl/'swrl:Imp',
   swrl_to_rule_(rule, Rule, A2-Bs2),
   swrl_to_variables_3(Rule, Variables),
   swrl_substitute_variables(Variables, [A2-Bs2], [A-Bs]).

swrl_to_rule(rule, Swrl, [rule(A-Bs, B)]) :-
   Rule := Swrl/'swrl:Imp',
   swrl_to_rule_(rule, Rule, A-Bs),
   member(B, Bs).

swrl_to_rule_(rule, Rule, A-Bs) :-
   Head := Rule/'swrl:head',
   swrl_to_prolog_atoms(2, Head, [A]),
   Body := Rule/'swrl:body',
   swrl_to_prolog_atoms(2, Body, Bs).


/* swrl_to_prolog_atoms(Mode, Swrl, Atoms) <-
      */

swrl_to_prolog_atoms(Mode, Swrl, Atoms) :-
   findall( Atom,
      swrl_to_class_or_property_atom(Swrl, Atom),
      Atoms_2 ),
   maplist( swrl_atom_to_prolog_atom(Mode),
      Atoms_2, Atoms ).


/* swrl_atom_to_prolog_atom(Mode, Atom_1, Atom_2) <-
      */

swrl_atom_to_prolog_atom(3, Atom_1, Atom_2) :-
   swrl_atom_to_predicate(Atom_1, P),
   findall( N-A,
      ( ( A := Atom_1/T/'swrl:Variable'@'rdf:ID'
        ; B := Atom_1/T@'rdf:resource',
          owl_reference_to_id(B, A) ),
        concat('swrl:argument', N, T) ),
      Pairs_1 ),
   sort(Pairs_1, Pairs_2),
   pair_lists(-, _, Arguments, Pairs_2),
   Atom_2 =.. [P|Arguments].

swrl_atom_to_prolog_atom(2, Atom_1, Atom_2) :-
   swrl_atom_to_predicate(Atom_1, P),
   Xs := Atom_1/content::'*',
   findall( A,
      swrl_atom_to_argument(Atom_1, Xs, A),
      Arguments ),
   Atom_2 =.. [P|Arguments].


/* swrl_atom_to_predicate(Atom, Predicate) <-
      */

swrl_atom_to_predicate(Atom, Predicate) :-
   ( Pred := Atom/'swrl:propertyPredicate'@'rdf:resource'
   ; Pred := Atom/'swrl:classPredicate'@'rdf:resource' ),
   owl_reference_to_id(Pred, Predicate).


/* swrl_to_class_or_property_atom(Swrl, Atom) <-
      */

swrl_to_class_or_property_atom(Swrl, Atom) :-
   ( Atom := Swrl/descendant::'swrl:IndividualPropertyAtom'
   ; Atom := Swrl/descendant::'swrl:ClassAtom' ).


/* swrl_atom_to_argument(Atom, Xs, A) <-
      */

swrl_atom_to_argument(Atom, Xs, A) :-
   ( A := Atom/descendant::'swrl:Variable'@'rdf:ID'
   ; ( member(T:As:[], Xs),
       concat('swrl:argument', _, T),
       Arg := (T:As:[])@'rdf:resource',
       owl_reference_to_id(Arg, A) ) ).


/* swrl_substitute_variables(Variables, Rules_1, Rules_2) <-
      */

swrl_substitute_variables(Variables, Rules_1, Rules_2) :-
   length(Variables, N),
   n_free_variables(N, Vs),
   pair_lists(Variables, Vs, Substitution),
   substitute(Rules_1, Substitution, Rules_2).


/* swrl_to_variables(Rule, Variables) <-
      */

swrl_to_variables(Swrl, Variables) :-
   findall( V,
      ( swrl_to_swrl_atom(Swrl, Atom),
        Xs := Atom/content::'*',
        swrl_atom_to_argument(Atom, Xs, V) ),
      Variables_2 ),
   sort(Variables_2, Variables).

swrl_to_swrl_atom(Swrl, Atom) :-
   Rule := Swrl/'swrl:Imp',
   ( X := Rule/'swrl:head'
   ; X := Rule/'swrl:body' ),
   swrl_to_class_or_property_atom(X, Atom).


/* swrl_to_variables_3(Rule, Variables) <-
      */

swrl_to_variables_3(Rule, Variables) :-
   findall( V,
      ( V := Rule/descendant::'swrl:Variable'@'rdf:ID'
      ; swrl_to_class_or_property_atom(Rule, Atom),
        Arg := Atom/T@'rdf:resource',
        concat('swrl:argument', _, T),
        owl_reference_to_id(Arg, V) ),
      Variables_2 ),
   sort(Variables_2, Variables).


/******************************************************************/


