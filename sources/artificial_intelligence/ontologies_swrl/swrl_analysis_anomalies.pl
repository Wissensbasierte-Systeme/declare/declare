

/******************************************************************/
/***                                                            ***/
/***       SWRL Ontologies:  Anomalies                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* anomaly(Type, Anomaly) <-
      */

anomaly(exact_circularity, As) :-
   derives(A1, A2),
   A1 \= A2,
   derives(A2, A1),
   sort([A1, A2], As).

anomaly(circularity_in_rules_and_taxonomy, A-Bs) :-
   rule(A-Bs),
   member(B, Bs),
   tc_derives_atom(A, B).

anomaly(circular_property, [C, Ps]) :-
   tc_connected_classes(C, Ps, C),
   member(P, Ps),
   \+ symmetric_property(P).

anomaly(partition_error, A-Bs) :-
   disjoint(B1, B2),
   ( ( isa(A, B1), isa(A, B2) )
   ; ( instance(A, B1), instance(A, B2) ) ),
   sort([B1, B2], Bs).

anomaly(incompatible_antecedent, A-Bs) :-
   rule(A-Bs),
   subset_all_different([B1, B2], Bs),
   incompatible_atoms(B1, B2).

anomaly(unsatisfiable_condition, A-Bs) :-
   rule(A-Bs),
   subset_all_different([B1, B2], Bs),
   incompatible_atoms(B1, B2).

anomaly(contradicting_consequent, A-Bs) :-
   rule(A-Bs),
   member(B, Bs),
   incompatible_atoms(A, B).

anomaly(ambivalent_rules, [R1, R2]) :-
   rule(R1), R1 = A1-Bs1,
   rule(R2), R2 = A2-Bs2,
   clause_subsumes(Bs1, Bs2),
   incompatible_atoms(A1, A2).

anomaly(multiple_functionality, C) :-
   ( functional_property(P)
   ; ( functional_property(Q), tc_derives(P, Q) ) ),
   max_cardinality_restriction(C, P, 1).

anomaly(implication_of_superclasses, A-Bs) :-
   rule(A-Bs),
   member(B, Bs),
   tc_derives_atom(B, A).

anomaly(redundant_transitivity_consequent, Rule) :-
   rule(Rule),
   head_predicate(Rule, R), transitive_property(R),
   body_predicate(Rule, P), equivalent(R, P),
   body_predicate(Rule, Q), equivalent(R, Q),
   P_xy =.. [P, X, Y], Q_yz =.. [Q, Y, Z], R_xz =.. [R, X, Z],
   rule_subsumes_chk(R_xz-[P_xy, Q_yz], Rule).

anomaly(redundant_symmetry_consequent, Rule) :-
   rule(Rule),
   head_predicate(Rule, R), symmetric_property(R),
   body_predicate(Rule, P), equivalent(R, P),
   R_xy =.. [R, X, Y], P_yx =.. [P, Y, X],
   rule_subsumes_chk(R_xy-[P_yx], Rule).

anomaly(redundant_derivation_antecedent, A-Bs) :-
   rule(A-Bs),
   subset_all_different([B1, B2], Bs),
   tc_derives_atom(B1, B2).

anomaly(redundant_transitivity_antecedent, Rule) :-
   rule(Rule),
   body_predicate(Rule, R), transitive_property(R),
   body_predicate(Rule, P), equivalent(R, P),
   body_predicate(Rule, Q), equivalent(R, Q),
   P_xy =.. [P, X, Y], Q_yz =.. [Q, Y, Z], R_xz =.. [R, X, Z],
   rule_subsumes_chk(_-[P_xy, Q_yz, R_xz], Rule).

anomaly(redundant_symmetry_antecedent, Rule) :-
   rule(Rule),
   body_predicate(Rule, P), symmetric_property(P),
   body_predicate(Rule, Q), equivalent(P, Q),
   P_xy =.. [P, X, Y], Q_yx =.. [Q, Y, X],
   rule_subsumes_chk(_-[P_xy, Q_yx], Rule).

anomaly(subsumed_rule, [A-As, B-Bs]) :-
   rule(A-As),
   rule(B-Bs),
   rule_subsumes_chk(A-As, B-Bs),
   \+ rule_subsumes_chk(B-Bs, A-As).

anomaly(unsupported_condition, A-Bs) :-
   rule(A-Bs),
   member(B, Bs),
   \+ rule(B-_).

anomaly(redundant_mincardinality_0, C) :-
   min_cardinality_restriction(C, _P, 0).

anomaly(subsumed_maxcardinality_1, C) :-
   ( functional_property(P)
   ; ( functional_property(Q) , tc_derives(P, Q) ) ),
   max_cardinality_restriction(C, P, 1).

anomaly(lazy_element, A) :-
   element(A),
   \+ isa(_, A),
   \+ in_rule(A),
   \+ instance(_, A).

anomaly(lonely_disjoint, C) :-
   class(C),
   siblings(Cs),
   disjoint_multiple(C, Cs),
   \+ ( sibling(C, M), disjoint(C, M) ).

anomaly(over_specific, [R1, R2, has_value(P, [V1, V2])]) :-
   rule(R1), rule(R2),
   R1 = A1-Body1, R2 = A2-Body2, R1 \= R2,
   delete(has_value(P, V1), Body1, B1),
   delete(has_value(P, V2), Body2, B2),
   A1-B1 = A2-B2.


/******************************************************************/


