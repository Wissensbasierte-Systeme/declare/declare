

/******************************************************************/
/***                                                            ***/
/***       SWRL Ontologies:  Anomaly Analysis                   ***/
/***                                                            ***/
/******************************************************************/


:- module( swrl_analysis, [
      swrl_prolog_file_to_anomalies/1,
      swrl_prolog_file_to_anomalies/2,
      swrl_file_to_anomalies/1 ] ).


:- [ 'swrl_analysis_prolog.pl',
     'swrl_analysis_anomalies.pl' ].


ontologies_swrl_directory(Directory) :-
   dislog_variable_get(source_path, Path),
   concat(Path, 'projects/ontologies_swrl/', Directory).

ontologies_swrl_program(File, Path) :-
   ontologies_swrl_directory(Directory),
   concat([Directory, 'swrl_programs/', File], Path).

 
/*** interface ****************************************************/


/* swrl_prolog_file_to_anomalies(N, File) <-
      */

swrl_prolog_file_to_anomalies(N) :-
   member(N, [1, 2, 3]),
   ontologies_swrl_program('swrl_facts.pl', File),
   swrl_prolog_file_to_anomalies(N, File).

swrl_prolog_file_to_anomalies(1, File) :-
   dislog_consult(File, Facts),
   swrl_prolog_facts_to_interpretation(Facts, I),
   swrl_prolog_module_reset(swrl_analysis),
   assert_facts(swrl_analysis, I),
   !,
   swrl_prolog_module_to_anomalies.

swrl_prolog_file_to_anomalies(2, File) :-
   gensym(datalog_, Module),
%  Module = datalog,
   ontologies_swrl_program('swrl_datalog_1.pl', File_Datalog_1),
   ontologies_swrl_program('swrl_prolog.pl', File_Prolog),
   ontologies_swrl_program('swrl_datalog_2.pl', File_Datalog_2),
   prolog_consult(File, I1),
   tp_iteration_prolog_file(File_Datalog_1, Module, I1, I2),
   Module:consult(File_Prolog),
   tp_iteration_prolog_file(File_Datalog_2, Module, I2, I3),
   swrl_anomalies_to_picture(I3).

swrl_prolog_file_to_anomalies(3, File) :-
   ontologies_swrl_program('swrl_datalog_1.pl', D1),
   ontologies_swrl_program('swrl_datalog_2.pl', D2),
   ontologies_swrl_program('swrl_prolog.pl', P),
   perfect_model_prolog_files([D1, D2, File], [P], Model),
   swrl_anomalies_to_picture(Model).


/* swrl_file_to_anomalies(File) <-
      */

swrl_file_to_anomalies(File) :-
   dread(xml, File, [Swrl]),
   swrl_to_prolog_module(Swrl, swrl_analysis),
   !,
   swrl_prolog_module_to_anomalies.


/* swrl_prolog_module_to_anomalies <-
      */

swrl_prolog_module_to_anomalies :-
   findall( anomaly(Type, A),
      anomaly(Type, A),
      Anomalies ),
   swrl_anomalies_to_picture(Anomalies).


/* swrl_anomalies_to_picture(Interpretation) <-
      */

swrl_anomalies_to_picture(Interpretation) :-
   findall( [Type, Anomaly],
      ( member(anomaly(Type, A), Interpretation),
        listvars(A, 1),
        term_to_atom(A, Anomaly) ),
      Pairs_2 ),
   sort(Pairs_2, Pairs),
   xpce_display_table(['Type', 'Anomaly'], Pairs).


/* swrl_to_prolog_module(Swrl, Module) <-
      */

swrl_to_prolog_module(Swrl, Module) :-
   swrl_to_prolog_facts(Swrl, Facts),
   swrl_prolog_module_reset(Module),
   assert_facts(Module, Facts).


/* swrl_prolog_module_reset(Module) <-
      */

swrl_prolog_module_reset(Module) :-
   swrl_to_predicates(Ps),
   ( foreach(P, Ps) do
        abolish(Module:P),
        dynamic(Module:P) ).


/* swrl_prolog_module_abolish(Module) <-
      */

swrl_prolog_module_abolish(Module) :-
   swrl_to_predicates(Ps),
   ( foreach(P, Ps) do
        abolish(Module:P) ).


/* swrl_prolog_module_listing(Module) <-
      */

swrl_prolog_module_listing(Module) :-
   swrl_to_predicates(Ps),
   ( foreach(P, Ps) do
        ( P = Pred/Arity,
          n_free_variables(Arity, Vs),
          Atom =.. [Pred|Vs],
          \+ catch( call(Module:Atom), _, fail )
        ; listing(Module:P) ) ).


/* swrl_to_predicates(Predicates_with_Arities) <-
      */

swrl_to_predicates(Predicates_with_Arities) :-
   findall( P/1,
      propert_predicate_to_owl_tag(P, _),
      Xs ),
   findall( P/N,
      ( clause(swrl_to_rule(_, _, [Rule]), _),
        nonvar(Rule),
        functor(Rule, P, N) ),
      Ys ),
   Zs = [isa/2, disjoint/2, complement/2,
      connected_classes/3, tc_connected_classes/3,
      derives/2, tc_derives/2],
   append([Xs, Ys, Zs], Predicates_with_Arities).


/* swrl_to_prolog_facts(Swrl, Interpretation) <-
      */

swrl_to_prolog_facts(Swrl, I) :-
   writeln(user, 'Construction of the rules ...'),
   findall( Rule,
      ( swrl_to_rule(Type, Swrl, Rule),
        Type \= rule ),
      Facts ),
   swrl_prolog_facts_to_interpretation(Facts, I),
   elements_to_lists(I, Facts_2),
   dislog_variable_get(output_path, Results),
   concat(Results, 'swrl_program_derived.tmp', Path),
   predicate_to_file( Path,
      dportray(lp_xml, Facts_2) ).


/* swrl_prolog_facts_to_interpretation(Facts, I) <-
      */

swrl_prolog_facts_to_interpretation(Facts, I) :-
   dislog_variable_get(source_path, Sources),
   concat([ Sources, 'projects/ontologies_swrl/',
      'swrl_analysis_datalog.pl' ], File),
   dislog_consult(File, Rules),
   append(Facts, Rules, Program),
   writeln(user, 'Tp Iteration ...'),
   dislog_variable_get(output_path, Results),
   concat(Results, 'tp_iteration_trace.tmp', Path),
   predicate_to_file( Path,
      tp_iteration(Program, I) ).


/******************************************************************/


