

/******************************************************************/
/***                                                            ***/
/***       SWRL Ontologies:  Evaluation                         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* swrl_file_evaluation(rules) <-
      */

swrl_file_evaluation(rules) :-
   File = 'examples/ontologies/family_2.owl',
   swrl_file_to_rules(File, _).


/* swrl_file_to_rules(File, Rules) <-
      */

swrl_file_to_rules(File, Rules) :-
   dread(xml, File, [Swrl]),
   swrl_to_rules(Swrl, Rules),
   dportray(lp, Rules),
   dportray(lp_window, Rules),
   swrl_ontology_rule_evaluation(Rules, Rules_2),
   dportray(lp_window, Rules_2).


/* swrl_to_rules(Swrl, Rules) <-
      */

swrl_to_rules(Swrl, Rules) :-
   findall( Rule,
      swrl_to_rule(_, Swrl, Rule),
      Rules_1 ),
   sort(Rules_1, Rules_2),
   length(Rules_1, N1),
   length(Rules_2, N2),
   ( N1 = N2
   ; M is N1 - N2,
     write_list(user, [M, ' duplicate rules\n']) ),
   swrl_to_variables(Swrl, Variables),
   swrl_substitute_variables(Variables, Rules_2, Rules).


/* swrl_ontology_rule_evaluation(Rules_1, Rules_2) <-
      */

swrl_ontology_rule_evaluation(Rules_1, Rules_2) :-
   swrl_ontology_rule_evaluation_program(Rules_a),
   swrl_program_to_rule_heads(Rules_a, Goals),
   append(Rules_1, Rules_a, Rules_b),
   swrl_prolog_evaluation(Goals, Rules_b, Rules_2).


/* swrl_prolog_evaluation(Goals, Program, Facts) <-
      */

swrl_prolog_evaluation(Goals, Program, Facts) :-
   Rules = [
      [isa(C1, C2)]-[sub_class(C1, C2)],
      [isa(C1, C3)]-[isa(C1, C2), sub_class(C2, C3)] ],
   findall( [sub_class(X, Y)],
      member([sub_class(X, Y)], Program),
      Facts_sub_class ),
   append(Facts_sub_class, Rules, Rules_isa),
   perfect_models(Rules_isa, [Model_isa]),
   list_of_elements_to_relation(Model_isa, Facts_isa),
   findall( [isa(X1, X2)],
      ( member([isa(C1, C2)], Facts_isa),
        X1 =.. [C1, _],
        X2 =.. [C2, _] ),
      Facts_isa_2 ),
   dportray(lp, Facts_isa_2),
   !,
   append(Program, Facts_isa_2, Program_2),
   star_line,
   findall( [Goal],
      ( member(Goal, Goals),
        swrl_evaluate(Program_2, Goal) ),
      Facts ),
   !,
   star_line,
   writeln(user, Facts).

swrl_program_to_rule_heads(Program, Heads) :-
   foreach(Rule, Program), foreach(Head, Heads) do
      parse_dislog_rule(Rule, [Head], _, _).

swrl_evaluate(Program, Goal) :-
   member(Rule, Program),
   parse_dislog_rule(Rule, [Goal], Atoms, []),
   writeln(user, [Goal]-Atoms),
   checklist( swrl_evaluate(Program),
      Atoms ).


/* swrl_ontology_rule_evaluation_program(Program) <-
      */

swrl_ontology_rule_evaluation_program([
   [implication_of_sub_class(A-[B])]-
      [rule(A-[B], _), isa(A, B)],
   [implication_of_super_class(A-[B])]-
      [rule(A-[B], _), isa(B, A)],
   [inconsistent_instance(C1, C2, I)]-
      [instance(I, C1), instance(I, C2), incompatible(C1, C2)],
   [recursion(A-Bs)]-
      [rule(A-Bs, B), rule(B-_, A)],
   [circularity_in_the_rule(A-Bs)]-
      [rule(A-Bs, B1), rule(A-Bs, B2), rule(B1-[B2], _)],
   [inconsistent_rule_pair(A1, A2, Bs)]-
      [rule(A1-Bs, _), rule(A2-Bs, _), incompatible(A1, A2)],
   [inconsistent_rule(A-Bs)]-
      [rule(A-Bs, B), isa(B, C), incompatible(A, C)],
   [inconsistent_rule_body(A-Bs)]-
      [rule(A-Bs, B1), rule(A-Bs, B2), incompatible(B1, B2)],
   [contradicting_rule_consequent(A-Bs)]-
      [rule(A-Bs, B), incompatible(B, A)],
   [subsummtion_rule(A-Bs)]-
      [rule(A-Bs, B), rule(A-[B], _)]
]).


/******************************************************************/


