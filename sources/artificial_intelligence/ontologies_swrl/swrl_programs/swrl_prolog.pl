

/******************************************************************/
/***                                                            ***/
/***       SWRL Ontologies:  Anomaly Analysis                   ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      isa/2, equivalent/2,
      derives/2, tc_derives/2,
      connected_classes/3, tc_connected_classes/3,
      property_restriction/3,
      min_cardinality_restriction/3,
      max_cardinality_restriction/3.


/*** interface ****************************************************/


% pure Prolog - subsumption

rule_subsumes_chk(Subsumption, R1, R2) :-
   \+ \+ rule_subsumes(Subsumption, R1, R2).

rule_subsumes(Subsumption, R1, R2) :-
   rule_to_clause(R1, C1),
   rule_to_clause(R2, C2),
   clause_subsumes(Subsumption, C1, C2).

rule_to_clause(A-Bs, [A|Cs]) :-
   negate_atoms(Bs, Cs).

clause_subsumes_chk(Subsumption, As, Bs) :-
   context_module(Module),
   user:clause_subsumes_chk(Module:Subsumption, As, Bs).

clause_subsumes(Subsumption, As, Bs) :-
   context_module(Module),
   user:clause_subsumes(Module:Subsumption, As, Bs).


% pure Prolog

head_predicate(A-_, P) :-
   functor(A, P, _).

body_predicate(_-Bs, P) :-
   member(B, Bs),
   functor(B, P, _).


% Prolog with Datalog - aggregation

siblings(Xs) :-
   sibling(X, _),
   findall( Z,
      sibling(X, Z),
      Xs ).

disjoint_multiple(X, Xs) :-
   checklist( disjoint(X),
      Xs ).


% Prolog with Datalog

tc_derives_atom_(~A, ~B) :-
   !,
   tc_derives_atom_(B, A).
tc_derives_atom_(A, B) :-
   ( A = B
   ; tc_derives_atom(A, B) ).

tc_derives_atom(A1, A2) :-
   A1 =.. [P1|Xs1], A2 =.. [P2|Xs2],
   tc_derives(P1, P2),
   Xs1 = Xs2.

incompatible_atoms(A1, A2) :-
   A1 =.. [P1|Xs1], A2 =.. [P2|Xs2],
   incompatible(P1, P2),
   Xs1 = Xs2.


% Datalog with Built-Ins

in_rule(A) :-
   rule(H-Body),
   ( A = H
   ; member(A, Body) ).

sibling(X, Y) :-
   sub_class(X, Z),
   sub_class(Y, Z),
   X \= Y.


% Datalog

element(E) :-
   ( class(E)
   ; object_property(E)
   ; datatype_property(E)
   ; transitive_property(E)
   ; symmetric_property(E)
   ; functional_property(E)
   ; inverse_functional_property(E) ).

incompatible(X, Y) :-
   ( complement(X, Y)
   ; disjoint(X, Y) ).

equivalent(X, X) :-
   element(X).


/******************************************************************/


