

:- dynamic
      instance/2, sub_class/2,
      restriction/3,
      object_property/1, datatype_property/1,
      functional_property/1,
      inverse_functional_property/1,
      equivalent_property/2, equivalent_class/2.


isa(C1, C2) :-
   sub_class(C1, C2).
isa(C1, C3) :-
   isa(C1, C2),
   sub_class(C2, C3).

derives(B, A) :-
   isa(B, A).
derives(B, A) :-
   rule(A-[B]).

derives(B, A) :-
   equivalent_property(B, A).
derives(B, A) :-
   equivalent_class(B, A).

tc_derives(A, A) :-
   class(A).
tc_derives(A, C) :-
   derives(A, C).
tc_derives(A, C) :-
   derives(A, B),
   tc_derives(B, C).

tc_connected_classes(A, [P], C) :-
   connected_classes(A, P, C).
tc_connected_classes(A, [P|Ps], C) :-
   connected_classes(A, P, B),
   tc_connected_classes(B, Ps, C),
   \+ member(P, Ps).

connected_classes(A, P, D) :-
   tc_derives(A, B),
   property_restriction(B, P, C),
   tc_derives(C, D).

property_restriction(A, P, B) :-
   restriction(hasValue, A, P=B).

min_cardinality_restriction(C, P, M) :-
   restriction(onProperty, C, P-minCardinality:M).

max_cardinality_restriction(C, P, M) :-
   restriction(onProperty, C, P-maxCardinality:M).

equivalent(X, Y) :-
   equivalent(Y, X).
equivalent(X, Y) :-
   equivalent(X, Z), equivalent(Z, Y).

transitive_property(P) :-
   transitive_property(Q),
   equivalent(P, Q).


