rule(p(X)- [q(X)]).
rule(p(X)- [a(X),b(X)]).
rule(q(X)- [a(1),b(X),c(X)]).
rule(r(X)- [a(1),b(2),c(X)]).
rule(a(X)- [b(X)]).
rule(a(X)- [p(X,_)]).
rule(b(X)- [p(X,X)]).
rule(r(X,Y)- [p(X,Z), q(Z,Y)]).
rule(r(X,Y)- [p(Y,X), q(X,_)]).
rule(a- [p(X,Y), q(Y,Z), r(X,Z)]).
transitive_property(p).
symmetric_property(r).
equivalent(p, q).
equivalent(p, r).

% rule(a(X)- [b(X), c(X)]).
% rule(a(X)- [b(X)]).

derives(p, q).
derives(q, p).

complement(a, b).

property_restriction(a, p1, b).
property_restriction(b, p2, a).

class(a).
class(b).

disjoint(b1, b2).
isa(a, b1).
isa(a, b2).


