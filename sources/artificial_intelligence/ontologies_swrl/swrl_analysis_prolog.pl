

/******************************************************************/
/***                                                            ***/
/***       SWRL Ontologies:  Anomaly Analysis                   ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      equivalent/2,
      isa/2,
      derives/2,
      tc_derives/2,
      connected_classes/3,
      tc_connected_classes/3,
      property_restriction/3,
      min_cardinality_restriction/3,
      max_cardinality_restriction/3.


/*** interface ****************************************************/


rule_subsumes_chk(A-As, B-Bs) :-
   clause_subsumes_chk([~A|As], [~B|Bs]).

head_predicate(A-_, P) :-
   functor(A, P, _).

body_predicate(_-Bs, P) :-
   member(B, Bs),
   functor(B, P, _).

tc_derives_atom(A1, A2) :-
   A1 =.. [P1|Xs1], A2 =.. [P2|Xs2],
   tc_derives(P1, P2),
   Xs1 = Xs2.

incompatible_atoms(A1, A2) :-
   A1 =.. [P1|Xs1], A2 =.. [P2|Xs2],
   incompatible(P1, P2),
   Xs1 = Xs2.
   
swrl_subsumes(As, Bs) :-
   copy_term(Bs, Cs),
   subsumes_(As, Bs),
   variant(Bs, Cs).

subsumes_([A|As], Bs) :-
   member(B, Bs), tc_derives(B, A),
   subsumes_(As, Bs).
subsumes_([], _).

subsumes_rule(A-As, B-Bs) :-
   copy_term(Bs, Cs),
   tc_derives(A, B),
   subsumes_(As, Bs),
   variant(Bs, Cs).

in_rule(A) :-
   rule(H-Body),
   ( A = H
   ; member(A, Body) ).

element(E) :-
   ( class(E)
   ; object_property(E)
   ; datatype_property(E)
   ; transitive_property(E)
   ; symmetric_property(E)
   ; functional_property(E)
   ; inverse_functional_property(E) ).

sibling(X, Y) :-
   sub_class(X, Z),
   sub_class(Y, Z),
   X \= Y.

siblings(Xs) :-
   sibling(X, _),
   findall( Z,
      sibling(X, Z),
      Xs ).

disjoint_multiple(X, Xs) :-
   checklist( disjoint(X),
      Xs ).

incompatible(X, Y) :-
   ( complement(X, Y)
   ; disjoint(X, Y) ).

equivalent(X, X) :-
   element(X).

subset_all_different([X|Xs], Ys) :-
   append(Ys_1, [X|Ys_2], Ys),
   append(Ys_1, Ys_2, Ys_3),
   subset_all_different(Xs, Ys_3).
subset_all_different([], _).
 

/******************************************************************/


