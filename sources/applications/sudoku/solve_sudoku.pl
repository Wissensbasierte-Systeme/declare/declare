

/******************************************************************/
/***                                                            ***/
/***       DDK:  Sudoku                                         ***/
/***                                                            ***/
/******************************************************************/


test(solve_sudoku, 1) :-
   solve_sudoku('data_099.pl').
test(solve_sudoku, 2) :-
   solve_sudoku('data_150.pl').
test(solve_sudoku, 3) :-
   solve_sudoku('data_150_b.pl').


/*** interface ****************************************************/


/* solve_sudoku(File) <-
      */

solve_sudoku(File) :-
   dislog_variable_get(source_path, Sources),
   dislog_file_get(home, '/results/dlv_result', Path_O),
   concat([
      Sources, 'projects/sudoku/sudoku.dlv ',
      Sources, 'projects/sudoku/', File ], Paths_I ),
   concat([Sources, 'projects/sudoku/', File], Path_I ),
   dislog_consult(Path_I, Program),
   sudoku_display_model(program, Program),
   program_to_stable_models_dlv_file_for_sudoku(Paths_I, Path_O),
   dlv_file_to_stable_models(Path_O, Models),
   !,
   sudoku_display_models_detailed(Models).

program_to_stable_models_dlv_file_for_sudoku(Paths_I, Path_O) :-
   home_directory(Home),
   concat([
      Home, '/soft/dlv/dlv.i386-linux-elf-gnulibc2.bin ',
      '-silent -N=9 -filter=p ', Paths_I,' > ', Path_O ],
      Command ),
   write(user, 'calling dlv ... '),
   !,
   write_list(user, ['\n', Command, '\n']),
   measure(us(Command)),
   nl(user).


/*** implementation ***********************************************/


/* sudoku_display_models_detailed(Models) <-
      */

sudoku_display_models_detailed(Models) :-
   setintersection(Models, Model),
   ( foreach(M, Models), foreach(N, Models_2) do
        subtract(M, Model, N) ),
   sudoku_display_model(Model),
   program_to_stable_models_dlv(Models_2, State),
   sudoku_state_to_alternatives(State, Int),
   ( Models_2 = [[]]
   ; sudoku_display_model(Int) ),
%  dportray(lp, State),
   length(Models, M),
   nl(user),
   ( M = 1,
     write_list(user, [M, ' solution !'])
   ; write_list(user, [M, ' solutions !']) ).


/* sudoku_state_to_alternatives(State, Int) <-
      */

sudoku_state_to_alternatives(State, Int) :-
   findall( I,
      ( member(Clause, State),
        sudoku_disjunction_to_alternative(Clause, I) ),
      Int ).

sudoku_disjunction_to_alternative(Clause, p(X, Y, M)) :-
   Clause = [p(X, Y, N)|As],
   maplist( sudoku_disjunction_to_alternative(X, Y),
      As, Ns ),
   sort([N|Ns], Ms),
   names_append_with_separator(Ms, ',', M).

sudoku_disjunction_to_alternative(X, Y, p(X, Y, N), N).


/* sudoku_display_models(Models) <-
      */

sudoku_display_models(Models) :-
   checklist( sudoku_display_model(model),
      Models ),
   length(Models, M),
   nl(user),
   write_list(user, [M, ' solutions !']).


/* sudoku_display_model(Mode, X) <-
      */

sudoku_display_model(X) :-
   sudoku_display_model(model, X).

sudoku_display_model(Mode, X) :-
   generate_interval(1, 9, Is),
   findall( Row,
      ( member(J, Is),
        sodoku_get_row(Mode, X, J, Row) ),
      Rows ),
   nl(user),
   writeln_list(user, Rows),
   xpce_display_table(['','','','','','','','',''], Rows).

sodoku_get_row(model, Model, J, Ns) :-
   generate_interval(1, 9, Is),
   ( foreach(I, Is), foreach(N, Ns) do
        ( ( term_to_atom(I, X),
            term_to_atom(J, Y),
            member(p(X, Y, N), Model) )
        ; member(p(I, J, N), Model)
        ; N = ' ' ) ),
   !.
sodoku_get_row(program, Program, J, Ns) :-
   generate_interval(1, 9, Is),
   ( foreach(I, Is), foreach(N, Ns) do
        ( member([p(I, J, N)], Program)
        ; N = ' ' ) ),
   !.


/******************************************************************/


