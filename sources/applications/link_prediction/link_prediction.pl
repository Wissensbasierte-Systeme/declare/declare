

/******************************************************************/
/***                                                            ***/
/***          Declare:  Link Prediction                         ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable(example_path, 'clingo/', Path),
   dislog_variable_set(clingo_example_path, Path).


/*** tests ********************************************************/


test(clingo_link_prediction_explanation_visualization, 1) :-
   File = 'dietmar.lp',
   clingo_link_prediction_explanation_visualization(File).

test(clingo_link_prediction_explanation_visualization, 2) :-
   File = 'ab_count_stable.lp',
   clingo_link_prediction_explanation_visualization(File).

test(clingo_link_prediction_explanation_visualization, 3) :-
   File = 'kinergy.lp',
   clingo_link_prediction_explanation_visualization(File).

test(clingo_link_prediction_explanation_visualization, 4) :-
   File = 'pq_ab_stable.lp',
   clingo_link_prediction_explanation_visualization(File).


/*** interface ****************************************************/


/* clingo_link_prediction_explanation_visualization(File) <-
      */

clingo_link_prediction_explanation_visualization(File) :-
   declare_suggest_file([
      'link_example.lp', 'dietmar.lp', 'cicek.lp',
      'anomalous_links.lp', 'ab_count_stable.lp',
      'daniel.lp'], File),
   dislog_variable(clingo_example_path, File, Path),
   clingo_file_to_explanation_visualization(File, Path).


/* declare_suggest_file(Files, File) <-
      */

declare_suggest_file(Files, File) :-
   ( var(File) ->
     member(File, Files)
   ; true ).


/* clingo_link_predictions <-
      */

clingo_link_predictions :-
   File = 'experiments/graph_medium_count_no_ni.lp',
   dislog_variable(clingo_example_path, File, Path),
%  Ns = [2,3,4,5,6,7,8,9,10,15,20,30,40,50,60,80,100],
   Ns = [2,20],
   clingo_link_predictions(Ns, Path).


/* clingo_link_predictions(Ns, Path) <-
      */

clingo_link_predictions(Ns, Path) :-
   ( foreach(N, Ns), foreach(Characteristics, Table) do
        clingo_link_prediction(N, Path, Characteristics) ),
   xpce_display_table('Clingo', [], Table),
   File_Table = 'clingo_link_predictions_table.tmp',
   dislog_variable(output_path, File_Table, Path_Table),
   predicate_to_file( Path_Table, writeln_list(Table) ).


/* clingo_link_prediction(N, File, Characteristics) <-
      */

clingo_link_prediction(N, File, Characteristics) :-
   dislog_variable(output_path, 'clingo_link_prediction.lp', F),
   clingo_file_prefix_with_constant(N, File, F),
   measure(clingo_file_to_models(F, [Model]), Time),
%  measure(clingo_file_to_models_ddbase(F, [Model]), Time),
   file_remove(F),
   Predicates_1 = [true_pos_/2, neighbours/3, remark/2],
   clingo_select_atoms(Predicates_1, Model, Atoms),
   Predicates_2 = [node/1, edge/2, edge_i/2,
      neighbours/2, neighbours/3, neighbours/4, remark/2],
   clingo_select_atoms(Predicates_2, Model, Graph),
%  star_line,
%  writeq(Model),
%  star_line,
   writelnq(Atoms),
%  writelnq(Graph),
   model_to_characteristics_2(Graph, Cs),
   writelnq(Cs),
%  star_line,
   model_to_characteristics(Model, Tuples),
   append([[ni,N]|Tuples], [[time,Time]], Tuples_2),
   re_pair_list(:, Tuples_2, Tuples_3),
   clingo_characteristics_nicen(Tuples_3, Characteristics),
   writelnq(Characteristics),
   !.

clingo_select_atoms(Predicates, Atoms_1, Atoms_2) :-
   findall( Atom,
      ( member(P/N, Predicates),
        member(Atom_, Atoms_1),
        term_to_atom(Atom, Atom_),
        functor(Atom, P, N) ),
      Atoms_2 ).

clingo_characteristics_nicen(Characteristics, Xs) :-
   Attributes = [ni, edge_t_/2, edge_lp_/2,
      false_neg/2, false_pos/2, true_pos/2, true_pos_/2,
      c_i/2, d_i/2, time],
   ( foreach(A, Attributes), foreach(A:V, Xs) do
        ( member(A:V, Characteristics) ; V = 0 ) ).

clingo_file_prefix_with_constant(N, File_1, File_2) :-
   dislog_variable(
      output_path, 'clingo_link_prediction_const_ni.lp', F),
   multify('*', 66, Stars_), concat(Stars_, Stars),
   write_list(F, [
      '\n\n% ', Stars, '\n\n', '#const ni=', N, '.']),
   us_declare(['cat ', F, ' ', File_1, ' > ', File_2]),
   file_remove(F).


/* clingo_file_to_explanation_visualization(File, Path) <-
      */

clingo_file_to_explanation_visualization(File, Path) :-
   clingo_file_to_explanations(Path, Es_1),
   ( member(File, ['dietmar.lp', 'd.lp',
        'link_example.lp', '2019_11_30_link_example.lp']) ->
     clingo_select_explanations(Es_1, Es_2)
   ; Es_1 = Es_2 ),
   show_explanations_xpce([Es_2]), star_line,
   clingo_explanations_to_rules(Es_1, Rules_1),
   dportray(lp, Rules_1), star_line,
   clingo_explanations_to_rules(Es_2, Rules_2),
   dportray(lp, Rules_2), star_line.


/* clingo_explanations_to_rules(Explanations, Rules) <-
      */

clingo_explanations_to_rules(Explanations, Rules) :-
   findall( H-BN,
      ( member(_-is_supported_by-Rule, Explanations),
        parse_rule(Rule, H-B-N),
        append(B, N, BN) ),
      Rules ).


/* clingo_select_explanations(Es_1, Es_2) <-
      */

clingo_select_explanations(Es_1, Es_2) :-
   findall( E,
      ( member(E, Es_1),
        E = X-is_supported_by-_,
%       functor(X, F, _),
%       member(F, [cn_lp, match, c_attrib])
%       member(X, [edge_lp_i(4, 2), c_i(_, 4, 2),
        member(X, [
%          edge_lp_a(_, _),
           edge_lp_i(_, _), edge_lp(_, _),
           edge_lp_i(_, _, _),
           edge_lp_i(1),
%          edge_lp_i(4, 2), edge_lp(4, 2),
%          c_i(_, 4, 2),
%          c_i(_, 3, 1),
           c_i(_, _, _),
%          c_a(_, _, _),
%          true_pos(4, 2),
           true_pos(_, _),
           false_pos(_, _), false_neg(_, _)]) ),
      Es_2 ).


/******************************************************************/


