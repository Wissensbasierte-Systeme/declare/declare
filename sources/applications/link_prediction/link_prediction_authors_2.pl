

/******************************************************************/
/***                                                            ***/
/***          Declare:  Link Prediction, Authors, 2             ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(link_prediction, link_prediction_edge_double) :-
   link_prediction_data_import,
   link_prediction_edge_double(Xs),
   writeln_list(user, Xs).


/*** data *********************************************************/


link_prediction_data_import :-
%  File = 'link_prediction/2020_10/dblp_v12_lp_edges_export.csv',
   File = 'link_prediction/2020_10/dblp_v12_lp.csv',
   dislog_variable_get(example_path, File, Path),
   read_file_to_csv_row_facts(",", Path).

link_prediction_edge(A, B, V1, Year, Category, V2) :-
   csv_row(N, [A, B, V1, Year_Atom, Category, V2]),
   N > 0,
   atom_number(Year_Atom, Year).


/*** interface ****************************************************/


/* link_prediction_multiple_edges(A, C, Xs) <-
      */

link_prediction_multiple_edges(File) :-
   predicate_to_file( File,
      link_prediction_multiple_edges ).

link_prediction_multiple_edges_refined(File) :-
   predicate_to_file( File,
      link_prediction_multiple_edges_refined ).

link_prediction_multiple_edges :-
   findall( _,
      ( link_prediction_author_2(A),
        write_list(['\n', A, ' -> ']), ttyflush,
        link_prediction_neighbour(A, Neighbours),
        write_list([Neighbours, '\n']),
        ttyflush,
        link_prediction_transitive_collaborator(A, C),
        link_prediction_multiple_edge(A, C, Xs),
        write_list(['   ', [A, C, Xs], '\n']),
        ttyflush ),
      _ ).

link_prediction_multiple_edges_refined :-
   findall( _,
      ( link_prediction_author_2(A),
        write_list(['\n', A, ' -> ']), ttyflush,
        link_prediction_neighbour(A, Neighbours),
        write_list([Neighbours, '\n']),
        ttyflush,
        length(Neighbours, N),
        link_prediction_transitive_collaborator(A, C),
        link_prediction_multiple_edge(A, C, Xs),
        ( member([C, _, _, _, _], Neighbours) ->
%       ( member([_, _, _, _, _], Neighbours) ->
          write_list([' * ', [A, C, Xs], '\n'])
        ; ( ( member(X:_, Xs), link_prediction_hub(X, N) ) ->
%       ; ( \+ ( member(X:_, Xs), \+ link_prediction_hub(X, N) ) ->
            write_list([' - ', [A, C, Xs], '\n'])
          ; write_list([' + ', [A, C, Xs], '\n']),
            ttyflush ) ) ),
      _ ).


/* link_prediction_multiple_edge(+A, +C, -Xs) <-
      */

link_prediction_multiple_edge(A, C, Xs) :-
   findall( B,
      link_prediction_double_edge_stored([A, B, C, _]),
      Ys ),
   length(Ys, N),
   N > 1,
   list_to_multiset(Ys, Xs),
   !.


/* link_prediction_double_edges_store <-
      */

link_prediction_double_edges_store :-
   retract_all(link_prediction_double_edge_stored(_)),
   forall( link_prediction_double_edge(X),
      ( write(user, '.'), ttyflush,
        Edge = link_prediction_double_edge_stored(X),
        assert(Edge) ) ).


/* link_prediction_double_edge([A, B, C, Explanation]) <-
      */

link_prediction_double_edge([A, B, C, Explanation]) :-
   link_prediction_edge(A, B, V_A, Year_A, Category_A, W_A),
   link_prediction_edge(B, C, V_B, Year_B, Category_B, W_B),
   A \= C,
   abs(Year_A - Year_B) < 10,
   \+ link_prediction_edge(A, C, _, _, _, _),
   Explanation =
      [[Category_A, Category_B], [Year_A, Year_B],
       [V_A, V_B], [W_A, W_B]].


/* link_prediction_author_2(Author) <-
      */

link_prediction_author_2(Author) :-
   link_prediction_authors_2(Authors),
   member(Author, Authors).

link_prediction_authors_2(Authors) :-
   findall( Author,
      link_prediction_author_sub_2(Author),
      Authors_2 ),
   sort(Authors_2, Authors).

link_prediction_authors_2(Authors, N) :-
   link_prediction_authors_2(Authors),
   length(Authors, N),
   write_list(user, [N, ' authors\n']).

link_prediction_author_sub_2(Author) :-
   ( link_prediction_edge(Author, _, _, _, _, _)
   ; link_prediction_edge(_, Author, _, _, _, _) ).


/* link_prediction_transitive_collaborator(A, C) <-
      */

link_prediction_transitive_collaborator(A, C) :-
   findall( C,
      link_prediction_double_edge_stored([A, _, C, _]),
      Cs_2 ),
   sort(Cs_2, Cs),
   member(C, Cs),
   A \= C.


/* link_prediction_neighbour(A, Neighbours) <-
      */

link_prediction_neighbour(A, Neighbours) :-
   findall( [B, V_A, Year_A, Category_A, W_A],
      link_prediction_edge(A, B, V_A, Year_A, Category_A, W_A),
      Neighbours ).


/* link_prediction_hubs(Pairs) <-
      */

link_prediction_hubs(Pairs) :-
   A = 31, B = 50,
   findall( [N, M],
      ( between(A, B, N),
        Atom = link_prediction_hub(_, N),
        findall( _, call(Atom), Atoms ),
        length(Atoms, M),
        writeln(N-M) ),
      Pairs ).  


/* link_prediction_hub(Author, N) <-
      */

link_prediction_hub(Author, N) :-
   link_prediction_author_2(Author),
   findall( Atom,
      ( Atom = link_prediction_edge(Author, _, _, _, _, _),
        call(Atom) ),
      Atoms ),
   length(Atoms, M),
   ( var(N) -> N = M
   ; M >= N ).


/* link_prediction_authors_to_facts <-
      */

link_prediction_authors_to_facts :-
   csv_row_facts_to_authors(Authors),
   link_prediction_authors_to_facts(Authors).


/* link_prediction_authors_to_facts(Authors) <-
      */

link_prediction_authors_to_facts(Authors) :-
   retract_all(author_1(_, _)),
   forall( member(A, Authors),
      ( findall( [B, M],
           ( csv_row(_, [A, B, M2, _, _, _]),
             atom_number(M2, M) ),
           As ),
        Fact = author_1(A, As),
        writeln(user, Fact),
        assert(Fact) ) ),
   !.


/* csv_row_facts_to_authors(Authors) <-
      */

csv_row_facts_to_authors(Authors) :-
   findall( Author,
      ( csv_row(N, [Author|_]),
        N \= 0 ),
      Authors_2 ),
   sort(Authors_2, Authors),
   length(Authors_2, I),
   length(Authors, J),
   writeln(user, I->J),
   !.


/* count_all(Predicate/N, K) <-
      */

count_all(Predicate/N, K) :-
   functor(Atom, Predicate, N),
   findall( _,
      call(Atom),
      Atoms ),
   length(Atoms, K).



/******************************************************************/


