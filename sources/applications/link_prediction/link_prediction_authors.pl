

/******************************************************************/
/***                                                            ***/
/***          Declare:  Link Prediction, Authors                ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/



/*** data *********************************************************/


/* icdm_author_files_consult <-
      */

icdm_author_files_consult :-
   dislog_variable_get(example_path, link_prediction, Path),
   F_1 = author_1, F_2 = author_2, F_3 = author_3,
   ( foreach(F, [F_1, F_2, F_3]) do
        concat([Path, '/', F], File),
        write_list(user, ['<--- ', File, '\n']),
        consult(File) ),
   collect_facts(author_1/2, Fs_1), length(Fs_1, N1),
   collect_facts(author_2/2, Fs_2), length(Fs_2, N2),
   collect_facts(author_3/2, Fs_3), length(Fs_3, N3),
   write_list(user,
      [author_1/2:N1, ', ', author_2/2:N2, ', ', author_3/2:N3]).


/* icdm_files_to_fact_files <-
      */

icdm_files_to_fact_files :-
   F_1 = 'link_prediction/author-author-wpapers.ICDM.txt',
   F_2 = 'link_prediction/author-author-wpapers.ICDM.lp',
   F_3 = 'link_prediction/author-label-count.ICDM.txt',
   F_4 = 'link_prediction/author-label-count.ICDM.lp',
   maplist( dislog_variable_get(example_path),
      [F_1, F_2, F_3, F_4], [File_1, File_2, File_3, File_4] ),
   icdm_file_to_fact_file(edge_i, File_1, File_2),
   icdm_file_to_fact_file(edge_a, File_3, File_4),
   write_list(user, ['---> ', File_1, '\n']),
   write_list(user, ['---> ', File_2, '\n']),
   write_list(user, ['---> ', File_3, '\n']),
   write_list(user, ['---> ', File_4, '\n']).


/* icdm_fact_files_consult <-
      */

icdm_fact_files_consult :-
   File_i = 'link_prediction/author-author-wpapers.ICDM.lp',
   File_a = 'link_prediction/author-label-count.ICDM.lp',
   maplist( dislog_variable_get(example_path),
      [File_i, File_a], [Path_i, Path_a] ),
   write_list(user, ['<--- ', Path_i, '\n']),
   consult(Path_i),
   write_list(user, ['<--- ', Path_a, '\n']),
   consult(Path_a).


/*** interface ****************************************************/


/* link_prediction_authors(Authors) <-
      */

link_prediction_authors(Authors) :-
   setof( U, V^S^edge_a(U, V, S), Authors ),
   length(Authors, N),
   writeln(user, N).


/* link_prediction_author(N) <-
      */

link_prediction_author(N) :-
   member(N, [1, 2, 4]),
   concat(author_, N, Predicate),
   Atom =.. [Predicate, _, _],
   retractall(Atom),
   link_prediction_authors(Authors),
   concat('link_prediction/author_', N, File),
   dislog_variable_get(example_path, File, Path), 
   predicate_to_file( Path,
      forall( member(Author, Authors),
         link_prediction_author(N, Author) ) ).
link_prediction_author(3) :-
   concat(author_, 3, Author_3),
   Atom =.. [Author_3, _, _],
   retractall(Atom),
   forall( author_2(A1, Cs1),
      ( findall( [Diff, A2],
           ( author_2(A2, Cs2),
             author_labels_difference(
                Cs1, Cs2, Diff) ),
           Cs3 ),
        sort(Cs3, Cs4),
        last_n_elements(10, Cs4, Cs5),
        writeln(user, author_3(A1, Cs5)), ttyflush,
        link_prediction_author_assert(3, A1, Cs5) ) ),
   concat('link_prediction/author_', 3, File),
   dislog_variable_get(example_path, File, Path),
   predicate_to_file( Path,
      forall( Atom, ( writeq(Atom), writeln('.') ) ) ).
%  collect_prolog_program(user:Author_3/2, Facts),
%  dwrite(pl, Path, Facts).

author_author_profile_profile(A1, A2, Dist_Lables, Dist_Edges, Cs1, Cs2) :-
   author_6(A1, Cs1),
   member([Dist_Lables, Dist_Edges, A2], Cs1),
   author_6(A2, Cs2),
   length(Cs1, N1),
   length(Cs2, N2),
   writeln(N1-N2).


/* author_5(A, N, Ds) <-
      */

author_5(A, N, Ds) :-
   author_5(A, Cs),
   first_n_elements(N, Cs, Ds).

author_5(A, Cs) :-
   author_2(A, Cs1),
   findall( [Diff, A2],
      ( author_2(A2, Cs2),
        author_labels_difference(Cs1, Cs2, Diff) ),
      Cs3 ),
   sort(Cs3, Cs).


/* author_6(A, N, Cs) <-
      */

author_6(A, N, Cs) :-
   ( var(N) -> author_5(A, Cs1)
   ; author_5(A, N, Cs1) ),
   findall( [Dist_Labels, Dist_Edges, A2],
      ( member([Dist_Labels, A2], Cs1),
%       \+ edge_i(A, A2, _),
        edge_i(A, A2, _),
        author_author_distance(A, A2, Dist_Edges),
%       Dist_Edges \= -1,
        true ),
      Cs ).


/* author_7(A, N, Cs) <-
      */

author_7(A, N, Cs) :-
   author_5(A, N, Cs1),
   findall( [Dist_Labels, Dist_Edges, A2],
      ( member([Dist_Labels, A2], Cs1),
        \+ edge_i(A, A2, _),
%       edge_i(A, A2, _),
        author_author_distance(A, A2, Dist_Edges),
%       Dist_Edges \= -1, 
        true ),
      Cs ).


/* authors_6_7(N) <-
      */

authors_6_7(N) :-
   forall( author_6(A, N, Cs_6),
      ( author_7(A, N, Cs_7),
        length(Cs_6, N_6), length(Cs_7, N_7),
        writelnq([A, N_6, N_7]) ) ).


/* link_prediction_author(N, Author) <-
      */

link_prediction_author(1, Author) :-
   findall( [V, T],
      ( U = Author,
        edge_i(U, V, S),
        U \= V,
        term_to_atom(T, S) ),
      Tuples ),
   link_prediction_author_assert(1, Author, Tuples).
link_prediction_author(2, Author) :-
   ddbase_aggregate( [Label, sum(S)],
      ( U = Author,
        edge_i(U, V, S1),
        U \= V,
        edge_a(V, Label, S2),
        term_to_atom(T1, S1),
        term_to_atom(T2, S2),
        S is T1 * T2 ),
      Tuples ),
   link_prediction_author_assert(2, Author, Tuples).
link_prediction_author(4, Author) :-
   author_3(Author, Cs3),
   author_1(Author, Cs1),
   link_prediction_author(4, Cs3, Cs1, Cs),
%  writeln(user, author_4(Author, Cs)), ttyflush,
   link_prediction_author_assert(4, Author, Cs).
   
link_prediction_author(4, Cs1, Cs2, Cs3) :-
   findall( [A, S1, S2],
      ( member([S1, A], Cs1),
        ( member([A, S2], Cs2) -> true
        ; S2 = '?' ) ),
      Cs3 ).


/* co_author_link <-
      */

co_author_link :-
   ddbase_aggregate( [Author_1, Type, length(Author_2)],
      co_author_link(Type, Author_1, Author_2, [_, _, _, _]),
      Tuples ),
   writeq(Tuples),
   Attributes = ['Author', 'Type', 'Atoms'],
   xpce_display_table(Attributes, Tuples).


/* co_author_link(Type, Author_1, Author_2, [S1, S2, S, Dist]) <-
      */

co_author_link(Type, Author_1, Author_2, [S1, S2, S, Dist]) :-
   co_author_link_(Type_2, Author_1, Author_2, [S1, S2, S, Dist]),
   ( Type_2 = false_positive, Dist = -1 ->
     Type = true_negative
   ; Type_2 = false_positive, S2 = '-' ->
     Type = true_negative
   ; Type = Type_2 ).

co_author_link_(Type, Author_1, Author_2, [S1, S2, S, Dist]) :-
   author_1(Author_1, Cs1),
   author_3(Author_1, Cs3),
   co_author_link_(Type, Author_2, Cs3, Cs1, [S1, S2, S]),
   author_author_distance(Author_1, Author_2, Dist).

co_author_link_(true_positive, A, Cs1, Cs2, [S1, S2, S]) :-
   member([S1, A], Cs1),
   member([A, S2], Cs2),
   round(S1/100, 0, S).
co_author_link_(false_positive, A, Cs1, Cs2, [S1, S2, S]) :-
   member([S1, A], Cs1),
   \+ ( no_singleton_variable(S2),
      member([A, S2], Cs2) ),
   S2 = '-',
   round(S1/100, 0, S).
co_author_link_(false_negative, A, Cs1, Cs2, [S1, S2, S]) :-
   member([A, S2], Cs2),
   \+ ( no_singleton_variable(S1),
      member([S1, A], Cs1) ),
   S1 = '-',
   round(S2 * 100, 0, S).


/*** implementation ***********************************************/


/* link_prediction_author_assert(N, Author, Tuples) <-
      */

link_prediction_author_assert(N, Author, Tuples) :-
   concat(author_, N, Predicate),
   Atom =.. [Predicate, Author, Tuples],
   assert(Atom),
   writeq(Atom), writeln('.'), ttyflush.


/* author_author_difference_based_on_labels(A1, A2, Diff) <-
      */

author_author_difference_based_on_labels(A1, A2, Diff) :-
   author_2(A1, Cs1),
   author_2(A2, Cs2),
   author_labels_difference(Cs1, Cs2, Diff).


/* author_labels_difference(Cs1, Cs2, Diff) <-
      */

author_labels_difference(Cs1, Cs2, Diff) :-
   ddbase_aggregate( [sum(S)],
      ( ( member([L,V1], Cs1),
          member([L,V2], Cs2),
          abs(V1-V2, S) )
      ; ( member([L,V1], Cs1),
          \+ member([L,_], Cs2),
          abs(V1, S) )
      ; ( member([L,V2], Cs2),
          \+ member([L,_], Cs1),
          abs(V2, S) ) ),
      [[D]] ),
   round(D, 3, Diff).


/* author_author_distance(A1, A2, Diff) <-
      */

author_author_distance(A1, A2, Diff) :-
   ddbase_aggregate( [max_save(T)],
      ( edge_i(A1, A2, S),
        term_to_atom(T, S)
      ; edge_i(A1, A3, S1),
        edge_i(A3, A2, S2),
        A1 \= A2,
        term_to_atom(T1, S1),
        term_to_atom(T2, S2),
        round(T1 * T2, 3, T) ),
      [[Diff]] ),
   !.
author_author_distance(_, _, -1).

max_save(Xs, Max) :-
   max(Xs, Max),
   !.
max_save([], '?').


/* link_prediction_anomaly(U, V, S) <-
      */

link_prediction_anomaly(U, V, S) :-
   edge_i(U, V, S),
   \+ ( edge_a(U, L, _),
        edge_a(V, L, _) ).


/* icdm_file_to_fact_file(Predicate, File_1, File_2) <-
      */

icdm_file_to_fact_file(Predicate, File_1, File_2) :-
   read_icdm_tuples(File_1, Tuples),
   icdm_tuples_to_facts(Predicate, Tuples, Facts),
   predicate_to_file(File_2,
      maplist( writelnq_with_dot, Facts ) ).

writelnq_with_dot(Fact) :-
   writeq(Fact),
   writeln('.').


/* read_icdm_tuples(File, Tuples) <-
      */

read_icdm_tuples(File, Tuples) :-
   dread(csv('\t'), File, Tuples).


/* icdm_tuples_to_facts(Predicate, Tuples, Facts) <-
      */

icdm_tuples_to_facts(Predicate, Tuples, Facts) :-
   maplist( icdm_tuple_to_fact(Predicate),
      Tuples, Facts ).

icdm_tuple_to_fact(Predicate, Tuple, Fact) :-
   Tuple = [A, B, W],
   Fact =.. [Predicate, A, B, W].


/******************************************************************/


