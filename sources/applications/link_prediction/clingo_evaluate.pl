

/******************************************************************/
/***                                                            ***/
/***          Declare:  Clingo Evaluate                         ***/
/***                                                            ***/
/******************************************************************/


:- home_directory(
      '/soft/clingo/clingo-5.3.0-linux-x86_64/', Clingo),
   dislog_variable_set(clingo, Clingo).

:- op(600, fx, count),
   op(640, fx, #),
   op(400, xfy, ..),
   op(650, fy, not),
   op(750, xfx, '!=').


/*** tests ********************************************************/


test(clingo, clingo_file_to_models(1)) :-
   dislog_variable(clingo_example_path,
      'ab_stable.lp', Path),
   clingo_file_to_models(Path).
test(clingo, clingo_file_to_models(2)) :-
   dislog_variable(clingo_example_path,
      'ab_count_stable.lp', Path),
   clingo_file_to_models(Path).
test(clingo, clingo_file_to_models(3)) :-
   dislog_variable(clingo_example_path,
      'anomalous_links.lp', Path),
   clingo_file_to_models(Path).


/*** interface ****************************************************/


/* clingo_file_to_models(File, Models) <-
      */

clingo_file_to_models(File) :-
   clingo_file_to_models(File, Models),
   forall( nth(N, Models, Model),
      ( write_list(['I_', N, ' =']),
        dportray(hi, Model) ) ),
   models_to_characteristics(Models, Tuples),
   maplist( list_to_tuple_atom, Tuples, Atoms),
   concat_with_separator(Atoms, ', ', A),
   write_list(['Characteristics = { ', A, ' } \n']),
   Attributes = ['Model', 'Predicate','Atoms'],
   xpce_display_table('Clingo', Attributes, Tuples).

clingo_file_to_models(File, Models) :-
   dislog_variable_get(
      output_path, 'clingo_evaluate_prune.lp', File_Prune),
   write_list(user, [File, ' ---> ', File_Prune, '\n']),
   clingo_prune_file(File, File_Prune),
   dislog_variable_get(clingo, 'clingo', Clingo),
   dislog_variable_get(output_path, 'clingo.json', Result),
   concat([Clingo, ' 0 --outf=2 ', File_Prune, ' > ', Result],
      Command),
   writeln(user, Command),
   writeln(user, 'Clingo ... '), ttyflush,
   ( us(Command)
   ; true ),
   clingo_read_models(Result, Models),
   file_remove(Result),
   file_remove(File_Prune),
   !.


/* models_to_characteristics(Models, Tuples) <-
      */

models_to_characteristics(Models, Tuples) :-
   ddbase_aggregate( [N, Predicate_Arity, length(Atom)],
      ( nth(N, Models, Model),
        member(Atom, Model),
        term_to_atom(Term, Atom),
        functor(Term, Predicate, Arity),
        Predicate_Arity = Predicate/Arity ),
      Tuples ).


/* model_to_characteristics(Model, Tuples) <-
      */

model_to_characteristics(Model, Tuples) :-
   ddbase_aggregate( [Predicate_Arity, length(Atom)],
      ( member(Atom, Model),
        term_to_atom(Term, Atom),
        functor(Term, Predicate, Arity),
        Predicate_Arity = Predicate/Arity ),
      Tuples ).

model_to_characteristics_2(Model, Tuples) :-
   ddbase_aggregate( [Predicate_Arity, length(Atom)],
      ( member(Atom, Model),
        functor(Atom, Predicate, Arity),
        Predicate_Arity = Predicate/Arity ),
      Tuples ).


/* clingo_read_models(File, Models) <-
      */

clingo_read_models(File, Models) :-
   clingo_read(File, Json),
   Json = json(Xs),
   nth(3, Xs, Call),
   Call = ('Call'=[json(['Witnesses'=Ws])]),
   ( foreach(W, Ws), foreach(Model, Models) do
        W = json(['Value'=Model]) ).

clingo_read(File, Json) :-
   open(File, read, Alias, [alias(input)]),
   json:json_read(Alias, Json),
%  writeln(json(Json)), wait,
   close(input).


/******************************************************************/


