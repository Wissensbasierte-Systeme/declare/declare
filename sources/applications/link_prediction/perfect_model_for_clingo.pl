

/******************************************************************/
/***                                                            ***/
/***        Declare:  Perfect Model for Clingo File             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* perfect_model_prolog_for_clingo_file(File, Model) <-
      */

perfect_model_prolog_for_clingo_file(File, Model) :-
   F_2 = 'clingo_file_exchange_constants.tmp',
   dislog_variable_get(output_path, F_2, File_2),
   clingo_file_exchange_constants(File, File_2),
   clingo_read_rules(File_2, Rules),
   file_remove(File_2),
   clingo_rules_to_prolog_rules(Rules, Prolog),
   perfect_model_prolog(Prolog, [], I),
   maplist( term_to_atom, I, Model ).


/* clingo_file_exchange_constants(File_1, File_2) <-
      */

clingo_file_exchange_constants(File_1, File_2) :-
   clingo_file_determine_constants(File_1, Assignment),
   read_file_to_name(File_1, Name_1),
   ( foreach(A:V, Assignment),
     foreach([B, W], Substitution) do
        ( name(A, B), name(V, W) ) ),
   name_exchange_sublist(Substitution, Name_1, Name_2),
   predicate_to_file( File_2, write(Name_2) ).

clingo_file_determine_constants(File, Assignment) :-
   read_file_to_lines(File, Lines),
   findall( A:V,
      ( member(Line, Lines),
        concat('#const ', X, Line),
        concat(Y, '.', X),
        term_to_atom(A=V, Y) ),
      Assignment ).


/* clingo_rules_to_prolog_rules(Rules, Prolog) <-
      */

clingo_rules_to_prolog_rules(Rules, Prolog) :-
   maplist( clingo_rule_to_prolog_rule, Rules, Prolog ).

clingo_rule_to_prolog_rule([A], A) :-
   !.
clingo_rule_to_prolog_rule([A]-Body_1, (A:-Body_2)) :-
   maplist( clingo_atom_to_prolog_atoms, Body_1, Atomss ),
   append(Atomss, Atoms_2),
   list_to_comma_structure(Atoms_2, Body_2).

clingo_atom_to_prolog_atoms(Atom_1, [Atom_2]) :-
   Atom_1 = '!='(X, Y),
   !,
   Atom_2 = \=(X, Y).
clingo_atom_to_prolog_atoms(Atom_1, [Atom_2]) :-
   Atom_1 = not(Goal),
   !,
   Atom_2 = \+(Goal).
clingo_atom_to_prolog_atoms(Atom_1, Atoms_2) :-
   Atom_1 = (N = #count { V:Goal }),
   !,
   Atoms_2 = [
      ddbase_aggregate( [length(V)], Goal, [[W]] ),
      W = N ].
clingo_atom_to_prolog_atoms(Atom, [Atom]).


/******************************************************************/


