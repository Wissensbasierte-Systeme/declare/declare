

/******************************************************************/
/***                                                            ***/
/***          Declare:  Clingo Explanations                     ***/
/***                                                            ***/
/******************************************************************/


/*** tests ********************************************************/


test(clingo, clingo_rules_to_memo_rules) :-
   Rules = [[a(X)]-[b(X), c(X)], [d(e)]],
   clingo_rules_to_memo_rules(Rules, Memo),
   star_line, dwrite(lp, Memo), star_line.

test(clingo_explanations, 1) :-
   dislog_variable_get(
      example_path, 'clingo/ab_stable.lp', Path),
   clingo_file_to_explanations(Path, Explanations),
   star_line, writeln_list(Explanations), star_line.

test(clingo_explanations, 2) :-
   dislog_variable_get(
      example_path, 'clingo/ab_stable.lp', Path),
   dislog_variable_get(
      output_path, 'clingo_evaluate.lp', File),
   clingo_file_extend_by_memo_rules(Path, Memo, File),
   clingo_file_to_models(File, Models),
   ( foreach(Model, Models) do
        ( dportray(chs, [Model]),
	  clingo_model_extract_explanations(
	     Model, Memo, Explanations),
          star_line, writeln_list(Explanations), star_line ) ).
      

/*** interface ****************************************************/


/* clingo_file_to_explanations(Path, Explanations) <-
      */

clingo_file_to_explanations(Path, Explanations) :-
   dislog_variable_get(output_path, 'clingo_evaluate.lp', File),
   clingo_file_extend_by_memo_rules(Path, Memo, File),
   clingo_file_to_models(File, Models),
%  clingo_file_to_models_ddbase(File, Models),
   !,
   member(Model, Models),
   write(user, 'Model ='),
   dportray(hi, Model),
   clingo_model_extract_explanations(
      Model, Memo, Explanations).

clingo_file_to_models_ddbase(File, [Model]) :-
   perfect_model_prolog_for_clingo_file(File, Model).


/* clingo_model_extract_explanations(
         Model, Memo, Explanations) <-
      */

clingo_model_extract_explanations(Model, Memo, Explanations) :-
   findall( B-is_supported_by-([B]-Bs1-Bs2),
      ( member([A]-[B|Bs], Memo), member(Name, Model),
        term_to_atom(D, Name), A = D,
        clingo_atoms_separate(Bs, Bs1-Bs2) ),
      Explanations ),
   !.

clingo_atoms_separate(As, Bs1-Bs2) :-
   sublist( clongo_atom_qualifies, As, Bs1 ),
   sublist( not_clongo_atom_qualifies, As, Bs2 ).

clongo_atom_qualifies(A) :-
   \+ not_clongo_atom_qualifies(A).

not_clongo_atom_qualifies(A) :-
   ( functor(A, '!=', 2) ; functor(A, not, 1)
   ; A = (_='#'(count({}(_:_)))) ).


/* clingo_file_extend_by_memo_rules(File_1, Memo, File_2) <-
      */

clingo_file_extend_by_memo_rules(File_1, Memo, File_2) :-
   dislog_variable(output_path, 'clingo_memo_1.lp', F_1),
   dislog_variable(output_path, 'clingo_memo_2.lp', F_2),
   clingo_read_rules(File_1, Rules_1),
   clingo_rules_to_memo_rules(Rules_1, Memo),
   clingo_write_rules(F_1, Memo),
   clingo_memo_rules_to_predicates(Memo, Predicates),
   clingo_write_show_predicates(F_2, Predicates),
   us_declare(
      ['cat ', File_1, ' ', F_1, ' ', F_2, ' > ', File_2]),
   file_remove(F_1), file_remove(F_2),
   !.

clingo_memo_rules_to_predicates(Memo, Predicates) :-
   findall( rule_fired/N,
      ( member([A]-_, Memo),
        functor(A, rule_fired, N) ),
      Predicates_2 ),
   sort(Predicates_2, Predicates).

clingo_write_show_predicates(File, Predicates) :-
   predicate_to_file( File,
      ( nl,
        ( foreach(Predicate, Predicates) do
             write_list(['#show ', Predicate, '.\n']) ),
        nl, nl ) ).


/* clingo_rules_to_memo_rules(Rules_1, Rules_2) <-
      */

clingo_rules_to_memo_rules(Rules_1, Rules_2) :-
   clingo_rules_to_memo_rules(1, Rules_1, Rules_2).

clingo_rules_to_memo_rules(N, [R1|Rs1], [R2|Rs2]) :-
   clingo_rule_to_memo_rule(N, R1, R2),
   M is N + 1,
   clingo_rules_to_memo_rules(M, Rs1, Rs2).
clingo_rules_to_memo_rules(_, [], []).

clingo_rule_to_memo_rule(N, Rule_1, Rule_2) :-
   Rule_1 = [A]-Bs1,
   !,
   clingo_atoms_separate(Bs1, Cs1-_),
   term_to_variables([A]-Cs1, Vs),
   A2 =.. [rule_fired, N|Vs],
   Rule_2 = [A2]-[A|Bs1].
clingo_rule_to_memo_rule(N, Rule_1, Rule_2) :-
   Rule_1 = [A],
   term_to_variables(Rule_1, Vs),
   A2 =.. [rule_fired, N|Vs],
   Rule_2 = [A2]-[A].


/******************************************************************/


