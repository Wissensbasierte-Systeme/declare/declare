

/******************************************************************/
/***                                                            ***/
/***          Declare:  Clingo Interface                        ***/
/***                                                            ***/
/******************************************************************/


/*** tests ********************************************************/


test(clingo, clingo_interface_write) :-
   Head = [pn(X1,X2)],
   Body = [node(X1), node(X2), not(edge(X1, X2)),
      '!='(X1, X2),
      n_attrib = #(count({}(X3:c_attrib(X3, X1, X2))))],
   Rule = Head-Body,
   star_line, writelnq(Rule),
   star_line, dwrite(lp, [Rule]),
   star_line, dportray(lp, [Rule]),
   star_line, clingo_write_rules(user, [Rule]),
   star_line.

test(clingo, clingo_interface_read_write) :-
   dislog_variable_get(example_path, '/clingo/ab.lp', Path),
   clingo_read_rules(Path, Rules),
   clingo_write_rules(user, Rules).


/*** interface ****************************************************/


/* clingo_write_rules(File, Rules) <-
      */

clingo_write_rules(File, Rules) :-
   dislog_variable_get(output_path, 'clingo_write_rules.lp', F),
   predicate_to_file(F, dportray(lp, Rules)),
%  predicate_to_file( F, clingo_write_rules(Rules) ),
   clingo_prune_file(F, File),
   file_remove(F).

clingo_prune_file(File_1, File_2) :-
   read_file_to_string(File_1, String_1),
   name(Name_1, String_1),
   Substitution = [ [" #","#"], [" {","{"] ],
%     [">_",">G_"], ["!=_","!=G_"],
%     [" _"," G_"],
%     ["(_","(G_"], [",_",",G_"],
   name_exchange_sublist(Substitution, Name_1, Name_2),
   predicate_to_file(File_2, write(Name_2)).

clingo_write_rules_(File, Rules) :-
   predicate_to_file( File,
      clingo_write_rules(Rules) ).

clingo_write_rules(Rules) :-
   maplist( clingo_write_rule, Rules ).


/* clingo_write_rule(Rule) <-
      */

clingo_write_rule([A]-Bs) :-
   !,
   clingo_write_atom(A), write(' :-\n   '),
   clingo_write_atoms(Bs), writeln('.').
clingo_write_rule([A]) :-
   clingo_write_atom(A), writeln('.').


/* clingo_write_atoms(Atoms) <-
      */

clingo_write_atoms(Atoms) :-
   write_list_with_separators(
      clingo_write_atom_, clingo_write_atom, Atoms ),
   !.


/* clingo_write_atom_(A) <-
      */

clingo_write_atom_(A) :-
   clingo_write_atom(A), write(',\n   ').


/* clingo_write_atom(Atom) <-
      */

clingo_write_atom(N='#'(count({}(X)))) :-
   write(N),
   write('=#count{'),
   write(X),
   write('}').
clingo_write_atom(not(A)) :-
   write('not '), write(A).
clingo_write_atom('!='(X,Y)) :-
   write(X), write('!='), write(Y).
clingo_write_atom(Atom) :-
   write(Atom).


/* clingo_read_rules(File, Rules) <-
      */

clingo_read_rules(File, Rules) :-
   dislog_variable_get(output_path, 'clingo_tmp.lp', F),
   clingo_file_convert_to_lp_file(File, F),
   dread(lp, F, Rules),
   file_remove(F).


/* clingo_file_convert_to_lp_file(File_1, File_2) <-
      */

clingo_file_convert_to_lp_file(File_1, File_2) :-
   dread(txt, File_1, Name_1),
   Substitution = [
      ["{"," {"],
%     ["}","})"],
      ["#const","% #const"],
      ["#show", "% #show"],
      ["=#", "= #"],
      ["!=", "'!='"] ],
   name_exchange_sublist(Substitution, Name_1, Name_2),
   predicate_to_file( File_2, write(Name_2) ).


/* clingo_file_remove_quotes(File_1, File_2) <-
      */

clingo_file_remove_quotes(File_1, File_2) :-
   dread(txt, File_1, Name_1),
   Substitution = [
      ["'\"","\""],
      ["\"'","\""] ],
   name_exchange_sublist(Substitution, Name_1, Name_2),
   predicate_to_file( File_2, write(Name_2) ).


/******************************************************************/


