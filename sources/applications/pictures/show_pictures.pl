

/******************************************************************/
/***                                                            ***/
/***       DDK:  Pictures                                       ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* directory_to_htmls(Directory, Htmls) <-
      */

directory_to_html_file(Directory, File) :-
   directory_to_htmls(Directory, Htmls),
   dwrite(html, File, html:Htmls).

directory_to_htmls(Directory, Htmls) :-
   directory_contains_files_recursive(
      [jpg, 'JPG'], Directory, Picture_Files),
   split(6, Picture_Files, Fs),
   maplist( picture_files_to_html,
      Fs, Htmls ).


/*** implementation ***********************************************/


/* picture_files_to_html(Xs, Hs) <-
      */

picture_files_to_html(Xs, ['<br>', Ys]) :-
   maplist( picture_file_to_html,
      Xs, Ys ).

picture_file_to_html(File, Html) :-
%  Html = img:[src:File, width:100, height:80, border:0]:[].
   Html = img:[src:File, width:100, border:0]:[].


/******************************************************************/


