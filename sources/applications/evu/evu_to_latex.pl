

/******************************************************************/
/***                                                            ***/
/***               EVU:  LaTeX Ouput                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tuples_to_latex_file(Fach,File) <-
      */

tuples_to_latex_file(Fach,File) :-
   predicate_to_file( File,
      ( counter_tuple(Fach,Tuples_1),
        tuples_to_latex(counter_tuple,Tuples_1),
        selection_tuple([1,6],Fach,Tuples_2),
        tuples_to_latex([1,6],Tuples_2),
        selection_tuple([j,t,n],Fach,Tuples_3),
        tuples_to_latex([j,t,n],Tuples_3) ) ).


/*** implementation ***********************************************/


/* tuples_to_latex(Type, Tuples) <-
      */

tuples_to_latex(Type, Tuples) :-
   foreach(Tuple, Tuples) do
      tuple_to_percents(Type, Tuple, Percent),
      evu_percent_to_latex(Percent).


/* tuple_to_percents(Type, N:Pairs_1, N:Pairs_2) <-
      */

tuple_to_percents(counter_tuple, Tuple, Tuple).

tuple_to_percents([1,6], N:Pairs_1, N:Pairs_4) :-
%  Pairs_1 = [avg:Avg_1|Pairs_2],
   Pairs_1 = [avg:_Avg_1|Pairs_2],
   pair_lists(:,Values,Numbers_2,Pairs_2),
   add(Numbers_2,M),
   ( ( M == 0, Q = 1 )
   ; Q is 1 / M ),
   multiply_list(Numbers_2,100*Q,Numbers_3),
   maplist( truncate_2,
      Numbers_3, Numbers_4 ),
   scalar_product([1.0,0.8,0.6,0.4,0.2,0.0],Numbers_3,Avg_2),
   truncate_2( Avg_2, Avg_6 ),
   pair_lists(:,Values,Numbers_4,Pairs_3),
%  Avg_4 is ( 6 - Avg_1 ) * 20,
%  truncate_2( Avg_4, Avg_5 ),
   Pairs_4 = [avg:Avg_6|Pairs_3].

tuple_to_percents([j,t,n], N:Pairs_1, N:Pairs_3) :-
   pair_lists(:,Values,Numbers_1,Pairs_1),
   add(Numbers_1,M),
   ( ( M == 0, Q = 1 )
   ; Q is 1 / M ),
   multiply_list(Numbers_1,100*Q,Numbers_2),
   maplist( truncate_2,
      Numbers_2, Numbers_3 ),
   pair_lists(:,Values,Numbers_3,Pairs_3).

truncate_2(X,Y) :-
   Y is truncate( 10 * X ) / 10.


/* evu_percent_to_latex <-
      */

evu_percent_to_latex(N:Value) :-
   member(N,[semester,1,3,5,10,16,17,18,19,20,30]),
   !,
   evu_question(N,Command),
   write_list(['\\newcommand{\\evu',Command,'}']),
   evu_percent_print_nice_for_latex(Value), nl.
evu_percent_to_latex(
      N:["Ja":Xj,"Teils":Xt,"Nein":Xn,"nicht erfolgt":Xne]) :-
   !,
   evu_question(N,Command),
   checklist( evu_percent_print_nice_for_latex(Command),
      [j:Xj, t:Xt, n:Xn, ne:Xne] ).
evu_percent_to_latex(71:_) :-
   !.
evu_percent_to_latex(N:[avg:Avg,1:X1,2:X2,3:X3,4:X4,5:X5,6:X6]) :-
   evu_question(N,Command),
   name(NL,[10]),
   write_list([
      '\\newcommand{\\evu',Command,
      '}{\\evuquestionss{',NL,'   \\',Command,'}']),
   checklist( evu_percent_print_nice_for_latex,
      [X1,X2,X3,X4,X5,X6,Avg] ),
   writeln('}').

evu_percent_print_nice_for_latex(Command, Suffix:X) :-
   write_list(['\\newcommand{\\evu',Command,Suffix,'}']),
   evu_percent_print_nice_for_latex(X), nl.

evu_percent_print_nice_for_latex(X) :-
   integer(X),
   !,
   write_list(['{',X,'.0}']).
evu_percent_print_nice_for_latex(X) :-
   round(X,2,Y),
   write_list(['{',Y,'}']).


/******************************************************************/


