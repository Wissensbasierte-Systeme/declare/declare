

/******************************************************************/
/***                                                            ***/
/***               EVU:  Configuration                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


evu_subjects([
   test,
   informatik,
   mathematik
   ]).

:- dislog_variables_set([
%     course_number_randoms - exist,
      course_number_randoms - missing,
%     evu_evaluation_mode - fach
      evu_evaluation_mode - vorlesung
   ]).

semester_id('14').
gueltig_bis('2006-08-15').
frage_gruppe(20052).

% evu_semester('ws0506').
  evu_semester('ss06').


/*** implementation ***********************************************/


% evu_dir('WS_05_06').
% evu_dir('SS_05').

evu_dir(Dir) :-
  evu_semester(Semester),
  name(Semester, [_, _|Xs]),
  name('WS_', [W, S, U]),
  ( Xs = [A, B, C, D],
    name(Dir, [W, S, U, A, B, U, C, D])
  ; Xs = [A, B],
    name(Dir, [S, S, U, A, B]) ).

evu_directory(home, Directory) :-
   home_directory(Home),
   concat(Home,
      '/organisation/Studiendekan/EVU/', Directory).

evu_directory(Path) :-
   evu_directory(home, Directory),
   evu_dir(Dir),
   concat([Directory, Dir, '/Vorbereitung/'], Path).

evu_file(vorlesungs_verzeichnis, File) :-
   evu_directory(Dir),
   evu_semester(Semester),
   concat([to_replace_, Semester, '_pruned.html'], F),
   name_exchange_sublist(
      [["to_replace_ss", "vv"], ["to_replace_ws", "vv"]],
      F, F2),
   concat([Dir, 'vorlesungen/', F2], File).

umfrage_im_jahr(N) :-
   semester_id(N).

semester_id_name(Name) :-
   semester_id(Id),
   concat(['\'', Id, '\''], Name).

  evu_db_name('evu').
% evu_db_name('evudb').

maximal_random_number(999999999).


/******************************************************************/


