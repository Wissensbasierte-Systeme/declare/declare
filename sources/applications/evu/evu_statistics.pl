

/******************************************************************/
/***                                                            ***/
/***             EVU:  Statistics                               ***/
/***                                                            ***/
/******************************************************************/


:- module( evu_statistics, [
      evu_statistics_remaining_percent/0 ] ).

 
evu_directory_studiendekan(Dir) :-
   home_directory(Home),
   concat(Home, '/organisation/Studiendekan/', Dir).


/*** interface ****************************************************/


/* evu_statistics_remaining_percent <-
      */

evu_statistics_remaining_percent :-
   evu_directory_studiendekan(Dir),
   concat(Dir, '2006_11_25/table.csv', File_1),
   evu_statistics_students(File_1, Rows_1),
   writeln_list(Rows_1),
   rows_to_remaining_percent(Rows_1, Rows_2),
   rows_to_remaining_percent_averages(Rows_2, [R1, R2]),
   concat(Dir, '2006_11_25/table_percents.csv', File_2),
   generate_interval(1, 14, Is),
   openoffice_display_table(File_2, [''|Is], [R1, R2|Rows_2]).


/*** implementation ***********************************************/


/* rows_to_remaining_percent(Rs1, Rs2) <-
      */

rows_to_remaining_percent([R1|Rs1], [R2|Rs2]) :-
   R1 = [Semester, X|_],
   findall( Z,
      ( nth(N, Rs1, R),
        M is N + 2,
        nth(M, R, Y),
        round(100*Y/X, 0, Z) ),
      Zs ),
   R2 = [Semester, 100|Zs],
   rows_to_remaining_percent(Rs1, Rs2).
rows_to_remaining_percent([], []).


/* rows_to_remaining_percent_averages(Rows, [R1, R2]) <-
      */

rows_to_remaining_percent_averages(Rows, [R1, R2]) :-
   first(Rows, R),
   findall( [Avg_1, Avg_2],
      ( nth(N, R, _),
        N > 1, 
        rows_to_nth_average(odd, N, Rows, Avg_1),
        rows_to_nth_average(even, N, Rows, Avg_2) ),
      Pairs ),
   pair_lists(Avgs_1, Avgs_2,  Pairs),
   R1 = ['WS'|Avgs_1],
   R2 = ['SS'|Avgs_2].

rows_to_nth_average(Pred, N, Rows, Avg) :-
   findall( X,
      ( nth(M, Rows, Row),
        nth(N, Row, X),
        apply(Pred, [M]) ),
      Xs ),
   average(Xs, Avg_2),
   round(Avg_2, 0, Avg). 


/* evu_statistics_students(File, Rows) <-
      */

evu_statistics_students(File, Rows) :-
   read_star_office_file(File, [_|Rows_2]),
   maplist( evu_statistics_row_to_numbers,
      Rows_2, Rows ).

evu_statistics_row_to_numbers(Xs, Ys) :-
   foreach(X, Xs), foreach(Y, Ys) do
      ( name_remove_elements(both, " ", X, Z),
        name_to_number(Z, Y) ).


/******************************************************************/


