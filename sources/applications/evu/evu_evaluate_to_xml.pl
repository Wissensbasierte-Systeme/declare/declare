

/******************************************************************/
/***                                                            ***/
/***             EVU:  Evaluation with Mysql                    ***/
/***                                                            ***/
/******************************************************************/


evu_evaluate_fach :-
   evu_evaluate(fach, 9, 'Informatik').
evu_evaluate_vorlesung :-
   evu_evaluate(vorlesung, 9, 10601).


/*** interface ****************************************************/


/* evu_evaluate(Mode, Semester, Fach) <-
      */

evu_evaluate(fach, Semester, Fach) :-
   dislog_variable_set(evu_evaluation_mode, fach),
   evu_evaluate_prepare(Semester, Fach),
   evu_item_to_xml(Fach, Xml_1),
   dislog_variable_get(source_path, Sources),
   concat(Sources,
      'projects/evu/evu_evaluate_xml_to_html', Fng),
   fn_transform_fn_item_fng(Fng, Xml_1, Xml_2),
   evu_sem_id_to_semester(Semester, Semester_Name),
   A = align:center, 
   Description = table:[
      border:1, bgcolor:yellow, A, width:'50%']:[
      tr:[A]:[
         th:[A]:['Semester'], th:[A]:['Fach'] ],
      tr:[A]:[
         td:[A]:[Semester_Name], td:[A]:[Fach] ] ],
   Xml = html:[
      head:[title:'EVU-Auswertung'],
      body:[Description, br:[], Xml_2] ],
   html_item_to_display(Xml).
evu_evaluate(vorlesung, Semester, Vnr) :-
   evu_sem_id_to_semester(Semester, Semester_Name),
   dislog_variable_get(output_path, Results),
   name_exchange_elements([" _"], Semester_Name, Semester_Name_2),
   concat([Results,
      'evu_tmp_', Semester_Name_2, '_', Vnr, '.html'], Path),
   evu_evaluate(vorlesung, Semester, Vnr, Xml),
   html_item_to_display(Path, Xml).

evu_evaluate(vorlesung, Semester, Vnr, Xml) :-
   dislog_variable_set(evu_evaluation_mode, vorlesung),
   evu_evaluate_prepare(Semester, Vnr),
   evu_item_to_xml(Vnr, Xml_1),
   dislog_variable_get(source_path, Sources),
   concat(Sources,
      'projects/evu/evu_evaluate_xml_to_html', Fng),
   fn_transform_fn_item_fng(Fng, Xml_1, Xml_2),
   evu_sem_id_to_semester(Semester, Semester_Name),
   evu_sem_id_vnr_title_lecturer(Semester, Vnr, Title, Lecturer),
   A = align:center, 
   Description = table:[
      border:1, bgcolor:yellow, A, width:'100%']:[
      tr:[A]:[
         th:[A]:['Semester'], th:[A]:['Vnr'],
         th:[A]:['Vorlesung'], th:[A]:['Dozent'] ],
      tr:[A]:[
         td:[A]:[Semester_Name], td:[A]:[Vnr],
         td:[A]:[Title], td:[A]:[Lecturer] ] ],
   evu_comments_html(Semester, Vnr, Rows),
   Comments = table:[border:1, A, width:'100%']:[
      tr:[
         th:[align:left]:[b:['Übungsleiter']],
         th:[align:left]:[b:['Kommentar']] ] | Rows ],
   Xml = html:[
      head:[title:'EVU-Auswertung'],
      body:[Description, br:[], Xml_2, br:[], Comments] ].


/* html_item_to_display(Path, Html) <-
      */

html_item_to_display(Html) :-
   dislog_variable_get(output_path, Results),
   concat(Results, 'html_item_tmp.html', Path),
   html_item_to_display(Path, Html).

html_item_to_display(Path, Html) :-
   dwrite(xml, Path, Html),
   html_file_prune_umlaute(Path),
   html_file_to_display(Path).
%  predicate_to_file( Path,
%     dwrite(xml, Html) ),
%  html_file_prune_umlaute(Path).


/* evu_item_to_xml(Item, Xml) <-
      */

evu_item_to_xml(Item, Xml) :-
   participants(Item, Participants),
   evu_tuples_to_xml(counter_tuple,
      [participants:Participants], Xmls_0),
   evu_item_to_xml(counter_tuple, Item, Xmls_1),
   evu_item_to_xml([1,6], Item, Xmls_2),
   evu_item_to_xml([j,t,n], Item, Xmls_3),
   append([Xmls_0, Xmls_1, Xmls_2, Xmls_3], Xmls_4),
   sort(Xmls_4, Xmls),
   Xml = questions:Xmls.


/* evu_item_to_xml(Type, Item, Xmls) <-
      */

evu_item_to_xml(Type, Item, Xmls) :-
   ( Type = counter_tuple ->
     counter_tuple(Item, Tuples)
   ; selection_tuple(Type, Item, Tuples) ),
   evu_tuples_to_xml(Type, Tuples, Xmls).


/* evu_tuples_to_xml(Type, Tuples, Xmls) <-
      */

evu_tuples_to_xml(Type, Tuples, Xmls) :-
   foreach(Tuple, Tuples), foreach(Xml, Xmls) do
      tuple_to_percents(Type, Tuple, Percent),
      evu_percent_to_xml(Percent, Xml).

evu_percent_to_xml(N:As, X) :-
   As = ["Ja":Xj,"Teils":Xt,"Nein":Xn,"nicht erfolgt":Xne],
   Bs = ['Ja':Xj,'Teils':Xt,'Nein':Xn,'nicht_erfolgt':Xne],
   !,
   evu_question_for_number(N, Question),
   X = question:[
      id:N, text:Question, type:jtn|Bs]:[].
evu_percent_to_xml(71:_, void).
evu_percent_to_xml(N:As, X) :-
   As = [avg:Avg,1:X1,2:X2,3:X3,4:X4,5:X5,6:X6],
   !,
   evu_question_for_number(N, Question),
   X = question:[
      id:N, text:Question, type:mark,
      avg:Avg,v1:X1,v2:X2,v3:X3,v4:X4,v5:X5,v6:X6]:[].
evu_percent_to_xml(N:Value, X) :-
   evu_question_for_number(N, Question),
   X = question:[
      id:N, text:Question, type:value, value:Value]:[].


/* evu_question_for_number(N, Question) <-
      */

evu_question_for_number(N, Question) :-
   evu_questions(Questions),
   member([N, _, _, Question], Questions).


/* group_evu_questions(Items, Groups) <-
      */

group_evu_questions(Items, Groups) :-
   foreach(G, [
      'Allgemeines', 'Vorlesungsstoff', 'Dozent',
      'Präsentation', 'Übungsleiter/in', 'Übungsaufgaben' ]),
   foreach(Group, Groups) do
      ( findall( Item,
           ( member(Item, Items),
             N := Item@id,
             evu_question_to_group(N, G) ),
           Group_2 ),
        Group = [tr:[td:[b:[G]]]|Group_2] ).


/* evu_question_to_group(Id, Group) <-
      */

evu_question_to_group(semester, Group) :-
   !,
   Group = 'Allgemeines'.
evu_question_to_group(participants, Group) :-
   !,
   Group = 'Allgemeines'.
evu_question_to_group(N, 'Allgemeines') :-
   term_to_atom_between(1, 5, N),
   !.
evu_question_to_group(N, 'Vorlesungsstoff') :-
   term_to_atom_between(6, 10, N), !.
evu_question_to_group(N, 'Dozent') :-
   term_to_atom_between(11, 15, N), !.
evu_question_to_group(N, 'Präsentation') :-
   term_to_atom_between(16, 24, N), !.
evu_question_to_group(N, 'Übungsleiter/in') :-
   term_to_atom_between(25, 28, N), !.
evu_question_to_group(N, 'Übungsaufgaben') :-
   term_to_atom_between(29, 32, N), !.

term_to_atom_between(I, J, N) :-
   term_to_atom(M, N),
   between(I, J, M).


/* evu_average_to_html_colour(Avg, Colour) <-
      */

evu_average_to_html_colour(Avg, '#00ff00') :-
   80 =< Avg.
evu_average_to_html_colour(Avg, '#ccff00') :-
   70 =< Avg, Avg < 80.
evu_average_to_html_colour(Avg, '#ffff00') :-
   60 =< Avg, Avg < 70.
evu_average_to_html_colour(Avg, '#ffcc00') :-
   Avg < 60.


/* evu_comments_html(Semester, Vnr, Rows) <-
      */

evu_comments_html(Semester, Vnr, Rows) :-
   evu_comments(Semester, Vnr, Tuples),
   findall( Row,
      ( member(Tuple, Tuples),
        evu_comment_to_html(Tuple, Row) ),
      Rows ).

evu_comment_to_html(['NULL', 'NULL'], _) :-
   !,
   fail.
evu_comment_to_html([Lecturer, Text], Row) :-
   evu_comment_null_prune(Lecturer, L),
   evu_comment_null_prune(Text, T),
   Row = tr:[td:[L], td:[T]].

evu_comment_null_prune('NULL', '') :-
   !.
evu_comment_null_prune(X, X).


/******************************************************************/


