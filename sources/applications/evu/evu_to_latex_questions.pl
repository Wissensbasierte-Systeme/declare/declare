

/******************************************************************/
/***                                                            ***/
/***               EVU:  Fragen in LaTeX                        ***/
/***                                                            ***/
/******************************************************************/


evu_question(1,allgemeinesa).
evu_question(2,allgemeinesb).
evu_question(3,allgemeinesc).
evu_question(4,allgemeinesdb).
evu_question(semester,allgemeinese).
evu_question(5,allgemeinesf).

evu_question(6,vorlesunga).
evu_question(7,vorlesungb).
evu_question(8,vorlesungc).
evu_question(9,vorlesungd).
evu_question(10,vorlesunge).

evu_question(11,dozenta).
evu_question(12,dozentb).
evu_question(13,dozentc).
evu_question(14,dozentd).
evu_question(15,dozente).

evu_question(16,praesentationaa).
evu_question(17,praesentationab).
evu_question(18,praesentationac).
evu_question(19,praesentationad).
evu_question(20,praesentationae).
evu_question(21,praesentationb).
evu_question(22,praesentationc).
evu_question(23,praesentationd).
evu_question(24,praesentatione).

evu_question(25,uebungsleitera).
evu_question(26,uebungsleiterb).
evu_question(27,uebungsleiterc).
evu_question(28,uebungsleiterd).

evu_question(29,uebungsaufgabenaa).
evu_question(30,uebungsaufgabenab).
evu_question(31,uebungsaufgabenb).
evu_question(32,uebungsaufgabenc).


/******************************************************************/


