

/******************************************************************/
/***                                                            ***/
/***             EVU:  Evaluation with Mysql                    ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      selection_tuple/3,
      counter_tuple/2,
      participants/2.


:- discontiguous
      selection_tuple_get/4,
      selection_tuple_get/6,
      selection_tuple_for_question_get/3,
      selection_tuple_for_question_get/6.


/*** interface ****************************************************/


/* evu_evaluate_to_latex(Semester,Fach) <-
      */

evu_evaluate_to_latex(Semester, Fach) :-
   dislog_variable_set(evu_evaluation_mode, fach),
   writeln_list(['', 'EVU Evaluate'-Fach-Semester, '']),
   evu_evaluate_prepare(Semester, Fach),
   dislog_variable_get(home, DisLog),
   concat([DisLog, '/results/answers_', Fach, '_', Semester],
      File),
   writeln_list(['', '---> output file:', File]),
   tuples_to_latex_file(Fach, File).


/*** implementation ***********************************************/


/* evu_evaluate_prepare(Semester,Fach) <-
      */

evu_evaluate_prepare(Semester,Fach) :-
   retractall(selection_tuple(_,_,_)),
   retractall(counter_tuple(_,_)),
   retractall(participants(_,_)),
   evu_announce('Evaluation of questions with marks 1 to 6'),
   selection_tuple_get(
      [1,6],Semester,Fach,Selection_Tuple_16),
   writeln(Selection_Tuple_16),
   assert(
      selection_tuple([1,6],Fach,Selection_Tuple_16) ),
   evu_announce('Evaluation of semesters'),
   counter_tuple_get(Semester,Fach,X),
   writeln(X),
   evu_announce('Evaluation of usage of media'),
   evu_intquest_tuple_media_get(Semester,Fach,Y),
   writeln(Y),
   append(X,Y,Counter_Tuple),
   assert(
      counter_tuple(Fach,Counter_Tuple) ),
   evu_announce('Evaluation of yes/no questions'),
   selection_tuple_get(
      [j,t,n],Semester,Fach,Selection_Tuple_jtn),
   writeln(Selection_Tuple_jtn),
   assert(
      selection_tuple([j,t,n],Fach,Selection_Tuple_jtn) ),
   evu_announce('Evaluation of participants'),
   evu_participants(Semester,Fach,Participants),
   write_list([Participants, ' participants \n']),
   assert(
      participants(Fach,Participants) ),
   evu_announce('Evaluation of number of evaluated courses'),
   evu_courses_from_mysql(Semester, Fach, N),
   write_list(user, [N, ' courses have been evaluated \n']).

evu_announce(Text) :-
   nl,
   write_list(['*** ', Text, ' *** \n']).


/* selection_tuple_get([j,t,n],Semester,Fach,Tuple) <-
      */

selection_tuple_get([j,t,n],Semester,Fach,Tuple) :-
   evu_strquest_tuples(Semester,Fach,Mysql_Tuples),
   Question_Ids = [
      2, 22, 24, 32 ],
   Answers = [
      ja:"Ja", teils:"Teils", nein:"Nein",
      nicht:"nicht erfolgt" ],
   findall( Question_Id:Pairs,
      ( member(Question_Id,Question_Ids),
        evu_questions(Questions),
        member([Question_Id,G,Q,_],Questions),
        selection_tuple_for_question_get(
           [j,t,n],Mysql_Tuples,G,Q,Answers,Pairs) ),
      Tuple ),
   !.
        
selection_tuple_for_question_get(
      [j,t,n],Mysql_Tuples,G,Q,Answers,Pairs) :-
   findall( B:N,
      ( member(A:B,Answers),
        selection_tuple_for_question_get(
           [j,t,n],Mysql_Tuples,[G,Q,_,A,N]) ),
      Pairs ).
        
selection_tuple_for_question_get(
      [j,t,n],Mysql_Tuples,[G,Q,_,A,N]) :-
   member([G,Q,_,A,N],Mysql_Tuples),
   !.
selection_tuple_for_question_get([j,t,n],_,[_,_,_,_,0]).


/* selection_tuple_get([1,6],Semester,Fach,Tuple) <-
      */

selection_tuple_get([1,6],Semester,Fach,Tuple) :-
   evu_strquest_tuples(Semester,Fach,Mysql_Tuples),
   generate_interval(1,6,Answers),
   Question_Ids = [
      6, 7, 8, 9, 11, 12, 13, 14, 15,
      21, 23, 25, 26, 27, 28, 29, 31 ],
   findall( Question_Id:[avg:Avg|Pairs],
      ( member(Question_Id,Question_Ids),
        evu_questions(Questions),
        member([Question_Id,G,Q,_],Questions),
        selection_tuple_for_question_get(
           [1,6],Mysql_Tuples,G,Q,Answers,Pairs),
        evu_answer_pairs_to_average(Pairs,Avg) ),
      Tuple ),
   !.
        
evu_answer_pairs_to_average(Xs,Avg) :-
   evu_answer_pairs_to_average(0:0,Xs,S:N),
   N \= 0,
   !,
   Avg is S / N.
evu_answer_pairs_to_average(_,'-').

evu_answer_pairs_to_average(S1:N1,[A:N|Xs],S3:N3) :-
   S2 is S1 + A * N,
   N2 is N1 + N,
   evu_answer_pairs_to_average(S2:N2,Xs,S3:N3).
evu_answer_pairs_to_average(S:N,[],S:N).

selection_tuple_for_question_get(
      [1,6],Mysql_Tuples,G,Q,Answers,Pairs) :-
   findall( A:N,
      ( member(A,Answers),
        selection_tuple_for_question_get(
           [1,6],Mysql_Tuples,[G,Q,_,A,N]) ),
      Pairs ).
        
selection_tuple_for_question_get(
      [1,6],Mysql_Tuples,[G,Q,_,A,N]) :-
   member([G,Q,_,A,N],Mysql_Tuples),
   !.
selection_tuple_for_question_get([1,6],_,[_,_,_,_,0]).


/* counter_tuple_get(Semester,Item,Tuple) <-
      */

counter_tuple_get(Semester,Item,Tuple) :-
   evu_intquest_tuples(Semester,Item,Mysql_Tuples),
   Question_Ids = [
      semester, 1, 3, 5, 10, 30 ],
   findall( Question_Id:Avg,
      ( member(Question_Id,Question_Ids),
        evu_questions(Questions),
        member([Question_Id,G,Q,_],Questions),
        counter_tuple_for_question_get(Mysql_Tuples,G,Q,Avg) ),
      Tuple ),
   !.

counter_tuple_for_question_get(Mysql_Tuples,G,Q,Avg) :-
   ( member([_,G,Q,_,Avg],Mysql_Tuples)
   ; number(Q),
     atom_number(Q2,Q),
     member([_,G,Q2,_,Avg],Mysql_Tuples) ),
   !.
counter_tuple_for_question_get(_,_,_,'-').


/* evu_intquest_tuple_media_get(Semester,Fach,Tuple) <-
      */

evu_intquest_tuple_media_get(Semester,Fach,Tuple) :-
   evu_intquest_tuples_media(Semester,Fach,Mysql_Tuples),
   Question_Ids = [
      16, 17, 18, 19, 20 ],
   findall( Question_Id:Avg,
      ( member(Question_Id,Question_Ids),
        evu_questions(Questions),
        member([Question_Id,G,Q,_],Questions),
        evu_intquest_tuple_media_for_question_get(
           Mysql_Tuples,G,Q,Avg) ),
      Tuple_a ),
   norm_pair_lists(100,Tuple_a,Tuple),
   !.

evu_intquest_tuple_media_for_question_get(
      Mysql_Tuples,G,Q,Avg) :-
   member([_,G,Q,_,Avg],Mysql_Tuples),
   !.
evu_intquest_tuple_media_for_question_get(_,_,_,0).


/* evu_count_courses(Fach,N) <-
      */

evu_count_courses(Fach,N) :-
   findall( Vnr,
      vorlesung(fach:Fach, vnr:Vnr, vname:_, dozent:_, tzahl:_),
      Vnrs ),
   length(Vnrs,N),
   writeln(N).


/* norm_pair_lists(Norm_Value,Pairs_1,Pairs_2) <-
      */

norm_pair_lists(Norm_Value,Pairs_1,Pairs_2) :-
   pair_lists(:,Labels,Values_1,Pairs_1),
   add(Values_1,Sum),
   ( Sum = 0 ->
     Pairs_2 = Pairs_1
   ; vector_multiply(Norm_Value/Sum,Values_1,Values_2),
     pair_lists(:,Labels,Values_2,Pairs_2) ).


/*** tests ********************************************************/


test(evu,selection_tuple_get_16) :-
   ( dislog_variable_get(mysql, no)
   ; dislog_variable_set(evu_evaluation_mode, fach),
     selection_tuple_get([1,6],3,'Informatik',Tuple),
     writeln(Tuple) ).
test(evu,counter_tuple_get) :-
   ( dislog_variable_get(mysql, no)
   ; dislog_variable_set(evu_evaluation_mode, fach),
     counter_tuple_get(3,'Informatik',Tuple),
     writeln(Tuple) ).
test(evu,evu_intquest_tuple_media_get) :-
   ( dislog_variable_get(mysql, no)
   ; dislog_variable_set(evu_evaluation_mode, fach),
     evu_intquest_tuple_media_get(3,'Informatik',Tuple),
     writeln(Tuple) ).
test(evu,selection_tuple_get_jtn) :-
   ( dislog_variable_get(mysql, no)
   ; dislog_variable_set(evu_evaluation_mode, fach),
     selection_tuple_get([j,t,n],3,'Informatik',Tuple),
     writeln(Tuple) ).
test(evu,evu_participants) :-
   ( dislog_variable_get(mysql, no)
   ; dislog_variable_set(evu_evaluation_mode, fach),
     evu_participants(3,'Informatik',Participants),
     writeln(Participants) ).


/******************************************************************/


