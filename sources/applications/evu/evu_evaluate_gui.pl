

/******************************************************************/
/***                                                            ***/
/***             EVU:  Evaluation GUI                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* evu_gui <-
      */

evu_gui :-
   Evaluations = ul:[
      li:[a:[href:'prolog:evu_lecturer_overview_html']:[
         'Lecturer Evaluation']],
      li:[a:[href:'prolog:evu_semester_overview_table_html']:[
         'Semester Evaluation']],
      li:[a:[href:'prolog:evu_course_overview']:[
         'Course Overview']] ],
   Html = html:[
      head:[
         meta:['http-equiv':'Content-Type',
            content:'text/html; charset=windows-1252']:[],
         title:[]:['EVU Tool'],
         meta:[content:'MSHTML 6.00.2800.1126', name:'GENERATOR']:[] ],
      body:[bgcolor:'#ffeecc']:[
         h3:[font:[color:'#8698fd']:['EVU - Evaluation Tool']],
%        br:[],
         Evaluations ] ],
   html_to_cms_doc_window('EVU', Html, size(300,200)).
 

/* evu_semester_overview_table_html <-
      */

evu_semester_overview_table_html :-
   evu_semester_overview_table_html(Table),
   Html = html:[
      body:[bgcolor:'#ffffff']:[
         h3:[font:[color:'#8698fd']:['Semester Evaluation']],
%        br:[],
         Table ] ],
   html_to_cms_doc_window(
      'EVU - Semester Evaluation', Html, size(400,300)).


/* evu_lecturer_overview_html(Fach, Lecturers) <-
      */

evu_lecturer_overview_html :-
   evu_lecturer_overview_html(Html),
   html_to_cms_doc_window('EVU - Lecturer Evaluation', Html).

evu_lecturer_overview_html(Html) :-
   evu_lecturer_overview_html('Informatik', ol:I),
   evu_lecturer_overview_html('Mathematik', ol:MI),
   ord_subtract(MI, I, M),
   Html = html:[
      body:[bgcolor:'#ffffff']:[
         h3:[font:[color:'#8698fd']:['Informatik']], ol:I,
         h3:[font:[color:'#8698fd']:['Mathematik']], ol:M ] ].

evu_lecturer_overview_html(Fach, ol:Lecturers_html) :-
%  evu_lecturer_to_course_overview(_, Tuples),
%  evu_lecturer_overview(Fach, Tuples, Lecturers_2),
%  evu_lecturers_prune(Lecturers_2, Lecturers),
   evu_subject_to_teachers(Fach, Teachers),
   findall( li:[a:[href:Prolog_Call]:[Teacher]],
      ( member(Teacher, Teachers),
        concat(['prolog:evu_lecturer_to_course_overview(''',
           Teacher, ''')'], Prolog_Call) ),
      Lecturers_html ).

evu_lecturer_overview(Fach, Tuples, Lecturers) :-
   findall( Lecturer,
      ( member(Tuple, Tuples),
        nth(6, Tuple, Fach),
        nth(5, Tuple, Lecturer) ),
      Lecturers_2 ),
   sort(Lecturers_2, Lecturers).
 

/* evu_lecturer_to_course_overview(Lecturer, Tuples) <-
      */

evu_lecturer_to_course_overview(Lecturer) :-
   evu_lecturer_to_course_overview(Lecturer, Tuples),
   html_display_table_cms_doc(Lecturer, ['Sid', 'Semester', 'Vnr',
      'Vorlesung', 'Dozent', 'Fach', 'Frageboegen'], Tuples).

evu_lecturer_to_course_overview(Lecturer, Tuples) :-
   evu_course_overview(Tuples_2),
   findall( Tuple,
      ( member(Tuple_2, Tuples_2),
        evu_lecturer_to_course_overview_tuple(Lecturer,
           Tuple_2, Tuple) ),
      Tuples ).

evu_lecturer_to_course_overview_tuple(Lecturer, X, Y) :-
   X = [Sem_Id, Sem, Vnr, Vname, Lecturer_2, Fach, Part],
   ( var(Lecturer),
     Lecturer = Lecturer_2
   ; name_contains_name(Lecturer_2, Lecturer) ),
   concat(['prolog:evu_evaluate(', 'vorlesung,', Sem_Id, ',',
      '''', Vnr, '''', ')'], Prolog_Call),
   Y = [Sem_Id, Sem, a:[href:Prolog_Call]:[Vnr],
      Vname, Lecturer_2, Fach, Part],
   !.


/* evu_semester_overview_table_html(Table) <-
      */

evu_semester_overview_table_html(Table) :-
   evu_semester_overview(Tuples),
   maplist( evu_semester_tuple_to_row,
      Tuples, Rows ),
   Header = tr:[
      th:[''], th:[''],
      th:[align:right]:['Informatik'],
      th:[align:right]:['Mathematik'] ],
   Table = table:[
      border:1, bgcolor:'#eeeeee', align:center]:[
      Header|Rows].

evu_semester_tuple_to_row([Sem_Id, Sem, I, M], Row) :-
   concat(['prolog:evu_evaluate(', 'fach,', Sem_Id, ',',
      '''Informatik''', ')'], Prolog_Call_I),
   concat(['prolog:evu_evaluate(', 'fach,', Sem_Id, ',',
      '''Mathematik''', ')'], Prolog_Call_M),
   Row = tr:[
      td:[align:right]:[Sem_Id], td:[align:right]:[Sem],
      td:[align:right]:[a:[href:Prolog_Call_I]:[I]],
      td:[align:right]:[a:[href:Prolog_Call_M]:[M]] ].


/* evu_semester_overview(Tuples) <-
      */

evu_semester_overview(Tuples) :-
   evu_course_overview(Ts),
   findall( [Sem_Id, Sem],
      ( member(T, Ts),
        append([Sem_Id, Sem], _, T) ),
      Tuples_2 ),
   sort(Tuples_2, Tuples_3),
   maplist( evu_semester_to_sheets(Ts),
      Tuples_3, Tuples ).

evu_semester_to_sheets(Tuples,
      [Sem_Id, Sem], [Sem_Id, Sem, I, M]) :-
   evu_semester_to_sheets(
      Tuples, 'Informatik', [Sem_Id, Sem], I),
   evu_semester_to_sheets(
      Tuples, 'Mathematik', [Sem_Id, Sem], M).

evu_semester_to_sheets(Tuples, Subject, [Sem_Id, Sem], XY) :-
   evu_semester_to_sheets(
      courses, Tuples, Subject, [Sem_Id, Sem], X),
   evu_semester_to_sheets(
      sheets, Tuples, Subject, [Sem_Id, Sem], Y),
   concat([X, ' / ', Y], XY).

evu_semester_to_sheets(
      courses, Tuples, Subject, [Sem_Id, Sem], N) :-
   findall( Vnr,
      member([Sem_Id, Sem, Vnr, _, _, Subject, _], Tuples),
      Vnrs ),
   length(Vnrs, N).
evu_semester_to_sheets(
      sheets, Tuples, Subject, [Sem_Id, Sem], N) :-
   findall( N,
      member([Sem_Id, Sem, _, _, _, Subject, N], Tuples),
      Ns ),
   maplist( term_to_atom,
      Ms, Ns ),
   add(Ms, N).


/******************************************************************/


