

/******************************************************************/
/***                                                            ***/
/***               EVU:  Preparation                            ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      course_number_randoms/3,
      vorlesung/1.

:- multifile
      vorlesung/1.


/*** interface ****************************************************/


prepare_tans_and_coupons :-
   create_coupon_files,
   write_insert_statements_tans,
   write_course_number_randoms.

complete_coupons :-
   coupons_latex_to_postscript.

prepare_courses :-
   write_insert_statements_courses.


/*** implementation ***********************************************/


/* coupon_latex_to_postscript <-
      */

coupons_latex_to_postscript :-
   findall( Vnr,
      vorlesung(vnr:Vnr,vname:_,dozent:_,tzahl:_),
      Vnrs ),
   checklist( coupon_latex_to_postscript,
      Vnrs ).

coupon_latex_to_postscript(Vnr) :-
   evu_directory(Path),
   concat(
      ['cd ',Path,'Coupons; latex coupons_',Vnr,'.tex;',
       'dvips ','coupons_',Vnr],
      Command),
   us(Command).


/* read_course_number_randoms <-
      */

read_course_number_randoms :-
   evu_directory(Path),
   concat(Path,'course_number_randoms',File),
   diconsult(File,State),
   checklist( assert_course_number_randoms,
      State ).

assert_course_number_randoms([course_number_randoms(Vnr,N,Tnrs)]) :-
   assert(course_number_randoms(Vnr,N,Tnrs)).

possibly_read_course_number_randoms :-
   dislog_variable_get(course_number_randoms,exist),
   !,
   read_course_number_randoms.
possibly_read_course_number_randoms.


/* write_course_number_randoms <-
      */

write_course_number_randoms :-
   evu_directory(Path),
   concat([Path,'course_number_randoms'],File),
   findall( [course_number_randoms(Vnr,N,Tnrs)],
      course_number_randoms(Vnr,N,Tnrs),
      State ),
   tell(File),
   writeln(':- disjunctive.'), nl,
   checklist( write_course_number_randoms,
      State ),
   nl, writeln(':- prolog.'),
   told.
%  disave(State,File).

write_course_number_randoms([course_number_randoms(Vnr,N,Tnrs)]) :-
   write('course_number_randoms( '),
   write(Vnr), write(', '),
   write(N), writeln(', ['),
   write_list_with_comma(Tnrs).

write_list_with_comma([X1,X2,X3,X4,X5,X6|Xs]) :-
   !,
   tab(3), write_with_comma([X1,X2,X3,X4,X5]), writeln(','),
   write_list_with_comma([X6|Xs]).
write_list_with_comma(Xs) :-
   tab(3), write_with_comma(Xs), writeln(' ]).').


/* create_coupon_files <-
      */

create_coupon_files :-
   dabolish(course_number_randoms/3),
   possibly_read_course_number_randoms,
   dabolish(vorlesung/1),
   consult_evu_vorlesungs_files,
   create_coupon_files_2,
   !.

create_coupon_files_2 :-
   findall( Vnr,
      vorlesung(vnr:Vnr,vname:_,dozent:_,tzahl:_),
      Vnrs ),
   list_to_ord_set(Vnrs,Vnrs_2),
   writeln_list(Vnrs_2),
   checklist( write_coupons,
      Vnrs_2 ).

write_coupons(Vnr) :-
   vorlesung_coupon(vnr:Vnr,vname:Vname,dozent:Dname,tzahl:N),
   get_course_number_randoms(Vnr,N,Tnrs),
   !,
   evu_directory(Path),
   concat([Path,'Coupons/coupons_input_',Vnr,'.tex'],File_Input),
   tell(File_Input),
   write_coupons(Vname,Vnr,Dname,Tnrs),
   told,
   write_coupon_file(Vnr),
   !.
  
get_course_number_randoms(Vnr,N,Tnrs) :-
   dislog_variable_get(course_number_randoms,exist),
   !,
   course_number_randoms(Vnr,N,Tnrs).
get_course_number_randoms(Vnr,N,Tnrs) :-
   get_course_number_randoms(N,Tnrs),
   assert(course_number_randoms(Vnr,N,Tnrs)).
 
get_course_number_randoms(N,L) :-
   maximal_random_number(Max),
   randoms(0,N,Max,L),
   list_to_ord_set(L,S),
   length(S,N).


write_coupons(Vname,Vnr,Dname,[T1,T2|Tnrs]) :-
   write_coupon(Vname,Vnr,Dname,T1),
   write_coupon(Vname,Vnr,Dname,T2),
   writeln('\\vspace*{6.5mm} \\\\'),
   write_coupons(Vname,Vnr,Dname,Tnrs).
write_coupons(Vname,Vnr,Dname,[T1]) :-
   write_coupon(Vname,Vnr,Dname,T1).
write_coupons(_,_,_,[]).


write_coupon(Vname,Vnr,Dname,Tnr) :-
   write_list([
      '\\coupon{',Vname,'}{',Vnr,'}{',Dname,'}{',Tnr,'}']),
   nl.


write_coupon_file(Vnr) :-
   evu_directory(Path),
   concat([Path,'Coupons/coupons_',Vnr,'.tex'],File),
   tell(File),
   writeln('\\documentclass{report}[12pt]'), nl,
   writeln('\\input{coupon_style}'), nl,
   writeln('\\pagestyle{empty}'), nl,
   writeln('\\begin{document}'), nl,
   writeln('\\hspace*{-7mm}'),
   write('\\input{coupons_input_'), write(Vnr), writeln('}'), nl,
   writeln('\\end{document}'), nl,
   told.
   
   
/* write_insert_statements_tans <-
      */

write_insert_statements_tans :-
   evu_directory(Path),
   concat(Path,'tans.sql',File),
   write(user,'---> '), writeln(user,File),
   write_insert_statements_tans(File),
   !.

write_insert_statements_tans(File) :-
   tell(File),
   nl,
   evu_db_name(Evu_DB_Name),
   concat(['use ',Evu_DB_Name,';'],Use_Line),
   writeln(Use_Line),
   nl,
   forall( vorlesung(vnr:Vnr,vname:Vname,dozent:_,tzahl:N),
      ( writeln('#'),
        write_list(['# ',Vnr]), nl,
        write_list(['# ',Vname]), nl,
        write_list(['# ',N,' Studierende ']), nl,
        writeln('#'),
        write_insert_statements_tans(Vnr,N) ) ),
   told.

write_insert_statements_tans(Vnr,N) :-
   course_number_randoms(Vnr,N,Randoms),
   checklist( write_insert_statement_tan(Vnr),
      Randoms ),
   !.

write_insert_statement_tan(Vnr,Tnr) :-
   gueltig_bis(Date),
   semester_id(Semester_Id),
   write_list([
      'INSERT INTO tans ( ',
      'sem_id, cours_id, tan, access, until )']), nl,
   checklist( mysql_print_item,
      ['   VALUES ( ',
       '''',Semester_Id,'''', ', ',
       {Vnr}, ', ', {Tnr}, ', ', 0, ', ', {Date}, ' );' ]),
   nl.

writeln_comma(X) :-
   write_list(['   ',X,',']), nl.


/* write_insert_statements_courses <-
      */

write_insert_statements_courses :-
   evu_directory(Path),
   concat([Path,'courses.sql'],File),
   consult_evu_vorlesungs_files,
   write(user,'---> '), writeln(user,File),
   write_insert_statements_courses(File).

write_insert_statements_courses(File) :-
   semester_id_name(Semester_Id_Name),
   tell(File),
   nl,
   evu_db_name(Evu_DB_Name),
   concat(['use ',Evu_DB_Name,';'],Use_Line),
   writeln(Use_Line),
   nl,
   forall( vorlesung(
              fach:Fach,vnr:Vnr,vname:Vname,dozent:Dozent,tzahl:_),
      write_insert_statement_course(
         Fach,Vnr,Vname,Dozent,Semester_Id_Name) ),
   told.

write_insert_statement_course(Fach,Vnr,Vname,Dozent,Semester_Id) :-
   substitute_in_name(Dozent,Dozent_1),
   substitute_in_name(Vname,Vname_1),
   name_remove_elements("\\",Dozent_1,Dozent_2),
   name_remove_elements("\\",Vname_1,Vname_2),
   write_list([
      'INSERT INTO courses ( ',
      'sem_id, cours_id, title, lecturer, ',
      'theory, practice, subject, field )']), nl,
   write_list(['   VALUES ( ',
      Semester_Id, ', ''', Vnr, ''', ''', Vname_2, ''', ''',
      Dozent_2, ''', NULL, NULL, ''', Fach, ''', NULL );' ]), nl, nl.


substitute_in_name(Name_1,Name_2) :-
   name(Name_1,List_1),
   substitute_in_list(List_1,List_2),
   name(Name_2,List_2).

substitute_in_list(List_1,List_2) :-
   name('\"',L1), name('e',E),
   append(L1,[A],X1), Y1 = [A|E],
   name('{\ss}',X2), name('ss',Y2),
   name('--',X3), name('-',Y3),
   substitute_in_list([X1-Y1,X2-Y2,X3-Y3],List_1,List_2),
   !.
  
substitute_in_list(Substitutions,List_1,List_2) :-
   member(X-Y,Substitutions),
   append(X,Rest_1,List_1),
   substitute_in_list(Substitutions,Rest_1,Rest_2),
   append(Y,Rest_2,List_2).
substitute_in_list(Substitutions,[X|Xs],[X|Ys]) :-
   substitute_in_list(Substitutions,Xs,Ys).
substitute_in_list(_,[],[]).


/* consult_evu_vorlesungs_files <-
      */

consult_evu_vorlesungs_files :-
   evu_subjects(Subjects),
   checklist( consult_evu_vorlesungs_file,
      Subjects ).
   
consult_evu_vorlesungs_file(Subject) :-
   evu_directory(Path),
%  evu_semester(Semester),
   concat([Path, 'vorlesungen/', Subject, '.pl'], File),
   [File].


/* consult_course_number_randoms <-
      */

consult_course_number_randoms :-
   evu_directory(Path),
   concat([Path,'course_number_randoms'],File),
   dconsult(File,State),
   forall( member([course_number_randoms(Vnr,N,Tnrs)],State),
      assert(course_number_randoms(Vnr,N,Tnrs)) ).


/* vorlesung(vnr:Vnr,vname:Vname,dozent:Dozent,tzahl:Tzahl) <-
      */

vorlesung(fach:Fach,vnr:Vnr,vname:Vname,
      dozent:Dozent,tzahl:Tzahl) :-
   vorlesung(Vorlesung),
   [Fach,Vnr,Vname,Dozent,Tzahl] :=
      Vorlesung^[fach,vnr,vname,dozent,tzahl].
vorlesung(vnr:Vnr,vname:Vname,dozent:Dozent,tzahl:Tzahl) :-
   vorlesung(Vorlesung),
   [Vnr,Vname,Dozent,Tzahl] :=
      Vorlesung^[vnr,vname,dozent,tzahl].
vorlesung_coupon(vnr:Vnr,vname:Vname,
      dozent:Dozent,tzahl:Tzahl) :-
   vorlesung(Vorlesung),
   [Vnr,Dozent,Tzahl] :=
      Vorlesung^[vnr,dozent,tzahl],
   ( ( Vname := Vorlesung^vname_short,
       ! )
   ; Vname := Vorlesung^vname ).
 

/******************************************************************/


