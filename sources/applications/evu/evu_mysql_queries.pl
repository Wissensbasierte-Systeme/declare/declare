

/******************************************************************/
/***                                                            ***/
/***             EVU:  Queries to MySQL                         ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      evu_semster_and_item_to_tuples/4.


/*** interface ****************************************************/


/* evu_subject_to_lecturers(Fach, Lecturers) <-
      */

evu_subject_to_lecturers(Fach, Lecturers) :-
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ {{lecturer}} ],
      from:[ courses ],
      where:[ subject = {Fach} ] ],
      Tuples_2 ),
   sort(Tuples_2, Tuples),
   ( foreach([X], Tuples), foreach(L, Lecturers) do
        name_remove_elements("\t", X, L) ).


/* evu_strquest_tuples(Semester,Item,Tuples) <-
      */

evu_strquest_tuples_for_question(Semester,Vnr,[X,Y],Tuples) :-
   dislog_variable_set(evu_evaluation_mode, vorlesung),
   evu_strquest_tuples(Semester,Vnr,Tuples_2),
   findall( Tuple,
      ( member(Tuple, Tuples_2),
        append([X,Y],_,Tuple) ),
      Tuples ),
   writeln_list(Tuples).

evu_semster_and_item_to_tuples(
      strquest, Semester, Item, Tuples) :-
   evu_strquest_tuples(Semester, Item, Tuples).

evu_strquest_tuples(Semester,Fach,Tuples) :-
   dislog_variable_get(evu_evaluation_mode, fach),
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ a^group_id, a^quest_id,
         {{s^text_s}}, answer, count(*) ],
      from:[ answ_gen-a, strquest-s, courses-z ],
      where:[ a^sem_id = Semester and
         a^sem_id = z^sem_id and
         a^cours_id = z^cours_id and
         z^subject = {Fach} and
%        s^sem_id = Semester and
         a^group_id = s^group_id and
         a^quest_id = s^quest_id ],
%        a^answer \= '0' ],
      group_by:[ a^group_id, a^quest_id,
         s^text_s, answer ] ],
      Tuples_2 ),
   maplist( evu_strquest_tuple_clean,
      Tuples_2, Tuples ).
evu_strquest_tuples(Semester,Vnr,Tuples) :-
   dislog_variable_get(evu_evaluation_mode, vorlesung),
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ a^group_id, a^quest_id,
         {{s^text_s}}, answer, count(*) ],
      from:[ answ_gen-a, strquest-s ],
      where:[ a^sem_id = Semester and
         a^cours_id = {Vnr} and
%        s^sem_id = Semester and
         a^group_id = s^group_id and
         a^quest_id = s^quest_id ],
%        a^answer \= '0' ],
      group_by:[ a^group_id, a^quest_id,
         s^text_s, answer ] ],
      Tuples_2 ),
   maplist( evu_strquest_tuple_clean,
      Tuples_2, Tuples ).

evu_strquest_tuple_clean(
      [Group,Question,Text_1,Answer,Count],
      [Group,Question,Text_2,Answer,Count] ) :-
   name_remove_elements(both,[9],Text_1,Text_2).


/* evu_participants(Semester,Item,Participants) <-
      */

evu_semster_and_item_to_tuples(
      participants, Semester, Item, Participants) :-
   evu_participants(Semester, Item, Participants).

evu_participants(Semester,Fach,Participants) :-
   dislog_variable_get(evu_evaluation_mode, fach),
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ count(*) ],
      from:[ answers-a, courses-z ],
      where:[ a^sem_id = Semester and
         a^sem_id = z^sem_id and
         a^cours_id = z^cours_id and
         z^subject = {Fach} ] ],
      [[Participants]] ),
   !.
evu_participants(Semester,Vnr,Participants) :-
   dislog_variable_get(evu_evaluation_mode, vorlesung),
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ count(*) ],
      from:[ answers-a ],
      where:[ a^sem_id = Semester and
         a^cours_id = {Vnr} ] ],
      [[Participants]] ),
   !.


/* evu_courses_from_mysql(Semester,Item,Courses) <-
      */

evu_courses_from_mysql(Semester,Fach,Courses) :-
   dislog_variable_get(evu_evaluation_mode, fach),
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ {{a^cours_id}} ],
      from:[ answers-a, courses-z ],
      where:[ a^sem_id = Semester and
         a^sem_id = z^sem_id and
         a^cours_id = z^cours_id and
         z^subject = {Fach} ] ],
      Tuples ),
   append(Tuples, Course_List),
   list_to_ord_set(Course_List,Course_Set),
   writeln(user,Course_Set),
   length(Course_Set,Courses),
   !.
evu_courses_from_mysql(Semester,Vnr,Courses) :-
   dislog_variable_get(evu_evaluation_mode, vorlesung),
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ {{a^cours_id}} ],
      from:[ answers-a ],
      where:[ a^sem_id = Semester and
         a^cours_id = {Vnr} ] ],
      Tuples ),
   append(Tuples, Course_List),
   list_to_ord_set(Course_List,Course_Set),
   writeln(user,Course_Set),
   length(Course_Set,Courses),
   !.


/* evu_intquest_tuples_media(Semester,Item,Tuples) <-
      */

evu_semster_and_item_to_tuples(
      intquest_media, Semester, Item, Tuples) :-
   evu_intquest_tuples_media(Semester, Item, Tuples).

evu_intquest_tuples_media(Semester,Fach,Tuples) :-
   dislog_variable_get(evu_evaluation_mode, fach),
   evu_participants(Semester,Fach,Participants),
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ a^sem_id, a^group_id, {{a^quest_id}},
         {{i^text_s}}, sum(answer)/Participants ],
      from:[ answ_gen-a, intquest-i, courses-z ],
      where:[ a^sem_id = Semester and
         a^sem_id = z^sem_id and
         a^cours_id = z^cours_id and
         z^subject = {Fach} and
%        i^sem_id = Semester and
         a^group_id = 4 and
         i^group_id = 4 and
         a^quest_id = i^quest_id and
         a^answer \= '0' ],
      group_by:[ a^sem_id,
         a^group_id, a^quest_id, i^text_s ] ],
      Tuples_2 ),
   maplist( evu_intquest_tuple_clean,
      Tuples_2, Tuples ).
evu_intquest_tuples_media(Semester,Vnr,Tuples) :-
   dislog_variable_get(evu_evaluation_mode, vorlesung),
   evu_participants(Semester,Vnr,Participants),
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ a^sem_id, a^group_id, {{a^quest_id}},
         {{i^text_s}}, sum(answer)/Participants ],
      from:[ answ_gen-a, intquest-i ],
      where:[ a^sem_id = Semester and
         a^cours_id = {Vnr} and
%        i^sem_id = Semester and
         a^group_id = 4 and
         i^group_id = 4 and
         a^quest_id = i^quest_id and
         a^answer \= '0' ],
      group_by:[ a^sem_id,
         a^group_id, a^quest_id, i^text_s ] ],
      Tuples_2 ),
   maplist( evu_intquest_tuple_clean,
      Tuples_2, Tuples ).


/* evu_intquest_tuples(Semester,Item,Tuples) <-
      */

evu_semster_and_item_to_tuples(
      intquest, Semester, Item, Tuples) :-
   evu_intquest_tuples(Semester, Item, Tuples).

evu_intquest_tuples(Semester,Fach,Tuples) :-
   dislog_variable_get(evu_evaluation_mode, fach),
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ a^sem_id, a^group_id, {{a^quest_id}},
         {{i^text_s}}, avg(answer) ],
      from:[ answ_gen-a, intquest-i, courses-z ],
      where:[ a^sem_id = Semester and
         a^sem_id = z^sem_id and
         a^cours_id = z^cours_id and
         z^subject = {Fach} and
%        i^sem_id = Semester and
         a^group_id = i^group_id and
         a^quest_id = i^quest_id and
         a^answer \= '0' ],
      group_by:[ a^sem_id, a^group_id, a^quest_id,
         i^text_s ] ],
      Tuples_2 ),
   maplist( evu_intquest_tuple_clean,
      Tuples_2, Tuples ).
evu_intquest_tuples(Semester,Vnr,Tuples) :-
   dislog_variable_get(evu_evaluation_mode, vorlesung),
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ a^sem_id, a^group_id, {{a^quest_id}},
         {{i^text_s}}, avg(answer) ],
      from:[ answ_gen-a, intquest-i ],
      where:[ a^sem_id = Semester and
         a^cours_id = {Vnr} and
%        i^sem_id = Semester and
         a^group_id = i^group_id and
         a^quest_id = i^quest_id and
         a^answer \= '0' ],
      group_by:[ a^sem_id, a^group_id, a^quest_id,
         i^text_s ] ],
      Tuples_2 ),
   maplist( evu_intquest_tuple_clean,
      Tuples_2, Tuples ).


/* evu_intquest_tuple_clean(Tuple_1, Tuple_2) <-
      */

evu_intquest_tuple_clean(
      [Sem_Id,Group,Question_1,Text_1,Avg],
      [Sem_Id,Group,Question_2,Text_2,Avg] ) :-
   name_exchange_elements(["ef"],Question_1,Question_a),
   name_remove_elements(both,[9],Question_a,Question_2),
   name_remove_elements(both,[9],Text_1,Text_2).


/* evu_sem_id_to_semester(Id, Semester) <-
      */

evu_sem_id_to_semester(Id, Semester) :-
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ {{text_s}} ],
      from:[ semester ],
      where:[ sem_id = Id ] ],
      [[Value]] ),
   name_remove_elements("\t", Value, Semester).


/* evu_sem_id_vnr_title_lecturer(Id, Vnr, Title, Lecturer) <-
      */

evu_sem_id_vnr_title_lecturer(Id, Vnr, Title, Lecturer) :-
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ {{title}}, {{lecturer}} ],
      from:[ courses ],
      where:[ sem_id = Id and
         cours_id = {Vnr} ] ],
      [[T, L]|_] ),
   name_remove_elements("\t", T, Title),
   name_remove_elements("\t", L, Lecturer).


/* evu_total_number_of_participants(Fach, Total) <-
      */

evu_total_number_of_participants(Fach, Total) :-
   member(Fach, ['Informatik', 'Mathematik', 'Test']),
   evu_course_overview(Tuples),
   findall( X,
      ( member(Tuple, Tuples),
        append(_, [Fach, X], Tuple) ),
      Xs ),
   add(Xs, Total).


/* evu_course_overview(Tuples) <-
      */

evu_course_overview :-
   evu_course_overview(Tuples_1),
   ( foreach(Tuple_1, Tuples_1), foreach(Tuple_2, Tuples_2) do
        ( foreach(Value_1, Tuple_1), foreach(Value_2, Tuple_2) do
             html_name_prune_umlaute(Value_1, Value_2) ) ),
   html_display_table('EVU', ['Sid', 'Semester', 'Vnr',
      'Vorlesung', 'Dozent', 'Fach', 'Teilnehmer'], Tuples_2).

evu_course_overview(Semester_List, Faecher, Tuples) :-
   evu_course_overview(Tuples_2),
   findall( Tuple,
      ( member(Tuple, Tuples_2),
        nth(1, Tuple, Semester),
        member(Semester, Semester_List),
        nth(6, Tuple, Fach),
        member(Fach, Faecher) ),
      Tuples ).
 
evu_course_overview(Tuples) :-
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ sem^sem_id, {{sem^text_s}}, {{s^cours_id}},
         {{t^title}}, {{lecturer}}, {{t^subject}}, count(*) ],
      from:[ tans-s, courses-t, semester-sem ],
      where:[ access = 1 and
         t^sem_id = s^sem_id and
         s^cours_id = t^cours_id and
         sem^sem_id = s^sem_id ],
      group_by:[ t^sem_id, sem^text_s,
         t^title, s^cours_id, lecturer, t^subject ] ],
      Tuples_2 ),
   maplist( evu_course_overview_clean_tuple,
      Tuples_2, Tuples_3 ),
   sort(Tuples_3, Tuples ),
   !.

evu_course_overview_clean_tuple(Xs, Ys) :-
   maplist( name_remove_elements("\t"),
      Xs, Zs ),
   ( foreach(Z, Zs), foreach(Y, Ys) do
        name(Z, Ns), name(Y, Ns) ).


/* evu_comments(Semester,Vnr,Tuples) <-
      */

evu_comments(Semester,Vnr,Tuples) :-
   dislog_variable_get(evu_evaluation_mode, vorlesung),
   evu_db_name(Database),
   mysql_select_execute([
      use:[ Database ],
      select:[ {{lect2}}, {{comment}} ],
      from:[ answers ],
      where:[ sem_id = Semester and
         cours_id = {Vnr} ] ],
      Tuples_2 ),
   maplist( evu_course_overview_clean_tuple,
      Tuples_2, Tuples ),
   !.


/******************************************************************/


