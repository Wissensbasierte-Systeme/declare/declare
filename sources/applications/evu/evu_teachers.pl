

/******************************************************************/
/***                                                            ***/
/***             EVU:  Teachers                                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* evu_teacher_overview(Fach, Pairs) <-
      */

evu_teacher_overview(Fach, Pairs) :-
   evu_subject_to_teachers(Fach, Lecturers, Teachers),
   maplist( evu_lecturer_to_all_names(Lecturers),
      Teachers, Pairs ),
   html_display_table('Teachers', ['Teacher', 'Names'], Pairs).


/* evu_subject_to_teachers(Fach, Lecturers, Teachers) <-
      */

evu_subject_to_teachers(Fach, Teachers) :-
   evu_subject_to_teachers(Fach, _, Teachers).

evu_subject_to_teachers(Fach, Lecturers, Teachers) :-
   evu_subject_to_lecturers(Fach, Lecturers),
   evu_lecturers_to_teachers(Lecturers, Teachers).

evu_lecturers_to_teachers(Lecturers, Teachers) :-
   maplist( name_split_at_position([", ", ","]),
      Lecturers, Xs ),
   append(Xs, Teachers_2),
   evu_lecturers_prune(Teachers_2, Teachers).


/* evu_lecturer_to_all_names(Lecturers, Lecturer, Tuple) <-
      */

evu_lecturer_to_all_names(Lecturers, Lecturer, Tuple) :-
   findall( Name,
      ( member(Name, Lecturers),
        name_contains_name(Name, Lecturer) ),
      Names ),
   Tuple = [Lecturer|Names].


/* evu_lecturers_prune(Lecturers_1, Lecturers_2) <-
      */

evu_lecturers_prune(Lecturers_1, Lecturers_2) :-
   evu_lecturer_names_strip_titles(Lecturers_1, Lecturers_3),
   evu_lecturers_to_minimal_names(Lecturers_3, Lecturers_2).


/* evu_lecturer_name_strip_title(Name_1, Name_2) <-
      */

evu_lecturer_names_strip_titles(Lecturers_1, Lecturers_2) :-
   maplist( evu_lecturer_name_strip_title,
      Lecturers_1, Lecturers ),
   sort(Lecturers, Lecturers_2).

evu_lecturer_name_strip_title(Name_1, Name_2) :-
   ( name_contains_name(Name_1, 'Dr. ')
   ; name_contains_name(Name_1, 'Drs. ') ),
   !,
   name_start_after_position(["Dr. ", "Drs. "], Name_1, Name_2).
evu_lecturer_name_strip_title(Name, Name).


/* evu_lecturers_to_minimal_names(Lecturers_1, Lecturers_2) <-
      */

evu_lecturers_to_minimal_names(Lecturers_1, Lecturers_2) :-
   findall( X,
      ( member(X, Lecturers_1),
        \+ ( member(Y, Lecturers_1),
             X \= Y,
             name_contains_name(X, Y) ) ),
      Lecturers_2 ).


/******************************************************************/


