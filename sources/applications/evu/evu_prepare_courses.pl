

/******************************************************************/
/***                                                            ***/
/***                EVU:  Prepare Courses                       ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* evu_vv_to_pl <-
      */

evu_vv_to_pl :-
   evu_file(vorlesungs_verzeichnis,Path_1),
   xml_file_to_fn_term(Path_1, FN_Term),
   Substitutions = [
      (td:Es)-(td:_:Es),
      (b:E)-(b:_:[E]),
      (b:E)-(i:_:[E]) ],
   fn_transform_elements(Substitutions,
      FN_Term, FN_Term_2 ),
   concat(Path_1,'.pl', Path_2),
   findall( T,
      ( E := FN_Term_2^_^tr,
        vv_entry_to_tuple(E,T) ),
      Ls ),
   list_to_ord_set(Ls,Ls_2),
   writeln(user, Ls_2),
   predicate_to_file( Path_2,
      checklist( writeln_vv_atom('Semester', 'Fach'),
         Ls_2 ) ).


/* evu_vorlesungen_to_xml(Subject) <-
      */

evu_vorlesungen_to_xml_informatik :-
   evu_vorlesungen_to_xml(informatik).

evu_vorlesungen_to_xml(Subject) :-
   dabolish(vorlesung/1),
   evu_directory(Path),
   concat([Path, 'vorlesungen/', Subject], File),
   concat(File, '.pl', File_pl),
   concat(File, '.xml', File_xml),
   [File_pl],
   findall( Course,
      ( vorlesung(As),
        lists:delete(As, vname:Vname, As_1),
        lists:delete(As_1, vname_short:Vname_short, As_2),
        lists:delete(As_2, dozent:Teacher, As_3),
        Course = vorlesung:As_3:[
           vname:Vname, vname_short:Vname_short,
           dozent:Teacher ] ),
      Courses ),
   dwrite(xml, File_xml, vorlesungen:[file:Subject]:Courses).


/*** implementation ***********************************************/


/* vv_entry_to_tuple(Entry, [Vnr, Name, Teachers]) <-
      */

vv_entry_to_tuple(Entry, [Vnr, Name, Teachers]) :-
   writeln(user, Entry),
%  Entry = [td:[b:Vnr_2|_], td:[b:Name_2|_], td:[b:Teachers_2]],
%  Entry = [td:[b:Vnr|_], td:[b:Name|_], td:[b:Teachers]],
   Entry = [td:[b:Vnr|_], td:[a:_:[b:Name]|_], td:[b:Teachers]],
   writeln(user, [Vnr, Name, Teachers]),
%  concat(' ', Vnr, Vnr_2),
%  concat(' ', Name, Name_2),
%  concat(' ', Teachers, Teachers_2),
   Exclusions = [
      '\\"Ubungen', 'Proseminar', 'Tutorien',
      'Seminar', 'Oberseminar', 'Praktikum', 'e n t' ],
   \+ ( member(X, Exclusions),
        concat(X, _, Name) ).


/* writeln_vv_atom(Semester, Fach, [Vnr, Name, Teachers]) <-
      */

writeln_vv_atom(Semester, Fach, [Vnr, Name, Teachers]) :-
   write_list([
      'vorlesung([\n',
      '  semester:', Semester,   ',\n',
      '  fach:''',   Fach,     ''',\n',
      '  vnr:',      Vnr,        ',\n',
      '  vname:''',  Name,     ''',\n',
      '  dozent:''', Teachers, ''',\n',
      '  tzahl:_ ]).\n\n' ]).


/******************************************************************/


