

/******************************************************************/
/***                                                            ***/
/***             EVU:  Fragen und Fragegruppen                  ***/
/***                                                            ***/
/******************************************************************/


evu_questions([
% strquest:

   [  2, 1, 2, 'Übungs-Teilnahme' ],

   [  6, 2, 1, 'Vorlesungsstoff ist interessant' ],
   [  7, 2, 2, 'Vorlesungsstoff ist hilfreich für Beruf' ],
   [  8, 2, 3, 'Vorlesungsstoff ist aktuell' ],
   [  9, 2, 4, 'Vorlesungsstoff ist anspruchsvoll' ],

   [ 11, 3, 1, 'Dozent kann für Stoff begeistern' ],
   [ 12, 3, 2, 'Dozent kann Stoff verständlich vermitteln' ],
   [ 13, 3, 3, 'Dozent geht auf Fragen ein' ],
   [ 14, 3, 4, 'Dozent hat gutes Verhältnis zu Studenten' ],
   [ 15, 3, 5, 'Dozent ist ausreichend vorbereitet' ],

   [ 21, 4, 2, 'Tempo der Vorlesung' ],
   [ 22, 4, 3, 'Anwendungen und Beispiele' ],
   [ 23, 4, 4, 'Präsentation ist klar und verständlich' ],
   [ 24, 4, 5, 'Skript zur Vorlesung' ],

   [ 25, 5, 1, 'Übungsleiter erklärt verständlich' ],
   [ 26, 5, 2,
     'Übungsleiter gestaltet Übung interessant und motivierend' ],
   [ 27, 5, 3, 'Übungsleiter geht auf Fragen ein' ],
   [ 28, 5, 4, 'Übungsleiter ist ausreichend vorbereitet' ],

   [ 29, 6, 1, 'Übungsaufgaben sind anspruchsvoll' ],
   [ 31, 6, 2,
     'Übungsaufgaben helfen beim Verständnis der Vorlesung' ],
   [ 32, 6, 3, 'Korrektur der Übungsaufgaben war hilfreich' ],

% intquest:

   [  1, 1, 1, 'Anzahl Vorlesungsteilnehmer' ],
   [  3, 1, 3, 'Anzahl Übungsteilnehmer' ],
   [  semester, 1, 5, 'Semester' ],
   [  5, 1, 6, 'SWS Vorl. und Üb.' ],
   [  participants, 1, 6, 'Anzahl Fragebögen' ],

   [ 10, 2, 5,
     'Aufwand (Stunden) zum Nacharbeiten der Vorlesung' ],

   [ 16, 4, '1a', 'Tafelanschrieb (Prozent)' ],
   [ 17, 4, '1b', 'Overhead-Folien (Prozent)' ],
   [ 18, 4, '1c', 'Computer-Folien (Prozent)' ],
   [ 19, 4, '1d', 'Computer-Demos (Prozent)' ],
   [ 20, 4, '1f', 'Sonstiges (Prozent)' ],

   [ 30, 6, '1a',
     'Aufwand (Stunden) für die Lösung der Übungsaufgaben' ] ]).


/******************************************************************/


