

/******************************************************************/
/***                                                            ***/
/***             EVU:  Evaluation for Teachers                  ***/
/***                                                            ***/
/******************************************************************/


:- setenv('BROWSER', firefox).


/*** interface ****************************************************/


/* evu_to_teachers(Semesters, Subjects, Commands_File) <-
      */

evu_to_teachers(Semesters, Subjects, Commands_File) :-
   evu_course_overview_to_html(Semesters, Subjects, _),
   evu_courses_and_emails(Semesters, Subjects, Commands_File).


/* evu_course_overview_to_html(Semesters, Subjects) <-
      */

evu_course_overview_to_html :-
   evu_course_overview_to_html(
      [13, 14], ['Informatik', 'Mathematik'], _).

evu_course_overview_to_html(Semesters, Subjects, Html) :-
   dislog_variable_get(home, DisLog),
   evu_course_overview(Semesters, Subjects, Tuples),
   ( foreach(Tuple, Tuples), foreach(Row, Rows) do
        evu_course_overview_tuple_to_tr_and_file(
           DisLog, Tuple, Row) ),
   Th = tr:[th:['Sid'], th:['Semester'], th:['Vnr'],
      th:['Vorlesung'], th:['Dozent'], th:['Fach'],
      th:['Frageboegen']],
   As = [border:1, cellspacing:1, bgcolor:'#eeeeee'],
   Html = html:[table:As:[Th|Rows]],
   concat(DisLog, '/projects/Evu/overview.html', File),
   dwrite(xml, File, Html),
   html_file_prune_umlaute(File),
   concat('file:', File, Url),
   www_open_url(Url).

evu_course_overview_tuple_to_tr_and_file(
      DisLog, Tuple, Row) :-
   Tuple = [N, Sem, Nr, V, D, F, T],
   evu_evaluate(vorlesung, N, Nr, Html),
   name_exchange_sublist(
      [[" ", "_"], ["/", "_"]], Sem, Sem_2),
   name_exchange_sublist(
      [[" ", "_"]], Nr, Nr_2),
   concat(['Semester/', Sem_2, '/',
      Sem_2, '_', Nr_2, '.html'], File),
   concat([DisLog, '/projects/Evu/', File], Path),
   dwrite(html, Path, Html),
   html_file_prune_umlaute(Path),
   write_list(user, ['---> ', Path, '\n']),
   Row = tr:[td:[N], td:[Sem], td:[Nr],
      td:[a:[href:File]:[V]], td:[D], td:[F],
      td:[align:center]:[T]].


/* html_file_prune_umlaute(File) <-
      */

html_prune_umlaute(Html_1, Html_2) :-
   dislog_variable_get(output_path, Results),
   concat(Results, 'tmp.html', Html_File),
   dwrite(xml, Html_File, Html_1),
   html_file_prune_umlaute(Html_File),
   dread(xml, Html_File, Html_2).

html_file_prune_umlaute(File) :-
   dread(txt, File, Name_1),
   html_name_prune_umlaute(Name_1, Name_2),
   dwrite(txt, File, Name_2).

html_name_prune_umlaute(Name_1, Name_2) :-
   Substitution = [
      [[195, 164], "&auml;"],
      [[195, 182], "&ouml;"],
      [[195, 188], "&uuml;"],
      [[195, 132], "&Auml;"],
      [[195, 150], "&Ouml;"],
      [[195, 156], "&Uuml;"],
      [[195, 159], "&szlig;"],
      ["�", "&auml;"],
      ["�", "&ouml;"],
      ["�", "&uuml;"],
      ["�", "&Auml;"],
      ["�", "&Ouml;"],
      ["�", "&Uuml;"],
      ["�", "&szlig;"] ],
   name_exchange_sublist(Substitution, Name_1, Name_2).


/* evu_courses_and_emails(Semesters, Subjects, Commands_File) <-
      */

evu_courses_and_emails :-
   evu_courses_and_emails(
      [12, 13], ['Informatik'], email_commands).

evu_courses_and_emails(Semesters, Subjects, Commands_File) :-
   evu_courses_and_emails_(Semesters, Subjects, Cs),
   nl(user),
   writeln_list(user, Cs),
   maplist( courses_to_mail_line('Mail_Text'),
      Cs, Ls ),
   dislog_variable_get(home, DisLog),
   concat([DisLog, '/projects/Evu/', Commands_File], Path),
   writeln_list(Path, Ls).

evu_courses_and_emails_(Semesters, Subjects, Cs) :-
   evu_course_overview(Semesters, Subjects, Tuples),
   ( foreach(Tuple, Tuples), foreach(Items, List_of_Items) do
        Tuple = [_, Sem, Nr, _, Lecturers, Subject, _],
        name_split_at_position(
           [", "], Lecturers, Group),
        ( foreach(Lecturer, Group), foreach(Item, Items) do
             write(user, [Sem, Nr, Lecturer, Subject]),
             evu_name_to_email(Lecturer, Subject, Email),
             write_list(user, [' ---> ', Email, '\n']),
             Item = [Sem, Nr, Email] ) ),
   append(List_of_Items, Items),
   sort(Items, Items_2),
   group_tuples(Items_2, [3], Cs).


/*** implementation ***********************************************/


/* courses_to_mail_line(File, Cs, Line) <-
      */

courses_to_mail_line(File, Cs, Line) :-
   first(Cs, C),
   nth(3, C, Email),
   maplist( course_and_email_to_attachment,
      Cs, Attachments ),
   Subject = 'Vorlesungsumfrage der Studiendekane',
   concat(
      ['cat ', File, ' | mail ', Email, ' -s ''', Subject, '''',
       ' -a bewertung.html'|Attachments], Line).

course_and_email_to_attachment([Sem, Nr, _], Attachment) :-
   name_exchange_sublist(
      [[" ", "_"], ["/", "_"]], Sem, Sem_2),
   name_exchange_sublist(
      [[" ", "_"]], Nr, Nr_2),
   concat([' -a ', Sem_2, '_', Nr_2, '.html'], Attachment).


/* evu_name_to_email(Name, Email) <-
      */

evu_name_to_email(Name, Subject, Email) :-
   ( name_contains_name(Name, 'Dr. ')
   ; name_contains_name(Name, 'Drs. ') ),
   !,
   name_start_after_position(["Dr. ", "Drs. "], Name, Name_2),
   evu_name_to_login(Name_2, Login),
   evu_name_to_lower_case(Login, Login_2),
   evu_name_to_lower_case(Subject, Subject_2),
   concat([Login_2, '@', Subject_2, '.uni-wuerzburg.de'], Email).
evu_name_to_email(Name, Subject, Email) :-
   evu_name_to_lower_case(Name, Name_2),
   evu_name_to_lower_case(Subject, Subject_2),
   concat([Name_2, '@', Subject_2, '.uni-wuerzburg.de'], Email).

evu_name_to_lower_case(Name_1, Name_2) :-
   atom_chars(Name_1, [C|Cs]),
   ( character_capital_to_lower(C, D)
   ; C = D ),
   concat([D|Cs], Name_2).

evu_name_to_login(Name, Login) :-
   ( member(Name-Login, [
        'Tran-Gia'-trangia,
        'Wolff von Gudenberg'-wolff ])
   ; Name = 'Kluegl-Frohnmeyer', Login = 'kluegl'
   ; Name = Login ),
   !.


/******************************************************************/


