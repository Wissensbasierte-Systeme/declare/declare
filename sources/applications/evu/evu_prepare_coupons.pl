

/******************************************************************/
/***                                                            ***/
/***             EVU:  Printing of Coupons                      ***/
/***                                                            ***/
/******************************************************************/


evu_coupons_directory(Directory) :-
   evu_directory(home, Evu),
   concat(Evu, 'SS_06/Vorbereitung/Coupons', Directory).


/*** interface ****************************************************/


/* evu_coupon_file_print(File, N) <-
      */

evu_coupon_file_print(File, N) :-
   generate_interval(1, N, Interval),
   checklist( evu_coupon_file_print_page(File),
      Interval ).

evu_coupon_file_print_page(File, N) :-
   evu_coupons_directory(Directory),
   concat(['/usr/home/seipel/print/', File, '_', N], F),
   concat([
      'psselect -p', N, ' ', Directory, '/', File,
      ' > ', F, '; lp -dhp4100 ', F], Command),
   us(Command),
   !.


/******************************************************************/


