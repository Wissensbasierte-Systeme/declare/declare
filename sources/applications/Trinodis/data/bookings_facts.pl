

/******************************************************************/
/***                                                            ***/
/***        Bookings:  Fact Basis                               ***/
/***                                                            ***/
/******************************************************************/
 


/*** interface ***************************************************/


/* tax_rule(Auto_Anr, Tax_Anr, Tax) <-
      */

tax_rule(8400, 1800, 0.19).
tax_rule(8401, 1801, 0.25).
tax_rule(8402, 1802, 0.07).
tax_rule(8403, 1803, 0.09).
tax_rule(8403, 1804, 0.41).


/* delivery_threshold(Anr, Anr_dt_over, Threshold) <-
      */

  delivery_threshold(8400, 8401, 100000).
% delivery_threshold(8402, 8403, 90000).
  delivery_threshold(8402, 8403, 200000).


/* account(Anr, Shipping_Country, Type) <-
      */

account(8400, 'FR', nonfood).
account(8401, 'FR', nonfood).
account(8402, 'FR',    food).
account(8403, 'FR',    food).


/* annual_sales(
         Shipping_Country, Year, After_Tax_of_Year, under) <-
      */

annual_sales('FR', 2009, 95500, under).
annual_sales('FR', 2010, 99800, under).


/******************************************************************/


