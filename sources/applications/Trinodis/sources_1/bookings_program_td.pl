

/******************************************************************/
/***                                                            ***/
/***           Trinodis:  Bookings - Program - TD               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* trinodis_state_of_delivery_threshold(
         +Sum_after_Tax, +Country,
         +Type, +Year, +Where, ?Anr) <-
      */

trinodis_state_of_delivery_threshold(
      _, Country, Type, Year, over, Anr_dt_over) :-
   account(Anr, Country, Type),
   Prior_Year is Year - 1,
   annual_sales(Country, Prior_Year, _, over),
   delivery_threshold(Anr, Anr_dt_over, _),
   !.
trinodis_state_of_delivery_threshold(
      _, Country, Type, Year, over, Anr_dt_over) :-
   account(Anr, Country, Type),
   annual_sales(Country, Year, _, over),
   delivery_threshold(Anr, Anr_dt_over, _),
   !.
trinodis_state_of_delivery_threshold(
      Sum_after_Tax, Country, Type, Year, over, Anr_dt_over) :-
   account(Anr, Country, Type),
   annual_sales(Country, Year, After_tax_of_year, under),
   delivery_threshold(Anr, Anr_dt_over, Threshold),
   Threshold < After_tax_of_year + Sum_after_Tax,
   !.
trinodis_state_of_delivery_threshold(
      Sum_after_Tax, Country, Type, Year, under, Anr) :-
   account(Anr, Country, Type),
   annual_sales(Country, Year, After_tax_of_year, under),
   delivery_threshold(Anr, _, Threshold),
   Threshold >= After_tax_of_year + Sum_after_Tax,
   !.


/* trinodis_after_tax(+Anr, +Pre_Tax, -After_Tax) <-
      */

trinodis_after_tax(Anr, Pre_Tax, After_Tax) :-
   ddbase_aggregate( [sum(Tax)], tax_rule(Anr, _, Tax), [[Sum]] ),
   Fraction is Sum / (1 + Sum),
   After_Tax is Pre_Tax * (1 - Fraction).


/******************************************************************/


