

/******************************************************************/
/***                                                            ***/
/***           Trinodis:  Bookings                              ***/
/***                                                            ***/
/******************************************************************/


% invoice = rechnung
% account = konto
% booking = buchung
% ANR = account_number = kontonummer
% invoice_line_item = rechnungsposition
% annual_sales = jahresumsatz
% delivery_threshold = lieferschwelle
% AT = after tax = netto
% PT = pre tax = brutto


:- dislog_variable_get(home,
      '/projects/Trinodis/sources', Path_1),
   dislog_variable_set(trinodis_sources, Path_1),
   dislog_variable_get(home,
      '/projects/Trinodis/data', Path_2),
   dislog_variable_set(trinodis_data, Path_2).

:- dislog_variable_get(trinodis_data,
      '/bookings_facts.pl', Path),
   consult(Path).

:- dynamic
      invoice/4,
      invoice_line_item/4.


/*** tests ********************************************************/


test(trinodis_invoice_to_xml, 1) :-
   Invoice = invoice:[
      id:123, country:'FR', year:2010, anr:1200]:[
      position:[line:1, type:food, pre_tax:214]:[],
      position:[line:2, type:food, pre_tax:107]:[] ],
   dwrite(xml, Invoice),
   writeln(user, '--->'),
   trinodis_invoice_to_xml(Invoice, Xml),
   dwrite(xml, Xml).


/*** interface ****************************************************/


/* invoice_to_xml(Invoice, Xml) <-
      */

trinodis_invoice_to_xml(Invoice, Xml) :-
   [Invoice_NR, Country, Year, ANR] :=
      Invoice@[id, country, year, anr],
   forall( As := Invoice/position@[line, type, pre_tax],
      ( Atom =.. [invoice_line_item, Invoice_NR|As],
        assert(Atom) ) ),
   assert(invoice(Invoice_NR, Country, Year, ANR)),
   trinodis_invoice_to_xml(Xml),
   dabolish(invoice/4),
   dabolish(invoice_line_item/4).

trinodis_invoice_to_xml(Xml) :-
   invoice(Invoice_NR, Country, Year, ANR),
   trinodis_invoice_after_tax(Invoice_NR, Country, Sum_after_Tax),
   trinodis_expense_bookings_pre_processing(Sum_after_Tax),
   ddbase_aggregate( [D, sum(C), list(Tuple)],
      ( Tuple = [_Type,_A,_B,C,D],
        apply(trinodis_expense_booking, Tuple) ),
        Bookings ),
   trinodis_bookings_to_positions(Bookings, Positions),
   Xml = invoice:[
      id:Invoice_NR, country:Country, year:Year, anr:ANR]:
      Positions,
   dabolish(trinodis_invoice_line_item_after_tax/4).


/* trinodis_expense_bookings_pre_processing(+Sum_after_Tax) <-
      */

trinodis_expense_bookings_pre_processing(Sum_after_Tax) :-
   close_num(invoice_ID, _),
   dabolish(booking/4),
   forall(
      trinodis_invoice_line_item_booking(
         Sum_after_Tax, _, _, _, ANR, Contra_ANR, Pre_tax),
      ( get_num(invoice_ID, Booking_ID),
        Booking = booking(ANR, Contra_ANR, Pre_tax, Booking_ID),
        assert(Booking) ) ).

trinodis_invoice_line_item_booking(Sum_after_Tax,
      Invoice_NR, Position, Year, ANR, Contra_ANR, Pre_tax) :-
   invoice(Invoice_NR, Shipping_country, Year, ANR),
   invoice_line_item(Invoice_NR, Position, Typ, Pre_tax),
   trinodis_state_of_delivery_threshold(
      Sum_after_Tax, Shipping_country, Typ, Year, _, Contra_ANR).


/* trinodis_expense_booking(
         +Mode, +ANR, +Tax_ANR, +Tax_Amount, +Booking_ID) <-
      Mode can be tax_posting or booking. */

trinodis_expense_booking(
      tax_posting, ANR, Tax_ANR, Tax_Amount, Booking_ID) :-
   trinodis_tax_posting(ANR, Tax_ANR, Booking_ID, Tax_Amount).
trinodis_expense_booking(
      booking, ANR, Contra_ANR, Booking_AT, Booking_ID) :-
   booking(ANR, Contra_ANR, Pre_tax, Booking_ID),
   trinodis_after_tax(Pre_tax, Contra_ANR, Booking_AT).


/* trinodis_tax_posting(
         +ANR_1, +ANR_2, +Booking_ID, -Tax_Amount) <-
      */  

trinodis_tax_posting(ANR, Tax_ANR, Booking_ID, Tax_Amount) :-
   booking(ANR, Auto_ANR, Pre_tax, Booking_ID),
   trinodis_after_tax(Pre_tax, Auto_ANR, Booking_AT),
   tax_rule(Auto_ANR, Tax_ANR, Tax),
   Tax_Amount is Booking_AT * Tax.
trinodis_tax_posting(
      Tax_ANR, Contra_ANR, Booking_ID, Tax_Amount) :-
   booking(Auto_ANR, Contra_ANR, Pre_tax, Booking_ID),
   trinodis_after_tax(Pre_tax, Auto_ANR, Booking_AT),
   tax_rule(Auto_ANR, Tax_ANR, Tax),
   Tax_Amount is Booking_AT * Tax.


/* trinodis_state_of_delivery_threshold(
         +Sum_after_Tax, +Shipping_country,
         +Type, +Year, +Where, ?ANR) <-
      */

trinodis_state_of_delivery_threshold(
      _, Shipping_country, Type, Year, over, ANR_dt_over) :-
   account(ANR, Shipping_country, Type),
   Prior_Year is Year - 1,
   annual_sales(Shipping_country, Prior_Year, _, over),
   delivery_threshold(ANR, ANR_dt_over, _),
   !.
trinodis_state_of_delivery_threshold(
      _, Shipping_country, Type, Year, over, ANR_dt_over) :-
   account(ANR, Shipping_country, Type),
   annual_sales(Shipping_country, Year, _, over),
   delivery_threshold(ANR, ANR_dt_over, _),
   !.
trinodis_state_of_delivery_threshold(
      Sum_after_Tax, Shipping_country,
      Type, Year, over, ANR_dt_over) :-
   account(ANR, Shipping_country, Type),
   annual_sales(Shipping_country, Year, After_tax_of_year, under),
   delivery_threshold(ANR, ANR_dt_over, Threshold),
   Threshold < After_tax_of_year + Sum_after_Tax,
   !.
trinodis_state_of_delivery_threshold(
      Sum_after_Tax, Shipping_country,
      Type, Year, under, ANR) :-
   account(ANR, Shipping_country, Type),
   annual_sales(Shipping_country, Year, After_tax_of_year, under),
   delivery_threshold(ANR, _, Threshold),
   Threshold >= After_tax_of_year + Sum_after_Tax,
   !.


/*** implementation **********************************************/


/* trinodis_invoice_after_tax(
         +Invoice_NR, +Shipping_country, -Sum_after_Tax) <-
      */

trinodis_invoice_after_tax(
      Invoice_NR, Shipping_country, Sum_after_Tax) :-
   dabolish(trinodis_invoice_line_item_after_tax/4),
   trinodis_invoice_line_item_after_tax(
      Invoice_NR, Shipping_country),
   ddbase_aggregate( [sum(After_Tax)],
      trinodis_invoice_line_item_after_tax(
         Invoice_NR, _, _, After_Tax),
      [[Sum_after_Tax]] ).


/* trinodis_invoice_line_item_after_tax(
         +Invoice_NR, +Shipping_Country) <-
      */

trinodis_invoice_line_item_after_tax(
      Invoice_NR, Shipping_Country) :- 
   forall(
      invoice_line_item(Invoice_NR, Position, Type, Pre_Tax),
      ( account(ANR, Shipping_Country, Type),
        trinodis_after_tax(Pre_Tax, ANR, After_Tax),
        Atom = trinodis_invoice_line_item_after_tax(
           Invoice_NR, Position, Type, After_Tax),
        assert(Atom) ) ).


/* trinodis_sum_tax_key(+ANR, -Sum_of_taxes) <-
      */

trinodis_sum_tax_key(ANR, Sum_of_taxes) :-
   ddbase_aggregate( [sum(Tax)],
      tax_rule(ANR, _, Tax),
      Xs ),
   flatten(Xs, [Sum_of_taxes|_]).


/* trinodis_after_tax(+Pre_tax, +ANR, -AT_amount) <-
      */

trinodis_after_tax(Pre_tax, ANR, AT_amount) :-
   trinodis_sum_tax_key(ANR, Sum_of_taxes),
   Fraction is Sum_of_taxes / (1 + Sum_of_taxes),
   AT_amount is Pre_tax - Pre_tax * Fraction.


/* trinodis_bookings_to_positions(+Bookings, -Positions) <-
      */

trinodis_bookings_to_positions(Bookings, Positions) :-
   foreach([Pos, Tax, Bs], Bookings),
   foreach(Position, Positions) do
      trinodis_bookings_to_positions_sub(Bs, Items),
      Position = position:[
         line:Pos, pre_tax:Tax]:Items.

trinodis_bookings_to_positions_sub(Bs, Items) :-
   ( foreach([A, B, C, D, _], Bs),
     foreach(Item, Items) do
        Item = A:[account:B, contra_account:C, after_tax:D]:[] ).


/******************************************************************/


