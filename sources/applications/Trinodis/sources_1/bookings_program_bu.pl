

/******************************************************************/
/***                                                            ***/
/***           Trinodis:  Bookings - Program - BU               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* trinodis_booking(+Bid, +Mode, +Anr, +Tax_Anr, -Tax_Amount) <-
      Mode can be tax_posting or booking. */

trinodis_booking(
      Bid, tax_posting, Anr, Tax_Anr, Tax_Amount) :-
   trinodis_booking_tp(Bid, Anr, Tax_Anr, Tax_Amount).
trinodis_booking(
      Bid, booking, Anr, Contra_Anr, Booking_AT) :-
   trinodis_booking(Bid, Anr, Contra_Anr, Pre_tax),
   trinodis_after_tax(Contra_Anr, Pre_tax, Booking_AT).

trinodis_booking_tp(Bid, Anr, Tax_Anr, Tax_Amount) :-
   ( trinodis_booking(Bid, Anr, Auto_Anr, Pre_tax),
     trinodis_after_tax(Auto_Anr, Pre_tax, Booking_AT) ),
   ( tax_rule(Auto_Anr, Tax_Anr, Tax),
     Tax_Amount is Booking_AT * Tax ).
trinodis_booking_tp(Bid, Tax_Anr, Contra_Anr, Tax_Amount) :-
   ( trinodis_booking(Bid, Auto_Anr, Contra_Anr, Pre_tax),
     trinodis_after_tax(Auto_Anr, Pre_tax, Booking_AT) ),
   ( tax_rule(Auto_Anr, Tax_Anr, Tax),
     Tax_Amount is Booking_AT * Tax ).


/* trinodis_booking(I:P, Anr, Contra_Anr, Pre_tax) <-
      */

trinodis_booking(I:P, Anr, Contra_Anr, Pre_tax) :-
   ( ( invoice(I, Country, Year, Anr),
       trinodis_invoice_sum_after_tax(I, Country, Sum) ),
     invoice_position(I, P, Type, Pre_tax) ),
   trinodis_state_of_delivery_threshold(
      Sum, Country, Type, Year, _, Contra_Anr).


/* trinodis_invoice_sum_after_tax(I, Country, Sum) <-
      */

trinodis_invoice_sum_after_tax(I, Country, Sum) :-
   ddbase_aggregate( [J, C, sum(After_Tax)],
      ( invoice_position(J, _, Type, Pre_Tax),
        once(account(Anr, C, Type)),
        trinodis_after_tax(Anr, Pre_Tax, After_Tax) ),
      Tuples ),
   { member([I, Country, Sum], Tuples) }.


/******************************************************************/


