

/******************************************************************/
/***                                                            ***/
/***           Trinodis:  Bookings                              ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_get(home,
      '/projects/Trinodis/sources_1', Path_1),
   dislog_variable_set(trinodis_sources_1, Path_1),
   dislog_variable_get(home,
      '/projects/Trinodis/data', Path_2),
   dislog_variable_set(trinodis_data, Path_2).

:- dislog_variable_get(trinodis_data,
      '/bookings_facts.pl', Path_1),
   consult(Path_1),
   dislog_variable_get(trinodis_sources_1,
      '/bookings_datalogs.pl', Path_2),
   consult(Path_2),
   dislog_variable_get(trinodis_sources_1,
      '/bookings_program_bu.pl', Path_3),
   consult(Path_3),
   dislog_variable_get(trinodis_sources_1,
      '/bookings_program_td.pl', Path_4),
   consult(Path_4).

:- dynamic
      invoice/4,
      invoice_position/4.


/*** tests ********************************************************/


test(trinodis_invoice_to_xml, 1) :-
   Invoice = invoice:[
      id:123, country:'FR', year:2010, anr:1200]:[
      position:[line:1, type:food, pre_tax:214]:[],
      position:[line:2, type:food, pre_tax:107]:[] ],
   dwrite(xml, Invoice),
   writeln(user, '--->'),
   trinodis_invoice_to_xml(Invoice, Xml),
   dwrite(xml, Xml).


/*** interface ****************************************************/


/* trinodis_invoice_to_xml(Invoice, Xml) <-
      */

trinodis_invoice_to_xml(Invoice, Xml) :-
   trinodis_invoice_assert_facts(Invoice),
   trinodis_invoice_to_xml_sub(Invoice, Xml),
   trinodis_invoice_retract_facts.

trinodis_invoice_to_xml_sub(Invoice, Xml) :-
   [I, Country, Year, Anr] :=
      Invoice@[id, country, year, anr],
   R = ({}(Goal) :- call(Goal)),
   assert(R),
   ddbase_aggregate( [Bid, sum(Amount), list(Tuple)],
      ( Tuple = [Bid, _, _, _, Amount],
        Bid = I:_,
        apply(trinodis_booking, Tuple) ),
      Bookings ),
   trinodis_bookings_to_positions(Bookings, Positions),
   As = [id:I, country:Country, year:Year, anr:Anr],
   Xml = invoice:As:Positions,
   retract(R).


/* trinodis_invoice_assert_facts(Invoice) <-
      */

trinodis_invoice_assert_facts(Invoice) :-
   [I, Country, Year, Anr] :=
      Invoice@[id, country, year, anr],
   assert(invoice(I, Country, Year, Anr)),
   forall( As := Invoice/position@[line, type, pre_tax],
      ( Atom =.. [invoice_position, I|As], assert(Atom) ) ).


/* trinodis_invoice_retract_facts <-
      */

trinodis_invoice_retract_facts :-
   dabolish(invoice/4),
   dabolish(invoice_position/4).


/* trinodis_load_bookings_program <-
      */

trinodis_load_bookings_program :-
   dislog_variable_get(trinodis_sources_1,
      '/bookings_program_bu.pl', File),
   consult(File).


/*** implementation **********************************************/


/* trinodis_bookings_to_positions(+Bookings, -Positions) <-
      */

trinodis_bookings_to_positions(Bookings, Positions) :-
   foreach([_:Pos, Tax, Bs], Bookings),
   foreach(Position, Positions) do
      trinodis_bookings_to_positions_sub(Bs, Items),
      Position = position:[line:Pos, pre_tax:Tax]:Items.

trinodis_bookings_to_positions_sub(Bs, Items) :-
   ( foreach([_, A, B, C, D], Bs),
     foreach(Item, Items) do
        Item = A:[account:B, contra_account:C, after_tax:D]:[] ).


/******************************************************************/


