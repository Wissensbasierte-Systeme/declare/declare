

/******************************************************************/
/***                                                            ***/
/***           Trinodis:  Bookings - DataLog_s                  ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(trinodis_invoice_to_bookings, Mode) :-
   member(Mode, [regular, pt]),
   Invoice = invoice:[
      id:123, country:'FR', year:2010, anr:1200]:[
      position:[line:1, type:food, pre_tax:214]:[],
      position:[line:2, type:food, pre_tax:107]:[] ],
   dwrite(xml, Invoice),
   writeln(user, '--->'),
   trinodis_invoice_to_bookings(Mode, Invoice, Bookings),
   ( Mode = regular ->
     writeln_list(Bookings)
   ; functor(Query, trinodis_booking, 5),
     xml_proof_atoms_to_picture_trinodis(Query, Bookings) ).


/*** interface ****************************************************/


/* trinodis_invoice_to_bookings(Mode, Invoice, Bookings) <-
      */

trinodis_invoice_to_bookings(Mode, Invoice, Bookings) :-
   [I, Country, Year, Anr] :=
      Invoice@[id, country, year, anr],
   Fact = invoice(I, Country, Year, Anr),
   findall( Atom,
      ( As := Invoice/position@[line, type, pre_tax],
        Atom =.. [invoice_position, I|As] ),
      Facts ),
   trinodis_invoice_to_bookings_sub(Mode, [Fact|Facts], Bookings),
   dabolish(invoice/4),
   dabolish(invoice_position/4).

trinodis_invoice_to_bookings_sub(regular, Facts, Bookings) :-
   dislog_variable_get(trinodis_sources_1,
      '/bookings_program_bu.pl', File),
   dread(pl, File, Program),
   append(Program, Facts, Datalog),
   R = ({}(Goal) :- call(Goal)),
   assert(R),
   perfect_model_prolog(Datalog, [], Bookings),
   retract(R).

trinodis_invoice_to_bookings_sub(pt, Facts, Bookings) :-
   dislog_variable_get(trinodis_sources_1,
      '/bookings_program_bu.pl', File),
   dread(pl, File, Program_2),
   append(Program_2, Facts, Program),
   Module = module,
   prolog_program_stratify(Program, [], _, Programs),
   checklist( dwrite(pl, user), Programs ),
   prolog_programs_expand_with_proof_trees(Programs, Datalogs),
%  checklist( dwrite(pl, user), Datalogs ),
   prolog_program_execute_dynamics(Program, Module, _),
   R = ( tree(G, T) :- call(G), T = node:[key:G]:[] ),
   assert(Module:R),
   tp_iteration_prolog_muliple(Datalogs, Module, [], Bookings),
%  perfect_model_prolog(Rules, [], Module, Bookings),
   retract(Module:R).


/* xml_proof_atoms_to_picture_trinodis(Atoms) <-
      */

xml_proof_atoms_to_picture_trinodis(Atoms) :-
   xml_proof_atoms_to_picture_trinodis(_, Atoms).

xml_proof_atoms_to_picture_trinodis(Query, Atoms) :-
   forall( member(tree(Query, Tree_1), Atoms),
      ( dwrite(xml, Tree_1),
        xml_proof_tree_simplify(Tree_1, Tree_2),
        xml_proof_tree_simplify(trinodis, Tree_2, Tree_3),
        xml_proof_tree_to_picture(Tree_3) ) ).


/* xml_proof_tree_simplify(Mode, Tree_1, Tree_2) <-
      */

xml_proof_tree_simplify(trinodis, Tree_1, Tree_2) :-
   ( Tree_1 = node:[key:Atom, rule:Id]:Trees_3 ->
     Tree_2 = node:[key:Key, rule:Id]:Trees_2
   ; Tree_1 = node:[key:Atom]:Trees_3 ->
     Tree_2 = node:[key:Key]:Trees_2 ),
   !,
   Substitutions = [
      trinodis_booking-booking, trinodis_booking_tp-b,
      booking_position-booking,
      invoice-inv, invoice_position-pos,
      trinodis_invoice_sum_after_tax-at,
      trinodis_state_of_delivery_threshold-th,
      delivery_threshold-th,
      trinodis_after_tax-at, tax_rule-r,
      annual_sales-sales,
      tax_country-country ],
   ( Atom =.. [P|Xs], member(P-Q, Substitutions) ->
     Key =.. [Q|Xs]
   ; Key = Atom ),
   maplist( xml_proof_tree_simplify(trinodis),
      Trees_3, Trees_2 ).
xml_proof_tree_simplify(trinodis, Tree_1, Tree_2) :-
   Tree_1 = node:As:Trees_1,
   maplist( xml_proof_tree_simplify(trinodis),
      Trees_1, Trees_2 ),
   Tree_2 = node:As:Trees_2.


/******************************************************************/


