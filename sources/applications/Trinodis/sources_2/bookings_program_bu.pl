

% booking_position(Line, Type, Mode, Country, Amount)

booking_position(Line, Type, Mode, Country, Amount) :-
   { invoice(Destination, _) },
   ( ( position(Line, Type, Value),
       tax_country(Tax_Country) ),
     tax(Tax_Country, Type, Tax_Rate) ),
   ( { Mode = profit,
       Country = Destination },
     Amount is Value / (1+Tax_Rate)
   ; { Mode = taxes,
       Country = Tax_Country },
     Amount is Value * Tax_Rate / (1+Tax_Rate) ).

tax_country(Tax_Country) :-
   { invoice(Destination, Year),
     delivery_threshold(Destination, Threshold) },
   ( { Y is Year - 1 ; Y is Year },
     annual_sales(Destination, Y, Total),
     Total > Threshold,
     { Tax_Country = Destination }
   ; { Y_1 is Year - 1 },
     ( annual_sales(Destination, Y_1, Total_1),
       Total_1 =< Threshold ),
     ( annual_sales(Destination, Year, Total_2),
       Total_2 =< Threshold ),
     { home_country(Home_Country),
       Tax_Country = Home_Country } ).

annual_sales(Destination, Year, Total) :-
   ddbase_aggregate( [Destination, Year, sum(A)],
      ( ( annual_sales_so_far(Destination, Year, A)
        ; ( invoice(Destination, Year),
            position(_, Type, B),
            home_country(Home_Country),
            tax(Home_Country, Type, Tax),
            A is B / (1+Tax) ) ) ),
      Tuples ),
   { member([Destination, Year, Total], Tuples) }.


