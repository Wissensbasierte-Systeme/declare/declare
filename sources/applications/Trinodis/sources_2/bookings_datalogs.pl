

trinodis_invoice_to_bookings_sub(pt) :-
   trinodis_invoice_to_bookings_sub(pt, Bookings),
   functor(Query, booking_position, 5),
   xml_proof_atoms_to_picture_trinodis(Query, Bookings).

trinodis_invoice_to_bookings_sub(pt, Bookings) :-
   dislog_variable_get(
      trinodis_sources_2, '/bookings_program_bu.pl', File),
   dread(pl, File, Program),
   Module = module,
   prolog_program_stratify(Program, [], _, Programs),
   checklist( dwrite(pl, user), Programs ),
   prolog_programs_expand_with_proof_trees(Programs, Datalogs),
%  checklist( dwrite(pl, user), Datalogs ),
   prolog_program_execute_dynamics(Program, Module, _),
   R = ( tree(G, T) :- call(G), T = node:[key:G]:[] ),
   assert(Module:R),
   tp_iteration_prolog_muliple(Datalogs, Module, [], Bookings),
%  perfect_model_prolog(Rules, [], Module, Bookings),
   retract(Module:R).


% annual_sales_so_far(Destination, Year, Amount)

  annual_sales_so_far('France', 2011, 92300).
  annual_sales_so_far('France', 2012, 99800).
% annual_sales_so_far('France', 2012, 80000).

% position(Line, Type, Value).

position(1, food,    211.00).
position(2, nonfood, 119.60).

% invoice(Destination, Year)

invoice('France', 2012).

% home_country(Country)

home_country('Germany').


% accounts(Country, Type, P_Anr_u, T_Anr_u, P_Anr_o, T_Anr_o)

accounts('France',      food, 'P_3300', 'T_3300', 'P_3301', 'T_3301').
accounts('France',   nonfood, 'P_3302', 'T_3302', 'P_3303', 'T_3303').
accounts('Germany',     food, 'P_4900', 'T_4900', 'P_4901', 'T_4901').
accounts('Germany',  nonfood, 'P_4902', 'T_4902', 'P_4903', 'T_4903').

% accounts(Country, Type, Mode, P_Anr, T_Anr)

accounts(Country, Type, under, P_Anr, T_Anr) :-
   accounts(Country, Type, P_Anr, T_Anr, _, _).
accounts(Country, Type, over, P_Anr, T_Anr) :-
   accounts(Country, Type, _, _, P_Anr, T_Anr).

% tax(Country, Type, Tax)

tax('France',     food, 0.055).
tax('France',  nonfood, 0.196).
tax('Germany',    food, 0.070).
tax('Germany', nonfood, 0.190).

% tax(Destination, Type, Mode, Tax)

tax(_, Type, under, Tax) :-
   home_country(Country),
   tax(Country, Type, Tax).
tax(Destination, Type, over, Tax) :-
   tax(Destination, Type, Tax).

% delivery_threshold(Country, Threshold)

delivery_threshold('France', 100000).


/*
% booking_position(Line, Type, Type_B, Anr, Amount)

booking_position(Line, Type, Type_B, Anr, Amount) :-
   invoice(Destination, Year),
   position(Line, Type, Value),
   annual_sales_to_mode(Destination, Year, Mode),
   tax(Destination, Type, Mode, Tax_Rate),
   ( Type_B = profit,
     accounts(Destination, Type, Mode, Anr, _),
     Amount is Value / (1+Tax_Rate)
   ; Type_B = taxes,
     accounts(Destination, Type, Mode, _, Anr),
     Amount is Value * Tax_Rate / (1+Tax_Rate) ).

annual_sales_to_mode(Destination, Year, Mode) :-
   invoice(Destination, Year),
   delivery_threshold(Destination, Threshold),
   ( ( Y is Year - 1 ; Y is Year ),
     annual_sales(Destination, Y, Total),
     Total > Threshold ->
     Mode = over
   ; Mode = under ).

annual_sales(Destination, Year, Total) :-
   ddbase_aggregate( [X, Y, sum(A)],
      ( X = Destination, Y = Year,
        ( annual_sales_so_far(X, Y, A)
        ; ( invoice(Destination, Y),
            position(_, Type, B),
            tax(X, Type, under, Tax),
            A is B / (1+Tax) ) ) ),
      Tuples ),
   member([Destination, Year, Total], Tuples).
*/

annual_sales_to_mode_sub(Destination, Year, Threshold, over) :-
   ( Y is Year - 1 ; Y is Year ),
   annual_sales(Destination, Y, Total),
   Total > Threshold.
annual_sales_to_mode_sub(Destination, Year, Threshold, under) :-
   Y_1 is Year - 1,
   annual_sales(Destination, Y_1, Total_1),
   Total_1 =< Threshold,
   annual_sales(Destination, Year, Total_2),
   Total_2 =< Threshold.


