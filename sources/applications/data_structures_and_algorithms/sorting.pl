

/******************************************************************/
/***                                                            ***/
/***          Algorithms:  Quicksort, Mergesort                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* quicksort(List, Sorted_List) <-
      */

quicksort([], []) :-
   !.
quicksort([X|Xs], Ys) :-
   quicksort_divide(X, Xs, Xs1, Xs2),
   quicksort(Xs1, Ys1),
   quicksort(Xs2, Ys2),
   append(Ys1, [X|Ys2], Ys).

quicksort_divide(X, Xs, Xs1, Xs2) :-
   findall( X1,
      ( member(X1, Xs),
        X1 < X ),
      Xs1 ),
   findall( X2,
      ( member(X2, Xs),
        X2 > X ),
      Xs2 ).


/* mergesort(List, Sorted_List) <-
      */

mergesort(Xs, Xs) :-
   length(Xs, N),
   N =< 1,
   !.
mergesort(Xs, Ys) :-
   middle_split(Xs, Xs1, Xs2),
   mergesort(Xs1, Ys1),
   mergesort(Xs2, Ys2),
   mergesort_merge(Ys1, Ys2, Ys).

mergesort_merge([], Xs, Xs) :-
   !.
mergesort_merge(Xs, [], Xs) :-
   !.
mergesort_merge([X1|Xs1], [X2|Xs2], [X|Xs]) :-
   ( X1 < X2,
     X = X1,
     mergesort_merge(Xs1, [X2|Xs2], Xs)
   ; X = X2,
     mergesort_merge([X1|Xs1], Xs2, Xs) ).


/******************************************************************/


