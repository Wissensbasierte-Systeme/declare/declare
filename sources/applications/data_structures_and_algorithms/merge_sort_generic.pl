

/******************************************************************/
/***                                                            ***/
/***          Algorithms:  Mergesort                            ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(merge_sort_generic, N) :-
   member(N, [2, 3, 4]),
   Comparator = nth_character_is_smaller(N),
   List_1 = [echo, abba, hallo],
   merge_sort_generic(Comparator, List_1, List_2),
   writeln(user, List_2). 

nth_character_is_smaller(N, Name_1, Name_2) :-
   name(Name_1, List_1),
   name(Name_2, List_2),
   nth(N, List_1, X),
   nth(N, List_2, Y),
   X < Y.


/*** interface ****************************************************/


/* merge_sort_generic(Comparator, List, Sorted_List) <-
      */

merge_sort_generic(_, Xs, Xs) :-
   length(Xs, N),
   N =< 1,
   !.
merge_sort_generic(Comparator, Xs, Ys) :-
   middle_split(Xs, Xs1, Xs2),
   merge_sort_generic(Comparator, Xs1, Ys1),
   merge_sort_generic(Comparator, Xs2, Ys2),
   merge_sort_merge_generic(Comparator, Ys1, Ys2, Ys).

merge_sort_merge_generic(_, [], Xs, Xs) :-
   !.
merge_sort_merge_generic(_, Xs, [], Xs) :-
   !.
merge_sort_merge_generic(
      Comparator, [X1|Xs1], [X2|Xs2], [X|Xs]) :-
   ( apply(Comparator, [X1, X2]) ->
     X = X1,
     merge_sort_merge_generic(Comparator, Xs1, [X2|Xs2], Xs)
   ; X = X2,
     merge_sort_merge_generic(Comparator, [X1|Xs1], Xs2, Xs) ).


/******************************************************************/


