

/******************************************************************/
/***                                                            ***/
/***          Algorithms:  Heapsort                             ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(heap_demo_delay, 0.25).


/*** tests ********************************************************/


test(teaching:heaps, create(xpce, 1)) :-
   heap_created_example.
test(teaching:heaps, create(xpce, 2)) :-
   heap_create_example.
test(teaching:heaps, sort(xpce, 3)) :-
   heap_sort_example.

heap_created_example :-
   heap_create([13,12,11,10,9,8,7,6,5,4,3,2,1], Heap),
   binary_tree_to_picture(_, Heap).

heap_create_example :-
   heap_create(_, [8,7,6,5,4,3,2,1], _).

heap_sort_example :-
   heap_sort([6,5,4,3,2,1], Elements),
   writeln(user, Elements).


/*** interface ****************************************************/


/* heap_sort(Xs, Ys) <-
      */

heap_sort(Xs, Ys) :-
   heap_create(Picture, Xs, Heap),
   heap_to_ord_set(Picture, Heap, Ys),
   send(Picture, destroy),
   !.


/******************************************************************/


