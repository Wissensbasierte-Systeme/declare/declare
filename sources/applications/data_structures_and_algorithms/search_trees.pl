

/******************************************************************/
/***                                                            ***/
/***          Data Structures:  Search Trees                    ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(teaching:search_trees, create(xpce, 1)) :-
   search_tree_example.

search_tree_example :-
%  Xs = [5,2,1,3,4,6,7,8],
   Xs = [8,4,2,1,3,6,5,7,12,10,9,11,13],
%  Xs = [2,1,3],
   search_tree_create(Xs, Tree),
   binary_tree_to_picture(_, Tree).


/*** interface ****************************************************/


/* search_tree_create(Keys, Tree) <-
      */

search_tree_create(Keys, Tree) :-
   binary_tree_empty(T),
   search_tree_insert_multiple(Keys, T, Tree),
%  iterate_list(search_tree_insert_2, T, Keys, Tree),
   dwrite(xml, Tree).


/* search_tree_insert_multiple(Keys, Tree_1, Tree_2) <-
      */

search_tree_insert_multiple([Key|Keys], Tree_1, Tree_2) :-
   search_tree_insert(Key, Tree_1, Tree_3),
   search_tree_insert_multiple(Keys, Tree_3, Tree_2).
search_tree_insert_multiple([], Tree, Tree).


/* search_tree_insert(Key, Tree_1, Tree_2) <-
      */

search_tree_insert_2(Tree_1, Key, Tree_2) :-
   search_tree_insert(Key, Tree_1, Tree_2).

search_tree_insert(Key, Tree_1, Tree_2) :-
   binary_tree_empty(Tree_1),
   binary_tree_parse(Tree_2, Key, _, _).
search_tree_insert(Key, Tree_1, Tree_2) :-
   binary_tree_parse(Tree_1, Root, Left_1, Right_1),
   ( ( Key = Root,
       Tree_2 = Tree_1 )
   ; ( Key < Root,
       search_tree_insert(Key, Left_1, Left_2),
       binary_tree_parse(Tree_2, Root, Left_2, Right_1) )
   ; ( Key > Root,
       search_tree_insert(Key, Right_1, Right_2),
       binary_tree_parse(Tree_2, Root, Left_1, Right_2) ) ).


/******************************************************************/


