

/******************************************************************/
/***                                                            ***/
/***          Data Structures:  Heaps                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* heap_to_ord_set(Picture, Heap, Elements) <-
      */

heap_to_ord_set(Picture, Heap, Elements) :-
   heap_delete_elements_in_picture(Picture, Elements, Heap, _).
 

/* heap_delete_elements_in_picture(
         Picture, Elements, Heap_1, Heap_2) <-
      */

heap_delete_elements_in_picture(Picture, [], Heap, Heap) :-
   binary_tree_empty(Heap),
   !,
   send(Picture, clear).
heap_delete_elements_in_picture(
      Picture, [E|Es], Heap_1, Heap_2) :-
   binary_tree_parse(Heap_1, E, _, _),
   heap_delete_element_in_picture(Picture, E, Heap_1, Heap_3),
   heap_delete_elements_in_picture(Picture, Es, Heap_3, Heap_2).


/* heap_delete_element_in_picture(
         Picture, Element, Heap_1, Heap_2) <-
      */

heap_delete_element_in_picture(
      Picture, Element, Heap_1, Heap_2) :-
   heap_delete_last_element(Element, Heap_1, Heap_3),
   binary_tree_to_picture(Picture, Heap_3), 
   heap_triple_operator_loop(Picture, Heap_3, Heap_2),
   binary_tree_to_picture(Picture, Heap_2).
heap_delete_element_in_picture(
      Picture, Element, Heap_1, Heap_2) :-
   heap_delete_last_element(Last, Heap_1, Heap_3),
   heap_delete_element(Element, Last, Heap_3, Heap_4),
   binary_tree_to_picture(Picture, Heap_4), 
   heap_triple_operator_loop(Picture, Heap_4, Heap_2),
   binary_tree_to_picture(Picture, Heap_2).
   
heap_delete_element(Element, Last, Heap_1, Heap_2) :-
   binary_tree_parse(Heap_1, Element, L1, R1),
   !,
   binary_tree_parse(Heap_2, Last, L1, R1).
heap_delete_element(Element, Last, Heap_1, Heap_2) :-
   binary_tree_parse(Heap_1, X, L1, R1),
   ( heap_delete_element(Element, Last, L1, L2),
     R2 = R1
   ; heap_delete_element(Element, Last, R1, R2),
     L2 = L1 ),
   binary_tree_parse(Heap_2, X, L2, R2).


/*** implementation ***********************************************/


/* heap_delete_last_element(Last, Heap_1, Heap_2) <-
      */

heap_delete_last_element(Last, Heap_1, Heap_2) :-
   binary_tree_parse(Heap_1, Last, L1, R1),
   binary_tree_empty(L1),
   binary_tree_empty(R1),
   !,
   binary_tree_empty(Heap_2).
heap_delete_last_element(Last, Heap_1, Heap_2) :-
   binary_tree_parse(Heap_1, X, L1, R1),
   heap_delete_left(L1, R1),
   !,
   heap_delete_last_element(Last, L1, L2),
   binary_tree_parse(Heap_2, X, L2, R1).
heap_delete_last_element(Last, Heap_1, Heap_2) :-
   binary_tree_parse(Heap_1, X, L1, R1),
   \+ heap_delete_left(L1, R1),
   !,
   heap_delete_last_element(Last, R1, R2),
   binary_tree_parse(Heap_2, X, L1, R2).


/* heap_delete_left(L, R) <-
      */

heap_delete_left(L, _) :-
   \+ binary_tree_is_complete(L).
heap_delete_left(L, R) :-
   binary_tree_is_complete(L),
   binary_tree_is_complete(R),
   binary_tree_to_height(L, H_L),
   binary_tree_to_height(R, H_R),
   H_L > H_R.


/******************************************************************/


