

/******************************************************************/
/***                                                            ***/
/***          Data Structures:  Heaps                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* heap_create(Picture, Keys, Heap) <-
      */

heap_create(Picture, Keys, Heap) :-
   binary_tree_empty(Heap_E),
   heap_insert_multiple(Picture, Keys, Heap_E, Heap),
   !.

heap_create(Keys, Heap) :-
   binary_tree_empty(Heap_E),
   heap_insert_multiple(Keys, Heap_E, Heap),
   !.


/* heap_insert_multiple(Picture, Keys, Heap_1, Heap_2) <-
      */

heap_insert_multiple(Picture, [Key|Keys], Heap_1, Heap_2) :-
   heap_insert(Key, Heap_1, Heap_3),
   binary_tree_to_picture(Picture, Heap_3),
   dislog_variable_get(heap_demo_delay, Seconds),
   wait_seconds(Seconds),
   heap_triple_operator_loop(Picture, Heap_3, Heap_4),
   heap_insert_multiple(Picture, Keys, Heap_4, Heap_2).
heap_insert_multiple(_, [], Heap, Heap).


/* heap_insert_multiple(Keys, Heap_1, Heap_2) <-
      */

heap_insert_multiple([Key|Keys], Heap_1, Heap_2) :-
   heap_insert(Key, Heap_1, Heap_3),
   heap_triple_operator_loop(Heap_3, Heap_4),
   heap_insert_multiple(Keys, Heap_4, Heap_2).
heap_insert_multiple([], Heap, Heap).


/* heap_insert(Key, Heap_1, Heap_2) <-
      */

heap_insert(Key, Heap_1, Heap_2) :-
   binary_tree_empty(Heap_1),
   !,
   binary_tree_parse(Heap_2, Key, _, _).
heap_insert(Key, Heap_1, Heap_2) :-
   binary_tree_parse(Heap_1, X1, L1, R1),
   ( heap_insert_left(L1, R1),
     heap_insert(Key, L1, L2),
     binary_tree_parse(Heap_2, X1, L2, R1)
   ; heap_insert(Key, R1, R2),
     binary_tree_parse(Heap_2, X1, L1, R2) ).


/*** implementation ***********************************************/


/* heap_triple_operator_loop(Picture, Heap_1, Heap_2) <-
      */

heap_triple_operator_loop(Picture, Heap_1, Heap_2) :-
   heap_triple_operator_step(Heap_1, Heap),
   Heap \= Heap_1,
   !,
   binary_tree_to_picture(Picture, Heap),
   heap_triple_operator_loop(Picture, Heap, Heap_2).
heap_triple_operator_loop(_, Heap, Heap).

heap_triple_operator_loop(Heap_1, Heap_2) :-
   heap_triple_operator_step(Heap_1, Heap),
   Heap \= Heap_1,
   !,
   heap_triple_operator_loop(Heap, Heap_2).
heap_triple_operator_loop(Heap, Heap).
 

/* heap_triple_operator_step(Heap_1, Heap_2) <-
      */

heap_triple_operator_step(Heap_1, Heap_2) :-
   heap_triple_operator(Heap_1, Heap_2),
   !.
heap_triple_operator_step(Heap_1, Heap_2) :-
   binary_tree_parse(Heap_1, X, L1, R1),
   !,
   heap_triple_operator_step(L1, L2),
   heap_triple_operator_step(R1, R2),
   binary_tree_parse(Heap_2, X, L2, R2).
heap_triple_operator_step(Heap, Heap) :-
   binary_tree_empty(Heap).


/* heap_triple_operator(Heap_1, Heap_2) <-
      */

heap_triple_operator(Heap_1, Heap_2) :-
   binary_tree_parse(Heap_1, X, L1, R1),
   binary_tree_parse(L1, Y, A, B),
   binary_tree_parse(R1, Z, C, D),
   heap_elements_rearrange(X-Y-Z, U-V-W),
   binary_tree_parse(L2, V, A, B),
   binary_tree_parse(R2, W, C, D),
   binary_tree_parse(Heap_2, U, L2, R2).
heap_triple_operator(Heap_1, Heap_2) :-
   binary_tree_parse(Heap_1, X, L1, R1),
   binary_tree_empty(L1),
   binary_tree_parse(R1, Z, C, D),
   heap_elements_rearrange(X-Z, U-W),
   binary_tree_parse(R2, W, C, D),
   binary_tree_parse(Heap_2, U, L1, R2).
heap_triple_operator(Heap_1, Heap_2) :-
   binary_tree_parse(Heap_1, X, L1, R1),
   binary_tree_parse(L1, Y, A, B),
   binary_tree_empty(R1),
   heap_elements_rearrange(X-Y, U-V),
   binary_tree_parse(L2, V, A, B),
   binary_tree_parse(Heap_2, U, L2, R1).

heap_elements_rearrange(X-Y-Z, Y-X-Z) :-
   Y < X, Y < Z.
heap_elements_rearrange(X-Y-Z, Z-Y-X) :-
   Z < X, Z < Y.
heap_elements_rearrange(X-Y, Y-X) :-
   Y < X.


/* heap_insert_left(L, R) <-
      */

heap_insert_left(L, R) :-
   binary_tree_to_height(L, H),
   binary_tree_to_height(R, H),
   binary_tree_is_complete(R). 
heap_insert_left(L, R) :-
   binary_tree_to_height(L, H_L),
   binary_tree_to_height(R, H_R),
   H_L > H_R,
   \+ binary_tree_is_complete(L). 
   

/******************************************************************/


