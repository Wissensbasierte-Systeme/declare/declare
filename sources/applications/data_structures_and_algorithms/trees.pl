

/******************************************************************/
/***                                                            ***/
/***          Data Structures:  Trees                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* edges_to_xml_tree(Edges, Root, Tree) <-
      */

edges_to_xml_tree(Edges, Root, Tree) :-
   findall( Vertex,
      member(Root-Vertex, Edges),
      Vertices ),
   maplist( edges_to_xml_tree(Edges),
      Vertices, Trees ),
   Tree = node:[key:Root]:Trees.


/******************************************************************/


