

/******************************************************************/
/***                                                            ***/
/***          Data Structures:  Binary Trees                    ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(binary_tree_mode, xml).


/*** interface ****************************************************/


/* binary_tree_empty(Tree) <-
      */

binary_tree_empty(Tree) :-
   ( dislog_variable_get(binary_tree_mode, xml),
     Tree = node:[]:[]
   ; Tree = [] ),
   !.


/* binary_tree_parse(Tree, Key, Lson, Rson) <-
      */

binary_tree_parse(Tree, Key, Empty, Empty) :-
   binary_tree_empty(Empty),
   ( dislog_variable_get(binary_tree_mode, xml),
     Tree = node:[key:Key]:[]
   ; Tree = [Key] ),
   !.
binary_tree_parse(Tree, Key, Lson, Rson) :-
   ( dislog_variable_get(binary_tree_mode, xml),
     Tree = node:[key:Key]:[Lson, Rson]
   ; Tree = [Key, Lson, Rson] ),
   !.


/* search_in_binary_tree(Key, Tree) <-
      */

search_in_binary_tree(Key, Tree) :-
   binary_tree_parse(Tree, Key, _, _).
search_in_binary_tree(Key, Tree) :-
   binary_tree_parse(Tree, Root, Left, Right),
   ( ( Key < Root,
       !,
       Tree = Left )
   ; ( Key > Root,
       !,
       Tree = Right ) ),
   search_in_binary_tree(Key, Tree).


/* keys_to_binary_tree(Keys, Tree) <-
      */

keys_to_binary_tree(Keys, Tree) :-
   binary_tree_empty(Empty),
   insert_into_binary_tree_multiple(Keys, Empty, Tree).


/* insert_into_binary_tree_multiple(Keys, Tree, New_Tree) <-
      */

insert_into_binary_tree_multiple([], Tree, Tree).
insert_into_binary_tree_multiple([Key|Keys], Tree, New_Tree) :-
   insert_into_binary_tree(Key, Tree, Tree_2),
   insert_into_binary_tree_multiple(Keys, Tree_2, New_Tree).


/* insert_into_binary_tree(Key, Tree, New_Tree) <-
      */

insert_into_binary_tree(Key, Tree, New_Tree) :-
   binary_tree_parse(Tree, Root, Left, Right),
   !,
   ( ( Key = Root,
       % Key is already in Tree
       New_Tree = Tree )
   ; ( Key < Root,
       insert_into_binary_tree(Key, Left, Left_1),
       binary_tree_parse(New_Tree, Root, Left_1, Right) )
   ; ( Key > Root,
       insert_into_binary_tree(Key, Right, Right_1),
       binary_tree_parse(New_Tree, Root, Left, Right_1) ) ).
insert_into_binary_tree(Key, Tree, New_Tree) :-
   binary_tree_empty(Tree),
   binary_tree_parse(New_Tree, Key, Tree, Tree).


/* binary_tree_to_picture(Picture, Tree) <-
      */

binary_tree_to_picture(Picture, Tree) :-
   binary_tree_add_brothers(Tree, Tree_2),
   binary_tree_to_picture_bfs(Picture, Tree_2),
   dislog_variable_get(heap_demo_delay, Seconds),
   wait_seconds(Seconds),
   !.

binary_tree_to_picture_bfs(Picture, Tree) :-
   binary_tree_empty(Tree),
   !,
   send(Picture, clear),
   send(Picture, synchronise).
binary_tree_to_picture_bfs(Picture, Tree) :-
   binary_tree_to_vertices_and_edges(Tree, Vertices, Edges),
   maplist( binary_tree_vertex_for_display,
      Vertices, Vertices_2 ),
   first(Vertices, V),
   ( integer(V) ->
     Root = V
   ; term_to_atom(V, Root) ),
   vertices_edges_to_picture_bfs(
      [Root], Vertices_2, Edges, Picture),
   send(Picture, synchronise).

binary_tree_vertex_for_display(X, Y) :-
   concat('*', _, X),
   !,
   Y = X-' '-circle-white-small.
binary_tree_vertex_for_display(X, X).


/* binary_tree_to_vertices_and_edges(Tree, Vertices, Edges) <-
      */

binary_tree_to_vertices_and_edges(Tree, Vertices, Edges) :-
   binary_tree_parse(Tree, X, T1, T2),
   binary_tree_to_vertices_and_edges(T1, Vs1, Es1),
   binary_tree_to_vertices_and_edges(T2, Vs2, Es2),
   findall( X-Y,
      ( member(T, [T1, T2]),
        binary_tree_parse(T, Y, _, _) ),
      Es ),
   append([X|Vs1], Vs2, Vertices),
   append([Es, Es1, Es2], Edges).
binary_tree_to_vertices_and_edges(Tree, [], []) :-
   binary_tree_empty(Tree).


/* binary_tree_to_edges(Tree, Edges) <-
      */

binary_tree_to_edges(Tree, Edges) :-
   binary_tree_to_edges([], Tree, Edges).

binary_tree_to_edges(_, Tree, []) :-
   binary_tree_empty(Tree).
binary_tree_to_edges(Fathers, Tree, Edges) :-
   binary_tree_parse(Tree, Key, Lson, Rson),
   findall( V-Key,
      member(V, Fathers),
      Edges_0 ),
   binary_tree_to_edges([Key], Lson, Edges_1),
   binary_tree_to_edges([Key], Rson, Edges_2),
   append([Edges_0, Edges_1, Edges_2], Edges).


/* binary_tree_to_number_of_nodes(Tree, N) <-
      */

binary_tree_to_number_of_nodes(Tree, 0) :-
   binary_tree_empty(Tree),
   !.
binary_tree_to_number_of_nodes(Tree, N) :-
   binary_tree_parse(Tree, _, L, R),
   binary_tree_to_number_of_nodes(L, X),
   binary_tree_to_number_of_nodes(R, Y),
   N is X + Y + 1.


/* binary_tree_is_complete(Tree) <-
      */

binary_tree_is_complete(Tree) :-
   binary_tree_empty(Tree),
   !.
binary_tree_is_complete(Tree) :-
   binary_tree_parse(Tree, _, L, R),
   binary_tree_is_complete(L),
   binary_tree_is_complete(R),
   binary_tree_to_height(L, H),
   binary_tree_to_height(R, H).


/* binary_tree_to_height(Tree, Height) <-
      */

binary_tree_to_height(Tree, -1) :-
   binary_tree_empty(Tree),
   !.
binary_tree_to_height(Tree, Height) :-
   binary_tree_parse(Tree, _, L, R),
   binary_tree_to_height(L, Height_L),
   binary_tree_to_height(R, Height_R),
   maximum(Height_L, Height_R, Max),
   Height is Max + 1,
   !.


/* binary_tree_add_brothers(Tree_1, Tree_2) <-
      */

binary_tree_add_brothers(Tree, Tree) :-
   binary_tree_empty(Tree),
   !.
binary_tree_add_brothers(Tree_1, Tree_2) :-
   binary_tree_parse(Tree_1, X, L1, R1),
   binary_tree_empty(L1),
   \+ binary_tree_empty(R1),
   !,
   gensym('*', N),
   binary_tree_parse(L2, N, _, _),
   binary_tree_add_brothers(R1, R2),
   binary_tree_parse(Tree_2, X, L2, R2).
binary_tree_add_brothers(Tree_1, Tree_2) :-
   binary_tree_parse(Tree_1, X, L1, R1),
   binary_tree_empty(R1),
   \+ binary_tree_empty(L1),
   !,
   gensym('*', N),
   binary_tree_parse(R2, N, _, _),
   binary_tree_add_brothers(L1, L2),
   binary_tree_parse(Tree_2, X, L2, R2).
binary_tree_add_brothers(Tree_1, Tree_2) :-
   binary_tree_parse(Tree_1, X, L1, R1),
   binary_tree_add_brothers(L1, L2),
   binary_tree_add_brothers(R1, R2),
   binary_tree_parse(Tree_2, X, L2, R2).


/******************************************************************/


