

/******************************************************************/
/***                                                            ***/
/***          Algorithms:  Quicksort                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


quick_sort_example :-
%  quick_sort([4,3,2,1]).
   quick_sort([8,4,10,7,2,6,3,5,1,9]).

quick_sort(List) :-
   dislog_variable_get(mine_sweeper_board_size,Size-_),
   list_to_array(List,Array),
   length(List,N),
   generate_interval(1,Size,Interval),
   clear_array_on_board(1,Interval),
   send_colour_to_board(colour(yellow),1,Interval),
   send_array_to_board(1,[1,N],Array), wait,
   quick_sort([1,N],Array,Array_2),
   writeln(Array_2).


quick_sort([N,N],Array,Array) :-
   wait,
   send_colour_save(colour(white),N-1), wait,
   !.
quick_sort([N,M],Array,Array) :-
   N > M,
   !.
quick_sort([N,M],Array_1,Array_2) :-
   M is N + 1,
   !,
   send_colour_save(colour(green),N-1),
   send_colour_save(colour(red),M-1), wait,
   ( ( aref(N,Array_1,Item_l),
       aref(M,Array_1,Item_r),
       Item_l > Item_r,
       array_exchange(N,M,Array_1,Array_2), wait )
   ; Array_2 = Array_1 ),
   send_colour_save(colour(white),N-1),
   send_colour_save(colour(white),M-1).
quick_sort([N,M],Array_1,Array_2) :-
   aref(M,Array_1,Pivot),
   K is M - 1,
   send_colour_save(colour(green),N-1), wait,
   send_colour_save(colour(red),K-1), wait,
   quick_sort_divide([N,K],N-M-Pivot,Array_1,Midlle,Array_3),
   N2 is Midlle - 1,
   M2 is Midlle + 1,
   send_colour_save(colour(white),Midlle-1), wait,
   quick_sort([N,N2],Array_3,Array_4),
   quick_sort([M2,M],Array_4,Array_2).


quick_sort_divide([N,M],L-R-_,Array_1,N,Array_2) :-
   N > M,
   ( M < L
   ; send_colour_save(colour(yellow),M-1) ),
   ( ( N \= R,
       send_colour_save(colour(red),R-1), wait,
       array_exchange(N,R,Array_1,Array_2),
       writeln(array_exchange_1(N,R,Array_1,Array_2)) )
   ; Array_2 = Array_1 ).
quick_sort_divide([N,M],L-R-Pivot,Array_1,Middle,Array_2) :-
   aref(N,Array_1,Item),
   Item < Pivot,
   !,
   K is N + 1,
   ( ( N = M,
       send_colour_save(colour(red),N-1) )
   ; send_colour_save(colour(yellow),N-1) ),
   send_colour_save(colour(green),K-1), wait,
   quick_sort_divide([K,M],L-R-Pivot,Array_1,Middle,Array_2).
quick_sort_divide([N,M],L-R-Pivot,Array_1,Middle,Array_2) :-
   aref(M,Array_1,Item),
   Item > Pivot,
   !,
   K is M - 1,
   ( ( N = M,
       send_colour_save(colour(green),N-1) )
   ; ( send_colour_save(colour(yellow),M-1),
       send_colour_save(colour(red),K-1), wait ) ),
   quick_sort_divide([N,K],L-R-Pivot,Array_1,Middle,Array_2).
quick_sort_divide([N,M],L-R-Pivot,Array_1,Middle,Array_2) :-
   N1 is N + 1,
   M1 is M - 1,
   array_exchange(N,M,Array_1,Array_3),
   send_colour_save(colour(green),N1-1),
   send_colour_save(colour(red),M1-1), wait,
   writeln(array_exchange_2(N,M,Array_1,Array_3)),
   quick_sort_divide([N1,M1],L-R-Pivot,Array_3,Middle,Array_2).

array_exchange(N,N,Array,Array) :-
   !.
array_exchange(N,M,Array_1,Array_2) :-
   aref(N,Array_1,Item_l),
   aref(M,Array_1,Item_r),
   aset(N,Array_1,Item_r,Array_3),
   aset(M,Array_3,Item_l,Array_2),
   send_label(Item_l,M-1),
   send_label(Item_r,N-1), wait,
   send_colour_save(colour(yellow),N-1),
   send_colour_save(colour(yellow),M-1).

send_colour_save(Colour,N-Line) :-
   send_colour(Colour,N-Line),
   !.
send_colour_save(_,_-_).
   

/******************************************************************/


