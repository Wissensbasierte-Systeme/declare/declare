

/******************************************************************/
/***                                                            ***/
/***     Data Structures and Algorithms:  Graphical Desktop     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* ds_and_alg_inspector <-
      */

ds_and_alg_inspector :-
   dislog_variable_get(source_path, Sources),
   concat(['file://', Sources, 'applications/',
      'data_structures_and_algorithms/ds_and_alg.html'], Path),
   Size = size(450, 260),
   file_to_cms_doc_window(
      'Declare', Path, Size, ds_and_alg_link_handler).

ds_and_alg_link_handler(Atom) :-
   term_to_atom(Goal, Atom),
   call(Goal).


/******************************************************************/


