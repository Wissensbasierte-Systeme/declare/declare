

/******************************************************************/
/***                                                            ***/
/***          Algorithms:  Mergesort                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


sb :-
   sort_board(10).

sort_board(N) :-
   dislog_variable_set(mine_sweeper_board_size,N-2),
   new(Frame,dialog('Merge Sort')),
   send(Frame, append,
      new(BTS,dialog_group(buttoms,group))),
   send(BTS,append,button('Quit',
      message(Frame,destroy)), right),
   send(BTS, layout_dialog),
   send(Frame, append,
      new(Board,picture('Board')),below),
   dislog_variable_get(mine_sweeper_board_size,N-M),
   forall(
      ( generate_interval(1,N,Interval_N),
        generate_interval(1,M,Interval_M),
        member(X,Interval_N), member(Y,Interval_M) ),
      create_field(Board,_,X-Y) ),
   X_Size is 403 + ( N - 8 ) * 50,
   Y_Size is 403 + ( M - 8 ) * 50,
   send(Board,size,size(X_Size,Y_Size)),
   assert(board_address(Board)),
   send(Frame,open).

merge_sort(List) :-
   dislog_variable_get(mine_sweeper_board_size,Size-_),
   list_to_array(List,Array),
   length(List,N),
   generate_interval(1,Size,Interval),
   clear_array_on_board(1,Interval),
   send_colour_to_board(yellow,1,Interval),
   send_array_to_board(1,[1,N],Array), wait,
   merge_sort([1,N],Array,_),
   generate_interval(1,N,Interval_Used),
   send_colour_to_board(cyan,1,Interval_Used).

merge_sort([N,N],Array,Array) :-
   !.
merge_sort([N,M],Array_1,Array_2) :-
   generate_interval(N,M,Interval),
   K_l is truncate((N+M)/2),
   K_r is K_l + 1,
   generate_interval(N,K_l,Interval_l),
   generate_interval(K_r,M,Interval_r),
   merge_sort([N,K_l],Array_1,Array_3),
   merge_sort([K_r,M],Array_3,Array_4),
   send_array_to_board(2,[N,K_l],Array_3),
   send_colour_to_board(green,2,Interval_l),
   send_array_to_board(2,[K_r,M],Array_4),
   send_colour_to_board(red,2,Interval_r), wait,
   clear_array_on_board(1,Interval), wait,
   new_array(New_1),
   merge_arrays(N,[N,K_l],[K_r,M],Array_4,New_1,New_2),
   merge_array_in(N,[N,M],New_2,Array_4,Array_2),
   send_array_to_board(1,[N,M],New_2), wait,
   send_colour_to_board(yellow,2,Interval),
   clear_array_on_board(2,Interval), wait.
   
clear_array_on_board(Line,[N|Ns]) :-
   send_label('',N-Line),
   clear_array_on_board(Line,Ns).
clear_array_on_board(_,[]).
   
send_colour_to_board(Colour,Line,[N|Ns]) :-
   send_colour(Colour,N-Line),
   send_colour_to_board(Colour,Line,Ns).
send_colour_to_board(_,_,[]).
   
send_array_to_board(_,[N,M],_) :-
   N > M,
   !.
send_array_to_board(Line,[N,M],Array) :-
   aref(N,Array,Item),
   send_label(Item,N-Line),
   K is N + 1,
   send_array_to_board(Line,[K,M],Array).

merge_array_in(_,[N1,N2],_,New,New) :-
   N1 > N2,
   !.
merge_array_in(N,[N1,N2],Array,New_1,New_2) :-
   aref(N1,Array,Item),
   aset(N,New_1,Item,New_3),
   M is N + 1,
   N3 is N1 + 1,
   merge_array_in(M,[N3,N2],Array,New_3,New_2).

merge_arrays(N,[N1,N2],[M1,M2],Array,New_1,New_2) :-
   ( ( N1 > N2,
       merge_array_in(N,[M1,M2],Array,New_1,New_2) )
   ; ( M1 > M2,
       merge_array_in(N,[N1,N2],Array,New_1,New_2) ) ),
   !.
merge_arrays(N,[N1,N2],[M1,M2],Array,New_1,New_2) :-
   aref(N1,Array,Item_l),
   aref(M1,Array,Item_r),
   M is N + 1,
   ( ( Item_l > Item_r,
       M3 is M1 + 1,
       aset(N,New_1,Item_r,New_3),
       send_label('',M1-2),
       send_label(Item_r,N-1), wait,
       merge_arrays(M,[N1,N2],[M3,M2],Array,New_3,New_2) )
   ; ( N3 is N1 + 1,
       aset(N,New_1,Item_l,New_3),
       send_label('',N1-2),
       send_label(Item_l,N-1), wait,
       merge_arrays(M,[N3,N2],[M1,M2],Array,New_3,New_2) ) ).


list_to_array(List,Array) :-
   new_array(New_Array),
   length(List,N),
   generate_interval(1,N,Interval),
   pair_lists_2(Interval,List,Pairs),
   iterate_list( aset_array,
      New_Array, Pairs, Array ).

aset_array(Array_1,Index-Item,Array_2) :-
   aset(Index,Array_1,Item,Array_2).


/******************************************************************/


