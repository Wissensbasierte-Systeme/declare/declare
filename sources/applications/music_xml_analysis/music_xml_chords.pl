

/******************************************************************/
/***                                                            ***/
/***          MusicXML Analysis:  Chords                        ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(music_xml, chord(interval)) :-
   Interval_1 = 'grosse Terz',
   Interval_2 = 'kleine Terz',
   Intervals = [Interval_1, Interval_2],
   mx_chords_intervals_to_chord_name(Intervals, Chordname),
   write(Chordname).

test(music_xml, chord(chordname)) :-
   Chord = chord:[
      note:[
         pitch:[ step:['C'], octave:['4'], alter:['0'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['E'], octave:['4'], alter:['-1'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['G'], octave:['4'], alter:['0'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['B'], octave:['4'],alter:['-1'] ],
         duration:['4'],
         type:[whole] ] ],
   mx_chords_chord_to_name(Chord, Name),
   write(Name).

test(music_xml, chord(interval_to_unknown_chord)) :-
   Interval_1 = 'Quarte',
   Interval_2 = 'kleine Terz',
   Interval_3 = 'grosse Sekunde',
   Interval_4 = 'kleine Sekunde',
   Interval_5 = 'grosse Sekunde',
   Intervals = [
      Interval_1, Interval_2, Interval_3,
      Interval_4, Interval_5],
   mx_chords_intervals_to_chord_name(Intervals, Chordname),
   write(Chordname).

test(music_xml, chord(switchend_chord)) :-
   Chord_1 = chord:[
      note:[
         pitch:[ step:['C'], octave:['4'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['E'], octave:['4'], alter:['-1'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['G'], octave:['4'], alter:['0'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['B'], octave:['4'] ],
         duration:['4'],
         type:[whole] ] ],
   mx_chords_chord_to_switched_chord(down, Chord_1, Chord_2),
   dwrite(xml, Chord_2).

test(music_xml, chord(root)) :-
   Chord = chord:[
      note:[
         pitch:[ step:['E'], octave:['4'], alter:['-1'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['G'], octave:['4'], alter:['0'] ],
          duration:['4'],
          type:[whole] ],
       note:[
         pitch:[ step:['B'], octave:['4'], alter:['-1'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[
         step:['D'] ,
            octave:['5'] ],
         duration:['4'],
         type:[whole] ] ],
   mx_chords_chord_to_root(Chord, Root),
   write(Root).

test(music_xml, chord(name_inversion)) :-
   Chord = chord:[
      note:[
         pitch:[ step:['C'], octave:['5'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['E'], octave:['4'], alter:['-1'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['G'], octave:['4'], alter:['0'] ],
         duration:['4'],
         type:[whole] ] ],
   mx_chords_chord_to_inversion(Chord, A),
   write(A).

test(music_xml, chord(sus_chord)) :-
   Chord = chord:[
      note:[
         pitch:[ step:['C'], octave:['4'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['D'], octave:['4'], alter:['0'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['G'], octave:['4'], alter:['0'] ],
         duration:['4'],
         type:[whole] ] ],
   mx_chords_chord_to_sus(Chord, Name),
   write(Name).

test(music_xml, chord(contracted)) :-
   Chord = chord:[
      note:[
         pitch:[ step:['C'], octave:['3'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['D'], octave:['5'], alter:['0'] ],
         duration:['4'],
         type:[whole] ] ],
   mx_chords_chord_to_contracted_chord(Chord, Contracted),
   dwrite(xml, Contracted).

test(music_xml, chord(switch)) :-
   Chord = chord:[
      note:[
         pitch:[ step:['C'], octave:['4'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['E'], octave:['4'], alter:['-1'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['G'], octave:['4'], alter:['0'] ],
         duration:['4'],
         type:[whole] ] ],
   mx_chords_chord_to_switched_chord(up, Chord, Chord_up),
   dwrite(xml, Chord_up),
   mx_chords_chord_to_switched_chord(down, Chord, Chord_down),
   dwrite(xml, Chord_down).

test(music_xml, chord(stacked_thirds)) :-
   Chord_1 = chord:[
      note:[
         pitch:[ step:['C'], octave:['4'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['E'], octave:['4'], alter:['-1'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['G'], octave:['3'], alter:['0'] ],
         duration:['4'],
         type:[whole] ] ],
   mx_chords_chord_in_stacked_thirds(Chord_1, Chord_2, X),
   writeln(X),
   dwrite(xml, Chord_2).

test(music_xml, chord(circle_progression)) :-
   Chord_A = chord:[
      note:[
         pitch:[ step:['C'], octave:['5'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['E'], octave:['5'], alter:['-1'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['G'], octave:['5'] ],
         duration:['4'],
         type:[whole] ] ],
   Chord_B = chord:[
      note:[
         pitch:[ step:['F'], octave:['4'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['A'], octave:['4'], alter:['-1'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[step:['C'], octave:['5'], alter:['0'] ],
         duration:['4'],
         type:[whole] ] ],
   mx_chords_circle_progression(Chord_A, Chord_B).

test(music_xml, chord(cadence)) :-
   Chord_A = chord:[
      note:[
         pitch:[ step:['C'] , octave:['5'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['E'], octave:['5'], alter:['0'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['G'], octave:['5'], alter:['0'] ],
        duration:['4'],
        type:[whole] ] ],
   Chord_B = chord:[
      note:[
         pitch:[ step:['F'] , octave:['4'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['A'], octave:['4'], alter:['0'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[step:['C'], octave:['5'], alter:['0'] ],
         duration:['4'],
         type:[whole] ] ],
   mx_chords_cadence('F', Chord_A, Chord_B).

test(music_xml, chord(is_tonic)) :-
   Chord = chord:[
      note:[
         pitch:[ step:['F'], octave:['3'], alter:['1'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['A'], octave:['3'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[ step:['C'], octave:['4'], alter:['1'] ],
         duration:['4'],
         type:[whole] ] ],
   mx_chords_chord_is_tonic('f#', Chord).


/*** interface ****************************************************/


/* mx_chord_to_name(Chord, Name) <-
      returns the name of a chord inclusive its root e.g. C-Dur */

mx_chords_chord_to_name(Chord, Name) :-
   mx_chords_chord_to_chord_name(Chord, Name_2),
   ( mx_chords_chord_to_root(Chord, Root_Step) ->
     concat([Root_Step, '-', Name_2], Name)
   ; Name = Name_2 ).


/* mx_chords_intervals_to_chord_name(Intervals, Chordname) <-
      returns the type-name of a chord e.g. Dur */

mx_chords_chord_to_chord_name(Chord, Chordname) :-
   mx_chords_chord_to_intervals(Chord, Intervals),
   mx_chords_intervals_to_chord_name(Intervals, Chordname).

mx_chords_intervals_to_chord_name(Intervals, Chordname) :-
     ( mx_chords_intervals_are_known_chord(Intervals, Chordname) ->
       dislog_variable_set(mx_chords_known_chord, true)
     ; Chordname = Intervals,
       dislog_variable_set(mx_chords_known_chord, false) ).

mx_chords_intervals_to_triad(Intervals, Triad) :-
   Intervals = [X, Y],
   Triples = [
      'kleine Terz' + 'kleine Terz' = 'Vermindert',
      'kleine Terz' + 'grosse Terz' = 'Moll',
      'grosse Terz' + 'kleine Terz' = 'Dur',
      'grosse Terz' + 'grosse Terz' = 'Uebermaessig' ],
   member(X + Y = Triad, Triples).

mx_chords_intervals_to_tetrad(Intervals, Tetrad) :-
   mx_chords_all_intervals_are_terz(Intervals),
   Intervals = [X, Y, Z],
   mx_chords_intervals_to_triad([X, Y], Triad),
   Triples = [
      'Dur' + 'kleine Terz' = 'Dominantseptakkord',
      'Dur' + 'grosse Terz' = 'Großer Septakkord',
      'Moll' + 'kleine Terz' = 'Mollseptakkord',
      'Moll' + 'grosse Terz' = 'Mollseptakkord mit großer Septime',
      'Vermindert' + 'kleine Terz' = 'Verminderter Septakkord',
      'Vermindert' + 'grosse Terz' = 'Halbverminderter Septakkord',
      'Uebermaessig' + 'kleine Terz' =
         'Uebermaessiger Septakkord' ],
   member(Triad + Z = Tetrad, Triples).

mx_chords_chord_is_unknown_chord(Chord) :-
   mx_chords_chord_to_intervals(Chord, Intervals),
   mx_chords_intervals_are_unknown_chord(Intervals).

mx_chords_intervals_are_unknown_chord(Intervals) :-
   \+ mx_chords_intervals_are_known_chord(Intervals, _).

mx_chords_chord_is_known_chord(Chord, Name) :-
   mx_chords_chord_to_intervals(Chord, Intervals),
   mx_chords_intervals_are_known_chord(Intervals, Name).

mx_chords_intervals_are_known_chord(Intervals, Name) :-
   ( mx_chords_intervals_to_sus(Intervals, Name)
   ; mx_chords_intervals_to_triad(Intervals, Name)
   ; mx_chords_intervals_to_tetrad(Intervals, Name) ).

mx_chords_chord_to_sus(Chord, Name) :-
   mx_chords_chord_to_contracted_chord(Chord, Contracted),
   mx_chords_chord_to_intervals(Contracted, Intervals),
   mx_chords_intervals_to_sus(Intervals, Name).

mx_chords_intervals_to_sus(Intervals, Name) :-
   ( Intervals = ['grosse Sekunde', 'Quarte'],
     Name = 'Sus2'
   ; Intervals = ['Quarte', 'grosse Sekunde'],
     Name = 'Sus4' ).

mx_chords_chord_to_contracted_chord(Chord, Contracted) :-
   Contracted := Chord*[/note/pitch/octave:['4']].

mx_chords_all_intervals_are_terz(Intervals) :-
   foreach(Interval, Intervals) do
      name_contains_name(Interval, 'Terz').

mx_chords_chord_in_stacked_thirds(Chord, Root_Position, 0) :-
   mx_chords_chord_sort(Chord, Root_Position),
   mx_chords_chord_to_intervals(Root_Position, Intervals),
   ( Intervals = [_,_,_]
   ; Intervals = [_,_] ),
   mx_chords_all_intervals_are_terz(Intervals),
   !.

mx_chords_chord_in_stacked_thirds(Chord, Root_Pos, 1) :-
   mx_chords_chord_sort(Chord, Chord_2),
   mx_chords_chord_to_switched_chord(down, Chord_2, Root_Pos),
   mx_chords_chord_to_intervals(Root_Pos, Intervals),
   ( Intervals = [_,_,_]
   ; Intervals = [_,_] ),
   mx_chords_all_intervals_are_terz(Intervals),
   !.

mx_chords_chord_in_stacked_thirds(Chord, Root_Pos, 2) :-
   mx_chords_chord_sort(Chord, Chord_2),
   mx_chords_chord_to_switched_chord(down, Chord_2, First_Inv),
   mx_chords_chord_to_switched_chord(down, First_Inv, Root_Pos),
   mx_chords_chord_to_intervals(Root_Pos, Intervals),
   ( Intervals = [_,_,_]
   ; Intervals = [_,_] ),
   mx_chords_all_intervals_are_terz(Intervals),
   !.

mx_chords_chord_in_stacked_thirds(Chord, Root_Pos, 3) :-
   mx_chords_chord_sort(Chord, Chord_2),
   mx_chords_chord_to_switched_chord(down, Chord_2, Sec_Inv),
   mx_chords_chord_to_switched_chord(down, Sec_Inv, First_Inv),
   mx_chords_chord_to_switched_chord(down, First_Inv, Root_Pos),
   mx_chords_chord_to_intervals(Root_Pos, Intervals),
   Intervals = [_,_,_],
   mx_chords_all_intervals_are_terz(Intervals),
   !.

mx_chords_chord_to_switched_chord(up, Chord_1, Chord_2) :-
   fn_item_parse(Chord_1, T:As:[Note_1|Notes]),
   [O1] := Note_1/pitch/octave/content::'*',
   add_to_atom(O1, '1', O2),
   Note_2 := Note_1*[/pitch/octave:[O2]],
   Chord_unsorted = T:As:[Note_2|Notes],
   mx_chords_chord_sort(Chord_unsorted, Chord_2),
   !.

mx_chords_chord_to_switched_chord(down, Chord_1, Chord_2) :-
   fn_item_parse(Chord_1, T:As:Notes),
   reverse(Notes, [Note_Last|Notes_Rest]),
   [O1] := Note_Last/pitch/octave/content::'*',
   add_to_atom(O1, '-1', O2),
   Note_New := Note_Last*[/pitch/octave:[O2]],
   Chord_unsorted = T:As:[Note_New|Notes_Rest],
   mx_chords_chord_sort(Chord_unsorted, Chord_2),
   !.


mx_chords_chord_sort(Chord_1, Chord_2) :-
   findall( Note,
      Note := Chord_1/note,
      Notes_1 ),
   mx_chords_notes_sort(Notes_1, Notes_2),
   Chord_2 = chord:Notes_2.

mx_chords_notes_sort(Notes_1, Notes_2) :-
   Comparator = mx_basics_note_smaller,
   mx_basics_mergesort_generic(Comparator, Notes_1, Notes_2).

mx_chords_chord_to_inversion(Chord, X) :-
   mx_chords_chord_in_stacked_thirds(Chord, _, X).

mx_chords_chord_to_intervals(Chord, Intervals) :-
   mx_chords_chord_to_tones(Chord, Tones),
   mx_chords_tones_to_intervals(Tones, Intervals).

mx_chords_chord_to_tones(Chord, Tones) :-
   findall( Note,
      Note := Chord/note,
      Notes ),
   maplist( mx_intervals_note_to_tone, Notes, Tones ).

mx_chords_tones_to_intervals(Tones, Intervals) :-
   findall( Interval,
      ( append(_, [Tone_1, Tone_2|_], Tones),
        mx_intervals_tones_to_interval_name(
           Tone_1, Tone_2, Interval) ),
      Intervals ).

mx_chords_chord_to_root(Chord, Root) :-
   mx_chords_chord_to_chord_name(Chord, _),
   dislog_variable_get(mx_chords_known_chord, true),
   mx_chords_chord_in_stacked_thirds(Chord, Root_P, _),
   [Step] := Root_P/nth_child::1/descendant::step/content::'*',
   ( [Alter] := Root_P/nth_child::1/descendant::alter/content::'*',
      ( Alter = '-1' ->
        atomic_concat(Step, b, Root)
      ; Alter = '1' ->
        atomic_concat(Step, '#', Root)
      ; Alter = '0' ->
        Root = Step ),
      !
   ; Root = Step ).


/* mx_chords_cadence(Key, Chord_A, Chord_B) <-
      becomes true, if Chord_A -> Chord_B is a cadence in the
      key Key. */

mx_chords_cadence(Key, Chord_A, Chord_B) :-
   mx_chords_circle_progression(Chord_A, Chord_B),
   mx_chords_chord_is_tonic(Key, Chord_B).

mx_chords_circle_progression(Chord_A, Chord_B) :-
   mx_chords_chord_sort(Chord_A, Chord_A2),
   mx_chords_chord_sort(Chord_B, Chord_B2),
   Root_A := Chord_A2/nth_child::1,
   Root_B := Chord_B2/nth_child::1,
   mx_intervals_notes_to_interval_name(Root_A, Root_B, Interval),
   name_contains_name(Interval, 'Quinte').

mx_chords_chord_is_tonic(Key, Chord) :-
   mx_chords_chord_to_name(Chord, Name),
   dislog_variable_get(mx_chords_known_chord, true),
   atomic_list_concat([Root|[Chordname]], '-', Name),
   ( Chordname = 'Moll',
     character_capital_to_lower(Root, R),
     Key = R,
     !
   ; Chordname = 'Dur',
     Key = Root ).


/******************************************************************/


