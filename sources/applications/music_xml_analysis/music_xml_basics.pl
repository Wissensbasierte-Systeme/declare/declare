

/******************************************************************/
/***                                                            ***/
/***          MusicXML Analysis:  Basics                        ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(music_xml, basics(tones)) :-
   dislog_variable_get(example_path,
      'music_xml/Dichterliebe01.xml', File_1),
   dislog_variable_get(output_path,
      'DLBasic.txt', File_2),
   mx_basics_file_to_tones(File_1, File_2).

test(music_xml, to_notes) :-
   dislog_variable_get(example_path,
      'music_xml/Dichterliebe01.xml', File_1),
   dislog_variable_get(output_path,
      'to_notes_DL.xml', File_2),
   mx_basics_file_to_note_file(File_1, File_2).

test(music_xml, basics(group)) :-
   dislog_variable_get(example_path,
      'music_xml/Dichterliebe01.xml', File_1),
   dislog_variable_get(output_path,
      'AkkDLBasic.xml', File_2),
   mx_basics_notes_file_group(File_1, File_2).

test(music_xml, basics(key)) :-
   dislog_variable_get(example_path,
      'music_xml/Dichterliebe01.xml', File),
%     'music_xml/003907B_.xml', File),
%     'music_xml/DebuMandSample.xml', File),
   dread(xml, File, [Music]),
   mx_basics_key(Music, Key),
   write(Key).

test(music_xml, basics(notes_to_min)) :-
   Note_1 = note:[
      pitch:[ step:['B'], octave:['4'], alter:['1'] ],
      duration:['4'],
      type:[whole] ],
   Note_2 = note:[
      pitch:[ step:['C'], octave:['5'], alter:['-1'] ],
      duration:['4'],
      type:[whole] ],
   mx_basics_notes_to_min(Note_1,Note_2,X),
   write(X).

test(music_xml, basics(sort)) :-
   Note_1 = note:[
      pitch:[ step:['D'], octave:['5'] ],
      duration:['4'],
      type:[whole] ],
   Note_2 = note:[
      pitch:[ step:['C'], octave:['5'] , alter:['0'] ],
      duration:['4'],
      type:[whole] ],
   Note_3 = note:[
      pitch:[ step:['B'], octave:['4'] ],
      duration:['4'],
      type:[whole] ],
   List_1 = [Note_1,Note_2,Note_3],
   Comparator = mx_basics_note_smaller,
   mx_basics_mergesort_generic(Comparator, List_1, List_2),
   writeln_list(List_2).


/*** interface ****************************************************/


/* mx_basics_file_to_tones(File_1, File_2) <-
      fills File_2 with the tone triple of File_1 */

mx_basics_file_to_tones(File_1, File_2) :-
   dread(xml, File_1, [Xml_1]),
   mx_basics_mx_to_tones_ip(Xml_1, Tones_ips),
%  mx_basics_tones_ips_to_mx_notes_ips(Tones_ips, Notes_ips),
%  mx_basics_mx_notes_to_mx(Notes_ips, Xml_1, Xml_2),
%  dwrite(xml, File_2, Xml_2).
   dwrite(txt, File_2, Tones_ips).


mx_basics_mx_to_tones_ip(Xml, Tones_ips) :-
   ddbase_aggregate( [P, Tones],
      ( Part := Xml/descendant::part::[@id=P],
        mx_basics_mx_to_tones(Part, Tones) ),
      Tones_ips ).


/* mx_basics_mx_to_tones(Xml, Tones) <-
      gets Tones = [Step, Alter, Octave] from an XML file */

mx_basics_mx_to_tones(Xml, Tones) :-
   findall( [S, A, O],
      ( X := Xml/descendant::'*'::[
           step/content::'*'=[S],
           octave/content::'*'=[O] ],
        ( [A] := X/alter/content::'*' ->
          true
        ; A = '0' ) ),
      Tones ).


/* mx_basics_file_to_note_file(File_1, File_2) <-
      reads all notes of File_1 and writes their pitch in File_2 */

mx_basics_file_to_note_file(File_1, File_2) :-
   dread(xml, File_1, [Xml_1]),
   mx_basics_mx_to_tones_ip(Xml_1, Tones_ips),
   mx_basics_tones_ips_to_mx_notes_ips(Tones_ips, Notes_ips),
   mx_basics_notes_to_mx(Notes_ips, Xml_1, Xml_2),
   dwrite(xml, File_2, Xml_2).

mx_basics_tones_ips_to_mx_notes_ips(Tones_ips, Notes_ips) :-
   foreach([P, Tones], Tones_ips),
   foreach([P, Notes], Notes_ips) do
      mx_basics_tones_to_mx_notes(Tones, Notes).


/* mx_basics_tones_to_mx_notes(Tones, Notes) <-
      creates mx-whole-notes of tone-triples */

mx_basics_tones_to_mx_notes(Tones, Notes) :-
   findall( Note,
      ( member([S, A, O], Tones),
        Note = note:[
           pitch:[step:[S], alter:[A], octave:[O]],
           duration:['4'],
           type:['whole'] ] ),
      Notes ).


/* mx_basics_mx_notes_to_mx(Notes_ips, Xml_1, Xml_2) <-
      combines selected notes in parts and the information of the
      former MusicXML-Document (Xml_1) to a new one (Xml_2). */

mx_basics_notes_to_mx(Notes_ips, Xml_1, Xml_2) :-
   ( foreach([P, Notes], Notes_ips),
     foreach([P, Measures], Measures_ips) do
        mx_basics_notes_to_measures_of_40(Notes, Measures) ),
   mx_basics_mx_to_title(Xml_1, Title),
   mx_basics_mx_to_partnames(Xml_1, Partnames),
   maplist( mx_basics_make_parth, Measures_ips, Partnames, PHs ),
   Part_List = 'part-list':PHs,
   ( foreach([P, Measures], Measures_ips), foreach(Part, Parts) do
        Part = part:[id:P]: Measures ),
   Xml_2 = 'score-partwise':[version:'2.0']:[
   work:['work-title':Title], Part_List, Parts].

mx_basics_notes_to_measures_of_40(Notes, Measures) :-
   split_multiple(40, Notes, Groups),
   findall( Measure,
      ( nth(N, Groups, Group),
        Measure = measure:[number:N]:[
           attributes:[
              divisions:['1'],
              key:[ fifth:['0'] ],
              time:[ beats:['4'], 'beat-type':['4'] ],
              clef:[ sign:['G'], line:['2'] ] ],
           Group ] ),
      Measures ).

mx_basics_mx_to_title(Xml_1, Title) :-
   [Title] := Xml_1/descendant::'work-title'/content::'*'.

mx_basics_mx_to_partnames(Xml_1, Partnames) :-
   let( Partnames := Xml_1/descendant::'part-name'/content::'*' ).

mx_basics_make_parth([P, _], Partname, Partheader) :-
   Partheader = 'score-part':[id:P]:['part-name':[Partname]].


/* mx_basics_key(Music, Key) <-
      returns the key of a MusicXML in FildNotation */

mx_basics_key(Music, Key) :-
   mx_basics_item_to_mx_key(Music, Mx_Key),
   [N_F] := Mx_Key/fifths/content::'*',
   [M] := Mx_Key/mode/content::'*',
   atom_number(N_F, F),
   mx_basics_circle_of_fifths(F, M, Key).

mx_basics_circle_of_fifths(F, M, Key) :-
   Circle = [
      ['Gb',eb], ['Db',bb], ['Ab',f], ['Eb',c], ['Bb',g], ['F',d],
      ['C',a], ['G',e], ['D',b], ['A','f#'], ['E','c#'], ['B','g#'],
      ['F#','d#'] ],
   Position is F+7,
   nth(Position, Circle, X),
   ( M = major, nth(1 ,X, Key)
   ; M = minor, nth(2, X, Key) ).

mx_basics_item_to_mx_key(Item, Mx_Key) :-
   findall( Key_Single,
      Key_Single := Item/descendant::key,
      Keys ),
   nth(1, Keys, Mx_Key).

mx_basics_notes_to_min(Note_1, Note_2, Note_1):-
   mx_intervals_distance(Note_1, Note_2, Distance),
   A is sign(Distance),
   A = 1.

mx_basics_notes_to_min(Note_1, Note_2, Note_2):-
   mx_intervals_distance(Note_1, Note_2, Distance),
   A is sign(Distance),
   A = -1.

mx_basics_notes_to_min(Note_1, Note_2, Note_1) :-
   mx_intervals_distance(Note_1, Note_2, 0).

mx_basics_note_smaller(Note_1, Note_2) :-
   mx_basics_notes_to_min(Note_1, Note_2, Note),
   !,
   Note_1 = Note.


/* mergesort_generic(Comparator, List, Sorted_List) <-
     */

mx_basics_mergesort_generic(_, Xs, Xs) :-
   length(Xs, N),
   N =< 1,
   !.
mx_basics_mergesort_generic(Comparator, Xs, Ys) :-
   middle_split(Xs, Xs1, Xs2),
   mx_basics_mergesort_generic(Comparator, Xs1, Ys1),
   mx_basics_mergesort_generic(Comparator, Xs2, Ys2),
   mx_basics_mergesort_merge_generic(Comparator, Ys1, Ys2, Ys).

mx_basics_mergesort_merge_generic(_, [], Xs, Xs) :-
   !.
mx_basics_mergesort_merge_generic(_, Xs, [], Xs) :-
   !.
mx_basics_mergesort_merge_generic(
   Comparator, [X1|Xs1], [X2|Xs2], [X|Xs]) :-
   ( apply(Comparator, [X1, X2]),
     X = X1,
     mx_basics_mergesort_merge_generic(
        Comparator, Xs1, [X2|Xs2], Xs)
   ; X = X2,
     mx_basics_mergesort_merge_generic(
        Comparator, [X1|Xs1], Xs2, Xs) ).


/******************************************************************/


