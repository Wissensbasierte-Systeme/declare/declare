

/******************************************************************/
/***                                                            ***/
/***          MusicXML Analysis:  Time                          ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(music_xml, time(attributes)) :-
   File = 'examples/music_xml/004207B_timewise.xml',
   dread(xml,File,[Xml_Timewise]),
   let( Measure_list := Xml_Timewise/descendant::measure),
   Measures = measure:Measure_list,
   mx_time_measure_attributess(Measures, 5, Attributess),
   write(Attributess).

test(music_xml, time(attributes,small)) :-
   Note_1 = note:[
      pitch:[step:['C'], octave:['5']],
      duration:['4'],
      type:[whole] ],
   Note_2 = note:[
      pitch:[step:['C'], octave:['5']],
      duration:['4'],
      type:[whole] ],
   Attributes = attributes:[
      divisions:[4],
      key:[fifth:[3], mode:[minor]],
      time:[symbol:commons]:[ beats:[4], 'beat-type':[4]],
      clef:[sign:['G'], line:[2]] ],
   Measures = measures:[
      measure:[part:[Attributes, Note_1, Note_2] ] ],
   mx_time_measure_attributess(Measures, 1, Attributes_back),
   write(Attributes_back).

test(music_xml, time(note_interval_check)) :-
   Note = note:[
      pitch:[step:['C'], octave:['5']],
      duration:['4'],
      type:[whole] ],
   mx_time_note_interval_check([1, 4], 5:Note).

test(music_xml, time(beat)) :-
   dislog_variable_get(example_path,
      'music_xml/004207B_timewise.xml', File),
   dread(xml, File, [Xml_Timewise]),
   mx_time_measure_and_beat_to_notes(
      Xml_Timewise, 1, beat(2, 1/8), Notes),
   flatten(Notes, Notes_Out),
   maplist( mx_intervals_note_to_tone,
      Notes_Out, Tones ),
   write(Tones).

test(music_xml, time(beat, small)) :-
   Note_1 = note:[
      pitch:[step:['C'], octave:['5']],
      duration:['4'],
      type:[whole] ],
   Note_2 = note:[
      pitch:[step:['D'], octave:['4']],
      duration:['4'],
      type:[whole] ],
   Attributes = attributes:[
      divisions:['4'],
      key:[fifth:['3'], mode:[minor]],
      time:[symbol:'commons']:[beats:['4'], 'beat-type':['4']],
      clef:[sign:['G'], line:['2']] ],
   Measures = measures:[
      measure:[number:'1']:[part:[Attributes, Note_1, Note_2]] ],
   mx_time_measure_and_beat_to_notes(
      Measures, 1, beat(1, 1/4), Notes),
   dwrite(xml, notes:Notes).

test(music_xml, time(capacity)) :-
   Note_Values = [1/4, 1/2, 1/8, 1],
   Divisions = [2, 4, 4, 4],
   maplist( mx_time_note_capacity,
      Note_Values, Divisions, Capacities ),
   write(Capacities).

test(music_xml, insert) :-
   M = a:[b:1]:[c:[4]],
   N := M <+> [haus:[6]],
   dwrite(xml, N).

test(music_xml, time(durations)) :-
   Item = notes:[
      note:[
         pitch:[
            step:['C'], octave:['5'] ],
         duration:['4'],
         type:[whole] ],
      note:[
         pitch:[
            step:['C'] ,octave:['5'] ],
         duration:['4'],
         type:[whole] ] ],
   mx_time_measure_to_start_list(part:Item, Sums),
   write(Sums).

test(music_xml, time(part_to_notes)) :-
   Note_1 =  note:[
      pitch:[step:['C'],
         octave:['5'] ],
      duration:['4'],
      type:[whole] ],
   Note_2 = note:[
      pitch:[step:['D'],
         octave:['5'] ],
      duration:['4'],
      type:[whole] ],
   Part = part:[ Note_1, Note_2 ],
   mx_time_note_interval_check([1, 4], 1:Note_1) -> write('true'),
   mx_time_part_to_vocalnotes(
      [1, 4], Part, Vocal_Notes),
%  writeq(Vocal_Notes),
   dwrite(xml, notes:Vocal_Notes).


/*** interface ****************************************************/


/* mx_time_measure_and_beat_to_notes(
         Measures, M_nr, beat(X, Y), Notes) <-
      Returns all notes which sound to the beat(X,Y)
      in the measure number M_Nr.
      A beat(1, 1/4) whould mean the first quarter note. */

mx_time_measure_and_beat_to_notes(
   Measures, M_Nr, beat(X, Y), Notes) :-
   mx_time_measure_attributess(Measures, M_Nr, Attributess),
   mx_time_measures_to_measure(Measures, M_Nr, M),
   let( Parts := M/descendant::part),
   maplist(mx_time_add_attributes, Attributess, Parts, P_As),
   Measure := measure:P_As,
   mx_time_beat_to_notes(Measure, beat(X, Y), Notes),
   !.


mx_time_beat_to_notes(Measure, beat(X, Y), Vocal_Notess) :-
   findall( Part,
      Part:= Measure/descendant::part,
      Parts ),
   ( foreach(Part, Parts), foreach([Lower, Upper], Intervals) do
        [Divisions] := Part/attributes/divisions/content::'*',
        atom_number(Divisions, D),
        mx_time_note_capacity(Y, D, Capacity),
        mx_time_to_time_interval(X, Capacity, [Lower, Upper]) ),
   maplist( mx_time_part_to_vocalnotes,
      Intervals, Parts, Vocal_Notess ).


mx_time_measures_to_measure(Measures, M_Nr, Measure) :-
   ( atom_number(Me_Nr, M_Nr),
     Measure := Measures/descendant::measure::[@number=Me_Nr],
     !
   ; Measure := Measures/nth_child::M_Nr ).

mx_time_add_attributes(Attributes, Part, Part_With_Attributes) :-
    Part_With_Attributes := Part <+> [Attributes].


mx_time_part_to_vocalnotes([Lower, Upper], Part, Vocal_Notes) :-
   mx_time_measure_to_start_list(Part, Starts),
   mx_time_part_to_notes(Part, Notes),
   pair_lists(:, Starts, Notes, Pairs_1),
   sublist( mx_time_note_interval_check([Lower, Upper]),
      Pairs_1, Pairs_2 ),
   pair_lists(:, _, Vocal_Notes, Pairs_2).

mx_time_note_interval_check([Lower, Upper], Start:Note) :-
   [N_Duration] := Note/descendant::duration/content::'*',
   atom_number(N_Duration, Duration),
   Start + Duration > Lower,
   Start =< Upper.

mx_time_part_to_notes(Part, Notes) :-
   findall( Note,
      Note := Part/descendant::note,
      Notes ).

mx_time_note_capacity(Note_Value, Divisions, Capacity) :-
   Capacity is Note_Value * 4 * Divisions.

mx_time_to_time_interval(X, Capacity, [Lower, Upper]) :-
   Upper is X * Capacity,
   Lower is Upper - Capacity + 1.

mx_time_from_time_interval(Lower:Upper, beat(X, Note_Value)) :-
   Note_Value is (Upper - Lower + 1) / 16,
   X is (Lower - 1) / (Upper - Lower + 1) + 1.


mx_time_measure_attributess(Measures, M_Nr, Attributess) :-
   M_Nr >= 0,
   ( mx_time_measures_to_measure(Measures, M_Nr, Measure),
     let( Attributess := Measure/descendant::attributes ),
     \+(Attributess = []),
     !
   ; Predecessor is M_Nr - 1,
     mx_time_measure_attributess(
        Measures, Predecessor, Attributess) ).


mx_time_measure_to_start_list(Item, Starts) :-
   findall( Note,
      Note := Item/descendant::note,
      Notes),
   findall( Sum,
      ( append(Xs, _, Notes),
        Item_2 = notes:Xs,
        findall( D,
           ( [D2] := Item_2/note/duration/content::'*',
             term_to_atom(D, D2) ),
	   Ds ),
        sum(Ds, Sum) ),
      Sums_0 ),
   maplist( add(1),
      Sums_0, Sums_1 ),
   mx_time_del_last_elem(Sums_1, Starts).

%  ddbase_aggregate( [sum(D)],
%     ( [D2] := Item_2/note/duration/content::'*',
%       term_to_atom(D, D2) ),
%     [Sum] ) ),

mx_time_del_last_elem(List_1, List_2) :-
   reverse(List_1, [_|List]),
   reverse(List, List_2).

mx_time_item_note_to_start(fermata, Item, Start) :-
   mx_time_measure_to_start_list(Item, Starts),
   let( Notes := Item/descendant::note ),
   nth(X, Notes, note:[]:Y),
   member(notations:[]:[ fermata:[_]:[] ], Y),
   nth(X, Starts, Start).


/******************************************************************/


