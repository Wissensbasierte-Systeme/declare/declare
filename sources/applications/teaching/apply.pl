

my_apply(A, Xs) :-
   A =.. As,
   append(As, Xs, Bs),
   B =.. Bs,
   call(B).

my_add(Xs, Y) :-
   iterate_list(add, 0, Xs, Y).

my_multiply(Xs, Y) :-
   iterate_list(multiply, 1, Xs, Y).


