

/******************************************************************/
/***                                                            ***/
/***          Teaching:  Graph Search in Labyrinth              ***/
/***                                                            ***/
/******************************************************************/


:- module( graph_search, [
      graph_search/2,
      graph_search/3 ] ).


/*** interface ****************************************************/


/* graph_search(+Node, -Path) <-
      */

graph_search(X, Path) :-
   graph_search(X, [X], Path).

graph_search(X, _, [X]) :-
   graph_sink(X).
graph_search(X, Xs, [X|Path]) :-
   graph_edge(X, Y),
   \+ member(Y, Xs),
   write(user, '->'), write(user, Y),
   graph_search(Y, [Y|Xs], Path).


/*** data *********************************************************/


graph_edge(X, Y) :-
   ( graph_edge_(X, Y)
   ; graph_edge_(Y, X) ).
 
graph_edge_(i, f).
graph_edge_(i, h).
graph_edge_(h, g).
graph_edge_(g, d).
graph_edge_(d, e).
graph_edge_(d, a).
graph_edge_(a, b).
graph_edge_(b, c).

graph_source(i).
graph_sink(c).


/******************************************************************/


