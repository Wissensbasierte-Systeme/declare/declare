

a(1, 2).
a(1, 3).
a(2, 4).
a(1, 2).


b(setof(1), X, Ys) :-
   setof( Y,
      a(X, Y),
      Ys ).
b(bagof(1), X, Ys) :-
   bagof( Y,
      a(X, Y),
      Ys ).
b(setof(2), X, Ys) :-
   setof( Y,
      X^a(X, Y),
      Ys ).
b(bagof(2), X, Ys) :-
   bagof( Y,
      X^a(X, Y),
      Ys ).

b(setof(1), Ys) :-
   setof( Y,
      a(_, Y),
      Ys ).
b(bagof(1), Ys) :-
   bagof( Y,
      a(_, Y),
      Ys ).
b(setof(2), Ys) :-
   setof( Y,
      X^a(X, Y),
      Ys ).
b(bagof(2), Ys) :-
   bagof( Y,
      X^a(X, Y),
      Ys ).


/* multiset_normalize_2(M_Set_1, M_Set_2) <-
      */

multiset_normalize_2(M_Set_1, M_Set_2) :-
   findall( X:M,
      ( bagof( N,
           member(X:N, M_Set_1),
           Ns ),
        sum(Ns, M) ),
      M_Set_3 ),
   list_to_ord_set(M_Set_3, M_Set_2).


