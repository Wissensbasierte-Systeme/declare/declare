

/******************************************************************/
/***                                                            ***/
/***          Teaching:  Dialog in XPCE                         ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      employee/5.


/* ask_employee_odbc <-
      */

ask_employee_odbc :-
   new(Dialog, dialog('Define employee')),
   send(Dialog, background, lightsteelblue3),
   send_list(Dialog, append, [
      new(N1, text_item(first_name)),
      new(N2, text_item(family_name)),
%     new(S, new(S, menu(sex))),
      new(S, menu(sex)),
      new(A, int_item(age, low := 18, high := 65)),
      new(D, menu(department, cycle)),
      button('ODBC_Connect',
         message(@prolog, ask_employee_odbc_connect)),
      button(cancel,
         message(Dialog, destroy)),
      button(enter,
         message(@prolog, assert_employee_odbc,
            N1?selection, N2?selection,
            S?selection, A?selection, D?selection ) ),
      button(show, message(@prolog, employee_table_odbc)) ]),
   send(S, on_image, 'on_marked.bm'),
   send(S, off_image, 'off_marked.bm'),
   send_list(S, append, [male, female]),
   send_list(D, append, [research, development, marketing]),
   send(Dialog, default_button, enter),
   send(Dialog, open).

ask_employee_odbc_connect :-
   getenv('USER', User),
   odbc_connect('mysql', _,
      [ user(User), password(''),
        alias(mysql), open(once) ]),
   odbc_query(mysql, 'use emp', Row, []),
   writeln(user, Row).

assert_employee_odbc(
      First_Name, Family_Name, Sex, Age, Department) :-
   format('Adding ~w ~w ~w, age ~w, working at ~w~n',
      [Sex, First_Name, Family_Name, Age, Department]),
   assert(
      employee(First_Name, Family_Name, Sex, Age, Department)),
   Attributes = ['Fname', 'Lname', 'Sex', 'Age', 'Dept'],
   Tuple = [First_Name, Family_Name, Sex, Age, Department],
   mysql_insert_tuples(empl, Attributes, [Tuple]).

employee_table_odbc :-
   findall( [First_Name, Family_Name, Sex, Age, Department],
      employee(First_Name, Family_Name, Sex, Age, Department),
      Rows ),
   xpce_display_table(
      ['First_Name', 'Family_Name', 'Sex', 'Age', 'Department'],
      Rows ),
   findall( Tuple,
      ( odbc_query(mysql, 'select * from empl', Row, []),
        Row =.. [row|Tuple] ),
      Tuples ),
   xpce_display_table(
      ['First_Name', 'Family_Name', 'Sex', 'Age', 'Department'],
      Tuples ).


/******************************************************************/


