

/******************************************************************/
/***                                                            ***/
/***          Teaching:  Dialog in XPCE                         ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      employee/5.


/* ask_employee <-
      */

ask_employee :-
   new(Dialog, dialog('Define employee')),
   send(Dialog, background, lightsteelblue3),
   send_list(Dialog, append, [
      new(N1, text_item(first_name)),
      new(N2, text_item(family_name)),
%     new(S, new(S, menu(sex))),
      new(S, menu(sex)),
      new(A, int_item(age, low := 18, high := 65)),
      new(D, menu(department, cycle)),
      button(cancel, message(Dialog, destroy)),
      button(enter, message(@prolog, assert_employee,
         N1?selection, N2?selection,
         S?selection, A?selection, D?selection ) ),
      button(show, message(@prolog, employee_table)) ]),
   send(S, on_image, 'on_marked.bm'),
   send(S, off_image, 'off_marked.bm'),
   send_list(S, append, [male, female]),
   send_list(D, append, [research, development, marketing]),
   send(Dialog, default_button, enter),
   send(Dialog, open).

assert_employee(First_Name, Family_Name, Sex, Age, Department) :-
   format('Adding ~w ~w ~w, age ~w, working at ~w~n',
      [Sex, First_Name, Family_Name, Age, Department]),
   assert(
      employee(First_Name, Family_Name, Sex, Age, Department)).

employee_table :-
   findall( [First_Name, Family_Name, Sex, Age, Department],
      employee(First_Name, Family_Name, Sex, Age, Department),
      Rows ),
   xpce_display_table(
      ['First_Name', 'Family_Name', 'Sex', 'Age', 'Department'],
      Rows ).


/******************************************************************/


