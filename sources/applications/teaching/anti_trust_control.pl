

/******************************************************************/
/***                                                            ***/
/***          Teaching:  Anti-Trust Control                     ***/
/***                                                            ***/
/******************************************************************/


:- module( anti_trust_control, [
      market_overview_table/0 ] ).


/*** interface ****************************************************/


/* find_trust(Market, Company, Total) <-
      */

find_trust(Market, Company, Total) :-
   trust_limit(Market, Threshold),
   market(Market, Company, Total),
   Total > Threshold.


/* market_overview_table <-
      */

market_overview_table :-
   findall( [Company, Market, Quota, Total],
      ( company(Company, Market, Quota),
        market_2(Market, Company, Total) ),
      Rows ),
   Attributes = ['Company', 'Market', 'Quota', 'Total'],
   xpce_display_table(Attributes, Rows).


/* market(Market, Company, Total) <-
      */

market(Market, Company, Total) :-
   company(Company, Market, Quota),
   findall( Comp,
      controlled(Comp, Company),
      Companies ),
   maplist( market(Market),
      Companies, Quotas ),
   sum([Quota|Quotas], Total).


/* market_2(Market, Company, Total) <-
      */

market_2(Market, Company, Total) :-
   company(Company, Market, _),
   nl(user), writeln(user, Company:Market),
   transitively_controlled_companies(Company, Companies),
   maplist( company_to_quota(Market),
      Companies, Quotas ),
   sum(Quotas, Total).

company_to_quota(Market, Company, Quota) :-
   company(Company, Market, Quota).


/* controlled(Company_2, Company_1) <-
      */

controlled(Company_2, Company_1) :-
   has_shares(Company_1, Company_2, N),
   N > 50.


/*
sum([X|Xs], Sum) :-
   sum(Xs, Sum_2),
   Sum is X + Sum_2.
sum([], 0).
*/


/* transitively_controlled_companies(Company, Companies) <-
      */

transitively_controlled_companies_(Company, Companies) :-
   findall( Y-X,
      controlled(X, Y),
      Edges ),
   edges_to_ugraph(Edges, Graph),
   reachable(Company, Graph, Companies).

transitively_controlled_companies(Company, Companies) :-
   findall( C,
      transitively_controlls(Company, [], C),
      Cs ),
   sort(Cs, Companies).

transitively_controlls(X, _, X).
transitively_controlls(X, Visited, Z) :-
   controlled(Y, X),
   \+ member(Y, Visited),
   write(user, '->'), write(user, Y),
   transitively_controlls(Y, [Y|Visited], Z).


/*** data *********************************************************/


has_shares(violet, hyacinth, 51).
has_shares(violet, orchid, 20).
has_shares(violet, gardenia, 30).
has_shares(violet, lily, 51).
has_shares(hyacinth, daisy, 30).
has_shares(hyacinth, iris, 70).
has_shares(gardenia, orchid, 51).
has_shares(fiat, opel, 2).
has_shares(gardenia, tulip, 60).
has_shares(lily, rose, 59).
has_shares(tulip, azalea, 20).
has_shares(tulip, begonia, 25).
has_shares(begonia, azalea, 60).


company(violet, toys, 8).
company(hyacinth, toys, 7).
company(opel, cars, 33).
company(orchid, toys, 13).
company(gardenia, toys, 0).
company(tulip, toys, 12).
company(ford, cars, 30).
company(daisy, toys, 8).
company(iris, toys, 12).
company(lily, toys, 2).
company(rose, toys, 4).
company(azalea, toys, 19).
company(begonia, toys, 15).
company(fiat, cars, 35).


trust_limit(toys, 20).
trust_limit(cars, 38).
trust_limit(gasoline, 40).


/******************************************************************/


