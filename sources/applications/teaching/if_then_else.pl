

square_root(X, Y) :-
   ( X >= 0 ->
     sqrt(X, Y)
   ; ( sqrt(abs(X), Z), concat(Z, 'i', Y) ) ).


