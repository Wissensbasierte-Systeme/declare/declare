

/******************************************************************/
/***                                                            ***/
/***          Teaching:  ODBC                                   ***/
/***                                                            ***/
/******************************************************************/


/* odbc_connect_to_mysql <-
      Connects to the MySQL database as user root. */

odbc_connect_to_mysql :-
   odbc_connect('mysql', _,
      [ user(root), password(''),
        alias(mysql), open(once) ]).


/* odbc_mysql_query(employee_project, Row) <-
      */

odbc_mysql_query(employee_project, Row) :-
   odbc_query(mysql, 'use company;'),
   name_append([
      'select e.lname, p.pname ',
      'from employee e, works_on w, project p ',
      'where e.ssn = w.essn and w.pno = p.pnumber;' ],
      Select_Command),
   odbc_query(mysql, Select_Command, Row, [types([atom, atom])]).


/* odbc_mysql_query(employee_project) <-
      */

odbc_mysql_query(employee_project) :-
   Sql_Command = [
      use:[ company ],
      select:[ {{e^lname}}, {{p^pname}} ],
      from:[ employee-e, works_on-w, project-p ],
      where:[ e^ssn = w^essn and w^pno = p^pnumber ] ],
   mysql_query_split_and_show(Sql_Command),
   mysql_select_execute_odbc(Sql_Command, Tuples),
   employee_project_tuples_assert(Tuples).

employee_project_tuples_assert(Tuples) :-
   retractall(employee_project(_,_)),
   ( foreach(X, Tuples) do
        ( writeln(X),
          A =.. [employee_project|X],
          assert(A) ) ),
   listing(employee_project/2).


/* mysql_query_split_and_show(Sql_Command) <-
      Splits an SQL-query in field-notation into its use-part and
      its select-part and shows the SQL-query in text-notation.  */

mysql_query_split_and_show(Sql_Command) :-
   star_line,
   mysql_select_command_to_name_1(Sql_Command, Use_Command),
   mysql_select_command_to_name_2(Sql_Command, Select_Command),
   mysql_select_command_to_types(Sql_Command, Types),
   writeln(user, Use_Command),
   writeln(user, Select_Command),
   writeln(user, Types),
   writeln(user, odbc_query(mysql, Use_Command)),
   writeln(user, odbc_query(mysql, Select_Command, 'Row', [Types])),
   star_line.


/******************************************************************/


