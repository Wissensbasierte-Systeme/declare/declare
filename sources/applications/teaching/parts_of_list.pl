

/******************************************************************/
/***                                                            ***/
/***          Teaching:  Parts-of-List                          ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      components/3.


/*** interface ****************************************************/


/* parts_of_list_table <-
      */

parts_of_list_table :-
   components(Components),
   maplist( component_to_row,
      Components, Rows_1 ),
   sort([3], Rows_1, Rows_2),
   reverse(Rows_2, Rows),
   Attributes = ['Component', 'Parts_of_List', 'Total_Cost'],
   xpce_display_table(Attributes, Rows).

components(Xs) :-
   setof( X,
      A^B^components(X, A, B),
      Xs ).

component_to_row(X, [X_2, Ys_2, Total_Cost]) :-
   parts_of_list(X:1, Parts),
   parts_of_list_to_total_cost(Parts, Total_Cost),
   list_to_comma_structure(Parts, Ys_1),
   term_to_atom(Ys_1, Ys_2),
   term_to_atom(X, X_2).


/* costs(Product, Total_Cost) <-
      */

costs(Product, Total_Cost) :-
   parts_of_list(Product, Parts),
   parts_of_list_to_total_cost(Parts, Total_Cost).
   
parts_of_list_to_total_cost(Parts, Total_Cost) :-
   maplist( cost,
      Parts, Costs ),
   sum(Costs, Total_Cost).

/*
sum([X|Xs], Sum) :-
   sum(Xs, Sum_2),
   Sum is X + Sum_2.
sum([], 0).
*/


/* cost(Component:Quantity, Cost) <-
      */

cost(Component:Quantity, Cost) :-
   price(Component, Price),
   Cost is Price * Quantity.


/* parts_of_list(Component, Parts) <-
      */

parts_of_list(C:Q, [C:Q]) :-
   \+ components(C, _, _),
   !.
parts_of_list(C:Q, Parts) :-
   findall( Ps,
      ( components(C, C1, Q1),
        Q2 is Q * Q1,
        parts_of_list(C1:Q2, Ps) ),
      Pss ),
   append(Pss, Parts_2),
   multiset_normalize(Parts_2, Parts).


/*** data *********************************************************/


/* components(Product, Component, N) <-
      */

components(engine, sparkplug, 4).
components(engine, cylinder, 4).
components(engine, valve, 4).
components(engine, crankshaft, 1).
components(cylinder, piston, 1).
components(cylinder, connecting-rod, 1).
components(valve, gasket, 1).
components(valve, hanger, 2).
components(crankshaft, joint, 8).
components(piston, screw, 2).
components(piston, gasket, 3).
components(connecting-rod, screw, 4).
components(connecting-rod, bolt, 4).
components(hanger, screw, 4).
components(hanger, bolt, 2).
components(joint, screw, 10).
components(joint, bolt, 20).


/* price(Component, Price) <-
      */

price(sparkplug, 10).
price(screw, 2).
price(gasket, 3).
price(bolt, 2).


/******************************************************************/


