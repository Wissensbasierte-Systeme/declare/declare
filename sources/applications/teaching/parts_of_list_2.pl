

/******************************************************************/
/***                                                            ***/
/***          Teaching:  Parts-of-List                          ***/
/***                                                            ***/
/******************************************************************/


/* costs(Product, Total_Cost) <-
      */

costs(Product, Total_Cost) :-
   parts_of_list(Product, Parts),
   maplist( cost,
      Parts, Costs ),
   sum(Costs, Total_Cost).

sum([X|Xs], Sum) :-
   sum(Xs, Sum_2),
   Sum is X + Sum_2.
sum([], 0).


/* cost(Component:Quantity, Cost) <-
      */

cost(Component:Quantity, Cost) :-
   price(Component, Price),
   Cost is Price * Quantity.


/* parts_of_list(Component, Parts) <-
      */

parts_of_list(C:Q, Parts) :-
   parts_of_list(C:Q),
   collect_parts_of_list_found(Parts_2),
   multiset_normalize(Parts_2, Parts).
   
parts_of_list(C:Q) :-
   \+ components(C, _, _),
   !,
   assert(parts_of_list_found(C:Q)).
parts_of_list(C:Q) :-
   forall( components(C, C1, Q1),
      ( Q2 is Q * Q1,
        parts_of_list(C1:Q2) ) ).


/* collect_parts_of_list_found(Parts) <-
      */

collect_parts_of_list_found(Parts) :-
   findall( Part,
      parts_of_list_found(Part),
      Parts ),
   retractall(parts_of_list_found(_)).


/* components(Product, Component, N) <-
      */

components(engine, sparkplug, 4).
components(engine, cylinder, 4).
components(engine, valve, 4).
components(engine, crankshaft, 1).
components(cylinder, piston, 1).
components(cylinder, connecting-rod, 1).
components(valve, gasket, 1).
components(valve, hanger, 2).
components(crankshaft, joint, 8).
components(piston, screw, 2).
components(piston, gasket, 3).
components(connecting-rod, screw, 4).
components(connecting-rod, bolt, 4).
components(hanger, screw, 4).
components(hanger, bolt, 2).
components(joint, screw, 10).
components(joint, bolt, 20).


/* price(Component, Price) <-
      */

price(sparkplug, 10).
price(screw, 2).
price(gasket, 3).
price(bolt, 2).


/******************************************************************/


