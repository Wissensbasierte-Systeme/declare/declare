

Xml ---> V :-
   findall( X,
      ( P := Xml^_^'Vorlesung',
        xml_to_prerequisites(Xml, P, X) ),
      Xs ),
   V = 'Vorlesungen':Xs.

xml_to_prerequisites(Xml, P, V) :-
   findall( X,
      ( S := Xml^_^'Vorlesung'::[
           @'VorlNr' = Vnr],
        Vnrs := P@'Voraussetzungen',
        name_contains_name(Vnrs, Vnr),
        xml_to_prerequisites(Xml, S, X) ),
      Xs ),
   [Titel] := P^'Titel'^content::'*',
   V = 'Vorlesung':['Titel':Titel]:Xs.


