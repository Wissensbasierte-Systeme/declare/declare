
Xml ---> Xml :-
   findall( (P:Tp)-(Q:Tq),
      ( P := Xml^_^'Vorlesung'::[
           @'Voraussetzungen' = Vnrs ]@'VorlNr',
        vnr_to_titel(Xml, P, Tp),
        name_split_at_position([" "], Vnrs, Qs),
        member(Q, Qs), 
        vnr_to_titel(Xml, Q, Tq) ),
      Edges ),
   writeln(user, Edges),
   edges_to_picture(Edges).

vnr_to_titel(Xml, P, Titel) :-
   writeln(user, P),
   [Titel] := Xml^_^'Vorlesung'::[
      @'VorlNr' = P, ^self::'*' = X, dwrite(xml, X)]
      ^'Titel'^content::'*',
   writeln(user, Titel).

