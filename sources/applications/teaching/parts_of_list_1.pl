

/******************************************************************/
/***                                                            ***/
/***          Teaching:  Parts-of-List                          ***/
/***                                                            ***/
/******************************************************************/


/* costs(Product, Total_Cost) <-
      */

costs(Product, Total_Cost) :-
   parts_of_list(Product, Parts),
   maplist( cost,
      Parts, Costs ),
   sum(Costs, Total_Cost).

sum([X|Xs], Sum) :-
   sum(Xs, Sum_2),
   Sum is X + Sum_2.
sum([], 0).


/* cost(Component:Quantity, Cost) <-
      */

cost(Component:Quantity, Cost) :-
   price(Component, Price),
   Cost is Price * Quantity.


/* parts_of_list(Component, Parts) <-
      */

parts_of_list(Component, Parts) :-
   parts_of_list(Component:1, [], Parts).

parts_of_list(C:Q, Parts_1, Parts_2) :-
   findall( C2:Q2,
      components(C, C2, Q2),
      Xs ),
   Xs \= [],
   !,
   parts_of_list(Q, Xs, Parts_1, Parts_2).
parts_of_list(C:Q, Parts_1, Parts_2) :-
   parts_of_list_update(C:Q, Parts_1, Parts_2).

parts_of_list(F, [C:Q|Ps0], Ps1, Ps3) :-
   Q2 is F * Q,
   parts_of_list(C:Q2, Ps1, Ps2),
   parts_of_list(F, Ps0, Ps2, Ps3).
parts_of_list(_, [], Ps, Ps).


/* parts_of_list_update(C:Q, Ps1, Ps2) <-
      */

parts_of_list_update(C:Q, [C:Q1|Ps], [C:Q2|Ps]) :-
   !,
   Q2 is Q1 + Q.
parts_of_list_update(C:Q, [P|Ps1], [P|Ps2]) :-
   parts_of_list_update(C:Q, Ps1, Ps2).
parts_of_list_update(C:Q, [], [C:Q]).


/* components(Product, Component, N) <-
      */

components(engine, sparkplug, 4).
components(engine, cylinder, 4).
components(engine, valve, 4).
components(engine, crankshaft, 1).
components(cylinder, piston, 1).
components(cylinder, connecting-rod, 1).
components(valve, gasket, 1).
components(valve, hanger, 2).
components(crankshaft, joint, 8).
components(piston, screw, 2).
components(piston, gasket, 3).
components(connecting-rod, screw, 4).
components(connecting-rod, bolt, 4).
components(hanger, screw, 4).
components(hanger, bolt, 2).
components(joint, screw, 10).
components(joint, bolt, 20).


/* price(Component, Price) <-
      */

price(sparkplug, 10).
price(screw, 2).
price(gasket, 3).
price(bolt, 2).


/******************************************************************/


