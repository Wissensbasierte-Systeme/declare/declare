

:- discontiguous
      likes/2.

:- dynamic
      nice/1.

likes(george, Y) :-
   woman(Y),
   likes(Y, books).
likes(mary, Y) :-
   ( nice(Y)
   ; loves(Y, cats) ).

woman(mary).
likes(mary, books).
loves(george, cats).


