

parbox :-
   send(new(W, window), open),
   send(W, display, new(P, parbox(W?width)), point(10, 10)),
   send(W, resize_message, message(P, width, @arg2?width-20)),
   send(P, alignment, justify),
   send_list(P, [
      cdata('This '),
      cdata('is a parbox.') ]).


