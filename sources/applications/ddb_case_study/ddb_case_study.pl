

/* question_answers(Pairs) <-
      */

question_answers([1:2, 2:3, 3:4, 4:2, 5:2, 6:2, 7:2]).


/* d3_score(Qid = Score, Score_Pairs) <-
      */

d3_score(Qid = Score, Score_Pairs) :-
   ddb_case_study:cond(Qid = A),
   atom_to_number(A, V),
   ( V > 0,
     nth(V, Score_Pairs, B),
     atom_to_number(B, Score)
   ; Score = 0 ),
   !.


/* chart_file_to_rules <-
      */

chart_file_to_rules(File) :-
   dislog_variable_get(example_path,
      'ddb_case_study/chart.csv', Chart_File),
   chart_file_to_rules(Chart_File, Rules),
   dwrite(lp, user, Rules),
   dwrite(lp, File, Rules).


/* chart_file_to_rules(File, Rules) <-
      */

chart_file_to_rules(File, Rules) :-
   dread(xls, File, Rows),
   maplist( row_to_rule,
      Rows, Rules ).


/* row_to_rule(Row, Rule) <-
      */

row_to_rule(Row, Rule) :-
   findall( Condition_and_Score,
      question_to_condition_and_score(Row, _, Condition_and_Score),
      Conditions_and_Scores ),
   pair_lists(:, Conditions, Scores, Conditions_and_Scores),
   append(Conditions, [add(Scores, Score)], Body),
   nth(1, Row, Diagnosis),
   Head = [d3_diagnosis(Diagnosis, Score)],
   Rule = Head-Body.


/* question_to_condition_and_score(Row, Qid, Condition:Score) <-
      */

question_to_condition_and_score(Row, Qid, Condition:Score) :-
   question_answers(Pairs),
   append(Pairs_2, [Id:N|_], Pairs),
   concat('Q', Id, Qid),
   findall( M,
      member(_:M, Pairs_2),
      Ms ),
   add(Ms, M),
   findall( S,
      ( between(1, N, I),
        J is M + 1 + I,
        nth(J, Row, S) ),
      Score_Pairs ),
   Condition =
      d3_score(Qid = Score, Score_Pairs).


