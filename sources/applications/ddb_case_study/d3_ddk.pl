

/******************************************************************/
/***                                                            ***/
/***       D3:  DDB Case Study                                  ***/
/***                                                            ***/
/******************************************************************/


:- module( ddb_case_study,
      [ ddb_case_study/0 ] ).

:- dynamic
      cond/1,
      finding/1.

/*** interface ****************************************************/


/* ddb_case_study <-
      */

ddb_case_study :-
   dislog_variable_get(example_path,
      'ddb_case_study/program.pl', File_Program),
   Module = ddb_case_study,
   tp_iteration_prolog_file(File_Program, Module, [], I2),
   retract_all(ddb_case_study:finding(_)),
   writeq(user, I2),
   findall( [Diagnosis, Score],
      member(d3_diagnosis(Diagnosis, Score), I2),
      Rows ),
   xpce_display_table(['Diagnosis', 'Score'], Rows),
   diagnosis_score_table_to_dislog_bar_chart(Rows).

diagnosis_score_table_to_dislog_bar_chart(Rows) :-
   dislog_variable_get(dislog_unit_colours, Colours),
   findall( [Diagnosis, Colour]-Score,
      ( nth(N, Rows, [Diagnosis, Score]),
        nth(N, Colours, Colour) ),
      Distribution ),
   dislog_bar_chart('Distribution', Distribution).


/* cond(C) <-
      */

cond(C) :-
   C =.. [Comparator, Qid, V],
   finding(Qid = Val),
   !,
   apply(Comparator, [Val, V]).
cond(C) :-
   C =.. [Comparator, Qid, V],
   d3_dialog_for_question_id(Qid, Val),
   !,
   assert(finding(Qid = Val)),
   apply(Comparator, [Val, V]).


/* d3_dialog_for_question_id(Id, Answer) <-
      */

d3_dialog_for_question_id(Id, Answer) :-
   dislog_variable_get(example_path,
      'ddb_case_study/questions.xml', File_Questions),
   d3_dialog_for_question_id(File_Questions, Id, Answer).


/* d3_dialog_for_question_id(KB_File, Id, Answer) <-
      */

d3_dialog_for_question_id(KB_File, Id, Answer) :-
   dread(xml, KB_File, [Questions]),
   Question := Questions/descendant_or_self::'Questions'/
      'Question'::[@'ID'=Id],
   d3_dialog(Question, Answer).


/******************************************************************/


