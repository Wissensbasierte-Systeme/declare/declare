

/******************************************************************/
/***                                                            ***/
/***          Selli:  XML Representation                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* selli_buyers_to_xml <-
      */

selli_buyers_to_xml :-
   dislog_variable_get(home, DisLog),
   concat(DisLog, '/projects/Selli/selli.xml', Path),
   selli_buyers_to_xml(Selling),
   dwrite(xml, Path, Selling).

selli_buyers_to_xml(Selling) :-
   buyers_to_xml(2, Buyers),
   findall( Seller,
      ( seller(Name, Sid),
        Seller = seller:[name:Name, sid:Sid]:[] ),
      Sellers ),
   ( purchase(Date, _, _, _, _)
   ; Date = '01.01.0001' ),
   Selling = selling:[date:Date]:[
      sellers:[]:Sellers,
      buyers:[]:Buyers ].


/*** implementation ***********************************************/


/* buyers_to_xml(Mode, Xml) <-
      */

buyers_to_xml(1, Xml) :-
   findall( Buyer,
      ( purchase(_Date, Tid, Bid, Seller, Price),
        seller(Seller, Sid),
        Buyer = buyer:[
%          date:Date,
           tid:Tid, bid:Bid,
%          seller:Seller,
           sid:Sid, price:Price ]:[] ),
      Xml ).

buyers_to_xml(2, Xml) :-
   findall( [Date, Tid, Bid, Sid, Seller, Item, Type, Price],
      purchase_enhanced(Date, Tid, Bid, Sid:Seller, Item:Type:Price),
      Buyer_Rows ),
   buyers_to_xml(Buyer_Rows, Xml).

buyers_to_xml(Buyers_1, Buyers_2) :-
   sort_by_components([3], Buyers_1, Buyers),
   buyers_to_xml_iterate(Buyers, Buyers_2).

buyers_to_xml_iterate(Ps1, Ps2) :-
   first(Ps1, [_, _, Bid|_]),
   buyers_to_xml_iterate(Bid, 0, Ps1, Ps2).
buyers_to_xml_iterate([], []).

buyers_to_xml_iterate(Bid, S1,
      [P|Ps], [buyer:As:[Item|Items]|Xmls]) :-
   P = [_, Tid, Bid, Sid, _, Iid, _, Price],
   Item = purchase:[tid:Tid, sid:Sid, iid:Iid, price:Price]:[],
   !,
   S2 is S1 + Price,
   buyers_to_xml_iterate(Bid, S2, Ps, [buyer:As:Items|Xmls]).
buyers_to_xml_iterate(Bid, S,
      Ps1, [buyer:[bid:Bid, total:S]:[]|Ps2]) :-
   buyers_to_xml_iterate(Ps1, Ps2).


/******************************************************************/


