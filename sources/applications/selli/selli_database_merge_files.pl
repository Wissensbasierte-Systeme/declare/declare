

/******************************************************************/
/***                                                            ***/
/***          Selli:  Merge Files                               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* selli_db_dml('merge(file)') <-
      */

selli_db_dml('merge(file)') :-
   selli_db_file('Input File A', File_A),
   selli_db_file('Input File B', File_B),
   selli_db_file('Output File', File_C),
   diconsult(File_A, State_A),
   diconsult(File_B, State_B),
   selli_db_dml_merge_sellers(
      State_A, State_B, Sellers_C),
   selli_db_dml_merge_transaction_numbers(
      State_A, State_B, N_Offset, N_Max_C, Nodes_C, Edge_AB),
   selli_db_dml_merge_buyer_numbers(
      State_A, State_B, Bid_Offset, Bid_Max_C),
   selli_db_dml_merge_purchases_and_types(
      State_A, State_B, N_Offset, Bid_Offset,
      Purchases_C, Purchase_Types_C),
   selli_db_dml_merge_graph_edges(
      State_A, State_B, N_Offset, Edge_AB, Edges_C),
   selli_append([Sellers_C, Purchases_C, Purchase_Types_C,
      [ [last_transaction_number(N_Max_C)],
        [last_buyer_number(Bid_Max_C)],
        [actual_node(N_Max_C)],
        [graph_nodes(Nodes_C)], [graph_edges(Edges_C)] ] ],
      State_C),
   disave(selli_proper_write,State_C,File_C),
   write(File_C), writeln(' saved.'),
   ddk_tone(success).

selli_db_dml_merge_transaction_numbers(
      State_A, State_B, N_Offset, N_Max_C, Nodes_C, Edge_AB) :-
   selli_transaction_numbers(State_A, Numbers_A, _-N_Max_A),
   selli_transaction_numbers(State_B, Numbers_B, N_Min_B-N_Max_B),
   N_Offset is N_Max_A - N_Min_B + 1,
   N_Max_C is N_Max_B + N_Offset,
   findall( N,
      ( member(N_B, Numbers_B),
        N is N_B + N_Offset ),
      Numbers_List ),
   list_to_ord_set(Numbers_List, Numbers),
   ord_union(Numbers_A, Numbers, Nodes_C),
   New_N_Min_B is N_Min_B + N_Offset,
   Edge_AB = N_Max_A-New_N_Min_B.

selli_db_dml_merge_buyer_numbers(
      State_A, State_B, Bid_Offset, Bid_Max_C) :-
   selli_buyer_numbers(State_A, _, _-Bid_Max_A),
   selli_buyer_numbers(State_B, _, Bid_Min_B-Bid_Max_B),
   Bid_Offset is Bid_Max_A - Bid_Min_B + 1,
   Bid_Max_C is Bid_Max_B + Bid_Offset.

selli_db_dml_merge_graph_edges(
      State_A, State_B, N_Offset, Edge_AB, Edges_C) :-
   member([graph_edges(Edges_A)], State_A),
   member([graph_edges(Edges_B)], State_B),
   findall( N1-N2,
      ( member(N1_B-N2_B, Edges_B),
        N1 is N1_B + N_Offset,
        N2 is N2_B + N_Offset ),
      Edges_List ),
   list_to_ord_set([Edge_AB|Edges_List], Edges),
   ord_union(Edges_A, Edges, Edges_C).


/* selli_db_dml_merge_sellers(State_A, State_B, Sellers_C) <-
      */

selli_db_dml_merge_sellers(State_A, State_B, Sellers_C) :-
   findall( [seller(S, Sid)],
      member([seller(S, Sid)], State_A),
      Sellers_A ),
   findall( [seller(S, Sid)],
      member([seller(S, Sid)], State_B),
      Sellers_B ),
   ord_union(Sellers_A, Sellers_B, Sellers_C),
   findall( 'File A':seller(S_A, Sid)-'File B':seller(S_B, Sid),
      ( member([seller(S_A, Sid)], Sellers_A),
        member([seller(S_B, Sid)], Sellers_B),
        S_A \== S_B ),
      Differences ),
   nl,
   writeln(user, 'The following seller numbers occur differently:'),
   writeln_list(user, Differences),
   nl.


/* selli_db_dml_merge_purchases_and_types(State_A, State_B,
         N_Offset, Bid_Offset, Purchases_C, Purchase_Types_C) <-
      */

selli_db_dml_merge_purchases_and_types(State_A, State_B,
      N_Offset, Bid_Offset, Purchases_C, Purchase_Types_C) :-
   selli_db_dml_get_purchases_and_types(
      0, 0, State_A, Purchases_A, Purchase_Types_A),
   selli_db_dml_get_purchases_and_types(
      N_Offset, Bid_Offset, State_B, Purchases_B, Purchase_Types_B),
   state_union(Purchases_A, Purchases_B, Purchases_C),
   nl,
   state_union(Purchase_Types_A, Purchase_Types_B, Purchase_Types_C).

selli_db_dml_get_purchases_and_types(
      N_Offset, Bid_Offset, State, Purchases, Purchase_Types) :-
   findall( [purchase(D, N, Bid, S, P)],
      ( member([purchase(D, N_2, Bid_2, S, P)], State),
        N is N_2 + N_Offset,
        Bid is Bid_2 + Bid_Offset ),
      Purchases ),
   findall( [purchase_type(N, Type)],
      ( member([purchase_type(N_2, Type)], State),
        N is N_2 + N_Offset ),
      Purchase_Types ).


/******************************************************************/


