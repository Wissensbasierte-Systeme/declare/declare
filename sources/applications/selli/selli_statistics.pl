

/******************************************************************/
/***                                                            ***/
/***          Selli:  Statistics                                ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library('plot/barchart')).


/*** interface ****************************************************/


/* selli_directory_to_statistics :-
      */

selli_directory_to_statistics :-
   Dir = 'projects/Selli/Data/Final_Versions/',
%  Dir = 'projects/Selli/Data/2010_09_25/',
   selli_directory_to_statistics(Dir).

selli_directory_to_statistics(Path) :-
   selli_directory_to_statistics_table(Path, Rows),
   selli_statistics_rows_to_bar_charts(Rows).


/* selli_directory_to_statistics_table(Path, Rows) <-
      */

selli_directory_to_statistics_table(Path, Rows) :-
   directory_contains_files(Path, Files),
   maplist( selli_file_to_statistics_row(Path),
      Files, Rows ),
   xpce_display_table(
      _, _, ['Date', 'Sum', 'To_Pay', 'Purchases'], Rows).

selli_file_to_statistics_row(Path, File, Row) :-
   concat(Path, File, Path_2),
   selli_db_dml_load_file(Path_2),
   selli_db_report_total(seller, Total),
   Total = [
      total('Sum':Sum_1, 'To_Pay':To_Pay_1, 'Purchases':Purchases)],
   dm_to_euro_if_necessary(File, Sum_1, Sum_2),
   dm_to_euro_if_necessary(File, To_Pay_1, To_Pay_2),
   Row = [File, Sum_2, To_Pay_2, Purchases].

dm_to_euro_if_necessary(Date, X, Y) :-
   string_to_list(Date, D1),
   string_to_list('2002_01_01', D2),
   ( list_compare(<, D1, D2) ->
     dm_to_euro(X, Y)
   ; Y = X ).


/* selli_statistics_rows_to_bar_charts(Rows) <-
      */

selli_statistics_rows_to_bar_charts(Rows) :-
   ( foreach(Row, Rows),
     foreach(X, Xs), foreach(Y, Ys), foreach(Z, Zs) do
        ( Row = [File, Sum, To_Pay, Purchases],
          X = [File, File]-Sum,
          Y = [File, File]-To_Pay,
          Z = [File, File]-Purchases ) ),
   selli_bar_chart('Sum', red, Xs),
%  selli_bar_chart('To_Pay', green, Ys),
   Ys = Ys,
   selli_bar_chart('Purchases', blue, Zs).


/* selli_statistics <-
      */

selli_statistics :-
   selli_dialog('SELLI - Statistics').

selli_dialog('SELLI - Statistics') :-
    dislog_frame_generic([
      header: 'SELLI - Statistics',
      above: [],
      interface: [ ],
      below: [
         'Purchases' - selli_bar_chart(purchases),
         'Sellers' - selli_bar_chart(sellers),
         'Buyers' - selli_bar_chart(buyers) ],
      close_button: [
         'Close' - right ] ]).


/* selli_bar_charts <-
      */

selli_bar_charts :-
   selli_bar_chart(buyers),
   selli_bar_chart(sellers),
   selli_bar_chart(purchases).

selli_bar_chart(buyers) :-
   dislog_dialog_variables( get, [ 'Date' - Date ] ),
   name_append(Date,' - Buyers',Buyers),
   state_to_buyer_sums(5,Distribution_Buyer),
   rgb_to_colour([11,11,15], Colour),
   selli_bar_chart(Buyers,Colour,Distribution_Buyer).
selli_bar_chart(sellers) :-
   dislog_dialog_variables( get, [ 'Date' - Date ] ),
   name_append(Date,' - Sellers',Sellers),
   state_to_seller_sums(5,Distribution_Seller),
   rgb_to_colour([11,15,11], Colour),
   selli_bar_chart(Sellers,Colour,Distribution_Seller).
selli_bar_chart(purchases) :-
   dislog_dialog_variables( get, [ 'Date' - Date ] ),
   name_append(Date,' - Purchases',Purchases),
   state_to_purchase_numbers(1,Distribution_Purchases),
   rgb_to_colour([15,11,11], Colour),
   selli_bar_chart(Purchases,Colour,Distribution_Purchases).


/* selli_bar_chart(Title,Colour,Distribution) <-
      */

selli_bar_chart(Title,Colour,Distribution) :-
   findall( [A,Colour]-Amount,
      member([A,_]-Amount,Distribution),
      Distribution_2),
   dislog_bar_chart(Title,Distribution_2),
   findall( [A,B,Amount],
      member([A,B]-Amount,Distribution),
      Rows ),
   xpce_display_table(_,_,Title,['From','To','Amount'],Rows).


/* state_to_buyer_sums(N,Distribution) <-
      */

state_to_buyer_sums(N,Distribution) :-
   selli_db_group_by(buyer,State),
   generate_sequence_of_intervals(N,Intervals),
   maplist( state_to_buyer_sum(State),
      Intervals, Distribution ),
   write_bar_entries([2,6,3,7],Distribution),
   sum_up_distribution(Distribution,_).


/* state_to_seller_sums(N,Distribution) <-
      For N between 1 and 6. */

state_to_seller_sums(N,Distribution) :-
   selli_db_group_by(seller,State),
   generate_sequence_of_intervals(N,Intervals),
   maplist( state_to_seller_sum(State),
      Intervals, Distribution ),
   write_bar_entries([2,6,3,7],Distribution),
   sum_up_distribution(Distribution,_).


/* state_to_purchase_numbers(N,Distribution) <-
      */

state_to_purchase_numbers(N,Distribution) :-
   dislog_variable_get(selli_db_file,File),
   diconsult(File,State),
   generate_sequence_of_intervals(N,Intervals),
   maplist( state_to_purchase_number(State),
      Intervals, Distribution ),
   write_bar_entries([2,6,3,7],Distribution).

write_bar_entries(All_Styles,Es) :-
   write_bar_entries(All_Styles,All_Styles,Es).

write_bar_entries(_,_,[]) :-
   !.
write_bar_entries(All_Styles,[Style|Styles],[_-E|Es]) :-
   write_bar_entry(E,Style),
   write_bar_entries(All_Styles,Styles,Es).
write_bar_entries(All_Styles,[],Es) :-
   nl,
   write_bar_entries(All_Styles,All_Styles,Es).

write_bar_entry(X,Y) :-
   write_list(['\\bar{',X,'}{',Y,'} ']).


/* sum_up_distribution(Es,Y) <-
      */

sum_up_distribution(Es,Y) :-
   sum_up_distribution(0,Es,Y),
   nl,
   write_list(['number: ',Y]).

sum_up_distribution(X,[_-E|Es],Y) :-
   Z is X + E,
   sum_up_distribution(Z,Es,Y).
sum_up_distribution(X,[],X).


/* generate_sequence_of_intervals(N,Intervals) <-
      */

generate_sequence_of_intervals(1,Intervals) :-
   generate_sequence_of_intervals(0,1,10,Intervals_1),
   generate_sequence_of_intervals(10,5,3,Intervals_2),
   generate_sequence_of_intervals(25,25,5,Intervals_3),
   generate_sequence_of_intervals(150,850,1,Intervals_4),
   append([Intervals_1,Intervals_2,Intervals_3,Intervals_4],Intervals).
%  generate_sequence_of_intervals(0,1,15,Intervals_1),
%  generate_sequence_of_intervals(15,5,7,Intervals_2),
%  generate_sequence_of_intervals(50,10,15,Intervals_3),
%  append([Intervals_1,Intervals_2,Intervals_3],Intervals).

generate_sequence_of_intervals(2,Intervals) :-
   generate_sequence_of_intervals(0,15,30,Intervals).

generate_sequence_of_intervals(3,[[-20,0],[-0.1,0]|Intervals]) :-
   generate_sequence_of_intervals(-20,1,20,Intervals).

generate_sequence_of_intervals(4,Intervals) :-
   generate_sequence_of_intervals(0,15,17,Intervals).

generate_sequence_of_intervals(5,Intervals) :-
   generate_sequence_of_intervals(0,15,17,Intervals_1),
   generate_sequence_of_intervals(255,745,1,Intervals_2),
   generate_sequence_of_intervals(1000,1000,1,Intervals_3),
   append([Intervals_1,Intervals_2,Intervals_3],Intervals).

generate_sequence_of_intervals(6,[[-1000,1000]]).


/* state_to_buyer_sum(State,Interval,Interval-N) <-
      */

state_to_buyer_sum(State,Interval,Interval-N) :-
   findall( [purchases(Bid,Price,Prices,Pieces)],
      ( member([purchases(Bid,Price,Prices,Pieces)],State),
        in_left_open_interval(Price,Interval) ),
      Purchases ),
   length(Purchases,N).


/* state_to_seller_sum(State,Interval,Interval-N) <-
      */

state_to_seller_sum(State,Interval,Interval-N) :-
   findall( [purchases(Sid,Sname,Price,Red,Prices,Pieces)],
      ( member([purchases(Sid,Sname,Price,Red,Prices,Pieces)],State),
        in_left_open_interval(Price,Interval) ),
      Purchases ),
   length(Purchases,N).


/* state_to_purchase_number(State,Interval,Interval-N) <-
      */

state_to_purchase_number(State,Interval,Interval-N) :-
   findall( [purchase(Date,Tid,Bid,Sname,Price)],
      ( member([purchase(Date,Tid,Bid,Sname,Price)],State),
        in_left_open_interval(Price,Interval) ),
      Purchases ),
   length(Purchases,N).

in_left_open_interval(X,[Y,Z]) :-
   Y < X,
   X =< Z.


/* generate_sequence_of_intervals(
         Start,Length,Number,Intervals) <-
      */

generate_sequence_of_intervals(_,_,0,[]) :-
   !.
generate_sequence_of_intervals(
      Start,Length,Number,[Interval|Intervals_2]) :-
   End is Start + Length,
   Interval = [Start,End],
   Number_2 is Number - 1,
   Start_2 is Start + Length,
   generate_sequence_of_intervals(
      Start_2,Length,Number_2,Intervals_2).


/* selli_databases(Type,State,Purchases) <-
      */

selli_databases(purchases1,State1,Purchases1) :-
   findall( [purchase(Date,Tid,Bid,Sname,Price)],
      member([purchase(Date,Tid,Bid,Sname,Price)],State1),
      Purchases1 ).

selli_databases(purchases1a,State1,Purchases1) :-
   findall( [purchase(Date,Tid,Bid,Sid,Price)],
      ( member([purchase(Date,Tid,Bid,Sname,Price)],State1),
        member([seller(Sname,Sid)],State1) ),
      Purchases1 ).


/* selli_databases(
         Type,State_1,State_2,Purchases,Tminus,Bminus) <-
      */

selli_databases(
      purchases2,State1,State2,Purchases2,Tminus,Bminus) :-
   findall( [purchase(Date,Tid,Bid,Sname,Price)],
      ( member([purchase(Date,Tid2,Bid2,Sid,Price)],State2),
        seller_number_to_name(State1,Sid,Sname),
        Tid is Tid2 - Tminus,
        Bid is Bid2 - Bminus ),
      Purchases2 ).

selli_databases(
      purchases2a,_State1,State2,Purchases2,Tminus,Bminus) :-
   findall( [purchase(Date,Tid,Bid,Sid,Price)],
      ( member([purchase(Date,Tid2,Bid2,Sid,Price)],State2),
        Tid is Tid2 - Tminus,
        Bid is Bid2 - Bminus ),
      Purchases2 ).


/* seller_number_to_name(State,Sid,Sname) <-
      */

seller_number_to_name(State,Sid,Sname) :-
   member([seller(Sname,Sid)],State),
   !.


/******************************************************************/


