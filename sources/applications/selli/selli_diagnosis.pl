

/******************************************************************/
/***                                                            ***/
/***          Selli:  Diagnosis                                 ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      selli_diagnosis/1.


/*** interface ****************************************************/


/* selli_diagnosis <-
      */

selli_diagnosis :-
   selli_diagnosis(Type),
   writeln(user, selli_diagnosis(Type)),
   bar_line,
   fail.
selli_diagnosis.


/* selli_diagnosis(Type) <-
      */

selli_diagnosis(negative_purchases) :-
   findall( purchase(Date,Tnr,Bid,Seller,Price), 
      ( purchase(Date,Tnr,Bid,Seller,Price),
        Price < 0 ),
      Purchases ),
   length(Purchases,N),
   writeln_list(user,Purchases),
   writeln(user,N),
   findall( Price,
      ( purchase(_,_,_,_,Price),
        Price < 0 ),
      Prices ),
   add(Prices,Sum),
   writeln(user,Sum).

selli_diagnosis(purchase_diagnosis) :-
   findall( purchase(Date,Tnr,Bid,Seller,Price), 
      purchase(Date,Tnr,Bid,Seller,Price),
      Purchases ),
   length(Purchases,N),
   writeln(user,N),
   findall( Price,
      purchase(_,_,_,_,Price),
      Prices ),
   add(Prices,Sum),
   writeln(user,Sum).

selli_diagnosis_2(all_sellers) :-
   findall( seller(Id,Name),
      seller(Name,Id),
      Sellers ),
   length(Sellers,N),
   list_to_ord_set(Sellers,Sellers_Sorted),
   length(Sellers_Sorted,N_Sorted),
   writeln_list(user,Sellers_Sorted),
   writeln(user,N),
   writeln(user,N_Sorted).

selli_diagnosis(purchase_without_seller) :-
   findall( [purchase(Date, Pid, Bid,  Name, Price)],
      ( purchase(Date, Pid, Bid,  Name, Price),
        \+ seller(Name,_) ), Purchases ),
   write('purchases without sellers:'),
   dportray(chs, Purchases).

selli_diagnosis(duplicate_sellers) :-
   findall( [seller(X,Y1),seller(X,Y2)],
      ( seller(X,Y1), seller(X,Y2), Y1 < Y2 ),
      Duplicate_Ids ),
   write('same name with different ids:'),
   dportray(dhs,Duplicate_Ids),
   findall( [seller(X1,Y),seller(X2,Y)],
      ( seller(X1,Y), seller(X2,Y), X1 @< X2 ),
      Duplicates_Names ),
   write('same id with different names:'),
   dportray(dhs,Duplicates_Names).

selli_diagnosis(irregularities) :-
   findall( Number,
      purchase(_,Number,_,_,_),
      Numbers ),
   list_to_ord_set(Numbers,Sorted_Numbers),
   missing_numbers(Sorted_Numbers,Missing_Numbers),
   writeln_list(user,
      ['missing transaction numbers:',Missing_Numbers]),
   findall( Bid,
      purchase(_,_,Bid,_,_),
      Bids ),
   list_to_ord_set(Bids,Sorted_Bids),
   missing_numbers(Sorted_Bids,Missing_Bids),
   writeln_list(user,['missing buyer numbers:',Missing_Bids]),
   dislog_variable_get('Graph',_-Edges),
   findall( N1-N2,
      ( member(N1-N2,Edges),
        \+ ( N2 is N1 + 1 ),
        M1 is N1 + 1,
        M2 is N2 - 1,
        missing_numbers_representation(M1,M2,I),
        \+ member(I,Missing_Numbers) ),
      Irregular_Inverse ),
   reverse(Irregular_Inverse,Irregular),
   writeln_list(user,['irregularities in list:',Irregular]).

missing_numbers([N1,N2|Ns],Ms) :-
   N2 is N1 + 1,
   !,
   missing_numbers([N2|Ns],Ms).
missing_numbers([N1,N2|Ns],[I|Ms]) :-
   M1 is N1 + 1,
   M2 is N2 - 1,
   missing_numbers_representation(M1,M2,I),
   missing_numbers([N2|Ns],Ms).
missing_numbers([_],[]).
missing_numbers([],[]).
   
missing_numbers_representation(M,M,M) :-
   !.
missing_numbers_representation(M1,M2,M1-M2).


/* selli_archive_to_html_table <-
      */

selli_archive_to_html_table :-
   selli_db_dir(Directory),
   name_append(Directory, 'Saves/', Path),
   selli_archive_to_html_table(Path).

selli_archive_to_html_table(Path) :-
   selli_archive_to_table_rows(Path, Rows),
   html_display_table('Selli Archive',
      ['File', 'Date', 'Time',
       'Sellers', 'Buyers', 'Purchases'], Rows).


/* selli_archive_to_table_rows(Path, Rows) <-
      */

selli_archive_to_table_rows(Path, Rows) :-
   directory_contains_files(Path, Files_1),
   writeln(user, Files_1),
   sublist( exists_file_in_directory(Path),
      Files_1, Files_2 ),
   maplist( selli_file_to_table_row(Path),
      Files_2, Rows ).

selli_file_to_table_row(Directory, File, Row) :-
   concat(Directory, File, Path),
   diconsult(Path, State),
   findall( seller(Name, Id),
      member([seller(Name, Id)], State),
   Sellers ),
   length(Sellers, N),
   findall( purchase(Date, Tnr, Bid, Seller, Price),
      member([purchase(Date, Tnr, Bid, Seller, Price)], State),
      Purchases ),
   length(Purchases, M),
   findall( Bid,
      member([purchase(_, _, Bid, _, _)], State),
      Buyers_2 ),
   list_to_ord_set(Buyers_2, Buyers),
   length(Buyers, K),
   selli_file_timestamp_to_date_and_time(File, Date, Time),
   Row = [File, Date, Time, N, K, M].

selli_file_timestamp_to_date_and_time(Timestamp, Date, Time) :-
   name(Timestamp, List),
   ( ( List =
          [Y1,Y2,Y3,Y4,_,M1,M2,_,D1,D2,_,H1,H2,X1,X2,_,S1,S2],
       name(Date, [D1,D2,46,M1,M2,46,Y1,Y2,Y3,Y4]),
       name(Time, [H1,H2,58,X1,X2,58,S1,S2]) )
   ; ( List =
          [Y1,Y2,Y3,Y4,_,M1,M2,_,D1,D2,_,H1,H2,X1,X2],
       name(Date, [D1,D2,46,M1,M2,46,Y1,Y2,Y3,Y4]),
       name(Time, [H1,H2,58,X1,X2]) )
   ; ( List =
          [Y1,Y2,Y3,Y4,_,M1,M2,_,D1,D2],
       name(Date, [D1,D2,46,M1,M2,46,Y1,Y2,Y3,Y4]),
       Time = '--' )
   ; ( Date = '--', Time = '--' ) ),
   !.

exists_file_in_directory(Directory, File) :-
   concat(Directory, File, Path),
   exists_file(Path).


/******************************************************************/


