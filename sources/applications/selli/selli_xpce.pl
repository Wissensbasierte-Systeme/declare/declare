

/******************************************************************/
/***                                                            ***/
/***        Selli:  Additional Interface Predicates             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


dislog_frame_generic_2(Description) :-
   dislog_frame_generic_2(_,Description).

dislog_frame_generic_2(Frame,Description) :-
   [ Header, Buttons_1,
     Interface_Left, Interface_Right,
     Buttons_2, CB ] :=
   Description^[ header, above,
     interface_left, interface_right,
     below, close_button ],
   new(Frame,frame(Header)),
   send(Frame,background,colour(white)),
   ( CB = [Name - Where],
     Close_Button = [Name - send(Frame,destroy) - Where]
   ; Close_Button = [] ),
   new(Dialog_I_Left,dialog),
   new(Dialog_I_Right,dialog),
   send(Dialog_I_Left,left,Dialog_I_Right),
   ( ( Buttons_1 = [],
       !,
       send(Frame,append,Dialog_I_Left),
       append(Buttons_2,Close_Button,Buttons_B) )
   ; ( new(Dialog_A,dialog),
       send(Frame,append,Dialog_A),
       send(Dialog_I_Left,below,Dialog_A),
       append(Buttons_1,Close_Button,Buttons_A),
       checklist( send_append(Dialog_A),
          Buttons_A ),
       Buttons_B = Buttons_2 ) ),
   dislog_frame_generic_2_toolbar(Dialog_A),
   new(Dialog_B,dialog),
   send(Dialog_B,below,Dialog_I_Right),
   send(Dialog_B,alignment,center),
   dislog_variable_get(dislog_frame_interface_color,Colour_1),
%  send(Dialog_A,background,Colour_2),
   send(Dialog_I_Left,background,Colour_1),
   send(Dialog_I_Right,background,Colour_1),
%  send(Dialog_B,background,Colour_2),
   dislog_frame_generic_fill_i_and_b_2(
      Dialog_I_Right,Interface_Right ),
   dislog_frame_generic_fill_i_and_b(
      Dialog_I_Left,Interface_Left, Dialog_B,Buttons_B ),
   send(Frame,open).

dislog_frame_generic_2_toolbar(Dialog) :-
   send(Dialog,append,new(TB,tool_bar)),
   send(TB,append,tool_button(
      message(@prolog,selli_db_find,first), 'left_arrow.bm')),
   send(TB,gap,size(4,0)),
   send(TB,append,tool_button(
      message(@prolog,selli_db_find,previous), 'ms_right_arrow.bm')),
   send(TB,append,tool_button(
      message(@prolog,selli_db_find,next), 'ms_left_arrow.bm')),
   send(TB,append,tool_button(
      message(@prolog,selli_db_find,last), 'right_arrow.bm')),
   send(TB,append,tool_button(
      message(@prolog,selli_db_find,goto), 'pin.xpm')).

dislog_frame_generic_fill_i_and_b_2(
      Dialog_I, Interface ) :-
   maplist( pair_to_triple,
      Interface, Triples ),
   checklist( send_text_menu(Dialog_I),
      Triples ),
   checklist( triple_to_dislog_variable,
      Triples ),
   send(Dialog_I,append,button('Store',
      message(@prolog,selli_db_dml,'insert(purchase) 2'))).


/*** tests ********************************************************/


frame_test :-
   new(Frame,frame(h)),
   new(D1,dialog),
   new(D2,dialog),
   new(D3,dialog),
   new(D4,dialog),
   send(D1,background(blue)),
   send(D2,background(red)),
   send(D3,background(green)),
   send(D4,background(yellow)),
   send(Frame,append,D1),
   send(D2,left,D3),
   send(D1,above,D2),
   send(D4,below,D2),
   send(D2,append,text_item('a',a)),
   send(D2,append,text_item('b',b)),
   send(D2,append,text_item('c',c)),
   send(D2,append,button('d',
      message(@prolog,writeln,d))),
   send(D1,append,new(TB,tool_bar)),
   send(TB,append,tool_button(
      message(@prolog,writeln,yes),
      'fatleft_arrow.bm')),
   send(Frame,open).

resource(left_arrow,image,
%  image('left_arrow.bm')).
   image('fast_forward.bmp')).
resource(right_arrow,image,
   image('right_arrow.bm')).
resource(fatright_arrow,image,
   image('fatright_arrow.bm')).
resource(fatleft_arrow,image,
   image('fatleft_arrow.bm')).
resource(cycle,image,
   image('cycle.bm')).


/******************************************************************/


