

/******************************************************************/
/***                                                            ***/
/***          Selli:  Database Reports                          ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      selli_db_report/1,
      selli_db_report/2.


/*** interface ****************************************************/


/* selli_db_report <-
      Report about the selli database. */

selli_db_report :-
   selli_db_report(seller),
   selli_db_report(buyer).


/* selli_db_report(seller) <-
      */

selli_db_report(seller) :-
   selli_db_report_total(seller, _),
   selli_db_report(seller, _).

selli_db_report_total(seller, Total) :-
   nl, write('*** Seller Report '),
   writeln_selli_time,
   selli_db_group_by(seller,Groups),
   findall( [Sid,Seller,Sum,Pay,No,Items],
      member([purchases(Sid,Seller,Sum,Pay,Items,No)],Groups),
      Seller_Table ),
   Attributes = ['Sid','Seller','Sum','To_Pay','N','Items'],
   selli_db_dir(Path),
   name_append(Path,'Reports/seller_report.xls',File_xls),
   write_list(user, ['---> ', File_xls, '\n']),
   Table = [Attributes, ['']|Seller_Table],
   write_star_office_file(Table, File_xls),
   concat(Path, 'Reports/seller_report.txt', File_txt),
   write_list(user, ['---> ', File_txt, '\n']),
   predicate_to_file( File_txt,
      ( nl, write('*** Seller Report '), writeln_selli_time,
        selli_db_groups_aggregate(Groups, Total),
        dportray(lp, Groups),
        dportray(lp, [Total]),
        nl ) ),
   dportray(lp, [Total]).
   
selli_db_groups_aggregate(Groups,Total) :-
   selli_db_group_by_projection(Groups,
      [purchases(_,_,Price,_,_,_)],Price, Price_Sum),
   selli_db_group_by_projection(Groups,
      [purchases(_,_,_,To_Pay,_,_)],To_Pay, To_Pay_Sum),
   selli_db_group_by_projection(Groups,
      [purchases(_,_,_,_,_,N)],N, N_Sum),
   Total =
      [total('Sum':Price_Sum,'To_Pay':To_Pay_Sum,'Purchases':N_Sum)].


/* selli_db_report(seller,Sid) <-
      */

selli_db_report(seller,Sid) :-
   nl, write('*** Seller Report '),
   writeln_selli_time,
   ddk_timestamp(Time),
   name_append('Seller Report - ',Time,Header),
   findall( [Date,Number,Sid,Seller,Bid,Item,Type,Price],
      purchase_enhanced(Date,Number,Bid,Sid:Seller,Item:Type:Price),
      Purchase_Rows ),
   purchases_add_total(Purchase_Rows,Purchase_Rows_2),
   Attributes =
      ['Date','Tid','Sid','Seller','Bid','Item','Type','Price'],
   ( var(Sid),
     ( selli_purchases_to_xpce_table(
          _, _, Header, Attributes, Purchase_Rows );
       xpce_display_table_with_status_bar(
          100, _, _, Header, Attributes, Purchase_Rows_2 ) );
     xpce_display_table( _, _, Header, Attributes,
        Purchase_Rows_2 ) ),
%  selli_db_dir(Path),
%  name_append(Path,'Reports/seller_report_long.xls',File_xls),
%  write(user,'---> '), writeln(user,File_xls),
%  predicate_to_file( File_xls,
%     checklist(
%        write_list_with_separators(
%           write_with_semicolon, writeln ),
%        [Attributes,[' ']|Purchase_Rows_2] ) ),
   ( var(Sid),
     true
   ; selli_seller_receipt ).


/* selli_db_report(buyer) <-
      */

selli_db_report(buyer) :-
   selli_db_group_by(buyer,Groups),
   nl, write('*** Buyer Report '), writeln_selli_time,
%  dportray(lp,Groups),
   selli_db_group_by_projection(Groups,
      [purchases(_,Price,_,_)],Price, Price_Sum),
   selli_db_group_by_projection(Groups,
      [purchases(_,_,_,N)],N, N_Sum),
   dportray(lp,[
      [total('Sum':Price_Sum,'Purchases':N_Sum)]]),
   nl.


/* selli_db_report(receipt) <-
      Print the receipt for a seller into a txt-file. */

selli_db_report(receipt) :-
   dislog_dialog_variable( get, ' Sid' - Sid ),
   seller(S,Sid),
   nl, write('*** Printing Receipt for Seller '), writeln(S),
   !,
   findall( Price,
      purchase(_,_,_,S,Price),
      Prices ),
   add(Prices,Price),
   round(Price * 0.85,2,To_Pay),
   dislog_dialog_variables( send, [
      'Sum' - Price, 'To Pay' - To_Pay, ' Seller' - S ] ),
   selli_db_print_file(File),
   write_list(['---> ',File]), nl,
   tell(File),
   bar_line_3,
   nl,
   writeln('Die Verkaeuferin'), nl,
   write_list(['   ',S]), nl, nl,
   write_list(['mit der Nummer ''',Sid,''' ']),
   writeln('hat folgende Artikel verkauft'), nl,
   write_list(['   ',Prices]), nl, nl,
   write('zum Preis von DM '), print_price(Price), nl, nl,
   write('Sie erhaelt DM '), print_price(To_Pay), nl, nl,
   bar_line_3,
   nl, nl,
   told.


/* selli_db_report('purchase(regular)') <-
      */

selli_db_report('purchase(regular)') :-
   selli_db_report('purchase(regular)',_).

selli_db_report('purchase(regular)',Bid) :-
   nl, write('*** Buyer Report '),
   writeln_selli_time,
   ddk_timestamp(Time),
   concat('Buyer Report - ',Time,Header),
%  findall( (Date,Number):(b:Bid,s:Sid=Seller):(Item,Type,Price),
%     purchase_enhanced(Date,Number,Bid,Sid:Seller,Item:Type:Price),
%     Purchases ),
%  writeln_list(Purchases),
   findall( [Date,Number,Bid,Sid,Seller,Item,Type,Price],
      purchase_enhanced(Date,Number,Bid,Sid:Seller,Item:Type:Price),
      Purchase_Rows ),
   Attributes = [
      'Date','Tid','Bid','Sid','Seller','Item','Type','Price' ],
   selli_purchases_to_xpce_table(Header, Attributes, Purchase_Rows).

selli_db_report('purchase(sorted)') :-
   selli_sort_purchases(Tuples),
   nl, write('*** Purchase Report '), writeln_selli_time,
   dportray(lp,Tuples),
   nl.


/* selli_sort_purchases(Ts) <-
      */

selli_sort_purchases([T|Ts]) :-
   dislog_variable_get('Actual',N),
   linear_list_find(first,N,M),
   purchase(D,M,B,S,P),
   T = [purchase(D,M,B,S,P)],
   selli_sort_purchases(M,Ts).
selli_sort_purchases(N,[T|Ts]) :-
   linear_list_find(next,N,M),
   !,
   purchase(D,M,B,S,P),
   T = [purchase(D,M,B,S,P)],
   selli_sort_purchases(M,Ts).
selli_sort_purchases(_,[]).


/* selli_db_statistics(buyer_formular) <-
      */

selli_db_statistics(buyer_formular) :-
   dislog_dialog_variables( get, [
      'Date' - Date, 'Bid' - Bid ] ),
   findall( Price,
      purchase(Date,_,Bid,_,Price),
      Prices ),
   add(Prices,Price),
   dislog_dialog_variables( send, [
%     'Price' - 0, 'Seller' - '', 'Sid' - 0 ] ).
      'Total' - Price ] ).
%  selli_db_report('purchase(regular)',Bid).

selli_db_statistics(seller_formular) :-
   dislog_dialog_variable( get, ' Sid' - I ),
   term_to_atom(Sid,I),
   seller(S,Sid),
   !,
   findall( Price,
      purchase(_,_,_,S,Price),
      Prices ),
   add(Prices,Price),
   round(Price * 0.85,2,To_Pay),
   dislog_dialog_variables( send, [
      'Sum' - Price, 'To Pay' - To_Pay,
      ' Seller' - S ] ),
   selli_db_report(seller,Sid).
selli_db_statistics(seller_formular) :-
   dislog_dialog_variables( send, [
      'Sum' - 0.00, 'To Pay' - 0.00,
      ' Seller' - 'does not exist' ] ).


/* selli_db_group_by(Who,Groups) <-
      */

selli_db_group_by(seller,Groups) :-
   findall( [Seller,Price],
      purchase(_,_,_,Seller,Price),
      Tuples ),
   selli_db_group_by(seller,Tuples,Groups).

selli_db_group_by(buyer,Groups) :-
   findall( [Bid,Price],
      purchase(_,_,Bid,_,Price),
      Tuples ),
   selli_db_group_by(buyer,Tuples,Groups).


/* selli_db_group_by(Who,Tuples,Groups) <-
      */

selli_db_group_by(buyer,Tuples,Groups) :-
   findall( Buyer,
      member([Buyer,_],Tuples),
      Buyers_2 ),
   list_to_ord_set(Buyers_2,Buyers),
   findall( [purchases(Buyer,Sum,Prices,N)],
%  findall( [(buyer:Buyer,pay:Sum,prices:Prices,items:N)],
      ( member(Buyer,Buyers),
        findall( Price,
           member([Buyer,Price],Tuples),
           Prices ),
        add(Prices,Sum),
        length(Prices,N) ),
      Groups_2 ),
   list_to_ord_set(Groups_2,Groups).
 
selli_db_group_by(seller,Tuples,Groups) :-
   findall( Seller,
      seller(Seller,_),
      Sellers_2 ),
   list_to_ord_set(Sellers_2,Sellers),
   findall( [purchases(I,Seller,Sum,To_Pay,Prices,N)],
%  findall( [[sid:I,seller:Seller,
%        sum:Sum,pay:To_Pay,prices:Prices,items:N]],
      ( member(Seller,Sellers),
        seller_name_to_numbers(Seller,I),
        findall( Price,
           member([Seller,Price],Tuples),
           Prices ),
        add(Prices,Sum),
        length(Prices,N),
        round(Sum * 0.85,2,To_Pay) ),
      Groups_2 ),
   list_to_ord_set(Groups_2,Groups).


/* selli_db_group_by_projection(
         Purchases,Template,Value,Sum) <-
      */

selli_db_group_by_projection(Purchases,Template,Value,Sum) :-
   findall( Value,
      member(Template,Purchases),
      Values ),
   add(Values,Sum).
      

/* selli_db_format_clean <-
      */

selli_db_format_clean :-
   dislog_dialog_variables( send, [
      'Sid' - '', 'Item' - '', 'Seller' - '', 'Price' - '' ] ).


/* selli_db_find(Mode) <-
      */

selli_db_find(Mode) :-
   dislog_variable_get('Graph',Nodes-_),
%  findall( Node,
%     ( member(Node,Nodes),
%       purchase(_,Node,_,_,_) ),
%     Purchase_Nodes ),
   ( ( Mode = first,
       minimum(Nodes,N) )
   ; ( Mode = last,
       maximum(Nodes,N) ) ),
   !,
   linear_list_find(Mode,N,M),
   selli_gui_update(M),
   dislog_variable_set('Actual',M),
   !.
selli_db_find(Mode) :-
   dislog_variable_get('Actual',N),
   linear_list_find(Mode,N,M),
   selli_gui_update(M),
   dislog_variable_set('Actual',M),
   !.
selli_db_find(_).


/* selli_gui_update(M) <-
      */

selli_gui_update(M) :-
   purchase(D,M,B,S,P),
   seller(S,I),
   purchase_type(M,Item,T),
   dislog_dialog_variables( send, [
      'Date' - D, 'Number' - M, 'Type' - T, 'Item' - Item,
      'Bid' - B, 'Seller' - S, 'Sid' - I, 'Price' - P ] ).


/* selli_seller_ranking(File) <-
      */

selli_seller_ranking(File) :-
   ddbase_aggregate( [sum(Price), Name],
      purchase(_, _, _, Name, Price),
      Pairs_1 ),
   sort(Pairs_1, Pairs_2),
   reverse(Pairs_2, Pairs_3),
   predicate_to_file( File,
      writeln_list(Pairs_3) ).


/******************************************************************/


