

/******************************************************************/
/***                                                            ***/
/***          Selli:  Display XPCE Table                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* selli_purchases_to_xpce_table_standard(
         Header, Attributes, Purchase_Rows) <-
      */

selli_purchases_to_xpce_table_standard(
      Header, Attributes, Purchase_Rows) :-
   purchases_add_total(Purchase_Rows, Purchase_Rows_2),
   xpce_display_table_with_status_bar(
      100, Window, Tabular, Header, Attributes, Purchase_Rows_2 ),
   send(Tabular, next_row),
   dislog_variable_set(selli_report_incremental,
      [window:Window, tabular:Tabular]).


/* purchases_add_total(Purchases_1,Purchases_2) <-
      */

purchases_add_total(Purchases_1,Purchases_2) :-
   sort_by_components([3],Purchases_1,Purchases),
   purchases_add_total_iterate(Purchases,Purchases_2).

purchases_add_total_iterate(Ps1,Ps2) :-
   first(Ps1,[_,_,Bid|_]),
   purchases_add_total_iterate(Bid,0,Ps1,Ps2).
purchases_add_total_iterate([],[]).

purchases_add_total_iterate(Bid,S1,[P|Ps1],[P|Ps2]) :-
   P = [_,_,Bid,_,_,_,_,Price],
   !,
   S2 is S1 + Price,
   purchases_add_total_iterate(Bid,S2,Ps1,Ps2).
purchases_add_total_iterate(_,S,Ps1,[Total|Ps2]) :-
   Total = ['','','','','','','',S],
   purchases_add_total_iterate(Ps1,Ps2).


/* xpce_display_table_with_status_bar(
         M,Table_Window,Tabular,Title,Attributes,Rows) <-
      */

xpce_display_table_with_status_bar(
      M,Table_Window,Tabular,Title,Attributes,Rows) :-
   new(Table_Window,auto_sized_picture(Title)),
   send(Table_Window,display,new(Tabular,tabular)),
   send(Tabular,border,1),
%  send(Tabular,cell_padding,size(8,4)),
   send(Tabular,cell_spacing,-1),
   send(Tabular,rules,all),
%  send(Tabular,colour,red),
   send(Tabular,colour,white),
   send_attributes(Tabular,Attributes),
   length(Rows,N), K is ceil(N/M),
   start_status_bar(
      xpce_display_table_with_status_bar,
      'Constructing the Table ...', K ),
   set_num(xpce_display_table_with_status_bar_num,0),
   send_rows_with_status_bar(M,Tabular,Rows),
   status_bar_disappear(xpce_display_table_with_status_bar),
   set_num(xpce_display_table_with_status_bar,0),
   send(Table_Window,open).

send_rows_with_status_bar(M,Tabular,[Row_1,Row_2|Rows]) :-
   status_bar_increase_mod(
      xpce_display_table_with_status_bar, M ),
   send_row(Tabular,Row_1),
   send(Tabular,next_row),
   send_rows_with_status_bar(M,Tabular,[Row_2|Rows]).
send_rows_with_status_bar(M,Tabular,[Row]) :-
   status_bar_increase_mod(
      xpce_display_table_with_status_bar, M ),
   send_row(Tabular,Row).
send_rows_with_status_bar(_,_,[]).

status_bar_increase_mod(Bar, M) :-
   get_num(Bar, N),
   ( 0 is N mod M ->
     status_bar_increase(Bar)
   ; true ).

   
/******************************************************************/


