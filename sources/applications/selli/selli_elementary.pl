

/******************************************************************/
/***                                                            ***/
/***          Selli:  Elementary Predicates                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* selli_term_to_atom(Field,Term,Atom) <-
      */

selli_term_to_atom(Field,_,A) :-
   A = '',
   ddk_message(['wrong value for ''',Field,''' !']),
   !,
   fail.
selli_term_to_atom(_,T,A) :-
   catch( term_to_atom(T,A), _, fail ),
   !.
/*
selli_term_to_atom(_,T,A) :-
   current_prolog_flag(syntax_errors,Flag),
   set_prolog_flag(syntax_errors,quiet),
   ( term_to_atom(T,A),
     set_prolog_flag(syntax_errors,Flag)
   ; set_prolog_flag(syntax_errors,Flag),
     fail ),
   !.
*/


/* selli_price_atom_transform(Price_1, Price_2) <-
      */

selli_price_atom_transform(Price_1, Price_2) :-
   name_exchange_elements([",."], Price_1, Price_A),
   concat('0', Price_A, Price_B),
   atom_to_number(Price_B, Price_C),
   round(Price_C, 2, Price_2).


/* sort_by_components(Ns,Xs_1,Xs_2) <-
      */

sort_by_components(Ns,Xs_1,Xs_2) :-
   findall( Sorter-X,
      ( member(X,Xs_1),
        maplist( list_to_nth(X),
           Ns, Sorter ) ),
      List_1 ),
   list_to_ord_set(List_1,List_2),
   findall( X,
      member(_-X,List_2),
      Xs_2 ).


list_to_nth(Xs,N,X) :-
   nth(N,Xs,X).


write_with_semicolon(X) :-
   write(X),
   write(' : ').

write_list_nl(Xs) :-
   write_list_with_separators(
      write_and_comma, writeln,
      Xs ).

write_and_comma(X) :-
   write(X),
   write(', ').


/* print_price(Price) <-
      */

print_price(Price) :-
   round(Price,0,Price),
   !,
   write(Price), write(',-').
print_price(Price) :-
   round(Price,1,Price),
   !,
   write(Price), write('0').
print_price(Price) :-
   write(Price).


/* selli_proper_write(Fact) <-
      */

selli_proper_write(Fact) :-
   Fact = [purchase(D,N,Bid,S,P)],
   !,
   write_list(
      ['purchase(''',D,''', ',N,', ',Bid,', ',
       '''',S,''', ',P,').'] ),
   nl.
selli_proper_write(Fact) :-
   Fact = [purchase_type(N,(Item,Type))],
   !,
   write_list(
      ['purchase_type(',N,', ','(''',Item,''',''',Type,''')).']),
   nl.
selli_proper_write(Fact) :-
   Fact = [purchase_type(N,Type)],
   !,
   write_list(
      ['purchase_type(',N,', ','''',Type,''').']),
   nl.
selli_proper_write(Fact) :-
   Fact = [seller(S,I)],
   !,
   write_list(
      ['seller(','''',S,''', ',I,').']),
   nl.
selli_proper_write(Fact) :-
   Fact = [graph_edges(Edges)],
   !,
   write('graph_edges('),
   selli_pp_list(3,10,Edges),
   writeln(').').
selli_proper_write(Fact) :-
   Fact = [graph_nodes(Nodes)],
   !,
   write('graph_nodes('),
   selli_pp_list(3,10,Nodes),
   writeln(').').
selli_proper_write(Fact) :-
   Fact = [A],
   write_list([A,'.']), nl.


/* seller_name_to_numbers(Seller,Numbers) <-
      */

seller_name_to_numbers(Seller,Numbers) :-
   findall( I,
      seller(Seller,I),
      Ids ),
   list_to_minus_structure(Ids,Numbers).


/* selli_transaction_numbers(State,Numbers,First-Last) <-
      */

selli_transaction_numbers(State,Numbers,First-Last) :-
   findall( N,
      member([purchase(_,N,_,_,_)],State),
      Numbers_List ),
   list_to_ord_set(Numbers_List,Numbers),
   first(Numbers,First),
   last(Numbers,Last).

selli_buyer_numbers(State,Bids,First-Last) :-
   findall( Bid,
      member([purchase(_,_,Bid,_,_)],State),
      Bids_List ),
   list_to_ord_set(Bids_List,Bids),
   first(Bids,First),
   last(Bids,Last).


/* writeln_selli_time <-
      */

writeln_selli_time :-
   ddk_timestamp(Time),
   write('*** '), write(Time), writeln(' ***').


/* selli_file_timestamp(Timestamp) <-
      */

selli_file_timestamp(Timestamp) :-
   get_time(U), convert_time(U,X2,X3,X4,X5,X6,X7,_X8),
   maplist( number_to_two_digit_name,
      [X2,X3,X4,X5,X6,X7], [Y2,Y3,Y4,Y5,Y6,Y7] ),
   concat(
      [Y2,'_',Y3,'_',Y4,'_',Y5,Y6,'_',Y7],
      Timestamp).


/* bar_line_3 <-
      */

bar_line_3 :-
   write('-------------------------------------'),
   writeln('-------------------------------------').


/* selli_pp_list(Tab,Items_per_Line,List) <-
      */

selli_pp_list(Tab,Items_per_Line,List) :-
   writeln('[ '), tab(Tab),
   selli_pp_list(Tab,1,Items_per_Line,List).
   
selli_pp_list(Tab,N,Items_per_Line,[X1,X2|Xs]) :-
   N =< Items_per_Line,
   !,
   write(X1), write(', '),
   M is N + 1,
   selli_pp_list(Tab,M,Items_per_Line,[X2|Xs]).
selli_pp_list(_,N,Items_per_Line,[X]) :-
   N =< Items_per_Line,
   !,
   write(X), write(' ]').
selli_pp_list(Tab,_,Items_per_Line,[X|Xs]) :-
   nl, tab(Tab),
   selli_pp_list(Tab,1,Items_per_Line,[X|Xs]).
selli_pp_list(_,_,_,[]) :-
   write(']').


/******************************************************************/


