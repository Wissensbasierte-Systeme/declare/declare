

/******************************************************************/
/***                                                            ***/
/***       Selli:  Receipts for Sellers and Buyers - PS         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* selli_seller_receipts_in_one_pdf <-
      */

selli_seller_receipts_in_one_pdf :-
   selli_seller_receipts,
   selli_seller_receipts_ps_files_to_one_pdf.

selli_seller_receipts_ps_files_to_one_pdf :-
   selli_db_dir(Path),
   concat(['cd ', Path, 'Reports/Seller_Receipts; '], Cd),
   cd_and_us_selli(Cd, 'latex receipts.tex'),
   cd_and_us_selli(Cd, 'dvipdf receipts.dvi'),
   cd_and_us_selli(Cd, 'acroread receipts.pdf &').

cd_and_us_selli(Cd, Command) :-
   concat(Cd, Command, Command_2),
   writeln(user, Command_2), ttyflush,
   us(Command_2).


/* selli_seller_receipts <-
      */

selli_seller_receipts :-
   selli_seller_receipts_ps,
   selli_receipt_files_to_latex_input_file.


/*** implementation ***********************************************/


/* selli_receipt_files_to_latex_input_file <-
      */

selli_receipt_files_to_latex_input_file :-
   selli_db_dir(Path),
   concat(Path,
      'Reports/Seller_Receipts/receipts_input.tex',
      File),
   selli_receipt_files_to_input_file(File).


/* selli_receipt_files_to_input_file(File) <-
      */

selli_receipt_files_to_input_file(File) :-
   selli_receipt_files(Files),
   list_split_into_sublists_of_size(2, Files,
      Receipt_File_Pairs),
   predicate_to_file(File,
      write_list_with_separators(
         selli_receipts_write_latex_1,
         selli_receipts_write_latex_2,
         Receipt_File_Pairs ) ).

selli_receipts_write_latex_1(Files) :-
   selli_receipts_write_latex_2(Files),
   writeln('\\newpage'), nl.

selli_receipts_write_latex_2([File, '000_Null.ps']) :-
   selli_receipts_write_latex_2([File]),
   !.
selli_receipts_write_latex_2([File_1, File_2]) :-
   write_list(['\\receipt{', File_1, '}']), nl,
   writeln('\\vspace*{10mm}'),
   write_list(['\\receipt{', File_2, '}']), nl,
   nl.
selli_receipts_write_latex_2([File]) :-
   write_list(['\\receipt{', File, '}']), nl,
   nl.

selli_receipt_files(Files) :-
   findall( seller_id_and_file(Sid, File),
      ( seller(Seller_1, Sid),
        name_exchange_elements(
           [" _", "�s", "/_", "�a", "�u", "�o", "�U", "�O"],
           Seller_1, Seller_2),
        sid_to_prefix_for_file(Sid, Prefix),
        concat([Prefix, Sid, '_', Seller_2, '.ps'], File) ),
      Sid_File_Atoms_1 ),
   list_to_ord_set(Sid_File_Atoms_1, Sid_File_Atoms_2),
   Empty = '000_Null.ps',
%  Limit = 32,
   Limit = 0,
   findall( [File, Empty],
      ( member(seller_id_and_file(Sid, File), Sid_File_Atoms_2),
        selli_asserted_purchases(seller:Sid, N),
        N > Limit ),
      Pairs ),
   flatten(Pairs, Files_1),
   findall( File,
      ( member(seller_id_and_file(Sid, File), Sid_File_Atoms_2),
        selli_asserted_purchases(seller:Sid, N),
        1 =< N, N =< Limit ),
      Files_2 ),
   findall( File,
      ( member(seller_id_and_file(Sid, File), Sid_File_Atoms_2),
        selli_asserted_purchases(seller:Sid, N),
        N = 0 ),
      Files_3 ),
   append(Files_1, Files_2, Files),
   length(Files_1, N1), M1 is N1/2,
   length(Files_2, N2),
   length(Files_3, N3),
   L is Limit + 1,
   ddk_message([
      '0: ', N3, ', 1-', Limit, ': ', N2, ', ', L, '-: ', M1]).

sid_to_prefix_for_file(Sid, '00') :-
   Sid < 10,
   !.
sid_to_prefix_for_file(Sid, '0') :-
   Sid < 100,
   !.
sid_to_prefix_for_file(_, '').


/* selli_receipt_add_bounding_box(File_1, File_2) <-
      */

selli_receipt_add_bounding_box(File_1, File_2) :-
   read_file_to_string(File_1, String_1),
   append(_, [10|String_2], String_1),
   append(_, [10|String_3], String_2),
   append(_, [10|String_4], String_3),
   append([
      "%!PS\n",
      "%%Title: Seller Receipt\n",
      "%%Creator: html2ps version 1.0 beta3\n",
      "%%Pages: 1\n",
      "%%BoundingBox: 150 300 650 700\n"],
      String_5),
   append(String_5, String_4, String_6),
   name(Name, String_6),
   predicate_to_file( File_2,
      write(Name) ).


/* selli_seller_receipts_ps <-
      */

selli_seller_receipts_ps :-
   findall( seller(Seller_2,Sid),
      ( seller(Seller_1,Sid),
        name_exchange_elements(
           [" _", "�s", "/_", "�a", "�u", "�o", "�U", "�O"],
           Seller_1, Seller_2) ),
      Seller_Pairs ),
   checklist_with_status_bar(
      'Computing Seller Receipts ...',
      selli_seller_receipt_and_ps_file,
      Seller_Pairs ).

selli_seller_receipt_and_ps_file(seller(Seller,Sid)) :-
   selli_table_for_receipt(seller,[_,Sid],Receipt),
   selli_db_dir(Path),
   sid_to_prefix_for_file(Sid, Prefix),
   concat([Path,
      'Reports/Seller_Receipts/',
      Prefix, Sid, '_', Seller, '.html'],
      File_html),
   selli_receipt_to_html_file(seller,Receipt,File_html),
   concat([Path,
      'Reports/Seller_Receipts/',
      Prefix, Sid, '_', Seller, '.ps'],
      File_ps),
   concat(['html2ps ', File_html, ' > ', File_ps],
      Command_1),
   us(Command_1),
   selli_receipt_add_bounding_box(File_ps, File_ps),
   !.


/* list_split_into_sublists_of_size(N, List, Lists) <-
      */

list_split_into_sublists_of_size(N, List, Lists) :-
   list_split_into_sublists_of_size(N, 0, List, [], Lists_2),
   maplist( reverse,
      Lists_2, Lists).
   
list_split_into_sublists_of_size(_, _, [], Ys, [Ys]) :-
   !.
list_split_into_sublists_of_size(N, N, Xs, Ys, [Ys|Lists]) :-
   !,
   list_split_into_sublists_of_size(N, 0, Xs, [], Lists).
list_split_into_sublists_of_size(N, M, [X|Xs], Ys, Lists) :-
   K is M + 1,
   list_split_into_sublists_of_size(N, K, Xs, [X|Ys], Lists).


/* selli_asserted_purchases(seller:Sid, N) <-
      */

selli_asserted_purchases(seller:Sid, N) :-
   selli_asserted_purchases([_, Sid], _, N).


/* selli_asserted_purchases([Bid, Sid], Purchases, N) <-
      */

selli_asserted_purchases([Bid, Sid], Purchases, N) :-
   findall( purchase(Date, Number, Bid, Seller, Price),
      ( purchase(Date, Number, Bid, Seller, Price),
        seller(Seller,Sid) ),
      Purchases ),
   length(Purchases, N).


/******************************************************************/


