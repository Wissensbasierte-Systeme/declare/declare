

/******************************************************************/
/***                                                            ***/
/***          Selli:  Graphical User Interface                  ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      selli_date/1,
      selli_dialog/1.


/*** used files ***************************************************/


selli_db_dir(Path) :-
   dislog_variable_get(home, DisLog_Directory),
   concat(DisLog_Directory, '/projects/Selli/', Path).

selli_db_file(Filename) :-
   selli_db_file('File', Filename).
selli_db_file(Label, Filename) :-
   selli_db_dir(Path),
   dislog_dialog_variable(get, Label-File),
   name_append(Path, File, Filename).
selli_db_print_file(Filename) :-
   selli_db_dir(Path),
   name_append(Path, 'selli_print_file', Filename).


/*** variables ****************************************************/


/* selli_date(Date) <-
      Date is of the form DD.MM.YYYY */

selli_date(Date) :-
   date(date(Year, Month, Day)),
   maplist( number_to_two_digit_name,
      [Year, Month, Day], [Y2, Y3, Y4] ),
   concat([Y4, '.', Y3, '.', Y2], Date).
%  date_is_current(Date).


:- selli_date(Date),
   dislog_variables_set([
      number_of_buyers - 0,
      selli_db_file - Date,
      selli_date - Date,
      selli_buyer_id - 1000,
      selli_total - 0.00,
      selli_db_number - 0,
      selli_number - 1,
      selli_seller - '',
      selli_seller_2 - '',
      selli_seller_id - '1',
      selli_seller_id_2 - '1',
      selli_seller_item - '1',
      selli_price - '0.00',
      selli_sum - 0.00,
      selli_to_pay - 0.00,
      selli_purchase_type - '-',
      selli_input_file_a - '',
      selli_input_file_b - '',
      selli_output_file - '',
      selli_receipts - html,
      selli_kindergarten - dettelbach
   ]).


/*** interface ****************************************************/


selli :-
   set_prolog_flag(encoding, iso_latin_1),
%  selli_date(Date),
   selli_dialog('SELLI - Main - 2').
%  selli_dialog('SELLI - Main').
%  selli_dialog('SELLI - Cashier').
%  selli_dialog('SELLI - Cashier 2').


selli_dialog('SELLI - Main') :-
   dislog_frame_generic_2([
      header: 'SELLI - Second Hand Selling',
      above: [
         'File' - selli_dialog('SELLI - File'),
%        'Cashier' - selli_dialog('SELLI - Cashier'),
%        'Cashier' - selli_dialog('SELLI - Cashier 2'),
         'Sellers' - selli_dialog('SELLI - Sellers'),
         'Statistics' - selli_statistics ],
      interface_left: [
         'Date' - selli_date,
         'Number' - selli_number,
         'Seller' - selli_seller,
         'Type' - selli_purchase_type,
         'Bid' - selli_buyer_id,
         'Total' - selli_total ],
      interface_right: [
         'Sid' - selli_seller_id,
         'Item' - selli_seller_item,
         'Price' - selli_price ],
      below: [
         'New Buyer' - selli_db_dml('insert(buyer)'),
         'Buyer Sum' - selli_db_statistics(buyer_formular),
         'Buyer Report' - selli_db_report('purchase(regular)'),
%        'Buyers' - selli_db_report(buyer),
         'Delete' - selli_db_dml('delete(purchase)') - below,
         'Update' - selli_db_dml('update(purchase)'),
         'Clean' - selli_db_format_clean ],
%        ':<-' - selli_db_find(first) - below,
%        '<-' - selli_db_find(previous),
%        '->' - selli_db_find(next),
%        '->:' - selli_db_find(last),
%        'Goto' - selli_db_find(goto)
      close_button: [
         'Close' - right ] ]),
   dislog_variable_set('Graph',[]-[]).

selli_dialog('SELLI - Cashier') :-
   dislog_frame_generic([
      header: 'SELLI - Cashier',
      above: [],
      interface: [
         'Sid' - selli_seller_id,
         'Item' - selli_seller_item,
         'Price' - selli_price ],
      below: [
%        '     T-Shirt     ' - selli_sell('T-Shirt'),
%        '     Kleid      ' - selli_sell('Kleid'),
%        ' Z: 2/3-Teiler ' - selli_sell('2/3-Teiler'),
%        '      Hose     ' - selli_sell('Hose'),
%        'W: Sweat-Shirt' - selli_sell('Sweat-Shirt') - below,
%        '     M�tze     ' - selli_sell('M�tze'),
%        '     Jacke      ' - selli_sell('Jacke'),
%        '    Y: Baby    ' - selli_sell('Baby'),
%        ' Kinderwagen ' - selli_sell('Kinderwagen') - below,
%        ' A: Fahrzeug ' - selli_sell('Fahrzeug'),
%        ' G: Spielzeug ' - selli_sell('Spielzeug'),
%        '  O: STORE  ' - selli_db_dml('insert(purchase)'),
         '     T-Shirt      ' - selli_sell('T-Shirt'),
         '    Kleid    ' - selli_sell('Kleid'),
         ' 2/3-Teiler ' - selli_sell('2/3-Teiler'),
         '   Hose   ' - selli_sell('Hose'),
         '  Sweat-Shirt  ' - selli_sell('Sweat-Shirt') - below,
         '   M�tze    ' - selli_sell('M�tze'),
         '   Jacke    ' - selli_sell('Jacke'),
         '   Baby   ' - selli_sell('Baby'),
         ' Kinderwagen ' - selli_sell('Kinderwagen') - below,
         ' Fahrzeug ' - selli_sell('Fahrzeug'),
         ' Spielzeug ' - selli_sell('Spielzeug'),
         '   Store  ' - selli_db_dml('insert(purchase)') - below ],
      close_button: [
         'Close' - right ] ]).

selli_dialog('SELLI - Cashier 2') :-
   dislog_frame_generic([
      header: 'SELLI - Cashier 2',
      above: [],
      interface: [
         'Sid' - selli_seller_id,
         'Item' - selli_seller_item,
         'Price' - selli_price ],
      below: [
         'Store' - selli_db_dml('insert(purchase) 2') ],
      close_button: [ ] ]).

selli_dialog('SELLI - File') :-
   selli_db_dir(Directory),
   new(Frame,frame('SELLI - File Manager')),
   send(Frame,append,new(Dialog_I,dialog)),
   dislog_variable_get(dislog_frame_interface_color,Colour),
   send(Dialog_I,background,Colour),
   send(Dialog_I,append,new(Browser,browser)),
   send(Browser,label,Directory),
   send(new(Dialog,dialog),below,Dialog_I),
   checklist( send_append(Dialog), [
      'New File' - selli_db_dml_create_new_file(Browser),
      'Load' - selli_db_dml_load_file(Directory,Browser?selection?key),
      'Save' - selli_db_dml_save_file(Directory,Browser?selection?key),
      'Save as' - selli_db_dml_save_as_file(Browser),
      'Merge' - selli_dialog('SELLI - Merge Files') - below,
      'View' - file_view(Directory,Browser?selection?key),
      'Close' - send(Frame,destroy) ] ),
   send(Browser,members,directory(Directory)?files),
   send(Frame,open).

/*
selli_dialog('SELLI - File') :-
   dislog_dialog_generic('SELLI - File',
      [ 'File' - selli_db_file ],
      [ 'Load' - selli_db_dml('load(file)') - below,
        'Save' - selli_db_dml('save(file)'),
        'Merge' - selli_dialog('SELLI - Merge Files') - below ]).
*/


selli_dialog('SELLI - Merge Files') :-
   dislog_frame_generic([
      header: 'SELLI - Merge Files',
      above: [],
      interface: [
         'Input File A' - selli_input_file_a,
         'Input File B' - selli_input_file_b,
         'Output File'  - selli_output_file ],
      below: [
         'Merge' - selli_db_dml('merge(file)') ],
      close_button: [
         'Close' - right ] ]).


selli_dialog('SELLI - Sellers') :-
   dislog_frame_generic([
      header: 'SELLI - Seller Manager',
      above: [ ],
      interface: [
         ' Sid' - selli_seller_id_2,
         ' Seller' - selli_seller_2,
         'Sum' - selli_sum,
         'To Pay' - selli_to_pay ],
      below: [
         'New Seller' - selli_db_dml('insert(seller)'),
         'Seller Sum' - selli_db_statistics(seller_formular),
         'Seller Report' - selli_db_report(seller),
         'Seller Receipts' -
            selli_seller_receipts_in_one_pdf - below ],
%        'Print' - selli_db_report(receipt) ],
%        'Print' - selli_seller_receipt - below ],
      close_button: [
%        'Close' - below,
         'Close' - right ] ]).


/******************************************************************/


