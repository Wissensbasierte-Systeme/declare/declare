

/******************************************************************/
/***                                                            ***/
/***          Selli:  File Operations                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* selli_db_dml('load(file)') <-
      */

selli_db_dml('load(file)') :-
   selli_db_file(File),
   selli_db_dml_load_file(File).

selli_db_dml_load_file(Directory,File) :-
   concat(Directory,File,Path),
   selli_db_dml_load_file(Path),
   selli_db_report('purchase(regular)').

selli_db_dml_load_file(Path) :-
   diconsult(Path,State),
   checklist( state_atoms_assert(State),
      [seller/2, purchase/5, purchase_type/2] ),
   member([actual_node(Actual)],State),
   member([last_buyer_number(Bid)],State),
   member([last_transaction_number(N)],State),
   member([graph_nodes(Nodes)],State),
   member([graph_edges(Edges)],State),
   dislog_variables_set([
      'Graph'-(Nodes-Edges),
      'Actual'-Actual,
      selli_buyer_id-Bid,
      selli_db_number-N,
      selli_db_file-Path ]),
   dislog_dialog_variables( send, [
      'Bid' - Bid,
      'Number' - N ] ),
   ( selli_db_find(last)
   ; true ),
   ddk_tone(success).


/* selli_db_dml('save(file)') <-
      */

selli_db_dml('save(file)') :-
   selli_db_dml('save_no_message(file)'),
   dislog_variable_get(selli_db_file,File),
%  selli_db_file(File),
   write(File), writeln(' saved.').

selli_db_dml('save_no_message(file)') :-
   dislog_variable_get(selli_db_file,File),
%  selli_db_file(File),
   selli_db_dml_save_file(File).
   

/* selli_db_dml_save_as_file(Browser) <-
      */

selli_db_dml_save_as_file(Browser) :-
   ask_name('Save as File','File',File),
   selli_db_dir(Directory),
   selli_db_dml_save_file(Directory,File),
   send(Browser,members,directory(Directory)?files).

/*
selli_db_dml_save_as_file(Browser) :-
   dislog_variable_set(entity_entered_name,''),
   dislog_frame_generic(Frame, [
      header: 'Save as File',
      above: [],
      interface: [
         'File' - entity_entered_name ],
      below: [
         'Save' - selli_db_dml_save_as_file(Frame,Browser) ],
      close_button: [
         'Cancel' - right ] ]).

selli_db_dml_save_as_file(Frame,Browser) :-
   selli_db_dir(Directory),
   dislog_dialog_variable(get,'File':File),
   selli_db_dml_save_file(Directory,File),
   send(Frame,destroy),
   send(Browser,members,directory(Directory)?files).
*/


/* selli_db_dml_archive <-
      */

selli_db_dml_archive :-
   selli_db_dml_archive_no_table,
   selli_archive_to_html_table.

selli_db_dml_archive_no_table :-
   selli_db_dir(Directory),
   selli_file_timestamp(Timestamp),
   name_append([Directory, 'Saves/', Timestamp], Path),
   selli_db_dml_save_file(Path).

selli_db_dml_archive_possibly :-
   dislog_variable_get(number_of_buyers, N1),
   N2 is N1 + 1,
   ( ( N2 = 10,
       !,
       selli_db_dml_archive_no_table,
       dislog_variable_set(number_of_buyers, 0) )
   ; dislog_variable_set(number_of_buyers, N2) ).
  

/* selli_db_dml_save_file(Directory,File) <-
      */

selli_db_dml_save_file(Directory,File) :-
   name_append(Directory,File,Path),
   selli_db_dml_save_file(Path).

selli_db_dml_save_file(Path) :-
   maplist( state_atoms_collect,
      [seller/2, purchase/5, purchase_type/2],
      [State_Sellers, State_Purchases, State_Purchase_Types] ),
   list_to_ord_set(State_Sellers,State_1),
   list_to_ord_set(State_Purchases,State_2),
   list_to_ord_set(State_Purchase_Types,State_3),
   dislog_variables_get([
      'Graph': Nodes-Edges,
      'Actual': Actual,
      selli_buyer_id: Bid,
      selli_db_number: N ]),
   list_to_ord_set(Nodes,Nodes_Sorted),
   list_to_ord_set(Edges,Edges_Sorted),
   State_4 = [
      [last_transaction_number(N)],
      [last_buyer_number(Bid)],
      [actual_node(Actual)],
      [graph_nodes(Nodes_Sorted)],
      [graph_edges(Edges_Sorted)] ],
   selli_append([State_1,State_2,State_3,State_4],State),
   disave( selli_proper_write,
      State, Path ),
%  writeln(disave(Path)),
   ddk_tone(success).


/* selli_db_dml_create_new_file(Browser) <-
      */

selli_db_dml_create_new_file(Browser) :-
   ask_name('New File Name', 'File', File),
   selli_db_dir(Directory),
   concat(Directory, File, Path),
   ( ( file_exists(Path),
       !,
       ddk_message([
          'This file exists already !\n', 'It cannot be created.']) )
   ; ( selli_db_dml_create_empty_state(State),
       disave(selli_proper_write, State, Path),
       ddk_tone(success),
       send(Browser, members, directory(Directory)?files),
       selli_db_dml_load_file(Path) ) ).

selli_db_dml_create_empty_state(State) :-
   State = [ [last_transaction_number(0)], [last_buyer_number(1)],
      [actual_node(0)], [graph_nodes([])], [graph_edges([])] ].


/******************************************************************/


