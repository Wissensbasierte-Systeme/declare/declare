

/******************************************************************/
/***                                                            ***/
/***          Selli:  Purchase Report in XPCE                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* selli_add_to_purchase_report(D, N, B, I, S, Item, Type, P) <-
      */

selli_add_to_purchase_report(D, N, B, I, S, Item, Type, P) :-
   catch(
      selli_add_to_purchase_report_2(
         D, N, B, I, S, Item, Type, P),
      _, true ).

selli_add_to_purchase_report_2(D, N, B, I, S, Item, Type, P) :-
   dislog_variable_get(selli_report_incremental, X),
   Tabular := X^tabular,
   Window := X^window,
   send_row(Tabular, [D, N, B, I, S, Item, Type, P]),
   send(Tabular, next_row),
   xpce_window_scroll_vertical(Window, 1),
   dislog_variable_set(
      selli_purchases_to_xpce_table_status, middle).


/* selli_new_buyer_in_purchase_report <-
      */

selli_new_buyer_in_purchase_report :-
   catch(
      selli_new_buyer_in_purchase_report_2,
      _, true ).

selli_new_buyer_in_purchase_report_2 :-
   dislog_variable_get(selli_report_incremental, X),
   Tabular := X^tabular,
   Window := X^window,
   selli_new_buyer_in_purchase_report_2_if(Window, Tabular),
   Attributes = ['Date', 'Tid',
      'Bid', 'Sid', 'Seller', 'Item', 'Type', 'Price' ],
   send_attributes(Tabular, Attributes),
   xpce_window_scroll_vertical(Window, 2),
   dislog_variable_set(
      selli_purchases_to_xpce_table_status, middle).

selli_new_buyer_in_purchase_report_2_if(_, _) :-
   dislog_variable_get(
      selli_purchases_to_xpce_table_status, started),
   !.
selli_new_buyer_in_purchase_report_2_if(Window, Tabular) :-
   dislog_dialog_variables( get, [
      'Date' - Date, 'Bid' - Bid ] ),
   findall( Price,
      purchase(Date, _, Bid, _, Price),
      Prices ),
   add(Prices, Price),
   send_row(Tabular, ['', '', '', '', '', '', '', Price]),
   send(Tabular, next_row),
   send_row(Tabular, white, ['']),
   send(Tabular, next_row),
   xpce_window_scroll_vertical(Window, 1).


/* selli_purchases_to_xpce_table(
         Window, Tabular, Title, Attributes, Purchases) <-
      */

selli_purchases_to_xpce_table(Title, Attributes, Purchases) :-
   selli_purchases_to_xpce_table(
      Window, Tabular, Title, Attributes, Purchases),
   dislog_variable_set(selli_report_incremental,
      [window:Window, tabular:Tabular]).

selli_purchases_to_xpce_table(
      Window, Tabular, Title, Attributes, Purchases) :-
   new(Window, auto_sized_picture(Title)),
   send(Window, display, new(Tabular, tabular)),
   send(Tabular, border, 1),
%  send(Tabular, cell_padding, size(8, 4)),
   send(Tabular, cell_spacing, -1),
   send(Tabular, rules, all),
   send(Tabular, colour, white),
   xpce_table_add_scroll_bar(Window),
%  ( length(Purchases, N),
%    N < 30,
%    xpce_table_add_scroll_bar(Window, Tabular)
%  ; true ),
   ( Purchases = [],
     send_attributes(Tabular, Attributes)
   ; true ),
   sort_by_components([3], Purchases, Purchases_2),
   selli_purchases_to_xpce_table_iterate(
      Window, Tabular, Attributes, Purchases_2),
   send(Tabular, next_row),
   send(Window, open),
   dislog_variable_set(
      selli_purchases_to_xpce_table_status, started).

selli_purchases_to_xpce_table_iterate(
      Window, Tabular, Attributes, Purchases) :-
   first(Purchases, [_, _, Bid|_]),
   send_attributes(Tabular, Attributes),
   selli_purchases_to_xpce_table_iterate(
      Window, Tabular, Attributes, Bid, 0, Purchases).
selli_purchases_to_xpce_table_iterate(_, _, _, []).

selli_purchases_to_xpce_table_iterate(
      Window, Tabular, Attributes, Bid, S1, [P|Ps]) :-
   P = [_, _, Bid, _, _, _, _, Price],
   !,
   send_row(Tabular, P),
   send(Tabular, next_row), 
   S2 is S1 + Price,
   selli_purchases_to_xpce_table_iterate(
      Window, Tabular, Attributes, Bid, S2, Ps).
selli_purchases_to_xpce_table_iterate(
      Window, Tabular, Attributes, _, S, Ps) :-
   Total = ['', '', '', '', '', '', '', S],
   send_row(Tabular, Total),
   send(Tabular, next_row),
   send_row(Tabular, white, ['']),
   send(Tabular, next_row),
   selli_purchases_to_xpce_table_iterate(
      Window, Tabular, Attributes, Ps).


/******************************************************************/


