

/******************************************************************/
/***                                                            ***/
/***          Selli:  Graphical User Interface - 2              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* selli_dialog('SELLI - Main - 2') <-
      */

selli_dialog('SELLI - Main - 2') :-
   ddk_version(Version),
   concat('SELLI - Second Hand Selling - ', Version, Title),
   selli_gui(_, Title).


/* selli_gui(Frame, Header) <-
      */

selli_gui(Frame, Header) :-
   new(Frame, frame(Header)),
   send(Frame, background, colour(white)),
   send(Frame, append, new(Dialog_A, dialog)),
   new(Dialog_B_1, dialog),
   send(new(Dialog_B_2, dialog), right, Dialog_B_1),
   send(Dialog_B_1, below, Dialog_A),
%  dislog_variable_get(dislog_frame_interface_color, Colour),
%  send(Dialog_A, background, lightsteelblue3),
%  send(Dialog_B_1, background, lightsteelblue2),
%  send(Dialog_B_2, background, lightsteelblue2),
%  rgb_to_colour([13.5, 13.5, 14], Colour_A),
%  rgb_to_colour([14.0, 14.0, 15.99], Colour_B),
%  rgb_to_colour([15.6, 15.6, 15.99], Colour_B),
   rgb_to_colour([15.2, 15.2, 15.99], Colour_B),
   rgb_to_colour([14.5, 14.5, 15.99], Colour_A),
   send(Dialog_A, background, Colour_A),
   send(Dialog_B_1, background, Colour_B),
   send(Dialog_B_2, background, Colour_B),
   send(Dialog_A, append, new(Menu_Bar, menu_bar)),
   selli_gui_send_pulldown_menus(Frame, Menu_Bar),
   selli_gui_send_toolbar(Dialog_A),
   send(Dialog_B_1, append,
      new(Dialog_G_Left, dialog_group('General', box))),
   send(Dialog_B_2, append,
      new(Dialog_G_Right, dialog_group('Cashier', box))),
   selli_gui_send_left_interface(Dialog_G_Left),
   selli_gui_send_right_interface(Dialog_G_Right),
   send(Frame, open).
   

/*** implementation ***********************************************/


selli_gui_send_left_interface(Dialog) :-
   Interface = [
      'Date' - selli_date,
      'Number' - selli_number,
      'Seller' - selli_seller,
      'Type' - selli_purchase_type,
      'Bid' - selli_buyer_id,
      'Total' - selli_total ],
   selli_gui_send_interface(Dialog,Interface).

selli_gui_send_right_interface(Dialog) :-
   Interface = [
      'Sid' - selli_seller_id,
      'Item' - selli_seller_item,
      'Price' - selli_price ],
   selli_gui_send_interface(Dialog,Interface),
   send(Dialog, append, button('Enter',
      message(@prolog, selli_db_dml, 'insert(purchase) 2'))).

selli_gui_send_interface(Dialog, Interface) :-
%  send(Dialog, gap, size(0, 25)),
   maplist( pair_to_triple,
      Interface, Triples ),
   checklist( send_text_menu_2(Dialog),
      Triples ),
   checklist( triple_to_dislog_variable,
      Triples ).

send_text_menu_2(Target,
      Menu_Name - DisLog_Variable - Reference ) :-
   dislog_variable_get(DisLog_Variable, Value),
   send(Target, append,
      new(Reference, text_item(Menu_Name, Value))),
   send(Reference, width, 20),
   send(Reference, label_font, font(helvetica, roman, 12)).


selli_gui_send_pulldown_menus(Frame, Menu_Bar) :-
   send_pulldown_menu(Menu_Bar, 'File', [
      'File Manager' - selli_dialog('SELLI - File'),
      'Seller Manager' - selli_dialog('SELLI - Sellers'),
      'Archive' - selli_db_dml_archive,
      'Quit' - send(Frame,destroy) ]),
   send_pulldown_menu(Menu_Bar, 'Edit', [
      'Delete' - selli_db_dml('delete(purchase)'),
      'Update' - selli_db_dml('update(purchase)'),
      'Clean' - selli_db_format_clean ]),
   send_pulldown_menu(Menu_Bar, 'Buyers', [
      'New Buyer' - selli_db_dml('insert(buyer)'),
      'Buyer Sum' - selli_db_statistics(buyer_formular),
      'Buyer Report' - selli_db_report('purchase(regular)') ]),
   send_pulldown_menu(Menu_Bar, 'Statistics', [
      'Purchases' - selli_bar_chart(purchases),
      'Sellers' - selli_bar_chart(sellers),
      'Buyers' - selli_bar_chart(buyers) ]).

selli_gui_send_toolbar(Dialog) :-
   send(Dialog, append, new(TB, tool_bar)),
   send(TB, append, new(First, tool_button(
      message(@prolog, selli_db_find, first),
      'left_arrow.bm'))),
%     'fatleft_arrow.bm'))),
   send(First, help_message, tag, 'First'),
   send(First, colour, lightsteelblue4),
   send(TB, gap, size(4, 0)),
   send(TB, append, new(Previous, tool_button(
      message(@prolog, selli_db_find, previous),
      'ms_right_arrow.bm'))),
   send(Previous, help_message, tag, 'Previous'),
   send(Previous, colour, lightsteelblue4),
   send(TB, append, new(Next, tool_button(
      message(@prolog, selli_db_find, next),
      'ms_left_arrow.bm'))),
   send(Next, help_message, tag, 'Next'),
   send(Next, colour, lightsteelblue4),
   send(TB, append, new(Last, tool_button(
      message(@prolog, selli_db_find, last),
      'right_arrow.bm'))),
%     'fatright_arrow.bm'))),
   send(Last, help_message, tag, 'Last'),
   send(Last, colour, lightsteelblue4),
   send(TB, append, new(Goto, tool_button(
      message(@prolog, selli_db_find, goto),
      'ol_pullright.bm'))),
%     'bullseye.bm'))),
%     'on_marked.bm'))),
   send(Goto, help_message, tag, 'Goto'),
   send(Goto, colour, lightsteelblue4),
   send(TB, append, new(New_Buyer, tool_button(
      message(@prolog,selli_db_dml, 'insert(buyer) 2'),
      'file.bm'))),
   send(New_Buyer, help_message, tag, 'New Buyer'),
   send(New_Buyer, colour, lightsteelblue4).


/******************************************************************/


