

/******************************************************************/
/***                                                            ***/
/***          Selli:  Receipts for Sellers and Buyers           ***/
/***                                                            ***/
/******************************************************************/


:- catch( use_module(library('netscape')),
      _, true ).


/*** interface ****************************************************/


/* selli_buyer_receipt(Bid) <-
      */

selli_seller_receipt(Sid) :-
   selli_receipt(seller,[_,Sid]).

selli_buyer_receipt(Bid) :-
   selli_receipt(buyer,[Bid,_]).

selli_receipt(Mode,Ids) :-
   dislog_variable_get(selli_receipts,html),
   !,
   selli_db_dir(Path),
   concat(Path,'Reports/selli_receipt.html',File),
   selli_table_for_receipt(Mode,Ids,Receipt),
   selli_receipt_to_html_file(Mode,Receipt,File),
   show_html_file_in_frame(File,size(700,500)).
%  html_file_to_display(File).
   

/* selli_receipt_to_html_file(Mode,Receipt,File) <-
      */
% Date,Name,Sid,Number,Price,Rows

selli_receipt_to_html_file(Mode,[Info,Width,Rows],File) :-
   ( Mode =  buyer,
     Title = 'Buyer Receipt'
   ; Mode =  seller,
     Title = 'Seller Receipt' ),
   concat(['<title>',Title,'</title>'],T),
   Head = head:[ T ],
   maplist( row_to_html_row,
      Rows, HTML_Rows ),
   Table = table:[ 'BORDER':0, 'WIDTH':Width, cellspacing:3,
%     'BGCOLOR':'#e9e9e9' ]:HTML_Rows,
      'BGCOLOR':white ]:HTML_Rows,
   table_to_receipt_html_body(Mode,Info,Table,Body),
   Document = [ html:[ Head, Body ] ],
   predicate_to_file( File,
      field_notation_to_xml(0,Document) ),
   selli_html_file_prune(File, File).

table_to_receipt_html_body( buyer,
      [Date,Number,Price], Table, Body ) :-
   selli_receipt_bottom_lines([Space,Line_1,Line_2]),
   selli_beautify_price(Price,Price_HTML),
   Body = body:[ '<hr>',
      blockquote:[
         h3:['Second-Hand Verkauf am ', Date, ' <br>',
            'Einzelaufstellung'],
         'Gekaufte Artikel: &nbsp;', Number,
         '<br><p>',
         blockquote:[ Table ],
         '<br>',
         'Kaufsumme: &nbsp;', Price_HTML, ' Euro',
         '<br><p> <br><p>',
         'Wir bedanken uns f�r Ihren Einkauf.',
         Space, Line_1, Line_2,
         '<br><p>', '<br><p>' ],
      '<hr>' ].

table_to_receipt_html_body( seller,
      [Date,Seller_Name,Sid,Number,Price], Table, Body ) :-
   selli_receipt_bottom_lines([Space,Line_1,Line_2]),
   round(0.15 * Price,2,Price_2),
   round(0.85 * Price,2,Price_3),
   selli_beautify_price(Price,Price_HTML),
   selli_beautify_price(Price_2,Price_2_HTML),
   selli_beautify_price(Price_3,Price_3_HTML),
%  Title = 'Second-Hand Verkauf',
   Title = 'Kinderkleidermarkt',
%  concat(['N&auml;chste Kleiderm&auml;rkte: ',
%     '28.03.2009 Fr&uuml;hjahr/Sommer, ',
%     '26.09.2009 Herbst/Winter'], Dates),
%  Dates = 'N&auml;chster Kleidermarkt: 25.09.2010 Herbst/Winter',
   Dates = 'N&auml;chster Kleidermarkt: 17.09.2011 Herbst/Winter',
   Body = body:[ '<hr>',
      blockquote:[
         h3:[Title, ' am ', Date, ' <br>', 'Einzelaufstellung'],
         h4:[Seller_Name],
         'Kundennummer: &nbsp;', Sid, '<br>&nbsp;<p>',
         blockquote:[
            'Verkaufte Artikel: &nbsp;', Number, '<br><p>',
            Table, '<p>',
            table:[align:left]:[
               tr:[ td:'Verkaufsumme',
                    td:[align:right]:[Price_HTML, ' Euro'] ],
               tr:[ td:'- 15 %',
                    td:[align:right]:[Price_2_HTML, ' Euro'] ],
               tr:[ td:'Erl&ouml;s',
                    td:[align:right]:[Price_3_HTML, ' Euro'] ] ] ],
         '<br><br><br><br><br>',
         'Wir bedanken uns f&uuml;r Ihre Teilnahme.',
         Space, Line_1, Line_2,
         '<br><p>', Dates, '<br><p>' ],
      '<hr>' ].


row_to_html_row(Row,HTML_Row) :-
   maplist( item_to_td_item,
      Row, TD_Items ),
   HTML_Row = tr:TD_Items.

item_to_td_item(Item,td:[Item]).


/* selli_receipt_bottom_lines([Space,Line_1,Line_2]) <-
      */

selli_receipt_bottom_lines([Space,Line_1,Line_2]) :-
   dislog_variable_get(selli_kindergarten,dettelbach),
   !,
   selli_receipt_bottom_line_space(Space),
   Line_1 = '___________________ <br>',
   Line_2 = 'Kindergarten Dettelbach'.
selli_receipt_bottom_lines([Space,Line_1,Line_2]) :-
   dislog_variable_get(selli_kindergarten,mainstockheim),
   !,
   selli_receipt_bottom_line_space(Space),
   Line_1 = '______________________ <br>',
   Line_2 = 'Kindergarten Mainstockheim'.

selli_receipt_bottom_line_space(Space) :-
   current_prolog_flag(unix,true),
   Space = '<br>&nbsp;<p>'.
selli_receipt_bottom_line_space(Space) :-
   current_prolog_flag(windows,true),
   Space = '<br>&nbsp;<p>'.


/* selli_table_for_receipt(Mode,[Bid,Sid],Receipt) <-
      */

selli_table_for_receipt(Mode,[Bid,Sid],Receipt) :-
   selli_date(Date),
   selli_asserted_purchases_to_items([Bid,Sid],Items),
   selli_items_to_table_width(Items,N,Width),
   selli_items_to_rows(Items,Rows),
   selli_asserted_purchases_to_total_price(Mode,[Bid,Sid],Price),
   ( Mode = buyer,
     Receipt = [[Date,N,Price],Width,Rows]
   ; Mode = seller,
     seller(Seller_Name,Sid),
     Receipt = [[Date,Seller_Name,Sid,N,Price],Width,Rows] ).

selli_items_to_table_width(Items,N,Width) :-
   length(Items,N),
   minimum(N,8,M),
   Width is 100 + M * 75.

selli_asserted_purchases_to_items([Bid,Sid],Items) :-
   findall( [Type,Price],
      ( purchase_enhanced(_,_,Bid,Sid:_,Item_2:Type_2:Price_2),
        selli_adjust_type(Item_2:Type_2,Type),
        selli_beautify_price(Price_2,Price) ),
      Items ).

selli_items_to_rows(Items,Rows) :-
   maplist( nth(1),
      Items, Articles ),
   maplist( nth(2),
      Items, Prices ),
   split_list_in_parts(8,Articles,As),
   split_list_in_parts(8,Prices,Ps),
   maplist( append(['Artikel']),
      As, As_2 ),
   maplist( append(['Preis']),
      Ps, Ps_2 ),
   merge_lists(As_2,Ps_2,Rows).

selli_asserted_purchases_to_total_price(Mode,[Bid,Sid],Price) :-
   ( Mode = buyer,
     Seller_Name = _
   ; Mode = seller,
     seller(Seller_Name,Sid) ),
   findall( Price,
      purchase(_,_,Bid,Seller_Name,Price),
      Prices ),
   add(Prices,Price).


/* split_list_in_parts(N,Xs,Ts) <-
      */

split_list_in_parts(N,Xs,[Xs]) :-
   length(Xs,M),
   M =< N,
   !.
split_list_in_parts(N,Xs,[T|Ts]) :-
   first_n_elements(N,Xs,T),
   append(T,Xs_2,Xs),
   split_list_in_parts(N,Xs_2,Ts).


/* merge_lists(Xs,Ys,XYs) <-
      */

merge_lists([X|Xs],[Y|Ys],[X,Y|XYs]) :-
   merge_lists(Xs,Ys,XYs).
merge_lists([],[],[]).


/* selli_html_file_prune(File_1, File_2) <-
      */

selli_html_file_prune(File_1, File_2) :-
   dread(txt, File_1, Name_1),
   Substitution = [
      [[228], "&auml;"],
      [[246], "&ouml;"],
      [[252], "&uuml;"],
      [[196], "&Auml;"],
      [[214], "&Ouml;"],
      [[220], "&Uuml;"],
      [[223], "&szlig;"] ],
   name_exchange_sublist(Substitution, Name_1, Name_2),
   dwrite(txt, File_2, Name_2).


/*** tests ********************************************************/


test(selli,selli_rows_to_buyer_receipt) :-
   selli_db_dir(Path),
   concat(Path, 'Reports/buyer_receipt.html', File),
   Date = '31.07.2002',
   Number = 10,
   Price = 152,
   Width = 700,
   Rows = [
      [ 'Artikel', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
      [ 'Preis', 5, 9, 5, 9, 2, 3, 4, 1.5, 2, 5 ] ],
   selli_receipt_to_html_file( buyer,
      [[Date,Number,Price],Width,Rows], File ).


/******************************************************************/


