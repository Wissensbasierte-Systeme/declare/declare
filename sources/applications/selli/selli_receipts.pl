

/******************************************************************/
/***                                                            ***/
/***          Selli:  Receipts for Sellers and Buyers           ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      selli_buyer_receipt/1,
      selli_seller_receipt/1.


/*** interface ****************************************************/


/* selli_seller_receipt <-
      */

selli_seller_receipt :-
   dislog_dialog_variable(get, ' Sid' - I),
   selli_term_to_atom('Sid',Sid,I),
   selli_seller_receipt(Sid),
   ddk_tone(success).

selli_seller_receipt(Sid) :-
   dislog_variable_get(selli_receipts,txt),
   !,
   selli_db_dir(Path),
   name_append(Path,'Reports/seller_receipt.txt',File),
   write('---> '), writeln(File),
   predicate_to_file( File,
      selli_seller_receipt(3,Sid) ).


/* selli_buyer_receipt <-
      */

selli_buyer_receipt :-
   dislog_dialog_variable( get, 'Bid' - Bid ),
   selli_buyer_receipt(Bid),
   ddk_tone(success).

selli_buyer_receipt(Bid) :-
   dislog_variable_get(selli_receipts,txt),
   !,
   selli_db_dir(Path),
   name_append(Path,'Reports/buyer_receipt.txt',File),
   write(user,'---> '), writeln(user,File),
   predicate_to_file( File,
      selli_buyer_receipt(3,Bid) ).


/*** implementation ***********************************************/


selli_seller_receipt(Tab,Sid) :-
   seller(Seller,Sid),
   bar_line_std_out, nl, nl,
   tab(Tab),
   selli_date(Date),
   write_list(
      ['Second-Hand Verkauf am ',Date,' - Einzelaufstellung']),
   nl, nl, nl,
   tab(Tab), writeln(Seller),
   name(Seller,List), length(List,Seller_Length),
   tab(Tab), writeln_bars(Seller_Length), nl, nl,
   tab(Tab), write('Kundennummer: '), writeln(Sid), nl,
   selli_sold_articles(Tab,Sid:Seller),
   selli_seller_calculation(Tab,Seller), nl, nl,
   tab(Tab), writeln('Wir bedanken uns f�r Ihre Teilnahme'), nl,
   tab(Tab), writeln('Kindergarten Dettelbach'), nl, nl,
   bar_line_std_out.


selli_buyer_receipt(Tab,Bid) :-
   bar_line_std_out, nl, nl,
   tab(Tab),
   selli_date(Date),
   write_list(
      ['Second-Hand Verkauf am ',Date,' - Einzelaufstellung']),
   nl, nl, nl,
   tab(Tab), writeln('K�ufer:'), nl,
   selli_bought_articles(Tab,Bid),
   selli_buyer_calculation(Tab,Bid), nl, nl,
   tab(Tab), writeln('Wir bedanken uns f�r Ihren Einkauf.'), nl, nl, nl,
   tab(Tab), writeln('_______________________'),
   tab(Tab), writeln('Kindergarten Dettelbach'), nl, nl,
   bar_line_std_out.


selli_sold_articles(Tab,Sid:Seller) :-
   tab(Tab),
   write('Verkaufte Artikel: '),
   findall( Type:Price,
      ( purchase_enhanced(_,_,_,Sid:Seller,Item_2:Type_2:Price),
        selli_adjust_type(Item_2:Type_2,Type) ),
      Items ),
   length(Items,Number_of_Items),
   write(Number_of_Items), write(', '),
   Tab_2 is Tab + 3,
   selli_pp_list(Tab_2,4,Items), nl.
        

selli_bought_articles(Tab,Bid) :-
   tab(Tab), write('Gekaufte Artikel: '),
   findall( Type:Price,
      ( purchase_enhanced(_,_,Bid,_,Item_2:Type_2:Price),
        selli_adjust_type(Item_2:Type_2,Type) ),
      Items ),
   length(Items,Number_of_Items),
   write(Number_of_Items), write(', '),
   Tab_2 is Tab + 3,
   selli_pp_list(Tab_2,4,Items), nl.


selli_buyer_calculation(Tab,Bid) :-
   findall( Price,
      purchase(_,_,Bid,_,Price),
      Prices ),
   add(Prices,Price),
   selli_beautify_price(Price,Price_2), nl,
   tab(Tab), write('Kaufssumme   Euro '), writeln(Price_2).
 

selli_seller_calculation(Tab,Seller) :-
   findall( Price,
      purchase(_,_,_,Seller,Price),
      Prices ),
   add(Prices,Price),
   selli_beautify_price(Price,Price_2),
   round(Price * 0.85,2,To_Pay),
   selli_beautify_price(To_Pay,To_Pay_2),
   round(Price * 0.15,2,Commission),
   selli_beautify_price(Commission,Commission_2), nl,
   tab(Tab), write('Verkaufssumme   Euro '), writeln(Price_2),
   tab(Tab), write('- 15%           Euro '), writeln(Commission_2),
   tab(Tab), writeln_bars(27), nl,
   tab(Tab), write('Erl�s           Euro '), writeln(To_Pay_2).


writeln_bars(N) :-
   N > 0,
   !,
   write('-'),
   M is N - 1,
   writeln_bars(M).
writeln_bars(0).


/* selli_beautify_price(Price_1,Price_2) <-
      */

selli_beautify_price(Price_1,Price_2) :-
   name(Price_1,List_1),
   selli_split_price(List_1,A,B),
   length(A,N), length(B,M),
   selli_price_add_in_front(N,Front),
   selli_price_add_at_back(M,Back),
   append([Front,A,".",B,Back],List_2),
   name(Price_2,List_2).
%  writeln(user,Price_1),
%  writeln(user,Price_2).
   
selli_split_price(List,A,B) :-
   ( append(A,[46|B],List)
   ; A = List, B = [] ),
   !.

selli_price_add_in_front(N,Xs) :-
   N =< 4,
   !,
   M is 4 - N,
   multify(" ",M,Ls),
   append(Ls,Xs),
   !.
selli_price_add_in_front(_,"").

selli_price_add_at_back(N, X) :-
   member([N, X], [
      [0, "-- "], [1, "0 "], [2, " "], [_, ""] ]),
   !.

/*
selli_price_add_at_back(0,"-- ") :-
   !.
selli_price_add_at_back(1,"0 ") :-
   !.
selli_price_add_at_back(2," ") :-
   !.
selli_price_add_at_back(_,"").
*/


/* selli_adjust_type(Item_Type_1,Item_Type_2) <-
      */

selli_adjust_type('':Type,Type) :-
   !.
selli_adjust_type(Item:'-',Number_Item) :-
   !,
   Number_Item = Item.
%  name_append('#',Item,Number_Item).
selli_adjust_type(Item:Type,Number_Item=Type) :-
   Number_Item = Item.
%  name_append('#',Item,Number_Item).
 

/******************************************************************/


