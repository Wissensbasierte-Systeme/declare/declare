

/******************************************************************/
/***                                                            ***/
/***          Selli:  Database Operations                       ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      purchase/5,
      purchase_type/2,
      selli_db_dml/1.
:- dynamic
      purchase/5,
      purchase_type/2,
      seller/2.
:- discontiguous
      selli_db_dml/1.


/*** interface ****************************************************/


/* selli_db_dml('insert(X)') <-
      data manipulation language for selli database. */


selli_db_dml('insert(seller)') :-
   dislog_dialog_variables( get, [
      ' Seller' - S, ' Sid' - I2 ] ),
   selli_term_to_atom(' Sid',I,I2),
   \+ seller(_,I),
   assert(seller(S,I)),
   listing(seller/2).
selli_db_dml('insert(seller)').


selli_db_dml('insert(buyer)') :-
   selli_buyer_receipt,
   selli_db_dml('insert(buyer) 2').

selli_db_dml('insert(buyer) 2') :-
   selli_new_buyer_in_purchase_report,
   selli_db_statistics(buyer_formular),
   dislog_variable_get(selli_buyer_id, N1),
   N2 is N1 + 1,
   dislog_variable_set(selli_buyer_id, N2),
   dislog_dialog_variable(send, 'Bid'-N2),
   selli_db_dml_archive_possibly.


selli_db_dml('insert(purchase)') :-
   get_new_selli_db_number(N),
   dislog_dialog_variables( get, [
      'Date' - D, 'Bid' - B,
      'Sid' - I2, 'Item' - Item, 'Price' - P2 ] ),
   selli_term_to_atom('Sid',I,I2),
   selli_price_atom_transform(P2,P1),
   selli_term_to_atom('Price',P,P1),
   !,
   seller(S,I),
   dislog_variable_get(selli_purchase_type,Type),
   dislog_dialog_variables( send, [
      'Sid' - '', 'Price' - '',
      'Number' - N, 'Type' - Type, 'Seller' - S ] ),
   assert(purchase(D,N,B,S,P)),
   assert(purchase_type(N,(Item,Type))),
   linear_list_insert(N),
   dislog_variable_set('Actual',N),
   findall( Price,
      purchase(D,_,B,_,Price),
      Prices ),
   add(Prices,Total),
   dislog_dialog_variable( send, 'Total' - Total ),
   selli_db_dml('save_no_message(file)'),
   writeln((D,N):(b:B,s:I=S):(Item=Type,P)),
   dislog_variable_get(selli_buyer_id,Bid),
   maximum(Bid,B,Bid_2),
   dislog_variable_set(selli_buyer_id,Bid_2).

selli_db_dml('insert(purchase) 2') :-
   get_new_selli_db_number(N),
   !,
   dislog_dialog_variables( get, [
      'Date' - D, 'Bid' - B,
      'Sid' - I2, 'Item' - Item, 'Price' - P2 ] ),
   selli_term_to_atom('Sid',I,I2),
   selli_price_atom_transform(P2,P1),
   selli_term_to_atom('Price',P,P1),
   selli_db_dml('insert(purchase) 2',[N,D,B,I,Item,P]).
   
selli_db_dml('insert(purchase) 2',[N,D,B,I,Item,P]) :-
   seller(S,I),
   \+ purchase_enhanced(_,_,_,I:_,Item:_:_),
   !,
   dislog_variable_get(selli_purchase_type,Type),
   dislog_dialog_variables( send, [
      'Sid' - '', 'Price' - '', 'Item' - '',
      'Number' - N, 'Type' - '-', 'Seller' - S ] ),
   assert(purchase(D,N,B,S,P)),
   assert(purchase_type(N,(Item,Type))),
   linear_list_insert(N),
   dislog_variable_set('Actual',N),
   findall( Price,
      purchase(D,_,B,_,Price),
      Prices ),
   add(Prices,Total),
   dislog_dialog_variable( send, 'Total' - Total ),
   selli_db_dml('save_no_message(file)'),
   writeln((D,N):(b:B,s:I=S):(Item=Type,P)),
   selli_add_to_purchase_report(D, N, B, I, S, Item, Type, P),
   dislog_variable_get(selli_buyer_id,Bid),
   maximum(Bid,B,Bid_2),
   dislog_variable_set(selli_buyer_id,Bid_2).
selli_db_dml('insert(purchase) 2',[_,_,_,I,Item,_]) :-
   seller(_,I),
   purchase_enhanced(_,_,_,I:_,Item:_:_),
   !,
   ddk_message([
      'Seller: ',I,', Item: ',Item,'\n',
      'has been sold already !']).
selli_db_dml('insert(purchase) 2',[_,_,_,I,_,_]) :-
   \+ seller(_,I),
   ddk_message([
      'Seller: ',I,'\n',
      'does not exist !']).


/* selli_db_dml('delete(X)') <-
      */

selli_db_dml('delete(purchase)') :-
   dislog_dialog_variables( get, [
      'Date' - D1, 'Number' - N1, 'Seller' - S1,
      'Bid' - B1, 'Price' - Price ] ),
   selli_price_atom_transform(Price,P),
   selli_term_to_atom('Price',P1,P),
   ( retract(purchase(D1,N1,B1,S1,P1))
   ; retract(purchase(D1,N1,B1,S1,P)) ),
   ( retract(purchase_type(N1,T1))
   ; true ),
   writeln(delete(purchase(D1,N1,B1,S1,P1,T1))),
   linear_list_delete(N1),
   selli_db_dml('save_no_message(file)'),
   dislog_variable_get('Actual',N2),
   purchase(D2,N2,B2,S2,P2),
   purchase_type(N2,I2,T2),
   dislog_dialog_variables( send, [
      'Date' - D2, 'Number' - N2, 'Item' - I2, 'Type' - T2,
      'Seller' - S2, 'Bid' - B2, 'Price' - P2 ] ),
   !.
selli_db_dml('delete(purchase)').


/* selli_db_dml('update(X)') <-
      */

selli_db_dml('update(purchase)') :-
   dislog_dialog_variables( get, [
      'Date' - D, 'Number' - N, 'Item' - Item, 'Type' - T,
      'Bid' - B, 'Sid' - I2, 'Price' - P2 ] ),
   selli_term_to_atom('Sid',I,I2),
   selli_price_atom_transform(P2,P1),
   selli_term_to_atom('Price',P,P1),
   seller(S,I),
   retract(purchase(_,N,_,_,_)),
   assert(purchase(D,N,B,S,P)),
   ( retract(purchase_type(N,_))
   ; true ),
   assert(purchase_type(N,(Item,T))),
   dislog_variable_set('Actual',N),
   dislog_dialog_variables( send, [
      'Date' - D, 'Seller' - S, 'Price' - P ] ),
   selli_db_dml('save_no_message(file)'),
   ddk_message([
      'Update of ',N,'\n',
      'Sid: ',I,', Item: ',Item,', Price: ',P]),
   dislog_dialog_variables( send, [
      'Sid' - '', 'Price' - '', 'Item' - '' ] ).


/* selli_sell(Type) <-
      */

selli_sell(Type) :-
   dislog_variable_set(selli_purchase_type,Type),
   selli_db_dml('insert(purchase)'),
   dislog_variable_set(selli_purchase_type,'-').


/* selli_append(Ys,Z) <-
      */

selli_append(Ys,Z) :-
   selli_append_loop([],Ys,Z).

selli_append_loop(X,[Y|Ys],Z) :-
   append(X,Y,XY),
   selli_append_loop(XY,Ys,Z).
selli_append_loop(X,[],X).


/* purchase_enhanced(
         Date,Number,Bid,Sid:Seller,Item:Type:Price) <-
      */

purchase_enhanced(Date,Number,Bid,Sid:Seller,Item:Type:Price) :-
   purchase(Date,Number,Bid,Seller,Price),
   seller(Seller,Sid),
   purchase_type(Number,Item,Type).

purchase_type(Number,Item,Type) :-
   purchase_type(Number,Item_Type),
   !,
   ( Item_Type = (Item,Type),
     !
   ; Item = '',
     Type = Item_Type ).
purchase_type(_,'','-').


/* get_new_selli_db_number(N) <-
      */

get_new_selli_db_number(N) :-
   dislog_variable_get(selli_db_number,M),
   K is M + 1,
   dislog_variable_set(selli_db_number,K),
   ( \+ purchase(_,K,_,_,_),
     N = K
   ; get_new_selli_db_number(N) ).


/******************************************************************/


