

/* container_item_to_relations(Xml, Relations) <-
      */

container_item_to_relations(Xml, Relations) :-
   fn_item_to_attributes_of_tag(Xml, 'Gebaeude', As_Gebaeude),
   fn_item_to_attributes_of_tag(Xml, 'Raum', As_Raum),
   fn_item_to_attributes_of_tag(Xml, 'Gestell', As_Gestell),
   fn_item_to_attributes_of_tag(Xml, 'Kaefig', As_Kaefig),
   findall( Tuple,
      ( Y := Xml/'Gebaeude',
        fn_item_to_attribute_values(Y, As_Gebaeude, Tuple) ),
      Gebaeude ),
   findall( [Geb|Tuple],
      ( Y := Xml/'Gebaeude'::[@id=Geb]/'Raum',
        fn_item_to_attribute_values(Y, As_Raum, Tuple) ),
      Raeume ),
   findall( [Geb, Raum|Tuple],
      ( Y := Xml/'Gebaeude'::[@id=Geb]/'Raum'::[@id=Raum]/'Gestell',
        fn_item_to_attribute_values(Y, As_Gestell, Tuple) ),
      Gestelle ),
   findall( [Geb, Raum, Gest|Tuple],
      ( Y := Xml/'Gebaeude'::[@id=Geb]/'Raum'::[@id=Raum]
           /'Gestell'::[@id=Gest]/'Kaefig',
        fn_item_to_attribute_values(Y, As_Kaefig, Tuple) ),
      Kaefige ),
   Relations = [Gebaeude, Raeume, Gestelle, Kaefige].


