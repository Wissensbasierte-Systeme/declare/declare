

/******************************************************************/
/***                                                            ***/
/***       VTRP:  Templates for Bills                           ***/
/***                                                            ***/
/******************************************************************/


:- home_directory(Home),
   concat([Home, '/research/projects/MediRP/',
      '2010/2010_12_31/'], Directory),
   dislog_variable_set(vtrp_rechnungen, Directory).


/*** tests ********************************************************/


test(vtrp, table_with_template_sequence_to_table) :-
   dislog_variable_get(vtrp_rechnungen, 'rechnung.html', File),
   dread(xml, File, [Html]),
   Table_1 := Html/descendant::table,
   vtrp_table_with_template_sequence_to_table(
      Table_1, Table_2),
   dwrite(xml, Table_2).

test(vtrp, vtrp_bill_file_transform) :-
%  dislog_variable_get(
%     vtrp_rechnungen, 'rechnung.html', File_1),
   dislog_variable_get(
      vtrp_rechnungen, 'rechnung_2011_10_12.html', File_1),
   dislog_variable_get(
      vtrp_rechnungen, 'rechnung_2.html', File_2),
   vtrp_bill_file_transform(File_1, File_2).

test(vtrp, employee) :-
   home_directory(Home),
   concat([ Home, '/teaching/Skripten/db_seipel/2010_2011/',
      'DB_und_DB_2/employee.html'], File),
   dread(xml, File, [Item_1]),
   vtrp_bill_item_transform(Item_1, Item_2),
   html_to_display(Item_2).


/*** interface ****************************************************/


/* vtrp_bill_file_transform(File_1, File_2) <-
      */

vtrp_bill_file_transform(File_1, File_2) :-
   dread(xml, File_1, [Item_1]),
   vtrp_bill_item_transform(Item_1, Item_2),
   dwrite(html, File_2, Item_2),
   html_to_display(Item_2),
   !. 


/* vtrp_bill_item_transform(Item_1, Item_2) <-
      */

vtrp_bill_item_transform(Item_1, Item_2) :-
   Options = [ predicate(vtrp_bill_item_transform_), item ],
   fn_transform(Options, Item_1, Item_3),
   fn_item_prune_lists(Item_3, Item_2).

vtrp_bill_item_transform_(X, Y) :-
   ( vtrp_mysql_statement(X, Y)
   ; vtrp_prolog_statement(X, Y)
   ; vtrp_table_for_attribute_to_value(X, Y)
   ; vtrp_template_for_barcode_to_img(X, Y)
%  ; vtrp_table_with_template_sequence_to_table(X, Y)
   ; ddk_element_with_template_sequence_to_element(X, Y)
   ; vtrp_element_with_template_sequence_to_element(X, Y)
   ; X = Y ).
%  write(user, 'transformed:\n'),
%  dwrite(xml, X),
%  dwrite(xml, Y),
%  !.


/*** implementation ***********************************************/


/* vtrp_mysql_statement(X, Y) <-
      */

vtrp_mysql_statement(X, Y) :-
   'template:mysql' := X/tag::'*',
   Create_Statement := X@statement,
   !,
   catch( odbc_query(mysql, Create_Statement, _, []),
      _, true ),
   Y = X.


/* vtrp_prolog_apply(X, Y) <-
      */

vtrp_prolog_statement(X, Y) :-
   'template:prolog' := X/tag::'*',
   Atom := X@goal,
   !,
   atom_to_term(Atom, Goal, _),
   writeln(user, Goal),
   apply(Goal, [Y]).


/* vtrp_table_for_attribute_to_value(X, Y) <-
      */

vtrp_table_for_attribute_to_value(X, Y) :-
   'template:attribute' := X/tag::'*',
   Key := X@db_key,
   !,
   Table := X@db_table,
   Attribute := X@db_attribute,
   name_split_at_position([" "], Key, Names),
   maplist( vtrp_atom_to_term,
      Names, Pairs ),
   ( foreach(A:V, Pairs), foreach(C, Cs) do
        ( atomic(V) ->
          W = V
        ; term_to_atom(V, W) ),
        concat([A, '=', W], C) ),
   concat_with_separator(Cs, ' and ', Condition),
   concat(['select ', Attribute,
      ' from ', Table, ' where ', Condition], Statement),
   odbc_query_to_tuples(mysql, Statement, Tuples),
   ( Tuples = [[Y]] ->
     true
   ; Y = '??' ).


/* vtrp_template_for_barcode_to_img(X, Y) <-
      */

vtrp_template_for_barcode_to_img(X, Y) :-
   'template:barcode' := X/tag::'*',
   Names := X/content::'*',
   concat_with_separator(Names, ' - ', String),
   vtrp_barcode(jpeg, String, File),
   Y = img:[src:File, height:100, width:80]:[].

 
/* vtrp_table_with_template_sequence_to_table(
         Table_1, Table_2) <-
      */

vtrp_table_with_template_sequence_to_table(Table_1, Table_2) :-
   Sequence := Table_1/'template:sequence',
   Header := Table_1/tr,
   !,
   vtrp_template_sequence_to_rows(Sequence, Rows),
   vtrp_rows_to_trs(Rows, Trs),
   As = [border:1, width:800, cellspacing:3, bgcolor:white],
   Table_2 = table:As:[Header|Trs],
   !.


/* vtrp_template_sequence_to_rows(Sequence, Rows) <-
      */

vtrp_template_sequence_to_rows(Sequence, Rows) :-
   vtrp_template_sequence_to_tuples(Sequence, Tuples),
   vtrp_template_sequence_to_mapping(
      Sequence, Variables_s->(Variables_t:Goals)),
   ( foreach(Variables_s, Tuples),
     foreach(Variables_t, Rows) do
        checklist( call, Goals ) ).


/* vtrp_rows_to_trs(Rows, Trs) <-
      */

vtrp_rows_to_trs(Rows, Trs) :-
   maplist( vtrp_row_to_tr,
      Rows, Trs ).

vtrp_row_to_tr(Values, Tr) :-
   ( foreach(V, Values),
     foreach(Td, Tds) do
        Td = td:[V] ),
   Tr = tr:Tds.


/* vtrp_template_sequence_to_tuples(Sequence, Tuples) <-
      */

vtrp_template_sequence_to_tuples(Sequence, Tuples) :-
   vtrp_template_sequence_to_attributes(Sequence, Pairs),
   mysql_table_attribute_pairs_to_select_from_condition(
      Pairs, Select-From),
   xml_get_optional_attribute(Sequence, db, vtrp, Database),
   ( Where := Sequence@where ->
     concat('where ', Where, Sql)
   ; xml_get_optional_attribute(Sequence, sql, '', Sql) ),
   concat([Select, ' ', From, ' ', Sql], Statement),
   mysql_use_database(mysql, Database),
   odbc_query_to_tuples(mysql, Statement, Tuples),
   !.


/* xml_get_optional_attribute(Item, Attribute, Default, Value) <-
      */

xml_get_optional_attribute(Item, Attribute, Default, Value) :-
   ( Value := Item@Attribute ->
     true
   ; Value = Default ).


/* vtrp_template_sequence_to_attributes(Sequence, Attributes) <-
      */

vtrp_template_sequence_to_attributes(Sequence, Attributes) :-
   vtrp_template_sequence_to_data_elements(Sequence, Items),
   maplist( vtrp_template_sequence_to_attribute_list,
      Items, Attribute_Lists ),
   append(Attribute_Lists, Attributes).

vtrp_template_sequence_to_attribute_list(T, [Table:Attribute]) :-
   'template:attribute' := T/tag::'*',
   Table := T@db_table,
   Attribute := T@db_attribute.
vtrp_template_sequence_to_attribute_list(T, [Table:Aggregate]) :-
   'template:aggregate' := T/tag::'*',
   Table := T@db_table,
   Atom := T@db_attribute,
   atom_to_term(Atom, Aggregate, Substitution),
   checklist( call, Substitution ).
vtrp_template_sequence_to_attribute_list(T, Attributes) :-
   'template:function' := T/tag::'*',
   Table := T@db_table,
   Atom := T@db_attribute,
   atom_to_term(Atom, Term, Substitution),
   free_variables(Term, As),
   checklist( call, Substitution ),
   ( foreach(Attribute, As),
     foreach(Table:Attribute, Attributes) do
        true ),
   !.


/* mysql_table_attribute_pairs_to_select_from_condition(
         Pairs, Select-From) <-
      */

mysql_table_attribute_pairs_to_select_from_condition(
      Pairs, Select-From) :-
   pair_lists(:, Tables_1, _, Pairs),
   ( foreach(T:A, Pairs), foreach(TA, Ps) do
        ( atom_to_term(A, Term, Substitution),
          checklist( call, Substitution ),
          Term =.. [Function, Attribute] ->
          concat([Function, '(', T, '.', Attribute, ')'], TA)
        ; concat([T, '.', A], TA) ) ),
   concat_with_separator(Ps, ', ', S),
   sort(Tables_1, Tables_2),
   concat_with_separator(Tables_2, ', ', F),
   concat('select ', S, Select),
   concat('from ', F, From).


/* vtrp_template_sequence_to_mapping(
         Xml, Variables_s->(Variables_t:Goals)) <-
      */

vtrp_template_sequence_to_mapping(
      Sequence, Variables_s->(Variables_t:Goals)) :-
   vtrp_template_sequence_to_data_elements(Sequence, Items),
   maplist( vtrp_template_sequence_to_template_and_calls,
      Items, Xs ),
   pair_lists(->, Sources, Targets, Xs),
   append(Sources, Variables_s),
   pair_lists(:, Variables_t, Goals, Targets).

vtrp_template_sequence_to_template_and_calls(
      T, [V]->(W:vtrp_convert(V, W))) :-
   'template:attribute' := T/tag::'*'.
vtrp_template_sequence_to_template_and_calls(
      T, Vs->(V:Goal)) :-
   'template:function' := T/tag::'*',
   Atom := T@db_attribute,
   atom_to_term(Atom, Term, _),
   functor(Term, Predicate, _),
   free_variables(Term, Vs),
   ( Predicate = -,
     Vs = [X, Y] ->
     Goal = vtrp_minus(X, Y, V)
   ; Goal = call(Term, V) ),
   !.
vtrp_template_sequence_to_template_and_calls(
      T, [V]->(V:true)) :-
   'template:aggregate' := T/tag::'*'.


/* vtrp_atom_to_term(Atom, Term) <-
      */

vtrp_atom_to_term(Atom, Term) :-
   atom_to_term(Atom, Term, Substitution),
   checklist( call, Substitution ).


/******************************************************************/


