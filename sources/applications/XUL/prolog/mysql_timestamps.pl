

/******************************************************************/
/***                                                            ***/
/***       MySQL:  Timestamps                                   ***/
/***                                                            ***/
/******************************************************************/


/*
   [timestamp(2010, 10, 21, 14, 32, 4, 0), '$null$']
*/


/*** interface ****************************************************/


/* mysql_timestamp_greater(T1, T2) <-
      */

mysql_timestamp_greater(T1, T2) :-
   mysql_timestamp_maximum(T1, T2, T1).


/* mysql_timestamp_minimum(T1, T2, T3) <-
      */

mysql_timestamp_minimum(T1, T2, T3) :-
   mysql_timestamp_to_days(T1, Ds1),
   mysql_timestamp_to_days(T2, Ds2),
   ( Ds1 > Ds2 ->
     T3 = T2
   ; T3 = T1 ).


/* mysql_timestamp_maximum(T1, T2, T3) <-
      */

mysql_timestamp_maximum(T1, T2, T3) :-
   mysql_timestamp_to_days(T1, Ds1),
   mysql_timestamp_to_days(T2, Ds2),
   ( Ds1 > Ds2 ->
     T3 = T1
   ; T3 = T2 ).


/* mysql_timestamp_to_days(Timestamp, Days) <-
      */

mysql_timestamp_to_days(Timestamp, Days) :-
   Timestamp =.. [timestamp|Xs],
   scalar_product([365, 30, 1, 1/24, 1/(24*60), 0, 0], Xs, Days).


/* mysql_timestamp_is_null(Timestamp) <-
      */

mysql_timestamp_is_null(Timestamp) :-
   ( Timestamp = '$null$'
   ; Timestamp = timestamp(0, 0, 0, 0, 0, 0, 0) ).


/* mysql_timestamp_to_atom(Timestamp, Atom) <-
      */

mysql_timestamp_to_atom(Timestamp, Atom) :-
   var(Timestamp),
   !,
   name_split_at_position([" ", "-", ":"], Atom, Tuple_1),
   list_remove_elements([''], Tuple_1, Tuple_2),
   length(Tuple_2, N),
   M is 7 - N,
   multify('0', M, Xs),
   append(Tuple_2, Xs, Tuple_3),
   maplist( atom_number,
      Tuple_3, Numbers ),
   Timestamp =.. [timestamp|Numbers].
mysql_timestamp_to_atom(Timestamp, Atom) :-
   Timestamp =
      timestamp(Year, Month, Day, Hours, Minutes, _, _),
   !,
   concat([Day, '.', Month, '.', Year, '  ',
      Hours, ':', Minutes], Atom).
mysql_timestamp_to_atom(Timestamp, Timestamp).


/* mysql_timestamp_difference_days(T1, T2, Days) <-
      */

mysql_timestamp_difference_days(T1, T2, Days) :-
   T1 =.. [timestamp, Y1, M1, D1|Xs1],
   T2 =.. [timestamp, Y2, M2, D2|Xs2],
   date_to_time_stamp(D1-M1-Y1, X1),
   date_to_time_stamp(D2-M2-Y2, X2),
   X is X1 - X2,
   pair_lists(-, Xs1, Xs2, Pairs),
   maplist( is,
      Xs, Pairs ),
   scalar_product([1, 1/24, 1/(24*60), 0, 0], [X|Xs], Days).
 
mysql_timestamp_difference_days_2(T1, T2, Days) :-
   mysql_timestamp_to_days(T1, Days_1),
   mysql_timestamp_to_days(T2, Days_2),
   Days is Days_1 - Days_2.


/******************************************************************/


