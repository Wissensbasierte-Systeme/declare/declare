

/******************************************************************/
/***                                                            ***/
/***       VTRP:  Templates for Bills                           ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(vtrp, vtrp_template_to_instances) :-
   dislog_variable_get(vtrp_rechnungen, 'rechnung.html', File),
   dread(xml, File, [Html]),
   Sequence := Html/descendant::'template:sequence',
   vtrp_template_to_instances(Sequence, Instances),
   dwrite(xml, p:Instances).


/*** interface ****************************************************/


/* vtrp_element_with_template_sequence_to_element(E1, E2) <-
      */

vtrp_element_with_template_sequence_to_element(E1, E2) :-
   fn_item_parse(E1, T:As:Es_1),
   maplist( vtrp_template_to_instances,
      Es_1, Ess_2 ),
   append(Ess_2, Es_2),
   E2 = T:As:Es_2.

   
/* vtrp_template_to_instances(Sequence, Instances) <-
      */

vtrp_template_to_instances(Sequence, Instances) :-
   'template:sequence' := Sequence/tag::'*',
   !,
   vtrp_template_to_template_and_substitution(
      Sequence, Template-Substitution ),
   vtrp_template_sequence_to_rows(Sequence, Rows),
   Es := Template/content::'*',
   ( foreach(Substitution, Rows), foreach(Es, Ess) do
        true ),
   append(Ess, Instances),
   !.
vtrp_template_to_instances(Xml, [Xml]).


/*** implementation ***********************************************/


/* vtrp_template_to_template_and_substitution(
         T1, T2-Substitution) <-
      */

vtrp_template_to_template_and_substitution(T, X-[X]) :-
   fn_item_parse(T, Tag:_:_),
   Tags = ['template:attribute', 'template:function'],
   member(Tag, Tags),
   !.
vtrp_template_to_template_and_substitution(T1, T2-Substitution) :-
   fn_item_parse(T1, Tag:As:Es_1),
   maplist( vtrp_template_to_template_and_substitution,
      Es_1, Pairs ),
   pair_lists(-, Es_2, Substitutions, Pairs),
   append(Substitutions, Substitution),
   T2 = Tag:As:Es_2.
vtrp_template_to_template_and_substitution(T, T-[]).


/* vtrp_template_sequence_to_data_elements(Sequence, Items) <-
      */

vtrp_template_sequence_to_data_elements(Sequence, Items) :-
   fn_item_parse(Sequence, Tag:_:Es),
   ( member(Tag, ['template:attribute', 'template:function']) ->
     Items = [Sequence]
   ; maplist( vtrp_template_sequence_to_data_elements,
        Es, Itemss ),
     append(Itemss, Items) ).
vtrp_template_sequence_to_data_elements(_, []).

/*
vtrp_template_sequence_to_data_elements(Sequence, Items) :-
   findall( Item,
      ( Item := Sequence/descendant::Tag,
        member(Tag, [
           'template:attribute', 'template:function' ]) ),
      Items ).
*/


/******************************************************************/


