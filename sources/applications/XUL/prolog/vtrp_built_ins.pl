

/******************************************************************/
/***                                                            ***/
/***       VTRP:  Built-Ins for Bills                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* vtrp_minus(X, Y, Z) <-
      */

vtrp_minus(T1, T2, A) :-
   \+ mysql_timestamp_is_null(T1),
   \+ mysql_timestamp_is_null(T2),
   mysql_timestamp_difference_days(T1, T2, A).
vtrp_minus(X, Y, Z) :-
   Z = X - Y.


/* vtrp_cost(X, Y, Cost) <-
      */

vtrp_cost([T1, T2], [I1, I2], Cost) :-
   vtrp_days([T1, T2], [I1, I2], Days),
   round(Days * 24 * 60, 1, Cost).


/* vtrp_days(T1, T2, Days) <-
      */

vtrp_days([T1, T2], [I1, I2], Days) :-
   \+ mysql_timestamp_is_null(T1),
   mysql_timestamp_is_null(T2),
   !,
   mysql_timestamp_to_atom(K2, I2),
   vtrp_days([T1, K2], [I1, I2], Days).
vtrp_days([T1, T2], [I1, I2], Days) :-
   \+ mysql_timestamp_is_null(T1),
   \+ mysql_timestamp_is_null(T2),
   !,
   mysql_timestamp_to_atom(K1, I1),
   mysql_timestamp_to_atom(K2, I2),
   ( ( mysql_timestamp_greater(K1, T2)
     ; mysql_timestamp_greater(T1, K2) ) ->
     Days = 0
   ; mysql_timestamp_maximum(T1, K1, J1),
     mysql_timestamp_minimum(T2, K2, J2),
     mysql_timestamp_difference_days(J2, J1, X),
     round(X, 5, Days) ).
vtrp_days(_, _, 0).


/* vtrp_convert(V, W) <-
      */

vtrp_convert(V, W) :-
   ( functor(V, timestamp, _) ->
     mysql_timestamp_to_atom(V, W)
   ; W = V ).


/* vtrp_barcode(jpeg, String, File) <-
      */

vtrp_barcode(jpeg, String, File) :-
   home_directory(Home),
   concat([Home, '/', String, '.jpg'], File).


/******************************************************************/


