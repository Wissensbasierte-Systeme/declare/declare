

/******************************************************************/
/***                                                            ***/
/***       DDK:  Templates                                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* ddk_element_with_template_sequence_to_element(E1, E2) <-
      */

ddk_element_with_template_sequence_to_element(E1, E2) :-
   _ := E1/descendant::'template:sequence'::[@prolog=_],
   fn_item_parse(E1, T:As:Es_1),
   maplist( ddk_template_sequence_to_instances,
      Es_1, Ess_2 ),
   append(Ess_2, Es_2),
   E2 = T:As:Es_2.


/*** implementation ***********************************************/


/* ddk_template_sequence_to_instances(Sequence, Instances) <-
      */

ddk_template_sequence_to_instances(Sequence, Instances) :-
   'template:sequence' := Sequence/tag::'*',
   _ := Sequence@prolog,
   ddk_template_sequence_to_substitutions(
      Sequence, Substitutions),
   ddk_template_to_template_and_substitution(
      Sequence, Template-Substitution),
   C := Template/content::'*',
   ( foreach(S, Substitutions), foreach(C, Cs) do
        ddk_template_substitution_to_instance(
           Substitution, S) ),
   append(Cs, Instances).
ddk_template_sequence_to_instances(Sequence, [Sequence]).

ddk_template_substitution_to_instance(Substitution, S) :-
   foreach(A=V, Substitution) do
      member(A=V, S).


/* ddk_template_sequence_to_substitutions(
         Sequence, Substitutions) <-
      */

ddk_template_sequence_to_substitutions(
      Sequence, Substitutions) :-
   Atom := Sequence@prolog,
   atom_to_term(Atom, Goal, Substitution),
   findall( Substitution,
      Goal,
      Substitutions ).


/* ddk_template_to_template_and_substitution(
         T, Term-Substitution) <-
      */

ddk_template_to_template_and_substitution(
      T, Term-Substitution) :-
   fn_item_parse(T, Tag:_:_),
   Tags = ['template:value'],
   member(Tag, Tags),
   !,
   Atom := T@prolog,
   atom_to_term(Atom, Term, Substitution).
ddk_template_to_template_and_substitution(T1, T2-Substitution) :-
   fn_item_parse(T1, Tag:As:Es_1),
   maplist( ddk_template_to_template_and_substitution,
      Es_1, Pairs ),
   pair_lists(-, Es_2, Substitutions, Pairs),
   append(Substitutions, Substitution),
   T2 = Tag:As:Es_2.
ddk_template_to_template_and_substitution(T, T-[]).


/******************************************************************/


