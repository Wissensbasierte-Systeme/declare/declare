

/******************************************************************/
/***                                                            ***/
/***       VTRP:  Bills                                         ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(vtrp, animals_in_time_interval) :-
   Interval = ['2010-12-31 14:00', '2010-12-31 17:00'],
   vtrp_animals_in_time_interval(Interval).


/*** interface ****************************************************/


/* vtrp_animals_in_time_interval(Interval) <-
      */

vtrp_animals_in_time_interval(Interval) :-
   vtrp_bill_table(Interval, Table),
   City = 'Bonn',
   date(date(Year, Month, Day)),
   concat([Day, '.', Month, '.', Year], Date),
   vtrp_bill_header(Header),
   vtrp_bill_city_and_date(City, Date, City_and_Date),
   vtrp_bill_footer(Footer),
   Body = body:[
      Header, hr:[],
      City_and_Date,
      h2:['Rechnung'],
      'Wir haben folgende Tiere betreut:', br:[],
      blockquote:[Table],
      'Daf&uuml;r stellen wir Ihnen 50 Euro in Rechnung.', p:[],
      'Mit freundlichen Gr&uuml;&szlig;en',
      br:[], br:[], hr:[],
      Footer ],
   Html = html:[head:[title:[test]], Body],
   dislog_variable_get(
      output_path, 'vtrp_bill.html', File_html),
   dislog_variable_get(
      output_path, 'vtrp_bill.ps', File_ps),
   dwrite(html, File_html, Html),
   concat(['html2ps ', File_html, ' > ', File_ps], Command),
   us(Command),
   html_to_display(Html),
   !.


/* vtrp_bill_header(Header) <-
      */

vtrp_bill_header(Header) :-
   As = [border:0, width:700, cellspacing:0, bgcolor:white],
   Row = tr:[
      td:[align:left]:[
         'Anschrift', br:[],
         'Stra&szlig;e', br:[],
         '...' ], td:['&nbsp;'],
      td:[align:left]:[
         'Institut f&uuml;r Molekulare Biomedizin', br:[],
         'Stra&szlig;e', br:[],
         '... Bonn', br:[],
         'Tel.:', br:[],
         'Fax.:', br:[],
         'Email:', br:[],
         'WWW:' ] ],
   Header = table:As:[Row].


/* vtrp_bill_footer(Footer) <-
      */

vtrp_bill_footer(Footer) :-
   As = [border:0, width:700, cellspacing:0, bgcolor:white],
   Row = tr:[
      td:[align:left]:[
         'Name', br:[],
         'Stra&szlig;e', br:[],
         'Ort' ],
      td:[align:left]:[
         'Tel.: ...', br:[],
         'Fax.: ...', br:[],
         'Email.: ...' ],
      td:[align:left]:[
         'Konto: ...', br:[],
         'BLZ: ...', br:[],
         'Bank ...' ] ],
   Footer = table:As:[Row].


/* vtrp_bill_city_and_date(City, Date, City_and_Date) <-
      */

vtrp_bill_city_and_date(City, Date, City_and_Date) :-
   As = [border:0, width:700, cellspacing:0, bgcolor:white],
   concat([City, ', den ', Date], C_a_D),
   Row = tr:[
      td:[align:left]:['&nbsp;'],
      td:[align:left]:['&nbsp;'],
      td:[align:left]:[C_a_D]],
   City_and_Date = table:As:[Row].


/* vtrp_bill_table(Interval, Table) <-
      */

vtrp_bill_table(Interval, Table) :-
   vtrp_animals_in_time_interval(Interval, Tuples),
   ( foreach(Tuple, Tuples), foreach(Row, Rows) do
        nth_multiple([1,3,4], Tuple, [Id, In_1, Out_1]),
        mysql_timestamp_to_atom(In_1, In_2),
        mysql_timestamp_to_atom(Out_1, Out_2),
        Row = tr:[
           td:[Id], td:[In_2], td:[Out_2],
           td:['2 Euro'] ] ),
   As = [border:1, width:500, cellspacing:3, bgcolor:white],
   As_th = ['BGCOLOR':'#dfdfff'],
   R = tr:[
      th:As_th:['ID'], th:As_th:['Zugang'], th:As_th:['Abgang'],
      th:As_th:['Kosten'] ],
   Table = table:As:[R|Rows].


/* vtrp_animals_in_time_interval([T1,T2], Tuples) <-
      */

vtrp_animals_in_time_interval([T1,T2], Tuples) :-
   concat([ 'select * from Tier where Zugang > ''',
      T1, ''' and Abgang < ''', T2, '''' ], Statement),
   writeln(user, Statement),
   odbc_query_to_tuples(mysql, Statement, Tuples).


/* html_nbsp(N, Space) <-
      */

html_nbsp(N, Space) :-
   multify('&nbsp;', N, Spaces),
   concat(Spaces, Space).

 
/******************************************************************/


