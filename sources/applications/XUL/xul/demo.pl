

xul_demo(Window) :-
   Test = [ [hello, world, one],
      [hallo, welt, zwei], [hello, universe, three] ],

   xul_common_item(treecol, [id:name, label:'Filename', flex:1], [], Col1),
   xul_common_item(treecol, [id:location, label:'Location', flex:2], [], Col2),
   xul_common_item(treecol, [id:size, label:'Size', flex:1], [], Col3),
   xul_common_item(treecols, [], [Col1, Col2, Col3], Cols),

   ( foreach(T1, Test), foreach(T2, Ts_2) do
        ( foreach(X1, T1), foreach(X2, Xs_2) do
             xul_common_item(treecell, [label:X1], [], X2) ),
        xul_common_item(treerow, [], [Xs_2], Row),
        xul_common_item(treeitem, [], [Row], T2) ),

   xul_common_item(treechildren, [], Ts_2, TreeChildren),
   xul_common_item(tree, [flex:1], [Cols, TreeChildren], Tree), 
   xul_common_item(button, [label:change, oncommand:'requestFromPL(\'send\')'], [], Button1),
   xul_common_item(spacer, [flex:1], [], Spacer),
   xul_common_item(button, [label:open, oncommand:'filePicker()'], [], Button2),
   xul_common_item(spacer, [flex:1], [], Spacer),
   xul_common_item(button, [label:close, oncommand:'window.close()'], [], Button3),
   xul_common_item(hbox, [], [Spacer, Button1, Button2, Button3], Hbox),
   xul_common_item(vbox, [flex:1], [Tree, Hbox], Vbox),

   xul_window(demo, 'Demo Window', [persist:'400 300'] , Vbox, Window).


xul_demo_change(Tree) :-
   Test = [ [na, world, one],
      [du, welt, zwei], [da, universe, three] ],
   xul_common_item(treecol, [id:name, label:'Filename', flex:1], [], Col1),
   xul_common_item(treecol, [id:location, label:'Location', flex:2], [], Col2),
   xul_common_item(treecol, [id:size, label:'Size', flex:1], [], Col3),
   xul_common_item(treecols, [], [Col1, Col2, Col3], Cols),
   ( foreach(T1, Test), foreach(T2, Ts_2) do
        ( foreach(X1, T1), foreach(X2, Xs_2) do
             xul_common_item(treecell, [label:X1], [], X2) ),
        xul_common_item(treerow, [], [Xs_2], Row),
        xul_common_item(treeitem, [], [Row], T2) ),
   xul_common_item(treechildren, [], Ts_2, TreeChildren),
   xul_common_item(tree, [flex:1], [Cols, TreeChildren], Tree).


