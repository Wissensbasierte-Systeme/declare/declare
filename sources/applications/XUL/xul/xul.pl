

/******************************************************************/
/***                                                            ***/
/***       Word Alignment                                       ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/

      

/*** implementation ***********************************************/


/* xul_window(+ID, +Title, +Properties, -Windows) <-
      */

xul_window(ID, Title, Properties, Content, Window) :-
   As = [ id:ID,
      title:Title,
      orient:horizontal,
      xmlns:'http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul' ],
   append(As, Properties, As1),
   Content1 = [script:[src:'xulfunctions.js']:[]],
   append(Content1, [Content], Content2),
   Window = window:As1:Content2.

xul_item_add_properties(T:As:Es, Properties, Item) :-
   append(As, Properties, As1),
   Item = T:As1:Es.

xul_item_add_content(T:As:Es, Content, Item) :-
   append(Es, Content, Es1),
   Item = T:As:Es1.

xul_common_item(Type, Properties, Content, Item) :-
   Item = Type:Properties:Content.
xul_common_item(Type, Properties, Content, Toolbox, Parent, Parent1) :-
   xul_common_item(Type, Properties, Content, Toolbox),
   Parent = T:As:Es,
   append(Es, Toolbox, Es1),
   Parent1 = T:As:Es1.

xul_toolbox(Properties, Content, Toolbox) :-
   xul_common_item(toolbox, Properties, Content, Toolbox).
xul_toolbox(Properties, Content, Toolbox, Parent, Parent1) :-
   xul_common_item(toolbox, Properties, Content, Toolbox, Parent, Parent1).

xul_vbox(Properties, Content, Vbox) :-
   xul_common_item(vbox, Properties, Content, Vbox).
xul_vbox(Properties, Content, Vbox, Parent, Parent1) :-
   xul_common_item(vbox, Properties, Content, Vbox, Parent, Parent1).


/* dwrite(xul, File, FN_Term) <-
      */
   
dwrite(xul, File, FN_Term) :-
   fn_term_to_xul_file(FN_Term, File).


/* fn_term_to_xul_file(FN_Term, File) <-
      */

fn_term_to_xul_file(FN_Term, File) :-
   predicate_to_file( File,
   ( writeln('<?xml version="1.0" encoding="UTF-8" ?>'),
     writeln('<?xml-stylesheet href="chrome://global/skin/" type="text/css"?>'),
     writeln('<?xml-stylesheet href="customstyle.css" type="text/css"?>'),
     nl,
     field_notation_to_xml(FN_Term) ) ).


/******************************************************************/


