var httpGetLatestEntries = createXHR();
var clientID = '1';

function createXHR() {
   if (typeof XMLHttpRequest != "undefined") {
      return new XMLHttpRequest();
   } else {
      throw new Error("No XHR object available. Please use Firefox!");
   }
}

function requestFromPL(goal) {
   var xhr = createXHR();
   xhr.onreadystatechange = function() {
      if (xhr.readyState == 4) {
         if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
            var tree = document.getElementById("mytree");
            var p = tree.parentNode;
            var eldoc = xhr.responseXML.documentElement.childNodes[0];
            p.replaceChild(eldoc, tree);
         } else {
            alert("Request was unsuccessful: " + xhr.status);
         }
      }
   };
   xhr.open("post", "http://localhost:6666", true);
   xhr.setRequestHeader('Content-Type', 'text/xml');
   xhr.send("<message><goal>"+goal+"</goal></message>");
}

function requestFromPL(goal, id) {
   var xhr = createXHR();
   xhr.onreadystatechange = function() {
      if (xhr.readyState == 4) {
         if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
            var node = document.getElementById(id);
            var parentNode = node.parentNode;
            var elementDoc = xhr.responseXML.documentElement.childNodes[0];
            parentNode.replaceChild(elementDoc, node);
         } else {
            alert("Request was unsuccessful: " + xhr.status);
         }
      }
   };
   xhr.open("post", "http://localhost:6666", true);
   xhr.setRequestHeader('Content-Type', 'text/xml');
   xhr.send("<message><goal>"+goal+"</goal></message>");
}

function sendToPL() {
   var xhr = createXHR();
   xhr.onreadystatechange = function() {
      if (xhr.readyState == 4) {
         if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
         } else {
            alert("Request was unsuccessful: " + xhr.status);
         }
      }
   };
   goal = sendToPL.arguments[0];
   var ids = "";
   var items = sendToPL.arguments.length

   var xmlRequest = "<message>"
   var goal = sendToPL.arguments[0];
   xmlRequest += "<goal>"+goal+"</goal>";
   for (i = 1;i < items; i++) {
      var id = sendToPL.arguments[i];
      var content = document.getElementById(id).value;
      xmlRequest += "<parameter>"+content+"</parameter>";
   }
   xmlRequest += "</message>";
   xhr.open("post", "http://localhost:6666", true);
   xhr.setRequestHeader('Content-Type', 'text/xml');
   xhr.send(xmlRequest);
}

function sendRequestPL() {
   var items = sendRequestPL.arguments.length
   var xhr = createXHR();
   var rid = sendRequestPL.arguments[items - 1];
   xhr.onreadystatechange = function() {
      if (xhr.readyState == 4) {
         if ((xhr.status >= 200 && xhr.status < 300) || xhr.status == 304) {
            var node = document.getElementById(rid);
            var parentNode = node.parentNode;
            var elementDoc = xhr.responseXML.documentElement.childNodes[0];
            parentNode.replaceChild(elementDoc, node);
         } else {
            alert("Request was unsuccessful: " + xhr.status);
         }
      }
   };
   var xmlRequest = "<message>"
   var goal = sendRequestPL.arguments[0];
   xmlRequest += "<goal>"+goal+"</goal>";
   for (i = 1;i < items -1; i++) {
      var id = sendRequestPL.arguments[i];
      var content = document.getElementById(id).value;
      xmlRequest += "<parameter>"+content+"</parameter>";
   }
   xmlRequest += "</message>";
   xhr.open("post", "http://localhost:6666", true);
   xhr.setRequestHeader('Content-Type', 'text/xml');
   xhr.send(xmlRequest);
}


function initLatestEntries() {
   getLatestEntries();
}

function createLatestEntriesReqObject() {
   var httpGetLatestEntries = createXHR();
}

function getLatestEntries() {
   //Funktion, mit der Anfragen nach neuen Einträgen gesendet wird
   //Anfrage senden, wenn Status der letzten Anfrage "completed" ist, bzw. "nicht
   //initialisiert" (d.h. erster Aufruf)
   if (httpGetLatestEntries.readyState == 4 || httpGetLatestEntries.readyState
      == 0) {
      //Übergabe der latestID
      httpGetLatestEntries.open("POST","http://localhost:6666", true);
      httpGetLatestEntries.onreadystatechange = handleHttpGetLatestEntries;
      httpGetLatestEntries.setRequestHeader('Content-Type', 'text/xml');
      httpGetLatestEntries.send("<message><goal>handle_loop_request</goal>"
            +"<parameter>"+clientID+"</parameter></message>");
   }
}

function handleHttpGetLatestEntries() {
	var sendData = false;
	var nodeID = null;
	var node = null;
   //wenn Anfrage den Status "completed" hat
   if (httpGetLatestEntries.readyState == 4) {
      //ermitteln der Antwort
      response = httpGetLatestEntries.responseXML.documentElement;
      //ermitteln der Messages; Überführung in ein Array
      messages = response.getElementsByTagName('message');
      //wenn es mindestens eine neue Nachricht hat, dann wird diese angezeigt!
      if(messages.length > 0) {
         for (var i=messages.length-1; i>=0; i--) {
            var mtype = messages[i].attributes.getNamedItem('type').value;
		  		if(mtype == 'send') {
               nodeID = messages[i].getElementsByTagName('node')[0]
						.attributes.getNamedItem('id').value;
               var nodeValue = messages[i].getElementsByTagName('node')[0]
						.firstChild;
               node = document.getElementById(nodeID);
               var parentNode = node.parentNode;
               parentNode.replaceChild(nodeValue, node);
			   }
				if(mtype == 'get') {
               var nodeID = messages[i].getElementsByTagName('node')[0]
						.attributes.getNamedItem('id').value;
               var node = document.getElementById(nodeID);
					sendData = true;
				}
         }
      }
		if(sendData == true) {
         httpGetLatestEntries.open("POST","http://localhost:6666", true);
         httpGetLatestEntries.setRequestHeader('Content-Type', 'text/xml');
      //   httpGetLatestEntries.send(null);
		//  XMLDocument xulNode = new XMLDocument();
		   var xulNode = new XMLSerializer().serializeToString(node);
	//		xulNode.addContent(node);
			httpGetLatestEntries.send("<message><goal>get_from_client</goal>"
            +"<parameter>"+clientID+"</parameter>"
            +"<parameter>"+nodeID+"</parameter>"
            +"<node>"+xulNode+"</node>"
   	   	+"</message>");
      
		}
		setTimeout('getLatestEntries();', 1000);
   }
}

function filePicker() {
   var nsIFilePicker = Components.interfaces.nsIFilePicker;
   var fileChooser =  Components.classes["@mozilla.org/filepicker;1"].
      createInstance(nsIFilePicker);
   fileChooser.init(window, "Select a Location to save",
      nsIFilePicker.modeSave);
   var fileBox = fileChooser.show();
}
