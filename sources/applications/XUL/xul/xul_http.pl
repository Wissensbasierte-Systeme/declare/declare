

/******************************************************************/
/***                                                            ***/
/***       XUL Server                                           ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library('http/thread_httpd')).
:- use_module(library('http/http_dispatch')).
:- use_module(library('http/html_write')).
:- use_module(library('http/http_parameters')).
:- use_module(library('http/http_client')).
:- use_module(library('http/http_mime_plugin')).
:- use_module(library('http/http_log')).
:- use_module(library(thread_pool)).


:- thread_pool_create(http_pool, 3, [
      local(20000), global(100000), trail(50000), backlog(5) ]).


:- dynamic(message/1).
:- dynamic(xul_node/3).


/*** interface ****************************************************/


start_server :-
   server(6666).

stop_server :-
   http_stop_server(6666, []).


/*** implementation ***********************************************/


server(Port) :-
   http_server(http_dispatch, [port(Port)]).

handle(Request) :-
   member(method(post), Request),
   post_xml_to_fn(Request, FN),
   format('Content-type: text/xml; charset=utf-8~n~n'),
   format('<temp xmlns="http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul">'),
   apply_goal_from_fn(FN),
   format('</temp>').

handle_loop_request(ClientID) :-
   forall( ( message(Y),
      xml_string_to_fn(Y, [Y_1]),
      ClientID := Y_1@clientID,
      writeln(Y) ),
      ( format(Y),
        retract(message(Y)) ) ).

post_xml_to_fn(Request, FN) :-
   http_read_data(Request, Data, []),
   xml_string_to_fn(Data, FN).

xml_string_to_fn(XML, FN) :-
   new_memory_file(Handle),
   open_memory_file(Handle, write, PostStream),
   write(PostStream, XML),
   close(PostStream),
   open_memory_file(Handle, read, PostStream1),
   Options = [dialect(xml)],
   load_structure(PostStream1, Content, Options),
   xml_swi_to_fn(Content, FN),
   close(PostStream1).

apply_goal_from_fn([FN]) :-
   [X] := FN/descendant_or_self::'goal'/content::'*',
   term_to_atom(X1, X),
   findall( Y,
      [Y] := FN/descendant_or_self::'parameter'/content::'*',
      Ys ),
   findall( Z,
      [Z] := FN/descendant_or_self::'node'/content::'*',
      Zs ),
   append(Ys, Zs, Ys_1),
   apply(X1, Ys_1).

get_from_client(ClientID, NodeID, XUL) :-
   asserta(xul_node(ClientID, NodeID, XUL)).

create_message(send, ClientID, NodeID, XUL) :-
   M = message:[type:send, clientID:ClientID]:[
      node:[id:NodeID]:[XUL] ],
   fn_term_to_xml_string(M, M1),
   asserta(message(M1)).

create_message(get, ClientID, NodeID, XUL) :-
   M = message:[type:get, clientID:ClientID]:[
      node:[id:NodeID]:[] ],
   fn_term_to_xml_string(M, M1),
   asserta(message(M1)),
   xul_node_from_client(ClientID, NodeID, XUL).

xul_node_from_client(ClientID, NodeID, XUL) :-   
   ( xul_node(ClientID, NodeID, XUL_1) -> 
     !,
     retractall(xul_node(ClientID, NodeID, XUL_1)),
     XUL_1 = T:[_|As]:Es,
     XUL = T:As:Es
   ; wait_seconds(1),
     xul_node_from_client(ClientID, NodeID, XUL) ).


/*** for browser testing ******************************************/

:- http_handler('/', handle, [spawn(http_pool)]).
:- http_handler('/test/demo3.xul',
      http_reply_file( 'demo/chrome/content/demo3.xul', 
      [mime_type('application/vnd.mozilla.xul+xml')]), [] ).
:- http_handler( '/test/xulfunctions.js',
      http_reply_file('demo/chrome/content/xulfunctions.js', []), [] ).

create_message(test1) :-
   Message ='<message type="send" clientID="1"><node id="Total"><textbox id="Total" flex="1" value="55"/></node></message>',
   asserta(message(Message)).

create_message(test2) :-
   Message ='<message type="send" clientID="1"><nodeID>Sid</nodeID><node><textbox id="Sid" flex="1" value="33"/></node></message>',
   asserta(message(Message)).

create_message(test3) :-
   Message ='<message type="get" clientID="1"><node id="Total" /></message>',
   asserta(message(Message)).


/********************************************************************/


