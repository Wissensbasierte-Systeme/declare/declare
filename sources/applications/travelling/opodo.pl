

/******************************************************************/
/***                                                            ***/
/***          Travelling:  Data Extraction from Opodo           ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      displayAvail/17.


/*** interface ****************************************************/


/* opodo_display_table <-
      */

opodo_display_table :-
   dislog_variable_get(example_path, Examples),
   concat(Examples, 'xml/opodo.txt', File),
   opodo_file_to_tuples(File, Tuples),
   writeln_list(user, Tuples),
   multify(' ', 17, Header_Tuple),
   html_display_table('Opodo', Header_Tuple, Tuples).


/* opodo_file_to_tuples(File, Tuples) <-
      */

opodo_file_to_tuples(File, Tuples) :-
   File_Prolog = 'results/opodo_tmp.pl',
   read_file_to_string(File, List_1),
   list_start_after_position(["displayAvail"], List_1, List_2),
   list_cut_at_position(["</script>"], List_2, List_3),
   list_exchange_elements([";."], List_3, List_4),
   append("displayAvail", List_4, List_5),
   name(Text, List_5),
   predicate_to_file( File_Prolog,
      write(Text) ),
   consult(File_Prolog),
   collect_arguments_generic(displayAvail/17, Tuples_2),
   maplist( opodo_tuple_to_tuple,
      Tuples_2, Tuples ).

opodo_tuple_to_tuple(Tuple_1, Tuple_2) :-
   maplist( opodo_entry_to_entry,
      Tuple_1, Tuple_2 ).

opodo_entry_to_entry([X], Y) :-
   is_list(X),
   !,
   name(Y, X).
opodo_entry_to_entry(X, Y) :-
   is_list(X),
   !,
   name(Y, X).
opodo_entry_to_entry(X, X).


/******************************************************************/


