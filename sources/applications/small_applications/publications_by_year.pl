

/* publications_by_year(Html, Pairs) <-
      */

publications_by_year(File) :-
   dread(html, File, [Html]),
   publications_by_year(Html, Pairs),
   xpce_display_table(['Year', 'Publications'], Pairs).

publications_by_year(Html, Pairs) :-
   ddbase_aggregate( [Year, length(Publication)],
      ( Publication := Html/descendant::li,
        publication_to_year(Publication, Year) ),
      Pairs ).


/* publication_to_year(Publication, Year) <-
      */

publication_to_year(Publication, Year) :-
   fn_item_parse(Publication, li:_:Es),
   term_to_atom(Es, Name_1),
   name_cut_at_position(["li:["], Name_1, Name_2),
   !,
   between(1980, 2020, Year),
   name_contains_name(Name_2, Year),
   write(user, '.'), ttyflush,
   !.


/* flatten_list_item(X, Ys) <-
      */

flatten_list_item(X, [Y|Ys]) :-
   Z := X/li,
   !,
   Y := X<->[/li],
   flatten_list_item(Z, Ys).
flatten_list_item(X, [X]).


