
my_dialog :-
   new(Dialog,dialog('Demo Dialog')),
   send(Dialog,append,button(quit,
      message(Dialog,destroy))),
   send(Dialog,append,button(make,
      message(@prolog,make))),
   send(Dialog,append,
      new(Picture,picture),below),
   send(Dialog,open),
   multiple_job(Picture,'',yellow,0,[1,2,3,4]),
   send(Picture,display,line(300),point(25,40)),
   send(Picture,background,colour('#eee')),
   multiple_job(Picture,1,red,1,[1,3]),
   multiple_job(Picture,2,blue,2,[1,2,4]),
   multiple_job(Picture,'',green,3,[2,4]).

tell_about_job(Job) :-
   job_assignment(Row,Maschine,Job),
   write_list(['Job ',Row,' on Maschine ',Maschine,' --> ']),
   tell_about_job(Job,Left-Right),
   writeln(Job-Left-Right).

tell_about_job(Job,Left-Right-Center_Y) :-
   get(Job,center_y,Center_Y),
   get(Job,left_side,Left),
   get(Job,right_side,Right).


maschine(1,25-75).
maschine(2,100-150).
maschine(3,175-200).
maschine(4,225-275).

row(0,10).
row(1,50).
row(2,75).
row(3,100).


multiple_job(Picture,Label,Colour,Row,Maschines) :-
   checklist( single_job(Picture,Label,Colour,Row),
      Maschines ).

single_job(Picture,Label,Colour,Row,Maschine) :-
   maschine(Maschine,Start-End),
   row(Row,Y),
   Width is End - Start,
   send_box(Picture,Start-Y,Width-20,Label,Colour,Device),
   assert(
      job_assignment(Row,Maschine,Device) ).

send_box(Picture,X-Y,B-H,Label,Colour,Device) :-
   U is X + B / 2,
   V is Y + H / 8,
   send(Picture,display,
      new(Device,device)),
   send(Device,display,
      new(Box,box(B,H)),point(X,Y)),
%  send(Box,colour,Colour),
%  send(Box,fill_pattern,@grey25_image),
   send(Box,fill_pattern,colour(Colour)),
   send(Device,display,
      new(Text,text(Label)),point(U,V)),
   send(Text,font,font(times,bold,14)),
   send(Device,recogniser,new(move_gesture)),
   send(Device,recogniser,
      click_gesture(left, '', double,
         message(@prolog,tell_about_job,Device))).


colour(white).
colour(red).
colour(green).
colour(blue).
colour(black).

append_colour(M,C) :-
   new(Img,pixmap(@nil,white,black,32,16)),
   send(Img,fill,colour(C)),
   send(M,append,menu_item(colour(C),label := Img)).

edit_graphical(Gr) :-
   new(Dialog,dialog(string('Edit graphical %s',Gr?name))),
   send(Dialog,append,new(Menu,menu(colour,choice,
      message(Gr,fill_pattern,@arg1)))),
   send(Menu,layout,horizontal),
   forall( colour(C),
      append_colour(Menu,C) ),
   send(Menu,default,Gr?fill_pattern),
   send(Dialog,append,slider(pen,0,10,Gr?pen,
      message(Gr,pen,@arg1))),
   send(Dialog,append,button(apply),below),
   send(Dialog,append,button(restore)),
   send(Dialog,append,button(quit,
      message(Dialog,destroy))),
   send(Dialog,default_button,apply),
   send(Dialog,open).


attribute_demo :-
   new(Dialog,dialog('Attribute Demo')),
   send(Dialog,append,button(quit,
      message(Dialog,destroy))),
   send(Dialog,append,
      new(Picture,picture),below),
   send(Picture,display,
      new(Box,box(100,100)),point(20,20)),
   get(Box,right_side,Name), writeln(Box-Name),
   send(Box,right_side,200),
   send(Box,height,200),
   send(Picture,display,
      new(Ellipse,ellipse(100,50)),point(150,20)),
   send_list([Box,Ellipse],fill_pattern,colour(white)),
   new(Click,click_gesture(left,'',double,
      message(@prolog,edit_graphical,@receiver))),
   send(Box,recogniser,Click),
   send(Box,recogniser,new(move_gesture)),
   send(Ellipse,recogniser,Click),
   send(Ellipse,recogniser,new(move_gesture)),
   send(Dialog,open).


