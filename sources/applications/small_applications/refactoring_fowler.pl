

/******************************************************************/
/***                                                            ***/
/***          Refactoring:  Example                             ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(refactoring:rental,1) :-
   test_calls( [
      customer_rental(_,Rental),
      _ := Rental^movie,
      _ := Rental^movie^title,
      _ := Rental^movie^price_code,
      _ := Rental^days_rented ] ).

test(refactoring:rental,2) :-
   test_call(person_to_rentals_report('Cameron',_,_)).

test(refactoring:rental,3) :-
   test_call(
      rentals_to_rentals_report(
         [ [ movie:[
                title:'Something about Mary',
                price_code:new_release ],
             days_rented:4 ],
           [ movie:[
                title:'Bonanza',
                price_code:regular,
                length:2 ],
             days_rented:7 ] ], 21.5, 3 ) ).

test(refactoring:rental,4) :-
   test_call( (
      Rentals =
         [ 1:[ m:[ t:t1, p:p1 ], d:4 ],
           2:[ m:[ t:t2, p:p2, l:2 ], d:7 ] ],
      findall( Number:Title,
         Title := Rentals^Number^m^t,
         Titles_1 ),
      writeln_list(Titles_1),
      star_line,
      findall( V:Title,
         Title := Rentals^V,
         Titles_2 ),
      writeln_list(Titles_2) ) ).


/*** database *****************************************************/


customer([name:Person,rentals:Rentals]) :-
   findall( Rental,
      customer_rental(Person,Rental),
      Rentals ).

customer_rental( 'Cameron',
   [ movie: [
        title: 'Something about Mary',
        price_code: new_release ],
     days_rented: 4 ] ).
customer_rental( 'Cameron',
   [ movie: [
        title: 'Bonanza',
        price_code: regular,
        length: 2 ],
     days_rented: 7 ] ).
customer_rental( 'Bill',
   [ movie: [
        title: 'Walt Disney',
        price_code: childrens ],
     days_rented: 5 ] ).


/*** interface ****************************************************/


person_to_rentals_report(Person,Cost,Points) :-
   customer([name:Person,rentals:Rentals]),
   rentals_to_rentals_report(Rentals,Cost,Points).

rentals_to_rentals_report(Rentals,Cost,Points) :-
   checklist( rental_to_report,
      Rentals ),
   sumlist( rental_to_cost,
      Rentals, Cost ),
   sumlist( rental_to_frequent_renter_points,
      Rentals, Points ),
   write_list(['Amount owed is ',Cost,' USD.']), nl,
   write_list(['You earned ',Points,' frequent renter points.']), nl.


rental_to_report(Rental) :-
   Title := Rental^movie^title,
   rental_to_cost(Rental,Cost),
   writeln([title:Title,cost:Cost]).


rental_to_frequent_renter_points(Rental,2) :-
   new_release := Rental^movie^price_code,
   Days := Rental^days_rented,
   Days > 1, !.
rental_to_frequent_renter_points(_,1).


rental_to_cost(Rental,Cost) :-
   Price_Code := Rental^movie^price_code,
   Days := Rental^days_rented,
   cost_information(Price_Code,
      Base_Price,Variable_Price,Overtime_Limit),
   cost_add_for_overtime(Days,Overtime_Limit,
      Variable_Price,Base_Price,Cost).

cost_add_for_overtime(Days,Limit,Factor,Cost_1,Cost_2) :-
   Days > Limit, !,
   Cost_2 is Cost_1 + (Days - Limit) * Factor.
cost_add_for_overtime(_,_,_,Cost,Cost).
   

/* cost_information(
      Price_Code,
      Base_Price, Variable_Price,
      Overtime_Limit) <-                       */

cost_information( regular, 2, 1.5, 2 ).
cost_information( childrens, 1.5, 1.5, 3 ).
cost_information( new_release, 0, 3, 0 ).


/*** elementary predicates ****************************************/


/*
?- update(
      [ movie: [
           title: 'Something about Mary',
           price_code: new_release ],
        days_rented: 4 ]^movie^price_code,
      children,
      Result ).

Result =
   [ movie:[
        title: 'Something about Mary',
        price_code: children ],
     days_rented: 4 ] 

Yes
*/

update(Object_1^Y^Z,V,Object_2) :-
   Object_3 := Object_1^Y,
   update(Object_3^Z,V,Object_4),
   new_substitute([Y:_,Y:Object_4],Object_1,Object_2).
update(Object_1^Y,V,Object_2) :-
   new_substitute([Y:_,Y:V],Object_1,Object_2).
   
new_substitute([X1:Y1,X2:Y2],[X1:Y1|XYs1],[X2:Y2|XYs2]) :-
   !,
   new_substitute([X1:Y1,X2:Y2],XYs1,XYs2).
new_substitute([X1:Y1,X2:Y2],[X:Y|XYs1],[X:Y|XYs2]) :-
   new_substitute([X1:Y1,X2:Y2],XYs1,XYs2).
new_substitute(_,[],[]).


/*
update(A^Y^Z,B,U) :-
   V := U,
   W := A^Y,
   update(W^Z,C,V),
   substitute([Y:_,Y:C],A,B).
update(A^Y,B,U) :-
   V := U,
   substitute([Y:_,Y:V],A,B).
*/


/* map(Xs,Object_1,Object_2) <-
      */

map([X|Xs],Object_1,Object_2) :-
   member(X:Object_3,Object_1),
   map(Xs,Object_3,Object_2).
map([],Object,Object).


/* sumlist(Predicate,Objects,Sum) <-
      */

sumlist(Predicate,Objects,Sum) :-
   maplist( Predicate, 
      Objects, Xs ),
   add(Xs,Sum).


/******************************************************************/


