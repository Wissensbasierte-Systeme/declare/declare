

/******************************************************************/
/***                                                            ***/
/***           DisLog :  Jess Interface                         ***/
/***                                                            ***/
/******************************************************************/


/* datalog_file_to_jess_file(File_1, File_2) <-
      */

datalog_file_to_jess_file(File_1, File_2) :-
   program_file_to_xml(File_1, Xml),
   xml_program_to_jess_file(Xml, File_2).


/* xml_program_to_jess_file(Xml, File) <-
      */

xml_program_to_jess_file(Xml, File) :-
   predicate_to_file( File,
      xml_program_to_jess(Xml) ).

xml_program_to_jess(Xml) :-
   writeln_list(['(clear)', '(reset)']),
   nl,
   forall( Rule := Xml^rule,
      xml_rule_to_jess(Rule) ).


/* xml_rule_to_jess(Rule) <-
      */

xml_rule_to_jess(Rule) :-
   Head := Rule^head,
   parse_fn_term(Head, head:_:[]),
   !.
xml_rule_to_jess(Rule) :-
   Body := Rule^body,
   parse_fn_term(Body, body:_:[]),
   !,
   Head := Rule^head,
   parse_fn_term(Head, head:_:[Atom]),
   xml_fact_to_jess(Atom).
xml_rule_to_jess(Rule) :-
   Head := Rule^head,
   Body := Rule^body,
   parse_fn_term(Head, head:_:[Atom]),
   parse_fn_term(Body, body:_:Atoms),
   gensym('_', N),
   write_list(['(defrule ', N]), nl,
   checklist( xml_atom_to_jess_in_rule,
      Atoms ),
   writeln('=>'),
   write('   '),
   xml_fact_to_jess(Atom),
   writeln(')').

xml_atom_to_jess_in_rule(Atom) :-
   write('   '),
   xml_atom_to_jess(Atom),
   nl.


/* xml_fact_to_jess(Atom) <-
      */

xml_fact_to_jess(Atom) :-
   write('(assert '),
   xml_atom_to_jess(Atom),
   writeln(')').


/* xml_atom_to_jess(Atom) <-
      */

xml_atom_to_jess(Atom) :-
   Predicate := Atom@predicate,
   term_to_atom((_:P)/_, Predicate),
   write_list(['(', P, ' ']),
   parse_fn_term(Atom, atom:_:Arguments),
   write_list_with_separators(
      xml_term_to_jess_middle,
      xml_term_to_jess_final,
      Arguments ),
   write(')').

xml_term_to_jess_middle(Term) :-
   xml_term_to_jess(Term),
   write(' ').

xml_term_to_jess_final(Term) :-
   xml_term_to_jess(Term).


/* xml_term_to_jess(Term) <-
      */

xml_term_to_jess(Term) :-
   Name := Term@name,
   !,
   write_list(['?', Name]).
xml_term_to_jess(Term) :-
   Constant := Term@functor,
   !,
   write(Constant).


/******************************************************************/


