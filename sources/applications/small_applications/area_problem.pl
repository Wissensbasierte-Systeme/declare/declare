

function_graph_display(voelker) :-
   function_graph_display(voelker,'Voelker',100,0.5,1000).
function_graph_display(area_problem) :-
   function_graph_display(area_problem,'Area Problem',1,1,1000).

% Solution:  S = [1.15873, 0.303301, 54.5942]


lambda_to_alpha(Lambda,Alpha) :-
   lambda_to_vexp_and_alpha(Lambda,_,Alpha).
lambda_to_vexp(Lambda,Vexp) :-
   lambda_to_vexp_and_alpha(Lambda,Vexp,_).

lambda_to_vexp_and_alpha( 514, 0.696, 0.007263138 ).
lambda_to_vexp_and_alpha( 488, 0.589, 0.012405150 ).
lambda_to_vexp_and_alpha( 476, 0.515, 0.014497404 ).

lambdas(476,488).
lambdas(488,514).
lambdas(476,514).


evaluate_function(voelker,XF,YF,Q,Val) :-
   X is XF * Q,
   lambdas(Lambda1,Lambda2),
   lambda_to_alpha(Lambda1,Alpha1),
   lambda_to_alpha(Lambda2,Alpha2),
   lambda_to_vexp(Lambda1,Vexp1),
   lambda_to_vexp(Lambda2,Vexp2),
   E2 is exp( 2 * Alpha2 * X ) - 1,
   E1 is exp( 2 * Alpha1 * X ) - 1,
   Y is Vexp1 * E1 - Vexp2 * E2,
   Val is YF * Y.

evaluate_function(area_problem,XF,YF,Q,Val) :-
   X is XF * Q,
%  R is 5*(X-0.5) + 0.0000001, exponate(abs(R),R,Val),
%  Val is X*sin(4/(1*X)).
   Phi is X * pi,
   Y is Phi*cos(2*Phi) - 0.5*sin(2*Phi) + pi/4,
   Val is YF * Y.


exponate(X,Y,Z) :-
   round(exp(Y*log(X)),2,Z).



solve_problem([Factor,Z,Degrees]) :-
   bisection(area_function,[0.2,0.4],[X,Y]),
   Z is (X+Y)/2,
   Factor is 2 * cos(pi*Z),
   Degrees is Z * 180.


bisection(Function,[X1,Y1],[X2,Y2]) :-
   power(10,8,P),
   Epsilon is 1/P,
   bisection(Function,Epsilon,[X1,Y1],[X2,Y2]).

bisection(_,Epsilon,[X,Y],[X,Y]) :-
   D is Y - X,
   D < Epsilon,
   !.
bisection(Function,Epsilon,[X1,Y1],[X2,Y2]) :-
   Z1 is (X1+Y1)/2,
   Goal =.. [Function,Z1,V1],
   call(Goal),
   ( ( V1 > 0, bisection(Function,Epsilon,[Z1,Y1],[X2,Y2]) )
   ; ( bisection(Function,Epsilon,[X1,Z1],[X2,Y2]) ) ).

area_function(Q,Val) :-
   evaluate_function(area_problem,1,1,Q,Val).


function_graph_display(Function,Header,XF,YF,N) :-
   generate_interval(1,N,I),
   findall( A-B,
      ( member(X,I),
        Q is X / N,
        evaluate_function(Function,XF,YF,Q,V),
        round(10*Q,2,A), round(2*V,2,B) ),
      Pairs ),
   generate_interval(1,10,J),
   findall( [K,Label],
      ( member(K,J),
        Label is K/10 ),
      X_Marks ),
   findall( [K,Label],
      ( member(K,J),
        Label is K/2 ),
      Y_Marks ),
   values_to_latex_file(Header,black,0.02,
      X_Marks,Y_Marks,[],[],Pairs),
   preview_latex_file.


