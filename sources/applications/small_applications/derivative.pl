
diff(X, X, 1) :-
   !.
diff(C, _X, 0) :-
   atomic(C).
diff(X^N, X, N*X^M) :-
   N > 0,
   M is N - 1.
diff(sin(X), X, cos(X)).
diff(cos(X), X, -sin(X)).
diff(log(X), X, 1/X).
diff(e^X, X, e^X).
diff(F+G, X, DF+DG) :-
   diff(F, X, DF),
   diff(G, X, DG). 
diff(F-G, X, DF-DG) :-
   diff(F, X, DF),
   diff(G, X, DG). 
diff(C*F, X, G) :-
   diff(C, X, 0),
   !,
   diff(F, X, DF),
   multi(C, DF, G).
diff(F*G, X, F*DG+DF*G) :-
   diff(F, X, DF),
   diff(G, X, DG). 

multi(C1, C2*F, C*F) :-
   !,
   C is C1 * C2.
multi(C, F, C*F).

