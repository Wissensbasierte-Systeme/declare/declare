

/******************************************************************/
/***                                                            ***/
/***          DentaLog:  Examination                            ***/
/***                                                            ***/ 
/******************************************************************/ 
 

:- dislog_variable_set( dentalog_knowledge_base,
      'projects/dentalog/dentalog_kb' ).


/*** interface ****************************************************/


/* dentalog_examination <-
      */

dentalog_examination :-
   dislog_variable_get(dentalog_knowledge_base, Dentalog_KB),
   dconsult(Dentalog_KB, Program),
   fact_filter(Program, State),
   star_line,
   writeln('Start of DentaLog Examinations'),
   star_line,
   dentalog_loop(Program, [], State).

dentalog_loop(Program, Examined, State) :-
   writeln_list([Examined, State]),
   tps_operator(Program, State, Derived_State),
   state_union(State, Derived_State, New_State),
   state_subtract(New_State, Examined, Delta_State),
   ( ( member([next_examination(E)], Delta_State), 
       member([possible_observations(E, List)], Program),
       !,
       ask_for_observations(E, List, Observations),
       state_union(Observations, New_State, State_2),
       state_union(
          [[next_examination(E)]], Examined, Examined_2),
       !,
       dentalog_loop(Program, Examined_2, State_2) )
   ; display_result_of_examination(Program, New_State) ).


/* display_result_of_examination(Program, New_State) <-
      */

display_result_of_examination(_, New_State) :-
   star_line,
   writeln('This is the Result of the Examination:'),
   nl,
   findall( examination(E, Observation),
      ( member([examination(E, N)], New_State),
        member([possible_observations(E, List)], New_State),
        member(N-Observation, List) ),
      All_Examinations ),
   writeln_list(All_Examinations),
   nl,
   findall( diagnosis(X),
      member([diagnosis(X)], New_State),
      Diagnoses ),
   writeln_list(Diagnoses),
   star_line.
%  flatten(New_State,Interpretation),
%  explain_coin([Interpretation], Program, Explanations),
%  show_explanations(Explanations).


/* ask_for_observations(Examination, List, Observations) <-
      */

ask_for_observations(Examination, List, Observations) :-
   star_line,
   writeln_list([
      'Examination:            ', Examination,
      'Possible Observations:  ' | List ]),
   write('Enter the Numbers of your Observations:  '),
   read_observations(Examination, Observations),
   nl,
   !.

read_observations(Examination, Observations) :-
   read_word(O),
   ( ( O == '.',
       Observations = [] )
   ; ( read_observations(Examination, E_Os),
       Observations =
          [[examination(Examination, O)]|E_Os] ) ).

read_word(Word) :-
   readword(32, Word, _).


/* grl_pp_dentalog_knowledge_base <-
      */

grl_pp_dentalog_knowledge_base :-
   dislog_variable_get(dentalog_knowledge_base, Dentalog_KB),
   dconsult(Dentalog_KB, Program),
   dentalog_knowledge_base_graph(Program, Edges),
   grl_pp_labeled_graph(Edges).


/* dentalog_knowledge_base_graph(Program, Edges) <-
      */

dentalog_knowledge_base_graph(Program, Edges) :-
   findall( ['E':E1, 'E':E2, O],
      ( member([next_examination(Ex2)]-[examination(Ex1, N)],
           Program),
        member([possible_observations(Ex1, List)], Program),
        member(N-Ob, List),
        abbreviate_sentences([Ex1, Ex2, Ob], [E1, E2, O]) ),
      Edges_1 ),
   findall( ['E':E, 'D':D, O],
      ( member([diagnosis(Di)]-[examination(Ex, N)], Program),
        member([possible_observations(Ex, List)], Program),
        member(N-Ob, List),
        abbreviate_sentences([Ex, Di, Ob], [E, D, O]) ),
      Edges_2 ),
   union(Edges_1, Edges_2, Edges).


/* abbreviate_sentences(Sentences_1, Sentences_2) <-
      */

abbreviate_sentences(Sentences_1, Sentences_2) :-
   maplist( abbreviate_sentence,
      Sentences_1, Sentences_2 ).

abbreviate_sentence(Sentence_1, Sentence_2) :-
   name(Sentence_1, Sentence_String),
   list_split_at_position([" "], Sentence_String, Words_Strings),
   maplist( nth1(1),
      Words_Strings, Abbreviated_String ),
   name(Sentence_2, Abbreviated_String).


/******************************************************************/


