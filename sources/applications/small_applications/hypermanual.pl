

/******************************************************************/
/***                                                            ***/
/***          Hypermanual:  Main File                           ***/
/***                                                            ***/
/******************************************************************/


hypermanual_file(
   html_manual,
   '$HOME/DisLog/Hypermanual/Hepa-Bilder' ).
hypermanual_file(
   link_tree,
   '$HOME/DisLog/Hypermanual/Hepa-Bilder/link_tree.html' ). 
hypermanual_file(
   results_of_search,
   '$HOME/DisLog/Hypermanual/results_of_search.html' ).
hypermanual_file(
   links,
   '$HOME/DisLog/Hypermanual/Hepa-Bilder/links.html' ).
hypermanual_file(
   hypermanual_insert_file,
   '$HOME/DisLog/Hypermanual/hypermanual_insert_file' ).


/*** interface ****************************************************/


mysql_select_hypermanual_ordered(Search_Atom) :-
   hypermanual_file(results_of_search,File),
   mysql_select_hypermanual(Search_Atom,State),
   length(State,N),
   hypermanual_state_to_files_and_scores(State,Files_and_Scores),
   maplist( hypermanual_state_select_file(State),
      Files_and_Scores, List_of_Score_File_State ),
   hypermanual_states_to_html_file(Search_Atom,
      N,List_of_Score_File_State,File).
 
mysql_select_hypermanual(Search_Atom) :-
   hypermanual_file(results_of_search,File),
   mysql_select_hypermanual(Search_Atom,State),
   hypermanual_state_to_html_file(Search_Atom,State,File).


hypermanual_files_to_insert_statements :-
   hypermanual_file(html_manual,File),
   hypermanual_files_to_insert_statements(File).


hypermanual_links(adjacency,out) :-
   mysql_select_hypermanual_links(State_1),
   hypermanual_links_group(State_1,State_2),
   mysql_select_hypermanual_links_grouped_show_state(State_2).

hypermanual_links(adjacency,in) :-
   mysql_select_hypermanual_links(State_1),
   hypermanual_links_invert_state(State_1,State_2),
   hypermanual_links_group(State_2,State_3),
   mysql_select_hypermanual_links_grouped_show_state(State_3).


/* There are two alternatives for building a link tree:
    - hypermanual_links(tree,dfs),
    - hypermanual_links(tree,cluster). */

hypermanual_links(tree,Mode) :-
   dislog_variable_set(link_mode(hypermanual),Mode),
   hypermanual_file(link_tree,File),
   hypermanual_edges(File).


/*** implementation ***********************************************/


hypermanual_state_select_file(State_1,
      Score-File,Score-File-State_2) :-
   Clause = [hypermanual(File,_,_)],
   findall( Clause,
      member(Clause,State_1),
      State_2 ).


hypermanual_search_to_files_and_scores(
      Search_Atom,Files_and_Scores) :-
   mysql_select_hypermanual(Search_Atom,State),
   hypermanual_state_to_files_and_scores(State,Files_and_Scores).

file_to_score(State,File,Score-File) :-
   findall( [File,N,Text],
      member([hypermanual(File,N,Text)],State),
      Triples ),
   hypermanual_triples_to_score(Triples,Score).

hypermanual_triples_to_score(Triples,Score) :-
   length(Triples,Score).

hypermanual_state_to_files_and_scores(State,Files_and_Scores) :-
   findall( File,
      member([hypermanual(File,_,_)],State),
      List ),
   list_to_ord_set(List,Files),
   maplist( file_to_score(State),
      Files, Files_and_Scores_List ),
   list_to_ord_set(Files_and_Scores_List,Files_and_Scores_Reverse),
   reverse(Files_and_Scores_Reverse,Files_and_Scores).

hypermanual_state_to_html_file(Search_Atom,State,File) :-
   length(State,N),
   hypermanual_states_to_html_file(Search_Atom,N,[State],File).

hypermanual_states_to_html_file(Search_Atom,N,States,File) :-
   write_list(['---> ',File]), nl,
   tell(File),
   writeln('<html>'), nl,
   writeln('<hr>'),
   writeln('<h2>Hypermanual - Results of Search</h2>'), nl,
   write_list(['There were ',N,' hits found in the manual for ',
      '<font color="#0000ff">', Search_Atom, '</font>.']), nl,
   writeln('<hr><br><p>'), nl,
   writeln('<blockquote>'),
   checklist( hypermanual_clause_to_html_file, States ),
   writeln('</blockquote>'), nl,
   writeln('</html>'),
   told.


hypermanual_clause_to_html_file(Score-File-State) :-
   !,
   write('<font color="#ccffcc">File: '),
   write_url(File),
   write_list(['<br> Hits:', Score, '</font><br><p><br><p>']), nl,
   hypermanual_clause_to_html_file_2(State),
   writeln('<hr>').

hypermanual_clause_to_html_file(State) :-
   hypermanual_clause_to_html_file(1,State).


hypermanual_clause_to_html_file(N,[[A]|State]) :-
   A = hypermanual(File,_Number,Text),
   concat(['<font color="#0000ff">Hit ', N,
      '</font>: <A HREF="file:',
      File, '"><pre>', Text, '</pre></A><br>'], HTML_Line),
   writeln(HTML_Line),
   M is N + 1,
   hypermanual_clause_to_html_file(M,State).
hypermanual_clause_to_html_file(_,[]).


hypermanual_clause_to_html_file_2(State) :-
  hypermanual_clause_to_html_file_2(1,State).

hypermanual_clause_to_html_file_2(N,[[A]|State]) :-
   A = hypermanual(File,_Number,Text),
   concat(['<pre><A HREF="file:',
      File, '">Hit ', N, '</A>:', Text, '</pre></center>'], HTML_Line),
   writeln(HTML_Line),
   M is N + 1,
   hypermanual_clause_to_html_file_2(M,State).
hypermanual_clause_to_html_file_2(_,[]).

  
hypermanual_links_to_html_file([links(Source,Destinations)]) :-
   write_url(Source), nl,
   writeln('<ol>'),
   checklist( hypermanual_destination_to_html_file, Destinations ),
   writeln('</ol>').

hypermanual_destination_to_html_file([Link_Text,Destination]) :-
   write_list(['   <li> ', Link_Text, ' </font> ---> ']),
   write_url(Destination), nl.


hypermanual_links_invert_state(State_1,State_2) :-
   maplist( hypermanual_link_invert, State_1, State_2 ).
   
hypermanual_link_invert([link(S,[T,D])],[link(D,[T,S])]).


mysql_select_hypermanual_links_grouped_show_state(State) :-
   hypermanual_file(links,File),
   tell(File),
   writeln('<html>'), nl,
   writeln('<hr>'),
   writeln('<h2>Hypermanual - Links</h2>'), nl,
   writeln('<hr><br><p>'), nl,
   writeln('<blockquote>'),
   checklist( hypermanual_links_to_html_file, State ),
   writeln('</blockquote>'),
   writeln('</html>'),
   told.


hypermanual_links_group(State_1,State_2) :-
   findall( Source,
      member([link(Source,_)],State_1),
      Sources_List ),
   list_to_ord_set(Sources_List,Sources),
   findall( [links(Source,Destinations)],
      ( member(Source,Sources),
        findall( Destination,
           member([link(Source,Destination)],State_1),
           Destinations ) ),
      State_2 ).
   
mysql_select_hypermanual_links(State) :-
   append(["use hypermanual_db; select 'mysql_result_atom([', ",
      "'''', file, ''',', '''', text, ''']).' ",
      "from hypermanual_dictionary ",
      "where text like '%<A HREF=%';"],
      Sql_Statements),
   name(Sql_Statements_2,Sql_Statements),
   mysql_select_execute_shell(Sql_Statements_2,Tuples),
   maplist( mysql_result_tuple_to_hypermanual_link,
      Tuples, State ).

mysql_select_hypermanual(Search_Atom,State) :-
   name(Search_Atom,Search_String),
   append(["use hypermanual_db; select 'mysql_result_atom([', ",
      "'''', file, ''',', number, ', ', ",
      "'''', text, ''']).' ",
      "from hypermanual_dictionary ",
      "where text like '%", Search_String, "%';"],
      Sql_Statements),
   name(Sql_Statements_2,Sql_Statements),
   mysql_select_execute_shell(Sql_Statements_2,Tuples),
   maplist( mysql_result_tuple_to_hypermanual_clause,
      Tuples, State ).

mysql_result_tuple_to_hypermanual_clause(
      [File,Number,Text],[A]) :-
   name_remove_elements(both,[9],File,File_2),
   name_remove_elements(both,[9],Text,Text_2),
   A =.. [hypermanual,File_2,Number,Text_2].


mysql_result_tuple_to_hypermanual_link([S,D],
      [link(Source,[Link_Text,Destination])]) :-
   name_remove_elements(both,[9],S,Source),
   name(D,List),
   name('<A HREF="',Start_1),
   name('<a href="',Start_2),
   list_start_after_position([Start_1,Start_2],List,List_2),
   name('">',End),
   list_cut_at_position([End],List_2,List_3),
   name(Destination,List_3),
   list_start_after_position([End],List_2,List_4),
   name('</A>',End_1),
   name('</a>',End_2),
   list_cut_at_position([End_1,End_2],List_4,List_5),
   name(Link_Text,List_5).


hypermanual_files_to_insert_statements(Directory) :-
   hypermanual_file(hypermanual_insert_file,P_sql),
   concat(Directory,'/*',Path),
   expand_file_name(Path,All_Files), writeln(user,All_Files),
   select_special_files(["htm","HTM"],All_Files,Files),
   write('---> '),
   writeln(P_sql),
   tell(P_sql),
   writeln('use hypermanual_db;'), nl,
   writeln('delete from hypermanual_dictionary;'), nl,
   checklist( hypermanual_file_to_insert_statement, Files ),
   told.


hypermanual_file_to_insert_statement(File) :-
   writeln(user,''),
   write(user,'processing '), writeln(user,File),
   hypermanual_read_file(File,Lines),
   length(Lines,N),
   write(user,N), writeln(user,' lines'),
   generate_interval(1,N,Interval),
   pair_lists(Interval,Lines,Pairs),
   checklist( hypermanual_line_to_insert_statement(File), Pairs ).


hypermanual_line_to_insert_statement(File,[N,Line]) :-
   write(user,'.'), ttyflush,
   list_exchange_elements(["'*"],Line,Line_2),
   name(Text,Line_2),
   writeln('INSERT INTO hypermanual_dictionary VALUES ('),
   write('   '''), write(File), write(''', '), write(N), writeln(','),
   write('   '''), write(Text), writeln(''' );').


hypermanual_read_file(A, C) :-
   see(A),
   hypermanual_read_lines_of_file(B),
   seen, !,
   hypermanual_split_lines(B,C).


hypermanual_read_lines_of_file(Lines) :-
   hypermanual_read_line_of_file([],B),
   ( B = eof(Line),
     reverse(Line,A),
     Lines = [A]
   ; hypermanual_read_lines_of_file(C),
     reverse(B,Line),
     Lines = [Line|C] ).

hypermanual_read_line_of_file(A,B) :-
   get0(C),
   ( C == 10,
     B = A
   ; C == -1,
     B = eof(A)
   ; C \== 10,
     hypermanual_read_line_of_file([C|A],B) ).


hypermanual_split_lines(Lines_1,Lines_2) :-
   maplist( hypermanual_split_line, Lines_1, List_of_Lines_2 ),
   append(List_of_Lines_2,Lines_2).

hypermanual_split_line(Line,[L|Ls]) :-
   member(13,Line),
   list_cut_at_position([[13]],Line,L),
   !,
   list_start_after_position([[13]],Line,Line_2),
   hypermanual_split_line(Line_2,Ls).
hypermanual_split_line(Line,[Line]).


/* select_special_files(["htm","HTM"],
      ['aaa.htm',bbb], Files).               */

select_special_files(Suffixes,Files_1,Files_2) :-
   sublist( select_special_file(Suffixes),
      Files_1, Files_2 ).

select_special_file(Suffixes,File) :-
   name(File,List),
   reverse(List,List_r),
   member(Suffix,Suffixes),
   reverse(Suffix,Suffix_r),
   ( append(Suffix_r,_,List_r)
   ; append(Suffix_r,_,List_r) ).


hypermanual_edges(File) :-
   mysql_select_hypermanual_links(State_1),
   maplist( cut_source_path,
      State_1, State_2 ),
   findall( V-W,
      member([link(V,[_,W])],State_2),
      Edges ),
   hypermanual_edges_to_html_file(Edges,File).

cut_source_path([link(V_1,[L,W])],[link(V_2,[L,W])]) :-
   name(V_1,L_1),
   reverse(L_1,L_2),
   list_cut_at_position(["/"],L_2,L_3),
   reverse(L_3,L_4),
   name(V_2,L_4).


hypermanual_edges_to_html_file(Edges,File) :-
   dislog_variable_get(link_mode(hypermanual),dfs),
   !,
%  tell(xxx), writeln_list(Edges), told,
   maplist( edge_to_packed_edge,
      Edges, Edges_2 ),
   edges_to_ugraph(Edges_2,U_Graph),
%  tell(yyy), writeln_list(U_Graph), told,
   ugraph_to_tree_2(U_Graph,['1-START.HTM'],Tree),
%  dportray(tree,Tree),
   hypermanual_trees_to_html_file([Tree],File).
hypermanual_edges_to_html_file(Edges,File) :-
   dislog_variable_get(link_mode(hypermanual),cluster),
   !,
   edges_to_reduced_ugraph_to_trees(Edges,Trees),
   hypermanual_trees_to_html_file(Trees,File).

edge_to_packed_edge(V1-V2,[V1]-[V2]).

hypermanual_trees_to_html_file(Trees,File) :-
   trees_to_html_file('Hypermanual - Link Tree', Trees, File).


/*** tests ********************************************************/


hypermanual_test(1) :-
   hypermanual_file(results_of_search,File),
   Search_Atom = 'Fieber',
   State = [[hypermanual('alkhep.htm', 45,  'Fieber ortaler')]],
   length(State,N),
   hypermanual_state_to_files_and_scores(State,Files_and_Scores),
   maplist( hypermanual_state_select_file(State),
      Files_and_Scores, List_of_Score_File_State ),
   hypermanual_states_to_html_file(Search_Atom,
      N,List_of_Score_File_State,File).


/******************************************************************/

 
