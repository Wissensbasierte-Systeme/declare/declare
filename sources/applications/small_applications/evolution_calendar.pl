

/******************************************************************/
/***                                                            ***/
/***       Evolution:  Calendar                                 ***/
/***                                                            ***/
/******************************************************************/



/******************************************************************/


/* vcalendar_to_ics(Events) <-
      */

vcalendar_to_ics(Events) :-
   writeln('BEGIN':'VCALENDAR'),
   writeln('CALSCALE:GREGORIAN'),
   writeln('PRODID:-//Ximian//NONSGML Evolution Calendar//EN'),
   writeln('VERSION:2.0'),
   length(Events, Length),
   ( for(I, 1, Length) do
        nth(I, Events, Event),
        vevent_to_ics(I, Event) ),
   writeln('END':'VCALENDAR').

vevent_to_ics(I, Event) :-
   member('DATE':D, Event),
   member('STAMP':S, Event),
   member('START':Start, Event),
   member('END':End, Event),
   concat([D, 'T', S, 'Z'], DTStamp),
   Url = '/freeassociation.sourceforge.net/Tzfile/Europe/Berlin',
   concat(['DTSTART;TZID=', Url, ':\n ',
       D, 'T', Start], Dtstart),
   concat(['DTEND;TZID=', Url, ':\n ',
       D, 'T', End], Dtend),
   writeln('BEGIN':'VEVENT'),
   writeln('UID':DTStamp-2005-1001-1-I@durbin),
   writeln('DTSTAMP':DTStamp),
   writeln('TRANSP':'OPAQUE'),
   writeln(Dtstart),
   writeln(Dtend),
   vevent_items_to_ics(Event),
   writeln('CREATED':DTStamp),
   writeln('LAST-MODIFIED':DTStamp),
   writeln('END':'VEVENT').

vevent_items_to_ics([X|Xs]) :-
   X = A:B,
   ( not(member(A, ['START', 'END', 'DATE', 'STAMP'])) ->
     write(A),
     write(':'),
     writeln(B)
   ; true ),
   vevent_items_to_ics(Xs).
vevent_items_to_ics([]).

my_vcalendar(Events) :-
   Events = [E1, E2],
   E1 = [
      'DATE':'20141016',
      'STAMP':'051050',
      'SUMMARY':'15:15 A',
      'START':'150000',
      'END':'153000'
      ],
   E2 = [
      'DATE':'20141016',
      'STAMP':'051050',
      'SUMMARY':'15:30 B',
      'START':'153000',
      'END':'160000'
      ].


/*** tests ********************************************************/

test(vcalendar_to_ics, 1) :-
   my_vcalendar(Events),
   F = 'evolution_calendar_test.ics',
   tell(F),
   vcalendar_to_ics(Events),
   told.

/******************************************************************/


