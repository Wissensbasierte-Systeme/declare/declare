

/******************************************************************/
/***                                                            ***/
/***       Cardinality Constraints: Basic Calculus              ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      cardinality_constraints_calculus/4.


/******************************************************************/


/* length_catch(Xs, N) <-
      */

length_catch(Xs, N) :-
   catch( length(Xs, N), _, fail ).


/* cardinality_constraints_calculus(basic,Cs1,Cs2,T:F) <-
      */

cardinality_constraints_calculus(basic,Cs1,Cs2,T:F) :-
   cardinality_constraints_implication_fixpoint(Cs1,Cs2),
   cardinality_constraints_definite_conclusions(Cs2,T,F),
   !.


/* cardinality_constraints_implication_fixpoint(Cs1,Cs2) <-
      the list Cs1 of cardinality constraints is simplified to
      the list Cs2 of cardinality constraints using the predicate
      cardinality_constraints_implication(Mode,C1,C2,C3).   */

cardinality_constraints_implication_fixpoint(Cs1,Cs2) :-
   cardinality_constraints_implication(Cs1,Cs3),
   ( ( Cs1 = Cs3, !,
       Cs2 = Cs3 )
   ; cardinality_constraints_implication_fixpoint(Cs3,Cs2) ).

cardinality_constraints_implication(Cs1,Cs2) :-
   cardinality_constraints_implication(replace,Cs1,Cs3),
%  writeln(user,replace(Cs3)),
   cardinality_constraints_implication(insert,Cs3,Cs4),
%  writeln(user,insert(Cs4)),
   ord_union(Cs3,Cs4,Cs5),
   set_canonize(cardinality_constraint_subsumes,Cs5,Cs2),
   !.


/* cardinality_constraints_implication(Mode,Cs1,Cs2) <-
      */

cardinality_constraints_implication(insert,Cs1,Cs2) :-
   findall( C3,
      ( member(C1,Cs1), member(C2,Cs1),
        cardinality_constraints_implication(insert,C1,C2,C3) ),
      Cs2_List ),
   list_to_ord_set(Cs2_List,Cs2).

cardinality_constraints_implication(replace,Cs1,Cs2) :-
   maplist( cardinality_constraints_implication_multiple(replace,Cs1),
      Cs1, List_of_Cs2 ),
   ord_union(List_of_Cs2,Cs2).
 
cardinality_constraints_implication_multiple(replace,Cs1,C2,Cs3) :-
   findall( C3,
      ( member(C1,Cs1),
        cardinality_constraints_implication(replace,C1,C2,C3) ),
      Cs_List ),
   list_to_ord_set(Cs_List,Cs),
   ( ( Cs = [], !,
       Cs3 = [C2] )
   ; Cs3 = Cs ).


/* cardinality_constraints_implication(Mode,Xs1-N1,Xs2-N2,Xs3-N3) <-
      Mode = insert:
         together Xs1-N1 and Xs2-N2 imply Xs3-N3,
      Mode = replace:
         Xs2-N2 can be replaced by Xs3-N3 due to Xs1-N1.  */

cardinality_constraints_implication(insert,Xs1-N1,Xs2-N2,Xs3-N3) :-
   \+ length_catch(Xs2,N2),
   \+ N2 = 0,
   ord_subtract(Xs1,Xs2,Xs3),
   N3 is N1 - N2,
   length_catch(Xs3,N3),
   Xs3-N3 \= []-0.
cardinality_constraints_implication(replace,Xs1-N1,Xs2-N2,Xs3-N3) :-
   \+ length_catch(Xs1,N1),
   \+ N1 = 0,
   ord_subset(Xs1,Xs2),
   N1 =< N2,
   ord_subtract(Xs2,Xs1,Xs3),
   N3 is N2 - N1,
   Xs3-N3 \= []-0.
cardinality_constraints_implication(replace,Xs1-N1,Xs2-N2,Xs3-N3) :-
   length_catch(Xs1,N1),
   ord_subtract(Xs2,Xs1,Xs3),
   ord_intersection(Xs2,Xs1,Xs),
   Xs \= [],
   length_catch(Xs,M),
   N3 is N2 - M,
   Xs3-N3 \= []-0.
cardinality_constraints_implication(replace,Xs1-N1,Xs2-N2,Xs3-N3) :-
   length_catch(Xs1,N1),
   length_catch(Xs2,N2),
   ord_union(Xs1,Xs2,Xs3),
   length_catch(Xs3,N3),
   Xs3 \= Xs2.
cardinality_constraints_implication(replace,Xs1-0,Xs2-N2,Xs3-N2) :-
   N2 \= 0,
   ord_subtract(Xs2,Xs1,Xs3).
cardinality_constraints_implication(replace,Xs1-0,Xs2-0,Xs3-0) :-
   ord_union(Xs1,Xs2,Xs3),
   Xs3 \= Xs2.


cardinality_constraint_subsumes(Xs1-N1,Xs2-N2) :-
   Xs1-N1 \= Xs2-N2,
   length_catch(Xs1,N1),
   length_catch(Xs2,N2),
   ord_subset(Xs2,Xs1).
cardinality_constraint_subsumes(Xs1-0,Xs2-0) :-
   Xs1 \= Xs2,
   ord_subset(Xs2,Xs1).


cardinality_constraint_removes(Xs1-_,Xs2-N2) :-
   Xs1 \= Xs2,
   \+ cardinality_constraint_definite(Xs2-N2),
   ord_subset(Xs1,Xs2).


/******************************************************************/


