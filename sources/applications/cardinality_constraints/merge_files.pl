

mine_sweeper_analysis_norm :-
   mine_sweeper_analysis_norm(Pairs),
   append(Pairs,List),
   findall( X,
      ( member(X,List),
        X \= '-' ),
      Xs ),
   average(Xs,A),
   writeln(A).


mine_sweeper_analysis_norm(Pairs) :-
   dislog_variable_get(cardinality_constraints_measures,File),
   dconsult(File,Clp_Inputs),
   findall( [Lp,Calc],
      ( member([clp_input(_,_,_,(Ts1,Ts2))],Clp_Inputs),
        member(lp(Lp1,_),Ts1),
        member(lp(Lp2,_),Ts2),
        divide_save(Lp1,Lp2,Lp),
        member(calc(Calc1,_),Ts1),
        member(calc(Calc2,_),Ts2),
        divide_save(Calc1,Calc2,Calc) ),
      Pairs ).


cardinality_constraints_merge_with_uli :-
   dislog_variable_get(home,DisLog_Directory),
   name_append(DisLog_Directory,
      '/examples/mine_sweeper/',Directory),
   name_append(Directory,'mine_sweeper',File_1),
   name_append(Directory,'psol6_6.pl',File_2),
   name_append(Directory,'mine_sweeper_merge',File_3),
   diconsult(File_1,Clp_Inputs_1),
   diconsult(File_2,Clp_Inputs_2),
%  findall( [clp_input(Constraints,True,False,Mode,Times)],
   findall( [clp_input(Constraints,True,False,Times)],
      ( member(
%          [clp_input(Constraints,True,False,Mode,Times_1)],
           [clp_input(Constraints,True,False,Times_1)],
           Clp_Inputs_1 ),
%       member(
%          [clp_output(Constraints,True,False,Time_2)],
%          Clp_Inputs_2 ),
        member([Clp_output],Clp_Inputs_2),
        Clp_output =.. [_,Constraints,True,False,Time_2],
%       Time is Time_2 / 1000,
        Time is Time_2 / 100,
%       Time > 0,
        Times_2 = [clp(Time,-)],
        ord_union(Times_1,Times_2,Times) ),
      Clp_Inputs ),
   disave(Clp_Inputs,File_3).


