

/******************************************************************/
/***                                                            ***/
/***       Cardinality Constraints: Smodels                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* cardinality_constraints_evaluate_with_smodels(N, Models) <-
      */

cardinality_constraints_evaluate_with_smodels(N, Models) :-
   File = 'mine_sweeper/mine_sweeper',
   cardinality_constraints_evaluate_with_smodels(File, N, Models).

cardinality_constraints_evaluate_with_smodels(File, N, Models) :-
   dconsult(File, Program),
   nth(N, Program, [A]),
   A =.. [_, Cs|_],
   cardinality_constraints_to_models_with_smodels(
      Cs, Models, Bombs:Frees),
   write(user, bombs),
   dportray(chs,[Bombs]),
   write(user, frees),
   dportray(chs,[Frees]).


cardinality_constraints_to_models_with_smodels(
      Cs, Models, Bombs:Frees) :-
   cardinality_constraints_to_models_with_smodels(Cs, Models),
   cardinality_constrains_and_models_to_tf(
      Cs, Models, Bombs:Frees).

cardinality_constrains_and_models_to_tf(
      Cs, Models, Bombs:Frees) :-
   ord_intersection(Models, Bombs),
   cardinality_constraints_to_positions(Cs, Positions),
   maplist( ord_subtract(Positions),
      Models, Complements ),
   ord_intersection(Complements, Frees),
   !.


/* cardinality_constraints_to_models_with_smodels(Cs, Models) <-
      */

cardinality_constraints_to_models_with_smodels(Cs, Models) :-
   dislog_variable_get(smodels_in, File_I),
   dislog_variable_get(smodels_out, File_O),
   dis_ex_extend_filename(File_I, Path_I),
   dis_ex_extend_filename(File_O, Path_O),
   predicate_to_file( Path_I,
      checklist( cardinality_constraint_to_fact_for_smodels,
         Cs ) ),
   name_append([
      'lparse < ',Path_I,' | smodels 0 > ',Path_O ],
      Command ),
   us(Command),
   parse_stable_models_file(Path_O, Ms),
   maplist( cc_atoms_from_smodels_to_pairs,
      Ms, Models ).


/*** implementation ***********************************************/


/* cardinality_constraint_to_fact_for_smodels(Xs-N) <-
      */

cardinality_constraint_to_fact_for_smodels(Xs-N) :-
   write(N), write(' {'),
   maplist( pair_to_atom_for_smodels,
      Xs, Ys ),
   write_with_comma(Ys),
   write('} '), write(N), writeln('.').

pair_to_atom_for_smodels(X-Y, Atom) :-
   name_append(['x_', X, '_', Y], Atom).


/* cc_atoms_from_smodels_to_pairs(Atoms, Pairs) <-
      */

cc_atoms_from_smodels_to_pairs(Atoms, Pairs) :-
   maplist( cc_atom_from_smodels_to_pair,
      Atoms, Pairs_2 ),
   list_to_ord_set(Pairs_2, Pairs).

cc_atom_from_smodels_to_pair(Atom, X-Y) :-
   name_start_after_position(["_"], Atom, X_Y),
   name_split_at_position(["_"], X_Y, [X, Y]).


/* cardinality_constraints_to_positions(Cs, Positions) <-
      */

cardinality_constraints_to_positions(Cs, Positions) :-
   findall( X,
      ( member(Xs-_, Cs),
        member(X, Xs) ),
      Positions_2 ),
   list_to_ord_set(Positions_2, Positions).


/******************************************************************/


