

/******************************************************************/
/***                                                            ***/
/***       Mine Sweeper: Measurements in Star Office            ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


/* clp_output_add_times <-
      */

clp_output_add_times :-
   read_so_data(Data),
   dconsult('mine_sweeper/mine_sweeper_numbers',State_1),
   maplist( clp_output_add_times(Data),
      State_1, State_2 ),
   dsave(State_2,'mine_sweeper/mine_sweeper_state').

clp_output_add_times(Data,Clp_Output_1,Clp_Output_2) :-
   Clp_Output_1 = [clp_output(Number,Cs,Ts,Fs,_)],
   member([Number,Lp,Calc,Clp_Amoung,Clp_Disjunctive],Data),
   Times = [clp(Clp_Amoung,Clp_Disjunctive),lp(Lp,-),calc(Calc,-)],
   Clp_Output_2 = [clp_output(Cs,Ts,Fs,Times)].


/* read_so_data(Data) <-
      */

read_so_data(Data) :-
   read_star_office_file('lp.csv',File_Contents),
   maplist( project_so_data,
      File_Contents, Data ).

project_so_data(Line,[Number,Lp,Calc,Clp_Amoung,Clp_Disjunctive]) :-
   nth(1,Line,Number),
   nth(11,Line,Lp),
   nth(12,Line,Calc),
   nth(14,Line,Clp_Amoung),
   nth(15,Line,Clp_Disjunctive).


/******************************************************************/


