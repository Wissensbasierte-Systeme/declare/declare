

/******************************************************************/
/***                                                            ***/
/***       Cardinality Constraints: Improved Calculus           ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      cardinality_constraints_imply/2.


cardinality_constraints_calculus(improved,Cs1,Cs2,T2:F2) :-
   cardinality_constraints_derivation(Cs1,Cs2,T2:F2),
   !.


cardinality_constraints_derivation(Cs1,Cs2,T2:F2) :-
   cardinality_constraints_derivation(Cs1,[]:[],Cs2,T:F),
   list_to_ord_set(T,T2),
   list_to_ord_set(F,F2).

cardinality_constraints_derivation(Cs1,T1:F1,Cs2,T2:F2) :-
   cardinality_constraints_derive(Cs1,T:F,Cs3),
   ord_union(T1,T,T3),
   ord_union(F1,F,F3),
   ( ( Cs1 = Cs3, !,
       Cs2 = Cs3, T2:F2 = T3:F3 )
   ; cardinality_constraints_derivation(Cs3,T3:F3,Cs2,T2:F2) ).
%  ( Cs1 = Cs3 ->
%    Cs2 = Cs3, T2:F2 = T3:F3
%  ; cardinality_constraints_derivation(Cs3,T3:F3,Cs2,T2:F2) ).

cardinality_constraints_derive(Cs1,T:F,Cs4) :-
%  init_timer(cardinality_constraints_derive),
   cardinality_constraints_imply(Cs1,Cs2),
   ord_union(Cs1,Cs2,Cs3),
   cardinality_constraints_reduce(Cs3,T:F,Cs4),
%  add_timer(cardinality_constraints_derive),
   !.


cardinality_constraints_fully_reduce(Cs1,T2:F2,Cs2) :-
   cardinality_constraints_fully_reduce(Cs1,[]:[],Cs2,T2:F2).

cardinality_constraints_fully_reduce(Cs1,T1:F1,Cs2,T2:F2) :-
   cardinality_constraints_reduce(Cs1,T:F,Cs3),
   cc_ord_union(T1:F1,T:F,T3:F3),
   ( ( T:F = []:[], !,
       Cs2 = Cs3, T2:F2 = T3:F3 )
   ; cardinality_constraints_fully_reduce(Cs3,T3:F3,Cs2,T2:F2) ).
   
cardinality_constraints_reduce(Cs1,T:F,Cs2) :-
%  init_timer(cardinality_constraints_reduce),
   cardinality_constraints_partition(
      cardinality_constraint_is_empty,
      Cs1, Cs_F, Cs3 ),
   cardinality_constraints_to_elements(Cs_F,F),
   cardinality_constraints_partition(
      cardinality_constraint_is_full,
      Cs3, Cs_T, Cs4 ),
   cardinality_constraints_to_elements(Cs_T,T),
   ord_union(T,F,TF),
   maplist( cardinality_constraint_reduce(T:TF),
      Cs4, Cs5 ),
   list_to_ord_set(Cs5,Cs2),
%  add_timer(cardinality_constraints_reduce),
   !.


cardinality_constraints_reduce_by(T:F,Cs1,Cs2,Cs3) :-
   cardinality_constraints_reduce_by(T:F,Cs1,Cs2),
   ord_subtract(Cs2,Cs1,Cs3).

cardinality_constraints_reduce_by(T:F,Cs1,Cs2) :-
   ord_union(T,F,TF),
   maplist( cardinality_constraint_reduce(T:TF),
      Cs1, Cs3 ),
   list_to_ord_set(Cs3,Cs2).

cardinality_constraint_reduce(T:TF,Xs1-N1,Xs2-N2) :-
   ord_intersection(Xs1,T,Xs),
   length(Xs,N),
   N2 is N1 - N,
   ord_subtract(Xs1,TF,Xs2).

cardinality_constraints_partition(Predicate,Cs1,Cs2,Cs3) :-
   sublist( Predicate,
      Cs1, Cs2 ),
   ord_subtract(Cs1,Cs2,Cs3).

cardinality_constraint_is_full(Xs-N) :-
   length(Xs,N).

cardinality_constraint_is_empty(_-0).

cardinality_constraints_to_elements(Constraints,Elements) :-
   findall( X,
      ( member(Xs-_,Constraints),
        member(X,Xs) ),
      List ),
   list_to_ord_set(List,Elements).


cardinality_constraints_imply(Cs1,Cs2) :-
   dislog_variable_get(cardinality_constraints_imply_mode,tree),
%  writeln(Cs1),
   cardinality_constraints_to_set_tree(Cs1,Tree),
%  dportray(tree,Tree),
   cardinality_constraints_imply_tree(Cs1,Tree,Cs2).
   
cardinality_constraints_imply_tree([C1|Cs1],Tree1,Cs2) :-
   C1 = M1-K1,
%  write('.  '),
   delete_clause_in_tree([card(K1)|M1],Tree1,Tree2),
%  Tree2 = Tree1,
%  dportray(tree,Tree2),
%  writeln(delete_clause_in_tree([card(K1)|M1],Tree1,Tree2)),
   set_and_set_tree_symmetric_difference_to_set_pairs(
      M1, Tree2, Pairs ),
%  writeln(pairs(Pairs)),
   maplist( cardinality_constraint_set_pair_to_cc_pair,
      Pairs, CC_Pairs ), 
%  writeln(cc_pairs(CC_Pairs)),
   findall( M4-K,
      ( member([M2-K2,N],CC_Pairs),
        cardinality_constraints_order(M2-K2,N-K1,M3-K3,M4-K4),
%       writeln(hallo(M3-K3,M4-K4)),
        K is K4 - K3,
        ( M3 = []
        ; length(M4,K) ),
        M4-K \= []-0 ),
      List ),
%  writeln(new(List)),
   list_to_ord_set(List,Cs3),
   !,
   ( ( cardinality_constraints_reduce(Cs3,T:F,_),
       T:F \= []:[], !,
       Cs4 = [] )
   ; cardinality_constraints_imply_tree(Cs1,Tree2,Cs4) ),
   ord_union(Cs4,Cs3,Cs2).
cardinality_constraints_imply_tree([],_,[]).
        
cardinality_constraint_set_pair_to_cc_pair(Set_Pair,[M1-K1,M2]) :-
   ( Set_Pair = [diff(M2),card(K1)|M1] 
   ; Set_Pair = [card(K1),diff(M2)|M1] ). 


cardinality_constraints_imply([C1|Cs1],Cs2) :-
   dislog_variable_get(cardinality_constraints_imply_mode,regular),
   !,
   cardinality_constraints_apply(C1,Cs1,Cs2).
cardinality_constraints_imply([],[]) :-
   dislog_variable_get(cardinality_constraints_imply_mode,regular),
   !.

cardinality_constraints_apply(C1,Cs2,Cs5) :-
   cardinality_constraints_application([C1],Cs2,Cs3),
   ( ( dislog_variable_get(cardinality_constraints_reduce_mode,early),
       cardinality_constraints_reduce(Cs3,T:F,_),
       T:F \= []:[], !,
       Cs4 = [] )
   ; cardinality_constraints_imply(Cs2,Cs4) ),
   ord_union(Cs4,Cs3,Cs5).


cardinality_constraints_applications(Cs1,Cs2,Cs3) :-
   cardinality_constraints_application(Cs1,Cs2,Cs4),
   ( ( ord_union(Cs1,Cs2,Cs),
       ord_subset(Cs4,Cs), !,
       Cs3 = [] )
   ; ( cardinality_constraints_applications(Cs4,Cs2,Cs5),
       ord_union(Cs5,Cs4,Cs3) ) ). 

cardinality_constraints_application(Cs1,Cs2,Cs3) :-
   findall( C,
      ( member(C1,Cs1),
        member(C2,Cs2),
        cardinality_constraints_order(C1,C2,C3,C4),
        cardinality_constraints_imply(C3,C4,C) ),
      List ),
   list_to_ord_set(List,Cs3),
   !.


cardinality_constraints_order(Xs1-N1,Xs2-N2,Xs1-N1,Xs2-N2) :-
   N1 =< N2.
cardinality_constraints_order(Xs1-N1,Xs2-N2,Xs2-N2,Xs1-N1) :-
   N2 =< N1.


/*
cardinality_constraints_imply(Cs1,Cs2) :-
   findall( Xs-N,
      ( member(Xs1-N1,Cs1),
        member(Xs2-N2,Cs1),
        N1 =< N2,
        Xs1-N1 \= Xs2-N2,
        cardinality_constraints_imply(Xs1-N1,Xs2-N2,Xs-N) ),
      List ),
   list_to_ord_set(List,Cs2).
*/

cardinality_constraints_imply(Xs1-N1,Xs2-N2,Xs3-N3) :-
%  N1 =< N2,
   ord_subtract(Xs2,Xs1,Xs3),
   dislog_counter_increase('('),
   N3 is N2 - N1,
   ( ( ord_subset(Xs1,Xs2), dislog_counter_increase(')'), ! )
   ; ( length(Xs3,N3), dislog_counter_increase(']') ) ).
%  cardinality_constraints_imply_test(Xs1,Xs2,Xs3-N3).

/*
cardinality_constraints_imply_test(Xs1,Xs2,_,_) :-
   ord_subset(Xs1,Xs2),
   !.
cardinality_constraints_imply_test(_,_,Xs3,N3) :-
   length(Xs3,N3).
*/


/******************************************************************/


