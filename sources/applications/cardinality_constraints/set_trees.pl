

test(cardinality_constraints:ccs_to_set_tree,1) :-
   Constraints = [
      [1-2, 1-3, 1-4, 2-2]-2, [1-2, 2-2]-1, [1-3, 1-4, 1-5]-1,
      [1-4, 1-5, 1-6, 2-6, 3-6]-2, [2-2, 4-2]-1, [2-6, 3-6, 4-6]-1,
      [3-6, 4-6, 5-4, 5-5, 5-6]-1, [4-2, 5-2, 5-3, 5-4]-1,
      [5-3, 5-4, 5-5]-1 ],
   cardinality_constraints_to_set_tree(Constraints,Tree),
   dportray(tree,Tree),
   set_and_set_tree_symmetric_difference_to_set_pairs(
      [1-2, 2-2], Tree, Pairs ),
   dportray(chs,Pairs).

test(cardinality_constraints:ccs_imply_tree,1) :-
   Constraints = [
      [1-2, 1-3, 1-4, 2-2]-2, [1-2, 2-2]-1, [1-3, 1-4, 1-5]-1,
      [1-4, 1-5, 1-6, 2-6, 3-6]-2, [2-2, 4-2]-1, [2-6, 3-6, 4-6]-1,
      [3-6, 4-6, 5-4, 5-5, 5-6]-1, [4-2, 5-2, 5-3, 5-4]-1,
      [5-3, 5-4, 5-5]-1 ],
   list_to_ord_set(Constraints,Cs),
   cardinality_constraints_derivation(Cs,_,T:F),
   writeln(T:F).
%  cardinality_constraints_to_set_tree(Cs,Tree),
%  cardinality_constraints_imply_tree(Cs,Tree,Cs2),
%  writeln(Cs2).


cardinality_constraints_to_set_trees :-
   tell(uuu),
   dislog_variable_get(cardinality_constraints_measures,File),
   dconsult(File,Clp_Inputs),
   maplist( clp_input_to_set_tree,
      Clp_Inputs, _Set_Trees ),
   told.
   
clp_input_to_set_tree([clp_input(Constraints,_,_,_)],Tree) :-
   cardinality_constraints_to_set_tree(Constraints,Tree),
   cardinality_constraints_weight(2,Constraints,Weight),
   tree_overlap_ratio(Tree,N),
   tree_state_overlap_ratio(Tree,M), K is M / N,
   maplist( name_exchange_elements([".,"]),
      [Weight,M,N,K], [Weight_2,M_2,N_2,K_2] ),
   writeln(Weight_2:M_2:N_2:K_2).
%  pp_ratio_information(Tree).


cardinality_constraints_to_set_tree(Cs,Tree) :-
   maplist( cardinality_constraint_to_set,
      Cs,Sets ),
   sets_to_set_tree(Sets,Tree).

cardinality_constraint_to_set(M-K,S) :-
   ord_union([card(K)],M,S).
% cardinality_constraint_to_set(M-_,M).


tree_in_set(S,[N|Ts_1],[N|Ts_2]) :-
   ord_subset(N,S),
   !,
   findall( T2,
      ( member(T1,Ts_1),
        tree_in_set(S,T1,T2),
        T2 \= [] ),
      Ts_2 ).
tree_in_set(_,_,[]).


