

/******************************************************************/
/***                                                            ***/
/***       Cardinality Constraints: Optimized Calculus          ***/
/***                                                            ***/
/******************************************************************/


cardinality_constraints_calculus(opt_1,Cs1,Cs2,T:F) :-
   cc_derivation_delta(Cs1,Cs2,T:F),
   !.
cardinality_constraints_calculus(opt_2,Cs1,Cs2,T:F) :-
   cc_derive_delta(Cs1,Cs2,T:F),
   !.


cc_derivation_delta(Cs1,Cs2,T2:F2) :-
   cc_derivation_delta(Cs1,Cs1,[]:[],Cs2,T:F),
   list_to_ord_set(T,T2),
   list_to_ord_set(F,F2).


cc_derivation_delta(Delta_Cs,Cs1,T1:F1,Cs2,T2:F2) :-
   cardinality_constraints_application_delta(Delta_Cs,Cs1,Cs3,T3:F3),
   ord_union(T1,T3,T4),
   ord_union(F1,F3,F4),
   ord_union(T3,F3,TF3),
   maplist( cardinality_constraint_reduce(T3:TF3),
      Cs1, Cs1_Reduced ),
   ord_union(Cs1_Reduced,Cs3,Cs4),
   ( ( Cs3 = [], !,
       T2:F2 = T4:F4 )
   ; cc_derivation_delta(Cs3,Cs4,T4:F4,Cs2,T2:F2) ).


cardinality_constraints_application_delta(Cs1,Cs2,Cs3,T3:F3) :-
   cardinality_constraints_application(Cs1,Cs2,Cs),
   ord_subtract(Cs,Cs2,New_Cs),
   cardinality_constraints_reduce(New_Cs,T3:F3,Cs3).


cardinality_constraints_application_init(Cs1,Cs3,T3:F3) :-
   cardinality_constraints_application_init(Cs1,Cs2),
%  writeln(real_init(Cs2)),
   cardinality_constraints_fully_reduce(Cs2,T3:F3,Cs3).

cardinality_constraints_application_init([C1|Cs1],Cs3) :-
   findall( C,
      ( member(C2,Cs1),
        cardinality_constraints_order(C1,C2,C3,C4),
        cardinality_constraints_imply(C3,C4,C) ),                 
      Cs3_a ),
   cardinality_constraints_application_init(Cs1,Cs3_b),
   ord_union(Cs3_a,Cs3_b,Cs3).
cardinality_constraints_application_init([],[]).


cc_derive_delta(Cs1,Cs2,T2:F2) :-
%  writeln(user,Cs1),
%  cc_derive_delta_2(Cs1,[],Cs1,[]:[],Cs2,T:F),
   cardinality_constraints_application_init(Cs1,Cs3,T3:F3),
%  writeln(user,init(Cs3,T3:F3)),
   ord_union(Cs3,Cs1,Cs1_a),
   cc_derive_delta_iteration(Cs3,T3:F3,Cs1_a,T3:F3,Cs2,T:F),
%  cc_derive_delta_iteration(Cs1,Cs1,[]:[],Cs2,T:F),
   list_to_ord_set(T,T2),
   list_to_ord_set(F,F2),
   !.

cc_derive_delta_iteration([],[]:[],Cs,S,Cs,S) :-
   !.
cc_derive_delta_iteration(Cs,S,Cs1,S1,Cs2,S2) :-
%  writeln((Cs-S)), writeln(Cs1), wait,
   cardinality_constraints_reduction(S,Cs1,Cs1_Reduced,Cs_a,S_a),
   cardinality_constraints_application_delta(Cs,Cs1_Reduced,Cs_b,S_b),
   ord_union(Cs_a,Cs_b,Cs_c),
%  writeln(ord_union(Delta_Cs_a,Delta_Cs_b,Delta_Cs_c)),
   cc_ord_union(S_a,S_b,S_c),
   cardinality_constraints_fully_reduce(Cs_c,S_d,Delta_Cs),
%  writeln(cardinality_constraints_fully_reduce(Delta_Cs_a,S_d,Delta_Cs)),
   ord_union(Delta_Cs,Cs1_Reduced,Cs3),
   cc_ord_union(S_c,S_d,Delta_S),
   cc_ord_union(S1,S,S3),
   cc_derive_delta_iteration(Delta_Cs,Delta_S,Cs3,S3,Cs2,S2).


cardinality_constraints_reduction([],Cs1,Cs1,[],[]:[]) :-
   !.
cardinality_constraints_reduction(S,Cs1,Cs1_Reduced,Cs2,S2) :-
   cardinality_constraints_reduce_by(S,Cs1,Cs1_Reduced,Cs),
   cardinality_constraints_fully_reduce(Cs,S2,Cs2).


cc_derive_delta_2([C|Cs],Cs0,Cs1,S1,Cs2,S2) :-
   member(C,Cs0),
   !,
   cc_derive_delta_2(Cs,Cs0,Cs1,S1,Cs2,S2).
cc_derive_delta_2([C|Cs],Cs0,Cs1,S1,Cs2,S2) :-
%  writeln(C-cs1(Cs1)), wait,
   cardinality_constraints_application_delta([C],Cs1,Cs3,S3),
%  writeln(cs3(Cs3,S3)), wait,
   cc_ord_union(S1,S3,S1_a),
   cardinality_constraints_reduce_by(S3,Cs1,Cs1_a),
   cardinality_constraints_reduce_by(S3,[C|Cs0],Cs0_a),
   ord_subtract(Cs3,Cs1_a,Cs3_a),
   cc_derive_delta_2(Cs3_a,Cs0_a,Cs1_a,S1_a,Cs1_b,S1_b),
   cardinality_constraints_reduce_by(S1_b,Cs,Cs_a),
   ord_union(Cs0_a,Cs3_a,Cs0_b),
   cardinality_constraints_reduce_by(S1_b,Cs0_b,Cs0_c),
   cc_derive_delta_2(Cs_a,Cs0_c,Cs1_b,S1_b,Cs1_c,S1_c),
   cardinality_constraints_reduce(Cs1_c,S,Cs2),
   cc_ord_union(S,S1_c,S2).
cc_derive_delta_2([],_,Cs,S,Cs,S).

cc_derive_delta([C|Cs],Cs1,T1:F1,Cs2,T2:F2) :-
%  writeln(C-cs1(Cs1)),
   cardinality_constraints_application_delta([C],Cs1,Cs3,T3:F3),
%  writeln(cs3(Cs3,T3:F3)),
   cc_derive_delta_next(Cs1,T1:F1,Cs3,T3:F3,Cs3_New,Cs4,T4:F4),
   cc_derive_delta(Cs3_New,Cs4,T4:F4,Cs5,T5:F5),
   cardinality_constraints_reduce_by(T5:F5,Cs,Cs_Reduced),
   cc_derive_delta(Cs_Reduced,Cs5,T5:F5,Cs6,T6:F6),
   cardinality_constraints_reduce(Cs6,T:F,Cs2),
   cc_ord_union(T:F,T6:F6,T2:F2).
cc_derive_delta([],Cs,T:F,Cs,T:F).

cc_derive_delta_next(Cs1,T1:F1,Cs3,[]:[],Cs3,Cs4,T1:F1) :-
   !,
   ord_union(Cs3,Cs1,Cs4).
cc_derive_delta_next(Cs1,T1:F1,Cs3,T3:F3,Cs3_New,Cs4,T4:F4) :-
   cc_ord_union(T1:F1,T3:F3,T1_a:F1_a),
   cardinality_constraints_reduce_by(T3:F3,Cs1,Cs1_a),
   cardinality_constraints_reduce(Cs1_a,T:F,Cs1_b),
   cardinality_constraints_reduce_by(T:F,Cs3,Cs3_a),
   cc_ord_union(T:F,T1_a:F1_a,T4:F4),
   ord_subtract(Cs1_b,Cs1,Cs1_c),
   ord_union(Cs1_c,Cs3_a,Cs3_New),
   ord_union(Cs3_a,Cs1_b,Cs4).


cc_ord_union(T1:F1,T2:F2,T3:F3) :-
   ord_union(T1,T2,T3),
   ord_union(F1,F2,F3).


/******************************************************************/


