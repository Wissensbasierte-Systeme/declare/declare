

/******************************************************************/
/***                                                            ***/
/***       Cardinality Constraints: Charts                      ***/
/***                                                            ***/
/******************************************************************/


chart_measure_colours([
   blue, red, cyan, magenta,
   green, yellow, orange ] ).


/*** interface ****************************************************/


/* cardinality_constraints_measures_display(File,N,Mode,Smooth) <-
      */

cardinality_constraints_measures_display :-
   cardinality_constraints_measures_display(
      'mine_sweeper/mine_sweeper',2,2,5).

cardinality_constraints_measures_display(File,N,Mode,Smooth) :-
   dislog_variable_set(cardinality_constraints_measures,File),
   dislog_variable_set(cc_weight_function,N),
   cardinality_constraints_measures_analysis(Mode,Smooth,
      [ calc-1, smodels-1, lp-1 ] ).
%     [ calc-1, clp_chip-1, clp_sicstus-1 ] ).
%     [ clp_sicstus-1, clp_chip-1, lp-1, lp_chip-1 ] ).
%     [ calc-1, clp-1, lp_opt-1, lp_good-1, lp-1 ] ).
%     [ tps-1, tps-2, pure-1, pure-2 ] ).
%     [ calc-1, lp-1, lp_tree-1, lp_opt-1, lp_good-1, clp-1 ] ).
%     [ calc-1, lp-1, lp_good-1, clp-1, lp_opt-1 ] ).

cardinality_constraints_measures_analysis(Mode,Smooth,Methods) :-
   dislog_variable_get(cardinality_constraints_measures,File),
   dconsult(File,Clp_Inputs),
   maplist_with_status_bar(
      cardinality_constraints_measures_to_coordinates(
         Mode, Smooth, Clp_Inputs ),
      Methods, Charts ),
   ghostview_cardinality_constraints_charts(Charts).

ghostview_cardinality_constraints_charts(Charts) :-
   dislog_variable_set(stock_mode,regular),
   coordinate_system(Coordinate_System),
   log_curve_coordinates(Coordinates),
   charts_plus_colours(Charts,Colour_Charts),
   ghostview_charts( [ Coordinate_System,
      ['',black,0.01,[],[],[],[],Coordinates] | Colour_Charts ] ).

charts_plus_colours(Charts,Colour_Charts) :-
   chart_measure_colours(Colours),
   charts_plus_colours(Charts,Colours,Colour_Charts).

charts_plus_colours([Chart|Charts],[Colour|Colours],
      [['',Colour,0.01,[],[],[],[],Chart]|Colour_Charts]) :-
   charts_plus_colours(Charts,Colours,Colour_Charts).
charts_plus_colours([],_,[]).


/* coordinate_system(Coordinate_System) <-
      */

coordinate_system(Coordinate_System) :-
   maplist( coordinate_transformation,
      [0.01,0.1,1,10,100,1000], [V1,V2,V3,V4,V5,V6] ),
   Coordinate_System = [
      'Cardinality Constraints', blue, 0.01,
      [ [V1,'$10^{-2}$'], [V2,'$10^{-1}$'],
        [V3,'$10^0$'], [V4,'$10^1$'],
        [V5,'$10^2$'], [V6,'$10^3$'] ],
      [ [V1,'$10^{-2}$'], [V2,'$10^{-1}$'],
        [V3,'$10^0$'], [V4,'$10^1$'],
        [V5,'$10^2$'] ], 
      [V1,V2,V3,V4,V5], [V1,V2,V3,V4,V5,V6], [] ].


/* log_curve_coordinates(Coordinates) <-
      */

log_curve_coordinates(Coordinates) :-
   generate_interval(1,65,I),
   vector_multiply(4/50,I,J), 
   maplist( add(8),
      J, K ),
   maplist( log_curve,
      K, Coordinates ).

log_curve(X,X-Y) :-
   Y is 3/2 * X - 12.


/* cardinality_constraints_measures_to_coordinates(
         Mode,Smooth,Clp_Inputs,Method-N,Coordinates) <-
      */

cardinality_constraints_measures_to_coordinates(
      Mode,Smooth,Clp_Inputs,Method-N,Coordinates) :-
   findall( X-Y,
      ( member([clp_input(Constraints,_,_,Times)],Clp_Inputs),
        cardinality_constraints_weight(Mode,Constraints,T1),
%       cardinality_constraints_overlap(Constraints,Overlap),
        Atom =.. [Method,A,B],
        member(Atom,Times),
        nth(N,[A,B],T),
%       T2 is T / Overlap,
        T2 is T,
%       T3 is T1 / (10**(Mode-4)),
        T3 is T1,
        coordinate_transformation(T3,X),
        coordinate_transformation(10*T2,Y) ),
%       coordinate_transformation(T2 / 100,Y) ),
      Coordinates_1 ),
   coordinates_average_x(Coordinates_1,Coordinates_2),
   coordinates_smoothen_y(Smooth,Coordinates_2,Coordinates_3),
%  append(Coordinates_1,Coordinates_3,Coordinates).
   Coordinates = Coordinates_3,
   writeln(user,Method-N).


/*
ghostview_charts(Charts) :-
   values_to_latex_file_muliple(Charts),
   preview_latex_file.

values_to_latex_file_muliple(Charts) :-
   stock_file(file_input,Chart_file_input),
   writeln('---> output file: '),
   writeln(Chart_file_input),
   tell(Chart_file_input),
   checklist( values_to_latex,
      Charts ),
   told.

values_to_latex(Chart) :-
   Goal =.. [values_to_latex,Chart],
   call(Goal).
*/


/* coordinate_transformation(X1,X2) <-
      */

coordinate_transformation(X1,X2) :-
%  log(10,100*(X1+0.001),X3),
   log(10,100*(X1+0.000001),X3),
   round(2*X3+2,2,X2).


/* cardinality_constraints_overlap(Constraints,Overlap) <-
      */

cardinality_constraints_overlap(Constraints,Overlap) :-
   cardinality_constraints_to_dlp(b,Constraints,Program),
   dlp_to_state(Program,State),
   state_overlap_ratio(State,Ratio),
%  Overlap is 1.5 * ( Ratio ** 2 ) / 1000.
   Overlap is Ratio ** 0.5.

dlp_to_state(Program,State) :-
   maplist( rule_to_clause,
      Program, State ).
   
rule_to_clause([]-As,As) :-
   !.
rule_to_clause(As-[],As) :-
   !.
rule_to_clause(As,As).
   

/* cardinality_constraints_weight(Mode,Constraints,Weight) <-
      */

cardinality_constraints_weight(Mode,Constraints,Weight) :-
   maplist( cardinality_constraint_weight(Mode),
      Constraints, Weights ),
   add(Weights,Weight),
   !.

cardinality_constraint_weight(Exponent,Xs-_,Weight) :-
   dislog_variable_get(cc_weight_function,1),
   length(Xs,M),
   round(M ** Exponent,2,Weight),
   !.
cardinality_constraint_weight(Exponent,Xs-K,Weight) :-
   dislog_variable_get(cc_weight_function,2),
   length(Xs,M),
   cc_weight_function(Exponent,M,K,Weight),
   !.


/* cc_weight_function(Exponent,M,K,Weight) <-
      */

cc_weight_function(Exponent,M,K,Weight) :-
   K1 is K + 1,
   K2 is M - K + 1,
   binomial(M,K1,W1),
   binomial(M,K2,W2),
   power(K1,Exponent,V1),
   power(K2,Exponent,V2),
   Weight is W1*V1 + W2*V2.


/* coordinates_average_x(Coordinates_1,Coordinates_2) <-
      */

coordinates_average_x(Coordinates_1,Coordinates_2) :-
   findall( X,
      member(X-Y,Coordinates_1),
      X_List ),
   list_to_ord_set(X_List,X_Values),
   findall( X-Y_Avg,
      ( member(X,X_Values),
        findall( Y,
           member(X-Y,Coordinates_1),
           Ys ),
        add(Ys,Y_Sum),
        length(Ys,N),
%       round(Y_Sum/N,2,Y_Avg) ),
        Y_Avg is Y_Sum/N ),
      Coordinates_2 ).


/* coordinates_smoothen_y(Smooth,Coordinates_1,Coordinates_2) <-
      */

coordinates_smoothen_y(Smooth,Coordinates_1,Coordinates_2) :-
   maplist( coordinate_to_stock_atom,
      Coordinates_1, State_1 ),
   stock_state_smooth(Smooth,State_1,State_2),
   maplist( stock_atom_to_coordinate,
      State_2, Coordinates_2 ).

coordinate_to_stock_atom(X-Y,[stock(c,c,X,Y,c)]).

stock_atom_to_coordinate([stock(c,c,X,Y,c)],X-Y).


/******************************************************************/


