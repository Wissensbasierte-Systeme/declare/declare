

/******************************************************************/
/***                                                            ***/
/***       Cardinality Constraints: Main                        ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(
      cardinality_constraints_minimal_model_state, mms ).


/*** interface ****************************************************/


/* cardinality_constraints_to_dlp(Constraints, Program) <-
      A set Constraints of cardinality constraints is represented
      by a dlp Program consisting of disjunctive facts and
      denials.   */

cardinality_constraints_to_dlp(Constraints, Program) :-
   cardinality_constraints_to_dlp(a, Constraints, Program).

cardinality_constraints_to_dlp(Mode, Constraints, Program) :-
   ( Mode = a
   ; Mode = b ),
   maplist( cardinality_constraint_to_dlp(Mode),
      Constraints, Programs ),
   append(Programs, Program).

cardinality_constraint_to_dlp(a, Set-K, Program) :-
   subsets_with_cardinality(Set, K, Conjunctions),
   tree_based_boolean_dualization(Conjunctions, Facts),
   M is K + 1,
   subsets_with_cardinality(Set, M, Denial_Bodies),
   maplist( denial_body_to_denial,
      Denial_Bodies, Denials ),
   append(Facts, Denials, Program).
cardinality_constraint_to_dlp(b, Set-K, Program) :-
   length(Set, N),  L is N - K + 1,
   subsets_with_cardinality(Set, L, Facts),
   M is K + 1,
   subsets_with_cardinality(Set, M, Denial_Bodies),
   maplist( denial_body_to_denial,
      Denial_Bodies, Denials ),
   append(Facts, Denials, Program).
cardinality_constraint_to_dlp(interval, Set-[K1, K2], Program) :-
   length(Set, N),  L is N - K1 + 1,
   subsets_with_cardinality(Set, L, Facts),
   M is K2 + 1,
   subsets_with_cardinality(Set, M, Denial_Bodies),
   maplist( denial_body_to_denial,
      Denial_Bodies, Denials ),
   append(Facts, Denials, Program).

denial_body_to_denial(Atoms, []-Atoms).


/* cardinality_constraints_simplify(Cs1, Cs2) <-
      */

cardinality_constraints_simplify(Cs1, Cs2) :-
   maplist( cardinality_constraint_simplify(Cs1),
      Cs1, Cs2 ).

cardinality_constraint_simplify(Constraints, Xs1-N1, Xs2-N2) :-
   findall( Xs,
      ( member(Xs3-N1, Constraints),
        ord_subset(Xs3, Xs1),
        Xs1 \== Xs3,
        ord_subtract(Xs1, Xs3, Xs) ),
      List_of_Xs ),
   ord_union(List_of_Xs, Xs4),
   ( ( Xs4 \= [], !,
       Xs2-N2 = Xs4-0 )
   ; ( Xs2-N2 = Xs1-N1 ) ).


/* cardinality_constraints_definite_conclusions(Cs, True, False) <-
      */

cardinality_constraints_definite_conclusions(Cs, True, False) :-
   findall( Xs,
      ( member(Xs-N, Cs),
        length(Xs, N) ),
      List_of_True ),
   ord_union(List_of_True, True),
   findall( Xs,
      member(Xs-0, Cs),
      List_of_False ),
   ord_union(List_of_False, False).


/* cardinality_constraint_definite(Truth_Value, Xs-N) <-
      */

cardinality_constraint_definite(true, Xs-N) :-
   Xs \= [],
   length(Xs, N).
cardinality_constraint_definite(false, Xs-0) :-
   Xs \= [].


/* cardinality_constraints_analyse_program(
         Program, State, State_Dual, Models) <-
      */

cardinality_constraints_analyse_program(
      Program, State, State_Dual, Models) :-
   minimal_models(Program, Models),
   pure_ground_program_to_model_state(Program, State),
   dlp_to_dual_program(Program, Program_Dual),
   pure_ground_program_to_model_state(Program_Dual,State_Dual).
 
pure_ground_program_to_model_state(Program, State) :-
   dislog_variable_get(
      cardinality_constraints_minimal_model_state, mms ),
   minimal_model_state(Program, State).
pure_ground_program_to_model_state(Program, State) :-
   dislog_variable_get(
      cardinality_constraints_minimal_model_state, pph ),
   pure_positive_hyperresolution_fixpoint(Program, State).


/******************************************************************/


