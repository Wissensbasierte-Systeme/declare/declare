

/******************************************************************/
/***                                                            ***/
/***       Cardinality Constraints: Elementary                  ***/
/***                                                            ***/
/******************************************************************/


 
/*** interface ****************************************************/


/* models_to_distribution(Models,Distribution) <-
      */

models_to_distribution(Models,Distribution) :-
   append(Models,Atoms),
   length(Models,L),
   findall( Atom:N,
      ( member(Atom,Atoms),
        findall( Model,
           ( member(Model,Models),
             member(Atom,Model) ),
           Models_with_Atom ),
        length(Models_with_Atom,K),
        round(K / L,1,N) ),
      Distribution ),
   writeln(user,Distribution).


/* state_select_singletons(State,Atoms) <-
      */

state_select_singletons(State,Atoms) :-
  findall( Atom,
      member([Atom],State),
      Atoms ).


/* set_canonize(Subsumption_Predicate,Set1,Set2) <-
      */

set_canonize(Subsumption_Predicate,Set1,Set2) :-
   findall( X,
      ( member(X,Set1),
        \+ ( member(Y,Set1),
             Goal =.. [Subsumption_Predicate,Y,X],
             call(Goal) ) ),
      Set2 ).


/* divide_save(X,Y,Q) <-
      */

divide_save('-',_,'-') :-
   !.
divide_save(_,'-','-') :-
   !.
divide_save(_,0,'-') :-
   !.
divide_save(0,_,'-') :-
   !.
divide_save(X,Y,Q) :-
   Q is X / Y.
             

/******************************************************************/


