

/******************************************************************/
/***                                                            ***/
/***       Cardinality Constraints: Pure Hyperresolution        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* pure_positive_hyperresolution_fixpoint(
         Program, New_State ) <-
      */

pure_positive_hyperresolution_fixpoint(Program, New_State) :-
   program_select_facts(Program, State),
   program_to_denial_tree(Program, Tree),
   pure_positive_hyperresolution_fixpoint(Tree, State, New_State).

pure_positive_hyperresolution_fixpoint(Tree, State, New_State) :-
   pure_positive_hyperresolution(
      Tree, State, Delta_State, State_2),
   ( ( Delta_State \= [],
       !,
       pure_positive_hyperresolution_fixpoint(
          Tree, State_2, New_State) )
   ; New_State = State_2 ).


/* pure_positive_hyperresolution(
         Tree, State, New_State, Union_State ) <-
      */

pure_positive_hyperresolution(
      Tree, State, New_State, Union_State) :-
   pure_positive_hyperresolution(Tree, State, S1),
   state_subtract(S1, State, New_State),
   state_union(State, New_State, S2),
   state_can(S2, Union_State),
   !.

pure_positive_hyperresolution(Tree, State, New_State) :-
   state_resolve_tree(State, Tree, New_State).
   

/* pure_positive_hyperresolution(Program, New_State) <-
      */

pure_positive_hyperresolution(Program, New_State) :-
   program_select_facts(Program, State),
   program_to_denial_tree(Program, Tree),
   pure_positive_hyperresolution(Tree, State, New_State).


/* program_to_denial_tree(Program,Tree) <-
      */

program_to_denial_tree(Program,Tree) :-
   program_to_denial_state(Program, State),
   state_to_tree(State, Tree).


/* program_to_denial_state(Program, State) <-
      */

program_to_denial_state(Program, State) :-
   findall( Body,
      ( member(Rule, Program),
        parse_dislog_rule_save(Rule, [], Body, []) ),
      State ).


/* program_select_facts(Program, State) <-
      */

program_select_facts(Program, State) :-
   sublist( rule_is_disjunctive_fact,
      Program, Program_2 ),
   maplist( simplify_disjunctive_fact,
      Program_2, State ).

rule_is_disjunctive_fact(Rule) :-
   parse_dislog_rule_save(Rule, _, [], []).

simplify_disjunctive_fact(Rule, C) :-
   parse_dislog_rule_save(Rule, C, [], []).


/* state_resolve_tree(State,Tree, New_State) <-
      */

state_resolve_tree(_,[],[]) :-
   !.
state_resolve_tree(State, Tree, New_State) :-
   state_resolve_tree_loop(State, Tree, New_State).

state_resolve_tree_loop(State, [Node], New_State) :-
   !,
   state_resolve_node(State, Node, New_State).
state_resolve_tree_loop(State, [Node|Trees], New_State) :-
   state_resolve_node(State, Node, Node_State),
   maplist( state_resolve_tree_loop(State),
      Trees, States ),
   ord_union(States, Union_State),
   state_disjunction_subtract_can(State,
      Node_State, Union_State, New_State).
state_resolve_tree_loop(_,[],[[]]).

state_resolve_node(State, Node, New_State) :-
   maplist( state_resolve(State),
      Node, States ),
   iterate_list( state_disjunction_subtract_can(State),
      [[]], States, New_State ).
   

/* state_disjunction_subtract_can(State, S1, S2, S3) <-
      */

state_disjunction_subtract_can(State, S1, S2, S3) :-
   state_disjunction_optimized(S1, S2, S4),
   state_subtract(S4, State, S5),
   state_can(S5, S3).


/******************************************************************/


