

/******************************************************************/
/***                                                            ***/
/***       Mine Sweeper: Charts in Star Office                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


cardinality_constraints_measures_analysis_so :-
   dislog_variable_get(output_path,Path),
   name_append(Path,'c_constraints.csv',File),
   tell(File),
   cardinality_constraints_measures_analysis_so( 2, 5,
      [calc-1,lp-1,pure-1,pure-2] ),
   told.

cardinality_constraints_measures_analysis_so(Mode,Smooth,Methods) :-
   dislog_variable_get(cardinality_constraints_measures,File),
   dconsult(File,Clp_Inputs),
   maplist( cardinality_constraints_measures_to_coordinates_so(
      Mode,Smooth,Clp_Inputs),
      Methods, [C1,C2|_] ),
   generate_interval(1,250,I),
   findall( B,
      ( member(X,I),
%       A is round(100 * log(X)),
        member(X-Y1,C1), member(X-Y2,C2),
        Z1 is Y1 + 0.000000000000000000000001,
        Z2 is Y2 + 0.000000000000000000000001,
%       log(10,Z1/Z2,Z),
%       round(Z,2,B),
%       round(log(Z1/Z2),2,B),
        round(Z1/Z2,3,B),
        name_exchange_elements([".,"],X,X1),
        name_exchange_elements([".,"],B,B1),
        write_list([X1,'\t',B1]), nl ),
      Factors ),
   length(Factors,N),
   add(Factors,S), round(S/N,2,Arithmetic),
   multiply(Factors,P), round(P**(1/N),2,Geometric),
   writeln(user,''),
   write(user,'number of measurements: '), writeln(user,N),
   write(user,'arithmetic mean value: '), writeln(user,Arithmetic),
   write(user,'geometric mean value: '), writeln(user,Geometric).


cardinality_constraints_measures_to_coordinates_so(
      Mode,_Smooth,Clp_Inputs,Method-N,Coordinates) :-
   findall( X-Y,
      ( member([clp_input(Constraints,_,_,Times)],Clp_Inputs),
        cardinality_constraints_weight(Mode,Constraints,X),
        ( ( N = 1, Atom =.. [Method,Y,_] )
        ; ( N = 2, Atom =.. [Method,_,Y] ) ),
        member(Atom,Times) ),
      Coordinates_1 ),
   coordinates_average_x(Coordinates_1,Coordinates_2),
   Coordinates = Coordinates_2.
%  coordinates_smoothen_y(Smooth,Coordinates_2,Coordinates).


multiply([X|Xs],Y) :-
   multiply(Xs,Z),
   Y is X * Z.
multiply([],1).


/******************************************************************/


