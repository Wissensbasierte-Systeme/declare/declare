

/******************************************************************/
/***                                                            ***/
/***       Mine Sweeper: Playing on the Board                   ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


/* mine_sweeper_board_update(Bombs, Frees, Distribution) <-
      */

mine_sweeper_board_update([], [], Distribution) :-
   !,
   checklist( send_label_2(12),
      Distribution ),
   refresh_board.
mine_sweeper_board_update(Bombs, Frees, _) :-     
   checklist( investigate_field,
      Frees ),
   value_to_color(true, Red),
   checklist( send_colour(Red),
      Bombs ),
   checklist( send_label(''),
      Bombs ),
   refresh_board.


/* investigate_field_address(Field_Address) <-
      */

investigate_field_address(Field_Address) :-
   get_atom_with_properties(
      [field_address:Field_Address, x:X, y:Y]),
   investigate_field(X-Y).

investigate_field(X-Y) :-
   get_atom_with_properties(
      [x:X, y:Y, value:true]),
   value_to_color(true, Red),
   send_colour(Red, X-Y),
   send_label(+, X-Y),
   !.
investigate_field(X-Y) :-
   value_to_color(false, Green),
   get_number_of_bombs(X-Y, N),
   ( ( N = 0, !, send_label('', X-Y) )
   ; send_label(N, X-Y) ),
   send_colour(Green, X-Y),
   investigate_neighbours(X-Y, N),
   !.

investigate_neighbours(X-Y, 0) :-
%  writeln(user, investigating_neighbours_of(X-Y)),
   value_to_color(false, Green),
   findall( Xn-Yn,
      ( neighbour_of_generate(X-Y, Xn-Yn),
        get_atom_with_properties(
           [x:Xn, y:Yn, colour:Colour]),
        Colour \= Green ),
      Neighbour_Fields ),
   checklist( investigate_field,
      Neighbour_Fields ),
   !.
investigate_neighbours(_, N) :-
   N > 0.


/* get_number_of_bombs(X-Y, N) <-
      */

get_number_of_bombs(X-Y, N) :-
   findall( Xn-Yn,
      ( neighbour_of_generate(X-Y, Xn-Yn),
        get_atom_with_properties([x:Xn, y:Yn, value:true]) ),
      Neighbours_with_Bombs ),
   length(Neighbours_with_Bombs, N),
%  writeln(user, calculated(X-Y, N)),
   !.


fields_with_colour(Colour, Fields) :-
   findall( X-Y,
      ( get_atom_with_properties(
           [x:X, y:Y,
            colour:Colour]) ),
      Fields ).


neighbours_with_colour(Colour, X-Y, Neighbours) :-
   findall( X1-Y1,
      ( neighbour_of_generate(X-Y, X1-Y1),
        get_atom_with_properties(
           [x:X1, y:Y1, colour:Colour]) ),
      Neighbours_List ),
   list_to_ord_set(Neighbours_List,Neighbours).


neighbour_of_generate(X1-Y1, X2-Y2) :-
   member(X, [-1,0,1]), member(Y, [-1,0,1]), X-Y \= 0-0,
   X2 is X1 + X,  Y2 is Y1 + Y.


send_value(Value, X-Y) :-
   get_atom_with_properties(Atom,
      [x:X, y:Y]),
   set_atom_properties(Atom,
      [value:Value]).


/* send_colour_to_address(Colour, Field_Address) <-
      */

send_red_colour_to_address(Field_Address) :-
   value_to_color(true, Red),
   send_colour_to_address(Red, Field_Address).

send_colour_to_address(Colour, Field_Address) :-
   get_atom_with_properties(Atom,
      [field_address:Field_Address]),
   send(Field_Address, fill_pattern, Colour),
   set_atom_properties(Atom, [colour:Colour]).


/* send_colour(Colour, X-Y) <-
      */

send_colour(Colour, X-Y) :-
   get_atom_with_properties(Atom,
      [field_address:Field_Address, x:X, y:Y]),
   send(Field_Address, fill_pattern, Colour),
   set_atom_properties(Atom, [colour:Colour]).


/* send_label(Text, X-Y) <-
      */

send_label_2(Size, X-Y:Text) :-
   send_label(Text, X-Y, Size).

send_label(Text, X-Y) :-
   send_label(Text, X-Y, 18).

send_label(Text, X-Y, Text_Size) :-
   dislog_variable_get(board_address, Board),
   dislog_variable_get(mine_sweeper_field_size, Field_Size),
   get_atom_with_properties(Atom,
      [text_address:Text_Address, x:X, y:Y]),
   send(Text_Address, destroy),
   X1 is (X - 1) * 50 + 19,  Y1 is (Y - 1) * 50 + 15,
   vector_multiply(Field_Size/50, [X1, Y1], [X2, Y2]),
   send(Board, display,
      new(Text_Address_2, text(Text)), point(X2, Y2) ),
   send(Text_Address_2, font, font(times, bold, Text_Size)),
   set_atom_properties(Atom,
      [label:Text, text_address:Text_Address_2]).


/* set_atom_properties(Atom, Properties) <-
      */

set_atom_properties(Atom, Properties) :-
   set_properties(Properties, Atom, Atom_2),
   retract(Atom),
   assert(Atom_2).

set_properties([P|Ps], Atom_1, Atom_2) :-
   set_properties(Ps, Atom_1, Atom_3),
   set_property(P, Atom_3, Atom_2).
set_properties([], Atom, Atom).

set_property(Property:Value, Atom_1, Atom_2) :-
   Atom_1 =.. [Predicate, Arguments_1],
   substitute_tuple(Arguments_1,
      [[Property:_, Property:Value]], Arguments_2),
   Atom_2 =.. [Predicate, Arguments_2].


/* get_atom_with_properties(Properties) <-
      */

get_atom_with_properties(Properties) :-
   get_atom_with_properties(_, Properties).

get_atom_with_properties(field(Tuple), Properties) :-
   field(Tuple),
   checklist( get_property(Tuple),
      Properties ).

get_property(Tuple, Property:Value) :-
   member(Property:Value, Tuple).


/******************************************************************/


