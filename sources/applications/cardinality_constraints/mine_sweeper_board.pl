

/******************************************************************/
/***                                                            ***/
/***       Mine Sweeper: Board                                  ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(mine_sweeper_board_size, 10-10),
   dislog_variable_set(mine_sweeper_field_size, 40).


% value_to_color(true, colour(red)).
% value_to_color(true, colour(blue)).

  value_to_color(true, Colour) :-
     rgb_to_colour([13, 0, 0], Colour).

% value_to_color(false, colour(green)).
  value_to_color(false, colour(grey)).

% value_to_color(unknown, colour(yellow)).
% value_to_color(unknown, colour(white)).

  value_to_color(unknown, Colour) :-
%    rgb_to_colour([14.5, 14.5, 14.5], Colour).
     rgb_to_colour([13.5, 13.5, 13.5], Colour).


/*** interface ****************************************************/


/* mine_sweeper <-
      */

mine_sweeper :-
   create_board.


/* create_board <-
      */

create_board :-
   new(Frame, dialog('Mine Sweeper (by D. Seipel)')),
   send(Frame, append,
      new(BTS, dialog_group(buttoms, group))),
   checklist( send_append(BTS), [
       'New Game' - new_game,
%      'Clear' - clear_board - right,
       'Show' - show_board - right,
       'Help' - mine_sweeper_decision_support(single) - right,
       'Help*' - mine_sweeper_decision_support(star) - right ]),
   send(BTS, append, button('Quit',
      message(Frame, destroy)), right),
   send(BTS, layout_dialog),
   send(Frame, append,
      new(Board, picture('Board')), below),
   dislog_variable_get(mine_sweeper_board_size, N-M),
   create_fields(Board, N-M),
   dislog_variable_get(mine_sweeper_field_size, Size),
   X_Size is Size * N + 7,
   Y_Size is Size * M + 7,
   send(Board, size, size(X_Size, Y_Size)),
   dislog_variable_set(board_address, Board),
   send(Frame, open).

new_game :-
   clear_board,
   colour_board_save.


/* refresh_board <-
      */

refresh_board :-
   dislog_variable_get(board_address, Board),
   send(Board, flush).


/* clear_board <-
      */

clear_board :-
   dislog_variable_get(mine_sweeper_board_size, N-M),
   value_to_color(unknown, Yellow),
   forall(
      ( number_in_interval(X, [1, N]),
        number_in_interval(Y, [1, M]) ),
      ( send_value(false, X-Y),
        send_colour(Yellow, X-Y),
        send_label('', X-Y) ) ).


/* colour_board <-
      */

colour_board_save :-
   colour_board,
   !.
colour_board_save :-
   colour_board_save.

colour_board :-
   dislog_variable_get(mine_sweeper_board_size, N-M),
   K is N * M,  L is K // 6,
   randoms(L, K, Numbers),
   maplist( number_to_field_address,
      Numbers, Addresses ),
   checklist( send_value(true),
      Addresses ).

number_to_field_address(Number, X-Y) :-
   dislog_variable_get(mine_sweeper_board_size, N-_),
   X is 1 + ((Number - 1) mod N),
   Y is 1 + ((Number - 1) // N).


/* create_fields(Board, N-M) <-
      */

create_fields(Board, N-M) :-
   retractall(field(_)),
   forall(
      ( number_in_interval(X, [1, N]),
        number_in_interval(Y, [1, M]) ),
      create_field(Board, _, X-Y) ).

create_field(Board, Field, X-Y) :-
   dislog_variable_get(mine_sweeper_field_size, Size),
   vector_multiply(Size, [X-1, Y-1], [X1, Y1]),
   vector_add([X1, Y1], [19*Size/50, 15*Size/50], [X2, Y2]),
   B is 47*Size/50,
   send(Board, display,
      new(Field, box(B, B)), point(X1, Y1) ),
   value_to_color(unknown, Yellow),
   send(Field, fill_pattern, Yellow),
   send(Field, colour, Yellow),
   send(Board, display,
      new(Text, text('')), point(X2, Y2) ),
   send(Text, font, font(times, bold, 18)),
   assert( field([
      field_address: Field, x: X, y: Y,
      value: false, status: unknown, colour: Yellow,
      text_address: Text, label: '' ]) ),
   new(Cl, click_gesture(left, '', single,
      message(@prolog,
         investigate_field_address, @receiver)) ),
   send(Field, recogniser, Cl),
   new(Cr, click_gesture(right, '', single,
      message(@prolog,
         send_red_colour_to_address, @receiver)) ),
   send(Field, recogniser, Cr).


/* show_board <-
      */

show_board :-
   new(Frame, dialog('Mine Sweeper')),
   send(Frame, append,
      new(BTS, dialog_group(buttoms, group))),
   send(BTS, append, button('Quit',
      message(Frame, destroy))),
   send(BTS, layout_dialog),
   send(Frame, append,
      new(Board, picture('Board')), below),
   dislog_variable_get(mine_sweeper_board_size, N-M),
   forall(
      ( number_in_interval(X, [1, N]),
        number_in_interval(Y, [1, M]) ),
      show_field(Board, X-Y) ),
   dislog_variable_get(mine_sweeper_field_size, Size),
   X_Size is Size * N + 7,
   Y_Size is Size * M + 7, 
   send(Board, size, size(X_Size, Y_Size)),
   send(Frame, open).

show_field(Board,X-Y) :-
   get_atom_with_properties(
      [x:X, y:Y, value:Value]),
   value_to_color(Value, Colour),
   dislog_variable_get(mine_sweeper_field_size, Size),
   vector_multiply(Size, [X-1, Y-1], [X1, Y1]),
   vector_add([X1, Y1], [19*Size/50, 15*Size/50], [X2, Y2]),
   B is 47*Size/50,
   send(Board, display,
      new(Field, box(B, B)), point(X1, Y1) ),
   send(Field, fill_pattern, Colour),
   send(Field, colour, Colour),
   send(Board, display,
      new(Text, text('')), point(X2, Y2) ),
   send(Text, font, font(times, bold, 18)).


/* board_to_cardinality_constraints(Cardinality_Constraints) <-
      */

board_to_cardinality_constraints(Cardinality_Constraints) :-
   value_to_color(false, Green),
   fields_with_colour(Green, Fields),
   findall( Unknown_Neighbours-M,
      ( member(X-Y, Fields),
        coordinates_to_cardinality_constraint(
           X-Y, Unknown_Neighbours-M ),
        Unknown_Neighbours \= [] ),
      Constraints_List ),
   list_to_ord_set(Constraints_List, Constraints),
   cardinality_constraints_simplify(
      Constraints, Cardinality_Constraints ).

coordinates_to_cardinality_constraint(X-Y, Unknown_Neighbours-M) :-
   value_to_color(unknown, Yellow),
   value_to_color(true, Red),
   neighbours_with_colour(Yellow, X-Y, Unknown_Neighbours),
   neighbours_with_colour(Red, X-Y, Red_Neighbours),
   length(Red_Neighbours, K),
   get_number_of_bombs(X-Y, N),
   M is N - K.


/* number_in_interval(I, [N, M]) <-
      */

number_in_interval(I, [N, M]) :-
   generate_interval(N, M, Interval),
   member(I, Interval). 


/******************************************************************/


