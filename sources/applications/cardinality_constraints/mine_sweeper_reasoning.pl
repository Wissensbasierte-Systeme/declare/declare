

/******************************************************************/
/***                                                            ***/
/***       Mine Sweeper: Reasoning Component                    ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


:- dynamic
      clp_input/4.


:- dislog_variables_set([
      mine_sweeper_ds -
         model_generation,
%     mine_sweeper_ds -
%        state_generation,
      cardinality_constraints_measures -
         'mine_sweeper/mine_sweeper',
      cardinality_constraints_measures_2 -
         'mine_sweeper/mine_sweeper_2',
      cardinality_constraints_imply_mode - tree ]).


/* mine_sweeper_decision_support(Mode) <-
      */

mine_sweeper_decision_support(Mode) :-
   board_to_cardinality_constraints(Cardinality_Constraints),
   Cardinality_Constraints \= [], !,
   cardinality_constraints_implication_fixpoint(
      Cardinality_Constraints, Simplified_Cardinality_Constraints ),
   cardinality_constraints_definite_conclusions(
      Simplified_Cardinality_Constraints, Bombs, Frees ),
   ( ( Bombs = [], Frees = [], !,
       mine_sweeper_decision_support(Mode,
          Simplified_Cardinality_Constraints) )
   ; ( mine_sweeper_board_update(Bombs,Frees,[]),
       mine_sweeper_decision_support_if(Mode,Bombs,Frees) ) ).
mine_sweeper_decision_support(_).


mine_sweeper_decision_support(Mode,Cardinality_Constraints) :-
   cardinality_constraints_to_dlp(b,Cardinality_Constraints,Program),
   start_timer(mine_sweeper_decision_support),
   mine_sweeper_decision_support(Program,Bombs,Frees,Distribution),
   mine_sweeper_board_update(Bombs,Frees,Distribution),
   stop_timer(mine_sweeper_decision_support,Time),
   assert(clp_input(Cardinality_Constraints,Bombs,Frees,Time)),
   mine_sweeper_decision_support_if(Mode,Bombs,Frees).

mine_sweeper_decision_support_if(_,[],[]) :-
   !.
mine_sweeper_decision_support_if(star,_,_) :-
   !,
   mine_sweeper_decision_support(star).
mine_sweeper_decision_support_if(_,_,_).


mine_sweeper_decision_support(Program,Bombs,Frees,Distribution) :-
   dislog_variable_get(mine_sweeper_ds,model_generation), !,
%  minimal_models(Program,Models),
   writeln(user, mine_sweeper_ds:model_generation_dlv),
   ttyflush,
   program_to_stable_models_dlv(Program,Models),
   writeln(user,'models - bombs'),
   dportray(chs,Models),
   ord_intersection(Models,Bombs),
   mine_sweeper_decision_support_if(
      Program,Models,Bombs,Frees,Distribution).
mine_sweeper_decision_support(Program,Bombs,Frees,Distribution) :-
   dislog_variable_get(mine_sweeper_ds,state_generation), !,
   cardinality_constraints_analyse_program(
      Program,Bomb_State,Free_State,Models),
   writeln(user,'models'),
   dportray(chs,Models), nl,
   writeln(user,'bombs are located at'),
   dportray(dhs,Bomb_State), nl,
   writeln(user,'free fields are at'),
   dportray(dhs,Free_State),
   state_select_singletons(Bomb_State,Bombs),
   state_select_singletons(Free_State,Frees),
   models_to_distribution(Models,Distribution).

mine_sweeper_decision_support_if(Program,Models,[],Frees,Distribution) :-
   !,
   dlp_to_dual_program(Program,Dual_Program),
   minimal_models(Dual_Program,Models_Dual),
   writeln(user,'models - free'),
   dportray(chs,Models_Dual),
   ord_intersection(Models_Dual,Frees),
   models_to_distribution(Models,Distribution).
mine_sweeper_decision_support_if(_,_,_,[],[]).
   

/******************************************************************/


