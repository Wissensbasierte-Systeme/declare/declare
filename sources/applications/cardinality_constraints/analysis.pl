

/******************************************************************/
/***                                                            ***/
/***       Cardinality Constraints: Analysis                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* cc_analysis_all <-
      */

cc_analysis_all :-
   cc_analysis_all([pure]).

cc_analysis_all(Methods) :-
%  start_timer(cardinality_constraints_derive),
%  start_timer(cardinality_constraints_reduce),
   dislog_variable_get(cardinality_constraints_measures,File),
   dconsult(File,Clp_Inputs),
   length(Clp_Inputs,N),
   start_status_bar(
      cc_analysis_all,'Analysing Cardinality Constraints',N),
   findall( [clp_input(Constraints,True,False,Times)],
      ( member([clp_input(Constraints,True,False,Ts)],Clp_Inputs),
        cc_analysis_all(Methods,Constraints,True,False,Times_2),
        status_bar_increase(cc_analysis_all),
        add_clp_time_2(Ts,Times_2,Times),
        clp_input_add_to_file(
           clp_input(Constraints,True,False,Times)) ),
      Clp_Outputs ),
   status_bar_disappear(cc_analysis_all),
   dislog_variable_get(cardinality_constraints_measures_2,File_2),
   write_list(user,['---> ',File_2]), nl(user),
   dsave(Clp_Outputs,File_2),
%  write('derive '), stop_timer(cardinality_constraints_derive),
%  write('reduce '), stop_timer(cardinality_constraints_reduce),
   us('rm xxx yyy').


/* cc_analysis_calculi_compare <-
      */

cc_analysis_calculi_compare :-
   dislog_variable_get(cardinality_constraints_measures,File),
   dconsult(File,Clp_Inputs),
   findall( [clp_input(Constraints,True:False,T:F)],
      ( member([clp_input(Constraints,True,False,_)],Clp_Inputs),
        cardinality_constraints_calculus( improved,
           Constraints, _, T:F ),
        True:False \= T:F ),     
      Clp_Outputs ),
   dislog_variable_get(cardinality_constraints_measures_2,File_2),
   write('---> '), writeln(File_2), 
   dsave(Clp_Outputs,File_2).


/*** implementation ***********************************************/


/* cc_analysis_all(Methods,Constraints,True,False,Times) <-
      */

cc_analysis_all(Methods,Constraints,True,False,Times) :-
   findall( Measure,
      ( member(Method,Methods),
        Measure =.. [Method,_,_],
        cc_analysis(
           Measure,Constraints,T:F),
        cc_analysis_alarm(
           Measure,Constraints,True:False,T:F) ),
      Times ).
   
cc_analysis_alarm(_,_,_:_,T:F) :-
   var(T),
   var(F),
   !.
cc_analysis_alarm(_,_,True:False,T:F) :-
   list_to_ord_set(True,T),
   list_to_ord_set(False,F),
   !.
cc_analysis_alarm(Measure,Constraints,True:False,T:F) :-
   writeln(user,'alarm'),
   writeln(user,Constraints),
   writeln(user,(Measure,True:False,T:F)).


/* cc_analysis(Method(T1,T2),Constraints,True:False) <-
      */

cc_analysis(lp_good(T1,-),Constraints,True:False) :-
   cc_analysis_measure(
      opt_2, T1, Constraints, True:False ).

cc_analysis(lp(T1,-),Constraints,True:False) :-
   dislog_variables_set([
      cardinality_constraints_imply_mode: regular,
      cardinality_constraints_reduce_mode: late ]),
   cc_analysis_measure(
      improved, T1, Constraints, True:False ).

cc_analysis(lp_opt(T1,-),Constraints,True:False) :-
   dislog_variables_set([
      cardinality_constraints_imply_mode: regular,
      cardinality_constraints_reduce_mode: early ]),
   cc_analysis_measure(
      improved, T1, Constraints, True:False ).

cc_analysis(lp_tree(T1,-),Constraints,True:False) :-
   dislog_variables_set([
      cardinality_constraints_imply_mode: tree ]),
   cc_analysis_measure(
      improved, T1, Constraints, True:False ).

cc_analysis(calc(T1,-),Constraints,True:False) :-
   cc_analysis_measure(
      basic, T1, Constraints, True:False ).

cc_analysis(pure(T1,T2),Constraints,_True:_False) :-
   cc_analysis_generic(
      pure_positive_hyperresolution_fixpoint,
      T1-T2, Constraints ).

cc_analysis(tps(T1,T2),Constraints,_True:_False) :-
   cc_analysis_generic(
      tps_fixpoint_iteration,T1-T2,Constraints).

cc_analysis(tpi(T1,T2),Constraints,_True:_False) :-
   cc_analysis_generic(
      minimal_models,T1-T2,Constraints).

cc_analysis(smodels(Time,-),Constraints,True:False) :-
   measure_until_measurable(
      ( cardinality_constraints_to_models_with_smodels(
           Constraints,Models),
        cardinality_constrains_and_models_to_tf(
           Constraints,Models,True:False) ),
      Time ),
   !.


/* cc_analysis_measure(
         Mode,Time,Constraints,True:False) <-
      */

cc_analysis_measure(Mode,Time,Constraints,True:False) :-
   measure_until_measurable(
      cardinality_constraints_calculus(Mode,
         Constraints, _, True:False ),
      Time ).
% Time is T * 1000.


/* cc_analysis_generic(Method,T1-T2,Constraints) <-
      */

cc_analysis_generic(Method,T1-T2,Constraints) :-
   cardinality_constraints_to_dlp(Constraints,Program),
   Goal =.. [Method,Program,_],
   measure_until_measurable( Goal, T1 ),
   dlp_to_dual_program(Program,Dual_Program),
   Dual_Goal =.. [Method,Dual_Program,_],
   measure_until_measurable( Dual_Goal, T2 ),
%  T1 is Ta * 1000,
%  T2 is Tb * 1000,
   !.


/* add_clp_time_<1,2>(Ts,Times_2,Times) <-
      */

add_clp_time_1(Ts,Times_2,Times) :-
   member(clp(T1,T2),Ts),
   !,
   Times = [clp(T1,T2)|Times_2].
add_clp_time_1(_,Times,Times).

add_clp_time_2(Ts,Times_2,Times) :-
   member(clp(T1,T2),Ts),
   divide_save(T1,4000,A1),
   divide_save(T2,4000,A2),
   !,
   Times = [clp(A1,A2)|Times_2].
add_clp_time_2(_,Times,Times).


/* clp_input_add_to_file(Clp_Input) <-
      */

clp_input_add_to_file(Clp_Input) :-
   tell(yyy),
   write(Clp_Input),
   writeln('.'),
   told,
   us('cat xxx yyy > zzz; mv zzz xxx').


/* list_clp_inputs(Mode,Clp_Inputs) <-
      */

list_clp_inputs(current) :-
   list_clp_inputs(current,_).

list_clp_inputs(current,Clp_Inputs) :-
   listing(clp_input),
   findall( [clp_input(Cs,True,False,Time)],
      ( clp_input(Cs,True,False,Time),
        True-False \= []-[] ),
      Clp_Inputs ),
   writeln('non-trivial results:'),
   writeln_list(Clp_Inputs).
list_clp_inputs(iteresting,Clp_Inputs) :-
   dislog_variable_get(cardinality_constraints_measures,File),
   dconsult(File,Clp_Inputs_2),
   length(Clp_Inputs_2,M),
   findall( [clp_input(Cs,True,False,Time)],
      ( member([clp_input(Cs,True,False,Time)],Clp_Inputs_2),
        True-False \= []-[] ),
      Clp_Inputs ),
   length(Clp_Inputs,N),
   writeln('non-trivial results:'),
   writeln_list(Clp_Inputs),
   writeln(how_many(M,N)).
list_clp_inputs(very_iteresting,Clp_Inputs) :-
   list_clp_inputs(iteresting,Clp_Inputs_2),
   findall( [clp_input(Cs,True_1,False_1,Time)],
      ( member([clp_input(Cs,True_1,False_1,Time)],Clp_Inputs_2),
        cardinality_constraints_implication_fixpoint(
           Cs,Cs2),
        cardinality_constraints_definite_conclusions(
           Cs2,True_2,False_2),
        True_1-False_1 \= True_2-False_2 ),
      Clp_Inputs ).
list_clp_inputs(times,Clp_Inputs) :-
   dislog_variable_get(cardinality_constraints_measures,File),
   dconsult(File,Clp_Inputs_2),
   findall( [clp_input(Cs,True_1,False_1,[calc(T,-)])],
      ( member([clp_input(Cs,True_1,False_1,_Time)],Clp_Inputs_2),
        Factor is 25,
        measure_multiple( Factor,
           cardinality_constraints_calculus(basic,Cs,_,_),
           Time_Calculus ),
%       T is Time_Calculus * 1000 * 1000 / Factor,
        T is Time_Calculus * 1000 / Factor,
        write(t(T)), ttyflush ),
      Clp_Inputs_List ),
   list_to_ord_set(Clp_Inputs_List,Clp_Inputs),
   disave(Clp_Inputs,xxx).


/* save_clp_inputs <-
      */

save_clp_inputs :-
   dislog_variable_get(cardinality_constraints_measures,File),
   dconsult(File,Clp_Inputs_1),
   findall( [clp_input(Cs,True,False,Time)],
      retract(clp_input(Cs,True,False,Time)),
      Clp_Inputs_2 ),
   state_union(Clp_Inputs_1,Clp_Inputs_2,Clp_Inputs_3),
   clp_inputs_means_values(Clp_Inputs_3,Clp_Inputs_4),
   dsave(Clp_Inputs_4,File),
   list_clp_inputs(iteresting,_).

clp_inputs_means_values(Clp_Inputs_1,Clp_Inputs_2) :-
   findall( [clp_input(Cs,True,False)],
      member([clp_input(Cs,True,False,_)],Clp_Inputs_1),
      Clp_Inputs_3 ),
   list_to_ord_set(Clp_Inputs_3,Clp_Inputs_4),
   maplist( clp_input_add_mean_value(Clp_Inputs_1),
      Clp_Inputs_4, Clp_Inputs_2 ).

clp_input_add_mean_value(Clp_Inputs_1,
      [clp_input(Cs,True,False)], [clp_input(Cs,True,False,Time)] ) :-
   findall( Time,
      member([clp_input(Cs,True,False,Time)],Clp_Inputs_1),
      Times ),
   average(Times,Time2),
   round(Time2,2,Time).


/* cardinality_constraints_measures_remove_times <-
      */

cardinality_constraints_measures_remove_times :-
   dislog_variable_get(cardinality_constraints_measures,File),
   dconsult(File,Clp_Inputs),
   findall( [clp_input(Constraints,True,False)],
      member([clp_input(Constraints,True,False,_)],Clp_Inputs),
      Clp_Outputs ),
   dislog_variable_get(cardinality_constraints_measures_2,File_2),
   dsave(Clp_Outputs,File_2).


/* cardinality_constraints_optimization_test(N,K) <-
      */

cardinality_constraints_optimization_test(N,K) :-
   generate_interval(1,N,Interval),
   M is K + 1,
   subsets_with_cardinality(Interval,M,State),
   state_to_tree(State,Tree),
   dportray(tree,Tree),
   pp_ratio_information(Tree),
   state_volume(State,Vs),
   tree_volume(Tree,Vt),
   Vst is Vs / Vt,
   writeln(Vs-Vt-Vst).

cardinality_constraints_optimization_test(Mode,N,K) :-
   generate_interval(1,N,Interval),
   cardinality_constraint_to_dlp(Mode,Interval-K,Program),
   cardinality_constraints_analyse_program(
      Program,True_State,False_State),
   program_select_facts(Program,Facts),
   program_to_denial_state(Program,Denials),
   cc_optimization_test_write_compare(Facts,True_State),
   cc_optimization_test_write_compare(Denials,False_State).

cc_optimization_test_write_compare(State,Facts) :-
   subtract(State,Facts,S1),
   subtract(Facts,State,S2),
   writeln(user,S1-S2).

 

/* cc_weight_function_overview(Exponent,Triples) <-
      */

cc_weight_function_overview(Exponent,Triples) :-
   generate_interval(0,8,Interval),
   findall( N-Pairs,
      ( member(N,Interval),
        findall( K-M,
           ( member(K,Interval),
             N >= K,
             cc_weight_function(Exponent,N,K,M) ),
           Pairs ) ),
      Triples ),
   !.


/*** tests ********************************************************/


test(cardinality_constraints:cc_weight_function_overview,0) :-
   cc_weight_function_overview(0,Ts),
   Ts = [
      0-[0-0],
      1-[0-1, 1-1],
      2-[0-2, 1-2, 2-2],
      3-[0-3, 1-4, 2-4, 3-3],
      4-[0-4, 1-7, 2-8, 3-7, 4-4],
      5-[0-5, 1-11, 2-15, 3-15, 4-11, 5-5],
      6-[0-6, 1-16, 2-26, 3-30, 4-26, 5-16, 6-6],
      7-[0-7, 1-22, 2-42, 3-56, 4-56, 5-42, 6-22, 7-7],
      8-[0-8, 1-29, 2-64, 3-98, 4-112, 5-98, 6-64, 7-29, 8-8] ].
test(cardinality_constraints:cc_weight_function_overview,1) :-
   cc_weight_function_overview(1,Ts),
   Ts = [
      0-[0-0],
      1-[0-1, 1-1],
      2-[0-2, 1-4, 2-2],
      3-[0-3, 1-9, 2-9, 3-3],
      4-[0-4, 1-16, 2-24, 3-16, 4-4],
      5-[0-5, 1-25, 2-50, 3-50, 4-25, 5-5],
      6-[0-6, 1-36, 2-90, 3-120, 4-90, 5-36, 6-6],
      7-[0-7, 1-49, 2-147, 3-245, 4-245, 5-147, 6-49, 7-7],
      8-[0-8, 1-64, 2-224, 3-448, 4-560,
         5-448, 6-224, 7-64, 8-8] ].
test(cardinality_constraints:cc_weight_function_overview,2) :-
   cc_weight_function_overview(2,Ts),
   Ts = [
      0-[0-0],
      1-[0-1, 1-1],
      2-[0-2, 1-8, 2-2],
      3-[0-3, 1-21, 2-21, 3-3],
      4-[0-4, 1-40, 2-72, 3-40, 4-4],
      5-[0-5, 1-65, 2-170, 3-170, 4-65, 5-5],
      6-[0-6, 1-96, 2-330, 3-480, 4-330, 5-96, 6-6],
      7-[0-7, 1-133, 2-567, 3-1085, 4-1085, 5-567, 6-133, 7-7],
      8-[0-8, 1-176, 2-896, 3-2128, 4-2800,
         5-2128, 6-896, 7-176, 8-8] ].


/******************************************************************/


