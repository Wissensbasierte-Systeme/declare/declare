

/******************************************************************/
/***                                                            ***/
/***       Conferences: Springer LNAI                           ***/
/***                                                            ***/
/******************************************************************/


/* inap_tracks_to_averages_html <-
      */

inap_tracks_to_averages_html :-
   inap_tracks_to_averages(Rows_1),
   sort_by_nth_component(9, Rows_1, Rows_2),
   reverse(Rows_2, Rows_3),
   number_the_rows(Rows_3, Rows_4),
   select_certain_rows(Rows_4, Rows_5),
   html_display_table('Springer',
      ['N', 'Title', 'Authors',
       'Tech', 'Orig', 'Pres', 'Overall', 'Conf', 'LNAI'],
      Rows_5).

inap_tracks_to_averages(Averages) :-
   Tracks = ['Track_2', 'Track_3', 'Track_4'],
   maplist( inap_track_to_averages,
      Tracks, As ),
   append(As, Averages).

inap_track_to_averages(Track, Averages) :-
   inap_track_to_marks(Track, Marks),
   marks_to_averages(Marks, Averages).

marks_to_averages(Marks, Averages) :-
   findall( N,
      ( member(N:_:_, Marks),
        number(N) ),
      Ns ),
   list_to_ord_set(Ns, Numbers),
   maplist( paper_number_to_average(Marks),
      Numbers, Averages ).

paper_number_to_average(Marks, N, [N, Title, Authors|Averages]) :-
   inap_paper([_, N, Authors, Title]),
   findall( X,
      member(N:_:X, Marks),
      Xs ),
   generate_interval(1, 6, Interval),
   maplist( nth_marks_to_average(Xs),
      Interval, Averages ).

nth_marks_to_average(Xs, I, Y) :-
   findall( Z,
      ( member(X, Xs),
        nth(I, X, Z) ),
      Zs ),
   findall( U,
      ( member(U, Zs),
        U \= '--' ),
      Us ),
   mean_value_save(Us, U),
   round(U, 1, Y).


/* sort_by_nth_component(N, Vs, Us) <-
      */

sort_by_nth_component(N, Vs, Us) :-
   findall( X:Xs,
      ( member(Xs, Vs),
        nth(N, Xs, X) ),
      Ys ),
   list_to_ord_set(Ys, Zs),
   findall( U,
      member(_:U, Zs),
      Us ).

   
number_the_rows(Rows_1, Rows_2) :-
   length(Rows_1, N),
   generate_interval(1, N, Interval),
   pair_lists(Interval, Rows_1, Rows_3),
   findall( [I|Row],
      member([I, Row], Rows_3),
      Rows_2 ).

select_certain_rows(Rows_1, Rows_2) :-
   findall( Row,
      ( member([_|Row], Rows_1),
        nth(1, Row, N),
        \+ member(N, [20, 9, 14, 13, 6, 29, 8,
              7, 30, 25, 18, 34, 32, 10]) ),
      Rows_2 ).

 
/******************************************************************/


