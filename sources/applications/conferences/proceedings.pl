

/******************************************************************/
/***                                                            ***/
/***       Conferences: Springer Proceedings                    ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      proceedings_paper_length/2.


/*** data *********************************************************/


/* proceedings_lncs_number(Number) <-
      */

  proceedings_lncs_number(3392).
% proceedings_lncs_number(2942).


/* proceedings_papers(Paper_Information) <-
      */

proceedings_papers(Paper_Information) :-
   proceedings_lncs_number(Number),
   proceedings_papers(Number, Paper_Information).


/* proceedings_papers(Number, Paper_Information) <-
      */

proceedings_papers(3392, [
   'Part I': 2,
   '03_Kleemann': 15: inap2004,
   '04_Seipel': 17: paper,
   '05_Shang': 16: shang,
   '11_Winniwarter': 18: inap,
   '12_Atzmueller': 14: qmsalsdrbInap2004LNAI,
   '15_Groth': 20: 'groth-inadp-extended',
   '16_Nanni': 18: 'Paper16LNAI',
   '17_Matsuo': 18: final_final,
   '21_Keinaenen': 15: 'besasp-v5',
   'Part II': 3,
   '37_Bartak': 16: final,
   '22_Bohlin': 19: bohlin_localsearch,
   '23_Wolf': 15: 'inap04-wolf-schlenker',
   '27_John': 15: inap04_rev_joge_upd,
   '28_Schlenker': 12: 'Schlenker',
   'Part III': 3,
   '31_Fukuda': 18: 'INAP2004_Fukuda_final',
   '33_Heumesser': 14: inap,
   '38_Bry': 12: xcerpt_semantics_abstract,
   '35_Bartenstein': 9: inap2004_bartenstein20040715,
   '36_Takahashi': 16: '0403_INAP_paper_r09',
   '39_Beierle': 15: wlp2004_nr12_lncs_final ]).

proceedings_papers(2942, [
   '00_Gottlob': 5,
   '00_Gurevich': 8,
   '24_Arieli': 17,
   '23_Christiansen': 18,
   '50_Demetrovics': 11,
   '16_De_Vos': 19,
   '46_Godfrey': 20,
   '52_Grahne': 18,
   '08_Hartmann': 18,
   '51_Hartmann': 20,
   '29_Hegner': 20,
   '66_Hurtado': 20,
   '22_Huynh': 17,
   '59_Lyaletski': 18,
   '48_Osorio': 12,
   '36_Sali': 10,
   '38_Schmitt': 20,
   '49_Shiri': 18,
   '18_Tzitzikas': 11,
   '44_Yaman': 18 ]).


/* proceedings_lncs_directory(Directory) <-
      */

proceedings_lncs_directory(Directory) :-
   proceedings_lncs_number(3392),
   home_directory(Home),
   concat([Home, '/research/conferences/inap2004/LNAI',
      '/Proceedings/lnai3392/' ], Directory ).
proceedings_lncs_directory(Directory) :-
   proceedings_lncs_number(2942),
   home_directory(Home),
   concat([Home, '/research/conferences/foiks2004',
      '/Proceedings_FoIKS_2004/lncs2942/' ], Directory ).

:- proceedings_lncs_directory(Directory),
   dislog_variable_set(
      proceedings_directory, Directory ).


/*** interface ****************************************************/


/* proceedings_directories_convert(Mode) <-
      */

proceedings_directories_convert(forth) :-
   proceedings_paper_conversions(Papers_1, Papers_2),
   ( foreach(Name_1, Papers_1), foreach(Name_2, Papers_2) do
        proceedings_directory_convert(forth, Name_1, Name_2) ).
proceedings_directories_convert(back) :-
   proceedings_paper_conversions(Papers_2, Papers_1),
   ( foreach(Name_1, Papers_1), foreach(Name_2, Papers_2) do
        proceedings_directory_convert(back, Name_1, Name_2) ).


/* proceedings_directory_convert(Name_1, Name_2) <-
      */

proceedings_directory_convert(forth, Name_1, Name_2) :-
   dislog_variable_get(
      proceedings_directory, Directory ),
   concat(Directory, Name_1, Directory_1),
   directory_contains_files(Directory_1, Files),
   findall( _,
      ( proceedings_paper_name_to_handle(Name_1, Handle),
        member(File, Files),
        concat(Handle, Extension, File),
        concat([Directory_1, '/', File], F1),
        concat([Directory_1, '/', Name_2, Extension], F2),
        concat(['mv ', F1, ' ', F2], Command_1),
        us(Command_1) ),
      _ ),
   concat(Directory, Name_2, Directory_2),
   concat(['mv ', Directory_1, ' ', Directory_2], Command_2),
   us(Command_2),
   !.
proceedings_directory_convert(back, Name_1, Name_2) :-
   dislog_variable_get(
      proceedings_directory, Directory ),
   concat(Directory, Name_1, Directory_1),
   directory_contains_files(Directory_1, Files),
   findall( _,
      ( proceedings_paper_handle_to_name(Handle, Name_2),
        member(File, Files),
        concat(Name_1, Extension, File),
        concat([Directory_1, '/', File], F1),
        concat([Directory_1, '/', Handle, Extension], F2),
        concat(['mv ', F1, ' ', F2], Command_1),
        us(Command_1) ),
      _ ),
   concat(Directory, Name_2, Directory_2),
   concat(['mv ', Directory_1, ' ', Directory_2], Command_2),
   us(Command_2),
   !.
proceedings_directory_convert(_, Name_1, Name_2) :-
   concat(['conversion ', Name_1, ' -> ', Name_2,
      ' not possible'], Message),
   write_list(user, [Message, '\n']).
%  ddk_message([Message]).


/*** implementation ***********************************************/


/* proceedings_paper_length(Papers) <-
      */

proceedings_paper_length(Papers) :-
   proceedings_papers(Paper_Information),
   findall( Name:Length,
      ( member(Paper, Paper_Information),
        ( Paper = Name:Length:_
        ; Paper = Name:Length,
          Length \= _:_ ) ),
      Papers ).


/* proceedings_paper_name_to_handle(Name, Handle) <-
      */

proceedings_paper_name_to_handle(Name, Handle) :-
   proceedings_papers(Paper_Information),
   !,
   ( member(Name:_:Handle, Paper_Information)
   ; member(Name:Length, Paper_Information),
     Length \= _:_,
     Handle = Name ).

proceedings_paper_handle_to_name(Handle, Name) :-
   proceedings_paper_name_to_handle(Name, Handle).


/* proceedings_paper_conversions(Papers_1, Papers_2) <-
      */

proceedings_paper_conversions(Papers_1, Papers_2) :-
   proceedings_paper_length(Papers_with_Numbers),
   findall( Paper,
      member(Paper:_, Papers_with_Numbers),
      Papers_1 ),
   maplist( proceedings_paper_conversion,
      Papers_1, Papers_2 ).

proceedings_paper_conversion(Paper_1, Paper_2) :-
   proceedings_paper_length(Papers_with_Numbers),
   append(Papers_2, [Paper_1:_|_], Papers_with_Numbers),
   findall( N,
      member(_:N, Papers_2),
      Ns ),
   add([1|Ns], M),
   lncs_page_number_to_directory(M, Paper_2).

lncs_page_number_to_directory(M, Paper) :-
   name(M, Xs),
   length(Xs, N),
   power(10, N, Power),
   proceedings_lncs_number(Lncs),
   Lncs_2 is Lncs * 10000 / Power,
   concat(Lncs_2, M, Paper).

 
/******************************************************************/


