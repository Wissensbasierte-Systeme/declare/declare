

/******************************************************************/
/***                                                            ***/
/***       DisLog:  Organization of Conferences - Statistics    ***/
/***                                                            ***/
/******************************************************************/


/* conference_xml_file_to_countries_count <-
      */

conference_xml_file_to_countries_count :-
   conference_directory(mailing,Directory),
   concat(Directory,'Emails.xml',Xml_File),
   Options = [ dialect(xml), max_errors(5000), space(remove) ],
   xml_file_to_fn_term(Xml_File,FN_Term,Options),
   conference_email_fn_term_to_countries(FN_Term,Countries),
   maplist( conference_email_fn_term_country_count(FN_Term),
      Countries, Rows ),
   xpce_display_table( _, _, 'Countries', ['Country','Number'],
      Rows ).

conference_email_fn_term_country_count(FN_Term,C,[C,N]) :-
   findall( Email,
      ( [Email] := FN_Term^'Table'^'Addresses'^'Person'^'Email',
        email_to_country(Email,C) ),
      Emails ),
   length(Emails,N).

conference_email_fn_term_to_countries(FN_Term,Countries) :-
   findall( Country,
      ( [Email] := FN_Term^'Table'^'Addresses'^'Person'^'Email',
        email_to_country(Email,Country) ),
      Countries_2 ),
   list_to_ord_set(Countries_2,Countries).


/* email_to_country(Email,Country) <-
      */

email_to_country(Email,Country) :-
   name(Email,String),
   reverse(String,String_2),
   list_cut_at_position(["."],String_2,String_3),
   reverse(String_3,String_4),
   !,
   name(Country,String_4).


/* email_file_overview(File, Tuples) <-
      */

email_file_overview(File) :-
   email_file_overview(File, Tuples),
   xpce_display_table(['Group', 'Type', 'Emails'], Tuples).

email_file_overview(File, Tuples) :-
   dread(xml, File, [X]),
   ddbase_aggregate( [Source, Type, length(Entry)],
      Entry := X/group::[@source=Source, @type=Type]/entry,
      Tuples ).


/* inap_2007_registration_file_to_xml_file(File_1, File_2) <-
      */
   
inap_2007_registration_file_to_xml_file(File_1, File_2) :-
   dread(xls, File_1, Tuples),
   maplist( inap_2007_registration_tuple_to_xml,
      Tuples, Entries ),
   Xml = emails:[
      group:[source:'INAP 2007', type:'Participants']:Entries ],
   dwrite(xml, File_2, Xml).

inap_2007_registration_tuple_to_xml(Tuple, Xml) :-
   maplist( name_remove_elements(both, "\""),
      Tuple, Xs ),
   ( length(Xs, 13) ->
     reverse(Xs, [A, B|Ys]),
     concat(B, A, C),
     reverse([C|Ys], Values)
   ; Values = Xs ),
   Attributes = [ gender, first_name, name,
      affiliation, street, zip, city, country,
      phone, fax, email, www ],
   pair_lists(:, Attributes, Values, As),
   Xml = entry:As:[].


/******************************************************************/


