

/******************************************************************/
/***                                                            ***/
/***       DisLog:  Organization of Conferences - Elementary    ***/
/***                                                            ***/
/******************************************************************/


conference_emails_pretty_print :-
   conference_directory(mailing,Directory),
   concat(Directory,'Emails.xml',Xml_File),
   concat(Directory,'Emails.xml.pretty',Xml_File_Pretty),
   xml_file_pretty_print(Xml_File,Xml_File_Pretty).

xml_file_pretty_print(Xml_File,Xml_File_Pretty) :-
   Options = [ dialect(xml), max_errors(5000), space(remove) ],
   xml_file_to_fn_term(Xml_File,FN_Term,Options),
   fn_term_to_xml_file(FN_Term,Xml_File_Pretty).


/******************************************************************/


