

/******************************************************************/
/***                                                            ***/
/***       DisLog:  Organization of Conferences - Mailings      ***/
/***                                                            ***/
/******************************************************************/



/*** files ********************************************************/


conference_directory(mailing,Directory) :-
   home_directory(Home),
   name_append([ Home,
      '/research/conferences/foiks2004/Mailing/Listen/' ],
      Directory ).


/*** implementation ***********************************************/


/* email_to_unix_mail_commands <-
      */

email_to_unix_mail_commands :-
   conference_directory(mailing,Directory),
   concat(Directory,'ACSW2001-list.commands',Command_File),
   conference_xml_file_to_emails(Emails),
   maplist( email_to_unix_mail_command(foiks_2004_cfp),
      Emails, Commands ),
   predicate_to_file( Command_File,
      writeln_list(Commands) ).


/* email_to_unix_mail_command(File,Email,Command) <-
      */

email_to_unix_mail_command(File,Email,Command) :-
   name_append([
      'mail -v -s "FoIKS''2004 - Preliminary Announcement" ',
      Email, ' < ', File ], Command ).


/* conference_xml_file_to_emails(Emails) <-
      */

conference_xml_file_to_emails(Emails) :-
   conference_directory(mailing,Directory),
%  concat(Directory,'test.xml',Xml_File),
%  concat(Directory,'test.emails',Email_File),
   concat(Directory,'ACSW2001-list.xml',Xml_File),
   concat(Directory,'ACSW2001-list.emails',Email_File),
   Options = [ dialect(xml), max_errors(5000), space(remove) ],
   xml_file_to_fn_term(Xml_File,FN_Term,Options),
   findall( Email,
      [Email] := FN_Term^'Addresses'^'Person'^'Email',
      Emails_2 ),
   list_to_ord_set(Emails_2,Emails),
   predicate_to_file( Email_File,
      write_conference_emails(20,Emails) ).

write_conference_emails(_,[]) :-
   !.
write_conference_emails(N,Emails) :-
   first_n_elements(N,Emails,Emails_n),
   write_list_with_separators( write_and_comma, writeln, Emails_n ),
   nl,
   append(Emails_n,Emails_2,Emails),
   write_conference_emails(N,Emails_2).


/* conference_file_to_xml_file <-
      */

conference_file_to_xml_file :-
   conference_directory(mailing,Directory),
%  File = 'ACSW2001-list',
%  As = [ 'BadgeName', 'Company', 'SatelliteChoice', 'Email' ],
   File = 'ACSW2001-list-attendees',
   As = [ 'NameBadge', 'Company', 'Email' ],
   concat(Directory,File,Path),
   conference_file_to_xml_file(Path,As).

conference_file_to_xml_file(File,As) :-
   concat(File,'.xml',Xml_File),
   conference_file_to_fn(File,As,FN_Term),
   write_list(['---> ',Xml_File]), nl,
   fn_term_to_xml_file(FN_Term,Xml_File).


/* conference_file_to_fn(File,As,Fn) <-
      */

conference_file_to_fn(File,As,Fn) :-
   read_conference_file(File,Lines),
   conference_lines_group(As,Lines,Fn).
   

/* read_conference_file(File,Lines) <-
      */

read_conference_file(File,Lines) :-
   write_list(['<--- ',File]), nl,
   read_file_to_string(File,String),
   list_split_at_position([[10]],String,Strings),
   maplist( name,
      Lines, Strings ).


/* emails_to_xml(File,FN_Term) <-
      */

emails_to_xml(File,FN_Term) :-
   read_conference_file(File,Lines),
   maplist( name_remove_elements(back," "),
      Lines, Lines_2 ),
   maplist( email_plus_tags,
      Lines_2, Elements ),
   concat(File,'.xml',Xml_File),
   FN_Term = ['Table':['Addresses':Elements]],
   fn_term_to_xml_file(FN_Term,Xml_File).

email_plus_tags(Email,'Person':['Email':Email]).


/* conference_lines_group(As,Ls,Es) <-
      */

conference_lines_group(As,Ls,Es) :-
   conference_lines_group(As,1,[],Ls,Es).

conference_lines_group(As,_,E,[''|Ls],['Person':F|Es]) :-
   reverse(E,F),
   conference_lines_group(As,1,[],Ls,Es).
conference_lines_group(As,N,E,[L|Ls],Es) :-
   nth(N,As,A),
   !,
   M is N + 1,
   conference_lines_group(As,M,[A:L|E],Ls,Es).
conference_lines_group(As,_,E,[L|Ls],['Person':F|Es]) :-
   reverse(E,F),
   conference_lines_group(As,1,[],[L|Ls],Es).
conference_lines_group(_,_,E,[],['Person':F]) :-
   reverse(E,F).


/******************************************************************/


