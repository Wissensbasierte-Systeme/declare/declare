

/******************************************************************/
/***                                                            ***/
/***       DisLog:  Organization of Conferences - Algebra       ***/
/***                                                            ***/
/******************************************************************/


/* evaluate_selection_term_to_emails(Emails) <-
      */

evaluate_selection_term_to_emails(Emails) :-
   conference_directory(mailing,Directory),
   concat(Directory,'2003_01_06_2200.commands',Command_File),
   evaluate_selection_term_to_emails(
      ( 'ACSW2001-list-attendees.txt' +
        'FoIKS_2000-list.txt' +
        'acsw-list.txt' +
        'dag-liste.txt' ) -
      ( 'FoIKS_2004_PC' +
        'ACSW2001-list.txt' ),
      Emails ),
   maplist( email_to_unix_mail_command(foiks_2004_cfp),
      Emails, Commands ),
   predicate_to_file( Command_File,
      writeln_list(Commands) ).


/* evaluate_selection_term_to_emails(Term,Emails) <-
      */

evaluate_selection_term_to_emails(Term,Emails) :-
   conference_directory(mailing,Directory),
   concat(Directory,'Emails.xml',Xml_File),
   Options = [ dialect(xml), max_errors(5000), space(remove) ],
   xml_file_to_fn_term(Xml_File,FN_Term,Options),
   evaluate_selection_term_to_emails(FN_Term,Term,Emails).

evaluate_selection_term_to_emails(FN_Term,X+Y,Emails) :-
   !,
   evaluate_selection_term_to_emails(FN_Term,X,Emails_X),
   evaluate_selection_term_to_emails(FN_Term,Y,Emails_Y),
   ord_union(Emails_X,Emails_Y,Emails).
evaluate_selection_term_to_emails(FN_Term,X-Y,Emails) :-
   !,
   evaluate_selection_term_to_emails(FN_Term,X,Emails_X),
   evaluate_selection_term_to_emails(FN_Term,Y,Emails_Y),
   ord_subtract(Emails_X,Emails_Y,Emails).
evaluate_selection_term_to_emails(FN_Term,X,Emails) :-
   fn_term_select_emails(FN_Term,X,Emails).


/* fn_term_select_emails(FN_Term,Source,Emails) <-
      */

fn_term_select_emails(FN_Term,Source,Emails) :-
   X := FN_Term^'Table',
   member(Addresses,X),
   Source := [Addresses]@'Addresses'^'Source',
   findall( Email,
      [Email] := [Addresses]^'Addresses'^'Person'^'Email',
      Emails_2 ),
   list_to_ord_set(Emails_2,Emails).


/******************************************************************/


