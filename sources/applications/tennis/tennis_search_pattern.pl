

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Search Pattern                       ***/
/***                                                            ***/
/******************************************************************/


/* tennis_search_pattern(Version, Reference, Frame) <-
      */

tennis_search_pattern(Version, VarRef, Frame) :-
   get(VarRef, selection, Var),
   term_to_atom(Var_Term, Var),
   send(Frame,destroy),
   tennis_search_patterns(Version, Var_Term).


/* tennis_search_patterns(Version, Variable) <-
      */

tennis_search_patterns(Version, Var_Term) :-
   dislog_variable_get(tennis_to_search_files, Files),
   maplist( tennis_search_pattern(Version, Var_Term),
      Files, Result ),
   tennis_search_pattern_file(Path),
   tell(Path),
   tennis_html_file_start,
   checklist( tennis_search_pattern_display_browser,
      Result ),
   tennis_html_file_end,
   told,
   tennis_start_browser(Path, 'Search Pattern').


/* tennis_search_pattern(Version, Variable, File, List) <-
      */

tennis_search_pattern(Version, Var, File, 
      [ID_Score_Times, Videos, File]) :-
   tennis_determine_hits(File),
   dislog_variable_get(tennis_point_fn, FN_Points),
   tennis_search_determine_video_files(Videos),
   tennis_search_determine_set_point_game_id(File, S_P_G_IDs),
   tennis_search_determine_score(File, Scores),
   tennis_search_pattern_determine_hits(File, Hits),!,
   triple_lists(S_P_G_IDs,Scores,Hits,To_Search_Rally2s),
   maplist( tennis_search_pattern_x, 
      FN_Points, Xs ),
   maplist( tennis_search_pattern_y, 
      FN_Points, Ys ),
   pair_lists(Xs, Ys, XYRs),
   reverse(XYRs, XYs),
   maplist( tennis_search_pattern_reverse_hit(Version, XYs, Var),
      To_Search_Rally2s, Co_Hits ),
   length(Co_Hits, Length), 
   fill_list_with_int(Length, Numbers),
   tennis_assign_rally_to_number(Co_Hits, Numbers, Nth),
   list_remove_elements([[]],Co_Hits,Co_Hits2),
   list_remove_elements([[]],Nth, Nth2),
   maplist( tennis_search_browser_determine_time,
      Co_Hits2, StartTime ),
   maplist( tennis_search_browser_determine_hit_id,
      Co_Hits2, IDs ),
   maplist( tennis_search_browser_determine_score,
      Co_Hits2, Scores_Browser ),
   four_lists(IDs, Scores_Browser, StartTime, Nth2, ID_Score_Times).
tennis_search_pattern(_, _, _, _).


tennis_search_browser_determine_time(Hits, StartTime) :-
   n_th_element(Hits, 3, FirstHit),
   last_element(FirstHit, StartTime).

tennis_search_browser_determine_hit_id(Hits, ID) :-
   first(Hits, ID).

tennis_search_browser_determine_score(Hits, Score) :-
   n_th_element(Hits, 2, Score).


tennis_search_pattern_x(FN_Term, X) :-
   X := FN_Term@hit^x.

tennis_search_pattern_y(FN_Term, Y) :-
   Y := FN_Term@hit^y.


/* tennis_search_pattern_reverse_hit(Version, 
   Variable, List, Variable, List, Result) <-
      */

tennis_search_pattern_reverse_hit(
      Version, XY, Var, [ID,Score|ZRs], Co_Hits_Leer) :-
   n_th_element(ZRs,1,Z1s),
   ( ( Version == 'A',
       tennis_search_pattern_hit(XY, Var, Z1s, Co_Hits) )
   ; ( ( Version == 'B',
         tennis_search_pattern_winning_hit(
            XY, Var, Z1s, Co_Hits) ) ) ),
   ( ( Co_Hits \= [],
       Co_Hits_Leer = [ID,Score|Z1s] )
   ; Co_Hits_Leer = Co_Hits ).


/* tennis_determine_search_hit(List, Variable, List, ResultList) <-
        */

tennis_determine_search_hit([DXA,DYA],Var,[XA,YA,Z],[XA,YA,Z]) :-  
   tennis_terms_to_atoms([
      X - XA, Y - YA, DX - DXA, DY - DYA]), 
   X2 is DX - Var,
   X3 is DX + Var,
   Y2 is DY - Var,
   Y3 is DY + Var,
   X >= X2, X =< X3, Y >= Y2,Y =< Y3.


/* tennis_search_pattern_hit(List, Variable, List, List) <-
      */

tennis_search_pattern_hit([XY|XYs],Var,[Z|Zs],[Co_Hit|Co_Hits]) :-
   tennis_determine_search_hit(XY, Var, Z, Co_Hit),
   tennis_prefix(XYs, Var, Zs, Co_Hits),
   !.
tennis_search_pattern_hit(XYs, Var, [_|Zs], Co_Hits) :-
   tennis_search_pattern_hit(XYs, Var, Zs, Co_Hits).
tennis_search_pattern_hit([],_,_,[]) :-
   !.
tennis_search_pattern_hit(_,_,[],[]) :-
   !.


/* tennis_prefix(List, Variable, List, List) <-
      */

tennis_prefix([XY|XYs], Var, [Z|Zs], [Co_Hit|Co_Hits]) :-
   tennis_determine_search_hit(XY, Var, Z, Co_Hit),
   tennis_prefix(XYs, Var, Zs, Co_Hits).
tennis_prefix([],_,_,[]) :-
   !.
tennis_prefix(_,_,[],[]) :-
   fail.


/* tennis_search_pattern_winning_hit(List, Variable, List, List) <-
      */

tennis_search_pattern_winning_hit([],_,[],[]) :-
   !.
tennis_search_pattern_winning_hit([],_,_,_) :-
   !.
tennis_search_pattern_winning_hit(_,_,[],_) :-
   !.
tennis_search_pattern_winning_hit([XY|XYs],Var,[Z|Zs],[Co_Hit|Co_Hits]) :-
   tennis_determine_search_hit(XY, Var, Z, Co_Hit),
   tennis_prefix_winning_hit(XYs, Var, Zs, Co_Hits),
   !.
tennis_search_pattern_winning_hit(XYs, Var, [_|Zs], Co_Hits) :-
   tennis_search_pattern_winning_hit(XYs, Var, Zs, Co_Hits).


/* tennis_prefix_winning_hit(List, Variable, List, List) <-
      */

tennis_prefix_winning_hit([],_,[],[]) :-
   !.
tennis_prefix_winning_hit([],_,_,_) :-
   fail.
tennis_prefix_winning_hit(_,_,[],_) :-
   fail.
tennis_prefix_winning_hit([XY|XYs],Var,[Z|Zs],[Co_Hit|Co_Hits]) :-
   tennis_determine_search_hit(XY, Var, Z, Co_Hit),
   tennis_prefix_winning_hit(XYs, Var, Zs, Co_Hits).


/* tennis_search_determine_video_files(Result) <-
      */

tennis_search_determine_video_files(Files) :-
   dislog_variable_get(tennis_current_output_file, XML_File),
   tennis_xml_file_to_fn_term(XML_File, FN_Term),
   findall( Video, 
      Video := FN_Term^match@set^video_file,
      Pathes ),
   maplist( tennis_path_to_filename,
      Pathes, Files ).


/* tennis_search_determine_set_point_game_id(File, List) <-
      */

tennis_search_determine_set_point_game_id(Fin3) :-
   tennis_file_to_match_fn_list(Match),
   tennis_search_determine_set_point_game_id_for_match(
      Match, Fin3).

tennis_search_determine_set_point_game_id(File, Fin3) :-
   tennis_xml_file_to_fn_term(File, Match),
   tennis_search_determine_set_point_game_id_for_match(
      Match, Fin3).

tennis_search_determine_set_point_game_id_for_match(
      Match, Fin3) :-
   findall( Lo,
      Lo := Match^match@set^id,
      Set_IDs ),
   findall( Se, 
      Se := Match^match^set, 
      Sets ),
   maplist( tennis_search_game_id,
      Sets, Each_Set_Game_IDs ),
   tennis_pair_lists(Set_IDs, Each_Set_Game_IDs, Set_and_Game_IDs),
   append(Set_and_Game_IDs, SetGameIDs),
   maplist( tennis_search_games,
      Sets, Games ),
   maplist( maplist(tennis_searches_point_id),
      Games, PoIDs ),
   append(PoIDs, PoIDEs),
   tennis_pair_lists2(SetGameIDs, PoIDEs, Fin),
   maplist( maplist(append),
      Fin, Fin2 ),
   append(Fin2, Fin3).
tennis_search_determine_set_point_game_id_for_match(_, _).


tennis_search_game_id(Sets, Game_IDs) :-
   findall( Game, 
      Game := Sets@game^id, 
      Game_IDs ).    

tennis_searches_point_id(Games, Point_IDs) :-
   findall( Point, 
      Point := Games@point^id, 
      Point_IDs ).   

tennis_search_games(Sets, Games) :-
   findall( Ga, 
      Ga := Sets^game, 
      Games ).
tennis_search_games(_,_).


/* tennis_search_pattern_determine_hits(File, List) <-
      */

tennis_search_pattern_determine_hits(File, X_Y_Time) :-
   tennis_xml_file_to_fn_term(File, FN_Term),
   findall(Point, 
      Point := FN_Term^match^set^game^point, 
      Points),
   maplist( tennis_find_hits,
      Points, Hits ),
   maplist( maplist(tennis_determine_x_coordinate),
      Hits, Xs ),
   maplist( maplist(tennis_determine_y_coordinate),
      Hits, Ys ), 
   maplist( maplist(tennis_determine_hit_time2),
      Hits, Times ),
   maplist( maplist(element_to_list),
      X1s, Xs ),
   maplist( maplist(element_to_list),
      Y1s, Ys ),
   list_remove_elements([[]],Times, Time2s),
   list_remove_elements([[]],X1s, X2s),
   list_remove_elements([[]],Y1s, Y2s),
   triple_lists_of_lists(X2s, Y2s, Time2s, X_Y_Time),
   dislog_variable_set(search_list_of_hits, X_Y_Time).
tennis_search_pattern_determine_hits(_, _).


/* tennis_search_determine_score(File, List) <-
      */

tennis_search_determine_score(File, Results) :-
   tennis_xml_file_to_fn_term(File, FN_Term),
   tennis_search_pattern_score_set(FN_Term, Score_Set1),
   append_set(Score_Set1, Score_Sets),
   findall( Se, 
      Se := FN_Term^match^set, 
      Sets ),
   maplist( tennis_search_pattern_score_game,
      Sets, Each_Set_Game_Score ),
   maplist( maplist(list_to_colon_structure),
      Each_Set_Game_Score, ESGS2 ),
   maplist( tennis_search_games,
      Sets, Games ),
   maplist( maplist(tennis_search_pattern_score_point(FN_Term)),
      Games, Score_Points ),
   append(Score_Points,ScorePoints),
   ( ( Score_Sets \= [], 
       tennis_more_than_one_set(
          Score_Sets, ESGS2, ScorePoints, Results) )
   ; tennis_one_set(ESGS2, ScorePoints, Results) ).
tennis_search_determine_score(_, _).

tennis_one_set(GameScore2, PointScore, Result2) :-
   element_to_list(GameScore, GameScore2),
   tennis_pair_lists3(GameScore, PointScore, GaPoScore),
   maplist(maplist(tennis_element_to_list5),
      GaPoScore, Result),
   append(Result, Result2).
tennis_one_set(_, _, _).


tennis_element_to_list5([Element,[S,W]], [Element,S,W]).


tennis_more_than_one_set(Sets, Games, Points, Results) :-
   tennis_pair_lists(Sets, Games, SG),
   append(SG, SG2),
   tennis_more_than_one_set2(SG2, Points, Results).

tennis_more_than_one_set(Sets, Games, Points, Results) :-
   length(Sets, LE),
   N is LE - 1, 
   first_n_elements(N, Sets, Set2s),
   tennis_pair_lists(Set2s, Games, SG),
   append(SG, SG2),
   tennis_more_than_one_set2(SG2, Points, Results).

tennis_more_than_one_set2(Sets, Points,Results) :-
   tennis_pair_lists3(Sets, Points,Fin),
   maplist(maplist(append),
      Fin, Fin2),
   append(Fin2, Results).
tennis_more_than_one_set2(_, _,_).


/* tennis_search_pattern_score_set(FN_Term, List) <-
      */

tennis_search_pattern_score_set(FN_Term, AA1) :-
   findall( Set, 
      Set := FN_Term^match^result@score^player_A,
      Score_SetA_Eles ),
   maplist( term_to_atom,
      Score_SetA_Atoms, Score_SetA_Eles ),
   findall(SetB, 
      SetB := FN_Term^match^result@score^player_B,
      Score_SetB_Eles),
   maplist( term_to_atom,
      Score_SetB_Atoms, Score_SetB_Eles ),
   pair_lists(Score_SetA_Atoms, Score_SetB_Atoms, Score_Sets),
   maplist( list_to_colon_structure,
      Score_Sets, AA1 ).
tennis_search_pattern_score_set(_, _).


/* tennis_search_pattern_score_game(FN_Term, List) <-
      */

tennis_search_pattern_score_game(Sets, Score_Games) :-
   findall( GameA,
      GameA := Sets@game^score_A, 
      Score_Game_As1 ),
   maplist( tennis_atom_to_term,
      Score_Game_As1, Score_Game_As ),
   findall( GameB,
      GameB := Sets@game^score_B,
      Score_Game_Bs1 ),
   maplist( tennis_atom_to_term,
      Score_Game_Bs1, Score_Game_Bs ),
   pair_lists(Score_Game_As, Score_Game_Bs, Score_Games).
tennis_search_pattern_score_game(_, _).


/* tennis_search_pattern_score_point(FN_Term, List) <-
      */

tennis_search_pattern_score_point(FN_Term, Games, Pair) :-
   findall( PointA,
      PointA := Games@point^score_A, 
      Score_Point_As ),
   findall( PointB,
      PointB := Games@point^score_B, 
      Score_Point_Bs ),
   findall( Winner,
      Winner := Games@point^winner,
      WinnersAB ),
   tennis_determine_player_names2(FN_Term),
   maplist( tennis_winnerAB_to_name,
      WinnersAB, Winners ),
   triple_lists(Score_Point_As, Score_Point_Bs, Winners,
      Score_Points),
   tennis_search_pattern_score_determines(Score_Points,Pair).
tennis_search_pattern_score_point(_, _, _).


tennis_search_pattern_score_determines(List,Result) :-
   maplist( tennis_search_pattern_score_determine,
      List, Result ).
tennis_search_pattern_score_determine([A,B,W],[R,W]) :-
   tennis_terms_to_atoms([
      AA - A,
      BB - B]),
   tennis_score_determine(AA,BB,C,D),
   concat_atom([C,':',D],R).  


tennis_assign_rally_to_number(Hits, Numbers, Nths) :-
   tennis_compare_lists(Hits, Numbers, Nths).

tennis_compare_lists([[]|As], [_|Bs], [[]|Cs]) :-
   tennis_compare_lists(As,Bs,Cs).
tennis_compare_lists([_|As], [B|Bs], [B|Cs]) :-
   tennis_compare_lists(As, Bs, Cs).
tennis_compare_lists([],[],[]).


tennis_winnerAB_to_name('A', NameA) :-
   dislog_variable_get(search_pattern_name_player_A, NameA).
tennis_winnerAB_to_name('B', NameB) :-
   dislog_variable_get(search_pattern_name_player_B, NameB).


/* tennis_clear_search_pattern_files <-
      */

tennis_clear_search_pattern_files :-
   dislog_variable_get(tennis_current_output_file, File),
   dislog_variable_set(tennis_to_search_files, [File]).

tennis_cancel_search_pattern_dialog(Frame) :-
   send(Frame, destroy),
   tennis_clear_search_pattern_files.


/******************************************************************/


