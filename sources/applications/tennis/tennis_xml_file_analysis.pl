

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Analysis of Matches                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tennis_xml_files_analyze(Characteristics) <-
      */

tennis_xml_files_analyze :-
   tennis_xml_files_analyze(Characteristics),
   html_display_table('Tennis Data',
      ['Player A', 'Player B', 'Tournament',
       'Sets', 'Games', 'Points', 'Hits'],
      Characteristics, size(500, 200) ).

tennis_xml_files_analyze(Characteristics) :-
   tennis_xml_files(Paths),
   maplist( tennis_xml_file_analyze,
      Paths, Characteristics ).


/* tennis_xml_files(Paths) <-
      */

tennis_xml_files(Paths) :-
   tennis_data_path(Data_Path),
   concat(Data_Path, 'tennis_match_index.xml', Index_File),
   xml_file_to_fn_term(Index_File, Index),
   findall( Path,
      ( Data_File := (xml:Index)^matches^match@file,
        concat(Data_Path, Data_File, Path) ),
      Paths ),
   writeln_list(user, Paths).


/* tennis_xml_files_polish <-
      */

tennis_xml_files_polish :-
   tennis_xml_files(Paths),
   checklist( tennis_xml_file_polish,
      Paths ).

tennis_xml_file_polish(Path) :-
   tennis_xml_file_to_fn_term(Path, [Xml]),
   dwrite(xml, Path, Xml).


/* tennis_xml_file_analyze(File, Characteristics) <-
      */

tennis_xml_file_analyze(File, Characteristics) :-
   xml_file_to_fn_term(File, Term),
   Match = (xml:Term),
   maplist( tennis_match_count(Match),
      [set, game, point, hit], Cs ),
   tennis_match_to_player(Match, 'A', Player_A),
   tennis_match_to_player(Match, 'B', Player_B),
   [Tournament] := Term^match^match_facts^tournament,
   Characteristics = [Player_A, Player_B, Tournament|Cs],
   !.

tennis_match_to_player(Match, Id, Name) :-
   Player := Match^match^player,
   Id := Player@id,
   !,
   Name := Player@name.

tennis_match_count(Match, Tag, N) :-
   findall( X,
      X := Match^_^Tag,
      Xs ),
   length(Xs, N).


/* tennis_xml_file_analyze(File, Player, Hits) <-
      */

tennis_xml_file_analyze(File, Player, Hits) :-
   xml_file_to_fn_term(File, Term),
   Match = (xml:Term),
   !,
   P := (Match)^_^player,
   Player := P@name,
   Id := P@id,
   findall( M:Y,
      ( Game := (Match)^_^game,
        Id := Game@service,
        Hit := Game^_^hit,
        N := Hit@id,
        name_to_number(N, M),
        M >= 3,
        \+ even(M),
        Y1 := Hit@y,
        name_to_number(Y1, Y2),
        Y is round(100*abs(Y2)/(23.77/2)),
        Y > 50 ),
      Hits ).


/* tennis_xml_file_analyze_length(File, Player, Averages) <-
      */

tennis_xml_file_analyze_length(File, Player, Averages) :-
   tennis_xml_file_analyze(File, Player, Hits),
   generate_interval(1, 20, Interval),
   maplist( pairs_to_average(Hits),
      Interval, Averages ).


pairs_to_average(Pairs, N, N:Average) :-
   Average := Pairs-average^N.
%  findall( V,
%     member(N:V, Pairs),
%     Vs ),
%  average(Vs, Average).


/******************************************************************/


