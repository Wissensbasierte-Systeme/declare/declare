

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Colouring                            ***/
/***                                                            ***/
/******************************************************************/


tennis_court_colour(red).
tennis_court_colour(green).
tennis_court_colour(blue).
tennis_court_colour(brown).

tennis_court_colour_to_numbers('red', [256,0,0]).
tennis_court_colour_to_numbers('blue', [0,0,256]).
tennis_court_colour_to_numbers('green', [0,256,0]).
tennis_court_colour_to_numbers('brown', [166,53,61]).


/*** interface ****************************************************/


/* tennis_colour_test_block(Colour) <-
      */

tennis_colour_test_block(Colour) :-
   tennis_court_colour_to_numbers(Colour, [R,G,B]),
   Red is R / 16,
   dislog_variable_set(colour_test(red), Red),
   Green is G / 16,
   dislog_variable_set(colour_test(green), Green),
   Blue is B / 16,
   dislog_variable_set(colour_test(blue), Blue),
   tennis_display_test_colour.


/* tennis_colour_test_slider(Colour,Number) <-
      */

tennis_colour_test_slider(Colour, Number) :-
   C is Number / 16,
   dislog_variable_set(colour_test(Colour), C),
   tennis_display_test_colour.


/* tennis_display_test_colour <-
      */

tennis_display_test_colour :-
   dislog_variable_get(colour_test_display_dialog, Dialog),
   tennis_dislog_variable_get_colour(RGB),
   rgb_to_colour(RGB, Colour),
   dislog_variable_set(current_court_colour_test, Colour),
   send(Dialog, background, Colour).


/* tennis_change_court_colour <-
      */

tennis_change_court_colour :-
   dislog_variable_get(current_court_colour_test, Colour),
   dislog_variable_set(current_court_colour, Colour),
   dislog_variable_get(court_colour_test_frame, Frame),
   tennis_picture_uncolour,
   tennis_save_colour_change,
   send(Frame, destroy).

tennis_change_court_colour_default :-
   dislog_variable_get(court_colour_test_frame, Frame),
   tennis_preferences(File),
   tennis_xml_file_to_fn_term(File, FN),
   FN_Term := FN^preferences^default,
   tennis_get_preferences_colour(FN_Term, Red, Green, Blue), 
   tennis_dislog_variable_set_colour([Red,Green,Blue]),
   rgb_to_colour([Red, Green, Blue], Colour),
   dislog_variable_set(current_court_colour, Colour),
   tennis_save_colour_change,
   tennis_picture_uncolour,
   send(Frame, destroy).

         
/* tennis_send_box(Colour,[X1,Y1],[X2,Y2]) <-
      */

tennis_send_box(Colour,[X1,Y1],[X2,Y2]) :-
   Factor = 50,
   tennis_coordinate_transform(Factor,[X1,Y1],[Xul,Yul]),
   tennis_coordinate_transform(Factor,[X2,Y2],[Xlr,Ylr]),
   dislog_variable_get(tennis_picture,Picture),
   B is Xlr - Xul,
   H is Ylr - Yul,
   send(Picture,display,
      new(Box,box(B,H)),point(Xul,Yul)),
   send(Box,fill_pattern,Colour),
   send(Box,colour,Colour).


/* tennis_coordinate_transform(Factor,[X1,Y1],[X2,Y2]) <-
      */

tennis_coordinate_transform(Factor,[X1,Y1],[X2,Y2]) :-
   tennis_court_format(Court),
   L := Court^length,  W := Court^width,
   dislog_variable_get(tennis_court_side,Side),
   dislog_variable_get(tennis_court_behind,Behind),
   Xm is Behind + L / 2,
   Ym is Side + W / 2,
   X2 is ( Xm + X1 * (L+2*Behind) / 2 ) * Factor,
   Y2 is ( Ym - Y1 * (W+2*Side) / 2 ) * Factor.


/* tennis_dislog_variable_get_colour([Red,Green,Blue]) <-
      */

tennis_dislog_variable_get_colour([Red, Green, Blue]) :-
   dislog_variables_get([
      colour_test(red) - Red,
      colour_test(green) - Green,
      colour_test(blue) - Blue]).
  
tennis_dislog_variable_set_colour([Red, Green, Blue]) :-
   dislog_variables_set([
      colour_test(red) - Red,
      colour_test(green) - Green,
      colour_test(blue) - Blue]).


/******************************************************************/


