

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Point Display                        ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(tennis_point_display, 1) :-
   File = 'projects/Tennis/2002_sampras_agassi_final.xml',
   forall( member(P, ['1', '2', '3', '4']),
      ( tennis_point_display(File, ['1','1',P]),
        wait_seconds(2) ) ).


/*** interface ****************************************************/


/* tennis_point_display(File, [S, G, P]) <-
      */

tennis_point_display(File, [S, G, P]) :-
   tennis_picture_uncolour,
   dread(xml, File, [Xml]),
   Xml_Point := Xml/set::[@id=S]/game::[@id=G]/point::[@id=P],
   star_line, dwrite(xml, Xml_Point), star_line,
   Service := Xml_Point@service,
   Top := Xml_Point@top,
   tennis_point_display_ball(
      File, [S, G, P]),
   tennis_point_display_player(
      File, Service, Top, top, [S, G, P]),
   tennis_point_display_player(
      File, Service, Top, bottom, [S, G, P]),
   maplist(term_to_atom, [Set, Game, Point], [S, G, P]),
   tennis_video_play_sgp(Set, Game, Point),
   !.


/*** implementation ***********************************************/


/* tennis_point_display_ball(File, [S, G, P]) <-
      */

tennis_point_display_ball(File, [S, G, P]) :-
   tennis_file_point_to_hits(File, [S, G, P], Hits),
%  tennis_file_point_to_hits(File, 4, 2, [S, G, P], Hits),
   tennis_trajectory_display(orange, Hits).


/* tennis_point_display_player(File, Type, [S, G, P]) <-
      */

tennis_point_display_player(File, Service, Top, Type, [S, G, P]) :-
   tennis_file_point_to_hits(File, [S, G, P], Hits),
   tennis_hits_to_player_trajectory(Type, Hits, Trajectory),
   ( Service = Top -> Serve = top
   ; Serve = bottom ),
   tennis_infer_service_return(Serve, Type, P, Hit),
   tennis_trajectory_display(darkblue, [Hit|Trajectory]).

tennis_infer_service_return(Serve, Type, P, Hit) :-
   term_to_atom(Q, P),
   ( ( Serve = top, odd(Q) )           -> ( Xs = -0.5, Xr =  3 )
   ; ( Serve = top, even(Q) )          -> ( Xs =  0.5, Xr = -3 )
   ; ( Serve = bottom, odd(Q) )        -> ( Xs =  0.5, Xr = -3 )
   ; ( Serve = bottom, even(Q) )       -> ( Xs = -0.5, Xr =  3 ) ),
   ( ( Serve = top, Type = top )       -> Hit = 0-[Xr,-13]
   ; ( Serve = top, Type = bottom )    -> Hit = 1-[Xs, 12.5]
   ; ( Serve = bottom, Type = top )    -> Hit = 0-[Xr, 13]
   ; ( Serve = bottom, Type = bottom ) -> Hit = 1-[Xs,-12.5] ).


/* tennis_hits_to_player_trajectory(Type, Hits, Trajectory) <-
      */

tennis_hits_to_player_trajectory(Type, Hits, Trajectory) :-
   ( Type = top -> Mode = even
   ; Type = bottom -> Mode = odd ),
   findall( N-Hit,
      ( nth(N, Hits, Hit_1), apply(Mode, [N]),
%       writeq(hit(N, Hit_1)),
        Hit_1 = [_, Y],
        ( N > 1 -> ( Y < 12 , Y > -12 )
        ; true ),
        ( Y =< -0.5 ; Y >= 0.5 ),    % ball was not in net
        M is N - 1, nth(M, Hits, Hit_2),
%       writelnq(previous_hit(M, Hit_2)),
        ( M = 1 -> Scale = 6
        ; Scale = 2 ),
        tennis_hits_to_stroke(Scale, Hit_1, Hit_2, Hit) ),
      Trajectory ),
%  writelnq(trajectory(Mode)=Trajectory),
%  star_line,
   !.

tennis_hits_to_stroke(Scale, Hit_1, Hit_2, Hit) :-
   Hit_1 = [X, _],
   vector_subtract(Hit_1, Hit_2, Plus_A),
   vector_scale(Scale, Plus_A, Plus_B),
   maplist(round_(2), Plus_B, Plus),
   ( X =< 0 -> X_2 is 0.5
   ; X_2 is -0.5 ),
   vector_add(Hit_1, Plus, Hit_3),
   vector_add(Hit_3, [X_2, 0], Hit_4),
   maplist(round_(2), Hit_4, Hit).

round_(N, X, Y) :-
   round(X, N, Y).


/* tennis_trajectory_display(Colour, Trajectory) <-
      */

tennis_trajectory_display(Colour, Trajectory) :-
   ( Trajectory = [Hit] -> Trajectory_2 = [Hit, Hit]
   ; Trajectory_2 = Trajectory ),
%  writelnq(tennis_trajectory_display(Colour, Trajectory_2)),
   forall( append(_, [H1, H2|_], Trajectory_2),
      tennis_point_display_hit(Colour, Colour, H1, H2) ),
%  star_line,
   !.


/* tennis_point_display_hit(Colour_P, Colour_L, Hit_1, Hit_2) <-
      */

tennis_point_display_hit(
      Colour_P, Colour_L, N-[X1, Y1], M-[X2, Y2]) :-
   !,
   dislog_variable_get(tennis_picture, Picture),
   tennis_convert_absolute_value_to_display_xy(1, X1, Y1, U1, V1),
   tennis_convert_absolute_value_to_display_xy(1, X2, Y2, U2, V2),
   tennis_hit_display_filled(Colour_P,  N-[U1, V1]),
   tennis_hit_display_filled(Colour_P,  M-[U2, V2]),
   vector_add([3,3,3,3], [U1,U2,V1,V2], [S1,S2,T1,T2]),
   tennis_line_send(Picture, (colour(Colour_L), S1-T1, S2-T2)).
tennis_point_display_hit(Colour_P, Colour_L, H1, H2) :-
   tennis_point_display_hit(Colour_P, Colour_L, ''-H1, ''-H2).


/******************************************************************/


