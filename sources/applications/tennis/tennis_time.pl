

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Time                                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tennis_current_time([H, M, S]) <-
      */

tennis_current_time([H, M, S]) :-
   get_time(T), 
   convert_time(T, _, _, _, H, M, S, _).


/* colon_name_to_terms(Name, Xs) <-
      */

colon_name_to_terms(Name, Xs) :-
   name_split_at_position([":"], Name, Names),
   maplist( term_to_atom, Xs, Names ).


/* tennis_colon_structure_to_time(Time, [H, M, S]) <-
      */

tennis_colon_structure_to_time(Time, [H, M, S]) :-
   name_split_at_position([":"], Time, Xs),
   maplist( term_to_atom, [H, M, S], Xs ).


/* tennis_hit_time(Time) <-
      */

tennis_hit_time(Time) :-
   ( ( tennis_current_time([CHo,CMi,CSe]), 
       dislog_variable_get(video_start_time, StartTime),
       time_difference([CHo,CMi,CSe], StartTime, [Ho,Mi,Se]))
   ; tennis_current_time([Ho,Mi,Se])),
   tennis_number_to_two_digits(Ho, Ho2),
   tennis_number_to_two_digits(Mi, Mi2),
   tennis_number_to_two_digits(Se, Se2),
   list_to_colon_structure([Ho2, Mi2, Se2], Time).


/* tennis_video_start_time(Video_Start_Time) <-
      */

tennis_video_start_time(Video_Start_Time) :-
   ( ( name(Video_Start_Time, A),
       list_split_at_position([".",":",","], A, Ls),
       maplist(name,
          HoMiSe, Ls),
       tennis_current_time([Ho,Mi,Se]),
       time_difference([Ho,Mi,Se], HoMiSe, StartTime),
       dislog_variable_set(video_start_time, StartTime) )
   ; true ).


/* triple_to_time([Rally, Numbers], [Rally, Times]) <-
      */

triple_to_time([Rally, Numbers], [Rally, Times]) :-
   tennis_numbers_to_time(Numbers, Times).


/* tennis_numbers_to_time([H1, M1, S1], [H2, M2, S2]) <-
      normalizes the time */
 
tennis_numbers_to_time([Hours, Minutes, Seconds], [H, M, S]) :-
   S is Seconds mod 60,
   S_Times is (Seconds - S) / 60,
   M is (Minutes + S_Times) mod 60,
   M_Times is (Minutes + S_Times - M) / 60,
   H is Hours + M_Times.


/* tennis_list_to_time_format([H, M, S], Time) <-
      */

tennis_list_to_time_format([H, M, S], Time) :-
   names_append_with_separator([H, M, S], ':', Time).


/* tennis_video_time_add(Time_1, Plus, Time_2) <-
      */

tennis_video_time_add(Time_1, Plus, Time_2) :-
   colon_name_to_terms(Time_1, Time_A),
   colon_name_to_terms(Plus, Time),
   time_addition(Time_A, Time, Time_B),
   list_to_colon_structure(Time_B, Time_C),
   term_to_atom(Time_C, Time_2),
   !.


/* tennis_video_time_subtract(Time_1, Minus, Time_2) <-
      */

tennis_video_time_subtract(Time_1, Minus, Time_2) :-
   colon_name_to_terms(Time_1, Time_A),
   colon_name_to_terms(Minus, Time),
   time_difference(Time_A, Time, Time_B),
   list_to_colon_structure(Time_B, Time_C),
   term_to_atom(Time_C, Time_2),
   !.


/* time_addition(Xs, Ys, Zs) <-
      */

time_addition(Xs, Ys, Zs) :-
   reverse(Xs, Us),
   reverse(Ys, Vs),
   time_addition_2(Us, Vs, 0, Ws,_),
   reverse(Zs, Ws).

time_addition_2([X|Xs], [Y|Ys], C, [Z|Zs], NC) :-
   time_addition_part(X, Y, C, Z, NC),
   time_addition_2(Xs, Ys, NC, Zs, _).
time_addition_2([], [], _, [], _).

time_addition_part(N1, N2, Carryover, N, Carryover_2) :-
   60 < N1 + N2 + Carryover,
   N is (N1 + N2 + Carryover - 60),
   Carryover_2 is 1.
time_addition_part(N1, N2, Carryover, N, Carryover_2) :-
   60 >= N1 + N2 + Carryover,
   N is (N1 + N2 + Carryover),
   Carryover_2 is 0.


/* time_difference(Xs, Ys, Zs) <-
      */

time_difference(Xs, Ys, Zs) :-
   reverse(Xs, Us),
   reverse(Ys, Vs),
   time_difference_2(Us, Vs, 0, Ws, _),
   reverse(Zs, Ws).

time_difference_2([X|Xs], [Y|Ys], C, [Z|Zs], NC) :-
   time_difference_part(X, Y, C, Z, NC),
   time_difference_2(Xs, Ys, NC, Zs, _).
time_difference_2([], [], _, [], _).

time_difference_part(N1, N2, Carryover, N, Carryover_2) :-
   N1 >= N2 + Carryover,
   N is (N1 - N2 - Carryover),
   Carryover_2 is 0.
time_difference_part(N1, N2, Carryover, N, Carryover_2) :-
   N1 < N2 + Carryover,
   N is (N1 + 60 - N2 - Carryover),
   Carryover_2 is 1.


/* tennis_settime_for_current_point(N, Times, Time) <-
      */

tennis_settime_for_current_point(N, [[N1, _]|Times], Time2) :-
   N > N1,
   tennis_settime_for_current_point(N, Times, Time2). 
tennis_settime_for_current_point(_, [Times|_], Times).


/******************************************************************/


