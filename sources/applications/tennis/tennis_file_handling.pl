

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: File Handling                        ***/
/***                                                            ***/
/******************************************************************/


tennis_data_path(Path) :-
   dislog_variable(home, Home),
   concat(Home, '/projects/Tennis/', Path).
tennis_source_path(Path) :-
   dislog_variable(home, Home),
   concat(Home, '/sources/projects/tennis/', Path).

tennis_output_file(Path) :-
   tennis_data_path_extend('tennis_match.xml', Path).
tennis_backup_file(Path) :-
   tennis_data_path_extend('tennis_match_old.xml', Path).
tennis_undo_file(Path) :-
   tennis_data_path_extend('tennis_undo_file.xml', Path).
tennis_browser_message_file(Path) :-
   tennis_data_path_extend('browser_result.txt', Path).
tennis_preferences(Path) :-
   tennis_data_path_extend('preferences.xml', Path).
tennis_smil_filename(Path) :-
   tennis_data_path_extend('tennisvideo.smil', Path).
tennis_info_file(Path) :-
   tennis_data_path_extend('info_matches.html', Path).
tennis_diagnosis_file(Path) :-
   tennis_data_path_extend('diagnosis.html', Path).
tennis_search_pattern_file(Path) :-
   tennis_data_path_extend('search_pattern.html', Path).

tennis_statistics_file(Path) :-
   tennis_data_path_extend('statistics.html', Path).
tennis_statistics_frame_file(Path) :-
   dislog_variable(source_path, Sources),
   concat(Sources,
      'projects/tennis/tennis_statistics.html', Path).
tennis_statistics_ground_balls_frame_file(Path) :-
   dislog_variable(source_path, Sources),
   concat(Sources,
      'projects/tennis/tennis_hand_statistics.html', Path).


tennis_data_path_extend(File, Path) :-
   tennis_data_path(Home),
   concat(Home, File, Path).
  

/*** interface ****************************************************/


tennis_load_preferences :-
   tennis_preferences(File),
   tennis_xml_file_to_fn_term(File,FN_Term),
   tennis_set_preferences(FN_Term).


tennis_save_colour_change :-
   tennis_dislog_variable_get_colour([R,G,B]),
   tennis_preferences(File),
   tennis_xml_file_to_fn_term(File,FN_Term),
   Substitutions = [
      (active:[]:[Size,
         colour:[ red: R, green: G, blue: B]:[], Video]) -
      (active:[]:[Size,
         colour:[ red: _, green: _, blue: _]:[], Video])],
   fn_transform_elements(Substitutions,FN_Term,FN_Term2),
   tennis_fn_term_to_xml_file(FN_Term2,File).


tennis_save_court_size_change :-
   dislog_variables_get([
      factor - Factor,
      tennis_court_side - Side,
      tennis_court_behind - Behind]),
   tennis_save_court_size_change(Factor,Side,Behind).


tennis_save_court_size_change(Factor, Side, Behind) :-
   tennis_preferences(File),
   tennis_xml_file_to_fn_term(File, FN_Term),
   Substitutions = [
      (active:[]:[
          size:[factor: Factor, side: Side, behind: Behind,
             format: Format]:[],
          Colour, Video]) -
      (active:[]:[          
          size:[factor: _, side: _, behind: _,
             format: Format]:[],
          Colour, Video]) ],
   fn_transform_elements(Substitutions, FN_Term, FN_Term_2),
   tennis_fn_term_to_xml_file(FN_Term_2, File).
 

tennis_save_video_size_change :-
   dislog_variables_get([
      tennis_video_width - Width,
      tennis_video_height - Height]), 
   tennis_save_video_size_change(Width, Height).

tennis_save_video_size_change(Width, Height) :-
   tennis_preferences(File),
   tennis_xml_file_to_fn_term(File, FN_Term),
   Substitutions = [
      (active:[]:[Size, Colour,
          video:[width: Width, height: Height]:[]]) -
      (active:[]:[Size, Colour,
          video:[width: _, height: _]:[]])],
   fn_transform_elements(Substitutions, FN_Term, FN_Term_2),
   tennis_fn_term_to_xml_file(FN_Term_2, File).
  

tennis_undo_save :-
   tennis_undo_file(File),
   tennis_xml_file_to_fn_term(File, FN_Term),
   dislog_variable_get(tennis_current_output_file, File_2),
   tennis_fn_term_to_xml_file(FN_Term, File_2),
   tennis_variable_start(File).


tennis_file_handling :-
   tennis_data_path(Directory),
   new(Frame,frame('Tennis - File Manager')),
   send(Frame,append,new(Dialog_I,dialog)),
   dislog_variable_get(dislog_frame_interface_color,Colour),
   send(Dialog_I,background,Colour),
   send(Dialog_I,append,new(Browser,browser('',size(40,15)))),
   send(Browser,label,Directory),
%  send(Dialog_I,size,size(300,230)),
   send(new(Dialog,dialog),below,Dialog_I),
   checklist( send_append(Dialog), [
      'Load' - tennis_load_file(Frame, Browser?selection?key),
      'Save as' - tennis_save_as_file(Frame, Browser?selection?key),
      'Close' - send(Frame,destroy) ] ),
   directory_contains_files(Directory, Files),
   findall( File,
      ( member(File, Files),
        concat(_, '.xml', File) ),
      Xml_Files ),
   chain_list(Chain, Xml_Files),
   send(Browser,members,Chain),
%  send(Browser,members,directory(Directory)?files),
   send(Frame,open).


tennis_load_file(Frame, File) :-
   tennis_data_path(Directory),
   name_append(Directory, File, Path),
   dislog_variable_set(tennis_current_output_file, Path),
   dislog_variable_set(tennis_to_search_files, [Path]),
   tennis_variable_start(Path),
   tennis_display_output_file_name,
   send(Frame, destroy).   

tennis_load_file(File) :-
   get(@finder, file, open, '*', File).


tennis_save_as_file(Frame, File) :-
   tennis_data_path(Directory),
   name_append(Directory, File, Path),
   dislog_variable_get(tennis_current_output_file, Current_File),
   tennis_xml_file_to_fn_term(Current_File, FN_Term),
   tell(Path),
   told,
   dislog_variable_set(tennis_current_output_file, Path),
   dislog_variable_set(tennis_to_search_files, [Path]),
   tennis_display_output_file_name,
   tennis_fn_term_to_xml_file(FN_Term, Path),
   send(Frame, destroy).


/* Save Match States */

tennis_start_new_match :-
   tennis_picture_uncolour,
   dislog_variable_get(tennis_current_output_file, File),
   Video = 'defaultvideo.avi',
   dislog_variable_set(current_video_file, Video),
   Game_Score_A = 0, Game_Score_B = 0,
   dislog_variable_set(game_score('A'), Game_Score_A),
   dislog_variable_set(game_score('B'), Game_Score_B),
   Point_Score_A = 0, Point_Score_B = 0,
   dislog_variables_set([
      point_score('A') - Point_Score_A,
      point_score('B') - Point_Score_B,
      name_player_A - 'Default A',
      name_player_B - 'Default B',
      match_score_A - ' ',
      match_score_B - ' ',
      hit_id - 0 ]),
   tennis_xml_file_to_fn_term(File,FN_Term),
   tennis_backup_file(Path),
   tennis_fn_term_to_xml_file(FN_Term, Path),
   Event_Game = [
      game:[id: 1, service: 'AorB', 
         score_A: Game_Score_A, 
         score_B: Game_Score_B]:[] ],
   Event_Set = [
      set:[id: 1,video_file: 'defaultvideo.avi', 
         score_A: Point_Score_A, score_B: Point_Score_B]:[
         Event_Game] ],
   Event_Match = [
      match:[]:[
         [player:[id :'A', name:'Default A']:[]], 
         [player:[id :'B', name:'Default B']:[]],
         result:[]:[],
         [match_facts:[tournament: '---', surface: '---', 
             where: '---', when: '---', round: '---']],
         Event_Set ] ],
   tennis_fn_term_to_xml_file(Event_Match,File),
   tennis_variable_start(File).


tennis_events_save_point :-
   dislog_variables_get([
      tennis_current_output_file - File,
      tennis_point_fn - HitsR,
      point_score('A') - Point_Score_A,
      point_score('B') - Point_Score_B,
      point_winner - Winner,
      current_player_at_top - Top,
      current_server - Current_Server,
      unforced_error - Error ]),
   reverse(HitsR, Hits),
   tennis_xml_file_to_fn_term(File,FN_Term),
   tennis_undo_file(UndoFile),
   tennis_fn_term_to_xml_file(FN_Term,UndoFile),
   tennis_determine_last_set(FN_Term, LastSets),
   tennis_determine_last_game(LastSets, LastGames),
   tennis_determine_next_point_id(LastGames, Point_ID),
   tennis_determine_game_id(LastSets, Game_IDA),
   term_to_atom(Game_IDA, Game_ID),
   Point_New = [
      point:[id: Point_ID, top: Top, 
         service: Current_Server,
         score_A: Point_Score_A, 
         score_B: Point_Score_B,
         winner: Winner, error: Error]:[
         Hits] ],
   append(LastGames, Point_New, Points_Save),
   Sets := FN_Term^match,
   last_element(Sets, Last_Set),
   element_to_list(Last_Set, Last_Sets),
   Substitutions = [
      (game:[id: Game_ID, service: Server,
          score_A: A, score_B: B]: Points_Save) -
      (game:[id: Game_ID, service: Server,
          score_A: A, score_B: B]: _) ],
   fn_transform_elements(Substitutions, Last_Sets, Last_Set_Save),
   substitute(Last_Set, Sets, Last_Set_Save, Set_Save),
   tennis_fn_term_to_xml_file([match:[]:Set_Save], File),
   dislog_variables_set([
      tennis_point_fn - [],
      hit_id - 0,
      unforced_error - 0]). 


tennis_events_save_game :-
   tennis_events_save_point,
   dislog_variable_get(tennis_current_output_file, File),
   tennis_xml_file_to_fn_term(File, FN_Term),
   tennis_determine_last_set(FN_Term, LastSets),
   tennis_determine_last_set_id(FN_Term, Set_IDA),
   term_to_atom(Set_IDA, Set_ID),
   tennis_determine_last_game_id(LastSets, Game_ID),
   Game_ID_New is (Game_ID + 1),
   dislog_variables_get([
      game_score('A') - Game_Score_A,
      game_score('B') - Game_Score_B]),
   Game_New = [
      game:[id: Game_ID_New, service: 'AorB',
         score_A: Game_Score_A,
         score_B: Game_Score_B]:[] ] ,
   append(LastSets, Game_New, Games_Save),
   Substitutions = [
      (set:[id: Set_ID, video_file: Video,
          score_A: Game_Score_A,
          score_B: Game_Score_B]: Games_Save) - 
      (set:[id: Set_ID, video_file: Video,
          score_A: _,
          score_B: _]: _)], 
   fn_transform_elements(Substitutions,FN_Term, FN_Term_Save),
   tennis_fn_term_to_xml_file(FN_Term_Save, File).


tennis_events_save_set :-
   tennis_events_save_point, 
   dislog_variables_get([
      tennis_current_output_file - File,
      game_score('A') - Set_Score_A,
      game_score('B') - Set_Score_B]),
   tennis_xml_file_to_fn_term(File, FN_Term),
   Game_ID_New is 1,
   tennis_determine_set_id(FN_Term, Set_IDs),
   Set_ID_New is Set_IDs +1,
   Result := FN_Term^match^result,
   Set_Result:= [
      score:[set: Set_IDs, 
         player_A: Set_Score_A, player_B: Set_Score_B]:[]],
   append(Result, Set_Result, Result_New),
   Substitutions = [
      (result:[]:[Result_New]) - (result:[]:_) ],
   fn_transform_elements(Substitutions, FN_Term, FN_Term_1),
   AllSets := FN_Term_1^match,
   Game_New = [
      game: [id: Game_ID_New, service: 'AorB',
         score_A: 0, score_B: 0]:[] ],
   Set_New = [
      set: [id: Set_ID_New, video_file: 'defaultvideo.avi',
         score_A: 0, score_B: 0]: Game_New ],
   append(AllSets, Set_New, Sets_Save),
   fn_transform_elements([(match:Sets_Save)-(match:_)], 
      FN_Term_1, FN_Term_Save),
   tennis_fn_term_to_xml_file(FN_Term_Save, File).


tennis_events_save_server :-
   dislog_variable_get(tennis_current_output_file, File),
   tennis_xml_file_to_fn_term(File, FN_Term),
   dislog_variable_get(current_server, Server),
   tennis_determine_last_set(FN_Term, LastSets),
   tennis_determine_last_set_id(FN_Term, Set_ID2),
   term_to_atom(Set_ID2, Set_ID),
   tennis_determine_last_game_id(LastSets, ID2),  
   term_to_atom(ID2, ID),
   Substitutions = [
      (game:[id: ID, service: Server,
          score_A: Score_A, score_B: Score_B]: Points) -
      (game:[id: ID, service: _,
          score_A: Score_A, score_B: Score_B]: Points)],
    fn_transform_elements(Substitutions, LastSets, LastSets2),
    Substitutions2 = [
      (set:[id: Set_ID, video_file: Video,
          score_A: Game_Score_A,
          score_B: Game_Score_B]: LastSets2) - 
      (set:[id: Set_ID, video_file: Video,
          score_A: Game_Score_A,
          score_B: Game_Score_B]: _)],
   fn_transform_elements(Substitutions2, FN_Term, FN_Term_Save),
   tennis_fn_term_to_xml_file(FN_Term_Save, File).


tennis_events_save_change_hit([X, Y, ID], X_New, Y_New, Hand, Type) :-
   dislog_variable_get(tennis_current_output_file,File),
   tennis_xml_file_to_fn_term(File,FN_Term),
   tennis_terms_to_atoms([X_New - X_Atom, Y_New - Y_Atom]),
   Substitutions = [
      (hit:[id: ID, hand: Hand, type: Type,
          time: Time, x: X_Atom, y: Y_Atom]:[]) -
      (hit:[id: ID, hand: _, type: _,    
          time: Time, x: X, y: Y]:[]) ],
   fn_transform_elements(Substitutions, FN_Term, FN_Term_2),
   tennis_fn_term_to_xml_file(FN_Term_2, File).


tennis_save_player_name(Name_A, Name_B) :-
   dislog_variable_get(tennis_current_output_file, File),
   tennis_xml_file_to_fn_term(File, FN_Term),
   Substitutions_A = [
      (player:[id:'A', name: Name_A]:[] ) -
      (player:[id:'A', name: _]:[]) ],
   Substitutions_B = [
      (player:[id:'B', name: Name_B]:[] ) -
      (player:[id:'B', name: _]:[]) ],
   fn_transform_elements(Substitutions_A, FN_Term, FN_Term_A),
   fn_transform_elements(Substitutions_B, FN_Term_A, FN_Term_AB),
   tennis_fn_term_to_xml_file(FN_Term_AB, File),
   tennis_send_label_group(Name_A, Name_B).


tennis_save_video_name(Video_File) :-
   dislog_variable_get(tennis_current_output_file, File),
   tennis_xml_file_to_fn_term(File, FN_Term),
   Substitutions = [
      (set:[id: Set_ID,
          video_file: Video_File,
          score_A: A, score_B: B]: Games_Save) - 
      (set:[id: Set_ID,
          video_file: 'defaultvideo.avi',
          score_A: A, score_B: B]: Games_Save)], 
   fn_transform_elements(Substitutions, FN_Term, FN_Term_2),
   tennis_fn_term_to_xml_file(FN_Term_2, File).


tennis_save_match_facts(Tournament, Surface, Where, When, Round) :-
   dislog_variable_get(tennis_current_output_file, File),
   tennis_xml_file_to_fn_term(File, FN_Term),
   Substitutions = [
      (match_facts:[]:[
         tournament:[]:[Tournament], surface:[]:[Surface],
         where:[]:[Where], when:[]:[When], round:[]:[Round] ]) -
      (match_facts:[]:[
         tournament:[]:[_], surface:[]:[_],
         where:[]:[_], when:[]:[_], round:[]:[_] ]) ],
   fn_transform_elements(Substitutions, FN_Term, FN_Term_2),   
   tennis_fn_term_to_xml_file(FN_Term_2, File). 
 

tennis_view_xml_file :-
   dislog_variable_get(tennis_current_output_file, File),
   win_shell(open, File).


tennis_browser_message_to_file_start :-
   tennis_browser_message_file(File),
   tell(File),
   writeln('Results:'),
   told.
  
tennis_browser_message_to_file(ID, Score, Time, Winner) :-
   tennis_browser_message_file(File),
   append(File),
   write_list([ID, '\t', Time, '\n',
      Score, '\t', Winner, '\n \n' ]),
   told.
 

tennis_write_info_to_file(File, NameA, NameB, Result,
      Tournament, Surface, Where, When, Round) :-
   concat([NameA, ' - ', NameB], Players),
   Html = li:[
      font:[color:'#8698fd']:[File], '<br><p>',
      table:[border:0, align:left]:[
         tr:[td:['Players:'], td:[Players]],
         tr:[td:['Result:'], td:[Result]],
         tr:[td:['Tournament:'], td:[Tournament]],
         tr:[td:['Surface:'], td:[Surface]],
         tr:[td:['Location:'], td:[Where]],
         tr:[td:['Date:'], td:[When]],
         tr:[td:['Round:'], td:[Round]],
         tr:[td:[''], td:['']] ]],
   dwrite(xml, Html).
%  tennis_write_html_table([
%     'Filename: ' - File,
%     'Players: ' - Players, 'Result: ' -  Result,
%     'Tournament: ' - Tournament, 'Surface: ' - Surface,
%     'Where: ' - Where, 'When: ' - When, 'Round: ' - Round,
%     ' ' - ' ', ' ' - ' ']).


/******************************************************************/


