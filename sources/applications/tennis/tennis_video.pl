

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Video Playing                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tennis_video_play_sgp(Set, Game, Point) <-
      */

tennis_video_play_sgp(Set, Game, Point) :-
   tennis_sgp_to_times(Set, Game, Point, Start, End),
   write_list(['playing ', Set-Game-Point, ' ', Start-End, '\n']),
%  Message =
%     playing-[Set-Game-Point]-[Start,End],
%  writelnq(Message),
   tty_flash,
   tennis_video_play_sequence(Set, Game, Start, End).

tennis_sgp_to_times(Set, Game, Point, Start, End) :-
   term_to_atom(Set, S),
   term_to_atom(Game, G),
   term_to_atom(Point, P),
   File = 'projects/Tennis/2002_sampras_agassi_final_times.xml',
   dread(xml, File, [Xml]),
   ( nonvar(Point) -> _ := Xml/set::[@id=S]/game::[@id=G]/
          point::[@id=P, @time_1=Start, @time_2=End]
   ; nonvar(Game) -> _ := Xml/set::[@id=S]/
          game::[@id=G, @time_1=Start, @time_2=End]
   ; nonvar(Set) -> _ := Xml/
          set::[@id=S, @time_1=Start, @time_2=End] ).


/* tennis_video_play_sequence(Set, Game, Start, End) <-
      */

tennis_video_play_sequence(Set, Game, Start, End) :-
   tennis_video_set_to_time(Set, Game, Start, File, S),
   tennis_video_set_to_time(Set, Game, End, _, E),
   video_play_sequence(File, '00:00:20', S, E).


/* tennis_video_set_to_time(Set, Game, Time_R, File, Time_A) <-
      */

tennis_video_set_to_time(Set, Game, Time_R, File, Time_A) :-
   ( Set = 1, Game =< 7, Add = +'00:00:00', N = 1
   ; Set = 1, Game >= 8, Add = +'00:01:10', N = 1
   ; Set = 2, Game =  _, Add = +'00:29:25', N = 1
   ; Set = 3, Game =< 4, Add = +'00:59:45', N = 1
%  ; Set = 3, Game >= 5, Add = -'00:13:12', N = 34
   ; Set = 3, Game >= 5, Add = -'00:13:18', N = 34
%  ; Set = 4, Game =< 7, Add = +'00:41:17', N = 34
   ; Set = 4, Game =< 7, Add = +'00:42:35', N = 34
%  ; Set = 4, Game >= 8, Add = -'00:30:34', N = 4 ),
   ; Set = 4, Game >= 8, Add = -'00:30:42', N = 4 ),
   concat(['samprasagassiset', N, '.avi'], F),
   ( Add = +A -> tennis_video_time_add(Time_R, A, Time_A)
   ; Add = -A -> tennis_video_time_subtract(Time_R, A, Time_A) ),
   tennis_data_path(Path), concat(Path, F, File).


/* video_play_seq <-
      */

video_play_seq :-
   tennis_smil_filename(File_Smil),
   dread(xml, File_Smil, [Xml]),
   Start := Xml/descendant::video@'clip-begin',
   End   := Xml/descendant::video@'clip-end',
   Path  := Xml/descendant::video@src,
   tennis_data_path(Data_Path),
   concat(Data_Path, Path, File),
   video_play_sequence(File, '00:00:00', Start, End).


/* video_play_sequence(File, Frame, Start, End) <-
      */

video_play_sequence(File, Frame, Start, End) :-
   tennis_video_time_subtract(Start, Frame, S),
   tennis_video_time_add(End, Frame, E),
   video_play_sequence(File, S, E).

video_play_sequence(File, Start, End) :-
   video_determine_length(Start, End, Length),
%  member(Player, [gmplayer, mplayer, smplayer]),
   Player = mplayer,
   concat([Player, ' -ao alsa -msglevel all=-1 -autosync 30 -geometry 700x525 ',
      '-ss ', Start, ' -endpos ', Length, ' ', File], Command),
%  -msglevel all=-1 -geometry 640x480 -endpos Length
   us(Command),
   writeln(user, Command).
%  win_shell(open, File).


/* video_determine_length(Start, End, Length) <-
      */

video_determine_length(Start, End, Length) :-
   colon_name_to_terms(Start, Time_S),
   colon_name_to_terms(End, Time_E),
   time_difference(Time_E, Time_S, Time),
   list_to_colon_structure(Time, Term),
   term_to_atom(Term, Length).


/* video_play <-
      */

/* Prolog Version 5.0.10

video_play :-
   get(@finder, file, open, 'avi', Path),
   tennis_data_path(Data_Path),
   name_append(Data_Path, File, Path),
   dislog_variable_set(current_video_file, File),
   tennis_save_video_name(File),
   tennis_display_video_file_name,
   win_shell(open, Path).

*/

video_play :-
   tennis_data_path(Directory),
   new(Frame,frame('Tennis - File Manager')),
   send(Frame,append,new(Dialog_I,dialog)),
   dislog_variable_get(dislog_frame_interface_color,Colour),
   send(Dialog_I,background,Colour),
   send(Dialog_I,append,new(Browser,browser)),
   send(Browser,label,Directory),
   send(new(Dialog,dialog),below,Dialog_I),
   checklist( send_append(Dialog), [
      'Play' - video_play(Frame, Browser?selection?key),
      'Close' - send(Frame,destroy) ] ),
   send(Browser,members,directory(Directory)?files),
   send(Frame,open).

video_play(Frame, File) :-
   send(Frame, destroy),
   tennis_data_path(Data_Path),
   name_append(Data_Path, File, Path),
   dislog_variable_set(current_video_file, File),
   tennis_save_video_name(File),
   tennis_display_video_file_name,
   win_shell(open, Path).   


/* tennis_video_play(Start) <-
      */

tennis_video_play(Start) :-
   tennis_video_adjust_start_time(Start, Start_2),
   tennis_smil_filename(File),
   dread(xml, File, [Xml]),
   Path := Xml/descendant::video@src,
   tennis_data_path(Data_Path),
   concat(['mplayer -ss ', Start_2, ' ', Data_Path, Path], Command),
   us(Command).

tennis_video_adjust_start_time(Time_1, Time_2) :-
   colon_name_to_terms(Time_1, Time_A),
   time_difference(Time_A, [00,00,10], Time_B),
   list_to_colon_structure(Time_B, Time_C),
   term_to_atom(Time_C, Time_2).


/* tennis_change_video_size_default <-
      */

tennis_change_video_size_default :-
   dislog_variable_get(change_video_size_frame, Frame),
   tennis_preferences(File),
   tennis_xml_file_to_fn_term(File, FN),
   FN_Term := FN^preferences^default,
   tennis_get_preferences_video(FN_Term, Width, Height), 
   dislog_variables_set([
      tennis_video_width - Width,
      tennis_video_height - Height]),
   tennis_save_video_size_change,
   send(Frame, destroy).


/* tennis_change_video_size <-
      */

tennis_change_video_size :-
   dislog_variables_get([
      change_video_size_frame - Frame,
      input_change_video_width - Ref_Video_Width,
      input_change_video_height - Ref_Video_Height]),
   get(Ref_Video_Width,selection, Width_Atom),
   get(Ref_Video_Height,selection, Height_Atom),
   tennis_terms_to_atoms([
      Width - Width_Atom,
      Height - Height_Atom]),
   dislog_variables_set([
      tennis_video_width - Width,
      tennis_video_height - Height]),
   tennis_save_video_size_change(Width, Height),
   send(Frame, destroy).


/* tennis_determine_smil_file_variable <-
      */

tennis_determine_smil_file_variable :-
   dislog_variables_get([
      current_video_file - File_Name,
      change_Hit - Points]),
   tennis_data_path(Data_Path),
   concat(Data_Path, File_Name, Video_Name),
   first(Points, First),
   last(Points, Last),
   last(First, Start),
   last(Last, End),
   colon_name_to_terms(Start, Start_1),
   colon_name_to_terms(End, End_1),
   time_difference(Start_1, [00,00,10], Start_Add),
   time_addition(End_1, [00,00,13], End_Add),
   list_to_colon_structure(Start_Add, Start_Time),
   list_to_colon_structure(End_Add, End_Time),
   Player_Names ='Agassi-Sampras',
   tennis_create_smil_file(Video_Name,
      Start_Time, End_Time, Player_Names).


/* tennis_create_smil_file(Video_Name,
         Start_Time, End_Time, Player_Names) <-
      */

tennis_create_smil_file(Video_Name,
      Start_Time, End_Time, Player_Names) :-
   tennis_path_to_filename(Video_Name, Video),
   tennis_smil_filename(Path),
   dislog_variables_get([
      tennis_video_width - Width,
      tennis_video_height - Height]),
   Xml = smil:[xmlns:'http://www.w3.org/2001/SMIL20/Language']:[
      head:[ meta:[name:title, content:Player_Names]:[],
         layout:[
            root-layout:[width:Width, height:Height, id:vr]:[],
            region:[id:video_region, fit:fill]:[] ] ],
      body:[ seq:[ video:[
         region:video_region, src:Video,
         clip-begin:Start_Time,
         clip-end:End_Time ]:[] ] ] ],
   dwrite(xml, Path, Xml),
   video_play_seq.


/******************************************************************/


