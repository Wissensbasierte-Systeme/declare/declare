

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Queries                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface  ***************************************************/


/* tennis_file_assert_facts(Path) <-
      */

tennis_file_assert_facts :-
   File = '2002_sampras_agassi_final.xml',
   home_directory(Home),
   concat([Home, '/Declare/projects/Tennis/', File], Path),
   tennis_file_assert_facts(Path).

tennis_file_assert_facts(Path) :-
   dread(xml, Path, [Match]),
   tennis_match_assert_hits(Match),
   tennis_match_assert_services(Match),
   tennis_match_assert_players(Match).

tennis_match_assert_hits(Match) :-
   retract_facts(tennis:hit/9),
   findall( [Set, Game, Point, Hit, Hand, Type, Time, X, Y],
      ( H := Match/set::[@id=Set_2]/game::[@id=Game_2]
           /point::[@id=Point_2]/hit::[@id=Hit_2],
        [Hand, Type, Time, X_2, Y_2] :=
           H-[@hand, @type, @time, @x, @y],
        maplist( atom_number,
           [Set_2, Game_2, Point_2, Hit_2, X_2, Y_2],
           [Set, Game, Point, Hit, X, Y] ) ),
      Tuples ),
   assert_relation(tennis:hit, Tuples).

tennis_match_assert_services(Match) :-
   retract_facts(tennis:service/3),
   findall( [Set, Game, Service],
      ( _ := Match/set::[@id=Set_2]
           /game::[@id=Game_2, @service=Service],
        atom_number(Set_2, Set),
        atom_number(Game_2, Game) ),
      Tuples ),
   assert_relation(tennis:service, Tuples).

tennis_match_assert_players(Match) :-
   retract_facts(tennis:player/2),
   findall( [Id, Name],
      _ := Match/player::[@id=Id, @name=Name],
      Tuples ),
   assert_relation(tennis:player, Tuples).


/* tennis_facts_to_average_game_length(Tuples) <-
      */

tennis_facts_to_average_game_length :-
   tennis_facts_to_average_game_length(_).

tennis_facts_to_average_game_length(Tuples) :-
   ddbase_aggregate( [S, G, sort_length(P), length(H)],
      tennis:hit(S, G, P, H, _, _, _, _, _),
      Tuples_2 ),
   xpce_display_table(['Set', 'Game', 'Points', 'Hits'], Tuples_2),
   ddbase_aggregate( [Player, avg(Ps), avg(Hs)],
      ( member([S, G, Ps, Hs], Tuples_2),
        tennis:service(S, G, Service),
        tennis:player(Service, Player) ),
      Tuples ),
   xpce_display_table(['Service', 'Points', 'Hits'], Tuples).

sort_length(Xs, N) :-
   sort(Xs, Ys),
   length(Ys, N).

avg(Xs, X) :-
   average(Xs, Y),
   round(Y, 2, X).


/******************************************************************/


