

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Analysis of Hits                     ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(tennis_aggregation, game).


/*** tests ********************************************************/


test(tennis_hit_analysis, aces) :-
   Player = 'A',
%  Player = 'B',
   File = 'projects/Tennis/2002_sampras_agassi_final_times.xml',
   tennis_play_aces(File, Player).

test(tennis_hit_analysis, volleys) :-
   tennis_file_to_volleys_per_game(
      '2002_sampras_agassi_final.xml').
test(tennis_hit_analysis, ranges) :-
   tennis_ranges_to_bar_charts(
      '2002_sampras_agassi_final.xml').
test(tennis_hit_analysis, returns) :-
   tennis_file_to_returns(
      '2002_sampras_agassi_final.xml').


/*** interface ****************************************************/


/* point_to_length_and_distance(Point, Length, Dist) <-
      */

point_to_length_and_distance(Point, Length, Dist) :-
   Point = point:_:Hits,
   length(Hits, Length),
   point_to_distance(Point, Dist).

point_to_distance(Point, Dist) :-
   Point = point:_:Hits,
   findall( D,
      ( append(_, [H1, H2|_], Hits),
        hit_to_xy(H1, C1),
        hit_to_xy(H2, C2),
        distance(C1, C2, D) ),
      Ds ),
   sum_of_list(Ds, Dist).

hit_to_xy(Hit, [X, Y]) :-
   Hit = hit:[_, _, _, _, x:X, y:Y]:[].

distance([X1, Y1], [X2, Y2], Dist) :-
   maplist( atom_number_,
      [X1, Y1, X2, Y2], [U1, V1, U2, V2] ),
   Dist is sqrt( (U2-U1)^2 + (V2-V1)^2 ).

sum_of_list([X|Xs], Sum) :-
   sum_of_list(Xs, S),
   Sum is X + S.
sum_of_list([], 0).

atom_number_(A, N) :-
   atom(A),
   !,
   atom_number(A, N).
atom_number_(N, N) :-
   number(N).


/* tennis_play_aces(File, Player) <-
      */

tennis_play_aces(File, Player) :-
   dread(xml, File, [Xml]),
   ddbase_aggregate( [average(K)],
      ( Xs := Xml/set/game::[@service=Player]/content::'*',
        length(Xs, K) ),
      [[H]] ),
   round(H, 1, L_avg),
   findall( Game,
      Game := Xml/set/game::[@service=Player],
      Games ),
   length(Games, M),
   findall( Point,
      Point := Xml/set/game::[@service=Player]/point,
      Points ),
   length(Points, L),
   findall( Ace,
      ( Point := Xml/set::[@id=S]/game::[@id=G]/point::[@id=P],
        Player := Point@service,
        Hits := Point/content::'*',
        length(Hits, 2),
        maplist( term_to_atom, Ace, [S, G, P]) ),
      Aces ),
   length(Aces, N),
   xpce_display_table(['Set', 'Game', 'Point'], Aces),
   write_list([N, ' aces in ',
      M, ' service games with average length ', L_avg,
      '; totally ', L, ' points\n']),
   forall( member(Ace, Aces),
      apply(tennis_video_play_sgp, Ace) ).


/* tennis_file_to_match(File, Match) <-
      */

tennis_file_to_match(Match) :-
   dislog_variable_get(tennis_current_output_file, Path),
   tennis_xml_file_to_fn_term(Path, [Match]).

tennis_file_to_match(File, Match) :-
   tennis_data_path(Data_Path),
   concat(Data_Path, File, Path),
   tennis_xml_file_to_fn_term(Path, [Match]).


/* tennis_file_to_volleys_per_game(File, List) <-
      */

tennis_file_to_volleys_per_game :-
   tennis_file_to_match(Match),
   tennis_match_to_volleys_per_game(Match, _).

tennis_file_to_volleys_per_game(File) :-
   tennis_file_to_volleys_per_game(File, _).

tennis_file_to_volleys_per_game(File, List) :-
   tennis_file_to_match(File, Match),
   tennis_match_to_volleys_per_game(Match, List).

tennis_match_to_volleys_per_game(Match, List) :-
   tennis_match_to_selected_hits_multiset(
      Match, tennis_point_to_volley('A'), Multiset_A),
   tennis_match_to_selected_hits_multiset(
      Match, tennis_point_to_volley('B'), Multiset_B),
   pair_lists(Multiset_A, Multiset_B, Pairs),
   ( foreach(X, Pairs), foreach(Y, List) do
        ( X = [I:A, I:B],
          list_to_comma_structure(I, T),
          term_to_atom(T, Label),
          Y = [Label, A, B] ) ),
   tennis_volleys_to_bar_chart(Match, List),
   !.

tennis_volleys_to_bar_chart(Match, List) :-
   length(List, X), Y is 10,
   new(Frame, frame('Volleys')),
   send(Frame, append,
      new(W, auto_sized_picture)),
   send(new(Dialog, dialog), below, W),
   tennis_ranges_to_bar_chart_append_legend(Dialog, Match),
   send(W, display,
      new(BC, bar_chart(vertical, 0, Y, 200, X))),
   ( foreach([SG, A, B], List) do
        send(BC, append, bar_group(SG,
           bar('A', A, lightblue),
           bar('B', B, orange))) ),
   send(Frame, open).


/* tennis_ranges_to_bar_charts(File) <-
      */

tennis_ranges_to_bar_charts :-
   tennis_file_to_match(Match),
   tennis_ranges_to_bar_charts_for_match(Match).

tennis_ranges_to_bar_charts(File) :-
   tennis_file_to_match(File, Match),
   tennis_ranges_to_bar_charts_for_match(Match).

tennis_ranges_to_bar_charts_for_match(Match) :-
   Xs = [0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5],
   tennis_ranges_to_bar_chart(Match, x, Xs),
   Ys = [0, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12],
   tennis_ranges_to_bar_chart(Match, y, Ys).


/* tennis_ranges_to_bar_chart(Match, X_or_Y, Vs) <-
      */

tennis_ranges_to_bar_chart(Match, X_or_Y, Vs) :-
   length(Vs, X), Y is 400,
   name_append('Hits - ', X_or_Y, H),
   new(Frame, frame(H)),
   send(Frame, append,
      new(W, auto_sized_picture)),
   send(new(Dialog, dialog), below, W),
   tennis_ranges_to_bar_chart_append_legend(Dialog, Match),
   send(W, display,
      new(BC, bar_chart(vertical, 0, Y, Y, X))),
   checklist( tennis_range_to_bar_group(Match, X_or_Y, BC),
      Vs ),
   send(Frame, open).

tennis_range_to_bar_group(Match, x, BC, V) :-
   tennis_ranges_to_hits(Match, [[V, 6], [0, 13]], A, B),
   name_append(['x > ', V], Label),
   send(BC, append, bar_group(Label,
      bar('A', A, lightblue),
      bar('B', B, orange))).
tennis_range_to_bar_group(Match, y, BC, V) :-
   tennis_ranges_to_hits(Match, [[0, 6], [V, 13]], A, B),
   name_append(['y > ', V], Label),
   send(BC, append, bar_group(Label,
      bar('A', A, lightblue),
      bar('B', B, orange))).


/* tennis_ranges_to_bar_chart_append_legend(Dialog, Match) <-
      */

tennis_ranges_to_bar_chart_append_legend(Dialog, Match) :-
   tennis_to_player_legend(Match, 'A', Text_A),
   tennis_to_player_legend(Match, 'B', Text_B),
   send(Dialog, display, text('Players'), point(10, 9)),
   send(Dialog, display, new(Box_A, box(10, 10)), point(20, 30)),
   send(Box_A, colour, lightblue),
   send(Box_A, fill_pattern, colour(lightblue)),
   send(Dialog, display, text(Text_A), point(40, 29)),
   send(Dialog, display, new(Box_B, box(10, 10)), point(20, 50)),
   send(Box_B, colour, orange),
   send(Box_B, fill_pattern, colour(orange)),
   send(Dialog, display, text(Text_B), point(40, 49)).

tennis_to_player_legend(Match, A_or_B, Text) :-
   Player := Match^player,
   A_or_B := Player@id,
   Player_A_or_B := Player@name,
   name_append([A_or_B, ': ', Player_A_or_B], Text).


/* tennis_ranges_to_hits(Match, Ranges, A, B) <-
      */

tennis_ranges_to_hits(Match, Ranges, A, B) :-
   tennis_match_to_selected_hits(Match,
      tennis_point_to_hit_within_range('A', Ranges),
      [_, _, _], Selected_Hits_A),
   length(Selected_Hits_A, A),
   tennis_match_to_selected_hits(Match,
      tennis_point_to_hit_within_range('B', Ranges),
      [_, _, _], Selected_Hits_B),
   length(Selected_Hits_B, B),
   writeln(Ranges-A-B).

tennis_point_to_hit_within_range(
      Player, [X_Range, Y_Range], Point, Hit) :-
   P := Point@service,
   Hit := Point^hit,
   Id := Hit@id, term_to_atom(N, Id),
   N > 2,
   Type := Hit@type, Type \= volley,
   ( P = Player, even(N)
   ; P \= Player, odd(N) ),
   \+ tennis_hit_before_was_type(Point, N, volley),
   tennis_range_condition(Hit, x, X_Range),
   tennis_range_condition(Hit, y, Y_Range).

tennis_hit_before_was_type(Point, N, Type) :-
   M is N - 1,
   term_to_atom(M, Id),
   Hit := Point^hit,
   Id := Hit@id,
   Type := Hit@type.

tennis_hit_before_was(Point, N, Conditions) :-
%  writeln(user, tennis_hit_before_was(N, Conditions)),
   M is N - 1,
   term_to_atom(M, Id),
   Hit := Point^hit,
   Id := Hit@id,
   hit_was_circumvented_test(Hit),
   fn_check_conditions(Hit, Conditions).

hit_was_circumvented_test(Hit) :-
   dislog_variable_get(tennis_hit_was_circumvented, yes),
   !,
   hit_was_circumvented(Hit).
hit_was_circumvented_test(_).

hit_was_circumvented(Hit) :-
   Xa := Hit@x, term_to_atom(X, Xa),
   Ya := Hit@y, term_to_atom(Y, Ya),
   Hand := Hit@hand,
   !,
   hit_was_circumvented(Hand, X, Y).

hit_was_circumvented(forehand, X, Y) :-
   !,
   X * Y >= 0.
hit_was_circumvented(backhand, X, Y) :-
   X * Y =< 0.

tennis_range_condition(Hit, Axis, [A, B]) :-
   X := Hit@Axis,
   term_to_atom(T, X),
   A < abs(T),
   abs(T) < B.


/* tennis_point_to_service(Player, Point, Hit) <-
      */

tennis_point_to_service(Player, Point, Hit) :-
   Player := Point@service,
   Hit_2 := Point^hit,
   Id := Hit_2@id, term_to_atom(N, Id),
   N = 2,
   ( forehand := Hit_2@hand,
     Hit := Hit_2*[@point_id:1]
   ; backhand := Hit_2@hand,
     Hit := Hit_2*[@point_id:2] ).


/* tennis_point_to_volley(Player, Point, Hit) <-
      */

tennis_point_to_volley(Player, Point, Hit) :-
   P := Point@service,
   Hit := Point^hit,
   Id := Hit@id, term_to_atom(N, Id),
   N > 2,
   volley := Hit@type,
   ( P = Player, odd(N)
   ; P \= Player, even(N) ).


/* tennis_point_to_return_taken_as(Player, Type, Point, Hit) <-
      */

tennis_point_to_return_taken_as(Player, Type, Point, Hit) :-
   tennis_point_to_return(Player, Point, Hit),
   Type := Hit@type.

tennis_point_to_return(Player, Point, Hit) :-
   Server := Point@service,
   Server \= Player,
   Hit_2 := Point^hit,
   Id := Hit_2@id, term_to_atom(N, Id),
   N = 3,
   tennis_hit_add_point_id(Point, Hit_2, Hit).
   
tennis_hit_add_point_id(Point, Hit_1, Hit_2) :-
   Point_Id := Point@id,
   Hit_2 := Hit_1*[@point_id:Point_Id],
   !.


/* tennis_point_to_hit_of_hand(Player, Hand, Point, Hit) <-
      */

tennis_point_to_hit_of_hand(Player, Hand, Point, Hit) :-
%  nl(user), writeln(user, Point),
   Server := Point@service,
   Hit_2 := Point^hit,
   Id := Hit_2@id, term_to_atom(N, Id),
   N >= 3,
%  write(user, N),
   tennis_point_to_hit_of_hand_test(Server, Player, N),
%  write(user, a),
   tennis_hit_before_was(Point, N, [@hand=Hand, @type=ground]),
%  writeln(user, b),
   tennis_hand_to_id(Hand, I),
   Hit := Hit_2*[@point_id:I].

tennis_point_to_hit_of_hand_test(Server, Player, N) :-
   even(N), Server == Player,
   !,
   dislog_variable_get(
      tennis_point_to_hit_of_hand_for_service_game, yes).
tennis_point_to_hit_of_hand_test(Server, Player, N) :-
   odd(N), Server \= Player,
   !,
   dislog_variable_get(
      tennis_point_to_hit_of_hand_for_return_game, yes).

tennis_hand_to_id(forehand, 1) :-
   !.
tennis_hand_to_id(backhand, 2).


/* tennis_match_to_hits_of_hand(Match, Player, Type, Location) <-
      */

tennis_match_to_hits_of_hand(Xs) :-
   V1 = tennis_point_to_hit_of_hand_for_service_game,
   ( member(service, Xs),
     dislog_variable_set(V1, yes)
   ; dislog_variable_set(V1, no) ),
   V2 = tennis_point_to_hit_of_hand_for_return_game,
   ( member(return, Xs),
     dislog_variable_set(V2, yes)
   ; dislog_variable_set(V2, no) ),
   V3 = tennis_hit_was_circumvented,
   ( member(circumvented, Xs),
     dislog_variable_set(V3, yes)
   ; dislog_variable_set(V3, no) ),
   dislog_variable_get(tennis_lines_coordinates, Cs),
   dislog_variable_set(tennis_lines_coordinates, []),
   !,
   tennis_picture_uncolour,
   tennis_file_to_match(Match),
   tennis_match_to_hits_of_hand(Match, 'A', backhand, bottom),
   tennis_match_to_hits_of_hand(Match, 'A', forehand, bottom),
   tennis_match_to_hits_of_hand(Match, 'B', backhand, top),
   tennis_match_to_hits_of_hand(Match, 'B', forehand, top),
   dislog_variable_set(tennis_lines_coordinates, Cs).

tennis_match_to_hits_of_hand(Match, Player, Hand, Location) :-
   tennis_match_to_selected_hits(Match,
      tennis_point_to_hit_of_hand(Player, Hand), [_,_,_], Hs),
   findall( Hit,
      member(_:Hit, Hs),
      Hits ),
   checklist( tennis_hit_display_xml(Location),
      Hits ),
   concat(['results/', Hand, '_', Player], File),
   dwrite(xml, File, Player:Hits).


/* tennis_match_to_returns(Match, Player, Location) <-
      */

tennis_file_to_returns(Type) :-
   tennis_picture_uncolour,
   tennis_file_to_match(Match),
   tennis_match_to_returns(Match, 'A', Type, bottom),
   tennis_match_to_returns(Match, 'B', Type, top),
   ddk_message(['from right: yellow', '\n', 'from left: red']).

tennis_file_to_returns(File, Type) :-
   tennis_picture_uncolour,
   tennis_file_to_match(File, Match),
   tennis_match_to_returns(Match, 'A', Type, bottom),
   tennis_match_to_returns(Match, 'B', Type, top).

tennis_match_to_returns(Match, Player, Type, Location) :-
   tennis_match_to_selected_hits(Match,
      tennis_point_to_return_taken_as(Player, Type),
      [_,_,_], Hs),
   findall( Hit,
      member(_:Hit, Hs),
      Hits ),
   length(Hits, N),
   ( Type = volley ->
     writeln(user, 'Serve-and-Volley':N)
   ; writeln(user, 'Returns':N) ),
   checklist( tennis_hit_display_xml(Location),
      Hits ),
   concat('results/returns_', Player, File),
   dwrite(xml, File, Player:Hits).


/* tennis_match_to_services <-
      */

tennis_match_to_services :-
   tennis_picture_uncolour,
   tennis_file_to_match(Match),
   tennis_match_to_services(Match, 'A', bottom),
   tennis_match_to_services(Match, 'B', top),
   ddk_message(['from right: yellow', '\n', 'from left: red']).

tennis_match_to_services(Match, Player, Location) :-
   tennis_match_to_selected_hits(Match,
      tennis_point_to_service(Player),
      [_,_,_], Hs),
   findall( Hit,
      member(_:Hit, Hs),
      Hits ),
   checklist( tennis_hit_display_xml(Location),
      Hits ), 
   concat('results/returns_', Player, File),
   dwrite(xml, File, Player:Hits).


/* tennis_hit_display_xml(Location, Hit) <-
      */

tennis_hit_display_xml(Location, Hit) :-
   X := Hit@x, term_to_atom(Xt, X),
   Y := Hit@y, term_to_atom(Yt, Y),
   tennis_point_move(Location, Xt-Yt, Xt2-Yt2),
   Point_Id_Atom := Hit@point_id,
   term_to_atom(Point_Id, Point_Id_Atom),
   ( odd(Point_Id), Hand = forehand
   ; even(Point_Id), Hand = backhand ),
   ( Location = top, Type = ground
   ; Location = bottom, Type = volley ),
   tennis_hit_display([Xt2, Yt2, [Hand, Type], _, _]).

tennis_point_move(bottom, X1-Y1, X2-Y2) :-
   Y1 > 0,
   !,
   X2 is -X1, Y2 is -Y1. 
tennis_point_move(top, X1-Y1, X2-Y2) :-
   Y1 < 0,
   !,
   X2 is -X1, Y2 is -Y1. 
tennis_point_move(_, X-Y, X-Y).


/* tennis_match_to_selected_hits(
         Match, Predicate, [S, G, P], Hits) <-
      */

tennis_match_to_selected_hits(
      Match, Predicate, [S, G, P], Hits) :-
   findall( [S, G, P]:Hit,
      ( Set := Match^set, S := Set@id,
        Game := Set^game, G := Game@id,
        Point := Game^point, P := Point@id,
        apply(Predicate, [Point, Hit]) ),
      Hits ).


/* tennis_match_to_selected_hits_multiset(
         Match, Predicate, Multiset) <-
      */

tennis_match_to_selected_hits_multiset(
      Match, Predicate, Multiset) :-
   dislog_variable_get(tennis_aggregation, game),
   tennis_match_to_set_game_pairs(Match, Pairs),
   ( foreach([S, G], Pairs), foreach([S, G]:N, Xs) do
        ( tennis_match_to_selected_hits(Match,
             Predicate, [S, G, _], Hits),
          length(Hits, N) ) ),
   multiset_normalize(Xs, Multiset).
tennis_match_to_selected_hits_multiset(
      Match, Predicate, Multiset) :-
   dislog_variable_get(tennis_aggregation, set),
   tennis_match_to_sets(Match, Sets),
   ( foreach(S, Sets), foreach([S]:N, Xs) do
        ( tennis_match_to_selected_hits(Match,
             Predicate, [S, _, _], Hits),
          length(Hits, N) ) ),
   multiset_normalize(Xs, Multiset).


/* tennis_match_to_sets(Match, Sets) <-
      */

tennis_match_to_sets(Match, Sets) :-
   findall( S,
      ( Set := Match^set, S := Set@id ),
      Xs ),
   list_to_ord_set(Xs, Sets).


/* tennis_match_to_set_game_pairs(Match, Pairs) <-
      */

tennis_match_to_set_game_pairs(Match, Pairs) :-
   findall( [S, G],
      ( Set := Match^set, S := Set@id,
        Game := Set^game, G := Game@id ),
      Pairs_2 ),
   list_to_ord_set(Pairs_2, Pairs).


/******************************************************************/


