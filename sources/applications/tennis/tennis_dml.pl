

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: DML                                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


exit_programm :-
   dislog_variables_get([
      tennis_dialog - Dialog,
      tennis_picture - Dialog_Picture ]),
   send(Dialog,destroy),
   send(Dialog_Picture,destroy),
   destroy_parameter_dialog.

   
/* tennis_hit_assert(X, Y, Hand, Type) <-
      */

tennis_hit_assert(X, Y, Hand, Type) :-
   tennis_hit_display(X, Y, [Hand, Type]),
   !,
   tennis_hit_assert(X, Y, [Hand, Type]).


/* tennis_hit_assert(Mode, Variable) <-
      */

tennis_hit_assert(service, X) :-
   tennis_picture_uncolour,
   dislog_variables_set([
      hit_id - 0,
      current_server - X,
      tennis_point_fn - [] ]),
   dislog_variable_get(current_player_at_top, Top),
   tennis_events_save_server,
   tennis_display_server(X, Top).


/* tennis_hit_change_assert(X, Y, Hand, Type) <-
      */

tennis_hit_change_assert(X, Y, Hand, Type) :-
   tennis_hit_display(X, Y, [Hand, Type]),
   tennis_hit_change_new_coordinate(X, Y, [Hand, Type]). 


/* tennis_hit_assert(Variable, Variable, List) <-
      */

tennis_hit_assert(X, Y, [Hand, Type]) :-
   dislog_variable_get(format, Format),
   tennis_convert_display_xy_to_absolute_value(
      Format, X, Y, XA, YA),
   tennis_hit_assert_2(XA, YA, [Hand, Type]).   

tennis_hit_assert_2(X, Y, [Hand, Type]) :-
   dislog_variables_get([
      hit_id - Hit_ID_Old,
      tennis_point_fn - FN_Points]),
   Hit_ID is Hit_ID_Old + 1,
   dislog_variable_set(hit_id, Hit_ID),
   tennis_hit_time(Time),
   FN_Point = [
      hit:[id:Hit_ID,
         hand:Hand, type:Type, 
         time:Time, x:X, y:Y]:[] ], 
   New_FN_Points = [FN_Point|FN_Points],
   dislog_variable_set(tennis_point_fn, New_FN_Points).


tennis_determine_hits :-
   dislog_variable_get(tennis_current_output_file, File),
   tennis_determine_hits(File).


/* tennis_determine_hits(File) <-
      */

tennis_determine_hits(File) :-
   tennis_xml_file_to_fn_term(File,FN_Term),
   findall( Lo,
      Lo := FN_Term^match^set^game^point,
      Points ),
   tennis_find_all_service(FN_Term, Server),
   dislog_variable_set(all_server, Server),
   maplist( tennis_find_hits,
      Points, Hits ),
   maplist( tennis_determine_hit_id,
      Points, Hit_ID1s ),
   maplist( maplist(tennis_determine_x_coordinate),
      Hits, Xs ),
   maplist( maplist(tennis_determine_y_coordinate),
      Hits, Ys ),
   maplist( maplist(tennis_determine_hit_time2),
      Hits, Times ),
   maplist( maplist(element_to_list),
      X1s, Xs ),
   maplist( maplist(element_to_list),
      Y1s, Ys ),
   list_remove_elements([[]], Times, Time2s),
   list_remove_elements([[]], X1s, X2s),
   list_remove_elements([[]], Y1s, Y2s),
   list_remove_elements([[]], Hit_ID1s, Hit_IDs),
   tennis_determine_hit_hand_and_types(Hits,Types),
   five_lists_of_lists(
      X2s, Y2s, Types, Hit_IDs, Time2s, X_Y_ID_Time),
   length(X_Y_ID_Time, Number_of_Points),
   dislog_variables_set([
      list_of_points_with_hits_display - X_Y_ID_Time,
      number_of_points_display - Number_of_Points ]).


/* tennis_determine_hit_hand_and_types(List, List) <-
      */

tennis_determine_hit_hand_and_types(Hits, Hand_and_Types) :- 
   maplist( maplist(tennis_determine_hit_hand),
      Hits, Hand1s ),
   maplist( maplist(element_to_list),
      Hands, Hand1s ),
   maplist( maplist(tennis_determine_hit_type),
      Hits, Type1s ),
   maplist( maplist(element_to_list),
      Types, Type1s ),
   pair_lists_of_lists(Hands,Types,Hand_and_Types),
   dislog_variable_set(list_of_hand_and_types_for_display,
      Hand_and_Types).


/* tennis_determine_point_score(FN_Term, Variable, Variable) <-
      */

tennis_determine_point_score(FN_Term, Point_A, Point_B) :-
   ( ( tennis_determine_last_set(FN_Term, Sets),
       tennis_determine_last_game(Sets, Games),
       tennis_last_element_list(Games, Points), 
       Point_A_Atom := Points@point^score_A,
       Point_B_Atom := Points@point^score_B,
       Winner := Points@point^winner,
       tennis_atoms_to_terms([
          Point_A_Atom - Point_A1,
          Point_B_Atom - Point_B1]),
       tennis_score_add_winner(Point_A1, Point_B1, Winner,
          Point_A, Point_B) )
   ; ( Point_A = 0,
       Point_B = 0 ) ).


tennis_determine_game_score(FN_Term, Game_A, Game_B) :-
   tennis_determine_last_set(FN_Term, Set),
   tennis_determine_last_game_score('A', Set, Game_A),
   tennis_determine_last_game_score('B', Set, Game_B).


tennis_determine_current_video(Match, Video) :-
   findall( Video,
      Video := Match^match@set^video_file,
   Videos ),
   last_element(Videos, Video).


tennis_determine_last_set(Match, LastSet) :-
   findall( Set,
      Set := Match^match^set,
      Sets ),
   last_element(Sets, LastSet).

tennis_determine_last_game(Sets, LastGames) :-
   findall( Game,
      Game := Sets^game,
      Games ),
   last_element(Games, LastGames).


tennis_hit_change_to_delete_coordinate(X, Y) :-
   Co = [X, Y],
   dislog_variable_set(change_delete_coordinate, Co),
   tennis_hit_display_delete(X, Y).


/* tennis_hit_change_new_coordinate(Variable, Variable, List) <-
      */

tennis_hit_change_new_coordinate(X, Y, [Hand, Type]) :-
   dislog_variable_get(format, Format),
   tennis_convert_display_xy_to_absolute_value(Format, X, Y, AX, AY),
   dislog_variables_get([
      change_Hit - Points,
      change_delete_coordinate - [DX, DY]]),
   tennis_convert_display_xy_to_absolute_value(
      Format, DX, DY, DAX, DAY),
   dislog_variable_set(change_delete_coordinate, [DAX, DAY]),
   maplist( tennis_determine_change_XY(DAX, DAY),
      Points, Co_Hits ),
   list_remove_elements([[]], Co_Hits, Co_Hit),
   n_th_element(Co_Hit, 1, Change_Hit),
   tennis_events_save_change_hit(Change_Hit, AX, AY, Hand, Type),
   tennis_change_hit_reload.

tennis_change_hit_reload :-
   tennis_picture_uncolour,
   tennis_determine_hits, 
   dislog_variable_get(current_point, N),
   tennis_point_show(N).

   
/* tennis_determine_change_XY(Variable, Variable, List, List) <-
      */

tennis_determine_change_XY(DX, DY, [XA, YA, _, ID, _], Hits) :-   
   tennis_terms_to_atoms([
      X - XA, 
      Y - YA]),
%  Varianz
   X2 is DX - 1,
   X3 is DX + 1,
   Y2 is DY - 1,
   Y3 is DY + 1,
   ( ( X >= X2, X =< X3, Y >= Y2, Y =< Y3, 
       Hits = [XA, YA, ID] )
   ; Hits = [] ).
  

tennis_unforced_error_a :-
   dislog_variable_set(unforced_error, 'A'),
   tennis_point_win('B').

tennis_unforced_error_b :-
   dislog_variable_set(unforced_error, 'B'),
   tennis_point_win('A').


/* tennis_point_win(Player) <-
      */

tennis_point_win('A') :-
   dislog_variable_set(point_winner, 'A'),
   dislog_variable_get(point_score('A'), ScoreA_Old),
   dislog_variable_get(point_score('B'), Score_B),
   CurrentScore is ScoreA_Old + 1,
   tennis_is_new_game(CurrentScore, Score_B, YorN),
   ( ( YorN == 'Yes',
       tennis_game_win('A') )
   ; ( YorN == 'No', 
       tennis_events_save_point,
       tennis_picture_uncolour,
       dislog_variable_set(point_score('A'), CurrentScore),
       tennis_display_score_game,
       tennis_determine_and_display_times ) ).
tennis_point_win('B') :-
   dislog_variable_set(point_winner, 'B'),
   dislog_variable_get(point_score('A'), Score_A),
   dislog_variable_get(point_score('B'), ScoreB_Old),
   CurrentScore is ScoreB_Old + 1,
   tennis_is_new_game(Score_A, CurrentScore, YorN),
   ( ( YorN == 'Yes',
       tennis_game_win('B') )
   ; ( YorN == 'No', 
       tennis_events_save_point,
       tennis_picture_uncolour,
       dislog_variable_set(point_score('B'), CurrentScore),
       tennis_display_score_game,
       tennis_determine_and_display_times ) ).


/* tennis_game_win(Player) <-
      */

tennis_game_win(Player) :-
   dislog_variable_set(game_winner, Player),
   dislog_variable_get(game_score(Player), Score),
   Current_Score is Score + 1,
   dislog_variable_set(game_score(Player), Current_Score),
   dislog_variable_get(game_score('A'), Game_A),
   dislog_variable_get(game_score('B'), Game_B),
   tennis_is_new_set(Game_A, Game_B, YorN),
   ( ( YorN == 'Yes',
       tennis_set_win_both )
   ; ( tennis_events_save_game,
       tennis_score_point_zero,
       tennis_display_score_game,
       tennis_display_score_set,
       tennis_picture_uncolour,
       tennis_determine_and_display_times ) ).


/* tennis_set_win(Player) <-
      */

tennis_set_win(Player) :-
   dislog_variable_get(game_score(Player), Score),
   Current_Score is Score + 1,
   dislog_variable_set(game_score(Player), Current_Score),
   tennis_set_win_both.

tennis_set_win_both :-
   tennis_events_save_set,
   tennis_score_game_zero,
   tennis_display_score_game,
   tennis_display_score_set,
   tennis_determine_score_match,
   tennis_display_score_match,
   tennis_determine_and_display_times,
   tennis_picture_uncolour.


/* tennis_match_win(Player) <-
      */

tennis_match_win(Player) :-
   tennis_set_win(Player).

 
tennis_score_point_zero :-
   dislog_variable_set(point_score('A'), 0),
   dislog_variable_set(point_score('B'), 0).

tennis_score_game_zero :-
   dislog_variable_set(game_score('A'), 0),
   dislog_variable_set(game_score('B'), 0),
   tennis_score_point_zero.


tennis_scroll_determine_score :-
   dislog_variable_get(tennis_current_output_file, File),
   tennis_scroll_determine_score(File).

tennis_scroll_determine_score(File) :-
   tennis_xml_file_to_fn_term(File, FN_Term),
   tennis_find_all_service(FN_Term, Server),
   dislog_variable_set(all_server, Server),
   tennis_search_tops(FN_Term, Tops),
   tennis_scroll_score_set(FN_Term, Score_Sets),
   tennis_winner_of_rallys(FN_Term),
   append_set_2(Score_Sets, ScoreSets),
   findall( Set,
      Set := FN_Term^match^set, 
      Sets ),
   maplist( tennis_scroll_score_game,
      Sets, Score_Games ),
   maplist( tennis_search_games,
      Sets, Games ),
   maplist( maplist(tennis_scroll_score_point),
      Games, Score_Points ),
   append(Score_Points, ScorePoints),
   tennis_scroll_more_than_one_set(ScoreSets, Score_Games,
      ScorePoints, Tops, Result),
   dislog_variable_set(tennis_score_for_and_backward, Result).


tennis_scroll_determine_time :-
   dislog_variable_get(tennis_current_output_file, File),
   tennis_scroll_determine_time(File),
   tennis_scroll_determine_settime(File).

tennis_scroll_determine_time(File) :-
   tennis_xml_file_to_fn_term(File, FN_Term),
   tennis_find_all_points(FN_Term, Points),
   maplist( tennis_determine_starttime_of_point,
      Points, Times ),
   dislog_variable_set(tennis_starttime_point, Times).

tennis_determine_starttime_of_point([Point|_], Times) :-
   Points = [Point],
   Times := Points@hit^time.

tennis_scroll_determine_settime(File) :-
   tennis_xml_file_to_fn_term(File, FN_Term),
   tennis_find_all_sets(FN_Term, Sets),
   maplist( tennis_number_of_points_in_set,
      Sets, Number ),   
   kum_list(Number, Number2), 
   maplist( tennis_determine_set_time,
      Sets, Times ),
   tennis_determine_number_settime(Number2, Times).

tennis_number_of_points_in_set(Set, Number) :-
   findall( Point,
      Point := Set^game^point,
      Points ),
   length(Points, Number). 

tennis_determine_number_settime(Number, Times) :-
   list_remove_last_element(Times, Times2),
   Times3 = [[0, 0, 0]|Times2],
   pair_lists(Number, Times3, Number_Times),
   dislog_variable_set(tennis_number_time_rally, Number_Times).


tennis_winner_of_rallys(Match) :-
   findall(Winner,
      Winner := Match^match^set^game@point^winner,
      Winner_Ids ),
   maplist( tennis_player_id_to_name,
      Winner_Ids, Winner_Names ),
   dislog_variable_set(tennis_winners_of_rallys, Winner_Names).

tennis_search_tops(Match, Tops) :-
   findall( Top,
      Top := Match^match^set^game@point^top,
      Top_Elements ),
   maplist( element_to_list,
      Top_Elements, Tops ). 
tennis_search_tops(_, _).


/* tennis_scroll_score_set(Match, Score_Sets) <-
      */

tennis_scroll_score_set(Match, Score_Sets) :-
   ( findall( Set_A,
        Set_A := Match^match^result@score^player_A,
        Sets_A ),
     findall( Set_B,
        Set_B := Match^match^result@score^player_B,
        Sets_B ),
     pair_lists(Sets_A, Sets_B, Score_Sets) )
   ; Score_Sets = [[]].


/* tennis_scroll_score_game(Sets, Score_Games) <-
      */

tennis_scroll_score_game(Sets, Score_Games) :-
   findall( Game_A,
      Game_A := Sets@game^score_A, 
      Games_A ),
   findall( Game_B,
      Game_B := Sets@game^score_B, 
      Games_B ),
   pair_lists(Games_A, Games_B, Score_Games).


/* tennis_scroll_score_point(Games, Score_Points) <-
      */

tennis_scroll_score_point(Games, Score_Points) :-
   findall( Point_A, 
      Point_A := Games@point^score_A, 
      Points_A ),
   findall( Point_B, 
      Point_B := Games@point^score_B, 
      Points_B ),
   pair_lists(Points_A, Points_B, Score_Points).


/* tennis_scroll_more_than_one_set(
         Sets, Games, Points, Tops, Results) <-
      */

tennis_scroll_more_than_one_set(
      Sets, Games, Points, Tops, Results) :-
   tennis_pair_lists(Sets, Games, Set_Games),
   tennis_scroll_more_than_one_set2(
      Set_Games, Points, Tops, Results).

tennis_scroll_more_than_one_set(
      Sets,Games,Points,Tops,Results) :-
   length(Sets, LS), N is LS - 1,
   first_n_elements(N, Sets, SetN),
   tennis_pair_lists(SetN, Games, Set_Games),  
   tennis_scroll_more_than_one_set2(
      Set_Games, Points, Tops, Results).

tennis_scroll_more_than_one_set2(Sets,Points,Tops,Results) :-
   append(Sets, SetGameScores),
   tennis_pair_lists2(SetGameScores, Points,Fin),
   maplist( maplist(append),
      Fin, Fin2 ),
   append(Fin2,SSGPL),
   tennis_pair_lists_8(SSGPL,Tops,Results).
tennis_scroll_more_than_one_set2(_,_,_,_) :-
   writeln('No !').


tennis_is_new_game(Point_A, Point_B, 'Yes') :-
   ( Point_A > 3
   ; Point_B > 3),
   abs(Point_A - Point_B, Result),
   Result >= 2.
tennis_is_new_game(_, _, 'No').


tennis_is_new_set(7, 6, 'Yes').
tennis_is_new_set(6, 7, 'Yes').
tennis_is_new_set(7, 5, 'Yes').
tennis_is_new_set(5, 7, 'Yes').
tennis_is_new_set(6, Game_B, 'Yes') :-
   Game_B < 5.
tennis_is_new_set(Game_A, 6, 'Yes') :-
   Game_A < 5.
tennis_is_new_set(_, _, 'No').

tennis_score_add_winner(A1, B, 'A', A, B) :-
   A is A1 + 1.
tennis_score_add_winner(A, B1, 'B', A, B) :-
   B is B1 + 1.


tennis_determine_sum_of_times([H, M, S], Time) :-
   add(H, Hours),
   add(M, Minutes),
   add(S, Seconds),
   tennis_numbers_to_time([Hours, Minutes, Seconds], Time).

tennis_determine_and_display_times :-
   dislog_variable_get(tennis_current_output_file, File),
   tennis_xml_file_to_fn_term(File, FN_Term),   
   tennis_determine_match_time(FN_Term, Match_Time),
   tennis_determine_current_set_time(FN_Term, Set_Time),
   tennis_display_times(Set_Time, Match_Time).


tennis_check(S, G, R, Result) :-
   dislog_variable_get(tennis_current_output_file, File),
   tennis_xml_file_to_fn_term(File, FN_Term),
   tennis_check(FN_Term, S, G, R, Result).

tennis_check(FN_Term, Number_of_Sets, Number_of_Games,
      Number_of_Rallys, Result) :-
   tennis_number_of_sets(FN_Term, Number_of_Sets),
   tennis_number_of_games(FN_Term, Number_of_Games),
   tennis_number_of_rallys(FN_Term, Number_of_Rallys),
   tennis_check_values(FN_Term, Result).

tennis_check_values(FN_Term, 'Values are ok') :-
   tennis_find_all_values(x, FN_Term, Xs),
   tennis_find_all_values(y, FN_Term, Ys),
   tennis_are_x_values_ok(Xs),
   tennis_are_y_values_ok(Ys).
tennis_check_values(_, 'Error').

tennis_number_of_sets(FN_Term, Number_of_Sets) :-
   tennis_find_all_set_ids(FN_Term, Sets),
   length(Sets, Number_of_Sets).

tennis_number_of_games(FN_Term, Number_of_Games) :-
   tennis_find_all_game_ids(FN_Term, Games),
   length(Games, Number_of_Games).

tennis_number_of_rallys(FN_Term, Number_of_Rallys) :-
   tennis_find_all_rally_ids(FN_Term, Rallys),
   length(Rallys, Number_of_Rallys).

tennis_are_x_values_ok(List) :-
   checklist( tennis_is_x_value_ok,
      List ).
tennis_is_x_value_ok(Value) :-
   Value < 18,
   Value > -18.
tennis_is_x_value_ok(_).

tennis_are_y_values_ok(List) :-
   checklist( tennis_is_y_value_ok,
      List ).
tennis_is_y_value_ok(Value) :-
   Value < 9,
   Value > -9.
tennis_is_y_value_ok(_).


tennis_create_info_file :-
   tennis_info_file(InfoFile),
   tell(InfoFile),
   tennis_html_file_start,
   dwrite(xml,
      h2:[font:[color:'#8698fd']:[
         'Files Containing Tennis Matches']]),
   writeln('<ol>'),
   tennis_data_path(Path),
   swi_directory_contains_files(Path, Files),
   maplist( tennis_is_xml_file, 
      Files, File2s ),
   list_remove_elements([[]], File2s, File3s),
   checklist( tennis_files_to_info_file(Path),
      File3s ),
   writeln('</ol>'),
   tennis_html_file_end,
   told,
%  html_file_to_display(InfoFile).
   show_html_file('Tennis Tool', InfoFile).


tennis_is_xml_file(File, File) :-
   name(File, File_Name),
   list_split_at_position(["."], File_Name, Parts),
   last_element(Parts, Type),
   name(TypeN, Type),
   TypeN == 'xml'.   
tennis_is_xml_file(_, []).     

tennis_files_to_info_file(Path, File) :-
   concat(Path, File, FilePath),
   tennis_get_info_from_fn_term(FilePath).

tennis_get_info_from_fn_term(File) :-
   tennis_xml_file_to_fn_term(File, FN_Term),
   tennis_determine_player_names(FN_Term, NameA, NameB),
   Match_Facts := FN_Term^match^match_facts,
   [TournamentL, SurfaceL, WhereL, WhenL, RoundL] :=
      Match_Facts^[tournament, surface, where, when, round],
   tennis_determine_match_result(FN_Term, Results),
   maplist( term_to_atom,
      Results, Result5s ),
   maplist( tennis_concat(' '),
      Result5s, Results2 ),
   concat(Results2, Results22),
   tennis_lists_to_elements([
      TournamentL - Tournament,
      SurfaceL - Surface,
      WhereL - Where,
      WhenL - When,
      RoundL - Round]),
   tennis_write_info_to_file(File, NameA, NameB, Results22, 
      Tournament, Surface, Where, When, Round). 
tennis_get_info_from_fn_term(_).
 

/******************************************************************/


