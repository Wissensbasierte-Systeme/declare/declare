

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Values                               ***/
/***                                                            ***/
/******************************************************************/


tennis_determine_player_names(Match) :-
   findall( Player, 
      Player := Match^match@player,
      Players ),
   checklist( tennis_set_player_name,
      Players ).


tennis_determine_player_names2(Match) :-
   findall( Player, 
      Player := Match^match@player,
      Players ),
   checklist( tennis_set_player_name2,
      Players ).

   
tennis_determine_player_names(Match, Name_A, Name_B) :-
   findall( Name,
      Name := Match^match@player^name,
      Names ),
   first(Names, Name_A),
   last_element(Names, Name_B).


tennis_set_player_name(Player) :-
   Player_Id := Player^id,
   Player_Name := Player^name,
   concat(name_player_, Player_Id, Name_player_X),
   dislog_variable_set(Name_player_X, Player_Name).

tennis_set_player_name2(Player) :-
   Player_Id := Player^id,
   Player_Name := Player^name,
   concat(search_pattern_name_player_, Player_Id, Name_player_X),
   dislog_variable_set(Name_player_X, Player_Name).


tennis_determine_match_time(FN_Term, Time_Display) :-
   tennis_determine_all_sets(FN_Term, Sets),
   maplist( tennis_determine_set_time,
      Sets, Times ),
   maplist( tennis_n_th_element(1),
      Times, Hours ),
   maplist( tennis_n_th_element(2),
      Times, Minutes ),
   maplist( tennis_n_th_element(3),
      Times, Seconds ),
   tennis_determine_sum_of_times([Hours,Minutes,Seconds], Time),
   maplist( tennis_number_to_two_digits,
      Time, Time2 ),
   tennis_list_to_time_format(Time2, Time_Display).
tennis_determine_match_time(_,  '00:00:00').

tennis_determine_current_set_time(FN_Term, Time_Display) :-
   tennis_determine_all_sets(FN_Term, Sets),
   last_element(Sets, Set),
   tennis_determine_set_time(Set, Time),
   maplist( tennis_number_to_two_digits,
      Time, Time2 ),
   tennis_list_to_time_format(Time2, Time_Display).
tennis_determine_current_set_time(_, '00:00:00').


tennis_determine_set_time(FN_Term, [H, M, S]) :-
   tennis_find_all_hits_in_set(FN_Term, Hits),
   last_element(Hits, LastHit),
   Time := LastHit^time,
   tennis_colon_structure_to_time(Time, [H, M, S]).
tennis_determine_set_time(_, [0,0,0]).


tennis_determine_all_sets(Match, Sets) :-
   findall( Set,
      Set := Match^match^set,
      Sets ).


tennis_determine_last_game_score(Player, Games, Score) :-
   concat(score_, Player, Score_X),
   findall( S,
      S := Games@game^Score_X,
      Scores ),
    maplist( tennis_atom_to_term,
       Scores, Scores_2 ),
    maximum(Scores_2, Score).


tennis_find_all_sets(Match, Sets) :-
   findall( Set,
      Set := Match^match^set,
      Sets ).

tennis_determine_all_games(Sets, Games) :-
   findall( Game,
      Game := Sets^game,
      Games ).

tennis_find_all_games(Match, Games) :-
   findall( Game,
      Game := Match^match^set^game,
      Games ).

tennis_find_all_points(Match, Points) :-
   findall( Point,
      Point := Match^match^set^game^point,
      Points ).

tennis_determine_all_points(Games, Points) :-
   findall( Point,
      Point := Games^game,
      Points ).


tennis_determine_hit_hand(Hit, Hands) :-
   findall( Hand, 
      Hand := Hit^hand,
      Hands ).

tennis_determine_hit_type(Hit, Types) :-
   findall( Type,
      Type := Hit^type,
      Types ).

tennis_find_hits(FN_Term, Hits):-
   findall( Hit,
      Hit := FN_Term@hit,
      Hits ).

tennis_determine_x_coordinate(Match, Xs) :-
   findall( X,
      X := Match^x,
      Xs ).

tennis_find_all_set_ids(Match, IDs):-
   findall( ID,
      ID := Match^match@set^id,
      IDs ).

tennis_find_all_game_ids(Match, IDs):-
   findall( ID,
      ID := Match^match^set@game^id,
      IDs ).

tennis_find_all_rally_ids(Match, IDs):-
   findall( ID,
      ID := Match^match^set^game@point^id,
      IDs ).


tennis_find_all_values(Coordinate, Match, Values) :-
   findall( Value,
      Value := Match^match^set^game^point@hit^Coordinate,
      Values_2 ),
   maplist( tennis_atom_to_term,
      Values_2, Values ).


tennis_find_all_service(Match, Services) :-
   findall( Service,
      Service := Match^match^set^game@point^service,
      Services ).


tennis_determine_hit_times(Hit, Times) :-
   findall( Time,
      Time := Hit^time,
      Times ).


tennis_determine_y_coordinate(Hit, Ys) :-
   findall( Y,
      Y := Hit^y,
      Ys ).


tennis_determine_point(Match, Point_IDs) :-
   findall( ID, 
      ID := Match^match^set^game^point,
      Point_IDs ).


tennis_determine_hit_id(Hits, Hit_IDs) :-
   findall( Id,
      Id := Hits@hit^id,
      Hit_IDs ).


tennis_determine_game_winner_of_points(Points, Winner) :-
   last_element(Points, LastPoint),
   element_to_list(LastPoint, LastPoints),
   Winner := LastPoints@point^winner.


tennis_determine_point_ids(Points, Point_IDs) :-
   findall( ID,
      ID := Points@point^id,
      Point_IDs ).


tennis_determine_last_game_id(FN_Term, ID) :-
   tennis_determine_game_ids(FN_Term, IDs_Atoms),
   maplist( tennis_atom_to_term,
      IDs_Atoms, IDs ),
   maximum(IDs, ID).


tennis_determine_game_ids(Games, Game_IDs) :-
   findall( ID, 
      ID := Games@game^id, 
      Game_IDs ).


tennis_determine_set_ids(Match, Set_IDs) :-
   findall( ID, 
      ID := Match^match@set^id, 
      Set_IDs ).


tennis_determine_last_set_id(FN_Term, Set_ID) :-
   tennis_determine_set_ids(FN_Term, Set_IDs),
   maplist( tennis_atom_to_term,
      Set_IDs, Set_IDs_Term ),
   maximum(Set_IDs_Term, Set_ID).


tennis_determine_hit_time(Hits, Time) :-
   Time := Hits@hit^time.

tennis_determine_hit_time2(Hit, Time) :-
   Time := Hit^time.


tennis_determine_next_point_id(FN_Term, ID) :-
   ( tennis_determine_point_ids(FN_Term, IDs),
     last_element(IDs, IDA),
     term_to_atom(ID_Term, IDA),
     ID is ID_Term +1 )
   ; ID is 1. 


tennis_determine_game_id(FN_Term, ID) :-
   ( tennis_determine_game_ids(FN_Term, IDs),
     last_element(IDs, IDA),
     term_to_atom(ID, IDA) )
   ; ID is 1.


tennis_determine_set_id(FN_Term, ID) :-
   ( tennis_determine_set_ids(FN_Term, IDs),
     last_element(IDs, IDA),
     term_to_atom(ID, IDA) )
   ; ID is 1.


/* tennis_determine_match_result(Match, Result) <-
      */

tennis_determine_match_result(Match, Result) :-
   tennis_determine_match_score('A', Match, Scores_A),
   tennis_determine_match_score('B', Match, Scores_B),
   pair_lists(Scores_A, Scores_B, Scores_AB),
   maplist( list_to_colon_structure,
      Scores_AB, Result ).

tennis_determine_match_score(Player, Match, Score) :-
   concat(player_, Player, Player_X),
   findall( Set,
      Set := Match^match^result@score^Player_X, 
      Score_2 ),
   maplist( tennis_atom_to_term,
      Score_2, Score ).


tennis_determine_rallys_each_set(Set, Rallys) :-
   findall( Rally,
      Rally := Set^game^point,
      Rallys ).
tennis_determine_rallys_each_set(_,_).


tennis_number_to_two_digits(0, '00').
tennis_number_to_two_digits(1, '01').
tennis_number_to_two_digits(2, '02').
tennis_number_to_two_digits(3, '03').
tennis_number_to_two_digits(4, '04').
tennis_number_to_two_digits(5, '05').
tennis_number_to_two_digits(6, '06').
tennis_number_to_two_digits(7, '07').
tennis_number_to_two_digits(8, '08').
tennis_number_to_two_digits(9, '09').
tennis_number_to_two_digits(X, X).


tennis_match_to_attributes(Match, Attributes) :-
   findall( As,
      As := Match^match^set^game@point,
      Attributes ).


tennis_games_to_attributes(Games, Attributes) :-   
   findall( As,
      As := Games^game@point,
      Attributes ).


tennis_find_all_game_attributes(Match, Attributes) :-
   findall( As,
      As := Match^match^set@game,
      Attributes ).


tennis_find_all_hits_in_set(Games, Hits) :-
   findall( Hit,
      Hit := Games^game^point@hit,
      Hits ).


tennis_player_id_to_name(Player_Id, Player_Name) :-
   concat(name_player_, Player_Id, Name_player_X), 
   dislog_variable_get(Name_player_X, Player_Name),
   !.
tennis_player_id_to_name(Player_Id, Player_Id).


/****************************************************************/


