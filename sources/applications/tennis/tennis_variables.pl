

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Variables                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tennis_point_set_input_video_file(N) <-
      */

tennis_point_set_input_video_file(N) :-
   tennis_point_to_video_file_name(N, Name),
   dislog_variable_get(input_video_file, File),
   send(File, value, Name),
   dislog_variable_set(current_video_file, Name).

tennis_point_to_video_file_name(N, Name) :-
   dislog_variable_get(tennis_set_to_video_file, Pairs),
   member([I, J]:Name, Pairs),
   between(I, J, N).


/* tennis_set_to_video_file_initialize(FN_Term) <-
      */

tennis_set_to_video_file_initialize(FN_Term) :-
   FN_Term = [Match],
   ddbase_aggregate( [Set, length(Point), Video_File_Name],
      ( Point := Match/set::[
           @id=Id, @video_file=Video_File_Name]/game/point,
        atom_to_number(Id, Set) ),
      Triples_1 ),
   sort(Triples_1, Triples_2),
   tennis_set_to_video_file_triples_to_pairs(1, Triples_2, Pairs),
   dislog_variable_set(tennis_set_to_video_file, Pairs).

tennis_set_to_video_file_triples_to_pairs(N, [X|Xs], [Y|Ys]) :-
   X = [_, Points, File],
   M is N + Points - 1,
   Y = [N, M]:File,
   K is M + 1,
   tennis_set_to_video_file_triples_to_pairs(K, Xs, Ys).
tennis_set_to_video_file_triples_to_pairs(_, [], []).


tennis_set_preferences(FN) :-
   FN_Term := FN^preferences^active,
   tennis_set_preferences_size(FN_Term),
   tennis_set_preferences_colour(FN_Term),
   tennis_set_preferences_video(FN_Term).


tennis_set_preferences_video(FN_Term) :-
   tennis_get_preferences_video(FN_Term, Width, Height),
   dislog_variables_set([
      tennis_video_width - Width,
      tennis_video_height - Height]).

tennis_get_preferences_video(FN_Term, Width, Height) :-
   W := FN_Term@video^width,
   H := FN_Term@video^height,
   tennis_atoms_to_terms([
      W - Width,
      H - Height]).


tennis_set_preferences_size(FN_Term) :-
   tennis_get_preferences_size(FN_Term, Factor, Behind, Side, Format),
   dislog_variables_set([
      factor - Factor,
      tennis_court_side - Side,
      tennis_court_behind - Behind,
      format - Format]).


tennis_get_preferences_size(FN_Term, Factor, Behind, Side, Format) :-
   F := FN_Term@size^factor,
   B := FN_Term@size^behind,
   S := FN_Term@size^side,
   Fo := FN_Term@size^format,
   tennis_atoms_to_terms([
      F - Factor,
      B - Behind,
      S - Side,
      Fo - Format]).


tennis_set_preferences_colour(FN_Term) :-
   tennis_get_preferences_colour(FN_Term, Red, Green, Blue),
   rgb_to_colour([Red,Green,Blue], Colour),
   dislog_variable_set(current_court_colour, Colour).

tennis_get_preferences_colour(FN_Term, Red, Green, Blue) :-
   R := FN_Term@colour^red,
   G := FN_Term@colour^green,
   B := FN_Term@colour^blue,
   tennis_atoms_to_terms([
      R - Red,
      G - Green,
      B - Blue]).


tennis_variable_start :-
   tennis_output_file(File),
   dislog_variable_set(tennis_current_output_file, File),
   dislog_variable_set(tennis_to_search_files, [File]),
   tennis_variable_start(File).

tennis_variable_start(File) :-  
   exists_file(File),
   tennis_xml_file_to_fn_term(File, FN_Term),
   tennis_determine_player_names(FN_Term),
   tennis_determine_point_score(FN_Term, Point_A, Point_B),
   tennis_determine_game_score(FN_Term, Game_A, Game_B),
   tennis_determine_current_video(FN_Term, Videofile),
   tennis_determine_match_time(FN_Term, Match_Time),
   tennis_determine_current_set_time(FN_Term, Set_Time),
   dislog_variables_set([
      current_player_at_top - 'A',
      point_score('A') - Point_A,
      point_score('B') - Point_B,
      game_score('A') - Game_A,
      game_score('B') - Game_B,
      current_video_file - Videofile,
      list_of_points_with_hits_display - [],
      tennis_point_fn - [],
      hit_id - 0,
      number_of_points_display - 1,
      current_point - 1,
      show_rally_or_hit - 1, 
      unforced_error - 0,
      length_last_hit - 0,
      tennis_modus - 'Input']),
   tennis_display_game_set_match(FN_Term),
   tennis_display_player_names,
   tennis_display_times(Set_Time, Match_Time),
   tennis_display_output_file_name,
   tennis_display_video_file_name,
   tennis_set_to_video_file_initialize(FN_Term).
tennis_variable_start(_).


tennis_display_game_set_match(FN_Term) :-
   findall(Score,
      Score := FN_Term^match^result@score^set,
      Scores),
   length(Scores, LES),
   tennis_find_all_sets(FN_Term, Sets),
   length(Sets, SS),
   (   (LES \= SS,
       tennis_display_score_game,
       tennis_display_score_set,
       tennis_display_score_match)
   ;   (LES == SS,
       dislog_variables_get([
          input_game_player_A - Ref_A,
          input_game_player_B - Ref_B]),
       tennis_display_score_game3(Ref_A, Ref_B, ' ', ' ', 'A'),
       tennis_display_score_set_2(' ', ' ', 'A'),
       tennis_display_score_match)). 
  
tennis_determine_score_match :-
   dislog_variable_get(tennis_current_output_file, File),
   tennis_xml_file_to_fn_term(File, FN_Term),
   tennis_determine_score_match(FN_Term).   

tennis_determine_score_match(FN_Term) :-
   tennis_determine_match_score('A', FN_Term, Score_As),
   tennis_determine_match_score('B', FN_Term, Score_Bs),
   score_match_to_display_format(Score_As, Display_A),
   score_match_to_display_format(Score_Bs, Display_B),
   dislog_variables_set([
      match_score_A - Display_A,
      match_score_B - Display_B ]).


score_match_to_display_format(Score, Score3) :-
   maplist(tennis_add_whitespace,
      Score, Score2),
   concat(Score2, Score3).  


tennis_add_whitespace(Element1, Element2) :-
   Space = ' ',
   concat(Element1, Space, Element2).
tennis_add_whitespace([], []).


/****************************************************************/

 
