

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Statistics                           ***/
/***                                                            ***/
/******************************************************************/


tennis_file_to_match_fn_list(Match) :-
   dislog_variable_get(tennis_current_output_file, Path),
   tennis_xml_file_to_fn_term(Path, Match).


/*** interface ****************************************************/


/* tennis_determine_att_rallys(Match, Rallys) <-
      */

tennis_determine_att_rallys(Rallys) :-
   tennis_file_to_match_fn_list(Match),
   tennis_determine_att_rallys(Match, Rallys).

tennis_determine_att_rallys(Match, Rallys) :-
   tennis_determine_rallys(Match, Rallys_2),
   tennis_match_to_attributes(Match, Point_Atts),
   pair_lists(Point_Atts, Rallys_2, Rallys).


/* tennis_determine_att_rallys_each_set(Match, Rallys) <-
      */

tennis_determine_att_rallys_each_set(Rallys) :-
   tennis_file_to_match_fn_list(Match),
   tennis_determine_att_rallys_each_set(Match, Rallys).

tennis_determine_att_rallys_each_set(Match, Rallys_2) :-
   tennis_determine_all_sets(Match, Sets),
   maplist( tennis_determine_rallys_each_set,
      Sets, Rallys ),
   maplist( tennis_games_to_attributes,
      Sets, Point_Atts ),
   pair_lists_of_lists(Point_Atts, Rallys, Rallys_2).
tennis_determine_att_rallys_each_set(_, 'Error 0').


/* tennis_determine_statistics_start <-
      */

tennis_determine_statistics_start :-
   tennis_file_to_match_fn_list(Match),
   tennis_statistics_set_variables,
   tennis_statistics_set_names(Match).

   
/* tennis_determine_service(Match) <-
      */

tennis_determine_service :-
   tennis_file_to_match_fn_list(Match),
   tennis_determine_service(Match).

tennis_determine_service(Match) :-
   tennis_statistics_set_variables,
   tennis_statistics_set_names(Match),
   tennis_determine_number_of_services(Match),
   tennis_determine_att_rallys_each_set(Match, Rallys),
   maplist( tennis_select_rallys_of_length(2),
      Rallys, Rallys_2 ),
   tennis_determine_services_each_set(Rallys_2).
tennis_determine_service(_).
   

/* tennis_determine_services_each_set(Rallys) <-
      */

tennis_determine_services_each_set(Rallys) :-
   dislog_variable_set(service_set_a, []),
   dislog_variable_set(service_set_b, []),
   checklist( service_each_set,
      Rallys ),
   dislog_variable_get(service_set_a, Services_A_r),
   reverse(Services_A_r, Services_A),
   dislog_variable_set(service_set_a, Services_A),
   dislog_variable_get(service_set_b, Services_B_r),
   reverse(Services_B_r, Services_B), 
   dislog_variable_set(service_set_b, Services_B).

service_each_set(Rallys) :-
   checklist( tennis_rally_to_service_point_or_fault,
      Rallys ),
   dislog_variables_get([
      tennis_service_point_a - PointA,
      tennis_service_fault_a - FaultA,
      service_set_a - SAs]),
   dislog_variables_set([
      service_set_a - [[PointA, FaultA]|SAs],
      tennis_service_point_a - 0,
      tennis_service_fault_a - 0]),
   dislog_variables_get([
      tennis_service_point_b - PointB,
      tennis_service_fault_b - FaultB,
      service_set_b - SBs]),
   dislog_variables_set([
      service_set_b - [[PointB, FaultB]|SBs],
      tennis_service_point_b - 0,
      tennis_service_fault_b - 0]).
service_each_set(_).

tennis_rally_to_service_point_or_fault([Att, _]) :-
   Server := Att^service,
   Winner := Att^winner,
   ( Server = Winner ->
     X = point
   ; X = fault ),
   char_type(Server, upper(P)),
   concat_atom([tennis_service, X, P], '_', V),
   dislog_variable_increment(V).
tennis_rally_to_service_point_or_fault(_).


/* tennis_determine_service_point_location(Match) <-
      */

tennis_determine_service_point_location :-
   tennis_file_to_match_fn_list(Match),
   tennis_determine_service_point_location(Match).    

tennis_determine_service_point_location(Match) :-
   dislog_variables_set([
      tennis_service_location_a - [],
      tennis_service_location_b - []]),
   tennis_determine_att_rallys(Match, Rallys),
   tennis_select_rallys_of_length(2, Rallys, Rallys_2),
   checklist( tennis_is_service_point_location,
      Rallys_2 ),
   tennis_determine_service_point_location_display.


/* tennis_determine_direct_point_service(Match) <-
      */

tennis_determine_direct_point_service :-
   tennis_file_to_match_fn_list(Match),
   dislog_variables_set([
      service_direct_set_a - [],
      service_direct_set_b - []]),
   tennis_determine_direct_point_service(Match).  

tennis_determine_direct_point_service(Match) :-
   tennis_determine_att_rallys_each_set(Match, Rallys),
   maplist( tennis_select_rallys_of_length(3),
      Rallys, Rallys_2 ),
   checklist( direct_points_each_set,
      Rallys_2 ),
   dislog_variable_update_service_direct_set('A'),
   dislog_variable_update_service_direct_set('B').

dislog_variable_update_service_direct_set(Player) :-
   char_type(Player, upper(P)),
   concat(service_direct_set_, P, V),
   dislog_variable_get(V, Old),
   length(Old, N),
   fill_list(N, List),
   pair_lists(List, Old, Pairs),
   reverse(Pairs, New), 
   dislog_variable_set(V, New).


/* direct_points_each_set(Rallys) <-
      */

direct_points_each_set(Rallys) :-
   checklist( tennis_rally_to_direct_point,
      Rallys ),
   dislog_variables_get([
      tennis_direct_point_service_a - A,
      service_direct_set_a - As,
      tennis_direct_point_service_b - B,
      service_direct_set_b - Bs]),
   dislog_variables_set([
      tennis_direct_point_service_a - 0,
      service_direct_set_a - [A|As],  
      tennis_direct_point_service_b - 0,
      service_direct_set_b - [B|Bs]]).

tennis_rally_to_direct_point([Att, _]) :-
   Player := Att^service,
   Player := Att^winner,
   char_type(Player, upper(P)),
   concat(tennis_direct_point_service_, P, V),
   dislog_variable_increment(V).
tennis_rally_to_direct_point(_).   


/* tennis_determine_ground_and_volley(Match) <-
      */

tennis_determine_ground_and_volley :-
   tennis_file_to_match_fn_list(Match),
   tennis_determine_ground_and_volley(Match). 

tennis_determine_ground_and_volley(Match) :-
   tennis_statistics_set_variables,
   tennis_statistics_set_names(Match),
   tennis_determine_att_rallys(Match, Rallys),  
   tennis_rallys_to_hit_statistic(Rallys),
   tennis_rallys_to_hit_types(Rallys).


/* tennis_determine_break(Match) <-
      */

tennis_determine_break :-
   tennis_file_to_match_fn_list(Match),
   tennis_determine_break(Match). 

tennis_determine_break(Match) :-
   tennis_statistics_set_variables,
   tennis_statistics_set_variables,
   tennis_statistics_set_names(Match),
   tennis_break(Match),
   tennis_breakpoints_2.


/* tennis_determine_serve_and_volley(Match) <-
      */

tennis_determine_serve_and_volley :-
   tennis_file_to_match_fn_list(Match),
   tennis_determine_serve_and_volley(Match).

tennis_determine_serve_and_volley(Match) :-
   tennis_statistics_set_variables,
   tennis_statistics_set_names(Match),
   tennis_determine_att_rallys(Match, Rallys),  
   tennis_service_and_volleys(Rallys).
   

/* tennis_determine_rallys_each_game <-
      */

tennis_determine_rallys_each_game :-
   tennis_file_to_match_fn_list(Match),
   tennis_statistics_set_variables,
   tennis_rallys_each_game(Match).


/* tennis_determine_length_each_rally(Match) <-
      */

tennis_determine_length_each_rally :-
   writeln(user, tennis_determine_length_each_rally),
   tennis_file_to_match_fn_list(Match),
   tennis_statistics_set_variables,
   tennis_determine_length_each_rally(Match).

tennis_determine_length_each_rally(Match) :-
   tennis_find_all_points(Match, Games),
   tennis_match_to_attributes(Match, Att),
   pair_lists(Att, Games, Games_2),
   checklist( tennis_length_each_rally_to_statistics,
      Games_2 ),
   tennis_length_each_rally_to_variable(
      'tennis_rally_length_a_a', 'tennis_rally_length_a_b',
      'tennis_length_server_a'),
   tennis_length_each_rally_to_variable(
      'tennis_rally_length_b_a', 'tennis_rally_length_b_b',
      'tennis_length_server_b'),
   tennis_length_each_rally_to_variable(
      'tennis_rally_length_a', 'tennis_rally_length_b',
      'tennis_length_of_rally').

tennis_length_each_rally_to_variable(DV1, DV2, DV_Save) :-
   dislog_variable_get(DV1, Server_AA),
   dislog_variable_get(DV2, Server_AB),
   dislog_variable_set(DV_Save, []),
   checklist( tennis_number_to_length1(DV_Save), 
      Server_AA ),
   checklist( tennis_number_to_length2(DV_Save), 
      Server_AB ),
   dislog_variable_get(DV_Save, A),
   sort(A, Sort_A),
   dislog_variable_set(DV_Save, Sort_A).

tennis_length_each_rally_to_statistics([Att, Points]) :-
   Server := Att^service,
   Winner := Att^winner, 
   length(Points, N),
   M is N - 1,
   tennis_rally_length_to_statistics(Server, Winner, M).

tennis_rally_length_to_statistics(Pl_1, Pl_2, N) :-
   char_type(Pl_1, upper(P1)),
   char_type(Pl_2, upper(P2)),
   concat_atom([tennis_rally_length, P1, P2], '_', V_12),
   dislog_variable_add_element(V_12, N),
   concat(tennis_rally_length_, P2, V_2),
   dislog_variable_add_element(V_2, N).
tennis_rally_length_to_statistics(_, _, _).


/* tennis_determine_returns(Match) <-
      */

tennis_determine_returns :-
   tennis_file_to_match_fn_list(Match),
   tennis_statistics_set_variables,
   tennis_determine_att_rallys(Match, Rallys),  
   checklist( tennis_return_point,
      Rallys ).

tennis_return_point([Att, Rally]) :-
   Server := Att^service,
   Winner := Att^winner,
   Server \= Winner,
   length(Rally, 3),
   n_th_element(Rally, 2, Hit),
   Hand := Hit@hand,
   char_type(Server, upper(P)),
   concat_atom([tennis_return, Hand, P], '_', V),
   dislog_variable_increment(V).
tennis_return_point(_).



/* tennis_determine_errors(Match) <-
      */

tennis_determine_errors :-
   tennis_file_to_match_fn_list(Match),
   tennis_statistics_set_variables,
   tennis_determine_errors(Match).

tennis_determine_errors(Match) :-
   tennis_find_all_sets(Match, Sets),
   maplist( tennis_find_errors_in_set, 
      Sets, Errors ),
   dislog_variable_set(current_number_count, 0),
   maplist( tennis_determine_error_a_b,
      Errors, Result ),
   dislog_variable_set(tennis_errors_each_set, Result).

tennis_find_errors_in_set(Sets, Errors) :-
   findall( Error,
      Error := Sets^game@point^error,
      Errors ).

tennis_determine_error_a_b(Errors, [New, A, B]) :-
   number_of_one_element_in_list('A', Errors, A),
   number_of_one_element_in_list('B', Errors, B),
   dislog_variable_get(current_number_count, Set),
   New is Set + 1,
   dislog_variable_set(current_number_count, New).


/* tennis_statistics_set_variables <-
      */

tennis_statistics_set_variables :-
   dislog_variables_set([
      test_statistic - 0,
      tennis_number_of_services_a - 0,
      tennis_number_of_services_b - 0,
      tennis_service_point_a - 0,
      tennis_service_point_b - 0,
      tennis_service_fault_a - 0,
      tennis_service_fault_b - 0,
      tennis_net_a - 0,
      tennis_net_b - 0,
      tennis_out_a - 0,
      tennis_out_b - 0,
      tennis_forehand_ground_a - 0,
      tennis_forehand_ground_b - 0,
      tennis_forehand_volley_a - 0,
      tennis_forehand_volley_b - 0,
      tennis_backhand_ground_a - 0,
      tennis_backhand_ground_b - 0,
      tennis_backhand_volley_a - 0,
      tennis_backhand_volley_b - 0,
      tennis_error_forehand_ground_a - 0,
      tennis_error_backhand_ground_a - 0,
      tennis_error_forehand_ground_b - 0,
      tennis_error_backhand_ground_b - 0,
      tennis_point_forehand_ground_a - 0,
      tennis_point_backhand_ground_a - 0,
      tennis_point_forehand_ground_b - 0,
      tennis_point_backhand_ground_b - 0,
      tennis_error_forehand_volley_a - 0,
      tennis_error_backhand_volley_a - 0,
      tennis_error_forehand_volley_b - 0,
      tennis_error_backhand_volley_b - 0,
      tennis_point_forehand_volley_a - 0,
      tennis_point_backhand_volley_a - 0,
      tennis_point_forehand_volley_b - 0,
      tennis_point_backhand_volley_b - 0,
      tennis_service_and_volley_a_loss - 0,
      tennis_service_and_volley_b_loss - 0,
      tennis_service_and_volley_a_win - 0,
      tennis_service_and_volley_b_win - 0,
      tennis_long_short_a_win -0,
      tennis_long_short_a_loss - 0,
      tennis_long_short_b_win - 0,
      tennis_long_short_b_loss - 0,
      tennis_break_a - 0,
      tennis_break_b - 0,
      tennis_break_points_a - 0,
      tennis_break_points_b - 0,
      tennis_game_length_a - [],
      tennis_game_length_b - [],
      tennis_return_forehand_a - 0,
      tennis_return_backhand_a - 0,
      tennis_return_forehand_b - 0,
      tennis_return_backhand_b - 0,
      tennis_rally_length_a_a - [],
      tennis_rally_length_a_b - [],
      tennis_rally_length_b_a - [],
      tennis_rally_length_b_b - [],
      tennis_rally_length_a - [],
      tennis_rally_length_b - [],
      tennis_direct_point_service_a - 0,
      tennis_direct_point_service_b - 0]).


/*** implementation **************************************************/


/* tennis_rallys_to_hit_types(Rallys) <-
      */

tennis_rallys_to_hit_types(Rallys) :-
   checklist( tennis_rally_to_hit_types,
      Rallys ).
tennis_rallys_to_hit_types(_).

tennis_rally_to_hit_types([Att, Rallys]) :-
   Server := Att^service,
   length(Rallys, N), 
   checklist( tennis_rally_to_hit_type(Server, N),
      Rallys ).
tennis_rally_to_hit_types(_).

tennis_rally_to_hit_type(Server, Last_ID, Hit) :-
   Hit_ID_2 := Hit@id,
   Hand := Hit@hand,
   Type := Hit@type,
   term_to_atom(Hit_ID, Hit_ID_2),
   N is Hit_ID mod 2,
   tennis_rally_to_hit_service_statistics(Server, Hit_ID),
   tennis_hit_type_statistics(
      Server, Hit_ID, Last_ID, N, Hand, Type).
tennis_rally_to_hit_type(_, _, _).

tennis_rally_to_hit_service_statistics(Player, 1) :-
   char_type(Player, upper(P)),
   concat(tennis_number_of_services_, P, V),
   dislog_variable_increment(V).
tennis_rally_to_hit_service_statistics(_, _).

tennis_hit_type_statistics(Player, ID, Last_ID, N, Hand, Type) :-
   ID > 1, Last_ID > ID,
   ( N = 0 ->
     tennis_switch_player(Player, Player_2)
   ; Player_2 = Player ),
   char_type(Player_2, upper(P)),
   concat_atom([tennis, Hand, Type, P], '_', V),
   dislog_variable_increment(V).
tennis_hit_type_statistics(_, _, _, _, _, _).


/* tennis_rallys_to_hit_statistic(Rallys) <-
      */

tennis_rallys_to_hit_statistic(Rallys) :-
   sublist( tennis_more_than_two_hits,
      Rallys, Big_Rallys ),
   checklist( tennis_rally_to_hit_statistics,
      Big_Rallys ).
tennis_rallys_to_hit_statistic(_).

tennis_more_than_two_hits([_, Rally]) :-
   length(Rally, N),
   N > 2.


/* tennis_rallys_each_game(Match) <-
      */

tennis_rallys_each_game(Match) :-
   tennis_find_all_games(Match, Games),
   tennis_find_all_game_attributes(Match, Att),
   pair_lists(Att, Games, Games_2),
   checklist( tennis_rally_each_game_to_statistic,
      Games_2 ),
   tennis_rallys_each_game_2('tennis_game_length_a'),
   tennis_rallys_each_game_2('tennis_game_length_b').
tennis_rallys_each_game(_).

tennis_rallys_each_game_2(Dislog_Variable) :-
   dislog_variable_get(Dislog_Variable, ARs),
   maplist( tennis_n_th_element(2),
      ARs, AA ),
   average(AA, X),
   number(X),
   tennis_round_two_decimals(X, Y), 
   reverse([['Average', Y]|ARs], As),
   dislog_variable_set(Dislog_Variable, As).
tennis_rallys_each_game_2(Dislog_Variable) :-
   dislog_variable_set(Dislog_Variable, [['Average', 0]]).

tennis_rally_each_game_to_statistic([Att, Games]) :-
   ID := Att^id,
   Server := Att^service,
   tennis_determine_game_winner_of_points(Games, Winner),
   length(Games, N),
   tennis_game_length_to_statistic(ID, Server, Winner, N).

tennis_game_length_to_statistic(13, _, _, _).
tennis_game_length_to_statistic(_, Player, Winner, Rallys) :-
   Rallys > 0,
   tennis_player_id_to_name(Winner, Name),
   char_type(Player, upper(P)),
   concat(tennis_game_length_, P, V),
   dislog_variable_add_element(V, [Name, Rallys]).
tennis_game_length_to_statistic(_, _, _, _).


/* tennis_breakpoints_2 <-
      */

tennis_breakpoints_2 :-
   tennis_file_to_match_fn_list(Match),
   tennis_breakpoints_2(Match).
 
tennis_breakpoints_2(Match) :-
   tennis_match_to_attributes(Match, Atts),
   tennis_search_determine_set_point_game_id(S_P_G_IDs),
   length(S_P_G_IDs, LE),
   fill_list(LE, IDRs), reverse(IDRs, IDs),
   pair_lists(IDs, S_P_G_IDs, Ns), 
   pair_lists(Atts, Ns, Lists), 
   dislog_variables_set([
      break_list_a - [],
      break_list_b - []]),
   checklist( tennis_is_break_point,
      Lists ).
tennis_breakpoints_2(_).


/* tennis_is_break_point([Att, [ID, [_,Game,_]]]) <-
      */

tennis_is_break_point([Att, [ID, [_,Game,_]]]) :-
   Game \= 13,
   Server := Att^service,
   Score_A_Atom := Att^score_A,
   Score_B_Atom := Att^score_B,
   term_to_atom(Score_A, Score_A_Atom),
   term_to_atom(Score_B, Score_B_Atom),
   tennis_is_break_point(Server, [Score_A, Score_B], ID).
tennis_is_break_point(_).


/* tennis_is_break_point(Player, [A, B], ID) <-
      */

tennis_is_break_point('A', [A, B], ID) :-
   A < B,
   B >= 3,
   dislog_variable_increment(tennis_break_points_b),
   dislog_variable_add_element(break_list_b, ID).
tennis_is_break_point('B', [A, B], ID) :-
   B < A,
   A >= 3,
   dislog_variable_increment(tennis_break_points_a),
   dislog_variable_add_element(break_list_a, ID).
tennis_is_break_point(_, _).


/* tennis_break_points_for_browser(Type) <-
      Type is either a or b. */

tennis_break_points_for_browser(Type) :-
   concat(break_list_, Type, L),
   concat(tennis_statistics_name_, Type, N),
   dislog_variable_get(L, List),
   dislog_variable_get(N, Name),
   tennis_break_points_for_browser(List, Name).

tennis_break_points_for_browser(List, Name) :-
   dislog_variable_get(tennis_current_output_file, File),
   tennis_determine_hits(File),
   tennis_search_determine_video_files(Videos),
   tennis_search_determine_set_point_game_id(File, S_P_G_IDs),
   tennis_search_determine_score(File, Scores),
   tennis_search_pattern_determine_hits(File, Hits),!,
   maplist( n_th_element(S_P_G_IDs),
      List, IDs ),
   maplist( n_th_element(Scores),
      List, Scores_2 ),
   maplist( n_th_element(Hits),
      List, Hits_2 ),
   triple_lists(IDs, Scores_2, Hits_2, Result_1),
   maplist( tennis_statistics_determine_time,
      Hits_2, StartTime ),
   maplist( tennis_search_browser_determine_score,
      Result_1, Scores_Browser ),
   four_lists(IDs, Scores_Browser, StartTime, List, ID_SC_TIR),
   reverse(ID_SC_TIR, ID_SC_TI),
   tennis_search_pattern_file(Path),
   predicate_to_file( Path,
      ( tennis_html_file_start,
        tennis_search_pattern_display_browser(
           [ID_SC_TI, Videos, File]),
        tennis_html_file_end ) ),
   name_append(['Breakpoints of ', Name], Title),
   tennis_start_browser(Path, Title).


/* tennis_statistics_determine_time(Hits, Time) <-
      */

tennis_statistics_determine_time(Hits, Time) :-
   first(Hits, Hit),
   n_th_element(Hit, 3, Time).
tennis_statistics_determine_time(_, _).


/* tennis_rally_to_hit_statistics([Att, Rallys]) <-
      */

tennis_rally_to_hit_statistics([Att, Rallys]) :-
   Winner := Att^winner,
   Server := Att^service,
   tennis_hit_facts(Rallys, N, Hand, Type),
   ( N = 0 ->
     tennis_switch_player(Server, Player)
   ; Player = Server ),
   ( Player = Winner ->
     InOrOut = in
   ; InOrOut = out ),
   tennis_hit_to_statistics(InOrOut, Player, Hand, Type).
tennis_rally_to_hit_statistics(_).

tennis_hit_facts(Hits, HitX, Hand, Type) :-
   length(Hits, LE),
   N is LE - 1,
   n_th_element(Hits, N, Hit),
   Hit_IDA := Hit@id,
   Hand := Hit@hand,
   Type := Hit@type,
   term_to_atom(Hit_ID, Hit_IDA),
   HitX is Hit_ID mod 2.

tennis_hit_to_statistics(In_Out, Player, Hand, Type) :-
   ( In_Out = in,
     X = point
   ; In_Out = out,
     X = error ),
   char_type(Player, upper(P)),
   concat_atom([tennis, X, Hand, Type, P], '_', V),
   dislog_variable_increment(V).
tennis_hit_to_statistics(_, _, _, _).


/* tennis_determine_rallys(Match, Rallys) <-
      */

tennis_determine_rallys(Match, Rallys) :-
   findall( Rally,
      Rally := Match^match^set^game^point,
      Rallys ),
   length(Rallys, N),
   dislog_variable_set(tennis_number_of_rallys, N).


/* tennis_select_rallys_of_length(N, Rallys_1, Rallys_2) <-
      */

tennis_select_rallys_of_length(N, Rallys_1, Rallys_2) :-
   sublist( tennis_rally_has_length(N),
      Rallys_1, Rallys_2 ).

tennis_rally_has_length(N, [_, Rally]) :-
   length(Rally, N).


/* tennis_determine_number_of_services(Match) <-
      */

tennis_determine_number_of_services(Match) :-
   tennis_find_all_service(Match, Services),
   length(Services, All),
   list_remove_elements(['B'], Services, ServicesA),
   length(ServicesA, ServiceA),
   ServiceB is All - ServiceA,
   dislog_variables_set([
      tennis_number_of_services_a - ServiceA,
      tennis_number_of_rallys - All,
      tennis_number_of_services_b - ServiceB]).


/* tennis_service_and_volleys(Rallys) <-
      */

tennis_service_and_volleys(Rallys) :-
   sublist( tennis_is_service_and_volley,
      Rallys, Rallys_2 ),
   checklist( tennis_statistics_service_and_volley,
      Rallys_2 ).

tennis_is_service_and_volley([_, Hits]) :-
   n_th_element(Hits, 3, Hit),
   volley := Hit@type.

tennis_statistics_service_and_volley([Att, _]) :-
   Server := Att^service,
   Winner := Att^winner,
   ( Server = Winner ->
     X = win
   ; X = loss ),
   char_type(Server, upper(P)),
   concat_atom([tennis_service_and_volley, P, X], '_', V),
   dislog_variable_increment(V).
tennis_statistics_service_and_volley(_).
    

/* tennis_break(Match) <-
      */

tennis_break(Match) :-
   tennis_find_all_games(Match, Games),
   tennis_find_all_game_attributes(Match, Atts),
   pair_lists(Atts, Games, Att_Game),
   checklist( tennis_is_break,
      Att_Game ).

tennis_is_break([Att, Game]) :-
   Server := Att^service,
   tennis_game_winner(Game, Winner),
   tennis_statistics_is_break(Server, Winner).

tennis_statistics_is_break('A', 'B') :-
   dislog_variable_increment(tennis_break_b).
tennis_statistics_is_break('B', 'A') :-
   dislog_variable_increment(tennis_break_a).
tennis_statistics_is_break(_, _).


/* tennis_game_winner(Game, Winner) <-
      */

tennis_game_winner(Game, Winner) :-
   last_element(Game, Point),
   Winner := Point@winner.
tennis_game_winner(_, 'AorB').    


/* tennis_statistics_set_names(Match) <-
      */

tennis_statistics_set_names(Match) :-
   tennis_determine_player_names(Match, Name_A, Name_B),
   dislog_variables_set([
      tennis_statistics_name_a - Name_A,
      tennis_statistics_name_b - Name_B]).


/* tennis_is_service_point_location([Att, Rallys]) <-
      */

tennis_is_service_point_location([Att, Rallys]) :-
   Server := Att^service,
   Winner := Att^winner,
   Server == Winner,
   last_element(Rallys, Hit),
   Xe := Hit@x,
   Ye := Hit@y,
   term_to_atom(X, Xe), 
   term_to_atom(Y, Ye),
   char_type(Server, upper(P)),
   concat(tennis_service_location_, P, V),
   dislog_variable_add_element(V, [X,Y]).
tennis_is_service_point_location(_).


/* tennis_switch_player(Player_1, Player_2) <-
      */

tennis_switch_player('A', 'B').
tennis_switch_player('B', 'A').


/******************************************************************/


