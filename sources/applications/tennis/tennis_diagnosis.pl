

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Diagnosis                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tennis_diagnosis <-
      */

tennis_diagnosis :-
   tennis_determine_statistics_start,
   tennis_determine_ground_and_volley,
   tennis_diagnosis(groundball, Ground),
   tennis_determine_errors,
   tennis_diagnosis(errors, Errors),
   tennis_determine_break,
   tennis_diagnosis(breaks, Breaks),
   tennis_display_diagnosis(Ground, Errors, Breaks).


/*** implementation ***********************************************/


tennis_diagnosis(groundball, [Analysis_A, Analysis_B]) :-
   get_variables_groundball(Ground_A, Ground_B),
   diagnose_groundballs(Ground_A, Analysis_A),
   diagnose_groundballs(Ground_B, Analysis_B).

tennis_diagnosis(errors, Analysis) :-
   get_error_variables(Errors_A, Errors_B),
   pair_lists(Errors_A, Errors_B, Errors),
   maplist( tennis_error_diagnosis,
      Errors, Analysis ).

tennis_diagnosis(breaks, [Points, Breaks, Percentage]) :-
   get_service_variables(Points_A, Breaks_A, Points_B, Breaks_B),
   proportion_points(Points_A, Points_B, Points),
   proportion_breaks(Breaks_A, Breaks_B, Breaks),
   tennis_diagnosis(breaks_percentage,
      [Breaks_A, Points_A, Breaks_B, Points_B, Percentage]).
tennis_diagnosis(breaks, _).

tennis_diagnosis(breaks_percentage,
      [Breaks_A, Points_A, Breaks_B, Points_B, Percentage]) :-
   Points_A > 0,
   Points_B > 0,
   Percentage_A is Breaks_A / Points_A,
   Percentage_B is Breaks_B / Points_B,
   proportion_percentage(Percentage_A, Percentage_B,
      Percentage).   
tennis_diagnosis(breaks_percentage, [_, _, _, _, 'NA']).

diagnose_groundballs(
      [Forehand, Forehand_Point, Forehand_Error,
       Backhand, Backhand_Point, Backhand_Error],
      [Ana1, Ana2, Ana3, Ana4, Ana5, Ana6]) :-
   diagnose_groundballs_errors(
      Forehand, Forehand_Error, Ana1),
   diagnose_groundballs_errors(
      Backhand, Backhand_Error, Ana2),
   diagnose_groundballs_points(
      Forehand, Forehand_Point, Ana3),
   diagnose_groundballs_points(
      Backhand, Backhand_Point, Ana4),
   diagnose_groundballs_rel_points(
      Forehand_Point, Backhand_Point, Ana5),
   diagnose_groundballs_rel_errors(
      Forehand_Error, Backhand_Error, Ana6).


diagnose_groundballs_errors(0, _, 'NA').
diagnose_groundballs_errors(Number, Errors, 'Not many') :-
   0.07 > Errors / Number.
diagnose_groundballs_errors(Number, Errors, 'Many') :-
   0.15 < Errors / Number.
diagnose_groundballs_errors(_, _, 'Nothing special').

diagnose_groundballs_points(0, _, 'NA').
diagnose_groundballs_points(Number, Points, 'Many') :-
   0.1 < Points / Number.
diagnose_groundballs_points(Number, Points, 'Not many') :-
   0.05 > Points / Number.
diagnose_groundballs_points(_, _, 'Nothing special').


diagnose_groundballs_rel_points(0, _, 'NA').
diagnose_groundballs_rel_points(_, 0, 'NA').
diagnose_groundballs_rel_points(Forehand, Backhand,
      'More Forehand Points') :-
   2.0 < Forehand / Backhand.
diagnose_groundballs_rel_points(Forehand, Backhand,
      'More Backhand Points') :-
   2.0 > Backhand / Forehand.
diagnose_groundballs_rel_points(_, _, 'Nothing special').


diagnose_groundballs_rel_errors(0, _, 'NA').
diagnose_groundballs_rel_errors(_, 0, 'NA').
diagnose_groundballs_rel_errors(Forehand, Backhand,
      'More Forehand Errors') :-
   2.0 < Forehand / Backhand.
diagnose_groundballs_rel_errors(Forehand, Backhand,
      'More Backhand Errors') :-
   2.0 > Backhand / Forehand.
diagnose_groundballs_rel_errors(_, _, 'Nothing special').


tennis_error_diagnosis([Error_A, Error_B], Advantage) :-
   (Error_A + Error_B) > 0,
   Differenc is Error_A / (Error_A + Error_B),
   Differenc < 0.4,
   dislog_variable_get(tennis_statistics_name_a, Name),
   concat('Advantage ', Name, Advantage). 
tennis_error_diagnosis([Error_A, Error_B], Advantage) :-
   (Error_A + Error_B) > 0,
   Differenc is Error_B / (Error_A + Error_B),
   Differenc < 0.4,
   dislog_variable_get(tennis_statistics_name_b, Name),
   concat('Advantage ', Name, Advantage). 
tennis_error_diagnosis([_, _], 'Deuce').



proportion_points(A, B, Advantage) :-
   (A + B) > 0,
   Percentage is A / (A + B),
   Percentage > 0.6,
   dislog_variable_get(tennis_statistics_name_a, Name),
   concat('Advantage ', Name, Advantage).
proportion_points(A, B, Advantage) :-
   (A + B) > 0,
   Percentage is B / (A + B),
   Percentage > 0.6,
   dislog_variable_get(tennis_statistics_name_b, Name),
   concat('Advantage ', Name, Advantage).
proportion_points(_, _, 'Deuce').

proportion_breaks(A, B, Advantage) :-
   Difference is A - B,
   Difference >= 2,
   dislog_variable_get(tennis_statistics_name_a, Name),
   concat('Advantage ', Name, Advantage).
proportion_breaks(A, B, Advantage) :-
   Difference is B - A,
   Difference >= 2,
   dislog_variable_get(tennis_statistics_name_b, Name),
   concat('Advantage ', Name, Advantage).
proportion_breaks(_, _, 'Deuce').

proportion_percentage(Percentage_A, Percentage_B, Advantage) :-
   Difference is Percentage_A - Percentage_B,
   Difference > 0.3,
   dislog_variable_get(tennis_statistics_name_a, Name),
   concat('Advantage ', Name, Advantage).
proportion_percentage(Percentage_A, Percentage_B, Advantage) :-
   Difference is Percentage_B - Percentage_A,
   Difference > 0.3,
   dislog_variable_get(tennis_statistics_name_b, Name),
   concat('Advantage ', Name, Advantage).
proportion_percentage(_, _, 'Deuce').


get_variables_groundball([FA, FPA, FEA, BA, BPA, BEA],
      [FB, FPB, FEB, BB, BPB, BEB]) :-
   dislog_variables_get([
      tennis_point_forehand_ground_a - FPA,
      tennis_forehand_ground_a - FA,
      tennis_error_forehand_ground_a - FEA,
      tennis_point_forehand_ground_b - FPB,
      tennis_forehand_ground_b - FB,
      tennis_error_forehand_ground_b - FEB,
      tennis_point_backhand_ground_a - BPA,
      tennis_backhand_ground_a - BA,
      tennis_error_backhand_ground_a - BEA,
      tennis_point_backhand_ground_b - BPB,
      tennis_backhand_ground_b - BB,
      tennis_error_backhand_ground_b - BEB]).

get_error_variables(Error_A, Error_B) :-
   dislog_variable_get(tennis_errors_each_set, Errors),
   tennis_sum_of_errors(Errors, Error_A, Error_B).

get_service_variables(Points_A, Breaks_A, Points_B, Breaks_B) :-
   dislog_variables_get([
      tennis_break_points_a - Points_A,
      tennis_break_a - Breaks_A,
      tennis_break_points_b - Points_B,
      tennis_break_b - Breaks_B]).

tennis_sum_of_errors(Errors, Error_As, Error_Bs) :-
   maplist(tennis_n_th_element(2),
      Errors, Error_As),
   maplist(tennis_n_th_element(3),
      Errors, Error_Bs).
tennis_sum_of_errors(_, _, _).


/* tennis_display_diagnosis(Ground, Errors, Breaks) <-
      */

tennis_display_diagnosis(Ground, Errors, Breaks) :-
   Ground = [As, Bs],
   As = [FEA, BEA, FPA, BPA, FBPA, FBEA],
   Bs = [FEB, BEB, FPB, BPB, FBPB, FBEB],
   tennis_diagnosis_file(File),
   concat('file://', File, Path),
   tell(File), told,
   append(File),
   dislog_variables_get([
      tennis_statistics_name_a - NA,
      tennis_statistics_name_b - NB]),
   tennis_html_file_start,
   tennis_write_html_table('center', [
      ' ' - NA - NB], [
      ' ' - ' ' - ' ',
      ' ' - ' ' - ' ',
      'Forehand Points: ' - FPA - FPB,
      'Backhand Points: ' - BPA - BPB,
      'Forehand Errors: ' - FEA - FEB,
      'Backhand Errors: ' - BEA - BEB,
      'Fore-/Backhand Points: ' - FBPA - FBPB,
      'Fore-/Backhand Points: ' - FBEA - FBEB,
      ' ' - ' ' - ' ',
      ' ' - ' ' - ' ']),
   tennis_display_diagnosis_breaks(Breaks),
   tennis_display_diagnosis_errors(Errors),
   tennis_html_file_end,
   told,
   tennis_statistics_doc_browser(Path, 'Diagnosis').

tennis_display_diagnosis_breaks([Points, Breaks, Percentage]) :-
   tennis_write_html_table('center', [
      'Breakpoints: ' - Points,
      'Breaks: ' - Breaks,
      'Percentage: ' - Percentage,
      ' ' - ' ' - ' ',
      ' ' - ' ' - ' ']).

tennis_display_diagnosis_breaks(_).

tennis_display_diagnosis_errors(Errors) :-
   Sets = ['Unforced Errors Set1: ', 'Unforced Errors Set2: ',
      'Unforced Errors Set3: ', 'Unforced Errors Set4: ',
      'Unforced Errors Set5: '],
   pair_lists([], Sets, Errors, Column),
   tennis_write_html_table('center', Column).
tennis_display_diagnosis_errors(_).


/******************************************************************/


