

/******************************************************************/
/***                                                            ***/
/***         Tennis Tool: Court Operations                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tennis_xml_term_to_tuple(Xml, As) <-
      */

tennis_xml_term_to_tuple(Xml, As) :-
   fn_item_parse(Xml, T:As_1:Es),
   ( T = hit, As = As_1
   ; T \= hit, member(Yml, Es),
     tennis_xml_term_to_tuple(Yml, As_2),
     append(As_1, As_2, As) ).
tennis_xml_term_to_tuple(Xml, []) :-
   \+ fn_item_parse(Xml, _:_:_).


/* tennis_xml_to_points(Is_x, Is_y, Xml, Points) <-
      Tessalation and coarsening of the XML trajectory. */

tennis_xml_to_points(Is_x, Is_y, Xml, Points) :-
   findall( point:As_2:Es_2,
      ( point:As_1:Es_1 := Xml/set::[@id=S]/
           game::[@id=G, @score_A=A, @score_B=B]/
              point::[@id=P, @score_A=E, @score_B=F],
        hit_list_transform(Is_x, Is_y, Es_1, Es_2),
        maplist( term_to_atom, [C,D], [A,B] ),
        maplist( term_to_atom, [H,I], [E,F] ),
        append([set:S, game:G, point:P,
            score_AB:(H:I), score_G:(C:D)], As_1, As_2) ),
      Points ).


/* tennis_xml_points_to_table(Points, Table) <-
      */

tennis_xml_points_to_table(Points, Table) :-
   findall( As,
      ( Point := Points/point,
        Point = point:As_1:Es_1,
%       Hit := Point/hit,
        nth(N, Es_1, Hit),
        Hit = hit:As_2:_,
        append([hit:N|As_1], As_2, As) ),
      Table ).


/*** implementation ***********************************************/


/* hit_list_transform(Is_x, Is_y, Hits_1, Hits_2) <-
      */

hit_list_transform(Is_x, Is_y, [H1, H2|Hits_1], [H3|Hits_2]) :-
   !,
   tennis_assign_to_interval(Is_x, H1, x, Ix),
   tennis_assign_to_interval(Is_y, H1, y, Iy),
   tennis_assign_to_interval(Is_x, H2, x, Jx),
   tennis_assign_to_interval(Is_y, H2, y, Jy),
   H1 = hit:As_1:_,
   H2 = hit:As_2:_,
   attributes_suffix_by('_1', As_1, Bs_1),
   attributes_suffix_by('_2', As_2, Bs_2),
   attributes_suffix_by('_1', ['Ix':Ix, 'Iy':Iy], Cs_1),
   attributes_suffix_by('_2', ['Ix':Jx, 'Iy':Jy], Cs_2),
   append([Cs_1, Cs_2, Bs_1, Bs_2], As_3),
   H3 = hit:As_3:[],
   hit_list_transform(Is_x, Is_y, [H2|Hits_1], Hits_2).
hit_list_transform(_, _, _, []).

tennis_assign_to_interval(
      Intervals, Item, Attribute, Interval) :-
   Value := Item@Attribute,
   name_to_number(Value, Number),
   tennis_assign_to_interval(Intervals, Number, Interval). 

attributes_suffix_by(Suffix, As_1, As_2) :-
   foreach(A1, As_1), foreach(A2, As_2) do
      A1 = Attribute_1: Value,
      concat(Attribute_1, Suffix, Attribute_2),
      A2 = Attribute_2: Value.


/* tennis_size_to_intervals(Size, N, Intervals) <-
      */

tennis_size_to_intervals(Size, Xs, Intervals) :-
   is_list(Xs),
   maplist( tennis_size_percentages_to_intervals(Size),
      Xs, Is ),
   append([[-inf,-Size]|Is], [[Size, +inf]], Intervals).
tennis_size_to_intervals(Size, N, Intervals) :-
   integer(N),
   ( for(I, 1, N), foreach(Interval, Is) do
        ( round(-Size + 2*(I-1) * Size / N, 2, A),
          round(-Size + 2*I * Size / N, 2, B),
          Interval = [A, B] ) ),
   append([[-inf,-Size]|Is], [[Size, +inf]], Intervals).

tennis_size_percentages_to_intervals(Size, [P1,P2], [X1,X2]) :-
   X1 is -Size + 2 * P1 * Size,
   X2 is -Size + 2 * P2 * Size.


/* tennis_assign_to_interval(Intervals, Value, Interval) <-
      */

% tennis_assign_to_interval(Intervals, Value, Interval) :-
tennis_assign_to_interval(Intervals, Value, M) :-
%  member(Interval, Intervals),
   nth(N, Intervals, Interval),
   M is N - 1,
   Interval = [A,B],
   ( A = -inf -> Value =< B
   ; B = +inf -> A < Value
   ; A < Value, Value =< B ).


/******************************************************************/


