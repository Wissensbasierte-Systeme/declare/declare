

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Statistics Display                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* statistics_link_handler(X) <-
      */

statistics_link_handler(X) :-
   member(X, [
      tennis_statistics_service_frame,
      tennis_statistics_ground_frame,
      tennis_statistics_volley_frame,
      tennis_statistics_break_frame,
      tennis_statistics_serve_and_volley_frame,
      tennis_determine_service_point_location,
      tennis_statistics_rallys_each_game_frame,
      tennis_statistics_return_frame,
      tennis_statistics_length_each_rally_frame,
      tennis_statistics_error_frame ]),
   !,
   call(X).

statistics_link_handler(tennis_break_points_a) :-
   tennis_break_points_for_browser(a).
statistics_link_handler(tennis_break_points_b) :-
   tennis_break_points_for_browser(b).   
statistics_link_handler(tennis_diagnosis_frame) :-
   tennis_diagnosis.

statistics_link_handler(tennis_match_to_hits_of_hand_sr) :-
   tennis_match_to_hits_of_hand([service, return]).
statistics_link_handler(tennis_match_to_hits_of_hand_s) :-
   tennis_match_to_hits_of_hand([service]).
statistics_link_handler(tennis_match_to_hits_of_hand_r) :-
   tennis_match_to_hits_of_hand([return]).

statistics_link_handler(tennis_match_to_hits_of_hand_src) :-
   tennis_match_to_hits_of_hand([service, return, circumvented]).
statistics_link_handler(tennis_match_to_hits_of_hand_sc) :-
   tennis_match_to_hits_of_hand([service, circumvented]).
statistics_link_handler(tennis_match_to_hits_of_hand_rc) :-
   tennis_match_to_hits_of_hand([return, circumvented]).


/* tennis_statistics_display <-
      */

tennis_statistics_display :-
   tennis_statistics_frame_file(File),
   concat('file://', File, Path),
   tennis_determine_statistics_start,
   tennis_statistics_display(
      'Tennis Statistics', [300, 250], Path).


/* tennis_statistics_ground_balls_display <-
      */

tennis_statistics_ground_balls_display :-
   tennis_statistics_ground_balls_frame_file(File),
   concat('file://', File, Path),
   tennis_determine_statistics_start,
   tennis_statistics_display(
      'Tennis Statistics for Groundballs', [300, 175], Path).


/* tennis_statistics_display(Title, [X, Y], Path) <-
      */

tennis_statistics_display(Title, [X, Y], Path) :-
   new(Frame, frame(Title)),
   send(Frame, append,
      new(CDW, cms_doc_window(Path))),
   send(CDW, size, size(X, Y)),
   send(CDW, link_handler, statistics_link_handler),
   send(Frame, open).


tennis_statistics_service_doc_browser(Path, Title) :-
   dislog_variables_get([
      tennis_statistics_name_a - NA,
      tennis_statistics_name_b - NB]),
   name_append(['Service of ', NA], NameA),
   name_append(['Service of ', NB], NameB),
   new(Dialog, dialog(Title)),
   send(Dialog, append,
      new(CDW, cms_doc_window(Path))),
   send(CDW, link_handler, statistics_link_handler),
   send(Dialog, append,
      button(close, message(Dialog, destroy))),
   send(Dialog, append,
      button(NameA, message(@prolog,tennis_bar_group_service_a))),
   send(Dialog, append,
      button(NameB, message(@prolog,tennis_bar_group_service_b))),
   send(Dialog, append,
      button('Aces Location', message(
         @prolog, tennis_determine_service_point_location))),
   send(Dialog, open).


tennis_statistics_doc_browser(Path, Title) :-
   new(Dialog, dialog(Title)),
   send(Dialog, append,
      new(CDW, cms_doc_window(Path))),
   send(CDW, link_handler, statistics_link_handler),
   send(Dialog, append,
      button(close, message(Dialog, destroy))),
   send(Dialog, open).


tennis_statistics_service_frame :-
   tennis_determine_service, 
   tennis_determine_direct_point_service, 
   tennis_statistics_service(
      NA, SA, AA, EA, DA, NB, SB, AB, EB, DB),
   html_display_table('Services',
      ['', NA, NB], [
      ['Services', SA, SB],
      ['Aces', AA, AB],
      ['Direct Points', DA, DB],
      ['Double Faults', EA, EB] ],
      size(250, 150)),
   tennis_bar_group_service_a,
   tennis_bar_group_service_b.


/* tennis_statistics_ground_frame <-
      */

tennis_statistics_ground_frame :-
   tennis_determine_ground_and_volley,
   tennis_statistics_forehand_ground(
      NA, FA, PFA, EFA, NB, FB, PFB, EFB),
   tennis_statistics_backhand_ground(
      BA, PBA, EBA, BB, PBB, EBB),
   html_display_table('Groundballs',
      ['', NA, NB], [
      ['Forehand', '', ''],
      ['Total', FA, FB],
      ['Points', PFA, PFB],
      ['Errors', EFA, EFB],
      ['Backhand', '', ''],
      ['Total', BA, BB],
      ['Points', PBA, PBB],
      ['Errors', EBA, EBB] ],
      size(250, 225)).


/* tennis_statistics_volley_frame <-
      */

tennis_statistics_volley_frame :-
   tennis_determine_ground_and_volley,
   tennis_statistics_forehand_volley(
      NA, FA, PFA, EFA, NB, FB, PFB, EFB),
   tennis_statistics_backhand_volley(
      BA, PBA, EBA, BB, PBB, EBB),
   html_display_table('Volleys',
      ['', NA, NB], [
      ['Forehand', '', ''],
      ['Total', FA, FB],
      ['Points', PFA, PFB],
      ['Errors', EFA, EFB],
      ['Backhand', '', ''],
      ['Total', BA, BB],
      ['Points', PBA, PBB],
      ['Errors', EBA, EBB] ],
      size(250, 225)).


tennis_statistics_break_frame :-
   tennis_determine_break,   
   tennis_statistics_file(File),
   concat('file://', File, Path),
   tennis_statistics_break(NA, NB, BA, BPA, BB, BPB),
   tennis_calculate_percent(BA, BPA, PA),
   tennis_calculate_percent(BB, BPB, PB),
   tell(File),
   tennis_html_file_start,
   tennis_write_html_table('center', [
      ' ' - (b:[NA]) - (b:[NB])], [
      'Number of Breakpoints: ' -
         (a:[href:'prolog:tennis_break_points_a']:[BPA]) -
         (a:[href:'prolog:tennis_break_points_b']:[BPB]),
      'Number of Breaks: ' - BA - BB,
      'Break Percent' - PA - PB ]),
%  dwrite(xml, ''),
%  dwrite(xml, h3:['Breakpoints']),
%  dwrite(xml,
%     ul:[
%        li:[a:[href:'prolog:tennis_break_points_a']:[NA]],
%        li:[a:[href:'prolog:tennis_break_points_b']:[NB]]] ),
   tennis_html_file_end,
   told,
   tennis_statistics_display(
      'Breakpoints and Breaks', [320, 100], Path).
%  tennis_statistics_doc_browser(
%     Path, 'Breakpoints and Breaks').


tennis_statistics_serve_and_volley_frame :-
   tennis_determine_serve_and_volley,   
   tennis_statistics_serve_and_volley(
      NA, NB, WA, LA, WB, LB),
   AA is WA + LA,
   AB is WB + LB,
   tennis_calculate_percent(WA, AA, PA),
   tennis_calculate_percent(WB, AB, PB),
   tennis_calculate_percent(LA, AA, QA),
   tennis_calculate_percent(LB, AB, QB),
   name_append([WA, ' (', PA, '%)'], XA),
   name_append([WB, ' (', PB, '%)'], XB),
   name_append([LA, ' (', QA, '%)'], YA),
   name_append([LB, ' (', QB, '%)'], YB),
   html_display_table('Serve and Volley',
      ['', NA, NB], [
      ['Attempts', AA, AB],
      ['Win', XA, XB],
      ['Loose', YA, YB] ],
      size(275, 115)).


tennis_statistics_return_frame :-
   tennis_determine_returns,   
   tennis_statistics_return(NA, NB, FA, BA, FB, BB),
   html_display_table('Returns',
      ['', NA, NB], [
      ['Forehand', FA, FB],
      ['Backhand', BA, BB] ],
      size(275, 115)).


tennis_statistics_rallys_each_game_frame :-
   tennis_determine_rallys_each_game,
   tennis_statistics_rallys_each_game(NA, NB, As, Bs),
   name_append(['Rallys for Services of ', NA], HeadA),
   name_append(['Rallys for Services of ', NB], HeadB),
   tennis_bar_chart(HeadA, As, 'orange'),
   tennis_bar_chart(HeadB, Bs, 'lightblue'),
   ddk_message(['The winners of the games \n',
      'are shown on the X-axis.']).

tennis_statistics_length_each_rally_frame :-
   tennis_determine_length_each_rally,
   !,
   dislog_variable_get(tennis_length_of_rally, Both),
   tennis_bar_group_chart(
      'Length of Rallys', Both, colour(blue), colour(red)).


tennis_statistcs_length_each_rally_frame(
      'tennis_length_server_a') :-
   dislog_variables_get([
      tennis_statistics_name_a - NA,
      tennis_length_server_a - As]),
   name_append(['Length of Rallys - Server: ', NA], HeadA),
   tennis_bar_group_chart_a_b(HeadA, As, 'blue', 'red').


tennis_statistcs_length_each_rally_frame(
      'tennis_length_server_b') :-
   dislog_variables_get([
      tennis_statistics_name_b - NB,
      tennis_length_server_b - Bs]),
   name_append(['Length of Rallys - Server: ', NB], HeadB),
   tennis_bar_group_chart_a_b(HeadB, Bs, 'blue', 'red').

   
tennis_statistics_error_frame :-
   tennis_determine_errors,
   !,
   tennis_statistics_errors(NA, NB, Errors),
   tennis_bar_group_chart2(NA, NB, Errors).
   
tennis_statistics_service(
      NA, SA, AA, EA, DA, NB, SB, AB, EB, DB) :-
   dislog_variables_get([
      tennis_statistics_name_a - NA,
      tennis_statistics_name_b - NB]),
   dislog_variables_get([
      tennis_number_of_services_a - SA,
      service_set_a - SetA,
      service_direct_set_a - DirectA,  
      tennis_number_of_services_b - SB,
      service_set_b - SetB,
      service_direct_set_b - DirectB]),
   tennis_statistics_point_fault(SetA, AA, EA),
   tennis_statistics_point_fault(SetB, AB, EB),
   tennis_statistics_direct_point(DirectA, DA),
   tennis_statistics_direct_point(DirectB, DB).

tennis_statistics_point_fault(Sets, Point, Error) :-
   maplist( tennis_n_th_element(1),
      Sets, Points ),
   add(Points, Point),
   maplist( tennis_n_th_element(2),
      Sets, Errors ),
   add(Errors, Error).

tennis_statistics_direct_point(Lists, Point) :-
   maplist( tennis_n_th_element(2),
      Lists, Points ),
   add(Points, Point).

tennis_statistics_service_each_set(a, As) :-
   dislog_variables_get([
      service_set_a - SetA,
      service_direct_set_a - DirectA]),
   pair_lists(DirectA, SetA, A),
   maplist( append,
      A, As ).

tennis_statistics_service_each_set(b, Bs) :-
   dislog_variables_get([
      service_set_b - SetB,
      service_direct_set_b - DirectB]),
   pair_lists(DirectB, SetB, B),
   maplist( append,
      B, Bs ).
   

tennis_statistics_forehand_ground(
      NA, FA, PFA, EFA, NB, FB, PFB, EFB) :-
   dislog_variables_get([
      tennis_statistics_name_a - NA,
      tennis_statistics_name_b - NB,
      tennis_forehand_ground_a - FA,
      tennis_forehand_ground_b - FB,
      tennis_error_forehand_ground_a - EFA,
      tennis_error_forehand_ground_b - EFB,
      tennis_point_forehand_ground_a - PFA,
      tennis_point_forehand_ground_b - PFB]).

tennis_statistics_backhand_ground(
      BA, PBA, EBA, BB, PBB, EBB) :-
   dislog_variables_get([
      tennis_backhand_ground_a - BA,
      tennis_backhand_ground_b - BB,
      tennis_error_backhand_ground_a - EBA,
      tennis_error_backhand_ground_b - EBB,
      tennis_point_backhand_ground_a - PBA,
      tennis_point_backhand_ground_b - PBB]).

tennis_statistics_forehand_volley(
      NA, VA, PVA, EVA, NB, VB, PVB, EVB) :-
   dislog_variables_get([
      tennis_statistics_name_a - NA,
      tennis_statistics_name_b - NB,
      tennis_forehand_volley_a - VA,
      tennis_forehand_volley_b - VB,
      tennis_error_forehand_volley_a - EVA,
      tennis_error_forehand_volley_b - EVB,
      tennis_point_forehand_volley_a - PVA,
      tennis_point_forehand_volley_b - PVB]).

tennis_statistics_backhand_volley(VA, PVA, EVA, VB, PVB, EVB) :-
   dislog_variables_get([
      tennis_backhand_volley_a - VA,
      tennis_backhand_volley_b - VB,
      tennis_error_backhand_volley_a - EVA,
      tennis_error_backhand_volley_b - EVB,
      tennis_point_backhand_volley_a - PVA,
      tennis_point_backhand_volley_b - PVB]).

tennis_statistics_break(NA, NB, BA, BPA, BB, BPB) :-
   dislog_variables_get([
      tennis_statistics_name_a - NA,
      tennis_statistics_name_b - NB,
      tennis_break_a - BA,
      tennis_break_b - BB,
      tennis_break_points_a - BPA,
      tennis_break_points_b - BPB]).


tennis_statistics_serve_and_volley(NA, NB, WA, LA, WB, LB) :-
   dislog_variables_get([
      tennis_statistics_name_a - NA,
      tennis_statistics_name_b - NB,
      tennis_service_and_volley_a_loss - LA,
      tennis_service_and_volley_b_loss - LB,
      tennis_service_and_volley_a_win - WA,
      tennis_service_and_volley_b_win - WB]).


tennis_statistics_rallys_each_game(NA, NB, A, B) :-
   dislog_variables_get([
      tennis_statistics_name_a - NA,
      tennis_statistics_name_b - NB,
      tennis_game_length_a - A,
      tennis_game_length_b - B]).


tennis_statistics_length_each_rally(NA, NB, A, B) :-
   dislog_variable_get(tennis_statistics_name_a, NA),
   dislog_variable_get(tennis_statistics_name_b, NB),
   dislog_variables_get([
      tennis_length_server_a - A,
      tennis_length_server_b - B]).

tennis_statistics_errors(NA, NB, Errors) :-
   dislog_variable_get(tennis_statistics_name_a, NA),
   dislog_variable_get(tennis_statistics_name_b, NB),
   dislog_variable_get(tennis_errors_each_set, Errors).

tennis_statistics_return(NA, NB, FA, BA, FB, BB) :-
   dislog_variable_get(tennis_statistics_name_a, NA),
   dislog_variable_get(tennis_statistics_name_b, NB),
   dislog_variables_get([
      tennis_return_forehand_a - FA,
      tennis_return_backhand_a - BA,
      tennis_return_forehand_b - FB,
      tennis_return_backhand_b - BB]).


tennis_bar_chart(Server, Lengths, Colour) :-
   length(Lengths, Number_of_Games_1),
   maximum([1, Number_of_Games_1], Number_of_Games),
   tennis_determine_y_max_coordinate(Lengths, Max),
   tennis_bar_chart(
      Server, Max, Number_of_Games, Lengths, Colour).

tennis_bar_chart(Server, Max, Games, Lengths, Colour) :-
   new(Frame, frame(Server)),
   send(Frame, append,
      new(W, auto_sized_picture)),
%  send(new(Dialog,dialog), below, W),
%  send(Dialog, append,
%     new(button('Close', message(Frame, destroy)))),
   send(W, display,
      new(BC, bar_chart(vertical, 0, Max, 100, Games))),
   checklist( tennis_append_bars(BC, Colour),
      Lengths ),
   send(W, open).

tennis_append_bars(Bar_Chart, Colour, [Winner, High]) :-
   send(Bar_Chart, append, bar(Winner, High, Colour)).
tennis_determine_y_max_coordinate(Lengths, Max) :-
   maplist( tennis_n_th_element(2),
      Lengths, List ),
   maximum(List, Max1),
   maximum([1, Max1], Max).
tennis_determine_y_max_coordinate(_, 1).


tennis_bar_group_service_a :-
   tennis_statistics_service_each_set(a, Sets),
   dislog_variable_get(tennis_statistics_name_a, Name),
   length(Sets, X1),
   maximum([X1, 1], X),
   tennis_determine_y_max_service(Sets, Y),
   tennis_bar_group_service(Name, X, Y, Sets).   

tennis_bar_group_service_b :-
   tennis_statistics_service_each_set(b, Sets),
   dislog_variable_get(tennis_statistics_name_b, Name),
   length(Sets, X1),
   maximum([X1, 1], X),
   tennis_determine_y_max_service(Sets, Y),
   tennis_bar_group_service(Name, X, Y, Sets).

tennis_bar_group_service(Name, X, Y, Sets) :-
   new(Frame, frame('Services')),
   send(Frame, append,
      new(W, auto_sized_picture)),
   send(new(DialogColour, dialog), below, W),
%  send(new(DialogControll, dialog), below, DialogColour), 
%  send(DialogControll, append,
%     button(close, message(Frame, destroy))),
   tennis_append_name_to_bar_group_service(Name, DialogColour), 
   send(W, display,
      new(BC, bar_chart(vertical, 0, Y, 100, X))),
   checklist( tennis_append_three_bar_groups(BC),
      Sets ),
   send(Frame, open).

tennis_append_three_bar_groups(BC, [Set, Direct, Point, Error]) :-
   name_append(['Set ', Set], Label),
   send(BC, append, bar_group(Label,
      bar('Aces', Point, green),
      bar('Direct Points', Direct, orange),
      bar('Double Faults', Error, red))).

tennis_append_name_to_bar_group_service(Name, Dialog) :-
%  send(Dialog, display,
%     text('X: Number of Set'), point(10, 10)),
%  send(Dialog, display,
%     text('Y: Number'), point(140, 10)),
   send(Dialog, display,
      text(Name), point(10, 9)),
   send(Dialog, display,
      new(Box, box(10, 10)), point(10, 30)),
   send(Box, colour, green),
   send(Box, fill_pattern, colour(green)),
   send(Dialog, display,
      text('Aces'), point(30, 29)),
   send(Dialog, display,
      new(Box2, box(10, 10)), point(10, 50)),
   send(Box2, colour, orange),
   send(Box2, fill_pattern, colour(orange)),
   send(Dialog, display,
      text('Direct Points'), point(30, 49)),
   send(Dialog, display,
      new(Box3, box(10, 10)), point(10, 70)),
   send(Box3, colour, red),
   send(Box3, fill_pattern, colour(red)),
   send(Dialog, display,
      text('Double Faults'), point(30, 69)).


tennis_determine_y_max_service(Sets, Max) :-
   maplist( tennis_n_th_element(2),
      Sets, Ds ),
   maplist( tennis_n_th_element(3),
      Sets, As ),
   maplist( tennis_n_th_element(4),
      Sets, Es ),
   maximum(Ds, D),
   maximum(As, A),
   maximum(Es, E),
   maximum([D, A, E, 1], Max).

tennis_bar_group_chart(Server, Lengths, Colour1, Colour2) :-
   length(Lengths, Number_of_Games),
   tennis_determine_y_max_coordinate2(Lengths, Max),
   tennis_bar_group_chart(
      Server, Max, Number_of_Games, Lengths, 
      Colour1, Colour2).
tennis_bar_group_chart(_,_,_,_).
tennis_bar_group_chart(
      Server, Max, Games, Lengths, Colour1, Colour2) :-
   new(Frame, frame(Server)),
   send(Frame, append,
      new(W, auto_sized_picture)),
   send(new(DialogColour, dialog), below, W),
   send(new(Dialog,dialog), below, DialogColour),
   dislog_variables_get([
      tennis_statistics_name_a - NameA,
      tennis_statistics_name_b - NameB]),
   name_append(['Server ', NameA], B1),
   name_append(['Server ', NameB], B2),
   tennis_append_name_to_bar_groups(DialogColour),
   send(Dialog, append,
      new(button('Close', message(Frame, destroy)))),
   checklist( send_append(Dialog), [
      B1 - tennis_statistcs_length_each_rally_frame(
              'tennis_length_server_a'),
      B2 - tennis_statistcs_length_each_rally_frame(
              'tennis_length_server_b')]),
   send(W, display,
      new(BC, bar_chart(vertical, 0, Max, 100, Games))),
   checklist( tennis_append_bar_groups(BC, Colour1, Colour2),
      Lengths ),
   send(W, open).

tennis_bar_group_chart_a_b(Server, Lengths, Colour1, Colour2) :-
   length(Lengths, Number_of_Games),
   tennis_determine_y_max_coordinate2(Lengths, Max),
   tennis_bar_group_chart_a_b(
      Server, Max, Number_of_Games, Lengths, 
      Colour1, Colour2).
tennis_bar_group_chart_a_b(Server, Max, Games, Lengths, 
      Colour1, Colour2) :-
   new(Frame, frame(Server)),
   send(Frame, append,
      new(W, auto_sized_picture)),
   send(new(DialogColour, dialog), below, W),
   send(new(Dialog,dialog), below, DialogColour),
   tennis_append_name_to_bar_groups(DialogColour),
   send(Dialog, append,
      new(button('Close', message(Frame, destroy)))),
   send(W, display,
      new(BC, bar_chart(vertical, 0, Max, 100, Games))),
   checklist( tennis_append_bar_groups(BC, Colour1, Colour2),
      Lengths ),
   send(W, open).

tennis_append_name_to_bar_groups(Dialog) :-
   send(Dialog, display, text('X: Length of Rallys'),
      point(10, 10)),
   send(Dialog, display, text('Y: Number of Rallys'),
      point(140, 10)),
   dislog_variables_get([
      tennis_statistics_name_a - NameA,
      tennis_statistics_name_b - NameB]),
   send(Dialog, display, new(Box, box(20, 20)), point(10, 30)),
   send(Box, colour(blue)),
   send(Box, fill_pattern, colour(blue)),
   send(Dialog, display, text(NameA), point(40,33)),
   send(Dialog, display, new(Box2, box(20, 20)), point(140, 30)),
   send(Box2, colour, red),
   send(Box2, fill_pattern, colour(red)),
   send(Dialog, display, text(NameB), point(170,33)).

tennis_append_bar_groups(Bar_Chart, Colour1, Colour2, [Label, A, B]) :-
   send(Bar_Chart, append, bar_group(Label,
      bar(y1, A, Colour1),
      bar(y2, B, Colour2))).
   
tennis_determine_y_max_coordinate2(Lengths, Max) :-
   maplist( tennis_n_th_element(2), 
      Lengths, List ),
   maximum(List, Max1),
   maplist( tennis_n_th_element(3), 
      Lengths, List2 ),
   maximum(List2, Max2),
   maximum([Max1, Max2], Max).
tennis_determine_y_max_coordinate2(_, 1).


tennis_bar_group_chart2(NA, NB, Errors) :-
   length(Errors, Number_of_Games),
   tennis_determine_y_max_coordinate2(Errors, Max),
   tennis_bar_group_chart2(NA, NB, Max, Number_of_Games, Errors).
   
tennis_bar_group_chart2(NameA, NameB, Max, Sets, Errors) :-
   new(Frame, frame('Unforced Errors each Set')),
   send(Frame, append,
      new(W, auto_sized_picture)),
   send(new(DialogColour, dialog), below, W),
   send(new(Dialog,dialog), below, DialogColour),
   tennis_append_name_to_bar_groups2(DialogColour, NameA, NameB),
   send(Dialog, append,
      new(button('Close', message(Frame, destroy)))),
   maximum([1, Max], Max2),
   send(W, display, new(BC, bar_chart(vertical, 0, Max2, 100, Sets))),
   checklist( tennis_append_bar_groups(BC, blue, red),
      Errors ),
   send(W, open).

tennis_append_name_to_bar_groups2(Dialog, NameA, NameB) :-
   send(Dialog, display, text('X: Number of Errors'),
      point(10, 10)),
   send(Dialog, display, text('Y: Sets'),
      point(160, 10)),  
   send(Dialog, display, new(Box, box(20, 20)), point(10, 30)),
   send(Box, colour, blue),
   send(Box, fill_pattern, colour(blue)),
   send(Dialog, display, text(NameA), point(40,33)),
   send(Dialog, display, new(Box2, box(20, 20)), point(140, 30)),
   send(Box2, colour, red),
   send(Box2, fill_pattern, colour(red)),
   send(Dialog, display, text(NameB), point(170,33)).
 

tennis_determine_service_point_location_display :-
   dislog_variable_get(tennis_service_location_a, As),
   dislog_variable_get(tennis_service_location_b, Bs),
   tennis_picture_uncolour,
   maplist( service_location_to_display_format_a,
      As, As_2 ),
   maplist( service_location_to_display_format_b,
      Bs, Bs_2 ),
%  checklist( tennis_hit_display_filled(blue),
%     Bs_2 ),
%  checklist( tennis_hit_display_filled(orange),
%     As_2 ).
   checklist( tennis_hit_display_filled(red),
      Bs_2 ),
   checklist( tennis_hit_display_empty(yellow),
      As_2 ).


service_location_to_display_format_a([X,Y], [XR, YR]) :-
   Y > 0,
   YA is -Y,
   XA is -X,
   dislog_variable_get(format, Format),
   tennis_convert_absolute_value_to_display_xy(
      Format, XA, YA, XR, YR).
service_location_to_display_format_a([XA, YA], [X,Y]) :-
   dislog_variable_get(format, Format),
   tennis_convert_absolute_value_to_display_xy(
      Format, XA, YA, X, Y).

service_location_to_display_format_b([X,Y], [XR, YR]) :-
   Y < 0,
   YA is -Y,
   XA is -X,
   dislog_variable_get(format, Format),
   tennis_convert_absolute_value_to_display_xy(
      Format, XA, YA, XR, YR).
service_location_to_display_format_b([XA, YA], [X,Y]) :-
   dislog_variable_get(format, Format),
   tennis_convert_absolute_value_to_display_xy(
      Format, XA, YA, X, Y).


/******************************************************************/


