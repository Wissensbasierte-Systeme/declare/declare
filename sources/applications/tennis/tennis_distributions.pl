

/******************************************************************/
/***                                                            ***/
/***        Tennis Tool: Distributions on Tiles                 ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_get(home, '/projects/Tennis', Tennis),
   dislog_variable_set(tennis, Tennis).


/*** tests ********************************************************/


test(tennis_table_to_distribution, Mode) :-
   File = 'examples/tennis/2002_sampras_agassi_final.xml',
   tennis_court_sizes(Width, Length),
   ( Mode = 1 ->
     N = [[0,0.25], [0.25,0.75], [0.75,1]],
     M = [[0,0.5], [0.5,1]]
   ; Mode = 2 ->
     N = [[0,0.25], [0.25,0.5], [0.5,0.75], [0.75,1]],
     M = [[0,0.231], [0.231, 0.5], [0.5, 0.769], [0.769,1]]
   ; Mode = 3 ->
     N = [[0,0.25], [0.25,0.5], [0.5,0.75], [0.75,1]],
     M = [[0,0.5], [0.5,1]] ),
   tennis_table_to_distribution(Width, Length, N, M, File).

test(tennis_tiles_to_sequences, Mode) :-
   File = 'examples/tennis/2002_sampras_agassi_final.xml',
   tennis_court_sizes(Width, Length),
   ( Mode = 1 -> N = 4, M = 2, Txy_1 = '1_1', Txy_2 = '4_2'
   ; Mode = 2 -> N = 4, M = 2, Txy_1 = '1_1', Txy_2 = '1_2'
   ; Mode = 3 -> N = 4, M = 2, Txy_1 = '4_2', Txy_2 = '4_1'
   ; Mode = 4 -> N = 4, M = 4, Txy_1 = '1_1', Txy_2 = '1_4' ),
   tennis_tiles_to_sequences(
      File, Width, Length, N, M, Txy_1, Txy_2).


/*** interface ****************************************************/


/* tennis_tiles_to_sequences(
         File, Width, Length, N, M, Txy_1, Txy_2) <-
      */

tennis_tiles_to_sequences(
      File, Width, Length, N, M, Txy_1, Txy_2) :-
   tennis_tiles_to_sequences(
      File, Width, Length, N, M, Txy_1, Txy_2, Sequences_1),
   tennis_tile_sequences_nicen(Sequences_1, Sequences_2),
   tennis_tiles_to_sequences_display(
      N, M, Txy_1, Txy_2, Sequences_2).
%  tennis_tiles_to_sequences_play(Sequences_2).

tennis_tile_sequences_nicen(Sequences_1, Sequences_2) :-
   tuples_atoms_to_numbers(Sequences_1, Sequences_A),
   sort(Sequences_A, Sequences_B),
   table_projection([1,2,3,6,7,4,5,8,9], Sequences_B, Sequences_2).


/* tennis_tiles_to_sequences_display(
         N, M, Txy_1, Txy_2, Sequences) <-
      */

tennis_tiles_to_sequences_display(
      N, M, Txy_1, Txy_2, Sequences) :-
   name_exchange_elements(["_,"], Txy_1, T1),
   name_exchange_elements(["_,"], Txy_2, T2),
   concat(['Tennis Table - (',
      T1, ') to (', T2, ') - Tesselation ', N, '*', M], Title),
   Attributes = ['  Set  ', 'Game', ' Point ', 'Score', 'Score',
      'Winner', ' Top ', 'Start', 'End'],
   xpce_display_table(Title, Attributes, Sequences),
   length(Sequences, K), write(K), writeln(' sequences').

tuples_atoms_to_numbers(Xs, Ys) :-
   maplist( tuple_atoms_to_numbers, Xs, Ys ).

tuple_atoms_to_numbers(Xs, Ys) :-
   maplist( atom_to_number_, Xs, Ys ).

atom_to_number_(Atom, Number) :-
   atomic(Atom),
   term_to_atom(Number, Atom),
   number(Number),
   !.
atom_to_number_(X, X).


/* tennis_tiles_to_sequences_play(Sequences) <-
      */

tennis_tiles_to_sequences_play(Sequences) :-
   forall( member([Set_, Game_, Point_,
         W, T, S_G, S_AB, Start, End], Sequences),
      ( maplist( term_to_atom,
           [Set, Game, Point], [Set_, Game_, Point_] ),
        write_list(['playing ', Set-Game-Point, ' ',
           W-T, ' ', S_G, ' ', S_AB, ' ', Start-End, '\n']),
%       Message =
%          playing:[Set-Game-Point]-[W-T]-[S_G,S_AB]-[Start,End],
%       writelnq(Message),
        tty_flash,
        tennis_video_play_sequence(Set, Game, Start, End) ) ).


/* tennis_tiles_to_sequences(
         File, Width, Length, N, M, Txy_1, Txy_2, Sequences) <-
      */

tennis_tiles_to_sequences(
      File, Width, Length, N, M, Txy_1, Txy_2, Sequences) :-
%  File to Table_Tennis
   dread(xml, File, [Xml]),
   tennis_xml_to_points(Width, Length, N, M, Xml, Points),
   tennis_xml_points_to_table(points:Points, Tuples_1),
   tennis_table_normalize(N, M, Tuples_1, Table_Tennis),
   xpce_display_table('Tennis Table - normalized', [], Table_Tennis),
%  Table_Tennis to Sequences
   ddbase_aggregate( [Set, Game, Point, W, T, Score_G, Score_AB],
      ( member(Tuple, Table_Tennis),
        members([set:Set, game:Game, point:Point], Tuple),
        members([winner:W, top:T], Tuple),
        members([score_G:Score_G, score_AB:Score_AB], Tuple),
        members(['Txy_1':Txy_1, 'Txy_2':Txy_2], Tuple) ),
      Tuples_2 ),
   sort(Tuples_2, Tuples_3),
   ddbase_aggregate( [Set, Game, Point, W, T,
         Score_G, Score_AB_Real, first(T1), last(T2)],
      ( member([Set, Game, Point, W, T, Score_G, Score_AB], Tuples_3),
        member(Tuple, Table_Tennis),
        members([set:Set, game:Game, point:Point], Tuple),
        members([time_1:T1, time_2:T2], Tuple),
        tennis_score_natural(Score_AB, Score_AB_Real) ),
      Sequences ),
%  Sequences to file
   tennis_tiles_to_sequences_to_file(
      Txy_1, Txy_2, Table_Tennis, Sequences).


/* tennis_tiles_to_sequences_to_file(
         Txy_1, Txy_2, Table_Tennis, Sequences) <-
      */

tennis_tiles_to_sequences_to_file(
      Txy_1, Txy_2, Table_Tennis, Sequences) :-
   Attributes = [ service, winner, top, error,
%     hit,
      'Ix_1', 'Ix_2', 'Iy_1', 'Iy_2',
      set, game, point, id, id_1, id_2, type_1, type_2, hand_1, hand_2,
%     time_1, time_2,
      score_A, score_B, score_AB, score_G ],
   ddbase_aggregate( [Set, Game, Point, W, T, S1, S2, T1, T2, list(Tuple)],
      ( member(Tuple_1, Sequences),
        member(Tuple_2, Table_Tennis),
        Tuple_1 = [Set, Game, Point, W, T, S1, S2, T1, T2],
        members([set:Set, game:Game, point:Point], Tuple_2),
        tennis_tuple_reduce(Attributes, Tuple_2, As),
        Tuple = hit:As:[] ),
      Tuples ),
   tennis_tiles_to_sequences_to_xml_points(Tuples, Points_Xml),
   concat(['tennis_tiles_to_hits_', Txy_1, '_to_', Txy_2, '.xml'], File),
   dislog_variable_get(output_path, File, Path),
   write_list(['---> ', Path]), nl,
   dwrite(xml, Path,
      points:['Txy_1':Txy_1, 'Txy_2':Txy_2]:Points_Xml).

tennis_tiles_to_sequences_to_xml_points(Tuples, Points_Xml) :-
   ( foreach(Tuple, Tuples), foreach(Point_Xml, Points_Xml) do
        ( Tuple = [Set, Game, Point, W, T, S1, S2, T1, T2, Xmls],
          Coordinates = [set:Set, game:Game, point:Point],
          Winner_Top = [winner:W, top:T],
          Scores = [score_G:S1, score_AB:S2],
          Times = [time_1:T1, time_2:T2],
          append([Coordinates, Winner_Top, Scores, Times], As),
          Point_Xml = point:As:Xmls ) ).


/* tennis_table_to_distribution(Width, Length, N, M, File) <-
      */

tennis_table_to_distribution(Width, Length, N, M, File) :-
   dread(xml, File, [Xml]),
   tennis_xml_to_points(Width, Length, N, M, Xml, Points),
   tennis_xml_points_to_table(points:Points, Table_1),
   tennis_table_normalize(N, M, Table_1, Table_2),
   tennis_table_reduce(Table_2, Table_3),
   tennis_table_to_distribution(N, M, Table_3, ab).


/* tennis_table_to_distribution(N, M, Table, Mode) <-
      */

tennis_table_to_distribution(N, M, Table, Mode) :-
%  tennis_table_to_distribution_1(Source, Table, Distribution_1),
   Attributes = ['Target', 'A=Sampras', 'B=Agassi'],
%  term_to_atom('Source'=Source, Title_1),
%  xpce_display_table(
%     Title_1, Attributes, Distribution_1),
   tennis_table_to_distribution_2(Table, Distribution_2),
   term_to_atom(N*M, Title_2),
   writeln(user, M*M),
   length(N, N_), N__ is N_ + 1,
   length(M, M_), M__ is M_ + 1,
   xpce_display_table(
      Title_2, ['Source'|Attributes], Distribution_2),
   writelnq_list(user, Distribution_2),
   findall( To,
      ( between(0, N__, X2),
        between(0, M__, Y2),
        concat(['(', X2, ',', Y2, ')'], To) ),
      Header_ ),
   concat_with_separator([' '|Header_], ':', Header),
   ( Mode = a ->
     dislog_variable_get(tennis, '/Data/aaa.csv', File),
     ddbase_aggregate( [From, list(A)],
        ( tennis_generate_from_to(N__, M__, From, To),
          ( member([From, To, A, B], Distribution_2) -> true
          ; A = 0, B = 0 ) ),
        Csv_1 )
   ; Mode = b ->
     dislog_variable_get(tennis, '/Data/bbb.csv', File),
     ddbase_aggregate( [From, list(B)],
        ( tennis_generate_from_to(N__, M__, From, To),
          ( member([From, To, A, B], Distribution_2) -> true
          ; A = 0, B = 0 ) ),
        Csv_1 )
   ; Mode = ab ->
     dislog_variable_get(tennis, '/Data/aaabbb.csv', File),
     ddbase_aggregate( [From, list(AB)],
        ( tennis_generate_from_to(N__, M__, From, To),
          ( member([From, To, A, B], Distribution_2) -> true
          ; A = 0, B = 0 ),
          AB is 1000*A + B ),
        Csv_1 ) ),
   maplist( tennis_concat_with_separator(':'), Csv_1, Csv_2 ),
   ddbase_aggregate( [I, sum(V)],
      ( member(Row, Csv_1),
        Row = [_, Values],
        ( nth(I, Values, V)
        ; length(Values, J), I is J+1,
          sum(Values, V) ) ),
      Sums_ ),
   pair_lists('[]', _, Sums, Sums_),
   concat_with_separator([' '|Sums], ':', Footer),
   append(Csv_2, [Footer], Csv_3),
   Csv_4 = [Header|Csv_3],
   writeln_list(File, Csv_4),
%  write(user, '---> '),
%  writeln(user, html_display_table),
%  html_display_table(
%     Title_2, ['Source'|Attributes], Distribution_2).
   tennis_distribution_select_remarkable(
      Distribution_2, Distribution_3),
   xpce_display_table(
      Title_2, ['Score', 'Source', 'Targets'],
      Distribution_3).

tennis_generate_from_to(N, M, From, To) :-
   between(0, N, X1),
   between(0, M, Y1),
   between(0, N, X2),
   between(0, M, Y2),
   concat(['(', X1, ',', Y1, ')'], From),
   concat(['(', X2, ',', Y2, ')'], To).

tennis_concat_with_separator(Separator, [X, Values], Row) :-
   sum(Values, V),
   append(Values, [V], Values_),
   concat_with_separator([X|Values_], Separator, Row).


/* tennis_table_to_distribution_1(Source, Table, Distribution) <-
      */

tennis_table_to_distribution_1(Source, Table, Distribution) :-
   tennis_table_to_distribution_aggregate(
      Source, 'A', Table, Table_A),
   tennis_table_to_distribution_aggregate(
      Source, 'B', Table, Table_B),
   tennis_table_to_tiles(Table, Tiles),
   ( foreach(Tile, Tiles), foreach(Tuple, Distribution) do
        ( ( member([Tile, N], Table_A) ; N = 0 ),
          ( member([Tile, M], Table_B) ; M = 0 ),
          ( 0 is N+M -> ( P1 = *, P2 = * )
          ; ( round(100*N/(N+M), 0, P1),
              round(100*M/(N+M), 0, P2) ) ),
          Tuple = [Tile, N=P1, M=P2] ) ).


/* tennis_table_to_distribution_2(Table, Distribution) <-
      */

tennis_table_to_distribution_2(Table, Distribution) :-
   tennis_table_to_distribution_aggregate(Table, Tuples),
   findall( [Source, Target, N, M],
      ( member([Source, Target, 'A', N], Tuples),
        member([Source, Target, 'B', M], Tuples) ),
      Tuples_AB ),
   findall( [Source, Target, N, 0],
      ( member([Source, Target, 'A', N], Tuples),
        \+ member([Source, Target, 'B', M], Tuples) ),
      Tuples_A ),
   findall( [Source, Target, 0, M],
      ( member([Source, Target, 'B', M], Tuples),
        \+ member([Source, Target, 'A', N], Tuples) ),
      Tuples_B ),
   append([Tuples_AB, Tuples_A, Tuples_B], Tuples_C),
   sort(Tuples_C, Distribution).
   

/* tennis_distribution_select_remarkable(
         Distribution_1, Distribution_2) <-
      */

tennis_distribution_select_remarkable(
      Distribution_1, Distribution_2) :-
   ddbase_aggregate( [max_(S), Source, reverse_sort_(X)],
      ( member([Source, Target, N, M], Distribution_1),
        N + M > 3,
        round((N-M)/(N+M), 2, Score),
        abs(Score) > 0.45,
        round(Score*10, 0, S),
        term_to_atom(T, Target),
        X = S-T-N:M ),
      Distribution_A ),
   sort(Distribution_A, Distribution_B),
   reverse(Distribution_B, Distribution_2).

max_(Xs, X) :-
   max(Xs, Max),
   min(Xs, Min),
   ( Max < 0 -> X = Min
   ; X = Max ).

reverse_sort_(Xs, Ys) :-
   sort(Xs, Us),
   ( ( first(Us, (V-_-_):_), V < 0 ) -> Vs = Us
   ; reverse(Us, Vs) ),
   maplist( term_to_atom, Vs, Ws),
   names_append_with_separator(Ws, '  ', Ys).


/* tennis_table_to_distribution_aggregate(+Table, -Tuples) <-
      */

tennis_table_to_distribution_aggregate(Table, Tuples) :-
   writelnq(tennis_table_to_distribution_aggregate),
   ddbase_aggregate( [S, T, Winner, length(Tuple)],
      ( member(Tuple, Table),
        Winner := Tuple/winner,
        Source := Tuple/'Txy_1',
        Target := Tuple/'Txy_2',
        name_split_at_position(["_"], Source, [Source_x,Source_y]),
        name_split_at_position(["_"], Target, [Target_x,Target_y]),
        term_to_atom(S_x, Source_x),
        term_to_atom(S_y, Source_y),
        term_to_atom(T_x, Target_x),
        term_to_atom(T_y, Target_y),
        concat(['(',S_x,',',S_y,')'], S),
        concat(['(',T_x,',',T_y,')'], T) ),
      Tuples ).


/* tennis_table_to_distribution_aggregate(
         +Source, +Winner, +Table, -Tuples) <-
      */

tennis_table_to_distribution_aggregate(
      Source, Winner, Table, Tuples) :-
   ddbase_aggregate( [Target, length(Tuple)],
      ( member(Tuple, Table),
        Winner := Tuple/winner,
        Source := Tuple/'Txy_1',
        Target := Tuple/'Txy_2' ),
      Tuples ).


/******************************************************************/


