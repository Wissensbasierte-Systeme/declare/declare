

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: GUI                                  ***/
/***                                                            ***/
/******************************************************************/


:- pce_autoload(finder, library(find_file)).
:- pce_global(@finder, new(finder)).


tennis_gui_text_font(font(screen, roman, 13)).

resource(X, image, Path) :-
   tennis_data_path(Source_Path),
   name_append([Source_Path, 'xpm/', X, '.xpm'], Path).


/*** interface ****************************************************/


/* tennis_tool <-
      */

tennis_tool :-
   tennis_load_preferences,
   new(Frame, frame('Tennis Database')),
   send(Frame, background, white),
   send(Frame, append, 
      new(Dialog_Menu_and_Tool_Bar, dialog)),
   rgb_to_colour([13.5, 13.5, 14], Colour),
%  Colour = colour(@default, 65535, 16384, 16384),
   rgb_to_colour([14, 14, 15.99], Colour_2),
%  dislog_variable_get(current_court_colour, Colour_2),
   send(Dialog_Menu_and_Tool_Bar, background, Colour),
   send(new(Dialog_File_Video, dialog),
      below, Dialog_Menu_and_Tool_Bar),
   send(Dialog_File_Video, background, Colour_2),
   send(new(Dialog_Score_Name, dialog),
      below, Dialog_File_Video),
   send(Dialog_Score_Name, background, Colour_2),
   send(new(Dialog_Group_Player, dialog),
      below, Dialog_Score_Name),
   send(Dialog_Group_Player, background, Colour_2),
   send(Dialog_Menu_and_Tool_Bar, append, 
      new(MB, menu_bar)),
   checklist( tennis_send_pulldown_menu(MB),
      [file, edit, format, preferences, statistics] ),
   tennis_send_toolbar(Dialog_Menu_and_Tool_Bar),
   tennis_send_score_name(Dialog_Score_Name),
   tennis_send_file_video(Dialog_File_Video),
   tennis_send_buttons(Dialog_Group_Player),
   dislog_variable_set(tennis_dialog, Frame),
   tennis_variable_start,
   send(Frame, open),
   tennis_picture_create_vertical.


/* tennis_picture_create_vertical <-
      */

tennis_picture_create_vertical :-
   new(Picture_Vertical, window('Tennis Court')),
   dislog_variable_set(tennis_picture, Picture_Vertical),
   dislog_variable_get(factor, Factor),
   tennis_picture_create_vertical(Picture_Vertical, Factor),
   send(Picture_Vertical, open).


/* tennis_send_pulldown_menu(MB, Type) <-
      */

tennis_send_pulldown_menu(MB, file) :-
   send_pulldown_menu(MB, file, [
      new - tennis_start_new_match,
      open - tennis_file_handling,
      'Save as' - tennis_file_handling,
      check - tennis_open_check_frame,
      quit - exit_programm ]).

tennis_send_pulldown_menu(MB, edit) :-
   send_pulldown_menu(MB, edit, [
      view - tennis_view_xml_file,
      undo - tennis_undo_save,
      reload - tennis_reload_display,
      'Visualize Ralley' - tennis_current_point_visualze,
      'Match Facts' - tennis_open_match_facts_dialog ]).
   
tennis_send_pulldown_menu(MB, format) :-
   send_pulldown_menu(MB, format, [
      horizontal - tennis_picture_horizontal,
      vertical - tennis_picture_vertical ]).

tennis_send_pulldown_menu(MB, preferences) :-
   send_pulldown_menu(MB, preferences, [
      'Court Size' - tennis_input_court_size,
      'Colour' - tennis_open_court_colouring_dialog,
      'Video Size' - tennis_input_video_size,
      'Modus' - tennis_change_modus,
      'Winning Parameter' - tennis_change_toolbar ]).

tennis_send_pulldown_menu(MB, statistics) :-
   send_pulldown_menu(MB, statistics, [
      'Info' - tennis_create_info_file,
      'Details' - tennis_xml_files_analyze,
      'Groundballs' - tennis_statistics_ground_balls_display,
      'Aces' - tennis_determine_service_point_location,
      'Returns' - tennis_file_to_returns(ground),
      'Serve & Volley' - tennis_file_to_returns(volley),
      'Volleys' - tennis_file_to_volleys_per_game,
      'Distributions' - tennis_ranges_to_bar_charts ]).
      

/* tennis_send_toolbar(Dialog) <-
      */

tennis_send_toolbar(Dialog) :-
   send(Dialog,append,new(TB,tool_bar)),
   send(TB,gap,size(10,0)),   
   tennis_toolbars(TB,[
      tennis_point_show_first - 'left_arrow.bm' - 
         'First',
      tennis_point_show_previous - 'ms_right_arrow.bm' - 
         'Previous',
      tennis_point_show_next - 'ms_left_arrow.bm' - 
         'Next',
      tennis_point_show_last - 'right_arrow.bm' - 
         'Last',
      video_play - resource(tennis_record) - 
         'Play',
      tennis_determine_smil_file_variable - resource(tennis_play) - 
         'Play Current Rally',
      tennis_display_swap_player_at_top - 'cycle.bm' - 
         'Swap Player',
      tennis_input_name_and_video - resource(tennis_input) - 
         'Input Video Time',
      tennis_picture_uncolour - resource(tennis_clear) - 
         'Clear',
      tennis_statistics_display - resource(tennis_statistic) - 
         'Statistics',
      tennis_open_search_pattern_dialog - resource(tennis_search) - 
         'Search' ]).


/* tennis_send_score_name(Dialog) <-
      */

tennis_send_score_name(Dialog) :-
   tennis_gui_text_font(Font),
   send(Dialog, append,
      new(Text_Item_1,text_item('  '))),
   send(Text_Item_1, length, 16),
   dislog_variable_set(input_name_player_A, Text_Item_1),   

   tennis_send_text_items([
      input_name_player_B - ' ' - below - 
         input_name_player_A - '16',
      input_game_player_B - 'Game' - right - 
         input_name_player_B - '3' - Font,  
      input_game_player_A - 'Game' - right - 
         input_name_player_A - '3' - Font,
      input_set_player_A - 'Set' - right - 
         input_game_player_A - '2' - Font,
      input_set_player_B - 'Set' - right - 
         input_game_player_B - '2' - Font,
      input_match_player_A - '' -  right - 
         input_set_player_A - '10' - Font - Off,
      input_match_player_B - '' -  right - 
         input_set_player_B - '10' - Font - Off]),
   send(Text_Item_1, label, '*'),
   dislog_variable_get(input_name_player_B, B),
   send(B, label, ' ').


tennis_display_server :-
   dislog_variables_get([
      current_server - A,
      current_player_at_top - Top]),
   tennis_display_server(A, Top).

tennis_display_server(X, Top) :-
   dislog_variables_get([
      input_name_player_A - Service_A,
      input_name_player_B - Service_B]),
   tennis_display_server(X, Top, Service_A, Service_B).

tennis_display_server('A', 'A', Ref_A, Ref_B) :- 
   send(Ref_A, label, '*'),
   send(Ref_B, label, ' ').
tennis_display_server('A', 'B', Ref_A, Ref_B) :- 
   send(Ref_A, label, ' '),
   send(Ref_B, label, '*').
tennis_display_server('B', 'A', Ref_A, Ref_B) :- 
   send(Ref_B, label, '*'),
   send(Ref_A, label, ' ').
tennis_display_server('B', 'B', Ref_A, Ref_B) :- 
   send(Ref_B, label, ' '),
   send(Ref_A, label, '*').
tennis_display_server(_, _, Ref_A, Ref_B) :- 
   send(Ref_A, label, ' '),
   send(Ref_B, label, ' ').
       

/* tennis_send_file_video(Dialog) <-
      */

tennis_send_file_video(Dialog) :-
   send(Dialog,append,
      new(Video_Name, text_item('Video File',''))), 
   send(Video_Name, length, 35),
   send(new(Video_Time, text_item('Time','')), 
      right, Video_Name),
   send(Video_Time,show_label,@off), 
   send(Video_Time, length, 9),
   send(new(Match_Time, text_item('Match Time','')),
      below, Video_Name),
   send(Match_Time, length, 9),
   dislog_variables_set([
      input_video_start_time - Video_Time,
      input_video_file - Video_Name,
      input_match_time - Match_Time]).


/* tennis_send_buttons(Dialog) <-
      */

tennis_send_buttons(Dialog) :-
   send(Dialog, append, new(DGA, dialog_group('Player A'))),
   send(new(DGB, dialog_group('Player B')), right, DGA),
   dislog_variables_set([
      dialog_group_player_A - DGA,
      dialog_group_player_B - DGB]),
   
   checklist( send_append(DGA), [
      'Service' - tennis_hit_assert(service, 'A'),
      'Game' - tennis_game_win('A'),
      'Point' - tennis_point_win('A') - below,
      'Set' - tennis_set_win('A'),
      'Error' - tennis_unforced_error_a - below,
      'Match' - tennis_match_win('A') ] ),
   
   checklist( send_append(DGB), [
      'Service' - tennis_hit_assert(service, 'B'),
      'Game' - tennis_game_win('B'),
      'Point' - tennis_point_win('B') - below,                
      'Set' - tennis_set_win('B'),
      'Error' - tennis_unforced_error_b - below,
      'Match' - tennis_match_win('B') ] ).


/* tennis_input_court_size <-
      */

tennis_input_court_size :-
   tennis_gui_text_font(Font),
   new(Frame,frame('Court size')),
   send(Frame, append, new(Dialog_Size, dialog)),
   send(new(Dialog_Size_Control,dialog), below, Dialog_Size),  
   send(Dialog_Size, append, new(Court_Size,text_item('Factor'))),
   send(Court_Size, length, 2),
   send(Court_Size,value_font,Font),
   send(Dialog_Size,append, new(Court_Side,text_item('Side'))),
   send(Court_Side,value_font,Font),
   send(Court_Side, length, 4),
   send(Dialog_Size,append, new(Court_Behind,text_item('Behind'))),
   send(Court_Behind,value_font,Font),
   send(Court_Behind, length, 4),
   
   checklist( send_append(Dialog_Size_Control), [
      'Default' - tennis_change_court_size_default,
      'Cancel' -  tennis_destroy(Frame),   
      'OK' - tennis_change_court_size - below]),
  
   dislog_variables_set([
      change_court_size_frame - Frame,
      input_change_court_size - Court_Size,
      input_change_court_side - Court_Side,
      input_change_court_behind - Court_Behind]),
   tennis_display_options_size,
   send(Frame,open).



/* tennis_input_video_size <-
      */

tennis_input_video_size :-
   tennis_gui_text_font(Font),
   new(Frame,frame('Court size')),
   send(Frame, append, new(Dialog_Size, dialog)),
   send(new(Dialog_Size_Control,dialog), below, Dialog_Size),  
   send(Dialog_Size, append, new(Video_Width,text_item('Width'))),
   send(Video_Width, length, 3),
   send(Video_Width,value_font,Font),
   send(Dialog_Size,append, new(Video_Height,text_item('Heigth'))),
   send(Video_Height,value_font,Font),
   send(Video_Height, length, 3),

   checklist( send_append(Dialog_Size_Control), [
      'Default' - tennis_change_video_size_default,
      'Cancel' -  tennis_destroy(Frame),   
      'OK' - tennis_change_video_size - below]),
  
   dislog_variables_set([
      change_video_size_frame - Frame,
      input_change_video_width - Video_Width,
      input_change_video_height - Video_Height]),
   tennis_display_options_video,
   send(Frame,open).


/* tennis_open_check_frame <-
      */

tennis_open_check_frame :-
   new(Frame, frame('Value Check')),
   send(Frame, append, new(Dialog_Values, dialog)),
   send(new(Dialog_Control, dialog), below, Dialog_Values),
   send(Dialog_Control,append,  
      button('OK', message(Frame,destroy))),
   tennis_check(Set, Game, Rally, Result),
   tennis_gui_text_font(Font),
   tennis_send_texts(Dialog_Values, Font, [
      'Number of Sets:      ' - Set - left,
      'Number of Games:     ' - Game - left,  
      'Number of Rallys:    ' - Rally - left,
      ' ' - center,
      '' - Result - left]),
   send(Frame, open).


/* tennis_open_match_facts_dialog <-
      */

tennis_open_match_facts_dialog :-
   new(Frame,frame('Match Facts')),
   send(Frame, append, new(Dialog_Facts,dialog)),
   send(new(Dialog_Facts_Control,dialog),below,Dialog_Facts), 
   send(Dialog_Facts,append,new(PlayerA, text_item('Player A:'))),
   send(PlayerA, length, 25),
   send(new(PlayerB, text_item('Player B:')),below, PlayerA),
   send(PlayerB, length, 25),  
   send(new(Tournament,text_item('Tournament:')), below, PlayerB),
   send(Tournament, length, 25),
   send(new(Surface, text_item('Surface:')),below,Tournament),
   send(Surface, length, 25),
   send(new(Where,text_item('Location:')),below,Surface),
   send(Where, length, 25),
   send(new(When,text_item('Time:')),below,Where),
   send(When, length, 25),
   send(new(Round,text_item('Round:')),below,When),
   send(Round, length, 25),

   send(Dialog_Facts_Control, append,
   button('OK',message(@prolog,tennis_match_facts_get_display_values,
        PlayerA,PlayerB,Tournament,Surface,Where,When,Round,Frame))),
   send(Dialog_Facts_Control,append,  
   button('Cancel', message(Frame,destroy))),
   tennis_match_facts_display_values(PlayerA, PlayerB,
      Tournament, Surface, Where, When, Round),
   dislog_variable_set(input_match_facts_tournament,Tournament),
   send(Frame,open).



/* tennis_open_search_pattern_dialog <-
      */

tennis_open_search_pattern_dialog :-
   new(Frame,frame('Search Pattern')),
   send(Frame, append, new(Dialog_File_Control, dialog)),
   send(new(Dialog_Var,dialog), below, Dialog_File_Control),
   send(new(Dialog_Facts_Control,dialog),below,Dialog_Var), 
   send(Dialog_File_Control, append,
        new(Browser, browser(@default, size(40,3)))),
   send(Dialog_File_Control, append,
      new(button('Add', message(@prolog, tennis_add_file_browser, Browser)))),
   tennis_display_search_file_browser(Browser),

   send(Dialog_Var, append,
      new(Distance,text_item('Distance in Meters:'))),
   send(Distance, length, 5),
   send(Distance, value, 1),

   send(Dialog_Facts_Control,append,
      button('Search', 
         message(@prolog,tennis_search_pattern, 'A', Distance, Frame))),
   send(Dialog_Facts_Control,append,
      button('Search-Winner', 
         message(@prolog,tennis_search_pattern, 'B', Distance, Frame))),
   send(Dialog_Facts_Control,append,  
      button('Cancel', message(
       @prolog, tennis_cancel_search_pattern_dialog, Frame))),
   send(Frame,open). 


/* tennis_add_file_browser(Browser) <-
      */

tennis_add_file_browser(Browser) :-
   tennis_load_file(Path),
   dislog_variable_get(tennis_to_search_files, Files),
   New = [Path|Files],
   dislog_variable_set(tennis_to_search_files, New),
   tennis_display_search_file_browser(Browser, Path).



tennis_display_search_file_browser(Browser) :-
   dislog_variable_get(tennis_to_search_files, Files),
   checklist( tennis_display_search_file_browser(Browser),
      Files ).

tennis_display_search_file_browser(Browser, File):- 
   send(Browser, append, File).

     
tennis_match_facts_display_values(PA, PB, Tournament, 
      Surface, Where, When,Round) :-
   tennis_match_facts_get_values(Tournament_Value, 
      Surface_Value, Where_Value, When_Value, Round_Value),
   dislog_variables_get([
      name_player_A - NameA,
      name_player_B - NameB]),      
   tennis_send_selection([PA,PB,Tournament, Surface, Where, When, Round],
      [NameA, NameB, Tournament_Value, Surface_Value, Where_Value,
       When_Value,Round_Value]).


tennis_match_facts_get_values(Tournament,Surface, Where, When, Round) :-
   dislog_variable_get(tennis_current_output_file,File),
   tennis_xml_file_to_fn_term(File,FN_Term),
   T_Term := FN_Term^match^match_facts^tournament,
   S_Term := FN_Term^match^match_facts^surface, 
   Where_Term := FN_Term^match^match_facts^where, 
   When_Term := FN_Term^match^match_facts^when, 
   R_Term := FN_Term^match^match_facts^round,
   maplist(element_to_list,[Tournament,Surface,Where,When,Round],
      [T_Term, S_Term, Where_Term, When_Term, R_Term]).

tennis_match_facts_get_display_values(
      PlayerA, PlayerB, Tournament,Surface,Where,When,Round,Frame) :-
   tennis_get_selection([PlayerA, PlayerB, Tournament,Surface,Where,
      When,Round,Frame],[NameA, NameB, Tournament_Value,Surface_Value,
      Where_Value,When_Value,Round_Value]),
   dislog_variables_set([
      name_player_A - NameA,
      name_player_B - NameB]), 
   send(Frame,destroy),
   tennis_save_player_name(NameA, NameB),
   tennis_display_player_names,
   tennis_save_match_facts(Tournament_Value,Surface_Value,Where_Value,
      When_Value,Round_Value).
   
tennis_open_court_colouring_dialog :-
   new(Frame,frame('Court Colour')),
   send(Frame,append,new(Dialog_Colour_Choice,dialog)),
   send(new(Dialog_Colour_Display,dialog),below,Dialog_Colour_Choice),    
   send(new(Dialog_Colour_Control,dialog),below,Dialog_Colour_Display),
   send(Dialog_Colour_Display,background,white),
   dislog_variables_set([
      colour_test_display_dialog - Dialog_Colour_Display,
      colour_test(red) - 0,
      colour_test(green) - 0,
      colour_test(blue) - 0 ]),
   send(Dialog_Colour_Choice, append,
      new(M, menu(colour, choice, and(
         message(Dialog_Colour_Display, background,@arg1),
         message(@prolog,tennis_colour_test_block,@arg1?name))))),
   send(M,layout, horizontal),
   forall(tennis_court_colour(C), append_colour(M,C)),
   send(Dialog_Colour_Choice, append, slider(red, 0, 256, 0,
      message(@prolog,tennis_colour_test_slider, red, @arg1))),
   send(Dialog_Colour_Choice, append, slider(green, 0, 256, 0,
      message(@prolog, tennis_colour_test_slider, green, @arg1))),
   send(Dialog_Colour_Choice, append, slider(blue, 0, 256, 0,
      message(@prolog,tennis_colour_test_slider, blue, @arg1))),
   
   send(Dialog_Colour_Control, append,
      button('Default', message(@prolog, 
         tennis_change_court_colour_default))),
   send(Dialog_Colour_Control, append,  
      button('Cancel', message(Frame,destroy))),
   send(Dialog_Colour_Control, append,
      button('OK',message(@prolog,tennis_change_court_colour))),
   
   dislog_variable_set(court_colour_test_frame,Frame),
   send(Frame,open).


tennis_display_server_2(N, Top) :-
   dislog_variable_get(all_server, Servers),
   n_th_element(Servers, N, Server),
   tennis_display_server(Server, Top).


tennis_display_player_names :-
   dislog_variables_get([
      input_name_player_A - Ref_Name_Player_A,
      input_name_player_B - Ref_Name_Player_B, 
      name_player_A - Name_Player_A,
      name_player_B - Name_Player_B,
      current_player_at_top - Top]),
   tennis_send_label_group(Name_Player_A,Name_Player_B),
   tennis_display_player_names2(Ref_Name_Player_A,Ref_Name_Player_B,
      Name_Player_A,Name_Player_B,Top).

tennis_display_player_names(Top) :-
   dislog_variables_get([
      input_name_player_A - Ref_Name_Player_A,
      input_name_player_B - Ref_Name_Player_B, 
      name_player_A - Name_Player_A,
      name_player_B - Name_Player_B]),
   tennis_send_label_group(Name_Player_A,Name_Player_B),
   tennis_display_player_names2(Ref_Name_Player_A,Ref_Name_Player_B,
      Name_Player_A,Name_Player_B,Top).

tennis_display_player_names2(Ref_Input_A, Ref_Input_B,
      Name_A, Name_B, 'A') :-
   tennis_send_selection([Ref_Input_A,Ref_Input_B],[Name_A,Name_B]).
tennis_display_player_names2(Ref_Input_A, Ref_Input_B,
      Name_A, Name_B, 'B') :-
   tennis_send_selection([Ref_Input_A,Ref_Input_B],[Name_B,Name_A]).


tennis_display_times(Set_Time, Match_Time) :-
   dislog_variables_get([ 
      input_video_start_time - Video,
      input_match_time - Match]),
   send(Video,selection, Set_Time),
   send(Match,selection, Match_Time).
tennis_display_times(_, _).


tennis_scroll_display_video_time(N) :-
   dislog_variables_get([
      input_video_start_time - Ref_Videotime,
      tennis_starttime_point - Times]),
   n_th_element(Times, N, Time),
   send(Ref_Videotime, selection, Time).

tennis_scroll_display_match_time(N) :-
   dislog_variable_get(input_match_time, Ref_Matchtime),
   tennis_determine_match_time_of_current_point(N, Time),
   send(Ref_Matchtime, selection, Time).
tennis_scroll_display_match_time(_).

tennis_determine_match_time_of_current_point(N, Time_Display) :-
   dislog_variables_get([
      tennis_starttime_point - Time_Points,
      tennis_number_time_rally - Number]),
   kum_list_time(Number, T),
   maplist( triple_to_time,
      T, Time ),
   tennis_settime_for_current_point(N, Time, [_, Time_2]),
   n_th_element(Time_Points, N, Time_Point),
   tennis_colon_structure_to_time(Time_Point, List),
   time_addition(List, Time_2, Result_1),
   maplist( term_to_atom,
      Result, Result_1 ),
   maplist( tennis_number_to_two_digits,
      Result, Result_2 ),
   tennis_list_to_time_format(Result_2, Time_Display).


tennis_display_score_game :-
   dislog_variables_get([
      point_score('A') - Score_A,
      point_score('B') - Score_B, 
      current_player_at_top - Top]),
   tennis_display_score_game_2(Score_A,Score_B,Top).

tennis_display_score_game_2(Ga_Sc_A,Ga_Sc_B,Top) :-
   dislog_variables_get([
      input_game_player_A - Ref_Input_A,
      input_game_player_B - Ref_Input_B]),
   tennis_score_determine(Ga_Sc_A,Ga_Sc_B,Score_A,Score_B),
   tennis_display_score_game3(Ref_Input_A,Ref_Input_B,Score_A,
      Score_B,Top).

tennis_display_score_game3(Ref_Input_A,Ref_Input_B,
      Score_A, Score_B, 'A') :-
   tennis_send_selection([Ref_Input_A,Ref_Input_B],
      [Score_A,Score_B]).
tennis_display_score_game3(Ref_Input_A,Ref_Input_B, 
      Score_A, Score_B, 'B') :-
   tennis_send_selection([Ref_Input_A,Ref_Input_B],
      [Score_B,Score_A]).


tennis_display_score_set :-
   dislog_variables_get([
      game_score('A') - Score_A,
      game_score('B') - Score_B,
      current_player_at_top - Top]),
   tennis_display_score_set_2(Score_A, Score_B, Top).

tennis_display_score_set_2(Score_A, Score_B, Top) :-
   dislog_variables_get([
      input_set_player_A - Ref_Input_A,
      input_set_player_B - Ref_Input_B]),
   tennis_display_score_set3(Ref_Input_A,Ref_Input_B,
      Score_A,Score_B,Top).

tennis_display_score_set3(Ref_Input_A, Ref_Input_B,
      Score_A, Score_B, 'A') :-
   tennis_send_selection([Ref_Input_A, Ref_Input_B],
      [Score_A, Score_B]).
tennis_display_score_set3(Ref_Input_A, Ref_Input_B, 
      Score_A, Score_B, 'B') :-
   tennis_send_selection([Ref_Input_A, Ref_Input_B],
      [Score_B, Score_A]).


tennis_display_score_match_prepare([]) :-
   tennis_display_score_match_2(' ',' ','A').

tennis_display_score_match_prepare(Ss) :-
   ( ( maplist( tennis_nth_element(1),
          Ss, Score_As ),
       score_match_to_display_format(Score_As, ScoreAs),
       maplist( tennis_nth_element(2),
          Ss, Score_Bs ) )
   ; ( n_th_element(Ss, 1, Score_A),
       element_to_list(Score_A, Score_As),
       n_th_element(Ss, 2, Score_B),
       element_to_list(Score_B, Score_Bs) ) ),
   score_match_to_display_format(Score_As, ScoreAs),
   score_match_to_display_format(Score_Bs, ScoreBs),
   tennis_display_score_match_2(ScoreAs, ScoreBs, 'A').


tennis_display_score_match :-
   tennis_determine_score_match,
   dislog_variables_get([
      match_score_A - Score_A,
      match_score_B - Score_B,
      current_player_at_top - Top]),
   tennis_display_score_match_2(Score_A, Score_B, Top).

tennis_display_score_match_2(Score_A, Score_B, Top) :-
   dislog_variables_get([
      input_match_player_A - Ref_Input_A,
      input_match_player_B - Ref_Input_B]),
   tennis_display_score_match_3(Ref_Input_A,Ref_Input_B,Score_A,
      Score_B,Top).

tennis_display_score_match_3(Ref_Input_A,Ref_Input_B,
      Score_A,Score_B,'A') :-
   tennis_send_selection([Ref_Input_A,Ref_Input_B],
      [Score_A,Score_B]).
tennis_display_score_match_3(Ref_Input_A,Ref_Input_B,
      Score_A,Score_B,'B') :-
   tennis_send_selection([Ref_Input_A,Ref_Input_B],
      [Score_B,Score_A]).

tennis_input_name_and_video :-
   dislog_variable_get(input_video_start_time, IVST),
   get(IVST, selection, Input_Video_Start_Time),
   tennis_video_start_time(Input_Video_Start_Time).

tennis_send_label_group(Name_A, Name_B) :-
   dislog_variables_get([
      dialog_group_player_A - DGA,
      dialog_group_player_B - DGB]),
   send(DGA, label, Name_A),
   send(DGB, label, Name_B).

tennis_display_output_file_name :-
   dislog_variables_get([
      tennis_current_output_file - Path,
      tennis_dialog - Frame]),
   tennis_data_path(Data_Path),
   name_append(Data_Path, File, Path),
   send(Frame, label(File)).
tennis_display_output_file_name.


tennis_display_video_file_name :-
   dislog_variables_get([
      current_video_file - File,
      input_video_file - Ref_Video_File_Name]),
   tennis_path_to_filename(File,Video),
   send(Ref_Video_File_Name, selection, Video).
tennis_display_video_file_name.


tennis_display_options_size :-
   tennis_send_selection_dislog_variables([
      input_change_court_size - factor,
      input_change_court_side - tennis_court_side,
      input_change_court_behind - tennis_court_behind]).

tennis_display_options_video :-
   tennis_send_selection_dislog_variables([
      input_change_video_width - tennis_video_width,
      input_change_video_height - tennis_video_height]).
   
tennis_display_start_match :-
   tennis_display_player_names,
   tennis_display_score_match,
   tennis_display_score_game,
   tennis_display_score_set,
   tennis_display_video_file_name.
   
tennis_display_swap_player_at_top :-
   dislog_variable_get(current_player_at_top, Top),
   ( ( Top == 'A',
       dislog_variable_set(current_player_at_top, 'B') )
   ; dislog_variable_set(current_player_at_top, 'A') ),
   tennis_display_player_names,
   tennis_display_server,   
   tennis_display_score_game,
   tennis_display_score_set,
   tennis_display_score_match.

tennis_point_show_score_display(N) :-
   dislog_variable_get(tennis_score_for_and_backward,
      Set_Game_Point_Tops),
   n_th_element(Set_Game_Point_Tops, N,
      [S,[G_A,G_B],[P_A,P_B],[Top]]),
   tennis_terms_to_atoms([
      Point_A - P_A,
      Point_B - P_B]),
   tennis_display_player_names(Top),
   tennis_display_server_2(N, Top),
   tennis_display_score_game_2(Point_A, Point_B, Top),
   tennis_display_score_set_2(G_A,G_B, Top),
   tennis_display_score_match_prepare(S).
tennis_point_show_score_display(_).

tennis_reload_display :-
   tennis_picture_uncolour,
   dislog_variable_get(tennis_current_output_file, File),
   tennis_variable_start(File). 

tennis_change_modus :-
   dislog_variable_get(tennis_modus, Modus_Old),
   tennis_swap_modus(Modus_Old, Modus),
   dislog_variable_set(tennis_modus, Modus),
   court_format_1,
   tennis_change_hit_reload.


tennis_change_toolbar :-
   tennis_picture_uncolour,
   dislog_variable_get(show_rally_or_hit, Rally_or_Hit),
   tennis_swap_modus(Rally_or_Hit, New),
   dislog_variable_set(show_rally_or_hit, New),
   tennis_change_toolbar(New).

tennis_change_toolbar(0) :-
   create_parameter_dialog,
   tennis_number_of_hits_display,
   tennis_point_show_first(0).

tennis_change_toolbar(1) :-
   destroy_parameter_dialog,
   dislog_variable_get(current_point, N),
   tennis_point_show(N).  

tennis_number_of_hits_display :-
   dislog_variable_get(current_point, N),
   dislog_variable_get(list_of_points_with_hits_display, Rallys),
   n_th_element(Rallys, N, Rally),
   length(Rally, Length),
   dislog_variable_set(number_of_hits_display, Length).

destroy_parameter_dialog :-
   dislog_variable_get(tennis_parameter_frame, Frame),
   send(Frame, destroy).
destroy_parameter_dialog.

create_parameter_dialog :-
   new(Frame, frame('Winning Chance')),
   send(Frame, append,
      new(Dialog, dialog)),
   send(new(Dialog_Winner, dialog), below, Dialog),
   dislog_variables_get([
      name_player_A - PlayerA,
      name_player_B - PlayerB]),
   send(Dialog, display,
      new(text(PlayerA)), point(50,10)),
   send(Dialog, display,   
      new(text(PlayerB)), point(170, 10)),
   display_parameter_box_empty(Dialog),
   dislog_variable_set(tennis_parameter_dialog, Dialog),
   dislog_variable_set(tennis_parameter_frame, Frame),
   send(Dialog_Winner, display,
      new(T, text('Winner: ')), point(50,10)),
   dislog_variable_set(tennis_parameter_winner_text, T),
   dislog_variable_set(tennis_parameter_winner_dialog, Dialog_Winner),
   send(Dialog, open).

display_parameter_box_empty(Dialog) :-
   A = [10,30,50,70,90,110,130,150,170,190,210, 230],
   checklist(display_parameters(Dialog), A), 
   display_parameter_box_line(Dialog).   

display_parameter_box_line(Dialog) :-
   send(Dialog, display,
      new(Line, line(130, 30, 130, 90))), 
   send(Line, pen, 2).

display_parameters(Dialog, X) :-
   send(Dialog, display, new(Box, box(20, 40)), point(X, 40)),
   send(Box, fill_pattern, colour(grey)).

display_parameter_fill_box(Dialog, X) :-
   send(Dialog, display, new(Box, box(20, 40)), point(X, 40)),
   send(Box, fill_pattern, colour(blue)).

display_parameter_box_winner(N) :-
   dislog_variable_get(tennis_winners_of_rallys, Winners),
   n_th_element(Winners, N, Winner),
   dislog_variable_get(tennis_parameter_winner_text, T),
   name_append(['Winner: ', Winner], Display),  
   send(T, value, Display).
   
tennis_swap_modus('Change', 'Input').
tennis_swap_modus('Input', 'Change').
tennis_swap_modus(0, 1).
tennis_swap_modus(1, 0).


/******************************************************************/


