

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Visualization                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tennis_hit_visualze(Colour, N, [X1, Y1, T1], [X2, Y2, T2]) <-
      */

tennis_hit_visualze(Colour, N, [X1, Y1, T1], [X2, Y2, T2]) :-
   T is 400 * (T2-T1)/N,
   ( for(I, 0, N) do
        X is X1 + (X2-X1)*I/N,
        Y is Y1 + (Y2-Y1)*I/N,
        tennis_ball_show(Colour, [X, Y], Circle),
        send(Circle, flush),
        wait_milli_seconds(T),
        send(Circle, destroy) ).


/* tennis_current_point_visualze <-
      */

tennis_current_point_visualze :-
   dislog_variable_get(list_of_points_with_hits_display, Points),
   dislog_variable_get(current_point, N),
   nth(N, Points, Point),
   maplist( tennis_hit_transform,
      Point, Hits ),
   tennis_current_point_visualze(Hits),
   !.

tennis_hit_transform(Hit_1, Hit_2) :-
   Hit_1 = [X1, Y1, [Hand, _], _, T1],
   atom_number(X1, X2),
   atom_number(Y1, Y2),
   tennis_colon_structure_to_time(T1, [H,M,S]),
   T2 is H * 3600 + M * 60 + S,
   ( Hand = forehand,
     Colour = yellow
   ; Colour = red ),
   Hit_2 = [Colour, [X2, Y2, T2]],
   !.

tennis_current_point_visualze([Hit_1, Hit_2|Hits]) :-
   Hit_1 = [Colour, P1],
   Hit_2 = [_, P2],
%  thread_create(tennis_hit_bong, _, [detached(true)]),
   tennis_hit_bong,
   tennis_hit_visualze(Colour, 10, P1, P2),
   tennis_current_point_visualze([Hit_2|Hits]).
tennis_current_point_visualze(_).


/* tennis_hit_bong <-
      */

tennis_hit_bong :-
   true.
tennis_hit_bong :-
   current_prolog_flag(unix, true),
   home_directory(Home),
   concat(['cat ', Home, '/diverse/Sound/sounds/bong.au ',
      '> /dev/audio'], Command),
   us(Command),
   !.


/******************************************************************/


