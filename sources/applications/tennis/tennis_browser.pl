

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Browser                              ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library(url)).
:- use_module(cms(cms_doc_browser)).


/*** link_handler *************************************************/


tennis_show_file(Arg_Atom) :-
%  term_to_atom(Arg1:Arg2:Arg3:Arg4:Arg5, Arg_Atom),
   concat_atom([Arg1, Arg2, Arg3, Arg4, Arg5], ':', Arg_Atom),
   concat_atom([Arg1, ':', Arg2, ':', Arg3], Time),
   concat_atom([Arg4, '.', Arg5], File),
   tennis_search_pattern_view_message(Time, File).

tennis_show_file(Arg_Atom) :-
%  term_to_atom(N:FileT:Ext, Arg_Atom),
   tennis_data_path(Path),
   concat_atom([N, FileT, Ext], ':', Arg_Atom),
   concat_atom([Path, FileT, '.', Ext], File),
   tennis_determine_hits(File),
   tennis_scroll_determine_score(File),
   term_to_atom(NT, N),
   tennis_point_show(NT).


/*** interface ****************************************************/


tennis_start_browser(File, Title) :-
   tennis_picture_uncolour,
   tennis_clear_search_pattern_files,
   tennis_browser_message_to_file_start,
   concat('file://', File, Path),
   new(Dialog, dialog(Title)),
   send(Dialog, append,
      new(CDW, cms_doc_window(Path))),
   send(CDW, link_handler, tennis_show_file),
%  send(Dialog, append,
%      button(ok, message(Dialog, destroy))),
   send(Dialog, open).
tennis_start_browser(_).   


/* tennis_search_pattern_display_browser(ResultR, Videos) <-
      browser from output of results of search. */

tennis_search_pattern_display_browser([Result, Videos, FilePath]) :-
%  write_list([
%     '<h3><font color="#8698fd">Data File</font></h3>',
%     '<blockquote>', FilePath, '</blockquote>']), nl,
%  writeln('<h3><font color="#8698fd">Breakpoints</font></h3>'),
   maplist( tennis_browser_key(Videos),
      Result, Key ),
   maplist( tennis_browser_message,
      Result, Display ),
   tennis_path_to_filename_colon(FilePath, FileName),
   send_browser_content(Key, Display, FileName).
tennis_search_pattern_display_browser(_).


/*** implementation ***********************************************/

   
/* tennis_browser_key(Videos, Search_Result, Key) <-
      */

tennis_browser_key(Videos, [[S,_,_],_, Time, _], TiFi) :-
   term_to_atom(ST, S),
   n_th_element(Videos, ST, FileX),
   tennis_split_videofile(FileX, FileX1),
   concat(Time, ':', Time2),
   concat(Time2, FileX1, TiFi1),
   term_to_atom(TiFi1, TiFi).
tennis_browser_key(_,_,_).

tennis_split_videofile(File, FileName) :-
   name(File, FileN),
   list_split_at_position(["."], FileN, FileNs),
   first(FileNs, File1),
   last_element(FileNs, File2),
   name(File11, File1),
   name(File22, File2),
   concat_atom([File11, ':', File22], FileName).  
tennis_split_videofile(_,_).


/* tennis_browser_message(Search_Result, Display_Row) <-
      */

tennis_browser_message([[S, G, P], Score, Time, Nth],
      [ID, Score_Display, Time, Winner, Nth]) :-
   tennis_browser_message_score(Score, Score_Display, Winner),
%  concat(['Set:', S, ' Game:', G, ' Point:', P], ID).
   concat(['S:', S, '-G:', G, '-P:', P], ID).
tennis_browser_message(_, _).


tennis_browser_message_score([Gt,Pt,Wt], Score, Winner) :-
   term_to_atom(Gt,G),
   tennis_browser_message_score_one_set(
      [], G, Pt, Wt, Score, Winner).

tennis_browser_message_score([St,Gt,Pt,Wt],Score,Winner) :- 
   tennis_terms_to_atoms([
      St - S,
      Gt - G]),
   tennis_browser_message_score_one_set(S,G,Pt,Wt,Score,Winner).

tennis_browser_message_score_one_set(S,G,P, W, Score, Winner) :-   
%  ( ( S == [],
%      concat(['Game: ', G, ' Point: ', P], Score) )
%    ; concat(['Set: ', S, ' Game: ', G, ' Point: ', P], Score) ),
   ( ( S == [],
       concat_atom([G, P], '-', Score) )
     ; concat_atom([S, G, P], '-', Score) ),
%  concat(['Winner: ', W], Winner).
   Winner = W.


/* send_browser_content(Browser, Keys, Strings) <-
      */

send_browser_content(Keys, Strings, File) :-
   pair_lists(Keys, Strings, Pairs),
   writeln('<table border="1" cellspacing="1" BGCOLOR="#eeeeee">'),
   Headline = tr:[
      th:['BGCOLOR':'#dfdfff']:[b:'Set-Game-Point'],
      th:['BGCOLOR':'#dfdfff']:[b:'Score'],
      th:['BGCOLOR':'#dfdfff']:[b:'Time'],
      th:['BGCOLOR':'#dfdfff']:[b:'Winner'] ],
   dwrite(xml, Headline),
%  write_list(['<colgroup width="', 1000,
%     '" span="', 4, '"/>']), nl,
   checklist( send_browser_content(File),
      Pairs ),
   writeln('</table>').
send_browser_content(_, _, _).

send_browser_content(File, [Key, [ID, Score, Time, Winner, Nth]]) :-
   term_to_atom(Key_1, Key),
   concat('prolog:', Key_1, Prolog_Key_1),
   term_to_atom(Prolog_Key_1, Prolog_Key),
   concat_atom(['prolog:', Nth, ':', File], N_File),
   term_to_atom(N_File, N_File_Atom),
   write_list(['<tr><td><a href=', N_File_Atom, '>', ID, '</a></td>',
      '<td>', Score, '</td>',
      '<td><a href=', Prolog_Key, '>', Time, '</a></td>',
      '<td>', Winner, '</td></tr>']), nl.
send_browser_content(_, _).


/* tennis_search_pattern_view_message(TimeVideo) <-
      */

tennis_search_pattern_view_message(Time, File) :-
   tennis_create_smil_file(File, Time, '2:0:0',
      'Show Current Search Pattern').


/******************************************************************/


