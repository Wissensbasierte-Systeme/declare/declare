

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Elementary                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tennis_xml_file_to_fn_term(File, DTD, FN_Term) <-
      */

tennis_xml_file_to_fn_term(File, FN_Term) :-
   Options = [
      dialect(xml), max_errors(5000),
      space(sgml), space(remove) ],
   xml_file_to_fn_term(File, FN_Term, Options).

tennis_xml_file_to_fn_term(File, DTD, FN_Term) :-
   Options = [
      dialect(xml), max_errors(5000), dtd(DTD),
      space(sgml), space(remove) ],
   xml_file_to_fn_term(File, FN_Term, Options).


/* tennis_fn_term_to_xml_file(FN_Term, File) <-
      */

tennis_fn_term_to_xml_file(FN_Term, File) :-
   predicate_to_file( File,
      ( writeln('<?xml version=''1.0'' encoding=''ISO-8859-1'' ?>'),
        nl,
        tennis_field_notation_to_xml(FN_Term) ) ).

tennis_field_notation_to_xml(Object) :-
   is_list(Object),
   !,
   checklist( field_notation_to_xml,
      Object ).


pair_lists_of_lists(
      [[X|Xs]|X2s],[[Y|Ys]|Y2s],[[[X,Y]|XYs]|X2sY2s]) :-
   pair_lists([X|Xs],[Y|Ys],[[X,Y]|XYs]),
   pair_lists_of_lists(X2s,Y2s,X2sY2s).
pair_lists_of_lists(_,_,[]).


tennis_pair_lists([X|Xs],[Y|Ys],[Z|XYs]) :-
   tennis_add_element(X,Y,Z),
   tennis_pair_lists(Xs,Ys,XYs).
tennis_pair_lists([],_,[]).

tennis_pair_lists2([X|Xs],[Y|Ys],[Z|XYs]) :-
   tennis_add_element2(X,Y,Z),
   tennis_pair_lists2(Xs,Ys,XYs).
tennis_pair_lists2([],_,[]).


tennis_add_element(X,[Y|Ys],[[X,Y]|Zs]) :-
   tennis_add_element(X,Ys,Zs).
tennis_add_element(_,[],[]).

tennis_add_element2(X,[Y|Ys],[[X,[Y]]|Zs]) :-
   tennis_add_element2(X,Ys,Zs).
tennis_add_element2(_,[],[]).
  
triple_lists_of_lists(
      [[X|Xs]|X2s],[[Y|Ys]|Y2s],[[Z|Zs]|Z2s],
      [[[X,Y,Z]|XYZs]|X2sY2sZ2s]) :-
   triple_lists([X|Xs],[Y|Ys],[Z|Zs],[[X,Y,Z]|XYZs]),
   triple_lists_of_lists(X2s,Y2s,Z2s,X2sY2sZ2s).
triple_lists_of_lists([],[],[],[]).


four_lists([A|As],[B|Bs],[C|Cs],[D|Ds],[[A,B,C,D]|Xs]) :-
   four_lists(As,Bs,Cs,Ds,Xs).
four_lists([],[],[],[],[]).

five_lists_of_lists([[U|Us]|U2s], [[V|Vs]|V2s],
      [[X|Xs]|X2s], [[Y|Ys]|Y2s], [[Z|Zs]|Z2s],
      [[[U,V,X,Y,Z]|UVXYZs]|U2sV2sX2sY2sZ2s]) :-
   five_lists([U|Us], [V|Vs], [X|Xs], [Y|Ys], [Z|Zs],
      [[U,V,X,Y,Z]|UVXYZs]),
   five_lists_of_lists(U2s,V2s,X2s,Y2s,Z2s,U2sV2sX2sY2sZ2s).
five_lists_of_lists([],[],[],[],[],[]).

five_lists([A|As],[B|Bs],[C|Cs],[D|Ds],[E|Es],[[A,B,C,D,E]|Xs]) :-
   five_lists(As,Bs,Cs,Ds,Es,Xs).
five_lists([],[],[],[],[],[]).
 

/* tennis_score_natural(N:M, Score) <-
      */

tennis_score_natural(N:M, X:Y) :-
   member(N:X, [0:0, 1:15, 2:30, 3:40]),
   member(M:Y, [0:0, 1:15, 2:30, 3:40]),
   !.
tennis_score_natural(N:N, deuce).
tennis_score_natural(N:M, adv:s) :-
   N is M + 1.
tennis_score_natural(N:M, r:adv) :-
   M is N + 1.
tennis_score_natural(N:M, game:s) :-
   N > M + 1.
tennis_score_natural(N:M, r:game) :-
   M > N + 1.


/* tennis_score_determine(N, M, X, Y) <-
      */

tennis_score_determine(3, 4, ' 40', 'Adv') :-
   !.
tennis_score_determine(4, 3, 'Adv', ' 40') :-
   !.
tennis_score_determine(A, B, RA, RB) :-
   A < 4, B < 4,
   !,
   tennis_point_number_to_score(A, RA),
   tennis_point_number_to_score(B, RB).
tennis_score_determine(A, A, 'Deu', 'Deu') :-
   A >= 4,
   !.
tennis_score_determine(A, B, ' 40', 'Adv') :-
   A >= 4, B >= 4,
   A < B,
   !.
tennis_score_determine(A, B, 'Adv', ' 40') :-
   A >= 4, B >= 4,
   A > B,
   !.
tennis_score_determine(_, _, '  0', '  0').

tennis_point_number_to_score(N, Score) :-
   member(N-Score, [0-'  0', 1-' 15', 2-' 30', 3-' 40']).


tennis_path_to_filename(Path, Video) :-
   name(Path, Path_Name),
   list_split_at_position(["/"], Path_Name, Parts),
   last_element(Parts, V),
   name(Video, V).

tennis_path_to_filename_colon(Path, Name) :-
   tennis_path_to_filename(Path, Video),
   tennis_split_videofile(Video, Name).


tennis_convert_display_xy_to_absolute_value(N,X,Y,XAR,YAR) :-
   ( N = 0, tennis_court_horizontal(Court)
   ; N = 1, tennis_court_vertical(Court) ),
   W := Court^width, L := Court^length,
   dislog_variables_get([
      tennis_court_side - S,
      tennis_court_behind - B,
      factor - Factor ]),
   X0 is W / 2 + S,
   Y0 is L / 2 + B,
   X1 is X / Factor,
   Y1 is Y / Factor,
   ( N = 0, XA is X0 - Y1, YA is Y0 - X1
   ; N = 1, XA is X1 - X0, YA is Y0 - Y1 ),
   tennis_round_two_decimals(XA, XAR),
   tennis_round_two_decimals(YA, YAR).


tennis_convert_absolute_value_to_display_xy(N,XAA,YAA,X,Y) :-
   ( N = 0, tennis_court_horizontal(Court)
   ; N = 1, tennis_court_vertical(Court) ),
   W := Court^width, L := Court^length,
   dislog_variables_get([
      tennis_court_side - S,
      tennis_court_behind - B,
      factor - Factor]),
   term_to_atom(XA,XAA),
   term_to_atom(YA,YAA),
   ( N = 0,
     Y is Factor * (W/2 + S - XA),
     X is Factor * (L/2 + B - YA)
   ; N = 1,
     X is Factor * (W/2 + S + XA),
     Y is Factor * (L/2 + B - YA) ). 


append_set([],[]).
append_set([A], [[],A]).
append_set([A,B],[[],A,AB]) :-
   append_with_hyphen(A,B,AB).
append_set([A,B,C],[[],A, AB, ABC]) :-
   append_set([A,B], [_,_, AB]),
   append_with_hyphen(AB,C,ABC).
append_set([A,B,C,D],[[],A, AB, ABC, ABCD]) :-
   append_set([A,B,C],[_, _, AB, ABC]),
   append_with_hyphen(ABC, D, ABCD).

append_set([A,B,C,D,E],[[], A,AB,ABC,ABCD,ABCDE]) :-
    append_set([A,B,C,D],[_,_,AB,ABC,ABCD]),
    append_with_hyphen(ABCD, E, ABCDE).

append_with_hyphen(A,B,AB3) :-
    term_to_atom(A, AA),
    term_to_atom(B, BB),
    concat([AA,'-'],A1),
    concat(A1,BB,AB1),
    atom(AB1),
    term_to_atom(AB3,AB1).


append_set_2([], [[]]).
append_set_2([A], [[],A]).
append_set_2([A,B],[[],A,AB]) :-
    append([A],[B],AB).
append_set_2([A,B,C],[[],A, AB, ABC]) :-
    append_set_2([A,B], [_,_, AB]),
    append(AB,[C],ABC).
append_set_2([A,B,C,D],[[],A, AB, ABC, ABCD]) :-
    append_set_2([A,B,C],[_, _, AB, ABC]),
    append(ABC, [D], ABCD).

append_set_2([A,B,C,D,E],[[], A,AB,ABC,ABCD,ABCDE]) :-
    append_set_2([A,B,C,D],[_,_,AB,ABC,ABCD]),
    append(ABCD, [E], ABCDE).


tennis_pair_lists3([X|Xs],[Y|Ys],[Z|XYs]) :-
   tennis_add_element3(X,Y,Z),
   tennis_pair_lists3(Xs,Ys,XYs).
tennis_pair_lists3([],_,[]).

tennis_add_element3(X,[Y|Ys],[[X,Y]|Zs]) :-
   tennis_add_element3(X,Ys,Zs).
tennis_add_element3(_,[],[]).


tennis_pair_lists_8([X|Xs],[Y|Ys],[Z|XYs]) :-
   tennis_add_element_8(X,Y,Z),
   tennis_pair_lists_8(Xs,Ys,XYs).
tennis_pair_lists_8([],_,[]).

tennis_add_element_8([S,G,P],Y,[S,G,P,Y]).
tennis_add_element_8(_,[],[]).


tennis_nth_element(N,List,Result) :-
   n_th_element(List,N,Result).

tennis_terms_to_atoms(Lists) :-
   checklist( tennis_term_to_atom,
      Lists ).

tennis_term_to_atom(T - A) :-
   term_to_atom(T, A).

tennis_atoms_to_terms(Lists) :-
   checklist( tennis_atom_to_term,
      Lists ).

tennis_atom_to_term(A - T) :-
   term_to_atom(T, A).

tennis_atom_to_term(A, T) :-
   term_to_atom(T, A).

tennis_send_selection(Refs, Values) :-
   foreach(Ref, Refs), foreach(Value, Values) do
      send(Ref, selection, Value).

tennis_get_selection(Refs, Values) :-
   foreach(Ref, Refs), foreach(Value, Values) do
      get(Ref, selection, Value).

tennis_get_selections(Items) :-
   checklist( tennis_get_selection,
      Items ).

tennis_get_selection(Dislog_Variable - Value) :-
   dislog_variable_get(Dislog_Variable, Ref),
   get(Ref, selection, Value).

tennis_lists_to_elements(Lists) :-
   checklist( tennis_list_to_element,
      Lists ).

tennis_list_to_element(List - Element) :-
   element_to_list(Element, List).

tennis_element_to_list([Element,[S,W]], [Element,S,W]).
tennis_element_to_list([], []).

tennis_lists_to_terms(Lists) :-
   checklist( tennis_list_to_term,
      Lists ).
   
tennis_list_to_term(List - Term) :-
   element_to_list(Element,List),
   term_to_atom(Term,Element).


tennis_toolbars(Target, Items) :-
   checklist( tennis_toolbar(Target),
      Items ).

tennis_toolbar(Target,
      Message - Message_Option - Picture - Help_Message) :-
   send(Target, append, new(Target2, tool_button(
      message(@prolog, Message, Message_Option), Picture))),
   send(Target2, help_message, tag, Help_Message).

tennis_toolbar(Target, Message - Picture - Help_Message) :-
   send(Target, append, new(Target2, tool_button(
      message(@prolog, Message), Picture))),
   send(Target2, help_message, tag, Help_Message).

tennis_send_text_items(Target, Items) :-
   checklist( tennis_send_text_item(Target),
      Items ).

tennis_send_text_items(Items) :-
   checklist( tennis_send_text_item,
      Items ).


tennis_send_text_item(Target,
      Dislog_Variable - Label - Value - Length) :-
   send(Target, append,
      new(Target2,text_item(Label, Value))),
   send(Target2, length, Length),
   dislog_variable_set(Dislog_Variable, Target2).


tennis_send_text_item(Dislog_Variable - Label 
      - Location - Location_Target - Length) :-
   dislog_variable_get(Location_Target, Location_Target_2),
   send(new(Target_2, text_item(Label)),
      Location, Location_Target_2),
   send(Target_2, length, Length),
   dislog_variable_set(Dislog_Variable, Target_2).


tennis_send_text_item(Dislog_Variable - Label -
      Location - Location_Target - Length - Value_Font) :-
   dislog_variable_get(Location_Target, Location_Target_2),
   send(new(Target_2, text_item(Label)),
      Location, Location_Target_2),
   send(Target_2, value_font, Value_Font),   
   send(Target_2, length, Length),
   dislog_variable_set(Dislog_Variable, Target_2).

tennis_send_text_item(Dislog_Variable - Label -
      Location - Location_Target - Length - Value_Font - _) :-
   dislog_variable_get(Location_Target, Location_Target2),
   send(new(Target2,text_item(Label)),Location,Location_Target2),
   send(Target2, value_font, Value_Font), 
   send(Target2, show_label, @off),  
   send(Target2, length, Length),
   dislog_variable_set(Dislog_Variable, Target2).


tennis_send_texts(Target, Font, Items) :-
   checklist(tennis_send_text(Target, Font), Items).

tennis_send_text(Target, Font, Text1 - Text2 - Position) :-
   send(Target, append, 
      text(string('%s%s', Text1, Text2), Position, Font)).

tennis_send_text(Target, Font, Text1 - Text2 - Position) :-
   send(Target, append, 
      text(string('%s%s', Text1, Text2), Position, Font)).

tennis_send_text(Target, Font, Text1 - Position) :-
   send(Target, append, text(Text1, Position, Font)).

tennis_list_to_element([A],A).

tennis_n_th_element(Number,List,Result) :-
   n_th_element(List,Number,Result).

tennis_destroy(Dialog) :-
   send(Dialog, destroy).

tennis_send_selection_dislog_variables(Items) :-
   checklist(tennis_send_selection_dislog_variable, Items).

tennis_send_selection_dislog_variable(Variable1 - Variable2) :-
   dislog_variables_get([
      Variable1 - Ref,
      Variable2 - Value]),
   send(Ref, selection, Value).

tennis_file_to_path(Path, File, FilePath) :-
   concat(Path, File, FilePath).

tennis_get_files_from_tennis_home_dir(Files) :-
   tennis_data_path(Path),
   swi_directory_contains_files(Path, FilesShort),
   maplist( tennis_file_to_path(Path), 
      FilesShort, Files ).

tennis_last_element_list(Lists, List) :-
   last_element(Lists, Element),
   element_to_list(Element, List).

tennis_round_two_decimals(Value, Value_Round) :-
   Value1 is Value * 100,
   round(Value1, Value2),
   Value_Round is Value2 / 100.

fill_list(0, []).
fill_list(A, [A|List]) :-
   B is A - 1,
   fill_list(B, List).

fill_list_with_int(A, ResultR) :-
   reverse(Result, ResultR),
   fill_list(A, Result).


tennis_list_remove_head([_|Xs], Xs).
tennis_list_remove_head([], []).

tennis_compare_names(A, B) :-
   name(A, A1),
   name(B, B1),
   tennis_list_remove_head(A1, A2),
   tennis_list_remove_head(B1, B2),
   A2 == B2.


tennis_concat(A, B, C) :-
   concat(B, A, C).


tennis_calculate_percent(_, 0, '--').
tennis_calculate_percent(A, B, C) :-
   C1 is A / B,
   C2 is C1 * 100,
   tennis_round_two_decimals(C2, C).

tennis_number_to_length1(Dislog_Variable, A) :-
   dislog_variable_get(Dislog_Variable, List),
   member([A, _, _], List),
   maplist( tennis_add_number_to_length_list1(A),
      List, List2 ),
   dislog_variable_set(Dislog_Variable, List2).

tennis_number_to_length1(Dislog_Variable, A) :-
   dislog_variable_get(Dislog_Variable, List),
   dislog_variable_set(Dislog_Variable, [[A, 1, 0]|List]).

tennis_number_to_length2(Dislog_Variable, A) :-
   dislog_variable_get(Dislog_Variable, List),
   member([A, _, _], List),
   maplist( tennis_add_number_to_length_list2(A),
      List, List2 ),
   dislog_variable_set(Dislog_Variable, List2).

tennis_number_to_length2(Dislog_Variable, A) :-
   dislog_variable_get(Dislog_Variable, List),
   dislog_variable_set(Dislog_Variable, [[A, 0, 1]|List]).

tennis_add_number_to_length_list1(A, [L,Ls,B], [L,L1s,B]) :-
   A == L,
   L1s is Ls +1.

tennis_add_number_to_length_list1(_, L, L).

tennis_add_number_to_length_list2(A, [L,As,Ls], [L,As,L1s]) :-
   A == L,
   L1s is Ls +1.

tennis_add_number_to_length_list2(_, L, L).


number_of_one_element_in_list(A, List, Result) :-
   dislog_variable_set(number_of_element, 0),  
   number_of_one_element_in_list(A, List),
   dislog_variable_get(number_of_element, Result).

number_of_one_element_in_list(A, [Ele|Lists]) :-
   A == Ele,
   dislog_variable_get(number_of_element, N),
   New is N + 1,
   dislog_variable_set(number_of_element, New),
   number_of_one_element_in_list(A, Lists).
number_of_one_element_in_list(A, [Ele|Lists]) :-
   A \= Ele,
   number_of_one_element_in_list(A, Lists). 
number_of_one_element_in_list(_, []).


kum_list_time(A, B) :-
   reverse(A, AR),
   kum_list_time2(AR, BR),
   reverse(BR, B).
kum_list_time(_, []).

kum_list_time2([[Rally,List1]|List1s], [[Rally,Result]|Results]) :-
   kum_list_time2(List1s, Results),
   sum_list_elements(List1s, Result1),
   ad_lists(Result1, List1, Result).
kum_list_time2([], []).

sum_list_elements(List1, [H, M, S]) :-
   maplist( tennis_n_th_element(2),
      List1, List ),
   maplist( tennis_n_th_element(1),
      List, Hours ),
   maplist( tennis_n_th_element(2),
      List, Minutes ),
   maplist( tennis_n_th_element(3),
      List, Seconds ),
   maplist( term_to_atom,
      Seconds_2, Seconds ),
   maplist( term_to_atom,
      Minutes_2, Minutes ),
   maplist( term_to_atom,
      Hours_2, Hours ),
   add(Hours_2, H), 
   add(Minutes_2, M),
   add(Seconds_2, S).
sum_list_elements([],[]).

ad_lists([A|As], [B|Bs], [C|Cs]) :-
   term_to_atom(A1,A),
   term_to_atom(B1,B),
   C is A1 + B1, 
   ad_lists(As, Bs, Cs).
ad_lists([],[],[]).


kum_list(A, B) :-
   reverse(A, AR),
   kum_list2(AR, BR),
   reverse(BR, B).
kum_list(_, []).

kum_list2([List1|List2s], [Result|Results]) :-
   kum_list2(List2s, Results),
   add(List2s, Result1),
   Result is Result1 + List1.
kum_list2([],[]).


list_remove_last_element(List, Results) :-
   reverse(List, ListR),
   list_remove_head(ListR, ResultsR),
   reverse(ResultsR, Results).

list_remove_head([_|Tail], Tail).
list_remove_head([], []).


/******************************************************************/


