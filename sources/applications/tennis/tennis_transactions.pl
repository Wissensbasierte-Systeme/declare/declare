

/******************************************************************/
/***                                                            ***/
/***         Tennis Tool: Transactions                          ***/
/***                                                            ***/
/******************************************************************/


test(tennis_data_mining, 1) :-
   File = 'examples/tennis/2002_sampras_agassi_final.xml',
   Support = 3, Confidence = 0.8,
   tennis_data_mining(File, Support, Confidence, _).

test(tennis_element_to_transactions, 1) :-
   File = 'examples/tennis/2002_sampras_agassi_final.xml',
   dread(xml, File, [Match]),
   tennis_element_to_transactions(Match, Transactions),
   writeln_list(user, Transactions).


/*** interface ****************************************************/


/* tennis_data_mining(File, Support, Confidence, Rules) <-
      */

tennis_data_mining(File, Support, Confidence, Rules) :-
   dread(xml, File, [Match]),
   tennis_element_to_transactions(Match, Transactions),
   association_rules(Transactions, Support, Confidence, Rules).


/* tennis_element_to_transactions(Element, Transactions) <-
      */

tennis_element_to_transactions(Element, Transactions) :-
   findall( Transaction,
      ( Point := Element/descendant::point,
        tennis_point_to_transaction(Point, Transaction) ),
      Transactions ),
   writeln_list(user, Transactions).


/*** implementation ***********************************************/


/* tennis_point_to_transaction(Point, Transaction) <-
      */

tennis_point_to_transaction(Point, Transaction) :-
   !,
   Hits := Point/content::'*',
   Server := Point@service,
   findall( A,
      ( nth_hit_to_direction(N, Hits, Direction),
        nth_hit_to_player(N, Server, Player),
        A = Direction:Player ),
      As ),
   findall( A,
      ( nth(N, Hits, Hit),
        volley := Hit@type,
        nth_hit_to_player(N, Server, Player),
        A = volley:Player ),
      Bs ),
   Xs <= list_to_multiset(append(As, Bs)),
   W := Point@winner,
   K <= length_coarsen(length(Hits)),
   sort([length:K, service:Server, winner:W|Xs], Transaction).
tennis_point_to_transaction(Point, Transaction) :-
   Hits := Point/content::'*',
   Server := Point@service,
   findall( A,
      ( nth(N, Hits, Hit),
        [H, T] := Hit-[@hand, @type],
        nth_hit_to_player(N, Server, Player),
        concat([Player, '_', H, '_', T], A) ),
      As ),
   list_to_multiset(As, Xs),
   W := Point@winner,
   length(Hits, L),
   sort([length:L, service:Server, winner:W|Xs], Transaction).

nth_hit_to_direction(N, Hits, Direction) :-
   nth(N, Hits, Hit_1),
   M is N + 1,
   nth(M, Hits, Hit_2),
   X_1 := Hit_1@x, atom_number(X_1, V_1),
   X_2 := Hit_2@x, atom_number(X_2, V_2),
   ( V_1 * V_2 < 0, abs(V_1-V_2) > 3 ->
     fail, Direction = cross
   ; ( V_1 * V_2 > 0, abs(V_1-V_2) < 1 ->
       Direction = long_line
     ; fail, Direction = middle ) ).

nth_hit_to_player(N, Server, Player) :-
   ( odd(N) -> Player = Server
   ; player_complement(Server, Player) ).

player_complement('A', 'B').
player_complement('B', 'A').

length_coarsen(N, M) :-
   ( between(0, 4, N) -> M = short
   ; between(5, 9, N) -> M = middle
   ; M = long ),
   !.


/******************************************************************/


