

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: Court                                ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_set(tennis_lines_coordinates, []).

tennis_court_horizontal([
   length: 23.77, width: 10.97,
   double: 1.37, service: 6.40,
   net: 0.91, foot:0.20,
   side: 3.65, behind: 6.40]).

tennis_court_vertical([
   width: 10.97, length: 23.77,
   double: 1.37, service: 6.40,
   net: 0.91, foot:0.20,
   behind: 2.80, side: 2.80]).

tennis_court_vertical_modify_side_and_behind :-
   !.
tennis_court_vertical_modify_side_and_behind :-
   dislog_variable_set(tennis_court_side, 6),
   dislog_variable_set(tennis_court_behind, 6).


/*** tests ********************************************************/


test(tennis_picture, 1) :-
   dislog_variable_get(tennis_picture,Picture),
   Colour = colour(@default, 16384, 16384, 65535),
   tennis_send_box(Colour, [-0.65,0], [-0.5,0.5]),
   tennis_lines_show(Picture, Colour, [[200,300],[100,100]]),
   tennis_lines_show(Picture, Colour, [[200,300],[100,150]]),
   tennis_lines_show(Picture, Colour, [[200,300],[100,200]]).


/*** interface ****************************************************/


/* tennis_court_sizes(Width, Length) <-
      */

tennis_court_sizes(Width, Length) :-
   tennis_court_vertical(As),
   members([width:W, length:L, double:D], As),
   Width is (W-2*D)/2,
   Length is L/2,
   !.


/* tennis_picture_horizontal <-
      */

tennis_picture_horizontal :-
   dislog_variable_get(tennis_picture, Dialog_Picture_Vertical),
   send(Dialog_Picture_Vertical, destroy),
   new(Picture_Horizontal, window('Tennis Court')),
   dislog_variable_set(tennis_picture, Picture_Horizontal),
   Horizontal = 0,
   dislog_variable_set(format, Horizontal),
   dislog_variable_get(factor, Factor),
   tennis_picture_create_horizontal(Picture_Horizontal, Factor),
   send(Picture_Horizontal, open).

tennis_picture_create_horizontal(Picture_Horizontal, Factor) :- 
   tennis_court_horizontal(Court),
   L := Court^length, W := Court^width,
   dislog_variable_get(tennis_court_side, S),
   dislog_variable_get(tennis_court_behind, B),
   X is Factor * (L + 2*B), Y is Factor * (W + 2*S), 
   send(Picture_Horizontal, size, size(X, Y)),
   dislog_variable_get(current_court_colour, Colour),
   send(Picture_Horizontal, background, Colour),
   tennis_court_create_horizontal(Picture_Horizontal, Factor),
   dislog_variable_set(tennis_picture, Picture_Horizontal),
   dislog_variable_get(tennis_modus, Modus),

   ( ( Modus == 'Input',
       send(Picture_Horizontal, label, 'Tennis Court'),
       send(Picture_Horizontal, recogniser,
          click_gesture(left, 's', single,
             message(@prolog, 
               tennis_hit_assert, @event?position?x, 
               @event?position?y, forehand, volley))),
       send(Picture_Horizontal, recogniser,
          click_gesture(left, '', single,
             message(@prolog, 
                tennis_hit_assert, @event?position?x, 
                @event?position?y, forehand, ground ))),
       send(Picture_Horizontal, recogniser,
          click_gesture(right, 's', single,
             message(@prolog, 
                tennis_hit_assert, @event?position?x, 
                @event?position?y, backhand, volley))),
       send(Picture_Horizontal, recogniser,
          click_gesture(right, '', single,
             message(@prolog, 
                tennis_hit_assert, @event?position?x, 
                @event?position?y, backhand, ground))) )

   ; ( Modus == 'Change',
       send(Picture_Horizontal, label, 'Tennis Court Change'),
       send(Picture_Horizontal, recogniser,
          click_gesture(left, 's', single,
             message(@prolog, 
                tennis_hit_change_assert, @event?position?x, 
                @event?position?y, forehand, volley))),
       send(Picture_Horizontal, recogniser,
          click_gesture(left, '', single,
             message(@prolog, 
                tennis_hit_change_assert, @event?position?x, 
                @event?position?y, forehand, ground ))),
       send(Picture_Horizontal, recogniser,
          click_gesture(right, 's', single,
             message(@prolog, 
                tennis_hit_change_assert, @event?position?x, 
                @event?position?y, backhand, volley))),
       send(Picture_Horizontal, recogniser,
          click_gesture(right, '', single,
             message(@prolog, 
                tennis_hit_change_assert, @event?position?x, 
                @event?position?y, backhand, ground))),
       send(Picture_Horizontal, recogniser,
          click_gesture(left, 'c', single,
             message(@prolog, tennis_hit_change_to_delete_coordinate, 
                @event?position?x, @event?position?y))) ) ).

tennis_court_create_horizontal(Picture_Horizontal, Factor) :-
   tennis_court_horizontal(Court),
   L := Court^length,  W := Court^width,
   D := Court^double,  S := Court^service,
   N := Court^net,     F := Court^foot,
   dislog_variable_get(tennis_court_side, Side),
   dislog_variable_get(tennis_court_behind, Behind),
   Y1 = Factor * Side, X1 = Factor * Behind,
   X4 is X1 + Factor * L, Y4 is Y1 + Factor * W,
   Y2 is Y1 + Factor * D, Y3 is Y4 - Factor * D,
   X2 is X1 + Factor * (L / 2 - S), X3 is X4 - Factor * (L / 2 - S),
   Xm is X1 + Factor * (L / 2),
   Ym is Y1 - Factor * N, Yn is Y4 + Factor * N,
   Yt is Y1 + Factor * (W / 2),
   Lf is X1 + Factor * F, Rf is X4 - Factor * F,
   checklist( tennis_line_send(Picture_Horizontal), [
      (X1-Y1,X4-Y1), (X1-Y1,X1-Y4), (X4-Y1,X4-Y4), (X1-Y4,X4-Y4), 
      (X1-Y2,X4-Y2), (X1-Y3,X4-Y3),
      (X2-Y2,X2-Y3), (X3-Y2,X3-Y3),
      (X2-Yt,X3-Yt), (X1-Yt,Lf-Yt), (X4-Yt,Rf-Yt),
      (colour(white), Xm-Ym, Xm-Yn) ]).


/* tennis_picture_vertical <-
      */

tennis_picture_vertical :-
   dislog_variable_get(tennis_picture, Picture_Horizontal),
   send(Picture_Horizontal, destroy),
   new(Picture_Vertical,window('Tennis Court')),
   dislog_variable_set(tennis_picture, Picture_Vertical),
   Vertical = 1,
   dislog_variable_set(format, Vertical),
   dislog_variable_get(factor, Factor),
   tennis_picture_create_vertical(Picture_Vertical, Factor),
   send(Picture_Vertical, open).

tennis_picture_create_vertical(Picture_Vertical, Factor) :-
   tennis_court_vertical(Court),
   tennis_court_vertical_modify_side_and_behind,
   dislog_variable_get(tennis_court_side, S),
   dislog_variable_get(tennis_court_behind, B),
   L := Court^length, W := Court^width,
   X is Factor * (W + 2*S), Y is Factor * (L + 2*B),
   send(Picture_Vertical, size, size(X,Y)),
   dislog_variable_get(current_court_colour, Colour),
   send(Picture_Vertical, background, Colour),
   tennis_court_create_vertical(Picture_Vertical, Factor),
   dislog_variable_get(tennis_modus, Modus),

   ( ( Modus == 'Input',
       send(Picture_Vertical, label, 'Tennis Court'),
       send(Picture_Vertical, recogniser,
          click_gesture(left, 's', single,
             message(@prolog, 
                tennis_hit_assert, @event?position?x, 
                @event?position?y, forehand, volley))),
       send(Picture_Vertical, recogniser,
          click_gesture(left, '', single,
             message(@prolog, 
                tennis_hit_assert, @event?position?x, 
                @event?position?y, forehand, ground ))),
       send(Picture_Vertical, recogniser,
          click_gesture(right, 's', single,
             message(@prolog, 
                tennis_hit_assert, @event?position?x, 
                @event?position?y, backhand, volley))),
       send(Picture_Vertical, recogniser,
          click_gesture(right, '', single,
             message(@prolog, 
                tennis_hit_assert, @event?position?x, 
                @event?position?y, backhand, ground))) )

   ; ( Modus == 'Change',
       send(Picture_Vertical, label, 'Tennis Court Change'),
       send(Picture_Vertical, recogniser,
          click_gesture(left, 's', single,
             message(@prolog, 
                tennis_hit_change_assert, @event?position?x, 
                @event?position?y, forehand, volley))),
       send(Picture_Vertical, recogniser,
          click_gesture(left, '', single,
             message(@prolog, 
                tennis_hit_change_assert, @event?position?x, 
                @event?position?y, forehand, ground ))),
       send(Picture_Vertical, recogniser,
          click_gesture(right, 's', single,
             message(@prolog, 
                tennis_hit_change_assert, @event?position?x, 
                @event?position?y, backhand, volley))),
       send(Picture_Vertical, recogniser,
          click_gesture(right, '', single,
             message(@prolog, 
                tennis_hit_change_assert, @event?position?x, 
                @event?position?y, backhand, ground))),
       send(Picture_Vertical, recogniser,
          click_gesture(left, 'c', single,
             message(@prolog, tennis_hit_change_to_delete_coordinate, 
                @event?position?x, @event?position?y))) ) ).

tennis_court_create_vertical(Picture, Factor) :-
   tennis_court_vertical(Court),
   L := Court^length,  W := Court^width,
   D := Court^double,  S := Court^service,
   N := Court^net,     F := Court^foot,
   tennis_court_vertical_modify_side_and_behind,
   dislog_variable_get(tennis_court_side, Side),
   dislog_variable_get(tennis_court_behind, Behind),
   X1 =  Factor * Side, Y1 = Factor * Behind ,
   X4 is X1 + Factor * W, Y4 is Y1 + Factor * L,
   X2 is X1 + Factor * D, X3 is X4 - Factor * D,
   Y2 is Y1 + Factor * (L / 2 - S), Y3 is Y4 - Factor * (L / 2 - S),
   Ym is Y1 + Factor * (L / 2),
   Xm is X1 - Factor * N, 
   Xn is X4 + Factor * N,
   Xt is X1 + Factor * (W / 2),
   Lf is Y1 + Factor * F, 
   Rf is Y4 - Factor * F,
   Y1_ is Y1+1, Y4_ is Y4-1,
   Xt = Xt, Lf = Lf, Rf = Rf,
   checklist( tennis_line_send(Picture), [
%     base lines
      (X1-Y4,X4-Y4), (X1-Y4_,X4-Y4_),
      (X1-Y1,X4-Y1), (X1-Y1_,X4-Y1_),
%     side lines
      (X1-Y1,X1-Y4), (X2-Y1,X2-Y4),
      (X3-Y1,X3-Y4), (X4-Y4,X4-Y1),
%     t lines
      (X2-Y2,X3-Y2), (X2-Y3,X3-Y3), 
%     service lines at net
      (Xt-Y2,Xt-Y3),
%     service lines at base lines
      (Xt-Y1,Xt-Lf), (Xt-Y4,Xt-Rf),
%     net
      (colour(black), Xm-Ym, Xn-Ym) ] ).


/* tennis_picture_uncolour <-
      */

tennis_picture_uncolour :-
   dislog_variable_get(format, Horizontal),
   Horizontal == 0, 
   FN_Point = [],
   tennis_clear_search_pattern_files,
   dislog_variable_set(hit_id, 0),
   dislog_variable_set(tennis_point_fn, FN_Point),
   dislog_variable_get(current_court_colour, Colour),
   tennis_send_box(Colour,[-1,1],[1,-1]),
   tennis_court_create_horizontal,
   !.

tennis_picture_uncolour :-
   dislog_variable_get(format, Vertical),
   Vertical == 1, 
   FN_Point = [],
   tennis_clear_search_pattern_files,
   dislog_variable_set(hit_id, 0),
   dislog_variable_set(tennis_point_fn, FN_Point),
   dislog_variable_get(current_court_colour, Colour),
   tennis_send_box(Colour, [-1,1], [1,-1]),
   tennis_court_create_vertical,
   !.


tennis_court_create_vertical :-
   dislog_variable_get(tennis_picture, Picture),
   dislog_variable_get(factor, Factor),
   tennis_court_create_vertical(Picture, Factor).

tennis_court_create_horizontal :-
   dislog_variable_get(tennis_picture, Picture),
   dislog_variable_get(factor, Factor),
   tennis_court_create_horizontal(Picture, Factor).


tennis_line_send(Picture, (X1-Y1,X2-Y2)) :-
   send(Picture, display, new(Line, line(X1,Y1,X2,Y2))),
   send(Line, colour(white)),
   send(Line, flush).
tennis_line_send(Picture, (Colour,X1-Y1,X2-Y2)) :-
   send(Picture, display, new(Line, line(X1,Y1,X2,Y2))),
   send(Line, Colour),
   send(Line, flush).


/* tennis_hit_display(X, Y, List) <-
      */

tennis_hit_display(X, Y, ['forehand', 'ground']) :-
   tennis_hit_display_filled(yellow, [X, Y]).
tennis_hit_display(X, Y, ['forehand', 'volley']) :-
   tennis_hit_display_empty(yellow, [X, Y]).
tennis_hit_display(X, Y, ['backhand', 'ground']) :-
   tennis_hit_display_filled(red, [X, Y]).
tennis_hit_display(X, Y, ['backhand', 'volley']) :-
   tennis_hit_display_empty(red, [X, Y]).
tennis_hit_display(X, Y, ['mark', Colour]) :-
   tennis_hit_display_empty(Colour, [X, Y]).

tennis_hit_display_filled(Colour, [X, Y]) :-
   tennis_hit_display_filled(Colour, ''-[X, Y]).

tennis_hit_display_filled(Colour, Text-[X, Y]) :-
   dislog_variable_get(tennis_picture, Picture),
   Y_ is Y + 5,
   send(Picture, display,
      new(T, text(Text)), point(X,Y_)),
   send(T, colour, colour(Colour)),
   tennis_hit_display_empty(Picture, Colour, [X, Y], Circle),
   send(Circle, colour(Colour)),
   send(Picture, flush).

tennis_hit_display_empty(Colour, [X, Y]) :-
   dislog_variable_get(tennis_picture, Picture),
   tennis_hit_display_empty(Picture, Colour, [X, Y]).

tennis_hit_display_empty(Picture, Colour, [X, Y]) :- 
   tennis_hit_display_empty(Picture, Colour, [X, Y], _).

tennis_hit_display_empty(Picture, Colour, [X, Y], Circle) :- 
   dislog_variable_get(factor, Factor),
   Size is (7 / 25) * Factor,
   send(Picture, display,
      new(Circle, circle(Size)), point(X,Y)),
%     new(Circle, box(1.2*Size,1.2*Size)), point(X,Y)),
   send(Circle, colour, colour(black)),
   send(Circle, fill_pattern, colour(Colour)).


/* tennis_ball_show(Colour, [X, Y]) <-
      */

tennis_ball_show(Colour, [X, Y], Circle) :-
   dislog_variable_get(format, Format),
   tennis_convert_absolute_value_to_display_xy(Format, X, Y, X2, Y2),
   tennis_hit_display_empty(Colour, [X2, Y2], Circle),
   send(Circle, colour(Colour)),
   !.


/* tennis_hit_display([XA, YA, [Hand, Type], _, _]) <-
      */

tennis_hit_display([XA, YA, [Hand, Type], _, _]) :-
   dislog_variable_get(tennis_lines_coordinates, Co),
   Co == [],
   dislog_variable_get(format, Format),
   tennis_convert_absolute_value_to_display_xy(Format, XA, YA, X, Y),
   Co_New = [X,Y],
   dislog_variable_set(tennis_lines_coordinates, [Co_New]),
   tennis_hit_display(X, Y, [Hand, Type]). 
tennis_hit_display([XA, YA, [Hand, Type], _, _]) :-
   dislog_variable_get(tennis_lines_coordinates,Co),
   Co \= [],
   dislog_variable_get(format, Format),
   tennis_convert_absolute_value_to_display_xy(Format, XA, YA, X, Y),
   Co_New = [[X,Y]|Co],
   dislog_variable_set(tennis_lines_coordinates, Co_New),
   tennis_hit_display(X, Y, [Hand, Type]).

tennis_hit_display_delete(X, Y) :-
   dislog_variable_get(current_court_colour, Colour),
   dislog_variable_get(tennis_picture, Picture),
   send(Picture, display, new(Circle, circle(14)), point(X,Y)),
   send(Circle, colour(Colour)),
   send(Circle, fill_pattern, Colour).

tennis_hit_display_change(X, Y) :-  
   dislog_variable_get(tennis_picture, Picture),
   send(Picture, display, new(Circle,circle(7)), point(X,Y)),
   send(Circle, colour(blue)),
   send(Circle, fill_pattern, colour(black)).


/* tennis_point_show(Which_Point) <-
      */

tennis_number_of_hits_of_rally(N) :-
   dislog_variable_get(list_of_points_with_hits_display, Points),
   n_th_element(Points, N, Hits),
   length(Hits, M),
   dislog_variable_set(number_of_hits_display, M).

tennis_point_show_first :-
   dislog_variable_get(show_rally_or_hit, Rally_or_Hit),
   tennis_point_show_first(Rally_or_Hit).

tennis_point_show_first(1) :-
   dislog_variable_set(current_point, 1),
   tennis_point_show(1).
tennis_point_show_first(0) :-
   dislog_variable_set(current_hit_id, 1),
   tennis_show_hit(1).


tennis_point_show_previous :-
   dislog_variable_get(show_rally_or_hit, Rally_or_Hit),
   tennis_point_show_previous(Rally_or_Hit).

tennis_point_show_previous(1) :-
   dislog_variable_get(current_point, N),
   M is N - 1,
   M >= 1,
   dislog_variable_set(current_point, M),
   tennis_point_show(M).
tennis_point_show_previous(1) :-
   dislog_variable_set(current_point, 1),
   tennis_point_show(1).
tennis_point_show_previous(0) :-
   dislog_variable_get(current_hit_id, ID),
   ID_New is ID - 1,
   ID_New >= 1,
   dislog_variable_set(current_hit_id, ID_New),
   tennis_show_hit(ID_New).
tennis_point_show_previous(0) :-
   dislog_variable_get(current_point, N),
   M is N - 1,
   M >= 1,
   dislog_variable_set(current_point, M),
   tennis_point_show_score_display(M),
   tennis_number_of_hits_of_rally(M),
   dislog_variable_get(number_of_hits_display, All),
   dislog_variable_set(current_hit_id, All),
   tennis_show_hit(All).
tennis_point_show_previous(0) :-
   dislog_variable_set(current_hit_id, 1),
   tennis_show_hit(1).


/* tennis_point_show_next(N) <-
      */

tennis_point_show_next :-
   dislog_variable_get(show_rally_or_hit, Rally_or_Hit),
   tennis_point_show_next(Rally_or_Hit).

tennis_point_show_next(1) :-
   dislog_variable_get(current_point, N),
   M is N + 1,
   dislog_variable_get(number_of_points_display, Number_of_Points),
   M =< Number_of_Points,
   dislog_variable_set(current_point, M),
   tennis_point_show(M).
tennis_point_show_next(1) :-
   tennis_point_show_last.
tennis_point_show_next(0) :-
   dislog_variable_get(current_hit_id, ID),
   ID_New is ID + 1,
   dislog_variable_get(number_of_hits_display, Number_of_Hits),
   ID_New =< Number_of_Hits,
   dislog_variable_set(current_hit_id, ID_New),
   tennis_show_hit(ID_New).
tennis_point_show_next(0) :-
   dislog_variable_get(current_point, N),
   M is N + 1,
   dislog_variable_get(number_of_points_display, Number_of_Points),
   M =< Number_of_Points,
   dislog_variable_set(current_point, M),
   tennis_point_show_score_display(M),
   tennis_number_of_hits_of_rally(M),
   dislog_variable_set(current_hit_id, 1),
   tennis_show_hit(1).
tennis_point_show_next(0) :-
   tennis_point_show_last(0).


/* tennis_point_show_last(N) <-
      */

tennis_point_show_last :-
   dislog_variable_get(show_rally_or_hit, Rally_or_Hit),
   tennis_point_show_last(Rally_or_Hit).

tennis_point_show_last(1) :-
   tennis_determine_hits,
   tennis_point_show_last2(1).
tennis_point_show_last(0) :-
   dislog_variable_get(current_point, N),
   tennis_point_show(N).

tennis_point_show_last2(1) :-
   dislog_variable_get(number_of_points_display, Last),
   dislog_variable_set(current_point, Last),
   tennis_point_show(Last).

  
/* tennis_point_show(N) <-
      */

tennis_point_show(N) :-
   dislog_variable_get(list_of_points_with_hits_display, Hits),
   Hits \= [],
   n_th_element(Hits, N, Points),   
   dislog_variable_set(change_Hit, Points),
   dislog_variable_set(change_Point_ID, N),
   tennis_picture_uncolour,
   dislog_variable_set(tennis_lines_coordinates, []),
   checklist( tennis_hit_display,
      Points ),
   tennis_lines_show,
   checklist( tennis_hit_display,
      Points ),
   tennis_scroll_display_video_time(N),
   tennis_scroll_display_match_time(N),
   tennis_point_show_score_display(N),
   tennis_point_set_input_video_file(N).
tennis_point_show(N) :-
   tennis_determine_hits,
   !,
   tennis_scroll_determine_score,
   tennis_scroll_determine_time,
   tennis_point_show(N).


/* tennis_show_hit(N) <-
      */

tennis_show_hit(N) :-
   dislog_variable_get(current_point, Current_Point),
   dislog_variable_get(list_of_points_with_hits_display, Points),
   n_th_element(Points, Current_Point, Hits),
   first_n_elements(N, Hits, Show_Hits),
   tennis_picture_uncolour,
   dislog_variable_set(tennis_lines_coordinates,[]),
   checklist( tennis_hit_display,
      Show_Hits ),
   tennis_lines_show,
   checklist( tennis_hit_display,
      Show_Hits ),
   display_parameter_box_winner(Current_Point),
   tennis_distance(Show_Hits).
tennis_show_hit(_) :-
   dislog_variable_get(tennis_parameter_dialog, Dialog),
   display_parameter_box_empty(Dialog).

tennis_distance(HitsR) :-
   reverse(HitsR, Hits),
   dislog_variable_get(current_point, NthRally),
   dislog_variable_get(all_server, AllServer),
   n_th_element(AllServer, NthRally, Server),
   length(HitsR, Length_of_Rally),
   modulo(Length_of_Rally, 2, Int),
   distance_for_player(Server, Int, Hitter),
   n_th_element(Hits, 1, First),
   n_th_element(Hits, 2, Second),
   n_th_element(Hits, 3, Third),
   n_th_element(First, 2, X1),
   n_th_element(Second, 2, X2),
   n_th_element(Third, 2, X3),
   term_to_atom(X11, X1),
   term_to_atom(X22, X2),
   term_to_atom(X33, X3),
   abs(X11, Xa11), abs(X22, Xa22), abs(X33, Xa33),
   H1 is Xa11 + Xa22,
   H2 is Xa22 + Xa33,
   D is H1 - H2,
   left_or_right(Hitter, D, LorR),
   abs(D, Da),
   distance_to_blocks(Da, Blocks),
   blocks_to_xy(Blocks, LorR, XY),
   dislog_variable_get(tennis_parameter_dialog, Dialog),
   display_parameter_box_empty(Dialog),
   checklist( display_parameter_fill_box(Dialog),
      XY ).


/* tennis_lines_show <-
      */

tennis_lines_show :-
   dislog_variable_get(tennis_picture, Picture),
   dislog_variable_get(tennis_lines_coordinates, Cos),
   rgb_to_colour([5.5, 5.5, 5.5], Colour),
   tennis_lines_show(Picture, Colour, Cos).

tennis_lines_show(Picture, Colour, [[X1,Y1], [X2,Y2]|Cs]) :-
   send(Picture, display,
      new(Line, line(X1+3,Y1+4,X2+3,Y2+4))),
   send(Line, colour(Colour)),
   !,
   tennis_lines_show(Picture, Colour, [[X2,Y2]|Cs]).
tennis_lines_show(_, _, [_|[]]).


/* tennis_change_court_size <-
      */

tennis_change_court_size :-
   dislog_variables_get([
      change_court_size_frame - Frame,
      input_change_court_size - Court_Size,
      input_change_court_side - Court_Side,
      input_change_court_behind - Court_Behind]),
   get(Court_Size,selection, Size_Atom),
   get(Court_Side,selection, Side_Atom),
   get(Court_Behind,selection, Behind_Atom),   
   tennis_terms_to_atoms([
      Factor - Size_Atom,
      Side - Side_Atom,
      Behind - Behind_Atom]),
   dislog_variables_set([
      factor - Factor,
      tennis_court_side - Side,
      tennis_court_behind - Behind]),
   tennis_save_court_size_change(Factor,Side,Behind),
   send(Frame,destroy),
   court_format_1.


tennis_change_court_size_default :-
   tennis_preferences(File),
   dislog_variable_get(change_court_size_frame,Frame),
   tennis_xml_file_to_fn_term(File,FN_Term),
   FN_Term_Default := FN_Term^preferences^default,
   tennis_set_preferences_size(FN_Term_Default),
   tennis_save_court_size_change,
   send(Frame,destroy),
   court_format_1.

  
tennis_court_format(Court) :-
   dislog_variable_get(format,Vertical),
   Vertical == 1,
   tennis_court_vertical(Court).
tennis_court_format(Court) :-
   dislog_variable_get(format,Horizontal),
   Horizontal == 0,
   tennis_court_horizontal(Court).


court_format_1 :-
   dislog_variable_get(format,Vertical),
   Vertical == 1,
   tennis_picture_vertical.
court_format_1 :-
   dislog_variable_get(format,Horizontal),
   Horizontal == 0,
   tennis_picture_horizontal. 


distance_for_player('A', 1, 'B').
distance_for_player('A', 0, 'A').
distance_for_player('B', 1, 'A').
distance_for_player('B', 0, 'B').


left_or_right('A', Value, 'Right') :-
   Value < 0.
left_or_right('A', Value, 'Left') :-
   0 < Value.
left_or_right('B', Value, 'Left') :-
   Value < 0.
left_or_right('B', Value, 'Right') :-
   0 < Value.
left_or_right(_ , 0, 'None').


distance_to_blocks(Distance, 1) :-
   0 =< Distance,
   Distance =< 2.
distance_to_blocks(Distance, 2) :-
   2 < Distance,
   Distance =< 4.
distance_to_blocks(Distance, 3) :-
   4 < Distance,
   Distance =< 6.
distance_to_blocks(Distance, 4) :-
   6 < Distance,
   Distance =< 8.
distance_to_blocks(Distance, 5) :-
   8 < Distance,
   Distance =< 10.
distance_to_blocks(Distance, 6) :-
   10 < Distance.


blocks_to_xy(1, 'Left', [110]).
blocks_to_xy(2, 'Left', [90, 110]).
blocks_to_xy(3, 'Left', [70, 90, 110]).
blocks_to_xy(4, 'Left', [50, 70, 90, 110]).
blocks_to_xy(5, 'Left', [30, 50, 70, 90, 110]).
blocks_to_xy(6, 'Left', [10, 30, 50, 70, 90, 110]).
blocks_to_xy(1, 'Right', [130]).
blocks_to_xy(2, 'Right', [130, 150]).
blocks_to_xy(3, 'Right', [130, 150, 170]).
blocks_to_xy(4, 'Right', [130, 150, 170, 190]).
blocks_to_xy(5, 'Right', [130, 150, 170, 190, 210]).
blocks_to_xy(6, 'Right', [130, 150, 170, 190, 210, 230]).


/******************************************************************/


