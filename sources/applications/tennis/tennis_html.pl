

/******************************************************************/
/***                                                            ***/
/***          Tennis Tool: HTML                                 ***/
/***                                                            ***/
/******************************************************************/


/* html_display_table_wehner(Title, Header, Rows) <-
      */

html_display_table_wehner(Title, Header, Rows) :-
   maplist( triple_to_minus_structure,
      [Header|Rows], [Header_2|Rows_2] ),
   tennis_statistics_file(File),
   concat('file:', File, Path),
   tell(File), told,
   append(File),
   tennis_html_file_start,
   tennis_write_html_table('center', [Header_2], Rows_2),
   tennis_html_file_end,
   told,
   tennis_statistics_doc_browser(Path, Title).

triple_to_minus_structure([X,Y,Z], X-Y-Z).


tennis_html_file_start :-
   tennis_html_file_start('Tennis Tool').

tennis_html_file_start(Title) :-
   writeln('<html>'),
   dwrite(xml, head:[title:Title]),
   writeln('<body>').


tennis_html_file_end :-
   writeln('</body>'),
   writeln('</html>').


tennis_write_html_table(Items) :-
   write('<table border="0">'),
   checklist( tennis_column_to_html_file,
      Items ),
   writeln('</table>').

tennis_write_html_table(Align, Items) :-
   write('<table border="0" align="'),
   write(Align), writeln('">'),
   checklist( tennis_column_to_html_file,
      Items ),
   writeln('</table>').

tennis_write_html_table(Align, Heads, Items) :-
   write('<table border="0" align="'),
   write(Align), writeln('">'),
   checklist( tennis_head_column_to_html_file,
      Heads ),
   checklist( tennis_column_to_html_file,
      Items ),
   writeln('</table>').


tennis_head_column_to_html_file(A-B-C) :-
   dwrite(xml, tr:[th:[b:[A]], th:[B], th:[C]]).


tennis_column_to_html_file(A-B-C-D) :-
   dwrite(xml, tr:[td:A, td:B, td:C, td:D]).

tennis_column_to_html_file([A, B]) :-
   dwrite(xml, tr:[td:A, td:B]).

tennis_column_to_html_file(A-B-C) :-
   dwrite(xml, tr:[
      td:[A],
      td:[width:150, align:center]:[B],
      td:[width:150, align:center]:[C] ]).

tennis_column_to_html_file(A-B) :-
   tennis_column_to_html_file([A, B]).

tennis_column_to_html_file(A) :-
   dwrite(xml, tr:[td:A]).


/******************************************************************/


