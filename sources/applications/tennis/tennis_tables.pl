

/******************************************************************/
/***                                                            ***/
/***         Tennis Tool: Tables                                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* tennis_file_add_times(File_1, File_2) <-
      */

tennis_file_add_times :-
   File_1 = 'projects/Tennis/2002_sampras_agassi_final.xml',
   File_2 = 'projects/Tennis/2002_sampras_agassi_final_times.xml',
   tennis_file_add_times(File_1, File_2).

tennis_file_add_times(File_1, File_2) :-
   dislog_variable_get(home,
      '/sources/applications/tennis/tennis_times.fng', Fng),
   fn_transform_xml_file_fng(Fng, File_1, File_2).


/* tennis_file_point_to_hits(File, [S, G, P], Hits) <-
      */

tennis_file_point_to_hits(File, [S, G, P], Hits) :-
   tennis_file_to_point(File, [S, G, P], Point),
   findall( [X,Y],
      ( _ := Point/hit::[@x=U, @y=V],
        term_to_atom(X, U),
        term_to_atom(Y, V) ),
      Hits ).

tennis_file_to_point(File, [S, G, P], Point) :-
   dread(xml, File, [Xml]),
   Point := Xml/set::[@id=S]/game::[@id=G]/point::[@id=P].


/* tennis_file_point_to_hits(File, N, M, [S, G, P], Hits) <-
      */

tennis_file_point_to_hits(File, N, M, [S, G, P], Hits) :-
   tennis_court_sizes(Width, Length),
   dread(xml, File, [Xml]),
   tennis_xml_to_points(Width, Length, N, M, Xml, Points),
   tennis_xml_points_to_table(points:Points, Table_1),
   tennis_table_normalize(N, M, Table_1, Table_2),
   xpce_display_table('Tennis Table - normalized', [], Table_2),
   first(Table_2, H), writeq(H),
   !,
   findall( [X,Y],
      ( member(Tuple, Table_2),
        members([set:S, game:G, point:P, x_1:U, y_1:V], Tuple),
        term_to_atom(X, U),
        term_to_atom(Y, V) ),
      Hits ),
   writeq(Hits).


/* tennis_file_to_table(File, Width, Length, N, M, Table) <-
      */

tennis_file_to_table(File, Width, Length, N, M, Table) :-
   dread(xml, File, [Xml]),
   tennis_xml_to_points(Width, Length, N, M, Xml, Points),
   tennis_xml_points_to_table(points:Points, Table_1),
   tennis_table_normalize(N, M, Table_1, Table_2),
   tennis_table_reduce(Table_2, Table).


/* tennis_xml_to_points(Width, Length, N, M, Xml, Points) <-
      */

tennis_xml_to_points(Width, Length, N, M, Xml, Points) :-
   tennis_size_to_intervals(Width, N, Is_x),
   tennis_size_to_intervals(Length, M, Is_y),
   tennis_intervals_to_tesselation(Is_x, Is_y),
   tennis_xml_to_points(Is_x, Is_y, Xml, Points),
   writeln('Intervals_x = '), writeln(Is_x),
   writeln('Intervals_y = '), writeln(Is_y).


/* tennis_table_proj́ect(Attributes, Tuple_1, Tuple_2) <-
      */

tennis_table_project(Attributes, Tuples_1, Tuples_2) :-
   maplist( tennis_tuple_project(Attributes),
      Tuples_1, Tuples_2 ).

tennis_tuple_project(Attributes, Tuple_1, Tuple_2) :-
   findall( A:V,
      ( member(A:V, Tuple_1),
        member(A, Attributes) ),
      Tuple_2 ).

tennis_table_project_(Attributes, Tuples_1, Tuples_2) :-
   maplist( tennis_tuple_project_(Attributes),
      Tuples_1, Tuples_2 ).

tennis_tuple_project_(Attributes, Tuple_1, Tuple_2) :-
   findall( A:V,
      ( member(A, Attributes),
        member(A:V, Tuple_1) ),
      Tuple_2 ).


/* tennis_table_reduce(Tuples_1, Tuples_2) <-
      projection of the tennis table
      excluding a set of irrelevant attributes */

tennis_table_reduce(Tuples_1, Tuples_2) :-
   Attributes = [ service,
%     winner,
      top, error,
      hit, set, game, point, id,
      id_1, id_2,
      type_1, type_2, hand_1, hand_2,
      time_1, time_2, score_A, score_B ],
   tennis_table_reduce(Attributes, Tuples_1, Tuples_2).


/* tennis_table_reduce(Attributes, Tuples_1, Tuples_2) <-
      */

tennis_table_reduce(Attributes, Tuples_1, Tuples_2) :-
   maplist( tennis_tuple_reduce(Attributes), Tuples_1, Tuples_2).

tennis_tuple_reduce(Attributes, Tuple_1, Tuple_2) :-
   findall( A:V,
      ( member(A:V, Tuple_1),
        \+ member(A, Attributes) ),
      Tuple_2 ).


/* tennis_table_normalize(N, M, Tuples_1, Tuples_2) <-
      */

tennis_table_normalize(N, M, Tuples_1, Tuples_2) :-
   dislog_variable_get(output_path,
      'tennis_table_normalize', File),
   write_list(user, ['---> ', File, '\n']),
   predicate_to_file( File,
      maplist(tennis_tuple_normalize(N, M), Tuples_1, Tuples_2) ).

tennis_tuple_normalize(N, M, Tuple_1, Tuple_2) :-
   tennis_values_and_intervals_normalize(N, M, Tuple_1, Tuple_A),
   Attributes = [
      x_1, y_1, x_2, y_2, 'Ix_1', 'Iy_1', 'Ix_2', 'Iy_2'],
   tennis_tuple_reduce(Attributes, Tuple_1, Tuple_B),
   append(Tuple_A, Tuple_B, Tuple),
   sort(Tuple_1, Tuple_1s),
   sort(Tuple, Tuple_2),
   writeq(Tuple_1s), writelnq('-->'),
   writelnq(Tuple_2), nl.

tennis_values_and_intervals_normalize(N_, M_, Tuple_1, Tuple_2) :-
   ( integer(N_) -> N = N_ ; length(N_, N) ),
   ( integer(M_) -> M = M_ ; length(M_, M) ),
   tennis_tuple_extract_values_and_intervals(Tuple_1, Top, Vs, Is),
   Vs = [X_1, Y_1, X_2, Y_2], Is = [Ix_1, Iy_1, Ix_2, Iy_2],
   ( Top = 'B' ->
     U_1 is -X_1, V_1 is -Y_1,
     U_2 is -X_2, V_2 is -Y_2,
     Iu_1 is N + 1 - Ix_1, Iv_1 is M + 1 - Iy_1,
     Iu_2 is N + 1 - Ix_2, Iv_2 is M + 1 - Iy_2,
     Numbers = [U_1, V_1, U_2, V_2, Iu_1, Iv_1, Iu_2, Iv_2]
   ; append(Vs, Is, Numbers) ),
   maplist(atom_number, Values, Numbers),
   Values = [_, _, _, _, A1, B1, A2, B2],
   concat_atom([A1, '_', B1], T1),
   concat_atom([A2, '_', B2], T2),
   Attributes = ['Txy_1', 'Txy_2',
      x_1, y_1, x_2, y_2, 'Ix_1', 'Iy_1', 'Ix_2', 'Iy_2'],
   pair_lists(':', Attributes, [T1, T2|Values], Tuple_2).

tennis_tuple_extract_values_and_intervals(Tuple, Top, Vs, Is) :-
   members( [top:Top, x_1:X_1, y_1:Y_1, x_2:X_2, y_2:Y_2,
      'Ix_1':Ix_1, 'Iy_1':Iy_1, 'Ix_2':Ix_2, 'Iy_2':Iy_2 ], Tuple),
   maplist(atom_number, [X_1, Y_1, X_2, Y_2], Vs),
   Is = [Ix_1, Iy_1, Ix_2, Iy_2].


/******************************************************************/


