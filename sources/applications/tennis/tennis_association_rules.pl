

/******************************************************************/
/***                                                            ***/
/***         Tennis Tool: Association Rules                     ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      association_rule/5.


/*** tests ********************************************************/


test(weka_tennis_table_to_association_rules, 1) :-
   retract_facts(association_rule/5),
   tennis_court_sizes(Width, Length),
%  N = 2, M = 2,
%  N = 2, M = 4,
%  N = 3, M = 2,
%  N = [[0,0.25], [0.25,0.75], [0.75,1]],
%  M = [[0,0.5], [0.5,1]],
%  N = 4, M = 2,
%  N = 4, M = 4,
   N = 4, M = 6,
   File = 'examples/tennis/2002_sampras_agassi_final.xml',
   write(user, '<--- '), writeln(user, File),
   weka_tennis_association_rules(File, Width, Length, N, M),
   weka_tennis_association_rules_display(N, M).

test(weka_tennis_table_to_association_rules, 2) :-
   test(weka_tennis_table_to_association_rules, 1),
   test(association_rules_invert_and_assert, 1),
   test(association_rules_sort_and_display, 1).

test(association_rules_invert_and_assert, 1) :-
   tennis_file_to_table(Table),
   association_rules_invert_and_assert(Table, Rules),
   writeln_list(user, Rules).

test(association_rules_sort_and_display, 1) :-
   association_rules_sort(Rules),
   writeln_list(user, Rules),
   weka_tennis_association_rules_display('?', '?').

test(association_rules_support_confidene, 1) :-
   tennis_file_to_table(Table),
   findall( F:C:Rule,
      ( member(Rule, [
           (['Txy_1':'1_1']-->['Txy_2':'1_2']),
           (['Txy_1':'4_1']-->['Txy_2':'4_2']),
           (['Txy_1':'1_2']-->['Txy_2':'1_1']),
           (['Txy_1':'4_2']-->['Txy_2':'4_1']) ]),
        association_rule_to_frequency_and_confidence(
           Table, Rule, F:C:Rule) ),
      Rules ),
   writeln_list(user, Rules).


/*** interface ****************************************************/


/* tennis_file_to_table(Table) <-
      */

tennis_file_to_table(Table) :-
   tennis_court_sizes(Width, Length),
%  N = 4, M = 2,
   N = 4, M = 4,
   File = 'examples/tennis/2002_sampras_agassi_final.xml',
   tennis_file_to_table(File, Width, Length, N, M, Table).


/* association_rules_invert_and_assert(Table, Rules) <-
      */

association_rules_invert_and_assert(Table, Rules) :-
   findall( F:C:Rule,
      ( association_rule(_, [A1:V1], [A2:V2], _, _),
        \+ association_rule(_, [A1:V2], [A2:V1], _, _),
        Rule = ([A1:V2]-->[A2:V1]),
        association_rule_to_frequency_and_confidence(
           Table, Rule, F:C:Rule),
        assert(association_rule('*', [A1:V2], [A2:V1], F, C)) ),
      Rules ).


/* association_rules_sort(Rules) <-
      */

association_rules_sort(Rules) :-
   findall( [C, F, Id, Bs, As],
      retract(association_rule(Id, Bs, As, F, C)),
      Tuples_1 ),
   sort(Tuples_1, Tuples_2),
   reverse(Tuples_2, Tuples_3),
   findall( association_rule(Id, Bs, As, F, C),
      ( member([C, F, Id, Bs, As], Tuples_3),
        assert(association_rule(Id, Bs, As, F, C)) ),
      Rules ).


/* weka_tennis_association_rules(File, Width, Length, N, M) <-
      */

weka_tennis_association_rules(File, Width, Length, N, M) :-
   weka_tennis_table_to_association_rules(
      File, Width, Length, N, M, Rules),
   assert_facts(Rules),
   !.


/* weka_tennis_table_to_association_rules(
         File, Width, Length, N, M, Rules) <-
      */

weka_tennis_table_to_association_rules(
      File, Width, Length, N, M, Rules) :-
   dread(xml, File, [Xml]),
   tennis_xml_to_points(Width, Length, N, M, Xml, Points),
   tennis_xml_points_to_table(points:Points, Table_1),
   tennis_table_normalize(N, M, Table_1, Table_2),
   xpce_display_table('Tennis Table - normalized', [], Table_2),
   Attributes_P = [hit, point, winner, error,
      'Ix_1','Iy_1', 'Ix_2','Iy_2', id_1, hand_1, id_2, hand_2],
   tennis_table_project_(Attributes_P, Table_2, Table_P),
   xpce_display_table('Tennis Table - projected - 1', [], Table_P),
   tennis_table_reduce(Table_2, Table_3),
   xpce_display_table('Tennis Table - reduced', [], Table_3),
%  Attributes = ['Txy_1','Txy_2', 'Ix_1','Iy_1', 'Ix_2','Iy_2'],
   Attributes = ['Txy_1','Txy_2'],
   tennis_table_project(Attributes, Table_3, Table_4),
   xpce_display_table('Tennis Table - projected - 2', [], Table_4),
   weka_association_lists_to_rules(Table_4, Rules_3),
   association_rules_xml_to_facts(Rules_3, Rules_4),
   tennis_association_rules_filter(Rules_4, Rules).


/*** implementation ***********************************************/


/* tennis_association_rules_filter(Rules_1, Rules_2) <-
      */

tennis_association_rules_filter(Rules_1, Rules_2) :-
   findall( Rule,
      ( member(Rule, Rules_1),
        Rule = association_rule(_, B, H, _, _),
        length(B, LB), LB < 2, length(H, LH), LH < 2,
        member('Txy_1':_, B), member('Txy_2':_, H)
/*      ( member('Txy_1':_, B)
        ; member('Ix_1':_, B)
        ; member('Iy_1':_, B) ),
        ( member('Txy_2':_, H)
        ; member('Ix_2':_, H)
        ; member('Iy_2':_, H) ),
        \+ ( member('Txy_1':_, B), member('Ix_1':_, B) ),
        \+ ( member('Txy_1':_, B), member('Iy_1':_, B) ),
        \+ ( member('Txy_2':_, H), member('Ix_2':_, H) ),
        \+ ( member('Txy_2':_, H), member('Iy_2':_, H) ),
        \+ ( member('Ix_2':_, B) ; member('Iy_2':_, B) ),
        \+ ( member('Ix_1':_, H) ; member('Iy_1':_, H) ),
        \+ ( member('Txy_1':_, H) ; member('Txy_2':_, B) ),
%       ( member('Txy_1':_, B) ; member('Txy_2':_, H) )
%       ( member('Ix_1':_, B)  , member('Ix_2':_, H) )
%       ( member('Ix_1':_, B)  ; member('Ix_2':_, B) ),
%       ( member('Ix_1':_, H)  ; member('Ix_2':_, H) )
*/
      ),
      Rules ),
   sort(Rules, Rules_2).
tennis_association_rules_filter(Rules_1, Rules_2) :-
   findall( Rule,
      ( member(Rule, Rules_1),
        Rule = association_rule(_, B, _H, _, _),
        member('Ix_1':'1', B) ),
      Rules_2 ).


/* weka_tennis_association_rules_display <-
      */

weka_tennis_association_rules_display(N, M) :-
   term_to_atom(N, N_),
   term_to_atom(M, M_),
   concat(['Association Rules - ', N_, '*', M_], Title),
   findall( [Id, B, H, S, C],
      association_rule(Id, B, H, S, C),
      Table ),
   Attributes = ['Rule_Id', 'Body   ->',
      'Head', 'Support', 'Confidence'],
   xpce_display_table(Title, Attributes, Table).


/******************************************************************/


