

/******************************************************************/
/***                                                            ***/
/***         Tennis Tool: Tesselation                           ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variables_set([
      tennis_tesselation_colour_light -
         colour(@default, 00000, 60000, 30000),
      tennis_tesselation_colour_dark -
         colour(@default, 16384, 50000, 16384) ]).


/*** interface ****************************************************/


/* tennis_intervals_to_tesselation(Intervals_x, Intervals_y) <-
      */

tennis_intervals_to_tesselation(Intervals_x, Intervals_y) :-
   dislog_variable_get(tennis_picture, Picture),
   tennis_picture_uncolour,
   dislog_variables_get([
      tennis_tesselation_colour_light - Colour_1,
      tennis_tesselation_colour_dark  - Colour_2 ]),
   length(Intervals_x, N_Max),
   length(Intervals_y, M_Max),
   forall(
      ( nth(N, Intervals_x, Ix), N > 1, N < N_Max,
        nth(M, Intervals_y, Iy), M > 1, M < M_Max ),
      ( ( even(N+M) -> Colour = Colour_1
        ; Colour = Colour_2 ),
        tennis_intervals_to_tile(
           Picture, Colour, N, M, Ix, Iy) ) ),
   tennis_court_create_vertical(Picture, 25).
tennis_intervals_to_tesselation(_, _).

tennis_intervals_to_tile(Picture, Colour, N, M, Ix, Iy) :-
   Ix = [X1, X2],
   Iy = [Y1, Y2],
   tennis_convert_absolute_value_to_display_xy(
      1, X1, Y1, A1, B1),
   tennis_convert_absolute_value_to_display_xy(
      1, X2, Y2, A2, B2),
   C is A2 - A1,
   D is B2 - B1,
   tennis_tile(Picture, Colour, box(C,D), point(A1,B1)),
   N_ is N - 1, M_ is M - 1,
   concat(['(',N_,',',M_,')'], Tessile),
   send(Picture,display,
      new(Text,text(Tessile)),
      point((A1+A2)/2-12.5,(B1+B2)/2-10) ),
   send(Text,colour,black),
   send(Text,font,font(times,bold,12)).


/* tennis_table_to_tiles(Table, Tiles) <-
      */

tennis_table_to_tiles(Table, Tiles) :-
   findall( Tile,
      ( member(Tuple, Table),
        ( Tile := Tuple/'Txy_1'
        ; Tile := Tuple/'Txy_2' ) ),
      Tiles_2 ),
   sort(Tiles_2, Tiles).


/*** implementation ***********************************************/


/* tennis_tile(Picture, Colour, box(X,Y), point(U,V)) <-
      */

tennis_tile(Picture, Colour, box(X,Y), point(U,V)) :-
   send(Picture, display, new(Box, box(X,Y)), point(U,V)),
   send(Box, fill_pattern, Colour),
   send(Box, colour, Colour).


/******************************************************************/


