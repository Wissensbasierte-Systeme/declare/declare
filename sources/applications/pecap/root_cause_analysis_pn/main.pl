

/******************************************************************/
/***                                                            ***/
/***        Pecap: Root Cause Analysis                          ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_get(source_path,
      'projects/pecap/root_cause_analysis_pn/', Path),
   dislog_variable_set(root_cause_analysis_pn, Path).
   

/*** used paths ***************************************************/


stablenet_rules_path(Path) :-
   dislog_variable_get(
      root_cause_analysis_pn, 'stablenet_rules.pl', Path).

root_cause_path(Path) :-
   dislog_variable_get(
      root_cause_analysis_pn, 'root_cause.dl', Path).

temp_dl_path(Path) :-
   dislog_variable_get(
      root_cause_analysis_pn, 'temp.dl', Path).


/*** modules ******************************************************/


:- consult(stablenet_rules).
:- use_module('root_cause_analysis_pn').


/******************************************************************/


