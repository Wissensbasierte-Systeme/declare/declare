

/******************************************************************/
/***                                                            ***/
/***      Pecap:  Root Cause Analysis                           ***/
/***                                                            ***/
/******************************************************************/


:- module( 'root_cause_analysis_pn', [] ).


/*** interface ****************************************************/


/* root_cause_analysis <-
      */

root_cause_analysis :-
   forall( tree_table_node(X-_),
      send(X, expanded_image, '16x16/opendir.xpm') ),
   root_causes(Model),
   forall( expanded_image(Model, N, I),
      ( atom_number(L, N),
        tree_table_node(X-L),
        send(X, expanded_image, I) ) ).

expanded_image(Model, Number, '16x16/error.xpm') :-
   member(defunct(Number), Model).
expanded_image(Model, Number, '16x16/false.xpm') :-
   member(root_cause(Number), Model).

root_causes(Model) :-
   root_cause_path(D1),
   stablenet_rules_path(D2),
   temp_dl_path(D3),
   concat_files(D1, D2, D3),
   perfect_model_prolog_files([D3], [], Model).


/* network_to_browser_ddk <-
      */

network_to_browser_ddk :-
   Config = config:[]:[
      icons:[]:[
         icon:[alias:hierarchy,
            file:'16x16/hierarchy.xpm']:[] ],
      nodes:[open:'opendir.xpm', close:'closedir.xpm']:[
         node:[alias:network,
            open:hierarchy, close:hierarchy]:[] ],
      mouse_clicks:[]:[
         mouse_click:[alias:init_tree_table]:[
            on_click:[button:left, type:double,
               module:user, predicate:create_tree_table,
               prmtrs:[@2]]:[] ] ] ],
   network_forest(Forest),
   Tree = network:[name:network,
      mouse_click:init_tree_table]:Forest,
   hierarchy_to_browser_ddk(Config, Tree, _).

ms_parent_root(X) :-
   ms_parent(_, X),
   \+ ms_parent(X, _).

ms_parent_roots(Xs) :-
   setof(X, ms_parent_root(X), Xs).

network_forest(Forest) :-
   ms_parent_roots(Roots),
   findall( Tree,
      ( member(Root, Roots),
        network_tree(Root, Tree) ),
      Forest ).

network_tree(Root, node:[name:Label]:Forest) :-
   ms_name_to_label(Root, Label),
   findall( Tree,
      ( ms_parent(Son, Root),
        network_tree(Son, Tree) ),
      Forest ).

ms_name_to_label(Node, Node) :-
   !.
ms_name_to_label(Node, Label) :-
   ms_name(Node, Name),
   !,
%  first_n_characters(5, Name, Name_2),
   concat([Node, '_', Name], Label).


/* create_tree_table(X) <-
      */

create_tree_table(X) :-
   retract_all(tree_table_entry(_, _)),
   create_tree_table_(X).

create_tree_table_(X) :-
   get(X, label, L),
   send(X, collapsed, @off),
   get(X, sons, Chain),
   chain_list(Chain, Sons),
   maplist( assert_tree_table_entry(X-L),
      Sons ),
   maplist( create_tree_table_,
      Sons ).

assert_tree_table_entry(X-L, Y) :-
   get(Y, label, M),
   assert(tree_table_entry(X-L, Y-M)),
   writeln(user, assert(tree_table_entry(X-L, Y-M))).

tree_table_node(X-L) :-
   ( tree_table_entry(X-L, _)
   ; tree_table_entry(_, X-L) ).


/******************************************************************/


