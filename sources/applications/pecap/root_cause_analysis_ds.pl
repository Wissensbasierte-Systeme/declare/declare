

/******************************************************************/
/***                                                            ***/
/***      Pecap:  Root Cause Analysis                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* root_cause_analysis <-
      */

root_cause_analysis :-
   forall( tree_table_node(X-_),
      send(X, expanded_image, '16x16/opendir.xpm') ),
   root_causes(_, Model),
   forall( member(defunct(L), Model),
      ( tree_table_node(X-L),
        send(X, expanded_image, '16x16/error.xpm') ) ),
%  forall( member(possible_root_cause(L), Model),
%     ( tree_table_node(X-L),
%       send(X, expanded_image, '16x16/stop.xpm') ) ),
   forall( member(root_cause(L), Model),
      ( tree_table_node(X-L),
        send(X, expanded_image, '16x16/false.xpm') ) ).

root_causes(Root_Causes, Model) :-
   dislog_variable_get(source_path,
      'projects/pecap/root_cause.dl', D),
   perfect_model_prolog_files([D], [], Model),
   findall( C,
      member(root_cause(C), Model),
      Root_Causes ).


/* network_to_browser_ddk <-
      */

network_to_browser_ddk :-
   Config = config:[]:[
      icons:[]:[
         icon:[alias:hierarchy,
            file:'16x16/hierarchy.xpm']:[] ],
      nodes:[open:'opendir.xpm', close:'closedir.xpm']:[
         node:[alias:network,
            open:hierarchy, close:hierarchy]:[] ],
      mouse_clicks:[]:[
         mouse_click:[alias:init_tree_table]:[
            on_click:[button:left, type:double,
               module:user, predicate:create_tree_table,
               prmtrs:[@2]]:[] ] ] ],
   Tree = network:[name:network,
      mouse_click:init_tree_table]:[
      node:[name:a]:[
         node:[name:b]:[
            node:[name:d]:[],
            node:[name:e]:[
               node:[name:f]:[] ] ],
         node:[name:c]:[] ] ],
   hierarchy_to_browser_ddk(Config, Tree, _).


/* create_tree_table(X) <-
      */

create_tree_table(X) :-
   retract_all(tree_table_entry(_, _)),
   create_tree_table_(X).

create_tree_table_(X) :-
   get(X, label, L),
%  write_list(user, [' * ', L, ' * ']),
%  get(X, collapsed, C),
   send(X, collapsed, @off),
   get(X, sons, Chain),
   chain_list(Chain, Sons),
%  ( foreach(S, Sons) do
%       ( get(S, label, M), write(user, M) ) ),
   checklist( assert_tree_table_entry(X-L),
      Sons ),
   checklist( create_tree_table_,
      Sons ).
%  send(X, collapsed, C).

assert_tree_table_entry(X-L, Y) :-
   get(Y, label, M),
   assert(tree_table_entry(X-L, Y-M)),
   writeln(user, assert(tree_table_entry(X-L, Y-M))).

tree_table_node(X-L) :-
   ( tree_table_entry(X-L, _)
   ; tree_table_entry(_, X-L) ).


/******************************************************************/


