

syslog_transformation(1) :-
   dread(txt, '../syslog_export_1000.csv', T1),
   name_exchange_elements(["09", "19", "29", "39", "49",
      "59", "69", "79", "89"], T1, T2),
   dwrite(txt, '001', T2).
syslog_transformation(2) :-
   dread(xls, '001', Rs1),
   sort(Rs1, Rs2),
   dwrite(xls, '002', Rs2).
syslog_transformation(3) :-
   dread(txt, '002', T2),
   name(T2, N2), text_parse_addresses(N3, N2, []), name(T3, N3),
   dwrite(txt, '003', T3).
syslog_transformation(4) :-
   dread(xls, '003', Rs1),
   sort(Rs1, Rs2), syslog_csv_transform(Rs2, Rs3), sort(Rs3, Rs4),
   dwrite(xls, '004', Rs4).


syslog_csv_transform(Rows_1, Rows_2) :-
   foreach(Row_1, Rows_1), foreach(Row_2, Rows_2) do
      maplist( row_field_transform,
         Row_1, Row_2 ).

row_field_transform(Name_1, Name_2) :-
   member(Name_2, ['Benutzerkennung', 'Benutzerdomaene',
      'Dienstkennung', 'Dienstname', 'Angegebener Bereichsname',
      'Domaene', 'Anmeldekennung', '{S-9-9', 'Y999DPK9\\']),
   name_contains_name(Name_1, Name_2).
row_field_transform(Name, Name).


text_parse_addresses(Ys) -->
   syslog_address, { ! }, text_parse_addresses(Xs),
   { append("[address]", Xs, Ys) }.
text_parse_addresses([X|Xs]) -->
   [X], { ! }, text_parse_addresses(Xs).
text_parse_addresses([]) -->
   [].

% (9x9.9x99F999E9)

syslog_address -->
   "(", full_number, "x", full_number, ".",
   full_number, "x", full_number,
   hex_number, hex_number, hex_number, hex_number, 
   hex_number, hex_number, hex_number, ")".

full_number -->
   [X], { member(X, "0123456789") }.
hex_number -->
   [X], { member(X, "0123456789ABCDEF") }.


