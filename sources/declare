

/******************************************************************/
/******************************************************************/
/***                                                            ***/
/***        ***   ***   **  *     **   ***   ***                ***/
/***        *  *  *    *    *    *  *  *  *  *                  ***/
/***        *  *  ***  *    *    ****  ****  ***                ***/
/***        *  *  *    *    *    *  *  *  *  *                  ***/
/***        ***   ***   **  ***  *  *  *  *  ***                ***/
/***                                                            ***/
/******************************************************************/
/******************************************************************/


:- op(100, fx, @).
:- op(200, xfy, @).
:- op(1010, fx, stratify).

:- set_prolog_flag(encoding, iso_latin_1).
:- set_prolog_flag(double_quotes, codes).
:- use_module(library('emacs/prolog_mode')).

:- dynamic
      list_undefined/0,
      list_undefined/1.
:- multifile
      list_undefined/0,
      list_undefined/1,
      base_literal/1,
      test/2,
      dread/3, dwrite/3,
      dportray/2.
:- discontiguous
      dislog_menue/1.      

list_undefined.
list_undefined(_).

no_singleton_variable(_).
no_singleton_variables(_).

% unix(system(_)).


/******************************************************************/


%  getenv('HOSTNAME', Host).

home_directory(Home) :-
   current_prolog_flag(unix, true),
   getenv('HOME', Home).
home_directory('C:/Programme') :-
   current_prolog_flag(windows, true).

home_directory(X, Y) :-
   home_directory(Home),
   concat(Home, X, Y).

home_directory(X, File, Path) :-
   concat(X, File, Y),
   home_directory(Y, Path).


/******************************************************************/


:- ['basic_algebra/basics/specials_swi'].
:- ['basic_algebra/basics/date'].
:- ['basic_algebra/basics/dislog_variables'].
:- ['biology_and_medicine/qualimed/qm_chains.pl'].

:- ['system/dislog_versions.pl'].
:- ['system/dislog_units.pl'].
:- ['system/dislog_sources.pl'].
:- ['system/dislog_blockings'].
% :- ['system/dislog_adapter'].

%  File = '.dislog_blockings',
%  ( exists_file(File) -> [File]
%  ; ['system/dislog_blockings.pl'] ).


/******************************************************************/


/* loading certain Sicstus-Prolog libraries
      for list processing,
      for ordered list processing,
      for graph processing. */

:- load_files(
      standard_libraries, [silent(false)] ).


/******************************************************************/


:- ( home_directory('/home/user') ->
     dislog_variable_set(screen_size, small)
   ; true ).


/******************************************************************/


/* dislog_menue(Type) <-
      */

dislog_menue(boot) :-
   write('Consulting Declare units ... '),
   !,
   findall( Unit,
      dislog_unit(Unit, _),
      Units ),
   length(Units, N),
   write_ln(N),
   Mode = true,
%  Mode = false,
/*
   Units_to_Load = [
      basic_algebra, xml, nm_reasoning,
%     software_engineering,
%     artificial_intelligence,
      databases ],
*/
   Units_to_Load = Units,
   forall(
      ( dislog_unit(Unit, _),
        member(Unit, Units_to_Load) ),
      dislog_unit_load(Mode, Unit) ).
/*
   dislog_sources(Sources),
%  dislog_sources_non_blocked(Sources),
%  consult(Sources),
   load_files(Sources, [silent(Mode)]).
*/

dislog_unit_load(Mode, Unit) :-
   write('%   unit('), write(Unit), write(')'),
%  write('%   unit:'), write(Unit),
   dislog_unit_to_sources(Unit, Sources),
%  dislog_unit_to_sources_non_blocked(Unit, Sources),
   length(Sources, N),
   load_files(Sources, [silent(Mode)]),
   write(' consulted, '),
   write(N),
   writeln(' files').

dislog_menue(welcome) :-
   ddk_version(Version, Date),
   nl,
   write('Welcome to the Declare '),
   concat(['Developers Toolkit (DDK, Version ',
      Version, ', ', Date, ')'], DDK_Version),
   writeln(DDK_Version),
   write('Copyright (c) 2023 '),
   writeln('University of Würzburg'), nl.
%  write('Dietmar Seipel, '),
%  writeln('All rights reserved'), nl.

:- dislog_menue(boot).

:- ['sources/software_engineering/program_analysis_mh/loader.pl'].

:- dislog_flag_set(xpce_mode, yes).
:- dislog_variable_get(home, '/.ddkrc', File),
   consult_if_exists(File).

:- dislog_menue(welcome).


/******************************************************************/


/* Flags for guiding the execution of certain operations. */

:- dislog_flags_set([
      trace_level:1,
      sd_mode:2,
      tps_mode:2,
      egcwa_mode:0,
      join_mode:regular,
      edge_mode:long,
      connected_atoms_mode:atoms,
      sql_trace_mode:oracle,
      pm_algorithm2_generate:atoms,
      evidential_stable_mode:a,
      mm_satchmo:1,
      xpce_list_browser_problem:yes ]).
%     xpce_list_browser_problem:no
 
:- set_prolog_flag(encoding, utf8).


/*** ODBC connections *********************************************/


declare_mysql_odbc_connections_init :-
   getenv('USER', User),
   declare_mysql_odbc_connections_init(User).

declare_mysql_odbc_connections_init(User) :-
   DSN = mysql,
   Alias_1 = mysql,
   mysql_odbc_connect(DSN, User, '', Alias_1, multiple),
   Alias_2 = odbc_connection_data_dictionary,
   mysql_odbc_connect(DSN, User, '', Alias_2, multiple),
   dislog_variable_set(Alias_2, Alias_2).

:- ( ( getenv('USER', User),
       member(User, [dietmar, seipel]) ) ->
     declare_mysql_odbc_connections_init(User)
   ; true ).


/******************************************************************/


:- ( dislog_flag_get(xpce_mode, yes) ->
     dislog_desktop_html
   ; true ).

:- home_directory('/.ddk', Path),
   consult_if_exists(Path).

% ?- use_module(library(cql/cql)).

:- set_prolog_flag(toplevel_prompt, 'Declare ?- ').
% :- dislog_variable_set(mysql, no).
  :- dislog_variable_set(mysql, yes).

% :- sgml_register_catalog_file('?', end).


/******************************************************************/


