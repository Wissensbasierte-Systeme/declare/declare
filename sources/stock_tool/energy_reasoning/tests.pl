

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Technical Chart Analysis            ***/
/***                                                            ***/
/******************************************************************/


energy_file(Path) :-
   dislog_variable_get(home, DisLog),
   concat([DisLog, '/projects/Energy',
      '/2004_11_04/energy/data/energy_2004_10_13.xml'],
      Path).


/*** tests ********************************************************/


test(technical_chart_analysis, 1) :-
   energy_file(File),
   technical_chart_analysis(File, ['2012003'],
      [rsi, roc, momentum, bollinger, random],
      1000, [14], Result),
   dwrite(xml, Result).

test(technical_chart_analysis, 2) :-
   technical_chart_analysis('2012002').

technical_chart_analysis(Wkn) :-
   technical_chart_analysis(Wkn, Result),
   dwrite(xml, Result).

technical_chart_analysis(Wkn, Result) :-
   member(Wkn, [
      '2012001', '2012002', '2012003', '2012004',
      '2022001', '2022002', '2022003', '2022004' ]),
   energy_file(File),
   generate_interval(10, 20, Interval_Lenghts),
   technical_chart_analysis(File, [Wkn],
      [rsi, roc, momentum, bollinger, random],
      1000, Interval_Lenghts, Result).


/******************************************************************/


