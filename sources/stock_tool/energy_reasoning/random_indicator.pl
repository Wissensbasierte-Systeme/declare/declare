

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Technical Chart Analysis            ***/
/***                                                            ***/
/******************************************************************/


:- module( random_indicator, [
      random/2,
      random_trade/4 ] ).

:- use_module([tca_auxilliary]).


/*** interface ****************************************************/


/* random(Date:_, Date:Random) <-
      */

random(Date:_, Date:Random) :-
   Random is random(2).


/* random_trade(Inds, Vals, Cash, Result) <-
      */

random_trade([Ind|Inds], [Val|Vals], Cash, Result) :-
   ( Ind = 1 ->
     ( buy(Val, Cash, Cash1, 0, Owns),
       random_trade(Inds, Vals, Owns, Cash1, Result) )
   ; random_trade(Inds, Vals, 0, Cash, Result) ).

random_trade([_], [Val], Owns, Cash, Result) :-
   !,
   Result is Owns * Val + Cash.
random_trade([Ind|Inds], [Val|Vals], Owns, Cash, Result) :-
   ( Ind == 1 ->
     ( buy(Val, Cash, Cash1, Owns, Owns1),
       random_trade(Inds, Vals, Owns1, Cash1, Result) )
   ; Cash1 is Cash + Val * Owns,
     random_trade(Inds, Vals, 0, Cash1, Result) ).


/******************************************************************/


