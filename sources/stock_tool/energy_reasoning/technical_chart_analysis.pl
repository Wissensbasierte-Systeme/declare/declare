

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Technical Chart Analysis            ***/
/***                                                            ***/
/******************************************************************/


:- module(technical_chart_analysis, [
      technical_chart_analysis/6 ]).

:- use_module([
      rsi_indicator,
      roc_indicator,
      momentum_indicator,
      bollinger_indicator,
      random_indicator ]).


/*** interface ****************************************************/

   
/* technical_chart_analysis_loop(Xml_File, Wkns,
         Methods, Cash, Interval_Lenghts, Results) <-
      */

technical_chart_analysis(Xml_File, Wkns,
      Methods, Cash, Interval_Lenghts, Results) :-
   maplist( technical_chart_analysis_sub(Xml_File,
         Methods, Interval_Lenghts, Cash),
      Wkns, Result ),
   flatten(Result, Result_2),
   Results = stocks:Result_2.

technical_chart_analysis_sub(Xml_File,
      Methods, Interval_Lenghts, Cash, Wkn, Results) :-
   xml_extract(Xml_File, Wkn, Values, Dates, Length),
   maplist( technical_chart_analysis_single(
         Wkn, Values, Dates, Length, Methods, Cash),
      Interval_Lenghts, Results ).

technical_chart_analysis_single(Wkn, Values, Dates, Length,
      Methods, Cash, Interval, Result) :-
   indicators(
      Methods, Dates, Values, Length, Interval, Indicators),
   trade(Indicators, Values, Interval, Cash, Results),
   Result = stock:[wkn:Wkn, interval:Interval|Results]:[].


/*** implementation ***********************************************/


/* trade(Indicators, Values, Interval, Cash, Results) <-
      */

trade(Indicators, Values, Interval, Cash, Results) :-
   Interval_1 is Interval - 1,
   split(Interval_1, Values, _, Values_2),
   maplist( trade_step(Values_2, Cash),
      Indicators, Results ).

trade_step(Values, Cash, Method-Inds, Method:Result) :-
   findall( Ind,
      member(_:Ind, Inds),
      Inds_1 ),
   atom_concat(Method, trade, Method_Trade),
   Goal =.. [Method_Trade, Inds_1, Values, Cash, Result],
   call(Goal).


/* indicators(Methods, Dates, Values, Length,
         Interval, Indicators) <-
      */

indicators(Methods, Dates, Values, Length,
      Interval, Indicators) :-
   Iterations is Length - Interval + 1, 
   windows(Iterations, Interval, Dates, Values, Windows),
   maplist( indicators_step(Windows),
      Methods, Indicators ).

indicators_step(Windows, Method, Method-Indicators) :-
   maplist( Method,
      Windows, Indicators ).


/* windows(Iterations, Interval, Dates, Values, Windows) <-
      */

windows(1, _, Dates, Values, [Date:Values]) :-
   !,
   last(Dates, Date).
windows(I, Interval, [D|Ds], [V|Vs], [WinDate:WinValues|Windows]) :-
   nth1(Interval, [D|Ds], WinDate), 
   first_n_elements(Interval, [V|Vs], WinValues),
   J is I - 1,
   windows(J, Interval, Ds, Vs, Windows).


/* xml_extract(Xml_File, Wkn, Values, Dates, Length) <-
      */

xml_extract(Xml_File, Wkn, Values, Dates, Length) :-
   dread(xml, Xml_File, [FN_Term]),
   entries(value, Wkn, FN_Term, Values_1),
   maplist( atom_number,
      Values_1, Values ),
   entries(date, Wkn, FN_Term, Dates),
   length(Values, Length).

entries(Which, Wkn, FN_Term, Entries) :-
   findall( Entry,
      Entry := FN_Term^chart::[@wkn=Wkn]^entry@Which,
      Entries ).


/******************************************************************/


