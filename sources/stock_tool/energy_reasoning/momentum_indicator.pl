

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Technical Chart Analysis            ***/
/***                                                            ***/
/******************************************************************/


:- module(momentum, [
      momentum/2,
      momentumtrade/4 ]).

:- use_module([tca_auxilliary]).


/*** interface ****************************************************/


/* momentum(Date:Values, Date:Momentum) <-
      */

momentum(Date:Values, Date:Momentum) :-
   first(Values, Momentum_1),
   last(Values, Momentum_2),
   Momentum is Momentum_2 - Momentum_1 - 1.


/* momentumtrade(Inds, Vals, Cash, Result) <-
      */

momentumtrade([Ind|Inds], [Val|Vals], Cash, Result) :-
   ( Ind >= 0 ->
     ( buy(Val, Cash, Cash_1, 0, Owns),
       momentum_trade(1, Inds, Vals, Owns, Cash_1, Result) )
   ; momentum_trade(2, Inds, Vals, 0, Cash, Result) ).

momentum_trade(_, [_], [Val], Owns, Cash, Result) :-
   !,
   Result is Owns * Val + Cash.
momentum_trade(Flag, [Ind|Inds], [Val|Vals], Owns, Cash, Result) :-
   ( Flag == 1 ->
     ( Ind < 0 ->
       ( Cash_1 is Cash + Owns * Val,
         momentum_trade(2, Inds, Vals, 0, Cash_1, Result) )
     ; momentum_trade(1, Inds, Vals, Owns, Cash, Result) )
   ; ( Ind > 0 ->
       ( buy(Val, Cash, Cash_1, Owns, Owns_1),
         momentum_trade(1, Inds, Vals, Owns_1, Cash_1, Result) )
     ; momentum_trade(2, Inds, Vals, Owns, Cash, Result) ) ).


/******************************************************************/


