

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Technical Chart Analysis            ***/
/***                                                            ***/
/******************************************************************/


:- module(rsi, [
      rsi/2,
      rsitrade/4 ]).

:- use_module([tca_auxilliary]).


/*** interface ****************************************************/


rsi_upper(70).
rsi_lower(30).


/* rsi(Date:Values, Date:Rsi) <-
      */

rsi(Date:Values, Date:Rsi) :-
   gains_losses(Values, Gains, Losses),
   ( Losses == 0 ->
     RS is (Gains / 0.000000001)
   ; RS is (Gains / Losses) ),
   Rsi is 100 - (100 / (1 + RS)).


/* rsitrade(Inds, Vals, Cash, Result) <-
      */

rsitrade([Ind|Inds], [Val|Vals], Cash, Result) :- 
   rsi_upper(High),
   rsi_lower(Low),
   ( Ind < Low ->
     rsi_trade(1, Inds, Vals, High, Low, 0, Cash, Result)
   ; ( Ind =< High ->
       ( buy(Val, Cash, Cash_1, 0, Owns),
         rsi_trade( 2, Inds, Vals,
            High, Low, Owns, Cash_1, Result) )
     ; rsi_trade(3, Inds, Vals,
          High, Low, 0, Cash, Result) ) ).

rsi_trade(_, [_], [Val], _, _, Owns, Cash, Result) :-
   !,
   Result is Cash + Owns * Val.
rsi_trade(Flag, [Ind|Inds], [Val|Vals],
      High, Low, Owns, Cash, Result) :-
   ( Flag == 1 ->
     ( Ind > Low ->
       ( buy(Val, Cash, Cash_1, Owns, Owns1),
         rsi_trade(2, Inds, Vals,
            High, Low, Owns1, Cash_1, Result) )
     ; rsi_trade(1, Inds, Vals,
          High, Low, Owns, Cash, Result) )
   ; ( Flag == 2 ->
       ( Ind < Low ->
         rsi_trade(1, Inds, Vals,
            High, Low, Owns, Cash, Result)
       ; ( Ind > High ->
           rsi_trade(3, Inds, Vals,
              High, Low, Owns, Cash, Result)
         ; rsi_trade(2, Inds, Vals,
              High, Low, Owns, Cash, Result) ) )
     ; ( Ind < High ->
         ( Cash_1 is Cash + Owns * Val,
           rsi_trade(2, Inds, Vals,
              High, Low, 0, Cash_1, Result) )
       ; rsi_trade(3, Inds, Vals,
            High, Low, Owns, Cash, Result) ) ) ).


/******************************************************************/


