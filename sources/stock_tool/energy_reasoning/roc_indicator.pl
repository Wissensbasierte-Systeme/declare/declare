

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Technical Chart Analysis            ***/
/***                                                            ***/
/******************************************************************/


:- module(roc, [
      roc/2,
      roctrade/4 ]).

:- use_module([tca_auxilliary]).


/*** interface ****************************************************/


/* roc(Date:Values, Date:Roc) <-
      */

roc(Date:Values, Date:Roc) :-
   first(Values, Roc_1),
   last(Values, Roc_2),
   Roc is 100 * (Roc_2 / Roc_1).


/* roctrade(Inds, Vals, Cash, Result) <-
      */

roctrade([Ind|Inds], [Val|Vals], Cash, Result) :-
   ( Ind >= 100 ->
     ( buy(Val, Cash, Cash1, 0, Owns),
       roc_trade(1, Inds, Vals, Owns, Cash1, Result) )
   ; roc_trade(2, Inds, Vals, 0, Cash, Result) ).

roc_trade(_, [_], [Val], Owns, Cash, Result) :-
   !,
   Result is Owns * Val + Cash.
roc_trade(Flag, [Ind|Inds], [Val|Vals], Owns, Cash, Result) :-
   ( Flag == 1 ->
     ( Ind < 100 ->
       ( Cash1 is Cash + Owns * Val,
         roc_trade(2, Inds, Vals, 0, Cash1, Result) )
     ; roc_trade(1, Inds, Vals, Owns, Cash, Result) )
   ; ( Ind > 100 ->
       ( buy(Val, Cash, Cash1, Owns, Owns1),
         roc_trade(1, Inds, Vals, Owns1, Cash1, Result) )
     ; roc_trade(2, Inds, Vals, Owns, Cash, Result) ) ).


/******************************************************************/


