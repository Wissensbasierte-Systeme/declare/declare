

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Technical Chart Analysis            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* technical_chart_analysis_ranking <-
      */

technical_chart_analysis_ranking :-
   findall( Result,
      technical_chart_analysis(_, Result),
      Results ),
   R = results:Results,
   Indicators = [rsi, roc, momentum, bollinger, random],
   findall( stock:[Indicator:Value, wkn:Wkn, interval:I]:[],
      _ := R^stocks^stock::[@wkn=Wkn,
         member(Indicator, Indicators),
         @Indicator=Value, @interval=I ],
      Stocks_2 ),
   sort(Stocks_2, Stocks),
%  dwrite(xml, stocks:Stocks),
   maplist( indicator_to_average(stocks:Stocks),
      Indicators, Averages ),
   dwrite(xml, averages:Averages:[]).

indicator_to_average(Stocks, Indicator, Indicator:Average) :-
   findall( Value,
      ( V := Stocks^stock@Indicator,
        term_to_atom(Value, V) ),
      Values ),
   average(Values, Average).


/******************************************************************/


