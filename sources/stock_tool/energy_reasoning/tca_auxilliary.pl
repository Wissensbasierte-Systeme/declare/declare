

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Technical Chart Analysis            ***/
/***                                                            ***/
/******************************************************************/


:- module(tca_auxilliary, [
      gains_losses/3,
      buy/5 ]).


/*** interface ****************************************************/


/* gains_losses(Values, Gains, Losses) <-
      */

gains_losses(Values, Gains, Losses) :-
   gains_losses(Values, 0, Gains, 0, Losses).

gains_losses([H1, H2|T], AG, Gains, AL, Losses) :-
   H1 >= H2,
   !,
   AL1 is AL + H1 - H2,
   gains_losses([H2|T], AG, Gains, AL1, Losses).
gains_losses([H1, H2|T], AG, Gains, AL, Losses) :-
   H1 < H2,
   !,
   AG1 is AG + H2 - H1,
   gains_losses([H2|T], AG1, Gains, AL, Losses).
gains_losses(_, Gains, Gains, Losses, Losses).


/* buy(Val, Old_Cash, New_Cash, Old_Owns, New_Owns) <-
      */

buy(Val, Old_Cash, New_Cash, Old_Owns, New_Owns) :-
   no_of_available_options(Old_Cash, Val, Owns_1),
   New_Owns is Old_Owns + Owns_1,
   New_Cash is Old_Cash - Val * Owns_1.

no_of_available_options(Num, Mod, Rem) :-
   Y is Num / Mod,
   Rem is float_integer_part(Y). 


/******************************************************************/


