

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Technical Chart Analysis            ***/
/***                                                            ***/
/******************************************************************/


:- module(bollinger_indicator, [
      bollinger/2,
      bollingertrade/4 ]).

:- use_module([tca_auxilliary]).


/*** interface ****************************************************/


/* bollinger(Date:Values, Date:[B_Upper:MA:B_Lower]) <-
      */

bollinger(Date:Values, Date:[B_Upper:MA:B_Lower]) :-
   length(Values, L),
   sumlist(Values, MA1),
   MA is (1 / L) * MA1,
   maplist(add(-MA), Values, Values1),
   maplist(square, Values1, Values2),
   sumlist(Values2, SD_Sum),
   SD is (1 / (L - 1)) * SD_Sum,
   B_Upper is MA + 2 * SD,
   B_Lower is MA - 2 * SD.


/* bollingertrade(Inds, Vals, Cash, Result) <-
      */

bollingertrade(Inds, Vals, Cash, Result) :-
   flatten(Inds, Inds2),
   bollinger_trade(Inds2, Vals, Cash, Result).

bollinger_trade([High:_:_|Inds], [Val|Vals], Cash, Result) :-
   ( Val > High ->
     ( buy(Val, Cash, Cash1, 0, Owns),
       bollinger_trade(Inds, Vals, Owns, Cash1, Result) )
   ; bollinger_trade(Inds, Vals, 0, Cash, Result) ).

bollinger_trade([_], [Val], Owns, Cash, Result) :-
   !,
   Result is Val * Owns + Cash.
bollinger_trade([High:_:Low|Inds], [Val|Vals],
      Owns, Cash, Result) :-
   ( float_between(Low, High, Val) ->
     bollinger_trade(Inds, Vals, Owns, Cash, Result)
   ; ( Val < Low ->
       ( Cash1 is Cash + Owns * Val,
         bollinger_trade(Inds, Vals, 0, Cash1, Result) )
     ; buy(Val, Cash, Cash1, Owns, Owns1),
       bollinger_trade(Inds, Vals, Owns1, Cash1, Result) ) ).


/******************************************************************/


