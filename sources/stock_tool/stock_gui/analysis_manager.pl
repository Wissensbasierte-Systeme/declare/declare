

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  XPCE Analysis Manager                ***/
/***                                                            ***/
/******************************************************************/


stock_analysis_manager :-
   new(Frame,frame('SMS - Analysis Manager')),
   send(Frame,append,new(Dialog_T,dialog)),
   send(Dialog_T,append,new(Date,text_item('Date',''))),
   dislog_variable_get(dsa_browser_colour,Colour),
   send(new(Dialog_I,dialog),below,Dialog_T),
   send(Dialog_I,background,Colour),
   send(Dialog_I,append,
      new(Slider_Day,slider('Day',-100,100,10))),
   send(Dialog_I,append,
      new(Slider_Week,slider('Week',-100,100,10))),
   send(Dialog_I,append,
      new(Slider_Month,slider('Month',-100,100,10))),
   send(Dialog_I,append,
      new(Slider_Year,slider('Year',-100,100,10))),
%  send(Dialog_I,size,size(250,40)),
   send(new(Dialog_II,dialog),below,Dialog_I),
   checklist( send_append(Dialog_II), [
      'Update' - stock_tool_update_preferences(
         Slider_Day?selection,
         Slider_Week?selection,
         Slider_Month?selection,
         Slider_Year?selection ),
      'Ranking' - stock_analysis_ranking(Date?selection),
      'Close' - send(Frame,destroy) ] ),
   send(Frame,open).

stock_analysis_ranking(Date) :-
   ensure_date_is_sql_date(Date,D),
   name_remove_elements(" ",D,SQL_Date),
%  writeln(user,SQL_Date),
   mysql_select_by_date_with_previous(SQL_Date).

stock_tool_update_preferences(D,W,M,Y) :-
   Preferences = [D,W,M,Y],
   sum_list(Preferences,Divisor),
   Divisor \= 0,
   !,
   dislog_variable_set(stock_preferences,[D,W,M,Y]),
   divide_list(Preferences,Divisor,List),
   vector_multiply(100,List,Normed_Preferences),
   vectors_multiply(List,[365,52,12,1],Weights),
   dislog_variables_set([
      stock_preferences_normed - Normed_Preferences,
      stock_weights - Weights ]).
stock_tool_update_preferences(D,W,M,Y) :-
   nl, star_line,
   writeln(user,'The sum of the preferences'),
   write(user,'   '), writeln(user,[D,W,M,Y]),
   writeln('is zero !').


/******************************************************************/


