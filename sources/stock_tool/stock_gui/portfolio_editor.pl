

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  XPCE Portfolio Editor                ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      stock_portfolio_operation/1.

:- dynamic
      dsa_portfolio_entity/1.
%     stock_portfolio_tuple/1.

:- dislog_variables_set([
      entity_portfolio - 'Muster',
      entity_number - 1,
      entity_company - 'Muster',
      entity_wkn - '111111',
      entity_date - '31.08.2001',
      entity_value - 0.0,
      entity_quantity - 0.0 ]).

stock_portfolio_default_values([
   name: '',
   number: 1,
   company: '',
   wkn: 1,
   date: '',
   value: 0.0,
   quantity: 0.0 ]).


/******************************************************************/


stock_portfolio_editor :-
   Variables =
      [ name     - entity_portfolio,
        number   - entity_number,
        company  - entity_company,
        wkn      - entity_wkn,
        date     - entity_date,
        value    - entity_value,
        quantity - entity_quantity ],
   dsa_variable_set(stock_portfolio_editor_fields, Variables),
   dislog_frame_generic([
      header: 'SMS - Portfolio Editor',
      above: [],
      interface: Variables,
      below: [
         '<<'      - stock_portfolio_operation(previous),
         '>>'      - stock_portfolio_operation(next),
         'Insert'  - stock_portfolio_operation(insert),
         'Delete'  - stock_portfolio_operation(delete),
         'Update'  - stock_portfolio_operation(update),
         'Save'    - stock_portfolio_operation(save) - below,
         'Save as' - stock_portfolio_operation(save_as) ],
      close_button: [ 'Close' - right ] ]),
   !.


/* stock_portfolio_editor_ <-
      */

stock_portfolio_editor_update(Portfolio_Name) :-
   dislog_variable_set(entity_portfolio,Portfolio_Name),
   retract_all(dsa_portfolio_entity(_)),
   ( ( stock_portfolio_tuple(Portfolio),
       Portfolio_Name := Portfolio^owner,
       Entries := Portfolio^members,
       Entries \= [ ] )
   ; Entries = [ [ ] ] ),
   stock_portfolio_assert_entries(Portfolio_Name,Entries),
   stock_portfolio_editor,
   first(Entries,Entry),
   Entity := Entry*[
      name: Portfolio_Name,
      number: 1 ],
   !,
   stock_portfolio_operation_show_entity(Entity).


stock_portfolio_editor_delete(Portfolio_Name) :-
   stock_portfolio_tuple(Portfolio),
   Portfolio_Name := Portfolio^owner,
   retract(stock_portfolio_tuple(Portfolio)),
   dislog_variable_get(stock_portfolio_browser,Browser),
   findall( Name,
      stock_portfolio_tuple(Name,_),
      Portfolio_Names ),
   chain_list(Chain,Portfolio_Names),
   send(Browser,members,Chain),
   ddk_tone(success).

stock_portfolio_editor_insert :-
   ask_name('New Portfolio','Name',Portfolio_Name),
   stock_portfolio_editor_update(Portfolio_Name).

/*
stock_portfolio_editor_insert :-
   dislog_variable_set(entity_entered_name,''),
   dislog_frame_generic(Frame, [
      header: 'New Portfolio',
      above: [],
      interface: [
         'Name' - entity_entered_name ],
      below: [
         'Accept' - stock_portfolio_editor_update_sub(Frame) ],
      close_button: [
         'Cancel' - right ] ]).    

stock_portfolio_editor_update_sub(Frame) :-
   dislog_dialog_variable(get,'Name':Portfolio_Name),
   stock_portfolio_editor_update_browser(Portfolio_Name),
   send(Frame,destroy),
   stock_portfolio_editor_update(Portfolio_Name).
*/

stock_portfolio_editor_update_browser(Portfolio_Name) :-
   dislog_variable_get(stock_portfolio_browser,Browser),
%  get_chain(Browser,members,Portfolio_Names_1),
   findall( Name,
      stock_portfolio_tuple(Name,_),
      Portfolio_Names_1 ),
   list_to_ord_set([Portfolio_Name|Portfolio_Names_1],
      Portfolio_Names_2 ),
   chain_list(Chain,Portfolio_Names_2),
   send(Browser,members,Chain).


/* stock_portfolio_operation(Operation) <-
      */

stock_portfolio_operation(save_as) :-
  ask_name('Save as','Name',Portfolio_Name),
  findall( Entity_2,
      ( dsa_portfolio_entity(Entity_1),
        field_notation_project(
           [company,wkn,date,value,quantity],
           Entity_1, Entity_2 ) ),
      Entities ),
   ( ( stock_portfolio_tuple(Tuple),
       Portfolio_Name := Tuple^owner,
       !,
       retract(stock_portfolio_tuple(Tuple)) )
   ; true ),
   assert(stock_portfolio_tuple([
      owner: Portfolio_Name,
      members: Entities ])),
   stock_portfolio_editor_update_browser(Portfolio_Name),
   ddk_tone(success).

/*
stock_portfolio_operation(save_as) :-
   dislog_variable_set(entity_entered_name,''),
   dislog_frame_generic(Frame, [
      header: 'Save as',
      above: [],
      interface: [
         'Name' - entity_entered_name ],
      below: [
         'Accept' -
            stock_portfolio_operation(save_as,Frame) ],
      close_button: [
         'Cancel' - right ] ]).

stock_portfolio_operation(save_as,Frame) :-
   findall( Entity_2,
      ( dsa_portfolio_entity(Entity_1),
        field_notation_project(
           [company,wkn,date,value,quantity],
           Entity_1, Entity_2 ) ),
      Entities ),
   dislog_dialog_variable(get,'Name':Portfolio_Name),
   ( ( stock_portfolio_tuple(Tuple),
       Portfolio_Name := Tuple^owner,
       !,
       retract(stock_portfolio_tuple(Tuple)) )
   ; true ),
   assert(stock_portfolio_tuple([
      owner: Portfolio_Name,
      members: Entities ])),
   send(Frame,destroy),
   stock_portfolio_editor_update_browser(Portfolio_Name),
   ddk_tone(success).
*/

stock_portfolio_operation(save) :-
   findall( Entity_2,
      ( dsa_portfolio_entity(Entity_1),
        field_notation_project(
           [company,wkn,date,value,quantity],
           Entity_1, Entity_2 ) ),
      Entities ),
   dislog_variable_get(entity_portfolio,Portfolio_Name),
   retract_all(stock_portfolio_tuple([
      owner:Portfolio_Name, _ ])),
   assert(stock_portfolio_tuple([
      owner: Portfolio_Name,
      members: Entities ])),
   stock_portfolio_editor_update_browser(Portfolio_Name),
   ddk_tone(success).

stock_portfolio_operation(update) :-
   Entity_1 = [
      name: _,
      number: Number,
      company: Company,
      wkn: _,
      date: Date_1,
      value: Value_1,
      quantity: _ ],
   dislog_dialog_variables(get,Entity_1),
   stock_name_and_date_to_wkn_and_min_stored_date(
      Company, Date_1, Wkn, Date_3 ),
   ( ( 0 is Value_1,
       Date_2 = Date_3,
       stock_wkn_and_date_to_value(Wkn,Date_2,Value_2) )
   ; ( Value_2 = Value_1,
       Date_2 = Date_1 ) ),
   !,
   ( ( dsa_portfolio_entity(Entity_3),
       Number := Entity_3^number,
       retract(dsa_portfolio_entity(Entity_3)) )
   ; true ),
   Entity_2 := Entity_1*[
      wkn: Wkn, date: Date_2, value: Value_2 ],
   dislog_dialog_variables(send,Entity_2),
   assert(dsa_portfolio_entity(Entity_2)),
   ddk_tone(success).
stock_portfolio_operation(update).

stock_portfolio_operation(insert) :-
   stock_portfolio_max_entity_number(N),
   M is N + 1,
   dislog_variable_get(entity_portfolio,Portfolio_Name),
   stock_portfolio_default_values(Defaults),
   Entity := Defaults*[
      name: Portfolio_Name,
      number: M ],
   stock_portfolio_operation_show_entity(Entity).

stock_portfolio_operation(delete) :-
   dislog_variable_get(entity_number,N),
   dsa_portfolio_entity(Entity),
   N := Entity^number,
   ( ( M is N - 1,
       stock_portfolio_legal_entity_number(M),
       stock_portfolio_operation(previous),
       retract(dsa_portfolio_entity(Entity)) )
   ; ( M is N + 1,
       stock_portfolio_legal_entity_number(M),
       stock_portfolio_operation(next),
       retract(dsa_portfolio_entity(Entity)) )
   ; ( retract(dsa_portfolio_entity(Entity)),
       dislog_variable_get(entity_portfolio,Portfolio_Name),
       stock_portfolio_default_values(Defaults),
       Entity_New := Defaults*[
          name: Portfolio_Name,
          number: 1 ],
       dislog_variable_set(entity_number,1),
       stock_portfolio_operation_show_entity(Entity_New) ) ).


stock_portfolio_operation(previous) :-
   stock_portfolio_operation_change_entity(add(-1)).

stock_portfolio_operation(next) :-
   stock_portfolio_operation_change_entity(add(1)).

stock_portfolio_operation_change_entity(Op) :-
   dislog_variable_get(entity_number,N),
   apply(Op,[N,M]),
   stock_portfolio_legal_entity_number(M),
   dislog_variable_set(entity_number,M),
   ( ( dsa_portfolio_entity(Entity),
       M := Entity^number )
   ; stock_portfolio_operation_change_entity(Op) ),
   !,
   stock_portfolio_operation_show_entity(Entity).
stock_portfolio_operation_change_entity(_).


stock_portfolio_operation_show_entity(Entity) :-
   stock_portfolio_default_values(Defaults),
%  field_notation_to_xml(['XML':Entity]),
   dsa_variable_get(stock_portfolio_editor_fields, Variables),
   forall( X := Entity^V,
      ( dislog_dialog_variable(send,V-X),
        member(V-DV,Variables),
        dislog_variable_set(DV,X) ) ),
   forall( X := Defaults^V,
      ( \+ _ := Entity^V,
        dislog_dialog_variable(send,V-X),
        member(V-DV,Variables),
        dislog_variable_set(DV,X)
      ; true ) ).
 

stock_portfolio_assert_entries(Portfolio_Name,Entries) :-
   stock_portfolio_assert_entries(Portfolio_Name,1,Entries).
   
stock_portfolio_assert_entries(Portfolio_Name,N,[E|Es]) :-
   stock_portfolio_assert_entry(Portfolio_Name,N,E),
   M is N + 1,
   stock_portfolio_assert_entries(Portfolio_Name,M,Es).
stock_portfolio_assert_entries(_,_,[]).

stock_portfolio_assert_entry(Portfolio_Name,Number,Entry) :-
   Entity := Entry*[
      name: Portfolio_Name,
      number: Number ],
   assert(dsa_portfolio_entity(Entity)).


stock_portfolio_max_entity_number(Max) :-
   findall( N,
      ( dsa_portfolio_entity(Entity),
        N := Entity^number ),
      Ns ),
   maximum(Ns,Max).

stock_portfolio_legal_entity_number(Number) :-
   findall( N,
      ( dsa_portfolio_entity(Entity),
        N := Entity^number ),
      Ns ),
   minimum(Ns,Min),
   maximum(Ns,Max),
   interval_contains_cc([Min,Max],Number).
   
interval_contains_cc([X,Y],N) :-
   X =< N,
   N =< Y.
 

stock_wkn_and_date_to_value(Wkn,Date,Value) :-
   date_to_mysql_date(Date,Date_2),
   mysql_select_wkn_and_date_to_values(Wkn,Date_2,Values),
   !,
   Values = [Value|_].

 
stock_name_and_date_to_wkn_and_min_stored_date(
      Company, Date_1, Wkn, Date_2 ) :-
   date_to_mysql_date(Date_1,Date_3),
   mysql_select_name_and_date_to_wkns_and_dates(
      Company, Date_3, Tuples ),
   !,
   Tuples = [[Wkn,Date_4]|_],
%  writeln([Company, Date_1, Wkn, Date_4]),
   mysql_date_to_date(Date_4,Date_2).


mysql_select_wkn_and_date_to_values(Wkn,Date,Values) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   Sql_Command = [
      use:[ stock ],
      select:[ f^value ],
      from:[ DSA_Stock_Database-f ],
      where:[ f^wkn = Wkn and f^date = {Date} ] ],
   mysql_select_execute(Sql_Command,Tuples),
   append(Tuples,Values).


mysql_select_name_and_date_to_wkns_and_dates(Company,Date,Tuples) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   Sql_Command = [
      use:[ stock ],
      select:[ f^wkn, min(date) ],
      from:[ DSA_Stock_Database-f, number_name-n ],
      where:[ n^name={Company} and f^wkn = n^wkn
         and f^date >= {Date} ],
      group_by:[ f^wkn ] ],
   mysql_select_execute(Sql_Command,Tuples).


mysql_date_to_date(Date_1,Date_2) :-
   Date_1 = Y-M-D,
   name_append([D,'-',M,'-',Y],Date_3),
   name_exchange_elements(["-."],Date_3,Date_2).
 
 
date_to_mysql_date(Date_1,Date_2) :-
   name(Date_1,L_dmy),
   list_cut_at_position(["."],L_dmy,L_d),
   list_start_after_position(["."],L_dmy,L_my),
   list_cut_at_position(["."],L_my,L_m),
   list_start_after_position(["."],L_my,L_y),
   append([L_y,"-",L_m,"-",L_d],L_ymd),
   name(Date_2,L_ymd).

file_date_to_nice_day(Date,Day) :-
   date_to_day(Date,D-M-Y1),
   ( ( Y1 < 50, !,
       Y2 is Y1 + 2000 )
   ; Y2 is Y1 + 1900 ),
   possibly_add_leading_zero(M,MM),
   possibly_add_leading_zero(D,DD),
   name_append([DD,'-',MM,'-',Y2],Day_2),
   name_exchange_elements(["-."],Day_2,Day).

possibly_add_leading_zero(X,Y) :-
   X < 10,
   !,
   concat('0',X,Y).
possibly_add_leading_zero(X,X).


/******************************************************************/

 
