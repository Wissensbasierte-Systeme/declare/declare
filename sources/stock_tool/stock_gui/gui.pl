

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  XPCE Interface Main Window           ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      dislog_stock_advisor/1,
      dislog_stock_advisor/2.

dsa :-
   ddk_tone(success),
   dislog_variable_set(mysql_result_file_prune, no),
   sms.

sms :-
   dislog_stock_advisor(close_main_window),
   dislog_stock_advisor(set_environment),
   dislog_stock_advisor(open_main_window).


dislog_stock_advisor(set_environment) :-
   rgb_to_colour([6,6,6],Dsa_Chart_Colour),
   rgb_to_colour([14,14,14],Browser_Colour),
   dislog_variables_set([
      chart_display_mode - xpce,  % postscript, gnuplot
      chart_display_mode_xpce - regular,
      max_ppm_file - 1,
      act_ppm_file - 1,
      dsa_visualisation_window_exists - 0,
      dsa_logscale_y - 0,
      dsa_pic_size - [750,600],
      oscilator - 0,
      dsa_max_iterations - 50,
      place_holder_for_vars - xxx,
      dsa_chart_picture - @ddd_p1,
      dsa_chart_frame - @ddd_f,
      dsa_menu_bar1 - @ddd_menu_bar1,
      dsa_copy_right_dialog - @ddd_d1,
      dsa_chart_colour - Dsa_Chart_Colour,
      dsa_chart_background_colour - Browser_Colour,
%     dsa_browser_colour - colour('yellow'),
      dsa_browser_colour - Browser_Colour,
      xpce_stock - 'Siemens' ]),
   dislog_counter(init,gif_file),
   dislog_counter(init,dsa_display_cache).


dislog_stock_advisor(set_default_parameters) :-
   dislog_variable_get(dax_date_default,Date),
   dislog_variables_set([
      chart_mode - regular,
      chart_display_mode - postscript,
      chart_display_mode_xpce - regular,
      dax_date - Date,
      smooth_parameter - 3,
      dsa_max_iterations - 50 ]).


dislog_stock_advisor(close_main_window) :-
   dislog_stock_advisor(close_visualisation_window),
   checklist( free,
      [ @ddd_f, @ddd_d1, @ddd_d2,
        @ddd_menu_bar1, @ddd_menu_bar2,
        @br1, @ddd_p1 ] ),
   dislog_flag_set(stock_announcement,0).


dislog_stock_advisor(open_main_window) :-
   concat('SMS - Stock Management System: ',
      'by D. Seipel, Copyright 1998-2004', CR_Text),
   ddk_version(Version),
   concat(['Stock Management System (Declare ',
      Version, ')'], Frame_Text),
   dislog_variable_get(dsa_chart_frame,Frame),
   dislog_variable_get(dsa_menu_bar1,Menu_Bar),
   dislog_variable_get(dsa_copy_right_dialog,CR_Dialog),
   new(Frame,frame(Frame_Text)),
   dislog_stock_advisor(create_picture,
      Picture_Block - Text_Item - Browser ),
   new(CR_Dialog,dialog(CR_Text)),
   send(CR_Dialog,append,new(Menu_Bar,menu_bar)),
   checklist( dislog_stock_advisor(send_pulldown_menu,Menu_Bar),
      [ 'File', 'Edit', 'History', 'Tools', 'Window', 'Help' ] ),
%  dislog_stock_advisor(create_fb_buttons,Menu_Bar),
   send(CR_Dialog,above,Picture_Block),
   send(Frame,append,Picture_Block),
   dislog_stock_advisor(add_shortcut_bar,
      Picture_Block - Text_Item - Browser ),
   send(Frame,open).


dislog_stock_advisor(create_fb_buttons,Menu_Bar) :-
   send(new(Dialog,dialog),right,Menu_Bar),
   send_append(Dialog, '<<' - change_display_stock(backward) ),
   send_append(Dialog, '>>' - change_display_stock(forward) ),
   send_append(Dialog, 'History' - dsa_history ).


dislog_stock_advisor(create_picture,
      Navigation_Hierarchy - Text_Item - Browser ) :-
   dsa_stock_tree_prepare,
   new(Navigation_Hierarchy,ds_hierarchy(' ')),
   dislog_variable_set(
      dsa_navigation_hierarchy, Navigation_Hierarchy ),
   send(Navigation_Hierarchy,width,220),

   dislog_variable_get(dsa_chart_background_colour,Browser_Colour),
   send(new(Dialog_T,dialog),below,Navigation_Hierarchy),
   send(Dialog_T,width,220),
   send(Dialog_T,background,Browser_Colour),
   send(Dialog_T,append,new(Text_Item,text_item('Company',''))),
   send(Text_Item,label_font,font(helvetica,roman,12)),
   send(Text_Item,value_width,120),
%  send(Dialog_T,size,size(200,40)),
%  send(new(Dialog_I,dialog),below,Dialog_T),
   Dialog_I = Dialog_T,
   send(Dialog_I,width,220),
%  dislog_variable_get(dsa_browser_colour,Browser_Colour),
   send(Dialog_I,background,Browser_Colour),
   send(Dialog_I,append,new(Browser,list_browser)),
%  send(new(Browser,list_browser),below,Dialog_T),
   ( dislog_variable_get(screen_size, small) ->
     Size = size(30,5)
   ; Size = size(30,7) ),
   send(Browser,size,Size),
   dislog_variable_set(stock_search_browser,Browser),
   send(Browser,label,'Wkn: Company'),
   send(Browser,show_label,false),

   dislog_stock_advisor(
      create_picture_sub, Navigation_Hierarchy ),

   send(Navigation_Hierarchy,expand_root).

dislog_stock_advisor(create_picture_sub,Navigation_Hierarchy) :-
   dislog_variable_get(dsa_chart_picture,Picture),
   send(new(Picture,picture),right,Navigation_Hierarchy),
%  send(new(Picture,doc_window),right,Navigation_Hierarchy),
   dislog_variable_get(dsa_pic_size,[X,Y]),
   send(Picture,size,size(X,Y)),
   dislog_variable_get(dsa_chart_colour,Colour),
   dislog_variable_get(dsa_chart_background_colour,Background_Colour),
   send(Picture,colour,Colour),
   send(Picture,background,Background_Colour),
   display_logo_bitmap.


dislog_stock_advisor(append_announcement_windows) :-
   dislog_flag_get(stock_announcement,0),
   send(@ddd_d2,append,new(@br1,list_browser),below),
   send(@br1,size,size(120,4)),
   send(@br1,label,'DSA Protocol'),
   dislog_flag_set(stock_announcement,1).
dislog_stock_advisor(append_announcement_windows).


dislog_stock_advisor(add_shortcut_bar,
      Picture_Block - Text_Item - Browser ) :-
   new(@ddd_d2,dialog),
   checklist( send_append(@ddd_d2), [
      'Search' - mysql_select_names_like_to_browser(
         Browser, Text_Item?selection ),
      'Chart' - chart_for_search_name(Browser?selection?key),
      'Table' - mysql_select_by_name_to_table_for_search(
         Browser?selection?key) ] ),
   chain_list(Chain,[]),
   send(Browser,members,Chain),
%  send(@ddd_d2,gap,size(30,0)),
   send(@ddd_d2,append,
      new(Menue_1,menu('        Curve',cycle)),right),
   send(Menue_1, label, '       '),
   send_list(Menue_1,append,[points,lines]),
   send(@ddd_d2,append,
      new(Menue_2,menu('Curve',cycle)),right),
   send(Menue_2, label, ''),
   send_list(Menue_2,append,[values,percents,weights]),
   ( dislog_variable_get(screen_size, small) ->
     Average = 'Avg'
   ; Average = 'Average' ),
   send(@ddd_d2,append,
      new(Menue_3,menu(Average,cycle)),right),
   send_list(Menue_3,append,[yes,no]),
   send(@ddd_d2,append,
      new(Menue_4,int_item('Smooth',low:=0,high:=50)),right),
%  send(@ddd_d2,append,button('Update',
%     message(@prolog,dsa_update_some_dislog_variables,
%        Menue_1,Menue_2,Menue_3,Menue_4)),right),
   dsa_variable_set(
      dsa_chart_parameter_menues, Menue_1-Menue_2-Menue_3-Menue_4),
   new(@ddd_d3,dialog),
   send(@ddd_d3,right,@ddd_d2),
   Status_Bar_Picture = @dsa_status_barpic,
   free(Status_Bar_Picture),
   send(@ddd_d3,append,
      new(Status_Bar_Picture,picture)),
   rgb_to_colour([14,14,14],Colour),
   send(Status_Bar_Picture, background, Colour),
   send(Status_Bar_Picture, size, size(151,21)),
   send(@ddd_d2,below,Picture_Block),
   dislog_variables_get([
      curve_style - S1,
      curve_type - S2,
      show_avg_line - S3,
      smooth_parameter - S4 ]),
   send(Menue_1,selection,S1),
   send(Menue_2,selection,S2),
   send(Menue_3,selection,S3),
   send(Menue_4,selection,S4).
      
dsa_update_some_dislog_variables :-
   dsa_variable_get(dsa_chart_parameter_menues, M1-M2-M3-M4),
   dsa_update_some_dislog_variables(M1, M2, M3, M4).

dsa_update_some_dislog_variables(M1,M2,M3,M4) :-
   get(M1,selection,S1),
   get(M2,selection,S2),
   get(M3,selection,S3),
   get(M4,selection,S4),
   dislog_variables_set([
      curve_style - S1,
      curve_type - S2,
      show_avg_line - S3,
      smooth_parameter - S4 ]).

/*
dislog_stock_advisor(add_shortcut_bar,Picture_Block) :-
   dislog_flag_get(stock_announcement,0),
   send(new(@ddd_d2,dialog),below,Picture_Block),
   Menu_Bar = @ddd_menu_bar2,
   send(@ddd_d2,append,new(Menu_Bar,menu_bar)),
   checklist(
      dislog_stock_advisor(send_pulldown_menu,Menu_Bar),
      [ 'Data Sources',
        'Analysis of Branches',
        'Analysis of Groups',
%       'Visualisation',
        'Analysis of Portfolios' ] ),
%  dislog_stock_advisor(append_announcement_windows),
   dislog_variable_get(dsa_chart_frame,Frame),
   send(Frame,fit).
*/


dislog_stock_advisor(remove_shortcut_bar) :-
   dislog_variable_get(dsa_chart_frame,Frame),
   dislog_flag_get(stock_announcement,1),
   send(Frame,delete,@ddd_d2),
   checklist( free, [ @ddd_d2, @ddd_menu_bar2, @br1 ] ),
   dislog_flag_set(stock_announcement,0),
   send(Frame,fit).


dislog_stock_advisor(clear_display_stock_bitmap) :-
   free(@bm).


dislog_stock_advisor(open_visualisation_window) :-
   dislog_variable_get(dsa_visualisation_window_exists,0),
   new(@ddd_fm,frame('DisLog Stock Mining')),
   dislog_flag_set(colon_width,20),
   dislog_flag_set(x_coord,0),
   send(@ddd_fm,append,
      new(@ddd_d3,dialog('DSM - DisLog Stock Mining'))),
   send(@ddd_d3,append,button('Close',
      message(@prolog,
         dislog_stock_advisor,close_visualisation_window))),
   name_append([
      'Stock Tool''s\n Visualisation Windows\n ',
      'for Data Mining\n by D. Seipel and A. Pillo'],
      About_Message ),
   send(@ddd_d3,append,button('About',
      message(@display,inform,About_Message))),
   send(new(@ddd_p2,picture('Stock Analysis')),below,@ddd_d3),
   send(@ddd_p2,size,size(800,520)),
   send(@ddd_p2,open_centered),
   dislog_variable_set(dsa_visualisation_window_exists,1).
dislog_stock_advisor(open_visualisation_window).


dislog_stock_advisor(close_visualisation_window) :-
%  send(@ddd_p2,destroy),
   free(@ddd_p2), free(@ddd_d3), free(@ddd_fm),
   dislog_variable_set(dsa_visualisation_window_exists,0).


dislog_stock_advisor(clear_display_window) :-
   dislog_variable_get(dsa_chart_picture,Picture),
   send(Picture,clear).


/******************************************************************/


