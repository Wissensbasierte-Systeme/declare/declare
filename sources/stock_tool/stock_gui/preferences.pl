

/******************************************************************/
/***                                                            ***/
/***        Stock Tool:  Preferences                            ***/
/***                                                            ***/
/******************************************************************/



/* dsa_dialog('DSA Preferences') <-
      */

dsa_dialog('DSA Preferences') :-
   new(Frame,frame('SMS - Preferences')),
   send(Frame,append,new(Dialog_I,dialog)),
   dislog_variable_get(dsa_browser_colour,Colour),
   send(Dialog_I,background,Colour),
   checklist( send_choice_menu_gen(Dialog_I), [
      'Show Curve as' - curve_style - points - lines,
      'Show Curve as' - curve_type - values - percents,
      'Show Average Line' - show_avg_line - yes - no,
%     'Average Line Colour' - avg_line_colour - orange - indianred,
      'Font Size' - font_size - 18 - 14 ] ),
   send_list(Dialog_I,append,[
      new(S,int_item(
         'Smooth Parameter', low:=0, high:=100 )),
      new(AS,int_item(
         'Smooth Parameter Avg', low:=0, high:=100 )) ]),
   dislog_variable_get(smooth_parameter,X),
   send(S,selection,X),
   dislog_variable_get(smooth_parameter_avg,Y),
   send(AS,selection,Y),
%  checklist( send_text_menu(Dialog_I), [
%     'Smooth Parameter' - smooth_parameter - S,
%     'Smooth Parameter Avg' - smooth_parameter_avg - AS ] ),
   send(new(Dialog,dialog),below,Dialog_I),
   send(Dialog,append,button(ok,message(Dialog,return,ok))),
   send(Dialog,append,button(defaults,
      message(Dialog,return,defaults))),
   send(Dialog,append,button('Admin',
      message(@prolog,dsa_dialog,'DSA Preferences - Administrator'))),
   send(Dialog,append,button(cancel,message(Dialog,return,cancel))),
   send(Dialog,default_button,ok),
   send(Frame,transient_for,@ddd_f),
   repeat,
      get(Dialog,confirm_centered,@ddd_f?area?center,Answer),
      ask_general_stock_tool_settings(Answer,Dialog,S,AS).
      
ask_general_stock_tool_settings(ok,Dialog,S,AS) :-
   get(S,selection,S2),
   get(AS,selection,AS2),
   float_between(0,100,S2),
   float_between(0,200,AS2),
   !,
   dislog_variables_set([
      smooth_parameter - S2,
      smooth_parameter_avg - AS2 ]),
   send(Dialog,destroy).
ask_general_stock_tool_settings(ok,Dialog,_,_) :-
   Output_String =
      'Please enter smooth parameters between 0..100',
   send(Dialog,report,error,Output_String),
   !,
   fail.
ask_general_stock_tool_settings(_,Dialog,_,_) :-
   send(Dialog,destroy).


/* dsa_dialog('DSA Preferences - Administrator') <-
      */

dsa_dialog('DSA Preferences - Administrator') :-
   new(Dialog,dialog('SMS - Preferences - Administrator')),
   checklist( send_choice_menu_gen(Dialog), [
      'Storage Mode' - storage_mode - mysql - dislog - xml,
      'MySQL Database' - dsa_stock_database - finanztreff - test,
      'Chart Mode' - chart_mode - regular - weighted,
      'Display Mode' - chart_display_mode - xpce - postscript - gnuplot,
      'XPCE Mode' - chart_display_mode_xpce - regular - percent,
      'Y-Axis Mode' - y_axis_mode - logarithmic - linear,
      'Portfolio' - portfolio - no - p1,
      'Start of Chart' - chart_start - 1997 - 1998 - 1999 - 2000 - 2001,
      'Stock Data' - stock_all_long - finanztreff - comdirekt ] ),
   checklist( send_text_menu(Dialog), [
      'Date' - dax_date - D,
      'Max Iterations' - dsa_max_iterations - I ] ),
   send(Dialog,append,button(ok,message(Dialog,return,ok))),
   send(Dialog,append,button(defaults,
      message(Dialog,return,defaults))),
   send(Dialog,append,button(cancel,message(Dialog,return,cancel))),
   send(Dialog,default_button,ok),
   send(Dialog,transient_for,@ddd_f),
   repeat,
      get(Dialog,confirm_centered,@ddd_f?area?center,Answer),
      ask_general_stock_tool_settings_admin(Answer,Dialog,I,D).

ask_general_stock_tool_settings_admin(ok,Dialog,I,D) :-
   get(I,selection,I2),
   get(D,selection,D2),
   float_between(0,1000000,I2),
   !,
   dislog_variables_set([
      dsa_max_iterations - I2,
      dax_date - D2 ]),
   send(Dialog,destroy).
ask_general_stock_tool_settings_admin(ok,Dialog,_,_) :-
   Output_String =
      'Please enter an iteration delimiter between 0..1000000',
   send(Dialog,report,error,Output_String),
   !,
   fail.
ask_general_stock_tool_settings_admin(defaults,Dialog,_,_) :-
   dislog_stock_advisor(set_default_parameters),
   send(Dialog,destroy),
   dsa_dialog('DSA Preferences').
ask_general_stock_tool_settings_admin(_,Dialog,_,_) :-
   send(Dialog,destroy).


/******************************************************************/


