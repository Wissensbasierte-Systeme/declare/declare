

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  XPCE Data Cleaning Manager           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* stock_data_cleaning_manager <-
      */

stock_data_cleaning_manager :-
   new(Frame,frame('SMS - Data Cleaning Manager')),
   send(Frame,append,new(Dialog_T,dialog)),
   send(Dialog_T,append,new(Text_Item,text_item('Name',''))),
%  send(Dialog_T,size,size(250,40)),
   send(new(Dialog_I,dialog),below,Dialog_T),
   dislog_variable_get(dsa_browser_colour,Colour),
   send(Dialog_I,background,Colour),
   send(Dialog_I,append,new(Browser,list_browser)),
   send(Browser,size,size(40,10)),
%  dislog_variable_set(stock_search_browser,Browser),
   send(Browser,label,'Wkn: Company'),
   send(new(Dialog,dialog),below,Dialog_I),
   checklist( send_append(Dialog), [
      'Search' - mysql_select_names_like_to_browser(
         Browser, Text_Item?selection ),
      'Chart' - chart_for_search_name(Browser?selection?key),
      'Table' - mysql_select_by_name_to_table_for_search(
         Browser?selection?key),
      'Clear Cache' - stock_values_cached_clear(
         Browser?selection?key),
      'Guess Splits' - mysql_select_big_changes_to_table_for_cleaning(
         Browser?selection?key) - below,
      'Manage Splits' -
         stock_manage_splits_dialog(
            'SMS - Manage Splits', Browser?selection?key ),
      'Show Changes' - xpce_display_changes_table - below,
      'Show Updates' - xpce_display_updates_table,
      'Close' - send(Frame,destroy) ] ),
   chain_list(Chain,[]),
   send(Browser,members,Chain),
   send(Frame,open).
      

/* mysql_select_big_changes_to_table_for_cleaning(Name) <-
      */

mysql_select_big_changes_to_table_for_cleaning(Name) :-
   name_start_after_position([": "],Name,Company),
   mysql_select_big_changes_to_table(Company).


/* stock_values_cached_clear(Name) <-
      */

stock_values_cached_clear(Name) :-
   name_start_after_position([": "],Name,Company),
   ( retract(stock_values_cached(_-[Company],_))
   ; true ).


/* stock_manage_splits_dialog(Label,Wkn_and_Name) <-
      */

stock_manage_splits_dialog(Label,Wkn_and_Name) :-
   name_start_after_position([": "],Wkn_and_Name,Company),
   name_cut_at_position([":"],Wkn_and_Name,Wkn),
   dislog_variables_set([
      stock_cleaning_name - Company,
      stock_cleaning_wkn - Wkn,
      stock_cleaning_date - 'DD.MM.YYYY',
      stock_cleaning_rate - 1.0 ]),
   dislog_frame_generic([
      header: Label,
      above: [],
      interface: [
         'Name' - stock_cleaning_name,
         'Wkn' - stock_cleaning_wkn,
         'Date' - stock_cleaning_date,
         'Rate' - stock_cleaning_rate ],
      below: [
         'Show Changes' - xpce_display_changes_table_one,
         'Show Updates' - xpce_display_updates_table_one,
         'Correct All Splits' - stock_data_change_all - below,
         'Correct One Split' - stock_data_change_one ],
      close_button: [
         'Close' - right ] ] ).
      

/* stock_data_change_all <-
      */

stock_data_change_all :-
   dislog_dialog_variables( get, [
      'Name' - Company ]),
   stock_data_change_values(Company).


/* stock_data_change_one <-
      */

stock_data_change_one :-
   dislog_dialog_variables( get, [
      'Name' - Company,
      'Wkn' - Wkn,
      'Date' - Date,
      'Rate' - Rate ]),
   ( ( ensure_date_is_sql_date(Date,SQL_Date),
       writeln(user,stock_data(change_value,[Wkn,SQL_Date,Rate])),
       add_stock_data_change_value_to_change_file(Wkn,SQL_Date,Rate),
       add_stock_multiply_tuple_to_update_file(Company,Wkn,Rate),
       stock_data(change_value,[Wkn,SQL_Date,Rate]) )
   ; true ).

ensure_date_is_sql_date(Date,SQL_Date) :-
   name_remove_elements(" ",Date,Date_2),
   !,
   name('.',[Dot]),
   ( ( name(Date_2,[D1,D2,Dot,M1,M2,Dot,Y1,Y2,Y3,Y4]),
       name(D,[D1,D2]), integer(D),
       name(M,[M1,M2]), integer(M),
       name(Y,[Y1,Y2,Y3,Y4]), integer(Y),
       name(' ',[Blank]),
       name('-',[Minus]),
       name( SQL_Date,
          [Blank,Y1,Y2,Y3,Y4,Minus,M1,M2,Minus,D1,D2,Blank] ) )
   ; ( write_list(user,
          ['Error: ',Date,' is not a correct SQL date !']),
       nl(user),
       !,
       fail ) ).


/* xpce_display_updates_table <-
      */

xpce_display_updates_table_one :-
   xpce_display_updates_table_common(Rows),
   dislog_dialog_variables( get, [
      'Wkn' - Wkn ] ),
   atom_number(Wkn, Wkn_2),
   findall( [Company,Wkn_2,A,B,Rate],
      member([Company,Wkn_2,A,B,Rate],Rows),
      Rows_Selected ),
   xpce_display_table(_,_,'Updates',
      ['Company','Wkn','From','To','Rate'], Rows_Selected ).

xpce_display_updates_table :-
   xpce_display_updates_table_common(Rows),
   xpce_display_table(_,_,'Updates',
      ['Company','Wkn','From','To','Rate'], Rows ).

xpce_display_updates_table_common(Rows) :-
   stock_file( sources_directory,
      'stock_knowledge_base/updates', File ),
   consult(File),
   mysql_select_number_name(State),
   findall( [Company,Wkn,A,B,Rate],
      ( stock_multiply_tuple_nice(Company,Wkn,Interval,Rate),
        day_interval_to_sql_date_interval(Interval,[A,B]),
        stock_number_to_first_name(State,Wkn,Company) ),
      Rows ).
      
stock_multiply_tuple_nice(Company,Wkn,Interval,Rate) :-
   stock_multiply_tuple(Company,Wkn,Interval,Rate).
stock_multiply_tuple_nice(Company,Wkn,'--',Rate) :-
   stock_multiply_tuple(Company,Wkn,Rate),
   \+ stock_multiply_tuple(Company,Wkn,_,Rate).

day_interval_to_sql_date_interval(Interval,[A,B]) :-
   Interval = [Day_1,Day_2],
   day_to_sql_date_2(Day_1,A),
   day_to_sql_date_2(Day_2,B).


/* xpce_display_changes_table <-
      */

xpce_display_changes_table_one :-
   xpce_display_changes_table_common(Rows),
   dislog_dialog_variables( get, [
      'Wkn' - Wkn ] ),
   atom_number(Wkn, Wkn_2),
   findall( [Company,Wkn_2,Date,Rate],
      member([Company,Wkn_2,Date,Rate],Rows),
      Rows_Selected ),
   xpce_display_table(_,_,'Changes',
      ['Company','Wkn','Date','Rate'], Rows_Selected ).

xpce_display_changes_table :-
   xpce_display_changes_table_common(Rows),
   xpce_display_table(_,_,'Changes',
      ['Company','Wkn','Date','Rate'], Rows ).

xpce_display_changes_table_common(Rows) :-
   stock_file( sources_directory,
      'stock_knowledge_base/changes', File ),
   consult(File),
   mysql_select_number_name(State),
   findall( [Company,Wkn,Date,Rate],
      ( stock_data_change_value([Wkn,SQL_Date,Rate]),
        mysql_date_name_to_date(SQL_Date,Date),
        stock_number_to_first_name(State,Wkn,Company) ),
      Rows ).
        

/* mysql_date_name_to_date(SQL_Date,Date) <-
      */

mysql_date_name_to_date(SQL_Date,Date) :-
   name(SQL_Date,L_ymd),
   list_cut_at_position(["-"],L_ymd,L_y),
   list_start_after_position(["-"],L_ymd,L_md),
   list_cut_at_position(["-"],L_md,L_m),
   list_start_after_position(["-"],L_md,L_d),
   append([L_d,".",L_m,".",L_y],L_dmy),
   name(Date,L_dmy).


/* stock_number_to_first_name(State,Wkn,Company) <-
      */

stock_number_to_first_name(State,Wkn,Company) :-
   member([number_name(Wkn,Company)],State),
   !.


/******************************************************************/


