

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Chart History Button                 ***/
/***                                                            ***/
/******************************************************************/     


:- dynamic
      dsa_display_cache_alias/2.
      

dsa_history :-
   findall( Browser_Item,
      ( dsa_display_cache(_,Goal),
        Goal =.. [_,Cache_Item|_],
        dsa_cache_item_to_browser_item(Cache_Item,Browser_Item) ),
      Browser_Items ),
   ( Browser_Items = []
   ; dsa_history_with_items(Browser_Items) ).

dsa_history_with_items(Browser_Items) :-
%  new(Frame,frame('History')),
   new(Frame,dialog('History')),
   send(Frame,append,new(Browser,browser)),
   dislog_variable_get(dislog_frame_interface_color,Colour),
   send(Browser,background,Colour),
%  send(Browser,width,80),
   send(Browser,label,'Previous'),
%  send(new(Dialog,dialog_group(buttons,group)),below,Browser),
%  send(new(Dialog,dialog),below,Browser),
   send(Frame,append,new(Dialog,dialog_group(buttons,group))),
   checklist( send_append(Dialog), [
      'Display' - dsa_cache_view(Browser?selection?key),
      'Close' - dsa_history_close(Frame) ] ),
   chain_list(Chain,Browser_Items),
   send(Browser,members,Chain),
   send(Frame,open).

dsa_history_close(Frame) :-
   retractall(dsa_display_cache_alias(_,_)),
   send(Frame,destroy).


dsa_cache_item_to_browser_item(Cache_Item,Browser_Item) :-
   is_list(Cache_Item),
   !,
   dsa_cache_items_append(Cache_Item,Browser_Item),
   assert(dsa_display_cache_alias(Cache_Item,Browser_Item)).
dsa_cache_item_to_browser_item(Cache_Item,Cache_Item).


dsa_cache_items_append([C1,C2|Cs],B) :-
   dsa_cache_items_append([C2|Cs],A),
   name_append([C1,'-',A],B).
dsa_cache_items_append([C],C).
dsa_cache_items_append([],'').

 
dsa_cache_view(Item) :-
   ( dsa_display_cache_alias(Cache_Item,Item)
   ; Cache_Item = Item ),
   dsa_display_cache(M,Goal),
   dislog_variable_set(act(dsa_display_cache),M),
   Goal =.. [_,Cache_Item|_],
   !,
   call(Goal).


/******************************************************************/     


