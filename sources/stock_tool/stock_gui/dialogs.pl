

/******************************************************************/
/***                                                            ***/
/***        Stock Tool:  XPCE Interface Stock Dialogs           ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      dsa_dialog/1.


/*** interface ****************************************************/


dsa_dialog(hoppenstedt) :-
   dislog_variable_set(stock_all_long,hoppenstedt),
   new(Window_Address,dialog('Hoppenstedt - Stock Data')),
   dsa_create_create_menu_bars(Window_Address,[
      [ 'DAX', number_name_hs_menue, 'dax',
        [a-b,c-d,e-f,g-k,l-r,s-z] ],
      [ 'Germany', number_name_hs_menue, 'germany',
        [a-b,c-d,e-f,g-k,l-r,s-z] ],
      [ 'England', number_name_hs_menue, 'england',
        [a-b,c-d,e-f,g-k,l-r,s-z] ],
      [ 'Europe G', number_name_hs_menue, 'europe_g',
        [a-b,c-d,e-f,g-k,l-r,s-z] ],
      [ 'Europe R', number_name_hs_menue, 'europe_r',
        [a-b,c-f,g-k,l-r,s-t,u-z] ],
      [ 'International', number_name_hs_menue, 'international',
        [a-a,b-c,d-g,h-m,n-s,t-z] ] ]),
   send(Window_Address,append,button(close,
      message(Window_Address,destroy))),
   send(Window_Address,open).

dsa_dialog(finanztreff) :-
   dislog_variable_set(stock_all_long,finanztreff),
   new(Window_Address,dialog('Finanztreff - Stock Data')),
   dsa_create_create_menu_bars(Window_Address,[
      [ 'Dax Werte', number_name_ft_menue, 'dax_100',
        [a-b,c-d,e-f,g-k,l-r,s-z] ],
      [ 'Neuer Markt', number_name_ft_menue, 'neuermarkt',
        [a-b,c-d,e-f,g-k,l-r,s-z] ],
      [ 'Euro Stock', number_name_ft_menue, 'eurostock',
        [a-b,c-d,e-f,g-k,l-r,s-z] ],
      [ 'Other', number_name_ft_menue, 'unknown',
        [a-b,c-d,e-f,g-k,l-r,s-z] ] ]),
   send(Window_Address,append,button(close,
      message(Window_Address,destroy))),
   send(Window_Address,open).


dsa_dialog(comdirekt) :-
   dislog_variable_set(stock_all_long,comdirekt),
   new(Window_Address,dialog('Comdirekt - Stock Data')),
   dsa_create_create_menu_bars(Window_Address,[
      [ 'Dax 100', number_name_cd_menue, 'dax100_export',
        [a-b,c-d,e-f,g-k,l-r,s-z] ],
      [ 'Neuer Markt', number_name_cd_menue, 'neuer_markt_export',
        [a-b,c-d,e-f,g-k,l-r,s-z] ],
      [ 'Euro 50', number_name_cd_menue, 'euro50_export',
        [a-b,c-d,e-f,g-k,l-r,s-z] ],
      [ 'Pharma', number_name_cd_menue, 'pharma_export',
        [a-b,c-d,e-f,g-k,l-r,s-z] ],
      [ 'Ausland', number_name_cd_menue, 'ausland_export',
        [a-a,b-b,c-c,d-g,h-k,m-n] ],
      [ '--', number_name_cd_menue, 'ausland_export',
        [o-r,s-s,t-z] ] ]),
   send(Window_Address,append,button(close,
      message(Window_Address,destroy))),
   send(Window_Address,open).


dsa_dialog('Analysis of Stock Groups') :-
   new(Dialog,dialog('Analysis of Stock Groups')),
   send(Dialog,append,
      new(Menu,menu('Available Groups',choice))),
%  send(Menu,label_area,area(5,5,20,20)),
   send(Menu,layout,vertical),
   send(Menu,gap,size(10,10)),
   findall( Stock_Groups,
      stock_tuple(Stock_Groups,_), Items ),
   send(Dialog,append,new(Browser,list_browser),right),
   checklist( dsa_append_item_to_menu(Menu,Browser), Items ),
   send(Browser,append,dict_item('...')),
   send(Browser,size,size(25,20)),
   send(Dialog,append,button(close,
      message(Dialog,destroy))),
   send(Dialog,gap,size(10,10)),
   send(Dialog,append,button(analyse,
      message(@prolog,
         dsa_dereference_and_execute,chart,Menu))),
   send(Dialog,append,button(draw_colons,
      message(@prolog,
         dereference_and_execute,chart,Menu))),
   send(Dialog,open).


dsa_append_item_to_menu(Menu,Browser,Item) :-
   send(Menu,append,menu_item(Item,
      message(@prolog,dsa_tell_me_more,Menu,Browser))).

dereference_and_execute(Predicate,Reference) :-
   get(Reference,selection,Argument),
%  writeln(user,[Predicate,Reference]),
   Goal =.. [Predicate,Argument],
   call(Goal).


dsa_tell_me_more(Menu,Browser) :-
   send(Browser,clear),
   get(Menu,selection,Company_or_Group),
   findall( Name_2,
      ( stock_group(Name_1,Company_or_Group),
        name_html_to_text(Name_1,Name_2) ),
      Name_List ),
   checklist( append_dict_item_to_browser(Browser),
      Name_List ).

append_dict_item_to_browser(Browser,Item) :-
   send(Browser,append,dict_item(Item)).


save_chart_file :-
   stock_announcement('Save chart file not implemented yet ...').


/*** currently not used *******************************************/


dsa_dialog('Choice Mode') :-
   new(Dialog,dialog('Choice Mode')),
   send(Dialog,append,new(Choice_Mode, menu('Mode',choice)),below),
   send_multiple(Choice_Mode,[
      gap:size(5,5),
      layout:horizontal,
      alignment:left,
      append:menu_item(regular,message(@prolog,
         dislog_variable_set,chart_mode,regular)),
      append:menu_item(weighted,message(@prolog,
         dislog_variable_set,chart_mode,weighted)) ]),
   send(Dialog,append,button(ok,message(Dialog,return,ok))),
   send(Dialog,append,button(cancel,message(Dialog,return,cancel))),
   repeat,
   get(Dialog,confirm_centered,@ddd_f?area?center,Answer),
   (   Answer == ok
   ->  send(Dialog,destroy)
   ;   fail ).


dsa_dialog('Smooth Parameter') :-
   new(Dialog,dialog('Smooth Parameter')),
   send_multiple(Dialog,[
      append:new(SP,text_item(smooth_par,7)),
      append:button(ok,message(Dialog,return,ok)),
      append:button(cancel,message(Dialog,return,cancel)) ]),
   send(Dialog,default_button,ok),
   send(Dialog,transient_for,@ddd_f),
   repeat,
   get(Dialog,confirm_centered,@ddd_f?area?center,Answer),
   (   Answer == ok
   ->  get(SP,selection,Smooth_Par),
       (  float_between(0,100,Smooth_Par)          
       -> send(Dialog,destroy)
       ;  send(Dialog,report,error,
             'Please enter a number between 0..100'),
          fail )           
   ;   !,                           
       send(Dialog,destroy),   
       fail ). 


dsa_dialog('Iterations Delimiter') :-
   new(Dialog,dialog('Iterations Delimiter')),
   send_multiple(Dialog,[
      append:new(IT,text_item(max_iterations, 50)),
      append:button(ok,message(Dialog,return,ok)),
      append:button(cancel,message(Dialog,return,cancel)) ]),
   send(Dialog,default_button,ok),
   send(Dialog,transient_for,@ddd_f),
   repeat,
   get(Dialog,confirm_centered,@ddd_f?area?center,Answer),
   (   Answer == ok
   ->  get(IT,selection,Iterations),
       (  float_between(0,1000000,Iterations)
       -> send(Dialog,destroy)
       ;  send(Dialog,report,error,
             'Please enter a number between 0..1.000.000'),
          fail )
   ;   !,
       send(Dialog,destroy),
       fail ).


dsa_dialog('Chart Display Mode') :-
   new(Dialog,dialog('Chart Display Mode')),
   send(Dialog,append,new(Display_Mode,
      menu('Display Mode',choice)),below),
   send_multiple(Display_Mode,[
      gap:size(5,5),
      layout:horizontal,
      alignment:left,
      append:menu_item(postscript,
         message(@prolog,dislog_variable_set,
            chart_display_mode,postscript)),
      append:menu_item(gnuplot,
         message(@prolog,dislog_variable_set,
            chart_display_mode,gnuplot)) ]),
   send_multiple(Dialog,[
      append:button(ok,message(Dialog,return,ok)),
      append:button(cancel,message(Dialog,return,cancel)) ]),
   repeat,
   get(Dialog,confirm_centered,@ddd_f?area?center,Answer),
   (   Answer == ok
   ->  send(Dialog,destroy)
   ;   fail
   ).


dsa_dialog('Tps Mode') :-
   new(Dialog,dialog('Tps Mode')),
   send(Dialog,append,
      new(Menu,menu('Tps Mode',choice))),
   send_multiple(Menu,[
      gap:size(5,5),
      label_format:right,
      alignment:left,
      layout:vertical,
      append:menu_item(tps_default,message(@prolog,
         write,'tps_default')),
      append:menu_item(tps_pfunc,message(@prolog,
         write,'tps_pfunc')),
      append:menu_item(tps_prob,message(@prolog,
         write,'tps_prob'))]),
   send(Dialog,append,button(close,
      message(Dialog,destroy))),
   send(Dialog,open).


dsa_dialog('Annotation Mode') :-
   new(Dialog,dialog('Annotation Mode')),
   send(Dialog,append,
      new(Menu,menu('Annotation Mode',choice))),
   send_multiple(Menu,[
      gap:size(5,5),
      label_format:right,
      alignment:left,
      layout:vertical,
      append:menu_item('Atom',message(@prolog,
         dislog_flag_set,a_mode,atom)),
      append:menu_item('Formula',message(@prolog,
         dislog_flag_set,a_mode,formula)),
      append:menu_item('Rule',message(@prolog,
         dislog_flag_set,a_mode,rule))]),
   send(Dialog,append,button(close,
      message(Dialog,destroy))),
   send(Dialog,open).


/******************************************************************/


