

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  XPCE Interface Utilities             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


ddd_save :-
   ask_name('Save Chart to File','Name',File),
   chart_print(File).

 
chart_print(File) :-
   stock_file(results,File,Path),
%  write(user,'---> '),
%  writeln(user,Path),
   postscript(@ddd_p1,Path).


display_logo_bitmap :-
   !.
display_logo_bitmap :-
   stock_file(logo_file,Logo_File),
   display_bitmap(gif,Logo_File,125,40),
   stock_announcement('Welcome to Stock Tool ...').

display_stock_bitmap :-
   convert_ps_to_gif(Gif_File),
   display_bitmap(gif,Gif_File,10,10),
   write_debug('sent to display and resized ...').


display_bitmap(gif,File,U,V) :-
   dislog_stock_advisor(clear_display_stock_bitmap),
   new(@bm,bitmap(File)),
   send(@ddd_p1,display,@bm,point(U,V)),
   dislog_variable_get(dsa_pic_size,[X,Y]),
   send(@ddd_p1,size,size(X,Y)).
display_bitmap(ppm,File,U,V) :-
   dislog_stock_advisor(clear_display_stock_bitmap),
   new(@bm,bitmap(File)),
   send(@ddd_p1,display,@bm,point(U,V)),
   name_append(['Displaying ',File],String),
   stock_announcement(String).


change_display_stock(Direction) :-
   dislog_variable_get(chart_display_mode,Mode),
   change_display_stock(Mode,Direction).

change_display_stock(postscript,Direction) :-
   dislog_counter(Direction,gif_file,N),
   number_to_gif_file(N,Gif_File),
   display_bitmap(gif,Gif_File,10,10).
change_display_stock(gnuplot,Direction) :-
   dislog_counter(Direction,gif_file,N),
   number_to_ppm_file(N,Ppm_File),
   display_bitmap(ppm,Ppm_File,0,0).
change_display_stock(xpce,Direction) :-
   dislog_counter(Direction,dsa_display_cache,N),
   dsa_display_cache(N,Goal),
   call(Goal).

display_announcement_wait_until_keypressed(String) :-
   send(@display,inform,String).

stock_announcement(String) :-
   stock_announcement_1(String).

stock_announcement_1(String) :-
   dislog_flag_get(stock_announcement,1),
%  send(@br1,clear),
   send(@br1,append,String),
   get(@br1,length,Position),
   send(@br1,scroll_to,Position-3).
stock_announcement_1(_).


/*** implementation ***********************************************/


convert_ps_to_gif(Gif_File) :-
   dislog_counter(increase,gif_file,N),
   number_to_gif_file(N,Gif_File),
   stock_file(file(ps),Ps_File), write(Ps_File),
   file_convert(ps_to_gif,Ps_File,Gif_File),
   write_debug('converted to gif...').


number_to_gif_file(N,File) :-
   dsa_number_to_file('Stock_',N,gif,File).

number_to_ppm_file(N,File) :-
   dsa_number_to_file('stockpic_',N,ppm,File).

number_to_ppm_file(N,File_1,File_2) :-
   name_append(['stockpic_',N,'.gif'],File_1),
   number_to_ppm_file(N,File_2).


dsa_number_to_file(Prefix,N,Extension,File) :-
   stock_file(dir(Extension),Directory),
   name_append([Directory,Prefix,N,'.',Extension],File).


delete_all_stock_gif_files :-
   stock_file(dir(gif),'*.',File),
   gif_file_extension(Ext),
   concat_atom(['rm ',File,Ext,' > /dev/null'],Command),
   shell(Command).
delete_all_stock_gif_files.



openwin_for_tps_fixpoint :-
   new(DisLogWin,dialog('DisLog Dialog')),
   dislog_flag_set(dislog_win, DisLogWin),
   send_list(DisLogWin, append, [
      button(quit, message(DisLogWin, destroy)),
      button(execute, message(@prolog, get_tps_and_execute)),
      text_item(tps_fixpoint) ]),
   send(DisLogWin, open).

closewin_for_tps_fixpoint :-
   dislog_flag_get(dislog_win, DisLogWin),
   free(DisLogWin).


get_tps_and_execute :-
   dislog_flag_get(dislog_win, DisLogWin),
   get(DisLogWin, member, tps, Operator),
   get(Operator, selection, Tps),
   get(DisLogWin, member, arg, Argument),
   get(Argument, selection, Arg),
   Atom =.. [Tps, Arg],
   call(Atom).
%  send(new(Viewer, view(Text)), open),
%  send(Viewer, append, Text).


ddd_view_file(File_Name) :-
%  stock_file(dir(gif),File_Name,File),
   stock_file(results,File_Name,File),
   new(ViewFrame,frame('File Viewer')),
   send(ViewFrame,append,new(Viewer,view(File))),
   send(new(Dialog,dialog),below,Viewer),
   send(Dialog,append,
      button(quit,message(ViewFrame,destroy))),
   send(ViewFrame,open),
   send(Viewer,load,File).


ddd_print :-
%  stock_file(dir(gif),Dir),
   stock_file(results,Dir),
   ddd_list_files(Dir).

ddd_list_files(Dir) :-
   new(DisLogFrame,frame('SMS - Files')),
   send(DisLogFrame,append,new(Browser,browser)),
   send(new(Dialog,dialog),below,Browser),
   send(Dialog,append,
      button(view,message(@prolog,ddd_view_file,
         Browser?selection?key))),
   send(Dialog,append,
      button(print,message(@prolog,ddd_postscript,
         Browser?selection?key))),
   send(Dialog,append,
      button(cancel,message(DisLogFrame,destroy))),
   send(Browser,members,directory(Dir)?files),
   send(DisLogFrame,open).


print_postscript(Address,Data_File) :-
   !,
%  stock_file(dir(gif),Data_File,File),
   stock_file(results,Data_File,File),
   name_append(['lpr -P',Address,' ',File],Command),
   unix(shell(Command)).


ddd_postscript(Data) :-
   new(D,dialog('SMS -> Printer')),
   send(D,append,new(A,text_item(printer_name,'lp'))),
   send(D,append,
      button(ok,and(
         message(@prolog,print_postscript,
            A?selection,Data),
         message(D,destroy)))),
   send(D,append,button(cancel,message(D,destroy))),
   send(D,default_button,ok),
   send(D,open).


/******************************************************************/


