

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  XPCE Portfolio Manager               ***/
/***                                                            ***/
/******************************************************************/


/* stock_portfolio_manager <-
      */

stock_portfolio_manager :-
   findall( Portfolio_Name,
      stock_portfolio_tuple(Portfolio_Name,_),
      Portfolio_Names ),
   stock_portfolio_manager_with_items(Portfolio_Names).

stock_portfolio_manager_with_items(Portfolio_Names) :-
   new(Frame,frame('SMS - Portfolio Manager')),
   send(Frame,append,new(Dialog_I,dialog)),
   dislog_variable_get(dsa_browser_colour,Colour),
   send(Dialog_I,background,Colour),
   send(Dialog_I,append,new(Browser,browser)),
   dislog_variable_set(stock_portfolio_browser,Browser),
   send(Browser,size,size(40,10)),
   send(Browser,label,'Portfolios'),
   send(new(Dialog,dialog),below,Dialog_I),
   checklist( send_append(Dialog), [
      'Edit' - stock_portfolio_editor_update(Browser?selection?key),
      'Insert' - stock_portfolio_editor_insert,
      'Delete' - stock_portfolio_editor_delete(Browser?selection?key),
      'Save' - stock_portfolio_tuples_save_to_file,
      'Overview' - xpce_display_portfolio_table - below,
      'Charts' - charts_portfolio(Browser?selection?key),
      'Close' - send(Frame,destroy) ] ),
   chain_list(Chain,Portfolio_Names),
   send(Browser,members,Chain),
   send(Frame,open).


/* stock_portfolio_tuples_save_to_xml_file <-
      */

stock_portfolio_tuples_save_to_xml_file :-
   stock_portfolio_tuples_to_xml(Xml),
   stock_file( sources_directory,
      'stock_knowledge_base/portfolios.xml', File ),
   dwrite(xml, File, Xml). 

stock_portfolio_tuples_to_xml(Xml) :-
   findall( Tuple_Xml,
      ( stock_portfolio_tuple(Tuple),
        stock_portfolio_tuple_to_xml(Tuple, Tuple_Xml) ),
      Tuples_Xml ),
   Xml = portfolios:[]:Tuples_Xml.

stock_portfolio_tuple_to_xml(Tuple, Tuple_Xml) :-
   Name := Tuple^owner,
   Cs := Tuple^members,
   findall( item:As:[],
      member(As, Cs),
      Items ),
   Tuple_Xml = portfolio:[name:Name]:Items.


/* stock_portfolio_tuples_save_to_file <-
      */

stock_portfolio_tuples_save_to_file :-
   stock_file( sources_directory,
      'stock_knowledge_base/portfolios', File ),
   write_list(['---> ',File]), nl,
   stock_portfolio_tuples_save_to_file(File).

stock_portfolio_tuples_save_to_file(File) :-
   tell(File),
   nl,
   writeln('% configuration file'),
   writeln('% this file is generated, please do not edit !'),
   nl,
   forall( stock_portfolio_tuple(Tuple),
      stock_portfolio_tuple_save_to_file(Tuple) ),
   told.

stock_portfolio_tuple_save_to_file(Tuple) :-
   [Owner, Members] := Tuple^[owner, members],
   writeln('stock_portfolio_tuple(['),
   write_list(['   owner: ''', Owner, ''',']), nl,
   ( ( Members = [],
       write('   members: [ ]'),
       ! )
   ; ( writeln('   members: ['),
       write_list_with_separators(write_aa, write_bb, Members) ) ),
   writeln(' ]).'), nl.


write_aa(Entity) :-
   Company := Entity^company,
   write_list(['      [ company: ''',Company,''',']), nl,
   delete(Entity,company:Company,Pairs),
   write_list_with_separators(write_a1,write_a2,Pairs).

write_a1(A:V) :-
   member(A,[company,date]),
   !,
   write_list(['        ',A,': ''',V,''',']),
   nl.
write_a1(A:V) :-
   write_list(['        ',A,': ',V,',']),
   nl.
write_a2(A:V) :-
   member(A,[company,date]),
   !,
   write_list(['        ',A,': ''',V,''' ],']),
   nl.
write_a2(A:V) :-
   write_list(['        ',A,': ',V,' ],']),
   nl.

write_bb(Entity) :-
   Company := Entity^company,
   write_list(['      [ company: ''',Company,''',']), nl,
   delete(Entity,company:Company,Pairs),
   write_list_with_separators(write_a1,write_b2,Pairs).

write_b2(A:V) :-
   member(A,[company,date]),
   !,
   write_list(['        ',A,': ''',V,''' ] ]']).
write_b2(A:V) :-
   write_list(['        ',A,': ',V,' ] ]']).


/******************************************************************/


