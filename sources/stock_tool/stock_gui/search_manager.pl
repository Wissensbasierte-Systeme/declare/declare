

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  XPCE Search Manager                  ***/
/***                                                            ***/
/******************************************************************/


stock_search_manager :-
   new(Frame,frame('SMS - Search Manager')),
   send(Frame,append,new(Dialog_T,dialog)),
   send(Dialog_T,append,new(Text_Item,text_item('Name',''))),
%  send(Dialog_T,size,size(250,40)),
   send(new(Dialog_I,dialog),below,Dialog_T),
   dislog_variable_get(dsa_browser_colour,Colour),
   send(Dialog_I,background,Colour),
   send(Dialog_I,append,new(Browser,list_browser)),
   send(Browser,size,size(40,10)),
   dislog_variable_set(stock_search_browser,Browser),
   send(Browser,label,'Wkn: Company'),
   send(new(Dialog,dialog),below,Dialog_I),
   checklist( send_append(Dialog), [
      'Search' - mysql_select_names_like_to_browser(
         Browser, Text_Item?selection ),
      'Chart' - chart_for_search_name(Browser?selection?key),
      'Table' - mysql_select_by_name_to_table_for_search(
         Browser?selection?key),
      'Close' - send(Frame,destroy) ] ),
   chain_list(Chain,[]),
   send(Browser,members,Chain),
   send(Frame,open).


mysql_select_names_like_to_browser(Browser,Name) :-
%  dislog_variable_get(stock_search_browser,Browser),
   name_append(['''%',Name,'%'''],Name_2),
   mysql_select_names_like(Name_2,State),
   maplist( number_name_clause_to_name,
      State, Names ),
   chain_list(Chain,Names),
   send(Browser,members,Chain).

number_name_clause_to_name(
      [number_name(Wkn,Company)], Name ) :-
   name_append([Wkn,': ',Company],Name).

   
mysql_select_by_name_to_table_for_search(Name) :-
   name_start_after_position([": "],Name,Company),
   mysql_select_by_name_to_table(Company).


chart_for_search_name(Name) :-
   dsa_update_some_dislog_variables,
   name_start_after_position([": "],Name,Company),
   writeln(chart(Company)),
   chart(Company).


/******************************************************************/


