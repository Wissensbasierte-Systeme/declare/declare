

/******************************************************************/
/***                                                            ***/
/***        Stock Tool:  XPCE Interface Buttons                 ***/
/***                                                            ***/
/******************************************************************/


dislog_stock_advisor(send_pulldown_menu,Window,'File') :-
   send_pulldown_menu(Window,'File',[
%     new   - dislog_stock_advisor(clear_display_stock_bitmap),
%     open  - select_xpce_stock,
%     save  - save_chart_file,
      save  - ddd_save,
      print - ddd_print,
      quit  - dislog_stock_advisor(close_main_window) ]).


dislog_stock_advisor(send_pulldown_menu,Window,'Edit') :-
   send_pulldown_menu(Window,'Edit',[
      'Clean Data'  -
         stock_data_cleaning(sort_and_remove_doubles,xxx),
      'Data Volume' -
         stock_chart(spooled_aggregated),
      'Data Increase' -
         stock_chart(spooled),
      'Show Cache'  -
         stock_values_cached(show),
      'Clear Cache' -
         stock_values_cached(clear),
      'Preferences' -
         dsa_dialog('DSA Preferences') ]).


dislog_stock_advisor(send_pulldown_menu, Window, 'History') :-
   send_pulldown_menu(Window, 'History', [
      'Back' - change_display_stock(backward),
      'Forward' - change_display_stock(forward),
      'Show History' - dsa_history ]).


dislog_stock_advisor(send_pulldown_menu,Window,'Tools') :-
   send_pulldown_menu(Window,'Tools',[
      'Stock Spooler'      - dsa_dialog('Stock Spooler'),
%     'Search Manager'     - stock_search_manager,
      'Portfolio Manager'  - stock_portfolio_manager,
      'Data Cleaning'      - stock_data_cleaning_manager,
      'Pattern Search'     - dsa_pattern_search,
      'Analysis'           - stock_analysis_manager ]).
%     'Stock Mining'       - stock_mining_predicates ]).
%     'Colons (absolute)'  - draw_colons(regular),
%     'Colons (relative)'  - draw_colons(percent),
%     'Analysis of Groups' -
%        dsa_dialog('Analysis of Stock Groups') ]).


dislog_stock_advisor(send_pulldown_menu,Window,'Window') :-
   send_pulldown_menu(Window,'Window',[
%     '>>' - change_display_stock(forward),
%     '<<' - change_display_stock(backward),
%     add_shortcut_bar -
%        dislog_stock_advisor(add_shortcut_bar),
%     remove_shortcut_bar -
%        dislog_stock_advisor(remove_shortcut_bar),
      clear_window -
         dislog_stock_advisor(clear_display_window),
      show_logo -
         display_logo_bitmap ]).


dislog_stock_advisor(send_pulldown_menu,Window,'Help') :-
   send(Window,append,new(Address,popup('Help'))),
   name_append([ 'SMS - Stock Management System\n',
      'Dietmar Seipel, Universität Würzburg'], Info_String),
   send_list(Address,append,[
      menu_item(about,message(@display,inform,Info_String)) ]).
%     menu_item(show_logo,message(@prolog,display_logo_bitmap)) ]).


dislog_stock_advisor(send_pulldown_menu,
      Window,'Data Sources') :-
   send_pulldown_menu(Window,'Stocks',[
      'Hoppenstedt' - dsa_dialog(hoppenstedt),
      'Finanztreff' - dsa_dialog(finanztreff),
      'Comdirekt'   - dsa_dialog(comdirekt) ]).


dislog_stock_advisor(send_pulldown_menu,
      Window,'Analysis of Branches') :-
   findall( Branch - charts_group(Branch),
      stock_branch(Branch,_),
      Pairs_Branch ),
   send_pulldown_menu(Window,'Branches',[
      'All Branches' - charts_group |
      Pairs_Branch ]).


dislog_stock_advisor(send_pulldown_menu,
      Window,'Analysis of Groups') :-
   findall( Stock_Pool - charts_pool(Stock_Pool),
      stock_pool(Stock_Pool,_),
      Pairs_Stock_Pool ),
   writeln(user,Pairs_Stock_Pool),
%  append([ 'Ranking of Companies' - dax_ranking(regular),
%     'Ranking of Groups'    - dax_ranking(grouped) ],
%     Pairs_Stock_Pool, Pairs ),
   Pairs = Pairs_Stock_Pool,
   send_pulldown_menu(Window,'Groups',Pairs).


dislog_stock_advisor(send_pulldown_menu,
      Window,'Analysis of Portfolios') :-
   findall( Name - charts_portfolio(Name),
      stock_portfolio_tuple(Name,_), Pairs ),
   send_pulldown_menu(Window,'Portfolios',Pairs).


dislog_stock_advisor(send_pulldown_menu,
      Window,'Visualisation') :-
   send_pulldown_menu(Window,'Visualisation',[
       visualization - choice_groups_and_execute ]).


/******************************************************************/


