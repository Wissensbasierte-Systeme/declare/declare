

/******************************************************************/
/***                                                            ***/
/***        Stock Tool:  XPCE Interface Design Patterns         ***/
/***                                                            ***/
/******************************************************************/


dsa_create_create_menu_bars(Window_Address,Bars) :-
   dsa_create_create_menu_bars(Window_Address,[],Bars).

dsa_create_create_menu_bars(Window_Address,Address_1,
      [[Name,Number_Name,Source,Range]|Bars]) :-
   dsa_create_create_menu_bar(Name,Window_Address,
      Address_1,Address_2,
      Number_Name,Source,Range),
   dsa_create_create_menu_bars(Window_Address,Address_2,Bars).
dsa_create_create_menu_bars(_,_,[]).


dsa_create_create_menu_bar(Name,Window_Address,
      Above_Address,Address,
      Number_Name,Source,Range) :-
   stock_file(stock_data,Number_Name,Path),
   stock_companies(Path,[Source|_],Companies),
   ( ( Name \== '--',
       send(Window_Address,append,
          new(Text_Item,text_item(Name))),
       dsa_xpce_send_position(Text_Item,below,Above_Address) )
   ; ( Text_Item = Above_Address ) ),
%  send(Window_Address,append,new(Address,menu_bar)),
%  checklist( dsa_create_selection_button(Address,Companies),
%     Range ),
%  dsa_xpce_send_position(Address,below,Text_Item),
   dsa_create_selection_buttons(Window_Address,Text_Item,Address,
      Companies,Range).


dsa_xpce_send_position(_,_,[]) :-
   !.
dsa_xpce_send_position(Address,Position,Above_Address) :-
   send(Address,Position,Above_Address).


dsa_create_selection_buttons(Window,Above,Last,Companies,[A,B,C|Range]) :-
   !,
   send(Window,append,new(Address,menu_bar)),
   dsa_xpce_send_position(Address,below,Above),
   checklist( dsa_create_selection_button(Address,Companies),
      [A,B,C] ),
   dsa_create_selection_buttons(Window,Address,Last,Companies,Range).
dsa_create_selection_buttons(_,Last,Last,_,[]) :-
   !.
dsa_create_selection_buttons(Window,Above,Address,Companies,Range) :-
   send(Window,append,new(Address,menu_bar)),
   dsa_xpce_send_position(Address,below,Above),
   checklist( dsa_create_selection_button(Address,Companies),
      Range ).

dsa_create_selection_button(Address,Companies,X-Y) :-
   character_lower_to_capital(X,X1),
   character_lower_to_capital(Y,Y1),
   name_append([' ',X1,'-',Y1],S),
   send(Address,append,new(Address_2,popup(S))),
   name(X,N1), name(Y,N2),
   findall( menu_item(Company_2,message(@prolog,chart,Company)),
      ( member(Company,Companies), name(Company,[N|_]),
        in_character_range(N1-N2,N),
        name_html_to_text(Company,Company_2) ),
      Items ),
   send_list(Address_2,append,Items).


/******************************************************************/


