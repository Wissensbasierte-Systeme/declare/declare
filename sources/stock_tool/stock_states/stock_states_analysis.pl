

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Analysis of Stock States             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* stock_state_analyse(State_1, State_2) <-
      */

stock_state_analyse(State_1, State_2) :-
   stock_state_add_previous_values(long, year, State_1, State_A),
   stock_state_smooth(5, State_A, State_B),
%  stock_state_analyse_write_to_file(State_B, '_B'),
   stock_state_differentiate(60, State_B, State_C),
   stock_state_smooth(20, State_C, State_D),
%  stock_state_analyse_write_to_file(State_B, '_C'),
   stock_state_abstract(State_D, State_2).

stock_state_analyse_write_to_file(State, File_Ending) :-
   stock_state_to_company(State, Company),
   dislog_variable_get(home, Home),
   concat([ Home, '/results/Abstractions/',
      Company, File_Ending ], File),
   stock_state_to_file(State, File).

stock_state_to_company([C|_], Company) :-
   C = [stock(Company, _, _, _, _)].
stock_state_to_company([], '???').


/* stock_state_analyse_weights(State_1, State_2) <-
      */

stock_state_analyse_weights(State_1, State_2) :-
   dislog_variable_get(stock_weights, Weights),
%  add(Weights, W),
   vector_multiply(1/1200, Weights, Weights_2),
   findall( [stock(Name, Number, Date, V, [Value|Previous])],
      ( member([Atom], State_1),
        Atom = stock(Name, Number, Date, Value, Previous),
        stock_atom_weight(Weights_2, Atom, Weight),
        V is 1 + Weight ),
      State_2 ).


/* mysql_select_by_name_with_previous(Name, State) <-
      */

mysql_select_by_name_with_previous(Name, State) :-
   mysql_select_by_name(Name, State_n),
   mysql_select_by_name_previous(Name, States_p),
%  stock_state_to_xpce_table(State_n),
   stock_states_previous_merge(State_n, States_p, State).


/* mysql_select_by_name_previous(Name, States) <-
      */

mysql_select_by_name_previous(Name, States) :-
   maplist( mysql_select_by_name_previous(Name),
      [p_day,p_week,p_month,p_year], States ).

mysql_select_by_name_previous(Name, Previous, State) :-
   dislog_variable_get(dsa_stock_database, DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ {{n^name}}, f^wkn, p^date, f^value,
         f^c_day, f^c_week, f^c_month, f^c_year ],
      from:[ DSA_Stock_Database-f,
         previous_dates-p, number_name-n ],
      where:[ n^name = {Name} and f^wkn = n^wkn and
         f^date = p^Previous ] ],
      Tuples ),
   maplist( mysql_result_tuple_to_stock_clause,
      Tuples, State ).


/* mysql_select_by_date_with_previous(Date) <-
      */

mysql_select_by_date_with_previous(Date) :-
   writeln(user,mysql_select_by_date_with_previous(Date)),
   mysql_select_by_date_with_previous(Date, State_1),
   mysql_select_number_name(Number_Name),
   maplist( stock_fact_normalize_name(Number_Name),
      State_1, State_2 ),
   list_to_ord_set(State_2, State_3),
   stock_state_analyse_weights(State_3, State_4),
   stock_state_sort_by_value(State_4, State_5),
   stock_state_to_xpce_table_extended(State_5).

stock_state_to_xpce_table_extended(State) :-
   maplist( stock_clause_weighted_to_tuple,
      State, Tuples ),
   reverse(Tuples, Rows),
   xpce_display_table(_,_,'SMS - Ranking',
      ['Name','Wkn','Date','Value','D','W','M','Y','Weight'],
      Rows ).
   
stock_clause_weighted_to_tuple(Clause, Tuple) :-
   Clause = [stock(Name, Number, D-M-Y, Weight, [Value|Previous])],
   Y2 is Y + 1900,
   date_to_natural_date(D-M-Y2, Date),
   no_singleton_variable(Previous_2),
   ( Previous = [] ->
     Previous_2 = ['--', '--', '--', '--']
   ; Previous_2 = Previous ),
   round(Weight, 2, Weight_2),
   append([Name, Number, Date, Value|Previous], [Weight_2], Tuple).

stock_fact_normalize_name(Number_Name, C1, C2) :-
   C1 = [stock(_, Number, Date, Value, Previous)],
   stock_number_to_first_name(Number_Name, Number,Name),
   C2 = [stock(Name, Number, Date, Value, Previous)].

   
/* mysql_select_by_date_with_previous(Date, State) <-
      */

mysql_select_by_date_with_previous(Date, State) :-
   mysql_select_by_date(Date, State_n),
   mysql_select_by_date_previous(Date, States_p),
%  stock_state_to_xpce_table(State_n),
   stock_states_previous_merge(State_n, States_p, State).


/* mysql_select_by_date_previous(Date, State) <-
      */

mysql_select_by_date_previous(Date, States) :-
   maplist( mysql_select_by_date_previous(Date),
      [p_day,p_week,p_month,p_year], States ).


/* mysql_select_by_date_previous(Date, Previous, State) <-
      */

mysql_select_by_date_previous(Date, Previous, State) :-
   dislog_variable_get(dsa_stock_database, DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ {{n^name}}, f^wkn, p^date, f^value,
         f^c_day, f^c_week, f^c_month, f^c_year ],
      from:[ DSA_Stock_Database-f,
         previous_dates-p, number_name-n ],
      where:[ p^date = {Date} and f^date = p^Previous and
         f^wkn = n^wkn ] ],
      Tuples ),
   maplist( mysql_result_tuple_to_stock_clause,
      Tuples, State ).


/* stock_states_previous_merge(State_1, States, State_2) <-
      */

stock_states_previous_merge(State_1, States, State_2) :-
   States = [S_d,S_w,S_m,S_y],
   findall( [stock(Name, Number, Date, Value, Previous)],
      ( member([stock(Name, Number, Date, Value,_)], State_1),
        nonvar(Value),
        \+ 0 is Value,
        stock_state_to_value(S_d,Name,Number,Date,V_d),
        stock_state_to_value(S_w,Name,Number,Date,V_w),
        stock_state_to_value(S_m,Name,Number,Date,V_m),
        stock_state_to_value(S_y,Name,Number,Date,V_y),
        maplist( values_to_percentage(Value),
           [V_d,V_w,V_m,V_y], Previous ) ),
      State_2 ).

values_to_percentage(V, W, P) :-
   Q is 100 * ( W / V - 1 ),
   round(Q, 2, P).

stock_state_to_value(State, Name, Number, Date, Value) :-
   member([stock(Name, Number, Date, Value, _)], State),
   !.


/* previous_atoms_to_sql_file(File) <-
      */

previous_atoms_to_sql_file(File) :-
   previous_atoms_to_previous_tuples(Tuples),
   previous_tuples_to_sql_file(Tuples, File).


/* previous_atoms_to_previous_tuples(Tuples) <-
      */

previous_atoms_to_previous_tuples(Tuples) :-
   dislog_variable_get(example_path, Path),
   maplist( concat(Path),
      [ 'mysql_date/minus_one_day',
        'mysql_date/minus_one_week',
        'mysql_date/minus_one_month',
        'mysql_date/minus_one_year' ],
      Files ),
   consult(Files),
   findall( [Date,D,W,M,Y],
      ( mysql_date_minus_one_day(Date,D),
        mysql_date_minus_one_week_2(Date,W),
        mysql_date_minus_one_month_2(Date,M),
        mysql_date_minus_one_year_2(Date,Y) ),
      Tuples ).
        
mysql_date_minus_one_week_2(Date, W) :-
   mysql_date_minus_one_week(Date, W),
   !.
mysql_date_minus_one_month_2(Date, M) :-
   mysql_date_minus_one_month(Date, M),
   !.
mysql_date_minus_one_year_2(Date, Y) :-
   mysql_date_minus_one_year(Date, Y),
   !.


/* previous_tuples_to_sql_file(Tuples, File) <-
      */

previous_tuples_to_sql_file(Tuples, File) :-
   tell(File),
   writeln('use stock;'), nl,
   writeln('INSERT INTO previous_dates VALUES'),
   write_list_with_separators(
      writeln_previous_dates(','),
      writeln_previous_dates(';'),
      Tuples ),
   told.
      
writeln_previous_dates(Separator, Dates) :-
   write('   ( '),
   write_list_with_separators(
      write_sql_date(', '),
      write_sql_date(''),
      Dates ),
   write(' )'), writeln(Separator).
      
write_sql_date(Separator, Date) :-
   write_list(['''', Date, '''', Separator]).


/*** appendix *****************************************************/


/* mysql_normalized_group_to_xpce(Group) <-
      */

mysql_normalized_group_to_xpce(Group) :-
   mysql_select_normalized_group(Group, Names, States_1),
   maplist( stock_state_differentiate,
      States_1, States_2 ),
   stock_states_to_display(Names, States_2).


/* stock_states_abstraction_to_xpce(Group) <-
      */

stock_states_abstraction_to_xpce(Group) :-
   dislog_variable_get(home, Home),
   concat([Home, '/results/Abstractions/', Group], File),
   dconsult(File, State),
   stock_state_split(Group, State, Names, States),
   maplist( stock_state_sort_by_date,
      States, States_2 ),
   stock_states_to_display(Names, States_2).


/******************************************************************/


