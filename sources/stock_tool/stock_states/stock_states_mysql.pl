
/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  mySQL                                ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      mysql_result_atom/1.
:- multifile
      mysql_result_atom/1.


/*** interface ****************************************************/


/* mysql_select_max_id(Id) <-
      */

mysql_select_max_id(Id) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ max(id) ],
      from:[ DSA_Stock_Database ] ],
      [[Max]] ),
   ( ( var(Max), Id = 0 )
   ; ( Id = Max ) ).


/* mysql_select_max_date(Date) <-
      */

mysql_select_max_date(Date) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ max(date) ],
      from:[ DSA_Stock_Database ] ],
      [[Max]] ),
   ( ( var(Max), Date = 0 )
   ; ( Date = Max ) ).


/* mysql_select_all_groups_average(State) <-
      */

mysql_select_all_groups_average(State) :-
   mysql_select_all_averages,
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ {{s^group_name}}, {--}, date,
         avg(value/average),
         avg(c_day), avg(c_week), avg(c_month), avg(c_year) ],
      from:[ DSA_Stock_Database-f, number_name-n, stock_group-s,
         stock_averages-a ],
      where:[ n^wkn = f^wkn and a^wkn = f^wkn and a^average \= 0 and
         s^name = n^name ],
      group_by: [ s^group_name, date ] ],
      Tuples ),
   maplist( mysql_result_tuple_to_stock_clause,
      Tuples, State ).


/* mysql_select_by_group(Group,Names,States) <-
      */

mysql_select_by_group(Group,Names,States) :-
   mysql_select_by_group(Group,State),
   stock_state_split(Group,State,Names,States).


/* mysql_select_by_group(Group,State) <-
      */

mysql_select_by_group(Group,State) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ {{n^name}}, f^wkn, f^date, f^value,
         f^c_day, f^c_week, f^c_month, f^c_year ],
      from:[ DSA_Stock_Database-f, number_name-n, stock_group-s ],
      where:[ n^wkn = f^wkn and s^name = n^name and
         s^group_name = {Group} ] ],
      Tuples ),
   maplist( mysql_result_tuple_to_stock_clause,
      Tuples, State ).


/* mysql_select_group_average(Group) <-
      */

mysql_select_group_average(Group) :-
   mysql_select_group_average(Group,Tuples),
   dislog_variable_get(home,DisLog),
   concat(DisLog,'/results/mysql_statements',File_1),
   concat(DisLog,'/results/mysql_result',File_2),
   predicate_to_file( File_1,
      writeln('use stock; delete from group_averages;') ),
   mysql_execute_file_to_file(File_1,File_2),
   ( predicate_to_file( File_1,
       ( writeln('use stock;'), nl,
         writeln('INSERT INTO group_averages VALUES'),
         write_list_with_separators(
            writeln_group_average_tuple(Group,','),
            writeln_group_average_tuple(Group,';'),
            Tuples ) ) )
   ; writeln(user,'no tuples found') ),
   mysql_execute_file_to_file(File_1,File_2).

writeln_group_average_tuple(Group,Stop,[Date,Value]) :-
    write_list(
      [ '   ( ', '''', Group, ''', ''',
        Date, ''', ', Value, ' )', Stop ] ), nl.


/* mysql_select_group_average(Group,Tuples) <-
      */

mysql_select_group_average(Group,Tuples) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ date, avg(value) ],
      from:[ DSA_Stock_Database-f, number_name-n, stock_group-s ],
      where:[ s^group_name = {Group} and
         s^name = n^name and n^wkn = f^wkn ],
      group_by: [ s^group_name, f^date ] ],
      Tuples ).


/* mysql_select_normalized_group(Group,Names,States) <-
      */

mysql_select_normalized_group(Group,Names,States) :-
   mysql_select_group_average(Group),
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ {{s^name}}, f^wkn, f^date, {{f^value/g^value}} ],
      from:[ DSA_Stock_Database-f, number_name-n,
         stock_group-s, group_averages-g ],
      where:[ s^group_name = {Group} and
         s^name = n^name and n^wkn = f^wkn and
         f^date = g^date ],
      order_by: [ f^wkn, f^date ] ],
      Tuples ),
   maplist( stock_normalized_tuple_to_atom,
      Tuples, State ),
   stock_state_split(Group,State,Names,States).

stock_normalized_tuple_to_atom(Tuple,Atom) :-
   Tuple = [ Name_1, Number, Date_1, Value_1 ],
   name_remove_elements(both,"\t",Name_1,Name_2),
   name_remove_elements(both,"\t",Value_1,Value_3),
   name(Value_3,List),
   name(Value_2,List),
   mysql_date_to_dsa_date(Date_1,Date_2),
   Atom = [ stock(Name_2,Number,Date_2,Value_2,[]) ].


/* mysql_select_name_to_number(Name,State) <-
      */

mysql_select_name_to_number(Name,State) :-
   mysql_select_execute([
      use:[ stock ],
      select:[ wkn, {{name}} ],
      from:[ number_name ],
      where:[ name = {Name} ] ],
      Tuples ),
   maplist( mysql_result_tuple_to_number_name_clause,
      Tuples, State ).


/* mysql_select_number_name(State) <-
      */

mysql_select_number_name(State) :-
   mysql_select_execute([
      use:[ stock ],
      select:[ wkn, {{name}} ],
      from:[ number_name ] ],
      Tuples ),
   maplist( mysql_result_tuple_to_number_name_clause,
      Tuples, State ).


/* mysql_select_names_like(Name,State) <-
      */

mysql_select_names_like(Name,State) :-
   mysql_select_execute([
      use:[ stock ],
      select:[ wkn, {{name}} ],
      from:[ number_name ],
      where:[ name like Name ] ],
      Tuples ),
   maplist( mysql_result_tuple_to_number_name_clause,
      Tuples, State ).


/* mysql_select_by_names(Names,State) <-
      */

mysql_select_by_names(Names,State) :-
   maplist( mysql_select_by_name,
      Names, States ),
   append(States,State).


/* mysql_select_by_name(Name,State) <-
      */

mysql_select_by_name_to_file(Name,Filename) :-
   mysql_select_by_name(Name,State),
   stock_state_sort_by_date(State,State_Sorted),
   reverse(State_Sorted,State_Sorted_Reverse),
   tell(Filename),
   dportray(lp,State_Sorted_Reverse),
   told.

mysql_select_by_name(Name,State) :-
   mysql_select_by_name_direct(Name,State_1),
   mysql_select_by_name_umtausch(Name,State_2),
   append(State_2,State_1,State),
   !.


/* mysql_select_by_name_to_table(Name) <-
     */

mysql_select_by_name_to_table(Name) :-
   mysql_select_by_name(Name,State),
   stock_state_add_previous_values(long,year,State,State_1),
   stock_state_sort_by_date(State_1,State_2),
   maplist( stock_clause_to_tuple,
      State_2, Tuples ),
   reverse(Tuples,Rows),
   Attributes = ['Name','Wkn','Date','Value','D','W','M','Y'],
   xpce_display_table(_,_,Name,Attributes,Rows).
%  html_display_table(Name,Attributes,Rows).

stock_clause_to_tuple(C,T) :-
   C = [stock(Name,Wkn,D-M-Y,Value,Previous)],
   Y2 is Y + 1900,
   date_to_natural_date(D-M-Y2,Date),
   ( Previous = [] ->
     Previous_2 = ['--','--','--','--']
   ; Previous_2 = Previous ),
   append([Name,Wkn,Date,Value],Previous_2,T).
   

/* mysql_select_by_name_umtausch(Name,State) <-
      */

mysql_select_by_name_umtausch(Name,State) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ {{u^name_from}}, f^wkn, f^date, f^value, u^rate,
         f^c_day, f^c_week, f^c_month, f^c_year ],
      from:[ DSA_Stock_Database-f,
         number_name-n, umtausch-u ],
      where:[ u^name_from = {Name} and n^wkn = f^wkn and
         u^name_to = n^name ] ],
      Tuples ),
   maplist( mysql_result_tuple_to_stock_clause,
      Tuples, State ),
   !.
mysql_select_by_name_umtausch(_,[]).


/* mysql_select_by_name_direct(Name,State) <-
      */

mysql_select_by_name_direct(Name,State) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   dislog_variable_get(chart_start,Year),
   concat(Year,'-01-01',Date),
%  writeln(user,Date),
   mysql_select_execute([
      use:[ stock ],
      select:[ {{n^name}}, f^wkn, f^date, f^value,
         f^c_day, f^c_week, f^c_month, f^c_year ],
      from:[ DSA_Stock_Database-f, number_name-n ],
      where:[ n^name = {Name} and f^wkn = n^wkn and f^date >= {Date} ] ],
      Tuples ),
   maplist( mysql_result_tuple_to_stock_clause,
      Tuples, State ).

xxx_mysql_select_by_name_direct(Name,State) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ {{n^name}}, f^wkn, f^date, f^value,
         f^c_day, f^c_week, f^c_month, f^c_year ],
      from:[ DSA_Stock_Database-f, number_name-n ],
      where:[ n^name = {Name} and f^wkn = n^wkn ] ],
      Tuples ),
   maplist( mysql_result_tuple_to_stock_clause,
      Tuples, State ).


/* mysql_select_by_date(Date,State) <-
      */

mysql_select_by_date(Date,State) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ {{n^name}}, f^wkn, f^date, f^value,
         f^c_day, f^c_week, f^c_month, f^c_year ],
      from:[ DSA_Stock_Database-f, number_name-n ],
      where:[ f^date = {Date} and f^wkn = n^wkn ] ],
      Tuples ),
   maplist( mysql_result_tuple_to_stock_clause,
      Tuples, State ).


/* mysql_select_by_number(Wkn,State) <-
      */

mysql_select_by_number(Wkn,State) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   dislog_variable_get(chart_start,Year),
   concat(Year,'-01-01',Date),
%  writeln(user,Date),
   mysql_select_execute([
      use:[ stock ],
      select:[ {{n^name}}, f^wkn, f^date, f^value,
         f^c_day, f^c_week, f^c_month, f^c_year ],
      from:[ DSA_Stock_Database-f, number_name-n ],
      where:[ f^wkn = Wkn and f^wkn = n^wkn and f^date >= {Date} ] ],
      Tuples ),
   maplist( mysql_result_tuple_to_stock_clause,
      Tuples, State ).

xxx_mysql_select_by_number(Wkn,State) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ {{n^name}}, f^wkn, f^date, f^value,
         f^c_day, f^c_week, f^c_month, f^c_year ],
      from:[ DSA_Stock_Database-f, number_name-n ],
      where:[ f^wkn = Wkn and f^wkn = n^wkn ] ],
      Tuples ),
   maplist( mysql_result_tuple_to_stock_clause,
      Tuples, State ).


/* mysql_select_all_averages <-
      */

mysql_select_all_averages :-
   mysql_select_all_averages(Tuples),
   dislog_variable_get(home,DisLog),
   concat(DisLog,'/results/mysql_statements',File_1),
   concat(DisLog,'/results/mysql_result',File_2),
   predicate_to_file( File_1,
      writeln('use stock; delete from stock_averages;') ),
   mysql_execute_file_to_file(File_1,File_2),
   ( predicate_to_file( File_1,
       ( writeln('use stock;'), nl,
         writeln('INSERT INTO stock_averages VALUES'),
         write_list_with_separators(
            writeln_stock_averages_insert_statement(','),
            writeln_stock_averages_insert_statement(';'),
            Tuples ) ) )
   ; writeln(user,'no tuples found') ),
   mysql_execute_file_to_file(File_1,File_2).

writeln_stock_averages_insert_statement(Stop,[Wkn,Average]) :-
   write_list(
      [ '   ( ',
        Wkn, ', ', Average, ' )', Stop ] ), nl.

mysql_select_all_averages(Tuples) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ wkn, avg(value) ],
      from:[ DSA_Stock_Database ],
      group_by: [ wkn ],
      having: [ avg(value) > 0 ] ],
      Tuples ).


test(stock_tool,stock_values_mysql(Wkn)) :-
   Name = 'BMW St',
   mysql_select_name_to_number(Name,[[number_name(Wkn,_)]]),
   mysql_select_by_number(Wkn,State_1),
   stock_values(Name,State_2_sorted_by_date),
   list_to_ord_set(State_2_sorted_by_date,State_2),
   !,
   ord_subtract(State_1,State_2,State_1_2),
   length(State_1_2,N_1_2),
   ord_subtract(State_2,State_1,State_2_1),
   length(State_2_1,N_2_1),
   ord_intersection(State_1,State_2,State_i),
   length(State_i,N_i),
   writeln('MySql - Dsa'), dportray(lp,State_1_2), 
   writeln('Dsa - MySql'), dportray(lp,State_2_1), 
   writeln(N_1_2-N_2_1-N_i), 
   State_1 = [C1|_], State_2 = [C2|_],
   dportray(lp,[C1,C2]).


test(stock_tool,mysql_select_command_write) :-
   DSA_Stock_Database = finanztreff,
   Group = 'Banken',
   Sql_Command = [
      use:[ stock ],
      select:[ {{n^name}}, f^wkn, f^date, f^value,
         f^c_day, f^c_week, f^c_month, f^c_year ],
      from:[ DSA_Stock_Database-f,
         number_name-n, stock_group-s ],
      where:[ n^wkn = f^wkn and
         s^name = n^name and
         s^group_name = {Group} ],
      group_by:[ f^date ] ],
   mysql_select_command_write(Sql_Command).


/*** implementation ***********************************************/


mysql_result_tuple_to_number_name_clause([Wkn,Name],[A]) :-
   name_remove_elements(both,[9],Name,Name_2),
   A =.. [number_name,Wkn,Name_2].

mysql_result_tuple_to_stock_clause(
      [Name,Wkn,Date,Value,Rate,CD,CW,CM,CY],[A]) :-
   name_remove_elements(both,[9],Name,Name_2),
   maplist( mysql_value_to_dsa_value,
      [CD,CW,CM,CY], Changes ),
   mysql_date_to_dsa_date(Date,Date_2),
   Value_2 is Value * Rate,
   A =.. [stock,Name_2,Wkn,Date_2,Value_2,Changes].

mysql_result_tuple_to_stock_clause(
      [Name,Wkn,Date,Value,CD,CW,CM,CY],[A]) :-
   name_remove_elements(both,[9],Name,Name_2),
   maplist( mysql_value_to_dsa_value,
      [CD,CW,CM,CY], Changes ),
   mysql_date_to_dsa_date(Date,Date_2),
   A =.. [stock,Name_2,Wkn,Date_2,Value,Changes].

mysql_value_to_dsa_value(Value,'--') :-
   var(Value),
   !.
mysql_value_to_dsa_value(Value,Value).

mysql_date_to_dsa_date(YY-MM-DD,DD-MM-YY_2) :-
   atom_to_number(YY, YY_1),
   YY_2 is YY_1 - 1900.


/*** tests ********************************************************/


test(stock_tool:stock_states_mysql, 1) :-
   mysql_select_by_number(519000, State),
   dportray(lp, State).

test(stock_tool:stock_states_mysql, 2) :-
   mysql_select_by_name('BMW St',State),
   dportray(lp, State).

test(stock_tool:stock_states_mysql, 3) :-
   mysql_select_execute([
      use:[ company ],
      select:[ e^ssn, {{e^lname}} ],
      from:[ employee-e ],
      where:[ ] ],
      Tuples ),
   writeln_list(Tuples).


/******************************************************************/


