

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Stock States - Averages              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* stock_state_mean_values(Group,States,State) <-
      */

stock_state_mean_values(Group,States,State) :-
   length(States,N),
   append(States,State_2),
   stock_state_mean_values(Group,N,State_2,State).


/* stock_state_mean_values(Group,N,State_1,State_2) <-
      Given a name Group of a group of N companies and
      a state State_1 of stock facts for these companies.
      We compute the mean values:
      firstly, we aggregate on companies and dates, and
      secondly, we aggregate on dates, but only for those
      dates for which we know all N values. */
      
stock_state_mean_values(Group,N,State_1,State_2) :-
   stock_state_group([x(-,x,x,-,-)],State_1,States_1),
   stock_states_mean_values(States_1,State_3),
   stock_state_group([x(-,-,x,-,-)],State_3,States_3),
   stock_states_mean_values(Group,N,States_3,State_2).


stock_states_mean_values(Group,N,States,State) :-
   maplist( stock_state_mean_value(Group,N),
      States, State_2 ),
   findall( C,
      ( member(C,State_2),
        C \= [] ),
      State ).

stock_state_mean_value(
      Group,N,State,[stock(Group,-,Date,V,P)]) :-
   length(State,N),
   !,
   stock_state_mean_value(State,[stock(_,_,Date,V,P)]).
stock_state_mean_value(_,_,_,[]).


stock_states_mean_values(States,State) :-
   maplist( stock_state_mean_value,
      States, State ).

stock_state_mean_value(State,C) :-
   length(State,N),
   stock_state_sum_atoms(State,C1),
%  writeln(user,stock_state_sum_atoms(State,C1)),
   C1 = [stock(Name,Number,Date,V1,P1)],
   divide_list([V1|P1],N,[V2|P2]),
   C  = [stock(Name,Number,Date,V2,P2)].

   
/* stock_state_sum_atoms(State,C) <-
      A stock fact C is computed by adding all stock
      facts in State with the same date. */

stock_state_sum_atoms([C1|Cs],C2) :-
   stock_state_sum_atoms(Cs,C1,C2).

stock_state_sum_atoms([C|Cs],C1,C2) :-
   stock_atoms_add(C1,C,C3),
   stock_state_sum_atoms(Cs,C3,C2).
stock_state_sum_atoms([],C,C).
   
stock_atoms_add(C1,C2,C3) :-
   C1 = [stock(_,_,D-M-Y,V1,P1)],
   C2 = [stock(Na2,Nu2,D-M-Y,V2,P2)],
   sum_list_saa([V1|P1],[V2|P2],[V3|P3]),
   C3 = [stock(Na2,Nu2,D-M-Y,V3,P3)].

sum_list_saa([V1|P1],[V2|P2],[V3|P3]) :-
   stock_values_add(V1,V2,V3),
   sum_list_saa(P1,P2,P3).
sum_list_saa([],_,[]).
sum_list_saa(_,[],[]).

stock_values_add('--',_,'--') :-
   !.
stock_values_add(_,'--','--') :-
   !.
stock_values_add(X,Y,Z) :-
   Z is X + Y.


/* stock_state_group(Pattern,State,States) <-
      Given a state State of stock facts and a pattern Pattern
      of the form `[stock(X1,...,XN)]'.
      State is grouped on all arguments, such that Xi = x. */

stock_state_group(Pattern,State,States) :-
   stock_pattern_to_positions(Pattern,Positions),
   stock_state_sort_by(Positions,State,State_2),
   stock_state_group(Pattern,State_2,[],[],States_2),
   reverse(States_2,States).

stock_state_group(Pattern,[C1,C2|Cs],State,States1,States2) :-
   stock_facts_are_equal_wrt_pattern(Pattern,C1,C2),
   !,
   stock_state_group(Pattern,[C2|Cs],[C1|State],States1,States2).
stock_state_group(Pattern,[C|Cs],State,States1,States2) :-
   reverse([C|State],State1),
   stock_state_group(Pattern,Cs,[],[State1|States1],States2).
stock_state_group(_,[],[],States1,States1) :-
   !.
stock_state_group(_,[],State,States1,[State|States1]).

stock_pattern_to_positions([Atom], Positions) :-
   Atom =.. [_|Args],
   pattern_to_positions(1, Args, Positions).

stock_facts_are_equal_wrt_pattern(C, C1, C2) :-
   C = [A], C1 = [A1], C2 = [A2],
   A =.. Ts, A1 =.. Ts1, A2 =.. Ts2, 
   tuples_are_equal_wrt_pattern(Ts, Ts1, Ts2).


/* stock_state_sort_by(Positions,State_1,State_2) <-
      Given a state State_1 of stock facts and a list
      Positions of numbers, State_1 is sorted w.r.t. the
      arguments indicated by Positions. */

stock_state_sort_by(Positions,State_1,State_2) :-
   maplist( stock_fact_tag_for_sort(Positions),
      State_1, State_3 ),
   list_to_ord_set(State_3,State_4),
   maplist( stock_fact_untag,
      State_4, State_2 ).

stock_fact_tag_for_sort(Positions, C, Tag-C) :-
   C = [A], A =.. [_|Args],
   nth_multiple(Positions, Args, Tag).

stock_fact_untag(_-C,C).


/* stock_company_to_mean_value(Company,Mean_Value) <-
      */

stock_company_to_mean_value(Company,Mean_Value) :-
   mysql_select_by_name(Company,State),
   stock_state_to_mean_value(State,Mean_Value).
 
stock_state_to_mean_value(State,Mean_Value) :-
   findall( Value,
      member([stock(_,_,_,Value,_)],State),
      Values ),
   mean_value_save(Values,Mean_Value).


/******************************************************************/


