

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Portfolios                           ***/
/***                                                            ***/
/******************************************************************/



/* stock_portfolio(Portfolio,Companies,States) <-
      */

stock_portfolio(Portfolio,[C|Cs],[State_m|States]) :-
   C = 'Overall',
   stock_portfolio_tuple(Portfolio,Cs,_,Fs),
   stock_companies_to_states(Cs,States_2),
   stock_states_multiply(Fs,States_2,States),
   stock_state_mean_values(C,States,State_m),
   !.


/* stock_portfolio(Portfolio,Companies,States,Pairs) <-
      */

stock_portfolio(Portfolio,[C|Cs],[State_m|States],Pairs) :-
   C = 'Overall',
   stock_portfolio_tuple(Portfolio,Cs,Ps,Fs),
   vectors_multiply(Ps,Fs,Prices),
   average(Prices,Price),
   pair_lists_2([C|Cs],[Price|Prices],Pairs),
   stock_companies_to_states(Cs,States_2),
   stock_states_multiply(Fs,States_2,States),
   stock_state_mean_values(C,States,State_m),
   !.



/* stock_portfolio_tuple(Portfolio,Cs,Ps,Fs) <-
      */

stock_portfolio_tuple(Portfolio,Cs,Ps,Fs) :-
   stock_portfolio_tuple(Portfolio,Triples),
   triple_lists_2(Cs,Ps,Fs,Triples).


/* stock_portfolio_tuple(Portfolio,Companies,Prices,Factors) <-
      */

stock_portfolio_tuple(Portfolio,Triples) :-
   stock_portfolio_tuple(T),
   [Portfolio,Members] := T^[owner,members],
   findall( Company-Value-Quantity,
      ( member(M,Members),
        [Company,Value,Quantity] :=
           M^[company,value,quantity] ),
      Triples ).


/******************************************************************/


