

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Consulting of Stock States           ***/
/***                                                            ***/
/******************************************************************/
 
 
:- dynamic
      stock_values_cached/2.


/*** interface ****************************************************/


/* stock_companies_to_states(Companies,States) <-
      */

stock_companies_to_states(Companies,States) :-
   dislog_variable_get(storage_mode,mysql),
   !,
   maplist_with_status_bar_dsa( mysql_select_by_name,
      Companies, States_2 ),
   maplist_with_status_bar_dsa(
      stock_state_add_previous_values(long,[week,month,year]),
      States_2, States ).
stock_companies_to_states(Companies,States) :-
   stock_consult(state,State),
   stock_normalize_names(Companies,Companies_Normalized),
   maplist( stock_state_select_generic_2(State),
      Companies_Normalized, States ).

stock_state_select_generic_2(State_1,Company,State_2) :-
   stock_state_select_generic(Company,State_1,State_2).


/* stock_consult(Type,State) <-
      basic predicate for consulting a part, indicated by Type,
      of the DSA database into the state State. */
 
stock_consult(Type,State) :-
   stock_file_save(Type,File),
   stock_values_cached(File,State),
   writeln('<--- cached data: '),
   writeln(File),
   !.
stock_consult(state,State) :-
   !,
   writeln('<--- input file: '),
   stock_consult(stock_all_long,State_1),
%  stock_consult(stock_all_long,State_1l),
%  stock_consult(stock_all_short,State_1s),
%  append(State_1l,State_1s,State_1),
   stock_consult(number_name,State_2),
   write('computing stock join:  '),
   measure_hidden( 1,
      stock_join(State_1,State_2,State) ),
   stock_file(state,File),
   assert(stock_values_cached(File,State)).

stock_consult(stock_all_long,State) :-
   stock_file(stock_data,stock_all_long,File),
   diconsult(File,State).
stock_consult(number_name,State) :-
   stock_file(stock_data,number_name,File),
   diconsult(File,State).


stock_file_save(Type,File) :-
   stock_file(Type,File),
   !.


stock_join(State_1,State_2,State) :-
   findall( [stock(Company,Number,Date,Value,Previous)],
      ( member([stock(Number,Date,Value,Previous)],State_1),
        stock_number_to_name(State_2,Number,Company) ),
      State ).
 

stock_values_cached(show) :-
   findall( X,
      stock_values_cached(X,_),
      List ),
   nl, writeln_list(List).
stock_values_cached(clear) :-
   retractall(stock_values_cached(_,_)).


stock_normalize_names(Names_1,Names_2) :-
   stock_consult(number_name,State),
   maplist( stock_name_to_number(State),
      Names_1, Numbers ),
   maplist( stock_number_to_name(State),
      Numbers, Names_2 ).


/* stock_number_to_name(State,Number,Name) <-
      transforms a stock number Number to its name Name
      based on State. */

stock_number_to_name(Number,Name) :-
   stock_consult(number_name,State),
   stock_number_to_name(State,Number,Name).

stock_number_to_name(State,Number,Name) :-
   member([number_name(Number,String)],State),
   name(Name,String),
   !.


/* stock_name_to_number(State,Name,Number) <-
      transforms a stock name Name to its number Number
      based on State. */

stock_name_to_number(Name,Number) :-
   dislog_variable_get(storage_mode,mysql),
   !,
   mysql_select_name_to_number(
      Name, [[number_name(Number,Name)]] ).
stock_name_to_number(Name,Number) :-
   stock_consult(number_name,State),
   stock_name_to_number(State,Name,Number).

stock_name_to_number(State,Name,Number) :-
   name(Name,String),
   member([number_name(Number,String)],State),
   !.


/* stock_values(Item,State) <-
      selects the stock values State for a company
      or a group Item. */

stock_values(regular,Item,State) :-
   stock_values_sorted(Item,State).
stock_values(weighted,Item,State) :-
   stock_values(Item,State_1),
   stock_state_weight_and_transform(State_1,State).

stock_values_sorted(Item,State) :-
   stock_values(Item,State_1),
   stock_state_sort_by_date(State_1,State).

stock_values(Group,State) :-
   stock_tuple_special(Group,Companies),
   stock_values(stock_all_long,long,Companies,State_2),
   length(Companies,N),
   stock_state_mean_values(Group,N,State_2,State),
   !.
stock_values(Company,State) :-
   determine_stock_value_type(Company,Type),
   name_append(['stock_all_',Type],Data_File_Type),
   stock_values(Data_File_Type,Type,[Company],State),
   !.

stock_values(Dax_File_Type,_,Companies,State) :-
   stock_file(Dax_File_Type,File),
   stock_values_cached(File-Companies,State),
   writeln('<--- cached data: '),
   writeln(File-Companies),
   !.
stock_values(Dax_File_Type,Type,Companies,State) :-
   stock_file(Dax_File_Type,File),
   stock_consult_state_2(Dax_File_Type,Companies,State_2),
   stock_state_add_previous_values(Type,[week,month,year],
      State_2, State ),
   assert( stock_values_cached(File-Companies,State) ).


/* stock_consult_state_2(Type,Companies,State) <-
      */

stock_consult_state_2(_,Companies,State) :-
   dislog_variable_get(storage_mode,mysql),
   !,
   mysql_select_by_names(Companies,State),
   !.
stock_consult_state_2(Dax_File_Type,Companies,State) :-
   writeln('<--- input file: '),
   stock_consult(Dax_File_Type,State_1),
   stock_consult(number_name,State_2),
   stock_state_select(Companies,State_1,State_2,State).


/******************************************************************/


