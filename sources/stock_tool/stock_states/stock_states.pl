

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Stock States                         ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      stock_state_add_previous_values/4.


/*** interface ****************************************************/


/* stock_state_select(Pattern,State_1,State_2) <-
      */


stock_state_select([Group,Date],State) :-
   dislog_variable_get(storage_mode,mysql),
   var(Date),
   !,
   mysql_select_by_group(Group,State).

stock_state_select(Pattern,State) :-
   stock_consult(state,State_1),
   stock_state_sort_by_date(State_1,State_2),
   stock_state_select(Pattern,State_2,State).


stock_state_select([Company,Number,Date],State_1,State_2) :-
   findall( [stock(Company,Number,Date,Value,Previous)],
      member([stock(Company,Number,Date,Value,Previous)],State_1),
      State_2 ).

stock_state_select([Group,Date],State_1,State_2) :-
   findall( [stock(Company,Number,Date,Value,Previous)],
      ( member([stock(Company,Number,Date,Value,Previous)],State_1),
        stock_group(State_1,Company,Group) ),
      State_2 ).


stock_state_select(Companies,State_1,State_2,State_3) :-
   findall( [stock(Company,Number,Date,Value,Previous)],
      ( member(Company,Companies),
        stock_name_to_number(State_2,Company,Number),
        member([stock(Number,Date,Value,Previous)],State_1) ),
      State ),
   list_to_ord_set(State,State_3).

stock_state_select_generic(Group,State_1,State_2) :-
   stock_tuple_special(Group,Companies),
   stock_state_select([Group,_],State_1,State),
   length(Companies,N),
   stock_state_mean_values(Group,N,State,State_2),
   !.
stock_state_select_generic(Company,State_1,State_2) :-
   determine_stock_value_type(Company,Type),
   stock_state_select([Company,_,_],State_1,State_A),
   stock_state_add_previous_values(Type,month,State_A,State_B),
   stock_state_add_previous_values(Type,year,State_B,State_2),
   !.


/* stock_state_sort_by_value(State_1,State_2) <-
      */

stock_state_sort_by_value(State_1,State_2) :-
   findall( [stock(Value,Company,Number,Date,Previous)],
      member([stock(Company,Number,Date,Value,Previous)],State_1),
      State_A ),
   list_to_ord_set(State_A,State_B),
   findall( [stock(Company,Number,Date,Value,Previous)],
      member([stock(Value,Company,Number,Date,Previous)],State_B),
      State_2 ).


/* stock_state_sort_by_date(State_1,State_2) <-
      */

stock_state_sort_by_date(State_1,State_2) :-
   findall( [stock(Y-M-D,Company,Number,Value,Previous)],
      member([stock(Company,Number,D-M-Y,Value,Previous)],State_1),
      State_A ),
   list_to_ord_set(State_A,State_B),
   findall( [stock(Company,Number,D-M-Y,Value,Previous)],
      member([stock(Y-M-D,Company,Number,Value,Previous)],State_B),
      State_2 ).

stock_state_sort_by_date_2(State_1,State_2) :-
   findall( [stock(Company,Y-M-D,Number,Value,Previous)],
      member([stock(Company,Number,D-M-Y,Value,Previous)],State_1),
      State_A ),
   list_to_ord_set(State_A,State_B),
   findall( [stock(Company,Number,D-M-Y,Value,Previous)],
      member([stock(Company,Y-M-D,Number,Value,Previous)],State_B),
      State_2 ).


/* stock_state_group_by_date(State_1,State_2) <-
      */

stock_state_group_by_date(State_1,State_2) :-
   findall( [stock(Y-M-D,Company,Number,Value,Previous)],
      member([stock(Company,Number,D-M-Y,Value,Previous)],State_1),
      State_A ),
   list_to_ord_set(State_A,State_B),
   findall( [stock(Company,Number,D-M-Y,Value,Previous)],
      member([stock(Y-M-D,Company,Number,Value,Previous)],State_B),
      State_2 ).

stock_state_sort_by_weight(State_1,State_2) :-
   findall( [stock(Previous,Company,Number,Date,Value)],
      member([stock(Company,Number,Date,Value,Previous)],State_1),
      State_A ),
   list_to_ord_set(State_A,State_B),
   findall( [stock(Company,Number,Date,Value,Previous)],
      member([stock(Previous,Company,Number,Date,Value)],State_B),
      State_2 ).


/* stock_state_replace_values_by_weights(State_1,State_2) <-
      */

stock_state_replace_values_by_weights(State_1,State_2) :-
   findall( [stock(Comp,Num,Date,Weight,Prev)],
      member([stock(Comp,Num,Date,_,[Weight|Prev])],State_1),
      State_2 ).


/* stock_state_weight(Mode,State,Result) <-
      */

stock_state_weight(grouped,State,Rankings) :-
   stock_groups(Groups),
   maplist( stock_state_weight_for_group(State),
      Groups, Rankings ).
stock_state_weight(regular,State_1,[State_2]) :-
   stock_state_weight(State_1,State_2).


stock_state_weight(State_1,State_2) :-
   dislog_variable_get(stock_weights,Weights),
   findall( [stock(Name,Number,Date,Value,[Weight|Previous])],
      ( member([Atom],State_1),
        Atom = stock(Name,Number,Date,Value,Previous),
        stock_atom_weight(Weights,Atom,Weight) ),
      List ),
   stock_state_sort_by_weight(List,List_Reverse),
   reverse(List_Reverse,State_2),
   !.

stock_state_weight_for_group(State_1,Group,State_2) :-
   stock_state_select([Group,_],State_1,State_3),
   stock_state_weight(State_3,State_2).

stock_atom_weight(Weights,Atom,Weight) :-
   Atom = stock(_,_,_,_,Previous),
   weighted_sum(Previous,Weights,Weight).


/* weighted_sum(Xs,Ws,Sum) <-
      */

weighted_sum([--|Xs],[_|Ws],Weight) :-
   !,
   weighted_sum(Xs,Ws,Weight).
weighted_sum([X|Xs],[W|Ws],Weight) :-
   weighted_sum(Xs,Ws,Weight_2),
%  Y is 1 + X/100,
%  power(Y,D,Z),
%  Weight is W * (Z - 1) * 100 + Weight_2.
   Weight is X * W + Weight_2.
weighted_sum([],[],0).


/* stock_state_normalize(State_1,State_2) <-
      */

stock_state_normalize(State_1,State_2) :-
   stock_state_normalize(State_1,State_2,_).

stock_state_normalize(State_1,State_2,Mean_Value) :-
   stock_state_smooth(State_1,State),
   stock_state_to_mean_value(State,Mean_Value),
   dsa_variable(y_origin_and_schrink,
      [Mean_Value,Origin_Y,Schrink_Y_2]),
   Schrink_Y is Schrink_Y_2 / 20,
   findall( [stock(Name,Number,Date,Value_2,Previous)],
      ( member([stock(Name,Number,Date,Value,Previous)],State),
        \+ true_equal(Value,0),
        value_normalize(linear,Origin_Y,Schrink_Y,Value,Value_2) ),
      State_2 ),
   !.


/* value_normalize(Mode,Origin,Schrink,Value1,Value2) <-
      */

value_normalize(logarithmic,_,_,0,0) :-
   !.
value_normalize(logarithmic,_,0,_,0) :-
   !.
value_normalize(logarithmic,0,_,_,0) :-
   !.
value_normalize(Mode,Origin,Schrink,Value1,Value2) :-
   value_normalize_function(Mode,Value1,Schrink,A),
   value_normalize_function(Mode,Origin,Schrink,B),
   round(A-B,3,Value2).

value_normalize_function(linear,X,Schrink,Y) :-
   Y is X / Schrink.
value_normalize_function(logarithmic,0,_,0) :-
   !.
value_normalize_function(logarithmic,X,Schrink,Y) :-
   Y is 10 * log( X / Schrink ).

value_normalize_2(MX-MY,OX-OY,SX-SY,Time-Value,X-Y) :-
   value_normalize(MX,OX,SX,Time,X),
   value_normalize(MY,OY,SY,Value,Y).


/* stock_state_weight_and_transform(State_1,State_2) <-
      */

stock_state_weight_and_transform(State_1,State_2) :-
   stock_state_weight(State_1,State_3),
   stock_state_replace_values_by_weights(State_3,State_4),
   stock_state_sort_by_date(State_4,State_5),
   dislog_variable_get(smooth_parameter,N),
   stock_state_smooth(N,State_5,State_2),
   !.


/* stock_state_smooth(State_1,State_2) <-
      */

stock_state_smooth(State_1,State_2) :-
   stock_state_sort_by_date(State_1,State),
   dislog_variable_get(smooth_parameter,N),
   stock_state_smooth(N,State,State_2),
   !.

stock_state_smooth(N,State_1,State_2) :-
   stock_state_smooth(N,State_1,[],State_2).

stock_state_smooth(N,[C1|Cs1],Cs2,[C3|Cs3]) :-
   C1 = [stock(Company,Number,Date,Value,Previous)],
   first_n_elements(N,Cs1,CsA),
   first_n_elements(N,Cs2,CsB),
   stock_state_select_values(CsA,VsA),
   stock_state_select_values(CsB,VsB),
   append([VsA,[Value],VsB],Vs),
   mean_value(Vs,V),
   C3 = [stock(Company,Number,Date,V,Previous)],
   stock_state_smooth(N,Cs1,[C1|Cs2],Cs3).
stock_state_smooth(_,[],_,[]).


/* stock_state_select_values(State,Values) <-
      */

stock_state_select_values(State,Values) :-
   findall( Value,
      member([stock(_,_,_,Value,_)],State),
      Values ).


/* stock_state_add_previous_values(
         Type,Xs,State_1,State_2) <-
      */

% stock_state_add_previous_values(Type,Xs,State_1,State_2) :-
%    stock_state_add_previous_values_2(Type,Xs,State_1,State),
%    stock_state_sort_by_date(State,State_2).

stock_state_add_previous_values(Type,[X|Xs],State_1,State_2) :-
   !,
   stock_state_add_previous_values(Type,X,State_1,State_3),
   stock_state_add_previous_values(Type,Xs,State_3,State_2).
stock_state_add_previous_values(_,[],State,State) :-
   !.

stock_state_add_previous_values(_,year,State,State) :-
   dislog_variable_get(chart_start,Start_Year),
   Start_Year \= 1997,
   !.
stock_state_add_previous_values(long,Period,State_1,State_2) :-
   maplist( stock_state_get_previous_values(Period),
      State_1, State_3 ),
   stock_states_merge(State_3,State_1,State_2).
stock_state_add_previous_values(short,_,State,State).

stock_state_get_previous_values(_,C_1,C_2) :-
   C_1 = [stock(_,_,_,_,[])],
   !,
   C_2 = C_1.

stock_state_get_previous_values(year,C_1,C_2) :-
   C_1 = [stock(_,_,_,_,[_,_,_,--])],
   !,
   C_2 = C_1.
stock_state_get_previous_values(year,C_1,C_2) :-
   C_1 = [stock(Name,Number,Date_1,Value_1,[_,_,_,Y])],
   Value_2 is Value_1 / ( 1 + ( Y / 100 ) ),
   minus_one_year(Date_1,Date_2),
   C_2 = [stock(Name,Number,Date_2,Value_2,[])].

stock_state_get_previous_values(month,C_1,C_2) :-
   C_1 = [stock(_,_,_,_,[_,_,--,_])],
   !,
   C_2 = C_1.
stock_state_get_previous_values(month,C_1,C_2) :-
   C_1 = [stock(Name,Number,Date_1,Value_1,[_,_,M,_])],
   Value_2 is Value_1 / ( 1 + ( M / 100 ) ),
   minus_four_weeks(Date_1,Date_2),
   C_2 = [stock(Name,Number,Date_2,Value_2,[])].

stock_state_get_previous_values(week,C_1,C_2) :-
   C_1 = [stock(_,_,_,_,[_,--,_,_])],
   !,
   C_2 = C_1.
stock_state_get_previous_values(week,C_1,C_2) :-
   C_1 = [stock(Name,Number,Date_1,Value_1,[_,W,_,_])],
   Value_2 is Value_1 / ( 1 + ( W / 100 ) ),
   minus_one_week(Date_1,Date_2),
   C_2 = [stock(Name,Number,Date_2,Value_2,[])].


/* stock_states_merge(Cs_1,Cs_2,Cs_3) <-
      */

stock_states_merge(Cs_1,Cs_2,Cs_3) :-
   stock_states_merge_sub(Cs_1,Cs_2,Cs_3).
%  stock_state_sort_by_date(Cs,Cs_3).

stock_states_merge_sub([C_1|Cs_1],Cs_2,Cs_3) :-
   C_1 = [stock(Name,Number,Date_1,_,_)],
   C_2 = [stock(Name,Number,Date_1,_,_)],
   member(C_2,Cs_2),
   stock_states_merge_sub(Cs_1,Cs_2,Cs_3).
stock_states_merge_sub([C_1|Cs_1],Cs_2,[C_1|Cs_3]) :-
   stock_states_merge_sub(Cs_1,Cs_2,Cs_3).
stock_states_merge_sub([],Cs,Cs).


/* stock_states_multiply(Fs,Ss1,Ss2) <-
      */

stock_states_multiply([F|Fs],[S1|Ss1],[S2|Ss2]) :-
   stock_state_multiply(F,S1,S2),
   stock_states_multiply(Fs,Ss1,Ss2).
stock_states_multiply([],[],[]).
 

/* stock_state_multiply(Factor,State_1,State_2) <-
      */

stock_state_multiply(Factor,State_1,State_2) :-
   maplist( stock_fact_multiply(Factor),
      State_1, State_2 ).
 
stock_fact_multiply(Factor,C1,C2) :-
   C1 = [stock(Company,Number,Date,Value1,Previous1)],
   multiply_list([Value1|Previous1],Factor,[Value2|Previous2]),
   C2 = [stock(Company,Number,Date,Value2,Previous2)].


/* stock_state_differentiate(Days,State_1,State_2) <-
      */

stock_state_differentiate(Days,[C1,C2|Cs1],[C3|Cs2]) :-
   C1 = [stock(Company,Number,Date_1,Value_1,_)],
   C2 = [stock(Company,Number,Date_2,Value_2,_)],
   date_to_time_stamp(Date_1,T_1),
   date_to_time_stamp(Date_2,T_2),
   T_1 \= T_2,
   name(Value_1,L_1), name(Value_A,L_1),
   name(Value_2,L_2), name(Value_B,L_2),
   Value_A \= 0,
   Value_B \= 0,
   !,
%  writeln(user,[Date_1,Value_A,Date_2,Value_B]),
   Percents is ( Value_B - Value_A ) / Value_A,
   Value_3 is 1 + Percents * Days / ( T_2 - T_1 ),
   maximum(0,Value_3,Value_4),
   C3 = [stock(Company,Number,Date_2,Value_4,[])],
   stock_state_differentiate(Days,[C2|Cs1],Cs2).
stock_state_differentiate(Days,[_,C2|Cs1],Cs2) :-
   stock_state_differentiate(Days,[C2|Cs1],Cs2).
stock_state_differentiate(_,[_],[]).
stock_state_differentiate(_,[],[]).


/* stock_state_split(Group,State,Names,States) <-
      */

stock_state_split(Group,State,Names,States) :-
   findall( Name,
      stock_group(Name,Group),
      Names ),
   maplist( stock_state_select_state_for_name(State),
      Names, States ).

stock_state_select_state_for_name(State_1,Name,State_2) :-
   findall( [stock(Name,Number,Date,Value,Previous)],
      member(
         [stock(Name,Number,Date,Value,Previous)],
         State_1 ),
      State_2 ).


/* stock_state_to_file(State,File) <-
      */

stock_states_to_file(States,File) :-
   append(States,State),
   stock_state_to_file(State,File).

stock_state_to_file(State,File) :-
   tell(File),
   writeln(':- disjunctive.'), nl,
   checklist( stock_fact_write,
      State ),
   nl, writeln(':- prolog.'),
   told.

stock_fact_write(C) :-
   C = [stock(Company,Number,Date,Value,Previous)],
   write_list([
      'stock(''', Company, ''', ', Number, ', ',
      Date, ', ', Value, ', ', Previous, ').']),
   nl.


/* stock_state_round(Round,State_1,State_2) <-
     */

stock_state_round(Round,State_1,State_2) :-
   findall( [stock(Name,Number,Date,Value_2,Previous)],
      ( member(
           [stock(Name,Number,Date,Value_1,Previous)],
           State_1 ),
        round(Value_1,Round,Value_2) ),
      State_2 ).


/* stock_state_abstract(State_1,State_2) <-
      */

stock_state_abstract(State_1,State_2) :-
   stock_state_abstract(20,1,State_1,State_2).

stock_state_abstract(N,Center,State_1,State_2) :-
   interval_abstraction(N,Center,Intervals),
   maplist( stock_fact_abstract(Intervals),
      State_1, State_2 ).

stock_fact_abstract(Intervals,C1,C2) :-
   C1 = [stock(Company,Number,Date,Value_1,Previous)],
   member([X,Y],Intervals),
   interval_contains([X,Y],Value_1),
   Value_2 is ( X + Y ) / 2,
   C2 = [stock(Company,Number,Date,Value_2,Previous)],
   !.
stock_fact_abstract(_,C,C).


/* interval_contains([X,Y],Z) <-
      */

interval_contains([X,Y],Z) :-
   X =< Z,
   Z < Y.


/* interval_abstraction(N,Center,Intervals) <-
      */

interval_abstraction(N,Center,Intervals) :-
   generate_interval(0,N,I),
   Start is Center * 0.525,
   Delta is Center / N,
   maplist( number_to_interval_abstract(Start,Delta),
      I, Intervals ).

number_to_interval_abstract(Start,Delta,N,[X,Y]) :-
   X is Start + N * Delta,
   Y is Start + ( N + 1 ) * Delta.
   
   
/* stock_fact_value_is_not_zero(C) <-
      */

stock_fact_value_is_not_zero(C) :-
   C = [stock(_,_,_,Value,_)],
   \+ 0 is Value.


/* stock_state_prune(State_1,State_2) :-
      */

stock_state_prune([C1,C2|Cs1],Cs2) :-
   C1 = [stock(Company,Number,Date,_,_)],
   C2 = [stock(Company,Number,Date,_,_)],
   stock_state_prune([C2|Cs1],Cs2).
stock_state_prune([C1|Cs1],[C1|Cs2]) :-
   stock_state_prune(Cs1,Cs2).
stock_state_prune([],[]).


/* stock_state_thin(State_1,State_2) <-
      */

stock_state_thin(N,State_1,[C|State_2]) :-
   length(State_1,L),
   L >= N,
   first_n_elements(State_1,N,State,State_3),
   stock_state_to_mean_fact(State,C),
   stock_state_thin(N,State_3,State_2).
stock_state_thin(_,State,[C]) :-
   stock_state_to_mean_fact(State,C).
stock_state_thin(_,[],[]).

stock_state_to_mean_fact(State,C) :-
   State = [[stock(Company,Number,Date,_,_)]|_],
   stock_state_to_mean_value(State,Mean),
   C = [stock(Company,Number,Date,Mean,[])].


/* stock_state_for_pattern_search(Item,N,State) <-
      */

stock_state_for_pattern_search(Item,N,State) :-
   stock_values(Item,State_1),
   !,
   stock_state_sort_by_date(State_1,State_2),
   stock_state_thin(N,State_2,State).
 

/******************************************************************/


