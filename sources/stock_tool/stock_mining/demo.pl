
/******************************************************************/
/***                                                            ***/
/***       STOCK Mining Tools: Demo Mining with Stock Data      ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library(pce)).   
:- use_module(contrib(contrib)).

:- require(
      [ emacs/1, forall/2, member/2, term_to_atom/2 ]).



stock_mining_predicates :-
   new(B, browser('DSA Mining Predicates', size(40,10))),
   send(B, confirm_done, @off),
   send(B, tab_stops, vector(150)),
   fill_stock_mining_predicates_browser(B),
   new(D, dialog),
   send(D, below, B),
   send(D, append, new(O, button(execute, 
      message(@prolog, execute_mining_demo,B)))),
   send(D, append, button(view_source, 
      message(@prolog,view_stock_mining_source_file, B))),
   send(D, append, button(quit, message(D?frame, free))),
   send(D, default_button, execute),
   send(B, open_message, message(O, execute)),
   send(B, style, title, style(font := font(helvetica, bold, 14))),
   send(B, open).

fill_stock_mining_predicates_browser(B) :-
   forall( stock_mining_demo_predicate(Name,Args,_),
      send(B, append,
         dict_item(Name,string('%s     %s', Name, Args))) ).


execute_mining_demo(Browser) :-
   get(Browser, selection, DictItem),
   (   (   DictItem == @nil
       ;   get(DictItem, style, title)
       )
   ->  send(@display, inform, 'First select a mining predicate')
   ;   get(DictItem, key, Name),
       dislog_variable_get(xpce_stock,Stock),
       Predicate =.. [Name,Stock,Result],
       call(Predicate),
       % term_to_atom(Predicate,Atom),
       name_append(['executing >>',Name,'<< for ',Stock],String),
       stock_announcement(String),
       put_to_dsa_announcement(Result)
   ).  


put_to_dsa_announcement(Term:[A,B]) :-
   term_to_atom(Term,Atom),
   name_append([Atom,':[',A,',',B,'].'],String),
   stock_announcement(String).


view_stock_mining_source_file(Browser) :-
   get(Browser, selection, DictItem),
   (   DictItem == @nil
   ->  send(@display, inform, 'First select a demo')
   ;   get(DictItem, key, Name),
       stock_mining_demo_predicate(Name,_,File),                        
       stock_file(sources_directory,Directory),
       name_append([Directory,'/',File],File_Path),
       emacs(File_Path)
   ).




/* stock_mining_demo_predicate(Predicate,Arguments,Location). */


stock_mining_demo_predicate(
   mine_stock_ups,2,stock_mining_main).
stock_mining_demo_predicate(
   mine_stock_downs,2,stock_mining_main).
stock_mining_demo_predicate(
   mine_stock_grouped_ups_3_to_4,2,stock_mining_main).
stock_mining_demo_predicate(
   mine_stock_grouped_ups_3_to_5,2,stock_mining_main).
stock_mining_demo_predicate(
   mine_stock_grouped_ups_4_to_5,2,stock_mining_main).
stock_mining_demo_predicate(
   mine_stock_grouped_downs_3_to_4,2,stock_mining_main).
stock_mining_demo_predicate(
   mine_stock_grouped_downs_3_to_5,2,stock_mining_main).
stock_mining_demo_predicate(
   mine_stock_grouped_downs_4_to_5,2,stock_mining_main).



select_xpce_stock :-
   new(B, browser('DAX-100 Stocks', size(40,10))),
   send(B, confirm_done, @off),
   send(B, tab_stops, vector(150)),
   stock_companies(Companies),
   fill_stock_browser(B,Companies),
   new(D, dialog),
   send(D, below, B),
   send(D, append,
      new(O, button(select, message(@prolog,set_xpce_stock,B)))),
   send(D, append, button(quit, message(D?frame, free))),
   send(D, default_button, select),   
   send(B, open_message, message(O, execute)),
   send(B, style, title, style(font := font(helvetica, bold, 14))),
   send(B, open).
   
fill_stock_browser(B,[Company|Companies]) :-
   name_to_nice_name(Company,NN),
   send(B, append, dict_item(Company,string('%s    ', NN))),
   fill_stock_browser(B,Companies).
fill_stock_browser(_,[]).


set_xpce_stock(Browser) :-
   get(Browser, selection, DictItem),
   (   (   DictItem == @nil
       ;   get(DictItem, style, title)
       )
   ->  send(@display, inform, 'First select a mining predicate')
   ;   get(DictItem, key, Company),
       dislog_variable_set(xpce_stock,Company),
       name_append([' --> xpce stock := ',Company],String),
       stock_announcement(String),
       write_debug(Company)
   ).


/******************************************************************/


