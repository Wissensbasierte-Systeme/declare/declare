
/******************************************************************/
/***                                                            ***/
/***       Stock Tool:  XPCE Interface Configuration            ***/
/***                                                            ***/
/***       by Adrian Pillo                                      ***/
/***                                                            ***/
/******************************************************************/


% :- pce_autoload(finder,library(finder)).
% :- pce_global(@finder,new(finder)).


stock_mining(mining_directory,Directory) :-
   stock_file(home,mining,Directory).

stock_mining(mining_results,Directory) :-
   stock_mining(mining_directory,Dir),
   name_append([Dir,'/results'],Directory).


stock_mining(mining_gnuplot_values_file,File) :-
   stock_mining(mining_directory,Dir),
   name_append([Dir,'/gnuplot','/','stock.dat'],File).

stock_mining(mining_gnuplot_pic_file,'stockpic.ppm').

stock_mining(mining_gnuplot_batch_file,File) :-
   stock_mining(mining_directory,Dir),
   name_append([Dir,'/gnuplot','/','stock'],File).


stock_mining(chart_ups_downs,File_Name) :-
   stock_mining(mining_results,Dir),
   name_append([Dir,'/chart_ups_downs'],File),
   absolute_file_name(File,File_Name).

stock_mining(meta_data,File_Name) :-
   stock_mining(mining_results,Dir),
   name_append([Dir,'/meta_data'],File),
   absolute_file_name(File,File_Name).

stock_mining_exists_file(File_Name) :-
   stock_mining(mining_results,Dir),
   name_append([Dir,'/',File_Name],File),
   exists_file(File).


/******************************************************************/


