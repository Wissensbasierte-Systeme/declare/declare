

/******************************************************************/
/***                                                            ***/
/***          STOCK Mining Tools: Mining with Stock Data        ***/
/***                                                            ***/
/******************************************************************/


stock_test :-
   stock_time_sequence(adidas,Time_Sequence),
   DisLog_Term =.. [result,Time_Sequence],
   % write_ln(DisLog_Term),
   dportray(lp,Time_Sequence),
   stock_mining(chart_ups_downs,File_Name),
   write_debug_list(['saving results in: ',File_Name]),
   dsave_2([[DisLog_Term]],File_Name).

sm_init :-
   dislog_flag_set(mining_protocol,1), 
   dislog_flag_set(meta_data_file,meta_data),
   stock_mining_initialisation.

stock_mining_initialisation :-
   stock_mining(mining_results,Directory),
   exists_directory(Directory),
   write('OK, directory already exists. '),
   init_meta_data_file,
   !.
stock_mining_initialisation :-
   write('Can''t find directory. Trying to create it ...'),
   stock_mining(mining_directory,Directory),
   exists_directory(Directory),
   stock_mining(mining_results,Directory_2),
   concat_atom(['mkdir ',Directory_2, ' > /dev/null'],Command),
   shell(Command),
   write('OK ...'),
   init_meta_data_file.
stock_mining_initialisation :-
   stock_mining(mining_directory,Directory_1),
   concat_atom(['mkdir ',Directory_1, ' > /dev/null'],Command_1),
   shell(Command_1),
   stock_mining(mining_results,Directory_2),
   concat_atom(['mkdir ',Directory_2, ' > /dev/null'],Command_2),
   shell(Command_2),
   write('OK ...'),
   init_meta_data_file.

init_meta_data_file :-
   stock_mining(meta_data,File),
   dislog_time(A,B),
   dsave_2([[starting]-[at]-[time_stamp(A,B)]],File). 

reset_meta_data_file :-
   stock_mining(meta_data,File_Name),
   delete_file(File_Name),
   init_meta_data_file.
reset_meta_data_file :-
   init_meta_data_file. 
      

dsave_stock_mining(Program,File_Name) :-
   stock_mining(mining_results,Dir),
   concat_atom([Dir,'/',File_Name],File),
   dsave_2(Program,File).

dconsult_stock_mining(File_Name,Program) :-
   stock_mining(mining_results,Dir),
   concat_atom([Dir,'/',File_Name],File),
   dconsult_2(File,Program).

dconsult_2(A, B) :-
   dconsult_2(A),
   current_program(B),
   clear_current_program_2, !.

dconsult_2(A) :-
   clear_current_program,
%  dis_ex_extend_filename(A, B),
   [A].

dsave_2(A,B) :-
   dstore_2(A,B).

dstore_2(A,B) :-
%  dis_ex_extend_filename(B, C),
   distore(A,B).


/*************** begin old implementation ***************/


register_meta_data(Program) :-
   write_debug_list(['overwriting with: ',Program]),
   stock_mining(meta_data,File_Name),
   write_debug_list(['saving in: ',File_Name]),
   dsave_2(Program,File_Name).

get_meta_data(Program) :-
   stock_mining(meta_data,File_Name),
   dconsult_2(File_Name,Program),
   write_debug(Program).

update_meta_data(Program) :-
   get_meta_data(Program_1),
   Program = [[Fact]],
   \+ member([Fact],Program_1),
   append(Program_1,Program,Program_2),
   write_debug_list(['updating meta data with: ',Program_2]),
   register_meta_data(Program_2).
update_meta_data(_) :-
   write(user,'Meta data already exists! Skipping update operation ...').


protocolate_result(Predicate,Type,Stock_Id,File) :-
   Protocol =.. [Predicate,Type,Stock_Id,File],
   update_meta_data([[Protocol]]).   


check_exist_data(Predicate,Type,Stock_Id,File) :-
   get_meta_data(Program),
   Protocol =.. [Predicate,Type,Stock_Id,File],
   member([Protocol],Program),
   bar_line,
   write_list(['data exists in file ',File]),
   nl,
   bar_line.
% check_exist_data(_,_,_,_).


/*************** end old implementation ***************/


remove_blanks_add_c_replace_var(String,String_Without_Blanks) :-
   atom(String),
   concat(c,String,C_String),
   name(C_String,List),
   remove_char32_45_from_list(List,List2),
   name(String_Without_Blanks,List2).
remove_blanks_add_c_replace_var(X,Dummy) :-
   var(X),
   dislog_variable_get(place_holder_for_vars,Dummy).

remove_char32_45_from_list(List,List2) :-
   remove_char32_45_from_list(List,[],List2).
remove_char32_45_from_list([C|Cs],List1,List2) :-
   C \== 32,
   C \== 45,
   C \== 46,
   append(List1,[C],Sofar),
   remove_char32_45_from_list(Cs,Sofar,List2).
remove_char32_45_from_list([_|Cs],List1,List2) :-
   remove_char32_45_from_list(Cs,List1,List2).
remove_char32_45_from_list([],L,L).


prepare_goal_for_meta_data(Goal,Prepared_Goal) :-
   Goal =.. [Pred|Args],
   maplist(remove_blanks_add_c_replace_var,Args,New_Args),
   Prepared_Goal =.. [Pred|New_Args].
   

update_protocol_result(Goal,Result) :-
   update_protocol_file(Goal,File),
   update_protocol(File,Result).


/* update_protocol(File,Result) <-
      writing Result into File */

update_protocol(File,Result) :-
   dsave_stock_mining(Result,File).


/* update_protocol_file(Goal,File) <-
      generating a file name from
      goal predicates and arguments */

update_protocol_file(Goal,File) :-
   prepare_goal_for_meta_data(Goal,Prepared_Goal),
   Prepared_Goal =.. L,
   pred_args_to_file_name(L,File),
%  ddd_replace_vars(L,New_L),
   update_stock_tool_meta_file(Prepared_Goal,File).

ddd_replace_vars(L,New_L) :-
   ddd_replace_vars(L,[],New_L).
ddd_replace_vars([A|As],L1,L2) :-
   nonvar(A),
   concat(c,A,Ac),
   append(L1,[Ac],Sofar),
   ddd_replace_vars(As,Sofar,L2).
ddd_replace_vars([_|As],L1,L2) :-
   dislog_variable_get(place_holder_for_vars,Dummy),
   append(L1,[Dummy],Sofar),
   ddd_replace_vars(As,Sofar,L2).
ddd_replace_vars([],Sofar,Sofar).
   

/* check_protocol(Goal,Result) <-
      if file exists then load 
      DisLog-Program into Result
      else fail */

check_protocol(Goal,Result) :-
   prepare_goal_for_meta_data(Goal,Prepared_Goal),
   Prepared_Goal =.. L,
   pred_args_to_file_name(L,File),
   !,
   check_stock_tool_meta_file(Prepared_Goal,File,Time),
   stock_mining_exists_file(File),
   Time = time_stamp(A,B),
   write_list(['reading data with time ',A,
      ' and date ',B,' USING CACHE DATA ...']),nl,
   dconsult_stock_mining(File,Result).


pred_args_to_file_name(L,File) :-
   pred_args_to_file_name(L,tmp,File).
pred_args_to_file_name([A|As],F,File) :-
   nonvar(A),
   name_append([F,'_',A],New_F),
   pred_args_to_file_name(As,New_F,File).
pred_args_to_file_name([_|As],F,File) :-
   dislog_variable_get(place_holder_for_vars,Dummy),
   name_append([F,'_',Dummy],New_F),
   pred_args_to_file_name(As,New_F,File).
pred_args_to_file_name([],F,F).
   

check_stock_tool_meta_file(Prepared_Goal,File,Time) :-
   get_stock_tool_meta_data(Program),
%  ddd_replace_vars(L,New_L),
   member([Prepared_Goal]-[File]-[Time],Program).


/* update_stock_tool_meta_file(Goal,File) <- 
      updating meta data file */ 

update_stock_tool_meta_file(Prepared_Goal,File) :-
   get_stock_tool_meta_data(Program_1),
   \+ member([Prepared_Goal]-_-_,Program_1),
   dislog_time(A,B),
   append( Program_1,
      [[Prepared_Goal]-[File]-[time_stamp(A,B)]],
      Program_2 ),
   write_debug_list(['updating meta data with: ',Program_2]),
   stock_mining(meta_data,File_Name),
   write_debug_list(['saving in: ',File_Name]),
   dsave_2(Program_2,File_Name).   


get_stock_tool_meta_data(Program) :-
   stock_mining(meta_data,File_Name),
   dconsult_2(File_Name,Program),
   write_debug(Program).



/* file output utilities */

append_new_line_to_file(Line,File) :-
   telling(Old),
   append(File),
   write_ln(Line),
   told,
   tell(Old).

write_new_line_to_file(Line,File) :-
   telling(Old),
   tell(File),
   write_ln(Line),
   told,
   tell(Old).

write_line_list_to_file(Lines,File) :-
   telling(Old),
   tell(File),
   output_lines_with_cr(Lines),
   told,
   tell(Old).

append_line_list_to_file(Lines,File) :-
   telling(Old),
   append(File),
   output_lines_with_cr(Lines),
   told,
   tell(Old).

output_lines_with_cr([Line|Lines]) :-
   write_ln(Line),
   output_lines_with_cr(Lines).
output_lines_with_cr([]).


read_lines_from_file(Lines,File) :-
   seeing(Old),
   see(File),
   read(Lines),
   at_end_of_stream,
   seen,
   see(Old).
   
/*
write_in(Result) :-
   stock_mining(chart_ups_downs,File),
   telling(Old),
%  tell(File),
   append(File),
   dportray(lists,Result),
   told,
   tell(Old).

read_out(Result) :-
   stock_mining(chart_ups_downs,File),
   seeing(Old),
   see(File),
   read(Result),
   at_end_of_stream,
   seen,
   see(Old).
*/


get_list_of_stocks_in_group(Name_List, Group) :-
   findall( Name_2,
      ( stock_group(Name_1,Group),
        name_html_to_text(Name_1,Name_2) ),
      Name_List ).


get_all_stocks(Stocks) :-
   stock_companies(Stocks).


%   highest_stock(Value,High),
%   lowest_stock(Value,Low),
   
   
/******************************************************************/
   
