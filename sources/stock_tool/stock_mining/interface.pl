

/******************************************************************/
/***                                                            ***/
/***       STOCK Mining Tools: Mining with Stock Data           ***/
/***                                                            ***/
/******************************************************************/


/* two complementary fields:
   1. fundamental analysis
       = analysis of company data
   2. technical stock analysis (what we do)
       = observing stock charts */


mine_prob_up_group(Group,Result) :-
   get_list_of_stocks_in_group(Name_List, Group),
   get_annotation_up_group([1,0],[L,H],Name_List),
   Annotation = [L,H],
   create_annotated_atom(prob_up_group(Group),Annotation,Result),
   bar_line,
   dportray(aa,Result),
   bar_line.


mine_prob_down_group(Group,Result) :-
   get_list_of_stocks_in_group(Name_List, Group),
   get_annotation_down_group([1,0],[L,H],Name_List), 
   Annotation = [L,H], 
   create_annotated_atom(prob_down_group(Group),Annotation,Result), 
   bar_line,
   dportray(aa,Result),
   bar_line.


mine_stock_ups(Stock,Result) :-
   stock_prob_ups(Stock,Prob),
   name_to_nice_name(Stock,S),
   Annotation = [Prob,Prob],
   create_annotated_atom(stock_up(S),Annotation,Result),
   bar_line,
   dportray(aa,Result),
   bar_line.


mine_stock_downs(Stock,Result) :-
   stock_prob_downs(Stock,Prob),
   name_to_nice_name(Stock,S),
   Annotation = [Prob,Prob],
   create_annotated_atom(stock_down(S),Annotation,Result),
   bar_line,
   dportray(aa,Result),
   bar_line.


mine_stock_grouped_ups_3_to_4(Stock,Result) :-
   get_grouped_ups(Stock,3,State_3),
   get_grouped_ups(Stock,4,State_4),
   length(State_3,L3),
   length(State_4,L4),
   Prob is L4 / L3,
   Annotation = [Prob,Prob],
   name_to_nice_name(Stock,S),
   create_annotated_atom(stock_up_3_to_4(S),Annotation,Result),
   bar_line,
   dportray(aa,Result),
   bar_line.

mine_stock_grouped_ups_3_to_5(Stock,Result) :-
   get_grouped_ups(Stock,3,State_3),
   get_grouped_ups(Stock,5,State_5),
   length(State_3,L3),
   length(State_5,L5),
   Prob is L5 / L3,
   Annotation = [Prob,Prob], 
   name_to_nice_name(Stock,S),
   create_annotated_atom(stock_up_3_to_5(S),Annotation,Result), 
   bar_line,
   dportray(aa,Result),
   bar_line.

mine_stock_grouped_ups_4_to_5(Stock,Result) :-
   get_grouped_ups(Stock,4,State_4),
   get_grouped_ups(Stock,5,State_5),
   length(State_4,L4),
   length(State_5,L5),
   Prob is L5 / L4,
   Annotation = [Prob,Prob], 
   name_to_nice_name(Stock,S),
   create_annotated_atom(stock_up_4_to_5(S),Annotation,Result), 
   bar_line,
   dportray(aa,Result),
   bar_line.

mine_stock_grouped_downs_3_to_4(Stock,Result) :-
   get_grouped_downs(Stock,3,State_3),
   get_grouped_downs(Stock,4,State_4),
   length(State_3,L3),
   length(State_4,L4),
   Prob is L4 / L3,
   Annotation = [Prob,Prob], 
   name_to_nice_name(Stock,S),
   create_annotated_atom(stock_down_3_to_4(S),Annotation,Result), 
   bar_line,
   dportray(aa,Result),
   bar_line.

mine_stock_grouped_downs_3_to_5(Stock,Result) :-
   get_grouped_downs(Stock,3,State_3),
   get_grouped_downs(Stock,5,State_5),
   length(State_3,L3),
   length(State_5,L5),
   Prob is L5 / L3,
   Annotation = [Prob,Prob], 
   name_to_nice_name(Stock,S),
   create_annotated_atom(stock_down_3_to_5(S),Annotation,Result), 
   bar_line,
   dportray(aa,Result),
   bar_line.

mine_stock_grouped_downs_4_to_5(Stock,Result) :-
   get_grouped_downs(Stock,4,State_4),
   get_grouped_downs(Stock,5,State_5),
   length(State_4,L4),
   length(State_5,L5),
   Prob is L5 / L4,
   Annotation = [Prob,Prob], 
   name_to_nice_name(Stock,S),
   create_annotated_atom(stock_down_4_to_5(S),Annotation,Result), 
   bar_line,
   dportray(aa,Result),
   bar_line.



/* base predicates supporting mining */


get_annotation_up_group([L,H],[Lo,Hi],[Stock|Stocks]) :-
   mine_stock_ups(Stock,Ann_Res),
   Ann_Res = stock_up(_):[Value,_],
   (   L > Value
   ->  NL = Value    
   ;   NL = L
   ),
   (   H < Value
   ->  NH = Value  
   ;   NH = H
   ),
   write_ln(Value), 
   get_annotation_up_group([NL,NH],[Lo,Hi],Stocks).
get_annotation_up_group([L,H],[L,H],[]).


get_annotation_down_group([L,H],[Lo,Hi],[Stock|Stocks]) :-
   mine_stock_downs(Stock,Ann_Res),
   Ann_Res = stock_down(_):[Value,_],
   (   L > Value
   ->  NL = Value
   ;   NL = L
   ),
   (   H < Value
   ->  NH = Value
   ;   NH = H
   ),
   write_ln(Value),
   get_annotation_down_group([NL,NH],[Lo,Hi],Stocks).
get_annotation_down_group([L,H],[L,H],[]).



stock_count_ups(Stock,Nr) :-
   check_protocol(stock_count_ups(Stock,Nr),[[Nr]]).

stock_count_ups(Stock,Nr) :-
   stock_values_sorted(Stock,State),
   update_protocol_file(stock_count_ups(Stock,Nr),File),
   count_ups_and_downs(State,0,0,0,Nr,_,_,_),
   update_protocol(File,[[Nr]]).


stock_count_downs(Stock,Nr) :-
   check_protocol(stock_count_downs(Stock,Nr),[[Nr]]).

stock_count_downs(Stock,Nr) :-
   stock_values_sorted(Stock,State),
   update_protocol_file(stock_count_downs(Stock,Nr),File),
   count_ups_and_downs(State,0,0,0,_,Nr,_,_),
   update_protocol(File,[[Nr]]).


stock_prob_ups(Stock,Prob) :-
   stock_count_downs(Stock,Nr_Downs),
   stock_count_ups(Stock,Nr_Ups),
   Prob is Nr_Ups / (Nr_Ups + Nr_Downs).

stock_prob_downs(Stock,Prob) :-   
   stock_count_downs(Stock,Nr_Downs),
   stock_count_ups(Stock,Nr_Ups),
   Prob is Nr_Downs / (Nr_Ups + Nr_Downs).


/* counting ups and downs of a stock */

stock_count_ups_and_downs(Stock,Nr_Ups,Nr_Downs) :-
   check_protocol(stock_count_ups_and_downs(Stock,Nr_Ups,Nr_Downs),
      [[Nr_Ups],[Nr_Downs]]).


stock_count_ups_and_downs(Stock,Nr_Ups,Nr_Downs) :-
   stock_values_sorted(Stock,State),
   update_protocol_file(stock_count_ups_and_downs(Stock,Nr_Ups,Nr_Downs),
      File),
   count_ups_and_downs(State,0,0,0,Nr_Ups,Nr_Downs,_,_),
   update_protocol(File,[[Nr_Ups],[Nr_Downs]]).


stock_time_sequence(Stock,Time_Sequence) :-
   check_protocol(stock_time_sequence(Stock,Time_Sequence),
      [[list(Time_Sequence)]]).

stock_time_sequence(Stock,Time_Sequence) :-
   update_protocol_file(stock_time_sequence(Stock,Time_Sequence),
      File),
   stock_values_sorted(Stock,State),
   count_ups_and_downs(State,0,0,0,_,_,[],Time_Sequence),
   update_protocol(File,[[list(Time_Sequence)]]).

/* end mining supporting rules */


/* begin experimental */

stock_sum_relative_ups_downs(Stock,Sum_Up,Sum_Down) :-
   stock_time_sequence(Stock,Time_Sequence),
   sum_neg_and_pos_values(Time_Sequence,0,Sum_Up,0,Sum_Down).

sum_neg_and_pos_values([[value(_,_,_,Value)]|States],
      SU,Sum_Up,SD,Sum_Down) :-
   (   Value > 0
   ->  SofarU is SU + Value,
       SofarD is SD
   ;   SofarD is SD + Value,
       SofarU is SU
   ),
   sum_neg_and_pos_values(States,SofarU,Sum_Up,SofarD,Sum_Down).
sum_neg_and_pos_values([],SU,SU,SD,SD).   


difference_up_down(Dif) :-
   dislog_variable_get(xpce_stock,Stock),
   difference_up_down(Stock,Dif).
   
difference_up_down(Stock,Dif) :-
   stock_sum_relative_ups_downs(Stock,Sum_Up,Sum_Down),
   Dif is Sum_Up + Sum_Down.

relation_up_down(Rel) :-
   dislog_variable_get(xpce_stock,Stock),
   relation_up_down(Stock,Rel).

relation_up_down(Stock,Rel) :-
   stock_sum_relative_ups_downs(Stock,Sum_Up,Sum_Down),
   Rel is Sum_Up / Sum_Down * -1.
   

/* end experimental */


/* count_ups_and_downs(State,Counter,Nr_U,Nr_D,
      Nr_Ups,Nr_Downs,Time_Row) <-
      counts and shows the ups (pos. dif.) and downs (neg. dif.)
      for a state form one day to the next day */

count_ups_and_downs([[stock(_,_,_,V_1,_)]|States],
      Counter,Nr_U,Nr_D,Nr_Ups,Nr_Downs,TR,Time_Row) :-   
   States = [[stock(_,P_2,D_2,V_2,_)]|_],
   V_1 < V_2,
   New_Counter is Counter + 1,
   New_Nr_U is Nr_U + 1,
   percent_change_from_to(V_1,V_2,Percent),
   write_debug_list([New_Counter,'. ',V_1,' ... ',V_2,' --> ',
      New_Nr_U,'.  UP ! with: ',Percent,'%']),
   append(TR,[[value(P_2,D_2,up,Percent)]],New_TR),
   count_ups_and_downs(States,New_Counter,
      New_Nr_U,Nr_D,Nr_Ups,Nr_Downs,New_TR,Time_Row).
count_ups_and_downs([[stock(_,_,_,V_1,_)]|States],
      Counter,Nr_U,Nr_D,Nr_Ups,Nr_Downs,TR,Time_Row) :-
   States = [[stock(_,P_2,D_2,V_2,_)]|_],
   V_1 >= V_2,
   New_Counter is Counter + 1,
   New_Nr_D is Nr_D + 1,
   percent_change_from_to(V_1,V_2,Percent),
   write_debug_list([
      New_Counter,'. ',V_1,' ... ',V_2,' --> ',
      New_Nr_D,'.  DOWN with: ',Percent,'%']),
   append(TR,[[value(P_2,D_2,down,Percent)]],New_TR),
   count_ups_and_downs(States,New_Counter,
      Nr_U,New_Nr_D,Nr_Ups,Nr_Downs,New_TR,Time_Row).
count_ups_and_downs([_|States],Counter,
      Nr_U,Nr_D,Nr_Ups,Nr_Downs,TR,Time_Row) :-
   New_Counter is Counter + 1,
   nl,
   count_ups_and_downs(States,New_Counter,
      Nr_U,Nr_D,Nr_Ups,Nr_Downs,TR,Time_Row).
count_ups_and_downs([_],_,Nr_U,Nr_D,Nr_U,Nr_D,TR,TR).


/* percent_change_from_to(Value_1,Value_2,Percent) <-
      percentage of the diference of Value_2 - Value_1 */

percent_change_from_to(Value_1,Value_2,Percent) :-
   Percent is (Value_2 - Value_1) * 100 / Value_1.

/* change_from_date(Stock,Date_1,Date_2,Change) <-
      takes the Stockvalues of Date_1 and Date_2
      and computes the change rate in percent */

change_from_date(Stock,Date_1,Date_2,Change) :-
   stock_values_sorted(Stock,State),
   member([stock(_,_,Date_1,Value_1,_)],State),
   member([stock(_,_,Date_2,Value_2,_)],State),
   write_debug_list([
      'First Value: ',Value_1,
      'Second Value: ',Value_2]),
   percent_change_from_to(Value_1,Value_2,Change).


/* get_grouped_ups(Stock,Nr,State) <-
      get groups of consecutive ups,
      and store the items in state */

get_grouped_ups(Stock,Nr,State) :-
   stock_time_sequence(Stock,Time_Sequence),
%  write(Time_Sequence),
   !,
   count_grouped_ups(Nr,Time_Sequence,[],State).



count_grouped_ups(3,[[value(V11,V12,up,V14)]|Tail],S1,S3) :-
   Tail = [[value(V21,V22,up,V24)],
      [value(V31,V32,up,V34)]|_],
   append(S1,[[group([value(V11,V12,up,V14),
      value(V21,V22,up,V24),
      value(V31,V32,up,V34)])]],S2),
   write_debug_list(['G3: ',value(V11,V12,up,V14),
      value(V21,V22,up,V24),
      value(V31,V32,up,V34)]),
   count_grouped_ups(3,Tail,S2,S3).
count_grouped_ups(4,[[value(V11,V12,up,V14)]|Tail],S1,S3) :-
   Tail = [[value(V21,V22,up,V24)],
      [value(V31,V32,up,V34)],
      [value(V41,V42,up,V44)]|_],
   append(S1,[[group([value(V11,V12,up,V14),
      value(V21,V22,up,V24),
      value(V31,V32,up,V34),
      value(V41,V42,up,V44)])]],S2),
   write_debug_list(['G4: ',value(V11,V12,up,V14),
      value(V21,V22,up,V24),
      value(V31,V32,up,V34),
      value(V41,V42,up,V44)]),
   count_grouped_ups(4,Tail,S2,S3).
count_grouped_ups(5,[[value(V11,V12,up,V14)]|Tail],S1,S3) :-
   Tail = [[value(V21,V22,up,V24)],
      [value(V31,V32,up,V34)],
      [value(V41,V42,up,V44)],
      [value(V51,V52,up,V54)]|_],
   append(S1,[[group([value(V11,V12,up,V14),
      value(V21,V22,up,V24),
      value(V31,V32,up,V34),
      value(V41,V42,up,V44),
      value(V51,V52,up,V54)])]],S2),
   write_debug_list(['G5: ',value(V11,V12,up,V14),
      value(V21,V22,up,V24),
      value(V31,V32,up,V34),
      value(V41,V42,up,V44),
      value(V51,V52,up,V54)]),
   count_grouped_ups(5,Tail,S2,S3).
count_grouped_ups(Nr,[_|Tail],S1,S3) :-
   count_grouped_ups(Nr,Tail,S1,S3).
count_grouped_ups(_,[],S,S).


/* get_grouped_downs(Stock,Nr,State) <-
      get groups of consecutive downs,
      and store the items in state */

get_grouped_downs(Stock,Nr,State) :-
   stock_time_sequence(Stock,Time_Sequence),
   !,
   count_grouped_downs(Nr,Time_Sequence,[],State).


count_grouped_downs(3,[[value(V11,V12,down,V14)]|Tail],S1,S3) :-
   Tail = [[value(V21,V22,up,V24)],[value(V31,V32,down,V34)]|_],
   append(S1,[[group([value(V11,V12,down,V14),
      value(V21,V22,down,V24),
      value(V31,V32,down,V34)])]],S2),
   write_debug_list(['G3: ',value(V11,V12,down,V14),
      value(V21,V22,down,V24),
      value(V31,V32,down,V34)]),
      count_grouped_downs(3,Tail,S2,S3).
count_grouped_downs(4,[[value(V11,V12,down,V14)]|Tail],S1,S3) :-
   Tail = [[value(V21,V22,down,V24)],
      [value(V31,V32,down,V34)],
      [value(V41,V42,down,V44)]|_],
   append(S1,[[group([value(V11,V12,down,V14),
      value(V21,V22,down,V24),
      value(V31,V32,down,V34),
      value(V41,V42,down,V44)])]],S2),
   write_debug_list(['G4: ',value(V11,V12,down,V14),
      value(V21,V22,down,V24),
      value(V31,V32,down,V34),
      value(V41,V42,down,V44)]),
   count_grouped_downs(4,Tail,S2,S3).
count_grouped_downs(5,[[value(V11,V12,down,V14)]|Tail],S1,S3) :-
   Tail = [[value(V21,V22,down,V24)],
      [value(V31,V32,down,V34)],
      [value(V41,V42,down,V44)],
      [value(V51,V52,down,V54)]|_],
   append(S1,[[group([value(V11,V12,down,V14),
      value(V21,V22,down,V24),
      value(V31,V32,down,V34),
      value(V41,V42,down,V44),
      value(V51,V52,down,V54)])]],S2),
   write_debug_list(['G5: ',value(V11,V12,up,V14),
      value(V21,V22,down,V24),
      value(V31,V32,down,V34),
      value(V41,V42,down,V44),
      value(V51,V52,down,V54)]),
      count_grouped_downs(5,Tail,S2,S3).
count_grouped_downs(Nr,[_|Tail],S1,S3) :-
   count_grouped_downs(Nr,Tail,S1,S3).
count_grouped_downs(_,[],S,S).


/******************************************************************/

