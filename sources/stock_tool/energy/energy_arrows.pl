

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Arrows                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* energy_simulation_to_xpce_and_chart(Strategies) <-
      */

energy_simulation_to_xpce_and_chart(Strategies) :-
%  stock_chart(share,'Peak 2004'),
   stock_file(data,Directory),
   name_append(Directory,'/xml/Peak_2004.xml',Path),
   stock_chart_file_to_xpce(Path),
   energy_xls_to_prolog(Initial_Rows),
   energy_simulation_to_xpce(Strategies,Initial_Rows,Action_Rows),
   energy_strategies_to_header(Strategies,Header),
   name_append('Actions',Header,Title),
   xpce_display_table(_,_,Title,
      ['Type','Date','Price','Money','Shares','Value'],
      Action_Rows),
%  writeln(user,Action_Rows),
   action_rows_to_arrows(Action_Rows,Arrow_State),
%  dportray(lp,Arrow_State),
   energy_chart_insert_boxes('Peak 2004',Arrow_State),
   !.

   
energy_strategies_to_header(Strategies,Header) :-
   Strategies = strategies(Buy,Sell),
   Buy  = [ (1:8) < B ],
   Sell = [ S < (1:8) ],
   !,
   name_append([' (',B,',',S,')'],Header).
energy_strategies_to_header(_,'').

   
action_rows_to_arrows(Action_Rows,Arrow_State) :-
   action_rows_to_arrows_sub(Action_Rows,States),
   append(States,Arrow_State).

action_rows_to_arrows_sub([X1,X2|Xs],[Stock|Stocks]) :-
   X1 = [_,_,_,Money_1,_,_],
   X2 = [Type,Date,Price_2,Money_2,_,_],
   ( Type = sell,
     Price is Price_2 + ( Money_1 - Money_2 ) / 20
   ; Type = buy,
     Price is Price_2 + ( Money_1 - Money_2 ) / 20 ),
   round(Money_1-Money_2,0,Money),
   Money \= 0,
   Stock = [
      [stock(-,-,T,Price_2,[])],
      [stock(-,-,T,Price,[])] ],
   term_to_atom(YY-MM-DD,Date),
   date_to_time_stamp(DD-MM-YY,T),
   action_rows_to_arrows_sub([X2|Xs],Stocks).
action_rows_to_arrows_sub([_,X2|Xs],Stocks) :-
   action_rows_to_arrows_sub([X2|Xs],Stocks).
action_rows_to_arrows_sub([_],[]).
action_rows_to_arrows_sub([],[]).


/* energy_chart_insert_boxes(Item,Box_State) <-
      */

energy_chart_insert_boxes(Item,Box_State) :-
   dislog_variables_get([
      dsa_chart_picture - Picture,
      dsa_chart_origin - Origin,
      x_factor - Factor,
      y_axis_mode - D_Mode ]),
%  value_dm_to_euro(1,Rate),
%  maplist( stock_clause_multiply_value(Rate),
%     Box_State, State ),
   State = Box_State,
   energy_state_to_graph_boxes(regular,D_Mode,
      Item,State,Time_Values_N),
%  writeln(user, Time_Values_N),
   maplist( time_value_stretch([0.9, 1.1]),
      Time_Values_N, Time_Values_N_Strechted_2 ),
   time_vectors_stretch(
      Time_Values_N_Strechted_2,Time_Values_N_Strechted),
%  writeln(user, Time_Values_N_Strechted),
   maplist( value_to_xpce_vector(Origin,Factor),
      Time_Values_N_Strechted, Vectors ),
%  writeln(user, Vectors),
   send_window_vectors(Picture,Vectors).

energy_state_to_graph_boxes(regular,D_Mode,Company,State,
      Time_Value_Pairs_N ) :-
   maplist( energy_clause_to_time_and_value,
      State, Time_Value_Pairs ),
   stock_print_parameters(Company,
      [_Color,_Diameter,Origin_X,Schrink_X]),
   stock_item_to_mean_value_energy(Company,Mean),
   stock_chart_compute_y_origin_and_schrink(
      Mean,Origin_Y,Schrink_Y),
   maplist( value_normalize_2(linear-D_Mode,
      Origin_X-Origin_Y,Schrink_X-Schrink_Y),
      Time_Value_Pairs, Time_Value_Pairs_N ).

stock_item_to_mean_value_energy(Item,Mean) :-
%  mysql_select_by_name(Item,State),
   stock_values_sorted(Item,State),
   stock_state_to_mean_value(State,Mean).

energy_clause_to_time_and_value([stock(_,_,T,V,_)],T-V).


/* send_window_vectors(Picture,Vectors) <-
      */

send_window_vectors(Picture,[[X1,Y1],[X2,Y2]|XYs]) :-
   X3 is X2 - X1,
   Y3 is Y2 - Y1,
   send(Picture,display,new(Line,line(X3,Y3)),point(X1,Y1)),
   send(Line,arrows,first),
   ( Y3 > 0,
     Colour = red
   ; Colour = green ),
   send(Line,colour(Colour)),
   send_window_vectors(Picture,XYs).
send_window_vectors(_,[]).


/*** tests ********************************************************/


test(energy,energy_chart_insert_boxes) :-
   Box_State = [
      [stock(-, -, 1600, 35, [])],
      [stock(-, -, 1600, 30, [])],
      [stock(-, -, 1400, 34, [])],
      [stock(-, -, 1400, 40, [])] ],
   writeln_list(Box_State),
   energy_chart_insert_boxes('Peak 2004',Box_State).

test(energy,energy_simulation_to_xpce_and_chart) :-
   Buy  = [ (1:8) < 34 ],
   Sell = [ 35 < (1:8) ],
   energy_simulation_to_xpce_and_chart(
      strategies(Buy,Sell) ).


/******************************************************************/


