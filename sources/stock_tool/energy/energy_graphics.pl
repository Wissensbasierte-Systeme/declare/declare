

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  XY - Plot                           ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variables_set([
      energy_graphics_width - 300,
      energy_graphics_height - 300 ]).


/*** interface ****************************************************/


/* energy_tuples_to_boxes(Parameters,Tuples) <-
     */

energy_tuples_to_boxes(Parameters,Tuples) :-
   create_energy_plot(Parameters,Picture),
%  dislog_variable_set(energy_graphics,Picture),
   dislog_variable_get(energy_graphics_width,Width),
   dislog_variable_get(energy_graphics_height,Height),
   Parameters = [_:X_Range,_:Y_Range],
   length(X_Range,N1),
   length(Y_Range,N2),
   B is Width / ( N1 - 2 ),
   H is Height / ( N2 - 2 ),
   energy_tuples_to_boxes(Picture,[Width,Height],[B,H],Tuples).

energy_tuples_to_boxes(Picture,[Width,Height],[B,H],Tuples) :-
   energy_tuples_to_value_ranges(Tuples,[VR,XR,YR]),
   maplist( energy_tuple_normalize([XR,YR],[Width,Height],[B,H]),
      Tuples, Tuples_2 ),
   writeln(user,Tuples_2),
   checklist( energy_tuple_to_box(Picture,[B,H],VR),
      Tuples_2 ).

energy_tuple_normalize([[X1,X2],[Y1,Y2]],[Width,Height],[B,H],
      [V,X0,Y0],[V,X3,Y3]) :-
   X3 is 100 + B / 2 + Width * ( X0 - X1 ) / ( X2 - X1 ),
   Y3 is 100 + H / 2 - Height * ( Y0 - Y1 ) / ( Y2 - Y1 ) + Height.


/* create_energy_plot(Parameters,Picture) <-
      */

create_energy_plot(Parameters,Picture) :-
   new(Dialog,dialog('Buy and Sell')),
   send(Dialog,append,
      new(Picture,picture),below),
   send(Picture,size,size(600,600)),
   send(Picture,background,colour(white)),
   energy_axes_labels(Parameters,Picture),
   send(Dialog,open).

energy_axes_labels(Parameters,Picture) :-
   Parameters = [_:X_Range,_:Y_Range],
   dislog_variable_get(energy_graphics_width,Width),
   dislog_variable_get(energy_graphics_height,Height),
   Parameters = [_:X_Range,_:Y_Range],
   length(X_Range,N1),
   length(Y_Range,N2),
   minimum(X_Range,Min_X), maximum(X_Range,Max_X),
   minimum(Y_Range,Min_Y), maximum(Y_Range,Max_Y),
   Width_2 is Width * ( N1 - 1 ) / ( N1 - 2 ),
   Height_2 is Height * ( N2 - 1 ) / ( N2 - 2 ),
   B is Width_2 / ( N1 - 2 ),
   H is Height_2 / ( N2 - 2 ),
   sublist( integer,
      X_Range, X_Range_2 ),
   sublist( integer,
      Y_Range, Y_Range_2 ),
   maplist( energy_x_range_normalize([Min_X,Max_X],B,Width_2),
      X_Range_2, X_Labels ),
   maplist( energy_y_range_normalize([Min_Y,Max_Y],H,Height_2),
      Y_Range_2, Y_Labels ),
   append(X_Labels,Y_Labels,Labels),
   checklist( energy_send_number_exact(Picture,black),
      Labels ).

energy_x_range_normalize([X1,X2],B,Width,X0,[X0,X3,Y3]) :-
   X3 is 100 + B / 2 + Width * ( X0 - X1 ) / ( X2 - X1 ),
   Y3 is 75.

energy_y_range_normalize([Y1,Y2],H,Height,Y0,[Y0,X3,Y3]) :-
   X3 is 75,
   Y3 is 100 + H / 2 - Height * ( Y0 - Y1 ) / ( Y2 - Y1 ) + Height.


/* energy_tuples_to_value_ranges(Tuples,Ranges) <-
      */

energy_tuples_to_value_ranges(Tuples,Ranges) :-
   maplist( energy_tuples_to_range(Tuples),
      [1,2,3], Ranges ).

energy_tuples_to_range(Tuples,N,[Min,Max]) :-
   findall( X,
      ( member(Tuple,Tuples),
        nth(N,Tuple,X) ),
      Xs ),
   minimum(Xs,Min),
   maximum(Xs,Max).


/* energy_tuple_to_box(Picture,[B,H],V_Range,[V,X,Y]) <-
      */

energy_tuple_to_box(Picture,[B,H],V_Range,[V,X,Y]) :-
   energy_number_to_colour(V,V_Range,Colour),
   energy_send_box(Picture,Colour,[B,H],[X,Y]),
   energy_send_number(Picture,[V,X,Y]).

energy_send_box(Picture,Colour,[B,H],[X,Y]) :-
   X_1 is X - B / 2,
   Y_1 is Y - H / 2,
   send(Picture,display,
      new(Box,box(B,H)),point(X_1,Y_1)),
   send(Box,fill_pattern,Colour),
   send(Box,colour,Colour).

energy_send_number(Picture,[V,X,Y]) :-
   energy_send_number(Picture,white,[V,X,Y]).

energy_send_number(Picture,Colour,[V,X,Y]) :-
   round(V,0,W),
   energy_send_number_exact(Picture,Colour,[W,X,Y]).

energy_send_number_exact(Picture,Colour,[V,X,Y]) :-
   X1 is X - 10,
   Y1 is Y - 10,
   send(Picture,display,
      new(Text,text(V)),point(X1,Y1)),
   send(Text,font,font(screen,roman,10)),
   send(Text,colour,Colour).


/* energy_number_to_colour(N,[Min,Max],Colour) <-
      */

energy_number_to_colour(N,[Min,Max],Colour) :-
   K is ( N - Min ) / ( Max - Min ),
   Blue is 5 + K * 11,
   rgb_to_colour([8,8,Blue],Colour).


/*** tests ********************************************************/


test(energy:energy_tuples_to_boxes,1) :-
   Tuples = [
      [1,32,33], [2,32,34], [3,32,35], [4,32,36],
      [8,33,33], [7,33,34], [6,33,35], [5,33,36],
      [9,34,33], [10,34,34], [11,34,35], [12,34,36],
      [16,35,33], [15,35,34], [14,35,35], [13,35,36] ],
   equidistant_partition([32,36],1,Range),
   energy_tuples_to_boxes([_:Range,_:Range],Tuples).


/******************************************************************/


