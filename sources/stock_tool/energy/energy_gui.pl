

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Graphical User Interface            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* energy_tool <-
      */

et :-
   energy_tool.

energy_tool :-
   dislog_variables_set([
      smooth_parameter - 0,
      chart_start - 2001,
      storage_mode - dislog ]),
   dsa,
   new(Dialog,dialog('Energy - Chart Analysis')),
   send(Dialog,gap,size(0,0)),
   send(Dialog,append,tab_stack(
      new(Tab_1,tab('Simulation')),
      new(Tab_2,tab('Optimization')))),
   new(DG_1,dialog_group('Strategy')),
   send(DG_1,append,new(Buy,text_item('Buy'))),
   send(DG_1,append,new(Sell,text_item('Sell'))),
   send(DG_1,append,new(Percents,text_item('Percents'))),
   send(Tab_1,append,DG_1),
   dislog_variables_set([
      energy_tool_buy - Buy,
      energy_tool_sell - Sell,
      energy_tool_perecents - Percents ]),
   send(Tab_1,append,button('Evaluate',
      message(@prolog,energy_simulation_for_gui))),
   send(Tab_1,append,button('Close',
      message(Dialog, destroy))),
   new(DG_2,dialog_group('Parameters')),
   send(DG_2,append,new(From,text_item('From'))),
   send(DG_2,append,new(To,text_item('To'))),
   send(DG_2,append,new(Step,text_item('Step Width'))),
   dislog_variables_set([
      energy_tool_from - From,
      energy_tool_to - To,
      energy_tool_step - Step ]),
   send(Tab_2,append,DG_2),
   send(Tab_2,append,button('Evaluate',
      message(@prolog,energy_optimization_for_gui))),
   send(Tab_2,append,button('Close',
      message(Dialog, destroy))),
   send(Dialog,open).


/* energy_simulation_for_gui <-
      */

energy_simulation_for_gui :-
   xpce_formular_get_to_term(energy_tool_buy,B),
   xpce_formular_get_to_term(energy_tool_sell,S),
   xpce_formular_get_to_term(energy_tool_perecents,Percents),
   Rate is Percents / 100,
   dislog_variable_set(energy_block_and_depot_to_sell,Rate),
   writeln(user,B-S), ttyflush,
   Buy  = [ (1:8) < B ],
   Sell = [ S < (1:8) ],
   energy_simulation_to_xpce_and_chart(strategies(Buy,Sell)).


/* energy_optimization_for_gui <-
      */

energy_optimization_for_gui :-
   xpce_formular_get_to_term(energy_tool_step,Step),
   xpce_formular_get_to_term(energy_tool_from,From),
   xpce_formular_get_to_term(energy_tool_to,To),
   equidistant_partition([From,To],Step,R),
   Parameters = [X:R,Y:R],
   Conditions = [ X < Y ],
   Buy  = [ (1:8) < X ],
   Sell = [ Y < (1:8) ],
   energy_simulations(
      strategies(Buy,Sell),
      Parameters, Conditions ).


/* xpce_formular_get_to_term(Field,Value) <-
      */

xpce_formular_get_to_term(Field,Value) :-
   xpce_formular_get(Field,X),
   term_to_atom(Value,X).


/******************************************************************/


