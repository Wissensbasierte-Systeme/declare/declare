

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Reasoning                           ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      dsa_variable/2.


dsa_variable(planning, retrospective).
dsa_variable(energy_simulation_to_xpce_table, yes).


/* energy_simulation_to_xpce(Strategies,Initial_Rows) <-
      */

energy_simulation_to_xpce(Strategies) :-
   energy_xls_to_prolog(Initial_Rows),
   energy_simulation_to_xpce(Strategies,Initial_Rows),
   !.

energy_simulation_to_xpce(Strategies,Initial_Rows) :-
   energy_simulation_to_xpce(Strategies,Initial_Rows,Action_Rows),
   xpce_display_table(_,_,'Actions',
      ['Type','Date','Price','Money','Shares','Value'],
      Action_Rows).

energy_simulation_to_xpce(Strategies,Initial_Rows,Action_Rows) :-
   energy_simulation(
      Strategies, Initial_Rows, Actions, Depot_Out ),
   energy_first_and_last_date_and_price(Initial_Rows,
      First_Date:First_Price, Last_Date:Last_Price ),
   depot_to_value(Last_Price,Depot_Out,Value),
   Naive_Rendite is 100 * Last_Price / First_Price,
   multify('',6,Row),
   append([Row,[start,First_Date,First_Price,100,0,100]|Actions],
      [[end,Last_Date,Last_Price,Value,0,Value],Row,
       [naive,'','','','',Naive_Rendite]],
      Action_Rows).

energy_simulation(Strategies,Initial_Rows,Actions,Depot_Out) :-
   measure(
      energy_simulation_measured(Strategies,
         Initial_Rows,Actions,Depot_Out),
      T ),
   Time is T * 1000,
   write_list(user,[Time,' sec -> ']),
   !.

energy_simulation_measured(strategies(Es_1,Es_2),
      Initial_Rows,Actions,Depot_Out) :-
   energy_strategy_initial_rows_to_blocks(
      strategy(Es_1),Initial_Rows,Blocks_1),
%  writeln(user,Blocks_1),
   energy_blocks_annotate(buy,Blocks_1,Blocks_3),
   energy_strategy_initial_rows_to_blocks(
      strategy(Es_2),Initial_Rows,Blocks_2),
%  writeln(user,Blocks_2),
   energy_blocks_annotate(sell,Blocks_2,Blocks_4),
   append(Blocks_3,Blocks_4,Blocks_5),
   energy_blocks_sort(Blocks_5,Blocks),
%  writeln_list(Blocks),
   energy_simulation_iteration(Blocks,[100,0],Depot_Out,Actions),
   !.


/* energy_strategy_initial_rows_to_blocks(
         Strategy, Initial_Rows, Blocks ) <-
      */

energy_strategy_initial_rows_to_blocks(
      Strategy, Initial_Rows, Blocks ) :-
   energy_strategy_to_state(Strategy,Initial_Rows,State),
   energy_strategy_state_to_rows(Strategy,State,Rows),
   energy_rows_split_into_blocks(Rows,Blocks).
   

/* energy_strategy_to_blocks(Strategy,Blocks) <-
      */

energy_strategy_to_blocks(Strategy,Blocks) :-
   energy_strategy_to_state_and_rows(Strategy,_,Rows),
   energy_rows_split_into_blocks(Rows,Blocks),
   !.


/* energy_rows_split_into_blocks(Rows,Blocks) <-
      */

energy_rows_split_into_blocks(Rows,Blocks) :-
   energy_rows_split_into_blocks(Rows,[],Blocks).

energy_rows_split_into_blocks([R|Rs],B,[Block|Blocks]) :-
   R = [''|_],
   !,
   reverse(B,Block),
   energy_rows_split_into_blocks(Rs,[],Blocks).
energy_rows_split_into_blocks([R|Rs],B,Blocks) :-
   energy_rows_split_into_blocks(Rs,[R|B],Blocks).
energy_rows_split_into_blocks([],_,[]).


/* energy_blocks_annotate(A,Blocks_1,Blocks_2) <-
      */

energy_blocks_annotate(A,Blocks_1,Blocks_2) :-
   maplist( energy_block_annotate(A),
      Blocks_1, Blocks_2 ).
   
energy_block_annotate(A,Block,Block-A).


/* energy_block_to_price(Block,Price) <-
      */

energy_block_to_price(Block,Price) :-
   ( dsa_variable_get(planning,introspective),
     first(Block,Row)
   ; dsa_variable_get(planning,retrospective),
     last_element(Block,Row) ),
   last_element(Row, P),
   term_to_atom(Price, P).


/* energy_block_to_date(Block,Date) <-
     */

energy_block_to_date(Block,Date) :-
   ( dsa_variable_get(planning,introspective),
     first(Block,Row)
   ; dsa_variable_get(planning,retrospective),
     last_element(Block,Row) ),
   first(Row,Date).


/* energy_block_and_depot_to_buy(Depot,Block,Rate) <-
      */

energy_block_and_depot_to_buy([Money,_],_,1) :-
   dislog_variable_get(energy_block_and_depot_to_sell,Rate),
   X is Money * ( 1 - Rate ),
   X < 5,
   !.
energy_block_and_depot_to_buy(_,_,Rate) :-
   dislog_variable_get(energy_block_and_depot_to_sell,Rate).

% energy_block_and_depot_to_buy(_,_,1).
% energy_block_and_depot_to_buy(_,_,0.5).


/* energy_block_and_depot_to_sell(Depot,Block,Rate) <-
      */

energy_block_and_depot_to_sell([_,Shares],_,1) :-
   dislog_variable_get(energy_block_and_depot_to_sell,Rate),
   X is Shares * ( 1 - Rate ),
   X < 0.25,
   !.
energy_block_and_depot_to_sell(_,_,Rate) :-
   dislog_variable_get(energy_block_and_depot_to_sell,Rate).
   
% energy_block_and_depot_to_sell(_,_,1).
% energy_block_and_depot_to_sell(_,_,0.5).


:- dislog_variable_set(energy_block_and_depot_to_sell,1).


/* energy_first_and_last_date_and_price(Initial_Rows,X1,X2) <-
      */

energy_first_and_last_date_and_price(
      Initial_Rows, Date_1:Price_1, Date_2:Price_2) :-
   first(Initial_Rows, Row_1),
   first(Row_1, Date_1),
   last_element(Row_1, Price_A),
   name_to_number(Price_A, Price_1),
   last_element(Initial_Rows, Row_2),
   first(Row_2, Date_2),
   last_element(Row_2, Price_B),
   name_to_number(Price_B, Price_2).
   

/* energy_simulation_iteration(Blocks,Depot_1,Depot_2,Actions) <-
      */

energy_simulation_iteration(
      [B-buy|Bs],[Money_1,Shares_1],Depot_3,
      [[buy,Date,Price,Money_2,Shares_2,Value]|Actions]) :-
   energy_block_to_date(B,Date),
   energy_block_to_price(B,Price),
   energy_block_and_depot_to_buy([Money_1,Shares_1],B,Rate),
   Shares_2 is Shares_1 + ( Money_1 * Rate ) / Price,
   Money_2 is Money_1 * ( 1 - Rate ),
   depot_to_value(Price,[Money_1,Shares_1],Value),
%  write_list(user,['buying at price ',Price,'; ',
%     'new depot is ',[Money_2,Shares_2],'; ',
%     'value is ',Value]), nl(user),
   energy_simulation_iteration(
      Bs,[Money_2,Shares_2],Depot_3,Actions).
energy_simulation_iteration(
      [B-sell|Bs],[Money_1,Shares_1],Depot_3,
      [[sell,Date,Price,Money_2,Shares_2,Value]|Actions]) :-
   energy_block_to_date(B,Date),
   energy_block_to_price(B,Price),
   energy_block_and_depot_to_sell([Money_1,Shares_1],B,Rate),
   Shares_2 is Shares_1 * ( 1 - Rate ),
   Money_2 is Money_1 + Shares_1 * Price * Rate,
   depot_to_value(Price,[Money_1,Shares_1],Value),
%  write_list(user,['selling at price ',Price,'; ',
%     'new depot is ',[Money_2,Shares_2],'; ',
%     'value is ',Value]), nl(user),
   energy_simulation_iteration(
      Bs,[Money_2,Shares_2],Depot_3,Actions).
energy_simulation_iteration([],Depot,Depot,[]).

   
/* depot_to_value(Price,[Money,Shares],Value) <-
      */

depot_to_value(Price,[Money,Shares],Value) :-
   name_to_number(Price, Price_2),
   Value is Money + Shares * Price_2.


/* energy_simulations(Strategies,Parameters) <-
      */

energy_simulations(Strategies,Parameters) :-
   energy_simulations(Strategies,Parameters,[]).

energy_simulations(Strategies, Parameters, Conditions) :-
   energy_xls_to_prolog(Initial_Rows),
   energy_first_and_last_date_and_price(
      Initial_Rows, _:_, _:Last_Price ),
   maplist( value_range_pair_to_value,
      Parameters, Variables ),
   measure( timer_2,
      findall( Value-Variables,
         ( checklist( paramter_to_value_from_range,
              Parameters ),
           checklist( call,
              Conditions ),
           energy_simulation(Strategies, Initial_Rows, _, Depot_Out),
           depot_to_value(Last_Price, Depot_Out, Value),
           writeln(user, Value-Variables) ),
         List ),
      T ),
   list_to_ord_set(List, Results_2),
   reverse(Results_2, Results),
   first(Results, Best_Solution),
   writeln(user, Best_Solution),
   Time is T * 1000,
   write_list(user, ['total time: ', Time, ' sec']), nl(user),
   energy_tables_to_xpce(Parameters, Results),
   !.

energy_tables_to_xpce(_, _) :-
   dsa_variable_get(energy_simulation_to_xpce_table, no),
   !.
energy_tables_to_xpce(Parameters, Results) :-
   maplist( value_and_variables_to_tuple,
      Results, Tuples ),
   xpce_display_table(_,_,'Simulation Results',
      ['Value'], Tuples ),
   energy_tables_to_xpce_sub(Parameters, Tuples).

energy_tables_to_xpce_sub(Parameters, Tuples) :-
   dsa_variable_get(energy_simulation_to_xpce_table, yes),
   !,
   energy_simulation_evaluation(Tuples, Tuples_2),
   xpce_display_table(_, _, 'Simulation Results',
      ['Value 1', 'Value 2'], Tuples_2 ),
   Table = [['Value 1', 'Value 2']|Tuples_2],
   File = 'projects/Energy/simulation.txt',
   write_star_office_file(Table, File),
   energy_tuples_to_boxes(Parameters, Tuples).
energy_tables_to_xpce_sub(_, _).

paramter_to_value_from_range(X:Range) :-
   member(X, Range).

value_range_pair_to_value(V:_, V).

value_and_variables_to_tuple(V-Vs, [V|Vs]).


/* energy_blocks_sort(Blocks_1,Blocks_4) <-
      */

energy_blocks_sort(Blocks_1,Blocks_4) :-
   maplist( energy_block_prefix_by_date,
      Blocks_1, Blocks_2 ),
%  bar_line,
%  writeln(user,Blocks_2),
   list_to_ord_set(Blocks_2,Blocks_3),
%  bar_line,
%  writeln(user,Blocks_3),
   maplist( energy_block_eliminate_date_prefix,
      Blocks_3, Blocks_4 ),
   !.
   
energy_block_prefix_by_date(Block-A,(Date_2)-Block-A) :-
   first(Block,Row),
   first(Row,Date_1),
   term_to_atom(Date_2,Date_1).
   
energy_block_eliminate_date_prefix(_-Block-A,Block-A).


/* energy_simulation_evaluation(Tuples_1,Tuples_2) <-
      */

energy_simulation_evaluation(Tuples_1,Tuples_2) :-
   maplist( energy_simulation_tuple_evaluation,
      Tuples_1, Tuples_3 ),
   list_to_ord_set(Tuples_3,Tuples_2).

energy_simulation_tuple_evaluation([V2,A,B],[V1,V2,A,B]) :-
   V1 is ( V2 - 100 ) * ( B - A ) / 10.


/******************************************************************/


