

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Main                                ***/
/***                                                            ***/
/******************************************************************/


   
/* energy(Es) <-
      */

energy(Es) :-
   energy_strategy_to_state_and_rows(
      strategy(Es), State, Rows ),
   term_to_atom(Es,X),
   stock_state_to_display(X,State),
   generate_energy_attributes(As),
   multify('',9,Row),
   xpce_display_table(_,_,'Dates',As,[Row|Rows]),
   !.


/* energy_strategy_to_state_and_rows(
         strategy(Es), State, Rows ) <-
      */

energy_strategy_to_state_and_rows(
      strategy(Es), State, Rows ) :-
   energy_strategy_to_state(strategy(Es),State),
   energy_strategy_state_to_rows(
      strategy(Es), State, Rows ).

energy_strategy_state_to_rows(strategy(Es),State,Rows) :-
   energy_comparators_to_ns(Es,Ns),
   findall( Row,
      ( append(_,[[stock(_,_,D-M-Y1,V,Xs)]|State_2],State),
        V > 1000,
        energy_state_row(
           [[stock(_,_,D-M-Y1,V,Xs)]|State_2],
           Ns, Row ) ),
      Rows ).

energy_state_row(State,Ns,[Date|Xs]) :-
   member(N,Ns),
   nth(N,State,[stock(_,_,D-M-Y1,_,Xs)]),
   Y2 is Y1 + 1900,
   term_to_atom(Y2-M-D,Date).
energy_state_row(_,_,Row) :-
   multify('',9,Row),
   !.


/* energy_strategy_to_state(Strategy,State) <-
      */

energy_strategy_to_state(Strategy,State) :-
   energy_xls_to_prolog(Rows),
   energy_strategy_to_state(Strategy,Rows,State).

energy_strategy_to_state(Strategy,Rows,State) :-
   term_to_atom(Strategy,Atom_1),
   name_append('energy_',Atom_1,Atom_2),
   term_to_atom(Predicate,Atom_2),
   energy_strategy_to_state_loop(Predicate,Rows,State).

energy_strategy_to_state_loop(Predicate,Rows,[U|Us]) :-
   apply(Predicate,[Rows,U]),
   Rows = [_|Rows_2],
   energy_strategy_to_state_loop(Predicate,Rows_2,Us).
energy_strategy_to_state_loop(_,_,[]).


/* energy_strategy(Mode,Row,Stock_Atom) <-
      */

energy_strategy(1, [X,Y,Z|_], U) :-
   Y = [Sql_Date|Ys],
%  writeln(user, Sql_Date),
   sql_date_to_stock_date(Sql_Date, Stock_Date),
%  writeln(user, Stock_Date),
   nth(9,X,X8), X8 \= '', name_to_number(X8,X8n),
   nth(9,Y,Y8), Y8 \= '', name_to_number(Y8,Y8n),
   nth(9,Z,Z8), Z8 \= '', name_to_number(Z8,Z8n),
   X8n < Y8n,
   Y8n < Z8n,
   !,
   euro_to_dm(1200, V),
   U = [stock('Strategy 1',1,Stock_Date,V,Ys)].
energy_strategy(1,[_,Y,_|_],U) :-
   Y = [Sql_Date|Ys],
   sql_date_to_stock_date(Sql_Date,Stock_Date),
   euro_to_dm(800,V),
   U = [stock('Strategy 1',1,Stock_Date,V,Ys)].

energy_strategy(Es,Rows,U) :-
   checklist( energy_strategy_compare(Rows),
      Es ),
   Rows = [[Sql_Date|Xs]|_],
   sql_date_to_stock_date(Sql_Date,Stock_Date),
   U = [stock('Strategy 1',1,Stock_Date,1500,Xs)].
energy_strategy(_,Rows,U) :-
   Rows = [[Sql_Date|Xs]|_],
   sql_date_to_stock_date(Sql_Date,Stock_Date),
   U = [stock('Strategy 1',1,Stock_Date,500,Xs)].


/* energy_strategy_compare(Rows,Term) <-
      */

energy_strategy_compare(Rows,(X<Y)) :-
   energy_tool_eval(Rows,X,X1),
   energy_tool_eval(Rows,Y,Y1),
   X1 < Y1.

energy_tool_eval(Rows,X-Y,C) :-
   !,
   energy_tool_eval(Rows,X,X1),
   energy_tool_eval(Rows,Y,Y1),
   C is X1 - Y1.
energy_tool_eval(Rows,X+Y,C) :-
   !,
   energy_tool_eval(Rows,X,X1),
   energy_tool_eval(Rows,Y,Y1),
   C is X1 + Y1.
energy_tool_eval(Rows,X*Y,C) :-
   !,
   energy_tool_eval(Rows,X,X1),
   energy_tool_eval(Rows,Y,Y1),
   C is X1 * Y1.
energy_tool_eval(Rows,X/Y,C) :-
   !,
   energy_tool_eval(Rows,X,X1),
   energy_tool_eval(Rows,Y,Y1),
   Y1 \= 0,
   C is X1 / Y1.
energy_tool_eval(Rows,A:B,C) :-
   !,
   nth(A,Rows,Ra),
   B1 is B + 1,
   nth(B1,Ra,X),
   X \= '', name_to_number(X,C).
energy_tool_eval(_,C,C).


/* energy_xls_to_prolog(Rows) <-
      */

energy_xls_to_prolog(Rows) :-
   File = 'projects/Energy/2002_08_02_data_sample.txt',
   write_list(user,['<--- ',File,' ']),
   measure(
      ( read_star_office_file(File,[_|Rs]),
        maplist( row_change_date_to_sql,
           Rs, Rows ) ), T ),
   Time is T * 1000,
   write_list(user,[Time,' sec']), nl(user),
   !.

row_change_date_to_sql([E_Date|Xs],[Sql_Date|Xs]) :-
   energy_date_to_sql_date(E_Date,Sql_Date).
%  maplist( term_to_atom, Ys, Xs ).


/* energy_xls_to_xpce <-
      */

energy_xls_to_xpce :-
   energy_xls_to_prolog(Rows),
   generate_energy_attributes(As),
   xpce_display_table( _, _, 'Energy', As, Rows ).


/* energy_xls_to_sql <-
      */

energy_xls_to_sql :-
   energy_xls_to_prolog(Rows),
   predicate_to_file( xxx,
      energy_rows_to_sql(464896,Rows) ).


/* energy_rows_to_sql(I,Rows) <-
      */

energy_rows_to_sql(I,[R|Rs]) :-
   energy_row_to_sql(I,R),
   length(R,N),
   J is I + N - 1,
   energy_rows_to_sql(J,Rs).
energy_rows_to_sql(_,[]).

energy_row_to_sql(I,[Date,X1,X2,X3,X4,Y1,Y2,Y3,Y4]) :-
   energy_row_to_sql(I,Date,2012001,[X1,X2,X3,X4]),
   J is I + 4,
   energy_row_to_sql(J,Date,2022001,[Y1,Y2,Y3,Y4]).
   
energy_row_to_sql(I,Date,Wkn,[''|Xs]) :-
   !,
   J is I + 1,
   Wkn_2 is Wkn + 1,
   energy_row_to_sql(J,Date,Wkn_2,Xs).
energy_row_to_sql(I,Date,Wkn,[X|Xs]) :-
   writeln('INSERT INTO finanztreff VALUES'),
   write('   ( '),
   write_list([
      I,', ',Wkn,', ''',Date,''', ',X,', ',
      'NULL, NULL, NULL, NULL );' ]), nl,
   J is I + 1,
   Wkn_2 is Wkn + 1,
   energy_row_to_sql(J,Date,Wkn_2,Xs).
energy_row_to_sql(_,_,_,[]).


/* energy_date_to_sql_date(E_Date,Sql_Date) <-
      */

energy_date_to_sql_date(E_Date,Sql_Date) :-
   name_split_at_position(["/"],E_Date,[Month,Day,Year]),
%  name_append([Year,'-',Month,'-',Day],Sql_Date).
   term_to_atom(Year-Month-Day,Sql_Date).


/* sql_date_to_stock_date(Sql_Date, Stock_Date) <-
      */

sql_date_to_stock_date(Sql_Date, Stock_Date) :-
   name_exchange_sublist([["\'", ""]], Sql_Date, Sql_Date_2),
   term_to_atom(Year_1-Month-Day, Sql_Date_2),
   Year_2 is Year_1 - 1900,
   Stock_Date = Day-Month-Year_2.


/* name_to_number(Name,Number) <-
      */

name_to_number(Name,Number) :-
   name(Name,List),
   name(Number,List).


/* energy_comparators_to_ns(Es,Ns) <-
      */

energy_comparators_to_ns(T,Ns) :-
   ( T =.. [F|Xs],
     member(F,[<,+,-,*,/])
   ; is_list(T),
     Xs = T ),
   !,
   maplist( energy_comparators_to_ns,
      Xs, Ms ),
   append(Ms,Ks),
   list_to_ord_set(Ks,Ns).
energy_comparators_to_ns(N:_,[N]) :-
   !.
energy_comparators_to_ns(_,[]).


/* generate_energy_attributes(As) <-
      */

generate_energy_attributes(As) :-
   generate_interval(1,4,Is),
   maplist( name_append('Base '),
      Is, Bs ),
   maplist( name_append('Peak '),
      Is, Ps ),
   append([['Date'],Bs,Ps],As).


/*** tests ********************************************************/


test(energy:energy_row_to_sql,1) :-
   energy_row_to_sql(1,'2002-5-8',2012001,[1,2,3,4]).

/*
test(energy:energy_strategy,1) :-
   maplist( energy_strategy(1), [
      [2001-7-25, 22.825, 23.225, 23.725, 24.4,
                  32.6, 32.7, 33.5, 34.45],
      [2001-7-26, 23.125, 23.35, 23.825, 24.5,
                  32.85, 32.95, 34, 34.7],
      [2001-7-27, 23.35, 23.375, 23.85, 24.4,
                  33.125, 33, 34.1, 34.7] ], X ),
   writeln(user, X).
*/


/******************************************************************/


