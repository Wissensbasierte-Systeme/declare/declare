

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Test Examples                       ***/
/***                                                            ***/
/******************************************************************/


test(energy,0) :-
   dsa.

test(energy,1) :-
   Es = [ (1:8) < (2:8), (2:8) < (3:8),
          (3:8) < (4:8), (5:8) < (4:8) ],
   energy(Es).

test(energy,2) :-
   Es = [ 1.1 * (1:8) < (4:8) ],
   energy(Es).
   
test(energy,3) :-
   Es = [ (1:8) - 1.3 * (1:4) < 0 ],
   energy(Es).
   
test(energy,4) :-
   Es = [ (1:8) / (2:7) - (3:8) / (4:7) < 0 ],
   energy(Es).

test(energy,energy_strategy_to_blocks) :-
   Es = [ (1:8) < (2:8), (2:8) < (3:8),
          (3:8) < (4:8), (5:8) < (4:8) ],
   energy_strategy_to_blocks(strategy(Es),Blocks),
   writeln_list(Blocks).
   
test(energy,energy_simulation(1)) :-
   Buy  = [ (1:8) < (2:8), (2:8) < (3:8),
            (3:8) < (4:8), (5:8) < (4:8) ],
   Sell = [ (1:8) < (2:8), (2:8) < (3:8),
            (4:8) < (3:8), (4:8) < (5:8) ],
   energy_simulation_to_xpce(strategies(Buy,Sell)).

test(energy,energy_simulation(2)) :-
%  R = [0.9, 1, 1.1],
   R = [0.9, 1],
%  R = [1],
   Parameters = [X1:R,X2:R,X3:R,X4:R,Y1:R,Y2:R,Y3:R,Y4:R],
%  Parameters = [0.9:_, 1:_, 0.9:_, 1:_, 1:_, 0.9:_, 0.9:_, 1:_],
   Buy  = [ X1 * (1:8) < (2:8), X2 * (2:8) < (3:8),
            X3 * (3:8) < (4:8), X4 * (5:8) < (4:8) ],
   Sell = [ Y1 * (1:8) < (2:8), Y2 * (2:8) < (3:8),
            Y3 * (4:8) < (3:8), Y4 * (4:8) < (5:8) ],
   dsa_variable_get(energy_simulation_to_xpce_table, V),
   dsa_variable_set(energy_simulation_to_xpce_table, limited),
   energy_simulations(
      strategies(Buy,Sell),
      Parameters ),
   dsa_variable_set(energy_simulation_to_xpce_table, V).

test(energy,energy_simulation(3)) :-
   Buy  = [ (1:8) < (2:8) ],
   Sell = [ (2:8) < (1:8) ],
   energy_simulation_to_xpce(
      strategies(Buy,Sell) ).

test(energy,energy_simulation(4)) :-
   equidistant_partition([33,36],0.25,R),
   Parameters = [X:R,Y:R],
   Conditions = [ X < Y ],
   Buy  = [ (1:8) < X ],
   Sell = [ Y < (1:8) ],
   energy_simulations(
      strategies(Buy,Sell),
      Parameters, Conditions ).

test(energy,energy_simulation(5)) :-
   Buy  = [ (1:8) < 33 ],
   Sell = [ 35.5 < (1:8) ],
   energy_simulation_to_xpce_and_chart(strategies(Buy,Sell)).
%  energy_simulation_to_xpce(strategies(Buy,Sell)).


/******************************************************************/


