

/******************************************************************/
/***                                                            ***/
/***          Energy Tool:  Excel to Stock State                ***/
/***                                                            ***/
/******************************************************************/



/* energy_xls_to_stock_state(State) <-
      */

energy_xls_to_stock_state :-
   energy_xls_to_stock_state(State),
   stock_file(stock_data,energy_data,File),
   disave(State,File).

energy_xls_to_stock_state(State) :-
   energy_xls_to_prolog(Rows),
   maplist( energy_row_to_stock_state,
      Rows, States ),
   append(States,State_2),
   sublist( stock_atom_known_value,
      State_2, State ).
   
stock_atom_known_value([stock(_,_,Value,_)]) :-
   Value \= ''.

energy_row_to_stock_state([Date|Xs],State) :-
   term_to_atom(Date_2,Date),
   mysql_date_to_dsa_date(Date_2,Dsa_Date),
   first_n_elements(4,Xs,Base),
   append(Base,Peak,Xs),
   generate_interval(1,4,I),
   pair_lists(Base,I,Base_Pairs),
   pair_lists(Peak,I,Peak_Pairs),
   maplist( energy_pair_to_stock_atom(Dsa_Date,2012000),
      Base_Pairs, Base_State ),
   maplist( energy_pair_to_stock_atom(Dsa_Date,2022000),
      Peak_Pairs, Peak_State ),
   append(Base_State,Peak_State,State).
   
energy_pair_to_stock_atom(
      Date,Basis,[X,N],[stock(Wkn,Date,X,[])]) :-
   Wkn is Basis + N.


/******************************************************************/


