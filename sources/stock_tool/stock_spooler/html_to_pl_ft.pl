

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Spooler for Finanztreff              ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      stock_term_to_tuples/3.


/*** interface ****************************************************/


/* stock_term_to_tuples(Date, Table, State) <-
      */

stock_term_to_tuples(Date, Table, State) :-
   dislog_variable_get(spool_source_version, 'Finanztreff 2003'),
   !,
   findall(
      stock(Name, Wkn, Date, Value, Percent, '--', '--', '--'),
      ( Row := Table^tr,
        A := Row-nth(3)^td,
        [Name_2] := A^a^content::'*',
        name_exchange_elements(["'`"], Name_2, Name),
        A2 := Row-nth(2)^td,
        [Wkn_2] := A2^content::'*',
        wkn_to_save_wkn(Wkn_2, Wkn),
        A3 := Row-nth(5)^td,
        [Value_2] := A3^a^content::'*',
        name_remove_elements(".", Value_2, Value_3),
        name_exchange_elements([",."], Value_3, Value_4),
        name_cut_at_position([[160]], Value_4, Value),
        [Percent_2] := Row-nth(7)^td^content::'*',
        name_cut_at_position(["%"], Percent_2, Percent_3),
        name_exchange_elements([",.", "�+"], Percent_3, Percent) ),
      State_2 ),
   sort(State_2, State).

stock_term_to_tuples(Date, Table, State) :-
   dislog_variable_get(spool_source_version, 'Finanztreff 2003'),
   !,
   findall(
      stock(Name, Wkn, Date, Value, Percent, '--', '--', '--'),
      ( Row := Table^tr,
        A := Row-nth(1)^td^a,
        [Name] := A^content::'*',
        A2 := Row-nth(2)^td^a,
        Wkn_2 := A2@href,
        name_start_after_position(["&s="], Wkn_2, Wkn_3),
        name_cut_at_position(["&b="], Wkn_3, Wkn_4),
        wkn_to_save_wkn(Wkn_4, Wkn),
        [Value_2|_] := Row-nth(4)^td^content::'*',
        name_remove_elements(".", Value_2, Value_3),
        name_exchange_elements([",."], Value_3, Value_4),
        name_cut_at_position([[160]], Value_4, Value),
        [Percent_2] := Row-nth(2)^td::[@id=_]^content::'*',
        name_cut_at_position(["%"], Percent_2, Percent_3),
        name_exchange_elements([",.", "�+"], Percent_3, Percent) ),
      State_2 ),
   sort(State_2, State).

stock_term_to_tuples(Date, Table, State) :-
   dislog_variable_get(spool_source_version, 'Finanztreff 2003'),
   findall(
      stock(Name, Wkn, Date, Value, Percent, '--', '--', '--'),
      ( [_, _, X, Y, _, Z| _] := Table^tr,
        [Name] := [X]^td^a,
        Wkn_2 := [X]^td@a^href,
        name_start_after_position(["&s="], Wkn_2, Wkn_3),
        name_cut_at_position(["&b="], Wkn_3, Wkn_4),
        wkn_to_save_wkn(Wkn_4, Wkn),
        [Value_2| _] := [Y]^td,
        name_remove_elements(".", Value_2, Value_3),
        name_exchange_elements([",."], Value_3, Value_4),
        name_cut_at_position([[160]], Value_4, Value),
        [Percent_2] := [Z]^td,
        name_cut_at_position(["%"], Percent_2, Percent_3),
        name_exchange_elements([",.", "�+"], Percent_3, Percent) ),
      State ).

stock_term_to_tuples(Date, Table, State) :-
   dislog_variable_get(
      spool_source_version, 'Finanztreff 2003 - A'),
   findall(
      stock(Name, Wkn, Date, Value, Percent, '--', '--', '--'),
      ( [_, X, _, Y, Z| _] := Table^tr,
        [Name] := [X]^td^a,
        [Wkn_2] := [X]^td^br,
        name_cut_at_position([" /"], Wkn_2, Wkn),
        [Value_2| _] := [Y]^td,
        name_exchange_elements([",."], Value_2, Value),
        [Percent_2] := [Z]^td^br,
        name_exchange_elements([",.", "�+"], Percent_2, Percent) ),
      State ).

stock_term_to_tuples(Date, Term, Tuples) :-
   dislog_variable_get(spool_source, 'Finanztreff'),
   stock_term_to_list(Term, List),
   maplist( stock_element_to_tuple(Date),
      List, Tuples ).

wkn_to_save_wkn(Wkn, 0) :-
   name(Wkn, [X|_]),
   X >= 65,
   !.
wkn_to_save_wkn(Wkn, Wkn).

stock_term_to_list(Term, List) :-
   Term = [ element( table, _,
      [ element( tbody, [], [_|List] ) ] ) ].

stock_element_to_tuple(Date,Element,
      stock(Name,Wkn,Date,Value,'--','--','--','--') ) :-
   Element = element( tr, [], [
      element( td, _,
         [ Name,
           element(br,_,_),
           Wkn_2 ] ),
%     element( td, _,
%        [ element(a,_,[Name]),
%          element(br,[],[]),
%          Wkn_2 ] ),
      element( td, _,
         [ Value_2|_ ] ) | _ ] ),
   name(Wkn_2,Wkn_List_2),
   list_start_after_position([" / "],Wkn_List_2,Wkn_List),
   name(Wkn,Wkn_List),
   name_exchange_elements([",."],Value_2,Value_3),
   possibly_stock_multiply_2(Wkn,Value_3,Value).


/* shorten_html_string(String_1,String_2) <-
      */

shorten_html_string(String_1,String_2) :-
   list_start_after_position(["</style>"],
      String_1, String_3),
   list_start_after_position(["</style>"],
      String_3, [10|String_4] ),
   list_cut_at_position(["</TABLE>"],
      String_4, String_5 ),
   append(String_5,"</TABLE>",String_6),
   !,
   list_remove_tags(["<A HREF","<FONT","</FONT","</a"],
      String_6, String_7 ),
   list_exchange_sublist([["#c6c6c6","'#c6c6c6'"]],
      String_7, String_2 ).


/*** implementation ***********************************************/


/* name_remove_a_hrefs(Name_1,Name_2) <-
      */

name_remove_a_hrefs(Name_1,Name_2) :-
   name(Name_1,List_1),
   list_remove_tag("<A HREF",List_1,List_2),
   name(Name_2,List_2).


/* list_remove_tags(Tags,List_1,List_2) <-
      */

list_remove_tags([T|Ts],List_1,List_2) :-
   list_remove_tag(T,List_1,List_3),
   list_remove_tags(Ts,List_3,List_2).
list_remove_tags([],List,List).
   
list_remove_tag(Begin,List_1,List_2) :-
   list_remove_parts([Begin,">"],List_1,List_2),
   !.

list_remove_parts([Begin,End],List_1,List_2) :-
   append(Begin,List,List_1),
   list_start_after_position([End],List,List_3),
   list_remove_parts([Begin,End],List_3,List_2).
list_remove_parts([Begin,End],[X|X1s],[X|Xs2]) :-
   list_remove_parts([Begin,End],X1s,Xs2).
list_remove_parts(_,[],[]).


/******************************************************************/


