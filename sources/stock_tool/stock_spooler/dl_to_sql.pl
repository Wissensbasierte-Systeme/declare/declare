

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Spooler for dl -> sql                ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      state_to_sql_insert_statements/2.


/*** interface ****************************************************/


dsa_variable(maximal_finanztreff,153082).


/* dsa_prolog_to_sql_insert_file(Dsa_File) <-
      an SQL file containing insert statements is created
      from the DSA file Dsa_File:
       - dsa_prolog_to_sql_insert_file(insert,finanztreff),
       - dsa_prolog_to_sql_insert_file(replace,number_name_ft),
       - dsa_prolog_to_sql_insert_file(stock_group).      */

dsa_prolog_to_sql_insert_file(stock_group) :-
   !,
   stock_file(stock_data,'stock_group.sql',P_sql),
   write('---> '), writeln(P_sql),
   tell(P_sql),
   writeln('use stock;'), nl,
   writeln('delete from stock_group;'), nl,
   stock_groups_to_insert_statements(_),
   told.


/* dsa_prolog_to_sql_insert_file(Mode,F_dsa) <-
      */

dsa_prolog_to_sql_insert_file(Mode,F_dsa) :-
   stock_file(stock_data,F_dsa,P_dsa),
   diconsult(P_dsa,State),
   name_append(P_dsa,'.sql',P_sql),
   write('---> '), writeln(P_sql),
   dsa_prolog_predicate_name_to_sql_table_name(
      F_dsa, T_sql ),
   tell(P_sql),
   writeln('use stock;'), nl,
   dsa_prolog_to_sql_insert_file_delete_from(Mode,T_sql),
   state_to_sql_insert_statements(T_sql,State),
   told.
   
dsa_prolog_to_sql_insert_file_delete_from(replace,T_sql) :-
   !,
   write('delete from '), write(T_sql), writeln(';'), nl.
dsa_prolog_to_sql_insert_file_delete_from(insert,_).


dsa_prolog_predicate_name_to_sql_table_name(
   number_name_ft, number_name ).
dsa_prolog_predicate_name_to_sql_table_name(
   finanztreff, finanztreff ).


/*** implementation ***********************************************/


/* stock_groups_to_insert_statements(Pairs) <-
      */

stock_groups_to_insert_statements(Pairs) :-
   findall( [Company,Branch],
      ( stock_branch(Branch,Companies),
        member(Company,Companies),
        write_stock_group_insert_statement(Company,Branch) ),
      Pairs ).

write_stock_group_insert_statement(Company,Branch) :-
   writeln('INSERT INTO stock_group VALUES ('),
   write('   '''), write(Company),
   write(''', '''), write(Branch), writeln(''' );').


/* state_to_sql_insert_statements(finanztreff,Stock_State) <-
      */

state_to_sql_insert_statements(finanztreff,Stock_State) :-
   sublist( stock_fact_has_proper_wkn,
      Stock_State, Stock_State_2 ),
   dsa_variable(maximal_finanztreff,N),
   M is N + 1,
   stock_state_to_insert_statements(M,Stock_State_2).

stock_fact_has_proper_wkn([stock(Wkn,_,_,_)]) :-
   nonvar(Wkn),
   Wkn \= '--'.

stock_state_to_insert_statements(N,[C|Cs]) :-
   stock_fact_to_insert_statement(N,C),
   !,
   M is N + 1,
   stock_state_to_insert_statements(M,Cs).
stock_state_to_insert_statements(N,[_|Cs]) :-
   !,
   stock_state_to_insert_statements(N,Cs).
stock_state_to_insert_statements(_,[]).

stock_fact_to_insert_statement(N,[A]) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   A =.. [stock,Wkn,Date,Value,Changes],
   write_list(
      ['INSERT INTO ', DSA_Stock_Database, ' VALUES (' ] ),
   write('   '), write(N), write(', '),
   write(Wkn), write(', '),
   dsa_write_date(Date),
   dsa_normalize_change_list(Changes,Changes_2),
   dsa_output_list( dsa_write_value, [Value|Changes_2] ),
   writeln(' );').

dsa_normalize_change_list([],['--','--','--','--']) :-
   !.
dsa_normalize_change_list(Changes,Changes).


/* dsa_output_list(Print_Predicate,[V1,V2|Vs]) <-
      */

dsa_output_list(Print_Predicate,[V1,V2|Vs]) :-
   !,
   Goal =.. [Print_Predicate,V1],
   call(Goal),
   write(', '),
   dsa_output_list(Print_Predicate,[V2|Vs]).
dsa_output_list(Print_Predicate,[V]) :-
   !,
   Goal =.. [Print_Predicate,V],
   call(Goal).
dsa_output_list(_,[]).


/* dsa_write_value(Value) <-
      */

dsa_write_value('--') :-
   !,
   write('NULL').
dsa_write_value(Value) :-
   write(Value).


/* dsa_write_date(DD-MM-YY) <-
      */

dsa_write_date(DD-MM-YY) :-
   write(''''),
   dsa_write_year(YY), write('-'),
   write(MM), write('-'),
   write(DD), write(''''),
   write(', ').

dsa_write_year(YY) :-
   YY < 50, !, YY_2 is YY + 2000, write(YY_2).
dsa_write_year(YY) :-
   YY < 200, !, YY_2 is YY + 1900, write(YY_2).
dsa_write_year(YY) :-
   write(YY).


/* state_to_sql_insert_statements(number_name,Number_Name) <-
      */

state_to_sql_insert_statements(number_name,Number_Name) :-
   checklist( number_name_to_insert_statement,
      Number_Name ).

number_name_to_insert_statement([number_name(Wkn,Name)]) :-
   writeln('INSERT INTO number_name VALUES ('),
   write('   '), write(Wkn), write(', '''),
%  Name_2 = Name,
   name(Name_2,Name), write(Name_2),
   writeln(''' );').


/******************************************************************/


