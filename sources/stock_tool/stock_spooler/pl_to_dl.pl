

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Spooler for pl -> dl                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* stock_spool(Type,Mode,Dates) <-
      */

stock_spool(pl_to_dl,finanztreff-txt,Dates) :-
   dsa_variable(files(finanztreff-txt,pl),Files),
   stock_spool(pl_to_dl,Dates,Files,
      [ insert - finanztreff,
        insert - number_name_ft ]).
stock_spool(pl_to_dl,Mode,Dates) :-
   ( Mode = finanztreff-htm
   ; Mode = finanztreff-htm-2009
   ; Mode = hoppenstedt-htm ),
   dsa_variable(files(Mode,pl),Files),
   stock_spool(pl_to_dl,Dates,Files,
      [ insert - finanztreff,
        insert - number_name_ft ]).
stock_spool(pl_to_dl,comdirekt-export,Dates) :-
   dsa_variable(files(comdirekt-export,pl),Files),
   stock_spool(pl_to_dl,Dates,Files,
      [ insert - comdirekt,
        insert - finanztreff,
        insert - number_name_cd ]).

stock_spool(pl_to_dl,Dates_I,Files,Actions) :-
   stock_data_dates(Dates_I,Dates_L),
   maplist( stock_consult_pl_files(Files),
      Dates_L, Lists_of_Relations ),
   append(Lists_of_Relations,List_of_Relations),
   append(List_of_Relations,Tuples),
   checklist( stock_tuples_to_dl_file(Tuples),
      Actions ),
   !.


stock_spool(pl_to_dl,[short]) :-
   writeln('<--- input file: '),
   findall( [Company,Number,Abbreviation],
      special_dax_value(Company,Number,Abbreviation),
      Tuples_NN ),
   maplist( get_stock_state(short),
      Tuples_NN, States ),
   append(States,List),
   list_to_ord_set(List,State),
   stock_file(stock_all_short,File_1),
   disave_2(State,File_1), 
   stock_file(number_name,File_2),
   stock_tuples_to_dl_file(
      insert - File_2,
      Tuples_NN, get_number_fact ),
   !.


/*** implementation ***********************************************/


stock_tuples_to_dl_file(Tuples,Op-File) :-
   stock_file(stock_data,File,File_Name),
   dsa_variable(transform_to(File),Predicate),
   stock_tuples_to_dl_file(Tuples,Op-File_Name,Predicate).

dsa_variable(transform_to(number_name),
   argument_to_name_number_facts).
dsa_variable(transform_to(number_name_ft),
   argument_to_name_number_facts).
dsa_variable(transform_to(number_name_cd),
   argument_to_name_number_facts).
dsa_variable(transform_to(stock_all_long),
   argument_to_stock_fact).
dsa_variable(transform_to(finanztreff),
   argument_to_stock_fact).
dsa_variable(transform_to(comdirekt),
   argument_to_stock_fact).

stock_tuples_to_dl_file(Tuples,insert-File_Name,Predicate) :-
   writeln('<--- input file: '),
   diconsult(File_Name,State),
   stock_tuples_to_dl_file(Tuples,File_Name,Predicate,State).
stock_tuples_to_dl_file(Tuples,replace-File_Name,Predicate) :-
   stock_tuples_to_dl_file(Tuples,File_Name,Predicate,[]).

stock_tuples_to_dl_file(Tuples,File_Name,Predicate,State) :-
   maplist( Predicate,
      Tuples, State_1 ),
   list_to_ord_set(State_1,State_2),
   list_to_ord_set(State,State_Old),
   state_union(State_2,State_Old,State_3),
   list_to_ord_set(State_3,State_4),
   disave_2(State_4,File_Name).


relation_euro_to_dm(yes,Relation1,Relation2) :-
   relation_euro_to_dm(Relation1,Relation2).
relation_euro_to_dm(no,Relation,Relation).


relation_euro_to_dm(Relation1,Relation2) :-
   dislog_variable_get(euro_to_dm,Rate),
   maplist( tuple_euro_to_dm(Rate),
      Relation1, Relation2 ).

tuple_euro_to_dm(Rate,T1,T2) :-
   T1 = [Name,Number,Date,V_Euro|Xs],
   euro_to_dm(Rate,V_Euro,V_DM),
   T2 = [Name,Number,Date,V_DM|Xs].


/* euro_to_dm(V_Euro,V_DM) <-
      */

euro_to_dm(V_Euro,V_DM) :-
   dislog_variable_get(euro_to_dm,Rate),
   euro_to_dm(Rate,V_Euro,V_DM).

euro_to_dm(Rate,V_Euro,V_DM) :-
   round( Rate * V_Euro, 2, V_DM ).


/* dm_to_euro(V_DM,V_Euro) <-
      */

dm_to_euro(V_DM,V_Euro) :-
   dislog_variable_get(euro_to_dm,Rate),
   round( V_DM / Rate, 2, V_Euro ).


argument_to_stock_fact(Argument,[Atom]) :-
   Argument = [_Name,Number,Date,Value|Previous],
   Atom =.. [stock,Number,Date,Value,Previous].

argument_to_name_number_facts(Argument,[Atom]) :-
   Argument = [Name,Number|_],
   name(Name,String),
   Atom =.. [number_name,Number,String].

get_number_fact(Argument,[Atom]) :-
   Argument = [Company,Number|_],
   name(Company,String),
   Atom =.. [number_name,Number,String].

get_number_fact(Source,Argument,[Atom]) :-
   Argument = [Company,Number|_],
   name(Company,String),
   Atom =.. [number_name,Source,Number,String].


stock_pl_files_to_number_name(Date,hoppenstedt) :-
   dsa_variable(files(hoppenstedt-htm),Files),
   stock_pl_files_to_number_name(Date,Files,
      number_name_hs_menue ).
stock_pl_files_to_number_name(Date,finanztreff) :-
   dsa_variable(files([finanztreff]),Files),
   stock_pl_files_to_number_name(Date,Files,
      number_name_ft_menue ).
stock_pl_files_to_number_name(Date,comdirekt) :-
   dsa_variable(files([comdirekt,export]),Files),
   stock_pl_files_to_number_name(Date,Files,
      number_name_cd_menue ).

stock_pl_files_to_number_name(Date,Files,File) :-
   checklist( stock_pl_file_to_number_name_reordered(Date,File),
      Files).

stock_pl_file_to_number_name_reordered(Date,File2,File1) :-
   stock_pl_file_to_number_name(Date,File1,File2).

stock_pl_file_to_number_name(Date,File1,File2) :-
   stock_file(data,Date,Path),
   concat([Path,'/',File1,'.pl'],Path1),
   stock_file(stock_data,File2,Path2),
   stock_consult_pl_file(Path1,Relation),
   date_to_day(Date,Day),
   maplist( get_number_fact([File1,Day]),
      Relation, State ),
   diinsert(State,Path2).


/******************************************************************/


