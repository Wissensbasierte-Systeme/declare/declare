

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Spooler for www -> pl                ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      stock_file_read_and_parse/3.


/*** tests ********************************************************/


test(stock_tool, stock_file_to_xpce_table) :-
   dislog_variable_get(example_path, Examples),
   concat(Examples, 'xml/stock_test.htm', Path),
   stock_file_read_and_parse(Path, '06_04_21', State),
   findall( [Name, Wkn, '10.04.2004', Value, Percent],
      member(
         stock(Name, Wkn, _, Value, Percent, '--', '--', '--'),
         State ),
      Rows ),
   xpce_display_table(_, _,
      ['Name', 'Wkn', 'Date', 'Value', 'Percent'], Rows).

/*
test(stock_tool, stock_file_to_xpce_table) :-
   dislog_variable_get(example_path, Examples),
   concat(Examples, 'xml/dax_30.htm', Path),
   stock_file_to_xpce_table(Path, _Attributes:_Data).
test(stock_tool, stock_file_to_html_table) :-
   dislog_variable_get(example_path, Examples),
   dislog_variable_get(source_path, Sources),
   concat(Examples, 'xml/dax.htm', Path),
   concat(Sources,
      'stock_tool/stock_spooler/stock_file_substitutions',
      Substitution_File),
   stock_file_select_data_table(Path, Table),
   fn_transform_fn_item_fng(Substitution_File,
      html:[table:Table], Html_2),
   html_to_display(Html_2).
*/


/*** interface ****************************************************/


/* stock_file_to_xpce_table(Path, Attributes:Data) <-
      */

stock_file_to_xpce_table(Path, Attributes:Data) :-
   stock_file_to_html_term(Path, HTML_Term),
   xml_swi_to_fn(HTML_Term, FN_Term_1),
   fn_hide_elements([
       br:_:_, class:_, rowspan:_, colspan:_,
       border:_, cellpadding:_, cellspacing:_, width:_,
       bgcolor:_, img:_:_ ],
      FN_Term_1, FN_Term_2 ),
   !,
   fn_table_to_xpce_table(FN_Term_2, Attributes, Data),
   xpce_display_table(_, _, Attributes, Data).


/* fn_table_to_xpce_table(Table, Attributes, Data) <-
      */

fn_table_to_xpce_table(Table, Attributes, Data) :-
   [Header_Row|Data_Rows] := Table^table^tbody,
   !,
   table_row_to_list(th, Header_Row, Attributes),
   maplist( table_row_to_list(td),
      Data_Rows, Data ).

table_row_to_list(Tag, tr:Entries, List) :-
   findall( X,
      ( member(Tag:Xs, Entries),
        member(X, Xs) ),
      List ).


/* stock_file_read_and_parse(Path, Date, State) <-
      */

stock_file_read_and_parse(Path, Date, State) :-
   dislog_variable_get(spool_source_version, 'Finanztreff 2009'),
   !,
   concat(Path, '.tbody', Path_2),
   stock_html_extract_tbody(Path, Path_2),
   stock_html_file_extract_rows(Path_2, Rows),
   date_to_day(Date, Day),
   ( foreach(Row, Rows), foreach(C, State) do
        Row = [Wkn, Name, _, Value, Change],
        C = stock(
           Name, Wkn, Day, Value, Change, '--', '--', '--' ) ).
       
stock_file_read_and_parse(Path, Date, State) :-
   dislog_variable_get(spool_source_version, 'Finanztreff 2003'),
   !,
   date_to_day(Date, Day),
   stock_file_select_data_table(Path, Table),
   stock_term_to_tuples(Day, Table, State).

% 13.05.2008

stock_file_select_data_table(Path, Table) :-
   dread(xml, Path, [Html]),
   ( findall( Row,
        Row := Html/_/tr::[@class=_],
        Rows ),
     Table = table:Rows
%    Table := Html/_/table::[@class='extendetKursliste']
   ; Table = table:[] ).

% 25.04.2006

stock_file_select_data_table(Path, Table) :-
   dread(xml, Path, [Html]),
   ( Table := Html^html^'BODY'^table^tr^td^table^tr^td^br^br
        ^table::[^tr^td^a^content::'*'=['Kurs']]
   ; Table = table:[] ).

stock_file_select_data_table(Path, Table) :-
   stock_file_purify(Path, Path_2),
   load_structure(Path_2, Content,
      [dialect(xml), max_errors(5000), space(sgml)]),
   xml_swi_to_fn(Content, FN_Term),
   name_append('rm ', Path_2, Command), us(Command),
   !,
   stock_term_select_data_table(FN_Term, Table).
   
stock_term_select_data_table(FN_Term, Table) :-
%  Path = 'HTML'^html^'BODY'^table^tr^td^br^br^br^table,
   Path = 'HTML'^html^'BODY'^table^tr^td^br^br^table,
   findall( Table,
      Table := FN_Term^Path,
      Tables ),
   length(Tables, N), writeln(user, N),
   last_element(Tables, Table).
%  nth(3, Tables, Table).
%  nth(4, Tables, Table).


stock_file_read_and_parse(Path, Date, State) :-
   dislog_variable_get(spool_source, 'Finanztreff'),
   !,
   date_to_day(Date, Day),
   stock_file_to_html_term(Path, HTML_Term),
   stock_term_to_tuples(Day, HTML_Term, State).

stock_file_to_html_term(Path, HTML_Term) :-
   load_html_file(Path, HTML_Term),
   !.

stock_file_to_html_term(Path, HTML_Term) :-
   read_file_to_string(Path, String_1),
   shorten_html_string(String_1, String_2),
   name(Name, String_2),
   name_append(Path, '.short', Path_2),
   tell(Path_2), writeln(Name), told,
   !,
   load_html_file(Path_2, HTML_Term),
   name_append('rm ', Path_2, Command), us(Command),
   !.

stock_file_purify(Path_1, Path_2) :-
   read_file_to_string(Path_1, String_1),
   list_exchange_sublist(
      [[[151], "-- "], ["<! SZMFRABO", "<!-- SZMFRABO"]],
      String_1, String_2),
   name(Name, String_2),
   name_append(Path_1, '.pure', Path_2),
   tell(Path_2), writeln(Name), told.


stock_file_read_and_parse(Path, Date, State) :-
   dislog_variable_get(spool_source, 'Hoppenstedt'),
   !,
   parse_table_rows(Path, Rows, _),
   date_to_day(Date, Day),
   maplist( row_to_stock_atom(Day),
      Rows, State ),
   !.


stock_file_read_and_parse(Path, Date, State) :-
   parse_table_rows(Path, Rows),
   sublist( stock_row,
      Rows, Stock_Rows ),
   date_to_day(Date, Day),
   maplist( row_to_stock_atom(Day),
      Stock_Rows, State ).


/*** implementation ***********************************************/


parse_table_rows_to_names(Path, Names) :-
   parse_table_rows(Path, Rows),
   maplist( string_to_name,
      Rows, Names ).

parse_table_rows(Path, Rows) :-
   parse_table_rows(Path, Rows, Rest),
   !,
   Rest = [].


parse_table_rows(Path, Rows, Rest) :-
   dislog_variable_get(spool_source, 'Hoppenstedt'),
   !,
   stock_file_read_www_table(Path, Lines),
%  checklist( writeln_as_name, Lines ),
   !,
   maplist( list_remove_elements(back, [13]),
      Lines, Lines_2 ),
   flatten(Lines_2, String_2),
%  writeln('end of flatten'),
   list_exchange_sublist([["&nbsp;", " "]], String_2, String),
%  list_cut_at_position(["<CAPTION", "<caption"], String_3, String),
   !,
%  name(Name, String), writeln(Name),
%  writeln('starting table rows'),
%  writeln_as_name(String),
   string_to_names(String, Atoms),
   hoppenstedt_table_rows(Rows, Atoms, Rest),
%  writeln('end of table rows'),
%  writeln_list(Rows),
   !.

parse_table_rows(Path, Rows, Rest) :-
   stock_file_read_www_table(Path, Lines),
   maplist( list_remove_elements(back, [13]),
      Lines, Lines_2 ),
   flatten(Lines_2, String_2),
%  writeln('end of flatten'),
   list_exchange_sublist([["&nbsp;", " "]], String_2, String_3),
   list_cut_at_position(["<CAPTION", "<caption"], String_3, String),
   string_to_names(String, Atoms),
   !,
%  writeln('starting table rows'),
   table_rows(Rows, Atoms, Rest),
%  writeln('end of table rows'),
   !.


row_to_stock_atom(Day, Row, Atom) :-
   dislog_variable_get(spool_source, 'Comdirekt'),
   ( Row = [_X1, [Company], [Number], [X4],
        _, _, _, _, _, _, _, [Change_Percent]|_]
   ; Row = [[Company], [Number], [X4],
        _, _, _, _, _, _, _, [Change_Percent]|_] ),
   possibly_dsa_name_to_number(Company, Number, Number_2),
   cut_off_percent(Change_Percent, Change),
   possibly_stock_multiply_2(Number_2, X4, Value),
   name_exchange_elements(["'`"], Company, Company_2),
   Atom = stock(Company_2, Number_2, Day,
      Value, Change, '--', '--', '--').
  
row_to_stock_atom(Day, Row, Atom) :-
   dislog_variable_get(spool_source, 'Finanztreff'),
   Row = [[Company, Number], _, _, [Value|_], _, _, [_, Change]|_],
   name_exchange_elements([",."], Value, Value_2),
   possibly_dsa_name_to_number(Company, Number, Number_2),
   possibly_stock_multiply_2(Number_2, Value_2, Value_3),
   name_exchange_elements(["'`"], Company, Company_2),
   name_remove_elements(back, " ", Company_2, Company_3),
   name_exchange_elements([",."], Change, Change_2),
   Atom = stock(Company_3, Number_2, Day,
      Value_3, Change_2, '--', '--', '--').

row_to_stock_atom(Day, Row, Atom) :-
   dislog_variable_get(spool_source, 'Hoppenstedt'),
   Row = [Company, Number, Value, Changes],
   possibly_dsa_name_to_number(Company, Number, Number_2),
   possibly_stock_multiply_2(Number_2, Value, Value_2),
   name_exchange_elements(["'`"], Company, Company_2),
   Atom =.. [stock, Company_2, Number_2, Day, Value_2|Changes].


possibly_dsa_name_to_number(Name, 'n/a', Number) :-
   !,
   dsa_name_to_number(Name, Number).
possibly_dsa_name_to_number(_, Number, Number).


cut_off_percent(Change_Percent, Change) :-
   name(Change_Percent, String_1),
   append(String_2, [37], String_1),
   !,
   name(Change, String_2).
cut_off_percent(Change, Change).


possibly_stock_multiply_2(Number, Value_1, Value_2) :-
   name(Number, N),
   name(Value_1, V_1),
   possibly_stock_multiply(N, V_1, V_2),
   name(Value_2, V_2).


stock_row(Row) :-
   \+ header_row(Row),
   \+ unknown_row(Row),
   \+ Row = [[_]].

header_row([['Portrait']|_]).
header_row([['Bezeichnung']|_]).
header_row([['Name', 'WPKN']|_]).

unknown_row([['unknown symbol specified']|_]).


table_rows([Row|Rows]) -->
   table_row(Row),
%  { writeln(user, Row) },
   !,
   table_rows(Rows),
   !.
table_rows([]) -->
   { true },
   !.

table_row(Row) -->
   necessary_tag(['TR']),
   table_row_entries(Row),
   necessary_tag(['/TR']),
   !.

table_row_entries([Entry|Entries]) -->
   table_row_entry(Entry),
   !,
   table_row_entries(Entries),
   !.
table_row_entries([]) -->
   { true },
   !.

table_row_entry(Entry) -->
   necessary_tag(['TD']),
   possible_tag(['FONT', 'font'], FONT),
   possible_tag(['a href', 'A HREF', 'A href'], AHREF),
   table_entities(Entry),
   possible_tag(['/a', '/A'], AHREF),
   possible_tag(['/FONT', '/font'], FONT),
   necessary_tag(['/TD']),
   !.

table_entities([Value|Values]) -->
   table_entity(Value),
   { Value \= '' },
   table_entities(Values),
   !.
table_entities(Values) -->
   necessary_tag(['br', 'BR']),
   table_entities(Values),
   !.
table_entities([]) -->
   !.

table_entity(--) -->
   necessary_tag(['img', 'IMG']),
   !.
table_entity(Value) -->
   necessary_tag(['a', 'A', 'font', 'FONT']),
   table_entity(Value),
   necessary_tag(['/a', '/A', '/font', '/FONT']),
   !.
table_entity(Value) -->
   skip_sequence,
   list_of_all_except(['<', '>'], Xs),
   { name_append(Xs, Value) },
   !.


list_of(Xs, [Y|Ys]) -->
   [Y],
   { member(Y, Xs) },
   !,
   list_of(Xs, Ys),
   !.
list_of(_, []) -->
   { true }.

list_of_all_except(Xs, [Y|Ys]) -->
   all_except(Xs, Y),
   list_of_all_except(Xs, Ys),
   !.
list_of_all_except(_, []) -->
   { true }.

all_except(Xs, Y) -->
   [Y],
   { \+ member(Y, Xs) },
   !.


skip_until_tag(Tags) -->
   { member(Tag, Tags), atom_chars(Tag, As) },
   ['<'|As], list_of_all_except(['>'], _), ['>'],
   !.
skip_until_tag(Tags) -->
   [_],
   skip_until_tag(Tags).


necessary_tag(Tags) -->
   skip_sequence,
   { member(Tag, Tags), atom_chars(Tag, As) },
   ['<'|As], list_of_all_except(['>'], _), ['>'],
   !.

possible_tag(Tags, 1) -->
   necessary_tag(Tags).
%  !.
possible_tag(_, 0) -->
   { true }.


skip_sequence -->
   [N],
   { name(N, [X]), X < 40 },
   !,
   skip_sequence,
   !.
skip_sequence -->
   { true },
   !.


stock_file_read_www_table(File, Lines) :-
   stock_file_fix_errors(File),
   see(File),
   dislog_variable_get(spool_source, Spool_Source),
   read_stock_lines_www_table(header, Spool_Source, Lines),
   seen.


read_stock_lines_www_table_until(Stop_String, Name) :-
   read_stock_line_www_table(Line),
%  name(Line_Name, Line), writeln(Line_Name),
   append(_, Line_Tail, Line), append(Stop_String, _, Line_Tail),
   !, 
   name(Name, Line).
read_stock_lines_www_table_until(Stop_String, Name) :-
   read_stock_lines_www_table_until(Stop_String, Name).

read_stock_lines_www_table(header, 'Hoppenstedt', Lines) :-
   read_stock_lines_www_table_until("Wertpapiername", _),
   read_stock_lines_www_table_until("</tr>", _),
   read_stock_lines_www_table(data, Lines).

read_stock_lines_www_table(header, 'Comdirekt', Lines) :-
   read_stock_line_www_table(Line),
   ( ( one_of_strings_is_prefix_of_list(
          ["<TABLE"], Line),
       read_stock_lines_www_table(data, Lines) )
   ; read_stock_lines_www_table(header, 'Comdirekt', Lines) ).
read_stock_lines_www_table(header, 'Finanztreff', Lines) :-
   read_stock_lines_www_table(header, 'Finanztreff', 1, Lines).
read_stock_lines_www_table(header, 'Finanztreff', N, Lines) :-
   N < 4,
   read_stock_line_www_table(Line),
   ( ( one_of_strings_is_prefix_of_list(
          ["<TABLE"], Line),
       M is N + 1,
       read_stock_lines_www_table(header, 'Finanztreff', M, Lines) )
   ; read_stock_lines_www_table(header, 'Finanztreff', N, Lines) ).
read_stock_lines_www_table(header, 'Finanztreff', 4, Lines) :-
   read_stock_lines_www_table(data, Lines).

read_stock_lines_www_table(data, Lines) :-
   read_stock_line_www_table(Line),
   ( ( one_of_strings_is_prefix_of_list(
          ["</TABLE", "</table"], Line),
       Lines = [] )
   ; ( read_stock_lines_www_table(data, Lines2),
       Lines = [Line|Lines2] ) ).

read_stock_line_www_table(Line) :-
   read_stock_line_www_table([], I_Line),
   reverse(I_Line, O_Line),
   list_remove_elements(front, " ", O_Line, Line).

read_stock_line_www_table(Sofar, Line) :-
   get0(C),
   ( ( C == 10, Line = Sofar )
   ; ( C == -1, Line = Sofar )
   ; ( C \== 10, read_stock_line_www_table([C|Sofar], Line) ) ).


/* stock_html_file_extract_rows(File, Rows) <-
      */

stock_html_file_extract_rows(File) :-
   stock_html_file_extract_rows(File, Rows),
   xpce_display_table(_, _,
      ['Name', 'Wkn', 'Date', 'Value', 'Percent'], Rows).

stock_html_file_extract_rows(File, Rows) :-
   dread(xml, File, [Html]),
   findall( [Wkn, Name, Date, Value, Change],
      ( Entry := Html/tr,
        stock_html_table_row_exists_td(Entry),
        [Wkn_2] := Entry/nth_child::2/content::'*',
        [Name_2] := Entry/nth_child::3/a/content::'*',
        name_cut_at_position(["..."], Name_2, Name_3),
        name_exchange_elements(["'`"], Name_3, Name),
        possibly_dsa_name_to_number(Name_3, Wkn_2, Wkn_3),
        wkn_to_save_wkn(Wkn_3, Wkn_4),
        name(Wkn_4, Xs), name(Wkn, Xs),
        [Value_2] := Entry/nth_child::5/a/content::'*',
        name_remove_elements(".", Value_2, Value_3),
        name_exchange_elements([",."], Value_3, Value_4),
        name_to_number(Value_4, Value_5),
        possibly_stock_multiply_2(Wkn, Value_5, Value),
        [Date] := Entry/nth_child::6/content::'*',
        [Change_2] := Entry/nth_child::7/content::'*',
%       name_exchange_elements([[±+]], Change_2, Change_3),
        name_exchange_elements([[177,43]], Change_2, Change_3),
        name_exchange_elements([",."], Change_3, Change_4),
        cut_off_percent(Change_4, Change),
        write(user, '.') ),
      Rows ).

stock_html_table_row_exists_td(Entry) :-
   _ := Entry/td,
   !.


/* stock_html_extract_tbody(File_1, File_2) <-
      */

stock_html_extract_tbody(File_1, File_2) :-
   dread(txt, File_1, Name_1),
   name_start_after_position(["<h3>Einzelwerte </h3>"], Name_1, Name_2),
   name_start_after_position(["<tbody>"], Name_2, Name_3),
   name_cut_at_position(["</tbody>"], Name_3, Name_4),
   concat(['<?xml version=''1.0'' encoding=''ISO-8859-1'' ?>\n',
      '<tbody>\n', Name_4, '\n</tbody>'], Name_5),
   dwrite(txt, File_2, Name_5).


/******************************************************************/


