

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Spooler for www -> pl                ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      stock_spool/2, stock_spool/3, stock_spool/4.


/*** interface ****************************************************/


/* stock_spool(www_to_pl,Source,[Date1,Date2]) <-
      transforms WWW data into Prolog data. */

stock_spool(www_to_pl,Source,Dates_I) :-
   stock_data_dates(Dates_I,Dates_L),
   checklist( stock_spool(single(www_to_pl),Source),
      Dates_L ).

stock_spool(single(www_to_pl),Source,Date) :-
   dislog_variable_get(spool_source,'Comdirekt'),
   dsa_variable(files(Source),Files),
   maplist( dsa_file_to_pair(html-pl),
      Files, Pairs ),
   checklist( stock_spool(pairs(www_to_pl),Source,Date),
      Pairs ).
stock_spool(single(www_to_pl),Source,Date) :-
   ( dislog_variable_get(spool_source,'Finanztreff')
   ; dislog_variable_get(spool_source,'Hoppenstedt') ),
   dsa_variable(files(Source),Files),
   ( dislog_variable_get(
        spool_source_version, 'Finanztreff 2009') ->
     H = html
   ; H = htm ),
   maplist( dsa_file_to_pair(H-pl),
      Files, Pairs ),
%  file_date_to_nice_day(Date,Day),
%  name_append(['Spooling ',Day,' ...'],Label),
%  checklist_with_status_bar( Label,
%  checklist_with_status_bar_advanced(
   checklist_with_status_bar_dsa(
      stock_spool(pairs(www_to_pl),Source,Date),
      Pairs ).

stock_spool(pairs(www_to_pl),Source,Date,File_1-File_2) :-
   dislog_variable_set(dax_date,Date),
   stock_file(data_date,File_1,Path_1),
   stock_file(data_date,File_2,Path_2),
   writeln_list(['<--- input file: ',Path_1]),
   stock_spool(pairs_sub(www_to_pl),Source,Date,Path_1-Path_2).

stock_spool(pairs_sub(www_to_pl),_,_,Path_1-_) :-
   \+ file_exists(Path_1),
   !,
   writeln('file does not exist !').
stock_spool(pairs_sub(www_to_pl),Mode,Date,Path_1-Path_2) :-
   ( Mode = comdirekt-html
   ; Mode = finanztreff-htm
   ; Mode = finanztreff-htm-2009
   ; Mode = hoppenstedt-htm ),
   stock_file_read_and_parse(Path_1,Date,Atoms),
   writeln('---> output file: '),
   writeln(Path_2),
   write_stock_file_pl(Path_2,Atoms),
   !.
stock_spool(pairs_sub(www_to_pl),Mode,Date,Path_1-Path_2) :-
   stock_file_read(Path_1,Lines),
%  checklist( writeln_as_name, Lines ),
   stock_spool_parse(www_to_pl,Mode,Date,Lines,Atoms),
   writeln('---> output file: '),
   writeln(Path_2),
   write_stock_file_pl(Path_2,Atoms),
   !.
   

/*** implementation ***********************************************/


stock_spool_parse(www_to_pl,finanztreff-html,_,Lines,Atoms) :-
   maplist( parse_stock_string,
      Lines, P_Strings ),
   lists_of_strings_to_stock_atoms(P_Strings,Stock_Atoms),
   beautify_stock_atoms(Stock_Atoms,Atoms).
stock_spool_parse(www_to_pl,finanztreff-txt,Date,Lines,Atoms) :-
%  maplist(parse_stock_string_2(Day),Lines,P_Strings),
   parse_stock_strings_date(Date,Lines,P_Strings),
   lists_of_strings_to_stock_atoms(P_Strings,Stock_Atoms),
   beautify_stock_atoms(Stock_Atoms,Atoms).
stock_spool_parse(www_to_pl,comdirekt-export,Date,Lines,Atoms) :-
   maplist( parse_with_stops,
      Lines, P_Strings ),
   lists_of_strings_to_stock_atoms(P_Strings,Stock_Atoms),
   project_stock_atoms_date(Date,Stock_Atoms,Atoms).


get_stock_state(short,[_,Number,Abbreviation],State) :-
   get_single_data(short,Abbreviation,Data),
   single_data_to_stock_state(Number,Data,State).

get_single_data(short,Company,Data) :-
   !,
   stock_file(charts,Company,File),
   consult(File),
   single_data(Company,Data).

single_data_to_stock_state(Number,
      [Date,Value|DV_Pairs],[[Atom]|State]) :-
   Atom =.. [stock,Number,Date,Value,[]],
   single_data_to_stock_state(Number,DV_Pairs,State).
single_data_to_stock_state(_,[],[]).


parse_stock_string(S,[N,W,Date|X_List]) :-
   stock_get_name_and_number(S,N,W,Y),
   stock_get_date(Y,Date,Ys),
   read_number_strings(5,Ys,X_List,_).

parse_stock_string_2(Date,String,Tuple) :-
   stock_get_name_and_number(String,Name,Number,Y),
   read_number_strings_2(
      [8,7,8,7,8],Y,[X1,_X2,_X3,_X4,X5],_),
   name(N1,X1), name(N5,X5),
   ( ( \+ true_equal(N1,0),
       Q is round( ( ( N5 - N1 ) / N1 ) * 10000 ) / 100,
       name(Q,V) )
   ; ( true_equal(N1,0),
       name('--',V) ) ),
   possibly_stock_multiply(Number,X5,X6),
   Tuple = [Name,Number,Date,X6,V,[],[],[]].

parse_stock_string_3(Date,String,Part_of_Name_1,X,Tuple) :-
   stock_get_name_and_number(String,Part_of_Name_2,Number,Y),
%  writeln_as_names(user,[String]), writeln(user,'-->'),
%  writeln_as_names(user,[Part_of_Name_2,Number,Y]), nl,
   read_number_strings_2(
      [8,7,8,7,8],Y,[X1,_X2,_X3,_X4,Z5],_),
   exchange_parse_stock_string_3(X,Z5,X5),
   relative_change_for_strings(X1,X5,Change),
   possibly_stock_multiply(Number,X5,Value),
   possibly_append_part_of_name(Part_of_Name_1,Part_of_Name_2,Name),
   list_exchange_elements(["'`"],Name,Correct_Name),
   Tuple = [Correct_Name,Number,Date,Value,Change,[],[],[]].
%  writeln(user,'***'), writeln(Tuple).


possibly_append_part_of_name([],Y,Y) :-
   !.
possibly_append_part_of_name(X,Y,Z) :-
   append([X,[32],Y],Z).
   
possibly_stock_multiply(Number,Value_1,Value_2) :-
   name(N,Number),
   stock_multiply_tuple(Name,N,Factor),
   !,
   name(V_1,Value_1),
   V_2 is V_1 * Factor,
   name(V_2,Value_2),
   write_list(['multiply ',Name,': ',V_1,' -> ',V_2]), nl.
possibly_stock_multiply(_,Value,Value).

possibly_stock_multiply_cd(Number,Value_1,Value_2) :-
   stock_multiply_tuple(Name,Number,Factor),
   make_number(Value_1,Value_3),
   !,
   Value_2 is Value_3 * Factor,
   write_list(['multiply ',Name,': ',Value_3,' -> ',Value_2]), nl.
possibly_stock_multiply_cd(_,Value,Value).

make_number(Value,Value) :-
   number(Value),
   !.
make_number(Value_1,Value_2) :-
   name(Value_1,Name_1),
   list_remove_elements(front," ",Name_1,Name_2),
   name(Value_2,Name_2).
   

exchange_parse_stock_string_3([],X,X) :-
   !.
exchange_parse_stock_string_3(Z,_,Z).

parse_stock_strings_2(Date,[S|Ss],[T|Ts]) :-
   parse_stock_string_2(Date,S,T),
   !,
   parse_stock_strings_2(Date,Ss,Ts).
parse_stock_strings_2(Date,[S1,S2|Ss],[T|Ts]) :-
   append(S1,[32|S2],S),
   parse_stock_string_2(Date,S,T),
   parse_stock_strings_2(Date,Ss,Ts).
parse_stock_strings_2(_,[],[]).


extraordinary_stock_text_line(Line,Part_of_Name,Y) :-
   list_contains_sequence(Line,[31,40],32),
   !,
   first_n_elements(35,Line,Part_of_Name_Plus),
   list_remove_elements(both," ",Part_of_Name_Plus,Part_of_Name),
   append(Part_of_Name_Plus,Rest_of_Line,Line),
   list_remove_elements(both," ",Rest_of_Line,X),
   list_exchange_elements([",."],X,Y).
extraordinary_stock_text_line(Line,Part_of_Name,[]) :-
   length(Line,L),
   L > 2, L < 35,
   list_remove_elements(both," ",Line,Part_of_Name).


parse_stock_strings_day(Date,Lines,P_Strings) :-
   date_to_day_string(Date,Day),
   parse_stock_strings_day(Day,Lines,P_Strings).

parse_stock_strings_day(Date,[S|Ss],[T|Ts]) :-
%  writeln(user,'*** b ***'), writeln_as_name(S),
   parse_stock_string_3(Date,S,[],[],T), !,
%  writeln(user,'*** b ***'),
   parse_stock_strings_day(Date,Ss,Ts).
parse_stock_strings_day(Date,[S1,S2|Ss],Ts) :-
%  writeln(user,'*** z ***'), writeln_as_name(S1),
   length(S1,L), L < 30, !,
   append(S1,[32|S2],S),
%  writeln(user,'*** z ***'),
   parse_stock_strings_day(Date,[S|Ss],Ts).
parse_stock_strings_day(Date,[S1,S2|Ss],[T|Ts]) :-
%  writeln(user,'*** a ***'), writeln_as_name(S1),
   extraordinary_stock_text_line(S1,Part_of_Name,Y), !,
%  writeln_as_names(user,[S1,Part_of_Name,Y]),
%  writeln(user,'*** a ***'),
   parse_stock_string_3(Date,S2,Part_of_Name,Y,T),
   parse_stock_strings_day(Date,Ss,Ts).
parse_stock_strings_day(Date,[S1,S2|Ss],Ts) :-
%  writeln(user,'*** c ***'), writeln_as_name(S1),
   append(S1,[32|S2],S),
%  writeln(user,'*** c ***'),
%  parse_stock_string_3(Date,S,[],[],T),
   parse_stock_strings_day(Date,[S|Ss],Ts).
parse_stock_strings_day(_,[],[]).


stock_get_name_and_number(Xs,Name,Number,Ys) :-
   stock_get_name_and_number(Xs,[],I_Name,Number,Ys),
   reverse(I_Name,Name).

stock_get_name_and_number([X1,X2,X3,X4,X5,X6|Xs],
      Name,Name,Number,Xs) :-
   digit(X1), digit(X2), digit(X3),
   digit(X4), digit(X5), digit(X6),
   !,
   Number = [X1,X2,X3,X4,X5,X6].
stock_get_name_and_number([X|Xs],Sofar,Name,Number,Ys) :-
   stock_get_name_and_number(Xs,[X|Sofar],Name,Number,Ys).


stock_get_date([X1,X2,X3,X4,X5,X6,X7,X8|Xs],Date,Xs) :-
   digit(X1),
   !,
   Date = [X1,X2,X3,X4,X5,X6,X7,X8].
stock_get_date([_|Xs],Date,Ys) :-
   stock_get_date(Xs,Date,Ys).


read_number_strings_2([Size|Sizes],S1,[X|Xs],Rest) :-
   read_number_string_4(Size,S1,X,S2),
   read_number_strings_2(Sizes,S2,Xs,Rest).
read_number_strings_2([],S,[],S).


read_number_string_4(_,Xs,Vs,Ys) :-
   read_number_string_4(Xs,Vs,Ys).

read_number_string_4(Xs,Vs,Ys) :-
   read_number_string_3(Xs,Zs,Ys),
   list_remove_elements(both," ",Zs,Us),
   list_exchange_elements([",."],Us,Vs).

read_number_string_3([32|Xs],Zs,Ys) :-     % skip blanks
   read_number_string_3(Xs,Zs,Ys).
read_number_string_3([91|Xs],[],Xs).       % [Kurs
read_number_string_3([X1,X2,X3|Xs],[X1,X2,X3],Xs) :-
   element(X1,",."),
   !.
read_number_string_3([X|Xs],[X|Zs],Ys) :-
   read_number_string_3(Xs,Zs,Ys).


read_number_string_2(Size,[32,X|Xs],[32],[X|Xs]) :-
   Size > 0,
   X \= 32,
   !.
read_number_string_2(Size,[X|Xs],[X|Zs],Ys) :-
   element(X,",."),
   Size < 3,
   !,
   read_number_string_2(2,Xs,Zs,Ys).
read_number_string_2(Size,[X1,X2,X3,X4|Xs],[X1,X2,X3],[X4|Xs]) :-
   element(X1,",."),
   X4 \= 32,
   Size > 3,
   !.
% read_number_string_2(1,[32|Xs],[32],Xs) :-
%    !.
read_number_string_2(Size,[X|Xs],[X|Zs],Ys) :-
   Size > 0,
   New_Size is Size - 1,
   read_number_string_2(New_Size,Xs,Zs,Ys).
% read_number_string_2(0,[X|Xs],[X|Zs],Ys) :-
%    X \= 32,
%    !,
%    read_number_string_2(0,Xs,Zs,Ys).
read_number_string_2(0,Xs,[],Xs).


read_number_strings(I,S1,[X|Xs],Rest) :-
   I > 0,
   read_number_string(S1,X,S2),
   J is I - 1,
   read_number_strings(J,S2,Xs,Rest).
read_number_strings(0,S,[],S).

read_number_string(Xs,S,Ys) :-
   read_number_string(Xs,[],R,Ys),
   reverse(R,S).

read_number_string([32|Xs],[],S,Ys) :-                /* ' ' */
   read_number_string(Xs,[],S,Ys).
read_number_string([32|Xs],S,S,Xs).                   /* ' ' */
read_number_string([37|Xs],S,S,Xs).                   /* '%' */
read_number_string([X|Xs],Sofar,S,Ys) :-
   read_number_string(Xs,[X|Sofar],S,Ys).


lists_of_strings_to_stock_atoms(S_Lists,Atoms) :-
   maplist( lists_to_names,
      S_Lists, N_Lists ),
   maplist( functor_and_arguments_to_atom(stock),
      N_Lists, Atoms ).


/* parse_with_stops(String,Stop,Items) <-
      */

parse_with_stops(String,Items) :-
   parse_with_stops(String,59,Items).

parse_with_stops([],_,[]) :-
   !.
parse_with_stops(Line,Stop,[Item|Items]) :-
   read_until_stop(Line,Stop,Item,Rest_Line),
   parse_with_stops(Rest_Line,Stop,Items).


/* read_until_stop(String,Stop,Item,Rest_String) <-
      */

read_until_stop([13|Xs],Stop,Ys,Rest_String) :-
   !,
   read_until_stop(Xs,Stop,Ys,Rest_String).
read_until_stop([Stop|Rest_String],Stop,[],Rest_String) :-
   !.
read_until_stop([39|Xs],Stop,[96|Ys],Rest_String) :-
   !,
   read_until_stop(Xs,Stop,Ys,Rest_String).
read_until_stop([X|Xs],Stop,[X|Ys],Rest_String) :-
   read_until_stop(Xs,Stop,Ys,Rest_String).
read_until_stop([],_,[],[]).


project_stock_atoms_date(Date,Atoms_1,Atoms_2) :-
   date_to_day(Date,Day),
   project_stock_atoms_day(Day,Atoms_1,Atoms_2).

project_stock_atoms_day(Day,[A1|As1],As2) :-
   A1 = stock('unknown symbol specified',_,_,_,_,_,_,_,_,_,_,_,_),
   !,
   project_stock_atoms_day(Day,As1,As2).
project_stock_atoms_day(Day,[A1|As1],[A2|As2]) :-
   A1 = stock(Name,'n/a',Value,_,_,_,_,_,_,_,_,_,Change),
   !,
   dsa_name_to_number(Name,Number),
   A2 = stock(Name,Number,Day,Value,Change,'--','--','--'),
   project_stock_atoms_day(Day,As1,As2).
project_stock_atoms_day(Day,[A1|As1],[A2|As2]) :-
   A1 = stock(Name,Number,Value,_,_,_,_,_,_,_,_,_,Change),
   possibly_stock_multiply_cd(Number,Value,Value_2),
   A2 = stock(Name,Number,Day,Value_2,Change,'--','--','--'),
   project_stock_atoms_day(Day,As1,As2).
project_stock_atoms_day(_,[],[]).


/******************************************************************/


