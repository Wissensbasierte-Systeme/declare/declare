

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Spooler for Hoppenstedt              ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


/*
test(hoppenstedt,stock_file_read_and_parse) :-
   dislog_variable_get(spool_source,Spool_Source),
   dislog_variable_set(spool_source,'Hoppenstedt'),
   stock_file_read_and_parse(
      '/home/seipel/hoppenstedt/dax_30.htm','00_04_01',Atoms),
   list_of_elements_to_relation(Atoms,State),
   dportray(lp,State),
   dislog_variable_set(spool_source,Spool_Source).
*/


/*** interface ****************************************************/


/* hoppenstedt_table_rows(Rows) -->
      a sequence of rows of an HTML-table from a Hoppenstedt stock
      file is converted into a sequence of tuples Row of the form
         Row = [Company,Number,Value,Changes],
      where Changes = [C_Day,C_Week,C_Month,C_Year]. */

hoppenstedt_table_rows([]) -->
   hoppenstedt_table_row(R),
   { first(R,'--'), ! }.
hoppenstedt_table_rows([R|Rs]) -->
   hoppenstedt_table_row(R),
   hoppenstedt_table_rows(Rs).


hoppenstedt_table_row(Row) -->
   necessary_tag(['TR','tr']),
   hoppenstedt_name_entry(Company),
   hoppenstedt_any_entry,
   hoppenstedt_entry(Number),
   hoppenstedt_entry(Value),
   hoppenstedt_any_entry,
   hoppenstedt_percent_entry(C_Day),
   hoppenstedt_percent_entry(C_Week),
   hoppenstedt_percent_entry(C_Month),
   hoppenstedt_percent_entry(C_Year),
   necessary_tag(['/TR','/tr']),
   { Changes = [C_Day,C_Week,C_Month,C_Year],
     Row = [Company,Number,Value,Changes] }.


hoppenstedt_percent_value(Value) -->
   hoppenstedt_value(Value),
   hoppenstedt_value(_).

hoppenstedt_value(Value) -->
   necessary_tag(['FONT','font']),
   table_entity(Value_2),
   { name(Value_2,List_2),
     list_remove_elements(".",List_2,List_3),
     list_exchange_elements([",."],List_3,List),
     name(Value,List) },
   necessary_tag(['/FONT','/font']).


hoppenstedt_name_entry(Value) -->
   necessary_tag(['TD','td']),
   hoppenstedt_name(Value),
   necessary_tag(['/TD','/td']).
hoppenstedt_name_entry(--) -->
   necessary_tag(['TD','td']),
   necessary_tag(['img']),
   necessary_tag(['/TD','/td']).

hoppenstedt_name(Value) -->
   necessary_tag(['FONT','font']),
   table_entity(Value_2),
   { name(Value_2,List_2),
     list_exchange_elements([",."],List_2,List),
     name(Value,List) },
   necessary_tag(['/FONT','/font']).


hoppenstedt_entry(Value) -->
   necessary_tag(['TD','td']),
   hoppenstedt_value(Value),
   necessary_tag(['/TD','/td']).
hoppenstedt_entry(--) -->
   necessary_tag(['TD','td']),
   necessary_tag(['img']),
   necessary_tag(['/TD','/td']).


hoppenstedt_percent_entry(Value) -->
   necessary_tag(['TD','td']),
   hoppenstedt_percent_value(Value),
   necessary_tag(['/TD','/td']).
hoppenstedt_percent_entry(--) -->
   necessary_tag(['TD','td']),
   necessary_tag(['img']),
   necessary_tag(['/TD','/td']).
   

hoppenstedt_any_entry -->
   necessary_tag(['TD','td']),
   skip_until_tag(['/TD','/td']).


/******************************************************************/


