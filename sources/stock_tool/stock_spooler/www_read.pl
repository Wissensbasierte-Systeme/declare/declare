

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Reading of www-File                  ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      read_stock_lines/2.


/*** interface ****************************************************/


stock_file_read(File,Lines) :-
   see(File),
   read_stock_lines(header,Lines),
   seen.


/*** implementation ***********************************************/


read_stock_lines(header,Lines) :-
   read_stock_line(Line),
   ( ( one_of_strings_is_prefix_of_list(
          ["___","<PRE>","Name"],Line),
       read_stock_lines(data,Lines) )
   ; read_stock_lines(header,Lines) ).
read_stock_lines(data,Lines) :-
   read_stock_line(Line),
   ( ( stock_file_test(end_of_data_line,Line),
       Lines = [] )
   ; ( stock_file_test(skip_line,Line),
       read_stock_lines(data,Lines) )
   ; ( read_stock_lines(data,Lines2),
       Lines = [Line|Lines2] ) ).


read_stock_line(Line) :-
   read_stock_line([],I_Line),
   reverse(I_Line,Line).

read_stock_line(Sofar,Line) :-
   get0(C),
   ( ( C == 10, Line = Sofar )
   ; ( C == -1, Line = Sofar )
   ; ( C \== 10, read_stock_line([C|Sofar],Line) ) ).


stock_file_test(end_of_data_line,Line) :-
   one_of_strings_is_prefix_of_list(
      ["</pre>","</PRE>","Erzeugt"],Line).
stock_file_test(end_of_data_line,[13]).
stock_file_test(end_of_data_line,Line) :-
   list_remove_elements(front," ",Line,Line_2),
   string_is_prefix_of_list("Kurse",Line_2).

stock_file_test(skip_line,Line) :-
   string_is_prefix_of_list("Name",Line).
stock_file_test(skip_line,[]).


one_of_strings_is_prefix_of_list(Strings,List) :-
   member(String,Strings),
   string_is_prefix_of_list(String,List).

string_is_prefix_of_list(String,List) :-
   append(String,_,List).


read_stock_lines(I,[Line|Lines]) :-
   I > 0,
   !,
   read_stock_line(Line),
   J is I - 1,
   read_stock_lines(J,Lines).
read_stock_lines(0,[]).


/******************************************************************/


