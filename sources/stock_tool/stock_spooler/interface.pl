

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Spooler                              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dsa_dialog('Stock Spooler') <-
      */

dsa_dialog('Stock Spooler') :-
   dislog_todays_date(Date),
   dislog_variables_set([
      spool_start_date - Date,
      spool_end_date - Date ]),
   Variables = [
      'Start Date' - spool_start_date,
      'End Date' - spool_end_date ],
   assert(dislog_variables_to_labels(Variables)),
   dislog_frame_generic([
      header: 'SMS - Stock Spooler',
      above: [],
      interface: Variables,
      below: [
         'W3 > PL'  - stock_spooler('W3 > PL'),
         'PL > DL'  - stock_spooler('PL > DL'),
         'DL > SQL' - stock_spooler('DL > SQL'),
         'SQL > DB' - stock_spooler('SQL > DB'),
         'W3 > DB' - stock_spooler('W3 > DB') - below,
         'Accept'   - set_spool_dates ],
      close_button: [ 'Close' - right ] ]).


/* dsa_dialog('Stock Spooler Admin') <-
      */

dsa_dialog('Stock Spooler Admin') :-
   new(Dialog,dialog('SMS - Stock Spooler Admin')),
   checklist( send_choice_menu_gen(Dialog), [
      'WWW Source' - spool_source -
         'Finanztreff' - 'Hoppenstedt' - 'Comdirekt',
%     'Currency' - currency - 'DM' - 'Euro',
      'Mode' - spool_mode -
         'W3 > PL' - 'PL > DL' - 'DL > SQL' - 'SQL > DB' ] ),
   checklist( send_text_menu(Dialog), [
      'Start Date' - spool_start_date - Start,
      'End Date' - spool_end_date - End ] ),
   send(Dialog,append,button('Accept',
      message(@prolog,set_spool_dates,Start,End))),
   send(Dialog,append,button('Close',
      message(Dialog,destroy))),
   send(Dialog,append,button('Spool',
      message(@prolog,stock_spooler))),
   send(Dialog,open).


/*** implementation ***********************************************/


set_spool_dates :-
   dislog_dialog_variables( get, [
      'Start Date': S,
      'End Date': E ]),
   dislog_variables_set([
      spool_start_date - S,
      spool_end_date - E ]).
   
set_spool_dates(Start,End) :-
   get(Start,selection,S),
   get(End,selection,E),
   dislog_variables_set([
      spool_start_date - S,
      spool_end_date - E ]).


stock_spooler :-
   dislog_variable_get(spool_source,Spool_Source),
   dislog_variable_get(spool_mode,Spool_Mode),
   dislog_variable_get(spool_start_date,Date_1),
   dislog_variable_get(spool_end_date,Date_2),
   stock_spooler(Spool_Source,Spool_Mode,[Date_1,Date_2]).

stock_spooler('W3 > DB') :-
   stock_spooler('W3 > PL'),
   stock_spooler('PL > DL'),
   stock_spooler('DL > SQL'),
   stock_spooler('SQL > DB'),
   stock_chart(spooled_aggregated).
stock_spooler(Spool_Mode) :-
   dislog_variable_get(spool_source,Spool_Source),
   dislog_dialog_variables( get, [
      'Start Date': Date_1,
      'End Date': Date_2 ]),
   stock_spooler(Spool_Source,Spool_Mode,[Date_1,Date_2]).

stock_spooler(Source, 'W3 > DB', Dates) :-
   stock_spooler(Source, 'W3 > PL', Dates),
   stock_spooler(Source, 'PL > DL', Dates),
   stock_spooler(Source, 'DL > SQL', Dates),
   stock_spooler(Source, 'SQL > DB', Dates),
   stock_chart(spooled_aggregated).
stock_spooler('Hoppenstedt','W3 > PL',Dates) :-
   stock_spool(www_to_pl,hoppenstedt-htm,Dates).
stock_spooler('Finanztreff','W3 > PL',Dates) :-
   dislog_variable_get(spool_source_version, Version),
   ( Version = 'Finanztreff 2003',
     Mode = finanztreff-htm
   ; Version = 'Finanztreff 2009',
     Mode = finanztreff-htm-2009
   ; Mode = finanztreff-txt ),
   stock_spool(www_to_pl,Mode,Dates).
stock_spooler('Comdirekt','W3 > PL',Dates) :-
%  stock_spool(www_to_pl,comdirekt-export,Dates).
   stock_spool(www_to_pl,comdirekt-html,Dates).
stock_spooler('Hoppenstedt','PL > DL',Dates) :-
   dislog_variable_adjust(change_euro_to_dm),
   stock_spool(pl_to_dl,hoppenstedt-htm,Dates).
stock_spooler('Finanztreff','PL > DL',Dates) :-
   dislog_variable_adjust(change_euro_to_dm),
   dislog_variable_get(spool_source_version, Version),
   ( Version = 'Finanztreff 2003',
     Mode = finanztreff-htm
   ; Version = 'Finanztreff 2009',
     Mode = finanztreff-htm-2009
   ; Mode = finanztreff-txt ),
   stock_spool(pl_to_dl,Mode,Dates).
stock_spooler('Comdirekt','PL > DL',Dates) :-
   dislog_variable_adjust(change_euro_to_dm),
   stock_spool(pl_to_dl,comdirekt-export,Dates).
stock_spooler(_,'DL > SQL',_) :-
   mysql_select_max_id(Maximal_Id),
   dsa_variable_set(maximal_finanztreff,Maximal_Id),
   dsa_prolog_to_sql_insert_file(insert,finanztreff),
   stock_file(stock_data,'finanztreff',File),
   writeln('clearing finanztreff'),
   disave_2([],File).
stock_spooler(_,'SQL > DB',_) :-
   stock_file(stock_data,'finanztreff.sql',File),
   name_append(['cat ',File,' | mysql'],Command),
   us(Command).

dislog_variable_adjust(change_euro_to_dm) :-
   dislog_variable_get(currency,'DM'),
   dislog_variable_set(change_euro_to_dm,yes).
dislog_variable_adjust(change_euro_to_dm) :-
   dislog_variable_get(currency,'Euro'),
   dislog_variable_set(change_euro_to_dm,no).


/******************************************************************/


