

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Charts for DAX Shares                ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      stock_state_to_graph/13.


/*** interface ****************************************************/


/* stock_state_to_chart(Modes,Company,State,Chart) <-
      */

stock_state_to_chart(Modes,Company,State,Chart) :-
   writeln(user, stock_state_to_chart(Modes,Company)),
   stock_state_adapt_updates(Company,State,State_2),
   stock_state_to_graph(Modes,Company,State_2,
      Company_Nice,Color,Diameter,
      X_Marks,Y_Marks,X_Lines,Y_Lines,Time_Values_N),
   ( ( stock_name_to_number(Company,Wkn),
       name_append([Company_Nice,'  ( ',Wkn,' )'],Label) )
   ; Label = Company_Nice ),
   Chart =
      [ Label, Color, Diameter,
        X_Marks,Y_Marks, X_Lines,Y_Lines, Time_Values_N ].

 
stock_state_to_avg_chart(Modes,Company,State,Smooth,Chart) :-
   stock_state_to_day_line(Modes,Company,State,
      Smooth, Time_Values_Avg ),
   dislog_variable_get(avg_line_colour,Colour_2),
   ( member(Colour_2,[black,white]),
     Colour = colour(Colour_2)
   ; Colour = Colour_2 ),
   Number is 2 * Smooth,
   name_append(
      ['         + ',Number,' days line'],Number_of_Days),
%  computer_number_of_days_label(Company,Smooth,Number_of_Days),
   Chart =
      [ Number_of_Days, Colour, 2,
        [],[],[],[], Time_Values_Avg ].

computer_number_of_days_label(Company,Smooth,Number_of_Days) :-
   name_to_nice_name_special(Company,Company_Nice),
   name(Company_Nice,List),
   length(List,N),
   name_multiply(N,'   ',Blanks),
   Number is 2 * Smooth,
   name_append([Blanks,' (',Number,' days)'],Number_of_Days).

name_multiply(0,_,'') :-
   !.
name_multiply(N,Name_1,Name_2) :-
   M is N - 1,
   name_multiply(M,Name_1,Name_3),
   name_append(Name_1,Name_3,Name_2).


/* stock_state_to_chart(
         Modes,N,Company,State,XY_Labels,Chart) <-
      */

stock_state_to_chart(Modes,N,Company,State,XY_Labels,Chart) :-
   dislog_variable_get(curve_type,values),
   !,
   stock_state_to_graph(Modes,N,Company,State,
      Company_Nice,XY_Labels,Color,Diameter,
      X_Marks,Y_Marks,X_Lines,Y_Lines,Time_Values_N),
   Chart =
      [ Company_Nice,Color,Diameter,
        X_Marks,Y_Marks,X_Lines,Y_Lines,Time_Values_N ].
stock_state_to_chart(Modes,N,Company,State,XY_Labels,Chart) :-
   dislog_variable_get(curve_type,percents),
   !,
   stock_state_to_graph_mean(Modes,N,Company,State,1.0,
      Company_Nice,XY_Labels,Color,Diameter,
      X_Marks,Y_Marks,X_Lines,Y_Lines,Time_Values_N),
   Chart =
      [ Company_Nice,Color,Diameter,
        X_Marks,Y_Marks,X_Lines,Y_Lines,Time_Values_N ].


/* stock_state_to_day_line( [regular,D_Mode],
         Company, State, Smooth, Time_Values_Avg ) <-
      */

stock_state_to_day_line([regular,D_Mode],Company,State,
      Smooth, Time_Values_Avg ) :-
   stock_state_to_time_values(State,Time_Values),
   stock_print_parameters(Company,
      [_Color,_Diameter,Origin_X,Schrink_X]),
   maplist( select_second_of_two,
      Time_Values, Values ),
   stock_chart_compute_100_percent_value(Company,Values,Mean),
   stock_state_sort_by_date(State,State_1),
   stock_state_smooth(Smooth,State_1,State_2),
   stock_state_to_time_values(State_2,Time_Values_2),
   maplist( shift_time_value(Smooth),
      Time_Values_2, Time_Values_3 ),
   stock_chart_compute_y_origin_and_schrink(
      Mean,Origin_Y,Schrink_Y),
   maplist( value_normalize_2(linear-D_Mode,
      Origin_X-Origin_Y,Schrink_X-Schrink_Y),
      Time_Values_3, Time_Values_Avg ).
   
shift_time_value(Shift,T1-V,T2-V) :-
   T2 is T1 + Shift.
   

stock_state_to_graph([regular,D_Mode],Company,State,
      Company_Nice,Color,Diameter,
      X_Marks,Y_Marks,X_Lines,Y_Lines,Time_Values_N) :-
   stock_state_to_time_values(State,Time_Values),
   stock_print_parameters(Company,
      [Color,Diameter,Origin_X,Schrink_X]),
   maplist( select_second_of_two,
      Time_Values, Values ),
   stock_chart_compute_100_percent_value(Company,Values,Mean),
   stock_chart_compute_y_origin_and_schrink(Mean,Origin_Y,Schrink_Y),
   stock_chart_compute_x_marks(Origin_X,Schrink_X,X_Marks),
   stock_chart_compute_y_marks(regular_1,D_Mode,
      Mean,Origin_Y,Schrink_Y,Y_Marks),
%  value_normalize(D_Mode,Origin_Y,Schrink_Y,Mean,Y_Mean),
   dislog_variable_get(x_lines,Percents),
   vector_multiply(Mean,Percents,Ys),
   maplist( value_normalize(D_Mode,Origin_Y,Schrink_Y),
      Ys, X_Lines ),
%  Mean_200 is 2 * Mean,
%  value_normalize(D_Mode,Origin_Y,Schrink_Y,Mean_200,Y_200),
%  X_Lines = [Y_Mean,Y_200],
   year_beginning_time_stamps(
      Origin_X, Schrink_X, Y_Lines ),
%  Y_Lines = [],
   maplist( value_normalize_2(linear-D_Mode,
      Origin_X-Origin_Y,Schrink_X-Schrink_Y),
      Time_Values, Time_Values_N ),
   name_to_nice_name_special(Company,Company_Nice).


stock_state_to_graph_mean([regular,D_Mode],N,Company,State,
      Mean,
      Company_Nice,XY_Labels,Color,Diameter,
      X_Marks,Y_Marks,X_Lines,Y_Lines,Time_Values_N) :-
   stock_state_to_time_values(State,Time_Values),
   stock_state_to_graph_sub([regular,D_Mode],N,Company,
      Time_Values,Mean,
      Company_Nice,XY_Labels,Color,Diameter,
      X_Marks,Y_Marks,X_Lines,Y_Lines,Time_Values_N).

stock_state_to_graph([regular,D_Mode],N,Company,State,
      Company_Nice,XY_Labels,Color,Diameter,
      X_Marks,Y_Marks,X_Lines,Y_Lines,Time_Values_N) :-
   stock_state_to_time_values(State,Time_Values),
   maplist( select_second_of_two,
      Time_Values, Values ),
   stock_chart_compute_100_percent_value(Company,Values,Mean),
   stock_state_to_graph_sub([regular,D_Mode],N,Company,
      Time_Values,Mean,
      Company_Nice,XY_Labels,Color,Diameter,
      X_Marks,Y_Marks,X_Lines,Y_Lines,Time_Values_N).

stock_state_to_graph_sub([regular,D_Mode],N,Company,
      Time_Values,Mean,
      Company_Nice,XY_Labels,Color,Diameter,
      X_Marks,Y_Marks,X_Lines,Y_Lines,Time_Values_N) :-
   stock_item_to_color(Company,Color),
   stock_print_parameters(Company,
      [_Color,Diameter,Origin_X,Schrink_X]),
   stock_chart_compute_y_origin_and_schrink(Mean,Origin_Y,Schrink_Y),
   stock_chart_compute_x_marks(Origin_X,Schrink_X,X_Marks),
   stock_chart_compute_y_marks(regular,D_Mode,
      Mean,Origin_Y,Schrink_Y,Y_Marks),
   dislog_variable_get(x_lines,Percents),
   vector_multiply(Mean,[2.0,2.5|Percents],Y_Values),
   maplist( value_normalize(D_Mode,Origin_Y,Schrink_Y),
      Y_Values, X_Lines ),
   year_beginning_time_stamps(
      Origin_X, Schrink_X, Y_Lines ),
%  Y_Lines = [],
   maplist( value_normalize_2(linear-D_Mode,
      Origin_X-Origin_Y,Schrink_X-Schrink_Y),
      Time_Values, Time_Values_N ),
   name_to_nice_name_special(Company,Company_Nice),
   stock_chart_compute_x_y_labels(N,XY_Labels).


stock_state_to_graph([weighted,D_Mode],N,Company,State,
      Company_Nice,XY_Labels,Color,Diameter,
      X_Marks,Y_Marks,X_Lines,Y_Lines,Time_Values_N) :-
   stock_item_to_color(Company,Color),
   stock_state_to_time_values(State,Time_Values),
   stock_print_parameters(Company,
      [_Color,Diameter,Origin_X,Schrink_X]),
   Origin_Y = -100, Schrink_Y = 60,
   stock_chart_compute_x_marks(Origin_X,Schrink_X,X_Marks),
   stock_chart_compute_y_marks(weighted,D_Mode,
      50,Origin_Y,Schrink_Y,Y_Marks),
   maplist( value_normalize_2(linear-D_Mode,
      Origin_X-Origin_Y,Schrink_X-Schrink_Y),
      Time_Values, Time_Values_N ),
   X_Lines = [], Y_Lines = [],
   name_to_nice_name(Company,Company_Nice),
   stock_chart_compute_x_y_labels(N,XY_Labels).


/*** implementation ***********************************************/


stock_chart_compute_100_percent_value(_,Values,Value) :-
   dislog_variable_get(portfolio,no),
   !,
   mean_value_save(Values,Value).

stock_chart_compute_100_percent_value(Company,_,Value) :-
   dislog_variable_get(portfolio,Pairs),
   member(Company-Value,Pairs),
   write_list(user,[Company,': ',Value]), writeln(user,' ').


stock_chart_compute_x_y_labels(N,[X_Label,Y_Label]) :-
   M is ( N + 2 ) mod 3,
   K is ( N - 1 - ( ( N - 1 ) mod 3 ) ) / 3,
   X_Label is 7 * M,
   Y_Label is - 2 - 0.7 * K.


stock_chart_compute_y_marks(regular_1,D_Mode,
      Mean,Origin_Y,Schrink_Y,Y_Marks) :-
   dsa_variable(percents,Percents),
   vector_multiply(Mean,Percents,Rates),
   maplist( value_normalize(D_Mode,Origin_Y,Schrink_Y),
      Rates, Rates_N ),
   values_dm_to_euro(Rates,Rates_Adapted),
   pair_lists(Rates_N,Rates_Adapted,Y_Marks).
stock_chart_compute_y_marks(regular,D_Mode,
      Mean,Origin_Y,Schrink_Y,Y_Marks) :-
   dsa_variable(percents,Percents),
   vector_multiply(Mean,Percents,Rates),
   vector_multiply(100,Percents,Percents_100),
   maplist( value_normalize(D_Mode,Origin_Y,Schrink_Y),
      Rates, Rates_N ),
   pair_lists(Rates_N,Percents_100,Y_Marks).

stock_chart_compute_y_marks(weighted,D_Mode,
      Mean,Origin_Y,Schrink_Y,Y_Marks) :-
   equidistant_partition([-2,8],1,Percents),
%  equidistant_partition([-2,8],0.4,Percents),
%  equidistant_partition([0.2,3.8],0.4,Percents),
   vector_multiply(Mean,Percents,Rates),
   maplist( value_normalize(D_Mode,Origin_Y,Schrink_Y),
      Rates, Rates_N ),
   pair_lists(Rates_N,Rates,Y_Marks).

stock_chart_compute_x_marks(Origin_X,Schrink_X,X_Marks) :-
   dsa_variable(dates,Dates),
   maplist( date_to_time_stamp,
      Dates, Timestamps ),
   maplist( value_normalize(linear,Origin_X,Schrink_X),
      Timestamps, Timestamps_N ),
   dsa_variable(dates_to_print,Dates_P),
   pair_lists(Timestamps_N,Dates_P,X_Marks).

year_beginning_time_stamps(
      Origin_X,Schrink_X,Timestamps_N) :-
   dsa_variable(dates,Dates),
   sublist( year_beginning_date,
      Dates, Ds ),
   maplist( date_to_time_stamp,
      Ds, Timestamps ),
   maplist( value_normalize(linear,Origin_X,Schrink_X),
      Timestamps, Timestamps_N ).

year_beginning_date(1-1-_).


stock_chart_compute_y_origin_and_schrink(Mean,O,S) :-
   dsa_variable(y_layout,[F,D]),
   O is F * Mean,
%  S is Mean / D.
   S is 0.3 * ( Mean / D ).


values_dm_to_euro(Values_DM,Values_Euro) :-
   dislog_variable_get(euro_to_dm,Rate),
   vector_multiply(1/Rate,Values_DM,Values_Euro).

value_dm_to_euro(Value_DM,Value_Euro) :-
   dislog_variable_get(euro_to_dm,Rate),
   Value_Euro is Value_DM / Rate.

   
stock_state_adapt_updates(Company,State_1,State_2) :-
   stock_multiply_tuple(Company,_,_,F),
   !,
   M is 1 / F,
   stock_state_multiply(M,State_1,State_2).
stock_state_adapt_updates(_,State,State).
   

/******************************************************************/


