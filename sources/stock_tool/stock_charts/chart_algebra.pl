

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  XPCE - XML Charts for DAX Shares     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* stock_xml_file_transform(Range, File_1, File_2) <-
      */

stock_xml_file_transform(Range, File_1, File_2) :-
   xml_file_to_fn_term(File_1, Charts_1),
   Charts_A := Charts_1^charts,
   maplist( stock_chart_stretch(Range),
      Charts_A, Charts_B ),
   Charts_2 = charts:[]:Charts_B,
   fn_term_to_xml_file(Charts_2, File_2).


/* stock_chart_stretch(Range, Chart_1, Chart_2) <-
      */

stock_chart_stretch(Range, Chart_1, Chart_2) :-
   first(Range,Min),
   last_element(Range,Max),
%  writeln(user,'*** p ***'),
%  xml_chart_to_mean_value(Chart_1, Mean),
%  writeln(user,'*** p ***'),
%  writeln(user,mean(Mean)),
   Values_1 := [Chart_1]^chart^values,
%  writeln(Chart_1),
%  writeln(Values_1),
%  writeln('*** a ***'),
   maplist( stock_chart_point_stretch([Min, Max]),
      Values_1, Values_2 ),
%  Y_Marks_1 := [Chart_1]^chart^coordinate_system^y_marks,
%  maplist( stock_chart_y_mark_stretch([Min, Max]),
%     Y_Marks_1, Y_Marks_2 ),
   xml_chart_to_y_marks(Chart_1, Range, Y_Marks_2),
%  X_Lines_1 := [Chart_1]^chart^coordinate_system^horizontal_lines,
%  maplist( stock_chart_horizontal_line_stretch([Min, Max]),
%     X_Lines_1, X_Lines_2 ),
   xml_chart_to_horizontal_lines(Chart_1, Range, X_Lines_2),
%  writeln('*** a ***'),
%  writeln(Values_2),
   [Chart_2] := [Chart_1]*[
      chart^values:Values_2,
      chart^coordinate_system^y_marks:Y_Marks_2,
      chart^coordinate_system^horizontal_lines:X_Lines_2 ].
%  writeln(Chart_2).

stock_chart_point_stretch([Min, Max],
      point:[x:X, y:Y1]:[], point:[x:X, y:Y2]:[]) :-
   y_atom_stretch([Min, Max], Y1, Y2).
   
stock_chart_y_mark_stretch([Min, Max],
      mark:[y:Y1, l:L]:[], mark:[y:Y2, l:L]:[]) :-
   y_atom_stretch([Min, Max], Y1, Y2).

stock_chart_horizontal_line_stretch([Min, Max],
      line:[y:Y1]:[], line:[y:Y2]:[]) :-
   y_atom_stretch([Min, Max], Y1, Y2).

y_atom_stretch([Min, Max], Y1, Y2) :-
   term_to_atom(Ya,Y1),
   Y is 50 + Ya / 8 * 100,
   Yb is 16 * (Y - Min * 100) / ( (Max - Min) * 100 ),
   term_to_atom(Yb,Y2).


/* xml_chart_to_mean_value(Chart, Mean) <-
      */

xml_chart_to_mean_value(Chart, Mean) :-
   Mark := [Chart]^chart^coordinate_system^y_marks@mark,
%  writeln(user,'*** p ***'),
   '4' := Mark^y,
   !,
   Mean_a := Mark^l,
   term_to_atom(Mean, Mean_a).
xml_chart_to_mean_value(_, '--').


/* xml_chart_to_horizontal_lines(Chart, Range, X_Lines) <-
      */

xml_chart_to_horizontal_lines(Chart, Range, X_Lines) :-
   xml_chart_to_mean_value(Chart, Mean),
   ( ( Mean = '--', X_Lines = [] )
   ; ( first(Range,Min),
       last_element(Range,Max),
       maplist( value_to_horizontal_line([Min, Max]),
          Range, X_Lines ) ) ).

value_to_horizontal_line([Min, Max], Value, line:[y:Y]:[]) :-
   Yt is 16 * (Value - Min) / (Max - Min),
   term_to_atom(Yt,Y).


/* xml_chart_to_y_marks(Chart, Range, Y_Marks) <-
      */

xml_chart_to_y_marks(Chart, Range, Y_Marks) :-
   xml_chart_to_mean_value(Chart, Mean),
   ( ( Mean = '--', Y_Marks = [] )
   ; ( first(Range,Min),
       last_element(Range,Max),
       maplist( value_to_y_mark(Mean, [Min, Max]),
          Range, Y_Marks ) ) ).

value_to_y_mark(Mean, [Min, Max], Value, mark:[y:Y, l:L]:[]) :-
   Yt is 16 * (Value - Min) / (Max - Min),
   Lt is Value * Mean,
   term_to_atom(Yt,Y),
   term_to_atom(Lt,L).


% stock_arrow_stretch([Min_Mean, Max_Mean],
%       [stock(A,B,C,Value_1,E)], [stock(A,B,C,Value_2,E)]) :-
%    Yt is 16 * (Value - Min_Mean) / (Max_Mean - Min_Mean),
%    term_to_atom(Yt,Y),
%    term_to_atom(Lt,L).

time_value_stretch([Min, Max], X-Y1, X-Y2) :-
   Y is 50 + Y1 / 8 * 100,
   Y2 is 16 * (Y - Min * 100) / ( (Max - Min) * 100 ).

time_vectors_stretch([A,B|Time_Values_1],[C,D|Time_Values_2]) :-
   time_vector_stretch([A,B],[C,D]),
   time_vectors_stretch(Time_Values_1,Time_Values_2).
time_vectors_stretch([],[]).

time_vector_stretch([X1-Y1, X2-Y2], [X1-Y1, X2-Y3]) :-
   Y3 is Y1 + ( Y2 - Y1 ) / 3.

 
/******************************************************************/


