

/******************************************************************/
/***                                                            ***/
/***       STOCK Mining Tools: XPCE Interface for Gnuplot       ***/
/***                                                            ***/
/***       by Adrian Pillo                                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


stock_state_to_gnuplot([regular,D_Mode],Company,State) :-
   stock_state_to_gnuplot_2(D_Mode,Company,State).
stock_state_to_gnuplot([weighted,D_Mode],Company,State) :-
   stock_state_weight_and_transform(State,State_2),
   stock_state_to_gnuplot_2(D_Mode,Company,State_2).
 

stock_state_to_gnuplot_2(D_Mode,Company,State) :-
   reset_stock_mining_gnuplot_files,
   put_stock_value_to_gnuplot_file(State),
   which_gnuplot_ppm_file(Pic_File,_),
   write_ln(Pic_File),
   name_to_nice_name(Company,Company_Name),
   dsa_put_param_to_gnuplot_file(
      D_Mode,[Company_Name],Pic_File),
   stock_mining(mining_gnuplot_batch_file,File),
   name_append(['gnuplot',' ',File,'> /dev/null'],
      Command),
   shell(Command),
   display_gnuplot_bitmap,
   !.


/* reset_stock_mining_gnuplot_files <-
      overwrite files with the actual
      stock tool time stamp */

reset_stock_mining_gnuplot_files :-
   stock_mining(mining_gnuplot_values_file,File1),
   stock_mining(mining_gnuplot_batch_file,File2),
   dislog_time(Time,_),
   term_to_atom(Time,Time_Atom),
   concat('# stock tool time stamp: ',Time_Atom,String),
   write_new_line_to_file(String,File1),
   write_new_line_to_file(String,File2).


write_in_gnuplot_file(String) :-
   stock_mining(mining_gnuplot_values_file,File),
   telling(Old),
   append(File),
   write_ln(String),
   told,
   tell(Old).


put_stock_value_to_gnuplot_file([[stock(_,_,D,V,_)]|States]) :-
   convert_date_for_gnuplot(D,Gnu_D), 
   term_to_atom(Gnu_D,Gnu_D_A),
   string_concat(Gnu_D_A,' ',Temp),   
   string_concat(Temp,V,String),
   write_in_gnuplot_file(String),
   put_stock_value_to_gnuplot_file(States).
put_stock_value_to_gnuplot_file([]).  
   

/* still in alpha test phase  */

put_stock_compare2_value_to_gnuplot_file([C1|Cs1],[C2|Cs2]) :-
   C1 = [stock(_,_,D1,V1,_)],
   C2 = [stock(_,_,_,V2,_)],
   convert_date_for_gnuplot(D1,Gnu_D),
   term_to_atom(Gnu_D,Gnu_D_A),
   term_to_atom(V1,V1a),
   term_to_atom(V2,V2a),
   name_append([Gnu_D_A,' ',V1a,' ',V2a],String),
   write_in_gnuplot_file(String),
   put_stock_compare2_value_to_gnuplot_file(Cs1,Cs2).
put_stock_compare2_value_to_gnuplot_file(_,_).
% put_stock_compare2_value_to_gnuplot_file([],[]). 

convert_date_for_gnuplot(T-M-J,T/M/J).


/* still in alpha test phase  */

compare_stock(Comp1,Comp2) :-
   dislog_variable_get(chart_mode,regular),
   stock_values_sorted(Comp1,State1),
   stock_values_sorted(Comp2,State2),
   put_stock_compare2_value_to_gnuplot_file(State1,State2),
   stock_mining(mining_gnuplot_pic_file,Pic_File),
   dsa_put_param_to_gnuplot_file(logarithmic,
      [Comp1,Comp2],Pic_File),
   stock_mining(mining_gnuplot_batch_file,Batch_File),
   call_gnuplot_for_stock_file(Batch_File).
   

/* still in alpha test phase  */

compare_and_display(Comp1,Comp2) :-
   compare_stock(Comp1,Comp2),
   stock_mining(mining_gnuplot_pic_file,Pic_File),
   stock_file(dir(ppm),Pic_File,Bitmap),
   display_bitmap(ppm,Bitmap,0,0).


/* still in alpha test phase  */

dsa_put_param_to_gnuplot_file(D_Mode,Companies,Pic_File) :-
   stock_mining(mining_gnuplot_batch_file,File),
   dsa_put_param_to_gnuplot_file(D_Mode,Companies,Pic_File,File),
   !.


dsa_put_param_to_gnuplot_file(D_Mode,Companies,Pic_File,File) :-
   dislog_variable_get(chart_mode,Chart_Mode),
   dsa_plot_output_title(Chart_Mode,Companies,Plot_Output_Title),
   dsa_plot_command_lines(Companies,Plot_Command_Lines),
   dsa_plot_output_line(Pic_File,Plot_Output_Line),
   process_line_list_to_file(D_Mode, Plot_Output_Title,
      Plot_Output_Line, Plot_Command_Lines, File).


dsa_plot_output_title(Chart_Mode,Companies,
      Plot_Output_Title) :-
   dsa_companies_to_companies_string(Companies,Companies_String),
   name_append(
      ['set title "Stock Analysis (',Chart_Mode,')',
       ' for ',Companies_String,'\nby D. Seipel and A. Pillo"'],
      Plot_Output_Title).

dsa_companies_to_companies_string([Company_1,Company_2],String) :-
   name_append([Company_1,' and ',Company_2],String).
dsa_companies_to_companies_string([Company],Company).


dsa_plot_command_lines([Company_1,Company_2],
      [Plot_Command_Line_1,Plot_Command_Line_2]) :-
   dsa_plot_command_line('plot ',Company_1,
      '1:2:(1.0)','lt 7 pt 4 \\',Plot_Command_Line_1),
   dsa_plot_command_line(', ',Company_2,
      '1:3:(1.0)','lt 3 pt 4 ',Plot_Command_Line_2).
dsa_plot_command_lines([Company],
      [Plot_Command_Line]) :-
   dsa_plot_command_line('plot ',Company,
      '1:2:(1.0)','lt 3 pt 1',Plot_Command_Line).

dsa_plot_command_line(Prefix,Company,String_1,String_2,
      Plot_Command_Line) :-
   stock_file(dir(ppm),'stock.dat',File),
   name_append(
      [Prefix,'''',File,'''',
       ' using ',String_1,' smooth acsplines t ', '''',Company,'''',
       ' with linesp ',String_2],
      Plot_Command_Line).

dsa_plot_output_line(Pic_File,Plot_Output_Line) :-
   stock_file(dir(ppm),Pic_File,File),
   name_append([
      'set output ',
      '''',File,''''],Plot_Output_Line).


process_line_list_to_file(D_Mode, Plot_Output_Title,
      Plot_Output_Line, Plot_Command_Lines, File) :-
   plot_command_setting(D_Mode,Setting),
   append([ [Plot_Output_Title], Setting,
      [Plot_Output_Line], Plot_Command_Lines ], Line_List),
   process_line_list_to_file(D_Mode,Line_List,File).
   
process_line_list_to_file(linear,Line_List,File) :-
   append_line_list_to_file(Line_List,File).
process_line_list_to_file(logarithmic,Line_List,File) :-
   write_line_list_to_file(Line_List,File).


plot_command_setting(D_Mode,Setting) :-
   plot_command_setting(D_Mode,l1,Setting_1),
   plot_command_setting(D_Mode,l2,Setting_2),
   plot_command_setting(D_Mode,l3,Setting_3),
   append([Setting_1,Setting_2,Setting_3],Setting).

plot_command_setting(_,l1,[
   'set xlabel "Date"',
   'set timefmt "%d/%m/%y"',
   'set ylabel "Stock Value\nDM"',
   'set xdata time',
   'set xrange [ : ]',                 % automatic range
   'set format x "%d/%m/%y"',
   'set yrange [ : ]' ]).              % automatic range
plot_command_setting(linear,l2,[]).
plot_command_setting(logarithmic,l2,[
   'set logscale y 2' ]).              % base 2 or 10
plot_command_setting(_,l3,[
   'set grid',
   'set terminal pbm small color' ]).


display_gnuplot_bitmap :-
   dislog_variable_get(act_ppm_file,N),
   number_to_ppm_file(N,File),
   display_bitmap(ppm,File,0,0).

which_gnuplot_ppm_file(File,File_Fullpath) :-
   dislog_variable_get(max_ppm_file,N),
   number_to_ppm_file(N,File,File_Fullpath),
   dislog_variable_set(act_ppm_file,N),
   M is N + 1,
   dislog_variable_set(max_ppm_file,M).


/******************************************************************/

