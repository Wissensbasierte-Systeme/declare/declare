

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  XPCE - XML Charts for DAX Shares     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xml_select_name_to_number(Item, Wkn) <-
      */

xml_select_name_to_number(Item, Wkn) :-
   stock_file(data,Directory),
   name_append(Directory, '/xml/number_name.xml', Path),
   xml_file_to_fn_term(Path, FN),
   As := FN^number_name@entry,
   Item := As^name,
   Wkn := As^wkn.


/* stock_number_name_to_xml_file <-
      */

stock_number_name_to_xml_file :-
   mysql_select_number_name(State),
   maplist( number_name_fact_to_number_name_entry,
      State, Entries ),
   stock_file(data,Directory),
   name_append(Directory, '/xml/number_name.xml', Path),
   fn_term_to_xml_file(number_name:[]:Entries, Path).

number_name_fact_to_number_name_entry(
      [number_name(Wkn,Name)],
      entry:[wkn:Wkn, name:Name]:[]) .


/* stock_charts_to_xml_files <-
      */

stock_charts_to_xml_files :-
   mysql_select_number_name(State),
   checklist_with_status_bar(
      'Compiling XML-Files for Charts ...',
      stock_chart_to_xml_file,
      State ).

stock_chart_to_xml_file([number_name(Wkn,Item)]) :-
   stock_file(data,Directory),
   name_append([Directory,'/xml/',Wkn,'.xml'],Path),
   stock_chart_to_xml_file(Item,Path).


/* stock_chart_file_to_xpce(File) <-
      */

stock_chart_file_to_xpce(File) :-
   file_exists(File),
   !,
   dislog_stock_advisor(clear_display_window),
   send_xpce_coordinate_system_box,
   stock_xml_file_to_charts(File,Charts_1),
%  writeln(user,'*** A ***'),
   maplist( stock_chart_thin,
      Charts_1, Charts_2 ),
%  writeln(user,'*** B ***'),
%  writeln(user,stock_chart_to_xpce(Y_Unit,XY_Labels,Chart_3)),
   checklist( stock_chart_to_xpce_2,
      Charts_2 ),
%  writeln(user,'*** C ***'),
   stock_chart_compute_x_y_labels(4,XY_Labels_Avg),
   remove_xy_labels_box(XY_Labels_Avg).
stock_chart_file_to_xpce(_).
      
stock_chart_to_xpce_2([Y_Unit,XY_Labels,Chart]) :-
   stock_chart_to_xpce(Y_Unit,XY_Labels,Chart).

stock_chart_thin(
      [Y_Unit,XY_Label,Chart_1],
      [Y_Unit,XY_Label,Chart_2] ) :-
   nth(8,Chart_1,Points_1),
   delete(Chart_1,Points_1,Chart_3),
   dislog_variable_get(stock_chart_thin,Thin),
   list_thin(Thin,Points_1,Points_2),
   append(Chart_3,[Points_2],Chart_2).


/* stock_chart_to_xml_file(Item,File) <-
      */

stock_chart_to_xml_file(Item,File) :-
   dislog_variable_get(curve_type,values),
   !,
   writeln(user,stock_chart(share,Item)),
   stock_values(Item,State),
   sublist( stock_fact_value_is_not_zero,
      State, State_2 ),
   stock_state_smooth(State_2,State_Smooth),
   dislog_variables_get([
      chart_mode-C_Mode,
      y_axis_mode-D_Mode ]),
   stock_state_to_chart_for_xpce(
      [C_Mode,D_Mode], Item, State_Smooth,
      Y_Unit, XY_Labels, Chart),
   dislog_variable_get(smooth_parameter_avg,Smooth),
   stock_state_to_avg_chart(
      [C_Mode,D_Mode], Item, State_Smooth, Smooth,
      Chart_Avg),
   stock_chart_compute_x_y_labels(4,XY_Labels_Avg),
   write('---> '), writeln(File),
   stock_charts_to_xml_file([
      [Y_Unit,XY_Labels,Chart],
      ['',XY_Labels_Avg,Chart_Avg] ],
      File),
   !.


/*** implementation ***********************************************/


/* stock_xml_file_to_charts(File,Charts) <-
      */

stock_xml_file_to_charts(File,Charts) :-
   xml_file_to_fn_term(File,[charts:[]:FN_Terms]),
   maplist( stock_fn_term_to_chart,
      FN_Terms, Charts ).


/* stock_charts_to_xml_file(Charts,File) <-
      */

stock_charts_to_xml_file(Charts,File) :-
   maplist( stock_chart_to_fn_term,
      Charts, FN_Terms ),
   fn_term_to_xml_file(charts:FN_Terms,File).


/* stock_chart_to_fn_term([Y_Unit,XY_Labels,Chart],FN) <-
      */

stock_chart_to_fn_term([Y_Unit,XY_Labels,Chart],FN) :-
%  writeln(user,'*** a ***'),
   Chart = [ Label, Colour, Diameter,
      X_Marks,Y_Marks, X_Lines,Y_Lines, Time_Values_N ],
   FN = chart:[
      diameter:Diameter, y_unit:Y_Unit]:[
      labels:[label:Label, x:X_Label, y:Y_Label]:[],
      Colour_Element,
      coordinate_system:[]:[
         x_marks:[]:X_Marks_3,
         y_marks:[]:Y_Marks_3,
         horizontal_lines:[]:X_Lines_2,
         vertical_lines:[]:Y_Lines_2 ],
      values:[]:Time_Values_N_2 ],
%  writeln(user,'*** b ***'),
   colour_to_colour_element(Colour, Colour_Element),
%  writeln(user,'*** b2 ***'),
   XY_Labels = [X_Label, Y_Label],
   maplist( pair_lists_3([x,label]),
      X_Marks, X_Marks_2 ),
%  writeln(user,X_Marks-X_Marks-X_Marks_2),
%  writeln(user,'*** c ***'), !,
   maplist( label_with(mark),
      X_Marks_2, X_Marks_3 ),
   maplist( pair_lists_3([y,l]),
      Y_Marks, Y_Marks_2 ),
   maplist( label_with(mark),
      Y_Marks_2, Y_Marks_3 ),
   maplist( label_with_2(line,y),
      X_Lines, X_Lines_2 ),
   maplist( label_with_2(line,x),
      Y_Lines, Y_Lines_2 ),
   maplist( time_value_pair_to_point,
      Time_Values_N, Time_Values_N_2 ),
%  writeln(user,'*** d ***'),
   !.


/* stock_fn_term_to_chart(FN,[Y_Unit,XY_Labels,Chart]) <-
      */

stock_fn_term_to_chart(FN,[Y_Unit,XY_Labels,Chart]) :-
%  writeln(user,'*** a ***'),
   Chart = [ Label, Colour, Diameter_t,
      X_Marks,Y_Marks, X_Lines,Y_Lines, Time_Values_N ],
   Diameter_a_Y_Unit := [FN]@chart,
   [Diameter_a, Y_Unit] := Diameter_a_Y_Unit^[diameter, y_unit],
   FN_2 := [FN]^chart,
%  writeln(FN_2),
   Labels := FN_2@labels,
   [Label, X_Label_a, Y_Label_a] :=
      Labels^[label, x, y],
   Colour_Attributes := FN_2@colour,
   Colour_Element = colour:Colour_Attributes:[],
   Coordinate_System := FN_2^coordinate_system,
   [X_Marks_3, Y_Marks_3, X_Lines_2, Y_Lines_2] :=
      Coordinate_System^[
         x_marks, y_marks, horizontal_lines, vertical_lines],
   Time_Values_N_2 := FN_2^values,
%  FN = chart:[
%     diameter:Diameter_a, y_unit:Y_Unit]:[
%     labels:[label:Label, x:X_Label_a, y:Y_Label_a]:[],
%     Colour_Element,
%     coordinate_system:[]:[
%        x_marks:[]:X_Marks_3,
%        y_marks:[]:Y_Marks_3,
%        horizontal_lines:[]:X_Lines_2,
%        vertical_lines:[]:Y_Lines_2 ],
%     values:[]:Time_Values_N_2 ],
   term_to_atom(X_Label_t,X_Label_a),
   term_to_atom(Y_Label_t,Y_Label_a),
   term_to_atom(Diameter_t,Diameter_a),
%  writeln(user,'*** b ***'),
   colour_to_colour_element(Colour, Colour_Element),
%  writeln(user,'*** b2 ***'),
   XY_Labels = [X_Label_t, Y_Label_t],
   maplist( label_with(mark),
      X_Marks_2, X_Marks_3 ),
   maplist( pair_lists_3([x,label]),
      X_Marks, X_Marks_2 ),
%  writeln(user,X_Marks-X_Marks-X_Marks_2),
%  writeln(user,'*** c ***'), !,
   maplist( label_with(mark),
      Y_Marks_2, Y_Marks_3 ),
   maplist( pair_lists_3([y,l]),
      Y_Marks, Y_Marks_2 ),
   maplist( label_with_2(line,y),
      X_Lines, X_Lines_2 ),
   maplist( label_with_2(line,x),
      Y_Lines, Y_Lines_2 ),
   maplist( time_value_pair_to_point,
      Time_Values_N, Time_Values_N_2 ),
%  writeln(user,'*** d ***'),
   !.

label_with(Label,Xs,Label:Xs:[]).

label_with_2(Label_1,Label_2,X_t,Label_1:[Label_2:X_a]:[]) :-
   term_to_atom(X_t,X_a).

pair_lists_3([X1,X2],[Y1,Y2],[X1:Y3,X2:Y4]) :-
   term_to_atom(Y1,Y3),
   term_to_atom(Y2,Y4).

time_value_pair_to_point(X_t-Y_t,point:[x:X_a, y:Y_a]:[]) :-
   term_to_atom(X_t,X_a),
   term_to_atom(Y_t,Y_a).

colour_to_colour_element(
      colour(C),
      colour:[c:C]:[]).
colour_to_colour_element(
      colour(@default,R_t,G_t,B_t),
      colour:[r:R_a, g:G_a, b:B_a]:[]) :-
   term_to_atom(R_t,R_a),
   term_to_atom(G_t,G_a),
   term_to_atom(B_t,B_a).


/* list_thin(N,Xs,Ys) <-
      */

list_thin(1,Xs,Xs) :-
   !.
list_thin(N,Xs,Ys) :-
   list_thin(0,N,Xs,Ys).

list_thin(0,N,[X|Xs],[X|Ys]) :-
   !,
   list_thin(1,N,Xs,Ys).
list_thin(M,N,[_|Xs],Ys) :-
   K is (M + 1) mod N,
   list_thin(K,N,Xs,Ys).
list_thin(_,_,[],[]).


/******************************************************************/


