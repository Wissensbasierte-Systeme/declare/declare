

/******************************************************************/
/***                                                            ***/
/***        Stock Tool:  XPCE Charts                            ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


chart(Item) :-
   dislog_variable_get(chart_display_mode,gnuplot),
   dislog_variable_get(chart_display_mode_xpce,Mode),
   dislog_variable_set(xpce_stock,Item),
   draw_colons(Mode).


/*** implementation ***********************************************/


draw_colons(regular) :-
   draw_colons_common(Stock),
   stock_values_sorted(Stock,State),
   State = [[stock(_,_,_,Value,_)]|_],
   set_fitting_value_flag(Value),
   checklist( draw_colon(regular), State ).
draw_colons(percent) :-
   draw_colons_common(Stock),
   stock_time_sequence(Stock,Time_Sequence),
   checklist( draw_colon(percent), Time_Sequence ).

draw_colons_common(Stock) :-
   dislog_variable_get(xpce_stock, Stock),
   dislog_stock_advisor(open_visualisation_window),
   send(@ddd_p2, clear),
   send(@ddd_p2, display, new(_, text(Stock, left,
      font(helvetica,bold,24))), point(20,494)),
   dislog_flag_set(x_coord, 0).

draw_colon(regular,[stock(_,_,_,Value,_)]) :-
   fitting_value(regular,Value,Height),
   Round_Value is round(Value),
   draw_colon(Height,Round_Value,colour(green),500).
draw_colon(percent,[value(_,_,_,Value)]) :-
   fitting_value(percent,Value,Height),
   reduce_fractional_part2(Value,Value_Reduced),
   draw_colon(Height,Value_Reduced,colour(green),260).

draw_colon(Height,Text,Colour,Y_Ax_0) :-
   dislog_flag_get(x_coord,X),
   Begin = Y_Ax_0 - Height - 10,
   Minus_Value is 11,
%  oscilate_from_x_to_y(10,20,Minus_Value),
   (   Height >= 0
   ->  Begin_Text = Begin - Minus_Value
   ;   Begin_Text = Begin + 3 ),
   dislog_flag_get(colon_width,Width),
   Shadow_X is X + 4,
   Shadow_Begin is Begin + 3,
   Shadow_Width = Width,
   send(@ddd_p2,display,
      new(Shadow_Box,box(Shadow_Width,Height)),
      point(Shadow_X,Shadow_Begin)),
   send(Shadow_Box,fill_pattern,colour(red)),
   send(@ddd_p2,display,
      new(B,box(Width,Height)),point(X,Begin)),
   send(B,fill_pattern,Colour),
   send(@ddd_p2,display,
      new(_,text(Text,
      left,font(helvetica,bold,10))),point(X,Begin_Text)),
   New_X is X + Width + 10,
   dislog_flag_set(x_coord,New_X).

set_fitting_value_flag(Wert) :-
   Wert < 450,
   dislog_flag_set(fitting_stock_value,450),
   !.
set_fitting_value_flag(Wert) :-
   Wert < 950,
   dislog_flag_set(fitting_stock_value,950),
   !.
set_fitting_value_flag(_) :-
   dislog_flag_set(fitting_stock_value,other),
   !.

fitting_value(regular,X,X) :-
   dislog_flag_get(fitting_stock_value,450).
fitting_value(regular,X,Y) :-
   dislog_flag_get(fitting_stock_value,950),
   Y is X / 2.
fitting_value(regular,X,Y) :-
   dislog_flag_get(fitting_stock_value,other),
   Y is X / 5.
fitting_value(percent,X,Y) :-
   Y is X * 30.

reduce_fractional_part2(Value,Value_Reduced) :-
   FP is float_fractional_part(Value),
   IP is float_integer_part(Value),
   Value_Reduced is IP + round(FP*100)/100.


oscilate_from_x_to_y(X,_,X) :-
   dislog_variable_get(oscilator,0),
   dislog_variable_set(oscilator,1).
oscilate_from_x_to_y(_,Y,Y) :-
   dislog_variable_get(oscilator,1),
   dislog_variable_set(oscilator,0).


/******************************************************************/


