

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  XPCE - Charts                        ***/
/***                                                            ***/
/******************************************************************/


:- [library('plot/axis')].


:- dynamic
      stock_values_for_wkn_cached/2.


/*** interface ****************************************************/


/* stock_values_for_wkn(Wkn, Values) <-
      */

stock_values_for_wkn(Wkn, Values) :-
   stock_values_for_wkn_cached(Wkn, Values),
   !.
stock_values_for_wkn(Wkn, Values) :-
   Statement = [
      use:[stock],
      select:[value], from:[finanztreff],
      where:[wkn=Wkn] ],
   mysql_select_execute(Statement, Tuples),
   flatten(Tuples, Values),
   assert(stock_values_for_wkn_cached(Wkn, Values)).


/* stock_values_for_wkn_to_picture_with_axis(Wkn) <-
      */

stock_values_for_wkn_to_picture_with_axis(Wkn) :-
   stock_values_for_wkn(Wkn, Values),
   ACs = [20-blue, 100-orange],
   values_to_picture_with_axis(Wkn, Values, ACs, _).


/*** implementation ***********************************************/


/* picture_with_axis(Picture, Parameters) <-
      */

picture_with_axis_open(Picture, Parameters) :-
   picture_with_axis(Picture, Parameters),
   send(Picture, open).

picture_with_axis(Picture, Parameters) :-
   ( [X0, Y0] := Parameters/origin/content::'*'
   ; [X0, Y0] = [40,320] ),
   [X1, X2] := Parameters/x/content::'*',
   [Y1, Y2] := Parameters/y/content::'*',
   [X3, Y3] := Parameters/step/content::'*',
   ( [X4, Y4] := Parameters/size/content::'*'
   ; [X4, Y4] = [400, 300] ),
   new(Picture, picture),
   send(Picture, size, size(X4+80, Y4+50)),
   send(Picture, display,
      plot_axis(x, X1, X2, X3, X4, point(X0, Y0))),
   send(Picture, display,
      plot_axis(y, Y1, Y2, Y3, Y4, point(X0, Y0))),
   !.


/* values_to_picture_with_axis(Wkn, Values, ACs, Picture) <-
      */

values_to_picture_with_axis(Wkn, Values, ACs, Picture) :-
   length(Values, N),
   minimum(Values, Min), maximum(Values, Max),
   writeln(user, N-[Min-Max]),
   round((Max - Min) / 5, Y_Step),
   Parameters = parameters:[
      origin:[40, 320], x:[0, N], y:[Min, Max],
      step:[365, Y_Step], size:[400, 300]],
   new(Dialog, frame(Wkn)),
   picture_with_axis(Picture, Parameters),
   send(Dialog, append, Picture),
   send(Dialog, open),
   generate_interval(1, N, Interval),
   ( foreach(M-Colour, ACs) do
        numbers_to_floating_averages(M, Values, Values_M),
        pair_lists(-, Interval, Values_M, Points),
        maplist( transform_point_to_plot_point(Parameters),
           Points, Points_2 ),
        send_points(Picture, colour(Colour), 2, Points_2) ).

transform_point_to_plot_point(Parameters, A1-B1, A2-B2) :-
   [X0, Y0] := Parameters/origin/content::'*',
   [X1, X2] := Parameters/x/content::'*',
   [Y1, Y2] := Parameters/y/content::'*',
   [X4, Y4] := Parameters/size/content::'*',
   A2 is X0 + (A1 - X1) / (X2 - X1) * X4,
   B2 is Y0 - (B1 - Y1) / (Y2 - Y1) * Y4.


/*** tests ********************************************************/


test(picture_with_axis, log) :-
   Parameters =  parameters:[
      origin:[40,320], x:[0,5], y:[-3,3],
      step:[1,1], size:[400, 300] ],
   picture_with_axis(Picture, Parameters),
   generate_interval(2, 50, I),
   ( foreach(N, I), foreach(Point, Points) do
        Point = N/10-log(N/10) ),
   maplist( transform_point_to_plot_point(Parameters),
      Points, Points_2 ),
   send_points(Picture, colour(blue), 10, Points_2),
   send(Picture, open).


/******************************************************************/


