

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Charts for DAX Shares                ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
%     stock_item_to_color/2,
      dsa_display_cache/2.

:- multifile
      chart/1, chart/2, chart/4,
      stock_chart/2.


/*** interface ****************************************************/


/* chart ... <-
      */

chart(Item) :-
   stock_chart(share,Item).

charts(Items) :-
   stock_chart(shares,Items).

charts_portfolio(Name) :-
   stock_chart(portfolio,Name).

charts_group(Group) :-
   stock_chart(group,Group).   

charts_group :-
   dislog_variable_get(storage_mode,mysql),
   dislog_variable_switch(chart_start,Chart_Start,1997),
   dislog_variable_switch(smooth_parameter,Smooth_Parameter,10),
   !,
   findall( Group,
      stock_branch(Group,_), Groups ),
   stock_chart(all_groups_mysql,Groups),
   dislog_variable_set(smooth_parameter,Smooth_Parameter),
   dislog_variable_set(chart_start,Chart_Start).
charts_group :-
   findall( Group,
      stock_branch(Group,_), Groups ),
   stock_chart(shares,Groups).

charts_pool(Stock_Pool) :-
   stock_pool(Stock_Pool,Items),
   stock_chart(shares,Items).


/* stock_chart(spooled) <-
      */

stock_chart(spooled) :-
   dislog_variable_get(storage_mode,mysql),
   stock_spooled_values([_,_],State),
   mysql_select_max_date(Date_1),
   mysql_date_to_date(Date_1,Date_2),
   name_append(
      'Stock Values (per day) - Maximal Date: ',
      Date_2, Label ),
   stock_state_to_display(Label,State),
   xpce_display_data_volume_table,
   !.

stock_chart(spooled_aggregated) :-
   dislog_variable_get(storage_mode, mysql),
   stock_spooled_values([_, _], State_1),
   mysql_select_max_date(Date_1),
   mysql_date_to_date(Date_1, Date_2),
   name_append(
      'Stock Values (per day) - Maximal Date: ',
      Date_2, Label ),
   stock_state_aggregate_over_time(State_1, State_2),
   stock_state_smooth(State_2, State_3),
   stock_state_to_display(Label, State_3),
   xpce_display_data_volume_table,
   !.

stock_state_aggregate_over_time(State_1, State_2) :-
   stock_state_aggregate_over_time(0, State_1, State_2).

stock_state_aggregate_over_time(Sofar, [C1|Cs1], [C2|Cs2]) :-
   C1 = [stock(x, x, Date_1, Value_1, Previous)],
   Value_2 is Sofar + Value_1,
   C2 = [stock(x, x, Date_1, Value_2, Previous)],
   stock_state_aggregate_over_time(Value_2, Cs1, Cs2).
stock_state_aggregate_over_time(_, [], []).


/* stock_chart(Type,Item) <-
      Computes the chart of the item Item, and display it
      according to Share_Mode, CD_Mode, C_Mode, D_Mode.
       - Share_Mode = share, shares,
       - CD_Mode =
            xpce, postscript, gnuplot,
       - C_Mode =
            regular: the actual values are shown,
            weighted: the weighted values are shown,
       - D_Mode =
            linear: the chart is linear,
            logarithmic: the chart is logarithmic. */

stock_chart(share,Item) :-
   dislog_variable_get(storage_mode,xml),
%  mysql_select_name_to_number(Item,[[number_name(Wkn,_)]]),
   xml_select_name_to_number(Item,Wkn),
   stock_file(data,Directory),
   name_append([Directory,'/xml/',Wkn,'.xml'],Path),
   stock_chart_file_to_xpce(Path),
   !.
stock_chart(share,Item) :-
   dislog_variable_get(curve_type,values),
   !,
   writeln(user,stock_chart(share,Item)),
   stock_values(Item,State),
   sublist( stock_fact_value_is_not_zero,
      State, State_2 ),
   stock_state_smooth(State_2,State_Smooth),
%  stock_state_to_xpce_table(State_2),
%  stock_state_to_xpce_table(State_Smooth),
   stock_state_to_display(Item,State_Smooth),
   !.
stock_chart(share,Item) :-
   dislog_variable_get(curve_type,percents),
   !,
   writeln(user,stock_chart(share,Item)),
   stock_values(Item,State),
   stock_state_sort_by_date(State,State_1),
   stock_state_analyse(State_1,State_2),
   dislog_variable_switch(curve_style,Style,lines),
   stock_states_to_display([Item],[State_2]),
   dislog_variable_set(curve_style,Style),
   !.
stock_chart(share,Item) :-
   dislog_variable_get(curve_type,weights),
   !,
   writeln(user,stock_chart(share,Item)),
   mysql_select_by_name_with_previous(Item,State_1),
   stock_state_analyse_weights(State_1,State_2),
   stock_state_smooth(State_2,State_3),
   stock_state_sort_by_date(State_3,State_A),
   stock_state_round(2,State_A,State_B),
   stock_state_to_xpce_table_extended(State_B),
   dislog_variable_switch(curve_type,Type,percents),
%  dislog_variable_switch(curve_style,Style,lines),
   stock_states_to_display([Item],[State_3]),
%  dislog_variable_set(curve_style,Style),
   dislog_variable_set(curve_type,Type).
stock_chart(share,Item) :-
   dislog_variable_get(curve_type,values_abstract),
   !,
   writeln(user,stock_chart(share,Item)),
   stock_values(Item,State),
   sublist( stock_fact_value_is_not_zero,
      State, State_2 ),
   writeln(user,stock_state_smooth),
%  stock_state_smooth(3,State_2,State_3),
   stock_state_prune(State_2,State_3),
   writeln(user,stock_state_smooth),
   stock_state_to_mean_value(State_3,Mean_Value),
   writeln(user,stock_state_abstract),
   stock_state_to_xpce_table(State_3),
   stock_state_abstract(20,Mean_Value,State_3,State_4),
   writeln(user,stock_state_abstract),
   writeln(user,stock_state_sort_by_date),
   stock_state_sort_by_date(State_4,State_5),
   writeln(user,stock_state_sort_by_date),
   stock_state_to_xpce_table(State_5),
   dislog_variable_switch(curve_type,Type,values),
   stock_state_to_display(Item,State_5),
   dislog_variable_set(curve_type,Type),
   !.

stock_chart(all_groups_mysql,Groups) :-
   writeln(user,stock_chart(all_groups_mysql,Groups)),
   stock_charts_average(Groups,States),
   maplist_with_status_bar_dsa( stock_state_smooth,
      States, States_Smooth ),
   stock_states_to_display(Groups,States_Smooth).

stock_chart(shares,Items) :-
   writeln(user,stock_chart(shares,Items)),
%  dislog_variable_get(chart_mode,C_Mode),
%  stock_charts(C_Mode,Items,States),
   maplist_with_status_bar_dsa( stock_values,
      Items, States ),
   maplist_with_status_bar_dsa( stock_state_smooth,
      States, States_Smooth ),
   stock_states_to_display(Items,States_Smooth).

stock_chart(portfolio,Portfolio) :-
   dislog_variable_get(curve_type,values),
   !,
   writeln(user,stock_chart(portfolio,Portfolio)),
   stock_portfolio(Portfolio,Items,States,Pairs),
   maplist_with_status_bar_dsa( stock_state_smooth,
      States, States_Smooth ),
   dislog_variable_set(portfolio,Pairs),
   stock_states_to_display(Items,States_Smooth),
   dislog_variable_set(portfolio,no),
   !.
stock_chart(portfolio,Portfolio) :-
   dislog_variable_get(curve_type,percents),
   !,
   writeln(user,stock_chart(portfolio,Portfolio)),
   stock_portfolio(Portfolio,Items,States),
   maplist_with_status_bar_dsa( stock_state_analyse,
      States, States_2 ),
   stock_states_to_display(Items,States_2),
   !.

stock_chart(group,Group) :-
   dislog_variable_get(curve_type,values),
   writeln(user,stock_chart(group,Group)),
   stock_tuple_special(Group,Companies),
%  dislog_variable_get(chart_mode,C_Mode),
%  stock_charts(C_Mode,[Companies],States),
%  stock_states_to_display(Companies,States).
   ( ( dislog_variable_get(storage_mode,mysql),
       !,
       mysql_select_by_group(Group,State),
       dislog_variable_get(chart_mode,C_Mode),
       maplist( stock_state_process(C_Mode,State),
          Companies, States ),
       stock_states_to_display(Companies,States) )
   ; stock_chart(shares,Companies) ),
   !.
stock_chart(group,Group) :-
   dislog_variable_get(curve_type,percents),
   writeln(user,stock_chart(group,Group)),
   dislog_variable_get(storage_mode,mysql),
   mysql_select_by_group(Group,Names,States_1),
   maplist_with_status_bar_dsa( stock_state_sort_by_date,
      States_1, States_2 ),
   maplist_with_status_bar_dsa( stock_state_analyse,
      States_2, States_3 ),
   stock_chart_save_states_to_file(Group,States_3),
   !,
   dislog_variable_switch(curve_style,Style,lines),
   stock_states_to_display(Names,States_3),
   dislog_variable_set(curve_style,Style).
stock_chart(group,Group) :-
   dislog_variable_get(curve_type,weights),
   !,
   writeln(user,stock_chart(group,Group)),
   stock_branch(Group,Items),
   maplist_with_status_bar_dsa(
      mysql_select_by_name_with_previous,
      Items, States_1 ),
   maplist_with_status_bar_dsa( stock_state_analyse_weights,
      States_1, States_2 ),
   maplist_with_status_bar_dsa( stock_state_smooth,
      States_2, States_3 ),
   dislog_variable_switch(curve_type,Type,percents),
   stock_states_to_display(Items,States_3),
   dislog_variable_set(curve_type,Type).

stock_chart_save_states_to_file(Group,States) :-
   dislog_variable_get(home,Home),
   name_append([Home,'/results/Abstractions/',Group],File),
   stock_states_to_file(States,File).


/* stock_charts(Objects,States) <-
      */

stock_charts(Objects,States) :-
   dislog_variable_get(chart_mode,C_Mode),
   stock_charts(C_Mode,Objects,States).
 
stock_charts_average(Groups,States) :-
   dislog_variable_get(storage_mode,mysql),
   !,
   mysql_select_all_groups_average(State),
   !,
   maplist_with_status_bar_dsa(
      stock_state_select_and_add_previous(State),
      Groups, States ).
   
stock_state_select_and_add_previous(State_1,Company,State_2) :-
   stock_state_select([Company,_,_],State_1,State_A),
   stock_state_add_previous_values(long,month,State_A,State_B),
   stock_state_add_previous_values(long,year,State_B,State_2).


/* stock_charts(C_Mode,Groups,States) <-
      */

stock_charts(C_Mode,Groups,States) :-
   dislog_variable_get(storage_mode,mysql),
   !,
   maplist_with_status_bar_dsa( mysql_select_by_group,
      Groups, Group_States ),
   append(Group_States,State),
   !,
   maplist_with_status_bar_dsa( stock_state_process(C_Mode,State),
      Groups, States ).
stock_charts(C_Mode,Items,States) :-
   stock_consult(state,State),
   !,
   maplist_with_status_bar_dsa( stock_state_process(C_Mode,State),
      Items, States ).
 

/* stock_state_process(State_1,Item,State_2) <-
      */

stock_state_process(State_1,Item,State_2) :-
   dislog_variable_get(chart_mode,C_Mode),
   stock_state_process(C_Mode,State_1,Item,State_2).

 
/* stock_state_process(C_Mode,State_1,Item,State_2) <-
      */

stock_state_process(regular,State_1,Item,State_2) :-
   stock_state_select_generic(Item,State_1,State_A),
   stock_state_smooth(State_A,State_2).
stock_state_process(weighted,State_1,Item,State_2) :-
   stock_state_select_generic(Item,State_1,State_A),
   stock_state_weight_and_transform(State_A,State_2).


/* stock_state_to_display(Item,State) <-
      */

stock_state_to_display(Item,State) :-
   dsa_remember( stock_state_to_display(Item,State) ),
   dislog_variables_get([
      chart_display_mode-CD_Mode,
      chart_mode-C_Mode,
      y_axis_mode-D_Mode ]),
   name_append('stock_state_to_',CD_Mode,Predicate),
   apply(Predicate,[[C_Mode,D_Mode],Item,State]).


/* stock_states_to_display(Items,States) <-
      */

stock_states_to_display(Items,States) :-
   dsa_remember( stock_states_to_display(Items,States) ),
   dislog_variables_get([
      chart_display_mode-CD_Mode,
      chart_mode-C_Mode,
      y_axis_mode-D_Mode ]),
   assert_colors_for_companies(Items,Pairs),
   name_append('stock_states_to_',CD_Mode,Predicate),
   apply(Predicate,[[C_Mode,D_Mode],Items,States]),
   retract_colors_for_companies(Pairs),
   !.
   

/* assert_colors_for_companies(Companies,Pairs) <-
      */

assert_colors_for_companies(Companies,Pairs) :-
   dsa_variable(stock_print_colors,Colors),
   assert_colors_for_companies(Companies,Colors,Pairs).

assert_colors_for_companies([N|Ns],[C|Cs],[N-C|Ps]) :-
   asserta(stock_item_to_color(N,C)),
   assert_colors_for_companies(Ns,Cs,Ps).
assert_colors_for_companies([N|Ns],[],Ps) :-
   dsa_variable(stock_print_colors,Cs),
   assert_colors_for_companies([N|Ns],Cs,Ps).
assert_colors_for_companies([],_,[]).


/* retract_colors_for_companies(Pairs) <-
      */

retract_colors_for_companies([N-C|Ps]) :-
   retract(stock_item_to_color(N,C)),
   retract_colors_for_companies(Ps).
retract_colors_for_companies([]).


/* dsa_remember(Goal) <-
      */

dsa_remember(Goal) :-
   dsa_display_cache(_,Goal),
   !.
dsa_remember(Goal) :-
   dislog_counter(get,dsa_display_cache,N),
   M is N + 1,
   assert(dsa_display_cache(M,Goal)),
   dislog_counter(set,dsa_display_cache,M).


/*** tests ********************************************************/


test(stock_tool:stock_states_to_display,1) :-
   test_states_for_stock_state_to_display(Names,States),
   stock_states_to_display(Names,States).

test(stock_tool:stock_states_to_display,2) :-
   test_states_for_stock_state_to_display(Names,States),
   dislog_variable_switch(curve_type,Type,percents),
   stock_states_to_display(Names,States),
   dislog_variable_set(curve_type,Type).

test_states_for_stock_state_to_display(Names,States) :-
   Names = ['A','B'],
   States = [State_1,State_2],
   State_1 = [
      [stock('A', 519000, 01-3-99, 1.19, [])],
      [stock('A', 519000, 11-3-99, 1.19, [])],
      [stock('A', 519000, 21-3-99, 1.17, [])],
      [stock('A', 519000, 01-4-99, 1.17, [])],
      [stock('A', 519000, 11-4-99, 1.15, [])],
      [stock('A', 519000, 21-4-99, 1.15, [])],
      [stock('A', 519000, 01-5-99, 1.13, [])],
      [stock('A', 519000, 11-5-99, 1.13, [])],
      [stock('A', 519000, 21-5-99, 1.11, [])],
      [stock('A', 519000, 01-6-99, 1.11, [])] ],
   State_2 = [
      [stock('B', 519000, 11-1-100, 1.00, [])],
      [stock('B', 519000, 21-1-100, 1.00, [])],
      [stock('B', 519000, 01-2-100, 1.00, [])],
      [stock('B', 519000, 11-2-100, 1.00, [])],
      [stock('B', 519000, 21-2-100, 1.00, [])],
      [stock('B', 519000, 01-3-100, 1.01, [])],
      [stock('B', 519000, 11-3-100, 1.01, [])],
      [stock('B', 519000, 21-3-100, 1.03, [])],
      [stock('B', 519000, 01-4-100, 1.03, [])],
      [stock('B', 519000, 11-4-100, 1.05, [])],
      [stock('B', 519000, 21-4-100, 1.05, [])],
      [stock('B', 519000, 01-5-100, 1.07, [])],
      [stock('B', 519000, 11-5-100, 1.09, [])],
      [stock('B', 519000, 21-5-100, 1.11, [])],
      [stock('B', 519000, 01-6-100, 1.13, [])] ].


/******************************************************************/


