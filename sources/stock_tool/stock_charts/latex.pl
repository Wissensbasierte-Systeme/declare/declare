

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  LaTeX - Charts for DAX Shares        ***/
/***                                                            ***/
/******************************************************************/


% Chart = [
%    Label,Color,Diameter,
%    X_Marks,Y_Marks,X_Lines,Y_Lines,Values ]


/*** interface ****************************************************/


stock_state_to_postscript(Modes,Item,State) :-
   stock_state_to_latex_file(Modes,Item,State),
   preview_latex_file.

stock_states_to_postscript(Modes,Items,States) :-
   stock_states_to_latex_file(Modes,Items,States),
   preview_latex_file.


ghostview_charts(Charts) :-
   stock_goal_to_stock_file(
      checklist( stock_chart_to_latex,
         Charts ),
      file_input ),
   preview_latex_file.
   
ghostview_values(Chart) :-
   stock_goal_to_stock_file(
      stock_chart_to_latex(Chart),
      file_input ),
   preview_latex_file.


preview_latex_file :-
   stock_file(results,Directory),
   stock_file(file,File),
   preview_latex_file(Directory,File),
   !.

preview_latex_file(Directory,File) :-
   dislog_variable_get(stock_mode,regular),
   writeln('---> postscript file: '),
   dislog_variable_get(ghostview_tool,Tool),
   name_append([
      'cd ',Directory,';',
      'latex ',File,' > /dev/null;',
      'dvips ',File,' > /dev/null;',
      Tool,' ',File,'.ps &'],Command),
   us(Command).
preview_latex_file(Directory,File) :-
   dislog_variable_get(stock_mode,xpce),
   writeln('---> postscript file: '),
   name_append([
      'cd ',Directory,';',
      'latex ',File,' > /dev/null;',
%     'dps ',File,' > /dev/null'],Command),
      '/usr/bin/dvips -o',File,'.ps ',File,'.dvi > /dev/null'],Command),
   us(Command),
   display_stock_bitmap.


file_convert(ps_to_gif,Ps_File,Gif_File) :-
   concat_atom([
      'pstogif -depth 8 -density 100 -out ',
         Gif_File, ' ', Ps_File,' > /dev/null'],
      Command),
   shell(Command).


stock_goal_to_stock_file(Goal,File) :-
   stock_file(File,Output_File),
   writeln('---> output file: '),
   writeln(Output_File),
   tell(Output_File),
   call(Goal),
   told.


/*** implementation ***********************************************/


/* stock_states_to_latex_file(Modes,Companies,States) <-
      Displays the charts States of the companies in the set
      Companies according to Modes=[C_Mode,D_Mode].
       - C_Mode = regular: the actual values are shown,
       - C_Mode = weighted: the weighted values are shown,
       - D_Mode = linear: the chart is linear,
       - D_Mode = logarithmic: the chart is logarithmic. */

stock_states_to_latex_file(Modes,Companies,States) :-
   stock_goal_to_stock_file(
      stock_states_to_latex_file(Modes,1,Companies,States),
      file_input ).

stock_states_to_latex_file(Modes,N,[C|Cs],[S|Ss]) :-
   stock_state_to_latex(Modes,N,C,S),
   M is N + 1,
   stock_states_to_latex_file(Modes,M,Cs,Ss).
stock_states_to_latex_file(_,_,[],[]).


/* stock_state_to_latex_file(Modes,Company,State) <-
      writes the state State of stock values for Company
      into a LaTeX file. */

stock_state_to_latex_file([weighted,D_Mode],Company,State) :-
   stock_state_weight_and_transform(State,State_2),
   stock_goal_to_stock_file(
      stock_state_to_latex([weighted,D_Mode],1,Company,State_2),
      file_input ).
stock_state_to_latex_file([regular,D_Mode],Company,State) :-
   stock_state_to_chart([regular,D_Mode],Company,State,Chart),
   stock_goal_to_stock_file(
      stock_chart_to_latex(Chart),
      file_input ),
   !.

 
stock_state_to_latex(Modes,N,Company,State) :-
   stock_state_to_chart(Modes,N,Company,State,XY_Labels,Chart),
   stock_chart_to_latex(XY_Labels,Chart),
   !.


stock_chart_to_latex(Chart) :-
   stock_chart_to_latex([-1,-2],Chart).


stock_chart_to_latex(XY_Labels,Chart) :-
   XY_Labels = [X,Y],
   Chart =
      [ Label,Color,Diameter,
        X_Marks,Y_Marks,X_Lines,Y_Lines,Values ],
   backslash(BS),
   nl,
   write_list([ '% --- ', Label, ' ---' ]), nl, nl,
   write_list([ BS, 'put (',X,',',Y,')',
      '{', BS, 'textcolor{',Color,'}',
      '{', BS, 'large', BS, 'sc ',Label,'}}']), nl, nl,
   writeln('% values'), nl,
   checklist( write_value_to_latex(Color,Diameter),
      Values), nl,
   writeln('% marks at x-axis'), nl,
   checklist( write_mark_at_axis(x),
      X_Marks), nl,
   writeln('% marks at y-axis'), nl,
   checklist( write_mark_at_axis(y),
      Y_Marks), nl,
   writeln('% horizontal lines'), nl,
   checklist( write_dotted_line(horizontal),
      X_Lines), nl,
   writeln('% vertical lines'), nl,
   checklist( write_dotted_line(vertical),
      Y_Lines), nl.
 

write_mark_at_axis(x,[X,Label]) :-
   write_mark_at_axis(line,[X,-0.1],[0,1]),
   write_mark_at_axis(label,[X,-0.5],Label).
write_mark_at_axis(y,[Y,Label]) :-
   write_mark_at_axis(line,[-0.1,Y],[1,0]),
   write_mark_at_axis(label,[-0.9,Y],Label).

write_mark_at_axis(line,[X,Y],[LX,LY]) :-
   backslash(BS),
   write_list([BS,'put (',X,',',Y,')',
      '{',BS,'line(',LX,',',LY,'){0.2}}']),
   nl.
write_mark_at_axis(label,[X,Y],Label) :-
   dsa_variable(label_size,Size),
   backslash(BS),
   write_list([BS,'put (',X,',',Y,')',
      '{',BS,'makebox(0,0){',BS,Size,BS,'sf ',Label,'}}' ]),
   nl.

 

write_dotted_line(horizontal,Y) :-
   backslash(BS),
   write_list([ BS, 'multiput(0,',Y,')(0.5,0){30}',
      '{', BS, 'circle*{0.05}}' ]),
   nl.
write_dotted_line(vertical,X) :-
   backslash(BS),
   write_list([ BS, 'multiput(', X, ',0)(0,0.5){20}',
      '{', BS, 'circle*{0.05}}' ]),
   nl.


write_value_to_latex(Color,Diameter,X-Y) :-
   real_normalize(X, X1),
   real_normalize(Y, Y1),
   backslash(BS),
   write_list([
      BS, 'put (', X1, ',', Y1, ')',
         '{', BS, 'textcolor{', Color, '}',
            '{', BS, 'circle*{', Diameter, '}}}' ]),
   nl.


/* real_normalize(X, Y) <-
      */

real_normalize(X, Y) :-
   X < 0,
   !,
   abs(X, U),
   real_normalize(U, V),
   name_append('- ', V, Y).
real_normalize(X, Y) :-
   name_split_at_position(["e-"], X, [M, E]),
   !,
   ( ( name_split_at_position(["."], M, [M1, M2]),
       name_append(M1, M2, M3) )
   ; M3 = M ),
   multify(0, E, [Z|Zeros]),
   flatten([Z, '.', Zeros, M3, a], L),
   name_append(L, T1),
   name(T1, L1),
   reverse(L1, [_|L2]),
   reverse(L2, L3),
   name(Y, [32|L3]).
real_normalize(X, X).


/*** tests ********************************************************/


test(stock(chart)) :-
   chart(postscript,
      'Heidelberger Druckmaschinen 2').
test(stock(chart)) :-
   chart(postscript,regular,logarithmic,
      'Heidelberger Druckmaschinen 2').
test(stock(chart)) :-
   chart(postscript,'Porsche Vz').
test(stock(ghostview_values)) :-
   ghostview_values( [ 'Test', green, 0.2,
      [[1,1],[2,2]], [[3,3],[4,4],[10,10]],
      [2,3], [4,6],
      [1-2,4-5,6-2] ] ).
test(stock(charts_2)) :-
   charts_2.


/******************************************************************/

 
