

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  XPCE - Charts for DAX Shares         ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* stock_state_to_xpce(Modes,Item,State) <-
      */

stock_state_to_xpce(Modes,Item,State) :-
   dislog_stock_advisor(clear_display_window),
   send_xpce_coordinate_system_box,
   stock_state_to_chart_for_xpce(Modes, Item, State,
      Y_Unit, XY_Labels, Chart),
   stock_chart_to_xpce(Y_Unit,XY_Labels,Chart),
   stock_state_to_xpce_avg(Modes,Item,State).
   
stock_state_to_chart_for_xpce(Modes, Item, State,
      Y_Unit, XY_Labels, Chart) :-
   stock_state_to_chart(Modes,Item,State,Chart),
   stock_chart_compute_x_y_labels(1,XY_Labels),
   stock_chart_compute_y_unit(Item,Y_Unit).


stock_state_to_xpce_avg(Modes,Item,State) :-
   dislog_variable_get(show_avg_line,yes),
   dislog_variable_get(smooth_parameter_avg,Smooth),
   \+ stock_values_per_day(Item),
   !,
   stock_state_to_avg_chart(
      Modes,Item,State,Smooth,Chart_Avg),
   stock_chart_compute_x_y_labels(4,XY_Labels_Avg),
   stock_chart_to_xpce('',XY_Labels_Avg,Chart_Avg),
   remove_xy_labels_box(XY_Labels_Avg).
stock_state_to_xpce_avg(_,_,_).


stock_chart_compute_y_unit(Item,Y_Unit) :-
   ( ( stock_values_per_day(Item),
       !,
       Y_Unit = '' )
   ; Y_Unit = 'Euro' ).
   
stock_values_per_day(Item) :-
   name(Item,List),
   append("Stock Values (per day)",_,List).


remove_xy_labels_box(XY_Labels) :-
   dislog_variable_get(dsa_chart_background_colour,Colour),
   stock_chart_to_xpce( '', XY_Labels,
      [ '', Colour, 2, [],[],[],[], [] ] ).


/* stock_states_to_xpce(Modes,Items,States) <-
      */

stock_states_to_xpce(Modes,Items,States) :-
   dislog_stock_advisor(clear_display_window),
   send_xpce_coordinate_system_box,
   stock_states_to_xpce_no_clear(Modes,Items,States).

stock_states_to_xpce_no_clear(Modes,Items,States) :-
   length(States,N),
   start_status_bar_dsa(stock_states_to_xpce,
      'displaying stock states ...',N),
   stock_states_to_xpce(Modes,1,Items,States),
   status_bar_disappear_dsa(stock_states_to_xpce).

stock_states_to_xpce(Modes,N,[C|Cs],[S|Ss]) :-
   stock_state_to_xpce(Modes,N,C,S),
   status_bar_increase_dsa(stock_states_to_xpce),
   M is N + 1,
   stock_states_to_xpce(Modes,M,Cs,Ss).
stock_states_to_xpce(_,_,[],[]).

stock_state_to_xpce(Modes,N,Company,State) :-
   ( ( dislog_variable_get(curve_style,lines), !,
       stock_state_sort_by_date_2(State,State_2) )
   ; State_2 = State ),
   stock_state_to_chart( Modes,N,Company,State_2,
      XY_Labels,Chart ),
   stock_chart_to_xpce('  % ',XY_Labels,Chart).
   

/* stock_chart_to_xpce(Y_Unit,XY_Labels,Chart) <-
      */

stock_chart_to_xpce(Y_Unit,XY_Labels,Chart) :-
   dislog_variables_get([
      dsa_chart_picture - Picture,
      dsa_chart_origin - Origin,
      x_factor - Factor ]),
   stock_chart_to_xpce(
      Picture,Origin,Y_Unit,XY_Labels,Factor,Chart).


/*** implementation ***********************************************/


/* stock_chart_to_xpce(
         Picture,Origin,Y_Unit,XY_Labels,Factor,Chart) <-
      */

stock_chart_to_xpce(
      Picture,Origin,Y_Unit,XY_Labels,Factor,Chart) :-
%  writeln(user, stock_chart_to_xpce(
%     Picture,Origin,Y_Unit,XY_Labels,Factor)),
   Chart = [
      Label,Colour,Diameter,
      X_Marks,Y_Marks,X_Lines,Y_Lines,Values_1 ],
   send_xpce_coordinate_system( Picture, Origin, Factor,
      Y_Unit, X_Lines, Y_Lines, X_Marks, Y_Marks ),
   sublist( x_is_greater_than_zero,
      Values_1, Values_2 ),
   maplist( value_to_xpce_vector(Origin,Factor),
      Values_2, Vectors ),
   send_curve(Picture,Colour,Diameter,Vectors),
   send_xpce_label(
      Picture,Origin,Factor,Label,XY_Labels,Colour).

x_is_greater_than_zero(X-_) :-
   X >= 0.


/* send_xpce_coordinate_system( Picture, Origin, Factor,
         Y_Unit, X_Lines, Y_Lines, X_Marks, Y_Marks ) <-
      */

send_xpce_coordinate_system( Picture, Origin, Factor,
      Y_Unit, X_Lines, Y_Lines, X_Marks, Y_Marks ) :-
   dislog_variable_get(x_line_colour,Colour_X),
   checklist( send_dotted_horizontal_line(
      Picture,Origin,Factor,Colour_X),
      X_Lines ),
   dislog_variable_get(y_line_dots,N),
   dislog_variable_get(y_line_colour,Colour_Y),
   checklist( send_dotted_vertical_line(
      Picture, Origin, Factor, Colour_Y, N ),
      Y_Lines ),
   checklist( send_x_mark(Picture,Origin,Factor),
      X_Marks ),
   checklist( send_y_mark(Picture,Origin,Factor),
      Y_Marks ),
   send_xpce_coordinate_system(Picture,Origin,Factor),
   send_xpce_coordinate_system_y_unit( Picture,Origin,Factor,
      Y_Unit ).


/* stock_chart_to_xpce_stand_alone(
         Size,Origin,Y_Unit,XY_Labels,Factor,Chart) <-
      */

stock_chart_to_xpce_stand_alone(
      Size,Origin,Y_Unit,XY_Labels,Factor,Chart) :-
   create_picture_dialog(Size,Picture),
   stock_chart_to_xpce(
      Picture,Origin,Y_Unit,XY_Labels,Factor,Chart).

create_picture_dialog(Size,Picture) :-
   create_picture_dialog(Size,_,Picture).

create_picture_dialog([X_Size,Y_Size],Dialog,Picture) :-
   new(Dialog,dialog('Chart Dialog')),
   send(Dialog,append,button(quit,
      message(Dialog,destroy))),
   send(Dialog,append,button(make,
      message(@prolog,make))),
   send(Dialog,append,
      new(Picture,picture),below),
   send(Picture,size,size(X_Size,Y_Size)),
   send(Dialog,open).


/* send_curve(Picture,Colour,Diameter,Vectors) <-
      */

send_curve(Picture,Colour,Diameter,Vectors) :-
   dislog_variable_get(curve_style,points),
   !,
   send_points(Picture,Colour,Diameter,Vectors).
send_curve(Picture,Colour,_,Vectors) :-
   dislog_variable_get(curve_style,lines),
%  sublist( y_is_not_null,
%     Vectors, Vectors_2 ),
%  send_lines(Picture,Colour,Vectors_2),
   send_lines(Picture,Colour,Vectors),
   !.
   
y_is_not_null([_,Y]) :-
   \+ 0 is Y.


/* send_lines(Picture, Colour, Vectors) <-
      */

send_lines(Picture, Colour, [[X1,Y1], [X2,Y2]|Vectors]) :-
   X2 - X1 > 1,
   !,
   send_point(Picture, Colour, 2, X1-Y1),
   send_point(Picture, Colour, 2, X2-Y2),
   send_lines(Picture, Colour, [[X2,Y2]|Vectors]).
send_lines(Picture, Colour, [V1, V2|Vectors]) :-
   !,
   send_line(Picture, Colour, V1, V2),
   send_lines(Picture, Colour, [V2|Vectors]).
send_lines(_, _, _).

send_line(Picture, Colour, [X1,Y1], [X2,Y2]) :-
   vector_add([X2,Y2], [-X1,-Y1], [X3,Y3]),
   send(Picture, display,
      new(Line, line(X3,Y3)), point(X1,Y1) ),
   send(Line, colour, Colour).


/* send_points(Picture,Colour,Diameter,Vectors) <-
      */

send_points(Picture,Colour,Diameter,Vectors) :-
   checklist(
      send_point(Picture,Colour,Diameter),
      Vectors ).

send_point(Picture,Colour,Diameter,[X,Y]) :-
   send_point(Picture,Colour,Diameter,X-Y).

send_point(Picture,Colour,Diameter,X-Y) :-
   vector_add([X,Y],[-Diameter/2,-Diameter/2],[A,B]),
   send(Picture,display,
      new(Circle,circle(Diameter)), point(A,B) ),
   send(Circle,colour,Colour),
   send(Circle,fill_pattern,Colour).


/* send_vector(Picture,X-Y,X1-Y1) <-
      */

send_vector(Picture,X-Y,X1-Y1) :-
   vector_add([X,Y],[X1,Y1],[X2,Y2]),
   send(Picture,display,
      new(_,arrow(length=20,wing=0.5)),point(X2,Y2)),
   send(Picture,display,
      new(_,line(X1,Y1)),point(X,Y)).


/* send_xpce_coordinate_system(Picture,Origin,Factor) <-
      */

send_xpce_coordinate_system_box :-
   dislog_variables_get([
      dsa_chart_picture - Picture,
      dsa_chart_origin - Origin,
      x_factor - Factor ]),
   send_xpce_coordinate_system_box(Picture,Origin,Factor).

send_xpce_coordinate_system_box(Picture,Origin,Factor) :-
   vector_multiply(Factor,Origin,[X,Y]),
   Width = 25*Factor,
   Height = 19*Factor,
   send(Picture,display,
      new(Box, box(Width,Height)), point(X,Y-Height) ),
   send(Box,fill_pattern,colour(white)).

send_xpce_coordinate_system(Picture,Origin,Factor) :-
   vector_multiply(Factor,Origin,[X,Y]),
   send(Picture,display,
%     line(X,Y,X+Factor*Factor,Y,second) ),
      line(X,Y,X+25*Factor,Y) ),
   send(Picture,display,
%     line(X,Y,X,Y-19*Factor,second) ),
      line(X,Y,X,Y-19*Factor) ),
   send(Picture,display,
      new(Circle,circle(0)), point(X,Y-20*Factor) ),
   dislog_variable_get(dsa_chart_background_colour,Colour),
   send(Circle,fill_pattern,Colour).


send_xpce_coordinate_system_y_unit(
      Picture, Origin, Factor, Unit ) :-
   vector_multiply(Factor,Origin,[X,Y]),
   send(Picture,display,
      new(Text,text(Unit)), point(X-37,Y+20-20*Factor) ),
   dislog_variable_get(y_unit_colour,Colour),
   send(Text,colour,Colour),
   send(Text,font,font(times,bold,12)).


send_x_mark(Picture,Origin,Factor,[X,Mark]) :-
   ( ( Mark = year(Year),
       vector_to_xpce_vector(Origin,Factor,
          [X-6*0.055-0.001,-1],Vector2),
       dislog_variable_get(x_year_colour,Colour),
       send_text(Picture,Colour,Vector2,Year),
       name_append('        ',7,Real_Mark) )
   ; Real_Mark = Mark ),
   name(Real_Mark,List), length(List,N),
   vector_to_xpce_vector(Origin,Factor,
      [X-N*0.055-0.001,-0.5],Vector),
%  vector_to_xpce_vector(Origin,Factor,[X,-0.1],[A,B]),
   vector_to_xpce_vector(Origin,Factor,[X,-0.2],[A,B]),
   send(Picture,display,
      line(0,-0.2*Factor), point(A,B) ),
   real_mark_to_write(Real_Mark,Real_Mark_Write),
   send_text(Picture,Vector,Real_Mark_Write).

real_mark_to_write('        1','').
real_mark_to_write('        3','').
real_mark_to_write('        5','').
real_mark_to_write('        7','').
real_mark_to_write('        9','').
real_mark_to_write('        11','').
real_mark_to_write('        2','        F').
real_mark_to_write('        4','       A').
real_mark_to_write('        6','        J').
real_mark_to_write('        8','       A').
real_mark_to_write('        10','        O').
real_mark_to_write('        12','        D').

real_mark_to_write(1,'').
real_mark_to_write(3,'').
real_mark_to_write(5,'').
real_mark_to_write(7,'').
real_mark_to_write(9,'').
real_mark_to_write(11,'').
real_mark_to_write(2,'        F').
real_mark_to_write(4,'       A').
real_mark_to_write(6,'        J').
real_mark_to_write(8,'       A').
real_mark_to_write(10,'        O').
real_mark_to_write(12,'        D').


send_y_mark(Picture,Origin,Factor,[Y,Mark]) :-
   beautify_y_mark(Mark,Mark_2),
   name(Mark_2,List), length(List,N),
   vector_to_xpce_vector(Origin,Factor,
%     [-0.9-N*0.055*Factor/30,Y+0.2],Vector),
      [-1.3-N*0.055*Factor/30,Y+0.2],Vector),
%  vector_to_xpce_vector(Origin,Factor,[-0.1,Y],[A,B]),
   vector_to_xpce_vector(Origin,Factor,[-0.2,Y],[A,B]),
   send(Picture,display,
      line(+0.2*Factor,0), point(A,B) ),
   send_text(Picture,Vector,Mark_2).

beautify_y_mark(Mark_1,Mark_2) :-
   round(Mark_1,1,Mark_2),
   !.
beautify_y_mark(Mark_1,Mark_2) :-
   round(Mark_1,2,Mark_3),
   round(Mark_1,1,Mark_4),
   round(Mark_1,0,Mark_5),
   ( ( Mark_5 \= Mark_3,
       Mark_3 = Mark_4, !,
       name_append(Mark_3,'0 ',Mark_2) )
   ; Mark_2 = Mark_3 ).


/* send_text(Picture,Colour,[X,Y],Mark) <-
      */

send_text(Picture,Colour,[X,Y],Mark) :-
   send(Picture,display,
      new(Text,text(Mark)), point(X,Y) ),
   send(Text,font,font(times,roman,10)),
   send(Text,colour,Colour).

send_text(Picture,[X,Y],Mark) :-
   send(Picture,display,
      new(Text,text(Mark)), point(X,Y) ),
   send(Text,font,font(times,roman,10)).


/* send_dotted_vertical_lines(Picture,Origin,Factor,Y_lines) <-
      */

send_dotted_vertical_lines(Picture,Origin,Factor,Y_lines) :-
%  writeln(user,
%     send_dotted_vertical_lines(Picture,Origin,Factor,Y_lines)),
   checklist( send_dotted_vertical_line(Picture,Origin,Factor),
      Y_lines ).

send_dotted_vertical_line(Picture,Origin,Factor,X) :-
   send_dotted_vertical_line(
      Picture,Origin,Factor,colour(black),20,X).

send_dotted_vertical_line(Picture,Origin,Factor,Colour,N,X) :-
   vector_to_xpce_vector(Origin,Factor,[X,0],Vector),
%  send_dotted_line(Picture,Colour,2,[0,-Factor/2],N,Vector).
   Vector = [A,B],
   Length is ( N + 4 ) * Factor / 2,
   send(Picture,display,
      new(Line,line(0,-Length)), point(A,B) ),
   send(Line,colour,Colour).


/* send_dotted_horizontal_lines(Picture,Origin,Factor,X_lines) <-
      */

send_dotted_horizontal_lines(Picture,Origin,Factor,X_lines) :-
   checklist( send_dotted_horizontal_line(Picture,Origin,Factor),
      X_lines ).

send_dotted_horizontal_line(Picture,Origin,Factor,Y) :-
   send_dotted_horizontal_line(
      Picture,Origin,Factor,colour(black),Y).

send_dotted_horizontal_line(Picture,Origin,Factor,Colour,Y) :-
   vector_to_xpce_vector(Origin,Factor,[0,Y],Vector),
%  N is truncate(2*Factor) - 2,
%  N is truncate(2*Factor),
   N is 49.85,
%  send_dotted_line(Picture,Colour,2,[Factor/2,0],N,Vector).
   Vector = [A,B],
   Length is N * Factor / 2,
   send(Picture,display,
      new(Line,line(Length,0)), point(A,B) ),
   send(Line,colour,Colour).


/* send_dotted_line(Picture,Colour,Diameter,Offset,N,Vector) <-
      */

send_dotted_line(Picture,Colour,Diameter,Offset,N,Vector) :-
   N >= 0, !,
   send_point(Picture,Colour,Diameter,Vector),
   vector_add(Vector,Offset,Vector_2),
   M is N - 1,
   send_dotted_line(Picture,Colour,Diameter,Offset,M,Vector_2).
send_dotted_line(_,_,_,_,_,_).


/* value_xpce_normalize(XO-YO,Factor,X1-Y1,X2-Y2) <-
      */

value_xpce_normalize(XO-YO,Factor,X1-Y1,X2-Y2) :-
   vector_add([XO,YO],[X1,-Y1],Vector),
   vector_multiply(Factor,Vector,[X2,Y2]).


/* value_to_xpce_vector(Origin,Factor,Pair,Vector) <-
      */

value_to_xpce_vector(Origin,Factor,X-Y,Vector) :-
   vector_add(Origin,[X,-Y],Vector_2),
   vector_multiply(Factor,Vector_2,Vector).

vector_to_xpce_vector(Origin,Factor,[X,Y],Vector) :-
   vector_add(Origin,[X,-Y],Vector_2),
   vector_multiply(Factor,Vector_2,Vector).


/* send_xpce_label(Picture,Origin,Factor,
         Label,[X_Label,Y_Label],Colour) <-
      */

send_xpce_label(Picture,Origin,Factor,
      Label,[X_Label,Y_Label],Colour) :-
   dislog_variable_get(font_size,Font_Size),
   vector_to_xpce_vector(Origin,Factor,[X_Label,Y_Label],[X,Y]),
   send(Picture,display,
      new(Text,text(Label)), point(X,Y) ),
   send(Text,font,font(times,bold,Font_Size)),
   send(Text,colour,Colour),
   vector_to_xpce_vector(Origin,Factor,
      [X_Label-0.7,Y_Label-0.10-0.0125*(Font_Size-14)],[X1,Y1]),
   send(Picture,display,
      new(Box,box(10,10)), point(X1,Y1) ),
   send(Box,fill_pattern,Colour),
   send(Box,colour,Colour).


/*** tests ********************************************************/


test(stock_tool:stock_chart_to_xpce,1) :-
   Size = [800,600], Origin = [5,22],
   Y_Unit = 35, XY_Labels = [1,-2], Factor = 10,
   Chart = [ 'Test', colour(green), 5,
      [[1,1],[2,2],[5,5]], [[3,3],[4,4],[10,10]],
      [2,3,4,5,6], [3,4,5,6],
      [1-2,4-5,6-2] ],
   stock_chart_to_xpce_stand_alone(
      Size, Origin, Y_Unit, XY_Labels, Factor, Chart ).


/******************************************************************/


