
% configuration file
% this file is generated, please do not edit !

stock_portfolio_tuple([
   owner: 'Portfolio 1',
   members: [
      [ company: 'Mannesmann',
        value: 1440,
        quantity: 1.5 ],
      [ company: 'BMW St',
        value: 1950,
        quantity: 2 ],
      [ company: 'Hoechst',
        value: 87,
        quantity: 35 ],
      [ company: 'Singulus Techn',
        value: 251.32,
        quantity: 20 ] ] ]).

stock_portfolio_tuple([
   owner: 'Portfolio 2',
   members: [
      [ company: 'Mannesmann',
        value: 1420,
        date: '01.04.1998',
        quantity: 2 ],
      [ company: 'Dt.Bank',
        value: 122.25,
        date: '06.03.1998',
        quantity: 30 ],
      [ company: 'Volkswagen St',
        value: 1475,
        date: '02.06.1998',
        quantity: 3 ],
      [ company: 'Bayer',
        value: 65.75,
        date: '01.09.1998',
        quantity: 50 ] ] ]).

stock_portfolio_tuple([
   owner: 'Fernsehen',
   members: [
      [ company: 'Bayer',
        wkn: 575200,
        date: '02.01.2001',
        value: 111.29,
        quantity: 1 ],
      [ company: 'BASF',
        wkn: 515100,
        date: '02.01.2001',
        value: 95.25,
        quantity: 1 ],
      [ company: 'DaimlerChrysler',
        wkn: 710000,
        date: '02.01.2001',
        value: 86.86,
        quantity: 1 ],
      [ company: 'Volkswagen St',
        wkn: 766400,
        date: '02.01.2001',
        value: 1065.93,
        quantity: 1 ],
      [ company: 'Commerzbank',
        wkn: 803200,
        date: '02.01.2001',
        value: 59.07,
        quantity: 1 ] ] ]).

stock_portfolio_tuple([
   owner: 'Test Portfolio',
   members: [
      [ company: 'Mannesmann',
        value: 1440,
        quantity: 1.5 ],
      [ company: 'Volkswagen St',
        value: 1475,
        date: '02.06.1998',
        quantity: 3 ] ] ]).

