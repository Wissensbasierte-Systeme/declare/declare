

stock_branch_colors([
   'Banken' - red,
   'Automobile' - green,
   'Chemie' - cyan,
   'Versorger' - blue,
   'Stahl' - yellow,
   'High Tech' - magenta,
   'Versicherungen' - black ]).


stock_branch('Banken',[
   'Bankges.Berlin',
   'Bay.Hypo-Verei',
   'BHF-Bank',
   'Commerzbank',
   'Dresdner Bank',
   'Dt.Bank']).

stock_branch('Versicherungen',[
   'Allianz N',
   'DBV-Winterthur',
   'ERGO Versicher',
   'Hannover R&#252;ckv',
   'M&#252;nch.R&#252;ck N50']).

stock_branch('Versorger',[
   'RWE St',
   'Veba',
   'VIAG',
   'E.ON AG']).

stock_branch('Chemie',[
   'BASF',
   'Bayer',
   'Henkel Vz',
   'Hoechst',
   'Merck',
   'Schering']).

stock_branch('Automobile',[
   'BMW St',
   'DAIMLERCHRYSLER AG',
   'Deutz',
   'Porsche Vz',
   'Volkswagen St']).

stock_branch('Stahl',[
   'Degussa',
   'FAG Kugelfisch',
   'Kl&#246;ckner-Werke',
%  'Krupp-Hoesch',
   'Metallges.',
   'Preussag',
%  'Rheinmetall St',
   'RHEINMETALL BLN.VZ.',
   'Thyssen']).

stock_branch('High Tech',[
   'SAP AG Vz',
   'Siemens',
   'Nokia',
   'IBM',
   'Cisco Systems' ]).

stock_branch('Test',[
   'IBM',
   'BMW St' ]).

stock_branch('Base',[
   'Base 2001',
   'Base 2002',
   'Base 2003',
   'Base 2004' ]).

stock_branch('Peak',[
   'Peak 2001',
   'Peak 2002',
   'Peak 2003',
   'Peak 2004' ]).

