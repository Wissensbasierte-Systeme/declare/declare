

/******************************************************************/
/***                                                            ***/
/***        Stock Tool:  Data Cleaning during Spooling          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


:- discontiguous
      stock_multiply_tuple/3, stock_multiply_tuple/4.

:- dynamic
      stock_multiply_tuple/3, stock_multiply_tuple/4.

:- multifile
      stock_multiply_tuple/3, stock_multiply_tuple/4.


/* stock_multiply_tuple(Name,WKN,Factor) <-
      The stock value of the share Name with the number WKN
      has to be multiplied by Factor.
      Only the first atom is relevant. */

stock_multiply_tuple(Name,Number,Factor) :-
   stock_multiply_tuple(Name,Number,_,Factor).
% stock_multiply_tuple(_,Number,Factor) :-
%    stock_multiply_tuple(_,Number,_,Factor).


/* stock_multiply_tuple(Name,WKN,Date_Interval,Factor) <-
      The Date_Interval is irrelevant for spooling;
      it is only stored for documentation puposes. */


/******************************************************************/


