

name_to_nice_name_tuple(Name,Nice_Name) :-
   name_to_nice_name_tuples(Pairs),
   member(Name-Nice_Name,Pairs).


name_to_nice_name_tuples( [
%
   'Aach-M&#252;n.Bet N' -          'Aach-M\\"un.Bet N',
   'Brau & Brunnen' -               'Brau \\& Brunnen',
   'DBV-Winterthur' -               'DBV-Win.',
   'D&#252;rr' -                    'D\\"urr',
   'Felten&Guil.En' -               'Felten\\&Guil.En',
   'Hannover R&#252;ckv' -          'Hann. R�ck',
%  'Hannover R&#252;ckv' -          'Hannover R\\"uckv',
   'Kl&#246;ckner-Werke' -          'Kl�ckner-Werke',
%  'Kl&#246;ckner-Werke' -          'Kl\\"ockner-Werke',
   'M&#252;nch.R&#252;ck N50' -     'M�nch. R�ck',
%  'M&#252;nch.R&#252;ck N50' -     'M\\"unch.R\\"uck N50',
   'Rh&#246;n-Klinik Vz' -          'Rh\\"on-Klinik Vz',
   'S&#252;dzucker Vz' -            'S\\"udzucker Vz',
   'Bayer.Vereinsb' -               'Bayer.\\ Vereinsbank',
   'Bayer.Hypo' -                   'Bayer.\\ Hypobank',
   'Bay.Hypo-Verei' -               'Bay. Hypo-V.',
   'Wuensche AG' -                  'W\\"unsche AG',
   'W&#252;nsche AG' -              'W\\"unsche AG',
%
   'ERGO Versicher' -               'ERGO Versicherung',
   'FAG Kugelfisch' -               'FAG Kugelf.',
%  'FAG Kugelfisch' -               'FAG Kugelfischer',
   'Dt.Bank' -                      'Deutsche Bank',
   'DAIMLERCHRYSLER AG' -           'DaimlerChrysler',
   'RHEINMETALL BLN.VZ.' -          'Rheinmetall',
%
%  'HANCKE & PETER IT SERVICE AG' - 'HANCKE \& PETER IT SERVICE AG',
%  'LPKF LASER & ELECTRONICS AG' -  'LPKF LASER \& ELECTRONICS AG',
%  'HOEFT & WESSEL' -               'HOEFT \& WESSEL',
%
   'BMW St' -                       'BMW',
   'SINGULUS TECHNOL.DM 5 IGL' -    'SINGULUS',
   'Singulus Techn' -               'Singulus',
   'Mannesmann' -                   'Vodafone' ] ).


