

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Stock Items                          ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      stock_tuple_special/2, special_dax_value/4.
 
:- multifile
      stock_tuple_special/2, special_dax_value/4.

 
/*** interface ****************************************************/


stock_group(State_1,Company,sonstige) :-
   member([stock(Company,_,_,_,_)],State_1),
   \+ special_stock_name(Company).
stock_group(_,Company,Group) :-
   stock_tuple_special(Group,Companies),
   member(Company,Companies).


stock_company(Company) :-
   stock_companies(Companies),
   member(Company,Companies).

stock_companies(Companies) :-
   stock_file(number_name,File),
   stock_companies(File,Companies).

stock_companies(File,Companies) :-
   diconsult(File,State),
   findall( Company,
      ( member([number_name(_,String)],State),
        name(Company,String) ),
      Companies_List ),
   list_to_ord_set(Companies_List,Companies),
   !.

stock_companies(File,Type,Companies) :-
   diconsult(File,State),
   findall( Company,
      ( member([number_name(Type,_,String)],State),
        name(Company,String) ),
      Companies_List ),
   list_to_ord_set(Companies_List,Companies),
   !.


stock_groups(Groups) :-
   findall( Group,
      stock_tuple(Group,_),
      Groups ).

stock_group(Name,Group) :-
   stock_tuple(Group,Names),
   member(Name,Names).


stock_tuple_special(Pool,Companies) :-
   stock_pool(Pool,Companies).
stock_tuple_special(Branch,Companies) :-
   stock_branch(Branch,Companies).


stock_tuple(Group,Names) :-
   stock_tuple_special(Group,Names).
stock_tuple(sonstige,Names) :-
   findall( Name,
      ( stock([Name|_]),
        \+ special_stock_name(Name) ),
      Names ).

special_stock_name(Name) :-
   stock_tuple_special(_Group,Names),
   member(Name,Names).


determine_stock_value_type(Company,short) :-
   special_dax_value(Company),
   !.
determine_stock_value_type(_,long).


special_dax_value(Name) :-
   special_dax_value(Name,_,_,_).
 
special_dax_value(Name,Number,Abbreviation) :-
   special_dax_value(Name,Number,Abbreviation,_).


/******************************************************************/


