

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Test Goals                           ***/
/***                                                            ***/
/******************************************************************/


test(stock_tool, init) :-
   dsa.

/*
test(stock_tool, stock_spool(1)) :-
   stock_spool(www_to_pl,1).

test(stock_tool, stock_spool(2)) :-
   stock_spool(pl_to_dl,[long]),
   stock_spool(pl_to_dl,[short]).
*/

test(stock_tool, stock_values(1)) :-
   stock_values('Banken', _State).
%  State = [
%     [stock('Banken', -, 1-4-98, 91.8643, [])],
%     [stock('Banken', -, 2-4-98, 92.7786, [])],
%     [stock('Banken', -, 3-4-98, 93.4143, [])],
%     [stock('Banken', -, 4-3-98, 80.3149, [])],
%     [stock('Banken', -, 5-3-98, 80.4271, [])]].

test(stock_tool, stock_values(2)) :-
   stock_values('IBM', _State).
%  State = [ [stock('IBM', 585, 6-2-98, 600, [])],
%     [stock('IBM', 585, 9-2-98, 598.008, [])],
%     [stock('IBM', 585, 10-2-98, 575.24, [])],
%     [stock('IBM', 585, 5-3-98, 577.489, [])],
%     [stock('IBM', 585, 25-3-98, 680.2, [-0.7,-0.7,19.54,10.96])],
%     [stock('IBM', 585, 26-3-98, 676, [-0.62,0.45,18.6,8.51])],
%     [stock('IBM', 585, 27-3-98, 675.5, [-0.07,-0.59,18.82,7.22])],
%     [stock('IBM', 585, 31-3-98, 680, [0.15,0,17.65,7.94])] ].

test(stock_tool, stock_consult) :-
   stock_consult(state, _State).
%  State = [ [stock('IBM', 585, 6-2-98, 600, [])],
%     [stock('IBM', 585, 9-2-98, 598.008, [])],
%     [stock('IBM', 585, 10-2-98, 575.24, [])],
%     [stock('IBM', 585, 5-3-98, 577.489, [])],
%     [stock('IBM', 585, 25-3-98, 680.2, [-0.7,-0.7,19.54,10.96])],
%     [stock('IBM', 585, 26-3-98, 676, [-0.62,0.45,18.6,8.51])],
%     [stock('IBM', 585, 27-3-98, 675.5, [-0.07,-0.59,18.82,7.22])],
%     [stock('IBM', 585, 31-3-98, 680, [0.15,0,17.65,7.94])],
%     [stock('Daimler', 777, 27-2-98, 91, [])],
%     [stock('Daimler', 777, 3-3-98, 97, [])],
%     [stock('Daimler', 777, 27-3-98, 97.5, [])],
%     [stock('Daimler', 777, 30-3-98, 95, [])]].
 
test(stock_tool, number_name(1)) :-
   stock_number_to_name([
      [number_name(500,[86,65,82,84,65])],
      [number_name(585,[71,69,65,32,86,122])],
      [number_name(761,[86,101,98,97])] ],
      761, Name),
   Name = 'Veba'.

test(stock_tool, number_name(2)) :-
   stock_name_to_number([
      [number_name(500,[86,65,82,84,65])],
      [number_name(585,[71,69,65,32,86,122])],
      [number_name(761,[86,101,98,97])] ],
      'Veba', Number),
   Number = 761.

test(stock_tool, stock_state_select(1)) :-
   stock_state_select([_,_,27-3-98],[
      [stock('IBM', 585, 6-2-98, 600, [])],
      [stock('IBM', 585, 9-2-98, 598.008, [])],
      [stock('IBM', 585, 10-2-98, 575.24, [])],
      [stock('IBM', 585, 5-3-98, 577.489, [])],
      [stock('IBM', 585, 25-3-98, 680.2, [-0.7,-0.7,19.54,10.96])],
      [stock('IBM', 585, 26-3-98, 676, [-0.62,0.45,18.6,8.51])],
      [stock('IBM', 585, 27-3-98, 675.5, [-0.07,-0.59,18.82,7.22])],
      [stock('IBM', 585, 31-3-98, 680, [0.15,0,17.65,7.94])],
      [stock('Daimler', 777, 27-2-98, 91, [])],
      [stock('Daimler', 777, 3-3-98, 97, [])],
      [stock('Daimler', 777, 27-3-98, 97.5, [])],
      [stock('Daimler', 777, 30-3-98, 95, [])] ],
      State),
   State = [
      [stock('IBM', 585, 27-3-98, 675.5, [-0.07,-0.59,18.82,7.22])],
      [stock('Daimler', 777, 27-3-98, 97.5, [])] ].

test(stock_tool, stock_state_select(2)) :-
   stock_state_select(['Daimler',_,27-3-98],[
      [stock('IBM', 585, 6-2-98, 600, [])],
      [stock('IBM', 585, 9-2-98, 598.008, [])],
      [stock('IBM', 585, 10-2-98, 575.24, [])],
      [stock('IBM', 585, 5-3-98, 577.489, [])],
      [stock('IBM', 585, 25-3-98, 680.2, [-0.7,-0.7,19.54,10.96])],
      [stock('IBM', 585, 26-3-98, 676, [-0.62,0.45,18.6,8.51])],
      [stock('IBM', 585, 27-3-98, 675.5, [-0.07,-0.59,18.82,7.22])],
      [stock('IBM', 585, 31-3-98, 680, [0.15,0,17.65,7.94])],
      [stock('Daimler', 777, 27-2-98, 91, [])],
      [stock('Daimler', 777, 3-3-98, 97, [])],
      [stock('Daimler', 777, 27-3-98, 97.5, [])],
      [stock('Daimler', 777, 30-3-98, 95, [])] ],
      State),
   State = [
      [stock('Daimler', 777, 27-3-98, 97.5, [])] ].

test(stock_tool, stock_state_select(3)) :-
   stock_state_select(['Banken',_-3-98],[
      [stock('Dt.Bank', 804, 6-2-98, 180, [])],
      [stock('Dt.Bank', 804, 9-2-98, 168, [])],
      [stock('Dt.Bank', 804, 10-2-98, 145.24, [])],
      [stock('Dt.Bank', 804, 5-3-98, 170.5, [])],
      [stock('Dt.Bank', 804, 25-3-98, 150.2, [-0.7,-0.7,9.54,10.9])],
      [stock('Dt.Bank', 804, 26-3-98, 156, [-0.62,0.45,18.6,8.5])],
      [stock('Dt.Bank', 804, 27-3-98, 155.5, [-0.07,-0.59,8.8,7.2])],
      [stock('Dt.Bank', 804, 31-3-98, 150, [0.15,0,17.65,7.94])],
      [stock('Daimler', 777, 27-2-98, 91, [])],
      [stock('Daimler', 777, 3-3-98, 97, [])],
      [stock('Daimler', 777, 27-3-98, 97.5, [])],
      [stock('Daimler', 777, 30-3-98, 95, [])] ],
      State),
   State = [
      [stock('Dt.Bank', 804, 5-3-98, 170.5, [])],
      [stock('Dt.Bank', 804, 25-3-98, 150.2, [-0.7,-0.7,9.54,10.9])],
      [stock('Dt.Bank', 804, 26-3-98, 156, [-0.62,0.45,18.6,8.5])],
      [stock('Dt.Bank', 804, 27-3-98, 155.5, [-0.07,-0.59,8.8,7.2])],
      [stock('Dt.Bank', 804, 31-3-98, 150, [0.15,0,17.65,7.94])] ].

test(stock_tool, stock_state_weight) :-
   stock_state_weight([
      [stock('IBM', 585, 6-2-98, 600, [])],
      [stock('IBM', 585, 9-2-98, 598.008, [])],
      [stock('IBM', 585, 10-2-98, 575.24, [])],
      [stock('IBM', 585, 5-3-98, 577.489, [])],
      [stock('IBM', 585, 25-3-98, 680.2, [-0.7,-0.7,19.54,10.96])],
      [stock('IBM', 585, 26-3-98, 676, [-0.62,0.45,18.6,8.51])],
      [stock('IBM', 585, 27-3-98, 675.5, [-0.07,-0.59,18.82,7.22])],
      [stock('IBM', 585, 31-3-98, 680, [0.15,0,17.65,7.94])],
      [stock('Daimler', 777, 27-2-98, 91, [])],
      [stock('Daimler', 777, 3-3-98, 97, [])],
      [stock('Daimler', 777, 27-3-98, 97.5, [])],
      [stock('Daimler', 777, 30-3-98, 95, [])] ],
      _Ranking).
%  Ranking = [
%     [stock('IBM', 585, 26-3-98, 676,
%         [159.85, -0.62, 0.45, 18.6, 8.51])],
%     [stock('IBM', 585, 25-3-98, 680.2,
%         [159.406, -0.7, -0.7, 19.54, 10.96])],
%     [stock('IBM', 585, 27-3-98, 675.5,
%         [156.017, -0.07, -0.59, 18.82, 7.22])],
%     [stock('IBM', 585, 31-3-98, 680,
%         [151.385, 0.15, 0, 17.65, 7.94])] ].

/*
test(stock_tool, dax_regular) :-
   dax('98_04_20',regular).

test(stock_tool, dax_grouped) :-
   dax('98_04_20',grouped).

test(stock_tool, ghostview_values) :-
   ghostview_values('Test',green,0.2,
      [[1,1],[2,2]], [[3,3],[4,4]], [2,3], [4,6],
       [1-2,4-5,6-2]).
*/

test(stock_tool, date_to_time_stamp) :-
   date_to_time_stamp(31-3-98,Timestamp),
   Timestamp = 90.


/******************************************************************/


