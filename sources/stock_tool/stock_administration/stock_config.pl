


/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Configuration                        ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      stock/1, dax_100/1.

:- multifile
      stock/1, single_data/2.


/*** interface ****************************************************/


% dsa_variable(end_date, '15.01.2010').
dsa_variable(end_date, '23.05.2003').


:- generate_interval(6, 28, Is),
   vector_multiply(0.1, Is, Js),
   dislog_todays_date(Today),
   dislog_variables_set([
      stock_mode - xpce,       % regular,
      ghostview_tool - gv,     % ghostview,
      dax_date - Today,
      dax_date_default - Today,
      euro_to_dm - 1.95583,
      change_euro_to_dm - yes,
      portfolio - no,
      smooth_parameter - 3,
      stock_chart_thin - 5,
      thin_parameter - 7,
      chart_mode - regular,
%     y_axis_mode - logarithmic,
      y_axis_mode - linear,
      storage_mode - mysql,
      dsa_stock_database - finanztreff,
%     x_lines - [0.7,1.0,1.1,1.5,2.0],
      x_lines - Js,
      x_line_colour - colour(grey),
      y_line_dots - 34,
      y_line_colour - colour(grey),
      y_unit_colour - colour(grey),
      x_year_colour - colour(grey),
      curve_style - lines,
      curve_type - values,
      chart_start - 1997,
      dsa_chart_origin - [3,20],
      stock_preferences - [1.5,20,100,20],
      show_avg_line - yes,
      avg_line_colour - colour(gold2),
%     avg_line_colour - colour(orange1),
%     avg_line_colour - colour(indianred),
%     avg_line_colour - colour(gold2),
%     avg_line_colour - colour(darkgoldenrod1),
      smooth_parameter_avg - 25 ]).

:- ( dislog_variable_get(screen_size, small) ->
     dislog_variables_set([
        x_factor - 12.5,
        font_size - 10 ])
   ; dislog_variables_set([
        x_factor - 25,
        font_size - 18 ]) ).

:- dislog_variable_get(dax_date, Date),
   dislog_variables_set([
      currency - 'DM',
      spool_source - 'Finanztreff',
%     spool_source_version - 'Finanztreff 2003',
      spool_source_version - 'Finanztreff 2009',
      spool_mode - 'W3 > PL',
      spool_start_date - Date,
      spool_end_date - Date ]).

:- dislog_variable_get(stock_preferences, Preferences),
   sum_list(Preferences, Divisor),
   divide_list(Preferences, Divisor, List),
   vector_multiply(100, List, Normed_Preferences),
   vectors_multiply(List, [365,52,12,1], Weights),
   dislog_variables_set([
      stock_preferences_normed - Normed_Preferences,
      stock_weights - Weights ]).


dsa_variable(chart_y_range,[0.5,2.5]).
% dsa_variable(chart_y_range,[0.5,1.7]).
dsa_variable(chart_y_width,0.1).

dsa_variable(chart_x_range,[Day_1,Day_2]) :-
   dsa_variable(start_day,Day_1),
   dsa_variable(end_day,Day_2).

% dsa_variable(end_day,15-2-106).
dsa_variable(end_day, D-M-Y) :-
   dsa_variable(end_date, Date),
   name_split_at_position(["."], Date, [D2, M2, Y2]),
   maplist( atom_to_number,
      [D2, M2, Y2], [D, M, Y3] ),
   Y is Y3 - 1900. 

dsa_variable(start_day,15-1-Y) :-
   dislog_variable_get(chart_start,Year),
   Y is Year - 1900.


dsa_variable(dates,Days) :-
   dsa_variable(chart_x_range,Interval),
   day_interval_to_firsts(Interval,Days).
dsa_variable(dates_to_print,Days_To_Print) :-
   dsa_variable(dates,Days),
   findall( Day_To_Print,
      ( member(Day,Days),
        day_to_day_to_print(Day,Day_To_Print) ),
      Days_To_Print ).
dsa_variable(percents,Percents) :-
   Percents = [0.5, 0.7, 1, 1.5, 2, 2.5],
   !.
dsa_variable(percents,Percents) :-
   dsa_variable(chart_y_range,Interval),
   dsa_variable(chart_y_width,Width),
   equidistant_partition(Interval,Width,Percents).


dsa_variable(x_layout,[Start,Schrink]) :-
   dsa_variable(chart_x_range,[D1,D2]),
   date_to_time_stamp(D1,Start),
   date_to_time_stamp(D2,End),
   round( ( End - Start ) / 23, 1, Schrink ).
dsa_variable(y_layout,[Start,Schrink]) :-
   dsa_variable(chart_y_range,[Start,End]),
   round( ( End - Start ) * 1.2, 1, Schrink).
   

dsa_variable(label_size,'small') :-
   dislog_variable_get(chart_start,Year),
   Year >= 2000,
   !.
dsa_variable(label_size,'tiny').


dsa_variable(chart_print_parameters,Parameters) :-
   dislog_variable_get(chart_display_mode,xpce),
   !,
%  rgb_to_colour([7,7,13],Colour),
   rgb_to_colour([9,9,14],Colour),
   Parameters = [Colour, 2].
%  ( ( dislog_variable_get(curve_style,points),
%      !,
%      rgb_to_colour([7,7,13],Colour),
%      Parameters = [Colour, 2] )
%  ; Parameters = [colour(blue), 2] ).
dsa_variable(chart_print_parameters,Parameters) :-
   Parameters = [red, 0.08].


dsa_variable(stock_items_to_colors,Pairs) :-
   stock_branch_colors(Pairs_2),
   Default = default - magenta,
   Pairs = [Default|Pairs_2].

dsa_variable(stock_print_colors,Colours) :-
   dislog_variable_get(chart_display_mode,xpce),
   !,
   maplist( rgb_to_colour,
%     [ [14,7,7], [7,13,7], [7,7,13],
%       [13,13,7], [13,7,13], [7,13,13],
%       [8,8,8],
%       [15,10,6], [6,10,15], [10,6,15],
%       [10,15,6], [15,6,10], [6,15,10] ],
      [ [14,6,6], [14,12,1], [6,6,14],
        [14,10,10], [12,14,6], [10,10,14],
        [11,11,11],
        [15,10,6], [6,10,15], [10,6,15],
        [10,15,6], [15,6,10], [6,15,10] ],
      Colours ).
dsa_variable(stock_print_colors,Colors) :-
   Colors = [
      red, green, blue,
      orange, magenta, yellow,
      cyan, black, white ].
 

/* dsa_name_to_number(Name,Number) <-
      */

dsa_name_to_number(
   'DJ EURO STOXX50 P',1000001).
dsa_name_to_number(
   'DJ STOXX50 P',1000002).
dsa_name_to_number(
   'DJ EURO STOXX P',1000003).
dsa_name_to_number(
   'DJ STOXX P',1000004).


/* stock_item_to_color(Item,Color) <-
      */

stock_item_to_color(Item,Color) :-
   dsa_variable(stock_items_to_colors,Pairs),
   ( ( member(Item-Color,Pairs), ! )
   ; ( member(default-Color,Pairs) ) ).


/* stock_print_parameters(Item,
         [Color,Circle_Diameter,X_Offset,X_Schrink]) <-
      indicates the way a stock item Item is printed
      in a chart. */

stock_print_parameters(Item,Parameters) :-
   special_dax_value(Item,_,_,Parameters),
   !.
stock_print_parameters(_,Parameters) :-
   dsa_variable(chart_print_parameters,Parameters_1),
   dsa_variable(x_layout,Parameters_2),
   append(Parameters_1,Parameters_2,Parameters).


/******************************************************************/


