

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Treatment of Date Values             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dislog_todays_date(Date) <-
      */

dislog_todays_date(Date) :-
   date(date(Year,Month,Day)),
   date_to_natural_date(Day-Month-Year,Date_N),
   name(Date_N,[D1,D2,_,M1,M2,_,_,_,Y3,Y4]),
   name('_',[Underscore]),
   name(Date,[Y3,Y4,Underscore,M1,M2,Underscore,D1,D2]).


/* date_to_natural_date(D-M-Y,Date) <-
      */

date_to_natural_date(D-M-Y,Date) :-
   ( D < 10 ->
     name_append(['0',D,'.'],A)
   ; name_append([D,'.'],A) ),
   ( M < 10 ->
     name_append([A,'0',M,'.'],B)
   ; name_append([A,M,'.'],B) ),
   name_append([B,Y],Date).


/* dax_day(Dax_Day) <-
      */

dax_day(Dax_Day) :-
   dislog_variable_get(dax_date,Date),
   date_to_day(Date,Dax_Day).


/* date_to_day(Date,DD-MM-YY) <-
      */

date_to_day(Date,DD-MM-YY) :-
   name(Date,[Y1,Y2,95,M1,M2,95,D1,D2]),
   name(DD,[D1,D2]), name(MM,[M1,M2]), name(YY,[Y1,Y2]).

date_to_day_string(Date,Day) :-
   name(Date,[Y1,Y2,95,M1,M2,95,D1,D2]),
   Day = [D1,D2,46,M1,M2,46,Y1,Y2].


/* beautify_dax_date(Date_1,Date_2) <-
      transforms DD.MM.YY to DD-MM-YY */

beautify_dax_date(Date_1,Date_2) :-
   name(Date_1,[D1,D2,46,M1,M2,46,Y1,Y2]),
   name(Day,[D1,D2]),
   name(Month,[M1,M2]),
   name(Year,[Y1,Y2]),
   Date_2 = Day-Month-Year.


/* days_in_year(Days,Year) <-
      */

days_in_year(366,Year) :-
   special_year(Year),
   !.
days_in_year(365,_).


/* days_in_month(Days,Month-Year) <-
      */

days_in_month(Days,N-_) :-
   N \= 2,
   !,
   usual_days_in_month(Days_List),
   nth(N,Days_List,Days).
days_in_month(29,2-Year) :-
   special_year(Year),
   !.
days_in_month(28,2-_).


/* usual_days_in_month(List_of_Days) <-
      */

usual_days_in_month([31,28,31,30,31,30,31,31,30,31,30,31]).


/* special_year(Year) <-
      */

special_year(Year) :-
   divisible_by(Year,4),
   ( ( \+ divisible_by(Year,100) )
   ; divisible_by(Year,400) ).


/* minus_one_week(D1-M1-Y1,D2-M2-Y2) <-
      */

minus_one_week(D-M-Y,D1-M-Y) :-
   D > 7,
   !,
   D1 is D - 7.
minus_one_week(D-M-Y,D1-M1-Y1) :-
   !,
   ( ( M = 1,
       M1 = 12,
       Y1 is Y - 1 )
   ; ( M1 is M - 1,
       Y1 = Y ) ),
   days_in_month(N,12-Y1),
   D1 is D + N - 7.


/* minus_four_weeks(D1-M1-Y1,D2-M2-Y2) <-
      */

minus_four_weeks(D-M-Y,D1-M-Y) :-
   D > 28,
   !,
   D1 is D - 28.
minus_four_weeks(D-1-Y,D1-12-Y1) :-
   !,
   Y1 is Y - 1,
   days_in_month(N,12-Y1),
   D1 is D + N - 28.
minus_four_weeks(D-M-Y,D1-M1-Y) :-
   M1 is M - 1,
   days_in_month(N,M1-Y),
   D1 is D + N - 28.


/* plus_one_year(D1-M1-Y1,D2-M2-Y2) <-
      */

plus_one_year(D-M-Y1,D-M-Y2) :-
   add(Y1,1,Y2).


/* minus_one_year(D1-M1-Y1,D2-M2-Y2) <-
      */

minus_one_year(D-M-Y1,D-M-Y2) :-
   add(Y2,1,Y1).


/* plus_one_month(D1-M1-Y1,D2-M2-Y2) <-
      */

plus_one_month(D-12-Y1,D-1-Y2) :-
   !,
   add(Y1,1,Y2).
plus_one_month(D-M1-Y,D-M2-Y) :-
   add(M1,1,M2).


/* stock_state_to_time_values(State,Time_Values) <-
      */

stock_state_to_time_values([C_1|Cs_1],[C_2|Cs_2]) :-
   C_1 = [stock(_,_,Date_1,Value_1,_)],
   date_to_time_stamp(Date_1,Timestamp_1),
   C_2 = Timestamp_1-Value_1,
   stock_state_to_time_values(Cs_1,Cs_2).
stock_state_to_time_values([],[]).


/* time_stamp_to_date(Timestamp,Date) <-
      converts an integer number Timestamp into a date D-M-Y. */

time_stamp_to_date(T,Date) :-
   T > 0,
   !,
   time_stamp_to_year_and_rest(1998,T,Year,Rest),
   time_stamp_to_date_within_year(Year,Rest,Date).
time_stamp_to_date(T,Date) :-
   T =< 0,
   !,
   time_stamp_to_year_and_rest_minus(1998,T,Year,Rest),
   time_stamp_to_date_within_year(Year,Rest,Date).

time_stamp_to_year_and_rest_minus(Y,T,Y,T) :-
   T > 0,
   !.
time_stamp_to_year_and_rest_minus(Y,T,Year,Rest) :-
   T =< 0,
   days_in_year(Ds,Y),
   T2 is T + Ds,
   Y2 is Y - 1,
   time_stamp_to_year_and_rest_minus(Y2,T2,Year,Rest).


time_stamp_to_year_and_rest(Y,T,Y,T) :-
   days_in_year(Ds,Y),
   Ds >= T,
   !.
time_stamp_to_year_and_rest(Y,T,Year,Rest) :-
   days_in_year(Ds,Y),
   Ds < T,
   T2 is T - Ds,
   Y2 is Y + 1,
   time_stamp_to_year_and_rest(Y2,T2,Year,Rest).


time_stamp_to_date_within_year(Y,T,Ds-M-Y) :-
   time_stamp_to_date_within_year(Y,T,1,Ds-M-Y).

time_stamp_to_date_within_year(Y,T,M,T-M-Y) :-
   days_in_month(Ds,M-Y),
   Ds >= T,
   !.
time_stamp_to_date_within_year(Y,T,M,Date) :-
   days_in_month(Ds,M-Y),
   Ds < T,
   T2 is T - Ds,
   M2 is M + 1,
   time_stamp_to_date_within_year(Y,T2,M2,Date).

 
/* date_to_time_stamp(D-M-Y,T) <-
      converts the date D-M-Y into a time stamp T,
      which is given by an integer number. */

date_to_time_stamp(D-M-Y,T) :-
   Y =< 200,
   !,
   Year is Y + 1900,
   date_to_time_stamp_long(D-M-Year,T).
date_to_time_stamp(D-M-Y,T) :-
   date_to_time_stamp_long(D-M-Y,T).

date_to_time_stamp_long(D-1-1998,D) :-
   !.
date_to_time_stamp_long(D-M-Y,T) :-
   Y < 1998,
   !,
   plus_one_month(D-M-Y,D1-M1-Y1),
   days_in_month(N,M-Y),
   date_to_time_stamp_long(D1-M1-Y1,T1),
   add(T,N,T1).
date_to_time_stamp_long(D-M-Y,T) :-
   Y >= 1998,
   !,
   plus_one_month(D1-M1-Y1,D-M-Y),
   days_in_month(N,M1-Y1),
   date_to_time_stamp_long(D1-M1-Y1,T1),
   add(T1,N,T).


/* day_in_date_interval(Day,[Date_1,Date_2]) <-
      */

day_in_date_interval(Day,[Date_1,Date_2]) :-
   date_to_day(Date_1,Day_1),
   date_to_day(Date_2,Day_2),
   day_leq(Day_1,Day),
   day_leq(Day,Day_2).


/* date_in_date_interval(Date,[Date_1,Date_2]) <-
      */

date_in_date_interval(Date,[Date_1,Date_2]) :-
   date_leq(Date_1,Date),
   date_leq(Date,Date_2).


/* date_leq(Date1,Date2) <-
      */

date_leq(Date1,Date2) :-
   date_to_day(Date1,Day1),
   date_to_day(Date2,Day2),
   day_leq(Day1,Day2).


/* day_leq(D1-M1-Y1,D2-M2-Y2) <-
      */

day_leq(D1-M1-Y1,D2-M2-Y2) :-
   ( ( Y1 < Y2,
       ! )
   ; ( Y1 = Y2, M1 < M2,
       ! )
   ; ( Y1 = Y2, M1 = M2, D1 =< D2 ) ).


/* date_allowed(Date) <-
      succeeds if Date starts with one of '97', '98',
      '99'. */

date_allowed(Date) :-
   name(Date, List),
   member(Year, [ '97', '98', '99', '00',
      '01', '02', '03', '04', '05',
      '06', '07', '08', '09', '10' ]),
   name(Year, L),
   append(L,_,List).


/* day_interval_to_firsts <-
      */

day_interval_to_firsts([_-M1-Y1,D2-M2-Y2],[D3-M3-Y3|Days]) :-
   plus_one_month(1-M1-Y1,D3-M3-Y3),
   day_leq(D3-M3-Y3,D2-M2-Y2),
   day_interval_to_firsts([D3-M3-Y3,D2-M2-Y2],Days).
day_interval_to_firsts(_,[]).


/* day_to_day_to_print(D-M-Y,YY) <-
      */

day_to_day_to_print(_-7-Y,YY) :-
   !,
   XX is Y + 1900,
%  name(XX,[_,_,N3,N4]),
%  append(["`",[N3,N4],"'"],L),
%  name(YY,L).
   YY = year(XX).
day_to_day_to_print(_-M-_,MM) :-
   concat('        ',M,MM).


/* date_difference(Date_1,Date_2,Days) <-
      */

date_difference(Date_1,Date_2,Days) :-
   Date_1 = date(Y1,M1,D1),
   date_to_time_stamp(D1-M1-Y1,Days_1),
   natural_date_to_date(Date_2,D2-M2-Y2),
   date_to_time_stamp(D2-M2-Y2,Days_2),
   Days is Days_1 - Days_2.


/* natural_date_to_date(Date,D-M-Y) <-
      */

natural_date_to_date(Date,D-M-Y) :-
   name(Date,[D1,D2,46,M1,M2,46,Y1,Y2,Y3,Y4]),
   name(D,[D1,D2]),
   name(M,[M1,M2]),
   name(Y,[Y1,Y2,Y3,Y4]).


/******************************************************************/


