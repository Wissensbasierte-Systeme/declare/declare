

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Elementary Predicates                ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


dsa_variable_get(Variable,Value) :-
   dsa_variable(Variable,Value).

dsa_variable_set(Variable,Value) :-
   retractall(dsa_variable(Variable,_)),
   assert(dsa_variable(Variable,Value)).


write_string_list(File,Strings) :-
   concat(Strings,String),
   write(File,String).

write_string_list(Strings) :-
   concat(Strings,String),
   write(String).


writeln_as_name(S) :-
   name(Name,S),
   writeln(Name).

writeln_as_names(File,[X]) :-
   !,
   name(Name,X),
   writeln(File,Name).
writeln_as_names(File,[X|Xs]) :-
   name(Name,X),
   write(File,Name),
   write(File,'*'),
   writeln_as_names(File,Xs).
writeln_as_names(_,[]).

write_as_name(X) :-
   name(Name,X),
   write(Name).


/* digit(X) <-
      */

digit(X) :-
   48 =< X,
   X =< 57.


/* in_character_range(A-B,N) <-
      */

in_character_range(A-B,N) :-
   A =< N, N =< B.
in_character_range(A-B,N) :-
   A - 32 =< N, N =< B - 32.


/* name_html_to_text(Name_1, Name_2) <-
      */

name_html_to_text(Name_1, Name_2) :-
   name(Name_1, String_1),
   string_html_to_text(String_1, String_2),
   name(Name_2, String_2).

string_html_to_text(String_1, String_2) :-
   name('&#', Sou),
   string_html_to_text(Sou, String_1, String_2).

string_html_to_text(Sou, String_1, String_2) :-
   append(Sou, [N1,N2,N3,_|String_3], String_1),
   !,
   string_html_to_text(Sou, String_3, String_4),
   N is 100 * (N1-48) + 10 * (N2-48) + (N3-48),
   append([N], String_4, String_2).
string_html_to_text(Sou, [N|String_1], [N|String_2]) :-
   string_html_to_text(Sou, String_1, String_2).
string_html_to_text(_, [], []).


/* functor_and_arguments_to_atom(Functor, Arguments, Atom) <-
      */

functor_and_arguments_to_atom(Functor, Arguments, Atom) :-
   Atom =.. [Functor|Arguments].


relative_change_for_strings(Old,New,Change) :-
   name(X,Old), name(Y,New),
   relative_change(X,Y,Z),
   name(Z,Change).

relative_change(X,_,'--') :-
   true_equal(X,0),
   !.
relative_change(X,Y,Z) :-
   Z is round( ( ( Y - X ) / X ) * 10000 ) / 100.

true_equal(X,Y) :-
   Z is X - Y,
   Z == 0.


/* stock_consult_pl_files(Files,Date,Relations) <-
      Consults all the stock pl-files in Files in the stock
      directory for Date into a list Relations of relations. */

stock_consult_pl_files(Files,Date,Relations) :-
   stock_file(data,Date,Directory),
   maplist( stock_consult_pl_file(Directory),
      Files, Relations ).

stock_consult_pl_file(Directory,Filename,Relation) :-
   name_append([Directory,'/',Filename],Path),
   stock_consult_pl_file(Path,Relation_2),
   dislog_variable_get(change_euro_to_dm,YN),
   relation_euro_to_dm(YN,Relation_2,Relation).


stock_consult_pl_file(File,Relation) :-
   stock_consult_pl_file(File),
   collect_arguments(stock,Relation),
   dabolish(stock/1).


stock_consult_pl_file(File) :-
   exists_file(File),
   writeln_list(['<--- input file: ']),
   !,
   consult(File).
stock_consult_pl_file(File) :-
   writeln_list(['<--- input file: ',File,
      'file does not exist !']).


/* stock_data_dates(Dates_I,Dates_L) <-
      Given a date interval Dates_I = [Date1,Date2], finds the
      list Dates_L of all dates Date in between Date1 and Date2
      such that there exists a dax-subdirectory for Date. */

stock_data_dates(Dates_I,Dates_L) :-
   stock_data_dates(Dates),
   findall( Date,
      ( member(Date,Dates),
        date_in_date_interval(Date,Dates_I) ),
      Dates_L ).

/* stock_data_dates(Dates_L) <-
      Finds the list Dates_L of all dates Date
      such that there exists a dax-subdirectory for Date. */

stock_data_dates(Dates_L) :-
   stock_file(data,Directory),
   directory_contains_files(Directory,Directories),
   sublist( date_allowed,
      Directories, Dates_L ).


/******************************************************************/


