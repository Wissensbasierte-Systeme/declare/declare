

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Nicer Names for Stocks               ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      name_to_nice_name_tuple/2.
 
:- multifile
      name_to_nice_name_tuple/2.


/******************************************************************/


/* beautify_stock_atoms(Atoms1,Atoms2) <-
      */

beautify_stock_atoms(Atoms1,Atoms2) :-
   maplist( beautify_stock_atom,
      Atoms1, Atoms2 ).

beautify_stock_atom(Atom1,Atom2) :-
   Atom1 =.. [stock,Name1,Number,Date1|Xs],
   beautify_dax_name(Name1,Name2),
   beautify_dax_date(Date1,Date2),
   Atom2 =.. [stock,Name2,Number,Date2|Xs].


beautify_stock_atoms_2(Atoms1,Atoms2) :-
   stock_consult(number_name,Number_Name_State),
   maplist( beautify_stock_atom_2(Number_Name_State),
      Atoms1, Atoms2 ).

beautify_stock_atom_2(State,Atom1,Atom2) :-
   Atom1 =.. [stock,Name1,Number,Date1|Xs],
   ( stock_number_to_name(State,Number,Name2)
   ; beautify_dax_name(Name1,Name2) ),
   beautify_dax_date(Date1,Date2),
   Atom2 =.. [stock,Name2,Number,Date2|Xs].
   

/* beautify_dax_name(Name1,Name2) <-
      */

beautify_dax_name(Name1,Name2) :-
   name(Name1,S1),
   reverse(S1,S2),
   beautify_string_at_front(S2,S3),
   beautify_string_at_front_2(S3,S4),
   beautify_string_at_front(S4,S5),
   reverse(S5,S6),
   name(Name2,S6).
   
beautify_string_at_front_2(S1,S2) :-
   name('[Chart]',S3),
   reverse(S3,S4),
   append(S4,S2,S1),
   !.
beautify_string_at_front_2(S,S).

beautify_string_at_front([32|Xs],Ys) :-
   !,
   beautify_string_at_front(Xs,Ys).
beautify_string_at_front(Xs,Xs).


/* name_to_nice_name(Name_1,Name_2) <-
      */

name_to_nice_name(Name_1,Name_2) :-
   name_to_nice_name_tuple(Name_1,Name_3),
   treat_backslashes_in_name(Name_3,Name_2),
   !.
name_to_nice_name(Name_1,Name_2) :-
   treat_backslashes_in_name(Name_1,Name_2),
   !.
 
name_to_nice_name_special(Name_1,Name_2) :-
   special_dax_value(Name_2,_,Name_1,_),
   !.
name_to_nice_name_special(Name_1,Name_2) :-
   name_to_nice_name(Name_1,Name_2).


/******************************************************************/


