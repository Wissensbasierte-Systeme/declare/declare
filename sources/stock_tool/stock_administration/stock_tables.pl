

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Tables for Visualization             ***/
/***                                                            ***/
/******************************************************************/



/* stock_state_to_xpce_table(State) <-
      */

stock_state_to_xpce_table(State) :-
   stock_state_to_company_name(State,Name),
   stock_state_to_xpce_table(Name,State).
   
stock_state_to_xpce_table(Name,State) :-
   maplist( stock_clause_to_tuple,
      State, Tuples ),
   reverse(Tuples,Rows),
   xpce_display_table(_,_,Name,
      ['Name','Wkn','Date','Value','D','W','M','Y'],
      Rows ).
   
stock_state_to_company_name([[stock(Name,_,_,_,_)]|_],Name) :-
   !.
stock_state_to_company_name(_,'???').


/* xpce_display_data_volume_table <-
      */

xpce_display_data_volume_table :-
   xpce_display_data_volume_table(_,_).

xpce_display_data_volume_table(T,W) :-
   stock_spooled_tuples([_,_],Tuples),
   maplist( stock_spooled_tuple_date_to_natural_date,
      Tuples, Tuples_2 ),
   reverse(Tuples_2,Rows),
   xpce_display_table(T,W,'SMS Data Volume',
%  html_display_table('SMS Data Volume',
      [ 'Date', 'Volume' ],
      Rows ).

stock_spooled_tuple_date_to_natural_date(
      [N,Y-M-D],[Date,N]) :-
   date_to_natural_date(D-M-Y,Date).


/* xpce_display_portfolio_table <-
      */

xpce_display_portfolio_table :-
   xpce_display_portfolio_table(_,_).

xpce_display_portfolio_table(T,W) :-
   findall( [Portfolio,Company,Wkn,Date,Value,Quantity],
      ( stock_portfolio_tuple(Tuple),
        [Portfolio,Members] := Tuple^[owner,members],
        member(Member,Members),
        [Company,Value,Quantity] :=
           Member^[company,value,quantity],
        maplist( get_field_value_or_default(Member),
           [wkn,date], [Wkn,Date] ) ),
      Rows ),
   xpce_display_table(T,W,'SMS Portfolios',
      [ 'Name', 'Company', 'Wkn', 'Date', 'Value', 'Quantity' ],
      Rows ).
        
get_field_value_or_default(O,V,A) :-
   A := O^V,
   !.
get_field_value_or_default(_,_,'--').


/******************************************************************/


