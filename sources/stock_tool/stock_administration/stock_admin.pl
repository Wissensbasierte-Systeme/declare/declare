

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Administration Tool                  ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      stock_data/1,
      stock_data/2,
      stock_data/3.


/*** interface ****************************************************/


/* stock_file_beautify(File_1,File_2) <-
      */

stock_file_beautify(File_1,File_2) :-
   dconsult( File_1, State ),
   disave( stock_fact_proper_write,
      State, File_2 ).


/* stock_data_change_values(Name) <-
      */

stock_data_change_values(Name) :-
   mysql_select_big_changes(Name,Pairs),
   mysql_select_name_to_number(Name,[[number_name(Wkn,Name)]]),
   checklist( stock_data_change_value(Wkn),
      Pairs ).
   
stock_data_change_value(Wkn,[Day_1-_,Day_2-_,Factor]) :-
   days_are_close_enough(Day_1,Day_2),
   round_factor(Factor,Rate),
   !,
   day_to_sql_date(Day_2,SQL_Date),
   writeln(user,
      stock_data(change_value,[Wkn,SQL_Date,Rate])),
   add_stock_data_change_value_to_change_file(Wkn,SQL_Date,Rate),
   mysql_select_number_name(Number_Name_State),
   stock_number_to_first_name(Number_Name_State,Wkn,Company),
   writeln(user,
      add_stock_multiply_tuple_to_update_file(Company,Wkn,Rate)),
   add_stock_multiply_tuple_to_update_file(Company,Wkn,Rate),
   stock_data(change_value,[Wkn,SQL_Date,Rate]).
stock_data_change_value(_,_).

days_are_close_enough(Day_1,Day_2) :-
   date_to_time_stamp(Day_1,Stamp_1),
   date_to_time_stamp(Day_2,Stamp_2),
   !,
   abs(Stamp_1-Stamp_2) < 5.

round_factor(Factor,Rate) :-
   Factor >= 1.9, !,
   round(Factor,0,Rate).
round_factor(Factor,Rate) :-
   Factor =< 0.7,
   Factor > 0,
   round(1/Factor,0,Inverse_Rate),
   Rate is 1 / Inverse_Rate.


/* add_stock_data_change_value_to_change_file(
         Wkn,SQL_Date,Rate) <-
      */

add_stock_data_change_value_to_change_file(Wkn,SQL_Date,Rate) :-
   name_remove_elements(" ",SQL_Date,SQL_Date_2),
   stock_file( sources_directory,
      'stock_knowledge_base/changes', File ),
   stock_file( results, changes_add, File_Add ),
%  stock_file( results, changes_new, File_New ),
   name_append(['stock_data_change_value([ ',
      Wkn, ', ''', SQL_Date_2, ''', ', Rate, ' ]).'],Line),
   predicate_to_file(File_Add,writeln(Line)),
   concat_files(File,File_Add,File),
   reconsult(File).


/* add_stock_multiply_tuple_to_update_file(
         Company,Wkn,SQL_Date,Rate) <-
      */

add_stock_multiply_tuple_to_update_file(Company,Wkn,Rate) :-
   multiply_rate_with_current_rate(Wkn,Rate,Rate_2),
   date(date(Y,M,D)),
   stock_file( sources_directory,
      'stock_knowledge_base/updates.pl', File ),
   stock_file( results, updates_add, File_Add ),
%  stock_file( results, updates_new, File_New ),
   name_append( [ '\n',
      'stock_multiply_tuple( \n   ''', Company, ''', ',
          Wkn, ', [', D, '-', M, '-', Y, ',_], ',
          Rate_2, ' ).' ], Line ),
   predicate_to_file(File_Add,writeln(Line)),
   concat_files(File_Add,File,File),
   reconsult(File).

multiply_rate_with_current_rate(Wkn,Rate_1,Rate_2) :-
   stock_multiply_tuple(_,Wkn,Rate),
   !,
   Rate_2 is Rate * Rate_1.
multiply_rate_with_current_rate(_,Rate,Rate).


/* day_to_sql_date(Day,SQL_Date) <-
      */

day_to_sql_date_2(Day,'--') :-
   var(Day),
   !.
day_to_sql_date_2(Day,SQL_Date) :-
   Day = D-M-Y,
   number_to_two_digits(D,DD),
   number_to_two_digits(M,MM),
   year_to_sql_year(Y,SQL_Year),
   name_append([' ',DD,'.',MM,'.',SQL_Year,' '],X),
   name_remove_elements(" ",X,SQL_Date).

day_to_sql_date(Day,SQL_Date) :-
   Day = D-M-Y,
   number_to_two_digits(D,DD),
   number_to_two_digits(M,MM),
   year_to_sql_year(Y,SQL_Year),
   name_append([' ',SQL_Year,'-',MM,'-',DD,' '],SQL_Date).
 
year_to_sql_year(YY,SQL_YY) :-
   YY < 50,
   !,
   SQL_YY is YY + 2000.
year_to_sql_year(YY,YY) :-
   YY > 1900,
   !.
year_to_sql_year(YY,SQL_YY) :-
   SQL_YY is YY + 1900.


/* mysql_select_big_changes_to_table(Name) <-
      */

mysql_select_big_changes_to_table(Name) :-
   mysql_select_big_changes(Name,Triples),
   maplist( prepare_big_changes_tuple_for_table,
      Triples, Rows ),
   xpce_display_table(_,_,Name,
      ['Date 1','Value 1','Date 2','Value 2','Ratio','Q'],
      Rows ).
   
prepare_big_changes_tuple_for_table(
      [ D1-M1-Y1-Value_1, D2-M2-Y2-Value_2, Ratio ],
      [ Date_1, Value_1, Date_2, Value_2, Ratio, Q ] ) :-
   Ya is Y1 + 1900,
   Yb is Y2 + 1900,
   date_to_natural_date(D1-M1-Ya,Date_1), 
   date_to_natural_date(D2-M2-Yb,Date_2),
   find_closest_quotient(10,Ratio,Q).


/* find_closest_quotient(K,X,Q) <-
     Among all pairs (N,M), where N and M are between 1 and K,
     it finds the pair whose quotient N/M is the closest to X.
     The result Q is the name 'N/M'. */

find_closest_quotient(_,0,'--') :-
   !.
find_closest_quotient(K,X,Q) :-
   ( ( X >= 1,
       Y = X )
   ; Y is 1 / X ),
   generate_interval(1,K,I),
   vector_multiply(Y,I,J),
   find_minimum(K,J,_:N/M),
   ( ( X >= 1,
       name_append([M,'/',N],Q) )
   ; name_append([N,'/',M],Q) ).

find_minimum(K,[X|Xs],D2:N/M) :-
   round(X,0,Z),
   minimum(K,Z,Y),
   D1 is abs(1-Y/X),
   find_minimum(K,Xs,2,D1:1/Y,D2:N/M).

find_minimum(K,[X|Xs],N,D1:N1/Y1,D2:N2/Y2) :-
   round(X,0,Z),
   minimum(K,Z,Y),
   D is abs(1-Y/X),
   ( ( D1 =< D,
       D3 = D1,
       N3/Y3 = N1/Y1 )
   ; ( D1 > D,
       D3 = D,
       N3/Y3 = N/Y ) ),
   M is N + 1,
   find_minimum(K,Xs,M,D3:N3/Y3,D2:N2/Y2).
find_minimum(_,[],_,D:N/M,D:N/M).


/* mysql_select_big_changes(Name,Pairs) <-
      */

mysql_select_big_changes(Name,Pairs) :-
   mysql_select_big_changes(Name,[0.7,1.5],Pairs).

mysql_select_big_changes(Name,[Min,Max],Pairs) :-
   mysql_select_by_name(Name,State_1),
   stock_state_sort_by_date(State_1,State_2),
   findall( [Date_1-Value_1,Date_2-Value_2,Q],
      ( append( _,
           [ [stock(Name, _, Date_1, Value_1, _)],
             [stock(Name, _, Date_2, Value_2, _)] | _ ],
           State_2 ),
        nonvar(Value_1), nonvar(Value_2), Value_2 > 0,
        Q is Value_1 / Value_2,
        ( Q >= Max 
        ; Q =< Min ) ),
      Pairs ).


/* stock_spool_from_infile <-
      */

stock_spool_from_infile :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   name_append([
      'use stock; load data infile "/tmp/spool_data" ',
      'into table ',DSA_Stock_Database,';' ], Sql_Command ),
   writeln(user,Sql_Command),
   mysql_execute(Sql_Command).


/* stock_spool_into_outfile(Date) <-
      */

stock_spool_into_outfile(Date) :-
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   name_append([
      'use stock; select * from ',DSA_Stock_Database,' ',
      'where date >= ''',Date,''' order by date ',
      'into outfile ''/tmp/spool_data'';' ], Sql_Command ),
   writeln(user,Sql_Command),
   mysql_execute(Sql_Command).


/* stock_spooled_tuples([Date_1,Date_2],Tuples) <-
      */

stock_spooled_tuples([Date_1,Date_2],Tuples) :-
   ( ( var(Date_1), var(Date_2), !,
       Where = [] )
   ; Where = [ date >= {Date_1} and date =< {Date_2} ] ),
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   mysql_select_execute([
      use:[ stock ],
      select:[ count(*), date ],
      from:[ DSA_Stock_Database ],
      where:Where,
      group_by:[ date ] ],
      Tuples ).


/* stock_spooled_values([Date_1,Date_2],State) <-
      */

stock_spooled_values([Date_1,Date_2],State) :-
   stock_spooled_tuples([Date_1,Date_2],Tuples),
   !,
   maplist( mysql_result_spooled_tuple_to_stock_clause,
      Tuples, State ).
% stock_spooled_values(Date,State) :-
%    stock_spooled_values([Date,Date],State).

mysql_result_spooled_tuple_to_stock_clause([Value,Date],[A]) :-
   mysql_date_to_dsa_date(Date,Date_2),
   dislog_variable_get(euro_to_dm,Rate),
   Value_2 is Value * Rate,
   A =.. [stock,x,x,Date_2,Value_2,[0,0,0,0]].


/* stock_add_missing_number_name <-
      */

stock_add_missing_number_name :-
   stock_add_missing_number_name(
      number_name_hs_menue,
      'number_name_hs.sql' ).

stock_add_missing_number_name(File_1,File_2) :-
   stock_file(stock_data,File_1,Path_1),
   stock_file(stock_data,File_2,Path_2),
   diconsult(Path_1,State_1),
   findall( [number_name(Wkn,Name)],
      ( member([number_name(_,Wkn,Name_String)],State_1),
        name(Name,Name_String) ),
      State_2 ),
   list_to_ord_set(State_2,State_3),
   mysql_select_number_name(State_4a),
   list_to_ord_set(State_4a,State_4),
   ord_subtract(State_3,State_4,State_5),
   ord_intersection(State_3,State_4,State_7),
   maplist( length,
      [State_3,State_4,State_5,State_7], Lengths ),
   writeln(Lengths),
   findall( [number_name(Wkn,Name_String)],
      ( member([number_name(Wkn,Name)],State_5),
        name(Name,Name_String) ),
      State_6 ),
   tell(Path_2),
   writeln('use stock;'), nl,
   state_to_sql_insert_statements(number_name,State_6),
   told.

stock_fact(change_date_and_name,C1,C2) :-
   C1 = [stock(Company,Number,Date_1,Value,Previous)],
   stock_change_date(Date_1,Date_2),
   name(Name,Company),
   C2 = [stock(Name,Number,Date_2,Value,Previous)].
   
stock_change_date(D1-M1-Y1,Y2-M2-D2) :-
   maplist( number_to_two_digits,
      [D1,M1,Y1], [D2,M2,Y2] ).

number_to_two_digits(X1,X2) :-
   name(X1,[Y]),
   !,
   string_to_atom([48,Y],X2).
number_to_two_digits(X,X).


/* stock_data(sort_by_date,File_1,File_2) <-
      */

stock_data(sort_by_date,File_1,File_2) :-
   maplist( stock_file(stock_data),
      [File_1,File_2], [Path_1,Path_2] ),
   diconsult(Path_1,State_1),
   list_to_ord_set(State_1,State_2),
%  stock_state_sort_by_date(State_1,State_2),
%  maplist( stock_fact(change_date_and_name),
%     State_1, State_2 ),
   disave( stock_fact_proper_write,
      State_2, Path_2 ).


/* stock_data(change_value,[Wkn,Date,Rate]) <-
      */

stock_data(change_value,[Wkn,Date,Rate]) :-
   dislog_variable_get(storage_mode,mysql),
   dislog_variable_get(dsa_stock_database,DSA_Stock_Database),
   name_append([
      'use stock; update ', DSA_Stock_Database,
      ' set value=value*', Rate, ' where wkn=', Wkn,
      ' and date >= ''', Date, ''';'], Sql_Statement ),
   mysql_execute(Sql_Statement).


/* stock_data(change_value,File_1,File_2) <-
     */

stock_data(change_value,File_1,File_2) :-
%  dislog_variable_get(euro_to_dm,Rate),
%  Rate = 2,
   Rate = 26,
   stock_data(change_value,
%     [ 851399-Rate-['99_05_27','99_06_04'],
%       870747-Rate-['99_03_29','99_06_04'] ],
      [ 519000-Rate-['99_08_23','99_12_31'] ],
      File_1, File_2 ).
   
stock_data(change_value,Changes,File_1,File_2) :-
   maplist( stock_file(stock_data),
      [File_1,File_2], [Path_1,Path_2] ),
   diconsult(Path_1,State_1),
   maplist( stock_fact(change_value,Changes),
      State_1, State_2 ),
   disave(State_2,Path_2).

stock_fact(change_value,Changes,C1,C2) :-
   C1 = [stock(Number,Day,Value_1,Previous)],
   member(Number-Factor-Dates,Changes),
   day_in_date_interval(Day,Dates),
   !,
   round( Factor * Value_1, 2, Value_2 ),
   C2 = [stock(Number,Day,Value_2,Previous)].
stock_fact(change_value,_,C,C).


/* number_name(Mode,File_1,File_2,File_3) <-
      */

number_name(Mode,File_1,File_2,File_3) :-
   maplist( stock_file(stock_data),
      [File_1,File_2,File_3], [Path_1,Path_2,Path_3] ),
   diconsult(Path_1,State_1),
   diconsult(Path_2,State_2),
   number_name_compare(Mode,State_1,State_2,State_3),
   disave( stock_fact_proper_write,
      State_3, Path_3 ).

number_name_compare(not_in_menue,State_1,State_2,State_3) :-
   findall( [number_name(Number,Name_2)],
      ( member([number_name(Number,Name_2)],State_2),
        \+ member([number_name([_,_],Number,_)],State_1) ),
      State_3 ).
number_name_compare(analyse_diff,State_1,State_2,State_3) :-
   findall( [number_name_diff(Number,Name_1,Name_2)],
      ( member([number_name([_,_],Number,Name_1)],State_1),
        member([number_name(Number,Name_2)],State_2),
        Name_1 \== Name_2 ),
      State_3 ).
number_name_compare(get_type,State_1,State_2,State_3) :-
   findall( [number_name([Type_2,Date],Number,Name_2)],
      ( member([number_name([Type_1,Date],Number,_)],State_1),
        member([number_name(Number,Name_2)],State_2),
        number_name_change_type(Type_1,Type_2) ),
      State_3 ).
number_name_compare(get_name,State_1,State_2,State_3) :-
   findall( [number_name([Type_1,Date],Number,Name_2)],
      ( member([number_name([Type_1,Date],Number,_)],State_1),
        member([number_name(Number,Name_2)],State_2) ),
      State_3 ).
number_name_compare(stock_add_name,State_1,State_2,State_3) :-
   findall( [stock(Name,Number,Date,Value,Previous)],
      ( member([stock(Number,Date,Value,Previous)],State_2),
        stock_number_to_name(State_1,Number,Name) ),
      State_3 ).

number_name_compare(change_type,State_1,State_2) :-
   findall( [number_name([Type_2,Date],Number,Name)],
      ( member([number_name([Type_1,Date],Number,Name)],State_1),
        number_name_change_type(Type_1,Type_2) ),
      State_2 ).


/* number_name_change_type(Type_1,Type_2) <-
      */

number_name_change_type(dax100_export,dax_100).
number_name_change_type(neuer_markt_export,neuermarkt).
number_name_change_type(euro50_export,eurostock).
number_name_change_type(pharma_export,pharma).
number_name_change_type(ausland_export,ausland).

% Hoppenstedt

number_name_change_type(dax_100,dax).
number_name_change_type(dax_30,dax).
number_name_change_type(dax_70,dax).
number_name_change_type(mdax,dax).
number_name_change_type(nemax,germany).
number_name_change_type(smax,germany).

number_name_change_type(eurotop_100,europe_g).
number_name_change_type(stoxx_euro_50,europe_g).
number_name_change_type(atx,europe_r).
number_name_change_type(aex,europe_r).
number_name_change_type(cac_40,europe_r).
number_name_change_type(smi,europe_r).

number_name_change_type(ftse_100,england).

number_name_change_type(dow_30,international).
number_name_change_type('s&p_100',international).
number_name_change_type(stoxx_50,international).


/* number_name <-
      */

number_name(nice,File) :-
   name_append([File,'.nice'],File_2),
   number_name(nice,File,File_2).

number_name(nice,File_1,File_2) :-
   maplist( stock_file(stock_data),
      [File_1,File_2], [Path_1,Path_2] ),
   diconsult(Path_1,State_1),
   list_to_ord_set(State_1,State_2),
   disave( stock_fact_proper_write,
      State_2, Path_2 ).

number_name(change_type,File_1,File_2) :-
   maplist( stock_file(stock_data),
      [File_1,File_2], [Path_1,Path_2] ),
   diconsult(Path_1,State_1),
   number_name_compare(change_type,State_1,State_3),
   list_to_ord_set(State_3,State_2),
   disave( stock_fact_proper_write,
      State_2, Path_2 ).


/* stock_fact_proper_write(Fact) <-
      */

stock_fact_proper_write(Fact) :-
   Fact = [number_name(Number,List)],
   name(Name,List),
   write_list(
      ['number_name( ',Number,', "',Name,'" ).']), 
   nl.
stock_fact_proper_write(Fact) :-
   Fact = [stock(Name,Number,Date,Value,Previous)],
   write_list(
      ['stock( "',Name,'", ',Number,', ',Date,', ',Value,', ',
          Previous,' ).']), 
   nl.
stock_fact_proper_write(Fact) :-
   Fact = [number_name(Type,Number,List)],
   name(Name,List),
   write_list(
      ['number_name( ',Type,', ',Number,', "',Name,'" ).']), 
   nl.
stock_fact_proper_write(Fact) :-
   Fact = [number_name_diff(Number,List_1,List_2)],
   name(Name_1,List_1),
   name(Name_2,List_2),
   write_list(
      ['nn(  ',Number,',  "',Name_1,'",  "',Name_2,'"  ).']), 
   nl.


/* number_name_state_pretty(State_1, State_2) <-
      */

number_name_file_pretty :-
   number_name_file_pretty(number_name_ft).

number_name_file_pretty(File) :-
   stock_file(stock_data, File, Path),
   diconsult(Path, State_1),
   number_name_state_pretty(State_1, State_2),
   disave(State_2, Path).

number_name_state_pretty(State_1, State_2) :-
   maplist( number_name_fact_pretty,
      State_1, State_2 ).

number_name_fact_pretty(Fact_1, Fact_2) :-
   Fact_1 = [number_name(Wkn, List)],
   name(Name_2, List),
   concat(['"', Name_2, '"'], Name),
   Fact_2 = [number_name(Wkn, Name)].


/******************************************************************/


