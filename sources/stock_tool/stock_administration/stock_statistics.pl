

assert_test_list(N,X) :-
   storage(_,_,_),
   create_test_list(N,X,List),
   assert(test_list(List)),
   storage(_,_,_).

create_test_list(1,X,X).
create_test_list(N,X,[L,L]) :-
%  storage(_,_,_),
   M is N - 1,
   create_test_list(M,X,L).


storage(List_L,List,List_U) :-
   system:statistics(heap,H1),
   system:statistics(heapused,HU1),
   system:statistics(heaplimit,HL1),
   divide_list([H1,HU1,HL1],94.862,[H,HU,HL]),
   system:statistics(local,L),
   system:statistics(localused,LU),
   system:statistics(locallimit,LL),
   system:statistics(global,G),
   system:statistics(globalused,GU),
   system:statistics(globallimit,GL),
   system:statistics(trail,T),
   system:statistics(trailused,TU),
   system:statistics(traillimit,TL),
   add([H,L,G,T],S),
   add([HU,LU,GU,TU],SU),
   add([HL,LL,GL,TL],SL),
   Divisor is 1024 * 1024,
   divide_list([SL,HL,LL,GL,TL],Divisor,List_L),
   writeln(List_L),
   divide_list([S,H,L,G,T],Divisor,List),
   writeln(List),
   divide_list([SU,HU,LU,GU,TU],Divisor,List_U),
   writeln(List_U).


