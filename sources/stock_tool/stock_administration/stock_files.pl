

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Used Files                           ***/
/***                                                            ***/
/******************************************************************/



/*** used files ***************************************************/


dsa_variable( files(finanztreff-html),
   [ 'dax_100' ] ).
dsa_variable( files(finanztreff-txt),
   [ 'dax100',
     'neuermarkt',
     'eurostock' ] ).
dsa_variable( files(finanztreff-htm-jan),
   [ 'dax_30', 'mdax', 'aex', 'dj', 'eurostoxx50',
     'isdex', 'nasdaq100', 'nemax50', 'nikkei225', 'smax',
     'nmbio', 'nmfin', 'nmind', 'nmindices',
     'nminternet', 'nmit', 'nmmed', 'nmsoftware',
%    'nment',
     'nmtech', 'nmtele' ] ).
dsa_variable( files(finanztreff-htm),
   [ 'aex', 'atx', 'cac40', 'dax', 'devisen', 'dj', 'djd',
     'estoxx', 'eurostoxx50', 'hdax', 'isdex', 'mdax',
     'mid-cap-market', 'nasdaq100', 'nemax', 'nikkei',
     'nmarkt', 'nmbio', 'nment', 'nmfin', 'nmind', 'nmindices',
     'nminternet', 'nmit', 'nmmed', 'nmsoftware',
     'nmtech', 'nmtele', 'smax', 'smi',
%    'sp500us',
     'tecdax' ] ).
dsa_variable( files(finanztreff-htm-2009), Files ) :-
   Wkns = [
      148429,  149002,  149666,  149896,  150398,
      158375,  159090,  159191,  159096,  159194,
      2265377, 2303555, 2303839, 2407420, 3311860,
      526906,  6183435, 6233669 ],
   ( foreach(Wkn, Wkns), foreach(File, Files) do
        concat('indizes_einzelwerte,i,', Wkn, File) ).
dsa_variable( files(comdirekt-export),
   [ 'dax100_export',
     'neuer-markt_export',
     'euro50_export',
     'pharma_export',
     'ausland_export' ] ).
dsa_variable( files(comdirekt-html),
   [ 'comdirekt_dax100',
     'dax100',
     'neuer-markt',
     'euro50',
     'pharma',
     'ausland' ] ).
dsa_variable( files(hoppenstedt-htm),
   [ 'dax_30', 'dax_70', 'dax_100', 'mdax',
     'stoxx_50', 'stoxx_euro_50',
     'dow_30', 's&p_100',
     'eurotop_100', 'ftse_100', 'cac_40',
     'smi', 'aex', 'atx', 'nemax', 'smax' ] ).


dislog_variable_set_number_name :-
   dislog_variable_get(stock_all_long, finanztreff),
   dislog_variable_set(number_name, number_name_ft).
dislog_variable_set_number_name :-
   dislog_variable_get(stock_all_long, comdirekt),
   dislog_variable_set(number_name, number_name_cd).

:- dislog_variable_set(stock_all_long, finanztreff),
   dislog_variable_set_number_name.
 

stock_file(dislog,Directory) :-
   dislog_variable_get(home,Directory).


stock_file(home,Directory) :-
   stock_file(dislog,'projects/Stock_Analysis',Directory).

stock_file(sources_directory,Directory) :-
   stock_file(dislog,'sources/stock_tool',Directory).


stock_file(logo_file,File) :-
   stock_file(home,'logobmp/logo.gif',File).

stock_file(data,Directory) :-
   stock_file(home,data,Directory).
stock_file(stock_data,Directory) :-
   stock_file(home,stock_data,Directory).
stock_file(charts,Directory) :-
   stock_file(home,charts,Directory).
stock_file(results,Directory) :-
   stock_file(home,results,Directory).
stock_file(pattern_search,Directory) :-
   Directory = 'sources/stock_tool/pattern_search'.
%  stock_file(sources_directory,pattern_search,Directory).

stock_file(file,File) :-
   stock_file(results,stock_chart_file,File).
stock_file(file_input,File) :-
   stock_file(results,'stock_chart_file_input.tex',File).
stock_file(file(ps),File) :-
   stock_file(results,'stock_chart_file.ps',File).

stock_file(dir(gif),Directory) :-
   stock_file(results,'gif',Directory).
stock_file(dir(ppm),Directory) :-
   stock_file(home,'mining/gnuplot',Directory).

stock_file(gnuplot_ppm_file,File) :-
   stock_file(dir(ppm),stockpic,File).

stock_file(number_name,File) :-
   dislog_variable_get(number_name,Number_Name),
   stock_file(stock_data,Number_Name,File).
stock_file(number_name_cd,File) :-
   stock_file(stock_data,number_name_cd,File).
stock_file(stock_all_short,File) :-
   stock_file(stock_data,stock_all_short,File).
stock_file(stock_all_long,File) :-
   dislog_variable_get(stock_all_long,Stock_All_Long),
   stock_file(stock_data,Stock_All_Long,File).
stock_file(comdirekt,File) :-
   stock_file(stock_data,comdirekt,File).
stock_file(state,File-state) :-
   dislog_variable_get(stock_all_long,Stock_All_Long),
   stock_file(stock_data,Stock_All_Long,File).


stock_file(data_date,Path) :-
   stock_file(home,Directory),
   dislog_variable_get(dax_date,Date),
   name_append([Directory,'/data/',Date],Path).

stock_file(html,File) :-
   stock_file(data_date,'stock.html',File).
stock_file(pl,File) :-
   stock_file(data_date,'stock.pl',File).
% stock_file(Filename,File) :-
%    stock_file(data_date,Filename,File),

stock_file(eval,File) :-
   stock_file(results,stock_eval,File).
stock_file(eval_input,File) :-
   stock_file(results,'stock_eval_input.tex',File).

stock_file(X,Y,Z) :-
   stock_file(X,U),
   name_append([U,'/',Y],Z).


dsa_variable( files(Type,Extension), Files ) :-
   dsa_variable(files(Type),Files_2),
   maplist( dsa_name_append_extension(Extension),
      Files_2, Files ).

dsa_file_to_pair(Extension_1-Extension_2,File,Pair) :-
   dsa_name_append_extension(Extension_1,File,File_1),
   dsa_name_append_extension(Extension_2,File,File_2),
   Pair = File_1 - File_2.

dsa_name_append_extension(pl,
      'comdirekt_dax100','dax100_export.pl') :-
   !.
dsa_name_append_extension(pl,File_1,File_2) :-
   member(File_1,[
      'dax100', 'neuer-markt', 'euro50',
      'pharma', 'ausland' ]),
   name_append([File_1,'_export.pl'],File_2),
   !.
dsa_name_append_extension(pl,'dax100','dax_100.pl') :-
   !.
dsa_name_append_extension(Extension,File_1,File_2) :-
   name_append([File_1,'.',Extension],File_2).
 

/******************************************************************/


