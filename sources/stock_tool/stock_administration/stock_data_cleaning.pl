

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Data Cleaning                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


stock_data_cleaning(fact_for_same_date,File_1,File_2,State) :-
   stock_file(stock_data,File_1,Path_1),
   stock_file(stock_data,File_2,Path_2),
   diconsult(Path_1,State_1),
   diconsult(Path_2,State_2),
   findall( C1,
      ( C1 = [stock(Number,Date,_,_)],
        C2 = [stock(Number,Date,_,_)],
        member(C1,State_1),
        member(C2,State_2) ),
      State ).

stock_data_cleaning(multiply,[Number,Dates],Factor,Result_File) :-
   stock_consult(stock_all_long,State_1),
   maplist(
      stock_fact_multiply([Number,Dates],Factor),
      State_1, State_2 ),
   stock_file(stock_data,Result_File,Path),
   disave_2(State_2,Path).

stock_fact_multiply([Number,[Date_1,Date_2]],Factor,C1,C2) :-
   C1 = [stock(Number,Date,Value1,Previous)],
   day_in_date_interval(Date,[Date_1,Date_2]),
   !,
   multiply_list([Value1],Factor,[Value2]),
   C2 = [stock(Number,Date,Value2,Previous)].
stock_fact_multiply(_,_,C,C).


stock_data_cleaning(sort_and_remove_doubles,Result_File) :-
   stock_consult(stock_all_long,State_1),
   list_to_ord_set(State_1,State_2),
   stock_file(stock_data,Result_File,Path),
   disave_2(State_2,Path).

stock_data_cleaning(euro_instead_of_dm,Result_File) :-
%  stock_file(stock_all_long,Filename), [Filename],
   stock_consult(stock_all_long,State_1),
   singleton_lists_to_elements(State_1,Atoms),
   assert_all(Atoms),
   findall( [Number,Date,Previous],
      ( stock(Number,Date,_,Previous),
        write(user,'+'), ttyflush ),
      Groups_with_Doubles ),
   list_to_ord_set(Groups_with_Doubles,Groups),
   findall( [stock(Number,Date,Value,Previous)],
      ( member([Number,Date,Previous],Groups),
        write(user,'.'), ttyflush,
        number_date_previous_to_values(
           [Number,Date,Previous],Values),
        member(Value,Values) ),
      State_2 ),
   writeln(user,'|'), ttyflush,
   retract_all(stock(_,_,_,_)),
   stock_file(stock_data,Result_File,Path),
   disave_2(State_2,Path).


stock_data_cleaning(euro_instead_of_dm) :-
   stock_consult(stock_all_long,State_1),
   stock_data_cleaning(euro_instead_of_dm,State_1,State_2),
   state_subtract(State_1,State_2,State_3),
   list_to_ord_set(State_3,State_4),
   stock_file(stock_all_long,File),
   disave_2(State_4,File).


dsa_integrity_constraint(two_names_per_number,Pairs) :-
   stock_consult(number_name,State_2),
   findall( [number_name(Number,Name)],
      ( member([number_name(Number,String)],State_2),
        name(Name,String) ),
      State ),
   findall( [two_names_per_number(Number,Name_1,Name_2)],
      ( member([number_name(Number,Name_1)],State),
        member([number_name(Number,Name_2)],State),
        Name_1 \== Name_2 ),
      Pairs ).


/*** implementation ***********************************************/


stock_data_cleaning(euro_instead_of_dm,State_1,State_2) :-
   dislog_variable_get(euro_to_dm,Factor),
   findall( [stock(Number,Date,Value_2,Previous)],
      ( member([stock(Number,Date,Value_1,Previous)],State_1),
        member([stock(Number,Date,Value_2,Previous)],State_1),
        round( Value_1 - Factor * Value_2, 0, 0 ) ),
      State_2 ),
   length(State_2,Length),
   writeln_list(user,
      [Length,' incorrect tuples with EURO instead of DM']).


number_date_previous_to_values([Number,Date,Previous],Values) :-
   findall( Value,
      stock(Number,Date,Value,Previous),
      Values_with_Doubles ),
   list_to_ord_set(Values_with_Doubles,Values_2),
   remove_incorrect_values(Values_2,Values).

remove_incorrect_values([0|Vs1],Vs2) :-
   !,
   remove_incorrect_values(Vs1,Vs2).
remove_incorrect_values([V|Vs1],Vs2) :-
   dislog_variable_get(euro_to_dm,Factor),
   member(W,Vs1),
   round( W - Factor * V, 0, 0 ),
   !,
   remove_incorrect_values(Vs1,Vs2).
remove_incorrect_values([V|Vs1],[V|Vs2]) :-
   remove_incorrect_values(Vs1,Vs2).
remove_incorrect_values([],[]).


/******************************************************************/


