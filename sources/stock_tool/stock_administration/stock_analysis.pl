

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Analysis of Stocks                   ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dax_ranking(Mode) <-
      */

dax_ranking(Mode) :-
   dislog_variable_get(dax_date,Date),
   dax(Date,Mode),
   stock_file(results,Directory),
   stock_file(eval,File_1),
   stock_file(file,File_2),
   preview_latex_file(Directory,File_1),
   name_append([
      'cp ',File_1,'.ps ',File_2,'.ps'],Command),
   us(Command),
   display_stock_bitmap.

dax :-
   dislog_variable_get(dax_date,Date),
   dax(Date,regular).
dax_g :-
   dislog_variable_get(dax_date,Date),
   dax(Date,grouped).

dax(Date) :-
   dax(Date,regular).
dax_g(Date) :-
   dax(Date,grouped).

dax(Date,Mode) :-
   dislog_variable_set(dax_date,Date),
   stock_to_latex_file(Date,Mode),
   stock_file(results,Directory),
   stock_file(eval,File),
   preview_latex_file(Directory,File),
   !.


/* stock_to_latex_file(Date, Mode) <-
      */

stock_to_latex_file(Date, Mode) :-
   date_to_day(Date, Day),
   write_dax_eval_header(Day, Mode),
   stock_state_select([_,_,Day], State),
   stock_state_weight(Mode, State, Result),
   stock_file(eval_input, File_eval),
   writeln('---> output file: '),
   writeln(File_eval),
   tell(File_eval),
   write_dax_eval_header_latex,
   rankings_to_latex(Mode, Result),
   told.


/*** implementation ***********************************************/


write_dax_eval_header(Day,Mode) :-
   star_line,
   write_list(['   Bewertung der DAX-Aktien:  ',Day]), nl,
   write_dax_eval_header_sub(Mode),
   star_line.

write_dax_eval_header_sub(grouped) :-
   writeln('   nach Branchen gruppiert').
write_dax_eval_header_sub(regular) :-
   writeln('   alle Aktien einzeln').


write_dax_eval_header_latex :-
   dax_day(Day),
   dislog_variable_get(stock_preferences_normed,[D1,W1,M1,Y1]),
   beautify_stock_weights([D1,W1,M1,Y1],[D,W,M,Y]),
   backslash(BS),
   write_list([BS,'begin{verbatim}']), nl,
   bar_line_dax, nl,
   write_list(['Bewertung der DAX-Aktien:  ',Day]), nl, nl,
   write_list(['Gewichte:  Vor-  ',
      'Tag = ',D,   ', Woche = ',W,
      ', Monat = ',M, ', Jahr = ',Y]), nl, nl,
   bar_line_dax,
   write_list([BS,'end{verbatim}']).
   
beautify_stock_weights([X|Xs],[Y|Ys]) :-
   Y is round( X * 10 ) / 10,
   beautify_stock_weights(Xs,Ys).
beautify_stock_weights([],[]).


bar_line_dax :-
   write('----------------------------------'),
   writeln('---------------------------------- ').


/* write_stock_file_pl(File_pl, Atoms) <-
      */

write_stock_file_pl(File_pl, Atoms) :-
   tell(File_pl),
   checklist( writeln_stock_atom,
      Atoms ),
   told.

writeln_stock_atom(Atom) :-
   Atom =.. [stock,Name|Xs],
   write(stock), write('(['),
   write(''''), write(Name), write(''''), write(','),
   write_with_comma(Xs),
   write(']).'), nl.

write_with_comma([X1,X2|Xs]) :-
   write_dax_save(X1), write(','),
   write_with_comma([X2|Xs]).
write_with_comma([X]) :-
   write_dax_save(X).
write_with_comma([]).

write_dax_save('') :-
   !,
   write('--').
write_dax_save(X) :-
   write(X).


/* rankings_to_latex(Mode, Rankings) <-
      */

rankings_to_latex(grouped, Rankings) :-
   stock_groups(Groups),
   writeln_rankings([3,5,1], Groups, Rankings).
rankings_to_latex(regular, [Ranking]) :-
   writeln_ranking(Ranking).

writeln_rankings(_,[],[]).
writeln_rankings([N|Ns],Gs,Rs) :-
   first_n_elements(Gs,N,Gs1,Gs2),
   first_n_elements(Rs,N,Rs1,Rs2),
   writeln_rankings_2(Gs1,Rs1),
   backslash(BS),
   nl, write_list([BS,'newpage']), nl,
   writeln_rankings(Ns,Gs2,Rs2).

writeln_rankings_2([G|Gs],[R|Rs]) :-
   writeln_ranking_sub(G-R), 
   writeln_rankings_2(Gs,Rs).
writeln_rankings_2([],[]).

writeln_ranking_sub(G-R) :-
   backslash(BS),
   write_list([BS,'vspace*{5mm}']), nl,
   write_list([BS,'noindent ', G,' ',BS,'vspace*{5mm}']), nl, nl,
   writeln_ranking(R), nl.

writeln_ranking([]).
writeln_ranking(List) :-
   first_n_elements(List,40,List_h,List_r),
   backslash(BS),
   write_list([BS,'begin{tabular}{r|p{35mm}|rrrrr} ',BS,'hline']), nl,
   name_append(['{',BS,'small +',BS,'%'],String),
   write_list(['EJG & Aktie & Kurs & ',
      String,'T} & ', String,'W} & ',
      String,'M} & ', String,'J} ',BS,BS,' ',
      BS,'hline']), nl,
   checklist(writeln_dax_tuple_for_latex,List_h),
   write_list([BS,'hline ',BS,'end{tabular} ',BS,'vspace*{5mm}']),
   nl, nl,
   ( List_r = []
   ; ( write_list([BS,'newpage']), nl,
       writeln_ranking(List_r) ) ).

writeln_dax_tuple_for_latex(Tuple) :-
%  writeln(user,writeln_dax_tuple_for_latex(Tuple)), wait,
   Tuple = [stock(N,_,_,K,[V,D,W,M,Y])],
   format("~2f",V), write(' & '),
   name_to_nice_name(N,N_Nice), write(N_Nice), write(' & '),
   format_tabular(K,' & '),  format_tabular(D,' & '),
   format_tabular(W,' & '),  format_tabular(M,' & '),
   backslash(BS),
   name_append([' ',BS,BS,' '],String),
   format_tabular(Y,String), nl.

format_tabular('--',Y) :-
   !,
   write('--'), write(Y).
format_tabular(X,Y) :-
   format("~2f",X), write(Y).
   

/******************************************************************/


