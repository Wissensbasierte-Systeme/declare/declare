

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  XPCE Interface Hierarchy             ***/
/***                                                            ***/
/******************************************************************/


:- pce_autoload(toc_window, library(pce_toc)).

:- pce_begin_class(ds_hierarchy, toc_window,
	"Browser for a directory-hierarchy").


/******************************************************************/


initialise(FB, Root:directory) :->
	send(FB, send_super, initialise),
	get(Root, name, Name),
	send(FB, root, toc_folder(Name, Root)).

open_node(_, D:file) :->
	get(D, base_name, Name),
	dsa_update_some_dislog_variables,
	ds_chart(Name).

ds_chart(Item) :-
   stock_branch(Item,_),
   !,
   stock_chart(group,Item).
ds_chart('All Groups') :-
   !,
   stock_chart( all_groups_mysql, [
      'Banken', 'Versicherungen', 'Versorger', 'Chemie',
      'Automobile', 'Stahl', 'High Tech' ]).
ds_chart(Item) :-
   stock_portfolio_tuple(T),
   Item := T^owner,
   !,
   charts_portfolio(Item).
ds_chart(Item) :-
   chart(Item).

expand_node(FB, D:directory) :->
	get_sub_dirs(D, Sub_Dirs_Names_Chain),
	get(Sub_Dirs_Names_Chain,
		map, ?(D, directory, @arg1), Sub_Dirs),
	get_sub_files(D, Sub_Files_Names_Chain),
	get(Sub_Files_Names_Chain,
		map, ?(D, file, @arg1), Sub_Files),
	send(Sub_Dirs, for_all,
		message(FB, son, D,
			create(toc_folder, @arg1?name, @arg1))),
	send(Sub_Files, for_all,
		message(FB, son, D,
			create(toc_file, @arg1?base_name, @arg1))).

get_sub_files(D, Sub_Files_Names_Chain) :-
	get(D, name, Dir_Name),
	findall( Sub_File_Name,
		ds_sub_file(Dir_Name, Sub_File_Name),
		Sub_Files_Names ),
        chain_list(Sub_Files_Names_Chain, Sub_Files_Names).

get_sub_dirs(D, Sub_Dirs_Names_Chain) :-
	get(D, name, Dir_Name),
	findall( Sub_Dir_Name,
		ds_sub_dir(Dir_Name, Sub_Dir_Name),
		Sub_Dirs_Names ),
        chain_list(Sub_Dirs_Names_Chain, Sub_Dirs_Names).

:- pce_end_class.


/******************************************************************/


