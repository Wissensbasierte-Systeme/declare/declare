

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  XPCE Interface Toc Tree              ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      ds_sub_dir/2,
      ds_sub_file/2.

:- dynamic
      ds_sub_dir/2,
      ds_sub_file/2.


/******************************************************************/


dsa_stock_tree_prepare :-
   retractall(ds_sub_dir(_, _)),
   retractall(ds_sub_file(_, _)),
   Root = ' ',
   assert(ds_sub_dir(Root, 'Stocks')),
   Sources_and_Ranges = [
      [ 'Dax', dax, [ a-b, c-d, e-f, g-k, l-r, s-z ] ],
      [ 'Germany', germany, [a-b, c-d, e-f, g-k, l-r, s-z] ],
      [ 'England', england, [a-b, c-d, e-f, g-k, l-r, s-z] ],
      [ 'Europe G', europe_g, [a-b, c-d, e-f, g-k, l-r, s-z] ],
      [ 'Europe R', europe_r, [a-b, c-f, g-k, l-r, s-t, u-z] ],
      [ 'International', international,
           [a-a, b-c, d-g, h-m, n-s, t-z] ],
      [ 'Energy', energy,
           [b-b, p-p] ] ],
   checklist( assert_ds_sub_dir,
      Sources_and_Ranges ),
   checklist( dsa_stock_tree_prepare,
      Sources_and_Ranges ),
   assert(ds_sub_dir(Root, 'Branches')),
   assert(ds_sub_file('Branches', 'All Groups')),
   findall( Branch,
      ( stock_branch(Branch, _),
        assert(ds_sub_file('Branches', Branch)) ),
      _ ),
   assert(ds_sub_dir(Root, 'Portfolios')),
   findall( Portfolio,
      ( stock_portfolio_tuple(T),
        Portfolio := T^owner,
        assert(ds_sub_file('Portfolios', Portfolio)) ),
      _ ).

assert_ds_sub_dir([Source|_]) :-
   assert(ds_sub_dir('Stocks',Source)).


dsa_stock_tree_prepare([Source,Source_2,Ranges]) :-
   Number_Name = number_name_hs_menue,
   stock_file(stock_data,Number_Name,Path),
   stock_companies(Path,[Source_2|_],Companies),
   checklist( dsa_stock_tree_prepare_range(Source,Companies),
      Ranges ).

dsa_stock_tree_prepare_range(Source,Companies,X-Y) :-
   character_lower_to_capital(X,X1),
   character_lower_to_capital(Y,Y1),
   name_append([Source, ': ', X1,'-',Y1],S),
   assert(ds_sub_dir(Source,S)),
   name(X,N1), name(Y,N2),
   findall( Company_2,
      ( member(Company,Companies), name(Company,[N|_]),
        in_character_range(N1-N2,N),
        name_html_to_text(Company,Company_2) ),
      Companies_2 ),
   checklist( assert_ds_sub_file(S), Companies_2 ).

assert_ds_sub_file(S,File) :-
   assert(ds_sub_file(S,File)).


/******************************************************************/


