#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
using namespace std;

// Spline Ausgleich als N�herung

const int grenze=1000;

int Potenz(int sdf, int fds), D[grenze], order;
long double h, s, t,fehler, a, b, A[grenze], phi3[650], phi2[400];
float coeff[grenze], funk[grenze], stelle[grenze], matrix[grenze][grenze];
float Scal(long double x), gauss(int u,int o), Dual(long double x);
float coeff1[grenze], Projektion(long double x);
float Dual(float x), Dualkoeffiz(int MM), Gammas(), abs(long double x);
long double dualcoeff[61], gamma[61];


void main(int argc, char* argv[])
{
  int i, j, k, q, m, d, pot, potenz, aG, bG, n, l, r;
  long double w, hh, x;
  char input[100], co_out[100], daub2[100], daub3[100];
  char gamm[100], dualco[100];

  ifstream in1(argv[1]);
  in1>>input;
  in1>>co_out;
  in1>>order;
  in1>>daub2;
  in1>>daub3;
  in1>>dualco;
  in1>>gamm;
  ifstream in(input);
  ofstream out("/root/DisLog/Stock_Analysis/results/gnuplot_output");
//  ofstream out1("/root/DisLog/Stock_Analysis/results/Fehler");
//  ofstream out2("/root/DisLog/Stock_Analysis/results/Daten");
//  ofstream outdual("/root/DisLog/Stock_Analysis/results/Dual");
  ofstream outpl(co_out);
  ifstream dual(dualco);
  ifstream gammain(gamm);
  for(k=0;k<=60;k++) dual>>dualcoeff[k];
  for(k=0;k<=60;k++) gammain>>gamma[k];

  ifstream phi22(daub2);
  ifstream phi33(daub3);

  for(k=0;k<=385;k++) { phi22>>w; phi22>>phi2[k]; }
  for(k=0;k<=641;k++) { phi33>>w; phi33>>phi3[k]; }

  i=1;

  while (!in.eof())
  {
    in>>d; in>>w;
    D[i]=d; A[i]=w;
    i++;
  }

  i--;
  a=1.; b=float(i);
  aG=1; bG=i;

  pot=1; potenz=1;
  while(pot<i) { n=pot; pot=pot*2; potenz++; }
  pot=pot/2; potenz--;

  h=(b-a)/n;

  for(i=1;i<=n+1;i++)
  {
    stelle[i]=a+(i-1)*h;
    funk[i]=A[int(stelle[i])];
//    out2<<stelle[i]<<"   "<<funk[i]<<endl;
  }


  for(j=-1;j<=n+1;j++)
  {
    for(i=1;i<=n+1;i++)
    {
      t=(stelle[i]-a)/h-j;
      if(sqrt(t*t)<2)
      {
        s=Scal(t);
        matrix[3+j][3+j]=matrix[3+j][3+j]+s*s;
        matrix[3+j][3+j+1]=matrix[3+j][3+j+1]+s*Scal(t-1);
        matrix[3+j][3+j+2]=matrix[3+j][3+j+2]+s*Scal(t-2);
        matrix[3+j][3+j+3]=matrix[3+j][3+j+3]+s*Scal(t-3);
        matrix[3+j][3+n+2]=matrix[3+j][3+n+2]+s*funk[i];
      }
    }
    matrix[3+j+1][3+j]=matrix[3+j][3+j+1];
    matrix[3+j+2][3+j]=matrix[3+j][3+j+2];
    matrix[3+j+3][3+j]=matrix[3+j][3+j+3];
  }

  gauss(-1,n+1);


  ofstream out3("/root/DisLog/Stock_Analysis/results/coeff");
  for(k=7;k<=n-1;k++) out3<<k<<"  "<<coeff[k]<<endl;

  int plotten = 10;
  hh=h/plotten;
  fehler=0.;
  for(j=35;j<=n*plotten-35;j++)
  {
    x=a+j*hh;
    s=Projektion(x);
    out<<x<<"   "<<s<<endl;
    s=A[int(x)]-s;
//    out1<<x<<"   "<<s<<endl;
    if(sqrt(s*s)>fehler) fehler=sqrt(s*s);
  }

  cout<<"n="<<n<<"   Order="<<order<<endl;

//  if((order==3)||(order==4)) Dualkoeffiz(20);
//  for(s=-5.;s<=5.;s=s+0.01) outdual<<s<<"  "<<Dual(s)<<endl;
//  Gammas();

  m=n;
  h=(b-a)/m;
  while(m>16)
  {
    m=int(m/2);
    h=2*h;
    for(k=0;k<=m;k++)
    {
      s=0.;
      for(i=-30;i<=26;i++)
      {
        r=2*k-i;   long double rtz= float(i);
        if((r>0)&&(r<2*m)) s=s+coeff[r]*Dual(rtz);
      }
      coeff1[k]=s;
    }
    for(j=0;j<=m;j++) coeff[j]=coeff1[j];

  }

  // normalize
  w=0;
  for(k=2;k<=m+1;k++) coeff[k]=coeff[k]/coeff[1];
  coeff[1]=1.;

  ofstream outco1("/root/DisLog/Stock_Analysis/results/coeff1");
  hh=h/plotten;
  for(j=0;j<=m*plotten;j++)
  {
    x=a+j*hh;
    s=Projektion(x);
    //outco1<<x<<"   "<<s<<endl;
  }

  for(j=1;j<=m-1;j++) outco1<<j<<"  "<<coeff[j]<<endl;


  outpl<<":- disjunctive."<<endl<<endl;
  for(j=1;j<=m-1;j++) outpl<<"wavelet_coefficient("<<j<<","<<coeff[j]<<")."<<endl;
  outpl<<endl<<":- prolog.";

  //for(w=-5.;w<=5.;w=w+0.01) outco1<<w<<"   "<<Scal(w)<<endl;

  //for(k=0;k<=60;k++) outdual<<k<<"  "<<gamma[k]<<endl;

}






float abs(long double x)
{
  long double absolut;
  absolut=sqrt(x*x);
  return absolut;
}

float Dual(long double x)
{
  float s,t;
  int i,j,k,l,m,n;

  if((order==3)||(order==4))
  {
    s=dualcoeff[30]*Scal(x);
    for(k=1;k<=30;k++) s=s+dualcoeff[30+k]*(Scal(x+k)+Scal(x-k));
  }
    else s=Scal(x);
  return s;
}



float Projektion(long double x)
{
  int j, k;
  float s, t;

  s=0.;
  t=(x-a)/h;
  j=int(t);
  for (k=j-1;k<=j+2;k++) s=s+coeff[3+k]*Scal(t-k);
  return s;
}



float Scal(long double x)
{
  int k;
  long double t, scal;

  if(order==2)
  {
   t=x;
   if((t<0.)||(t>1.)) scal=0.;
     else scal=1.;
  }

  if(order==3)
  {
   t=sqrt(x*x);
   if(t>=1.5) scal=0;
   if((t<1.5)&&(t>=0.5)) scal=(t-1.5)*(t-1.5)/2;
   if(t<0.5) scal=0.75-t*t;
  }
  if(order==4)
  {
   t=sqrt(x*x);
   if(t>=2) scal=0.;
   if((t>=1)&&(t<2)) scal=(2-t)*(t-2)*(t-2)/6.;
   if(t<1) scal=(4+3*t*t*(t-2))/6.;
  }

  if(order==12)
  {
    t=x+1.5;
    k=int(t/3.*384.);
    if((t<=0)||(t>=3)) scal=0.;
    if((t>0)&&(t<3.)) scal=phi2[k];
  }

  if(order==13)
  {
    t=x+2.5;
    if((t<=0)||(t>=5)) scal=0.;
      else scal=phi3[int(t/5.*640.)];
  }
  //cout<<scal<<endl;

  return scal;
}



float gauss(int u, int n)
{
  float s, factor, aa[grenze];
  int i, j, k, r;

  for(k=0;k<=n-1;k++)
  {                          	// Pivotzeile
    r=k;
    s=sqrt(matrix[3+k][3+k]*matrix[3+k][3+k]);
    for(i=k+1;i<=n;i++)
      if(sqrt(matrix[3+i][3+k]*matrix[3+i][3+k])>s)
      {
        r=i;
        s=sqrt(matrix[3+i][3+k]*matrix[3+i][3+k]);
      }
   if(s!=0)
   {
    				// Zeilentausch
    for(j=k;j<=n+1;j++)
    {
      aa[j]=matrix[3+k][3+j];
      matrix[3+k][3+j]=matrix[3+r][3+j];
      matrix[3+r][3+j]=aa[j];
    }

    				// Elimination
    for(j=k+1;j<=n;j++)
    {
      factor=matrix[3+j][3+k]/matrix[3+k][3+k];
      for(i=k;i<=n+1;i++) matrix[3+j][3+i]=matrix[3+j][3+i] - factor*matrix[3+k][3+i];
    }
   }
  }

  				//Berechnung der Loes
  coeff[3+n]=matrix[3+n][3+n+1]/matrix[3+n][3+n];
  for(j=n-1;j>=0;j--)
  {
    coeff[3+j]=matrix[3+j][3+n+1];
    for(i=j+1;i<=n;i++) coeff[3+j]=coeff[3+j]-matrix[3+j][3+i]*coeff[3+i];
    coeff[3+j]=coeff[3+j]/matrix[3+j][3+j];
  }
}














