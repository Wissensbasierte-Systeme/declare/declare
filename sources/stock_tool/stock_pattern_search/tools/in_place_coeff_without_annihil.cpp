#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
using namespace std;

void main(int argc, char* argv[])
{
  int n, d, i, i1, allg, j,k, l, D[100][10000], I, J, M, L, K;
  int zaehler, laenge[100], length, pot;
  float A[100][10000], Potenz(float x, int k), a, c, w;
  char input[100], co_out[100];

  ifstream in(argv[1]);
  in>>input;
  in>>co_out;
  ifstream in1(input);
//  ofstream out("/root/DisLog/Stock_Analysis/results/gnuplot_output");
  ofstream out_dis(co_out);
  i=0; i1=0; zaehler=1;

    for(k=0;k<=10000;k++) { A[1][k]=0.; D[1][k]=0; }

  while (!in1.eof())
  {
    in1>>d; in1>>w;
    if ((d<D[zaehler][i1-1]+7)||(i==0)) {
      D[zaehler][i1]=d; A[zaehler][i1]=w; laenge[zaehler]=i1;
      i++; i1++;
    }
    else {
      zaehler++; i1=0;
      for(k=0;k<=10000;k++) { A[zaehler][k]=0.; D[zaehler][k]=0; }
      D[zaehler][i1]=d; A[zaehler][i1]=w; laenge[zaehler]=i1;
      i++; i1++;
    }
  }
  i--;

  out_dis<<":- disjunctive."<<endl<<endl;//<<"[";

  allg=1;
//  for(k=2;k<=i;k++) A[allg][k]=A[allg][k]/A[allg][1];
//  A[allg][1]=1;

    pot=0; length=1;
    while(length<laenge[allg]) { pot=pot+1; length=length*2; }

    I=1; J=2; M=length; n=pot;
    for(L=1;L<=n;L++)  {
      M=M/2;
      for(K=0;K<=M-1;K++)   {
        a=(A[allg][J*K]+A[allg][J*K+I])/2;
        c=(A[allg][J*K]-A[allg][J*K+I])/2;
        A[allg][J*K]=a;
        A[allg][J*K+I]=c;
      }
      I=J;
      J=2*J;
    }
    for(L=1;L<=length;L++) out_dis<<"wavelet_coefficient(" << L << ", " << A[allg][L] << ")." << endl;


  out_dis<<endl<<endl<<":- prolog.";

}



float Potenz(float x, int k)
{
  float pot=1;
  int i;

  for (i=1;i<=k;i++) pot=pot*x;

  return pot;
}




