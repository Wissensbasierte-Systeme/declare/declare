#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
using namespace std;

const int grenze=1000;

int order;
float coeff[grenze], Dualkoeffiz(int MM), Gammas(), matrix[grenze][grenze];
long double dualcoeff[61], gamma[61], phi3[650], phi2[400];
float Scal(long double x), gauss(int u,int o), Dual(long double x);

void main(int argc, char* argv[])
{
  char input[100], co_out[100], daub2[100], daub3[100];
  char gamm[100], dualco[100];
  int k;
  long double w;

  ifstream in1(argv[1]);
  in1>>input;
  in1>>co_out;
  in1>>order;
  in1>>daub2;
  in1>>daub3;
  in1>>dualco;
  in1>>gamm;

  ofstream dual(dualco);
  ofstream gammaout(gamm);

  ifstream phi22(daub2);
  ifstream phi33(daub3);

  for(k=0;k<=385;k++) { phi22>>w; phi22>>phi2[k]; }
  for(k=0;k<=641;k++) { phi33>>w; phi33>>phi3[k]; }

  Dualkoeffiz(20);
  Gammas();
  for(k=0;k<=61;k++) dual<<dualcoeff[k]<<endl;
  for(k=0;k<=61;k++) gammaout<<gamma[k]<<endl;




}


float Dual(long double x)
{
  float s,t;
  int i,j,k,l,m,n;

  if((order==3)||(order==4))
  {
    s=dualcoeff[30]*Scal(x);
    for(k=1;k<=30;k++) s=s+dualcoeff[30+k]*(Scal(x+k)+Scal(x-k));
  }
    else s=Scal(x);
  return s;
}

float Scal(long double x)
{
  int k;
  long double t, scal;

  if(order==2)
  {
   t=x;
   if((t<0.)||(t>1.)) scal=0.;
     else scal=1.;
  }

  if(order==3)
  {
   t=sqrt(x*x);
   if(t>=1.5) scal=0;
   if((t<1.5)&&(t>=0.5)) scal=(t-1.5)*(t-1.5)/2;
   if(t<0.5) scal=0.75-t*t;
  }
  if(order==4)
  {
   t=sqrt(x*x);
   if(t>=2) scal=0.;
   if((t>=1)&&(t<2)) scal=(2-t)*(t-2)*(t-2)/6.;
   if(t<1) scal=(4+3*t*t*(t-2))/6.;
  }

  if(order==12)
  {
    t=x+1.5;
    k=int(t/3.*384.);
    if((t<=0)||(t>=3)) scal=0.;
    if((t>0)&&(t<3.)) scal=phi2[k];
  }

  if(order==13)
  {
    t=x+2.5;
    if((t<=0)||(t>=5)) scal=0.;
      else scal=phi3[int(t/5.*640.)];
  }
  //cout<<scal<<endl;

  return scal;
}

float gauss(int u, int n)
{
  float s, factor, aa[grenze];
  int i, j, k, r;

  for(k=0;k<=n-1;k++)
  {                          	// Pivotzeile
    r=k;
    s=sqrt(matrix[3+k][3+k]*matrix[3+k][3+k]);
    for(i=k+1;i<=n;i++)
      if(sqrt(matrix[3+i][3+k]*matrix[3+i][3+k])>s)
      {
        r=i;
        s=sqrt(matrix[3+i][3+k]*matrix[3+i][3+k]);
      }
   if(s!=0)
   {
    				// Zeilentausch
    for(j=k;j<=n+1;j++)
    {
      aa[j]=matrix[3+k][3+j];
      matrix[3+k][3+j]=matrix[3+r][3+j];
      matrix[3+r][3+j]=aa[j];
    }

    				// Elimination
    for(j=k+1;j<=n;j++)
    {
      factor=matrix[3+j][3+k]/matrix[3+k][3+k];
      for(i=k;i<=n+1;i++) matrix[3+j][3+i]=matrix[3+j][3+i] - factor*matrix[3+k][3+i];
    }
   }
  }

  				//Berechnung der Loes
  coeff[3+n]=matrix[3+n][3+n+1]/matrix[3+n][3+n];
  for(j=n-1;j>=0;j--)
  {
    coeff[3+j]=matrix[3+j][3+n+1];
    for(i=j+1;i<=n;i++) coeff[3+j]=coeff[3+j]-matrix[3+j][3+i]*coeff[3+i];
    coeff[3+j]=coeff[3+j]/matrix[3+j][3+j];
  }
}



float Dualkoeffiz(int MM)
{
  float s,t;
  int i,j,k,l,m,n;
  long double alpha[121];

  n=30;
  m=order;
  for(k=-60;k<=60;k++) alpha[60+k]=0;
  for(k=0;k<=m-1;k++)
  {
    s=0.;
    for(i=1+k*MM;i<=MM*m;i++) s=s+Scal(i/MM)*Scal(i/MM-k);
    alpha[60+k]=s/MM;
  }
  for(k=1;k<=m-1;k++) alpha[60-k]=alpha[60+k];

  for(j=0;j<=30;j++)
    for(k=0;k<=30+1;k++) matrix[3+j][3+k]=0.;

  for(j=0;j<=n;j++)
  {
    matrix[3+j][3+0]=alpha[60+j];
    for(k=1;k<=n;k++) matrix[3+j][3+k]=alpha[60+k-j]+alpha[60+k+j];
  }
  matrix[3+0][3+n+1]=1;

  gauss(0,n);
  for(k=0;k<=n;k++)
  {
    dualcoeff[30+k]=coeff[3+k];
    dualcoeff[30-k]=coeff[3+k];
  }
}


float Gammas()
{
  long double x, h1;
  int m, i, j;

  m=100;
  h1=1./m;

  for(i=-30;i<=30;i++)
  {
    gamma[30+i]=0;
    for(j=0;j<=order*m;j++)
    {
      x=float(j)/m;
      gamma[30+i]=gamma[30+i]+Scal(x)*Dual((x-i)/2);
    }
    gamma[30+i]=gamma[30+i]/(2*m);
  }
}



