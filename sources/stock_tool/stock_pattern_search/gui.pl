

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Pattern Search - GUI                 ***/
/***                                                            ***/
/******************************************************************/



:- module( dsa_pattern_search_gui, [
      dsa_pattern_search/0,
      pattern_search_settings/1,
      pattern_search_settings/2,
      pattern_search_set_variable_smooth/1,
      pattern_search_market_close/0,
      pattern_search_market_clear/0 ] ).	

:- discontiguous
      pattern_search_settings/1,
      pattern_search_settings/2,
      pattern_search_settings/3.


/******************************************************************/


dsa_pattern_search :-
   pattern_search_settings(start_dialog).


/* pattern_search_settings(start_dialog) <-
      */

pattern_search_settings(start_dialog) :-
   pattern_search_free_list,
   pattern_search_initialise,
   pattern_search_variable_get(smooth, Smooth),
   dislog_variable_get(
      dislog_frame_interface_color, Colour ),
   new(@main_frame, frame),
   send(@main_frame, label, 'SMS - Pattern Search'),
   pattern_search( create_dialog, [
      name: main_dialog,
      title: 'Pattern Search Manager',
      menu_bar: [
         title: 'File',
         items: [
            quit - pattern_search_settings(end) ] ]: below,
      menu_bar: [
         title: 'Edit',
         items:[
            clear_Display -
               pattern_search_settings(clear_display),
            auto_Draw -
               pattern_search_settings(
                  preferences_auto_draw) ] ]: right,
      menu_bar: [
         title: 'Stocks',
         items: [
            dax_30 - pattern_search_settings(
               market_dialog, dax_30),
            dax_100 - pattern_search_settings(
               market_dialog, dax_100),
            eurostock - pattern_search_settings(
               market_dialog, eurostock),
            neuermarkt - pattern_search_settings(
               market_dialog, neuermarkt),
            other - pattern_search_settings(
               market_dialog, other) ] ]: right,
      button: [
         search, pattern_search_visualisation,
         check ]: right ]),
   pattern_search( create_dialog, [
      name: main_dialog_2,
      picture: [
         name: main_pic,
         width: 330,
         height: 150,
         background: Colour ]: below ]),
   send(@main_pic, display, new(@main_device, device)),
   send(@main_pic, recogniser,
      click_gesture(left, '', single, message(@prolog,
         pattern_search_settings_draw_dots_by_hand,
         @event?position?x, @event?position?y ))),
   pattern_search( create_dialog, [
      name: main_dialog_3,
      slider: [
         title: smooth,
         begin: 1, end: 50,
         default: Smooth,
         function: pattern_search_set_variable_smooth ]: below,
      text_item: [
         name: stockname,
         label: 'Stock:',
         selection: ' ' ]: below,
      text_item: [
         name: marketname,
         label: 'Market:',
         selection: ' ' ]: below ]),
   pattern_search( create_dialog, [
      name: main_dialog_4,
      button: [ 'Chart',
         pattern_search_evaluation, exact ]: below,
      button: [ 'Table',
         pattern_search_xpce_display_table ]: right,
      button: [ ' << ',
         pattern_search_visualisation, previous ]: right,
      button: [ ' >> ',
         pattern_search_visualisation, next]: right ] ),
   send(@main_frame,append,@main_dialog),
   send(@main_frame,append,@main_dialog_2),
   send(@main_frame,append,@main_dialog_3),
   send(@main_frame,append,@main_dialog_4),
   send(@main_dialog_2,below,@main_dialog),
   send(@main_dialog_3,below,@main_dialog_2),
   send(@main_dialog_4,below,@main_dialog_3),
   send(@main_frame,fit),
   send(@main_dialog,open).

pattern_search_set_variable_smooth(X) :-
   pattern_search_variable_set(smooth,X).


/* pattern_search_initialise <-
      */

pattern_search_initialise :-
   pattern_search_variable_set(curve, []),
   pattern_search_get_browser_and_numbername_list(
      dax_100, _, Numbername_List),
   pattern_search_variables_set([
      number_name_list - Numbername_List,
      status - clear,
      nodes - 3,
      start - 0,
      market - dax100,
      stocklist - [1],
      smooth - 5,
      test - no_test,
      visu - no ]).


/* pattern_search_settings(end) <-
      */

pattern_search_settings(end) :-
   send(@main_dialog, destroy),
   pattern_search_free_list.

pattern_search_free_list :-
   checklist( free, [
      @main_dialog,
      @main_dialog_2, @main_dialog_3, @main_dialog_4,
      @main_device, @main_pic,
      @auto_draw_dialog,
      @market_dialog, @values_dialog,
      @market_browser1, @market_browser2,
      @dates_dialog, @dates_pic, @dates_browser, @dates_dev,
      @main_frame,
      @stockname, @marketname ] ).


/* pattern_search_settings(clear_display) <-
      */

pattern_search_settings(clear_display) :-
   pattern_search_variable_get(curve, P),
   forall( member(_-min(B), P),
      free(B) ),
   forall( member(_-max(B1), P),
      free(B1) ),
   pattern_search_variables_set([
      curve - [],
      status - clear ]).


/******************************************************************/


/* pattern_search_settings(preferences_auto_draw) <-
      */

pattern_search_settings(preferences_auto_draw) :-
   free(@auto_draw_dialog),
   pattern_search_variable_get(nodes, K),
   ( K < 3 ->
     K1 = 3
   ; ( K > 9 ->
       K1 = 9
     ; K1 = K ) ),
   pattern_search_save_old_settings,
   pattern_search_variable_get(start, St),
   ( St = 0 ->
     Start = min
   ; Start = max ),
   pattern_search( create_dialog, [
      name:auto_draw_dialog,
      title:'Auto Draw',
      menu:[ title:number_of_nodes, type:choice,
         function:pattern_search_set_number_of_nodes,
         items:[3,4,5,6,7,8,9], columns:1,
         default:K1 ]: next_row,
      menu:[ title:start_with, type:choice,
         function:pattern_search_set_start_node,
         items:[min,max], columns:1,
         default:Start ]: below,
      button:[draw,pattern_search_preferences_apply]:below,
      button:[quit,pattern_search_preferences_cancel]:right ]),
   send(@auto_draw_dialog, open).


pattern_search_save_old_settings :-
   pattern_search_variables_get([
      nodes - Kn,
      start - St,
      smooth - Smooth,
      oldnodes - Kn,
      oldstart - St,
      oldsmooth - Smooth ]).


/* pattern_search_set_number_of_nodes(X) <-
      only influences auto-draw. */

pattern_search_set_number_of_nodes(X) :-
   pattern_search_variable_set(nodes,X),
   pattern_search_settings(clear_display).


/* pattern_search_set_start_node(X) <-
      only influences auto-draw. */

pattern_search_set_start_node(X) :-
   X = min ->
     pattern_search_variable_set(start,0)
   ; pattern_search_variable_set(start,1),
   pattern_search_settings(clear_display).	


/******************************************************************/


pattern_search_preferences_apply :-
   send(@auto_draw_dialog, destroy),
   pattern_search_variable_set(status,ok),
   free(@auto_draw_dialog),
   pattern_search_settings(auto_draw).

pattern_search_preferences_cancel :-
   pattern_search_variables_get([
      oldnodes - Kn,
      oldstart - St,
      oldsmooth - Smooth ]),
   pattern_search_variables_set([
      nodes - Kn,
      start - St,
      smooth - Smooth ]),
   send(@auto_draw_dialog, destroy),
   free(@auto_draw_dialog).	


/* pattern_search_settings(auto_draw) <_
      */

pattern_search_settings(auto_draw) :-
   pattern_search_settings(clear_display),
   pattern_search_get_nodes_list(List),
   checklist( pattern_search_settings_draw_dots,
      List ),
   pattern_search_settings(connect_dots) ,
   pattern_search_variable_set(status,ok).


pattern_search_get_nodes_list(L) :-
   pattern_search_variable_get(nodes,Nodes),
   generate_interval(1,Nodes,Int),
   maplist( pattern_search_change_number_to_x(Nodes),
      Int, L ).

pattern_search_change_number_to_x(Nodes, N, N-K) :-
   Diff = 320 / Nodes,
   K is N * Diff.
	

pattern_search_settings_draw_dots(N-K) :-
   pattern_search_variable_get(start,St),
   pattern_search_settings(draw_dots, N, K, St).


/* pattern_search_settings(draw_dots) <_
      */

pattern_search_settings(draw_dots, N, K, Start) :-
   T is N mod 2,
   T1 is T + Start,
   ( T1 = 1 ->
     ( Y is 140 - N,
       pattern_search_settings(create_dot, N-K-Y-min-blue) )
   ; ( Y is 60 + N,
       pattern_search_settings(create_dot, N-K-Y-max-green) ) ).
      	

/* pattern_search_settings(create_dot) <_
      */

pattern_search_settings(create_dot, N-X-Y-Attribute-Colour) :-
   send(@main_device, display, new(Box, box(4,4)), point(X,Y)),
   pattern_search_variable_get(curve, P),
   ( Attribute = min ->
     append(P, [N-min(Box)], P_new)
   ; append(P, [N-max(Box)], P_new) ),
   pattern_search_variable_set(curve, P_new),
   send(Box, colour, Colour),
   send(Box, recogniser, new(move_gesture)).



/* pattern_search_settings(draw_dots_by_hand) <_
      */

pattern_search_settings_draw_dots_by_hand(X,Y) :-
   pattern_search_settings(draw_dots_by_hand, X, Y).

pattern_search_settings(draw_dots_by_hand, _, _) :-
   pattern_search_variables_get([
      status - draw_yourself,
      curve - Curve ]),
   length(Curve, 9),
   display_announcement_wait_until_keypressed(
      '9 Nodes is the Maximum!'),
   !.

pattern_search_settings(draw_dots_by_hand, X, Y) :-
   pattern_search_variable_get(status,clear),
   pattern_search_settings(create_dot, 1-X-Y-min-blue),
   pattern_search_variable_set(status,draw_yourself),
   !.

pattern_search_settings(draw_dots_by_hand, X, Y) :-
   pattern_search_variables_get([
      status - draw_yourself,
      curve - P ]),
   length(P, 1),
   ( member( 1-min(Box1), P)
   ; member( 1-max(Box1), P) ),
   get(Box1, x, X1),
   get(Box1, y, Y1),
   ( Y1 < Y ->
     ( free(Box1),
       pattern_search_variable_set(curve, []),
       pattern_search_settings(create_dot, 1-X1-Y1-max-green),
       pattern_search_settings(create_dot, 2-X-Y-min-blue),
       pattern_search_variable_set(start,1),
       pattern_search_settings(connect_dots) )
   ; ( pattern_search_variable_set(start,0),
       pattern_search_settings(create_dot, 2-X-Y-max-green),
       pattern_search_settings(connect_dots) ) ),
   !.

pattern_search_settings(draw_dots_by_hand, X, Y) :-
   pattern_search_variable_get(status,draw_yourself),
   pattern_search_variable_get(curve, P),
   length(P,N),
   K is N + 1,
   ( member( N-min(_), P) ->
      ( pattern_search_settings(create_dot, K-X-Y-max-green),
        pattern_search_settings(connect_dots) )
      ; ( pattern_search_settings(create_dot, K-X-Y-min-blue),
          pattern_search_settings(connect_dots) ) ).


/* pattern_search_settings(connect_dots) <-
      */

pattern_search_settings(connect_dots)  :-
   forall( pattern_search_variable(link, _-Ln),
      free(Ln) ),
   retractall(pattern_search_variable(link,_-_)),
   pattern_search_variable_get(curve,Curve),
   length(Curve, K1),
   K is K1 - 1,
   generate_interval(1,K,Int),
   forall( member(N,Int),
      pattern_search_settings(connect_dot_to_following, N) ).

pattern_search_settings(connect_dot_to_following, N) :-
   pattern_search_variable_get(curve, P),
   M is N + 1,
   ( ( member(N-min(B1), P),
       member(M-max(B2), P) )
   ; ( member(N-max(B1), P),
       member(M-min(B2), P) ) ),
   send(B1, handle, handle(w, h, in)),
   send(B2, handle, handle(w/2, 0, out)),
   send(B1, connect, B2,
      new(L, link(in, out, line(arrows := none)))),
   assert(pattern_search_variable(link,N-L)).
	
   	
/******************************************************************/


/* pattern_search_settings(market_dialog) <-
      */

pattern_search_settings(market_dialog, Selection) :-
   free(@market_dialog),
   free(@market_browser1),  free(@market_browser2),
   pattern_search_variables_set([
      market - Selection,
      stocklist - [] ]),
   pattern_search_get_browser_and_numbername_list(
      Selection, Num_nam, Numbernamelist),
   pattern_search_variable_set(
      number_name_list, Numbernamelist),
   name_append([Selection, ' - Stocks'], Title),
   pattern_search( create_dialog, [
      name:market_dialog,
      title:Title,
      label:[text:'double-click to select or remove']:below,
      list_browser:[
         name:market_browser1,
         label:'available',
         members:Num_nam ]: below,
      list_browser:[
         name:market_browser2,
         label:'selected' ]: right,
      button:[close,pattern_search_market_close]:below,
      button:[clear,pattern_search_market_clear]:right ] ),
   send(@market_browser1, open_message,
      message(@prolog,
         pattern_search_settings_market_add, @arg1?key)),
   send(@market_browser2, open_message,
      message(@prolog,
          pattern_search_settings_market_remove, @arg1?key)),
   send(@market_dialog, open).

pattern_search_get_browser_and_numbername_list(Sel, R, Nnl) :-
   stock_file(stock_data, number_name_ft_menue, File),
   diconsult(File, State),
   findall( N,
      member( [number_name( [Sel, _], _, N )], State ),
      Names_1 ),
   maplist( name,
      Names_2, Names_1 ),
   maplist( name_to_nice_name,
      Names_2, Names_3 ),
   sort(Names_3,R),
   append_index(1, R, Nnl).

pattern_search_market_close :-
   send(@market_dialog,destroy),
   free(@market_dialog).

pattern_search_market_clear :-
   pattern_search_variable_set(stocklist, []),
   send(@market_browser2, members, [' ']).


/* pattern_search_settings(market_add) <-
      */

pattern_search_settings_market_add(X) :-
   pattern_search_settings(market_add,X).

pattern_search_settings(market_add,X) :-
   pattern_search_variables_get([
      stocklist - Stocklist,
      number_name_list - Number_name ]),
   pattern_search_get_names_from_stocklist(
      Stocklist, Number_name, Stocknamelist),
   name_to_quoted_name(X,X1),
   member([Nr-X1],Number_name),
   not(member(Nr,Stocklist)),
   append(Stocklist, [Nr], L1),
   append(Stocknamelist, [X1], Stocknamelist1),
   flatten(L1,L2),
   flatten(Stocknamelist1, Stocknamelist2),
   pattern_search_variable_set(stocklist,L2),
   maplist( name_remove_elements("'"),
      Stocknamelist2, Stocknamelist3 ),
   send(@market_browser2, members, Stocknamelist3).


/* pattern_search_settings(market_remove) <-
      */

pattern_search_settings_market_remove(X) :-
   pattern_search_settings(market_remove,X).

pattern_search_settings(market_remove,X) :-
   pattern_search_variables_get([
      stocklist - Stocklist,
      number_name_list - Number_name ]),
   pattern_search_get_names_from_stocklist(
      Stocklist, Number_name, Stocknamelist),
   name_to_quoted_name(X,X1),
   member([Nr-X1], Number_name),
   list_remove_elements([Nr], Stocklist, L1),
   list_remove_elements([X1], Stocknamelist, Stocknamelist1),
   pattern_search_variable_set(stocklist,L1),
   maplist( name_remove_elements("'"),
      Stocknamelist1, Stocknamelist2 ),
   ( Stocknamelist2 = [] ->
     send(@market_browser2, members, [' '])
   ; send(@market_browser2, members, Stocknamelist2) ).

pattern_search_get_names_from_stocklist(
      Stocklist, Number_Name, Names) :-
   findall( Name,
      ( member(Nr, Stocklist),
        member([Nr-Name],Number_Name) ),
      Names ).


/******************************************************************/


