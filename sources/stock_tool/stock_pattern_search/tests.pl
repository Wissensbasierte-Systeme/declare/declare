

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Pattern Search - Tests               ***/
/***                                                            ***/
/******************************************************************/


test(pattern_search, init) :-
   nl, nl, nl,
   dsa_pattern_search_gui:pattern_search_initialise.

test(pattern_search, settings_auto_draw) :-
   nl, nl, nl,
   checklist( free,
      [ @main_dialog, @main_pic, @main_device] ),
   pattern_search(create_dialog, [
      name:main_dialog,
      title:'Test Visualisation Predicates',
      picture:[name:main_pic,width:380,height:200]:below,
      button:[quit,pattern_search_settings, end]:below ] ),
      send(@main_pic, display, new(@main_device, device)),
   send(@main_dialog, open),
   pattern_search_settings(auto_draw).

/*
test(pattern_search, settings_create_box) :-
   nl, nl, nl,
   pattern_search_settings(clear_display),
   pattern_search_settings(create_box, 1-100-100-max-red).

test(pattern_search, settings_draw_boxes_by_hand) :-
   nl, nl, nl,
   pattern_search_variable_set(nodes,1),
   pattern_search_variable_set(status,draw_yourself),
   dsa_pattern_search_gui:pattern_search_settings(
      draw_boxes_by_hand, 200,100).

test(pattern_search, settings_clear_display) :-
   nl, nl, nl,
   pattern_search_settings(clear_display).
*/

test(pattern_search, settings_end_settings_main_test) :-
   nl, nl, nl,
   send(@main_dialog, destroy),
   checklist( free,
      [ @main_dialog, @main_pic, @main_device] ).

/*
test(pattern_search, visualisation_check) :-
   nl, nl, nl,
   pattern_search_variable_set(nodes, 10),
   pattern_search_visualisation(check).
*/

/*
test(pattern_search, visualisation_dialog) :-
   nl, nl, nl,
   pattern_search_variable_set(nodes, 3),
   pattern_search_visualisation(dialog,ok).
*/

test(pattern_search, create_dialog) :-
   nl, nl, nl,
   free(@dialog),
%  free(@browser),
   pattern_search(create_dialog, [
      name: dialog,
      title: 'Test - Dialog',
      picture: [
         name: pic,
         width: 120, height: 60 ]: right,
%     list_browser: [
%        name: browser,
%        label: 'Test',
%        members: [
%           'This', 'is', 'a', 'test-browser' ] ]: right,
      menu: [
         title: 'Test-Menu',
         type: choice,
         function: write_ln,
         items: [ 'John', 'Jack', 'Joe' ] ]: next_row,
      slider: [
         title: 'Slider', begin: 1, end: 1000,
         default: 800, function: write_ln ]: next_row,
      button: [
         quit, test_visualisation_dialog_end ]: below,
      button: [
         make, make ]: right ]),
   send(@dialog,open).

test(pattern_search, visualisation_dialog_end) :-
   test_visualisation_dialog_end.

test_visualisation_dialog_end :-
   nl, nl, nl,
   send(@dialog, destroy),
   checklist( free,
%     [ @dialog, @pic, @browser] ).
      [ @dialog, @pic ] ).

test(pattern_search, visualisation_x_check) :-
   nl, nl, nl,
   Pattern = [
      1-min(100,100),
      2-max(130,60),
      3-min(160,90) ],
   pattern_search_visualisation(x_check, Pattern, Status),
   writeln_list(['Pattern-parameter: ', Pattern]), nl,
   writeln_list(['Status: ',Status]).

test(pattern_search, visualisation, x_check) :-
   nl, nl, nl,
   Pattern = [
      1-min(100,100),
      2-max(160,60),
      3-min(130,90) ],
   pattern_search_visualisation(x_check, Pattern, Status),
   writeln_list(['Pattern: ', Pattern]), nl,
   writeln_list(['Status: ',Status]).

test(pattern_search, visualisation_min_max_check_1) :-
   nl, nl, nl,
   Pattern = [
      1-min(100,100),
      2-max(130,60),
      3-min(160,90) ],
   pattern_search_visualisation(
      min_max_check, Pattern, ok, Status),
   write_list(['Pattern-parameter: ', Pattern]), nl,
   writeln_list(['Status: ',Status]).

test(pattern_search, visualisation_min_max_check_2) :-
   nl, nl, nl,
   Pattern = [
      1-min(100,100),
      2-max(130,120),
      3-min(160,140) ],
   pattern_search_visualisation(
      min_max_check, Pattern, ok, Status),
   writeln_list(['Pattern-parameter: ', Pattern]), nl,
   writeln_list(['Status: ',Status]).

test(pattern_search, visualisation_min_max_check_3) :-
   nl, nl, nl,
   Pattern = [
      1-min(100,100),
      2-max(130,160),
      3-min(160,140) ],
   pattern_search_visualisation(
      min_max_check, Pattern, ok, Status),
   writeln_list(['Pattern-parameter: ', Pattern]), nl,
   writeln_list(['Status: ',Status]).

test(pattern_search, visualisation_min_max_check_4) :-
   nl, nl, nl,
   pattern_search_visualisation(
      min_max_check, _, failx, Status),
   writeln_list(['Status: ',Status]).

test(pattern_search, evaluation_get_evaluation_info) :-
   nl, nl, nl,
   pattern_search_variable_set(test, test),
   pattern_search_evaluation(get_evaluation_info,
%     Stock_States, Stock-Names, Pattern),
      Stock_States, Pattern),
   flatten( Stock_States, State),
   write_ln('Stock State: '),
   writeln_list(State), nl,
   write_ln('List was flatted for optical reasons!'), nl,
%  writeln_list(['Stock-Names', Stock_names]), nl,
   writeln_list(['Pattern: ', Pattern]).

test(pattern_search, evaluation_get_pattern_range) :-
   nl, nl, nl,
   Pattern = [
      1-min(74,161),
      2-max(135,72),
      3-min(170,119) ],
   pattern_search_evaluation(get_pattern_range,
      Pattern, Pattern_range),
   writeln_list(['Pattern :', Pattern]),
   writeln_list(['Pattern Range :', Pattern_range]).

test(pattern_search, evaluation_chart_reduction) :-
   nl, nl, nl,
   Time_val = [
      100-20, 101-21, 102-20, 103-19, 104-18, 105-17, 106-16,
      107-17, 108-18, 109-19, 110-18, 111-17, 112-16, 113-15,
      114-14, 115-13, 116-14, 117-15 ],
   pattern_search_evaluation(chart_reduction, Time_val, Chart),
   writeln_list(['Time_val :', Time_val]),
   writeln_list(['Chart :', Chart]).

test(pattern_search, evaluation_match_pattern) :-
   nl, nl, nl,
   pattern_search_variable_set(smooth, 0),
   Stock_State = [
      [stock('Test', 0000, 6-3-98, 42, [])],
      [stock('Test', 0000, 7-3-98, 41, [])],
      [stock('Test', 0000, 8-3-98, 42, [])],
      [stock('Test', 0000, 9-3-98, 43, [])],
      [stock('Test', 0000, 10-3-98, 44, [])],
      [stock('Test', 0000, 11-3-98, 45, [])],
      [stock('Test', 0000, 12-3-98, 44, [])],
      [stock('Test', 0000, 13-3-98, 43, [])],
      [stock('Test', 0000, 14-3-98, 44, [])],
      [stock('Test', 0000, 15-3-98, 45, [])] ],
   Pattern = [
      1-min(74,161), 2-max(135,72), 3-min(170,119) ],
   pattern_search_evaluation( match_pattern,
      Pattern, Stock_State, Result_Patterns ),
   writeln_list(['Stock State: ', Stock_State]),
   writeln_list(['Pattern: ', Pattern]), nl,
   writeln_list(['--> Result: ', Result_Patterns]),
   pattern_search_variable_set(smooth, 5).

test(pattern_search, evaluation_match_pattern) :-
   nl, nl, nl,
   Pattern = [ 3-min, 1-min, 2-max ],
   Chart = [
      max(101, 21), min(106, 16),
      max(109, 19), min(115, 13)],
   pattern_search_match_pattern_chart(
      Pattern, Chart, Result_Patterns ),
   write_list(['Pattern Range :', Pattern]), nl,
   write_list(['Chart :', Chart]), nl, nl,
   write_list(['--> Result: ', Result_Patterns]), nl.

test(pattern_search,
      exact_evaluation_get_pattern_coordinate_list) :-
   nl, nl, nl,
   Pattern = [
      1-min(74,161), 2-max(135,72), 3-min(170,119) ],
   pattern_search_pattern_to_pattern_coordinates(
      Pattern, Pattern_Coordinates ),
   writeln_list(
      ['--> Pattern_Coordinates: ',Pattern_Coordinates]).

test(pattern_search,
      exact_evaluation_match_coordinate_to_percentage) :-
   nl, nl, nl,
   Coordinates = [
      923-21.0791, 938-21.4745, 939-21.4482 ],
   Pattern_Coordinates = [
      90-133, 149-211, 225-173 ],
   pattern_search_coordinate_to_percentage(
      Pattern_Coordinates, Coordinates, Percentage ),
   writeln_list(['--> Result: ', Percentage]).


pattern_search_test_state([
   [stock('AGIV', 502820, 1-2-99, 38.82, [-3.08, --, --, --])],
   [stock('AGIV', 502820, 1-3-99, 37.16, [-1.04, --, --, --])],
   [stock('AGIV', 502820, 2-6-98, 51.4, [2.8, 5.98, 12.97, 60.37])],
   [stock('AGIV', 502820, 2-6-99, 40.29, [1.08, --, --, --])] ]).


/******************************************************************/


