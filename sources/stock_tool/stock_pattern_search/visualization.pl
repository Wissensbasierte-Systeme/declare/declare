

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Pattern Search - Visualization       ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      pattern_search_visualisation/1,
      pattern_search_visualisation/2,
      pattern_search_visualisation/3.


/*** interface ****************************************************/


/* pattern_search_visualisation(check) <-
      */

pattern_search_visualisation(check) :-
   pattern_search_variable_get(curve,Curve),
   length(Curve, Nodes),
   Nodes < 3,
   display_announcement_wait_until_keypressed(
      '3 Nodes is Minimum!').

pattern_search_visualisation(check) :-
   pattern_search_variable_get(curve, Curve),
   pattern_search_curve_to_pattern(Curve, Pattern),
   ( pattern_search_variable_get(status, clear) ->
     display_announcement_wait_until_keypressed(
        'No pattern has been chosen!')
   ; ( pattern_search_visualisation(x_check, Pattern, Status),
       pattern_search_visualisation(min_max_check,
          Pattern, Status, Status1),
       pattern_search_visualisation(dialog, Status1) ) ).


/* pattern_search_visualisation(dialog,Success) <-
      */

pattern_search_visualisation(dialog, ok) :-
   pattern_search_variable_get(visu,yes),
   pattern_search_evaluation(refresh).

pattern_search_visualisation(dialog, ok) :-
   pattern_search_variable_set(visu,yes),
   retractall(pattern_search_variable(exact,_)),
   pattern_search_variable_set(nr_list,1),
%  pattern_search_visualisation_create_append_dialog,
   pattern_search_evaluation(refresh).

pattern_search_visualisation_create_append_dialog :-
   checklist( free, [
      @dates_dialog, @dates_pic,
%     @dates_browser,
      @dates_dev,
      @stockname, @marketname ] ),
   pattern_search(create_dialog, [
      name: dates_dialog,
      title: 'Dates',
%     list_browser: [
%        name: dates_browser,
%        width: 80]: below,
      text_item: [
         name: stockname,
         label: 'Stock:',
         selection: ' ' ]: next_row,
      text_item: [
         name: marketname,
         label: 'Market:',
         selection: ' ' ]: next_row,
      button: [close,pattern_search_dates_close]: next_row,
%     button: [refresh, pattern_search_evaluation, refresh]: right,
      button: ['Draw Boxes', pattern_search_evaluation, exact]: right,
      button: [' << ', pattern_search_visualisation, previous]: right,
      button: [' >> ', pattern_search_visualisation, next]: right ]),
   send(@dates_dialog, below, @main_dialog2),
   send(@main_frame, fit),
   send(@dates_dialog, open).


pattern_search_visualisation(dialog, fail(y)) :-
   display_announcement_wait_until_keypressed(
      'Min-Max mismatch!   Keep the Direction!'),
   !.
pattern_search_visualisation(dialog, fail(x)) :-
   display_announcement_wait_until_keypressed(
      'Pattern mismatch!   Keep the Direction!'),
   !.


pattern_search_dates_close :-
   send(@main_frame, delete, @dates_dialog),
   send(@main_frame, fit),
   checklist( free,
      [ @dates_dialog, @dates_browser, @dates_dev ] ),
   pattern_search_variable_set(visu,no).


/* pattern_search_visualisation(previous) <-
      */

pattern_search_visualisation(previous) :-
   pattern_search_variable_get(nr_list,N),
   N > 1,
   K is N - 1,
   pattern_search_variable_set(nr_list,K),
   pattern_search_visualisation(display_stock_name,K).


/* pattern_search_visualisation(next) <-
      */

pattern_search_visualisation(next) :-	
   pattern_search_variable_get(list_of_startdates,R),
   pattern_search_variable_get(nr_list,N),
   length(R,L),
   N < L,
   K is N + 1,
   pattern_search_variable_set(nr_list,K),
   pattern_search_visualisation(display_stock_name,K).


/* pattern_search_visualisation(display_stock_name) <-
      */

pattern_search_visualisation(display_stock_name, N) :-
%  pattern_search_variable_get(list_of_startdates,R),
   pattern_search_variable_get(stocklist,Sl),
   pattern_search_variable_get(market, Mar),
   nth(N,Sl,N2),	
%  nth(N,R,R2),
%  flatten(R2, R1),
%  ( R1 \= [] ->
%    send(@dates_browser, members, R1)
%  ; send(@dates_browser, members, [' ', ' ', ' ', ' ',
%       '                                 No pattern',
%       '                                of this shape',
%       '                                  available!']) ),
   pattern_search_variable_get(number_name_list,Nnl),
   member([N2-S],Nnl),
   name_remove_elements("'",S,S1),
   send(@stockname, selection(S1)),
   send(@marketname, selection(Mar)).
%  pattern_search_xpce_display_table(N).


/* pattern_search_curve_to_pattern(Curve,Pattern) <-
      */

pattern_search_curve_to_pattern(Curve,Pattern) :-
   findall( N-min(X,Y),
      ( member( N-min(B), Curve),
        get(B,x,X),
        get(B,y,Y) ),
      P1 ),
   findall( N1-max(X1,Y1),
      ( member( N1-max(B1), Curve),
        get(B1,x,X1),
        get(B1,y,Y1) ),
      P2 ),
   append(P1, P2, P3),
   sort(P3, Pattern).


/* pattern_search_visualisation(x_check) <_
      */

pattern_search_visualisation(x_check, Pattern, ok) :-
   findall( N-X,
      ( member( N-min(X,_), Pattern)
      ; member( N-max(X,_), Pattern) ),
      L ),
   sort(L, L1),
   maplist( select_second_of_two,
      L1, L2 ),
   sort(L2,L2),
   pattern_search_variable_set(status,ok),
   !.
pattern_search_visualisation(x_check, _, fail(x)).


/* pattern_search_visualisation(min_max_check) <_
      */

pattern_search_visualisation(min_max_check, Pattern, ok, Status) :-
   pattern_search_visualisation(min_max_check, Pattern, Status),
   !.
pattern_search_visualisation(min_max_check, _, Status, Status).


/* pattern_search_visualisation(
         min_max_check, Pattern, Result ) <-
      The first rule checks if the pattern has 3 nodes in a row.
      The rules 2 and 3 check if the pattern is upside down. */

pattern_search_visualisation(min_max_check, Pattern, fail(y)) :-
   findall( N-Y,
      ( member( N-min(_,Y), Pattern)
      ; member( N-max(_,Y), Pattern) ),
      L1 ),
   sort(L1,L2),
   select_second_of_two(L2,L),
   append(_,[A,B,C|_],L),
   ( ( A > B, B > C )
   ; ( A < B, B < C ) ),
   !.

pattern_search_visualisation(min_max_check, Pattern, fail(y)) :-
   findall( X,
      member(X, Pattern),
      List ),
   append(_,[_-min(_,Y1),_-max(_,Y2),_-min(_,Y3)|_], List),
   ( Y1 < Y2
   ; Y3 < Y2 ).

pattern_search_visualisation(min_max_check, Pattern, fail(y)) :-
   findall( X,
      member(X, Pattern),
      List ),
   append(_,[_-max(_,Y1),_-min(_,Y2),_-max(_,Y3)|_], List),
   ( Y1 > Y2
   ; Y3 > Y2 ).

pattern_search_visualisation(min_max_check, _, ok) :-
   pattern_search_variable_set(status,ok).	


/******************************************************************/


