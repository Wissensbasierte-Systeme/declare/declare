

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Pattern Search - Evaluation          ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      pattern_search_evaluation/1,
      pattern_search_evaluation/2,
      pattern_search_evaluation/3,
      pattern_search_evaluation/4,
      pattern_search_evaluation/5,
      pattern_search_evaluation/6.


/* pattern_search_evaluation(get_evaluation_info) <-
      */

pattern_search_evaluation(
      get_evaluation_info, Stock_States, Pattern ) :-
   pattern_search_variables_get([
      stocklist - Selected,
      number_name_list - Number_Name,
      curve - Curve,
      test - Test ]),
   pattern_search_curve_to_pattern(Curve,Pattern),
   maplist(
      pattern_search_evaluation(
         get_stock_state, Test, Number_Name ),
      Selected, Stock_States ).


/* pattern_search_evaluation(refresh) <-
      */

pattern_search_evaluation(refresh) :-
   pattern_search_evaluation(
      get_evaluation_info, Stock_States, Pattern ),
   maplist( pattern_search_evaluation(match_pattern,Pattern),
      Stock_States, List_of_Result_Patterns_2 ),
   pattern_search_evaluation(
      date_conversions,
      List_of_Result_Patterns_2, List_of_Result_Patterns ),
   pattern_search_variable_set(
      list_of_startdates, List_of_Result_Patterns ),
   pattern_search_visualisation(display_stock_name,1).


/* pattern_search_evaluation(get_stock_state) <-
      */

pattern_search_evaluation(
      get_stock_state, no_test, Number_name, X, State ) :-
   member([X-Name], Number_name),
   name_remove_elements("'", Name, Name_1),
%  mysql_select_by_name(Name_1,State_1),
%  stock_state_add_previous_values(long,year,State_1,State_2),
%  stock_state_sort_by_date(State_2,State).
   dislog_variable_get(thin_parameter,N),
   stock_state_for_pattern_search(Name_1,N,State),
   length(State,Length),
   writeln(Name_1-Length).
pattern_search_evaluation(
      get_stock_state, test, _, _, State ) :-
   pattern_search_test_state(State_1),
   stock_state_sort_by_date(State_1,State).


/* pattern_search_evaluation(normalize) <-
      */

pattern_search_evaluation(
      normalize, Time_Values_1, Time_Values_2 ) :-
   maplist( select_second_of_two,
      Time_Values_1, Values_1 ),
   first(Values_1,V),
   maplist( time_value_divide(V),
      Time_Values_1, Time_Values_2 ).

time_value_divide(X,T-V1,T-V2) :-
   V2 is V1 / X.


/* pattern_search_evaluation(get_pattern_range) <-
      */

pattern_search_evaluation(
      get_pattern_range, Pattern, Pattern_Range ) :-
   findall( Y-N-min,
      member(N-min(_,Y),Pattern),
      Y_Boxes_1 ),
   findall( Y1-N1-max,
      member(N1-max(_,Y1),Pattern),
      Y_Boxes_2 ),
   append(Y_Boxes_1,Y_Boxes_2,Y_Boxes_3),
   sort(Y_Boxes_3,Y_Boxes_4),
   maplist( select_second_and_third_of_three,
      Y_Boxes_4, Y_Boxes ),
   reverse(Y_Boxes,Pattern_Range).


/* pattern_search_evaluation(chart_reduction) <-
      */

pattern_search_evaluation(chart_reduction,Time_Value,Chart) :-
   findall( X-Y-Type,
      ( append(_,[_-Y1,X-Y,_-Y2|_],Time_Value),
        ( ( Y1 > Y,
            Y2 > Y,
            Type = min )
        ; ( Y1 < Y,
            Y2 < Y,
            Type = max ) ) ),
      Chart_1 ),
   sort(Chart_1,Chart_2),
   maplist( pattern_search_change_to_chart_form,
      Chart_2, Chart ).

pattern_search_change_to_chart_form(X-Y-min,min(X,Y)).
pattern_search_change_to_chart_form(X-Y-max,max(X,Y)).


/* pattern_search_evaluation(
         match_pattern, Pattern, State, Result_Patterns ) <-
      */

pattern_search_evaluation(
      match_pattern, Pattern, State, Result_Patterns ) :-
   pattern_search_variable_get(smooth,Smooth),
   stock_state_smooth(Smooth,State,State_1),
   stock_state_to_time_values(State_1,Time_Values_1),
   sort(Time_Values_1,Time_Values_2),
   pattern_search_evaluation(
      chart_reduction, Time_Values_2, Chart_2 ),
   sort(Chart_2,Chart),
   pattern_search_match_pattern_chart(
      Pattern, Chart, Result_Patterns ).

pattern_search_match_pattern_chart(
      Pattern, Chart, Result_Patterns ) :-
   pattern_search_evaluation(get_pattern_range,Pattern,Range),
   maplist( pattern_search_change_to_chart_form,
      Chart_2, Chart ),
   sort(Chart_2,Chart_3),
   pattern_search_match_pattern_chart(
      Pattern, Range, Chart_3, Result_Patterns ).

pattern_search_match_pattern_chart(
      Pattern, Range, Chart_3, Result_Patterns ) :-
   length(Pattern,N),
   n_free_variables(N,Xs),  
   n_free_variables(N,Ys),  
   n_free_variables(N,Ms),
   generate_interval(1,N,Is),
   triple_lists_2(Xs,Ys,Ms,XYM_s),
   triple_lists_2(Ys,Ms,Is,YMI_s),
   pair_lists_2(Xs,Ys,XY_s),
   first(Xs,X),
   findall( X-XY_s,
      ( middle_sequence(Chart_3,XYM_s),
        sort(YMI_s,YMI_s_2),
        maplist( pattern_search_change_to_range_form,
           YMI_s_2, YMI_Range ),
        YMI_Range = Range ),
      Result_Patterns ).

pattern_search_change_to_range_form(_-M-N,N-M).	  	  	


/* pattern_search_evaluation(date_conversions) <-
      */

pattern_search_evaluation(
      date_conversions, List_of_results, Date_Names_Lists ) :-
   maplist( select_second_of_two_list,
      List_of_results, Exacts ),
   pattern_search_variable_set(exact,Exacts),
   maplist( pattern_search_evaluation(exact_to_date_names),
      Exacts, Date_Names_Lists ).


/* pattern_search_evaluation(exact_to_date_names) <-
      */

pattern_search_evaluation(
      exact_to_date_names, Exact, [Lines] ) :-
   maplist( pattern_search_extract_info_from_exact,
      Exact, Lines ).


/* pattern_search_xpce_display_table <-
      */

pattern_search_xpce_display_table :-
   pattern_search_variable_get(nr_list,N),
   pattern_search_xpce_display_table(N).

pattern_search_xpce_display_table(N) :-
   pattern_search_variable_get(exact,Exacts),
   nth(N,Exacts,Exact),
   maplist( pattern_search_extract_info_from_exact_2,
      Exact, Tuples_2 ),
   maplist( tuple_change_dm_to_euro,
      Tuples_2, Tuples ),
   pattern_search_get_item(Company),
   xpce_display_table(_,_,Company,[from,to,min,max],Tuples).

tuple_change_dm_to_euro([F,T,X1,X2],[F,T,Y1,Y2]) :-
   values_dm_to_euro([X1,X2],[Y1,Y2]).


/* pattern_search_extract_info_from_exact(TVs,Line) <-
      */

pattern_search_extract_info_from_exact(TVs,Line) :-
   pattern_search_extract_info_from_exact_2(
      TVs, [Min_D,Max_D,Min_V,Max_V] ),
   name_append([
      'from: ',Min_D, ', to: ', Max_D, ',     ',
      'min: ', Min_V, ', max: ', Max_V], Line_2 ),
   name_remove_elements("'",Line_2,Line).


/* pattern_search_extract_info_from_exact_2(TVs,Line) <-
      */

pattern_search_extract_info_from_exact_2(TVs,Line) :-
   pair_lists_2(Ts,Vs,TVs),
   list_to_min_and_max(Ts,Min_T,Max_T),
   list_to_min_and_max(Vs,Min_V,Max_V),
   time_stamp_to_date(Max_T,Max_D),
   date_to_natural_date(Max_D,Max_DN),
   time_stamp_to_date(Min_T,Min_D),
   date_to_natural_date(Min_D,Min_DN),
   Line = [Min_DN,Max_DN,Min_V,Max_V].


/* pattern_search_evaluation(exact) <-
      */

pattern_search_evaluation(exact) :-
   pattern_search_evaluation(show_chart),
   pattern_search_variables_get([
      exact - Exacts,
      nr_list - N,
      curve - Curve ]),
   nth(N,Exacts,Coordinates),
   pattern_search_curve_to_pattern(Curve,Pattern),
   pattern_search_coordinates_to_percentages(
      Pattern, Coordinates, Percentages ),
   pattern_search_percentages_to_boxes(
      Coordinates, Percentages, Boxes ),
   pattern_search_boxes_to_state(Boxes,Box_State),
   pattern_search_get_item(Item),
   stock_chart_insert_boxes(Item,Box_State).


/* pattern_search_coordinates_to_percentages(
         Pattern, Coordinates, Percentages ) <-
      */

pattern_search_coordinates_to_percentages(
      Pattern, Coordinates, Percentages ) :-
   pattern_search_pattern_to_pattern_coordinates(
      Pattern, Pattern_Coordinates ),
   maplist(
      pattern_search_coordinate_to_percentage(
         Pattern_Coordinates ),
      Coordinates, Percentages ).


/* pattern_search_pattern_to_pattern_coordinates(
         Pattern, Pattern_Coordinates ) <-
      */

pattern_search_pattern_to_pattern_coordinates(
      Pattern, Pattern_Coordinates ) :-
   findall( N-X-Y,
      ( member(N-min(X,Y),Pattern)
      ; member(N-max(X,Y),Pattern) ),
      Node_Coordinates_1 ),
   sort(Node_Coordinates_1,Node_Coordinates_2),
   maplist( select_second_and_third_of_three,
      Node_Coordinates_2, Node_Coordinates ),
   first(Node_Coordinates,Bottom),
   select_second_of_two(Bottom,Start),
   maplist(
      pattern_search_invert_y_coordinates(Start),
      Node_Coordinates, Pattern_Coordinates ).

pattern_search_invert_y_coordinates(Start,X-Y1,X-Y2) :-
   Y2 is 2 * Start - Y1.
%  ( Y1 > Start ->
%    Y2 is Start - ( Y1 - Start )
%  ; Y2 is Start + ( Start - Y1 ) ).


/* pattern_search_percentages_to_boxes(
         Coordinates, Percentages, Boxes ) <-
      */

pattern_search_percentages_to_boxes(
      Coordinates, Percentages, Boxes ) :-
   pair_lists_2(Coordinates,Percentages,Pairs),
   maplist( pattern_search_co_per_pair_to_box,
      Pairs, Boxes ).

pattern_search_co_per_pair_to_box(XYs-Percentage,Box) :-
   pair_lists_2(Xs,Ys,XYs),
   list_to_min_and_max(Xs,X_min,X_max),
   list_to_min_and_max(Ys,Y_min,Y_max),
   ( Percentage < 45 ->
     Colour = red
   ; Colour = black ),
   Box = [X_min-Y_min,X_max-Y_max,Percentage,Colour].


/* pattern_search_boxes_to_state(Boxes,Box_State) <-
      */

pattern_search_boxes_to_state(Boxes,Box_State) :-
   pattern_search_get_item(Name),
   name_append(['''',Name,''''],Name_2),
   pattern_search_boxes_to_state(Name_2,Boxes,Box_State).

pattern_search_boxes_to_state(Name,Boxes,Box_State) :-
   maplist( pattern_search_box_to_state(Name),
      Boxes, Box_States ),
   append(Box_States,Box_State).

pattern_search_box_to_state(Name,Box,State) :-
   Box = [D1-V1,D2-V2,Per,Colour],
   State = [
      [stock(Name,Per,D1,V1,[Colour])],
      [stock(Name,Per,D2,V2,[Colour])] ].


/* pattern_search_evaluation(show_chart) <-
      */

pattern_search_evaluation(show_chart) :-
   dislog_variable_get(smooth_parameter,Main_Smooth),
   pattern_search_get_item(Item),
   pattern_search_variable_get(smooth,Smooth),
   dislog_variable_set(smooth_parameter,Smooth),
%  chart(Item),
   dislog_variable_get(thin_parameter,N),
   stock_state_for_pattern_search(Item,N,State_1),
   stock_state_smooth(State_1,State_2),
   stock_state_to_display(Item,State_2),
   dislog_variable_set(smooth_parameter,Main_Smooth).


/* pattern_search_coordinate_to_percentage(
         Pattern_Coordinates, Coordinate, Percentage ) <-
      */

pattern_search_coordinate_to_percentage(
      Pattern_Coordinates, Coordinates, Percentage ) :-
   pattern_search_coordinate_to_percentage_sub(
      Pattern_Coordinates, Pattern_Coordinates_2, PC_Dist ),
   pattern_search_coordinate_to_percentage_sub(
      Coordinates, Coordinates_2, C_Dist ),
   pattern_search_evaluation(
      get_percentages, Pattern_Coordinates_2, Coordinates_2,
      PC_Dist, C_Dist, Percentages ),
   average(Percentages,Average),
   Percentage is Average * 100.

pattern_search_coordinate_to_percentage_sub(
      Coordinates_1, Coordinates_2, C_Dist_X-C_Dist_Y ) :-
   first(Coordinates_1,C_First),
   select_first_of_two(C_First,C_First_X),
   last_element(Coordinates_1,C_Last),
   select_first_of_two(C_Last,C_Last_X),
   C_Dist_X is C_Last_X - C_First_X,
   maplist( select_second_of_two,
      Coordinates_1, C_Ys ),
   list_to_min_and_max(C_Ys,C_Y_min,C_Y_max),
   C_Dist_Y is C_Y_max - C_Y_min,
   list_delete_first_element(Coordinates_1,Coordinates_3),
   maplist(
      pattern_search_normalize(C_First_X-C_Y_min),
      Coordinates_3, Coordinates_2 ).

list_to_min_and_max(List,Min,Max) :-
   min_list(List,Min),
   max_list(List,Max).
   
list_delete_first_element(Xs,Ys) :-
   first(Xs,X),
   list_remove_elements([X],Xs,Ys).

pattern_search_normalize(X_min-Y_min,X1-Y1,X2-Y2) :-
   X2 is X1 - X_min,
   Y2 is Y1 - Y_min.


/* pattern_search_evaluation(get_percentages) <-
      */

pattern_search_evaluation(
      get_percentages,
      XYps, XYcs, Dp_x-Dp_y, Dc_x-Dc_y, Percentages ) :-
   pair_lists(XYps,XYcs,Pairs),
   maplist(
      pattern_search_evaluation_get_percentage(
         Dp_x-Dp_y, Dc_x-Dc_y ),
      Pairs, Percentages ).

pattern_search_evaluation_get_percentage(
      Dp_x-Dp_y, Dc_x-Dc_y, [Xp-Yp,Xc-Yc], Percentage ) :-
   Pp_x is Xp / Dp_x,
   Pc_x is Xc / Dc_x,
   Pp_y is Yp / Dp_y,
   Pc_y is Yc / Dc_y,
   euklidian_distance([Pp_x,Pp_y],[Pc_x,Pc_y],Distance),
   Percentage = Distance / sqrt(2),
   !.


/* pattern_search_get_item(Item) <-
      */

pattern_search_get_item(Item) :-
   pattern_search_variables_get([
      nr_list - N,
      stocklist - Selected,
      number_name_list - Number_Names ]),
   nth(N,Selected,Number),
   member([Number-Item_with_Quotes],Number_Names),
   name_remove_elements("'",Item_with_Quotes,Item).


/******************************************************************/


