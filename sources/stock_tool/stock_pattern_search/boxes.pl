

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Pattern Search - XPCE Boxes          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


stock_chart_insert_boxes :-
   Box_State = [
      [stock('BMW St', 585, 37, 750, [])],
      [stock('BMW St', 585, 187, 1110, [])],
      [stock('BMW St', 585, 405, 600, [])],
      [stock('BMW St', 585, 500, 750, [])] ],
   writeln_list(Box_State),
   stock_chart_insert_boxes('BMW St',Box_State).


stock_chart_insert_boxes(Item,Box_State) :-
   dislog_variables_get([
      dsa_chart_picture - Picture,
      dsa_chart_origin - Origin,
      x_factor - Factor ]),
%  dportray(lp,Box_State),
%  value_dm_to_euro(1,Rate),
%  maplist( stock_clause_multiply_value(1/Rate),
%     Box_State, State ),
%  dportray(lp,Box_State),
   stock_state_to_graph_boxes(regular,logarithmic,
      Item,Box_State,Time_Values_N),
%  write_list(['Time_Values_N = ',Time_Values_N]), nl,
   maplist( value_to_xpce_vector(Origin,Factor),
      Time_Values_N, Vectors ),
   send_window_boxes(Picture,Vectors).


stock_clause_multiply_value(Factor,C1,C2) :-
   C1 = [stock(Company,Number,Date,Value_1,Previous)],
   Value_2 is Factor * Value_1,
   C2 = [stock(Company,Number,Date,Value_2,Previous)].


/*** implementation ***********************************************/


stock_state_to_graph_boxes(regular,D_Mode,Company,State,
      Time_Value_Pairs_N ) :-
   maplist( stock_clause_to_time_and_value,
      State, Time_Value_Pairs ),
   stock_print_parameters(Company,
      [_Color,_Diameter,Origin_X,Schrink_X]),
   stock_item_to_mean_value(Company,Mean),
   stock_chart_compute_y_origin_and_schrink(
      Mean,Origin_Y,Schrink_Y),
   maplist( value_normalize_2(linear-D_Mode,
      Origin_X-Origin_Y,Schrink_X-Schrink_Y),
      Time_Value_Pairs, Time_Value_Pairs_N ).


stock_clause_to_time_and_value([stock(_,_,T,V1,_)],T-V2) :-
   V2 is V1 * 1.02.


stock_item_to_mean_value(Item,Mean) :-
   mysql_select_by_name(Item,State_1),
   stock_state_sort_by_date(State_1,State_2),
   pattern_search_variable_get(smooth,N),
   stock_state_smooth(N,State_2,State_3),
   stock_state_to_mean_value(State_3,Mean).


send_window_boxes(Picture,[[X1,Y1],[X2,Y2]|XYs]) :-
   X3 is X2 - X1,
   Y3 is Y2 - Y1,
   send(Picture,display,new(Box,box(X3,Y3)),point(X1,Y1)),
   send(Box,colour(red)),
   send_window_boxes(Picture,XYs).
send_window_boxes(_,[]).


/******************************************************************/

 
