

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Pattern Search - Elementary          ***/
/***                                                            ***/
/******************************************************************/



/* pattern_search_variable_set <-
      */

pattern_search_variable_set(Variable,Value) :-
   retractall(pattern_search_variable(Variable,_)),
   assert(pattern_search_variable(Variable,Value)).

pattern_search_variables_set([X-Y|Ps]) :-
   pattern_search_variable_set(X,Y),
   pattern_search_variables_set(Ps).
pattern_search_variables_set([]).


/* pattern_search_variable_get <-
      */

pattern_search_variable_get(Variable,Value) :-
   pattern_search_variable(Variable,Value).

pattern_search_variables_get([X-Y|Ps]) :-
   pattern_search_variable_get(X,Y),
   pattern_search_variables_get(Ps).
pattern_search_variables_get([]).


/* show_pattern_search_variables <-
      */

show_pattern_search_variables :-	
   nl, writeln('Pattern Search - Variables:'), nl,
   writeln_list([
      link: 'Link item',
      nodes: 'Number of nodes',
      start: 'Min or Max',
      status: 'Status of display',
      market: 'The actual market',
      stocklist: 'List of selected stock numbers',
      smooth: 'Smooth parameter',
      number_name_list:
         'list of numbers and names within the chosen market',
      nr_list:
         'stocklist number of displayed stock in values dialog',
      curve: 'Drawn pattern',
      list_of_startdates:
         'list of dates where similar patterns start in chart' ]),
   write('For nodes, start and smooth there exists '),
   writeln('the corresponding variable'),
   write('beginning with old, which stores '),
   writeln('the former settings before a change').


/* change_dash_to_blanks(W) <-
      */

change_dash_to_blanks(-1) :-
   !.
change_dash_to_blanks(45) :-
   put(32),
   get0(W1),
   !,
   change_dash_to_blanks(W1).
change_dash_to_blanks(W) :-
   put(W),
   get0(W1),
   !,
   change_dash_to_blanks(W1).


/* remove_dashes_from_file(File,In) <-
      */

remove_dashes_from_file(File,In) :-
   see(File),
   tell(In),
   get0(W),
   change_dash_to_blanks(W),
   told,
   seen.


/* change_chars_in_file(A,B, File1, File2) <-
      */

change_chars_in_file(A,B, File1, File2) :-
   see(File1),
   tell(File2),
   get0(W),
   change_chars(A,B,W),
   told,
   seen.
change_chars(_,_,-1) :-
   !.
change_chars(A, B, A) :-
   put(B),
   get0(W),
   change_chars(A,B,W),
   !.
change_chars(A, B, C) :-
   put(C),
   get0(W),
   change_chars(A, B, W).


/* append_index(N,Xs, L) <-
      */

append_index(N,[X|Xs], L) :-
   N1 is N + 1,
   append_index(N1, Xs, L1),
   name_to_quoted_name(X, X1),
   append([[N-X1]], L1, L).
append_index(_,[],[]).


/******************************************************************/


