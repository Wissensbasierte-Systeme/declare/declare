

/******************************************************************/
/***                                                            ***/
/***          Stock Tool:  Pattern Search - Clustering          ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      clustering/1,
      clustering/2,
      clustering/3,
      clustering/4.


/*** interface ****************************************************/


/* clustering(dialog) <-
      */

clustering(dialog) :-
   pattern_search( create_dialog, [
      name:cluster_dialog,
      title:'Clustering',
      menu_bar:[ title:'File',items:[
         quit - clustering(end) ]]:right,
      list_browser:[
         name:cluster_browser1,label:'Clusters', width:40]:below,
         menu_bar:[ title:'Wavelet Type', items:[
         haar -  clustering(select_wavelet, haar),
         daubechies_2 - clustering(select_wavelet, daub2),
         daubechies_3 - clustering(select_wavelet, daub3),
         bSpline_3 - clustering(select_wavelet, bspline3),
         bSpline_4 - clustering(select_wavelet, bspline4) ]]:next_row,
      text_item:[name:wavelet, label:'Wavelet', selection:' ']:below,
      menu_bar:[ title:'Data Sources', items:[
         dax_30 - clustering(visualisation, dax_30),
         test - clustering(visualisation, test),
         dax_100 - clustering(visualisation, dax_100),
         neuermarkt - clustering(visualisation, neuermarkt),
         eurostock - clustering(visualisation, eurostock) ]]:below ]),
   send(@cluster_browser1, height, 18),
   send(@cluster_dialog, open),
   send(@wavelet, selection('haar')),
%  send(@approx, selection('spline')),
   clustering(initialize).


/* clustering(initialize) <-
      */

clustering(initialize) :-
   pattern_search_variable_set(wavelet, haar),
   pattern_search_variable_set(approx, spline),
   pattern_search_variable_set(threshold-haar, 0.015),
   pattern_search_variable_set(threshold-daub2,0.015),
   pattern_search_variable_set(threshold-daub3, 0.015),
   pattern_search_variable_set(threshold-bspline3, 0.015),
   pattern_search_variable_set(threshold-bspline4, 0.015),
   pattern_search_variable_set(get_tv_bar, off),
   pattern_search_variable_set(compare_bar, off).


/* clustering(end) <-
      */

clustering(end) :-
   checklist( free, [
      @cluster_dialog, @wavelet, @approx, @cluster_browser1,
      @bardev, @pict ] ).


clustering(select_wavelet, Wavelet_type) :-
   pattern_search_variable_set(wavelet, Wavelet_type),
   send(@wavelet, selection(Wavelet_type)).

clustering(select_approx, Approx_type) :-
   pattern_search_variable_set(approx, Approx_type),
   send(@approx, selection(Approx_type)).


/* clustering(visualisation) <-
      */

clustering(visualisation, Selection) :-
   send(@cluster_browser1, members([' '])),
   send(@cluster_browser1, flush),
   send(@cluster_browser1, members, [' ']),
   pattern_search_get_browser_and_numbername_list(Selection,
      Browserlist, Numbernamelist),
   append(['verfuegbare Titel: '], Browserlist, Browserlist1),
   send(@cluster_browser1, members(Browserlist1)),
   ( pattern_search_variable_get(number_name_list_cl, Num_alt) ->
     pattern_search_variable_set(old_number_name_list_cl, Num_alt)
   ; pattern_search_variable_set(old_number_name_list_cl, dummy) ),
   pattern_search_variable_set(number_name_list_cl, Numbernamelist),
   length(Numbernamelist, Length),
   generate_interval(1, Length, Int),
   ( pattern_search_variable_get(
        old_number_name_list_cl, Numbernamelist) ->
     write_ln( '---> cached Data')
   ; ( start_status_bar(get_tv_bar, 'Importing Charts', Length),
       maplist( clustering(get_tv),
          Int, _ ),
       ( pattern_search_variable_get(get_tv_bar, on) ->
         status_bar_disappear(get_tv_bar) ) ) ),
   pattern_search_variable_get(wavelet, Wave),
   clustering(create_cppinfo_file, Wave),
   concat_atom(['Comparing Charts using ',Wave, ' wavelet'], Label),
   Clength is Length * ( Length + 1 ) / 2,
   start_status_bar(compare_bar, Label, Clength),
   maplist( clustering(compare, Length),
      Int, Result ),
   ( pattern_search_variable_get(compare_bar, on) ->
     status_bar_disappear(compare_bar) ),
   list_remove_elements(' ', Result, Result1),
   clustering(rem_duplicate_list_entries, Result1, Result2),
   flatten(Result2, Resu),
   remove_front_blanks(Resu, Resu1),
   ( Resu1 = [] ->
     Resu2 = ['  no matching!']
   ; Resu2 = Resu1 ),
   send(@cluster_browser1, members, Resu2),
   free(@bar),
   write_ln(Resu).

remove_front_blanks([' '|Xs], R) :-
   remove_front_blanks(Xs, R),
   !.
remove_front_blanks(X, X).


/* clustering(rem_duplicate_list_entries) <-
      */

clustering(rem_duplicate_list_entries, [X|Xs], R) :-
   findall( M,
      ( member(M,Xs),
        sublist(M,X) ),
      List ),
   list_remove_elements(List, Xs, Xs1),
   clustering(rem_duplicate_list_entries, Xs1, R1),
   append([X],R1, R).
clustering(rem_duplicate_list_entries, [], []).


/* clustering(get_dualcoeff) <-
      */

clustering(get_dualcoeff) :-
   absolute_file_name('', Path1),
   concat_atom([ Path1,
      'Stock_Analysis/results/cpp_info' ], Cpp),
   concat_atom([ Path1,
      'sources/stock_tool/tools/dual_coeff' ], Prog),
   concat_atom([Prog, ' ', Cpp], Op),
   shell(Op).


/* clustering(get_tv,X,Tv3) <-
      */

clustering(get_tv, X, Tv3) :-
   pattern_search_variable_get(get_tv_bar, on),
   !,
   status_bar_increase(get_tv_bar),
   send(@display, synchronise),
   pattern_search_variable_get(number_name_list_cl, Numbernamelist),
   member([X-Name11], Numbernamelist),
   name_remove_elements("'", Name11, Name1),
   mysql_select_by_name(Name1,State11),
   stock_state_to_time_values(State11, Tv),
   findall( Time-Val, ( member(Time-Val,Tv), Time<700 ), Tvv),
   sort(Tvv, Tv1),
%  outliers_remove(Tv1, Tv11),
   ( length(Tv1,0) ->
     Tv2 = [1-0]
   ; Tv2 = Tv1 ),
   clustering(invers_split, Name1, Tv2, Tv3),
   !,
   clustering(normalize, Tv3, Tv4),
   pattern_search_variable_set(tv-X,Tv4),
   clustering(create_plotfile, X, Name1),
   write_ln(['read in ',Name1]).
clustering(get_tv, _, _).


outliers_remove([X1,X2|Xs], R) :-
   X2 < 2 * X1 / 3,
   X3 is X2 * 2,
   outliers_remove([X3|Xs], R1),
   append([X1],R1,R),
   !.
outliers_remove([X1,X2|Xs], R) :-
   outliers_remove([X2|Xs], R1),
   append([X1],R1,R),
   !.
outliers_remove(Xs,Xs).


/* clustering(normalize) <-
      */

clustering(normalize, [A-B|Xs], [A-1.0|R]) :-
   maplist( clustering_divide_second(B),
      Xs, R ).

clustering_divide_second(D, A-B, A-B1) :-
   B1 is B / D.


/* clustering(remove_doubles) <-
      */

clustering(remove_doubles, [], []) :-
   !.

clustering(remove_doubles, [D-V|Xs], B) :-
   list_remove_elements([D-_,D-_,D-_,D-_,D-_],Xs, X1s),
   !,
   clustering(remove_doubles, X1s, B1),
   append([D-V],B1,B).


/* clustering(invers_split) <-
      */

clustering_split('Karstadt',700, 15).
clustering_split('Linde',576, 10).
clustering_split('MAN St',422, 10).
clustering_split('Preussag',470, 10).
clustering_split('Schering',882, 3).
clustering_split('VIAG',604, 25).
clustering_split('SAP AG Vz',905, 3).


clustering(invers_split,Name,A, B) :-
   clustering_split(Name, Day, Factor),
   !,
   maplist( clustering_multiply(Day, Factor),
      A, B ).
clustering(invers_split, _, A, A).

clustering_multiply(T, W, D-V, D-V1) :-
   D > T,
   V1 is V * W,
   !.
clustering_multiply(_, _, D-V, D-V).


clustering_multiply(X1, X2, Y, D-V, D-V1) :-
   D > X1,
   D < X2,
   V < Y,
   V1 is V * 2,
   !.
clustering_multiply(_, _, _, D-V, D-V).


/* clustering(compare) <-
      */

clustering(compare, Length, X, R) :-
   pattern_search_variable_get(compare_bar, on),
   !,
   X1 is X + 1,
   generate_interval(X1, Length, Int),
   maplist( clustering(compare1, X),
      Int, Res ),
   !,
   pattern_search_variable_get(wavelet, Wave),
   pattern_search_variable_get(threshold-Wave, Thresh),
   findall( K, (member([X,K,D], Res), D<Thresh), Re),
   append([X], Re, Resu),   write_ln(Resu),
   clustering(number_to_name, Resu, R1),
   append(R1, [' '], R2),
   ( ( length(R2, L), L < 3 ) ->
     R = [' ']
   ; R = R2 ),
   !.
clustering(compare, _, _, [' ']).

clustering(number_to_name, A, B) :-
   pattern_search_variable_get(
      number_name_list_cl, Numbernamelist),
   findall( N,
      ( member(Nr, A),
        member([Nr-N], Numbernamelist) ),
      B1 ),
   maplist( name_remove_elements("'"),
      B1, B ).


/* clustering(compare1, X, Y, R) <-
      */

clustering(compare1, X, Y, R) :-
   pattern_search_variable_get(compare_bar, on),
   !,
   status_bar_increase(compare_bar),
   pattern_search_variable_get(tv-X,Time_v1),
   pattern_search_variable_get(tv-Y,Time_v2),
   clustering(get_unifiers, Time_v1-Time_v2, Tv_li1),
   clustering(get_unifiers, Time_v2-Time_v1, Tv_li2),
   clustering(split_tv, Tv_li1, Tv_list1),
   !,
   clustering(split_tv, Tv_li2, Tv_list2),
   !,
   maplist( clustering(remove_doubles),
      Tv_list1, Tvlist1 ),
   !,
   maplist( clustering(remove_doubles),
      Tv_list2, Tvlist2 ),
   !,
   findall( El1,
      ( member(El1,Tvlist1),
        length(El1,Length1),
        Length1 < 33 ),
      Del1 ),
   list_remove_elements(Del1, Tvlist1, Tv1),
   findall( El2,
      ( member(El2,Tvlist2),
        length(El2,Length2),
        Length2 < 33 ),
      Del2 ),
   list_remove_elements(Del2, Tvlist2, Tv2),
   length(Tv1, Length),
   generate_interval(1, Length, Int),
   maplist( clustering(compare2, X-Y, Tv1, Tv2),
      Int, R_list ),
   !,
   length(R_list, R_length),
   sum_list(R_list, Sum),
   !,
   Diff is Sum/R_length,
   append([X,Y],[Diff], R),
   pattern_search_variable_get(wavelet, Wave),
   write_list(['Wavelet: ',Wave, ',     ', 'Diff: ', Diff]), nl,
   !.
clustering(compare1, _,_,1000).


/* clustering(compare2, X-Y, Tv_list1, Tv_list2, N, R) <-
      there exists one rule for Haar wavelets and one rule for
      the other wavelets. */

clustering(compare2, X-Y, Tv_list1, Tv_list2, N, R) :-
   pattern_search_variable_get(wavelet, haar),
   !,
   pattern_search_variable_get(compare_bar, on),
   !,
   nth(N, Tv_list1, T1),
   maplist( select_second_of_two,
      T1, Tvs1 ),
   nth(N, Tv_list2, T2),
   maplist( select_second_of_two,
      T2, Tvs2 ),
   get_pot(Tvs1,P),               % predicates in the appendix
%  append_zeros(Tvs1, P, Tv1),
%  append_zeros(Tvs2, P, Tv2),
   first_n_elements(P, Tvs1, Tv1),
   first_n_elements(P, Tvs2, Tv2),
   clustering(get_diff, Tv1, Tv2, Diffges),
   haar_algorithm(Tv1, Coeff1),
   stock_file(results,Res),
   concat_atom([Res, '/Koeff/', X],File1),
   tell(File1), writeln_list(Coeff1), told,
   haar_algorithm(Tv2, Coeff2),
   concat_atom([Res, '/Koeff/', Y],File2),
   tell(File2), writeln_list(Coeff2), told,
   write_list(['Haar-Alg.: ',X, '  -  ', Y,
      '     Differenz  ', Diffges]), nl,
   clustering(get_diff, Coeff1, Coeff2, R).

clustering(compare2, X-Y, Tv_list1, Tv_list2, N, R) :-
   pattern_search_variable_get(compare_bar, on), !,
   nth(N, Tv_list1, T1),
   nth(N, Tv_list2, T2),
   clustering(get_diff2, T1, T2, Diffges),
   write_ln(['Diff_ges', Diffges]),
   clustering(create_files,T1, T2),
   write_ln(X-Y),
   stock_file(results, tv1, Cp1_file),
   pattern_search_variable_get(wavelet, Wave),
   clustering(get_coeff, Wave, Cp1_file, Coeff1),
   Apptv11 = T1,
   stock_file(results, tv2, Cp2_file),
   clustering(get_coeff, Wave, Cp2_file, Coeff2), !,
   diconsult(
      '/home/chrissi/DisLog/Stock_Analysis/results/Approximation',
      App2),
   approx_to_tv(App2, Apptv21),
   clustering(get_unifiers, Apptv11-Apptv21, Apptv1),
   clustering(get_unifiers, Apptv21-Apptv11, Apptv2),
   sort(Apptv2,Apptv222),
   sort(Apptv1,Apptv111),
   maplist( select_second_of_two,
      Apptv111, Valuesapp1 ),
   maplist( select_second_of_two,
      Apptv222, Valuesapp2 ),
   clustering(get_diff2, Valuesapp1, Valuesapp2, Diff_proj),
   writeln_list(['Differenz Projektionen: ',Diff_proj]),
%  clustering(get_diff, Valuesanf2, Valuesapp2, AnfangApp),
%  writeln_list(['Differenz Anfang-Naeherung: ',AnfangApp]),
   clustering(get_diff2, Coeff1, Coeff2, R).
clustering(compare2, _-_, _, _, _, 1000).

approx_to_tv(A, B) :-
  findall(T-V,
     member([approximation(T, V)], A),
     B ).


/* clustering(get_unifiers) <-
      */

clustering(get_unifiers, Tv1-Tv2, Tvlist) :-
   findall( T-V,
      ( member(T-V, Tv1),
        member(T-_, Tv2) ),
      Tvlist ).


/* clustering(split_tv) <-
      */

clustering(split_tv, Tvs, Tv_list) :-
   clustering_find_gaps(Tvs, Gaps),
   clustering_map_gaps_to_lists(Tvs, Gaps, Tv_list).


/* clustering_find_gaps(List, Gaps) <-
      find gaps between timestamps that are larger than 8 days  */

clustering_find_gaps([], []) :-
   !.
clustering_find_gaps(List, Gaps) :-
   findall( D2,
      ( append(_, [D1-_, D2-_|_], List),
        D is D2 - D1,
        D > 8 ),
      Gaps ).


clustering_map_gaps_to_lists(Tvs, [], [Tvs]) :-
   !.
clustering_map_gaps_to_lists(Tvs, [X|Xs], R) :-
   findall( D-V,
      ( member(D-V, Tvs),
        D < X ),
      List ),
   list_remove_elements(List, Tvs, Tvs1),
   clustering_map_gaps_to_lists(Tvs1, Xs, R1),
   append([List], R1, R).


/*  clustering(get_coeff) <-
      */

clustering(get_coeff, _, Cp, State) :-
   absolute_file_name('', Path1),
   stock_file(results, 'input', Input),
   concat_atom(['cp ', Cp, ' ',Input], Copy),
   shell(Copy),
   stock_file(results, 'cpp_info', Infofile),
   concat_atom([Path1,
      'sources/stock_tool/stock_pattern_search/tools/Ausgleich',
         ' ', Infofile], Operation),
   shell(Operation),
   stock_file(results, 'coeff.pl', Coeff_file),
   diconsult(Coeff_file, State1),
   length(State1, Length),
   ( Length > 1 ->
     findall( S,
        member([wavelet_coefficient(_,S)],State1),
        State )
   ; State = [nothing] ),
   !.


/*  clustering(get_diff) <-
      */

clustering(get_diff, _, [nothing], 1000) :-
   !.
clustering(get_diff, [nothing], _, 1000) :-
   !.
clustering(get_diff, Coeff1, Coeff2, Diff) :-
   substract_list(Coeff1, Coeff2, Diff3),
   maplist( square,
      Diff3, Diff2 ),
   sum_list(Diff2, Diff1),
   sqrt(Diff1, Dif),
   length(Coeff1, L),
   Diff is Dif/L.

clustering(get_diff2, _, [nothing], 1000) :-
   !.
clustering(get_diff2, [nothing], _, 1000) :-
   !.
clustering(get_diff2, Coeff1, Coeff2, Diff) :-
   substract_list(Coeff1, Coeff2, Diff3),
   maplist( square,
      Diff3, Diff2 ),
   sum_list(Diff2, Diff1),
   length(Coeff1, L),
   Dif is Diff1/L,
   sqrt(Dif, Diff).


/* clustering(create_files) <-
      puts time_values into files,
      that can be read by wavelet-programs. */

clustering(create_files,Tvlist1,Tvlist2) :-
   stock_file(results, 'tv1.tv', Tv1_file),
   stock_file(results, 'tv1', Cp1_file),
   stock_file(results, 'tv2.tv', Tv2_file),
   stock_file(results, 'tv2', Cp2_file),
   predicate_to_file(Tv1_file,
      writeln_list(Tvlist1) ),
   predicate_to_file(Tv2_file,
      writeln_list(Tvlist2) ),
   predicate_from_file( Tv1_file, (
      get0(W1),
      predicate_to_file(Cp1_file,change_dash_to_blanks(W1)) ) ),
   predicate_from_file( Tv2_file, (
      get0(W2),
      predicate_to_file(Cp2_file,change_dash_to_blanks(W2)) ) ),
   concat_atom(['rm ',Tv1_file], Del1),
   concat_atom(['rm ',Tv2_file], Del2),
   shell(Del1),
   shell(Del2).


/* clustering(create_cppinfo_file) <-
      */

clustering(create_cppinfo_file, Wavelet) :-
   stock_file(results, 'cpp_info', Infofile),
   stock_file(results, 'input', Input),
   stock_file(results, 'coeff.pl', Output),
   stock_file(home, '/wavelet_data/ortho_phi2', O2),
   stock_file(home, '/wavelet_data/ortho_phi3', O3),
   stock_file(home, '/wavelet_data/dualc', Dual),
   stock_file(home, '/wavelet_data/gammas', Gamma),
   wavelet_order(Wavelet, Order),
   tell(Infofile),
   write_ln(Input),
   write_ln(Output),
   write_ln(Order),
   write_ln(O2),
   write_ln(O3),
   write_ln(Dual),
   write_ln(Gamma),
   ( pattern_search_variable_get(approx, spline) ->
     write_ln(1)
   ; write_ln(2) ),
   told,
   !.

wavelet_order(haar,2).
wavelet_order(daub2,12).
wavelet_order(daub3,13).
wavelet_order(bspline3, 3).
wavelet_order(bspline4, 4).


/* clustering(create_plotfile) <-
      */

clustering(create_plotfile, X, Name) :-
   stock_file(results, Res),
   concat_atom([Res, '/output'],Out),
   concat_atom([Res, '/input'],In),
   pattern_search_variable_get(tv-X,Tv),
   tell(Out),
   writeln_list(Tv),
   told,
   predicate_from_file( Out, (
      get0(W1),
      predicate_to_file(In, change_dash_to_blanks(W1)) ) ),
   concat_atom([Res, '/gnu_source'], Source),
   concat_atom([Res, '/dax30/', X, '.ps'],Chart),
   tell(Source),
   write_ln('set terminal postscript portrait'),
   write_ln('set size 1, 0.5'),
   write('set title '), put(34), write(Name), put(34), nl,
   write('set output '), put(34), write(Chart), put(34),nl,
   write('plot '), put(34), write(In), put(34),
   write_ln(' notitle with lines'),
   told,
   concat_atom(['gnuplot < "', Source, '"'], Plot),
   shell(Plot).


/*** appendix *****************************************************/


get_pot(List,P) :-
   length(List, L),
   get_pot(1,L,P).
get_pot(A,L,P) :-
   A < L,
   B is A * 2,
   get_pot(B,L,P),
   !.
get_pot(P,_,P1) :-
   P1 is P / 2.

append_zeros(L1, P, L2) :-
   length(L1,L),
   L < P,
   append(L1, [0], L11),
   append_zeros(L11,P,L2),
   !.
append_zeros(L1, _, L1).


/* haar_algorithm(List, R) <-
      */

haar_algorithm(List, R) :-
   length(List,L),
   L > 128,
   haar_transformation(List, R1),
   length(R1, L1),
   ( L1 < 1 ->
     R2 = [nothing]
   ; R2 = R1 ),
   haar_algorithm(R2,R),
   !.
haar_algorithm(List,List).


haar_transformation([X1,X2|Xs],[Y|Ys]) :-
   Y is ( X1 + X2 ) / 2.0,
   haar_transformation(Xs,Ys),
   !.
haar_transformation([],[]).


/******************************************************************/


