

/******************************************************************/
/***                                                            ***/
/***          FnQuery:  FLWOR Expressions                       ***/
/***                                                            ***/
/******************************************************************/


:- op(995, fx, for),
   op(995, fx, let),
   op(995, fx, where),
   op(995, fx, return),
   op(992, yfy, in).

:- dynamic
      flwor_evaluate_found/1.


/*** tests ********************************************************/


test(flwor, 1) :-
   dislog_variable_get(example_path, 'stocks/stock.xml', File),
   flwor( Xs :=
      ( for C in doc(File)/chart,
        let Wkn := C@wkn,
        for E := C/entry,
        for D := E@date,
        for V := E@value,
        where (atom_to_number(V, N), N =< 40),
        return pair:[wkn:Wkn]:[date:[D], value:[V]] ) ),
   dwrite(xml, dates:Xs).


/*** interface ****************************************************/


/* flwor(Xs := Cs) <-
      */

flwor(Xs := Cs) :-
   findall( X,
      ( flwor_evaluate(Cs),
        retract(flwor_evaluate_found(X)) ),
      Xs ).
   
flwor_evaluate((X,Y)) :-
   flwor_evaluate(X),
   flwor_evaluate(Y).
flwor_evaluate(for X in doc(File)/Path) :-
   dread(xml, File, Ys),
   last(Ys, Y),
   X := Y/Path.
flwor_evaluate(for X := Y) :-
   call(X := Y).
flwor_evaluate(let Cs) :-
   once(call(Cs)).
flwor_evaluate(where Cs) :-
   call(Cs).
flwor_evaluate(return X) :-
   assert(flwor_evaluate_found(X)).


/******************************************************************/


