

/******************************************************************/
/***                                                            ***/
/***       Field Notation:  GUI for FNQuery                     ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_get(example_path, Examples),
   concat(Examples, 'xml/', Ex),
   dislog_variable_set(fn_query_examples_directory, Ex),
   dislog_variable_get(output_path, Results),
   dislog_variable_set(fn_query_results_directory, Results),
   dislog_variable_set(fn_query_gui_font_size, 10),
   dislog_variable_set(
      fn_query_gui_panel_size_left, size(50, 20)),
   dislog_variable_set(
      fn_query_gui_panel_size_right, size(50, 20)),
   dislog_variable_set(fn_query_result_type, xml).


% emacs_view class definitions
:- use_module(library('emacs/emacs')).


:- pce_begin_class(
      frame_with_buffers, frame, "Frame with buffers").

initialise(F, Args:any ...) :->
   Msg =.. [initialise | Args],
   send_super(F, Msg).


unlink(F) :->
   get(F, members, Chain),
   new(Bs, chain),
   send(Chain, for_some, and(
      message(@arg1, instance_of, view),
      message(Bs, append, @arg1?editor?text_buffer) )),
   send_super(F, unlink),
   send(Bs, for_some, and(
      message(@arg1?editors, empty),
      message(@arg1, free) )).

:- pce_end_class(frame_with_buffers).


/*** interface ****************************************************/


/* fn_query_gui <-
      */

fn_query_gui(Size) :-
   fn_query_gui(Size, Size).

fn_query_gui(Size_L, Size_R) :-
   dislog_variable_set(
      fn_query_gui_panel_size_left, size(Size_L, 10)),
   dislog_variable_set(
      fn_query_gui_panel_size_right, size(Size_R, 10)),
   fn_query_gui.


/* fn_query_gui_new <-
      */

fn_query_gui_new :-
   fn_query_gui_special_new(_).

fn_query_gui_special_new([P, S, F, T]) :-
   Title = 'FnQuery - XML Queries and Transformations',
   new(Frame, frame_with_buffers(Title)),
   send(Frame, background, colour(white)),
   send(Frame, append, new(Dialog_Header, dialog)),
   send(Dialog_Header, append, new(Menu_Bar, menu_bar)),
   new(Dialog, dialog),
   send(Dialog, background, lightsteelblue3),
   fn_query_gui_create_dialog_new(Dialog, [P, S, F, T]),
   fng_send_pulldown_menus_new(Frame, Menu_Bar, [P, S, F, T]),
   fn_query_gui_send_toolbar_new(Dialog_Header, [P, S, F, T]),
   rgb_to_colour([13.5,13.5,14], Colour_A),
   rgb_to_colour([14,14,15.99], Colour_B),
   send(Dialog_Header, background, Colour_A),
   send(Dialog, background, Colour_B),
   send(Dialog, below, Dialog_Header),
   send(T, right, F),
   send(F, below, Dialog),
   send(Frame, open).


/* fn_query_gui_with_file(File) <-
      */

fn_query_gui_with_file_new :-
   dislog_variable_get(ddb_gui_current_file, File),
   fn_query_gui_with_file_new(File).

fn_query_gui_with_file_new(File) :-
   fn_query_gui_special_new([_, S, F, _]),
   send(S, selection, File),
   send(F, load(File)).


/* fn_query_gui_create_dialog(Dialog, [P, S, F, T]) <-
      */

fn_query_gui_create_dialog_new(Dialog, [P, S, F, T]) :-
   dislog_variable_get(fn_query_examples_directory, Dir),
   send_list(Dialog, append, [
      new(S, text_item('Document')),
      new(P, text_item('Program')) ]),
   send_list([P, S], size, size(400, 50)),
%  send(D, item_size, size(40, 10)),
   send_list([P, S], selection, Dir),
   fn_query_gui_send_fonts_new([P, S]),
   dislog_variable_get(
      fn_query_gui_panel_size_left, Panel_Size_L),
   dislog_variable_get(
      fn_query_gui_panel_size_right, Panel_Size_R),
   new(EB_1, emacs_buffer('Source File')),
   send(EB_1, auto_save_mode, @off),
   send(EB_1, mode(xml)),
   new(EB_2, emacs_buffer('Program File')),
   send(EB_2, auto_save_mode, @off),
   send(EB_2, mode(prolog)),
   get(Panel_Size_L, width, W_L),
   get(Panel_Size_R, width, W_R),
   get(Panel_Size_L, height, H),
   new(F, emacs_view(EB_1, W_L, H)),
   new(T, emacs_view(EB_2, W_R, H)),
   dislog_variable_get(fn_query_gui_font_size, Font_Size),
   send(F, font, font(screen, roman, Font_Size)),
   send(T, font, font(screen, roman, Font_Size)).

fn_query_gui_send_fonts_new([P, S]) :-
   dislog_variable_get(fn_query_gui_font_size, Size),
   send_list([P, S], value_font, font(helvetica, roman, Size)),
   send_list([P, S], label_font, font(helvetica, bold, Size)).


/* fng_send_pulldown_menus(Frame, Menu_Bar, [P, S, F, T]) <-
      */

fng_send_pulldown_menus_new(Frame, Menu_Bar, [P, S, F, T]) :-
   send_pulldown_menu(Menu_Bar, 'File', [
      'Load Document' -
         fn_query_gui_load_file_new(S, F),
      'Load Program' -
         fn_query_gui_load_file_new(P, T),
      'Save Document' -
         fn_query_gui_save_file_new(S, F),
      'Save Program' -
         fn_query_gui_save_file_new(P, T),
      'Quit' - send(Frame, destroy) ]),
   send_pulldown_menu(Menu_Bar, 'Edit', [
      'Transform Document' -
         fng_transformation_for_gui_new(F, T),
      'Select in Document' -
         fn_select_for_gui_new(F, T),
      'Select in Prolog File' -
         fn_select_for_gui_to_pl_new(F, T) ]),
   send_pulldown_menu(Menu_Bar, 'Help', []).


/* fn_query_gui_send_toolbar(Dialog, [P, S, F, T]) <-
      */

fn_query_gui_send_toolbar_new(Dialog, [P, S, F, T]) :-
   send(Dialog, append, new(TB, tool_bar)),
   send(TB, gap, size(4, 0)),
   fn_query_gui_send_toolbar_items(TB, [
      'Transform Source File' - '16x16/copy.xpm' -
         fng_transformation_for_gui_new(F, T),
      'Select in Source File' - '16x16/handpoint.xpm' -
         fn_select_for_gui_new(F, T),
      gap,
      'Load Document' - '16x16/book2.xpm' -
         fn_query_gui_load_file_new(S, F),
      'Save Source File' - '16x16/save.xpm' -
         fn_query_gui_save_file_new(S, F),
      gap,
      'Load Program' - '16x16/open.xpm' -
         fn_query_gui_load_file_new(P, T),
      'Save Program' - '16x16/save.xpm' -
         fn_query_gui_save_file_new(P, T) ]).


/* fn_query_gui_send_toolbar_items(TB, Items) <-
      */

fn_query_gui_send_toolbar_items(TB, Items) :-
   checklist( fn_query_gui_send_toolbar_item(TB),
      Items ).

fn_query_gui_send_toolbar_item(TB, gap) :-
   !,
   send(TB, append, gap).
fn_query_gui_send_toolbar_item(TB, Help-Icon-Goal) :-
   Goal =.. Xs,
   Message =.. [message, @prolog|Xs],
   send(TB, append, new(Button, tool_button(Message, Icon))),
   send(Button, help_message, tag, Help),
   send(Button, colour, lightsteelblue4).


/*** implementation ***********************************************/


/* fn_select_for_gui_to_pl(S, F, T) <-
      */

fn_select_for_gui_to_pl_new(F, T) :-
   dislog_variable_set(fn_query_result_type, pl),
   fn_select_for_gui_new(F, T),
   dislog_variable_set(fn_query_result_type, xml).


/* fn_select_for_gui(S, F, T) <-
      */

fn_select_for_gui_new(F, T) :-
   dislog_variable_get(fn_query_results_directory, Results),
   concat(Results, 'fn_query_selection_tmp.pl', File_P),
   send(T, save(File_P)),
   concat(Results, 'fn_query_selection_source_tmp.xml', File_S),
   send(F, save(File_S)),
   get(F, file, File),
   get(File, absolute_path, N),
   concat(N, '.tmp', File_Out),
   fn_select_for_gui_sub_new(File_P, File_S, File_Out),
   file_view_for_gui_new(File_Out).

file_view_for_gui_new(File) :-
   dislog_variable_get(fn_query_gui_font_size, Font_Size),
   new(EB, emacs_buffer(File)),
   send(EB, auto_save_mode, @off),
   send(EB, mode(xml)),
   new(V, emacs_view(EB)),
   send(V, font, font(screen, roman, Font_Size)),
   new(Frame, frame_with_buffers),
   send(Frame, append, V),
   send(Frame, open).

file_view_for_gui(File) :-
   file_view_for_gui_(File, _).

file_view_for_gui(Label, File) :-
   file_view_for_gui_(File, Viewer),
   send(Viewer, label, Label).

file_view_for_gui_(File, Viewer) :-
   dislog_variable_get(fn_query_gui_font_size, Font_Size),
   send(new(Viewer, view(File)), open),
   send(Viewer, load(File)),
   send(Viewer, font, font(screen, roman, Font_Size)).

fn_select_for_gui_sub_new(File_P, File_In, File_Out) :-
%  nl(user),
%  writeln(user, fn_select_for_gui_sub_new(File_P, File_In, File_Out)),
%  nl(user),
   fn_item_substitution_tmp_prepare(File_P, Predicate),
   dread(xml, File_In, Xml),
   last(Xml, Xml_1),
   ( dislog_variable_get(fn_mode, fn),
     Item_1 = Xml_1
   ; dislog_variable_get(fn_mode, gn),
     fn_to_gn(Xml_1, Item_1) ),
%  writeln(user, before(Item_1)),
   !,
   apply(Predicate, [Item_1, Item_2]),
%  writeln(user, after(Item_2)),
   ( dislog_variable_get(fn_mode, fn),
     Xml_2 = Item_2
   ; dislog_variable_get(fn_mode, gn),
     gn_to_fn(Item_2, Xml_2) ),
   ( dislog_variable_get(fn_query_result_type, xml),
     dwrite(xml, File_Out, Xml_2)
   ; dislog_variable_get(fn_query_result_type, pl),
%    program_to_dependency_graph_xpce(Xml_2),
     predicate_to_file( File_Out,
        dportray(lp_xml, Xml_2) ) ).

/*
fn_select_for_gui_sub(File_S, File_1, File_2) :-
   dislog_consult(File_S, Program),
   member(Rule, Program),
   Rule = [Xml_1 ---> Xml_2]-Body,
   Read = dread(xml, File_1, Xml),
   Last = last(Xml, Xml_1),
   Write = dwrite(xml, File_2, Xml_2),
   append([[Read, Last], Body, [Write]], Actions),
   list_to_comma_structure(Actions, Goal),
   call(Goal).
*/


/* fng_transformation_for_gui(Directory, File, T) <-
      */

fng_transformation_for_gui_new(F, T) :-
   dislog_variable_get(fn_query_results_directory, Results),
   concat(Results, 'fng_transformation_tmp.fng', File_P),
   send(T, save(File_P)),
   concat(Results, 'fn_query_selection_source_tmp.xml', File_S),
   send(F, save(File_S)),
   get(F, file, File),
   get(File, absolute_path, N),
   concat([N, '.tmp'], File_Out),
   fn_transform_xml_file_fng_file_view(File_P, File_S, File_Out).


/* fn_query_gui_load_file(Text_Item, View) <-
      */

fn_query_gui_load_file_new(T, V) :-
   get(T, selection, Path),
   file_directory_name(Path, Dir),
   get(@finder, file(exists := @on, directory := Dir), File),
   send(T, selection, File),
   send(V, load(File)).


/* fn_query_gui_save_program(Text_Item, View) <-
      */

fn_query_gui_save_file_new(T, V) :-
   get(T, selection, Path),
   file_directory_name(Path, Dir),
   get(@finder, file(exists := @off, directory := Dir), File),
   send(V, save(File)).


/******************************************************************/


