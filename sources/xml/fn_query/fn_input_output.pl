

/******************************************************************/
/***                                                            ***/
/***       DDK:  Field Notation to XML                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dread(Dialect, File, FN_Term)
   xml_file_to_fn_term(File, FN_Term) <-
      */

dread(_, File, _) :-
   \+ exists_file(File),
   !,
   write_list(
      ['DDK Error: file "', File, '" does not exist !\n']),
   fail.
dread(xml(Alias), File, FN_Triple_2) :-
   !,
   alias_to_dtd(Alias, DTD),
   Options = [dialect(xml), types(DTD)],
   xml_io:read_structure(File, [FN_Triple_1], Options),
   file_directory_name(File, Dir),
   xml_io:resolve_file_references_in_xml(
      Dir, Alias, FN_Triple_1, FN_Triple_2),
   !.
dread(xml, File, [Id]) :-
   dislog_variable_get(fn_mode, gn),
   !,
   xml_file_to_fn_term(File, FN_Term),
   last(FN_Term, FN_Item),
   fn_to_gn(FN_Item, Id),
   !.
dread(xml, File, FN_Term) :-
   !,
   xml_file_to_fn_term(File, FN_Term),
   !.
dread(xml_swi, File, Content) :-
   !,
   Options = [dialect(xml), max_errors(5000), space(remove)],
   load_structure(File, Content, Options),
   !.

dread(sgml, File, FN_Term) :-
   Options = [dialect(sgml), max_errors(200000)],
   load_structure(File, Content, Options),
   xml_swi_to_fn(Content, FN_Term),
   !.
dread(html, File, FN_Term) :-
%  Options = [dialect(sgml), max_errors(5000), space(remove)],
   Options = [dialect(sgml)],
   load_structure(File, Content, Options),
   xml_swi_to_fn(Content, FN_Term),
   !.
dread(Dialect, File, FN_Term) :-
   \+ member(Dialect, [txt, pl, lp, xls, csv, csv(_)]),
   Options = [dialect(Dialect), max_errors(5000), space(remove)],
   sgml_file_to_fn_term(File, FN_Term, Options).

xml_file_to_fn_term(File, FN_Term, Options) :-
   sgml_file_to_fn_term(File, FN_Term, Options).

xml_file_to_fn_term(File, FN_Term) :-
%  Options = [dialect(xml), max_errors(5000), space(sgml)],
   Options = [dialect(xml), max_errors(5000), space(remove)],
   sgml_file_to_fn_term(File, FN_Term, Options).


/* sgml_file_to_fn_term(File, FN_Term, Options) <-
      */

sgml_file_to_fn_term(File, FN_Term, Options) :-
   load_structure(File, Content, Options),
   xml_swi_to_fn(Content, FN_Term),
   !.


/* dwrite(xml, File, FN_Term, Options) <-
      */

dwrite(xml, File, FN_Term, Options) :-
   fn_term_to_xml_file(FN_Term, File, Options).


/* dwrite(xml, File, FN_Term)
   fn_to_xml_file(FN_Term, File) <-
      */

dwrite(xmls, File, FN_Terms) :-
   predicate_to_file( File,
      checklist( field_notation_to_xml(0),
         FN_Terms ) ).
dwrite(xml, File, Id) :-
   dislog_variable_get(fn_mode, gn),
   !,
   ( gn_to_fn(Id, Item) ->
     X = Item
   ; X = Id ),
   fn_term_to_xml_file(X, File).
dwrite(xml, File, FN_Term) :-
   fn_term_to_xml_file(FN_Term, File).
dwrite(xml_2, File, FN_Term) :-
   xml_io:fn_term_to_xml_file(FN_Term, File).
dwrite(html, File, FN_Term) :-
   predicate_to_file( File,
      ( write_list([ '<meta http-equiv="Content-Type" ',
           'content="text/html; charset=UTF-8"/>\n']),
        field_notation_to_xml(FN_Term) ) ).

fn_to_xml_file(FN_Term, File) :-
   fn_term_to_xml_file(FN_Term, File).


/* dwrite(xml, FN_Term)
   fn_to_xml(FN_Term) <-
      */

dwrite(xml, FN_Term) :-
   field_notation_to_xml(FN_Term).
dwrite(html, FN_Term) :-
   field_notation_to_xml(FN_Term).

fn_to_xml(FN_Term) :-
   field_notation_to_xml(FN_Term).


/* pretty_print_xml_file(File_1, File_2) <-
      */

pretty_print_xml_file(File_1, File_2) :-
   xml_file_to_fn_term(File_1, [FN_Term]),
   fn_item_parse(FN_Term, Tag:_:_),
   ( member(Tag, [html, htm, 'HTML']),
     predicate_to_file( File_2,
        field_notation_to_xml(FN_Term) )
   ; fn_term_to_xml_file(FN_Term, File_2) ),
   !.


/* fn_item_to_pretty_fn_item(Item_1, Item_2) <-
      */

fn_item_to_pretty_fn_item(Item_1, Item_2) :-
   dislog_variable_get(output_path, 'fn_item_tmp.xml', File),
   dwrite(xml, File, Item_1, [encoding(utf8)]),
   dread(xml, File, [Item_2]).


/*** implementation ***********************************************/


/* xml_files_to_fn_term(Files, FN_Term) <-
      */

xml_files_to_fn_term(Files, FN_Term) :-
   maplist( xml_file_to_fn_term,
      Files, FN_Terms ),
   append(FN_Terms, FN_Term).


/* fn_term_to_xml_file(FN_Term, File) <-
      */

fn_term_to_xml_file(FN_Term, File) :-
   Options = [encoding('ISO-8859-1')],
   fn_term_to_xml_file(FN_Term, File, Options).

fn_term_to_xml_file(FN_Term, File, Options) :-
   member(encoding(Encoding), Options),
   predicate_to_file( File,
      ( write_list([ '<?xml version=''1.0'' ',
           'encoding=''', Encoding, ''' ?>\n\n']),
        field_notation_to_xml(FN_Term) ) ).


/* fn_term_to_xml_string(FN_Term, Name) <-
      */

fn_term_to_xml_string(FN_Term, Name) :-
   fn_item_parse(FN_Term, T:As:Es),
   !,
   maplist( fn_term_to_xml_string,
      Es, Names_Es ),
   maplist( attribute_value_pair_to_string,
      As, Names_As ),
   ( Es = [] ->
     Xs = [['<', T], Names_As, ['/>']]
   ; Xs = [['<', T], Names_As, ['>'], Names_Es, ['</', T, '>']] ),
   append(Xs, Names),
   concat(Names, Name).
fn_term_to_xml_string(Name, Name).

/*
fn_term_to_xml_string(FN_Term, Name) :-
   dislog_variable_get(output_path, Path),
   random_file_name(Path, File),
   open(File, write, Stream),           
   switch(S, Stream),
   field_notation_to_xml(FN_Term),             
   switch(S),
   open(File, read, Stream),
   read_stream_to_codes(Stream, Codes),
   close(Stream),
   concat('rm ', File, Command),
   unix(system(Command)),
   name(Name, Codes).

random_file_name(Path, File) :-
   statistics(real_time, [A, B]),
   concat([Path, random_file, '_', A, '_', B], File).
*/


/* xml_string_to_fn_term(Name, FN_Term) <-
      */

xml_string_to_fn_term(Name, FN_Term) :-
   xml_string_to_fn_terms(Name, [FN_Term]).


/* xml_string_to_fn_terms(Name, FN_Terms) <-
      */

xml_string_to_fn_terms(Name, FN_Terms) :-
   new_memory_file(Handle),
   open_memory_file(Handle, write, Post_Stream),
   write(Post_Stream, Name),
   close(Post_Stream),
   open_memory_file(Handle, read, Post_Stream_1),
   Options = [dialect(xml)],
   load_structure(Post_Stream_1, Content, Options),
   xml_swi_to_fn(Content, FN_Terms),
   close(Post_Stream_1).

 
/* field_notation_to_xml(Object) <-
      */

field_notation_to_xml(Object) :-
   is_list(Object),
   !,
%  writeln('<>'),
   checklist( field_notation_to_xml(0),
      Object ),
%  writeln('</>'),
   !.
field_notation_to_xml(Object) :-
   field_notation_to_xml(0, Object),
   !.

field_notation_to_xml(I, A:V:W) :-
   !,
   field_notation_to_xml_write(I, A:V:W).
field_notation_to_xml(I, A:V) :-
   !,
   field_notation_to_xml_write(I, A:V).
field_notation_to_xml(I, Object) :-
   is_list(Object),
   !,
   checklist( field_notation_to_xml(I),
      Object ).
field_notation_to_xml(I, comment(Xs)) :-
   !,
   name(Text, Xs),
   tab(I),
   write_list(['<!--', Text, '-->\n']).
field_notation_to_xml(I, declare(Xs)) :-
   !,
   name(Text, Xs),
   tab(I),
   write_list(['<!', Text, '>\n']).
field_notation_to_xml(I, pi(Text)) :-
   !,
   tab(I),
   write_list(['<?', Text, '?>\n']).
field_notation_to_xml(I, V) :-
   tab(I),
   writeln(V).

field_notation_to_xml_write(I, A:V:[]) :-
   !,
   tab(I), write_list(['<', A]),
   checklist( field_notation_write_attribute,
      V ),
   writeln('/>').
field_notation_to_xml_write(I, A:V:X) :-
   \+ is_list(X),
   !,
   tab(I), write_list(['<', A]),
   checklist( field_notation_write_attribute,
      V ),
   write_list(['>', X, '</', A, '>']), nl.
field_notation_to_xml_write(I, A:V:[X]) :-
   atomic(X),
   !,
   tab(I), write_list(['<', A]),
   checklist( field_notation_write_attribute,
      V ),
   write_list(['>', X, '</', A, '>']), nl.
field_notation_to_xml_write(I, A:V:W) :-
   tab(I), write_list(['<', A]),
   checklist( field_notation_write_attribute,
      V ),
   writeln('>'),
   J is I + 3,
   field_notation_to_xml(J, W),
   tab(I), write_list(['</', A, '>']), nl.
field_notation_to_xml_write(I, A:X) :-
   \+ is_list(X),
   !,
   tab(I), write_list(['<', A, '>', X, '</', A, '>']), nl.
field_notation_to_xml_write(I, A:[X]) :-
   atomic(X),
   !,
   tab(I), write_list(['<', A, '>', X, '</', A, '>']), nl.
field_notation_to_xml_write(I, A:[]) :-
   tab(I), write_list(['<', A, '/>']), nl.
field_notation_to_xml_write(I, A:V) :-
   tab(I), write_list(['<', A, '>']), nl,
   J is I + 3,
   field_notation_to_xml(J, V),
   tab(I), write_list(['</', A, '>']), nl.

field_notation_write_attribute(Attribute:Value) :-
   ( atomic(Value),
     Value_2 = Value
   ; term_to_atom(Value, Value_2) ),
   name_exchange_sublist([["\"", "&quot;"]], Value_2, Value_3),
   write_list([' ', Attribute, '="', Value_3, '"']),
%  write_list([' ', Attribute, '="', Value, '"']),
   !.


/* attribute_value_pairs_to_string(As, Separator, String) <-
      */

attribute_value_pairs_to_string(As, Separator, String) :-
   attribute_value_pairs_to_strings(As, Strings),
   concat_with_separator(Strings, Separator, String).

attribute_value_pairs_to_strings(As, Strings) :-
   maplist( attribute_value_pair_to_string,
      As, Strings ).

/* attribute_value_pair_to_string(A:V, Name) <-
      */

attribute_value_pair_to_string(A:V, Name) :-
   concat([' ', A, '="', V, '"'], Name).


/*** tests ********************************************************/


test(fn_query:field_notation_to_xml, 1) :-
   Object = [
      rented_movie:[
         a:1, b:2 ]: [
         movie:[
            title:'Something about Mary',
            price_code:new_release ],
         days_rented:4 ] ],
   field_notation_to_xml(Object).

test(fn_query:field_notation_to_xml, 2) :-
   Movie_Database = [
      (rental:[id:1]):[
         movie:[
            title:'Something about Mary',
            price_code:new_release ],
         days_rented:4 ],
      (rental:[id:2]):[
         movie:[
            title:'Bonanza',
            price_code:regular,
            length:2 ],
         days_rented:7 ] ],
   findall( Id-Title,
      ( Title := Movie_Database^Rental^movie^title,
        Id := [Rental]^rental^id ),
      Report ),
   writeln_list(Report).


/******************************************************************/


