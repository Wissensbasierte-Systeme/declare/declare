

/******************************************************************/
/***                                                            ***/
/***       Field Notation:  FN Update - Insert                  ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      fn_update/3.


/*** interface ****************************************************/


/* fn_update(<+>, U, P:A, X) <-
      */

fn_update(<+>, U, [P|Ps], X) :-
   fn_update(<+>, U, P, Y),
   fn_update(<+>, Y, Ps, X).
fn_update(<+>, U, [], U).

fn_update(<+>, U, P:A, X) :-
   fn_path_split(P, [P1, P2]),
   P1 = (Op_1, V1),
   nonvar(V1),
   V1 =.. [::, W1, Conditions],
   !,
   fn_update_2(<+>, (Op_1, W1)::Conditions, P2:A, U, X).
fn_update(<+>, U, P:A, X) :-
   fn_path_split(P, [P1, P2]),
   P1 = (Op_1, V1),
   fn_update_2(<+>, (Op_1, V1), P2:A, U, X).

fn_update(<+>, U, P:A, X) :-
   writeln_trace(user,
      fn_update_single_a(<+>, U, P:A, X)),
   fn_path_split(P, [(Op, V)]),
   V =.. [::, W, Conditions],
   !,
   fn_update_2(<+>, (Op, W)::Conditions, []:A, U, X).
fn_update(<+>, U, P:A, X) :-
   writeln_trace(user,
      fn_update_single_b(<+>, U, P:A, X)),
   fn_path_split(P, [(Op, V)]),
   writeln_trace(user,
      fn_path_split_here(P, [(Op, V)])),
   writeln_trace(user,
      calling_fn_update_2(<+>, (Op, V), []:A, U, X)),
   fn_update_2(<+>, (Op, V), []:A, U, X).


/*** implementation ***********************************************/


/* fn_update_2(<+>, Selector::Conditions, P:A, U, X) <-
      */

fn_update_2(<+>, (^, V), Item, U, X) :-
   fn_item_parse(Item, []:As_1:Es_1),
   writeln_trace(user, Item),
   !,
   fn_item_parse(U, T2:As_2:Es_2),
   writeln_trace(user, fn_item_parse(U, T2:As_2:Es_2)),
   append(Es_2, [V:As_1:Es_1], Es),
   X = T2:As_2:Es.
fn_update_2(<+>, (^, V)::Conditions, P:A, U, X) :-
   writeln_trace(user,
      fn_update_2_a(<+>, (^, V)::Conditions, P:A, U, X)),
   fn_item_parse(U, T:As:Es_1),
   ( foreach(E1, Es_1), foreach(E2, Es_2) do
        fn_update_3(<+>, (^, V)::Conditions, P:A, E1, E2) ),
   X = T:As:Es_2.
fn_update_2(<+>, (^, V), P:A, U, X) :-
   writeln_trace(user,
      fn_update_2_b(<+>, (^, V), P:A, U, X)),
   fn_item_parse(U, T:As:Es_1),
   ( ( member(Item, Es_1),
       fn_item_parse(Item, V:_:_),
       ( foreach(E1, Es_1), foreach(E2, Es_2) do
            fn_update_3(<+>, (^, V), P:A, E1, E2) ),
       !,
       X = T:As:Es_2 )
   ; ( P \= [],
       fn_update(<+>, V:[], P:A, Z),
       X = T:As:[Z|Es_1] )
   ; X = T:As:[V:A|Es_1] ).

fn_update_2(<+>, (@, V)::Conditions, []:A, U, X) :-
   writeln_trace(user,
      fn_update_2_c(<+>, (@, V)::Conditions, []:A, U, X)),
%  fn_check_conditions(X, Conditions),
   fn_check_conditions(U, Conditions),
   !,
   fn_item_parse(U, T:As_1:Es),
   fn_update_2_(As_1, V:A, As_2),
   X = T:As_2:Es.
fn_update_2(<+>, (@, _)::_, []:_, U, U).
fn_update_2(<+>, (@, V), []:A, U, X) :-
   fn_item_parse(U, T:As_1:Es),
   writeln_trace(user, As_2 := As_1 * [V:A]),
   fn_update_2_(As_1, V:A, As_2),
   X = T:As_2:Es.

fn_update_2_(As_1, V:_, _) :-
   member(V:_, As_1),
   !,
   writeln(user, a), fail.
fn_update_2_(As_1, V:A, As_2) :-
   As_2 := As_1 * [V:A].


/* fn_update_3(<+>, Selector::Conditions, P:A, X, Y) <-
      */

fn_update_3(<+>, (^, V)::Conditions, P:A, X, Y) :-
   writeln_trace(user,
      fn_update_3_a(<+>, (^, V)::Conditions, P:A, X, Y)),
   P \= [],
   writeln_trace(user,
      fn_check_conditions(X, Conditions)),
   fn_check_conditions(X, Conditions),
   writeln_trace(user, ii),
   fn_item_parse(X, V:_:_),
   writeln_trace(user, iii),
   writeln_trace(user, fn_update_now(X, P:A, Y)),
   fn_update(<+>, X, P:A, Y),
   !.
fn_update_3(<+>, (^, V), P:A, X, Y) :-
   P \= [],
   fn_item_parse(X, V:_:_),
   fn_update(<+>, X, P:A, Y),
   !.
fn_update_3(<+>, _, _, X, X).

fn_update_3(<+>, (^, V)::Conditions, []:A, X, Y) :-
   fn_check_conditions(X, Conditions),
   fn_item_parse(X, V:As:Es),
   append(Es, [A], Fs),
   Y = V:As:Fs.
fn_update_3(<+>, (^, V), []:A, X, Y) :-
   fn_update_3(<+>, (^, V)::[], []:A, X, Y).


/******************************************************************/


