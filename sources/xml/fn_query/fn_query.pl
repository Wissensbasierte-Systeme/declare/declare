

/******************************************************************/
/***                                                            ***/
/***       Field Notation: FN_Query                             ***/
/***                                                            ***/
/******************************************************************/


:- op(50, xfy, ::).
% :- op(200, xfy, @).
:- op(100, fx, ^).
:- op(200, xfy, /).
:- op(100, fx, /).
:- op(400, yfx, <+>).
:- op(400, yfx, <->).

:- discontiguous
      (:=)/2.


/*** interface ****************************************************/


/* Object := Expression <-
      */

X := doc(F)/Path :-
   dread(xml, F, [O]),
   E =.. [/, O, Path],
   X := E.
X := doc(F)@Path :-
   dread(xml, F, [O]),
   E =.. [@, O, Path],
   X := E.
 
X := U-(Agg^V) :-
   !,
   findall( Y,
      Y := U^V,
      Ys ),
   apply(Agg, [Ys, X]).

Xs := U-Ps :-
   is_list(Ps),
   !,
   Y := U,
   maplist( fn_apply(Y),
      Ps, Xs ).

X := U-Goal :-
   \+ is_list(Goal),
   !,
   Y := U,
   fn_apply_goal(Y, Goal, X).

% Xs := U^Ps :-
%   is_list(Ps),
%   maplist( fn_apply(U),
%     Ps, Xs ).

X := U*[P|Ps] :-
   Y := U,
   fn_update(Y, P, Z),
   X := Z*Ps.
X := U*[] :-
   X := U,
   !.

X := U <+> [P|Ps] :-
   Y := U,
   fn_update(<+>, Y, P, Z),
   X := Z <+> Ps.
X := U <+> [] :-
   X := U,
   !.

X := U <-> [P|Ps] :-
   Y := U,
   fn_update(<->, Y, P, Z),
   X := Z <-> Ps.
X := U <-> [] :-
   X := U,
   !.

X := X :-
   \+ fn_complex_expression(X).

/*
X := E :-
   \+ fn_complex_expression(E),
   fn_item_parse(E, X).
*/

/*
X := E :-
   E =.. [Op, U, P],
   Op = /,
   !,
   xp_apply(U, (Op, P), X).
*/

X := E :-
   E =.. [Op, U, P],
   member(Op, [^, /, @]),
   fn_item_parse(U, _),
   fn_apply(U, (Op, P), X).

X := E :-
   E =.. [Op, U, P],
   member(Op, [^, /, @]),
   \+ fn_item_parse(U, _),
   Y := U,
   fn_apply(Y, (Op, P), X).

% X := U^(V*W) :-
%    !,
%    X := (U^V)*W.


/*** implementation ***********************************************/


/* fn_apply(U, P, X) <-
      */

fn_apply(U, (Op, Ps), Xs) :-
   is_list(Ps),
   !,
   fn_encapsulate_selectors(Op, Ps, OPs),
   maplist( fn_apply(U),
      (OPs), Xs ).
fn_apply(U, P, X) :-
   fn_path_split(P, [Q|Qs]),
   fn_get(U, Q, Y),
   fn_apply_iterate(Y, Qs, X).
fn_apply(U, P, X) :-
   fn_path_split(P, [Q1, Q2]),
   Q1 = (_, V1), Q2 = (_, V2),
   var(V1), nonvar(V2),
   fn_selector_duplicate(Q1, Q1a, Q1b),
   fn_get(U, Q1a, Y),
   fn_apply_iterate(Y, [Q1b, Q2], X).
fn_apply(U, P1*P2, X) :-
   fn_apply(U, P1, Y),
   fn_update(Y, P2, X).
fn_apply(U, P-Ps, Xs) :-
   is_list(Ps),
   !,
   fn_path_split(P, Qs),
   fn_apply_iterate(U, Qs, V),
   maplist( fn_apply(V),
     Ps, Xs ).
fn_apply(U, P-Goal, X) :-
   \+ is_list(Goal),
   !,
   fn_apply(U, P, V),
   fn_apply_goal(V, Goal, X).

fn_apply_iterate(U, [P|Ps], X) :-
   fn_apply(U, P, Y),
   fn_apply_iterate(Y, Ps, X).
fn_apply_iterate(U, [], U).

fn_apply_goal(U, Goal, X) :-
   Goal =.. As,
   delete_last_element(As, P, Bs),
   findall( Y,
      fn_apply(U, P, Y),
      Ys ),
   Goal_2 =.. Bs,
   apply(Goal_2, [Ys, X]).
      

/* fn_encapsulate_selectors(Op, Ps, Ops) <-
      */

fn_encapsulate_selectors(Op, Ps, Ops) :-
   maplist( fn_encapsulate_selector(Op),
      Ps, Ops ).

fn_encapsulate_selector(Op, P, (Op, P)).


/* fn_check_conditions(X, Conditions) <-
      */

fn_check_conditions(X, Conditions) :-
%  foreach(Condition, Conditions) do
%     fn_check_condition(X, Condition).
   checklist( fn_check_condition(X),
      Conditions ).

fn_check_condition(X, Condition) :-
   Condition =.. [Op, Path, Value],
   member(Op, [<, =<, =, ==, \=, >=, >]),
   could_be_an_fn_path(Path),
   !,
   fn_path_split(Path, List),
   fn_apply_iterate(X, List, Y),
   Goal =.. [Op, Y, Value],
   call(Goal).
fn_check_condition(X, Condition) :-
   could_be_an_fn_path(Condition),
   fn_path_split(Condition, List),
   !,
   fn_apply_iterate(X, List, _).
fn_check_condition(_, Condition) :-
   call(Condition).

could_be_an_fn_path(Path) :-
   functor(Path, F, _),
   member(F, [^, /, @]).


/* writeln_trace(X, Y) <-
      */

% writeln_trace(_, Y) :-
%    !,
%    writeln(Y).
writeln_trace(_, _) :-
   !.
writeln_trace(X, Y) :-
   writeln(X, Y).


/* fn_query_test(X := Term) <-
      */

fn_query_test(X := Term) :-
   !,
   Y := Term,
   !,
   X = Y.
fn_query_test(Goal) :-
   call(Goal).


/*** tests ********************************************************/


test(fn_query:fn_query, 1) :-
   checklist( fn_query_test, [
      [[b:1, c:3], 3] := [a:[b:2, c:3], c:3 ]-[^a*[^b:1], ^c],
      [b:1, c:3] := [a:[b:2, c:3], c:3]^a*[^b:1],
      [b:1, c:3] := ([a:[b:2, c:3], c:3]^a)*[^b:1],
      [a:[b:2, c:4], c:2] := [a:[b:2, c:3], c:3]*[^a^c:4, ^c:2],
      U = [b:1, a:2, a:3, b:5, a:7],
      4 := U-average^a,
      5 := U-nth(2)^b,
      V = [a:[b:1]:[b:2, c:4:3], a:4, c:3],
      [b:2, c:4:3] := V^a,
      1 := V@a^b,
      4 := V^a@c,
      7 := [a:[b:[d:7:3]:1]:[b:2, c:3], a:4]@a@b@d,
      [a:[d:2:7]:[b:2, c:3], c:8:3] :=
         [a:[d:2:3]:[b:2, c:3], c:3]*[@a^d:7, @c:8],
      [a:1, b:2] := [a:1]*[^b:2],
      W = a:[b:2]:[c:[f:5]:[d:[e:4]:3]],
      (c:[f:5]:[d:[e:4]:3]) := W^c,
      5 := W^c@f,
      4 := W^c^d@e,
      2 := W@b ]).


/******************************************************************/


