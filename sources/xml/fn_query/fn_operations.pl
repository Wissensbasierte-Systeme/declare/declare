

/******************************************************************/
/***                                                            ***/
/***       DDK:  SWI-DOM to Field Notation                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xml_swi_to_fn(Item_1, Item_2) <-
      */

xml_swi_to_fn(element(T, As1, Es1), T:As2:Es2) :-
   !,
   maplist( xml_swi_attribute_to_fn,
      As1, As2 ),
   maplist( xml_swi_to_fn,
      Es1, Es2 ).
xml_swi_to_fn(X, Y) :-
   xml_swi_entity_to_fn(X, Y),
   !.
xml_swi_to_fn(Xs, Ys) :-
   is_list(Xs),
   !,
   maplist( xml_swi_to_fn,
      Xs, Ys ).
xml_swi_to_fn(X, X).


/* xml_swi_attribute_to_fn(X, Y) <-
      */

xml_swi_attribute_to_fn(A=V, A:V).


/* swi_xml_entity_to_fn(X, Y) <-
      */

xml_swi_entity_to_fn(X, Y) :-
   ( nonvar(X), 
     X = entity(V) ->
     concat(['&', V, ';'], Y)
   ; ( var(X), atomic(Y),
       concat('&', U, Y),
       concat(V, ';', U) ->
       X = entity(V) ) ).


/* xml_fn_item_enrich(Item_1, Item_2) <-
      */

xml_fn_item_enrich(Item_1, Item_2) :-
   ( Item_1 = T1:As1:Es1,
     maplist( xml_fn_attribute_value_pair_enrich,
        As1, As2 ),
     Item_2 = T2:As2:Es2
   ; Item_1 = T1:Es1,
     Item_2 = T2:Es2 ),
   !,
   xml_fn_attribute_value_pair_enrich(T1, T2),
   maplist( xml_fn_item_enrich,
      Es1, Es2 ).
xml_fn_item_enrich(Item, Item).

xml_fn_attribute_value_pair_enrich(Name:Value, Term:Value) :-
   name_split_at_position([":"], Name, Names),
   list_to_functor_structure(:, Names, Term).

 
/******************************************************************/


