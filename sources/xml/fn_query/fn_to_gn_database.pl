

/******************************************************************/
/***                                                            ***/
/***       Field Notation:  Fn and Gn                           ***/
/***                                                            ***/
/******************************************************************/


:- dynamic
      reference/4,
      attribute/3,
      value/3.

:- ( dislog_variable_get(fn_mode, _),
     true
   ; dislog_variable_set(fn_mode, fn) ).


/*** interface ****************************************************/


/* gn_database_listing <-
      */

gn_database_listing :-
   collect_and_list_facts(reference/4),
   collect_and_list_facts(attribute/3),
   collect_and_list_facts(value/3).

collect_and_list_facts(Predicate/Arity) :-
   nl,
   collect_facts(Predicate/Arity, Facts),
   ( foreach(Fact, Facts) do
        writeq(Fact),
        writeln('.') ).


/* fn_to_gn(Xml, Item) <-
      */

fn_to_gn(Xml, Item) :-
   dislog_variable_switch(fn_mode, Mode, fn),
   gn_object_get('&', Root),
   fn_to_database([Xml], Root),
   reference(Root, _, Item, _),
   dislog_variable_set(fn_mode, Mode).


/* fn_item_link_with_children(Item, Items) <-
      */

fn_item_link_with_children(Item, Items) :-
%  gn_to_fn(Item, Xml_1),
%  writeln(user, before_link(Item=Xml_1, Items)),
   forall( ( reference(Item, _, I, _), \+ member(I, Items) ),
      retract(reference(Item, _, I, _)) ),
   ( foreach(I, Items) do
        ( retract(reference(_, T, I, N))
        ; true ),
        assertz(reference(Item, T, I, N)) ).
%  gn_to_fn(Item, Xml_2).
%  writeln(user, after_link_(Item=Xml_2)).


/******************************************************************/


