

/******************************************************************/
/***                                                            ***/
/***       Field Notation:  FN_Algebra                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* fn_item_parse(Item_1, Item_2) <-
      */

% field notation

fn_item_parse(V:W:As:X, Item) :-
   !,
   Item = (V:W):As:X.
fn_item_parse(V:As:X, Item) :-
   !,
   Item = V:As:X.
fn_item_parse(V:X, V:[]:X) :-
   !.


% SWI DOM
 
fn_item_parse(element(T,As,Es), T:Bs:Es) :-
   foreach(A=V, As), foreach(A:V, Bs) do
      true.


% graph notation

fn_item_parse(Id, T:As:Es) :-
   reference(_, T, Id, _),
   findall( A:V,
      attribute(Id, A, V),
      As ),
   findall( N-Id_2,
      reference(Id, _, Id_2, N),
      Es_1 ),
   findall( N-V,
      value(Id, V, N),
      Es_2 ),
   append(Es_1, Es_2, Es_3),
   sort(Es_3, Es_4),
   pair_lists_2(_, Es, Es_4).


/* fn_item_prune(V:W:As:X, Item) <-
      */

fn_item_prune(V:W:As:X, Item) :-
   !,
   fn_item_prune((V:W):As:X, Item).
fn_item_prune(V:[]:X, Item) :-
   !,
   Item = V:X.
fn_item_prune(Item, Item).


/* fn_term_to_fn_term(Term1, T2:As2:Es2) <-
      */

fn_term_to_fn_term(Term1, T2:As2:Es2) :-
   fn_item_parse(Term1, T1:As1:Es1),
   term_replace_minus(T1, T2),
   maplist( association_to_association,
      As1, As2 ),
   ( ( is_list(Es1),
       maplist( fn_term_to_fn_term,
          Es1, Es2 ) )
   ; Es2 = Es1 ).
   
association_to_association(A1:V, A2:V) :-
   term_replace_minus(A1, A2).

term_replace_minus(X, Y) :-
   term_to_atom(X, A),
   name_exchange_elements(["-_"], A, C),
   term_to_atom(Y, C).


/* field_notation_project(Attributes,Object_1,Object_2) <-
      */

field_notation_project(Attributes,Object_1,Object_2) :-
   findall( A:V,
      ( member(A,Attributes),
        V := Object_1^A ),
      Object_2 ).


/* fn_paths_to_tree(Paths,Trees) <-
      */

fn_paths_to_tree(Paths,Trees) :-
   findall( X,
      member(X^_,Paths),
      Xs_2 ),
   list_to_ord_set(Xs_2,Xs),
   findall( [[X]|X_Trees],
      ( member(X,Xs),
        findall( X_Path,
           member(X^X_Path,Paths),
           X_Paths ),
        fn_paths_to_tree(X_Paths,X_Trees) ),
      Trees ).


/* fn_path_to_list(Path,List) <-
      */

fn_path_to_list(X,[Y]) :-
   var(X),
   !,
   X = Y.
fn_path_to_list(X^Xs,[X|Ys]) :-
   !,
   fn_path_to_list(Xs,Ys).
fn_path_to_list(X,[X]).


/* flatten_path(Path_1,Path_2) <-
      */

flatten_path((A^B)^C,A^D) :-
   !,
   flatten_path(B^C,D).
flatten_path(A,A).


/* fn_triple_condense(Xml_1, Xml_2) <-
      */

fn_triple_condense(Xml_1, Xml_2) :-
   fn_item_parse(Xml_1, T:As:Es_1),
   !,
   maplist( fn_triple_condense,
      Es_1, Es ),
   fn_text_merge('', Es, Es_2),
   Xml_2 = T:As:Es_2.
fn_triple_condense(Xml, Xml).

fn_text_merge(Sofar_1, [X|Xs], Ys) :-
   atomic(X),
   !,
   concat([Sofar_1, ' ', X], Sofar_2),
   fn_text_merge(Sofar_2, Xs, Ys).
fn_text_merge('', [X|Xs], [X|Ys]) :-
   !,
   fn_text_merge('', Xs, Ys).
fn_text_merge(Sofar, [X|Xs], [Sofar, X|Ys]) :-
   !,
   fn_text_merge('', Xs, Ys).
fn_text_merge(Sofar, [], [Sofar]).


/******************************************************************/


