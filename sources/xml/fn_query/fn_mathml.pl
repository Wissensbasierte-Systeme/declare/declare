

/******************************************************************/
/***                                                            ***/
/***          Field Notation:  MathML                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* mathml_formula_to_classification(
         [N,Equation], [N,T_1,T_2,Degrees,Type,Class] ) <-
      */

mathml_formula_to_classification(
      [N,Equation], [N,T_1,T_2,Degrees,Type,Class] ) :-
   writeln_trace_2(user,Equation),
   mathml_formula_to_function_symbols(
      Equation, Symbols_1, Symbols_2 ),
   mathml_formula_to_derivation_degree(
      y, Equation, Degrees_2 ),
   mathml_formula_to_term_type(
      y, Equation, Type_2 ),
   mathml_term_type_to_atom(Type_2,Type),
   mathml_term_type_to_class(Type_2,Class),
   writeln_trace_2(user,Symbols_1-Symbols_2),
   writeln_trace_2(user,Type),
%  bar_line,
   list_to_comma_atom(Symbols_1,T_1),
   list_to_comma_atom(Symbols_2,T_2),
   list_to_comma_atom(Degrees_2,Degrees),
   !.
mathml_formula_to_classification(
      [N,_], [N,'--','--','--','--','--'] ).


/* mathml_formula_to_function_symbols(
         FN_Term, Symbols_1, Symbols_2 ) <-
      */

mathml_formula_to_function_symbols(FN_Term,Symbols) :-
   mathml_formula_to_function_symbols(
      FN_Term, Symbols_1, Symbols_2 ),
   append(Symbols_1,Symbols_2,Symbols).

mathml_formula_to_function_symbols(
      FN_Term, Symbols_1, Symbols_2 ) :-
   findall( X,
      ( ( [A|_] := FN_Term^apply
        ; [A|_] := FN_Term^_^apply ),
        ( [] := [A]^X,
          member(X,[sin,cos,tan,exp]) ) ),
      Xs_1 ),
   findall( X,
      ( ( [A|_] := FN_Term^apply
        ; [A|_] := FN_Term^_^apply ),
        ( [X] := [A]^ci,
          atomic(X) ) ),
      Xs_2 ),
   !,
   length(Xs_1,N_1), writeln_trace_2(N_1),
   length(Xs_2,N_2), writeln_trace_2(N_2),
   list_to_ord_set(Xs_1,Symbols_1),
   list_to_ord_set(Xs_2,Symbols_2).


/* mathml_formula_to_derivation_degree(Y,Equation,Degrees) <-
      */

mathml_formula_to_derivation_degree(Y,Equation,Degrees) :-
   findall( Degree,
      ( ( A := Equation^_^apply
        ; A := Equation^apply ),
        _ := A^diff,
        Degree_2 := A^bvar^degree^cn,
        [Y] := A^ci,
        term_to_atom(Degree,Degree_2) ),
      Degrees ).


/* list_to_comma_atom(List, Atom) <-
      */

list_to_comma_atom(List, Atom) :-
   term_to_atom(List, A),
   ( concat('[ ', B, A) ->
     true
   ; concat('[', B, A) ),
   concat(Atom, ']', B).

/*
list_to_comma_atom([], '') :-
   !.
list_to_comma_atom(Xs, A) :-
   list_to_comma_structure(Xs, S),
   term_to_atom(S, A).
*/


/* mathml_term_types_apply_operator(Operator,Types,Type) <-
      */

mathml_term_types_apply_operator(times,[Type],Type) :-
   !.
mathml_term_types_apply_operator(Mode,[T|Ts],Type) :-
   mathml_term_types_apply_operator(Mode,Ts,T_2),
   mathml_term_types_apply_operator(Mode,T,T_2,Type).
mathml_term_types_apply_operator(_,[],[]).


/* mathml_term_types_apply_operator(Operator,Type_1,Type_2,Type) <-
      */

mathml_term_types_apply_operator(Operator,Type_1,Type_2,Type) :-
   member(Operator,[plus,minus,eq]),
   ord_union(Type_1,Type_2,Type).

mathml_term_types_apply_operator(times,[],Type,Type) :-
   !.
mathml_term_types_apply_operator(times,Type,[],Type) :-
   !.
mathml_term_types_apply_operator(times,Type_1,Type_2,Type) :-
   writeln_trace(user,
      mathml_term_types_apply_operator(times,Type_1,Type_2,Type)),
   findall( X,
      ( member(X1,Type_1),
        member(X2,Type_2),
        mathml_term_type_times(times,X1,X2,X) ),
      Type_3 ),
   list_to_ord_set(Type_3,Type).

mathml_term_type_times(times,Type_1,Type_2,Type) :-
   writeln_trace(user,
      mathml_term_type_times(times,Type_1,Type_2,Type)),
   findall( (D,N),
      ( member((D,N1),Type_1),
        mathml_type_process((D,N1),Type_2,(D,N))
      ; member((D,N2),Type_2),
        mathml_type_process((D,N2),Type_1,(D,N)) ),
      Type_3 ),
   list_to_ord_set(Type_3,Type),
   writeln_trace(user,
      mathml_term_type_times(times,Type)).

mathml_type_process((D,N1),Type,(D,N)) :-
   member((D,N2),Type),
   !,
   N is N1 + N2.
mathml_type_process((D,N),_,(D,N)).
    

/* mathml_formula_to_term_type(Y,Formula,Type) <-
      */

mathml_formula_to_term_type(Y,Formula,Type) :-
   mathml_formula_to_term_type_sub(Y,Formula,Type_2),
   delete(Type_2,[],Type),
   !.
mathml_formula_to_term_type(Y,Formula,Type) :-
   mathml_formula_to_term_type_sub(Y,Formula,Type).

mathml_formula_to_term_type_sub(Y,Formula,Type) :-
   _ := Formula^power,
   !,
   ( Formula = [_,A,_],
     mathml_formula_to_term_type_sub(Y,[plus:[],A],T),
     [M] := Formula^cn,
     term_to_atom(N,M),
     writeln_trace_2(user,M),
     multify(T,N,Ts),
     writeln_trace_2(user,Ts),
     mathml_term_types_apply_operator(times,Ts,Type),
     writeln_trace_2(user,Type)
   ; Type = [] ),
   !.
mathml_formula_to_term_type_sub(Y,Formula,Type) :-
   member(Operator,[plus,minus,times,eq]),
   _ := Formula^Operator,
   !,
   Formula = [_|Formulas],
   findall( Formula_3,
      Formula_3 := Formulas^apply,
      Formulas_3 ),
   maplist( mathml_formula_to_term_type_sub(Y),
      Formulas_3, Types ),
   findall( (0,1),
      [y] := Formula^ci,
      T ),
   mathml_term_types_apply_operator(Operator,[[T]|Types],Type).
mathml_formula_to_term_type_sub(Y,Formula,[[(Degree,1)]]) :-
   _ := Formula^diff,
   !,
   [Degree_2] := Formula^bvar^degree^cn,
   [Y] := Formula^ci,
   term_to_atom(Degree,Degree_2).
mathml_formula_to_term_type_sub(_,_,[]).

   
/* mathml_term_type_to_atom(Xs,Atom) <-
      */

mathml_term_type_to_atom(Xs,Atom) :-
   maplist( mathml_term_type_to_atom_sub,
      Xs, Ys ),
   term_append(+,Ys,Atom).
   
mathml_term_type_to_atom_sub(Xs,Y) :-
   maplist( mathml_term_type_to_atom_sub_sub,
      Xs, Zs ),
   term_append(*,Zs,Y),
   !.
   
mathml_term_type_to_atom_sub_sub((0,1),Y) :-
   !,
   term_to_atom(y,Y).
mathml_term_type_to_atom_sub_sub((0,N),Y) :-
   !,
   term_to_atom(y^N,Y).
mathml_term_type_to_atom_sub_sub((Degree,1),Y) :-
   !,
   multify('�',Degree,Diffs),
   name_append(Diffs,Diff),
   name_append(y,Diff,Y).
mathml_term_type_to_atom_sub_sub((Degree,N),Y) :-
   multify('�',Degree,Diffs),
   name_append(Diffs,Diff),
   name_append([y,Diff,'^',N],Y).


/* mathml_term_type_to_class(Type,Class) <-
      */

mathml_term_type_to_class(Type,'Linear') :-
   \+ ( member(X,Type), member((_,N),X), N > 1 ),
   \+ ( member(X,Type), length(X,N), N > 1 ),
   !.
mathml_term_type_to_class(
      [ [(0,1)], [(0,2)], [(1,1)] ], 'Riccati' ) :-
   !.
mathml_term_type_to_class(Type,'Riccati Special') :-
   subset(Type,
      [ [(0,1)], [(0,2)], [(1,1)] ] ),
   !.
mathml_term_type_to_class(Type,'Abelian') :-
   \+ ( member(X,Type), length(X,N), N > 1 ),
   mathml_term_type_to_maximals(Type,Maximals),
   member((0,3),Maximals),
   member((1,1),Maximals),
   \+ ( member((D,_),Maximals), D > 1 ),
   !.
mathml_term_type_to_class(_,'--').


/* mathml_term_type_to_maximals(Type,Maximals) <-
      */

mathml_term_type_to_maximals(Type,Maximals) :-
   findall( D,
      ( member(X,Type),
        member((D,_),X) ),
      Ds_2 ),
   list_to_ord_set(Ds_2,Ds),
   writeln_trace_2(user,Ds),
   findall( (D,M),
      ( member(D,Ds),
        findall( N,
           ( member(X,Type),
             member((D,N),X) ),
           Ns ),
        maximum(Ns,M) ),
      Maximals ),
   !.


/* term_append(Op,Xs,Y) <-
      */

term_append(_,[X],X) :-
   !.
term_append(Op,[X|Xs],Y) :-
   term_append(Op,Xs,Z),
   name_append([X,Op,Z],Y).
term_append(_,[],'').
   

/* writeln_trace_2 <-
      */

writeln_trace_2(_).
writeln_trace_2(_,_).


/*** tests ********************************************************/


test(fn_query:fn_mathml,
      mathml_term_types_apply_operator_plus) :-
   mathml_term_types_apply_operator(plus,[
      [ [(2,1),(3,1)], [(2,1),(3,2)], [(2,3)] ],
      [ [(2,1),(3,1)], [(3,2),(4,1)] ] ],
      Type ),
   writeln_list(Type).

test(fn_query:fn_mathml,
      mathml_term_types_apply_operator_times) :-
   mathml_term_types_apply_operator(times,[
      [ [(2,1),(3,1)], [(2,1),(3,2)], [(2,3)] ],
      [ [(2,1),(3,1)], [(3,2),(4,1)] ] ],
      Type ),
   writeln_list(Type).

test(fn_query:fn_mathml,
      mathml_formula_to_term_type_short) :-
   mathml_formula_to_term_type( y,
      [ diff:[]:[],
           bvar:[]:[ci:[]:[x], degree:[]:[cn:[type:integer]:[2]]],
           ci:[type:fn]:[y] ],
      Type ),
   writeln(Type).

test(fn_query:fn_mathml,
      mathml_formula_to_term_type) :-
   mathml_formula_to_term_type( y,
      [ eq:[]:[],
        apply:[]:[ diff:[]:[],
           bvar:[]:[ci:[]:[x], degree:[]:[cn:[type:integer]:[2]]],
           ci:[type:fn]:[y] ],
        apply:[]:[ times:[]:[],
           apply:[]:[ plus:[]:[],
              apply:[]:[ times:[]:[],
                 ci:[]:['A'],
                 apply:[]:[ ci:[type:fn]:[f],
                    ci:[]:[x]]],
              ci:[]:['B']],
           ci:[type:fn]:[y]] ],
      Type ),
   writeln(Type).

test(fn_query:fn_mathml,
      mathml_formula_to_term_type_sub) :-
   mathml_formula_to_term_type_sub( y,
      [power:[]:[], ci:[type:fn]:[y], cn:[type:integer]:[2]],
      Type ),
   writeln(Type).

test(fn_query:fn_mathml,
      mathml_term_type_to_maximals) :-
   mathml_term_type_to_maximals(
%     [ [(2,1),(3,1)], [(2,1),(3,2)], [(2,3)] ],
      [ [(0,2)] ,[(0,3)], [(1,1)] ],
      Maximals ),
   writeln(Maximals).


/******************************************************************/


