

/******************************************************************/
/***                                                            ***/
/***       Field Notation:  FN Update - Delete                  ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* fn_update(<->, U, P, X) <-
      */

fn_update(<->, U, [P|Ps], X) :-
   fn_update(<->, U, P, Y),
   fn_update(<->, Y, Ps, X).
fn_update(<->, U, [], U).

fn_update(<->, U, P, X) :-
   fn_path_split(P, [P1, P2]),
   P1 = (Op_1, V1),
   nonvar(V1),
   V1 =.. [::, W1, Conditions],
   !,
   fn_update_2(<->, (Op_1, W1)::Conditions, P2, U, X).
fn_update(<->, U, P, X) :-
   fn_path_split(P, [P1, P2]),
   P1 = (Op_1, V1),
   fn_update_2(<->, (Op_1, V1), P2, U, X).

fn_update(<->, U, P, X) :-
   writeln_trace(user,
      fn_update_single_a(<->, U, P, X)),
   fn_path_split(P, [(Op, V)]),
   V =.. [::, W, Conditions],
   !,
   fn_update_2(<->, (Op, W)::Conditions, [], U, X).
fn_update(<->, U, P, X) :-
   writeln_trace(user,
      fn_update_single_b(<->, U, P, X)),
   fn_path_split(P, [(Op, V)]),
   writeln_trace(user,
      fn_path_split_here(P, [(Op, V)])),
   writeln_trace(user,
      calling_fn_update_2(<->, (Op, V), [], U, X)),
   fn_update_2(<->, (Op, V), [], U, X).


/*** implementation ***********************************************/


/* fn_update_2(<->, Selector::Conditions, P, U, X) <-
      */

fn_update_2(<->, (Op, V), P, U, X) :-
   fn_update_2(<->, (Op, V)::[], P, U, X).

fn_update_2(<->, (^, V)::Conditions, P, U, X) :-
   fn_item_parse(U, T:As:Es_1),
   findall( E2,
      ( member(E1, Es_1),
        fn_update_3(<->, (^, V)::Conditions, E1, P, E2) ), 
      Es_2 ),
   X = T:As:Es_2.

fn_update_2(<->, (@, V), [], U, X) :-
   fn_item_parse(U, T:As_1:Es),
   delete_with_variables(As_1, V:_, As_2),
   X = T:As_2:Es.


/* fn_update_3(<->, (^, V)::Conditions, E1, P, E2) <-
      */

fn_update_3(<->, (^, V)::Conditions, E, _, E) :-
   fn_update_3_no_delete((^, V)::Conditions, E),
   !.
fn_update_3(<->, _, E1, P, E2) :-
   P \= [],
   fn_update(<->, E1, P, E2).

fn_update_3_no_delete(_, E) :-
   \+ fn_item_parse(E, _).
fn_update_3_no_delete((^, V)::_, E) :-
   fn_item_parse(E, T:_:_),
   T \= V.
fn_update_3_no_delete((^, V)::Conditions, E) :-
   fn_item_parse(E, T:_:_),
   T = V,
   \+ fn_check_conditions(E, Conditions).


/* delete_with_variables(Xs, Y, Zs) <-
      */

delete_with_variables(Xs, Y, Zs) :-
   findall( X,
      ( member(X, Xs),
        \+ X = Y ),
      Zs ).


/******************************************************************/


