

/******************************************************************/
/***                                                            ***/
/***       Field Notation:  FN Query Tests                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* fn_item_lists_from_files_compare(Es_1, Es_2) <-
      */

fn_item_lists_from_files_compare(Es_1, Es_2) :-
   ( foreach(E_1, Es_1), foreach(E_2, Es_2) do
        fn_items_from_files_compare(E_1, E_2 ) ).

fn_items_from_files_compare(Item_1, Item_2) :-
   atomic(Item_1),
   atomic(Item_2),
   Item_1=Item_2.
fn_items_from_files_compare(Item_1, Item_2) :-
%  writeln(fn_items_from_files_compare),
%  writeln(Item_1=Item_2),
   fn_item_parse(Item_1, T:As_1:Es_1),
   fn_item_parse(Item_2, T:As_2:Es_2),
%  writeln(after_fn_item_parse),
%  writeln(before_attribute_compare),
%  writeq(As_1=As_2), nl,
   maplist( fn_attribute_value_pair_to_term,
      As_1, Terms_1 ),
   maplist( fn_attribute_value_pair_to_term,
      As_2, Terms_2 ),
%  writeln(after_attribute_compare),
%  writeq(Terms_1=Terms_2), nl,
   Terms_1 = Terms_2,
   fn_item_lists_from_files_compare(Es_1, Es_2).

fn_attribute_value_pair_to_term(query:V, query:W) :-
   !,
   term_to_atom(W, V).
fn_attribute_value_pair_to_term(A:V, A:V).


/******************************************************************/


