

/******************************************************************/
/***                                                            ***/
/***       Field Notation: FN_Transform                         ***/
/***                                                            ***/
/******************************************************************/


test(fn_item_transform, 1) :-
   ['examples/xml/stocks/compute_percent'],
   dislog_variable_set(fn_mode, gn),
   File_S = 'examples/xml/stocks/stock_2.fng',
   File_X = 'examples/xml/stocks/stock_2.xml',
   dread(xml, File_X, [Item_1]), !,
   fn_transform([item, fng(File_S)], Item_1, Item_2),
   dwrite(xml, user, Item_1),
   dwrite(xml, user, Item_2).


/*** interface ****************************************************/


/* fn_item_transform_message(Type, Item) <-
      */

fn_item_transform_message(_, _) :-
   !.
fn_item_transform_message_(Type, Item) :-
   gn_to_fn(Item, Xml),
   write(user, Type), write(user, ': '),
   writeln(user, Item = Xml).
%  wait.


/* fn_transform(Options, Xml_1, Xml_2) <-
      */

fn_transform(Options, File_1, File_2) :-
   member(predicate(Predicate), Options),
   member(file, Options),
   !,
   dread(xml, File_1, Items),
   last(Items, Item_1),
   writeln(user, here_1),
   fn_item_transform(Predicate, Item_1, Item_2),
   writeln(user, here_2),
   dwrite(xml, File_2, Item_2).
fn_transform(Options, Item_1, Item_2) :-
   member(predicate(Predicate), Options),
   member(item, Options),
   !,
   fn_item_transform(Predicate, Item_1, Item_2).

fn_transform(Options, File_1, File_2) :-
   member(fng(File_S), Options),
   member(file, Options),
   !,
   fn_transform_xml_file_fng(File_S, File_1, File_2).
fn_transform(Options, Item_1, Item_2) :-
   member(fng(File_S), Options),
   member(item, Options),
   !,
   fn_item_transform_message(i(0), Item_1),
   fn_transform_fn_item_fng(File_S, Item_1, Item_2),
   fn_item_transform_message(o(0), Item_2).

fn_transform(Options, File_1, File_2) :-
   member(fng_program(Fng), Options),
   member(file, Options),
   !,
   File_S = 'results/fng_tmp.fng',
   dwrite(pl, File_S, Fng),
   fn_transform_xml_file_fng(File_S, File_1, File_2).
fn_transform(Options, Item_1, Item_2) :-
   member(fng_program(Fng), Options),
   member(item, Options),
   !,
   File_S = 'results/fng_tmp.fng',
   dwrite(pl, File_S, Fng),
   fn_transform_fn_item_fng(File_S, Item_1, Item_2).


/* fn_item_transform(Table, Path, Item_1, Item_2) <-
      */

fn_item_transform(Table, Path, Item_1, Item_2) :-
   once( ( fn_item_parse(Item_1, T:As:Es),
   maplist( fn_item_transform(Table, [T:As|Path]),
      Es, Es_2 ),
   Table_2 =.. [Table, Path],
   fn_item_replace(Table_2, T:As:Es_2, Item_2) ) ).
fn_item_transform(_, _, declare(_X), '') :-
   !.
fn_item_transform(_, _, Item, Item).


/* fn_item_transform(Table, Item_1, Item_2) <-
      */

fn_item_transform(Table, Item_1, Item_2) :-
   dislog_variable_get(fn_mode, gn),
   once( (
      fn_item_parse(Item_1, _:_:Es),
      maplist( fn_item_transform(Table),
         Es, Es_2 ),
      fn_item_link_with_children(Item_1, Es_2),
      fn_item_replace(Table, Item_1, Item_2),
      fn_item_transform_message(i(2), Item_1),
      fn_item_transform_message(o(2), Item_2) ) ).
fn_item_transform(Table, Item_1, Item_2) :-
   dislog_variable_get(fn_mode, fn),
   fn_item_parse(Item_1, T:As:Es),
   !,
   maplist( fn_item_transform(Table),
      Es, Es_2 ),
   fn_item_replace(Table, T:As:Es_2, Item_2).
fn_item_transform(_, declare(_X), '') :-
   !.
fn_item_transform(_, Item, Item).


/* fn_item_replace(Table, Item_1, Item_2) <-
      */

fn_item_replace(Table, Item_1, Item_2) :-
   dislog_variable_get(fn_mode, gn),
   !,
   fn_item_transform_message(i(1), Item_1),
   apply(Table, [Item_1, Item_2]),
   fn_item_transform_message(o(1), Item_2).
fn_item_replace(Table, Item_1, Item_2) :-
   fn_item_parse(Item_1, T:As:Es),
   apply(Table, [T:As:Es, Item_2]).


/* fn_transform_elements_fixpoints(Xs, Es_1, Es_2) <-
      */

fn_transform_elements_fixpoints(Xs, Es_1, Es_2) :-
   iterate_list( fn_transform_elements_fixpoint_mod,
      Es_1, Xs, Es_2 ).

fn_transform_elements_fixpoint_mod(Es_1, Substitutions, Es_2) :-
   fn_transform_elements_fixpoint(Substitutions, Es_1, Es_2).


/* fn_transform_elements_fixpoint(Substitutions,Es_1,Es_2) <-
      */

fn_transform_elements_fixpoint(Substitutions, Es_1, Es_2) :-
   fixpoint( fn_transform_elements(Substitutions),
      Es_1, Es_2 ).


/* fn_transform_elements(Substitutions, Es_1, Es_2) <-
      */

fn_transform_elements(Substitutions, Es_1, Es_2) :-
   is_list(Es_1),
   !,
   maplist( fn_transform_element(Substitutions),
      Es_1, Es_2 ).
fn_transform_elements(Substitutions, Item_1, Item_2) :-
   fn_transform_element(Substitutions, Item_1, Item_2).

fn_transform_element(Substitutions, Item_1, Item_2) :-
   fn_apply_substitutions(Substitutions, Item_1, Item_3),
   fn_transform_inner_elements(Substitutions, Item_3, Item_2).
   

/*** interface ****************************************************/


/* fn_transform_inner_elements(Substitutions, Item_1, Item_2) <-
      */

fn_transform_inner_elements(Substitutions, Item_1, Item_2) :-
   fn_item_parse(Item_1, N:As_1:Es_1),
   !,
   fn_transform_elements(Substitutions, As_1, As_2),
   fn_transform_elements(Substitutions, Es_1, Es_2),
   fn_item_prune(N:As_2:Es_2, Item_2).
fn_transform_inner_elements(Substitutions, Es_1, Es_2) :-
   is_list(Es_1),
   !,
   fn_transform_elements(Substitutions, Es_1, Es_2).
fn_transform_inner_elements(_, Item, Item).


/* fn_apply_substitutions(Substitutions, E1, E2) <-
      */

fn_apply_substitutions([Sub|Subs], E1, E2) :-
   fn_apply_substitution(Sub, E1, E3),
   fn_apply_substitutions(Subs, E3, E2),
   !.
fn_apply_substitutions([], E, E).

fn_apply_substitution(Substitution, E1, E2) :-
   copy_term(Substitution, Substitution_2),
   E2-E1 = Substitution_2,
   !.
fn_apply_substitution(Substitution, E1, E2) :-
   copy_term(Substitution, Substitution_2),
   E2-E1-Predicate = Substitution_2,
   apply(Predicate, [E1, E2]),
   !.
fn_apply_substitution(_, E, E).


/* fn_hide_elements(Elements, Xs, Ys) <-
      */

fn_hide_elements(Elements, Xs, Ys) :-
   is_list(Xs),
   !,
   sublist( fn_dont_hide_element(Elements),
      Xs, Zs ),
   maplist( fn_hide_elements_sub(Elements),
      Zs, Ys ).
fn_hide_elements(_,X,X).

% fn_hide_elements_sub(Elements, Es_1, Es_2) :-
%    is_list(Es_1),
%    !,
%    sublist( fn_dont_hide_element(Elements),
%       Es_1, Es_2 ).
fn_hide_elements_sub(Elements, Name:As1:Es1, Element_2) :-
   !,
   sublist( fn_dont_hide_element(Elements),
      As1, As2 ),
   fn_hide_elements(Elements, Es1, Es2),
   ( ( As2 = [],
       Element_2 = Name:Es2 )
   ; Element_2 = Name:As2:Es2 ).
fn_hide_elements_sub(Elements, Name:Es1, Element_2) :-
   !,
   fn_hide_elements_sub(Elements, Name:[]:Es1, Element_2).
fn_hide_elements_sub(_, Name, Name).

fn_dont_hide_element(Elements, X) :-
   \+ member(X, Elements).


/*** tests ********************************************************/


test(xml, fn_transform_elements) :-
   FN_Term = b:[c:1]:[d:2],
   Substitutions = [
      (a:As:Es)-(b:As:Es),
      (e:Es)-(d:Es) ],
   fn_transform_elements_fixpoint(Substitutions,
      FN_Term, FN_Term_2 ),
   dwrite(xml, FN_Term_2).

test(xml, fn_transform_elements_2) :-
   FN_Term = b:[c:1]:[d:2],
   Substitutions = [
      (a:As:Es)-(b:As:Es),
      _-(d:Es)-fn_transform_elements_test_condition ],
   fn_transform_elements_fixpoint(Substitutions,
      FN_Term, FN_Term_2 ),
   dwrite(xml, FN_Term_2).

fn_transform_elements_test_condition(d:2, e:2).


/******************************************************************/


