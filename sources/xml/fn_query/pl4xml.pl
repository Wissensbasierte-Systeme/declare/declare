

/******************************************************************/
/***                                                            ***/
/***       Pl4Xml:  Select                                      ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      pl4xml_get/3.


/*** interface ****************************************************/


/* pl4xml_select(X, Location_Path, Y) <-
      */

pl4xml_select(X, L/Ls, Y) :-
   !,
   pl4xml_get(X, L, Z),
   pl4xml_select(Z, Ls, Y).
pl4xml_select(X, L, Y) :-
   pl4xml_get(X, L, Y).


/* pl4xml_get_all(Item, Location_Path, Items) <-
      */

pl4xml_get_all(Item, Location_Path, Items) :-
   findall( I,
      pl4xml_get(Item, Location_Path, I),
      Items ).


/* pl4xml_get(Item_1, Location_Step, Item_2) <-
      */

pl4xml_get(Item_1, Axis::X, Item_2) :-
   nonvar(X),
   X = Node_Test::Predicates,
   !,
   pl4xml_get(Item_1, Axis::Node_Test, Item_2),
   pl4xml_check_conditions(Item_2, Predicates).

pl4xml_get(Item_1, updates::[U|Us], Item_2) :-
   pl4xml_get(Item_1, update::U, Item_3),
   pl4xml_get(Item_3, updates::Us, Item_2).
pl4xml_get(Item, updates::[], Item).

pl4xml_get(Item_1, update::Item, Item_2) :-
   pl4xml_get(Item, tag::'*', T),
   pl4xml_get_all(Item_1, child::T, Items_1),
   pl4xml_delete_subelements(Item_1, Items_1, Item_3),
   pl4xml_add_subelements(Item_3, [Item], Item_2).

pl4xml_get(Item_1, update::(L/Ls), Item_2) :-
   L = child::_,
   pl4xml_get_all(Item_1, L, Items_1),
   pl4xml_delete_subelements(Item_1, Items_1, Item_3),
   maplist( pl4xml_get__(update::Ls),
      Items_1, Items_2 ),
   pl4xml_add_subelements(Item_3, Items_2, Item_2).

pl4xml_delete_subelements(Item_1, Items, Item_2) :-
   fn_item_parse(Item_1, T:As:Es_1),
   subtract(Es_1, Items, Es_2),
   Item_2 = T:As:Es_2.

pl4xml_add_subelements(Item_1, Items, Item_2) :-
   fn_item_parse(Item_1, T:As:Es_1),
   append(Es_1, Items, Es_2),
   Item_2 = T:As:Es_2.

pl4xml_get__(Location_Path, Item_1, Item_2) :-
   pl4xml_get(Item_1, Location_Path, Item_2).

pl4xml_get(Item, path::Path, Value) :-
   pl4xml_get(Item, attribute::Attribute, Value),
   Path = attribute::Attribute.
pl4xml_get(Item_1, path::Path, Item_2) :-
   pl4xml_get(Item_1, child::T, Item_3),
   ( Item_2 = Item_3,
     Path = child::T
   ; pl4xml_get(Item_3, path::Ts, Item_2),
     Path = child::T/Ts ).

pl4xml_get(Item, branch::Paths, Items) :-
   maplist( pl4xml_get(Item),
      Paths, Items ).

pl4xml_get(Item, attribute::Attribute, Value) :-
   ( dislog_variable_get(fn_mode, gn),
     attribute(Item, Attribute, Value)
   ; fn_item_parse(Item, _:As:_),
     member(Attribute:Value, As) ).
pl4xml_get(Item_1, child::Tag, Item_2) :-
   ( dislog_variable_get(fn_mode, fn),
     fn_item_parse(Item_1, _:_:Es),
     member(Item_2, Es),
     fn_item_parse(Item_2, Tag:_:_)
   ; reference(Item_1, Tag, Item_2, _) ).

pl4xml_get(Item_1, transform::File, Item_2) :-
   fn_transform_fn_item_fng(File, Item_1, Item_2).

pl4xml_get(Item, tag::'*', T) :-
   fn_item_parse(Item, T:_:_).
pl4xml_get(Item, attributes::'*', As) :-
   fn_item_parse(Item, _:As:_).
pl4xml_get(Item, content::'*', Es) :-
   fn_item_parse(Item, _:_:Es).

pl4xml_get(Item, self::T, Item) :-
   pl4xml_get(Item, tag::'*', T).
pl4xml_get(Item_1, nth_child::N, Item_2) :-
   fn_item_parse(Item_1, _:_:Es),
   nth(N, Es, Item_2).
pl4xml_get(Item_1, descendant::T, Item_2) :-
   ( pl4xml_get(Item_1, child::T, Item_2)
   ; pl4xml_get(Item_1, child::'*', Item_3),
     pl4xml_get(Item_3, descendant::T, Item_2) ).
pl4xml_get(Item_1, descendant_or_self::T, Item_2) :-
   ( pl4xml_get(Item_1, descendant::T, Item_2)
   ; pl4xml_get(Item_1, self::T, Item_2) ).

% only for graph notation

pl4xml_get(Item_1, parent::T, Item_2) :-
   reference(Item_2, _, Item_1, _),
   reference(_, T, Item_2, _).
pl4xml_get(Item_1, Axis::T, Item_2) :-
   member(Axis, [preceding_sibling, following_sibling]),
   reference(Item_p, _, Item_1, _),
   fn_item_parse(Item_p, _:_:Es),
   nth(N, Es, Item_1),
   ( Axis = preceding_sibling, M is N - 1
   ; Axis = following_sibling, M is N + 1 ),
   nth(M, Es, Item_2),
   pl4xml_get(Item_2, tag::'*', T).
pl4xml_get(Item_1, ancestor::T, Item_2) :-
   ( pl4xml_get(Item_1, parent::T, Item_2)
   ; pl4xml_get(Item_1, parent::'*', Item_3),
     pl4xml_get(Item_3, ancestor::T, Item_2) ).
pl4xml_get(Item_1, ancestor_or_self::T, Item_2) :-
   ( pl4xml_get(Item_1, ancestor::T, Item_2)
   ; pl4xml_get(Item_1, self::T, Item_2) ).
pl4xml_get(Item_1, Axis::T, Item_2) :-
   member(Axis, [preceding, following]),
   concat(Axis, '_sibling', Axis_2),
   pl4xml_get(Item_1, ancestor_or_self::'*', Item_3),
   pl4xml_get(Item_3, Axis_2::T, Item_2).


/* pl4xml_check_conditions(Item, Conditions) <-
      */

pl4xml_check_conditions(Item, Conditions) :-
   checklist( pl4xml_check_condition(Item),
      Conditions ).

pl4xml_check_condition(Item, Condition) :-
   Condition =.. [Op, Path, Value],
   member(Op, [<, =<, =, ==, \=, >=, >]),
   pl4xml_possible_path(Path),
   !,
   pl4xml_select(Item, Path, Item_2),
   Goal =.. [Op, Item_2, Value],
   call(Goal).
pl4xml_check_condition(Item, Condition) :-
   pl4xml_possible_path(Condition),
   !,
   pl4xml_select(Item, Condition, _).
pl4xml_check_condition(_, Condition) :-
   call(Condition).

pl4xml_possible_path(Path) :-
   could_be_an_fn_path(Path).
pl4xml_possible_path(Path) :-
   ( Path = _::_::_
   ; Path = _::_ ).


/******************************************************************/


