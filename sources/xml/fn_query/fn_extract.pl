

/******************************************************************/
/***                                                            ***/
/***       Field Notation:  Extraction                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* case_study(d3, rule_extract, Fn_Mode) <-
      */

case_study(d3, rule_extract, Fn_Mode) :-
   Ns = [1, 10, 100, 1000, 2000, 4000, 8000],
%  Ns = [1, 10, 100, 200],
%  Ns = [400],
   Rid = 'RADD534',
   dislog_variable_set(fn_mode, Fn_Mode),
   measure( d3_knowledge_base_read(KB), Time ),
   writeln(user, Time),
   maplist( case_study(d3, KB, Rid),
      Ns, Times ),
   pair_lists_2(Ns, Times, Pairs),
   nl,
   writeln(user, Pairs).

case_study(d3, KB, Rid, N, Time) :-
   Selectors = [
      'QContainers':0, 'Questions':0,
      'Diagnoses':0, 'KnowledgeSlices':N ],
   ( ( dislog_variable_get(fn_mode, fn),
       d3_test_extract(Selectors, KB, KB_2) )
   ; ( dislog_variable_get(fn_mode, dbn),
       d3_test_extract_dbn(Selectors, KB, KB_2) ) ),
   measure_until_measurable( 
      knowledge_slice_select(KB_2, Rid, _),
      Time ),
   !.


/* knowledge_slice_select(KB, Rid, Rules) <-
      */

knowledge_slice_select(_, Rid, Rules) :-
   dislog_variable_get(fn_mode, dbn),
   !,
   findall( Rule,
      ( reference(_, 'KnowledgeSlice', R),
        attribute(R, 'ID', Rid),
        dbn_to_fn(R, Rule) ),
      Rules ).
knowledge_slice_select(KB, Rid, Rules) :-
   dislog_variable_get(fn_mode, fn),
   findall( Rule,
      ( Rule := KB^'KnowledgeSlices'^'KnowledgeSlice',
        Rid := Rule@'ID' ),
      Rules ).


/* d3_test_extract(Selectors, KB) <-
      */

d3_test_extract :-
   Selectors = [
      'QContainers':1, 'Questions':2,
      'Diagnoses':3, 'KnowledgeSlices':4 ],
   d3_test_extract(Selectors, _).

d3_test_extract(Selectors, KB_2) :-
   d3_knowledge_base_read(KB_1),
   d3_test_extract(Selectors, KB_1, KB_2),
   case_study_file(d3, test, Path),
   name_append(Path, '.compact', Path_2),
   write_list(user, ['---> ', Path_2]), nl,
   dwrite(xml, Path_2, KB_2).


/* d3_test_extract_dbn(Selectors, KB, dbn) <-
      */

d3_test_extract_dbn(Selectors, KB, dbn) :-
   d3_test_extract(Selectors, KB, KB_2),
   retract_facts(reference/3),
   retract_facts(attribute/3),
   retract_facts(value/2),
%  measure( fn_term_to_dbn([KB_2]), Time ),
   measure( fn_to_database([KB_2]), Time ),
   write_list(user, [time:Time, '-']), ttyflush.

   
/* d3_test_extract(Selectors, KB_1, KB_2) <-
      */

d3_test_extract(Selectors, KB_1, KB_2) :-
   fn_item_parse(KB_1, T:As:Es_1),
   member(T:N, Selectors),
   !,
   length(Es_1, M),
   write_list(user, [T:N:M, '-']),
   first_n_elements(N, Es_1, Es_2),
   KB_2 = T:As:Es_2.
d3_test_extract(Selectors, KB_1, KB_2) :-
   fn_item_parse(KB_1, T:As:Es_1),
   !,
   maplist( d3_test_extract(Selectors),
      Es_1, Es_2 ),
   KB_2 = T:As:Es_2.   
d3_test_extract(_, KB, KB).


/* dislog_xml_extract(Selector, N) <-
      */

dislog_xml_extract(Selector, N) :-
   dread(xml, 'SonoConsult.xml', [Xml]),
   findall( X,
      ( X := Xml^Selector
      ; X := Xml^file^Selector
      ; X := Xml^'QContainers'^Selector ),
      Files_1 ),
   first_n_elements(N, Files_1, Files_2),
   dwrite(
      xml, 'results/SonoConsult_extract.xml', dislog:Files_2).
   

/******************************************************************/


