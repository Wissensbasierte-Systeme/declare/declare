

/******************************************************************/
/***                                                            ***/
/***       DisLog:  Field Notation - Schemas                    ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* fn_triple_to_schema_edges(KB, Edges) <-
      */

fn_triple_to_schema_edges(KB, Edges) :-
%  fn_triple_to_schema_edges_extended(KB, [_, Edges]).
   fn_triple_to_schema_edges_extended(KB, [Edges_A, Edges_E]),
   append(Edges_A, Edges_E, Edges).


/* fn_triple_to_schema_edges_extended(
         KB, [Edges_A, Edges_E]) <-
      */

fn_triple_to_schema_edges_extended(KB, [Edges_A, Edges_E]) :-
   fn_item_parse(KB, Tag:Attributes:Elements),
   !,
%  writeln(user, Tag),
   tag_to_other_tag(Tag, T),
   findall( T-T_2,
      ( member(Tag_2:_, Attributes),
        tag_to_other_tag(Tag_2, T_2) ),
      Edges_A_1 ),
   findall( T-T_2,
      ( member(Element, Elements),
        fn_item_parse(Element, Tag_2:_:_),
        tag_to_other_tag(Tag_2, T_2) ),
      Edges_E_1 ),
   maplist( fn_triple_to_schema_edges_extended,
      Elements, List ),
   pair_lists(List_A, List_E, List),
   flatten(List_A, Edges_A_2),
   append(Edges_A_1, Edges_A_2, Edges_A_3),
   list_to_ord_set(Edges_A_3, Edges_A),
   flatten(List_E, Edges_E_2),
   append(Edges_E_1, Edges_E_2, Edges_E_3),
   list_to_ord_set(Edges_E_3, Edges_E).
fn_triple_to_schema_edges_extended(_, [[], []]).


/* tag_to_other_tag(Tag_1, Tag_2) <-
      */

% tag_to_other_tag(Tag_1, Tag_2) :-
%    name_append(arg, X, Tag_1),
%    !,
%    name_append([arg, '_', X], Tag_2).
tag_to_other_tag(Tag, Tag).


/******************************************************************/


