

/******************************************************************/
/***                                                            ***/
/***       Xml:  X_Query_X to FN_Query                          ***/
/***                                                            ***/
/******************************************************************/


xqueryx_data_path(Path) :-
   dislog_variable_get(home, DisLog),
   concat(DisLog, '/projects/XQuery/', Path).
xqueryx_file(File) :-
   dislog_variable_get(home, DisLog),
   concat(DisLog, '/projects/XQuery/xqueryx.xml', File).


xqueryx_file_evaluate :-
   xqueryx_file(File),
   xqueryx_file_evaluate(File, Result),
   dwrite(xml, Result).

xqueryx_file_to_rule :-
   xqueryx_file(File),
   xqueryx_file_to_rule(File, Rule),
   pretty_print(rule, Rule).

xqueryx_file_to_fn_query :-
   xqueryx_file(File),
   xqueryx_file_to_fn_query(File, Query),
   dwrite(xml, Query).


/*** interface ****************************************************/


/* xqueryx_file_evaluate(File, Result) <-
      */

xqueryx_file_evaluate(File, Result) :-
   xqueryx_file_to_rule(File, Rule),
   Rule = [template(Tag:[T])]-Condition,
   list_to_comma_structure(Condition, Goal),
   findall( T,
      call(Goal),
      Ts ),
   Result = Tag:Ts.


/* xqueryx_file_to_rule(File, Rule) <-
      */

xqueryx_file_to_rule(File, Rule) :-
   xqueryx_file_to_fn_query(File, [Query]),
   xqueryx_to_tag(Query, Tag),
   xqueryx_to_for(Query,
      'xqx:forClause':Cs_2, Cs_1),
   xqueryx_to_where(Query,
      'xqx:whereClause':Cs_4, Cs_3),
   xqueryx_to_return(Query,
      'xqx:returnClause':Template, Cs_5),
   append([Cs_1, Cs_2, Cs_3, Cs_4, Cs_5], Cs),
   xqueryx_massage_rule_to_rule(
      [template(Tag:Template)]-Cs, Rule).

xqueryx_to_return(Query, Return, Assignments) :-
   Return_2 := Query^_^'xqx:returnClause',
   fn_term_to_path_expressions(
      Return_2, Assignments, Substitutions),
   substitute_recursively_no_copy(
      Substitutions, Return_2, Return).

xqueryx_to_where(Query, Where, Assignments) :-
   Where_2 := Query^_^'xqx:whereClause',
   Where_2 = 'xqx:whereClause':[Condition],
   condition_to_path_expressions(
      Condition, Assignments, Substitutions),
   substitute_recursively([[v(X), X]], Where_2, Where_3),
   substitute_recursively_no_copy(
      Substitutions, Where_3, Where).

xqueryx_to_for(Query, For, Assignments) :-
   For_2 := Query^_^'xqx:forClause',
   For_2 = 'xqx:forClause':[_ := Path],
   Path =.. [_, document(v(File))|_],
   n_free_variables(1, [Variable]),
   substitute_recursively_no_copy(
      [[document(v(File)), Variable]],
      For_2, For ),
   xqueryx_data_path(Data_Path),
   concat(Data_Path, File, Data_File),
   Assignments = [
      dread(xml, Data_File, [V]),
      Variable = document:[V]].

xqueryx_to_tag(Query, Tag) :-
   _:[Tag] := Query^_^'xqx:queryBody'^'xqx:expr'^'xqx:tagName'.

xqueryx_massage_rule_to_rule(Rule_1, Rule_2) :-
   free_variables(Rule_1, Vars),
   length(Vars, N),
   n_free_variables(N, Variables),
   copy_term(Variables, Vs),
   listvars(Vs, 2),
   pair_lists(Vs, Variables, Substitution),
   listvars(Rule_1, 2),
   n_free_variables(1, [Variable]),
   substitute_recursively_no_copy(
      [[$b, Variable]|Substitution], Rule_1, Rule_2).


/* xqueryx_file_to_fn_query(File, Query) <-
      */

xqueryx_file_to_fn_query(File, Query) :-
   dread(xml, File, [_|Query_2]),
   S_1 = [
      (v(Value)) -
         ('xqx:expr':_:['xqx:value':_:[Value]]),
      ($Var) -
         ('xqx:expr':[
             'xsi:type':'xqx:variable']:[
             'xqx:name':[Var]]) ],
   S_2 = [
      _ -
         ('xqx:expr':[
             'xsi:type':'xqx:pathExpr']:[
             Variable,
             'xqx:stepExpr':[
                'xqx:xpathAxis':[child],
                Test:[
                   'xqx:nodeName':[
                      'xqx:QName':[Selector]]]]]) -
         variable_test_selector_to_term(
            [Variable, Test, Selector]),
      (^Selector) -
         ('xqx:stepExpr':[
             'xqx:xpathAxis':[child],
             'xqx:elementTest':[
                'xqx:nodeName':[
                   'xqx:QName':[Selector]]]]) ],
   S_3 = [
      (conj:[type:Op]:Ps) -
         ('xqx:expr':_:[
             'xqx:opType':[Op],
             'xqx:parameters':Ps]),
      (Attribute:Value) -
         ('xqx:expr':[
             'xsi:type':'xqx:attributeConstructor']:[
             'xqx:attributeName':[Attribute],
             'xqx:attributeValue':[Value]]),
      (greater_save(A, B)) -
         (conj:[type:'>']:[A, B]),
      (A = B) -
         (conj:[type:'=']:[A, B]) ],
   S_4 = [
      _ -
         ('xqx:expr':['xsi:type':'xqx:functionCallExpr']:[
             'xqx:functionName':[Function],
             'xqx:parameters':Parameters]) -
         xqueryx_apply_function(Function, Parameters),
      (A, B) -
         (conj:[type:'AND']:[A, B]) ],
   S_5 = [
      (Tag:Attributes:Content) -
         ('xqx:expr':[
             'xsi:type':'xqx:elementConstructor']:[
             'xqx:tagName':[Tag],
             'xqx:attributeList':Attributes,
             'xqx:elementContent':Content ]),
      _ -
         ('xqx:expr':[
             'xsi:type':'xqx:pathExpr']:Content) -
         xqueryx_construct_path_expression(Content) ],
   S_6 = [
      ($Variable := Path) -
         ('xqx:forClauseItem':[
             'xqx:typedVariableBinding':[
                'xqx:varName':[Variable] ],
             'xqx:forExpr':[Path] ]) ],
   fn_transform_elements_fixpoints(
      [S_1, S_2, S_3, S_4, S_5, S_6], Query_2, Query ).


/* substitute_recursively(Substitution, Term_1, Term_2) <-
      */

substitute_recursively(Substitution, Term_1, Term_2) :-
   member([X, Y], Substitution),
   copy_term([X, Y], [Term_1, Term_2]),
   !.
substitute_recursively(Substitution, Term_1, Term_2) :-
   Term_1 =.. [Op|Terms_1],
   maplist( substitute_recursively(Substitution),
      Terms_1, Terms_2 ),
   Term_2 =.. [Op|Terms_2].

substitute_recursively_no_copy(
      Substitution, Term_1, Term_2) :-
   member([Term_1, Term_2], Substitution),
   !.
substitute_recursively_no_copy(
      Substitution, Term_1, Term_2) :-
   Term_1 =.. [Op|Terms_1],
   maplist( substitute_recursively_no_copy(Substitution),
      Terms_1, Terms_2 ),
   Term_2 =.. [Op|Terms_2].


/* path_expression(Path) <-
      */

path_expression(Path) :-
   Path =.. [Op|_],
   member(Op, ['/', '^', '@']).


/* fn_term_to_path_expressions(
         Term, Assignments, Substitutions) <-
      */

fn_term_to_path_expressions(Term, Assignments, Substitutions) :-
   fn_term_to_path_expressions(Term, Pairs),
   pair_lists(Assignments, Substitutions, Pairs).

fn_term_to_path_expressions(Term, Pairs) :-
   fn_item_parse(Term, _:As:Es),
   !,
   findall( [Assignment, [Attribute:Path, Attribute:Variable]],
      ( member(Attribute:Path, As),
        path_expression(Path),
        n_free_variables(1, [Variable]),
        Assignment = (Variable := Path) ),
      Pairs_1 ),
   maplist( fn_term_to_path_expressions,
      Es, Pss_2 ),
   append(Pss_2, Pairs_2),
   append(Pairs_1, Pairs_2, Pairs).
fn_term_to_path_expressions(Term,
      [[Assignment, [Term, Variable]]]) :-
   path_expression(Term),
   !,
   n_free_variables(1, [Variable]),
   Assignment = (Variable := Term).
fn_term_to_path_expressions(_, []).


/* condition_to_path_expressions(
         Condition, Assignments, Substitutions) <-
      */

condition_to_path_expressions(
      Condition, Assignments, Substitutions) :-
   condition_to_path_expressions(Condition, Pairs),
   pair_lists(Assignments, Substitutions, Pairs).

condition_to_path_expressions((A, B), Pairs) :-
   !,
   condition_to_path_expressions(A, Pairs_1),
   condition_to_path_expressions(B, Pairs_2),
   append(Pairs_1, Pairs_2, Pairs).
condition_to_path_expressions(Condition, Pairs) :-
   Condition =.. [_|Expressions],
   findall( [Assignment, [Path, Variable]],
      ( member(Path, Expressions),
        path_expression(Path),
        n_free_variables(1, [Variable]),
        Assignment =
           ( Variable := Path
           ; (_:_:[Variable]) := Path
           ; (_:[Variable]) := Path ) ),
      Pairs ).


/*** implementation ***********************************************/


variable_test_selector_to_term(
      [Variable, Test, Selector], _, X) :-
   Test = 'xqx:attributeTest',
   X =.. ['@', Variable, Selector].
variable_test_selector_to_term(
      [Variable, Test, Selector], _, X) :-
   Test = 'xqx:elementTest',
   X =.. ['^', Variable, Selector].
 

xqueryx_apply_function(Function, Parameters, _, Term) :-
   Term =.. [Function|Parameters].


xqueryx_construct_path_expression([X], _, X) :-
   !.
xqueryx_construct_path_expression([X|Xs], _, Y) :-
   xqueryx_apply_path_expression(Xs, Path, Op),
   Y =.. [Op, X, Path].

xqueryx_apply_path_expression([X], Selector, Op) :-
   X =.. [Op, Selector],
   !.
xqueryx_apply_path_expression([X|Xs], Path, Op) :-
   X =.. [Op_1, Selector],
   xquery_op_normalize(Op_1, Op),
   xqueryx_apply_path_expression(Xs, Path_2, Op_2),
   Path =.. [Op_2, Selector, Path_2].

xquery_op_normalize('^', '^').


greater_save(X, Y) :-
   term_to_atom(U, X),
   term_to_atom(V, Y),
   U > V.


/* pretty_print(rule, Rule) <-
      */

pretty_print(rule, Rule) :-
   pretty_print(rule, 0, Rule).

pretty_print(rule, N, Rule) :-
   listvars(Rule, 1),
   Rule = [A],
   tab(N),
   writeln(A). 
pretty_print(rule, N, Rule) :-
   listvars(Rule, 1),
   Rule = [A]-Bs,
   tab(N),
   M is N + 3,
   write_list([A, ' :- \n']),
   write_list_with_separators(
      pretty_print(atom, M, ','),
      pretty_print(atom, M, '.'),
      Bs ).

pretty_print(atom, N, Separator, A) :-
   tab(N),
   write_list([A, Separator, '\n']).


/******************************************************************/


