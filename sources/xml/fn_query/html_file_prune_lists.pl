

/******************************************************************/
/***                                                            ***/
/***             DisLog:  Html File Prune Lists                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* html_file_prune_lists(File_1, File_2) <-
      */

html_file_prune_lists(File, File) :-
   dislog_flag_get(xpce_list_browser_problem, no),
   !.
html_file_prune_lists(File_1, File_2) :-
   dislog_variable_get(output_path,
      'tmp_html_for_cms_doc_window.html', File_b),
   ( concat('file://', File_a, File_1)
   ; File_a = File_1 ),
   ( concat('file://', File_b, File_2)
   ; File_b = File_2 ),
   !,
   html_file_prune_lists_(File_a, File_b).

html_file_prune_lists_(File_1, File_2) :-
   dread(html, File_1, Xs),
   last(Xs, Item_1),
   fn_item_prune_lists(Item_1, Item_2),
   concat('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 ',
      'Transitional//EN">', Doctype),
   dwrite(xmls, File_2, [Doctype, Item_2]),

   dread(html, File_2, Ys),
   last(Ys, Item_3),
   Title := Item_3/descendant::title,
   Fng_2 = [ (meta:Es--->Title :- Es = Es), (X--->X) ],
   fn_transform([item, fng_program(Fng_2)], Item_3, Item_4),
   dwrite(xmls, File_2, [Doctype, Item_4]),

   Substitution = [["</p>",""], ["</br>",""], ["</hr>",""],
      ["<p>></p>",""], ["<hr>></hr>","<hr>"]],
   dread(txt, File_2, Name_1),
   name_exchange_sublist(Substitution, Name_1, Name_2),
   dwrite(txt, File_2, Name_2).

fn_item_prune_lists(Item_1, Item_2) :-
   Fng_1 = [ (ul:As:Es--->blockquote:As:Es),
      (ol:As:Es--->blockquote:As:Es),
      (li:Es--->br:Es), (X--->X) ],
   fn_transform([item, fng_program(Fng_1)], Item_1, Item_2).


/*
html_file_prune_lists_(File_1, File_2) :-
   Fng_1 = [ (ul:As:Es--->blockquote:As:Es),
      (ol:As:Es--->blockquote:As:Es),
      (li:Es--->br:Es), (X--->X) ],
   dread(xml, File_1, Xs),
   last(Xs, Item_1),
   fn_item_parse(Item_1, _:_:Es_1),
   fn_transform([item, fng_program(Fng_1)], html:Es_1, Item_2),
   fn_item_parse(Item_2, _:_:Es_2),
   concat('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 ',
      'Transitional//EN">', Doctype),
   dwrite(xmls, File_2, [Doctype, 'HTML':Es_2]).

item_to_item_for_fn_transform(Item_1, Item_2) :-
   ( fn_item_parse(Item_1, 'HTML':_:Es),
     Item_2 = 'html':[]:Es
   ; Item_2 = Item_1 ).
*/


/* html_to_file_for_cms_doc_window(Html, File) <-
      */

html_to_file_for_cms_doc_window(Html, File) :-
   html_to_html_with_information(Html, Html_2),
   dislog_variable_get(output_path, Results),
   concat([Results, cms_doc_window_tmp], Path),
   predicate_to_file(Path, dwrite(xml, Html_2)),
   concat(['file://', Path], File).

html_to_html_with_information(Html, Html) :-
   _ := Html/head/meta,
   !.
html_to_html_with_information(Html_1, Html_2) :-
   Xs := Html_1/content::'*',
   Html_2 = 'HTML':[
      head:[
         meta:['http-equiv':'Content-Type',
            content:'text/html; charset=windows-1252']:[],
         title:[]:[title],
         meta:[content:'MSHTML 6.00.2800.1126', name:'GENERATOR']:[] ],
      body:[bgcolor:'#ffeecc']:Xs ].


/******************************************************************/


