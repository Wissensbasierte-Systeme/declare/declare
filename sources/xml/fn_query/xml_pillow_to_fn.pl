

/******************************************************************/
/***                                                            ***/
/***       DisLog:  Reader for XML Using Pillow                 ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* pillow_read(xml, File, Fn) <-
      */

pillow_read(xml, File, Fn) :-
   pillow_read_terms(xml, File, Terms),
   xml_pillow_to_fn(Terms, Fn).

pillow_read_terms(xml, File, Terms) :-
   read_file_to_string(File, String),
   list_remove_elements([10], String, String_2),   
   xml2terms(String_2, Terms).


/* pillow_fn_item_remove_just_blanks(Item_1, Item_2) <-
      */

pillow_fn_item_remove_just_blanks(Item_1, Item_2) :-
   fn_item_parse(Item_1, T:As:Es_1),
   sublist( not_just_blanks,
      Es_1, Es_3 ),
   maplist( pillow_fn_item_remove_just_blanks,
      Es_3, Es_2 ),
   Item_2 = T:As:Es_2.
pillow_fn_item_remove_just_blanks(Item, Item) :-
   atomic(Item).

not_just_blanks(X) :-
   \+ atomic(X),
   !.
not_just_blanks(X) :-
   name(X, N),
   sort(N, M),
   M \= " ".


/* xml_pillow_to_fn(Element_1, Element_2) <-
      */

xml_pillow_to_fn(xmldecl(As1), xmldecl:As2:[]) :-
   !,
   maplist( xml_pillow_attribute_to_fn,
      As1, As2 ).
xml_pillow_to_fn(elem(Name, As1), Name:As2:[]) :-
   !,
   maplist( xml_pillow_attribute_to_fn,
      As1, As2 ).
xml_pillow_to_fn(env(Name, As1, Es1), Name:As2:Es2) :-
   !,
   maplist( xml_pillow_attribute_to_fn,
      As1, As2 ),
   maplist( xml_pillow_to_fn,
      Es1, Es2 ).
xml_pillow_to_fn(entity(X), Y) :-
   !,
   name_append(['&', X, ';'], Y).
xml_pillow_to_fn(X, Y) :-
   checklist( number,
      X ),
   !,
   name(Y, X).
xml_pillow_to_fn(Xs, Ys) :-
   is_list(Xs),
   !,
   maplist( xml_pillow_to_fn,
      Xs, Ys ).
xml_pillow_to_fn(X, X).

xml_pillow_attribute_to_fn(A=W, A:V) :-
   name(V, W).


/* swi_xml_entity_to_fn(entity(X), Y) <-
      */

xml_pillow_entity_to_fn(entity(X), Y) :-
   name_append(['&', X, ';'], Y).


/*** tests ********************************************************/


test(fn_query:pillow_read, 1) :-
   test_pillow_read_read_document(Xml),
   checklist( dwrite(xml),
      Xml ).
test(fn_query:pillow_read, 2) :-
   test_pillow_read_read_document(Xml),
   X := (document:Xml)^library^language^developer@'ID',
   writeln(X).
test(fn_query:pillow_read, 3) :-
   test_pillow_read_read_document(Xml),
   checklist( dwrite(xml),
      Xml ).

test_pillow_read_read_document(Xml) :-
   dislog_variable_get(example_path, Examples),
   concat(Examples, 'xml/diverse/langbase.xml', File),
   pillow_read(xml, File, Xml).


/******************************************************************/


