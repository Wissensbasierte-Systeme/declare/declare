

xml_diff(Xml_1, Xml_2) :-
   fn_item_parse(Xml_1, T_1:As_1:Es_1),
   fn_item_parse(Xml_2, T_2:As_2:Es_2),
   ( T_1 \= T_2 -> ( writeq(T_1 \= T_2), wait )
   ; As_1 \= As_2 -> ( writeq(As_1 \= As_2), wait )
   ; ( foreach(X_1, Es_1), foreach(X_2, Es_2) do
          xml_diff(X_1, X_2) ) ).

xml_equal_attribute_value_pairs([A:V1|As_1], [A:V2|As_2]) :-
   term_to_atom(T, V1),
   term_to_atom(T, V2),
   xml_equal_attribute_value_pairs(As_1, As_2).
xml_equal_attribute_value_pairs([], []).
   
   
