

/******************************************************************/
/***                                                            ***/
/***       Field Notation:  FN List Select                      ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      fn_get/3.

:- multifile
      fn_get/3.


/*** interface ****************************************************/


/* fn_get(Items_1, (^, V), Items_2) <-
      */

fn_get(Items_1, (^, V), Items_2) :-
   is_list(Items_1),
   var(V),
   !,
   fn_get(Items_1, (^, V::[]), Items_2).
fn_get(Items_1, (^, V), Items_2) :-
   is_list(Items_1),
   V =.. [::, W, Conditions],
%  W \= call,
   !,
   writeln_trace(user,
      call(fn_get(Items_1, (^, V1), Items_2))),
   member(Item, Items_1),
   fn_item_parse(Item, W:_:Items_2),
   writeln_trace(user,
      success(fn_get(Items_1, (^, V1), Items_2))),
   fn_check_conditions(Items_2, Conditions).
fn_get(Items_1, (^, V), Items_2) :-
   is_list(Items_1),
   fn_get(Items_1, (^, V::[]), Items_2).


/* fn_get(Items, (@, V), Attributes) <-
      */

fn_get(Items, (@, V), Attributes) :-
   is_list(Items),
   writeln_trace(user,
      call(fn_get(Items, (@, V), Attributes))),
   member(Item, Items),
   fn_item_parse(Item, V:Attributes:_),
   writeln_trace(user,
      success(fn_get(Items, (@, V), Attributes))).


/******************************************************************/


