

/******************************************************************/
/***                                                            ***/
/***       Field Notation:  FN Select                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* fn_get(Item, (@, Attribute), Value) <-
      */

fn_get(Item, (@, Attribute), Value) :-
   !,
   ( dislog_variable_get(fn_mode, gn) ->
     attribute(Item, Attribute, Value)
   ; fn_item_parse(Item, _:As:_),
     member(Attribute:Value, As) ).


/* fn_get(Item_1, (^, Loc), Item_2) <-
      Loc can be
       - Axis::Test::Predicates,
       - Tag::Predicates,
       - Tag.
      If Loc is a variable, then it is treated as a tag name. */

fn_get(Item_1, (^, Loc), Item_2) :-
   fn_loc_parse(Loc, Loc_2, Predicates),
   !,
   fn_get(Item_1, (^, Loc_2), Item_2),
   fn_check_conditions(Item_2, Predicates).

fn_loc_parse(Loc, Axis::Test, Predicates) :-
   nonvar(Loc),
   Loc = Axis::TC,
   nonvar(TC),
   TC = Test::Predicates,
   is_list(Predicates).
fn_loc_parse(Loc, T, Predicates) :-
   nonvar(Loc),
   Loc = T::Predicates,
   is_list(Predicates).

fn_get(Item_1, (^, Loc), Item_2) :-
   nonvar(Loc),
   Loc = call::Predicate,
   !,
   call(Predicate, Item_1, Item_2),
   !.

fn_get(Item_1, (^, Loc), Item_2) :-
   nonvar(Loc),
   Loc = transform::File,
   !,
   fn_transform_fn_item_fng(File, Item_1, Item_2),
   !.

fn_get(Item_1, (^, Loc), Item_2) :-
   nonvar(Loc),
   Loc = Axis::_,
   var(Axis),
   !,
   member(Axis, [
      self, child,
      descendant, descendant_or_self,
      descendent, descendent_or_self,
      attributes ]),
   fn_get(Item_1, (^, Loc), Item_2).

fn_get(Item, (^, Loc), T) :-
   Loc == tag::'*',
   !,
   fn_item_parse(Item, T:_:_).
fn_get(Item, (^, Loc), As) :-
   Loc == attributes::'*',
   !,
   fn_item_parse(Item, _:As:_).
fn_get(Item, (^, Loc), Es) :-
   Loc == content::'*',
   !,
   fn_item_parse(Item, _:_:Es).
fn_get(Item, (^, Loc), Item_2) :-
   Loc == text::'*',
   !,
   fn_item_parse(Item, _:_:Es),
   names_append_with_separator(Es, ', ', Item_2).

fn_get(Item, (^, Loc), Item) :-
   fn_loc_test(Loc = self::T),
   !,
   T := Item^tag::'*'.
fn_get(Item_1, (^, Loc), Item_2) :-
   fn_loc_test(Loc = child::T),
   !,
   fn_get(Item_1, (^, T), Item_2).
fn_get(Item_1, (^, Loc), Item_2) :-
   fn_loc_test(Loc = attribute::T),
   !,
   fn_get(Item_1, (@, T), Item_2).
fn_get(Item_1, (^, Loc), Item_2) :-
   fn_loc_test(Loc = nth_child::N),
   !,
   fn_item_parse(Item_1, _:_:Es),
   nth(N, Es, Item_2).
fn_get(Item_1, (^, Loc), Item_2) :-
   ( fn_loc_test(Loc = descendant::T)
   ; fn_loc_test(Loc = descendent::T) ),
   !,
   ( fn_get(Item_1, (^, child::T), Item_2)
   ; fn_get(Item_1, (^, child::'*'), Item_3),
     fn_get(Item_3, (^, Loc), Item_2) ).
fn_get(Item_1, (^, Loc), Item_2) :-
   nonvar(Loc),
   ( Loc = descendant_or_self::T
   ; Loc = descendent_or_self::T ),
   !,
   ( fn_get(Item_1, (^, descendant::T), Item_2)
   ; fn_get(Item_1, (^, self::T), Item_2) ).


% only for graph notation

fn_get(Item_1, (^, Loc), Item_2) :-
%  dislog_variable_get(fn_mode, gn),
   fn_loc_test(Loc = parent::T),
   !,
   reference(Item_2, _, Item_1, _),
   reference(_, T, Item_2, _).
fn_get(Item_1, (^, Loc), Item_2) :-
%  dislog_variable_get(fn_mode, gn),
   member(Axis, [preceding_sibling, following_sibling]),
   fn_loc_test(Loc = Axis::T),
   !,
   reference(Item_p, _, Item_1, _),
   fn_item_parse(Item_p, _:_:Es),
%  writeln(user, Item_p:Es),
   nth(N, Es, Item_1),
%  ( Axis = preceding_sibling,
%    M is N - 1
%  ; Axis = following_sibling,
%    M is N + 1 ),
   nth(M, Es, Item_2),
   ( Axis = preceding_sibling ->
     M < N
   ; Axis = following_sibling,
     M > N ),
   T := Item_2^tag::'*'.
%  T = Item_2.
fn_get(Item_1, (^, Loc), Item_2) :-
%  dislog_variable_get(fn_mode, gn),
   fn_loc_test(Loc = ancestor::T),
   !,
   ( fn_get(Item_1, (^, parent::T), Item_2)
   ; fn_get(Item_1, (^, parent::'*'), Item_3),
     fn_get(Item_3, (^, Loc), Item_2) ). 
fn_get(Item_1, (^, Loc), Item_2) :-
%  dislog_variable_get(fn_mode, gn),
   nonvar(Loc),
   Loc = ancestor_or_self::T,
   !,
   ( fn_get(Item_1, (^, ancestor::T), Item_2)
   ; fn_get(Item_1, (^, self::T), Item_2) ).
fn_get(Item_1, (^, Loc), Item_2) :-
%  dislog_variable_get(fn_mode, gn),
   nonvar(Loc),
   member(Axis, [preceding, following]),
   Loc = Axis::T,
   !,
   concat(Axis, '_sibling', Axis_2),
   fn_get(Item_1, (^, ancestor_or_self::'*'), Item_3),
   fn_get(Item_3, (^, Axis_2::'*'), Item_4),
   fn_get(Item_4, (^, descendant_or_self::T), Item_2).


/* fn_get(Item_1, (^, Tag), Item_2) <-
      */

fn_get(Item_1, (^, Tag), Item_2) :-
   ( dislog_variable_get(fn_mode, fn),
     fn_item_parse(Item_1, _:_:Es),
     member(Item_2, Es),
     fn_item_parse(Item_2, Tag:_:_)
   ; reference(Item_1, Tag, Item_2, _) ).


/* fn_loc_test(Loc = Axis::T) <-
      */

fn_loc_test(Loc = Axis::T) :-
   ( Loc == Axis::'*'
   ; nonvar(Loc),
     Loc = Axis::T ).


/* fn_get_identical(Path, Item_1, Item_2, Value) <-
      */

fn_get_identical(Path, Item_1, Item_2) :-
   fn_get_identical(Path, Item_1, Item_2, _).

fn_get_identical(Path, Item_1, Item_2, Value) :-
   fn_path_to_op_and_tail(Path, [Op, Tail]),
   Expression_1 =.. [Op, Item_1, Tail],
   Expression_2 =.. [Op, Item_2, Tail],
   Value := Expression_1,
   Value := Expression_2.


/******************************************************************/


