

/******************************************************************/
/***                                                            ***/
/***       Field Notation:  FN Update                           ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      fn_update/4,
      fn_update_2/5, fn_update_3/5.

:- discontiguous
      fn_update/3.


/*** interface ****************************************************/


/* fn_update(U, P:A, X) <-
      */

fn_update(U, [P|Ps], X) :-
   fn_update(U, P, Y),
   fn_update(Y, Ps, X).
fn_update(U, [], U).

fn_update(U, P:A, X) :-
   is_list(U),
   fn_path_split(P, [P1, P2]),
   fn_apply(U, P1, Y),
   fn_update(Y, P2:A, Z),
   fn_update(U, P1:Z, X).
fn_update(U, P:A, X) :-
   is_list(U),
   fn_path_split(P, [(O, V)]),
   fn_set(U, (O, V):A, X).

fn_update(U, P:A, X) :-
   fn_path_split(P, [P1, P2]),
   P1 = (Op_1, V1),
   nonvar(V1),
   V1 =.. [::, W1, Conditions],
   !,
   fn_update_2((Op_1, W1)::Conditions, P2:A, U, X).
fn_update(U, P:A, X) :-
   fn_path_split(P, [P1, P2]),
   P1 = (Op_1, V1),
   fn_update_2((Op_1, V1), P2:A, U, X).

fn_update(U, P:A, X) :-
   writeln_trace(user,
      fn_update_single_a(U, P:A, X)),
   fn_path_split(P, [(Op, V)]),
   V =.. [::, W, Conditions],
   !,
   fn_update_2((Op, W)::Conditions, []:A, U, X).
fn_update(U, P:A, X) :-
   writeln_trace(user,
      fn_update_single_b(U, P:A, X)),
   fn_path_split(P, [(Op, V)]),
   writeln_trace(user,
      fn_path_split_here(P, [(Op, V)])),
   writeln_trace(user,
      calling_fn_update_2((Op, V), []:A, U, X)),
   fn_update_2((Op, V), []:A, U, X).


/*** implementation ***********************************************/


/* fn_update_2(Selector::Conditions, P:A, U, X) <-
      */

fn_update_2((Op, V)::Conditions, P:A, U, X) :-
   member(Op, [/, ^]),
   writeln_trace(user,
      fn_update_2_a((^, V)::Conditions, P:A, U, X)),
   fn_item_parse(U, T:As:Es_1),
   ( foreach(E1, Es_1), foreach(E2, Es_2) do
        fn_update_3((^, V)::Conditions, P:A, E1, E2) ),
   X = T:As:Es_2.
fn_update_2((Op, V), P:A, U, X) :-
   member(Op, [/, ^]),
   writeln_trace(user,
      fn_update_2_b((^, V), P:A, U, X)),
   fn_item_parse(U, T:As:Es_1),
   ( ( member(Item, Es_1),
       writeln_trace(user, after_member),
       fn_item_parse(Item, V:_:_),
       writeln_trace(user, after_parse),
       !,
       writeln_trace(user, Es_1),
       ( foreach(E1, Es_1), foreach(E2, Es_2) do
            ( writeln_trace(user, fn_update_3((^, V), P:A, E1, E2)),
              fn_update_3((^, V), P:A, E1, E2) ) ),
       writeln_trace(user, after_foreach),
       X = T:As:Es_2 )
   ; ( P \= [],
       fn_update(V:[], P:A, Z),
       X = T:As:[Z|Es_1] )
   ; X = T:As:[V:A|Es_1] ).

fn_update_2((@, V)::Conditions, []:A, U, X) :-
   writeln_trace(user,
      fn_update_2_c((@, V)::Conditions, []:A, U, X)),
   fn_check_conditions(X, Conditions),
   !,
   fn_item_parse(U, T:As_1:Es),
   As_2 := As_1 * [V:A],
   X = T:As_2:Es.
fn_update_2((@, _)::_, []:_, U, U).
fn_update_2((@, V), []:A, U, U) :-
   dislog_variable_get(fn_mode, gn),
   retract_all(user:attribute(U, A, _)),
   assert(user:attribute(U, V, A)).
fn_update_2((@, V), []:A, U, X) :-
   fn_item_parse(U, T:As_1:Es),
   writeln_trace(user, As_2 := As_1 * [V:A]),
   As_2 := As_1 * [V:A],
   writeln_trace(user, As_2 := As_1 * [V:A]),
   X = T:As_2:Es.


/* fn_update_3(Selector::Conditions, P:A, X, Y) <-
      */

fn_update_3((^, V)::Conditions, P:A, X, Y) :-
   writeln_trace(user,
      fn_update_3_a((^, V)::Conditions, P:A, X, Y)),
   P \= [],
   writeln_trace(user,
      fn_check_conditions(X, Conditions)),
   fn_check_conditions(X, Conditions),
   writeln_trace(user, ii),
   fn_item_parse(X, V:_:_),
   writeln_trace(user, iii),
   writeln_trace(user, fn_update_now(X, P:A, Y)),
   fn_update(X, P:A, Y),
   !.
fn_update_3((^, V), P:A, X, Y) :-
   P \= [],
   fn_item_parse(X, V:_:_),
   fn_update(X, P:A, Y),
   !.
fn_update_3(_, P:_, X, X) :-
   P \= [].

fn_update_3((^, V)::Conditions, []:A, X, Y) :-
   writeln_trace(user, fn_update_3((^, V)::Conditions, []:A, X, Y)),
   fn_check_conditions(X, Conditions),
   fn_item_parse(X, V:As:_),
   Y = V:As:A.
fn_update_3((^, V), []:A, X, Y) :-
   writeln_trace(user, ok),
   fn_update_3((^, V)::[], []:A, X, Y).
fn_update_3(_, []:_, X, X).


/* fn_set(Xs, Selector:Value, Ys) <-
      */

fn_set(Xs, (@, V):A, Ys) :-
   is_list(Xs),
   !,
   maplist( fn_set_single((@, V):A),
      Xs, Zs ),
   list_to_ord_set(Zs, Ys).
fn_set(Xs, (^, V):A, Ys) :-
   is_list(Xs),
   maplist( fn_set_single((^, V):A),
      Xs, Zs ),
   ( ( member(V:_, Zs),
       !,
       list_to_ord_set(Zs, Ys) )
   ; list_to_ord_set([V:A|Zs], Ys) ).

fn_set(Item_1, (@, V):A, Item_2) :-
   \+ is_list(Item_1),
   !,
   fn_item_parse(Item_1, T:As_1:Es),
   As_2 := As_1*[V:A],
   writeln_trace(user, As_2 := As_1*[V:A]),
   fn_item_prune(T:As_2:Es, Item_2).

fn_set(Item_1, (^, V):A, Item_2) :-
   \+ is_list(Item_1),
   V =.. [::, W, Conditions],
   !,
   fn_item_parse(Item_1, T:As:Es_1),
   maplist( fn_set_under_conditions(Conditions, (^, W):A),
      Es_1, Es_2 ),
   fn_item_prune(T:As:Es_2, Item_2).
fn_set(Item_1, (^, V):A, Item_2) :-
   \+ is_list(Item_1),
   !,
   fn_item_parse(Item_1, T:As:Es_1),
   fn_transform_elements_fixpoint([A-(V:_:_)], Es_1, Es_2),
   fn_item_prune(T:As:Es_2, Item_2).

fn_set_under_conditions(Conditions, (^, W):A, Item, W:A) :-
   fn_item_parse(Item, W:_:_),
   fn_check_conditions(Item, Conditions),
   !.
fn_set_under_conditions(_, _, Item, Item).


/* fn_set_single(P:A, Item_1, Item_2) <-
      */

fn_set_single((@, V):A, Item, V:A:B) :-
   fn_item_parse(Item, V:_:B),
   !.
fn_set_single((@, _):_, Item, Item) :-
   !.

/*
fn_set_single((^, V):As:Es, Item, V:As:Es) :-
   fn_item_parse(Item, V:_:_),
   !.
*/
fn_set_single((^, V):Es, Item, V:Es) :-
   fn_item_parse(Item, V:[]:_),
   !.
fn_set_single((^, V):Es, Item, V:As:Es) :-
   fn_item_parse(Item, V:As:_),
   !.
fn_set_single((^, _):_, Item, Item).


/* fn_update_test(X := E, Test) <-
      */

fn_update_test(X := E, Test) :-
   findall( solution:[X],
      X := E,
      Xs ),
   fn_expression_replace_operand(E, E2),
   listvars(E2, 1, _),
   Test = test:[query:E2]:Xs.

fn_expression_replace_operand(E1, E2) :-
   fn_item_parse(E1, _),
   !,
   E2 = xml.
fn_expression_replace_operand(E1, E2) :-
   E1 =.. [Op, _, Path],
   Op \= *,
   E2 =.. [Op, xml, Path].
fn_expression_replace_operand(E1, E2) :-
   E1 =.. [*, P1, P2],
   fn_expression_replace_operand(P1, P3),
   E2 =.. [*, P3, P2].


/*** tests ********************************************************/


test(fn_query:fn_update, fn_update_test_suite) :-
   fn_update_tests,
   !,
   fn_update_tests_check_result.

fn_update_tests_check_result :-
   Test_Suite_File = 'tests/fn_update_test_suite.xml',
%  current_prolog_flag(version, Version),
%  ( Version < 60000 ->
%    Test_Suite_File = 'tests/fn_update_test_suite_swi_5.xml'
%  ; Test_Suite_File = 'tests/fn_update_test_suite_swi_6.xml' ),
   dread(xml, 'results/fn_update_test_suite.xml', [Test_Suite]),
   dread(xml, Test_Suite_File, [Test_Suite_2]),
   fn_item_parse(Test_Suite, test_suite:_:Es),
   fn_item_parse(Test_Suite_2, test_suite:As_2:Es_2),
   !,
   fn_item_lists_from_files_compare(Es, Es_2),
%  Es = Es_2,
   Date := As_2^date,
   Time := As_2^time,
   write_list(user, [
      '% testing ... test suite, same results as ',
      Date, ' ', Time]),
   nl(user).


/* fn_update_tests <-
      */

fn_update_tests :-
   dread(xml, 'sources/xml/fn_query/stocks.xml', [Xml]),
   fn_update_tests(Xml, Tests),
   dislog_date_and_time(Date, Time),
   Test_Suite = test_suite:[
      date:Date, time:Time]:[
      document:[Xml]|Tests],
   dwrite(xml, 'results/fn_update_test_suite.xml', Test_Suite).

fn_update_tests(Xml, Tests) :-
   maplist( fn_update_test, [
      X := Xml^chart * [^entry::[@date='14.12.2002']@value:'60'],
      X := Xml^chart::[@wkn='600800'] * [^entry@value:'60'],
      X := Xml^chart * [^entry::[@value='50']@value:'60'],
      X := Xml^chart * [
         ^entry::[@value=V, add_to_atom(V, '1', W)]@value:W],
      X := Xml * [^chart^entry::[@date='14.12.2002']@value:'60'],
      X := Xml * [^chart^entry::[@date='14.12.2002']@time:'10:00'],
      X := Xml * [^chart^entry::[@date='14.12.2002']^time:['10:00']],
      X := Xml * [^chart^entry::[@date='14.12.2002']
         ^time_with_offset:[time:[offset:'+01:00']:['10:00']]],
      X := Xml * [^chart^entry^time^hour:'10:00'],
      X := Xml <+> [^chart::[@wkn='200400']
         ^entry:[date:'16.12.2002', value:'60']:[]],
      X := Xml <-> [^chart::[@wkn='600800']
         ^entry::[@date='14.12.2002']@value] ],
      Tests ).

add_to_atom(Xa, Ya, Za) :-
   term_to_atom(Xt, Xa),
   term_to_atom(Yt, Ya),
   Zt is Xt + Yt,
   term_to_atom(Zt, Za).


/******************************************************************/


