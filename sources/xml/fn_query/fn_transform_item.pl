

/******************************************************************/
/***                                                            ***/
/***       Field Notation: FN_Transform                         ***/
/***                                                            ***/
/******************************************************************/


:- op(1190, xfx, --->).


/*** interface ****************************************************/


/* fn_item_transform_fng(Fng, Item_1, Item_2) <-
      */

fn_item_transform_fng(Fng, Item_1, Item_2) :-
   fn_item_substitution_tmp_prepare(Fng, Predicate),
   fn_item_transform(Predicate, Item_1, Item_2),
   !.


/* fn_transform_fn_item_fng(File_S, Item_1, Item_2) <-
      */

fn_transform_fn_item_fng(File_S, Item_1, Item_2) :-
   dislog_variable_get(output_path, Results),
   concat(Results,
      'fn_transform_fn_item_fng_tmp_1.xml', File_1),
   concat(Results,
      'fn_transform_fn_item_fng_tmp_2.xml', File_2),
   ( dislog_variable_get(fn_mode, fn) ->
     dwrite(xml, File_1, Item_1)
   ; gn_to_fn(Item_1, Xml),
     dwrite(xml, File_1, Xml) ),
   fn_transform_xml_file_fng(File_S, File_1, File_2),
   dread(xml, File_2, [Item_2]),
   delete_file_(File_1),
   delete_file_(File_2).

delete_file_(File) :-
   file_exists(File),
   !.
delete_file_(File) :-
   delete_file(File).


/* fn_transform_xml_file_fng(File_S, File_1, File_2) <-
      */

fn_transform_xml_file_fng_file_view(File_S, File_1, File_2) :-
   fn_transform_xml_file_fng(File_S, File_1, File_2),
   file_view(File_2).

fn_item_transform_no_files(File_S, Item_1, Item_2) :-
   fn_item_substitution_tmp_prepare(File_S, Predicate),
   fn_item_transform(Predicate, Item_1, Item_2).

fn_transform_xml_file_fng(File_S, File_1, File_2) :-
   fn_item_substitution_tmp_prepare(File_S, Predicate),
   write_list(user, ['---> ', File_1, '\n']),
   dread(xml, File_1, Xml),
   last(Xml, Xml_1),
   ( dislog_variable_get(fn_mode, fn),
     Item_1 = Xml_1
   ; dislog_variable_get(fn_mode, gn),
     fn_to_gn(Xml_1, Item_1) ),
   !,
   fn_item_transform(Predicate, Item_1, Item_2),
   ( dislog_variable_get(fn_mode, fn),
     Xml_2 = Item_2
   ; dislog_variable_get(fn_mode, gn),
     gn_to_fn(Item_2, Xml_2) ),
   write_list(user, ['<--- ', File_2, '\n']),
   dwrite(xml, File_2, Xml_2).


/* fn_item_substitution_tmp_prepare(File, Predicate) <-
      A binary substitution predicate Predicate
      is created from a substitution file File.
      Predicate/2 is exported from a module Predicate,
      which hides the substitution rules from File. */

fn_item_substitution_tmp_prepare(File, Predicate) :-
   ( var(Predicate),
     gensym(fn_item_substitution_tmp_, Predicate),
     !
   ; true ),
   dislog_variable_get(output_path, Results),
   concat(Results, Predicate, Target),
   concat([Results, Predicate, '_header'], Header),
   Goal =.. [Predicate, X, Y],
   dportray(Header, lp, [
      []-[module(Predicate, [Predicate/2])],
      []-[discontiguous('--->'/2)],
      [Goal]-[X--->Y]]),
   read_file_to_string(Header, Xs),
   read_file_to_string(File, Ys),
   append([Xs, "\n", Ys, "\n"], Zs),
   name(Substitutions, Zs),
   predicate_to_file(Target, write(Substitutions)),
   consult(Target),
   delete_file_(Header),
   delete_file_(Target).


/*** tests ********************************************************/


test(fn_query:fn_transform,
      fn_transform_xml_file_fng('Jean Paul')) :-
   ( dislog_variable_get(fn_transform_directory, Directory),
     file_exists(Directory) ->
     concat(Directory, 'substitution.fng', Path_S),
     concat(Directory, 'test.xml', Path_1),
     concat(Directory, 'test_result.xml', Path_2),
     fn_transform_xml_file_fng(Path_S, Path_1, Path_2)
   ; true ).


/******************************************************************/


