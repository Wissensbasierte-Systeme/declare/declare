

/******************************************************************/
/***                                                            ***/
/***       Field Notation:  FN Path                             ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* fn_path_to_op_and_tail(Path, [Op, Tail]) <-
      */

fn_path_to_op_and_tail(Path, [Op, Tail]) :-
   fn_path_split(Path, [(Op, Tail)]).
fn_path_to_op_and_tail(Path, [Op, Tail]) :-
   fn_path_split(Path, [(Op, P), (Op2, P2)]),
   Tail =.. [Op2, P, P2].
   

/* fn_path_split(Path, [Selector|Paths]) <-
      */

fn_path_split((/, V), Paths) :-
   !,
   fn_path_split((^, V), Paths).

fn_path_split((Op, V), [(Op, V)]) :-
   member(Op, [^, @]),
   var(V).
fn_path_split((Op, V_0), [(Op, V_1), (Op_2, V_2)]) :-
   member(Op, [^, @]),
   ( member(Op_3, [^, @])
   ; nonvar(V_0),
     Op_3 = / ),
   V_0 =.. [Op_3, V_1, V_2],
   slash_to_hat(Op_3, Op_2).
fn_path_split((Op, V), [(Op, V)]) :-
   member(Op, [^, @]),
   \+ fn_complex_expression(V).

fn_path_split(P, [(Op_1, V_1), (Op_2, V_2)]) :-
   member(Op_3, [^, /, @]),
   P =.. [Op_3, P_1, V_2],
   slash_to_hat(Op_3, Op_2),
   member(Op_4, [^, /, @]),
   P_1 =.. [Op_4, V_1],
   slash_to_hat(Op_4, Op_1).
fn_path_split(P, [(Op, V)]) :-
   member(Op, [^, @]),
   P =.. [Op, V].
fn_path_split(P, [(^, V)]) :-
   P =.. [/, V].

fn_path_split(P, [(^, V_1), (Op_2, V_2)]) :-
   member(Op_2, [^, /, @]),
   P =.. [Op_2, P_1, V_2],
   P_1 =.. [V_1].
fn_path_split(P, [(^, V)]) :-
   P =.. [V].

slash_to_hat(/, ^) :-
   !.
slash_to_hat(X, X).


/* fn_complex_expression(E) <-
      */

fn_complex_expression(_@_).
fn_complex_expression(_^_).
fn_complex_expression(_*_).
fn_complex_expression(_/_).
fn_complex_expression(_<+>_).
fn_complex_expression(_<->_).
% fn_complex_expression(_::_).


/* fn_selector_duplicate(P, P1, P2) <-
      */

fn_selector_duplicate((Op_1, V), (Op_1, V_1), (Op_2, V_2)) :-
   member(Op_1, [^, @]),
   member(Op_2, [^, @]),
   V =.. [Op_2, V_1, V_2].


/* fn_path_add_in_front(O, P, (O, P)) <-
      */

fn_path_add_in_front(O, P, (O, P)).


/******************************************************************/


