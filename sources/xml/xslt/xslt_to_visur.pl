

/******************************************************************/
/***                                                            ***/
/***          Visur:  Rule/Goal-Graphs                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xslt_file_to_picture(File) <-
      */

xslt_file_to_picture(File, Picture) :-
   xslt_file_to_vertices_and_edges(File, Vertices, Edges),
   xpce_picture_clear(Picture),
   vertices_edges_to_picture(Vertices, Edges, Picture).

xslt_file_to_picture(File) :-
   xslt_file_to_vertices_and_edges(File, Vertices, Edges),
   vertices_edges_to_picture(Vertices, Edges).
%  vertices_edges_to_gxl_graph(Vertices, Edges, Gxl),
%  gxl_to_xpce:gxl_graph_to_picture(Gxl).


/* xslt_file_to_vertices_and_edges(File, Vertices, Edges) <-
      */

xslt_file_to_vertices_and_edges(File, Vertices, Edges) :-
   xml_file_to_fn_term(File, [Xslt]),
   xslt_to_edges(Xslt, Edges),
%  writeln(user, edges(Edges)),
   xslt_to_vertices(Xslt, Vertices_All),
   length(Edges, N),
   edges_to_vertices(Edges, Vertices),
%  writeln(user, vertices(Vertices)),
%  vertices_edges_reduce(Vertices, Edges, Vertices_2, Edges_2),
   ord_subtract(Vertices_All, Vertices, Vertices_Isolated),
   length(Vertices_All, N1),
   length(Vertices_Isolated, N2),
   star_line,
   write_list([
      'edges: ', N,
      ', all vertices: ', N1, ', isolated vertices: ', N2]), nl,
   star_line,
   writeln_list(Vertices_Isolated),
   star_line.


/* xslt_to_vertices(Xslt, Vertices) <-
      */

xslt_to_vertices(Xslt, Vertices) :-
   findall( V,
      ( Template := Xslt^'xsl:template',
        ( ( Name := Template@name,
            V = Name )
        ; ( Match := Template@match,
%           V = match:Match ) ) ),
            V = Match ) ) ),
      Vertices_2 ),
   list_to_ord_set(Vertices_2, Vertices).


/* xslt_to_edges(Xslt, Edges) <-
      */

xslt_to_edges(Xslt, Edges) :-
   findall( V-W,
      ( Template := Xslt^'xsl:template',
        ( ( Name := Template@name,
            V = Name )
        ; ( Match := Template@match,
%           V = match:Match ) ),
            V = Match ) ),
        xslt_template_to_vertex(Template, W) ),
      Edges ).

xslt_template_to_vertex(Template, Vertex) :-
   ( Vertex := Template^'xsl:call-template'@name
   ; Vertex := Template^_^'xsl:call-template'@name ).
xslt_template_to_vertex(Template, Vertex) :-
   ( Vertex := Template^'xsl:apply-templates'@select
   ; Vertex := Template^_^'xsl:apply-templates'@select ),
   !.
xslt_template_to_vertex(Template, Vertex) :-
   ( _ := Template^'xsl:apply-templates'
   ; _ := Template^_^'xsl:apply-templates' ),
   Vertex = 'xsl:apply-templates'.


/*** tests ********************************************************/


test(xml:xslt_file_to_picture, 1) :-
   dislog_variable_get(example_path, Examples),
   concat(Examples, 'xml/myxupdate2xslt.xsl', File),
   file_exists(File),
   xslt_file_to_picture(File).


/******************************************************************/


