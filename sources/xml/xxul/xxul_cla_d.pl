

/******************************************************************/
/***                                                            ***/
/***           XXUL:  Classes                                   ***/
/***                                                            ***/
/******************************************************************/



/*** implementation ***********************************************/


:- pce_begin_class(xxul_statusbar, dialog).

variable(maxwidth, int, none).
variable(progressbar, device, none).
variable(progressbox, box, none).
variable(text, text, none).

initialise(Win, Width:width=[int], Colour:colour=[colour]) :->
   send_super(Win, initialise),
   default(Width, 150, Nwidth),
   default(Colour, colour(@default, 0, 0, 0), Ncolour),
   new(Txt, text('          ', left, normal)),
   get(Txt, height, Height),
   new(Pbar, device),
   new(Fbox, box(Nwidth, Height)),
   send(Fbox, pen(1)),
   Nheight is Height - 2,
   new(Pbox, box(0, Nheight)),
   send(Pbox, pen(0)),
   send(Pbox, fill_pattern(Ncolour)),
   send(Pbar, display(Fbox, point(0, 0))),
   send(Pbar, display(Pbox, point(1, 1))),
   Mwidth is Nwidth - 2,
   Args1 = [ append(Txt),
      append(Pbar, right),
      border(size(0, 0)),
      pen(0),
      slot(maxwidth, Mwidth),
      slot(progressbar, Pbar),
      slot(progressbox, Pbox),
      slot(text, Txt) ],
   send_list(Win, Args1),
   Args2 = [ hor_shrink(100),
      hor_stretch(100),
      ver_shrink(0),
      ver_stretch(0) ],
   send_list(Win?tile, Args2).

clear(Win) :->
   get(Win, slot(progressbox), Pbox),
   send_list(Pbox, [width(0), flush]).

resize(Win) :->
   get(Win, slot(progressbar), Pbar),
   get(Win?area, width, Wlen),
   get(Pbar, width, Plen),
   Xcor is Wlen - Plen - 1,
   send(Pbar, x(Xcor)).

progress(Win, Percent:int) :->
   between(0, 100, Percent),
   get(Win, slot(maxwidth), Max),
   get(Win, slot(progressbox), Pbox),
   Factor is Percent / 100,
   Width is round(Factor * Max),
   send_list(Pbox, [width(Width), flush]).

string(Win, String:char_array) :->
   get(Win, slot(text), Text),
   send_list(Text, [string(String), flush]).

:- pce_end_class.


:- pce_begin_class(xxul_listbox, label_box).

variable(list, list_browser, none).

initialise(Dev, Name:name=[name]) :->
   send_super(Dev, initialise(Name)),
   new(List, list_browser),
   get(new(_, menu), value_font, Font),
   send(List, font(Font)),
   send_super(Dev, append(List)),
   send(Dev, slot(list, List)).

append(Dev, Item:menu_item) :->
   get(Dev, slot(list), List),
   send(List?dict, append(dict_item(Item?value))).

clear(Dev) :->
   get(Dev, slot(list), List),
   send(List, clear).

members(Dev, Vals:chain) :<-
   get(Dev, slot(list), List),
   get(List?dict, members, Chain),
   chain_list(Chain, Items),
   new(Vals, chain),
   forall( member(Item, Items),
      ( get(Item?label, value, Val),
        send(Vals, append(Val)) ) ).

selection(Dev, Value:name*) :<-
   get(Dev, slot(list), List),
   ( get(List?selection, key, Value) ->
     true
   ; Value = @nil ).

size(Dev, Size:size) :->
   get(Dev, slot(list), List),
   send(List, size(Size)).

:- pce_end_class.


:- pce_begin_class(xxul_textarea, label_box).

variable(editor, editor, none).

initialise(Dev, Name:name=[name]) :->
   send_super(Dev, initialise(Name)),
   new(Editor, editor),
   get(new(_, text_item), value_font, Font),
   send(Editor, font(Font)),
   Args = [append(Editor), slot(editor, Editor)],
   send_list(Dev, Args).

clear(Dev) :->
   get(Dev, slot(editor), Editor),
   send(Editor, clear).

selection(Dev, String:string) :<-
   get(Dev, slot(editor), Editor),
   get(Editor?text_buffer, contents, String).

size(Dev, Size:size) :->
   get(Dev, slot(editor), Editor),
   send(Editor, size(Size)).

:- pce_end_class.


/*****************************************************************/


