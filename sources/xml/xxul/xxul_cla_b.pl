

/******************************************************************/
/***                                                            ***/
/***           XXUL:  Classes                                   ***/
/***                                                            ***/
/******************************************************************/



/*** implementation ***********************************************/


:- pce_begin_class(xxul_picture, picture).

scroll_horizontal(Win, Direction:name, Unit:name, Amount:int ) :->
   ( Direction = backwards,
     get(Win?bounding_box, left_side, Bleft),
     get(Win?visible, left_side, Vleft),
     Bleft > Vleft  ->
     true
   ; Direction = forwards,
     get(Win?bounding_box, right_side, Bright),
     get(Win?visible, right_side, Vright),
     Vright > Bright ->
     true
   ; send_super(Win, scroll_horizontal(Direction, Unit, Amount))).

scroll_vertical(Win, Direction:name, Unit:name, Amount:int ) :->
   ( Direction = backwards,
     get(Win?bounding_box, top_side, Btop),
     get(Win?visible, top_side, Vtop),
     Btop > Vtop ->
     true
   ; Direction = forwards,
     get(Win?bounding_box, bottom_side, Bbottom),
     get(Win?visible, bottom_side, Vbottom),
     Vbottom > Bbottom ->
     true
   ; send_super(Win, scroll_vertical(Direction, Unit, Amount))).

:- pce_end_class.


:- pce_begin_class(xxul_spreadsheet, xxul_picture).

variable(columns, number, none).
variable(table, table, none).

initialise(Win, Size:size=[size], Cols:columns=int) :->
   send_super(Win, initialise(size := Size)),
   new(Table, table),
   Args1 = [
      cell_spacing(size(0, 0)),
      rules(groups),
      width(Win?width) ],
   send_list(Table, Args1),
   Columns is max(1, Cols),
   forall( between(1, Columns, Cind),
      ( get(Table, column(Cind, @on), Column),
        send(Column, rubber(rubber(1, 100, 100))),
        send(Column, end_group(@on)) ) ),
   send(?(Table, column, Columns, @off), end_group(@off)),
   Args2 = [
      horizontal_scrollbar(@off),
      layout_manager(Table),
      recogniser(resize_table_slice_gesture(column, left)),
      slot(columns, number(Columns)),
      slot(table, Table) ],
   send_list(Win, Args2).

append(Win, Row:xxul_spreadsheet_row) :->
   get(Win, slot(table), Table),
   get(Table, row_range, tuple(_, High)),
   send(Win, insert_row(Row, High + 1)).

cell(Win, Cind:int, Rind:int, Cell:xxul_spreadsheet_cell) :<-
   get(Win, slot(table), Table),
   get(Table, cell(Cind, Rind), Cell).

clear(Win) :->
   get(Win, row_range, tuple(Low, High)),
   send(Win, delete_rows(Low, High)).

delete_row(Win, Rind:int) :->
   send(Win, delete_rows(Rind, Rind)).

delete_rows(Win, From:int, To:int) :->
   get(Win, slot(table), Table),
   send(Table, delete_rows(From, To, @off)).

insert_row(Win, Row:xxul_spreadsheet_row, Rind:int) :->
   get(Win, slot(columns), Columns),
   get(Win, slot(table), Table),
   send(Columns, equal(Row?size)),
   send(Table, insert_row(Rind, Row)).

resize(Win) :->
   get(Win, slot(table), Table),
   send(Table, width(Win?width)).

row(Win, Rind:int, Row:xxul_spreadsheet_row) :<-
   get(Win, slot(table), Table),
   get(Table, row(Rind, @off), Row).

row_range(Win, Tuple:tuple) :<-
   get(Win, slot(table), Table),
   get(Table, row_range, Tuple).

:- pce_end_class.


:- pce_begin_class(xxul_spreadsheet_cell, table_cell).

initialise(Cell) :->
   send_super(Cell, initialise(new(Item, text_item))),
   send(Item, show_label(@off)),
   Args = [
      background(white),
      cell_padding(size(0, 0)),
      halign(stretch) ],
   send_list(Cell, Args).

background(Cell, Colour:colour) :->
   send(Cell?image?value_text, background(Colour)),
   send(Cell?image, redraw).

colour(Cell, Colour:colour) :->
   send(Cell?image?value_text, colour(Colour)),
   send(Cell?image, redraw).

editable(Cell, Bool:bool) :->
   send(Cell?image, editable(Bool)).

font(Cell, Font:font) :->
   send(Cell?image, value_font(Font)).

message(Cell, Msg:message) :->
   send(Cell?image, message(Msg)).

selection(Cell, String:char_array) :->
   send(Cell?image, selection(String)).

selection(Cell, String:char_array) :<-
   get(Cell?image, selection, String).

:- pce_end_class.


:- pce_begin_class(xxul_spreadsheet_row, table_row).

background(Row, Colour:colour) :->
   send(Row, for_all(message(@arg1, background, Colour))).

draw_rule(Row, Bool:bool) :->
   send(Row, end_group(Bool)).

:- pce_end_class.


/******************************************************************/


