

/******************************************************************/
/***                                                            ***/
/***           XXUL:  Library                                   ***/
/***                                                            ***/
/******************************************************************/



/*** implementation ***********************************************/


/* xxul_create_object(+Class, +Args, +As, -Obj) <-
      */

xxul_create_object(Class, Args, As, Obj) :-
   maplist(xxul_gen_init_term(As), Args, Inits),
   Term =.. [Class|Inits],
   new(Obj, Term).

xxul_gen_init_term(As, Aname:Pname:Type, Pname := Pval) :-
   member(Aname:Aval, As),
   !,
   xxul_aval_to_pval(Aval, Type, Pval).
xxul_gen_init_term(_, _:Pname:_, Pname := @default).


/* xxul_test_object(+Class, +Classlist, +As, -Obj) <-
      */

xxul_test_object(window, As, Obj) :-
   memberchk(reference:Atom, As),
   term_to_atom(Ref, Atom),
   object(Ref),
   get(Ref, class_name, Class),
   xxul_classlist(window, Classlist),
   memberchk(Class, Classlist),
   get(Ref, tile, Tile),
   get(Tile, root, Tile),
   Obj = Ref.

xxul_classlist(window, [browser, picture, view]).


/* xxul_inherit_frame(+Parent, +Child) <-
      */

xxul_inherit_frame(Par, Chd) :-
   get(Par, attribute(xxul_frame), Fr),
   send(Chd, attribute(xxul_frame, Fr)).


/* xxul_save_frame_id(+Frame, +As) <-
      */

xxul_save_frame_id(Fr, As) :-
   member(id:Id, As),
   !,
   send(Fr, add(Id, Fr)).
xxul_save_frame_id(_, _).


/* xxul_save_object_id(+Object, +As) <-
      */

xxul_save_object_id(Obj, As) :-
   member(id:Id, As),
   !,
   get(Obj, attribute(xxul_frame), Fr),
   send(Fr, add(Id, Obj)).
xxul_save_object_id(_ , _).


/* xxul_set_property(+Object, +Triple, +As) <-
      */

xxul_set_property(Obj, Aname:Mname:Type, As) :-
   member(Aname:Aval, As),
   !,
   xxul_set_property(Obj, Aval:Mname:Type).
xxul_set_property(_, _, _).

xxul_set_property(Obj, Aval:Mname:Type) :-
   xxul_aval_to_pval(Aval, Type, Pval),
   Term =.. [Mname, Pval],
   send(Obj, Term).


/* xxul_set_content(+Object, +Double, +Cs) <-
      */

xxul_set_content(Obj, Mname:Type, [Cval]) :-
   !,
   xxul_set_property(Obj, Cval:Mname:Type).
xxul_set_content(_, _, _).


/* xxul_set_message(+Object, +Triple, +As) <-
      */

xxul_set_message(Obj, Aname:Mname:Type, As) :-
   member(Aname:Aval, As),
   !,
   xxul_set_message(Obj, Aval:Mname:Type).
xxul_set_message(_, _, _).

xxul_set_message(Obj, Aval:Mname:message) :-
   get(Obj, attribute(xxul_frame), Fr),
   Term =.. [Mname, message(Fr, apply, Aval)],
   send(Obj, Term).


/* xxul_aval_to_pval(+Aval, +Type, -Pval) <-
      */

xxul_aval_to_pval(Aval, bool, Pval) :-
   !,
   List = [[true, @on], [false, @off], [none, @nil]],
   memberchk([Aval, Pval], List).
xxul_aval_to_pval(Aval, colour, Pval) :-
   !,
   Pval = colour(Aval).
xxul_aval_to_pval(Aval, image, Pval) :-
   !,
   new(F, file(Aval)),
   Pval = image(F).
xxul_aval_to_pval(Aval, image_or_name, Pval) :-
   !,
   ( concat('/', Aval, Bval),
     dislog_variable_get(home, Bval, File),
     exists_file(File) ->
     new(F, file(File)),
     Pval = image(F)
   ; Pval = Aval ).
xxul_aval_to_pval(Aval, int, Pval) :-
   !,
   atom_number(Aval, Pval).
xxul_aval_to_pval(Aval, font, Pval) :-
   !,
   atomic_list_concat([Family, Style, Atom], ' ', Aval),
   atom_number(Atom, Points),
   Pval = font(Family, Style, Points).
xxul_aval_to_pval(Aval, orient, Pval) :-
   !,
   List = [[horizontal, right], [vertical, next_row]],
   memberchk([Aval, Pval], List).
xxul_aval_to_pval(Aval, vector, Pval) :-
   !,
   atom_number(Aval, Num),
   Pval = vector(Num).
xxul_aval_to_pval(Aval, size, Pval) :-
   !,
   get(new(_, size), convert(Aval), Obj),
   object(Obj, Pval).
xxul_aval_to_pval(Aval, string, Pval) :-
   !,
   swritef(Pval, Aval).
xxul_aval_to_pval(Val, _, Val).


/* xxul_get_gap(+Device, -Size) <-
      */

xxul_get_gap(Dev, Size) :-
   get(Dev, class_name, dialog),
   !,
   get(Dev, border, Size).
xxul_get_gap(Dev, Size) :-
   get(Dev, gap, Size).


/* xxul_modify_tile_of_window(+Win, +As) <-
      */

xxul_modify_tile_of_window(Win, As) :-
   get(Win, tile, Tile),
   xxul_set_property(Tile, hstretch:hor_shrink:int, As),
   xxul_set_property(Tile, hstretch:hor_stretch:int, As),
   xxul_set_property(Tile, vstretch:ver_shrink:int, As),
   xxul_set_property(Tile, vstretch:ver_stretch:int, As).

xxul_modify_tile_of_tbox(Win) :-
   get(Win, tile, Tile),
   get(Win, tiles, Chain),
   chain_list(Chain, Tiles),
   xxul_set_tile_parameter_to_min(Tile, hor_shrink, Tiles),
   xxul_set_tile_parameter_to_min(Tile, hor_stretch, Tiles),
   xxul_set_tile_parameter_to_min(Tile, ver_shrink, Tiles),
   xxul_set_tile_parameter_to_min(Tile, ver_stretch, Tiles).

xxul_set_tile_parameter_to_min(Tile, Pname, Tiles) :-
   findall( Val,
     ( member(Obj, Tiles),
       get(Obj, Pname, Val) ),
     Vals ),
   min_list(Vals, Min),
   Term =.. [Pname, Min],
   send(Tile, Term).


/******************************************************************/


