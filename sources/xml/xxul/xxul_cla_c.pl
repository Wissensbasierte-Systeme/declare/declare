

/******************************************************************/
/***                                                            ***/
/***           XXUL:  Classes                                   ***/
/***                                                            ***/
/******************************************************************/



/*** implementation ***********************************************/


:- pce_begin_class(xxul_treelist, xxul_picture).

variable(chaintable, chain_table, none).
variable(font, font, both).
variable(selection, xxul_node*, get).
variable(tree, tree, none).

initialise(Win, Size:size=[size]) :->
   send_super(Win, initialise(size := Size)),
   new(Tree, tree),
   new(Msg1, message(Win, select, @receiver)),
   Args1 = [ direction(list),
      node_handler(click_gesture(left, '', single, Msg1)) ],
   send_list(Tree, Args1),
   new(Msg2, message(Win, unselect)),
   Args2 = [ display(Tree, point(10, 5)),
      recogniser(click_gesture(left, '', single, Msg2)),
      slot(chaintable, new(_, chain_table)),
      slot(font, new(_, font(helvetica, normal, 12))),
      slot(selection, @nil),
      slot(tree, Tree) ],
   send_list(Win, Args2).

append(Win, Node:xxul_node, Par:xxul_node) :->
   send(Win?contains, member(Par)),
   get(Win, slot(chaintable), Table),
   send(Table, append(Node?key, Node)),
   send(Par, son(Node)),
   send(Node, contained_in(Win)).

clear(Win) :->
   get(Win, slot(chaintable), Table),
   get(Win, slot(tree), Tree),
   send(Table, clear),
   send(Tree, root(@nil)),
   send(Win, slot(selection, @nil)).

contains(Win, Chain:chain) :<-
   get(Win, slot(tree), Tree),
   get(Tree, contains, Chain).

delete(Win, Node:xxul_node) :->
   send(Win?contains, member(Node)),
   not(get(Win, root, Node)),
   get(Win, slot(chaintable), Table),
   send(Table, delete(Node?key, Node)),
   send(Node, delete),
   ( get(Win, slot(selection), Node) ->
     send(Win, slot(selection, @nil))
   ; true ).

gap(Win, Gap:int) :->
   get(Win, slot(tree), Tree),
   send(Tree, level_gap(Gap)).

member(Win, Key:name, Chain:chain) :<-
   get(Win, slot(chaintable), Table),
   get(Table, member(Key), Chain).

open_message(Win, Msg:message) :->
   get(Win, slot(tree), Tree),
   new(Gesture, click_gesture(left, '', double, Msg)),
   send(Tree, node_handler(Gesture)).

root(Win, Node:xxul_node) :->
   send(Win, clear),
   get(Win, slot(chaintable), Table),
   get(Win, slot(tree), Tree),
   send(Table, append(Node?key, Node)),
   send(Tree, root(Node)),
   send(Node, contained_in(Win)).

root(Win, Root:xxul_node*) :<-
   get(Win, slot(tree), Tree),
   get(Tree, root, Root).

select(Win, Node:xxul_node) :->
   send(Win?contains, member(Node)),
   send(Win, unselect),
   send(Node, selected(@on)),
   send(Win, slot(selection, Node)).

unselect(Win) :->
   get(Win, selection, Node),
   ( Node = @nil ->
     true
   ; send(Node, selected(@off)),
     send(Win, slot(selection, @nil)) ).

:- pce_end_class.


:- pce_begin_class(xxul_node, node).

variable(bitmap, bitmap, none).
variable(key, name, get).
variable(text, text, none).
variable(treelist, xxul_treelist*, none).

initialise(Node, Key:key=name) :->
   new(Bmp, bitmap),
   new(Text, text(Key)),
   new(Dev, figure),
   send_list(Dev, [border(2), display(Bmp), display(Text)]),
   send_super(Node, initialise(Dev)),
   Args = [ slot(bitmap, Bmp),
      slot(key, Key),
      slot(text, Text),
      slot(treelist, @nil) ],
   send_list(Node, Args).

colour(Node, Colour:colour) :->
   get(Node, slot(text), Text),
   send(Text, colour(Colour)).

contained_in(Node, Win:xxul_treelist) :->
   get(Node, slot(text), Text),
   send(Node, slot(treelist, Win)),
   send(Text, font(Win?font)).

contained_in(Node, Win:xxul_treelist) :<-
   get(Node, slot(treelist), Win).

image(Node, Img:image) :->
   get(Node, slot(bitmap), Bmp),
   get(Node, slot(text), Text),
   send(Bmp, image(Img)),
   get(Bmp, width, Len),
   Xcor is Len + 3,
   send(Text, x(Xcor)).

selected(Node, Bool:bool) :->
   get(Node, slot(text), Text),
   send(Text, selected(Bool)).

string(Node, String:char_array) :->
   get(Node, slot(text), Text),
   send(Text, string(String)).

underline(Node, Bool:bool) :->
   get(Node, slot(text), Text),
   send(Text, underline(Bool)).

:- pce_end_class.


/******************************************************************/


