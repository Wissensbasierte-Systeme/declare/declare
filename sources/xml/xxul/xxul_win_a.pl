

/******************************************************************/
/***                                                            ***/
/***           XXUL:  Window System                             ***/
/***                                                            ***/
/******************************************************************/



/*** implementation ***********************************************/


/* xxul_item_to_browser_as_child(+Triple, -Win, +Par) <-
      */

xxul_item_to_browser_as_child(browser:As:Cs, Win, Par) :-
   xxul_create_object(browser, [size:size:size], As, Win),
   xxul_inherit_frame(Par, Win),
   xxul_save_object_id(Win, As),
   xxul_set_property(Win, font:font:font, As),
   xxul_set_property(Win, tabstop:tab_stops:vector, As),
   xxul_set_message(Win, oncommand:open_message:message, As),
   maplist(xxul_build_browser(Win), Cs),
   xxul_modify_tile_of_window(Win, As).

xxul_build_browser(Win, bi:As:[]) :-
   xxul_item_to_browser_item_as_child(bi:As:[], Item, Win),
   send(Win, append(Item)).

xxul_item_to_browser_item_as_child(bi:As:[], Item, Par) :-
   Args1 = [ bold:bold:bool,
     background:background:colour,
     colour:colour:colour,
     underline:underline:bool ],
   xxul_create_object(style, Args1, As, Style),
   get(Style, object_reference, Name),
   send(Par, style(Name, Style)),
   Args2 = [key:key:name, label:label:string],
   xxul_create_object(dict_item, Args2, As, Item),
   send(Item, style(Name)).


/* xxul_item_to_editor_as_child(+Triple, -Win, +Par) <-
      */

xxul_item_to_editor_as_child(editor:As:[], Win, Par) :-
   xxul_create_object(view, [size:size:size], As, Win),
   xxul_inherit_frame(Par, Win),
   xxul_save_object_id(Win, As),
   xxul_set_property(Win, background:background:colour, As),
   xxul_set_property(Win, editable:editable:bool, As),
   xxul_set_property(Win, font:font:font, As),
   xxul_set_property(Win, colour:colour:colour, As),
   xxul_modify_tile_of_window(Win, As).


/* xxul_item_to_menubar_as_child(+Triple, -Win, +Par) <-
      */

xxul_item_to_menubar_as_child(menubar:[]:Cs, Win, Par) :-
   new(Win, xxul_menubar),
   xxul_inherit_frame(Par, Win),
   maplist(xxul_build_menubar(Win), Cs).

xxul_build_menubar(Win, menu:As:Cs) :-
   xxul_item_to_menu_as_child(menu:As:[], Menu, Win),
   maplist(xxul_build_menu(Menu), Cs),
   send(Win, append(Menu)).

xxul_item_to_menu_as_child(menu:As:[], Menu, Par) :-
   xxul_create_object(popup, [label:name:name], As, Menu),
   xxul_inherit_frame(Par, Menu).

xxul_build_menu(Menu, gap:[]:[]) :-
   !,
   send(Menu, append(gap)).
xxul_build_menu(Menu, mi:As:[]) :-
   !,
   xxul_create_object(menu_item, [label:value:name], As, Item),
   xxul_inherit_frame(Menu, Item),
   xxul_set_message(Item, oncommand:message:message, As),
   send(Menu, append(Item)).
xxul_build_menu(Menu, menu:As:Cs) :-
   xxul_item_to_menu_as_child(menu:As:[], Chd, Menu),
   maplist(xxul_build_menu(Chd), Cs),
   send(Menu, append(Chd)).


/* xxul_item_to_picture_as_child(+Triple, -Win, +Par) <-
      */

xxul_item_to_picture_as_child(picture:As:[], Win, Par) :-
   xxul_create_object(xxul_picture, [size:size:size], As, Win),
   xxul_inherit_frame(Par, Win),
   xxul_save_object_id(Win, As),
   xxul_modify_tile_of_window(Win, As).


/* xxul_item_to_statusbar_as_child(+Triple, -Win, +Par) <-
      */

xxul_item_to_statusbar_as_child(statusbar:As:[], Win, Par) :-
   Args = [colour:colour:colour, width:width:int],
   xxul_create_object(xxul_statusbar, Args, As, Win),
   xxul_inherit_frame(Par, Win),
   xxul_save_object_id(Win, As).


/* xxul_item_to_tbox_as_child(+Triple, -Win, +Par) <-
      */

xxul_item_to_tbox_as_child(tbox:[]:Cs, Win, Par) :-
   new(Win, xxul_tabbed_window),
   xxul_inherit_frame(Par, Win),
   maplist(xxul_build_tbox(Win), Cs),
   xxul_modify_tile_of_tbox(Win).

xxul_build_tbox(Win, browser:As:Cs) :-
   !,
   xxul_item_to_browser_as_child(browser:As:Cs, Chd, Win),
   xxul_append_to_tabbed_window(Win, Chd, As).
xxul_build_tbox(Win, editor:As:[]) :-
   !,
   xxul_item_to_editor_as_child(editor:As:[], Chd, Win),
   xxul_append_to_tabbed_window(Win, Chd, As).
xxul_build_tbox(Win, picture:As:[]) :-
   !,
   xxul_item_to_picture_as_child(picture:As:[], Chd, Win),
   xxul_append_to_tabbed_window(Win, Chd, As).
xxul_build_tbox(Win, spreadsheet:As:Cs) :-
   !,
   xxul_item_to_spreadsheet_as_child(spreadsheet:As:Cs, Chd, Win),
   xxul_append_to_tabbed_window(Win, Chd, As).
xxul_build_tbox(Win, treelist:As:Cs) :-
   !,
   xxul_item_to_treelist_as_child(treelist:As:Cs, Chd, Win),
   xxul_append_to_tabbed_window(Win, Chd, As).
xxul_build_tbox(Win, window:As:[]) :-
   xxul_item_to_window_as_child(window:As:[], Chd, Win),
   xxul_append_to_tabbed_window(Win, Chd, As).

xxul_append_to_tabbed_window(Win, Chd, As) :-
   member(label:Label, As),
   !,
   send(Win, append(Chd, Label)).
xxul_append_to_tabbed_window(Win, Chd, _) :-
   send(Win, append(Chd, '')).


/* xxul_item_to_treelist_as_child(+Triple, -Win, +Par) <-
      */

xxul_item_to_treelist_as_child(treelist:As:Cs, Win, Par) :-
   xxul_create_object(xxul_treelist, [size:size:size], As, Win),
   xxul_inherit_frame(Par, Win),
   xxul_save_object_id(Win, As),
   xxul_set_property(Win, font:font:font, As),
   xxul_set_property(Win, gap:gap:int, As),
   xxul_set_message(Win, oncommand:open_message:message, As),
   maplist(xxul_build_treelist(Win), Cs),
   xxul_modify_tile_of_window(Win, As).

xxul_build_treelist(Win, node:As:Cs) :-
   xxul_item_to_treelist_node(node:As:[], Node),
   send(Win, root(Node)),
   maplist(xxul_build_treelist_node(Node), Cs).

xxul_build_treelist_node(Node, node:As:Cs) :-
   xxul_item_to_treelist_node(node:As:[], Son),
   get(Node, contained_in, Win),
   send(Win, append(Son, Node)),
   maplist(xxul_build_treelist_node(Son), Cs).

xxul_item_to_treelist_node(node:As:[], Node) :-
   xxul_create_object(xxul_node, [key:key:name], As, Node),
   xxul_set_property(Node, collapsed:collapsed:bool, As),
   xxul_set_property(Node, colour:colour:colour, As),
   xxul_set_property(Node, image:image:image, As),
   xxul_set_property(Node, label:string:string, As),
   xxul_set_property(Node, underline:underline:bool, As).


/* xxul_item_to_window_as_child(+Triple, -Win, +Par) <-
      */

xxul_item_to_window_as_child(window:As:[], Win, Par) :-
   xxul_test_object(window, As, Win),
   xxul_inherit_frame(Par, Win),
   xxul_save_object_id(Win, As),
   xxul_modify_tile_of_window(Win, As).


/******************************************************************/


