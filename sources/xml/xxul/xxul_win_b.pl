

/******************************************************************/
/***                                                            ***/
/***           XXUL:  Window System                             ***/
/***                                                            ***/
/******************************************************************/



/*** implementation ***********************************************/


/* xxul_item_to_dialog_as_child(+Triple, -Win, +Par) <-
      */

xxul_item_to_dialog_as_child(dialog:As:Cs, Win, Par) :-
   new(Win, dialog),
   xxul_inherit_frame(Par, Win),
   xxul_set_property(Win, background:background:colour, As),
   xxul_set_property(Win, gap:gap:size, As),
   xxul_set_property(Win, pen:pen:int, As),
   maplist(xxul_build_dialog_group(Win, next_row), Cs),
   xxul_modify_tile_of_window(Win, As).

xxul_build_dialog_group(Dev, Pos, dbox:As:Cs) :-
   !,
   xxul_item_to_dialog_item_as_child(dbox:[]:[], Item, Dev),
   memberchk(orient:Aval, As),
   xxul_aval_to_pval(Aval, orient, Pval),
   maplist(xxul_build_dialog_group(Item, Pval), Cs),
   send(Dev, append(Item, Pos)).
xxul_build_dialog_group(Dev, Pos, T:As:Cs) :-
   member(T, [checkbox, listbox, menubox, radiobox]),
   !,
   xxul_item_to_dialog_item_as_child(T:As:[], Item, Dev),
   maplist(xxul_build_dialog_menu(Item), Cs),
   send(Dev, append(Item, Pos)).
xxul_build_dialog_group(Dev, Pos, T:As:Cs) :-
   xxul_item_to_dialog_item_as_child(T:As:Cs, Item, Dev),
   send(Dev, append(Item, Pos)).

xxul_item_to_dialog_item_as_child(button:As:[], Item, Dev) :-
   !,
   new(Item, button('')),
   xxul_inherit_frame(Dev, Item),
   xxul_set_property(Item, label:label:image_or_name, As),
   xxul_set_message(Item, oncommand:message:message, As).
xxul_item_to_dialog_item_as_child(checkbox:As:[], Item, Dev) :-
   !,
   xxul_create_object(menu, [label:name:name], As, Item),
   xxul_inherit_frame(Dev, Item),
   xxul_save_object_id(Item, As),
   Args = [ kind(marked),
      layout(horizontal),
      multiple_selection(@on) ],
   send_list(Item, Args),
   xxul_set_property(Item, rows:columns:int, As).
xxul_item_to_dialog_item_as_child(dbox:[]:[], Item, Dev) :-
   !,
   new(Item, dialog_group(kind := group)),
   xxul_inherit_frame(Dev, Item),
   xxul_get_gap(Dev, Size),
   Args = [border(size(0, 0)), gap(Size)],
   send_list(Item, Args).
xxul_item_to_dialog_item_as_child(intbox:As:[], Item, Dev) :-
   !,
   Args = [label:name:name, low:low:int, high:high:int],
   xxul_create_object(int_item, Args, As, Item),
   xxul_inherit_frame(Dev, Item),
   xxul_save_object_id(Item, As).
xxul_item_to_dialog_item_as_child(label:As:Cs, Item, Dev) :-
   !,
   xxul_create_object(label, [font:font:font], As, Item),
   xxul_inherit_frame(Dev, Item),
   xxul_save_object_id(Item, As),
   xxul_set_property(Item, width:width:int, As),
   xxul_set_content(Item, selection:string, Cs).
xxul_item_to_dialog_item_as_child(listbox:As:[], Item, Dev) :-
   !,
   xxul_create_object(xxul_listbox, [label:name:name], As, Item),
   xxul_inherit_frame(Dev, Item),
   xxul_save_object_id(Item, As),
   xxul_set_property(Item, size:size:size, As).
xxul_item_to_dialog_item_as_child(menubox:As:[], Item, Dev) :-
   !,
   xxul_create_object(menu, [label:name:name], As, Item),
   xxul_inherit_frame(Dev, Item),
   xxul_save_object_id(Item, As),
   send(Item, kind(cycle)).
xxul_item_to_dialog_item_as_child(radiobox:As:[], Item, Dev) :-
   !,
   xxul_create_object(menu, [label:name:name], As, Item),
   xxul_inherit_frame(Dev, Item),
   xxul_save_object_id(Item, As),
   Args = [kind(marked), layout(horizontal)],
   send_list(Item, Args),
   xxul_set_property(Item, rows:columns:int, As).
xxul_item_to_dialog_item_as_child(space:As:[], Item, _) :-
   !,
   new(Item, dialog_group(kind := group)),
   xxul_set_property(Item, size:size:size, As).
xxul_item_to_dialog_item_as_child(textarea:As:[], Item, Dev) :-
   !,
   xxul_create_object(xxul_textarea, [label:name:name], As, Item),
   xxul_inherit_frame(Dev, Item),
   xxul_save_object_id(Item, As),
   xxul_set_property(Item, size:size:size, As).
xxul_item_to_dialog_item_as_child(textbox:As:[], Item, Dev) :-
   xxul_create_object(text_item, [label:name:name], As, Item),
   xxul_inherit_frame(Dev, Item),
   xxul_save_object_id(Item, As),
   xxul_set_property(Item, width:length:int, As).

xxul_build_dialog_menu(Menu, di:As:[]) :-
   xxul_create_object(menu_item, [label:value:name], As, Item),
   send(Menu, append(Item)).


/* xxul_item_to_spreadsheet_as_child(+Triple, -Win, +Par) <-
      */

xxul_item_to_spreadsheet_as_child(spreadsheet:As:Cs, Win, Par) :-
   Args = [columns:columns:int, size:size:size],
   xxul_create_object(xxul_spreadsheet, Args, As, Win),
   xxul_inherit_frame(Par, Win),
   xxul_save_object_id(Win, As),
   maplist(xxul_build_spreadsheet(Win), Cs),
   xxul_modify_tile_of_window(Win, As).

xxul_build_spreadsheet(Win, row:As:Cs) :-
   xxul_item_to_spreadsheet_row_as_child(row:As:Cs, Row, Win),
   send(Win, append(Row)).

xxul_item_to_spreadsheet_row_as_child(row:As:Cs, Row, Par) :-
   new(Row, xxul_spreadsheet_row),
   xxul_inherit_frame(Par, Row),
   maplist(xxul_build_spreadsheet_row(Row), Cs),
   xxul_set_property(Row, background:background:colour, As),
   xxul_set_property(Row, line:draw_rule:bool, As).

xxul_build_spreadsheet_row(Row, cell:As:Cs) :-
   xxul_item_to_spreadsheet_cell_as_child(cell:As:Cs, Cell, Row),
   send(Row, append(Cell)).

xxul_item_to_spreadsheet_cell_as_child(cell:As:Cs, Cell, Par) :-
   new(Cell, xxul_spreadsheet_cell),
   xxul_inherit_frame(Par, Cell),
   xxul_set_property(Cell, colour:colour:colour, As),
   xxul_set_property(Cell, editable:editable:bool, As),
   xxul_set_property(Cell, font:font:font, As),
   xxul_set_content(Cell, selection:string, Cs),
   xxul_set_message(Cell, oncommand:message:message, As).


/******************************************************************/


