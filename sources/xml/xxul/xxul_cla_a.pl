

/******************************************************************/
/***                                                            ***/
/***           XXUL:  Classes                                   ***/
/***                                                            ***/
/******************************************************************/



/*** implementation ***********************************************/


:- pce_extend_class(name).

label_name(Name, Label:name) :<-
   Label = Name.

:- pce_end_class.


:- pce_begin_class(xxul_frame, frame).

variable(xxul_table, hash_table, none).

initialise(Fr, Label:label=[name]) :->
   send_super(Fr, initialise(Label)),
   send(Fr, slot(xxul_table, new(_, hash_table))).

add(Fr, Id:name, Obj:any) :->
   get(Fr, slot(xxul_table), Table),
   send(Table, append(Id, Obj)).

apply(Fr, Atom:prolog) :->
   term_to_atom(Term, Atom),
   Term =.. [Fun|Ids],
   findall(member(Id), member(Id, Ids), List),
   maplist(get(Fr), List, Objs),
   apply(Fun, Objs).

member(Fr, Id:name, Obj:any) :<-
   get(Fr, slot(xxul_table), Table),
   get(Table, member(Id), Obj).

:- pce_end_class.


:- pce_begin_class(xxul_menubar, dialog).

variable(menubar, menu_bar, none).

initialise(Win) :->
   send_super(Win, initialise),
   new(Mbar, menu_bar),
   send_super(Win, append(Mbar)),
   Args1 = [ border(size(0, 0)),
      pen(0),
      slot(menubar, Mbar) ],
   send_list(Win, Args1),
   Args2 = [ hor_shrink(100),
      hor_stretch(100),
      ver_shrink(0),
      ver_stretch(0) ],
   send_list(Win?tile, Args2).

append(Win, Menu:popup) :->
   get(Win, slot(menubar), Mbar),
   send(Mbar, append(Menu)).

:- pce_end_class.


:- pce_begin_class(xxul_tabbed_window, dialog).

variable(tabstack, tab_stack, none).
variable(tiles, chain, get).

initialise(Win) :->
   send_super(Win, initialise),
   new(Tabs, tab_stack),
   send_super(Win, append(Tabs)),
   Args = [ border(size(0, 0)),
      pen(0),
      slot(tabstack, Tabs),
      slot(tiles, new(_, chain)) ],
   send_list(Win, Args).

append(Win, Obj:window, Name:name) :->
   new(Tab, xxul_window_tab(Obj, Name)),
   get(Win, slot(tabstack), Tabs),
   send(Tabs, append(Tab)),
   send(Win?tiles, append(Obj?tile)),
   send_list(Win, [layout_dialog, resize(Tab)]).

resize(Win, Tab:[xxul_window_tab]) :->
   get(Win, slot(tabstack), Tabs),
   get(Win, area, area(_, _, Width, Height)),
   get(Tabs, graphicals, Chain),
   chain_list(Chain, List),
   findall( Labh,
      ( member(Obj, List),
        get(Obj, label_size, size(_, Labh)) ),
      Labhs ),
   max_list(Labhs, Max),
   Tabh is Height - Max,
   ( Tab = @default ->
     forall( member(Obj, List),
        send(Obj, size(size(Width, Tabh))) )
   ; send(Tab, size(size(Width, Tabh))) ).

:- pce_end_class.


:- pce_begin_class(xxul_window_tab, tab).

variable(window, window, none).

initialise(Tab, Win:window, Name:name) :->
   send_super(Tab, initialise(Name)),
   Args = [border(size(0, 0)), display(Win), slot(window, Win)],
   send_list(Tab, Args).

size(Tab, Size:size) :->
   in_pce_thread(send(Tab, resize_window)),
   send_super(Tab, size(Size)).

resize_window(Tab) :->
   get(Tab, size, size(Width, Height)),
   get(Tab, slot(window), Win),
   ( get(Win, decoration, Decor),
     not(Decor = @nil) ->
     true
   ; Decor = Win ),
   send(Decor, do_set(0, 0, Width, Height)).

:- pce_end_class.


/******************************************************************/


