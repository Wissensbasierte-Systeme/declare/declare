

/******************************************************************/
/***                                                            ***/
/***           XXUL:  Main Module                               ***/
/***                                                            ***/
/******************************************************************/


:- use_module(library(pce)).


:- [ xxul_cla_a,
     xxul_cla_b,
     xxul_cla_c,
     xxul_cla_d,
     xxul_lib,
     xxul_win_a,
     xxul_win_b ].


/*** interface ****************************************************/


/* xxul_item_to_gui(+Triple, -Frame) <-
      */

xxul_item_to_gui(frame:As:Cs, Fr) :-
   xxul_create_object(xxul_frame, [title:label:name], As, Fr),
   send(Fr, attribute(xxul_frame, Fr)),
   xxul_save_frame_id(Fr, As),
   xxul_set_property(Fr, resizable:can_resize:bool, As),
   Options = [frame(Fr), level(first)],
   maplist(xxul_build_tile_hierarchy(Options), Cs, Nodes),
   xxul_process_tile_hierarchy(vbox:Nodes, Win),
   send(Fr, append(Win)).


/* xxul_item_to_object(+Triple, -Obj) <-
      */

xxul_item_to_object(di:As:[], Obj) :-
   !,
   xxul_create_object(menu_item, [label:value:name], As, Obj).
xxul_item_to_object(node:As:[], Obj) :-
   xxul_item_to_treelist_node(node:As:[], Obj).


/* xxul_item_to_object_as_child(+Triple, -Obj, +Par) <-
      */

xxul_item_to_object_as_child(bi:As:[], Obj, Par) :-
   !,
   xxul_item_to_browser_item_as_child(bi:As:[], Obj, Par).
xxul_item_to_object_as_child(row:As:Cs, Obj, Par) :-
   xxul_item_to_spreadsheet_row_as_child(row:As:Cs, Obj, Par).


/*** implementation ***********************************************/


/* xxul_build_tile_hierarchy(+Options, +Triple, -Double) <-
      */

xxul_build_tile_hierarchy(Options, browser:As:Cs, Win:[]) :-
   !,
   memberchk(frame(Fr), Options),
   xxul_item_to_browser_as_child(browser:As:Cs, Win, Fr),
   xxul_set_property(Win, label:label:name, As).
xxul_build_tile_hierarchy(Options, dialog:As:Cs, Win:[]) :-
   !,
   memberchk(frame(Fr), Options),
   xxul_item_to_dialog_as_child(dialog:As:Cs, Win, Fr),
   xxul_set_property(Win, label:label:name, As).
xxul_build_tile_hierarchy(Options, editor:As:[], Win:[]) :-
   !,
   memberchk(frame(Fr), Options),
   xxul_item_to_editor_as_child(editor:As:[], Win, Fr),
   xxul_set_property(Win, label:label:name, As).
xxul_build_tile_hierarchy(Options, hbox:[]:Cs, hbox:Nodes) :-
   !,
   memberchk(frame(Fr), Options),
   maplist(xxul_build_tile_hierarchy([frame(Fr)]), Cs, Nodes).
xxul_build_tile_hierarchy(Options, menubar:[]:Cs, Win:[]) :-
   !,
   memberchk(frame(Fr), Options),
   memberchk(level(first), Options),
   xxul_item_to_menubar_as_child(menubar:[]:Cs, Win, Fr).
xxul_build_tile_hierarchy(Options, picture:As:[], Win:[]) :-
   !,
   memberchk(frame(Fr), Options),
   xxul_item_to_picture_as_child(picture:As:[], Win, Fr),
   xxul_set_property(Win, label:label:name, As).
xxul_build_tile_hierarchy(Options, spreadsheet:As:Cs, Win:[]) :-
   !,
   memberchk(frame(Fr), Options),
   xxul_item_to_spreadsheet_as_child(spreadsheet:As:Cs, Win, Fr),
   xxul_set_property(Win, label:label:name, As).
xxul_build_tile_hierarchy(Options, statusbar:As:[], Win:[]) :-
   !,
   memberchk(frame(Fr), Options),
   memberchk(level(first), Options),
   xxul_item_to_statusbar_as_child(statusbar:As:[], Win, Fr).
xxul_build_tile_hierarchy(Options, tbox:[]:Cs, Win:[]) :-
   !,
   memberchk(frame(Fr), Options),
   xxul_item_to_tbox_as_child(tbox:[]:Cs, Win, Fr).
xxul_build_tile_hierarchy(Options, treelist:As:Cs, Win:[]) :-
   !,
   memberchk(frame(Fr), Options),
   xxul_item_to_treelist_as_child(treelist:As:Cs, Win, Fr),
   xxul_set_property(Win, label:label:name, As).
xxul_build_tile_hierarchy(Options, vbox:[]:Cs, vbox:Nodes) :-
   !,
   memberchk(frame(Fr), Options),
   maplist(xxul_build_tile_hierarchy([frame(Fr)]), Cs, Nodes).
xxul_build_tile_hierarchy(Options, window:As:[], Win:[]) :-
   memberchk(frame(Fr), Options),
   xxul_item_to_window_as_child(window:As:[], Win, Fr),
   xxul_set_property(Win, label:label:name, As).


/* xxul_process_tile_hierarchy(+Double, -Win) <-
      */

xxul_process_tile_hierarchy(hbox:Nodes, Win) :-
   !,
   maplist(xxul_process_tile_hierarchy, Nodes, Wins),
   Wins = [Win|_],
   forall( append(_, [First, Second|_], Wins),
      send(Second, right(First)) ).
xxul_process_tile_hierarchy(vbox:Nodes, Win) :-
   !,
   maplist(xxul_process_tile_hierarchy, Nodes, Wins),
   Wins = [Win|_],
   forall( append(_, [First, Second|_], Wins),
      send(Second, below(First)) ).
xxul_process_tile_hierarchy(Win:[], Win).


/******************************************************************/


