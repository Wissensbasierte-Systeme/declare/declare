

/******************************************************************/
/***                                                            ***/
/***          Field Notation:  Stock Case Study                 ***/
/***                                                            ***/
/******************************************************************/



case_study_file(stock,htm(1),Path) :-
   stock_file(data,'02_05_03/dax_30.htm',Path).
case_study_file(stock,htm(2),Path) :-
   stock_file(data,'02_05_03/amazon.htm',Path).


/*** interface ****************************************************/


/* case_study(html_file_to_fn,N) <-
      */

case_study(html_file_to_fn,N) :-
   case_study_file(stock,htm(N),Path),
   load_structure(Path,Content,
      [ dialect(xml), max_errors(500), space(sgml) ] ),
   concat(Path,'.swi_term',Path_2),
   predicate_to_file( Path_2,
      portray(term,Content) ),
   xml_swi_to_fn(Content,FN_Term),
   concat(Path,'.fn',Path_3),
   predicate_to_file( Path_3,
      writeln(FN_Term) ),
   assert(fn_term(N,FN_Term)).


/* case_study(fn_term_find_paths,Mode) <-
      */

case_study(fn_term_find_paths,[1,FN_Term,Paths]) :-
   findall( A,
      ( L := FN_Term^A,
        member('Allianz',L) ),
      Paths ).
case_study(fn_term_find_paths,[2,FN_Term,Paths]) :-
   findall( AliB,
      ( L := FN_Term^A^li^B,
        member(X,L),
        name_contains_name(X,'Elektronik'),
        flatten_path(A^li^B,AliB) ),
      Paths ).


/* case_study(extract_data_from_fn_term,Mode) <-
      */

case_study(extract_data_from_fn_term,N) :-
%  fn_term(N,O),
   my_fn_term(O),
   case_study(fn_term_find_paths,[N,O,Paths_1]),
   writeln_list(Paths_1),
   !,
   findall( TR,
      ( member(Path_1,Paths_1),
        path_cut_at_end(Path_1,Path_3),
        TR := O^Path_3 ),
      TRs ),
   Hiding = [
%      class:_, 'CLASS':_,
%      rowspan:_, colspan:_,
%      border:_, cellpadding:_, cellspacing:_, width:_,
%      bgcolor:_,
%      'SIZE':_, 'FACE':_, 'COLOR':_,
%      shape:_,
%      'HREF':_,
       input:_:_,
       br:_:_, 'BR':_:_ ],
   maplist( fn_hide_elements(Hiding),
      TRs, TRs_2 ),
/*
   Substitutions = [
      X-('FONT':_:[X]),
      X-('FONT':_:X),
%     X-(b:[X]),
      X-('A':X),
      X-(img:_:[X]),
      ('TD':X)-('TD':_:['A':_:X]),
      ('TD':X)-('TD':_:X),
      ('TH':X)-('TH':_:X) ],
*/
   Substitutions = [
      X-(td:_:[X]),
      X-(a:_:X),
      X-(td:_:[X]),
      X-(img:_:_),
      X-(a:_:X) ],
   maplist( fn_transform_elements(Substitutions),
      TRs_2, TRs_3 ),
%  TRs_3 = TRs_2,
   predicate_to_file(
%     'Stock_Analysis/data/02_05_03/extract.fn',
      'extract.fn',
      writeln_list(TRs_3) ).

path_cut_at_end(Path_1,Path_2) :-
   fn_path_to_list(Path_1,List_1),
   reverse(List_1,[_,_|List_3]),
   reverse(List_3,List_2),
   fn_path_to_list(Path_2,List_2).


/* fn_path_cut_at_end(Cutters, Path_1, Path_2) <-
      */

fn_path_cut_at_end(Cutters, Path_1, Path_2) :-
   boolean_dualization([Cutters], Cs),
   fn_path_to_list(Path_1, List_1),
   reverse(List_1, List_A),
   list_start_after_position(Cs, List_A, List_B),
   reverse(List_B, List_2),
   fn_path_to_list(Path_2, List_2).


/* case_study(get_all_paths,Mode) <-
      */

case_study(get_all_paths,N) :-
   fn_term(N,O),
   findall( A,
      ( _ := O^A^tr
      ; _ := O^A^'TR' ),
      Paths_1 ),
   list_to_ord_set(Paths_1,Paths_1a),
   writeln_list(Paths_1a),
   fn_paths_to_tree(Paths_1a,Trees),
%  tree_simplify_3([[]|Trees],Tree),
   dportray(tree,[[]|Trees]).


/* stock_html_to_path_and_relevant_table(
         Html, Path, Path_2, Table) <-
      */

stock_html_to_path_and_relevant_table(
      Html, Path, Path_2, Table) :-
   ( Table := Html/Path/'TABLE'
   ; Table := Html/Path/'Table'
   ; Table := Html/Path/'table' ),
   Xs := Table/Path_2/content::'*',
   ( member('B�rse / WKN', Xs)
   ; member('Adidas', Xs) ),
   \+ fn_path_contains(Path_2, 'TABLE'),
   \+ fn_path_contains(Path_2, 'Table'),
   \+ fn_path_contains(Path_2, 'Table').


/******************************************************************/


