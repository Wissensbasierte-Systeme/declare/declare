

/******************************************************************/
/***                                                            ***/
/***          FN Case Study: Biology                            ***/
/***                                                            ***/
/******************************************************************/


case_study_file(bio, annot_mp, Path) :-
   home_directory(Home),
   name_append(Home,
      '/research/projects/Bio_DB/annot_mp.html', Path).


/*** interface ****************************************************/


/* case_study(bio_kb, Mode) <-
      */

case_study(bio_kb, 1) :-
   case_study_file(bio, annot_mp, Path_1),
   Options = [ dialect(xml), max_errors(5000) ],
   xml_file_to_fn_term(Path_1, FN_Term, Options),
   name_append(Path_1, '.pretty', Path_2),
   predicate_to_file( Path_2,
      field_notation_to_xml(FN_Term) ).


/******************************************************************/


