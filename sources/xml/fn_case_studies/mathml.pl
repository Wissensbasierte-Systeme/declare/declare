

/******************************************************************/
/***                                                            ***/
/***          Field Notation:  MathML Queries                   ***/
/***                                                            ***/
/******************************************************************/



case_study_file(mathml,beispiele,Path) :-
   dislog_variable(home,DisLog),
   concat(DisLog,'/projects/Kamke/beispiele_2.xml',Path).


/*** interface ****************************************************/


/* case_study(mathml,Mode) <-
      */

case_study(mathml,read_and_pretty) :-
   case_study_file(mathml,beispiele,Path_1),
   Options = [ dialect(xml), max_errors(5000) ],
   xml_file_to_fn_term(Path_1,FN_Term,Options),
   name_append(Path_1,'.pretty',Path_2),
   predicate_to_file( Path_2,
      field_notation_to_xml(FN_Term) ),
   name_append(Path_1,'.fn',Path_3),
   predicate_to_file( Path_3,
      writeln(FN_Term) ).

case_study(mathml,function_symbols) :-
   case_study_file(mathml,beispiele,Path_1),
   Options = [ dialect(xml), max_errors(5000) ],
   xml_file_to_fn_term(Path_1,FN_Term,Options),
   mathml_formula_to_function_symbols(FN_Term,Symbols),
   name_append(Path_1,'.result',Path_2),
   predicate_to_file( Path_2,
      field_notation_to_xml(Symbols) ).
   
case_study(mathml,overview_table) :-
   case_study_file(mathml,beispiele,Path),
   Options = [ dialect(xml), max_errors(5000) ],
   xml_file_to_fn_term(Path,FN_Term,Options),
   set_num(mathml_equation,0),
   findall( [N,Equation],
      ( Equation := FN_Term^m_math^apply,
        _ := Equation^eq,
        get_num(mathml_equation,N) ),
      Equations ),
   maplist_with_status_bar(
      mathml_formula_to_classification,
      Equations, Rows ),
   xpce_display_table(_,_,'ODE Diagnosis',
      ['#','Built-Ins','Variables','Degree','Type','Class'],
      Rows ).
   

/******************************************************************/


