

/******************************************************************/
/***                                                            ***/
/***          Field Notation:  DBWorld                          ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* dbw_index_file_to_emails(Index_File, Email_File) <-
      */

dbw_index_file_to_emails :-
   dbw_index_file_to_emails('browse_dbw.html', dbw_all).

dbw_index_file_to_emails(Index_File, Email_File) :-
   ( file_exists(Email_File)
   ; tell(Email_File), write(''), told ), 
   dread(xml, Index_File, [Xml]),
   forall( Tr := Xml^'BODY'^'TABLE'^'TBODY'^'TR',
      dbw_table_row_file(Tr, Email_File) ).

dbw_table_row_file(Tr, Email_File) :-
   star_line,
   dislog_variable_get(output_path, Path),
   dbw_table_row_to_information(Tr, Info, Url, Web_Page),
   star_line,
   get_url_to_file(Url, File),
   dbw_file_to_name(File, DBWorld_Message),
   dbw_information_to_mail_header(Info, Web_Page, Header),
   concat([Header, '\n\n', DBWorld_Message, '\n\n'], Name),
   concat(Path, 'db_world_tmp_1', Tmp_1),
   concat(Path, 'db_world_tmp_2', Tmp_2),
   predicate_to_file( Tmp_1,
      writeln(Name) ),
   concat(['cat ', Email_File, ' ', Tmp_1, ' > ', Tmp_2,
      '; mv ', Tmp_2, ' ', Email_File], Command),
   us(Command),
   !.


/*** implementation ***********************************************/


dbw_table_row_to_information(Tr, Info, Url, Web_Page) :-
   maplist( dbw_get_item(Tr),
      [1, 2, 3, 5], [Sent, Message_Type, From, Deadline] ),
   name_exchange_sublist([["-", " "]], Sent, Date),
   Subject_ := Tr-nth(4)^'TD',
   Url := Subject_^'A'@'HREF',
   writeln(user, Url),
   Subjects := Subject_^'A'^content::'*',
   concat(Subjects, Subject),
   writeln(user, Subject),
   Web_Page_ := Tr-nth(6)^'TD',
   ( Web_Page := Web_Page_^'A'@'HREF'
   ; Web_Page = '' ),
   writeln(user, Web_Page),
   Info = [Date, From, Subject, Message_Type, Deadline].

dbw_information_to_mail_header(Info, Web_Page, Header) :-
   Info = [Date, From, Subject, Message_Type, Deadline],
   concat([
      'From owner-dbworld@cs.wisc.edu May 01 09:00:00 2006\n',
      'Return-Path: <dbworld-bounces@cs.wisc.edu>\n',
      'Date: Mon, ', Date, ' 09:00:00 +0800 (CST)\n',
      'From: "', From, '" <owner-dbworld@cs.wisc.edu>\n',
      'Subject: [Dbworld] ', Subject, '\n',
      'X-DBWorld-Message-Type: ', Message_Type, '\n',
      'X-DBWorld-Deadline: ', Deadline, '\n',
      'X-DBWorld-Web-Page: ', Web_Page], Header).

dbw_file_to_name(File, Name) :-
   dread(xml, File, [Xml]),
   read_file_to_name(File, Name_2),
   ( fn_item_parse(Xml, 'HTML':_:_),
     _ := Xml^'BODY'^'PRE'^content::'*',
     name_start_after_position(["<PRE>"], Name_2, Name_3),
     name_cut_at_position(["</PRE>"], Name_3, Name)
   ; Name = Name_2 ).

dbw_get_item(Tr, N, Item) :-
   Item_ := Tr-nth(N)^'TD',
   ( [Item] := Item_^content::'*'
   ; Item := '' ),
   writeln(user, Item),
   !.


/******************************************************************/


