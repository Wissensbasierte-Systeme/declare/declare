

/******************************************************************/
/***                                                            ***/
/***          Field Notation:  Party Case Study                 ***/
/***                                                            ***/
/******************************************************************/



case_study_file(party,overview,Path) :-
   dislog_variable(home,DisLog),
   concat(DisLog,'/ortsver/ov_leit.htm',Path).
case_study_file(party,uebersicht,Path) :-
   dislog_variable(home,DisLog),
   concat(DisLog,'/ortsver/ov_uebersicht.xls',Path).


/*** interface ****************************************************/


/* overview_file_to_soffice <-
      */

overview_file_to_soffice :-
   case_study_file(party,overview,Path_1),
   Options = [ dialect(xml), max_errors(5000) ],
   xml_file_to_fn_term(Path_1,FN_Term,Options),
   concat(Path_1,'.pretty',Path_2),
   predicate_to_file( Path_2,
      field_notation_to_xml(FN_Term) ),
   findall( [Url,Title],
      ( X := FN_Term^_@area,
        Url := X^href,
        attributes_to_title(X,Title) ),
      Pairs_1 ),
   list_to_ord_set(Pairs_1,Pairs_2),
   writeln_list(Pairs_2),
   maplist_with_status_bar( load_ortsvereins_file,
      Pairs_2, L ),
   append(L,Rows),
   delete(Rows,[center:_:_,_],Rows_2),
   case_study_file(party,uebersicht,Path_3),
   overview_to_soffice(Rows_2,Path_3).

attributes_to_title(X,Title) :-
   Title := X^title,
   !.
attributes_to_title(_,'').

load_ortsvereins_file(
      [Path_1,Name_1],[['',''],['Ort',Name_2]|Pairs]) :-
   ( name(Name_1,[C1|Cs]),
     C2 is C1 - 32,
     name(Name_2,[C2|Cs])
   ; Name_2 = Name_1 ),
   Options = [ dialect(xml), max_errors(5000) ],
   xml_file_to_fn_term(Path_1,FN_Term,Options),
   findall( tr:X,
      X := FN_Term^_^tr,
      FN_Term_1 ),
   Substitutions = [
      (b:[]:X)-(center:_:X),
      (td:[]:X)-(td:_:[_,font:_:X]),
      (td:[]:X)-(td:_:[font:_:X,_]),
      (font:[]:X)-(font:_:[b:_:X]),
      (td:[]:X)-(td:_:[div:_:X]),
      (td:[]:X)-(td:_:[font:_:X]),
      (td:[]:X)-(td:_:X),
      (td:[]:Y)-(td:_:[b:_:Y]),
      (font:[]:X)-(font:_:X) ],
   fn_transform_elements(Substitutions,
      FN_Term_1, FN_Term_2 ),
   fn_transform_elements(Substitutions,
      FN_Term_2, FN_Term_3 ),
   Substitutions_3 = [
      (b:[]:[])-(img:_:_),
      (b:[]:X)-(b:_:[font:_:X]),
      (td:[]:X)-(td:_:[b:_:X]),
      (td:[]:X)-(td:_:[br:_:X]),
      (td:[]:[Y|X])-(td:_:[Y,br:_:X]),
      (td:[]:[X,Y,Z])-(td:_:[X,font:_:[Y],font:_:[Z]]),
      (td:[]:[X,Y,Z,U])-
         (td:_:[X,font:_:[Y],font:_:[Z],font:_:[],font:_:[U]]),
      (td:[]:[X,Y])-(td:_:[X,a:_:[Y]]),
      (td:[]:[X])-(td:_:[u:_:[b:_:X]]),
      (td:[]:[X,Y])-(td:_:[b:_:[X],Y]),
      (td:[]:X)-(td:_:[font:_:X]) ],
   fn_transform_elements(Substitutions_3,
      FN_Term_3, FN_Term_4 ),
   fn_transform_elements(Substitutions_3,
      FN_Term_4, FN_Term_5 ),
   concat(Path_1,'.pretty',Path_2),
   predicate_to_file( Path_2,
      field_notation_to_xml(FN_Term_5) ),
   findall( [X,Y],
      ( O := FN_Term_5^tr,
        [X] := O-nth(1)^td,
        Ys := O-nth(2)^td,
        length(Ys,N), N < 3,
        maplist( term_to_atom,
           Ys, As ),
        name_append(As,Y) ),
      Pairs ),
   !.


/* overview_to_soffice(Rows,File) <-
      */

overview_to_soffice(Rows,File) :-
   delete(Rows,[center:_:_,_],Rows_2),
   delete(Rows_2,[center:_:_,_],Rows_3),
   predicate_to_file( File,
      checklist(
         write_list_with_separators(
            write_with_blank,writeln_with_blank),
         Rows_3 ) ).

writeln_with_blank(X) :-
   write_with_blank(X),
   nl.

write_with_blank('Ort') :-
   write_with_blank('Ort:').
write_with_blank(X) :-
   term_to_atom(X,A),
   name(A,List_1),
   list_exchange_sublist([["&nbsp;",""],[":",""]],List_1,List_2),
   name(B,List_2),
   term_to_atom(Y,B),
   write(Y),
   write(': ').


/******************************************************************/


