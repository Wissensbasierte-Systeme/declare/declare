

/******************************************************************/
/***                                                            ***/
/***          Field Notation:  MathML Queries                   ***/
/***                                                            ***/
/******************************************************************/


case_study_file(che,che_summary,Path) :-
   dislog_variable(home,DisLog),
   name_append(DisLog,'/CHE/che_summary.xml',Path).


/*** interface ****************************************************/


/* case_study(che,Mode) <-
      */

case_study(che,1) :-
   case_study_file(che,che_summary,Path_1),
   load_structure(Path_1,Content,
      [ dialect(xml), max_errors(500) ] ),
   xml_swi_to_fn(Content,FN_Term),
   name_append(Path_1,'.pretty',Path_2),
   predicate_to_file( Path_2,
      field_notation_to_xml(FN_Term) ),
   che_fn_term_to_paths(FN_Term,Paths),
   writeln(Paths),
   che_fn_term_to_aggregations(FN_Term,Paths,Aggs_List),
   list_to_ord_set(Aggs_List,Aggs),
   writeln_list(Aggs).

che_fn_term_to_paths(FN_Term,Paths) :-
   findall( alle^Geber^Year,
      _ := FN_Term^alle^_^Geber^Year,
      Path_List ),
   list_to_ord_set(Path_List,Paths).

che_fn_term_to_aggregations(FN_Term,Paths,Aggs) :-
   findall( Geber^Year:Agg,
      ( member(Path,Paths),
        Path = alle^Geber^Year,
        fn_path_to_agg(FN_Term,alle^_^Geber^Year,Agg) ),
      Aggs ).
        
fn_path_to_agg(FN_Term,Path,Xs-Agg) :-
   findall( X,
      ( [V] := FN_Term^Path,
        term_to_atom(W,V),
        che_convert_to_euro(Path,W,X) ),
      Xs ),
   add(Xs,Agg),
   !.

che_convert_to_euro(alle^ls_5^_^_,W,X) :-
   X is W * 1.00.
che_convert_to_euro(alle^ls_4^_^_,W,X) :-
   X is W * 1.00.
che_convert_to_euro(alle^ls_3^_^_,W,X) :-
   X is W * 1.95583.
che_convert_to_euro(alle^ls_2^_^_,W,X) :-
   X is W * 1.00.
che_convert_to_euro(alle^ls_1^_^_,W,X) :-
   X is W * 1.95583.


/******************************************************************/


