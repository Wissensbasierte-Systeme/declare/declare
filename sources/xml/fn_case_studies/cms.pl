

/******************************************************************/
/***                                                            ***/
/***       Field Notation:  Directory Search Case Study         ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


/*
test(xml_directory_search,1) :-
   xml_directory_search(
      'GIN', 'projects/XML_Simone_2/AGR',
      Result),
   field_notation_to_xml([Result]).
*/

test(xml:xml_directory_search,2) :-
   xml_directory_search(
      'Allianz', ['projects/XML_Simone', 'index.xml'],
      Result ),
   field_notation_to_xml(Result).


/*** interface ****************************************************/


/* xml_directory_search(Keyword,Directory,Result) <-
      */

xml_directory_search(Keyword,[Directory,File],Result) :-
   name_append([Directory,'/',File],Path),
   Options = [ dialect(xml), max_errors(5000), space(sgml) ],
   xml_file_to_fn_term(Path,[_|FN_Term],Options),
   findall( 'File':F,
      ( F := FN_Term^'DIRECTORY'^'FILE',
        [D] := F^'DESCRIPTION',
        name_contains_name(D,Keyword) ),
      Fs_1 ),
   findall( [Sub_Directory,Sub_File],
      ( D := FN_Term^'DIRECTORY'^'DIRECTORY',
        [Sub_Dir] := D^'NAME',
        [Sub_File] := D^'INDEX',
        name_append([Directory,'/',Sub_Dir],Sub_Directory) ),
      Sub_Directories ),
   maplist( xml_directory_search(Keyword),
      Sub_Directories, Fs_2 ),
   append(Fs_1,Fs_2,Fs),
   Result = 'DIRECTORY':['NAME':Directory|Fs].

xml_directory_search(Keyword,Directory,Result) :-
   name_append(Directory,'/index.xml',Path),
   Options = [ dialect(xml), max_errors(5000), space(sgml) ],
   xml_file_to_fn_term(Path,[_|FN_Term],Options),
   findall( 'File':['NAME':N,'DESCRIPTION':D],
      ( F := FN_Term^'DIRECTORY'^'FILE',
        [D] := F^'DESCRIPTION',
        [Name] := F^'NAME',
        name_append([Directory,'/',Name],N),
        name_contains_name(D,Keyword) ),
      Fs_1 ),
   findall( Sub_Directory,
      ( [Sub_Dir]:= FN_Term^'DIRECTORY'^'FOLDER'^'NAME',
        name_append([Directory,'/',Sub_Dir],Sub_Directory) ),
      Sub_Directories ),
   maplist( xml_directory_search(Keyword),
      Sub_Directories, Fs_2 ),
   append(Fs_2,Fs_3),
   append(Fs_1,Fs_3,Fs),
%  Result = 'FOLDER':['NAME':Directory|Fs].
   Result = Fs.

   
/* xml_file_simone_search(Keyword,File_1,File_2) <-
      */

xml_file_simone_search(Keyword,File_1,File_2) :-
   Options = [ dialect(xml), max_errors(5000), space(sgml) ],
   xml_file_to_fn_term(File_1,[_|FN_Term_1],Options),
   name_append(File_1,'.pretty',File_3),
   predicate_to_file( File_3,
      field_notation_to_xml(FN_Term_1) ),
   findall( 'File':F,
      ( F := FN_Term_1^'DIRECTORY'^'FILE',
        [D] := F^'DESCRIPTION',
        name_contains_name(D,Keyword) ),
      Fs ),
   length(Fs,N),
   bar_line(user),
   write_list(user,
      ['   search for keyword in DESCRIPTION: ''',
       Keyword,'''']), nl(user),
   write_list(user,['   in file: ''',File_1,'''']), nl(user),
   write_list(user,['   ',N,' documents found']), nl(user),
   bar_line(user),
   write_list(user,['---> ',File_2]), nl(user),
   FN_Term_2 = ['DIRECTORY':Fs],
   Header = [
      '<?xml version="1.0" encoding="ISO_8859-1"?>',
      '<!DOCTYPE DIRECTORY SYSTEM "../nc_index.dtd">',
      '<?xml:stylesheet type="text/xsl" href="../nc_index.xsl"?>' ],
   predicate_to_file( File_2,
      ( writeln_list(Header),
        field_notation_to_xml(0,FN_Term_2) ) ).
%  term_to_atom(FN_Term_2,FN_Atom),
%  portray(term,FN_Atom).


/******************************************************************/


