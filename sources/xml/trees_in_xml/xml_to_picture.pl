

/******************************************************************/
/***                                                            ***/
/***            XML:  XML to Picture                            ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(xml_to_picture, 1) :-
   Item = game:[a:'D', b:'S']:[
      set:[id:1, win:'D']:[],
      test,
      set:[id:2, win:'S']:[] ],
   fn_item_to_picture(Item).
test(xml_to_picture, 2) :-
   Item = game:[a:'D', b:'S']:[
      set:[id:1, win:'D']:[],
      test,
      set:[id:2, win:'S']:[] ],
   fn_triple_to_picture(Item).


/*** interface ****************************************************/


/* fn_item_to_picture(Item) <-
      */

fn_item_to_picture(Item) :-
   fn_item_to_vertices_and_edges(Item, _, Vertices, Edges),
   vertices_edges_to_picture_bfs(Vertices, Edges).

fn_item_to_vertices_and_edges(Item, Id, Vertices, Edges) :-
   get_num(fn_item_id, Id),
   ( Items := Item/content::'*' ->
     true
   ; Items = [] ),
   ( Tag := Item/tag::'*' ->
     fn_item_vertex_classify(element, Id-Tag, V)
   ; fn_item_vertex_classify(content, Id-Item, V) ),
   maplist( fn_item_to_vertices_and_edges,
      Items, Ids, Verticess, Edgess ),
   ( foreach(I, Ids), foreach(E, Es1) do
        E = Id-I ),
   findall( I=A:Value,
      ( Value := Item@A,
        get_num(fn_item_id, I) ),
      Triples ),
   ( foreach(I=A:Value, Triples),
     foreach(E, Es2), foreach(W, Ws) do
        E = Id-I,
        term_to_atom(A=Value, Atom),
        fn_item_vertex_classify(finding, I-Atom, W) ),
   append([Es2, Es1|Edgess], Edges),
   append([[V|Ws]|Verticess], Vertices).

fn_item_vertex_classify(
      element, Id-Tag, Gxl_Vertex) :-
   Gxl_Vertex = node:[id:Id]:[
      prmtr:[color:red, symbol:circle, size:medium]:[
         string:[bubble:'']:[Tag] ] ].
fn_item_vertex_classify(
      content, Id-Value, Gxl_Vertex) :-
   Gxl_Vertex = node:[id:Id]:[
      prmtr:[color:green, symbol:circle, size:medium]:[
         string:[bubble:'']:[Value] ] ].
fn_item_vertex_classify(
      finding, Id-Label, Gxl_Vertex) :-
   Gxl_Vertex = node:[id:Id]:[
      prmtr:[color:blue, symbol:triangle, size:medium]:[
         string:[bubble:'']:[Label] ] ].


/* fn_triple_to_picture(Xml, Picture) <-
      */

fn_triple_to_picture(Xml) :-
   fn_triple_to_picture(Xml, _).

fn_triple_to_picture(Xml, Picture) :-
   fn_triple_to_edges(Xml, Edges_2),
   edges_to_vertices(Edges_2, Vertices_2),
   maplist( fn_triple_vertex_classify,
      Vertices_2, Vertices ),
   findall( V-W,
      ( member(V2-W2, Edges_2),
        V := V2@id,
        W := W2@id ),
      Edges ),
   !,
   vertices_edges_to_picture_bfs(Vertices, Edges, Picture).


/* fn_triple_to_edges(Xml, Edges) <-
      */

fn_triple_to_edges(Xml, Edges) :-
   set_num(fn_triple_vertex, 0),
   fn_triple_to_edges('*', Xml, Edges).

fn_triple_to_edges(Father, Xml, Edges) :-
   fn_item_parse(Xml, T:As:Es),
   !,
   get_num(fn_triple_vertex, I1),
   Node_1 = e:[id:I1, value:T]:[],
   findall( Edge,
      ( member(A:V, As),
        get_num(fn_triple_vertex, I2),
        get_num(fn_triple_vertex, I3),
        Node_2 = a:[id:I2, value:A]:[],
        Node_3 = v:[id:I3, value:V]:[],
        ( Edge = Node_1-Node_2
        ; Edge = Node_2-Node_3 ) ),
      Edges_As ),
   maplist( fn_triple_to_edges(Node_1),
      Es, Edges_List ),
   append(Edges_List, Edges_Es),
   ( fn_item_parse(Father, _),
     append([[Father-Node_1], Edges_As, Edges_Es], Edges)
   ; append(Edges_As, Edges_Es, Edges) ).
fn_triple_to_edges(Father, Xml, [Father-Node]) :-
   get_num(fn_triple_vertex, I),
   Node = t:[id:I, value:Xml]:[].


/* fn_triple_vertex_classify(Vertex, Gxl_Vertex) <-
      */

fn_triple_vertex_classify(Vertex, Gxl_Vertex) :-
   fn_item_parse(Vertex, Type:[id:Id, value:Label]:[]),
   Xs = [ e-red-circle-medium, a-blue-triangle-medium,
     v-white-triangle-small, t-white-circle-small ],
   member(Type-Color-Symbol-Size, Xs),
   Gxl_Vertex = node:[id:Id]:[
      prmtr:[color:Color, symbol:Symbol, size:Size]:[
         string:[bubble:'']:[Label] ] ].


/******************************************************************/


