

/******************************************************************/
/***                                                            ***/
/***          XML:  XML Schema Graph to Picture                 ***/
/***                                                            ***/
/******************************************************************/



/*** tests ********************************************************/


test(xml_schema_graph_to_picture, 1) :-
   Item = game:[a:'D', b:'S']:[
      set:[id:1, win:'D']:[],
      test,
      set:[id:2, win:'S']:[] ],
   xml_schema_graph_to_picture(Item).


/*** interface ****************************************************/


/* xml_file_to_schema_picture(File) <-
      */

xml_file_to_schema_picture(File) :-
   dread(xml, File, [Xml]),
   xml_schema_graph_to_picture(Xml).


/* xml_schema_graph_to_picture(Xml, Picture) <-
      */

xml_schema_graph_to_picture(Xml) :-
   xml_schema_graph_to_picture(Xml, _).

xml_schema_graph_to_picture(Xml, Picture) :-
   fn_triple_to_schema_picture_2(Xml, Picture).


/* fn_triple_to_schema_picture_2(+Xml) <-
      */

fn_triple_to_schema_picture_2(Xml) :-
   fn_triple_to_schema_picture_2(Xml, _).

fn_triple_to_schema_picture_2(Xml, Picture) :-
   fn_triple_to_schema_edges_extended(Xml, [Edges_A, Edges_E]),
   maplist( fn_schema_vertex_classify(attribute_2),
      Edges_A, Vertices_A_2 ),
   findall( E-E_A,
      ( member(E-A, Edges_A),
        concat([E, '_', A], E_A) ),
      Edges_A_2 ),
   edges_to_vertices(Edges_E, Vertices_E),
   maplist( fn_schema_vertex_classify(element),
      Vertices_E, Vertices_E_2 ),
   append(Vertices_A_2, Vertices_E_2, Vertices_2),
   list_to_ord_set(Vertices_2, Vertices),
   append(Edges_A_2, Edges_E, Edges),
%  writeln(user, Edges),
%  vertices_edges_to_gxl_graph(Vertices, Edges, Graph),
%  gxl_to_xpce:gxl_graph_to_new_picture(Graph, _).
   vertices_edges_to_picture_bfs(Vertices, Edges, Picture).


/* fn_triple_to_schema_picture(+Xml) <-
      */

fn_triple_to_schema_picture(Xml) :-
   fn_triple_to_schema_edges_extended(Xml, [Edges_A, Edges_E]),
   findall( A,
      member(_-A, Edges_A),
      Vertices_A ),
   maplist( fn_schema_vertex_classify(attribute),
      Vertices_A, Vertices_A_2 ),
   edges_to_vertices(Edges_E, Vertices_E),
   maplist( fn_schema_vertex_classify(element),
      Vertices_E, Vertices_E_2 ),
   append(Vertices_A_2, Vertices_E_2, Vertices_2),
   list_to_ord_set(Vertices_2, Vertices),
   append(Edges_A, Edges_E, Edges),
   vertices_edges_to_gxl_graph(Vertices, Edges, Graph),
   gxl_to_xpce:gxl_graph_to_new_picture(Graph, _).


fn_schema_vertex_classify(attribute, Vertex, Gxl_Vertex) :-
   Gxl_Vertex = node:[id:Vertex]:[
      prmtr:[color:blue, symbol:triangle, size:medium]:[
         string:[bubble:'']:[Vertex] ] ].
fn_schema_vertex_classify(attribute_2, E-A, Gxl_Vertex) :-
   concat([E, '_', A], Vertex),
   Gxl_Vertex = node:[id:Vertex]:[
      prmtr:[color:blue, symbol:triangle, size:medium]:[
         string:[bubble:'']:[A] ] ].
fn_schema_vertex_classify(element, Vertex, Gxl_Vertex) :-
   Gxl_Vertex = node:[id:Vertex]:[
      prmtr:[color:red, symbol:circle, size:medium]:[
         string:[bubble:'']:[Vertex] ] ].


/******************************************************************/


