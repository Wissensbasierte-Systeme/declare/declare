

/******************************************************************/
/***                                                            ***/
/***             XML:  Proof Trees                              ***/
/***                                                            ***/
/******************************************************************/


:- multifile
      xml_proof_tree_simplify/3.


/*** tests ********************************************************/


test(xml_proof_trees, simplify) :-
   Tree_1 = node:[key:'tc(c, c, 3)', rule:r]:[
      node:[key:and]:[
         node:[key:'arc(c, a)', rule:f3]:[],
         node:[key:'tc(a, c, 2)', rule:r]:[
            node:[key:'test', rule:test]:[],
            node:[key:and]:[
               node:[key:'arc(a, b)', rule:f1]:[],
               node:[key:'tc(b, c, 1)', rule:r]:[
                  node:[key:'arc(b, c)', rule:f2]:[] ] ] ] ] ],
   dwrite(xml, Tree_1),
   xml_proof_tree_to_picture(Tree_1),
   xml_proof_tree_simplify(Tree_1, Tree_2),
   dwrite(xml, Tree_2),
   xml_proof_tree_to_picture(Tree_2).


/*** interface ****************************************************/


/* xml_proof_tree_to_picture(Item) <-
      */

xml_proof_tree_to_picture(Item) :-
   xml_proof_tree_to_vertices_and_edges(
      Item, Vertices, Edges),
   vertices_edges_to_picture_bfs(Vertices, Edges).


/* xml_proof_tree_simplify(Tree_1, Tree_2) <-
      */

xml_proof_tree_simplify(Tree_1, Tree_2) :-
   Tree_1 = node:As:[node:[key:and]:Trees_3],
   !,
   maplist( xml_proof_tree_simplify,
      Trees_3, Trees_2 ),
   Tree_2 = node:As:Trees_2.
xml_proof_tree_simplify(Tree_1, Tree_2) :-
   Tree_1 = node:As:Trees_1,
   maplist( xml_proof_tree_simplify,
      Trees_1, Trees_2 ),
   Tree_2 = node:As:Trees_2.


/* xml_proof_tree_simplify(Mode, Tree_1, Tree_2) <-
      */

xml_proof_tree_simplify(d3, Tree_1, Tree_2) :-
   ( Tree_1 = node:[key:Atom, rule:Id]:Trees_3 ->
     Tree_2 = node:[key:Key, rule:Id]:Trees_2
   ; Tree_1 = node:[key:Atom]:Trees_3 ->
     Tree_2 = node:[key:Key]:Trees_2 ),
   !,
   no_singleton_variable(Q),
   ( Atom = d3_finding(indicated_question, Q) -> Key = Q
   ; Atom = d3_finding(D, S) -> concat([D, ' with ', S], Key)
   ; Atom = d3_condition(equal, Q, V) -> Key = V
   ; Key = Atom ),
   maplist( xml_proof_tree_simplify(d3),
      Trees_3, Trees_2 ).
xml_proof_tree_simplify(d3, Tree_1, Tree_2) :-
   Tree_1 = node:As:Trees_1,
   maplist( xml_proof_tree_simplify(d3),
      Trees_1, Trees_2 ),
   Tree_2 = node:As:Trees_2.


/*** implementation ***********************************************/


/* xml_proof_tree_to_vertices_and_edges(
         Item, Vertices, Edges) <-
      */

xml_proof_tree_to_vertices_and_edges(Item, Vertices, Edges) :-
   xml_proof_tree_to_vertices_and_edges(
      Item, _, Vertices, Edges),
   close_num(fn_item_id).

xml_proof_tree_to_vertices_and_edges(
      Item, Id, Vertices, Edges) :-
   get_num(fn_item_id, Id),
   Items := Item/content::'*',
   maplist( xml_proof_tree_to_vertices_and_edges,
      Items, Ids, Verticess, Edgess ),
   ( foreach(I, Ids), foreach(E, Es) do
        E = Id-I ),
   Key := Item@key,
   ( Rule := Item@rule ->
     get_num(fn_item_id, Id2),
     append([[Id-Id2|Es]|Edgess], Edges),
     xml_proof_tree_vertex_classify(key, Id-Key, V1),
     xml_proof_tree_vertex_classify(rule, Id2-Rule, V2),
     append([[V1,V2]|Verticess], Vertices)
   ; member(Key, [and, or, not]) ->
     xml_proof_tree_vertex_classify(junctor, Id-Key, V1),
     append([Es|Edgess], Edges),
     append([[V1]|Verticess], Vertices)
   ; xml_proof_tree_vertex_classify(leaf, Id-Key, V1),
     append([Es|Edgess], Edges),
     append([[V1]|Verticess], Vertices) ).

xml_proof_tree_vertex_classify(key, Id-Key, Gxl_Vertex) :-
   Gxl_Vertex = node:[id:Id]:[
      prmtr:[color:red, symbol:circle, size:medium]:[
         string:[bubble:'']:[Key] ] ].
xml_proof_tree_vertex_classify(leaf, Id-Key, Gxl_Vertex) :-
   Gxl_Vertex = node:[id:Id]:[
      prmtr:[color:orange, symbol:circle, size:medium]:[
         string:[bubble:'']:[Key] ] ].
xml_proof_tree_vertex_classify(junctor, Id-Key, Gxl_Vertex) :-
   Gxl_Vertex = node:[id:Id]:[
      prmtr:[color:white, symbol:rhombus, size:medium]:[
         string:[bubble:'']:[Key] ] ].
xml_proof_tree_vertex_classify(rule, Id-Rule, Gxl_Vertex) :-
   Gxl_Vertex = node:[id:Id]:[
      prmtr:[color:blue, symbol:box, size:medium]:[
         string:[bubble:'']:[Rule] ] ].


/******************************************************************/


