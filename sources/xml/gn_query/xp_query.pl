

/******************************************************************/
/***                                                            ***/
/***           FnQuery:  XPath Access to XML                    ***/
/***                                                            ***/
/******************************************************************/



/*** implementation ***********************************************/


/* xp_apply(U,P,X) <-
      */

xp_apply(U, (Op, Path), X) :-
   xp_path_split((Op,Path), Paths),
   xp_apply_iterate(U, Paths, X).


/* xp_apply_iterate(U,[P|Ps],X) <-
      */

xp_apply_iterate(U, [H|T], X) :-
   xp_get(U, H, Es),
   xp_apply_iterate(Es, T, X).
xp_apply_iterate(U, [], U).


/* xp_get(U, (/, L::A), Es) <-
      the first predicate is for unconditioned access,
      the second is for access with with conditions. */

xp_get(U, (/, L::A), Es) :-
   A =.. [Sel],
   gn_get(L, U, Es, Sel).
xp_get(U, (/, L::A), Es) :-
   A =.. [::, Sel, Conditions],
   nonvar(Conditions),
   gn_get(L, U, Es, Sel),
   xp_check_conditions(Es, Conditions).


/* xp_check_conditions(X, Conditions) <-
      checks if solution X fullfills conditions Conditions. */

xp_check_conditions(X, Conditions) :-
   checklist( xp_check_condition(X),
      Conditions ).

xp_check_condition(X, Condition) :-
   Condition =.. [Op, Path, Value],
   member(Op, [<, =<, =, ==, >=, >, \=]),
   !,
   xp_path_split(Path, List),
   xp_apply_iterate(X, List, Y),
   Goal =.. [Op, Y, Value],
   call(Goal).
xp_check_condition(Item, Cond) :-
   xp_path_split(Cond, List),
   xp_apply_iterate(Item, List, _).


/* xp_path_split(Path, Paths) <-
      */

xp_path_split((Op,Path), Paths) :-
   xp_path_split((Op, Path), [], Paths_Rev),
   reverse(Paths_Rev, Paths).
xp_path_split(Path, Paths) :-
   Path =.. [Op, Path1],
   Paths = [(Op, Path1)].
xp_path_split(Path, Paths) :-
   Path =.. [Op, Begin, Rest],
   Begin =.. [Op1, Path1],
   xp_path_split((Op,Rest), [(Op1, Path1)], Paths_Rev),
   reverse(Paths_Rev, Paths).

xp_path_split((Op, Path), Paths_Acc, Paths) :-
   Path =.. [/, Begin, Rest],
   Paths_Acc_1 = [(Op, Begin)|Paths_Acc],
   xp_path_split((/, Rest), Paths_Acc_1, Paths).
xp_path_split((Op,Path), Paths_Acc, Paths) :-
   Path =.. [::, _, _],
   Paths = [(Op, Path)|Paths_Acc].


/*** tests ********************************************************/


test(gn_query, 3) :-
   gn_remove,
   dislog_variable_set(fn_mode, fn),
   fn_to_database([
      apply:[attr:value]:[
         ci:[type:fn, test:yes]:[y],
         ci:[]:[x]] ]),
   '&1' := '&0'/child::apply,
   value := '&0'/child::apply/attribute::attr,
   '&2' := '&0'/child::apply/child::ci,
   fn := '&0'/child::apply/child::ci/attribute::type,
   yes := '&0'/child::apply/child::ci/attribute::test,
   '&3' := '&0'/child::apply/child::ci,
   gn_remove,
   dislog_variable_set(fn_mode, fn).

test(gn_query, 4) :-
   gn_remove,
   dislog_variable_set(fn_mode, fn),
   fn_to_database([
      apply:[attr:value]:[
         ci:[type:fn, test:3]:[y],
         ci:[]:[x]] ]),
   '&1' := '&0'/child::apply::[/attribute::attr],
   '&1' := '&0'/child::apply::[/attribute::attr='value'],
   '&1' := '&0'/child::apply::[/attribute::attr\='val'],
   '&1' := '&0'/child::apply::[/child::ci],
   '&1' := '&0'/child::apply::[/child::ci/attribute::type],
   '&1' := '&0'/child::apply::[/child::ci/attribute::type='fn'],
   '&1' := '&0'/child::apply::[/child::ci/attribute::test],
   '&1' := '&0'/child::apply::[/child::ci/attribute::test=3],
   '&1' := '&0'/child::apply::[/child::ci/attribute::test=<3],
   '&1' := '&0'/child::apply::[/child::ci/attribute::test>=3],
   gn_remove,
   dislog_variable_set(fn_mode, fn).


/******************************************************************/


