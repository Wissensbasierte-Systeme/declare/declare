

/******************************************************************/
/***                                                            ***/
/***        Field Notation:  Gn to Fn                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gn_to_fn(Gn, Fn) <-
      */

gn_to_fn(Gn) :-
   gn_to_fn(Gn, Fn),
   writeq(Fn),
   nl.
%  dwrite(xml, Fn).

gn_to_fn(Gn, Fn) :-
   reference(_, Tag, Gn, _),
   gn_to_fn(Gn, Tag, Fn).

gn_to_fn(Gn, Tag, Tag:As:Es) :-
   findall( A:V,
      attribute(Gn, A, V),
      As ),
   findall( N-Fn_2,
      ( reference(Gn, Tag_2, Gn_2, N),
        gn_to_fn(Gn_2, Tag_2, Fn_2) ),
      Fns_2 ),
   findall( N-V,
      value(Gn, V, N),
      Values ),
   append(Fns_2, Values, Es_2),
   sort(Es_2, Es_3),
   pair_lists_2(_, Es, Es_3).


/******************************************************************/


