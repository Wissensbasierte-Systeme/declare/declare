

/******************************************************************/
/***                                                            ***/
/***          GN Notation vs FN Notation:  Tests                ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous
      d3_knowledge_base_to_question_edges_test/3,
      d3_knowledge_base_to_rule_edges_test/3,
      d3_knowledge_base_to_neighbours_of_question_test/3.


/*** interface ****************************************************/


/* d3_test_series(Predicate, Mode, Time, CV) <- 
      executes a test series for the given predicate and option,
      returns the average time and the coeeficient of 
      variation of the measures */

d3_test_series(Results) :-
   d3_knowledge_base_read(KB),
   gn_remove,
   fn_to_database([KB]),
   !,
   findall( Result,
      ( d3_test_series(Predicate, Mode, Time, _),
        Result =
           d3_test:[
              predicate:Predicate, mode:Mode, time:Time]:[] ),
      Results ),
   dwrite(xml, d3_test_series:[]:Results).

d3_test_series(Predicate, Mode, Time, CV) :-
   member(Predicate, [
      d3_knowledge_base_to_question_edges_test,
      d3_knowledge_base_to_rule_edges_test,
      d3_knowledge_base_to_neighbours_of_question_test ]),
   member(Mode, [fn, gn_opt, gn, xp]),
   d3_test_series_sub(Predicate, Mode, Time, CV).

d3_test_series_sub(Predicate, Mode, Time, CV) :-
%  call(Predicate, Mode, T1, _),
%  call(Predicate, Mode, T2, _),
%  call(Predicate, Mode, T3, _),
%  call(Predicate, Mode, T4, _),
%  call(Predicate, Mode, T5, _),
%  statistical_values([T1, T2, T3, T4, T5], Time, _, _, CV),
   call(Predicate, Mode, Time, _),
   write_list(user, [
      '\n', Predicate, ', ', Mode, ' --> ', Time, '\n']),
   CV = '-',
   !.
 

/*** implementation ***********************************************/


/* d3_knowledge_base_to_question_edges_test(
         Mode, Time, Edges) <-
      */


d3_knowledge_base_to_question_edges_test(
      fn, Time, Edges) :-
   dislog_variable_set(fn_mode, fn),
   d3_knowledge_base_read(KB),
   measure( d3_knowledge_base_to_question_edges(KB, Edges),
      Time ).


d3_knowledge_base_to_question_edges_test(
      xp, Time, Edges) :-
   dislog_variable_set(fn_mode, xp),
   measure( d3_knowledge_base_to_question_edges_xp('&0', Edges),
      Time ).

d3_knowledge_base_to_question_edges_xp(KB, Edges) :-
   findall( Q1-Q2,
      ( QContainer := KB/descendant::'QContainer',
        Q1 := QContainer/attribute::'ID',
        Q2 := QContainer/child::'Children'
           /child::'Child'/attribute::'ID'),
      Edges_2 ),
   list_to_ord_set(Edges_2, Edges_3),
   reachable_edges('Qcl600', Edges_3, Edges).


d3_knowledge_base_to_question_edges_test(
      gn_opt, Time, Edges) :-
   dislog_variable_set(fn_mode, gn),
   measure( d3_knowledge_base_to_question_edges_opt(Edges),
      Time ).

d3_knowledge_base_to_question_edges_opt(Edges) :-
   findall( Q1-Q2,
      ( reference(_, 'QContainer', QContainer, _),
        Q1 := QContainer@'ID',
%       attribute(QContainer, 'ID', Q1),
        Children := QContainer^'Children',
%       reference(QContainer, 'Children', Children, _),
        Child := Children^'Child',
%       reference(Children, 'Child', Child, _),
        Q2 := Child@'ID' ),
%       attribute(Child, 'ID', Q2)  ),
      Edges_2 ),
   list_to_ord_set(Edges_2, Edges_3),
   reachable_edges('Qcl600', Edges_3, Edges).


d3_knowledge_base_to_question_edges_test(
      gn, Time, Edges) :-
   dislog_variable_set(fn_mode, gn),
   measure( d3_knowledge_base_to_question_edges('&0', Edges),
      Time ).


/* d3_knowledge_base_to_rule_edges_test(Mode, Time, Edges) <-
      */


d3_knowledge_base_to_rule_edges_test(
      fn, Time, Edges) :-
   dislog_variable_set(fn_mode, fn),
   d3_knowledge_base_read(KB),
   measure(
      ( d3_knowledge_base_to_rule_edges(KB, 'P181', Edges)
      ; Edges is 1 ),
      Time ).


d3_knowledge_base_to_rule_edges_test(
      gn, Time, Edges) :-
   dislog_variable_set(fn_mode, gn),
   measure(
      ( d3_knowledge_base_to_rule_edges('&1', 'P181', Edges)
      ; Edges is 1 ),
      Time ).


d3_knowledge_base_to_rule_edges_test(
      xp, Time, Edges) :-
   dislog_variable_set(fn_mode, xp),
   measure(
      ( d3_knowledge_base_to_rule_edges_xp('&1', Edges)
      ; Edges is 1 ),
      Time ).

d3_knowledge_base_to_rule_edges_xp(KB, Edges) :-
   d3_knowledge_base_to_rule_edges_xp(KB, 'P181', Edges).

d3_knowledge_base_to_rule_edges_xp(KB, Diagnosis, Edges) :-
   findall( Edge,
      ( d3_knowledge_base_to_rule_edge_xp(KB, E),
        edge_with_label_split(E, Edge) ),
      Edges_2 ),
   reaching_edges(Diagnosis, Edges_2, Edges).

d3_knowledge_base_to_rule_edge_xp(KB, Cid-Rid-Aid) :-
   Rule :=KB/child::'KnowledgeSlices'/child::'KnowledgeSlice',
   Rid := Rule/attribute::'ID',
   'RuleComplex' := Rule/attribute::type,
   Cid := Rule/descendant::'Condition'/attribute::'ID',
   ( Aid := Rule/child::'Action'
        /child::'Diagnosis'/attribute::'ID'
   ; Aid := Rule/child::'Action'
        /child::'Question'/attribute::'ID' ).


/* d3_knowledge_base_to_neighbours_of_question_test(
         Mode, Time, Edges) <-
      */


d3_knowledge_base_to_neighbours_of_question_test(
      fn, Time, Edges) :-
   dislog_variable_set(fn_mode, fn),
   d3_knowledge_base_read(KB),
   measure( d3_knowledge_base_to_neighbours_of_question(KB, Edges) ,
      Time ).


d3_knowledge_base_to_neighbours_of_question_test(
      gn, Time, Edges) :-
   dislog_variable_set(fn_mode, gn),
   measure(
      ( d3_knowledge_base_to_neighbours_of_question('&1', Edges)
      ; Edges = 1 ),
      Time ).


d3_knowledge_base_to_neighbours_of_question_test(
      xp, Time, Edges) :-
   dislog_variable_set(fn_mode, xp),
   measure(
      ( d3_knowledge_base_to_neighbours_of_question_xp('&1', Edges)
      ; Edges = 1 ),
      Time ).

d3_knowledge_base_to_neighbours_of_question_xp(KB, Edges) :-
   findall( Edge,
      ( d3_knowledge_base_to_neighbour_of_question_xp(KB, E),
        edge_is_incident_with_node(E, 'Msi250'),
        edge_with_label_split(E, Edge) ),
      Edges ).

d3_knowledge_base_to_neighbour_of_question_xp(KB, Cid-Rid-Aid) :-
   Rule :=KB/child::'KnowledgeSlices'/child::'KnowledgeSlice',
   Rid := Rule/attribute::'ID',
   'RuleComplex' := Rule/attribute::type,
   Cid := Rule/descendant::'Condition'/attribute::'ID',
   ( Aid := Rule/child::'Action'/child::'Question'/attribute::'ID'
   ; Aid := Rule/child::'Action'/descendant::_/attribute::'ID' ).



/* d3_knowledge_base_extract_n_rules_to_file(N) <-
      creates a XML file which contains the n first rules */

d3_knowledge_base_extract_n_rules_to_file(N) :-
   d3_knowledge_base_extract_n_rules(N, Rules),
   dislog_variable_get(output_path, Results),
   concat_atom([Results, 'SonoConsult_', N, '.xml'], File),
   fn_term_to_xml_file(
      'KnowledgeBase':[]:[
         'KnowledgeSlices':[]:
         Rules ],
      File).


/* d3_knowledge_base_extract_n_rules_to_prolog_gn(N) <-
      writes the XML database representation
      for the first N rules to the database */

d3_knowledge_base_extract_n_rules_to_prolog_gn(N) :-
   d3_knowledge_base_extract_n_rules(N, Rules),
   gn_remove,
   fn_to_database([
      'KnowledgeBase':[]:[
         'KnowledgeSlices':[]:
         Rules] ]).


/* d3_knowledge_base_extract_n_rules(N, Rules) <-
      extracts the first n rules
      from the FN representation of SonoConsult.xml */

d3_knowledge_base_extract_n_rules(N, Rules) :-
   dislog_variable_set(fn_mode, fn),
   N < 8025,
   d3_knowledge_base_read(KB),
   findall( R,
      R := KB^'KnowledgeSlices'^'KnowledgeSlice',
      Rs ),
   first_n_elements(N, Rs, Rules).


/******************************************************************/


