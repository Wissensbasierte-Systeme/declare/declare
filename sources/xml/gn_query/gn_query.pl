

/******************************************************************/
/***                                                            ***/
/***          XML Graph Notation:  Queries                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* gn_item_parse(Object, T:As:C) <-
       */

gn_item_parse(Object, T:As:C) :-
   reference(_, T, Object, _),
   findall( Type:Value,
      attribute(Object, Type, Value),
      As ),
   findall( reference(Object, T1, D, Z),
      reference(Object, T1, D, Z),
      C1 ),
   findall( value(Object, V, Z1),
      value(Object, V, Z1),
      C2 ),
   append(C1, C2, C).


/* gn_path_to_tag(Tag, Path) <-
      finds the path to the given Tag,
      uses gn_path_to_source */

gn_path_to_tag(Tag, Path) :-
   reference(Start, Tag, _, _),
   gn_path_to_source(Start, '&0', [Tag], Path).


/* gn_path_to_value(Value, Path) <-
      finds the path to the given Value,
      uses gn_path_to_source */

gn_path_to_value(Value, Path) :-
   value(Start, Value, _),
   reference(New_Start, Tag, Start, _),
   gn_path_to_source(New_Start, '&0', [Tag], Path).


/* gn_path_to_attribute_type(Type, Path) <-
      finds the path to the given attribute Type,
      uses gn_path_to_source */

gn_path_to_attribute_type(Type, Path) :-
   attribute(Start, Type, _),
   reference(New_Start, Tag, Start, _),
   gn_path_to_source(New_Start, '&0', [Tag], Path).


/* gn_path_to_attribute_value(Value, Path) <-
      finds the path to the given tag,
      uses gn_path_to_source */

gn_path_to_attribute_value(Value, Path) :-
   attribute(Start, _, Value),
   reference(New_Start, Tag, Start, _),
   gn_path_to_source(New_Start, '&0', [Tag], Path).


/* gn_childs(Object, Childs) <-
      finds for a given object all childs
      in the XML tree */

gn_get_collection(childs, Object, Childs) :-
   findall( Child-Tag,
      gn_get(child, Object, Child, Tag),
      Childs ).


/* gn_descendants(Object, Descendants) <-
      finds for a given object all descendants
      in the XML tree */

gn_get_collection(descendants, Object, Descendants) :-
   findall( Descendant-Tag,
      gn_get(descendant, Object, Descendant, Tag),
      Descendants ).


/* gn_parents(Object, Parents) <-
      finds for a given object the parents
      in the XML tree */

gn_get_collection(parents, Object, [Parents-Tag]) :-
   reference(Parents, Tag, Object, _).


/* gn_ancestors(Object, Ancestors) <-
      finds for a given object all ancestors
      in the XML tree */

gn_get_collection(ancestors, Object, Ancestors) :-
   findall( Ancestor-Tag,
      gn_get(ancestor, Object, Ancestor, Tag),
      Ancestors ).


/* gn_following_siblings(Object, Following_Siblings) <-
      finds for a given object all following siblings
      in the XML tree */

gn_get_collection(following_siblings, Object, Following_Siblings) :-
   findall( Following_Sibling-Tag,
      gn_get(following_sibling, Object, Following_Sibling, Tag),
      Following_Siblings ).


/* gn_preceding_siblings(Object, Preceding_Siblings) <-
      finds for a given object all preceding siblings
      in the XML tree */

gn_get_collection(preceding_siblings, Object, Preceding_Siblings) :-
   findall( Preceding_Sibling-Tag,
      gn_get(preceding_sibling, Object, Preceding_Sibling, Tag),
      Preceding_Siblings ).


/* gn_followings(Object, Followings) <-
      finds for a given object all preceding objects
      in the XML tree */

gn_get_collection(followings, Object, Followings) :-
   findall( Following-Tag,
      gn_get(following, Object, Following, Tag),
      Followings).


/* gn_precedings(Object, Precedings) <-
      finds for a given object all preceding objects
      in the XML tree */

gn_get_collection(precedings, Object, Precedings) :-
   findall( Preceding-Tag,
      gn_get(preceding, Object, Preceding, Tag),
      Precedings ).


/* gn_selfs(Object, [Object]) <-
      finds for a given object the object
      in the XML tree */

gn_get_collection(selfs, Object, Objects) :-
   findall( Self-Tag,
      gn_get(self, Object, Self, Tag),
      Objects ).


/* gn_descendant_or_selfs(Object, Des_or_Selfs) <-
      finds for a given object all descendants
      or the object itself in the XML tree */

gn_get_collection(descendant_or_selfs, Object, Des_or_Selfs) :-
   findall( Des_or_Self-Tag,
      gn_get(descendant_or_self, Object, Des_or_Self, Tag),
      Des_or_Selfs ).


/* gn_ancestor_or_selfs(Object, Anc_or_Selfs) <-
      finds for a given object all ancestors
      or the object itself in the XML tree */

gn_get_collection(ancestor_or_selfs, Object, Anc_or_Selfs) :-
   findall( Anc_or_Self-Tag,
      gn_get(ancestor_or_self, Object, Anc_or_Self, Tag),
      Anc_or_Selfs ).


/*** implementation ***********************************************/


/* gn_get(child, Object, Child, Tag) <-
      finds for a given object a child object and its tag
      in the XML tree */

gn_get(child, Object, Child, Tag) :-
   reference(Object, Tag, Child, _).


/* gn_get(attribute, Object, Value, Type) <-
      finds for a given object the value
      of the attribute type */

gn_get(attribute, Object, Value, Type) :-
   attribute(Object, Type, Value).


/* gn_get(parent, Object, Parent, Tag) <-
      finds for a given object the parent and its tag
      in the XML tree */

gn_get(parent, Object, Parent, Tag) :-
   reference(Parent, _, Object, _),
   reference(_, Tag, Parent, _).
gn_get(parent, Object, Parent, Tag) :-
   reference(Parent, _, Object, _),
   Parent = '&0',
   Tag = 'XML Document Root'.


/* gn_get(ancestor, Object, Ancestor, Tag) <-
      finds for a given object an ancestor and its tag
      in the XML tree */

gn_get(ancestor, Object, Ancestor, Tag) :-
   path_between_objects(Ancestor, Object, _),
   reference(_, Tag, Ancestor, _).
gn_get(ancestor, Object, Ancestor, Tag) :-
   path_between_objects(Ancestor, Object, _),
   Ancestor = '&0',
   Tag = 'XML Document Root'.


/* gn_get(following_sibling, Object, Following_Sibling, Tag) <-
      finds for a given object a following sibling
      and its tag in the XML tree */

gn_get(following_sibling, Object, Following_Sibling, Tag) :-
   reference(Parent, _, Object, _),
   reference(Parent, Tag, Following_Sibling, _),
   gn_object_number_extract(Object, Number),
   gn_object_number_extract(Following_Sibling, Sibling_Number),
   Sibling_Number > Number.


/* gn_get(preceding_sibling, Object, Preceding_Sibling, Tag) <-
      finds for a given object a preceding sibling
      in the XML tree and returns its tag */

gn_get(preceding_sibling, Object, Preceding_Sibling, Tag) :-
   reference(Parent, _, Object, _),
   reference(Parent, Tag, Preceding_Sibling, _),
   gn_object_number_extract(Object, Number),
   gn_object_number_extract(Preceding_Sibling, Sibling_Number),
   Sibling_Number < Number.


/* gn_get(following, Object, Following, Tag) <-
      finds for a given object a preceding object
      in the XML tree and returns its tag */

gn_get(following, Object, Following, Tag) :-
   reference(_, Tag, Following, _),
   gn_object_number_extract(Object, Number),
   gn_object_number_extract(Following, Following_Number),
   Following_Number > Number,
   not(gn_get(descendant, Object, Following, Tag)).


/* gn_get(preceding, Object, Preceding, Tag) <-
      finds for a given object a preceding objects
      in the XML tree and returns its tag */

gn_get(preceding, Object, Preceding, Tag) :-
   reference(_, Tag, Preceding, _),
   gn_object_number_extract(Object, Number),
   gn_object_number_extract(Preceding, Preceding_Number),
   Preceding_Number < Number,
   not(gn_get(ancestor, Object, Preceding, Tag)).


/* gn_self(Object, Object, Tag) <-
      finds for a given object the object in the XML tree
      and returns its tag */

gn_get(self, Object, Object, Tag) :-
   reference(_, Tag, Object, _).
gn_get(self, '&0', '&0', 'XML Document Root').


/* gn_descendant_or_self(Object, Descendant_or_Self, Tag) <-
      finds for a given object a descendant or the object itself
      and its tag in the XML tree */

gn_get(descendant_or_self, Object, Descendant_or_Self, Tag) :-
   gn_get(self, Object, Descendant_or_Self, Tag).
gn_get(descendant_or_self, Object, Descendant_or_Self, Tag) :-
   gn_get(descendant, Object, Descendant_or_Self, Tag).


/* gn_ancestor_or_self(Object, Ancestor_or_Self, Tag) <-
      finds for a given object an ancestor or the object itself
      and its tag in the XML tree */

gn_get(ancestor_or_self, Object, Ancestor_or_Self, Tag) :-
   gn_get(self, Object, Ancestor_or_Self, Tag).
gn_get(ancestor_or_self, Object, Ancestor_or_Self, Tag) :-
   gn_get(ancestor, Object, Ancestor_or_Self, Tag).


/* gn_get(descendant, Object, Descendant, Tag) <-
      finds for a given object a descendant and its tag */

gn_get(descendant, Object, Descendant, Tag) :-
   Object = '&0',
   !,
   reference(_, Tag, Descendant, _).
gn_get(descendant, Object, Descendant, Tag) :-
   path_between_objects(Object, Descendant, Tag).


/* gn_path_to_source(Start, '&0', _, Path) <-
      finds to path from the given Start to the source &0 */

gn_path_to_source('&0', '&0', Path, Path).
gn_path_to_source(Start, '&0', Tags, Path) :-
   reference(New_Start, Tag, Start, _),
   gn_path_to_source(New_Start, '&0', [Tag|Tags], Path).


/* path_between_objects(O1, O2, Destination_Tag) <-
      proves the existence of a path from O1 to O2 */

path_between_objects(O1, O2, Destination_Tag) :-
   reference(O1, Destination_Tag, O2, _).
path_between_objects(O1, O2, Destination_Tag) :-
   reference(O1, _, O3, _),
   path_between_objects(O3, O2, Destination_Tag).


/* gn_object_number_extract(Object, Number) <-
      extracts the nunber part of a gn object */

gn_object_number_extract(Object, Number) :-
   atom_length(Object, Length),
   Sub_Length is Length-1,
   sub_atom(Object, 1, Sub_Length , _, Sub),
   atom_codes(Sub, Sub_Codes),
   number_codes(Number, Sub_Codes).


/* gn_objects_sort(Unsorted_Objects, Sorted_Objects) <-
      sorts a list of gn objects */

gn_objects_sort(Unsorted_Objects, Sorted_Objects) :-
   maplist( gn_object_number_extract, Unsorted_Objects,
      Unsorted_Numbers ),
   sort(Unsorted_Numbers, Sorted_Numbers),
   maplist( number_to_gn_object, Sorted_Numbers, Sorted_Objects).

number_to_gn_object(Number, Object) :-
   name('&', Name1),
   number_codes(Number, Name2),
   append(Name1, Name2, Name),
   name(Object, Name).


/******************************************************************/


