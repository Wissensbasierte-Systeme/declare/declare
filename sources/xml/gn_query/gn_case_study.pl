

/******************************************************************/
/***                                                            ***/
/***         FnQuery:  Case Study Cocktail Checker              ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* cocktail_checker <-
      Starts the graphical user interface of
      the application Cocktail checker */

cocktail_checker :-
   free_cck_graphical_variables,
   cck_find_input_files(Input_Files),
   open_cck_gui,
   cck_append_file_browser1(Input_Files),
   cck_append_file_browser2(Input_Files),
   cck_append_button,
   send(@cck_main_frame, open).


/* cck_possible_cocktails(Input_1, Input_2, Pos_List, Neg_List) <-
      finds for to given XML files representing a collection
      of cocktail receipts and the contents of a fridge,
      a list with possible cocktails and impossible cocktails */

cck_possible_cocktails(Input_1, Input_2, Pos_List, Neg_List) :-
   gn_remove,
   xml_file_to_gn_large(Input_1),
   xml_file_to_gn_large(Input_2),
   gn,
   findall( Cocktail,
      Cocktail := '&0'^cocktailrezepte^cocktailrezept@cocktail,
      Cocktails ),
   cck_find_positiv_cocktails(Cocktails, Pos_List1),
   reverse(Pos_List1, Pos_List),
   cck_find_negativ_cocktails(Pos_List, Cocktails, Neg_List),
   !.

cck_possible_cocktails(Input_1, Input_2) :-
   cck_possible_cocktails(Input_1, Input_2, Pos_List, Neg_List),
   cck_write_result_file(Pos_List, Neg_List),
   cck_show_result_file.


/*** implementation ***********************************************/


/* cck_find_positiv_cocktails(Cocktails, Pos_List)
      finds all possible cocktails from list Cocktails */

cck_find_positiv_cocktails(Cocktails, Pos_List) :-
   sublist( cck_check_cocktail,
      Cocktails, Pos_List ).

/* cck_find_negativ_cocktails(Pos_List, Cocktails, Neg_List) <-
      finds all impossible cocktails from list Cocktails */

cck_find_negativ_cocktails([], Neg_List, Neg_List).
cck_find_negativ_cocktails([H|T], Neg_Akk, Neg_List) :-
   delete(Neg_Akk, H, Neg_Akk1),
   cck_find_negativ_cocktails(T, Neg_Akk1, Neg_List).


/* cck_check_cocktail(Cocktail) <-
      checks if preparation of Cocktail is possible */

cck_check_cocktail(Cocktail) :-
   findall( Zutat-Menge,
      _ := '&0'^cocktailrezepte
         ^cocktailrezept::[
            @cocktail=Cocktail,
            ^zutat::[@name=Zutat, @menge=Menge]],
      Zutaten ),
   ( foreach(Zutat-Menge, Zutaten) do
        cck_check_zutat(Zutat-Menge) ).

cck_check_zutat(Zutat-Menge) :-
   _ := '&0'^vorraete^vorrat::[@name=Zutat, @menge=Menge2],
   cck_compare_amount(Menge2, Menge).

cck_compare_amount(Menge2, Menge1) :-
   cck_amount_to_integer(Menge1, Integer1),
   cck_amount_to_integer(Menge2, Integer2),
   Integer1 < Integer2.

cck_amount_to_integer(Menge, Integer) :-
   atom_length(Menge, Length),
   Length2 is Length-2,
   sub_atom(Menge, 0, Length2, _, Integer_Part),
   atom_codes(Integer_Part, Ascii_List),
   name(Integer, Ascii_List).


/* cck_write_result_file(Pos_List, Neg_List) <-
      writes the result file for the application
      Cocktail checker */

cck_write_result_file(Pos_List, Neg_List) :-
   tell('cck_result.html'),
   writeln_list(['<html>',
      '<head>',
      '<title>Cocktailchecker - Ergebnis</title>',
      '</head>',
      '<body>',
      '<table border="0" cellpadding="5">',
      '<tr><td align="left"><b>M�gliche Cocktails:</b></td></tr>',
      '<tr><td align="left"></td></tr>']),
   cck_write_positiv_cocktails(Pos_List),nl,
   writeln_list(['<tr><td align="left">',
      '<b>Unm�gliche Cocktails:</b><br>',
      '</td></tr>']),
   cck_write_negativ_cocktails(Neg_List),nl,
   writeln_list(['</table>',
      '</body>',
      '</html>']),
   told.

cck_write_positiv_cocktails([]) :-
   writeln_list(['<tr><td align="left">',
      'keine',
      '</td></tr>',
      '<tr><td align="left"></td></tr>']),
   !.
cck_write_positiv_cocktails(Pos_List) :-
   checklist(cck_write_positiv_cocktail, Pos_List).

cck_write_positiv_cocktail(Cocktail) :-
   findall( Zutat-Menge,
      _ := '&0'^cocktailrezepte
         ^cocktailrezept::[
            @cocktail=Cocktail,
            ^zutat::[@name=Zutat, @menge=Menge]],
      Zutaten ),
   X := '&0'^cocktailrezepte
      ^cocktailrezept::[@cocktail=Cocktail],
   Z := X ^zubereitung,
   value(Z, Zubereitung, _),
   write('<tr><td align="left">'), nl,
   write('<b>'),write(Cocktail),write('</b><br>'),nl,
   ( foreach(Zutat-Menge, Zutaten) do
     cck_write_zutat(Zutat-Menge)),
   writeln_list([Zubereitung,
      '<br>',
      '</td></tr>',
      '<tr><td align="left"></td></tr>']).

cck_write_zutat(Zutat-Menge) :-
   write(Menge),write(' '), write(Zutat),write('<br>').

cck_write_negativ_cocktails([]) :-
   writeln_list(['<tr><td align="left">',
      'keine',
      '</td></tr>']),
   !.
cck_write_negativ_cocktails(Neg_List) :-
   write('<tr><td align="left">'), nl,
   checklist(cck_write_negativ_cocktail, Neg_List),
   write('</td></tr>'), nl.

cck_write_negativ_cocktail(Cocktail) :-
   write(Cocktail), write('<br>'),nl.


/*** GUI **********************************************************/


/* free_cck_graphical_variables <-
      frees the graphical varaibeles for the application */

free_cck_graphical_variables :-
   checklist( free, [
      @cck_main_frame, @cck_result_window, @cck_list_browsers,
      @cck_list_browsers_layout, @cck_file_browser1,
      @cck_file_browser2 ]).


/* open_cck_gui <-
      creates the main frame of the application */

open_cck_gui :-
   new(@cck_main_frame, frame('Cocktailchecker')),
   new(@cck_result_window, doc_window),
   send(@cck_main_frame, append, @cck_result_window),
   send(new(@cck_list_browsers, dialog), left,
      @cck_result_window),
   send(@cck_list_browsers, size, size(225, 544)),
   send(@cck_list_browsers, gap, size(20, 0)),
   send(@cck_list_browsers, append,
      new(@cck_list_browsers_layout,
         dialog_group(buttons, group))),
   send(@cck_list_browsers_layout, gap, size(0, 30)).


/* cck_append_file_browser1(Input_Files) <-
       */

cck_append_file_browser1(Input_Files) :-
   send(@cck_list_browsers_layout, append, label('')),
   new(@cck_file_browser1, browser),
   send(@cck_file_browser1?list_browser,
      label, 'Cocktailrezepte:'),
   send_list(@cck_file_browser1, append, Input_Files),
   send(@cck_list_browsers_layout, append, @cck_file_browser1).


/* cck_append_file_browser2(Input_Files) <-
       */

cck_append_file_browser2(Input_Files) :-
   new(@cck_file_browser2, browser),
   send(@cck_file_browser2?list_browser, label, 'Vorraete:'),
   send_list(@cck_file_browser2, append, Input_Files),
   send(@cck_list_browsers_layout, append,
      @cck_file_browser2, below).


/* cck_append_button <-
       */

cck_append_button :-
   send(@cck_list_browsers_layout, append,
      button('Bestimme m�gliche Cocktails',
         message(@prolog, cck_possible_cocktails,
            @cck_file_browser1?selection?key,
            @cck_file_browser2?selection?key))).

cck_find_input_files(Input_Files) :-
   home_directory(Home),
   concat(Home, '/XML-Files/', Directory),
   working_directory(_, Directory),
   expand_file_name('*.xml', Input_Files).


/* cck_show_result_file <-
       */

cck_show_result_file :-
   free(@cck_result_doc_window),
   send(@cck_result_window,
      display(new(@cck_result_doc_window, pbox),
      point(10, 10))),
   send(@cck_result_window,
      resize_message(message(@cck_result_doc_window, width,
         @arg2?width-20))),
   load_html_file('cck_result.html', F),
   send(@cck_result_doc_window, show(F)).


/******************************************************************/


