

/******************************************************************/
/***                                                            ***/
/***        XML Graph_Notation: Path Parser                     ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xpath_similar_to_classic_syntax(XPath, Classic) <-
      parses a path given in XPath-similar synatx to classic
      syntax */

xpath_similar_to_classic_syntax(XPath, Classic) :-
   xp_path_split(XPath, XPaths),
   gn_parse_xs_to_cs(XPaths, '', Classic).


/*** implementation ***********************************************/


/* gn_parse_xs_to_cs(List, C, C) <-
      creates from the list with XPath similar entries
      the classical syntax using recursion */

gn_parse_xs_to_cs([], C, C).
gn_parse_xs_to_cs([H|T], CAkk, C) :-
   gn_parse_xs_to_cs_step(H, CStep),
   concat_atoms([CAkk,CStep],CAkk1),
   gn_parse_xs_to_cs(T, CAkk1, C).


/* gn_parse_xs_to_cs_step(S, SL) <-
      rekursion step */

gn_parse_xs_to_cs_step((/,S), SL) :-
   S =.. [::, A, T],
   T =.. [T],
   nonvar(T),
   gn_parse_xs_to_cs_concat_step(A, T, SL).
gn_parse_xs_to_cs_step((/,S), SL) :-
   S =.. [::, A, T],
   T =.. [T],
   var(T),
   gensym('Var', T2),
   gn_parse_xs_to_cs_concat_step(A, T2, SL).
gn_parse_xs_to_cs_step((/,S), SL) :-
   S =.. [::, A, T],
   T =.. [::, T2, L],
   nonvar(T2),
   gn_parse_xs_to_cs_concat_step(A, T2, L, SL).
gn_parse_xs_to_cs_step((/,S), SL) :-
   S =.. [::, A, T],
   T =.. [::, T2, L],
   var(T2),
   gensym('Var', T3),
   gn_parse_xs_to_cs_concat_step(A, T3, L, SL).


/* gn_parse_xs_to_cs_concat_step(A, T, SL) <-
      concat the atoms for one classic step without conditions */

gn_parse_xs_to_cs_concat_step(child, T, SL) :-
   concat_atoms(['^',T], SL).
gn_parse_xs_to_cs_concat_step(attribute, T, SL) :-
   concat_atoms(['@',T], SL).
gn_parse_xs_to_cs_concat_step(descendant, T, SL) :-
   concat_atoms(['^_^',T], SL).


/* gn_parse_xs_to_cs_concat_step(A, T, L, SL) <-
      concat the atoms for one classic step with conditions */

gn_parse_xs_to_cs_concat_step(child, T, L, SL) :-
   concat_atoms(['^',T], SL1),
   gn_parse_xs_to_cs_concat_conditions(L, '::[', CL),
   concat_atoms([SL1, CL], SL).
gn_parse_xs_to_cs_concat_step(attribute, T, L, SL) :-
   concat_atoms(['@',T], SL1),
   gn_parse_xs_to_cs_concat_conditions(L, '::[', CL),
   concat_atoms([SL1, CL], SL).
gn_parse_xs_to_cs_concat_step(descendant, T, L, SL) :-
   concat_atoms(['^_^',T], SL1),
   gn_parse_xs_to_cs_concat_conditions(L, '::[', CL),
   concat_atoms([SL1, CL], SL).

gn_parse_xs_to_cs_concat_conditions([], Akk, Res) :-
   atom_length(Akk, L),
   L2 is L-2,
   sub_atom(Akk,0,L2,_,Akk1),
   concat_atoms([Akk1, ']'], Res).
gn_parse_xs_to_cs_concat_conditions([H|T], Akk, Res) :-
   xpath_similar_to_classic_syntax(H, HRes),
   concat_atoms([Akk, HRes, ', '], Akk1),
   gn_parse_xs_to_cs_concat_conditions(T,Akk1,Res).


/******************************************************************/


