

/******************************************************************/
/***                                                            ***/
/***        XML Graph Notation:  Parser for FN -> GN            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xml_file_to_gn(File) <-
      converts a XML-file into its graph notation. */

xml_file_to_gn(File) :-
   size_file(File, Size),
   Size < 10000,
   xml_file_to_gn_small(File).
xml_file_to_gn(File) :-
   size_file(File, Size),
   Size >= 10000,
   xml_file_to_gn_large(File).


/*** implementation ***********************************************/


/* xml_file_to_gn_small(File) <-
      Converts an XML file into graph notation.
      Should only be uesd for small XML files.
      For big XML files use xml_file_to_gn_large. */

xml_file_to_gn_small(File) :-
   dislog_variable_set(fn_mode, fn),
   xml_file_to_fn_term(File, Fn),
   assert(attribute(dummy, dummy, dummy)),
   retract(attribute(dummy, dummy, dummy)),
   fn_to_table_lists(Fn, References, Values, Attributes),
   writeln( 'The following facts are appended to the Prolog
      Database:'), nl,
   checklist( assert_reference_to_prolog_database,
      References), nl,
   checklist( assert_value_to_prolog_database,
      Values), nl,
   checklist( assert_attribute_to_prolog_database,
      Attributes), nl.


/* fn_to_table_lists(Fn, References, Values, Attributes) <-
      constructs the lists of relations for the XML-representation
      into a relational scheme */

fn_to_table_lists(Fn, References, Values, Attributes) :-
   dislog_variables_set([
      references - [],
      values - [],
      attributes - [] ] ),
   ( foreach(Fn_Part, Fn) do
        update_relational_lists(Fn_Part, '&0') ),
   dislog_variables_get([
      references - Rev_References,
      values - Rev_Values,
      attributes - Rev_Attributes ] ),
   reverse(Rev_References, References),
   reverse(Rev_Values, Values),
   reverse(Rev_Attributes, Attributes).


/* update_relational_lists(Fn, Source) <-
      goes from the sources node one level deeper into the XML tree
      and collects the structural information */

update_relational_lists(Fn, Source) :-
   element_to_list(Fn, Fns),
   O := Fns^Label,
   term_to_atom(Label, Label_Atom),
   not(sub_atom(Label_Atom, _, _, _, '^')),
   not(sub_atom(Label_Atom, _, _, _, '@')),
   Attribut := Fns@_,
   update_reference_list(Source, Label, Destination),
   checklist( update_attribute_list(Destination),
      Attribut ),
   ( foreach(OPart, O) do
        update_relational_lists(OPart, Destination) ).
update_relational_lists(Fn, Source) :-
   update_value_list(Fn, Source).

update_reference_list(Source, Label, Destination) :-
   gn_object_get('&', Destination),
   get_num('@e', Element),
   dislog_variable_get(references, References),
   dislog_variable_set(references,
      [Source-Label-Destination-Element|References]).

update_attribute_list(Object, Label:Value):-
   dislog_variable_get(attributes, Attributes),
   dislog_variable_set(attributes,
      [Object-Label-Value|Attributes]).

update_value_list(Fn_Part, Source) :-
   get_num('@e', Element),
   dislog_variable_get(values, Values),
   dislog_variable_set(values, [Source-Fn_Part-Element|Values]).


/* assert_reference_to_prolog_database(
         Source - Label - Destination - Element) <-
      asserts a reference fact to the prolog database
      and informs the user about that */

assert_reference_to_prolog_database(
      Source - Label - Destination - Element) :-
   assertz(reference(Source, Label, Destination, Element)),
   write_list([ 'reference(\'', Source, '\', \'', Label, '\', \'',
      Destination, '\', ', Element, ').\n' ]).


/* assert_value_to_prolog_database(Object - Value - Element) <-
      asserts a value fact to the prolog database and informs the
      user about that */

assert_value_to_prolog_database(Object - Value - Element) :-
   assertz(value(Object, Value, Element)),
   write_list([ 'value(\'', Object, '\', \'', Value, '\', ',
      Element, ').\n' ]).


/* assert_attribut_to_prolog_database(Object - Label - Value) <-
      asserts a attribute fact to the prolog database
      and informs the user about that */

assert_attribute_to_prolog_database(Object - Label - Value) :-
   assertz(attribute(Object, Label, Value)),
   write_list([ 'attribute(\'', Object, '\', \'', Label,
      '\', \'', Value, '\').\n' ]).


/* xml_file_to_gn_large(File) <-
      creates for the given XML-File its representation in graph 
      notation and writes the discovered facts on the fly
      to the database */

xml_file_to_gn_large(File) :-
   dislog_variable_set(fn_mode, fn),
   xml_file_to_fn_term(File, Fn),
   dynamic(attribute/3),
%  assert(attribute(dummy, dummy, dummy)),
%  retract(attribute(dummy, dummy, dummy)),
   fn_to_database(Fn).


/* fn_to_database(Fn) <-
      converts the FN-list Fn to the database representation */

fn_to_database(Fn) :-
   fn_to_database(Fn, '&0').

fn_to_database(Fn, Id) :-
   ( foreach(Fn_Part, Fn) do
        gn_assert_relations(Fn_Part, Id) ).


/* gn_assert_relations(Fn_Part, Source) <-
      discovers the facts and writes the database */

gn_assert_relations(Fn_Part, Source) :-
   element_to_list(Fn_Part, Fn_Parts),
   O := Fn_Parts^Label,
   term_to_atom(Label, Label_Atom),
   not(sub_atom(Label_Atom, _, _, _, '^')),
   not(sub_atom(Label_Atom, _, _, _, '@')),
   Attribut := Fn_Parts@_,
   gn_assert_reference(Source, Label, Destination),
   checklist( gn_assert_attribute(Destination),
      Attribut ),
   ( foreach(OPart,O) do
        gn_assert_relations(OPart, Destination) ).
gn_assert_relations(Fn_Part, Source) :-
   gn_assert_value(Fn_Part, Source).

gn_assert_reference(Source, Label, Destination) :-
   gn_object_get('&', Destination),
   get_num('@e', Element),
   assertz(reference(Source, Label, Destination, Element)),
   writeln(assertz(reference(Source, Label, Destination, Element))).

gn_assert_attribute(Object, Label:Value) :-
   assertz(attribute(Object, Label, Value)).

gn_assert_value(Value, Object) :-
   get_num('@e', Element),
   assertz(value(Object, Value, Element)).


/* gn_remove <-
      removes all reference, attribute and value entries and
      sets the gensym counter for '&' back to 0 */

gn_remove :-
   retractall(reference(_, _, _, _)),
   retractall(attribute(_, _, _)),
   retractall(value(_, _, _)),
   set_num('&', 0),
   set_num('@e', 0).


/* gn_object_get <-
      creates the actual number of the gn_object */

gn_object_get(Root,Atom) :-
   get_num(Root,Num),
   name(Root,Name1),
   number_codes(Num,Name2),
   append(Name1,Name2,Name),
   name(Atom,Name).


/*** tests ********************************************************/


test(gn_query, 1) :-
   gn_remove,
   dislog_variable_set(fn_mode, fn),
   fn_to_database([
      apply:[attr:value]:[
         ci:[type:fn, test:yes]:[y],
         ci:[]:[x] ] ]),
   reference('&0', 'apply', '&1', _),
   reference('&1', 'ci', '&2', _),
   reference('&1', 'ci', '&3', _),
   value('&2', 'y', _),
   value('&3', 'x', _),
   attribute('&1', 'attr', 'value'),
   attribute('&2', 'type', 'fn'),
   attribute('&2', 'test', 'yes'),
   dislog_variable_set(fn_mode, gn),
   'value' := '&0'^apply@attr,
   '&2' := '&0'^apply^ci,
   '&3' := '&0'^apply^ci,
   'fn' := '&0'^apply^ci@type,
   'yes' := '&0'^apply^ci@test,
   gn_remove,
   dislog_variable_set(fn_mode, fn).

test(gn_query, 2) :-
   gn_remove,
   dislog_variable_set(fn_mode, fn),
   fn_to_database([
      apply:[attr:value]:[
         ci:[type:fn, test:3]:[y],
         ci:[]:[x] ] ]),
   dislog_variable_set(fn_mode, gn),
   '&1' := '&0'^apply::[@attr],
   '&1' := '&0'^apply::[@attr='value'],
   '&1' := '&0'^apply::[@attr\='val'],
   '&1' := '&0'^apply::[^ci],
   '&1' := '&0'^apply::[^ci@type],
   '&1' := '&0'^apply::[^ci@type='fn'],
   '&1' := '&0'^apply::[^ci@test],
   '&1' := '&0'^apply::[^ci@test=3],
   '&1' := '&0'^apply::[^ci@test<4],
   '&1' := '&0'^apply::[^ci@test>2],
   gn_remove,
   dislog_variable_set(fn_mode, fn).


/******************************************************************/


