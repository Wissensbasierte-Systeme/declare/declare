

/******************************************************************/
/***                                                            ***/
/***        ASCII-Text: ASCII-Code based Text Handling          ***/
/***                                                            ***/
/******************************************************************/


:- module(ascii_text, [
      list_empty/1, list_to_pairlist/3, list_compare/3,
      list_replace/4, list_unique_replace/4, list_nth_replace/4,
      list_insert/4,
      list_copy/4,
%     contains/2,
      last_index_of/3, start_split/4, end_split/4, ends_with/2,
      char_split/3,
      char_compose/3, char_compose_prefix/3, char_compose_suffix/3,
      trim/2, trim_head/2, trim_tail/2,
      to_upper_case/2, to_lower_case/2,
      before_signal_char/3, after_signal_char/3,
      is_whitespace/1, is_white/1, is_visible/1,
      flow_normalize/2, white_to_space/2,
      space_normalize/2,
      char_print/1,
      is_ascii_text/1 ]).


/*** interface ****************************************************/


/* list_empty(+List) <-
      succeeds if List as an empty list. */

list_empty(List) :-
   \+ nth1(1, List, _).

/* list_to_pairlist(?List_1, ?List_2, ?Pair_List) <-
      combines lists List_1, List_2 to a list of pairs Pair_List.
      The n-th pair contains the n-th elements of List_1 and List_2
      respectively. At least two of the three arguments must be
      bound before calling the predicate. */

list_to_pairlist([],[],[]).

list_to_pairlist([First_1|Rest_1], [First_2|Rest_2],
      [[First_1, First_2]|Rest]) :-
   list_to_pairlist(Rest_1, Rest_2, Rest).

/* list_compare(-Delta, +List_1, +List_2 <-
      lexically compares two lists List_1 and List_2 returning
      <, >, = in first argument with the obvious meaning. */

list_compare('=', [], []).
list_compare('<', [], [_|_]).
list_compare('>', [_|_], []).

list_compare(Value, [First_1|Rest_1], [First_2|Rest_2]) :-
   First_1 < First_2 ->
      Value = '<'
      ; ( First_1 > First_2 ->
           Value = '>'
           ; list_compare(Value, Rest_1, Rest_2) ).

/* list_nth_replace(+List, +N, +New_Element, -New_List) <-
      replaces N-th element of List by New_Element returning the
      result New_List. The predicate doesn't alter the list if
      N is an invalid index out of list index range. */

list_nth_replace([], _, _, []).

list_nth_replace([_|Rest], 1, New_Element, [New_Element|Rest]) :-
   !.

list_nth_replace([First|Rest], N, New_Element, [First|Rest_List]) :-
   N1 is N - 1,
   list_nth_replace(Rest, N1, New_Element, Rest_List).

/* list_unique_replace(+List, +Old, +New, -New_List) <-
      replaces unique element Old by element New in List returning
      result New_List. */

list_unique_replace([], _, _, []).

list_unique_replace([Old|Rest], Old, New, [New|Rest]) :-
   !.

list_unique_replace([First|Rest], Old, New, [First|New_Rest]) :-
   list_unique_replace(Rest, Old, New, New_Rest).

/* list_replace(+List, +Old, +New, -New_List) <-
      replaces every occurrence of Old in List by New returning
      result New_List. */

list_replace([], _, _, []).

list_replace([Old|Rest], Old, New, [New|New_Rest]) :-
   !,
   list_replace(Rest, Old, New, New_Rest).

list_replace([First|Rest], Old, New, [First|New_Rest]) :-
   list_replace(Rest, Old, New, New_Rest).

/* list_copy(+List, +Start, +Count, -New_List) <-
      copies Count elements beginning at one-based index Start from
      List to New_List. If List contains fewer elements with an
      index greater equal Start than Count all these elements are
      being copied to New_List. */

list_copy([], _, _, []) :-
   !.

list_copy(List, 1, Count, New_List) :-
   !,
   list_recursive_copy(List, Count, New_List).

list_copy([_|Rest], Start, Count, New_List) :-
   Next_Pos is Start - 1,
   list_copy(Rest, Next_Pos, Count, New_List).

/* list_insert(+List, +Element, +Position, -New_List) <-
      inserts Element into List at one-based index Position returning
      result New_List. If Position is an invalid index Element is
      appended at the end of List. */

list_insert([], Element, _, [Element]).

list_insert([First|Rest], Element, 1, [Element, First|Rest]) :-
   !.

list_insert([First|Rest], Element, Position, [First|Rest_List]) :-
   New_Pos is Position - 1,
   list_insert(Rest, Element, New_Pos, Rest_List).


/* contains(+Text, +Search_Text) <-
      succeeds if Search_Text is contained in Text. */

contains([First|Rest], [First|Rest_Match]) :-
   text_match(Rest, Rest_Match),
   !.

contains([_|Rest], Search_Text) :-
   ascii_text:contains(Rest, Search_Text).

contains(_, []).

/* last_index_of(+Text, +Char, ?Index) <-
      returns index of last occurrence of Char in Text as Index. */

last_index_of(Text, Char, Index) :-
   length(Text, Length),
   text_search_char(Text, Char, Length, Index).

/* index_of(+Text, +Char, ?Index) <-
      returns index of first occurrence of Char in Text as Index. */

index_of(Text, Char, Index) :-
   text_first_search(Text, Char, 1, Index).

/* start_split(+Text, +N, ?Prefix, ?Suffix) <-
      returns first N characters of Text as Prefix and the remainder
      as Suffix. */

start_split(Text, 0, [], Text).

start_split([], _, [], []) :-
   !.

start_split([First|Rest], Index, [First|More], Suffix) :-
   Index > 0,
   Zero_Count is Index - 1,
   ascii_text:start_split(Rest, Zero_Count, More, Suffix).

/* ends_with(+Text, ?Suffix) <-
      succeeds if Text ends with Suffix. */

ends_with(Text, Suffix) :-
   length(Suffix, N),
   ascii_text:end_split(Text, N, Suffix, _).

/* end_split(+Text, +N, ?Suffix, ?Prefix) <-
      returns last N characters of Text as Suffix and the text
      before as Prefix. */

end_split(Text, N, Suffix, Prefix) :-
   reverse(Text, Reversed),
   ascii_text:start_split(Reversed, N, Suffix_Reversed, Prefix_Reversed),
   reverse(Suffix_Reversed, Suffix),
   reverse(Prefix_Reversed, Prefix).

/* char_split(+Text, +Split_Char, ?Split) <-
      splits Text using separator character Split_Char returning a
      list of the separated text fragments as Split. */

char_split(Text, Split_Char, Split) :-
   recursive_split(Text, Split_Char, Split, []).

/* char_compose(+Split, +Split_Char, ?Text) <-
      composes elements of list Split to Text separating the list
      elements by character Split_Char. */

char_compose(List, Separator, Text) :-
   glue_text(List, Separator, Glued),
   flatten(Glued, Text).

/* char_compose_prefix(+Split, +Split_Char, ?Text) <-
      composes elements of list Split to Text separating the list
      elements by character Split_Char. Split_Char is also added to
      Text as a prefix character. */

char_compose_prefix(List, Split_Char, [Split_Char|Text]) :-
   ascii_text:char_compose(List, Split_Char, Text).

/* char_compose_suffix(+Split, +Split_Char, ?Text) <-
      composes elements of list Split to Text separating the list
      elements by character Split_Char. Split_Char is also appended
      to the end of Text. */

char_compose_suffix(List, Split_Char, Text) :-
   ascii_text:char_compose(List, Split_Char, Raw_Text),
   append(Raw_Text, [Split_Char], Text).

/* trim(+Text, ?Trimmed) <-
      removes leading and trailing whitespace characters from Text
      returning result as Trimmed. */

trim([],[]).

trim(Text, New_Text) :-
   ascii_text:trim_head(Text, Trimmed_Head),
   ascii_text:trim_tail(Trimmed_Head, New_Text).

/* trim_head(+Text, ?Trimmed) <-
      removes leading whitespace characters from Text
      returning result as Trimmed. */

trim_head([First|Rest], [First|Rest]) :-
   First > 32,
   !.

trim_head([First|Rest], New_Text) :-
   First < 33,
   !,
   ascii_text:trim_head(Rest, New_Text).

trim_head([],[]).

/* trim_tail(+Text, ?Trimmed) <-
      removes trailing whitespace characters from Text
      returning result as Trimmed. */

trim_tail(Text, New_Text) :-
   text_trim_tail_with_state(Text, _, New_Text).

/* to_lower_case(+Text, ?Low_Text) <-
      transforms Text into lower case version Low_Text. */

to_lower_case(Text, Low_Text) :-
   maplist(code_to_lower_case,
      Text, Low_Text).

/* to_upper_case(+Text, ?Up_Text) <-
      transforms Text into upper case version Up_Text. */

to_upper_case(Text, Up_Text) :-
   maplist(code_to_upper_case,
      Text, Up_Text).

/* before_signal_char(+Text, +Signal_Char, ?Before_Text) <-
      returns characters of Text before Signal_Char as
      Before_Text. */

before_signal_char([], _, []).

before_signal_char([Signal|_], Signal, []) :-
   !.

before_signal_char([First|Rest], Signal, [First|Rest_Text]) :-
   ascii_text:before_signal_char(Rest, Signal, Rest_Text).

/* after_signal_char(+Text, +Signal_Char, ?After_Text) <-
      returns characters of Text after Signal_Char as After_Text. */

after_signal_char([], _, []).

after_signal_char([Signal|Rest], Signal, Rest) :-
   !.

after_signal_char([_|Rest], Signal, New_Text) :-
   ascii_text:after_signal_char(Rest, Signal, New_Text).

/* is_whitespace(?Ascii_Code) <-
      succeeds if Ascii-Code represents a whitespace character. */

is_whitespace(Ascii_Code) :-
   Ascii_Code < 33.

/* is_white(+Text) <-
      succeeds if Text exclusively consists of whitespace. */

is_white([]).

is_white([First|Rest]) :-
   ascii_text:is_whitespace(First),
   ascii_text:is_white(Rest).

/* is_visible(+Text) <-
      succeeds if Text contains at least one visible character. */

is_visible(Text) :-
   \+ ascii_text:is_white(Text).

/* flow_normalize(+Text, -Normalized) <-
      transforms Text into Normalized by replacing whitespaces by
      spaces and eliminating successive space characters. */

flow_normalize([Char], Result) :-
   white_to_space([Char], Result).

flow_normalize([First, Second|Rest], Transformed) :-
   ascii_text:flow_normalize([Second|Rest], Rest_List),
   normalize_sequence(First, Second, New_Start),
   append(New_Start, Rest_List, Transformed).

/* white_to_space(+Text, ?Spaced) <-
      replaces all whitespaces in Text by space characters returning
      result Spaced. */

white_to_space(Text, Space_Text) :-
   maplist(to_space,
      Text, Space_Text).

/* space_normalize(+Text, -Normalized) <-
      ensures that a list of text fragments is separated by single
      spaces if concatenated. */

space_normalize([], []).

space_normalize([First|Rest], [First_Trimmed|Space_Rest]) :-
   ascii_text:trim(First, First_Trimmed),
   sublist(ascii_text:is_visible,
      Rest, Visible_Rest),
   maplist(text_start_space,
      Visible_Rest, Space_Rest).

/* char_print(+Text) <-
      prints Text as a sequence of characters. */

char_print(Text) :-
   checklist(put_char,
      Text).

/* is_ascii_text(+Text) <-
      succeeds if argument Text is a text represented as a list of
      8-bit-ASCII character codes. */

is_ascii_text(Text) :-
   checklist(code_in_ascii_range, Text).


/*** implementation ***********************************************/


list_recursive_copy(_, Count, []) :-
   Count < 1,
   !.

list_recursive_copy([], _, []).

list_recursive_copy([First|Rest], Count, [First|More]) :-
   Next_Count is Count - 1,
   list_recursive_copy(Rest, Next_Count, More).

text_match(_, []).

text_match([First|Rest], [First|Rest_Match]) :-
   text_match(Rest, Rest_Match).

text_search_char(_, _, 0, 0) :-
   !.

text_search_char(Text, First, Current, Current) :-
   nth1(Current, Text, First).

text_search_char(Text, First, Current, Index) :-
   \+ nth1(Current, Text, First),
   Previous is Current - 1,
   text_search_char(Text, First, Previous, Index).

text_first_search([], _, _, 0).

text_first_search([First|_], First, Current, Current) :-
   !.

text_first_search([_|Rest], Char, Current, Index) :-
   Next is Current + 1,
   text_first_search(Rest, Char, Next, Index).

recursive_split([], _, [], []).

recursive_split([], _, [[First|Rest]], [First|Rest]).

recursive_split([First|Rest], First, Result, []) :-
   !,
   recursive_split(Rest, First, Result, []).

recursive_split([First|Rest], First, Result, Current) :-
   !,
   recursive_split(Rest, First, Next_Result, []),
   append([Current], Next_Result, Result).

recursive_split([First|Rest], Char, Result, Current) :-
   append(Current, [First], Next_Current),
   recursive_split(Rest, Char, Result, Next_Current).

glue_text([], _, []).

glue_text([Char], _, [Char]).

glue_text([First, Second|Rest], Separator,
      [First, Separator|Rest_Text]) :-
   glue_text([Second|Rest], Separator, Rest_Text).

text_trim_tail_with_state([First|Rest], text, [First|Rest_Tail]) :-
   First > 32,
   !,
   text_trim_tail_with_state(Rest, _, Rest_Tail).

text_trim_tail_with_state([First|Rest], text, [First|Rest_Tail]) :-
   First < 33,
   text_trim_tail_with_state(Rest, text, Rest_Tail).

text_trim_tail_with_state([First|Rest], space, []) :-
   First < 33,
   text_trim_tail_with_state(Rest, space, _),
   !.

text_trim_tail_with_state([], space, []).

code_to_lower_case(Code, Lower_Case) :-
   code_type(Code, to_upper(Lower_Case)).

code_to_upper_case(Code, Upper_Case) :-
   code_type(Code, to_lower(Upper_Case)).

normalize_sequence(First, Second, []) :-
   ascii_text:is_whitespace(First),
   ascii_text:is_whitespace(Second).

normalize_sequence(First, Second, [Result]) :-
   ascii_text:is_whitespace(First),
   \+ ascii_text:is_whitespace(Second),
   Result = 32.

normalize_sequence(First, _, [First]) :-
   \+ ascii_text:is_whitespace(First).

to_space(Code, 32) :-
   ascii_text:is_whitespace(Code),
   !.

to_space(Code, Code).

text_start_space(Text, [32|Space_Text]) :-
   ascii_text:trim(Text, Space_Text).

code_in_ascii_range(Char) :-
  number(Char),
  between(0, 255, Char).


/******************************************************************/


