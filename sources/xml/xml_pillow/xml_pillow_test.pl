

/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  Tests                                 ***/
/***                                                            ***/
/******************************************************************/


:- discontiguous test/2.


/*** initialization ***********************************************/


/*** loading and transforming of example file ***/

xml_file_load_text(Text) :-
   seeing(Old),
   see('examples/xml/langbase.xml'),
   xml_file_collect_text(Text),
   seen,
   see(Old).

xml_file_collect_text([First|Rest]) :-
   get_code(First),
   First \== -1,
   !,
   xml_file_collect_text(Rest).

xml_file_collect_text([]).

xml_file_to_pillow(Page) :-
   xml_file_load_text(Text),
   xml2terms(Text, Page).


/*** extraction of specific terms from example Pillow tree ***/

xml_term_extract(root, Page, Term) :-
   xml_pillow_navigate:root_element(Page, Term).
xml_term_extract(root_decl, Page, Term) :-
   xml_pillow_navigate:toplevel_note(Page, Term),
   xml_pillow_type_check(shallow, xml_declaration, Term).
xml_term_extract(script, Page, Term) :-
   xml_term_extract(root, Page, Root),
   xml_pillow_navigate:nth_child_element(Root, 3, Term).
xml_term_extract(lang, Page, Term) :-
   xml_term_extract(root, Page, Root),
   xml_pillow_navigate:nth_child_element(Root, 1, Term).
xml_term_extract(empty, Page, Term) :-
   xml_term_extract(script, Page, Script),
   xml_pillow_navigate:nth_child_element(Script, 1, Term).
xml_term_extract(comment, Page, Term) :-
   xml_term_extract(script, Page, Script),
   xml_pillow_navigate:nth_child_term(Script, 4, Term).
xml_term_extract(declaration, Page, Term) :-
   xml_pillow_navigate:toplevel_note(Page, Term),
   xml_pillow_type_check(shallow, declaration, Term).
xml_term_extract(first_name, Page, Term) :-
   xml_term_extract(text_child, Page, Text_Child),
   xml_pillow_navigate:nth_child_element(Text_Child, 1, First_Name),
   xml_pillow_query:shallow_text(First_Name, Term).
xml_term_extract(attr_element, Page, Term) :-
   xml_term_extract(lang, Page, Lang),
   xml_pillow_navigate:nth_child_element(Lang, 1, Term).
xml_term_extract(attr_term, Page, Term) :-
   xml_term_extract(attr_element, Page, Attr_Element),
   xml_pillow_query:attributes(Attr_Element, Attributes),
   nth1(1, Attributes, Term).
xml_term_extract(text_element, Page, Term) :-
   xml_term_extract(lang, Page, Term).
xml_term_extract(text_child, Page, Term) :-
   xml_term_extract(lang, Page, Lang),
   xml_pillow_navigate:nth_child_element(Lang, 2, Term).
xml_term_extract(last_text_child, Page, Term) :-
   xml_term_extract(lang, Page, Lang),
   xml_pillow_navigate:nth_child_element(Lang, 4, Term).
xml_term_extract(id_ref, Page, Term) :-
   xml_term_extract(empty, Page, Term).
xml_term_extract(id_multi_ref, Page, Term) :-
   xml_term_extract(root, Page, Root),
   xml_pillow_navigate:nth_child_element(Root, 4, Temp),
   xml_pillow_navigate:nth_child_element(Temp, 1, Term).


/*** pretty printing **********************************************/


xml_named_term_print(Name, Term) :-
   nl,
   write(Name),
   write_ln(' ='),
   xml_unnamed_term_print(Term).

xml_named_term_print(always_deep, Name, Term) :-
   nl,
   write(Name),
   write_ln(' ='),
   xml_pillow_print:xml_deep(normalized, Term, 0),
   nl.

xml_unnamed_term_print(Term) :-
   xml_pillow_type_check(shallow, Type, Term),
   \+ Type == attribute,
   xml_pillow_print:xml_shallow(normalized, Term, 0),
   nl.

xml_unnamed_term_print(Term) :-
   xml_pillow_type_check(shallow, attribute, Term),
   xml_pillow_print:xml_deep(normalized, Term),
   nl.

xml_unnamed_term_print(Value) :-
   \+ xml_pillow_type_check(shallow, _, Value),
   write(Value),
   nl.

xml_title_result_print(Title, Result) :-
   nl,
   write_ln(Title),
   nl,
   write_ln(Result),
   nl.


/*** pretty prints title and a list of elements ***/

xml_title_values_print(Title, Elements) :-
   nl,
   write_ln(Title),
   nl,
   write_ln('['),
   nl,
   xml_test_list_element_print(Elements),
   nl,
   write_ln(']'),
   nl.

xml_test_list_element_print([]).

xml_test_list_element_print([Element]) :-
   xml_unnamed_term_print(Element).

xml_test_list_element_print([First, Second|Rest]) :-
   xml_unnamed_term_print(First),
   write_ln(','),
   nl,
   xml_test_list_element_print([Second|Rest]).

xml_name_value_print(Name, Value) :-
   nl,
   write(Name),
   write(': '),
   write_ln(Value),
   nl.

xml_test_value_print(Text) :-
   nl,
   write_ln(Text),
   nl.

xml_code_element_pair(Page) :-
   xml_term_extract(ID, Page, Term),
   xml_named_term_print(ID, Term).

xml_test_name_print(Name) :-
   nl,
   write_ln(Name),
   write_ln('-----------------------------------------------------'),
   nl.


/*** element extraction tests *************************************/


test(xml_pillow, element_extract) :-
   xml_test_name_print(element_extract),
   xml_file_to_pillow(Page),
   findall( _,
      xml_code_element_pair(Page),
      _ ).


/***** field notation tests ***************************************/


/* conversion from pillow terms to field notation */

test(xml_pillow, pillow_to_field_notation) :-
   xml_test_name_print(pillow_to_field_notation),
   xml_file_to_pillow(Page),
   xml_pillow_field_notation:pillow_to_field_notation(Page, Field),
   nl,
   write(Field),
   nl.


/* pretty printing of field notation */

test(xml_pillow, field_notation_print) :-
   xml_test_name_print(field_notation_print),
   xml_file_to_pillow(Page),
   xml_pillow_field_notation:pillow_to_field_notation(Page, Field),
   nl,
   xml_pillow_field_notation:field_notation_print(Field),
   nl.


/* conversion from field notation to pillow terms */

test(xml_pillow, field_notation_to_pillow) :-
   xml_test_name_print(field_notation_to_pillow),
   xml_file_to_pillow(Page),
   xml_pillow_field_notation:pillow_to_field_notation(Page, Field),
   xml_pillow_field_notation:field_notation_to_pillow(Field, Page_2),
   xml_named_term_print(always_deep, 'Twice transformed document: ',
      Page_2).


/* path variable */

test(xml_pillow, path_variable) :-
   xml_test_name_print(path_variable),
   xml_file_to_pillow(Page),
   xml_pillow_field_notation:pillow_to_field_notation(Page, Field),
   Result := Field^library^Any^description,
   xml_name_value_print('Field^library^Any^description', Result),
   xml_name_value_print('Variable path segment', Any).


/* star operator */

test(xml_pillow, star_operator) :-
   xml_test_name_print(star_operator),
   xml_file_to_pillow(Page),
   xml_pillow_field_notation:pillow_to_field_notation(Page, Field),
   Result := Field^Any^firstname,
   xml_name_value_print('Field^Any^firstname', Result),
   xml_name_value_print('Variable path segment', Any).


/*** type check tests *********************************************/


/* shallow check of non-empty inner element */

test(xml_pillow, shallow_check_root) :-
   xml_test_name_print(shallow_check_root),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   bagof( Type,
      xml_pillow_type_check(shallow, Type, Root),
      Type_List ),
   xml_title_result_print(
      'Shallow types of root element:', Type_List).


/* deep check of non-empty inner element */

test(xml_pillow, deep_check_root) :-
   xml_test_name_print(deep_check_root),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   bagof( Type,
      xml_pillow_type_check(deep, Type, Root),
      Type_List ),
   xml_title_result_print(
      'Deep types of root element:', Type_List).


/* shallow check of empty element */

test(xml_pillow, shallow_check_empty) :-
   xml_test_name_print(shallow_check_empty),
   xml_file_to_pillow(Page),
   xml_term_extract(empty, Page, Empty),
   bagof( Type,
      xml_pillow_type_check(shallow, Type, Empty),
      Type_List ),
   xml_title_result_print(
      'Shallow types of empty element:', Type_List).


/* deep check of empty element */

test(xml_pillow, deep_check_empty) :-
   xml_test_name_print(deep_check_empty),
   xml_file_to_pillow(Page),
   xml_term_extract(empty, Page, Empty),
   bagof( Type,
      xml_pillow_type_check(deep, Type, Empty),
      Type_List ),
   xml_title_result_print(
      'Deep types of empty element:', Type_List).


/* shallow check of non-empty leaf element */

test(xml_pillow, shallow_check_last_text_child) :-
   xml_test_name_print(shallow_check_last_text_child),
   xml_file_to_pillow(Page),
   xml_term_extract(last_text_child, Page, Last_Text_Child),
   bagof( Type,
      xml_pillow_type_check(shallow, Type, Last_Text_Child),
      Type_List ),
   xml_title_result_print(
      'Shallow types of last_text_child element:', Type_List).


/* deep check of non-empty leaf element */

test(xml_pillow, deep_check_last_text_child) :-
   xml_test_name_print(deep_check_last_text_child),
   xml_file_to_pillow(Page),
   xml_term_extract(last_text_child, Page, Last_Text_Child),
   bagof( Type,
      xml_pillow_type_check(deep, Type, Last_Text_Child),
      Type_List ),
   xml_title_result_print(
      'Deep types of last_text_child element:', Type_List).


/* shallow check of comment */

test(xml_pillow, shallow_check_comment) :-
   xml_test_name_print(shallow_check_comment),
   xml_file_to_pillow(Page),
   xml_term_extract(comment, Page, Comment),
   bagof( Type,
      xml_pillow_type_check(shallow, Type, Comment),
      Type_List ),
   xml_title_result_print(
      'Shallow types of comment:', Type_List).


/* deep check of comment */

test(xml_pillow, deep_check_comment) :-
   xml_test_name_print(deep_check_comment),
   xml_file_to_pillow(Page),
   xml_term_extract(comment, Page, Comment),
   bagof( Type,
      xml_pillow_type_check(deep, Type, Comment),
      Type_List ),
   xml_title_result_print('Deep types of comment:', Type_List).


/* shallow check of declaration */

test(xml_pillow, shallow_check_decl) :-
   xml_test_name_print(shallow_check_decl),
   xml_file_to_pillow(Page),
%  xml_term_extract(decl, Page, Decl),
   xml_term_extract(declaration, Page, Decl),
   bagof( Type,
      xml_pillow_type_check(shallow, Type, Decl),
      Type_List ),
   xml_title_result_print('Shallow types of decl:', Type_List).


/* deep check of declaration */

test(xml_pillow, deep_check_decl) :-
   xml_test_name_print(deep_check_decl),
   xml_file_to_pillow(Page),
%  xml_term_extract(decl, Page, Decl),
   xml_term_extract(root_decl, Page, Decl),
   bagof( Type,
      xml_pillow_type_check(deep, Type, Decl),
      Type_List ),
   xml_title_result_print('Deep types of decl:', Type_List).


/* shallow check of XML declaration */

test(xml_pillow, shallow_check_root_decl) :-
   xml_test_name_print(shallow_check_root_decl),
   xml_file_to_pillow(Page),
   xml_term_extract(root_decl, Page, Root_Decl),
   bagof( Type,
      xml_pillow_type_check(shallow, Type, Root_Decl),
      Type_List ),
   xml_title_result_print('Shallow types of root_decl:', Type_List).


/* deep check of XML declaration */

test(xml_pillow, deep_check_root_decl) :-
   xml_test_name_print(deep_check_root_decl),
   xml_file_to_pillow(Page),
   xml_term_extract(root_decl, Page, Root_Decl),
   bagof( Type,
      xml_pillow_type_check(deep, Type, Root_Decl),
      Type_List ),
   xml_title_result_print('Deep types of root_decl:', Type_List).


/* shallow check of text */

test(xml_pillow, shallow_check_first_name) :-
   xml_test_name_print(shallow_check_first_name),
   xml_file_to_pillow(Page),
   xml_term_extract(first_name, Page, First_Name),
   bagof( Type,
      xml_pillow_type_check(shallow, Type, First_Name),
      Type_List ),
   xml_title_result_print(
      'Shallow types of first_name:', Type_List).


/* deep check of text */

test(xml_pillow, deep_check_first_name) :-
   xml_test_name_print(deep_check_first_name),
   xml_file_to_pillow(Page),
   xml_term_extract(first_name, Page, First_Name),
   bagof( Type,
      xml_pillow_type_check(deep, Type, First_Name),
      Type_List ),
   xml_title_result_print(
      'Deep types of first_name:', Type_List).


/* shallow check of document */

test(xml_pillow, shallow_check_page) :-
   xml_test_name_print(shallow_check_page),
   xml_file_to_pillow(Page),
   bagof( Type,
      xml_pillow_type_check(shallow, Type, Page),
      Type_List ),
   xml_title_result_print('Shallow types of page:', Type_List).


/* deep check of document */

test(xml_pillow, deep_check_page) :-
   xml_test_name_print(deep_check_page),
   xml_file_to_pillow(Page),
   bagof( Type,
      xml_pillow_type_check(deep, Type, Page),
      Type_List ),
   xml_title_result_print(
      'Deep types of page:', Type_List).


/* shallow check of attribute term */

test(xml_pillow, shallow_check_attr_term) :-
   xml_test_name_print(shallow_check_attr_term),
   xml_file_to_pillow(Page),
   xml_term_extract(attr_term, Page, Attr_Term),
   bagof( Type,
      xml_pillow_type_check(shallow, Type, Attr_Term),
      Type_List ),
   xml_title_result_print(
      'Shallow types of attr_term:', Type_List).


/* deep check of document */

test(xml_pillow, deep_check_attr_term) :-
   xml_test_name_print(deep_check_attr_term),
   xml_file_to_pillow(Page),
   xml_term_extract(attr_term, Page, Attr_Term),
   bagof( Type,
      xml_pillow_type_check(deep, Type, Attr_Term),
      Type_List ),
   xml_title_result_print(
      'Deep types of attr_term:', Type_List).


/*** xml_pillow_navigate tests ************************************/


/* root_element */

test(xml_pillow, root_element) :-
   xml_test_name_print(root_element),
   xml_file_to_pillow(Page),
   xml_term_navigate:root_element(Page, Root),
   xml_named_term_print(
      'xml_pillow_navigate:root_element(Page, Root) detects',
      Root).


/* any_element */

test(xml_pillow, any_element) :-
   xml_test_name_print(any_element),
   xml_file_to_pillow(Page),
   findall( Any,
      xml_pillow_navigate:any_element(Page, Any),
      Elements ),
   xml_title_values_print(
      'xml_pillow_navigate:any_element(Page, Any) detects',
      Elements).


/* toplevel_note */

test(xml_pillow, toplevel_note) :-
   xml_test_name_print(toplevel_note),
   xml_file_to_pillow(Page),
   findall( Note,
      xml_pillow_navigate:toplevel_note(Page, Note),
      Notes ),
   xml_title_values_print(
      'xml_pillow_navigate:toplevel_note(Page, Note) detects',
      Notes).


/* child_term */

test(xml_pillow, child_term) :-
   xml_test_name_print(child_term),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   findall( Child,
      xml_pillow_navigate:child_term(Root, Child),
      Children ),
   xml_title_values_print(
      'xml_pillow_navigate:child_term(Root, Child) detects',
      Children).


/* child_element */

test(xml_pillow, child_element) :-
   xml_test_name_print(child_element),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   findall( Child,
      xml_pillow_navigate:child_element(Root, Child),
      Children ),
   xml_title_values_print(
      'xml_pillow_navigate:child_element(Root, Child) detects',
      Children).


/* self_or_descendant */

test(xml_pillow, self_or_descendant) :-
   xml_test_name_print(self_or_descendant),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   findall( Desc,
      xml_pillow_navigate:self_or_descendant(Root, Desc),
      Descs ),
   xml_title_values_print(
      'xml_pillow_navigate:self_or_descendant(Root, Desc) detects',
      Descs).


/* descendant */

test(xml_pillow, descendant) :-
   xml_test_name_print(descendant),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   findall( Desc,
      xml_pillow_navigate:descendant(Root, Desc),
      Descs ),
   xml_title_values_print(
      'xml_pillow_navigate:descendant(Root, Desc) detects',
      Descs).


/* any_children */

test(xml_pillow, any_children) :-
   xml_test_name_print(any_children),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   xml_pillow_navigate:any_children(Root, Children),
   xml_title_values_print(
      'xml_pillow_navigate:any_children(Root, Children) detects',
      Children).


/* element_children */

test(xml_pillow, element_children) :-
   xml_test_name_print(element_children),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   xml_pillow_navigate:element_children(Root, Children),
   xml_title_values_print(
      'xml_pillow_navigate:element_children(Root, Children) detects',
      Children).


/* id_element */

test(xml_pillow, id_element) :-
   xml_test_name_print(id_element),
   xml_file_to_pillow(Page),
   xml_pillow_navigate:id_element(Page, "RUP", Element),
   xml_named_term_print(
      'xml_pillow_navigate:id_element(Root, \"RUP\", Element)',
      Element).


/* text_pattern_element */

test(xml_pillow, text_pattern_element) :-
   xml_test_name_print(text_pattern_element),
   xml_file_to_pillow(Page),
   xml_pillow_navigate:text_pattern_element(Page, "Ru*", Element),
   xml_named_term_print(
      'xml_pillow_navigate:text_pattern_element(Root, \"Ru*\", Element)',
      Element).


/* text_content_element */

test(xml_pillow, text_content_element) :-
   xml_test_name_print(text_content_element),
   xml_file_to_pillow(Page),
   xml_pillow_navigate:text_content_element(Page, "Prog", Element),
   xml_named_term_print(
      'xml_pillow_navigate:text_content_element(Root, \"Prog\", Element)',
      Element).


/* named_element */

test(xml_pillow, named_element) :-
   xml_test_name_print(named_element),
   xml_file_to_pillow(Page),
   findall( Element,
      xml_pillow_navigate:named_element(Page, script, Element),
      Elements ),
   xml_title_values_print(
      'xml_pillow_navigate:named_element(Page, script, Element) detects',
      Elements).


/* named_child_element */

test(xml_pillow, named_child_element) :-
   xml_test_name_print(named_child_element),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   findall( Element,
      xml_pillow_navigate:named_child_element(Root, script, Element),
      Elements ),
   xml_title_values_print(
      'xml_pillow_navigate:named_child_element(Root, script, Element) detects',
      Elements).


/* father_element */

test(xml_pillow, father_element) :-
   xml_test_name_print(father_element),
   xml_file_to_pillow(Page),
   xml_term_extract(empty, Page, Empty),
   xml_pillow_navigate:father_element(Page, Empty, Element),
   xml_named_term_print(
      'xml_pillow_navigate:father_element(Page, Empty, Element) detects',
      Element).


/* ancestor */

test(xml_pillow, ancestor) :-
   xml_test_name_print(ancestor),
   xml_file_to_pillow(Page),
   xml_term_extract(empty, Page, Empty),
   findall( Element,
      xml_pillow_navigate:ancestor(Page, Empty, Element),
      Ancestors ),
   xml_title_values_print(
      'xml_pillow_navigate:ancestor(Page, Empty, Element) detects',
      Ancestors).


/* self_or_ancestor */

test(xml_pillow, self_or_ancestor) :-
   xml_test_name_print(self_or_ancestor),
   xml_file_to_pillow(Page),
   xml_term_extract(empty, Page, Empty),
   findall( Element,
      xml_pillow_navigate:self_or_ancestor(Page, Empty, Element),
      Elements ),
   xml_title_values_print(
      'xml_pillow_navigate:self_or_ancestor(Page, Empty, Element) detects',
      Elements).


/* global_successor */

test(xml_pillow, global_successor) :-
   xml_test_name_print(global_successor),
   xml_file_to_pillow(Page),
   xml_term_extract(last_text_child, Page, Last_Text_Child),
   xml_pillow_navigate:global_successor(Page, Last_Text_Child, Element),
   xml_named_term_print(
      'xml_pillow_navigate:global_successor(Page, Last_Text_Child, Element)',
      Element).


/* global_before */

test(xml_pillow, global_before) :-
   xml_test_name_print(global_before),
   xml_file_to_pillow(Page),
   xml_term_extract(empty, Page, Empty),
   findall( Element,
      xml_pillow_navigate:global_before(Page, Element, Empty),
      Elements ),
   xml_title_values_print(
      'xml_pillow_navigate:global_before(Page, Element, Empty) detects',
      Elements).


/* global_after */

test(xml_pillow, global_after) :-
   xml_test_name_print(global_after),
   xml_file_to_pillow(Page),
   xml_term_extract(empty, Page, Empty),
   findall( Element,
      xml_pillow_navigate:global_after(Page, Element, Empty),
      Elements ),
   xml_title_values_print(
      'xml_pillow_navigate:global_before(Page, Element, Empty) detects',
      Elements).


/* global_predecessor */

test(xml_pillow, global_predecessor) :-
   xml_test_name_print(global_predecessor),
   xml_file_to_pillow(Page),
   xml_term_extract(empty, Page, Empty),
   xml_pillow_navigate:global_predecessor(Page, Empty, Element),
   xml_named_term_print(
      'xml_pillow_navigate:global_predecessor(Page, Empty, Element)',
      Element).


/* local_before */

test(xml_pillow, local_before) :-
   xml_test_name_print(local_before),
   xml_file_to_pillow(Page),
   xml_term_extract(last_text_child, Page, Last_Text_Child),
   findall( Element,
      xml_pillow_navigate:local_before(Page, Element, Last_Text_Child),
      Elements ),
   xml_title_values_print(
      'xml_pillow_navigate:local_before(Page, Element, Last_Text_Child) detects',
      Elements).


/* local_after */

test(xml_pillow, local_after) :-
   xml_test_name_print(local_after),
   xml_file_to_pillow(Page),
   xml_term_extract(last_text_child, Page, Last_Text_Child),
   findall( Element,
      xml_pillow_navigate:local_after(Page, Last_Text_Child, Element),
      Elements ),
   xml_title_values_print(
      'xml_pillow_navigate:local_after(Page, Last_Text_Child, Element) detects',
      Elements).


/* nth_child_element */

test(xml_pillow, nth_child_element) :-
   xml_test_name_print(nth_child_element),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   xml_pillow_navigate:nth_child_element(Root, 3, Element),
   xml_named_term_print(
      'xml_pillow_navigate:nth_child_element(Root, 3, Element)',
      Element).


/* nth_child_term */

test(xml_pillow, nth_child_term) :-
   xml_test_name_print(nth_child_term),
   xml_file_to_pillow(Page),
   xml_term_extract(text_element, Page, Text_Element),
   xml_pillow_navigate:nth_child_term(Text_Element, 1, Term),
   xml_named_term_print(
      'xml_pillow_navigate:nth_child_term(Text_Element, 1, Term)',
      Term).


/* reference_element */

test(xml_pillow, reference_element) :-
   xml_test_name_print(reference_element),
   xml_file_to_pillow(Page),
   xml_term_extract(id_ref, Page, ID_Ref),
   xml_pillow_navigate:reference_element(Page, ID_Ref, Attrib, Element),
   xml_named_term_print(
      'xml_pillow_navigate:reference_element(
         ID_Ref, Page, Attrib, Element)',
      Element),
   string_to_list(Attr_String, Attrib),
   xml_name_value_print('Name of reference attribute', Attr_String).


/* multi_reference_element */

test(xml_pillow, multi_reference_element) :-
   xml_test_name_print(multi_reference_element),
   xml_file_to_pillow(Page),
   xml_term_extract(id_multi_ref, Page, ID_Multi_Ref),
   findall( Element,
      xml_pillow_navigate:multi_reference_element(
         Page, ID_Multi_Ref, _, Element),
      Elements ),
   xml_title_values_print(
      'xml_pillow_navigate:multi_reference_element(
         Page, ID_Multi_Ref, Attrib, Element)',
      Elements).


/* nth_local_successor */

test(xml_pillow, nth_local_successor) :-
   xml_test_name_print(nth_local_successor),
   xml_file_to_pillow(Page),
   xml_term_extract(text_child, Page, Text_Child),
   xml_pillow_navigate:nth_local_successor(Page, Text_Child, 2, Element),
   xml_named_term_print(
      'xml_pillow_navigate:nth_local_successor(
         Page, Text_Child, 2, Element)',
      Element).


/* nth_local_predecessor */

test(xml_pillow, nth_local_predecessor) :-
   xml_test_name_print(nth_local_predecessor),
   xml_file_to_pillow(Page),
   xml_term_extract(text_child, Page, Text_Child),
   xml_pillow_navigate:nth_local_predecessor(Page, Text_Child, 1, Element),
   xml_named_term_print(
      'xml_pillow_navigate:nth_local_predecessor(
         Page, Text_Child, 1, Element)',
      Element).


/* sibling_element */

test(xml_pillow, sibling_element) :-
   xml_test_name_print(sibling_element),
   xml_file_to_pillow(Page),
   xml_term_extract(text_child, Page, Text_Child),
   findall( Element,
      xml_pillow_navigate:sibling_element(Page, Text_Child, Element, _),
      Elements ),
   xml_title_values_print(
      'xml_pillow_navigate:sibling_element(
         Page, Text_Child, Element, _) detects',
      Elements).


/* child_element_range */

test(xml_pillow, child_element_range) :-
   xml_test_name_print(child_element_range),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   findall( Children,
      xml_pillow_navigate:child_element_range(Root, 2, 3, Children),
      List ),
   xml_title_values_print(
      'xml_pillow_navigate:child_element_range(
         Root, 2, 3, Children) detects',
      List).


/* child_term_range */

test(xml_pillow, child_term_range) :-
   xml_test_name_print(child_term_range),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   findall( Terms,
      xml_pillow_navigate:child_term_range(Root, 2, 5, Terms),
      List ),
   xml_title_values_print(
      'xml_pillow_navigate:child_term_range(Root, 2, 5, Terms) detects',
      List).


/*** xml_pillow_query tests ***************************************/


/* attribute_name */

test(xml_pillow, attribute_name) :-
   xml_test_name_print(attribute_name),
   xml_file_to_pillow(Page),
   xml_term_extract(attr_element, Page, Attr_Element),
   xml_pillow_query:attribute_name(Attr_Element, "type"),
   xml_test_value_print(
      'xml_pillow_query:attribute_name(Attr_Element, "type") successful.').


/* attribute_name */

test(xml_pillow, attribute_name_2) :-
   xml_test_name_print(attribute_name_2),
   xml_file_to_pillow(Page),
   xml_term_extract(attr_term, Page, Attr_Term),
   xml_pillow_query:attribute_name(Attr_Term, Name),
   xml_name_value_print(
      'xml_pillow_query:attribute_name(Attr_Term, Name)',
      Name).


/* attribute_value */

test(xml_pillow, attribute_value) :-
   xml_test_name_print(attribute_value),
   xml_file_to_pillow(Page),
   xml_term_extract(attr_term, Page, Attr_Term),
   xml_pillow_query:attribute_value(Attr_Term, Value),
   xml_name_value_print(
      'xml_pillow_query:attribute_value(Attr_Term, Value)',
      Value).


/* attributes of element */

test(xml_pillow, attributes) :-
   xml_test_name_print(attributes),
   xml_file_to_pillow(Page),
   xml_term_extract(attr_element, Page, Attr_Element),
   xml_pillow_query:attributes(Attr_Element, Attributes),
   xml_name_value_print(
      'xml_pillow_query:attributes(Attr_Element, Attributes)',
      Attributes).


/* attributes of XML declaration */

test(xml_pillow, attributes2) :-
   xml_test_name_print(attributes2),
   xml_file_to_pillow(Page),
   xml_term_extract(root_decl, Page, Root_Decl),
   xml_pillow_query:attributes(Root_Decl, Attributes),
   xml_name_value_print(
      'xml_pillow_query:attributes(Root_Decl, Attributes)',
      Attributes).


/* attribute_names */

test(xml_pillow, attribute_names) :-
   xml_test_name_print(attribute_names),
   xml_file_to_pillow(Page),
   xml_term_extract(attr_element, Page, Attr_Element),
   xml_pillow_query:attribute_names(Attr_Element, Names),
   xml_name_value_print(
      'xml_pillow_query:attribute_names(Attr_Element, Names)',
      Names).


/* element_name */

test(xml_pillow, element_name) :-
   xml_test_name_print(element_name),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   xml_pillow_query:element_name(Root, Name),
   xml_name_value_print(
      'xml_pillow_query:element_name(Root, Name)', Name).


/* note_type */

test(xml_pillow, note_type) :-
   xml_test_name_print(note_type),
   xml_file_to_pillow(Page),
   xml_term_extract(root_decl, Page, Root_Decl),
   xml_pillow_query:note_type(Type, Root_Decl),
   xml_name_value_print(
      'xml_pillow_query:note_type(Type, Root_Decl)', Type).


/* shallow_text_terms */

test(xml_pillow, shallow_text_terms) :-
   xml_test_name_print(shallow_text_terms),
   xml_file_to_pillow(Page),
   xml_term_extract(text_element, Page, Text_Element),
   xml_pillow_query:shallow_text_terms(Text_Element, Terms),
   xml_title_values_print(
      'xml_pillow_query:shallow_text_terms(Text_Element, Terms) detects',
      Terms).


/* deep_text_terms */

test(xml_pillow, deep_text_terms) :-
   xml_test_name_print(deep_text_terms),
   xml_file_to_pillow(Page),
   xml_term_extract(text_child, Page, Text_Child),
   xml_pillow_query:deep_text_terms(Text_Child, Terms),
   xml_title_values_print(
      'xml_pillow_query:deep_text_terms(Text_Child, Terms) detects',
      Terms).


/* shallow_text */

test(xml_pillow, shallow_text) :-
   xml_test_name_print(shallow_text),
   xml_file_to_pillow(Page),
   xml_term_extract(text_element, Page, Text_Element),
   xml_pillow_query:shallow_text(Text_Element, Terms),
   xml_named_term_print(
      'xml_pillow_query:shallow_text(Text_Element, Terms)',
      Terms).


/* deep_text */

test(xml_pillow, deep_text) :-
   xml_test_name_print(deep_text),
   xml_file_to_pillow(Page),
   xml_term_extract(text_child, Page, Text_Child),
   xml_pillow_query:deep_text(Text_Child, Terms),
   xml_named_term_print(
      'xml_pillow_query:deep_text(Text_Child, Terms)',
      Terms).


/* element_namespace */

test(xml_pillow, element_namespace) :-
   xml_test_name_print(element_namespace),
   Element = env('ns:local', [], []),
   xml_pillow_query:element_namespace(Element, Namespace),
   xml_named_term_print('Element with namespace', Element),
   xml_named_term_print(
      'xml_pillow_query:element_namespace(env(\'ns:local\', [], []), Namespace)',
      Namespace).


/* local_element_name */

test(xml_pillow, local_element_name) :-
   xml_test_name_print(local_element_name),
   Element = env('ns:local', [], []),
   xml_pillow_query:local_element_name(Element, Local_Name),
   xml_named_term_print('Element with namespace', Element),
   xml_named_term_print(
      'xml_pillow_query:local_element_name(env(\'ns:local\', [], []), Local_Name)',
      Local_Name).


/* attribute_value */

test(xml_pillow, attribute_value) :-
   xml_test_name_print(attribute_value),
   xml_file_to_pillow(Page),
   xml_term_extract(attr_element, Page, Attr_Element),
   xml_pillow_query:attribute_value(Attr_Element, "type", Attr_Value),
   xml_named_term_print(
      'xml_pillow_query:attribute_value(Attr_Element, \"type\", Attr_Value)',
      Attr_Value).


/* shallow_text/3 */

test(xml_pillow, shallow_text_3) :-
   xml_test_name_print(shallow_text_3),
   xml_file_to_pillow(Page),
   xml_term_extract(text_element, Page, Text_Element),
   xml_pillow_query:shallow_text(Text_Element, [trim], Trimmed),
   xml_named_term_print(
      'xml_pillow_query:shallow_text(Text_Element, [trim], Trimmed)',
      Trimmed).


/* deep_text/3 */

test(xml_pillow, deep_text_3) :-
   xml_test_name_print(deep_text_3),
   xml_file_to_pillow(Page),
   xml_term_extract(text_child, Page, Text_Child),
   xml_pillow_query:deep_text(
      Text_Child, [flow_normalize, space_normalize], Text),
   xml_named_term_print(
      'xml_pillow_query:deep_text(Text_Child, [flow_normalize, space_normalize], Text)',
      Text).


/* local_element_position */

test(xml_pillow, local_element_position) :-
   xml_test_name_print(local_element_position),
   xml_file_to_pillow(Page),
   xml_term_extract(text_child, Page, Text_Child),
   xml_pillow_query:local_element_position(Text_Child, Page, Position),
   xml_name_value_print(
      'xml_pillow_query:local_element_position(Text_Child, Page, Position)',
      Position).


/* local_term_position */

test(xml_pillow, local_term_position) :-
   xml_test_name_print(local_term_position),
   xml_file_to_pillow(Page),
   xml_term_extract(text_child, Page, Text_Child),
   xml_pillow_query:local_term_position(Text_Child, Page, Position),
   xml_name_value_print(
      'xml_pillow_query:local_term_position(Text_Child, Page, Position)',
      Position).


/* element_depth */

test(xml_pillow, element_depth) :-
   xml_test_name_print(element_depth),
   xml_file_to_pillow(Page),
   xml_term_extract(text_child, Page, Text_Child),
   xml_pillow_query:element_depth(Text_Child, Page, Depth),
   xml_name_value_print(
      'xml_pillow_query:element_depth(Text_Child, Page, Depth)',
      Depth).


/*** xml_pillow_transform tests ***********************************/


/* term_to_page */

test(xml_pillow, term_to_page) :-
   xml_test_name_print(term_to_page),
   xml_file_to_pillow(Page),
   xml_term_extract(text_child, Page, Text_Child),
   xml_pillow_transform:term_to_page(Text_Child, New_Page),
   xml_named_term_print(always_deep,
      'xml_pillow_transform:term_to_page(Text_Child, New_Page)',
      New_Page).


/* comment_create */

test(xml_pillow, comment_create) :-
   xml_test_name_print(comment_create),
   xml_pillow_transform:comment_create("comment text", Comment),
   xml_named_term_print(
      'xml_pillow_transform:comment_create(\"comment text\", Comment)',
      Comment).


/* declaration_create */

test(xml_pillow, declaration_create) :-
   xml_test_name_print(declaration_create),
   xml_pillow_transform:declaration_create("declaration text", Declaration),
   xml_named_term_print(
      'xml_pillow_transform:declaration_create(\"declaration text\", Declaration)',
      Declaration).


/* xml_decl_create */

test(xml_pillow, xml_decl_create) :-
   xml_test_name_print(xml_decl_create),
   xml_pillow_transform:xml_decl_create([=(version,"1.0")], XML_Decl),
   xml_named_term_print(
      'xml_pillow_transform:xml_decl_create([=(version,\"1.0\")], XML_Decl)',
      XML_Decl).


/* document_transform */

test(xml_pillow, document_transform) :-
   xml_test_name_print(document_transform),
   xml_file_to_pillow(Page),
   xml_pillow_transform:document_transform(Page, whitespace_filter(Page), Filtered),
   xml_named_term_print(always_deep,
      'xml_pillow_transform:document_transform(Page, whitespace_filter(Page), Filtered)',
      Filtered),
   xml_name_value_print('Whitespace filtered data structure', Filtered).


/* page_term_remove */

test(xml_pillow, page_term_remove) :-
   xml_test_name_print(page_term_remove),
   xml_file_to_pillow(Page),
   xml_term_extract(text_child, Page, Text_Child),
   xml_pillow_transform:page_term_remove(Page, Text_Child, New_Page),
   xml_named_term_print(always_deep,
      'xml_pillow_transform:page_term_remove(Page, Text_Child, New_Page)',
      New_Page).


/* children_replace */

test(xml_pillow, children_replace) :-
   xml_test_name_print(children_replace),
   xml_file_to_pillow(Page),
   xml_term_extract(text_child, Page, Text_Child),
   xml_term_extract(empty, Page, Empty),
   xml_pillow_transform:children_replace(Text_Child, Empty, New_Term),
   xml_named_term_print(always_deep,
      'xml_pillow_transform:children_replace(Text_Child, Empty, New_Term)',
      New_Term).


/* child_remove */

test(xml_pillow, child_remove) :-
   xml_test_name_print(child_remove),
   xml_file_to_pillow(Page),
   xml_term_extract(root, Page, Root),
   xml_term_extract(text_element, Page, Text_Element),
   xml_pillow_transform:child_remove(Root, Text_Element, New_Root),
   xml_named_term_print(always_deep,
      'xml_pillow_transform:child_remove(Root, Text_Element, New_Root)',
      New_Root).


/* attribute_remove */

test(xml_pillow, attribute_remove) :-
   xml_test_name_print(attribute_remove),
   xml_file_to_pillow(Page),
   xml_term_extract(attr_element, Page, Attr_Element),
   xml_pillow_transform:attribute_remove(Attr_Element, "type", New_Element),
   xml_named_term_print(always_deep,
      'xml_pillow_transform:attribute_remove(Attr_Element, \"type\", New_Element)',
      New_Element).


/* html_logical_attribute_add */

test(xml_pillow, html_logical_attribute_add) :-
   xml_test_name_print(html_logical_attribute_add),
   xml_file_to_pillow(Page),
   xml_term_extract(attr_element, Page, Attr_Element),
   xml_pillow_transform:html_logical_attribute_add(Attr_Element, "html", New_Element),
   xml_named_term_print(always_deep,
      'xml_pillow_transform:html_logical_attribute_add(Attr_Element, "html", New_Element)',
      New_Element).


/* element_create/2 */

test(xml_pillow, element_create) :-
   xml_test_name_print(element_create),
   xml_pillow_transform:element_create(ename, [att="value"], Element),
   xml_named_term_print(always_deep,
      'xml_pillow_transform:element_create(ename, [att=\"value\"], Element)',
      Element).


/* element_create/3 */

test(xml_pillow, element_create_2) :-
   xml_test_name_print(element_create_2),
   xml_pillow_transform:element_create(ename, [att="value"], ["Text"], Element),
   xml_named_term_print(always_deep,
      'xml_pillow_transform:element_create(ename, [att=\"value\"], [\"Text\"], Element)',
      Element).


/* attribute_create */

test(xml_pillow, attribute_create) :-
   xml_test_name_print(attribute_create),
   xml_pillow_transform:attribute_create(anyname, "value", Attribute),
   xml_named_term_print(
      'xml_pillow_transform:attribute_create(attr_name, "value", Attribute)',
      Attribute).


/* element_name_edit */

test(xml_pillow, element_name_edit) :-
   xml_test_name_print(element_name_edit),
   xml_file_to_pillow(Page),
   xml_term_extract(text_element, Page, Text_Element),
   xml_pillow_transform:element_name_edit(Text_Element, new_name, New_Name),
   xml_named_term_print(
      'xml_pillow_transform:element_name_edit(Root, new_name, New_Name)',
      New_Name).


/* note_text_edit */

test(xml_pillow, note_text_edit) :-
   xml_test_name_print(note_text_edit),
   xml_file_to_pillow(Page),
   xml_term_extract(comment, Page, Comment),
   xml_pillow_transform:note_text_edit(Comment, "new comment text", New_Comment),
   xml_named_term_print(
      'xml_pillow_transform:note_text_edit(Comment, "new comment text", New_Comment)',
      New_Comment).


/* page_term_replace */

test(xml_pillow, page_term_replace) :-
   xml_test_name_print(page_term_replace),
   xml_file_to_pillow(Page),
   xml_term_extract(text_element, Page, Text_Element),
   xml_pillow_transform:page_term_replace(Page, Text_Element, ["Replaced Term"], New_Page),
   xml_named_term_print(always_deep,
      'xml_pillow_transform:page_term_replace(Page, Text_Element, ["Replaced Term"], New_Page)',
      New_Page).


/* child_insert */

test(xml_pillow, child_insert) :-
   xml_test_name_print(child_insert),
   xml_file_to_pillow(Page),
   xml_term_extract(text_element, Page, Text_Element),
   xml_pillow_transform:child_insert(Text_Element, ["Inserted Term"], 4, New_Element),
   xml_named_term_print(always_deep,
      'xml_pillow_transform:child_insert(Text_Element, ["Inserted Term"], 4, New_Element)',
      New_Element).


/* attribute_edit */

test(xml_pillow, attribute_edit) :-
   xml_test_name_print(attribute_edit),
   xml_file_to_pillow(Page),
   xml_term_extract(attr_element, Page, Attr_Element),
   xml_pillow_transform:attribute_edit(Attr_Element, "type", "NewML", New_Element),
   xml_named_term_print(
      'xml_pillow_transform:attribute_edit(Attr_Element, "type", "NewML", New_Element)',
      New_Element).


/* attribute_add */

test(xml_pillow, attribute_add) :-
   xml_test_name_print(attribute_add),
   xml_file_to_pillow(Page),
   xml_term_extract(attr_element, Page, Attr_Element),
   xml_pillow_transform:attribute_add(Attr_Element, "new", "value", New_Element),
   xml_named_term_print(
      'xml_pillow_transform:attribute_add(Attr_Element, "new", "value", New_Element)',
      New_Element).


/* subtree_copy */

test(xml_pillow, subtree_copy) :-
   xml_test_name_print(subtree_copy),
   xml_file_to_pillow(Page),
   xml_term_extract(attr_element, Page, Attr_Element),
   xml_pillow_transform:subtree_copy(Attr_Element, newname, [], New_Element),
   xml_named_term_print(always_deep,
      'xml_pillow_transform:subtree_copy(Attr_Element, newname, [], New_Element)',
      New_Element).


/* page_term_insert */

test(xml_pillow, page_term_insert) :-
   xml_test_name_print(page_term_insert),
   xml_file_to_pillow(Page),
   xml_term_extract(empty, Page, Empty),
   xml_term_extract(text_element, Page, Text_Element),
   xml_pillow_transform:page_term_insert(Page, Empty, Text_Element, 1, New_Page),
   xml_named_term_print(always_deep,
      'xml_pillow_transform:page_term_insert(Page, Empty, Text_Element, 1, New_Page)',
      New_Page).


/*** ascii_text tests *********************************************/


/* list_empty */

test(ascii_text, list_empty) :-
   xml_test_name_print(list_empty),
   ascii_text:list_empty([]),
   \+ ascii_text:list_empty([content, content]),
   xml_test_value_print(
      'ascii_text:list_empty successful for [] -> fails otherwise').


/* list_to_pairlist */

test(ascii_text, list_to_pairlist) :-
   xml_test_name_print(list_to_pairlist),
   ascii_text:list_to_pairlist([a, b], [aa, bb], Pairs),
   xml_name_value_print(
      'ascii_text:list_to_pairlist([a, b], [aa, bb], Pairs)',
      Pairs).


/* list_compare */

test(ascii_text, list_compare) :-
   String_1 = "abc",
   string_to_list(String_1, List_1),
   String_2 = "abd",
   string_to_list(String_2, List_2),
   xml_test_name_print(list_compare),
   ascii_text:list_compare(Delta, List_1, List_2),
   xml_name_value_print(
      'ascii_text:list_compare(Delta, List_1, List_2)',
      Delta).


/* list_nth_replace */

test(ascii_text, list_nth_replace) :-
   xml_test_name_print(list_nth_replace),
   ascii_text:list_nth_replace([a, b, c, d, e], 4, zzz, Result),
   xml_name_value_print(
      'ascii_text:list_nth_replace([a, b, c, d, e], 4, zzz, Result)',
      Result).


/* list_unique_replace */

test(ascii_text, list_unique_replace) :-
   xml_test_name_print(list_unique_replace),
   ascii_text:list_unique_replace([a, b, c, b, c, b, a], b, zzz, Result),
   xml_name_value_print(
      'ascii_text:list_unique_replace([a, b, c, b, c, b, a], b, zzz, Result)',
      Result).


/* list_replace */

test(ascii_text, list_replace) :-
   xml_test_name_print(list_replace),
   ascii_text:list_replace([a, b, c, b, c, b, a], b, zzz, Result),
   xml_name_value_print(
      'ascii_text:list_replace([a, b, c, b, c, b, a], b, zzz, Result)',
      Result).


/* list_copy */

test(ascii_text, list_copy) :-
   xml_test_name_print(list_copy),
   ascii_text:list_copy([a, b, c, b, c, b, a], 2, 4, Result),
   xml_name_value_print(
      'ascii_text:list_copy([a, b, c, b, c, b, a], 2, 4, Result)',
      Result).


/* list_insert */

test(ascii_text, list_insert) :-
   xml_test_name_print(list_insert),
   ascii_text:list_insert([a, b, c, b, c, b, a], zzz, 4, Result),
   xml_name_value_print(
      'ascii_text:list_insert([a, b, c, b, c, b, a], zzz, 4, Result)',
      Result).


/* contains */

test(ascii_text, contains) :-
   String_1 = "abcdefghij",
   string_to_list(String_1, List_1),
   String_2 = "efgh",
   string_to_list(String_2, List_2),
   xml_test_name_print(contains),
   ascii_text:contains(List_1, List_2),
   xml_test_value_print(
      'ascii_text:contains("abcdefghij", "efgh") successful').


/* last_index_of */

test(ascii_text, last_index_of) :-
   String = "abcbcba",
   string_to_list(String, List),
   xml_test_name_print(last_index_of),
   ascii_text:last_index_of(List, 98, Index),
   xml_name_value_print(
      'ascii_text:last_index_of("abcbcba", 98, Index)',
      Index).


/* index_of */

test(ascii_text, index_of) :-
   String = "abcbcba",
   string_to_list(String, List),
   xml_test_name_print(index_of),
   ascii_text:index_of(List, 98, Index),
   xml_name_value_print(
      'ascii_text:index_of("abcbcba", 98, Index)',
      Index).


/* start_split */

test(ascii_text, start_split) :-
   String = "abcbcba",
   string_to_list(String, List),
   xml_test_name_print(start_split),
   ascii_text:start_split(List, 3, Prefix, Suffix),
   xml_name_value_print(
      'ascii_text:start_split("abcbcba", 3, Prefix, Suffix) -> Prefix',
      Prefix),
   xml_name_value_print(
      'ascii_text:start_split("abcbcba", 3, Prefix, Suffix) -> Suffix',
      Suffix).


/* end_split */

test(ascii_text, end_split) :-
   String = "abcbcba",
   string_to_list(String, List),
   xml_test_name_print(end_split),
   ascii_text:end_split(List, 2, Suffix, Prefix),
   xml_name_value_print(
      'ascii_text:end_split("abcbcba", 2, Suffix, Prefix) -> Prefix',
      Prefix),
   xml_name_value_print(
      'ascii_text:end_split("abcbcba", 2, Suffix, Prefix) -> Suffix',
      Suffix).


/* ends_with */

test(ascii_text, ends_with) :-
   String_1 = "abcbcba",
   string_to_list(String_1, List_1),
   String_2 = "bcba",
   string_to_list(String_2, List_2),
   xml_test_name_print(ends_with),
   ascii_text:ends_with(List_1, List_2),
   xml_test_value_print(
      'ascii_text:ends_with("abcbcba", "bcba") successful').


/* char_split */

test(ascii_text, char_split) :-
   String = "abccbcba",
   string_to_list(String, List),
   xml_test_name_print(char_split),
   ascii_text:char_split(List, 99, Split),
   xml_name_value_print(
      'ascii_text:char_split("abccbcba", 99, Split)',
      Split).


/* char_compose */

test(ascii_text, char_compose) :-
   String_1 = "ab",
   string_to_list(String_1, List_1),
   String_2 = "zzz",
   string_to_list(String_2, List_2),
   String_3 = "water",
   string_to_list(String_3, List_3),
   xml_test_name_print(char_compose),
   ascii_text:char_compose([List_1, List_2, List_3], 124, Text),
   xml_name_value_print(
      'ascii_text:char_compose(["ab", "zzz", "water"], 124, Text)',
      Text).


/* char_compose_prefix */

test(ascii_text, char_compose_prefix) :-
   String_1 = "ab",
   string_to_list(String_1, List_1),
   String_2 = "zzz",
   string_to_list(String_2, List_2),
   String_3 = "water",
   string_to_list(String_3, List_3),
   xml_test_name_print(char_compose_prefix),
   ascii_text:char_compose_prefix([List_1, List_2, List_3], 124, Text),
   xml_name_value_print(
      'ascii_text:char_compose_prefix(["ab", "zzz", "water"], 124, Text)',
      Text).


/* char_compose_suffix */

test(ascii_text, char_compose_suffix) :-
   String_1 = "ab",
   string_to_list(String_1, List_1),
   String_2 = "zzz",
   string_to_list(String_2, List_2),
   String_3 = "water",
   string_to_list(String_3, List_3),
   xml_test_name_print(char_compose_suffix),
   ascii_text:char_compose_suffix([List_1, List_2, List_3], 124, Text),
   xml_name_value_print(
      'ascii_text:char_compose_suffix(["ab", "zzz", "water"], 124, Text)',
      Text).


/* trim */

test(ascii_text, trim) :-
   String = " \n\rcore text\r   \n",
   string_to_list(String, List),
   xml_test_name_print(trim),
   ascii_text:trim(List, Text),
   xml_name_value_print(
      'ascii_text:trim(" \\n\\rcore text\\r   \\n", Text)',
      Text).


/* trim_head */

test(ascii_text, trim_head) :-
   String = " \n\rcore text\r   \n",
   string_to_list(String, List),
   xml_test_name_print(trim_head),
   ascii_text:trim_head(List, Text),
   xml_name_value_print(
      'ascii_text:trim_head(" \\n\\rcore text\\r   \\n", Text)',
      Text).


/* trim_tail */

test(ascii_text, trim_tail) :-
   String = " \n\rcore text\r   \n",
   string_to_list(String, List),
   xml_test_name_print(trim_tail),
   ascii_text:trim_tail(List, Text),
   xml_name_value_print(
      'ascii_text:trim_tail(" \\n\\rcore text\\r   \\n", Text)',
      Text).


/* to_lower_case */

test(ascii_text, to_lower_case) :-
   String = "AbcBCBa_09A",
   string_to_list(String, List),
   xml_test_name_print(to_lower_case),
   ascii_text:to_lower_case(List, Text),
   xml_name_value_print(
      'ascii_text:to_lower_case("AbcBCBa_09A", Text)',
      Text).


/* to_upper_case */

test(ascii_text, to_upper_case) :-
   String = "AbcBCBa_09A",
   string_to_list(String, List),
   xml_test_name_print(to_upper_case),
   ascii_text:to_upper_case(List, Text),
   xml_name_value_print(
      'ascii_text:to_upper_case("AbcBCBa_09A", Text)',
      Text).


/* before_signal_char */

test(ascii_text, before_signal_char) :-
   String = "abcbcba",
   string_to_list(String, List),
   xml_test_name_print(before_signal_char),
   ascii_text:before_signal_char(List, 99, Text),
   xml_name_value_print(
      'ascii_text:before_signal_char("abcbcba", 99, Text)',
      Text).


/* after_signal_char */

test(ascii_text, after_signal_char) :-
   String = "abcbcba",
   string_to_list(String, List),
   xml_test_name_print(after_signal_char),
   ascii_text:after_signal_char(List, 99, Text),
   xml_name_value_print(
      'ascii_text:after_signal_char("abcbcba", 99, Text)',
      Text).


/* is_whitespace */

test(ascii_text, is_whitespace) :-
   xml_test_name_print(is_whitespace),
   ascii_text:is_whitespace(32),
   xml_test_value_print(
      'ascii_text:is_whitespace(32) successful').


/* is_white */

test(ascii_text, is_white) :-
   String = "\r   \t\n  \r\b",
   string_to_list(String, List),
   xml_test_name_print(is_white),
   ascii_text:is_white(List),
   xml_test_value_print(
      'ascii_text:is_white("\\r   \\t\\n  \\r\\b") successful').


/* is_visible */

test(ascii_text, is_visible) :-
   String = "\r   core text\t\n  \r\b",
   string_to_list(String, List),
   xml_test_name_print(is_visible),
   ascii_text:is_visible(List),
   xml_test_value_print(
      'ascii_text:is_visible("\\r   core text\\t\\n  \\r\\b") successful').


/* flow_normalize */

test(ascii_text, flow_normalize) :-
   String = "Text 1   \rText 2\n  \rText 3  ",
   string_to_list(String, List),
   xml_test_name_print(flow_normalize),
   ascii_text:flow_normalize(List, Text),
   xml_name_value_print(
      'ascii_text:flow_normalize(["Text 1   \\rText 2\\n  \\rText 3  "], Text)',
      Text).


/* white_to_space */

test(ascii_text, white_to_space) :-
   String = "Text 1   \rText 2\n  \rText 3  ",
   string_to_list(String, List),
   xml_test_name_print(white_to_space),
   ascii_text:white_to_space(List, Text),
   xml_name_value_print(
      'ascii_text:white_to_space(["Text 1   \\rText 2\\n  \\rText 3  "], Text)',
      Text).


/* space_normalize */

test(ascii_text, space_normalize) :-
   String_1 = "Text 1   ",
   string_to_list(String_1, List_1),
   String_2 = "\rText 2\n  \r",
   string_to_list(String_2, List_2),
   String_3 = "Text 3  ",
   string_to_list(String_3, List_3),
   xml_test_name_print(space_normalize),
   ascii_text:space_normalize([List_1, List_2, List_3], Text),
   xml_name_value_print(
      'ascii_text:space_normalize(["Text 1   ", "\\rText 2\\n  \\r", "Text 3  "], Text)',
      Text).


/* char_print */

test(ascii_text, char_print) :-
   String = "abcdcdcbcba",
   string_to_list(String, List),
   xml_test_name_print(char_print),
   ascii_text:char_print(List),
   xml_test_value_print(
      'ascii_text:char_print successful').


/* is_ascii_text */

test(ascii_text, is_ascii_text) :-
   String = "abcdcdcbcba",
   string_to_list(String, List),
   xml_test_name_print(is_ascii_text),
   ascii_text:is_ascii_text(List),
   xml_test_value_print(
      'ascii_text:is_ascii_text("abcdcdcbcba") successful').


/*** hash_table tests *********************************************/


/* value_put */

test(hash_table, value_put_get) :-
   xml_test_name_print(value_put_get),
   hash_table:value_put(alpha, root, functor(arg1, arg2)),
   findall(Term,
      hash_table:value_get(alpha, root, Term), Terms),
   xml_title_values_print(
      'value_put of functor(arg1, arg2) -> value_get',
      Terms),
   hash_table:remove_all(alpha).


/* value_put */

test(hash_table, value_put_get_2) :-
   xml_test_name_print(value_put_get_2),
   hash_table:value_put(alpha, root, functor(arg1, arg2)),
   hash_table:value_put(alpha, root, 55),
   findall(Term,
      hash_table:value_get(alpha, root, Term), Terms),
   xml_title_values_print(
      'Repeated value_put of functor(arg1, arg2) and 55 -> value_get',
      Terms),
   hash_table:remove_all(alpha).


/* value_put_unique */

test(hash_table, value_put_unique) :-
   xml_test_name_print(value_put_unique),
   hash_table:value_put_unique(alpha, root, functor(arg1, arg2)),
   hash_table:value_put_unique(alpha, root, 55),
   findall(Term,
      hash_table:value_get(alpha, root, Term), Terms),
   xml_title_values_print(
      'Repeated value_put_unique of functor(arg1, arg2) and 55 -> value_get',
      Terms),
   hash_table:remove_all(alpha).


/* value_remove */

test(hash_table, value_remove) :-
   xml_test_name_print(value_remove),
   hash_table:value_put(alpha, root, functor(arg1, arg2)),
   hash_table:value_put(alpha, leaf, 55),
   hash_table:value_remove(alpha, leaf),
   \+ hash_table:value_get(alpha, leaf, _),
   xml_test_value_print('value_remove after value_put fails correctly.'),
   hash_table:remove_all(alpha).


/* value_remove_first */

test(hash_table, value_remove_first) :-
   xml_test_name_print(value_put_get_2),
   hash_table:value_put(alpha, root, functor(arg1, arg2)),
   hash_table:value_put(alpha, root, 55),
   hash_table:value_remove_first(alpha, root),
   findall(Term,
      hash_table:value_get(alpha, root, Term), Terms),
   xml_title_values_print(
      'value_put of functor(arg1, arg2) and 55 -> value_remove_first -> value_get',
      Terms),
   hash_table:remove_all(alpha).


/* value_remove_all */

test(hash_table, value_remove_all) :-
   xml_test_name_print(value_remove_all),
   hash_table:value_put(alpha, root, 55),
   hash_table:value_put(alpha, root, functor(arg1, arg2)),
   hash_table:value_put(alpha, leaf, 0),
   hash_table:remove_all(alpha),
   \+ hash_table:value_get(alpha, root, _),
   \+ hash_table:value_get(alpha, leaf, _),
   xml_test_value_print('value_get fails completely after remove_all.').


/* key */

test(hash_table, key) :-
   xml_test_name_print(key),
   hash_table:value_put(alpha, 1, functor(arg1, arg2)),
   hash_table:value_put(alpha, 2, 55),
   findall(Term,
      hash_table:key(alpha, Term), Terms),
   xml_title_values_print(
      'Repeated key operation for keys 1 and 2',
      Terms),
   hash_table:remove_all(alpha).


/* keys */

test(hash_table, keys) :-
   xml_test_name_print(keys),
   hash_table:value_put(alpha, 1, functor(arg1, arg2)),
   hash_table:value_put(alpha, 2, 55),
   hash_table:keys(alpha, Keys),
   xml_title_values_print(
      'Keys operation for keys 1 and 2',
      Keys),
   hash_table:remove_all(alpha).


/* elements */

test(hash_table, elements) :-
   xml_test_name_print(elements),
   hash_table:value_put(alpha, root, functor(arg1, arg2)),
   hash_table:value_put(alpha, leaf, 55),
   hash_table:elements(alpha, Elements),
   xml_title_values_print(
      'Elements operation for values functor(arg1, arg2) and 55',
      Elements),
   hash_table:remove_all(alpha).


/* unique_table_key */

test(hash_table, unique_table_key) :-
   xml_test_name_print(unique_table_key),
   hash_table:unique_table_key(Key_1),
   hash_table:unique_table_key(Key_2),
   Difference is Key_2 - Key_1,
   xml_name_value_print(
      'Difference of successive unique table keys',
      Difference),
   hash_table:remove_all(alpha).


/******************************************************************/


