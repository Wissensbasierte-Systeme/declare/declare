

/******************************************************************/
/***                                                            ***/
/***            XML-Pillow:  XML Data Extraction                ***/
/***                                                            ***/
/******************************************************************/


:- use_module(ascii_text).
:- use_module(hash_table).

:- ensure_loaded(library/pillow).

:- use_module(xml_pillow_alternative).
:- use_module(xml_pillow_data_operation).
:- use_module(xml_pillow_examples).
:- use_module(xml_pillow_navigate).
:- use_module(xml_pillow_print).
:- use_module(xml_pillow_query).
:- use_module(xml_pillow_transform).
% :- use_module(xml_pillow_www_support).
:- [ xml_pillow_www_support ].

:- use_module(xml_pillow_field_notation).

:- [xml_pillow_type_check].


/*
:- [web_agent_variables].
:- [web_agent_util].
:- [web_agent_data].
:- [web_agent_action].
:- [web_agent].
*/


:- [xml_pillow_test].


/******************************************************************/


