

/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  Higher Operations with Data           ***/
/***                                                            ***/
/******************************************************************/


:- module( xml_pillow_data_operation, [
      xml_pillow_aggregate/3,
      xml_pillow_aggregate/4,
      collect_list/4,
      collect_set/4,
      join/4,
      pattern_search/3,
      all_satisfy/2,
      any_satisfies/2 ] ).

:- use_module(xml_pillow_navigate).


/*** interface ****************************************************/


/* xml_pillow_aggregate(
         +List, +Extract_Pred, +Command, -Result) <-
      extracts numeric values from element list List by calling
      extraction predicate Extract_Pred and performs standard
      aggregation specified by atom Command. */      

xml_pillow_aggregate(List, Extract_Pred, Command, Result) :-
   maplist( Extract_Pred,
      List, Data ),
   maplist( number_chars,
      Numbers, Data ),
   xml_pillow_d_o_aggregate(Command, Numbers, Result).


/* collect_list(+Operation, +Args, +N, -List) <-
      encapsulates bagof behind another interface, predicate
      Operation is called with arguments Args, the Nth argument
      is collected in list List. */

collect_list(Operation, Args, Result_Index, List) :-
   nth1(Result_Index, Args, Result_Arg),
   bagof( Result_Arg,
      apply(Operation, Args),
      List ).


/* collect_list(+Operation, +Args, +N, -List) <-
      encapsulates setof behind another interface, predicate
      Operation is called with arguments Args, the Nth argument
      is collected in set Set. */

collect_set(Operation, Args, Result_Index, List) :-
   nth1(Result_Index, Args, Result_Arg),
   setof( Result_Arg,
      apply(Operation, Args),
      List ).


/* join(+Tuples_1, +Tuples_2, +Join_Pred, -Result) <-
      joins tuple lists Tuples_1, Tuples_2 with the join condition
      per tuple pair given by Join_Pred. */

join([], _, _, []).
join(_, [], _, []).
join([First|Rest], Tuples_2, Join_Pred, Result) :-
   xml_pillow_d_o_join_first(First, Tuples_2, Join_Pred, First_List),
   xml_pillow_data_operation:join(Rest, Tuples_2, Join_Pred, More),
   append(First_List, More, Result).


/* xml_pillow_aggregate(
         +List, +Extract_Pred, +Command, -Result) <-
      performs standard aggregation specified by atom Command on
      list of numeric values List. */

xml_pillow_aggregate(List, Command, Result) :-
   xml_pillow_d_o_aggregate(Command, List, Result).


/* pattern_search(+Page, +Pattern_Pred, -Element) <-
      searches for elements that are root elements of a pattern
      detected by predicate Pattern_Pred. */

pattern_search([], _, _) :-
   fail.
pattern_search(Page, Pattern_Pred, Element) :-
   xml_pillow_navigate:any_element(Page, Element),
   call(Pattern_Pred, Page, Element).


/* all_satisfy(+List, +Check_Pred) <-
      checks whether all values in List comply with the condition
      expressed by predicate Check_Pred. */

all_satisfy(List, Check_Pred) :-
   checklist( Check_Pred,
      List ).


/* any_satisfies(+List, +Check_Pred) <-
      checks whether any value in List complies with the condition
      expressed by predicate Check_Pred. */

any_satisfies([First|_], Check_Pred) :-
   call(Check_Pred, First),
   !.
any_satisfies([_|Rest], Check_Pred) :-
   xml_pillow_data_operation:any_satisfies(Rest, Check_Pred).


/*** implementation ***********************************************/


xml_pillow_d_o_aggregate(max, [Number], Number) :-
   !.
xml_pillow_d_o_aggregate(max, [First|Rest], Value) :-
   xml_pillow_d_o_aggregate(max, Rest, Rest_Max),
   Value is max(First, Rest_Max).
xml_pillow_d_o_aggregate(min, [Number], Number) :-
   !.
xml_pillow_d_o_aggregate(min, [First|Rest], Value) :-
   xml_pillow_d_o_aggregate(min, Rest, Rest_Min),
   Value is min(First, Rest_Min).
xml_pillow_d_o_aggregate(sum, [Number], Number) :-
   !.
xml_pillow_d_o_aggregate(sum, [First|Rest], Value) :-
   xml_pillow_d_o_aggregate(sum, Rest, Rest_Sum),
   Value is First + Rest_Sum.
xml_pillow_d_o_aggregate(count, List, Value) :-
   length(List, Value).
xml_pillow_d_o_aggregate(avg, List, Value) :-
   \+ list_empty(List),
   xml_pillow_d_o_aggregate(sum, List, Sum),
   xml_pillow_d_o_aggregate(count, List, Count),
   Value is Sum / Count.


xml_pillow_d_o_join_first(_, [], _, []).
xml_pillow_d_o_join_first(Tuple, [First|Left], Join_Pred,
      [Result|More_Results]) :-
   call(Join_Pred, Tuple, First),
   append(Tuple, First, Result),
   xml_pillow_d_o_join_first(Tuple, Left, Join_Pred, More_Results),
   !.
xml_pillow_d_o_join_first(Tuple, [_|Left], Join_Pred, Result) :-
   xml_pillow_d_o_join_first(Tuple, Left, Join_Pred, Result).


/******************************************************************/


