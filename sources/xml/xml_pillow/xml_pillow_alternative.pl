

/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  More Efficient Term Tree              ***/
/***                                                            ***/
/******************************************************************/


/*** interface ****************************************************/


:- module(xml_pillow_alternative, [
      pillow_transform/3,
      transformed_type_check/3,
      trans_element_rank/2,
      trans_element_page/2,
      trans_father/2,
      trans_before/2,
      trans_element_name/2,
      trans_attributes/2,
      trans_any_children/2,
      trans_page_print/1,
      trans_term_print/2,
      trans_attribute_print/1 ]).

:- ensure_loaded(library/pillow).
:- use_module(hash_table).
:- use_module(ascii_text).


/* pillow_transform(+Pillow_Term, -Trans_Term, +Hash_Table) <-
      transforms pillow term tree into alternative optimized
      structure Trans_Term that corrects some weaknesses of
      pillow. The result uses the hash table identified by table key
      Hash_Table. */

pillow_transform(Pillow_Term, Trans_Term, Hash_Table) :-
   list_transform(Pillow_Term, Trans_Term, _, 1, _, Hash_Table),
   hash_table:value_put(Hash_Table, 0, Trans_Term).


/* trans_element_rank(+Element, ?Rank) <-
      determines position Rank of element Element within XML document.
      Rank is defined by the order of the elements in the document
      source. */

trans_element_rank(Element, Rank) :-
   functor(Element, env, _),
   arg(2, Element, Rank).


/* trans_element_page(+Element, -Page) <-
      retrieves XML document Page the element Element belongs to. */

trans_element_page(Element, Page) :-
   trans_element_page_key(Element, Table),
   hash_table:get(Table, 0, Page).


/* trans_father(+Element, -Father) <-
      returns father element Father of Element. */

trans_father(Element, Father) :-
   trans_element_rank(Element, Rank),
   trans_element_page_key(Element, Table_Key),
   hash_table:get(Table_Key, Rank, Father).


/* trans_before(+Element_1, +Element_2) <-
      succeeds if position of Element_1 in XML source is before
      that of Element_2. */

trans_before(Element_1, Element_2) :-
   trans_element_rank(Element_1, Rank_1),
   trans_element_rank(Element_2, Rank_2),
   Rank_1 < Rank_2.


/* trans_element_name(+Term, ?Name) <-
      return name Name of element Term. */

trans_element_name(Term, Name) :-
   transformed_type_check(shallow, element, Term),
   arg(3, Name, _).


/* trans_attributes(+Term, ?Attributes) <-
      returns Attributes of element Term. */

trans_attributes(Term, Attributes) :-
   transformed_type_check(shallow, element, Term),
   arg(4, Attributes, _).


/* trans_any_children(+Term, -Children) <-
      returns Children of element Term. */

trans_any_children(Term, Children) :-
   transformed_type_check(shallow, nonempty_element, Term),
   arg(5, Children, _).


/* trans_page_print(+Tree) <-
      pretty prints document Tree. */

trans_page_print(Tree) :-
   transformed_list_print(Tree, 0).


/* trans_term_print(+Term, +Level) <-
      pretty prints term of document tree inserted by Level spaces. */

trans_term_print(Term, Level) :-
   transformed_type_check(shallow, text, Term),
   !,
   non_element_text(Term, Text),
   tab(Level),
   ascii_text:trim(Text, Trimmed),
   checklist( put_char,
      Trimmed ),
   nl.
trans_term_print(Term, Level) :-
   transformed_type_check(shallow, comment, Term),
   !,
   non_element_text(Term, Text),
   tab(Level),
   write('<!-- '),
   checklist( put_char,
      Text ),
   write(' -->'),
   nl.
trans_term_print(Term, Level) :-
   transformed_type_check(shallow, declaration, Term),
   !,
   non_element_text(Term, Text),
   tab(Level),
   write('<!'),
   checklist( put_char,
      Text ),
   write('>'),
   nl.
trans_term_print(Term, Level) :-
   transformed_type_check(shallow, xmldecl, Term),
   !,
   arg(3, Term, Attributes),
   tab(Level),
   write('<?xml '),
   checklist( trans_attribute_print,
      Attributes ),
   write('>'),
   nl.
trans_term_print(Term, Level) :-
   transformed_type_check(shallow, empty_element, Term),
   !,
   trans_element_name(Term, Name),
   trans_attributes(Term, Attributes),
   tab(Level),
   write('<'),
   write(Name),
   checklist( trans_attribute_print,
      Attributes ),
   write('/>'),
   nl.
trans_term_print(Term, Level) :-
   transformed_type_check(shallow, empty_element, Term),
   !,
   trans_element_name(Term, Name),
   trans_attributes(Term, Attributes),
   trans_any_children(Term, Any_Children),
   tab(Level),
   write('<'),
   write(Name),
   checklist( trans_attribute_print,
      Attributes ),
   write('>'),
   nl,
   Next_Level is Level + 2,
   transformed_list_print(Any_Children, Next_Level),
   tab(Level),
   write('</'),
   write(Name),
   write('>'),
   nl.


/* trans_attribute_print(+Attribute) <-
      pretty prints Attribute (of an element). */

trans_attribute_print(Attribute) :-
   functor(Attribute, '=', _),
   write(' '),
   arg(1, Attribute, Name),
   arg(2, Attribute, Value),
   write(Name),
   write('="'),
   checklist( put_char,
      Value ),
   write('"').
trans_attribute_print(Attribute) :-
   functor(Attribute, Name, 1),
   write(' '),
   write(Name),
   write('="'),
   write(Name),
   write('"').


/* transformed_type_check(+Mode, ?Type, +Term) <-
      checks whether Term has type Type. Mode = shallow performs
      a minimal superficial check, Mode = deep will check Term
      recursively yet isn't implemented yet. */

transformed_type_check(shallow, element, env(_, _, _, _, _)).
transformed_type_check(shallow, element, Term) :-
   transformed_type_check(shallow, empty_element, Term).
transformed_type_check(shallow, empty_element, $(_, _, _, _)).
transformed_type_check(shallow, empty_element, elem(_, _, _, _)).
transformed_type_check(shallow, inner_element,
   env(_, _, _, _, [_|_])).
transformed_type_check(shallow, nonempty_element,
   env(_, _, _, _, _)).
transformed_type_check(shallow, comment, comment(_, _, _)).
transformed_type_check(shallow, declaration, declare(_, _, _)).
transformed_type_check(shallow, xml_declaration, xmldecl(_, _, _)).
transformed_type_check(shallow, text, text(_, _, [_|_])).
transformed_type_check(shallow, term_list, []).
transformed_type_check(shallow, term_list, [_|_]).
transformed_type_check(shallow, empty_page, []).
transformed_type_check(shallow, attribute, =(_, _)).
transformed_type_check(shallow, attribute, Attribute) :-
   atom(Attribute).
transformed_type_check(shallow, note, Term) :-
   transformed_t_c_note_term(Name),
   transformed_type_check(shallow, Name, Term).


/*** implementation ***********************************************/


list_transform([First|Rest], [First_Trans|Rest_Trans],
      [Current_ID|Rest_ID], Current_ID, Next_ID, Hash_Table) :-
   term_transform(First, First_Trans, Current_ID, Updated_ID,
      Hash_Table),
   list_transform(Rest, Rest_Trans, Rest_ID, Updated_ID, Next_ID,
      Hash_Table).
list_transform([], [], [], Current_ID, Current_ID, _).


term_transform(Term, Trans_Term, Current_ID, Next_ID, Hash_Table) :-
   is_list(Term),
   ascii_text:flow_normalize(Term, Trans_Text),
   Trans_Term =.. [text, Hash_Table, Current_ID, Trans_Text],
   Next_ID is Current_ID + 1.
term_transform(Term, Trans_Term, Current_ID, Next_ID, Hash_Table) :-
   transformed_type_check(shallow, note, Term),
   non_element_text(Term, Text),
   functor(Term, Name, 1),
   Trans_Term =.. [Name, Hash_Table, Current_ID, Text],
   Next_ID is Current_ID + 1.
term_transform(Term, Trans_Term, Current_ID, Next_ID, Hash_Table) :-
   transformed_type_check(shallow, empty_element, Term),
   trans_element_name(Term, Name),
   trans_attributes(Term, Attributes),
   Trans_Term =.. [elem, Hash_Table, Current_ID, Name, Attributes],
   Next_ID is Current_ID + 1.
term_transform(Term, Trans_Term, Current_ID, Next_ID, Hash_Table) :-
   transformed_type_check(shallow, nonempty_element, Term),
   trans_element_name(Term, Name),
   trans_attributes(Term, Attribs),
   trans_any_children(Term, Terms),
   Temp_ID is Current_ID + 1,
   list_transform(Terms, Trans_Terms, IDs, Temp_ID, Next_ID,
      Hash_Table),
   Trans_Term =.. [env, Hash_Table, Current_ID, Name, Attribs,
      Trans_Terms],
   checklist(father_relation_store(Hash_Table, Trans_Term),
      IDs).


transformed_t_c_note_term(comment).
transformed_t_c_note_term(declaration).
transformed_t_c_note_term(xml_declaration).


trans_element_page_key(Element, Page_Key) :-
   functor(Element, env, _),
   arg(1, Element, Page_Key).


non_element_text(Term, Text) :-
   arg(3, Term, Text).


father_relation_store(Table, Value, Element_ID) :-
   hash_table:value_put(Table, Element_ID, Value).


transformed_list_print([Term|Rest], Level) :-
   trans_term_print(Term, Level),
   transformed_list_print(Rest, Level).
transformed_list_print([], _).


/******************************************************************/


