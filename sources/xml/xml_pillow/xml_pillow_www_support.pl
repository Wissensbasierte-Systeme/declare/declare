

/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  Web Support                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


:- module(xml_pillow_www_support, [
      hypertext_simple_fetch/2,
      hypertext_param_fetch/3,
      url_fetch/3,
      binary_fetch/3,
      html_fetch_redirect/4,
      url_protocol_set/3,
      url_host_set/3,
      url_port_set/3,
      url_path_set/3,
      url_file_name_set/3,
      url_querystring_set/3,
      url_anchor_set/3,
      http_get_param_create/3,
      http_query_assign/3,
      http_query_param_get/3,
      http_query_param_concat/3,
      rel_to_abs_url/3,
      rel_to_abs_url_text/3,
      abs_to_rel_url/3,
      is_redirection/2,
      is_hypertext/2,
      is_server_error/2,
      is_inaccessible/2,
      content_type/2,
      url_encode/2,
      url_decode/2,
      url_protocol/2, url_protocol_text/2,
      url_host/2, url_host_text/2,
      url_port/2, url_port_text/2,
      url_select_path/2,
      url_file_name/2,
      url_querystring/2, url_querystring_text/2,
      url_anchor/2, url_anchor_text/2,
      url_split/2,
      url_compose/2 ]).



/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  Web Support                           ***/
/***                                                            ***/
/******************************************************************/


:- use_module(ascii_text).


/*** interface ****************************************************/


/* hypertext_simple_fetch(+URL, -Page) <-
      downloads document Page addressed by URL. */

hypertext_simple_fetch(URL, Page) :-
   xml_pillow_www_support:hypertext_param_fetch(URL, [], Page).


/* is_redirection(+Response, -Next_URL) <-
      succeeds if HTTP Response signals a redirection, returns
      target of redirection Next_URL. */

is_redirection(Response, Next_URL) :-
   member(status(_, Code, _), Response),
   xml_pillow_w_s_http_is_redirect_code(Code),
   member(location(New_URL), Response),
   atom_codes(New_URL, Next_URL).


/* is_hypertext(+Response, -Type) <-
      succeeds if HTTP Response belongs to a hypertext resource,
      returns its type (xml, html etc.) in Type. */

is_hypertext(Response, Type) :-
   member(content_type(text, Type, _), Response),
   xml_pillow_w_s_http_is_hypertext(Type).


/* is_server_error(+Response, -Error_Data) <-
      analyses whether HTTP Response notifies a server error and
      returns a status(Error, Code, Details) term with error name
      Error, error code Code, and a short description Details. */
   
is_server_error(Response, status(Error, Code, Details)) :-
   member(status(Error, Code, Details), Response),
   atom_to_number(Code, Code_Number),
   between(500, 599, Code_Number).


/* is_server_error(+Response, -Error_Data) <-
      analyses whether HTTP Response notifies an inaccessible
      resource and returns a status(Error, Code, Details) term
      with error name Error, error code Code, and a short
      description Details. */

is_inaccessible(Response, status(Error, Code, Details)) :-
   member(status(Error, Code, Details), Response),
   atom_to_number(Code, Code_Number),
   between(400, 499, Code_Number).


/* content_type(+Response, -Content_Data) <-
      extracts content type data as content_type(Type, Sub_Type,
      Params) term. Arguments are self-explaining. */

content_type(Response, content_type(Type, Sub_Type, Params)) :-
   member(content_type(Type, Sub_Type, Params), Response).


/* url_encode(+Text, -URL_Text) <-
      encodes Text so it can be used within a URL, result is
      URL_Text. */

url_encode(Text, URL_Text) :-
   xml_pillow_w_s_encode_loop(Text, Code_Text),
   flatten(Code_Text, URL_Text).


/* url_decode(+URL_Text, -Text) <-
      decodes URL_Text so it can be read as normal Text. */

url_decode(URL_Text, Text) :-
   xml_pillow_w_s_decode_loop(URL_Text, Code_Text),
   flatten(Code_Text, Text).


/* url_protocol(+Split, -Protocol) <-
      extracts protocol from URL in splitted form Split as atom. */

url_protocol(Split, Protocol) :-
   nth1(1, Split, Protocol).


/* url_host(+Split, -Host) <-
      extracts host from URL in splitted form Split as atom. */

url_host(Split, Host) :-
   nth1(2, Split, Host).


/* url_port(+Split, -Port) <-
      extracts Port from URL in splitted form Split as number. */

url_port(Split, Port) :-
   nth1(3, Split, Port).


/* url_select_path(+Split, -Path) <-
      extracts pure path component without server and file name
      from URL in splitted form Split. */

url_select_path(Split, Path) :-
   nth1(4, Split, Path).


/* url_file_name(+Split, -File_Name) <-
      extracts file name File_Name from URL in splitted form
      Split. */

url_file_name(Split, File_Name) :-
   nth1(5, Split, File_Name).


/* url_querystring(+Split, -Query) <-
      extracts query string Query from URL in splitted form Split. */

url_querystring(Split, Query) :-
   nth1(6, Split, Query).


/* url_anchor(+Split, -Anchor) <-
      extracts Anchor from URL in splitted form Split. */

url_anchor(Split, Anchor) :-
   nth1(7, Split, Anchor).


/* url_anchor_text(+Split, -Text) <-
      extracts Anchor from URL in splitted form Split using
      notation with leading #. */

url_anchor_text(Split, Text) :-
   xml_pillow_www_support:url_anchor(Split, Anchor),
   \+ Anchor = [] ->
      flatten(["#", Anchor], Text)
      ; Text = [].


/* url_querystring_text(+Split, -Text) <-
      extracts query string Query from URL in splitted form Split
      using notation with leading ?. */

url_querystring_text(Split, Text) :-
   xml_pillow_www_support:url_querystring(Split, Query),
   \+ Query = [] ->
      flatten(["?", Query], Text)
      ; Text = [].


/* url_port_text(+Split, -Port) <-
      extracts Port from URL in splitted form Split as text. */

url_port_text(Split, [58|Port]) :-
   xml_pillow_www_support:url_port(Split, Port_Number),
   number_codes(Port_Number, Port).


/* url_host_text(+Split, -Text) <-
      extracts Host from URL in splitted form Split as text. */

url_host_text(Split, Text) :-
   xml_pillow_www_support:url_host(Split, Host),
   atom_codes(Host, Text).


/* url_protocol_text(+Split, -Text) <-
      extracts Protocol from URL in splitted form Split as text. */

url_protocol_text(Split, Text) :-
   xml_pillow_www_support:url_protocol(Split, Protocol),
   atom_codes(Protocol, Raw_Text),
   append(Raw_Text, "://", Text).


/* url_split(+URL, -Split) <-
      splits URL into a list of its atomic components protocol,
      host, port, path, file name, query string, and anchor. */

url_split(URL, [http, Host, Port, Path, File_Name, Query, Anchor]) :-
   url_info(URL, URL_Term),
   !,
   arg(1, URL_Term, Host),
   arg(2, URL_Term, Port),
   arg(3, URL_Term, File),
   xml_pillow_w_s_file_split(File, [Path, File_Name, Query, Anchor]).
url_split(URL, Split) :-
   xml_pillow_w_s_resource_protocol(Protocol, Length, Short_Protocol),
   ascii_text:to_lower_case(URL, Lower_URL),
   ascii_text:start(Lower_URL, Length, Protocol, Rest),
   append("http://", Rest, Test_URL),
   url_info(Test_URL, _),
   xml_pillow_www_support:url_split(Test_URL, Raw_Split),
   xml_pillow_www_support:url_protocol_set(Raw_Split, Short_Protocol,
      Split).


/* url_compose(+Split, -URL) <-
      composes URL basing on the splitted form Split. */

url_compose(Split, URL) :-
   is_list(Split),
   length(Split, 7),
   xml_pillow_www_support:url_anchor_text(Split, Anchor_Text),
   xml_pillow_www_support:url_querystring_text(Split, Query_Text),
   xml_pillow_www_support:url_file_name(Split, File_Text),
   xml_pillow_www_support:url_select_path(Split, Path_Text),
   xml_pillow_www_support:url_port_text(Split, Port_Text),
   xml_pillow_www_support:url_host_text(Split, Host_Text),
   xml_pillow_www_support:url_protocol_text(Split, Protocol),
   flatten([Protocol, Host_Text, Port_Text, Path_Text, File_Text,
      Query_Text, Anchor_Text], URL).


/*** implementation ***********************************************/


xml_pillow_w_s_http_is_redirect_code(301).
xml_pillow_w_s_http_is_redirect_code(302).

xml_pillow_w_s_http_is_hypertext(xml).
xml_pillow_w_s_http_is_hypertext(html).

xml_pillow_w_s_encode_loop([], []).

xml_pillow_w_s_encode_loop([First|Rest], [First_Trans|Rest_Text]) :-
   xml_pillow_w_s_url_char_encode(First, First_Trans),
   xml_pillow_w_s_encode_loop(Rest, Rest_Text).

xml_pillow_w_s_encode_loop([First|Rest], [First|Rest_Text]) :-
   \+ xml_pillow_w_s_url_char_encode(First, _),
   xml_pillow_w_s_encode_loop(Rest, Rest_Text).

xml_pillow_w_s_url_char_encode(32, [43]).

xml_pillow_w_s_url_char_encode(Code, [37, First, Second]) :-
   var(Code),
   !,
   xml_pillow_w_s_decimal_to_hex_ascii(Code, [First, Second]).

xml_pillow_w_s_url_char_encode(Code, [37, First, Second]) :-
   xml_pillow_w_s_critical_url_char(Code),
   xml_pillow_w_s_decimal_to_hex_ascii(Code, [First, Second]).

xml_pillow_w_s_decimal_to_hex_ascii(Decimal, [First, Second]) :-
   xml_pillow_w_s_compute_decimal(First, Second, Decimal).

xml_pillow_w_s_critical_url_char(34).
xml_pillow_w_s_critical_url_char(35).
xml_pillow_w_s_critical_url_char(37).
xml_pillow_w_s_critical_url_char(38).
xml_pillow_w_s_critical_url_char(39).
xml_pillow_w_s_critical_url_char(43).
xml_pillow_w_s_critical_url_char(47).
xml_pillow_w_s_critical_url_char(58).
xml_pillow_w_s_critical_url_char(59).
xml_pillow_w_s_critical_url_char(60).
xml_pillow_w_s_critical_url_char(61).
xml_pillow_w_s_critical_url_char(62).
xml_pillow_w_s_critical_url_char(63).
xml_pillow_w_s_critical_url_char(64).
xml_pillow_w_s_critical_url_char(91).
xml_pillow_w_s_critical_url_char(92).
xml_pillow_w_s_critical_url_char(93).
xml_pillow_w_s_critical_url_char(94).
xml_pillow_w_s_critical_url_char(121).
xml_pillow_w_s_critical_url_char(123).
xml_pillow_w_s_critical_url_char(124).
xml_pillow_w_s_critical_url_char(126).
xml_pillow_w_s_critical_url_char(Code) :-
   Code > 126.
xml_pillow_w_s_critical_url_char(Code) :-
   Code < 32.

xml_pillow_w_s_compute_decimal(First, Second, Decimal) :-
   xml_pillow_w_s_dec_ascii(First_Number, First),
   xml_pillow_w_s_dec_ascii(Second_Number, Second),
   Decimal is First_Number * 16 + Second_Number.

xml_pillow_w_s_dec_hex(0, '0').
xml_pillow_w_s_dec_hex(1, '1').
xml_pillow_w_s_dec_hex(2, '2').
xml_pillow_w_s_dec_hex(3, '3').
xml_pillow_w_s_dec_hex(4, '4').
xml_pillow_w_s_dec_hex(5, '5').
xml_pillow_w_s_dec_hex(6, '6').
xml_pillow_w_s_dec_hex(7, '7').
xml_pillow_w_s_dec_hex(8, '8').
xml_pillow_w_s_dec_hex(9, '9').
xml_pillow_w_s_dec_hex(10, 'A').
xml_pillow_w_s_dec_hex(11, 'B').
xml_pillow_w_s_dec_hex(12, 'C').
xml_pillow_w_s_dec_hex(13, 'D').
xml_pillow_w_s_dec_hex(14, 'E').
xml_pillow_w_s_dec_hex(15, 'F').

xml_pillow_w_s_dec_ascii(0, 48).
xml_pillow_w_s_dec_ascii(1, 49).
xml_pillow_w_s_dec_ascii(2, 50).
xml_pillow_w_s_dec_ascii(3, 51).
xml_pillow_w_s_dec_ascii(4, 52).
xml_pillow_w_s_dec_ascii(5, 53).
xml_pillow_w_s_dec_ascii(6, 54).
xml_pillow_w_s_dec_ascii(7, 55).
xml_pillow_w_s_dec_ascii(8, 56).
xml_pillow_w_s_dec_ascii(9, 57).
xml_pillow_w_s_dec_ascii(10, 65).
xml_pillow_w_s_dec_ascii(11, 66).
xml_pillow_w_s_dec_ascii(12, 67).
xml_pillow_w_s_dec_ascii(13, 68).
xml_pillow_w_s_dec_ascii(14, 69).
xml_pillow_w_s_dec_ascii(15, 70).

xml_pillow_w_s_dec_to_ascii(Decimal, Hex_Ascii) :-
   number(decimal),
   var(Hex_Ascii),
   xml_pillow_w_s_compute_hex_ascii(Decimal, Hex_Ascii).

xml_pillow_w_s_compute_hex_ascii(Decimal, [First, Second]) :-
   First_Number is Decimal // 16,
   Second_Number is Decimal mod 16,
   xml_pillow_w_s_dec_ascii(First_Number, First),
   xml_pillow_w_s_dec_ascii(Second_Number, Second).

xml_pillow_w_s_decode_loop([], []).

xml_pillow_w_s_decode_loop([37, Second, Third|Rest],
      [First_Trans|Rest_Text]) :-
   xml_pillow_w_s_url_char_encode(First_Trans, [37, Second, Third]),
   !,
   xml_pillow_w_s_decode_loop(Rest, Rest_Text).

xml_pillow_w_s_decode_loop([43|Rest], [32, Rest_Text]) :-
   !,
   xml_pillow_w_s_decode_loop(Rest, Rest_Text).

xml_pillow_w_s_decode_loop([First|Rest], [First|Rest_Text]) :-
   xml_pillow_w_s_decode_loop(Rest, Rest_Text).

xml_pillow_w_s_file_split([], [[], [], [], []]) :-
   !.

xml_pillow_w_s_file_split([47|Rest], [[47|Rest_Dir], File_Name, Query,
       Anchor]) :-
   !,
   xml_pillow_w_s_file_split(Rest,
      [Rest_Dir, File_Name, Query, Anchor]).

xml_pillow_w_s_file_split(File, [Path, File_Name, Query, Anchor]) :-
   xml_pillow_w_s_path_info(File, Path, File_Name, Query, Anchor).

xml_pillow_w_s_path_info([], [], [], [], []).

xml_pillow_w_s_path_info(File, Path, File_Name, Query, Anchor) :-
   ascii_text:last_index_of(File, 47, Index),
   ascii_text:start(File, Index, Path, Name_Rest),
   xml_pillow_w_s_url_file_name(Name_Rest, File_Name, End_Signal,
      Param_Rest),
   xml_pillow_w_s_url_param(Param_Rest, End_Signal, Query, Anchor).

xml_pillow_w_s_url_file_name([], [], 0, []).

xml_pillow_w_s_url_file_name([First|Rest], [], First, Rest) :-
   xml_pillow_w_s_url_file_end(First),
   !.

xml_pillow_w_s_url_file_name([First|Rest], [First|Rest_Name], Signal,
      Rest_Text) :-
   xml_pillow_w_s_url_file_name(Rest, Rest_Name, Signal, Rest_Text).

xml_pillow_w_s_url_param([], _, [], []).

xml_pillow_w_s_url_param(_, 0, [], []).

xml_pillow_w_s_url_param(Text, Signal, [], Text) :-
   xml_pillow_w_s_url_anchor_char(Signal).

xml_pillow_w_s_url_param(Text, Signal, Query, Anchor) :-
   xml_pillow_w_s_url_querystring_char(Signal),
   ascii_text:split(Text, 35, [Query|Anchor]),
   length(Anchor, Length),
   Length =< 1.

xml_pillow_w_s_url_file_end(63).        % ?
xml_pillow_w_s_url_file_end(35).        % #

xml_pillow_w_s_url_anchor_char(35).           % #
xml_pillow_w_s_url_querystring_char(63).      % ?

xml_pillow_w_s_resource_protocol("http://", 7, http).
xml_pillow_w_s_resource_protocol("https://", 8, https).
xml_pillow_w_s_resource_protocol("ftp://", 6, ftp).


/******************************************************************/



/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  Web Support                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* hypertext_param_fetch(+URL, +Request, -Page) <-
      downloads document addressed by URL using HTTP request
      parameters Request, converts it into pillow term tree Page. */

hypertext_param_fetch(URL, Request, Page) :-
   xml_pillow_www_support:binary_fetch(URL, Request, Content),
   html2terms(Content, Page).


/* url_fetch(+URL, +Request, -Response) <-
      fetches HTTP response of HTTP request for URL with request
      parameters Request. */

url_fetch(URL, Request, Response) :-
   url_info(URL, URL_Term),
   fetch_url(URL_Term, Request, Response).


/* binary_fetch(+URL, +Request, -Content) <-
      downloads resource addressed by URL using HTTP request
      parameters Request as binary resource Content. */

binary_fetch(URL, Request, Content) :-
   xml_pillow_www_support:url_fetch(URL, Request, Response),
   member(content(Content), Response).


/* url_protocol_set(+Split, +Protocol, -New_Split) <-
      sets new Protocol in splitted URL Split,
      result is New_Split. */

url_protocol_set(Split, Protocol, New_Split) :-
   ascii_text:list_nth_replace(Split, 1, Protocol, New_Split).


/* url_host_set(+Split, +Host, -New_Split) <-
      sets new Host in splitted URL Split, result is New_Split. */

url_host_set(Split, Host, New_Split) :-
   ascii_text:list_nth_replace(Split, 2, Host, New_Split).


/* url_port_set(+Split, +Port, -New_Split) <-
      sets new Port in splitted URL Split, result is New_Split. */

url_port_set(Split, Port, New_Split) :-
   ascii_text:list_nth_replace(Split, 3, Port, New_Split).


/* url_path_set(+Split, +Path, -New_Split) <-
      sets new Path in splitted URL Split, result is New_Split. */

url_path_set(Split, Path, New_Split) :-
   ascii_text:list_nth_replace(Split, 4, Path, New_Split).


/* url_file_name_set(+Split, +File_Name, -New_Split) <-
      sets new file name File_Name in splitted URL Split,
      result is New_Split. */

url_file_name_set(Split, File_Name, New_Split) :-
   ascii_text:list_nth_replace(Split, 5, File_Name, New_Split).


/* url_querystring_set(+Split, +Query, -New_Split) <-
      sets new query string Query in splitted URL Split,
      result is New_Split. */

url_querystring_set(Split, Query, New_Split) :-
   ascii_text:list_nth_replace(Split, 6, Query, New_Split).


/* url_anchor_set(+Split, +Anchor, -New_Split) <-
      sets new Anchor in splitted URL Split, result is New_Split. */

url_anchor_set(Split, Anchor, New_Split) :-
   ascii_text:list_nth_replace(Split, 7, Anchor, New_Split).


/* http_get_param_create(+Param, +Value, -Result) <-
      creates new HTTP parameter as Param=Value term Result. */

http_get_param_create(Param, Value, Result) :-
   append(Param, "=", Temp),
   append(Temp, Value, Result).


/* http_query_assign(+Text, +URL, -New_URL) <-
      assigns query string Text to URL given as text,
      result is New_URL. */

http_query_assign(Text, URL, New_URL) :-
   append(URL, [63], Temp_URL),
   append(Temp_URL, Text, New_URL).


/* http_query_param_get(+Query_String, +Param, -Value) <-
      extracts Value of query parameter Param from query string
      Query_String. */

http_query_param_get(Query_String, Param, Value) :-
   ascii_text:split(Query_String, 38, Params),
   xml_pillow_w_s_search_param(Params, Param, Value).


/* http_query_param_concat(+String_1, +String_2, -Result) <-
      concatenates query parameter strings String_1 and String_2,
      result is Result. */

http_query_param_concat(String_1, String_2, Result) :-
   flatten([String_1, 38, String_2], Result).


/* rel_to_abs_url(+Rel_URL, +Base_URL, -Abs_URL) <-
      converts a relative URL referring to host specified in Base_URL
      to absolute URL Abs_URL. Base_URL is given as splitted list,
      Abs_URL is constructed as splitted list. */

rel_to_abs_url(Rel_URL, Base_URL, Abs_URL) :-
   xml_pillow_www_support:rel_to_abs_url_text(Rel_URL, Base_URL,
      URL_Text),
   xml_pillow_www_support:url_split(URL_Text, Abs_URL).


/* rel_to_abs_url_text(+Rel_URL, +Base_URL, -Abs_URL) <-
      converts a relative URL referring to host specified in Base_URL
      to absolute URL Abs_URL. Base_URL is given as splitted list,
      Abs_URL is constructed as text URL. */

rel_to_abs_url_text([], Base_URL, URL_Text) :-
   xml_pillow_www_support:url_compose(Base_URL, URL_Text).
rel_to_abs_url_text(Rel_URL, _, Rel_URL) :-
   xml_pillow_w_s_protocols(Protocol, Length),
   ascii_text:to_lower_case(Rel_URL, Lower_Rel_URL),
   ascii_text:start(Lower_Rel_URL, Length, Protocol, _),
   !,
   Protocol = "http://" ->
      xml_pillow_www_support:url_split(Rel_URL, _)
      ; true.
rel_to_abs_url_text([47|Rest_Path], Base_URL, URL_Text) :-
   !,
   is_list(Base_URL),
   length(Base_URL, 7),
   xml_pillow_www_support:url_port_text(Base_URL, Port_Text),
   xml_pillow_www_support:url_host_text(Base_URL, Host_Text),
   flatten(["http://", Host_Text, Port_Text, 47, Rest_Path],
      URL_Text).
rel_to_abs_url_text(Rel_URL, Base_URL, URL_Text) :-
   is_list(Base_URL),
   length(Base_URL, 7),
   xml_pillow_www_support:url_select_path(Base_URL, Base_Path),
   append(Base_Path, Rel_URL, Raw_Path),
   ascii_text:split(Raw_Path, 47, Parts),
   xml_pillow_w_s_kill_dot_loop(Parts, Clean_Parts, _),
   ascii_text:compose_prefix(Clean_Parts, 47, Path_Text),
   xml_pillow_www_support:rel_to_abs_url_text(Path_Text, Base_URL,
      URL_Text).


/* abs_to_rel_url(+Abs_URL, +Base_URL, -Rel_URL) <-
      converts an absolute URL Abs_URL to a relaitve Rel_URL assuming
      the referring document's base address is given by Base_URL.
      Base_URL is given as splitted list of URL components. */

abs_to_rel_url([], _, "").
abs_to_rel_url([47|Rest_Path], [47|Rest_Base_URL], Rel_URL) :-
   !,
   ascii_text:split(Rest_Base_URL, 47, Base_Split),
   ascii_text:split(Rest_Path, 47, Ref_Split),
   xml_pillow_w_s_dot_construct(Base_Split, Ref_Split, Rel_URL).
abs_to_rel_url([47|Rest_Path], Base_URL, Rel_URL) :-
   !,
   xml_pillow_www_support:url_select_path(Base_URL, Raw_Path),
   append(Raw_Path, "/x", Base_Path),
   xml_pillow_www_support:abs_to_rel_url([47|Rest_Path], Base_Path,
      Rel_URL).
abs_to_rel_url(Abs_URL, Base_URL, Rel_URL) :-
   xml_pillow_w_s_protocols(Protocol, Length),
   ascii_text:to_lower_case(Abs_URL, Lower_Abs_URL),
   atom_codes(Protocol, Protocol_Text),
   ascii_text:start(Lower_Abs_URL, Length, Protocol_Text, _),
   !,
   Protocol = 'http://' ->
      xml_pillow_w_s_abs_to_rel(Abs_URL, Base_URL, Rel_URL)
      ; Rel_URL = Abs_URL.
abs_to_rel_url(Ref_Path, _, Ref_Path).


/* html_fetch_redirect(+URL, +Request, -Page, -URL_History) <-
      performs download of document given by URL with HTTP
      request parameters Request. The download procedure follows
      all redirections until a resource can be downloaded. The
      sequence of addressed (redirected) URL's is stored in
      URL_History. */

html_fetch_redirect(URL, Request, Page, URL_History) :-
   xml_pillow_w_s_fetch_redirect(URL, Request, [URL], Page,
      URL_History).


/*** implementation ***********************************************/


xml_pillow_w_s_fetch_redirect(URL, Request, Old_History, Page,
      URL_History) :-
   xml_pillow_www_support:url_fetch(URL, Request, Response),
   xml_pillow_w_s_decide_on_redirect(Response, Old_History, Page,
      URL_History).

xml_pillow_w_s_decide_on_redirect(Response, Old_History, Page,
      URL_History) :-
   xml_pillow_www_support:is_redirection(Response, New_URL),
   !,
   xml_pillow_w_s_decide_on_redirect_cycle(Old_History, New_URL,
      Page, URL_History).

xml_pillow_w_s_decide_on_redirect(Response, Old_History, Page,
      Old_History) :-
   member(content(Content), Response),
   html2terms(Content, Page).

xml_pillow_w_s_decide_on_redirect_cycle(Old_History, New_URL, _, _) :-
   member(New_URL, Old_History),
   !,
   fail.

xml_pillow_w_s_decide_on_redirect_cycle(Old_History, New_URL, Page,
      URL_History) :-
   xml_pillow_w_s_fetch_redirect(New_URL, [], [New_URL|Old_History],
      Page, URL_History).

xml_pillow_w_s_search_param([First|_], Param, Value) :-
   ascii_text:split(First, 61, [Param, Value]),
   !.

xml_pillow_w_s_search_param([_|Rest], Param, Value) :-
   xml_pillow_w_s_search_param(Rest, Param, Value).

/* known WWW protocols */

xml_pillow_w_s_protocols('http://', 7).
xml_pillow_w_s_protocols('ftp://', 6).
xml_pillow_w_s_protocols('mailto://', 9).
xml_pillow_w_s_protocols('javascript://', 13).

xml_pillow_w_s_kill_dot_loop(Parts, Clean_Parts, Signal) :-
   xml_pillow_w_s_eliminate_dots(Parts, Temp_Solution, Signal),
   xml_pillow_w_s_decide_next_loop(Temp_Solution, Signal,
      Clean_Parts).

xml_pillow_w_s_eliminate_dots([], [], pathfound).

xml_pillow_w_s_eliminate_dots([Path_Part], _, _) :-
   xml_pillow_w_s_url_path_upward(Path_Part),
   !,
   fail.

xml_pillow_w_s_eliminate_dots([Path_Part], [], pathfound) :-
   xml_pillow_w_s_url_path_constant(Path_Part).

xml_pillow_w_s_eliminate_dots([Path_Part], [Path_Part], pathfound).

xml_pillow_w_s_eliminate_dots([First, _|_], _, _) :-
   xml_pillow_w_s_url_path_upward(First),
   !,
   fail.

xml_pillow_w_s_eliminate_dots([First, Second|Rest], Text, Signal) :-
   xml_pillow_w_s_url_path_constant(First),
   !,
   xml_pillow_w_s_eliminate_dots([Second|Rest], Text, Signal).

xml_pillow_w_s_eliminate_dots([_, Second|Rest], Rest, dotsfound) :-
   xml_pillow_w_s_url_path_upward(Second).

xml_pillow_w_s_eliminate_dots([First, Second|Rest], Text, Signal) :-
   xml_pillow_w_s_url_path_constant(Second),
   !,
   xml_pillow_w_s_eliminate_dots([First|Rest], Text, Signal).

xml_pillow_w_s_eliminate_dots([First,Second|Rest], [First|Rest_Text],
                              Signal) :-
   \+ xml_pillow_w_s_url_path_upward(Second),
   xml_pillow_w_s_eliminate_dots([Second|Rest], Rest_Text, Signal).

xml_pillow_w_s_decide_next_loop(Solution, pathfound, Solution).

xml_pillow_w_s_decide_next_loop(Temp_Solution, dotsfound, Solution) :-
   xml_pillow_w_s_kill_dot_loop(Temp_Solution, Solution, _).

xml_pillow_w_s_url_path_separator(47).

xml_pillow_w_s_url_path_upward([46, 46]).

xml_pillow_w_s_url_path_constant([46]).

xml_pillow_w_s_abs_to_rel(Abs_URL, Base_URL, Rel_URL) :-
   xml_pillow_www_support:url_split(Abs_URL, Ref_URL_Split),
   xml_pillow_www_support:url_host(Base_URL, Base_Host),
   xml_pillow_www_support:url_host(Ref_URL_Split, Base_Host),
   !,
   xml_pillow_www_support:url_select_path(Ref_URL_Split, Ref_Path),
   xml_pillow_www_support:url_file_name(Ref_URL_Split, Ref_File),
   xml_pillow_w_s_file_win_clean(Ref_File, Clean_File),
   append(Ref_Path, Clean_File, Check_Path),
   xml_pillow_www_support:abs_to_rel_url(Check_Path, Base_URL,
      Rel_Path),
   xml_pillow_www_support:url_querystring_text(Ref_URL_Split, Ref_Query),
   xml_pillow_w_s_file_win_clean(Ref_Query, Clean_Query),
   xml_pillow_www_support:url_anchor_text(Ref_URL_Split, Ref_Anchor),
   xml_pillow_w_s_file_win_clean(Ref_Anchor, Clean_Anchor),
   flatten([Rel_Path, Clean_Query, Clean_Anchor], Rel_URL).

xml_pillow_w_s_abs_to_rel(Abs_URL, _, Abs_URL).

xml_pillow_w_s_dot_construct([First|Rest], [First|Ref_Rest],
      Rel_URL) :-
   !,
   xml_pillow_w_s_dot_construct(Rest, Ref_Rest, Rel_URL).

xml_pillow_w_s_dot_construct(Base_Split, Ref_Split, Rel_URL) :-
   xml_pillow_w_s_base_dots(Base_Split, Base_Dots),
   xml_pillow_w_s_win_clean_split(Ref_Split, Clean_Split),
   ascii_text:compose(Clean_Split, 47, Ref_Path),
   append(Base_Dots, Ref_Path, Rel_URL).

xml_pillow_w_s_base_dots([], []).
xml_pillow_w_s_base_dots([_], []) :-
   !.
xml_pillow_w_s_base_dots([_|Rest], [46, 46, 47|Rest_Dots]) :-
   xml_pillow_w_s_base_dots(Rest, Rest_Dots).

xml_pillow_w_s_win_clean_split([], []).

xml_pillow_w_s_win_clean_split([Single], [Single_Clean]) :-
   !,
   xml_pillow_w_s_file_win_clean(Single, Single_Clean).

xml_pillow_w_s_win_clean_split([First|Rest], [First|Clean_Rest]) :-
   xml_pillow_w_s_win_clean_split(Rest, Clean_Rest).

/* replaces illegal chars in Windows file names by underscores */

xml_pillow_w_s_file_win_clean([], []).

xml_pillow_w_s_file_win_clean([First|Rest], [95|Clean_Rest]) :-
   xml_pillow_w_s_win_illegal_char(First),
   !,
   xml_pillow_w_s_file_win_clean(Rest, Clean_Rest).

xml_pillow_w_s_file_win_clean([First|Rest], [First|Clean_Rest]) :-
   xml_pillow_w_s_file_win_clean(Rest, Clean_Rest).

/* illegal characters in MS Windows file names */

xml_pillow_w_s_win_illegal_char(42).
xml_pillow_w_s_win_illegal_char(47).
xml_pillow_w_s_win_illegal_char(58).
xml_pillow_w_s_win_illegal_char(60).
xml_pillow_w_s_win_illegal_char(62).
xml_pillow_w_s_win_illegal_char(63).
xml_pillow_w_s_win_illegal_char(92).
xml_pillow_w_s_win_illegal_char(124).


/******************************************************************/


/******************************************************************/


