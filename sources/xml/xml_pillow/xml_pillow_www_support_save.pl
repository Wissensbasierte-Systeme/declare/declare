

/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  Web Support                           ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


:- module(xml_pillow_www_support, [
      hypertext_simple_fetch/2,
      hypertext_param_fetch/3,
      url_fetch/3,
      binary_fetch/3,
      html_fetch_redirect/4,
      url_protocol_set/3,
      url_host_set/3,
      url_port_set/3,
      url_path_set/3,
      url_file_name_set/3,
      url_querystring_set/3,
      url_anchor_set/3,
      http_get_param_create/3,
      http_query_assign/3,
      http_query_param_get/3,
      http_query_param_concat/3,
      rel_to_abs_url/3,
      rel_to_abs_url_text/3,
      abs_to_rel_url/3,
      is_redirection/2,
      is_hypertext/2,
      is_server_error/2,
      is_inaccessible/2,
      content_type/2,
      url_encode/2,
      url_decode/2,
      url_protocol/2, url_protocol_text/2,
      url_host/2, url_host_text/2,
      url_port/2, url_port_text/2,
      url_select_path/2,
      url_file_name/2,
      url_querystring/2, url_querystring_text/2,
      url_anchor/2, url_anchor_text/2,
      url_split/2,
      url_compose/2 ]).


:- [ xml_pillow_www_support_2,
     xml_pillow_www_support_3_4 ].


/******************************************************************/


