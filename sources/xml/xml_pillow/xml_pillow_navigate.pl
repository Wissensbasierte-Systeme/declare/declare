

/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  Navigation in XML-Documents           ***/
/***                                                            ***/
/******************************************************************/


:- module(xml_pillow_navigate, [
      root_element/2,
      any_element/2, id_element/3, named_element/3,
      toplevel_note/2,
      text_pattern_element/3, text_content_element/3,
      child_term/2, child_element/2, named_child_element/3,
      nth_child_element/3, nth_child_term/3,
      self_or_descendant/2, descendant/2,
      father_element/3, father_of_term_element/3,
      self_or_ancestor/3, ancestor/3,
      global_successor/3, global_predecessor/3,
      global_before/3, global_after/3,
      local_before/3, local_after/3,
      nth_local_predecessor/4, nth_local_successor/4,
      any_children/2, element_children/2,
      reference_element/4, multi_reference_element/4,
      sibling_element/4,
      child_element_range/4, child_term_range/4 ]).

:- use_module(xml_pillow_query).


/*** interface ****************************************************/


/* root_element(+Page, ?Root) <-
      returns root element Root of document Page. */

root_element(Page, Root) :-
   sublist(xml_pillow_type_check(shallow, element),
      Page, Elements),
   nth1(1, Elements, Root).


/* any_element(+Page, ?Element) <-
      returns an arbitrary element Element of document Page. */

any_element(Page, Element) :-
   sublist(xml_pillow_type_check(shallow, element),
      Page, Elements),
   member(Candidate, Elements),
   xml_pillow_navigate:self_or_descendant(Candidate, Element).


/* toplevel_note(+Page, ?Note) <-
      returns a note (comment, declaration etc. situated on the
      same hierarchical level as the root element of document Page. */

toplevel_note(Page, Note) :-
   sublist(xml_pillow_type_check(shallow, note),
      Page, Notes),
   member(Note, Notes).


/* child_term(+Element, ?Child) <-
      returns a child term of Element. */

child_term(Element, Child) :-
   xml_pillow_navigate:any_children(Element, List),
   member(Child, List).


/* child_term(+Element, ?Child) <-
      returns a child element of Element. */

child_element(Element, Child) :-
   xml_pillow_navigate:child_term(Element, Child),
   xml_pillow_type_check(shallow, element, Child).


/* self_or_descendant(+Element, ?Element) <-
      returns Element, and alternatively all descendant elements. */

self_or_descendant(Element, Element).
self_or_descendant(Element, Descendant) :-
   xml_pillow_navigate:descendant(Element, Descendant).


/* descendant(+Element, ?Descendant) <-
      returns all descendant elements of Element. */

descendant(Element, Descendant) :-
   xml_pillow_navigate:child_element(Element, Child),
   xml_pillow_navigate:self_or_descendant(Child, Descendant).


/* any_children(+Element, ?List) <-
      returns list of all child terms of Element. */

any_children(Element, List) :-
   xml_pillow_type_check(shallow, nonempty_element, Element),
   arg(3, Element, List).


/* element_children(+Element, ?List) <-
      returns list of all child elements of Element. */

element_children(Element, List) :-
   xml_pillow_type_check(shallow, nonempty_element, Element),
   xml_pillow_navigate:any_children(Element, Any_List),
   sublist( xml_pillow_type_check(shallow, element),
      Any_List, List ).


/* id_element(+Page, +ID, ?Element) <-
      returns element with id attribute value ID in document Page. */

id_element(Page, ID, Element) :-
   xml_pillow_navigate:any_element(Page, Element),
   xml_pillow_query:attribute_value(Element, "id", ID).


/* text_pattern_element(+Page, +Pattern, -Element) <-
      returns Element in document Page containing text that can
      be described by wildcard pattern Pattern. */

text_pattern_element(Page, Pattern, Element) :-
   xml_pillow_navigate:any_element(Page, Element),
   xml_pillow_query:shallow_text(Element, Text_List),
   string_to_list(Text_String, Text_List),
   atom_codes(Pattern_Atom, Pattern),
   wildcard_match(Pattern_Atom, Text_String).

/* text_content_element(+Page, +Text, -Element) <-
      returns Element in document Page containing text fragment
      Text. */


text_content_element(Page, Text, Element) :-
   xml_pillow_navigate:any_element(Page, Element),
   xml_pillow_query:shallow_text(Element, Text_List),
   ascii_text:to_lower_case(Text_List, Normed_Text),
   ascii_text:to_lower_case(Text, Lower_Text),
   ascii_text:contains(Normed_Text, Lower_Text).


/* named_element(+Page, +Name, -Element) <-
      returns Element in document Page having name Name. */

named_element(Page, Name, Element) :-
   xml_pillow_navigate:any_element(Page, Element),
   xml_pillow_query:element_name(Element, Name).


/* named_child_element(+Element, +Name, -Child) <-
      returns child element Child of Element having name Name. */

named_child_element(Element, Name, Child) :-
   xml_pillow_navigate:child_element(Element, Child),
   xml_pillow_query:element_name(Child, Name).


/* father_of_term_element(+Page, +Term, -Father) <-
      returns father element of pillow term Term in document Page. */

father_of_term_element(Page, Term, Father) :-
   xml_pillow_navigate:any_element(Page, Father),
   xml_pillow_navigate:child_term(Father, Term).


/* father_element(+Page, +Element, -Father) <-
      return father Father of Element in document Page. */

father_element(Page, Element, Father) :-
   xml_pillow_navigate:any_element(Page, Father),
   xml_pillow_navigate:child_element(Father, Element).


/* ancestor(+Page, +Element, -Ancestor) <-
      returns ancestor element Ancestor of Element in document Page. */

ancestor(Page, Element, Ancestor) :-
   xml_pillow_navigate:father_element(Page, Element, Father),
   xml_pillow_navigate:self_or_ancestor(Page, Father, Ancestor).


/* self_or_ancestor(+Page, +Element, -Ancestor) <-
      returns Element and alternatively ancestor element Ancestor
      of Element in document Page. */

self_or_ancestor(_, Element, Element).
self_or_ancestor(Page, Element, Ancestor) :-
   xml_pillow_navigate:ancestor(Page, Element, Ancestor).


/* global_successor(+Page, +Element, -Successor) <-
      returns successor element Successor of Element in document
      Page according to the order of elements in document source. */

global_successor(_, Element, Successor) :-
   xml_pillow_navigate:nth_child_element(Element, 1, Successor),
   !.
global_successor(Page, Element, Successor) :-
   equal_successor(Page, Element, Successor).


/* global_before(+Page, +Element_1, +Element_2) <-
      succeeds if Element_1 occurs before Element_2 in document Page. */

global_before(Page, Element_1, Element_2) :-
   xml_pillow_navigate:ancestor(Page, Element_2, Element_1).
global_before(Page, Element_1, Element_2) :-
   xml_pillow_navigate:any_element(Page, Element_1),
   xml_pillow_mavigate:any_element(Page, Element_2),
   bagof( Ancestor_1,
      xml_pillow_navigate:ancestor(Page, Element_1, Ancestor_1),
      List_1 ),
   bagof( Ancestor_2,
      xml_pillow_navigate:ancestor(Page, Element_2, Ancestor_2),
      List_2 ),
   reverse([Element_1|List_1], Ancestor_1_List),
   reverse([Element_2|List_2], Ancestor_2_List),
   xml_pillow_n_ancestor_compare(Ancestor_1_List, Ancestor_2_List).


/* global_after(+Page, +Element_1, +Element_2) <-
      succeeds if Element_1 occurs after Element_2 in document Page. */

global_after(Page, Element_1, Element_2) :-
   xml_pillow_navigate:global_before(Page, Element_2, Element_1).


/* global_predecessor(+Page, +Element, -Predecessor) <-
      returns predecessor element Predecessor of Element in document
      Page according to the order of elements in document source. */

global_predecessor(Page, Elem, Predecessor) :-
   xml_pillow_navigate:nth_local_predecessor(Page, Elem, 1, Before),
   !,
   xml_pillow_n_last_enclosed_element(Before, Predecessor).
global_predecessor(Page, Elem, Predecessor) :-
   xml_pillow_navigate:father_element(Page, Elem, Predecessor).


/* local_before(+Page, ?Element_1, ?Element_2) <-
      checks whether Element_1 occurs before sibling Element_2 in
      document Page. */

local_before(Page, Element_1, Element_2) :-
   xml_pillow_navigate:sibling_element(Page, Element_1, Element_2, _),
   xml_pillow_query:local_element_position(Element_1, Page, Position_1),
   xml_pillow_query:local_element_position(Element_2, Page, Position_2),
   Position_1 < Position_2.


/* local_after(+Page, ?Element_1, ?Element_2) <-
      checks whether Element_1 occurs after sibling Element_2 in
      document Page. */

local_after(Page, Element_1, Element_2) :-
   xml_pillow_navigate:local_before(Page, Element_2, Element_1).


/* nth_child_element(+Element, ?N, ?Child) <-
      checks whether element Child is Nth child element of Element. */

nth_child_element(Element, N, Child) :-
   xml_pillow_navigate:any_children(Element, List),
   sublist(xml_pillow_type_check(shallow, element),
      List, Elements),
   nth1(N, Elements, Child).


/* nth_child_term(+Element, ?N, ?Any_Child) <-
      checks whether term Any_Child is Nth child term of Element. */

nth_child_term(Element, N, Any_Child) :-
   xml_pillow_navigate:any_children(Element, List),
   nth1(N, List, Any_Child).


/* reference_element(+Page, +Source, ?Ref_Attribute, ?Target) <-
      performs IDREF navigation from base element Source to Target
      using referencing attribute Ref_Attribute. */

reference_element(Page, Source, Ref_Attribute, Target) :-
   xml_pillow_query:attribute_value(Source, Ref_Attribute, ID),
   xml_pillow_navigate:id_element(Page, ID, Target).


/* multi_reference_element(+Page, +Source, ?Ref_Attribute, ?Target) <-
      performs IDREFS navigation from base element Source to Target
      using referencing attribute Ref_Attribute. */

multi_reference_element(Page, Source, Ref_Attribute, Target) :-
   xml_pillow_query:attribute_value(Source, Ref_Attribute, ID_Seq),
   ascii_text:char_split(ID_Seq, 32, ID_List),
   member(ID, ID_List),
   xml_pillow_navigate:id_element(Page, ID, Target).


/* nth_local_successor(+Page, ?Element, +N, ?Successor) <-
      returns sibling Successor of Element with local position 
      greater than that of Element by increment N. */

nth_local_successor(Page, Element, N, Successor) :-
   xml_pillow_navigate:father_element(Page, Element, Father),
   xml_pillow_navigate:nth_child_element(Father, Index, Element),
   Succ_Index is Index + N,
   xml_pillow_navigate:nth_child_element(Father, Succ_Index, Successor).


/* nth_local_predecessor(+Page, ?Element, +N, ?Predecessor) <-
      returns sibling Predecessor of Element with local position 
      smaller than that of Element by increment N. */

nth_local_predecessor(Page, Element, N, Predecessor) :-
   xml_pillow_navigate:nth_local_successor(Page, Predecessor, N,
      Element).


/* sibling_element(+Page, ?Element_1, ?Element_2, -Father) <-
      succeeds if Element_1 and Element_2 are siblings with father
      element Father. */

sibling_element(Page, Element_1, Element_2, Father) :-
   xml_pillow_navigate:father_element(Page, Element_1, Father),
   xml_pillow_navigate:father_element(Page, Element_2, Father),
   \+ Element_1 == Element_2.


/* child_element_range(+Element, +From, +To, ?Child) <-
      returns all child elements of Element with local position
      between From and To including the interval limits. */

child_element_range(Element, From, To, Child) :-
   between(From, To, Index),
   xml_pillow_navigate:nth_child_element(Element, Index, Child).


/* child_term_range(+Element, +From, +To, ?Child) <-
      returns all child term of Element with local position
      between From and To including the interval limits. */

child_term_range(Element, From, To, Child) :-
   between(From, To, Index),
   xml_pillow_navigate:nth_child_term(Element, Index, Child).


/*** implementation ***********************************************/


xml_pillow_n_ancestor_child_index(Page, Element, Ancestor,
      Child_Index) :-
   xml_pillow_navigate:nth_child_element(
      Ancestor, Child_Index, Child),
   xml_pillow_navigate:self_or_ancestor(Page, Element, Child).


xml_pillow_n_last_enclosed_element(Element, Element) :-
   \+ xml_pillow_navigate:nth_child_element(Element, 1, _),
   !.
xml_pillow_n_last_enclosed_element(Element, Sub_Element) :-
   xml_pillow_navigate:element_children(Element, Children),
   length(Children, N),
   xml_pillow_navigate:nth_child_element(Element, N, Child),
   xml_pillow_n_last_enclosed_element(Child, Sub_Element).


xml_pillow_n_ancestor_compare([Father, Current|Rest_1],
      [Father, Current|Rest_2]) :-
   !,
   xml_pillow_n_ancestor_compare([Current|Rest_1], [Current|Rest_2]).
xml_pillow_n_ancestor_compare([Father_1, Current_1|_],
      [Father_2, Current_2|_]) :-
   xml_pillow_navigate:nth_child_element(Father_1, Index_1, Current_1),
   xml_pillow_navigate:nth_child_element(Father_2, Index_2, Current_2),
   Index_1 < Index_2.


equal_successor(Page, Element, Successor) :-
   xml_pillow_navigate:nth_local_successor(Page, Element, 1,
      Successor),
   !.
equal_successor(Page, Element, Successor) :-
   xml_pillow_navigate:father_element(Page, Element, Father),
   equal_successor(Page, Father, Successor).


/******************************************************************/


