

/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  Terms in Field Notation               ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


:- module(xml_pillow_field_notation, [
%     ':='/2,
      pillow_to_field_notation/2,
      field_notation_print/1,
      field_notation_to_pillow/2,
      field_notation_query/3
      ]).

:- ensure_loaded(library/pillow).

:- use_module(ascii_text).
:- use_module(xml_pillow_query).


/* pillow_to_field_notation(+Pillow, -Field_Notation) <-
      converts pillow term tree into field notation term tree. */

pillow_to_field_notation(Pillow, Field_Notation) :-
   fn_list_transform(Pillow, Field_Notation).


/* field_notation_to_pillow(+Pillow, -Field_Notation) <-
      converts field notation term tree into pillow term tree. */

field_notation_to_pillow([], []) :-
   !.
field_notation_to_pillow(Text, Transformed) :-
   atom(Text),
   atom_codes(Text, Transformed),
   !.
field_notation_to_pillow([First|Rest], [First_Pillow|Rest_Pillow]) :-
   field_notation_to_pillow(First, First_Pillow),
   !,
   field_notation_to_pillow(Rest, Rest_Pillow).
field_notation_to_pillow([_|Rest], Pillow) :-
   field_notation_to_pillow(Rest, Pillow).
field_notation_to_pillow(a(_):_, _) :-
   !,
   fail.
field_notation_to_pillow(Name:List, Pillow) :-
   is_list(List),
   !,
   fnp_attributes_collect(List, Attributes),
   field_notation_to_pillow(List, Children),
   Pillow =.. [env, Name, Attributes, Children].
field_notation_to_pillow(Name:Value, Pillow) :-
   atom_codes(Value, ASCII_Value),
   Pillow =.. [env, Name, [], [ASCII_Value]].


/* field_notation_print_2(+Term, +Spaces) <-
      pretty prints field notation terms in a syntax similar
      to XML inserting Spaces empty spaces at the beginning
      of a line */

field_notation_print_2(Any) :-
   field_notation_print_2(Any, 0).

field_notation_print_2([], _).
field_notation_print_2([First|Rest], Spaces) :-
   field_notation_print_2(First, Spaces),
   field_notation_print_2(Rest, Spaces).
field_notation_print_2(a(Name):List, Spaces) :-
   !,
   tab(Spaces),
   write('@'),
   write(Name),
   write(': '),
   write(List),
   nl.
field_notation_print_2(Name:List, Spaces) :-
   is_list(List),
   !,
   tab(Spaces),
   write('<'),
   write(Name),
   write_ln('>'),
   Next_Spaces is Spaces + 4,
   field_notation_print_2(List, Next_Spaces),
   tab(Spaces),
   write('</'),
   write(Name),
   write_ln('>').
field_notation_print_2(Name:Data, Spaces) :-
   !,
   tab(Spaces),
   write('<'),
   write(Name),
   write_ln('>'),
   Next_Spaces is Spaces + 4,
   field_notation_print_text(Data, Next_Spaces),
   tab(Spaces),
   write('</'),
   write(Name),
   write_ln('>').
field_notation_print_2(Text, Spaces) :-
   atom(Text),
   field_notation_print_text(Text, Spaces).


/* field_notation_print(+Term, +Spaces) <-
      pretty prints field notation terms */

field_notation_print(Any) :-
   field_notation_print(Any, 0).

field_notation_print([], _) :-
   !.
field_notation_print([First|Rest], Spaces) :-
   field_notation_print(First, Spaces),
   fn_list_separator_print(Rest),
   field_notation_print(Rest, Spaces).
field_notation_print(a(Name):List, Spaces) :-
   !,
   tab(Spaces),
   write(a(Name)),
   write(':\''),
   write(List),
   write('\'').
field_notation_print(Name:List, Spaces) :-
   is_list(List),
   !,
   tab(Spaces),
   write(Name),
   write_ln(':['),
   Next_Spaces is Spaces + 4,
   field_notation_print(List, Next_Spaces),
   tab(Spaces),
   write(']').
field_notation_print(Name:Data, Spaces) :-
   !,
   tab(Spaces),
   write(Name),
   write(':\''),
   write(Data),
   write('\'').
field_notation_print(Text, Spaces) :-
   atom(Text),
   field_notation_print_text(Text, Spaces).


/* field_notation_query(+Query, +Terms, -Result) <-
      performs a query on field notation Terms by converting
      Terms into pillow structure, applying query predicate Query,
      and converting query result into field notation Result again. */

field_notation_query(Query, Terms, Result) :-
   field_notation_to_pillow(Terms, Pillow),
   call(Query, Pillow, Pillow_Result),
   pillow_to_field_notation(Pillow_Result, Result).


/* X := Y <-
      Syntax for descending paths in field notation terms */

/*
X := U^V :-
   var(V),
   !,
   ( member(V:X,U)
   ; ( member(V_1:Y,U),
       X := Y^V_2,
       V = V_1^V_2 ) ).
X := U^V^W :-
   var(V),
   member(V_1:Y,U),
   X := Y^V_2^W,
   V = V_1^V_2.
X := U^V^W :-
   !,
   member(V:Y,U),
   X := Y^W.
X := U^V :-
   !,
   member(V:X,U).
X := X.
*/


/*** implementation ***********************************************/


fn_list_transform([First|Rest], [First_Trans|Rest_Trans]) :-
   fn_is_transformable(First),
   fn_term_transform(First, First_Trans),
   !,
   fn_list_transform(Rest, Rest_Trans).
fn_list_transform([_|Rest], Transformed) :-
   fn_list_transform(Rest, Transformed).
fn_list_transform([], []).

fn_is_transformable([First|Rest]) :-
   ascii_text:trim([First|Rest], Trimmed),
   \+ Trimmed = [].


fn_is_transformable(env(_, _, _)).
fn_is_transformable(elem(_, _)).
fn_is_transformable($(_, _)).


fn_term_transform([First|Rest], Transformed) :-
  name(Transformed, [First|Rest]).

fn_term_transform(elem(Name, []), Name:'') :-
   !.

fn_term_transform(elem(Name, Attributes), Transformed) :-
   fn_attr_list_transform(Attributes, Attr_Transformed),
   Transformed = Name:Attr_Transformed.

fn_term_transform($(Name, []), Name:'') :-
   !.

fn_term_transform($(Name, Attributes), Transformed) :-
   fn_attr_list_transform(Attributes, Attr_Transformed),
   Transformed = Name:Attr_Transformed.

fn_term_transform(env(Name, [], Children), Transformed) :-
   fn_is_direct_value(Children),
   \+ Children == [],
   !,
   fn_list_transform(Children, [Data]),
   Transformed = Name:Data.

fn_term_transform(env(Name, Attributes, Children), Transformed) :-
   fn_attr_list_transform(Attributes, Attr_Transformed),
   fn_list_transform(Children, Children_Transformed),
   append(Attr_Transformed, Children_Transformed, Fields),
   Transformed = Name:Fields.

fn_is_direct_value([]).

fn_is_direct_value([First|Rest]) :-
   xml_pillow_type_check(shallow, text, First),
   !,
   checklist(xml_pillow_type_check(shallow, note),
      Rest).

fn_is_direct_value([First|Rest]) :-
   xml_pillow_type_check(shallow, note, First),
   fn_is_direct_value(Rest).

fn_attr_list_transform([], []) :-
   !.

fn_attr_list_transform([First|Rest], [First_Trans|Rest_Trans]) :-
   xml_pillow_query:attribute_name(First, Ascii_Name),
   atom_codes(Name, Ascii_Name),
   xml_pillow_query:attribute_value(First, Value_ASCII),
   name(Value, Value_ASCII),
   First_Trans = a(Name):Value,
   fn_attr_list_transform(Rest, Rest_Trans).

fnp_attributes_collect([], []).

fnp_attributes_collect([a(Name):Value|Rest],
      [=(Name, ASCII_Value)|Attrs]) :-
   !,
   atom_codes(Value, ASCII_Value),
   fnp_attributes_collect(Rest, Attrs).

fnp_attributes_collect([_|Rest], Attributes) :-
   fnp_attributes_collect(Rest, Attributes).

field_notation_print_text(Text, Spaces) :-
   tab(Spaces),
   write('\''),
   write(Text),
   write('\'').

fn_list_separator_print([]) :-
   !,
   nl.

fn_list_separator_print(_) :-
   write_ln(',').

fn_list_nth_occurrence(List, Any:Z, N) :-
   var(Any),
   !,
   maplist(fn_copy_field_name,
      List, List_Names),
   list_to_set(List_Names, Names),
   member(Any, Names),
   fn_list_nth_search(List, Any:Z, N).

fn_list_nth_occurrence(List, Any:Z, N) :-
   fn_list_nth_search(List, Any:Z, N).

fn_list_nth_search(List, Any:Z, N) :-
   var(N),
   !,
   fn_list_any_collect(List, Any:Z, N, 1).

fn_list_nth_search([Element:Z|_], Element:Z, 1) :-
   !.

fn_list_nth_search([Element:_|Rest], Element:Z, N) :-
   !,
   N > 1,
   N1 is N - 1,
   fn_list_nth_search(Rest, Element:Z, N1).

fn_list_nth_search([_|Rest], Element, N) :-
   fn_list_nth_search(Rest, Element, N).

fn_list_any_collect([Element:Z|_], Element:Z, N, N).

fn_list_any_collect([Element:_|Rest], Element:Z, N, N1) :-
   !,
   N2 is N1 + 1,
   fn_list_any_collect(Rest, Element:Z, N, N2).

fn_list_any_collect([_|Rest], Element, N, N1) :-
   fn_list_any_collect(Rest, Element, N, N1).

fn_copy_field_name(Name:_, Name).


/*** tests

X := U^V^W :-
   member(Z:Y,U),
   X := Y^T^W,
   \+ T = '',
   V = Z^T.

X := U^V^W :-
   var(V),
   X := U^W,
   V = ''.

X := U^V$N :-
   !,
   fn_list_nth_occurrence(U, V:X, N).

X := U^V$N^W :-
   !,
   fn_list_nth_occurrence(U, V:Y, N),
   X := Y^W.

X := U^V :-
   var(V),
   !,
   ( member(V:X,U)
   ; ( member(V_1:Y,U),
       X := Y^V_2,
       V = V_1^V_2 ) ).

X := U^V^W :-
   var(V),
   member(V_1:Y,U),
   X := Y^V_2^W,
   V = V_1^V_2.

X := U^V^W :-
   !,
   member(V:Y,U),
   X := Y^W.


X := U^V :-
   !,
   member(V:X,U).

X := X.

*/

/******************************************************************/


