

/******************************************************************/
/***                                                            ***/
/***            XML-Pillow:  Querying XML-Terms                 ***/
/***                                                            ***/
/******************************************************************/


:- module(xml_pillow_query, [
      attribute_name/2,
      attribute_value/2,
      attribute_value/3,
      attributes/2,
      attribute_names/2,
      element_name/2,
      note_type/2,
      shallow_text_terms/2,
      deep_text_terms/2,
      shallow_text/2,
      shallow_text/3,
      deep_text/2,
      deep_text/3,
      element_namespace/2,
      local_element_name/2,
      local_element_position/3,
      local_term_position/3,
      element_depth/3 ]).

:- use_module(ascii_text).
:- use_module(xml_pillow_navigate).


/*** interface ****************************************************/


/* attribute_name(+Attribute, ?Name) <-
      returns Name of Attribute. */

attribute_name(=(Attr_Name, _), Name) :-
   string_to_list(Attr_Name, Name).
attribute_name(Term, Name) :-
   ( xml_pillow_type_check(shallow, element, Term)
   ; xml_pillow_type_check(shallow, xml_declaration, Term) ),
   xml_pillow_query:attributes(Term, Attributes),
   xml_pillow_q_any_attribute(Attributes, Name).
attribute_name(Attr_Name, Name) :-
   atom(Attr_Name),
   string_to_list(Attr_Name, Name).


/* attribute_name(+Attribute, ?Value) <-
      returns Value of Attribute. */

attribute_value(=(_, Value), Value).
attribute_value(Name, Name) :-
   atom(Name).


/* attribute_names(+Element, ?Names) <-
      returns list Names of all attribute names of Element. */

attribute_names(Element, List) :-
   xml_pillow_query:attributes(Element, Attributes),
   xml_pillow_q_attribute_names(Attributes, List).


/* attributes(+Element, ?List) <-
      returns List of all attributes of Element. */

attributes(Element, List) :-
   xml_pillow_type_check(shallow, element, Element),
   arg(2, Element, List),
   !.
attributes(X_Decl, List) :-
   xml_pillow_type_check(shallow, xml_declaration, X_Decl),
   arg(1, X_Decl, List).


/* element_name(+Element, ?Name) <-
      returns Name of Element. */

element_name(Element, Name) :-
   xml_pillow_type_check(shallow, element, Element),
   arg(1, Element, Name).


/* shallow_text_terms(+Element, -Text_Terms) <-
      returns list of text fragments contained in Element and not
      in its descendants. The order of the list elements corresponds
      to the order in the document source. */

shallow_text_terms(Element, Text_Terms) :-
   xml_pillow_navigate:any_children(Element, Children),
   sublist(xml_pillow_type_check(shallow, text),
      Children, Text_Terms).


/* deep_text_terms(+Element, -Text_Terms) <-
      returns list of text fragments contained in Element and its
      descendants. The order of the list elements corresponds
      to the order in the document source. */

deep_text_terms(Element, Text_Terms) :-
   xml_pillow_navigate:any_children(Element, Children),
   xml_pillow_q_deep_text_find(Children, Text_Terms).


/* shallow_text(+Element, -Text) <-
      returns Text contained in Element and not in its descendants
      as string. */

shallow_text(comment(Text), Text).
shallow_text(declare(Text), Text).
shallow_text(Element, Text) :-
   xml_pillow_query:shallow_text(Element, [], Text).


/* deep_text(+Element, -Text) <-
      returns Text contained in Element and its descendants
      as string. */

deep_text(Element, Text) :-
   xml_pillow_query:deep_text(Element, [], Text).


/* element_namespace(+Element, ?Namespace) <-
      returns namespace component of the name of Element, '' if
      no namespace speicified. */

element_namespace(Element, Namespace) :-
   xml_pillow_query:element_name(Element, Name),
   atom_codes(Name, Name_Text),
   ascii_text:before_signal_char(Name_Text, 58, Namespace),
   \+ Namespace = Name_Text.


/* local_element_name(+Element, ?Tag_Name) <-
      returns Element name without namespace component. */

local_element_name(Element, Tag_Name) :-
   xml_pillow_query:element_name(Element, Name),
   atom_codes(Name, Name_Text),
   ascii_text:after_signal_char(Name_Text, 58, Temp),
   xml_pillow_q_retrieve_tag_name(Name_Text, Temp, Tag_Name).


/* attribute_value(+Term, +Attr_Name, ?Value) <-
      returns Value of attribute Attr_Name of element Term. */

attribute_value(Term, Attr_Name, Value) :-
   xml_pillow_query:attributes(Term, Attributes),
   xml_pillow_q_attribute_find(Attributes, Attr_Name, Value).


/* shallow_text(+Element, +Options, -Text) <-
      returns Text contained in Element and not in its descendants
      as string. Options is a list of filter options that control
      corrective text transformations with reference to whitespace
      fragments. */

shallow_text(Element, Options, Text) :-
   xml_pillow_query:shallow_text_terms(Element, Text_Terms),
   xml_pillow_q_text_clean(Text_Terms, Options, Clean_Terms),
   flatten(Clean_Terms, Text).


/* deep_text(+Element, +Options, -Text) <-
      returns Text contained in Element and in its descendants
      as string. Options is a list of filter options that control
      corrective text transformations with reference to whitespace
      fragments. */

deep_text(Element, Options, Text) :-
   xml_pillow_query:deep_text_terms(Element, Text_Terms),
   xml_pillow_q_text_clean(Text_Terms, Options, Clean_Terms),
   flatten(Clean_Terms, Text).


/* local_element_position(+Element, +Page, ?Position) <-
      returns local Position of Element contained in document Page. */

local_element_position(Element, Page, Position) :-
   xml_pillow_navigate:father_element(Page, Element, Father),
   !,
   xml_pillow_navigate:nth_child_element(Father, Position, Element).
local_element_position(_, _, 1).


/* local_term_position(+Term, +Page, ?Position) <-
      returns local Position of pillow Term contained in document
      Page. */

local_term_position(Term, Page, Position) :-
   xml_pillow_navigate:father_element(Page, Term, Father),
   !,
   xml_pillow_navigate:nth_child_term(Father, Position, Term).
local_term_position(Term, Page, Position) :-
   nth1(Position, Page, Term).


/* element_depth(+Term, +Page, ?Depth) <-
      returns hierarchy level Depth of Term in document Page, root
      element is assigned depth 1. */

element_depth(Term, Page, Depth) :-
   xml_pillow_navigate:father_element(Page, Term, Father),
   !,
   xml_pillow_query:element_depth(Father, Page, Father_Depth),
   Depth is Father_Depth + 1.
element_depth(Term, Page, 1) :-
   xml_pillow_navigate:root_element(Page, Term)
   ; xml_pillow_navigate:toplevel_note(Page, Term).


/*** implementation ***********************************************/


xml_pillow_q_attribute_find([(Raw_Name=Value)|_], Name, Value) :-
   atom_codes(Raw_Name, Raw_Name_Codes),
   ascii_text:to_lower_case(Raw_Name_Codes, Name),
   !.
xml_pillow_q_attribute_find([Raw_Name|_], Name, Result) :-
   atom(Raw_Name),
   !,
   atom_codes(Raw_Name, Result),
   ascii_text:to_lower_case(Result, Name).
xml_pillow_q_attribute_find([_|Rest], Name, Result) :-
   xml_pillow_q_attribute_find(Rest, Name, Result).


xml_pillow_q_any_attribute([First|_], Name) :-
   xml_pillow_query:attribute_name(First, Name).
xml_pillow_q_any_attribute([_|Rest], Name) :-
   xml_pillow_q_any_attribute(Rest, Name).


xml_pillow_q_attribute_names([First|Rest], [Name|Restlist]) :-
   xml_pillow_q_attribute_names(Rest, Restlist),
   xml_pillow_query:attribute_name(First, Name_Atom),
   string_to_list(Name_Atom, Name).
xml_pillow_q_attribute_names([], []).


xml_pillow_q_text_clean(Clean, [], Clean).
xml_pillow_q_text_clean(Dirty, [Option|Rest], Clean) :-
   xml_pillow_q_text_clean_option(Dirty, Option, Better),
   xml_pillow_q_text_clean(Better, Rest, Clean).

xml_pillow_q_text_clean_option(Clean, keep, Clean).
xml_pillow_q_text_clean_option(Dirty, kill_empty, Clean) :-
   sublist( ascii_text:is_visible,
      Dirty, Clean ).
xml_pillow_q_text_clean_option(Dirty, white_to_space, Clean) :-
   maplist( ascii_text:white_to_space,
      Dirty, Clean ).
xml_pillow_q_text_clean_option(Dirty, space_normalize, Clean) :-
   ascii_text:space_normalize(Dirty, Clean).
xml_pillow_q_text_clean_option(Dirty, trim, Clean) :-
   sublist( ascii_text:is_visible,
      Dirty, Visible ),
   maplist( ascii_text:trim,
      Visible, Clean ).
xml_pillow_q_text_clean_option(Dirty, flow_normalize, Clean) :-
   maplist( ascii_text:flow_normalize,
      Dirty, Clean ).


xml_pillow_q_deep_text_find([], []).
xml_pillow_q_deep_text_find([First|Rest], [First|Rest_Terms]) :-
   xml_pillow_type_check(shallow, text, First),
   !,
   xml_pillow_q_deep_text_find(Rest, Rest_Terms).
xml_pillow_q_deep_text_find([First|Rest], Result_Text) :-
   xml_pillow_type_check(shallow, inner_element, First),
   !,
   xml_pillow_navigate:any_children(First, Children),
   xml_pillow_q_deep_text_find(Children, Sub_Text),
   xml_pillow_q_deep_text_find(Rest, Rest_Text),
   append(Sub_Text, Rest_Text, Result_Text).
xml_pillow_q_deep_text_find([_|Rest], Text) :-
   !,
   xml_pillow_q_deep_text_find(Rest, Text).


xml_pillow_q_retrieve_tag_name(Text, [], Text) :-
   !.
xml_pillow_q_retrieve_tag_name(_, Name, Name).


note_type(Type, Note) :-
   xml_pillow_type_check(shallow, note, Note),
   functor(Note, Type, _).


/******************************************************************/


