

/******************************************************************/
/***                                                            ***/
/***            XML-Pillow:  Pretty Printing                    ***/
/***                                                            ***/
/******************************************************************/


:- module(xml_pillow_print, [
      url_chars/1,
      http_response_chars/1,
      start_tag/1,
      end_tag/1,
      xml_deep/2, xml_deep/3,
      xml_shallow/2, xml_shallow/3]).


:- use_module(xml_pillow_www_support).
:- use_module(xml_pillow_query).
:- use_module(ascii_text).


/*** interface ****************************************************/


/* url_chars(+URL_Split) <-
      prints URL given in splitted form according to module
      xml_pillow_www_support. */

url_chars(URL_Split) :-
   xml_pillow_www_support:url_compose(URL_Split, Composed),
   ascii_text:char_print(Composed).


/* http_response_chars(+Response) <-
      pretty prints HTTP response text Response. */

http_response_chars([First|Rest]) :-
   functor(First, Name, Args),
   write(Name),
   write(': '),
   xml_pillow_p_aux_arg_print(First, 1, Args),
   xml_pillow_print:http_response_chars(Rest).
http_response_chars([]).


/* start_tag(+Term) <-
      pretty prints start tag of element Term. */

start_tag(Term) :-
   xml_pillow_query:element_name(Term, Tag),
   xml_pillow_query:attributes(Term, Attributes),
   write('<'),
   write(Tag),
   checklist(xml_pillow_print:xml_deep(original),
      Attributes),
   write('>').


/* end_tag(+Term) <-
      pretty prints end tag of element Term. */

end_tag(Term) :-
   xml_pillow_query:element_name(Term, Tag),
   write('</'),
   write(Tag),
   write('>').


/* xml_deep(+Style, +Term) <-
      pretty prints pillow Term, Style = original prints text
      as formatted in the original document, Style = normalized
      prints text in a clean space indented style. */

xml_deep(_, Name=Value) :-
   !,
   write(' '),
   write(Name),
   write('="'),
   ascii_text:char_print(Value),
   write('"').
xml_deep(_, Attribute) :-
   atom(Attribute),
   !,
   write(' '),
   write(Attribute),
   write('="'),
   write(Attribute),
   write('"').
xml_deep(Style, XML) :-
   xml_pillow_print:xml_deep(Style, XML, 0).


/* xml_shallow(+Style, +Term) <-
      pretty prints pillow Term, Style = original prints text
      as formatted in the original document, Style = normalized
      prints text in a clean space indented style, children elements
      are omitted. */

xml_shallow(Style, XML) :-
   xml_pillow_print:xml_shallow(Style, XML, 0).


/* xml_deep(+Style, +Term, +Spaces) <-
      pretty prints pillow Term, Style = original prints text
      as formatted in the original document, Style = normalized
      prints text in a clean space indented style, Spaces corresponds
      to the minimal number of spaces at the beginning of each line. */

xml_deep(_, [], _) :-
   !.
xml_deep(Style, Text, Spaces) :-
   xml_pillow_type_check(deep, text, Text),
   xml_pillow_p_is_output_text(Style, Text),
   !,
   xml_pillow_p_spaces_output(Style, Spaces),
   xml_pillow_p_text_output(Style, Text),
   xml_pillow_p_line_end_output(Style).
xml_deep(_, Text, _) :-
   xml_pillow_type_check(deep, text, Text).
xml_deep(Style, [First|Rest], Spaces) :-
   !,
   xml_pillow_print:xml_deep(Style, First, Spaces),
   xml_pillow_print:xml_deep(Style, Rest, Spaces).
xml_deep(Style, comment(Text), Spaces) :-
   !,
   xml_pillow_p_spaces_output(Style, Spaces),
   write('<!-- '),
   ascii_text:char_print(Text),
   write(' -->'),
   xml_pillow_p_line_end_output(Style).
xml_deep(Style, declare(Text), Spaces) :-
   !,
   xml_pillow_p_spaces_output(Style, Spaces),
   write('<!'),
   ascii_text:char_print(Text),
   write('>'),
   xml_pillow_p_line_end_output(Style).
xml_deep(Style, xmldecl(Attributes), Spaces) :-
   !,
   xml_pillow_p_spaces_output(Style, Spaces),
   write('<?xml'),
   checklist(xml_pillow_print:xml_deep(_),
      Attributes),
   write('>'),
   xml_pillow_p_line_end_output(Style).
xml_deep(Style, elem(Tag, Attributes), Spaces) :-
   !,
   xml_pillow_p_spaces_output(Style, Spaces),
   write('<'),
   write(Tag),
   checklist(xml_pillow_print:xml_deep(_),
      Attributes),
   write('/>'),
   xml_pillow_p_line_end_output(Style).
xml_deep(Style, $(Tag, Attributes), Spaces) :-
   !,
   xml_pillow_p_spaces_output(Style, Spaces),
   xml_pillow_print:start_tag($(Tag, Attributes)),
   xml_pillow_p_line_end_output(Style).
xml_deep(Style, Term, Spaces) :-
   functor(Term, env, 3),
   !,
   xml_pillow_p_spaces_output(Style, Spaces),
   xml_pillow_print:start_tag(Term),
   xml_pillow_p_line_end_output(Style),
   xml_pillow_navigate:any_children(Term, Children),
   More_Spaces is Spaces + 2,
   xml_pillow_print:xml_deep(Style, Children, More_Spaces),
   xml_pillow_p_spaces_output(Style, Spaces),
   xml_pillow_print:end_tag(Term),
   xml_pillow_p_line_end_output(Style).


/* xml_shallow(+Style, +Term, +Spaces) <-
      pretty prints pillow Term, Style = original prints text
      as formatted in the original document, Style = normalized
      prints text in a clean space indented style, Spaces corresponds
      to the minimal number of spaces at the beginning of each line.
      Child elements of Term are omitted. */

xml_shallow(Style, Term, Spaces) :-
   xml_pillow_type_check(shallow, nonempty_element, Term),
   !,
   xml_pillow_p_spaces_output(Style, Spaces),
   xml_pillow_print:start_tag(Term),
   xml_pillow_p_line_end_output(Style),
   xml_pillow_navigate:any_children(Term, Children),
   More_Spaces is Spaces + 2,
   xml_pillow_print:xml_shallow(Style, Children, More_Spaces),
   xml_pillow_p_spaces_output(Style, Spaces),
   xml_pillow_print:end_tag(Term),
   xml_pillow_p_line_end_output(Style).
xml_shallow(Style, [First|Rest], Spaces) :-
   \+ xml_pillow_type_check(deep, text, [First|Rest]),
   xml_pillow_p_term_short(Style, First, Spaces),
   xml_pillow_print:xml_shallow(Style, Rest, Spaces),
   !.
xml_shallow(Style, Term, Spaces) :-
   xml_pillow_print:xml_deep(Style, Term, Spaces).


/*** implementation ***********************************************/


xml_pillow_p_aux_arg_print(Term, Current, Args) :-
   Current < Args,
   xml_pillow_p_aux_arg_print(Term, Current, Args, ', '),
   !.

xml_pillow_p_aux_arg_print(Term, Current, Args) :-
   Current = Args,
   xml_pillow_p_aux_arg_print(Term, Current, Args, '\n'),
   !.

xml_pillow_p_aux_arg_print(_, Current, Args) :-
   Current > Args.

xml_pillow_p_aux_arg_print(Term, Current, Args, Terminator) :-
   arg(Current, Term, Content),
   is_list(Content),
   !,
   ascii_text:char_print(Content),
   write(Terminator),
   xml_pillow_aux_next_arg_print(Term, Current, Args).

xml_pillow_p_aux_arg_print(Term, Current, Args, Terminator) :-
   arg(Current, Term, Content),
   write(Content),
   write(Terminator),
   xml_pillow_aux_next_arg_print(Term, Current, Args).

xml_pillow_aux_next_arg_print(Term, Current, Args) :-
   Next is Current + 1,
   xml_pillow_p_aux_arg_print(Term, Next, Args).

xml_pillow_p_is_output_text(original, _).

xml_pillow_p_is_output_text(normalized, Text) :-
   ascii_text:is_visible(Text).

xml_pillow_p_text_output(original, Text) :-
   ascii_text:char_print(Text).

xml_pillow_p_text_output(normalized, Text) :-
   ascii_text:trim(Text, Trimmed_Text),
   ascii_text:char_print(Trimmed_Text).

xml_pillow_p_line_end_output(original).

xml_pillow_p_line_end_output(normalized) :-
   nl.

xml_pillow_p_spaces_output(original, _).

xml_pillow_p_spaces_output(normalized, Spaces) :-
   tab(Spaces).

xml_pillow_p_term_short(Style, Term, Spaces) :-
   xml_pillow_type_check(shallow, element, Term),
   !,
   xml_pillow_query:element_name(Term, Tag),
   xml_pillow_p_spaces_output(Style, Spaces),
   write('<'),
   write(Tag),
   write(' ...>'),
   xml_pillow_p_line_end_output(Style).

xml_pillow_p_term_short(Style, Term, Spaces) :-
   xml_pillow_print:xml_deep(Style, Term, Spaces).


/******************************************************************/


