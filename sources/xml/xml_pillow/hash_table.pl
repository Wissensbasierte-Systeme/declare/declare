

/******************************************************************/
/***                                                            ***/
/***             Hash-Table: A Prolog Hash Table                ***/
/***                                                            ***/
/******************************************************************/


:- module(hash_table, [
      value_put/3,
      value_put_unique/3,
      value_get/3,
      value_remove/2,
      value_remove_first/2,
      remove_all/1,
      key/2,
      keys/2,
      elements/2,
      unique_table_key/1 ]).


/*** interface ****************************************************/


/* remove_all(+Table_Key) <-
      removes all entries of hash table identified by table
      key Table_Key. */

remove_all(Table_Key) :-
   recorded(Table_Key, Element_Key, _),
   !,
   hash_table:value_remove(Table_Key, Element_Key),
   hash_table:remove_all(Table_Key).
remove_all(_).


/* unique_table_key(+Table_Key) <-
      creates unique table key that is never created again within
      the same run of the program. */

unique_table_key(Table_Key) :-
   recorded('current~time~counter~~~', Table_Key, Reference),
   erase(Reference),
   hash_table_next_unique_key(Table_Key).
unique_table_key(Table_Key) :-
   \+ recorded('current~time~counter~~~', Table_Key),
   get_time(Table_Key),
   hash_table_next_unique_key(Table_Key).


/* key(+Table_Key, ?Key) <-
      retrieves an element key Key used in the hash table identified
      by table key Table_Key. */

key(Table_Key, Key) :-
   recorded(Table_Key, Key).


/* key(+Table_Key, -Keys) <-
      retrieves all element keys Keys used in the hash table
      identified by table key Table_Key. */

keys(Table_Key, Keys) :-
   findall( Key,
      hash_table:key(Table_Key, Key),
      Keys ).


/* elements(+Table_Key, -Elements) <-
      retrieves all elements stored in the hash table identified
      by table key Table_Key. */

elements(Table_Key, Elements) :-
   hash_table:keys(Table_Key, Keys),
   maplist(hash_table_get_key_elements(Table_Key),
      Keys, Raw_Elements),
   flatten(Raw_Elements, Elements).


/* value_remove(+Table_Key, +Element_Key) <-
      removes all elements stored in the hash table identified by
      table key Table_Key with element key Element_Key. */

value_remove(Table_Key, Element_Key) :-
   hash_table:value_remove_first(Table_Key, Element_Key),
   !,
   hash_table:value_remove(Table_Key, Element_Key).
value_remove(Table_Key, Element_Key) :-
   recorded(Table_Key, Element_Key, Key_Reference) ->
      erase(Key_Reference)
      ; true.


/* value_remove_first(+Table_Key, +Element_Key) <-
      removes first element stored in the hash table identified by
      table key Table_Key with element key Element_Key. */

value_remove_first(Table_Key, Element_Key) :-
   hash_table_key_construct(Table_Key, Element_Key, Key),
   recorded(Key, _, Reference),
   !,
   erase(Reference).


/* value_put_unique(+Table_Key, +Element_Key, +Value) <-
      inserts Value into hash table Table_Key using key Element_Key,
      makes sure that no other value is stored using the same
      element key. */

value_put_unique(Table_Key, Element_Key, Value) :-
   hash_table:value_remove(Table_Key, Element_Key),
   hash_table:value_put(Table_Key, Element_Key, Value),
   !.
value_put_unique(Table_Key, Element_Key, Value) :-
   hash_table:value_put(Table_Key, Element_Key, Value).


/* value_put(+Table_Key, +Element_Key, +Value) <-
      inserts Value into hash table Table_Key using key Element_Key,
      several values f�r element key are allowed. */

value_put(Table_Key, Element_Key, Value) :-
   hash_table_key_construct(Table_Key, Element_Key, Key),
   recorda(Key, Value),
   recorded(Table_Key, Element_Key) ->
      true
      ; recorda(Table_Key, Element_Key).


/* value_get(+Table_Key, +Element_Key, -Value) <-
      retrieves value Value from hash table Table_Key using key 
      Element_Key. */

value_get(Table_Key, Element_Key, Value) :-
   hash_table_key_construct(Table_Key, Element_Key, Key),
   recorded(Key, Value).


/*** implementation ***********************************************/


hash_table_key_construct(Table_Key, Element_Key, Key) :-
   is_list(Element_Key),
   !,
   atom_codes(Elem_Key, Element_Key),
   concat_atom([Table_Key, '~~~', Elem_Key], Key).

hash_table_key_construct(Table_Key, Element_Key, Key) :-
   atom(Element_Key),
   concat_atom([Table_Key, '~~~', Element_Key], Key).

hash_table_next_unique_key(Previous_Key) :-
   Next_Key is Previous_Key + 1,
   recorda('current~time~counter~~~', Next_Key).

hash_table_get_key_elements(Table_Key, Element_Key, Elements) :-
   bagof( Element,
      hash_table:value_get(Table_Key, Element_Key, Element),
      Elements ).


/******************************************************************/


