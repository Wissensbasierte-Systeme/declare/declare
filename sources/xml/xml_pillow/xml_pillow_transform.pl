

/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  Higher Operations with Data           ***/
/***                                                            ***/
/******************************************************************/


:- module(xml_pillow_transform, [
      term_to_page/2,
      comment_create/2,
      declaration_create/2,
      xml_decl_create/2,
      element_create/3,
      element_create/4,
      attribute_create/3,
      document_transform/3,
      page_term_remove/3,
      page_term_replace/4,
      page_term_insert/5,
      child_remove/3,
      child_insert/4,
      children_replace/3,
      attribute_remove/3,
      attribute_edit/4,
      attribute_add/4,
      html_logical_attribute_add/3,
      element_name_edit/3,
      note_text_edit/3,
      subtree_copy/4 ]).


:- use_module(ascii_text).
:- use_module(xml_pillow_navigate).
:- use_module(xml_pillow_query).


/*** interface ****************************************************/


/* term_to_page(+Term, -Page) <-
      transforms pillow Term into a document
      with root element Term. */

term_to_page(Term, [Term]).


/* comment_create(+Text, -Comment) <-
      creates pillow Comment term with text Text. */

comment_create(Text, Comment) :-
   Comment =.. [comment, Text].


/* declaration_create(+Text, -Declaration) <-
      creates pillow Declaration term with text Text. */

declaration_create(Text, Declaration) :-
   Declaration =.. [declare, Text].


/* xml_decl_create(+Attributes, -XML_Decl) <-
      creates pillow XML_Decl term with Attributes. */

xml_decl_create(Attributes, XML_Decl) :-
   XML_Decl =.. [xmldecl, Attributes].


/* document_transform(+Terms, +Filter, -Result) <-
      transforms pillow document Terms into Result using
      transformation Filter. */

document_transform(Terms, Filter, Result) :-
   xml_pillow_tr_list_filter(Terms, Filter, Result, keep),
   !.


/* page_term_remove(+Page, +Term, -New_Page) <-
      removes Term from document Page, result is New_Page. */

page_term_remove(Page, Term, New_Page) :-
   xml_pillow_navigate:father_element(Page, Term, Father),
   xml_pillow_navigate:any_children(Father, Children),
   delete(Children, Term, Reduced_Children),
   xml_pillow_tr_page_children_replace(Page, Father, Reduced_Children,
      New_Page).
page_term_remove(Page, Term, New_Page) :-
   \+ xml_pillow_navigate:father_element(Page, Term, _),
   delete(Page, Term, New_Page).


/* children_replace(+Element, +New_Children, -New_Element) <-
      replaces list of children of Element by New_Children, result
      is New_Element. */

children_replace(Element, New_Children, New_Element) :-
   xml_pillow_query:element_name(Element, Name),
   xml_pillow_query:attributes(Element, Attributes),
   element_create(Name, Attributes, New_Children, New_Element).


/* child_remove(+Element, +Child, -New_Element) <-
      removes child term Child from Element, result is New_Element. */

child_remove(Element, Child, New_Element) :-
   xml_pillow_type_check(shallow, inner_element, Element),
   xml_pillow_query:element_name(Element, Name),
   xml_pillow_query:attributes(Element, Attributes),
   xml_pillow_navigate:any_children(Element, List),
   delete(List, Child, New_Children),
   element_create(Name, Attributes, New_Children, New_Element).


/* child_remove(+Element, +Attr_Name, -New_Element) <-
      removes attribute with name Attr-Name from Element, result is
      New_Element. */

attribute_remove(Element, Attr_Name, New_Element) :-
   xml_pillow_type_check(shallow, element, Element),
   xml_pillow_query:attributes(Element, Attributes),
   sublist(xml_pillow_tr_attr_filter(Attr_Name),
      Attributes, New_Attributes),
   xml_pillow_query:element_name(Element, Name),
   xml_pillow_transform:subtree_copy(Element, Name, New_Attributes,
      New_Element).


/* html_logical_attribute_add(+Element, +Attribute, -New_Element) <-
      adds logical HTML Attribute to Element, result is New_Element. */

html_logical_attribute_add(Element, Attribute, New_Element) :-
   xml_pillow_transform:attribute_add(Element, Attribute, Attribute,
      New_Element).


/* element_create(+Name, +Attributes, -Term) <-
      creates empty element Term with Name and Attributes. */

element_create(Name, Attributes, Term) :-
   Term =.. [elem, Name, Attributes].


/* attribute_create(+Name, +Value, -Attribute) <-
      creates Attribute as Name=Value pair. */

attribute_create(Name, Value, =(Name, Value)).


/* element_name_edit(+Element, +New_Name, -New_Element) <-
      sets name of Element as New_Name, result is New_Element. */

element_name_edit(Element, New_Name, New_Element) :-
   xml_pillow_type_check(shallow, element, Element),
   xml_pillow_query:attributes(Element, Attributes),
   xml_pillow_transform:subtree_copy(Element, New_Name, Attributes,
      New_Element).


/* note_text_edit(+Note, +New_Text, -New_Note) <-
      replaces text of comment or declaration Note by New_Text,
      result is New_Element. */

note_text_edit(comment(_), Text, comment(Text)).
note_text_edit(declare(_), Text, declare(Text)).


/* page_term_replace(+Page, +Term, +New_Term, -New_Page) <-
      replaces Term in document Page by New_Term, result is
      New_Element. */

page_term_replace(Page, Term, New_Term, New_Page) :-
   xml_pillow_navigate:father_element(Page, Term, Father),
   xml_pillow_navigate:any_children(Father, Children),
   ascii_text:list_unique_replace(Children, Term, New_Term,
      New_Children),
   xml_pillow_tr_page_children_replace(Page, Father, New_Children,
      New_Page).
page_term_replace(Page, Term, New_Term, New_Page) :-
   \+ xml_pillow_navigate:father_element(Page, Term, _),
   ascii_text:list_unique_replace(Page, Term, New_Term, New_Page).


/* child_insert(+Element, +Child, +Position -New_Element) <-
      inserts term Child into list of children of Element at Position,
      result is New_Element. */

child_insert(Element, Child, Position, New_Element) :-
   xml_pillow_type_check(shallow, inner_element, Element),
   xml_pillow_query:element_name(Element, Name),
   xml_pillow_query:attributes(Element, Attributes),
   xml_pillow_navigate:any_children(Element, List),
   ascii_text:list_insert(List, Child, Position, New_Children),
   element_create(Name, Attributes, New_Children, New_Element).


/* attribute_edit(+Term, +Attribute, +New_Value, -New_Term) <-
      replaces value of Attribute of element Term by New_Value,
      result is New_Term. */

attribute_edit(Term, Attribute, New_Value, New_Term) :-
   xml_pillow_type_check(shallow, element, Term),
   xml_pillow_query:attribute_name(Term, Attribute),
   xml_pillow_query:attributes(Term, Attributes),
   xml_pillow_tr_recursive_replace(Attributes, Attribute, New_Value,
      New_Attributes),
   xml_pillow_query:element_name(Term, Name),
   xml_pillow_transform:subtree_copy(Term, Name, New_Attributes,
      New_Term).


/* attribute_add(+Element, +Attribute, +Value, -New_Element) <-
      adds attribute Attribute=Value to Element,
      result is New_Element. */

attribute_add(Element, Attribute, Value, New_Element) :-
   xml_pillow_type_check(shallow, element, Element),
   xml_pillow_query:element_name(Element, Name),
   xml_pillow_query:attributes(Element, Attributes),
   atom_codes(Inner_Attr_Name, Attribute),
   attribute_create(Inner_Attr_Name, Value, Attr_Term),
   xml_pillow_transform:subtree_copy(Element, Name,
      [Attr_Term|Attributes], New_Element).


/* subtree_copy(+Element, +Name, +Attributes, -New_Element) <-
      copies children of Element into a new element New_Element with
      Name and Attributes. */

subtree_copy(Element, Name, Attributes, New_Element) :-
   xml_pillow_type_check(shallow, empty_element, Element),
   !,
   element_create(Name, Attributes, New_Element).
subtree_copy(Element, Name, Attributes, New_Element) :-
   xml_pillow_navigate:any_children(Element, Children),
   element_create(Name, Attributes, Children, New_Element).


/* element_create(+Name, +Attributes, +Children, -Term) <-
      creates element Term with Name, Attribute, and list of
      Children. */

element_create(Name, Attributes, Children, Term) :-
   Term =.. [env, Name, Attributes, Children].


/* page_term_insert(+Page, +Term, +Father, +Position, -New_Page) <-
      inserts Term as child of Father at local Position into
      Page, result is New_Page. */

page_term_insert(Page, Term, Father, Position, New_Page) :-
   xml_pillow_navigate:any_children(Father, Children),
   ascii_text:list_insert(Children, Term, Position, New_Children),
   xml_pillow_tr_page_children_replace(Page, Father, New_Children,
      New_Page).


/*** implementation ***********************************************/


xml_pillow_tr_list_neutral(keep).
xml_pillow_tr_list_neutral(kill_tree).
xml_pillow_tr_list_neutral(kill_tag).
xml_pillow_tr_list_neutral(keep_tree).

xml_pillow_tr_term_neutral(keep).
xml_pillow_tr_term_neutral(keep_content).
xml_pillow_tr_term_neutral(kill_descendant).

xml_pillow_tr_shallow_tag_kill(kill_tag).
xml_pillow_tr_shallow_tag_kill(kill_content).

xml_pillow_tr_list_filter([], _, [], _).

xml_pillow_tr_list_filter([First|Rest], Filter, Result,
      kill_descendant) :-
   xml_pillow_type_check(shallow, element, First),
   !,
   xml_pillow_tr_list_filter(Rest, Filter, Result, kill_descendant).

xml_pillow_tr_list_filter([First|Rest], Filter, Result,
      kill_descendant) :-
   xml_pillow_tr_term_filter(First, Filter, First_Result),
   xml_pillow_tr_list_filter(Rest, Filter, Rest_Result,
      kill_descendant),
   append(First_Result, Rest_Result, Result).

xml_pillow_tr_list_filter([First|Rest], Filter, Result,
      kill_content) :-
   \+ xml_pillow_type_check(shallow, element, First),
   !,
   xml_pillow_tr_list_filter(Rest, Filter, Result, kill_content).

xml_pillow_tr_list_filter([First|Rest], Filter, Result,
      kill_content) :-
   xml_pillow_tr_term_filter(First, Filter, First_Result),
   xml_pillow_tr_list_filter(Rest, Filter, Rest_Result, kill_content),
   append(First_Result, Rest_Result, Result).

xml_pillow_tr_list_filter([First|Rest], Filter, Result,
      keep_content) :-
   \+ xml_pillow_type_check(shallow, element, First),
   !,
   xml_pillow_tr_list_filter(Rest, Filter, Rest_Result, keep_content),
   append([First], Rest_Result, Result).

xml_pillow_tr_list_filter([First|Rest], Filter, Result,
      keep_content) :-
   xml_pillow_tr_term_filter(First, Filter, First_Result),
   xml_pillow_tr_list_filter(Rest, Filter, Rest_Result, keep_content),
   append(First_Result, Rest_Result, Result).

xml_pillow_tr_list_filter([First|Rest], Filter, Result, State) :-
   xml_pillow_tr_list_neutral(State),
   xml_pillow_tr_term_filter(First, Filter, First_Result),
   xml_pillow_tr_list_filter(Rest, Filter, Rest_Result, State),
   append(First_Result, Rest_Result, Result).

xml_pillow_tr_term_filter(Term, Filter, Result) :-
   call(Filter,
      Term, New_Term, New_State),
   xml_pillow_tr_propagate_state(New_Term, Filter, Result, New_State).

xml_pillow_tr_propagate_state(Term, _, [Term], keep_tree).

xml_pillow_tr_propagate_state(Term, Filter, [Result], State) :-
   xml_pillow_tr_term_neutral(State),
   xml_pillow_type_check(shallow, inner_element, Term),
   xml_pillow_navigate:any_children(Term, Content),
   xml_pillow_tr_list_filter(Content, Filter, Content_Result, State),
   xml_pillow_query:element_name(Term, Name),
   xml_pillow_query:attributes(Term, Attributes),
   element_create(Name, Attributes, Content_Result, Result).

xml_pillow_tr_propagate_state(Term, _, [Term], State) :-
   xml_pillow_tr_term_neutral(State),
   \+ xml_pillow_type_check(shallow, inner_element, Term).

xml_pillow_tr_propagate_state(Term, Filter, Result, State) :-
   xml_pillow_tr_shallow_tag_kill(State),
   xml_pillow_type_check(shallow, inner_element, Term),
   xml_pillow_navigate:any_children(Term, Content),
   xml_pillow_tr_list_filter(Content, Filter, Result, kill_tag).

xml_pillow_tr_propagate_state(Term, _, [], State) :-
   xml_pillow_tr_shallow_tag_kill(State),
   \+ xml_pillow_type_check(shallow, inner_element, Term).

xml_pillow_tr_propagate_state(_, _, [], kill_tree).

xml_pillow_tr_recursive_replace([], _, _, []).

xml_pillow_tr_recursive_replace([First|Rest], Attr_Name, New_Value,
      [New_First|Rest]) :-
   xml_pillow_query:attribute_name(First, Attr_Name),
   !,
   atom_codes(Inner_Attr_Name, Attr_Name),
   attribute_create(Inner_Attr_Name, New_Value, New_First).

xml_pillow_tr_recursive_replace([First|Rest], Attr_Name, New_Value,
      [First|New_Rest]) :-
   xml_pillow_tr_recursive_replace(Rest, Attr_Name, New_Value,
      New_Rest).

xml_pillow_tr_attr_filter(Name, Attr_Term) :-
   \+ xml_pillow_query:attribute_name(Attr_Term, Name).

xml_pillow_tr_page_children_replace(Page, Element, New_Children,
      New_Page) :-
   xml_pillow_transform:children_replace(Element, New_Children,
      New_Element),
   xml_pillow_transform:page_term_replace(Page, Element, New_Element,
      New_Page).


/******************************************************************/


