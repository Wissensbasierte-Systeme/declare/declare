

/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  Web Support                           ***/
/***                                                            ***/
/******************************************************************/


:- use_module(ascii_text).


/*** interface ****************************************************/


/* hypertext_param_fetch(+URL, +Request, -Page) <-
      downloads document addressed by URL using HTTP request
      parameters Request, converts it into pillow term tree Page. */

hypertext_param_fetch(URL, Request, Page) :-
   xml_pillow_www_support:binary_fetch(URL, Request, Content),
   html2terms(Content, Page).


/* url_fetch(+URL, +Request, -Response) <-
      fetches HTTP response of HTTP request for URL with request
      parameters Request. */

url_fetch(URL, Request, Response) :-
   url_info(URL, URL_Term),
   fetch_url(URL_Term, Request, Response).


/* binary_fetch(+URL, +Request, -Content) <-
      downloads resource addressed by URL using HTTP request
      parameters Request as binary resource Content. */

binary_fetch(URL, Request, Content) :-
   xml_pillow_www_support:url_fetch(URL, Request, Response),
   member(content(Content), Response).


/* url_protocol_set(+Split, +Protocol, -New_Split) <-
      sets new Protocol in splitted URL Split,
      result is New_Split. */

url_protocol_set(Split, Protocol, New_Split) :-
   ascii_text:list_nth_replace(Split, 1, Protocol, New_Split).


/* url_host_set(+Split, +Host, -New_Split) <-
      sets new Host in splitted URL Split, result is New_Split. */

url_host_set(Split, Host, New_Split) :-
   ascii_text:list_nth_replace(Split, 2, Host, New_Split).


/* url_port_set(+Split, +Port, -New_Split) <-
      sets new Port in splitted URL Split, result is New_Split. */

url_port_set(Split, Port, New_Split) :-
   ascii_text:list_nth_replace(Split, 3, Port, New_Split).


/* url_path_set(+Split, +Path, -New_Split) <-
      sets new Path in splitted URL Split, result is New_Split. */

url_path_set(Split, Path, New_Split) :-
   ascii_text:list_nth_replace(Split, 4, Path, New_Split).


/* url_file_name_set(+Split, +File_Name, -New_Split) <-
      sets new file name File_Name in splitted URL Split,
      result is New_Split. */

url_file_name_set(Split, File_Name, New_Split) :-
   ascii_text:list_nth_replace(Split, 5, File_Name, New_Split).


/* url_querystring_set(+Split, +Query, -New_Split) <-
      sets new query string Query in splitted URL Split,
      result is New_Split. */

url_querystring_set(Split, Query, New_Split) :-
   ascii_text:list_nth_replace(Split, 6, Query, New_Split).


/* url_anchor_set(+Split, +Anchor, -New_Split) <-
      sets new Anchor in splitted URL Split, result is New_Split. */

url_anchor_set(Split, Anchor, New_Split) :-
   ascii_text:list_nth_replace(Split, 7, Anchor, New_Split).


/* http_get_param_create(+Param, +Value, -Result) <-
      creates new HTTP parameter as Param=Value term Result. */

http_get_param_create(Param, Value, Result) :-
   append(Param, "=", Temp),
   append(Temp, Value, Result).


/* http_query_assign(+Text, +URL, -New_URL) <-
      assigns query string Text to URL given as text,
      result is New_URL. */

http_query_assign(Text, URL, New_URL) :-
   append(URL, [63], Temp_URL),
   append(Temp_URL, Text, New_URL).


/* http_query_param_get(+Query_String, +Param, -Value) <-
      extracts Value of query parameter Param from query string
      Query_String. */

http_query_param_get(Query_String, Param, Value) :-
   ascii_text:split(Query_String, 38, Params),
   xml_pillow_w_s_search_param(Params, Param, Value).


/* http_query_param_concat(+String_1, +String_2, -Result) <-
      concatenates query parameter strings String_1 and String_2,
      result is Result. */

http_query_param_concat(String_1, String_2, Result) :-
   flatten([String_1, 38, String_2], Result).


/* rel_to_abs_url(+Rel_URL, +Base_URL, -Abs_URL) <-
      converts a relative URL referring to host specified in Base_URL
      to absolute URL Abs_URL. Base_URL is given as splitted list,
      Abs_URL is constructed as splitted list. */

rel_to_abs_url(Rel_URL, Base_URL, Abs_URL) :-
   xml_pillow_www_support:rel_to_abs_url_text(Rel_URL, Base_URL,
      URL_Text),
   xml_pillow_www_support:url_split(URL_Text, Abs_URL).


/* rel_to_abs_url_text(+Rel_URL, +Base_URL, -Abs_URL) <-
      converts a relative URL referring to host specified in Base_URL
      to absolute URL Abs_URL. Base_URL is given as splitted list,
      Abs_URL is constructed as text URL. */

rel_to_abs_url_text([], Base_URL, URL_Text) :-
   xml_pillow_www_support:url_compose(Base_URL, URL_Text).
rel_to_abs_url_text(Rel_URL, _, Rel_URL) :-
   xml_pillow_w_s_protocols(Protocol, Length),
   ascii_text:to_lower_case(Rel_URL, Lower_Rel_URL),
   ascii_text:start(Lower_Rel_URL, Length, Protocol, _),
   !,
   Protocol = "http://" ->
      xml_pillow_www_support:url_split(Rel_URL, _)
      ; true.
rel_to_abs_url_text([47|Rest_Path], Base_URL, URL_Text) :-
   !,
   is_list(Base_URL),
   length(Base_URL, 7),
   xml_pillow_www_support:url_port_text(Base_URL, Port_Text),
   xml_pillow_www_support:url_host_text(Base_URL, Host_Text),
   flatten(["http://", Host_Text, Port_Text, 47, Rest_Path],
      URL_Text).
rel_to_abs_url_text(Rel_URL, Base_URL, URL_Text) :-
   is_list(Base_URL),
   length(Base_URL, 7),
   xml_pillow_www_support:url_select_path(Base_URL, Base_Path),
   append(Base_Path, Rel_URL, Raw_Path),
   ascii_text:split(Raw_Path, 47, Parts),
   xml_pillow_w_s_kill_dot_loop(Parts, Clean_Parts, _),
   ascii_text:compose_prefix(Clean_Parts, 47, Path_Text),
   xml_pillow_www_support:rel_to_abs_url_text(Path_Text, Base_URL,
      URL_Text).


/* abs_to_rel_url(+Abs_URL, +Base_URL, -Rel_URL) <-
      converts an absolute URL Abs_URL to a relaitve Rel_URL assuming
      the referring document's base address is given by Base_URL.
      Base_URL is given as splitted list of URL components. */

abs_to_rel_url([], _, "").
abs_to_rel_url([47|Rest_Path], [47|Rest_Base_URL], Rel_URL) :-
   !,
   ascii_text:split(Rest_Base_URL, 47, Base_Split),
   ascii_text:split(Rest_Path, 47, Ref_Split),
   xml_pillow_w_s_dot_construct(Base_Split, Ref_Split, Rel_URL).
abs_to_rel_url([47|Rest_Path], Base_URL, Rel_URL) :-
   !,
   xml_pillow_www_support:url_select_path(Base_URL, Raw_Path),
   append(Raw_Path, "/x", Base_Path),
   xml_pillow_www_support:abs_to_rel_url([47|Rest_Path], Base_Path,
      Rel_URL).
abs_to_rel_url(Abs_URL, Base_URL, Rel_URL) :-
   xml_pillow_w_s_protocols(Protocol, Length),
   ascii_text:to_lower_case(Abs_URL, Lower_Abs_URL),
   atom_codes(Protocol, Protocol_Text),
   ascii_text:start(Lower_Abs_URL, Length, Protocol_Text, _),
   !,
   Protocol = 'http://' ->
      xml_pillow_w_s_abs_to_rel(Abs_URL, Base_URL, Rel_URL)
      ; Rel_URL = Abs_URL.
abs_to_rel_url(Ref_Path, _, Ref_Path).


/* html_fetch_redirect(+URL, +Request, -Page, -URL_History) <-
      performs download of document given by URL with HTTP
      request parameters Request. The download procedure follows
      all redirections until a resource can be downloaded. The
      sequence of addressed (redirected) URL's is stored in
      URL_History. */

html_fetch_redirect(URL, Request, Page, URL_History) :-
   xml_pillow_w_s_fetch_redirect(URL, Request, [URL], Page,
      URL_History).


/*** implementation ***********************************************/


xml_pillow_w_s_fetch_redirect(URL, Request, Old_History, Page,
      URL_History) :-
   xml_pillow_www_support:url_fetch(URL, Request, Response),
   xml_pillow_w_s_decide_on_redirect(Response, Old_History, Page,
      URL_History).

xml_pillow_w_s_decide_on_redirect(Response, Old_History, Page,
      URL_History) :-
   xml_pillow_www_support:is_redirection(Response, New_URL),
   !,
   xml_pillow_w_s_decide_on_redirect_cycle(Old_History, New_URL,
      Page, URL_History).

xml_pillow_w_s_decide_on_redirect(Response, Old_History, Page,
      Old_History) :-
   member(content(Content), Response),
   html2terms(Content, Page).

xml_pillow_w_s_decide_on_redirect_cycle(Old_History, New_URL, _, _) :-
   member(New_URL, Old_History),
   !,
   fail.

xml_pillow_w_s_decide_on_redirect_cycle(Old_History, New_URL, Page,
      URL_History) :-
   xml_pillow_w_s_fetch_redirect(New_URL, [], [New_URL|Old_History],
      Page, URL_History).

xml_pillow_w_s_search_param([First|_], Param, Value) :-
   ascii_text:split(First, 61, [Param, Value]),
   !.

xml_pillow_w_s_search_param([_|Rest], Param, Value) :-
   xml_pillow_w_s_search_param(Rest, Param, Value).

/* known WWW protocols */

xml_pillow_w_s_protocols('http://', 7).
xml_pillow_w_s_protocols('ftp://', 6).
xml_pillow_w_s_protocols('mailto://', 9).
xml_pillow_w_s_protocols('javascript://', 13).

xml_pillow_w_s_kill_dot_loop(Parts, Clean_Parts, Signal) :-
   xml_pillow_w_s_eliminate_dots(Parts, Temp_Solution, Signal),
   xml_pillow_w_s_decide_next_loop(Temp_Solution, Signal,
      Clean_Parts).

xml_pillow_w_s_eliminate_dots([], [], pathfound).

xml_pillow_w_s_eliminate_dots([Path_Part], _, _) :-
   xml_pillow_w_s_url_path_upward(Path_Part),
   !,
   fail.

xml_pillow_w_s_eliminate_dots([Path_Part], [], pathfound) :-
   xml_pillow_w_s_url_path_constant(Path_Part).

xml_pillow_w_s_eliminate_dots([Path_Part], [Path_Part], pathfound).

xml_pillow_w_s_eliminate_dots([First, _|_], _, _) :-
   xml_pillow_w_s_url_path_upward(First),
   !,
   fail.

xml_pillow_w_s_eliminate_dots([First, Second|Rest], Text, Signal) :-
   xml_pillow_w_s_url_path_constant(First),
   !,
   xml_pillow_w_s_eliminate_dots([Second|Rest], Text, Signal).

xml_pillow_w_s_eliminate_dots([_, Second|Rest], Rest, dotsfound) :-
   xml_pillow_w_s_url_path_upward(Second).

xml_pillow_w_s_eliminate_dots([First, Second|Rest], Text, Signal) :-
   xml_pillow_w_s_url_path_constant(Second),
   !,
   xml_pillow_w_s_eliminate_dots([First|Rest], Text, Signal).

xml_pillow_w_s_eliminate_dots([First,Second|Rest], [First|Rest_Text],
                              Signal) :-
   \+ xml_pillow_w_s_url_path_upward(Second),
   xml_pillow_w_s_eliminate_dots([Second|Rest], Rest_Text, Signal).

xml_pillow_w_s_decide_next_loop(Solution, pathfound, Solution).

xml_pillow_w_s_decide_next_loop(Temp_Solution, dotsfound, Solution) :-
   xml_pillow_w_s_kill_dot_loop(Temp_Solution, Solution, _).

xml_pillow_w_s_url_path_separator(47).

xml_pillow_w_s_url_path_upward([46, 46]).

xml_pillow_w_s_url_path_constant([46]).

xml_pillow_w_s_abs_to_rel(Abs_URL, Base_URL, Rel_URL) :-
   xml_pillow_www_support:url_split(Abs_URL, Ref_URL_Split),
   xml_pillow_www_support:url_host(Base_URL, Base_Host),
   xml_pillow_www_support:url_host(Ref_URL_Split, Base_Host),
   !,
   xml_pillow_www_support:url_select_path(Ref_URL_Split, Ref_Path),
   xml_pillow_www_support:url_file_name(Ref_URL_Split, Ref_File),
   xml_pillow_w_s_file_win_clean(Ref_File, Clean_File),
   append(Ref_Path, Clean_File, Check_Path),
   xml_pillow_www_support:abs_to_rel_url(Check_Path, Base_URL,
      Rel_Path),
   xml_pillow_www_support:url_querystring_text(Ref_URL_Split, Ref_Query),
   xml_pillow_w_s_file_win_clean(Ref_Query, Clean_Query),
   xml_pillow_www_support:url_anchor_text(Ref_URL_Split, Ref_Anchor),
   xml_pillow_w_s_file_win_clean(Ref_Anchor, Clean_Anchor),
   flatten([Rel_Path, Clean_Query, Clean_Anchor], Rel_URL).

xml_pillow_w_s_abs_to_rel(Abs_URL, _, Abs_URL).

xml_pillow_w_s_dot_construct([First|Rest], [First|Ref_Rest],
      Rel_URL) :-
   !,
   xml_pillow_w_s_dot_construct(Rest, Ref_Rest, Rel_URL).

xml_pillow_w_s_dot_construct(Base_Split, Ref_Split, Rel_URL) :-
   xml_pillow_w_s_base_dots(Base_Split, Base_Dots),
   xml_pillow_w_s_win_clean_split(Ref_Split, Clean_Split),
   ascii_text:compose(Clean_Split, 47, Ref_Path),
   append(Base_Dots, Ref_Path, Rel_URL).

xml_pillow_w_s_base_dots([], []).
xml_pillow_w_s_base_dots([_], []) :-
   !.
xml_pillow_w_s_base_dots([_|Rest], [46, 46, 47|Rest_Dots]) :-
   xml_pillow_w_s_base_dots(Rest, Rest_Dots).

xml_pillow_w_s_win_clean_split([], []).

xml_pillow_w_s_win_clean_split([Single], [Single_Clean]) :-
   !,
   xml_pillow_w_s_file_win_clean(Single, Single_Clean).

xml_pillow_w_s_win_clean_split([First|Rest], [First|Clean_Rest]) :-
   xml_pillow_w_s_win_clean_split(Rest, Clean_Rest).

/* replaces illegal chars in Windows file names by underscores */

xml_pillow_w_s_file_win_clean([], []).

xml_pillow_w_s_file_win_clean([First|Rest], [95|Clean_Rest]) :-
   xml_pillow_w_s_win_illegal_char(First),
   !,
   xml_pillow_w_s_file_win_clean(Rest, Clean_Rest).

xml_pillow_w_s_file_win_clean([First|Rest], [First|Clean_Rest]) :-
   xml_pillow_w_s_file_win_clean(Rest, Clean_Rest).

/* illegal characters in MS Windows file names */

xml_pillow_w_s_win_illegal_char(42).
xml_pillow_w_s_win_illegal_char(47).
xml_pillow_w_s_win_illegal_char(58).
xml_pillow_w_s_win_illegal_char(60).
xml_pillow_w_s_win_illegal_char(62).
xml_pillow_w_s_win_illegal_char(63).
xml_pillow_w_s_win_illegal_char(92).
xml_pillow_w_s_win_illegal_char(124).


/******************************************************************/


