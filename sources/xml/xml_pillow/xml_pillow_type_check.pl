

/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  Type Checking of XML-Terms            ***/
/***                                                            ***/
/******************************************************************/


:- use_module(ascii_text).


/*** interface ****************************************************/


/* xml_pillow_type_check(+Mode, ?Type, +Term) <-
      determines XML type of a term Term. Term must be a pillow
      xml or html term. Atom Mode determines the intensity of the
      type check. Mode = deep causes a detailed type check without
      any assumptions regarding Term. Mode = shallow assumes Term to
      be a pillow term and extracts its type after a minimal
      check. */

xml_pillow_type_check(shallow, element, env(_, _, _)).

xml_pillow_type_check(shallow, element, Term) :-
   xml_pillow_type_check(shallow, empty_element, Term).

xml_pillow_type_check(shallow, empty_element, $(_, _)).

xml_pillow_type_check(shallow, empty_element, elem(_, _)).

xml_pillow_type_check(shallow, inner_element, env(_, _, [_|_])).

xml_pillow_type_check(shallow, leaf_element, Term) :-
   xml_pillow_type_check(shallow, empty_element, Term).

xml_pillow_type_check(shallow, leaf_element, env(_, _, Children)) :-
   sublist(xml_pillow_type_check(shallow, element),
      Children, []).

xml_pillow_type_check(shallow, nonempty_element, env(_, _, _)).

xml_pillow_type_check(shallow, comment, comment(_)).

xml_pillow_type_check(shallow, declaration, declare(_)).

xml_pillow_type_check(shallow, xml_declaration, xmldecl(_)).

xml_pillow_type_check(shallow, text, [_|_]).

xml_pillow_type_check(shallow, term_list, []).

xml_pillow_type_check(shallow, term_list, [_|_]).

xml_pillow_type_check(shallow, document, Terms) :-
   sublist(xml_pillow_type_check(shallow, element),
      Terms, Roots),
   length(Roots, 1).

xml_pillow_type_check(shallow, empty_page, []).

xml_pillow_type_check(shallow, attribute, =(_, _)).

xml_pillow_type_check(shallow, attribute, Attribute) :-
   atom(Attribute).

xml_pillow_type_check(shallow, note, Term) :-
   xml_pillow_t_c_note_term(Name),
   xml_pillow_type_check(shallow, Name, Term).

xml_pillow_type_check(deep, element, env(Tag, Attributes, Terms)) :-
   atom(Tag),
   checklist(xml_pillow_type_check(deep, attribute),
      Attributes),
   xml_pillow_type_check(deep, term_list, Terms).

xml_pillow_type_check(deep, element, Term) :-
   xml_pillow_type_check(deep, empty_element, Term).

xml_pillow_type_check(deep, empty_element, $(Tag, Attributes)) :-
   atom(Tag),
   checklist(xml_pillow_type_check(deep, attribute),
      Attributes).

xml_pillow_type_check(deep, empty_element, elem(Tag, Attributes)) :-
   atom(Tag),
   checklist(xml_pillow_type_check(deep, attribute),
      Attributes).

xml_pillow_type_check(deep, inner_element,
      env(Tag, Attributes, Terms)) :-
   xml_pillow_type_check(deep, element, env(Tag, Attributes, Terms)),
   \+ ascii_text:list_empty(Terms).

xml_pillow_type_check(deep, leaf_element, Term) :-
   xml_pillow_type_check(deep, empty_element, Term).

xml_pillow_type_check(deep, leaf_element,
      env(Tag, Attributes, Terms)) :-
   xml_pillow_type_check(deep, element, env(Tag, Attributes, Terms)),
   sublist(xml_pillow_type_check(shallow, element),
      Terms, []).

xml_pillow_type_check(deep, nonempty_element,
      env(Tag, Attributes, Terms)) :-
   xml_pillow_type_check(deep, element, env(Tag, Attributes, Terms)).

xml_pillow_type_check(deep, comment, comment(Text)) :-
   xml_pillow_t_c_term_is_string(Text).

xml_pillow_type_check(deep, declaration, declare(Text)) :-
   xml_pillow_t_c_term_is_string(Text).

xml_pillow_type_check(deep, xml_declaration, xmldecl(Attributes)) :-
   checklist(xml_pillow_type_check(deep, attribute),
      Attributes).

xml_pillow_type_check(deep, text, Text) :-
   xml_pillow_t_c_term_is_string(Text).

xml_pillow_type_check(deep, document, Terms) :-
   xml_pillow_type_check(deep, term_list, Terms),
   xml_pillow_type_check(shallow, document, Terms).

xml_pillow_type_check(deep, term_list, []).

xml_pillow_type_check(deep, term_list, [First|Rest]) :-
   xml_pillow_type_check(deep, _, First),
   xml_pillow_type_check(deep, term_list, Rest),
   !.

xml_pillow_type_check(deep, empty_page, []).

xml_pillow_type_check(deep, attribute, =(Name, Value)) :-
   atom(Name),
   xml_pillow_t_c_term_is_string(Value).

xml_pillow_type_check(deep, attribute, Name) :-
   atom(Name).

xml_pillow_type_check(deep, note, Term) :-
   xml_pillow_t_c_note_term(Name),
   xml_pillow_type_check(deep, Name, Term).


/*** implementation ***********************************************/


xml_pillow_t_c_term_is_string(Text) :-
   is_list(Text),
   checklist(xml_pillow_t_c_ascii_range,
      Text).

xml_pillow_t_c_ascii_range(Char) :-
   number(Char),
   between(0, 255, Char).

xml_pillow_t_c_note_term(comment).
xml_pillow_t_c_note_term(declaration).
xml_pillow_t_c_note_term(xml_declaration).


/******************************************************************/


