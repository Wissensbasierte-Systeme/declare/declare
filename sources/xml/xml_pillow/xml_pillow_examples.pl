

/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  Small Examples                        ***/
/***                                                            ***/
/******************************************************************/


:- module(xml_pillow_examples, [
      element_name_index/3,
      query_record_data/2,
      query_record_labels/2,
      query_record_label_value_pairs/2,
      query_record_named_tuple/2,
      element_shallow_compare/3,
      html_example_filter/4,
      whitespace_filter/4,
      html_table_cell_pattern/2,
      html_link_catch/4,
      html_javascript_filter/4,
      pillow_dfs_list/2,
      xml_ql_query/6,
      developer_pattern_example/2,
      name_tuple_example/2,
      name_transform_example/3 ]).

:- use_module(xml_pillow_data_operation).
:- use_module(xml_pillow_navigate).
:- use_module(xml_pillow_query).
:- use_module(xml_pillow_transform).
:- use_module(ascii_text).


/*** interface ****************************************************/


/* element_name_index(+Page, +Name, -Index) <-
      creates an index list of all elements contained in document
      Page possessing name Name. */

element_name_index(Page, Name, Index) :-
   xml_pillow_data_operation:collect_list(
      xml_pillow_navigate:named_element, [Page, Name, _], 3, Index).


/* query_record_...(+Element, -Record) <-
      assumes a simple XML structure of the type
         <record>
           <data1>data</data1>
           <data1>data</data1>
           ...
         </record>
      and retrieves records as ordered lists of elements and/or
      texts. Element must be the enclosing XML element. */

query_record_data(Element, Record) :-
   xml_pillow_navigate:element_children(Element, Children),
   maplist(xml_pillow_query:shallow_text,
      Children, Record).

query_record_labels(Element, Record) :-
   xml_pillow_navigate:element_children(Element, Children),
   maplist(xml_pillow_query:element_name,
      Children, Record).

query_record_label_value_pairs(Element, Record) :-
   xml_pillow_examples:query_record_data(Element, Data),
   xml_pillow_examples:query_record_labels(Element, Labels),
   ascii_text:list_to_pairlist(Labels, Data, Record).

query_record_named_tuple(Element, [Labels|Data]) :-
   xml_pillow_examples:query_record_data(Element, Data),
   xml_pillow_examples:query_record_labels(Element, Labels).


/* element_shallow_compare(-Delta, +Element_1, +Element_2) <-
      compares XML terms Element_1 and Element_2 according to the
      Delta specification of SWI-Prolog predicate predsort. Basing
      on this predicate XML elements may be sorted by their
      shallow text content in increasing order. */

element_shallow_compare(Delta, Element_1, Element_2) :-
   xml_pillow_query:shallow_text(Element_1, Text_1),
   xml_pillow_query:shallow_text(Element_2, Text_2),
   ascii_text:list_compare(Delta, Text_1, Text_2).


/* html_example_filter(+Page, +Term, -New_Term, -Command) <-
       transforms Term into New_Term and returns Command that
       specifies how to handle New_Term. Page is the document
       containing Term. */

html_example_filter(_, Term, Term, keep) :-
   \+ xml_pillow_type_check(shallow, element, Term).
html_example_filter(_, Term, Term, Result) :-
   xml_pillow_type_check(shallow, element, Term),
   xml_pillow_query:element_name(Term, Name),
   html_pillow_name_filter(Name, Result).


/* whitespace_filter(+Page, +Term, -New_Term, -Command) <-
       transforms Term into New_Term and returns Command that
       specifies how to handle New_Term. Page is the document
       containing Term. */

whitespace_filter(_, Term, Term, keep) :-
   \+ xml_pillow_type_check(shallow, text, Term),
   !.
whitespace_filter(_, Term, Term, kill_tree) :-
   ascii_text:is_white(Term).
whitespace_filter(_, Term, Term, keep) :-
   ascii_text:is_visible(Term).


/* html_table_cell_pattern(+Page, +Term) <-
       checks whether Term is an HTML table cell containing another
       table with attribute border set to 0. */

html_table_cell_pattern(_, Term) :-
   xml_pillow_query:element_name(Term, td),
   xml_pillow_navigate:child_element(Term, Child),
   xml_pillow_query:element_name(Child, table),
   xml_pillow_query:attribute_value(Child, "border", "0").


/* html_link_catch(+Page, +Page_URL, +Ref_URL, -New_Page) <-
       converts all links of HTML document Page with URL Page_URL
       into absolute links and envelopes them into Ref_URL by
       attaching a query string 'URL=<< link >>' to Ref_URL.
       New_Page is identical with Page but contains manipulated
       link URL's. */

html_link_catch(Page, Page_URL, Ref_URL, New_Page) :-
   xml_pillow_transform:document_transform(Page,
      html_link_twist_filter(Ref_URL, Page_URL), New_Page).


/* html_javascript_filter(+Page, +Term, -New_Term, -Command) <-
       filters unnecessary Script tags with content from an HTML
       page Page. */

html_javascript_filter(_, Term, _, kill_tree) :-
   xml_pillow_type_check(shallow, element, Term),
   xml_pillow_query:element_name(Term, script),
   !.
html_javascript_filter(_, Term, Term, keep_content).


/* xml_ql_query(+Pat_Pred, +Tuple_Pred, +Transform_Pred, +Wrap_Name,
      +Page, -Result) <-
      performs simple query that complies with the XML-QL query logic.
      Pat_Pred is a pattern checking predicate, Tuple_Pred constructs
      tuples from extracted patterns, Transform_Pred constructs result
      terms from a tuple, Wrap_Name is the root element name of the
      result Result. The operations are performed on document Page. */

xml_ql_query(Pat_Pred, Tuple_Pred, Transform_Pred, Wrap_Name, Page,
      Result) :-
%  1
   bagof( Pat,
      xml_pillow_data_operation:pattern_search(Page, Pat_Pred, Pat),
      [First_Pat|Rest_Pat] ),
%  2
   xml_ql_tuple_construct(Tuple_Pred, Variables, First_Pat,
      First_Tuple),
   maplist( xml_ql_tuple_construct(Tuple_Pred, _),
      Rest_Pat, Rest_Tuples ),
%  3
   maplist( call(Transform_Pred, Variables),
      [First_Tuple|Rest_Tuples], Transformed ),
%  4
   xml_ql_result_wrap(Wrap_Name, Transformed, Result).


/* developer_pattern_example(+Page, +Element) <-
      example pattern tester that checks whether element is a
      developer that contains lastname and firstname children. */

developer_pattern_example(_, Element) :-
   xml_pillow_query:element_name(Element, developer),
   xml_pillow_navigate:named_child_element(Element, lastname, _),
   xml_pillow_navigate:named_child_element(Element, firstname, _).


/* name_tuple_example(+Element, -Tuple_Element) <-
      example tuple constructor, its alternative bindings of result
      variable Tuple_Element constitute the tuple. */

name_tuple_example(Element, [last, Value]) :-
   xml_pillow_navigate:named_child_element(Element, lastname, Last),
   xml_pillow_query:shallow_text(Last, [trim], Value).
name_tuple_example(Element, [first, Value]) :-
   xml_pillow_navigate:named_child_element(Element, firstname, First),
   xml_pillow_query:shallow_text(First, [trim], Value).


/* name_transform_example(+Page, +Tuple, -Term) <-
      example pillow term constructor that creates a result term Term
      using the data given by tuple Tuple and document Page. */

name_transform_example(_, [Last, First], Term) :-
   flatten([Last, ", ", First], Name_Text),
   xml_pillow_transform:element_create(name,
      [profession="developer"], Name_Text, Term).


/* pillow_dfs_list(+Input, -Output) <-
      performs a depth first search in pillow term tree,
      the elements are returned in correct order as alternative
      results Output. */

pillow_dfs_list([First|Rest], [First|Rest]) :-
   ascii_text:is_ascii_text([First|Rest]),
   !.
pillow_dfs_list([First|_], Term) :-
   pillow_dfs_list(First, Term).
pillow_dfs_list([_|Rest], Term) :-
   !,
   pillow_dfs_list(Rest, Term).
pillow_dfs_list(env(N, A, C), env(N, A, C)).
pillow_dfs_list(env(_, _, C), Term) :-
   !,
   pillow_dfs_list(C, Term).
pillow_dfs_list(Term, Term) :-
   \+ is_list(Term).


/*** implementation ***********************************************/


html_pillow_is_table_tag(table).
html_pillow_is_table_tag(tr).
html_pillow_is_table_tag(td).


html_pillow_is_formatter_tag(b).
html_pillow_is_formatter_tag(font).
html_pillow_is_formatter_tag(p).
html_pillow_is_formatter_tag(pre).


html_pillow_name_filter(Name, keep_content) :-
   html_pillow_is_table_tag(Name),
   !.
html_pillow_name_filter(Name, kill_tag) :-
   html_pillow_is_formatter_tag(Name),
   !.
html_pillow_name_filter(Name, kill_content) :-
   \+ html_pillow_is_formatter_tag(Name),
   \+ html_pillow_is_table_tag(Name).


xml_pillow_html_url_attribute(a, "href").
xml_pillow_html_url_attribute(form, "target").
xml_pillow_html_url_attribute(frame, "src").
xml_pillow_html_url_attribute(img, "src").


xml_pillow_html_http_equiv("refresh").
xml_pillow_html_http_equiv("redirect").


html_link_twist_filter(URL, Base_URL, Term, New_Term, keep) :-
   xml_pillow_type_check(shallow, element, Term),
   xml_pillow_query:element_name(Term, meta),
   xml_pillow_query:attribute_value(Term, "http-equiv", Equiv_Value),
   ascii_text:to_lower_case(Equiv_Value, Lower_Case),
   xml_pillow_html_http_equiv(Lower_Case),
   !,
   xml_pillow_query:attribute_value(Term, "content", Content_Value),
   ascii_text:after_signal_char(Content_Value, 61, Old_URL),
   xml_pillow_www_support:rel_to_abs_url_text(Old_URL, Base_URL,
      Abs_URL),
   xml_pillow_www_support:get_param_create("URL", Abs_URL, Param),
   xml_pillow_www_support:http_query_assign(Param, URL, New_URL),
   ascii_text:before_signal_char(Content_Value, 61, Before_Text),
   append(Before_Text, [61], Temp_Text),
   append(Temp_Text, New_URL, New_Content_Value),
   xml_pillow_transform:attribute_edit(Term, content,
      New_Content_Value, New_Term).
html_link_twist_filter(URL, Base_URL, Term, New_Term, keep) :-
   xml_pillow_type_check(shallow, element, Term),
   xml_pillow_query:element_name(Term, Name),
   xml_pillow_html_url_attribute(Name, Attribute),
   !,
   xml_pillow_query:attribute_value(Term, Attribute, Old_URL),
   xml_pillow_www_support:rel_to_abs_url_text(Old_URL, Base_URL,
      Abs_URL),
   xml_pillow_www_support:http_get_param_create("URL", Abs_URL,
      Param),
   xml_pillow_www_support:http_query_assign(Param, URL, New_URL),
   xml_pillow_transform:attribute_edit(Term, Attribute, New_URL,
      New_Term).

html_link_twist_filter(_, _, Term, Term, keep).


xml_ql_tuple_construct(Tuple_Pred, Variables, Pattern, Tuple) :-
   bagof( Value,
      call(Tuple_Pred, Pattern, Value),
      Name_Values ),
   ascii_text:list_to_pairlist(Variables, Tuple, Name_Values).


xml_ql_result_wrap('', Terms, Terms) :-
   !.

xml_ql_result_wrap(Name, Terms, Result) :-
   xml_pillow_transform:element_create(Name, [], Terms, Result).


xml_ql_example_pattern(_, Element) :-
   xml_pillow_query:element_name(Element, developer).


xml_ql_example_tuple(Pat, [last, Value]) :-
   xml_pillow_navigate:named_child_element(Pat, lastname, Last),
   xml_pillow_query:shallow_text(Last, [trim], Value).
xml_ql_example_tuple(Pat, [first, Value]) :-
   xml_pillow_navigate:named_child_element(Pat, firstname, First),
   xml_pillow_query:shallow_text(First, [trim], Value).


xml_ql_example_transform(_, [Last, First], Term) :-
   flatten([Last, ", ", First], Name_Text),
   xml_pillow_transform:element_create(name,
      [profession="developer"], Name_Text, Term).


/******************************************************************/


