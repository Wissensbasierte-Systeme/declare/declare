

/******************************************************************/
/***                                                            ***/
/***         XML-Pillow:  Web Support                           ***/
/***                                                            ***/
/******************************************************************/


:- use_module(ascii_text).


/*** interface ****************************************************/


/* hypertext_simple_fetch(+URL, -Page) <-
      downloads document Page addressed by URL. */

hypertext_simple_fetch(URL, Page) :-
   xml_pillow_www_support:hypertext_param_fetch(URL, [], Page).


/* is_redirection(+Response, -Next_URL) <-
      succeeds if HTTP Response signals a redirection, returns
      target of redirection Next_URL. */

is_redirection(Response, Next_URL) :-
   member(status(_, Code, _), Response),
   xml_pillow_w_s_http_is_redirect_code(Code),
   member(location(New_URL), Response),
   atom_codes(New_URL, Next_URL).


/* is_hypertext(+Response, -Type) <-
      succeeds if HTTP Response belongs to a hypertext resource,
      returns its type (xml, html etc.) in Type. */

is_hypertext(Response, Type) :-
   member(content_type(text, Type, _), Response),
   xml_pillow_w_s_http_is_hypertext(Type).


/* is_server_error(+Response, -Error_Data) <-
      analyses whether HTTP Response notifies a server error and
      returns a status(Error, Code, Details) term with error name
      Error, error code Code, and a short description Details. */
   
is_server_error(Response, status(Error, Code, Details)) :-
   member(status(Error, Code, Details), Response),
   atom_to_number(Code, Code_Number),
   between(500, 599, Code_Number).


/* is_server_error(+Response, -Error_Data) <-
      analyses whether HTTP Response notifies an inaccessible
      resource and returns a status(Error, Code, Details) term
      with error name Error, error code Code, and a short
      description Details. */

is_inaccessible(Response, status(Error, Code, Details)) :-
   member(status(Error, Code, Details), Response),
   atom_to_number(Code, Code_Number),
   between(400, 499, Code_Number).


/* content_type(+Response, -Content_Data) <-
      extracts content type data as content_type(Type, Sub_Type,
      Params) term. Arguments are self-explaining. */

content_type(Response, content_type(Type, Sub_Type, Params)) :-
   member(content_type(Type, Sub_Type, Params), Response).


/* url_encode(+Text, -URL_Text) <-
      encodes Text so it can be used within a URL, result is
      URL_Text. */

url_encode(Text, URL_Text) :-
   xml_pillow_w_s_encode_loop(Text, Code_Text),
   flatten(Code_Text, URL_Text).


/* url_decode(+URL_Text, -Text) <-
      decodes URL_Text so it can be read as normal Text. */

url_decode(URL_Text, Text) :-
   xml_pillow_w_s_decode_loop(URL_Text, Code_Text),
   flatten(Code_Text, Text).


/* url_protocol(+Split, -Protocol) <-
      extracts protocol from URL in splitted form Split as atom. */

url_protocol(Split, Protocol) :-
   nth1(1, Split, Protocol).


/* url_host(+Split, -Host) <-
      extracts host from URL in splitted form Split as atom. */

url_host(Split, Host) :-
   nth1(2, Split, Host).


/* url_port(+Split, -Port) <-
      extracts Port from URL in splitted form Split as number. */

url_port(Split, Port) :-
   nth1(3, Split, Port).


/* url_select_path(+Split, -Path) <-
      extracts pure path component without server and file name
      from URL in splitted form Split. */

url_select_path(Split, Path) :-
   nth1(4, Split, Path).


/* url_file_name(+Split, -File_Name) <-
      extracts file name File_Name from URL in splitted form
      Split. */

url_file_name(Split, File_Name) :-
   nth1(5, Split, File_Name).


/* url_querystring(+Split, -Query) <-
      extracts query string Query from URL in splitted form Split. */

url_querystring(Split, Query) :-
   nth1(6, Split, Query).


/* url_anchor(+Split, -Anchor) <-
      extracts Anchor from URL in splitted form Split. */

url_anchor(Split, Anchor) :-
   nth1(7, Split, Anchor).


/* url_anchor_text(+Split, -Text) <-
      extracts Anchor from URL in splitted form Split using
      notation with leading #. */

url_anchor_text(Split, Text) :-
   xml_pillow_www_support:url_anchor(Split, Anchor),
   \+ Anchor = [] ->
      flatten(["#", Anchor], Text)
      ; Text = [].


/* url_querystring_text(+Split, -Text) <-
      extracts query string Query from URL in splitted form Split
      using notation with leading ?. */

url_querystring_text(Split, Text) :-
   xml_pillow_www_support:url_querystring(Split, Query),
   \+ Query = [] ->
      flatten(["?", Query], Text)
      ; Text = [].


/* url_port_text(+Split, -Port) <-
      extracts Port from URL in splitted form Split as text. */

url_port_text(Split, [58|Port]) :-
   xml_pillow_www_support:url_port(Split, Port_Number),
   number_codes(Port_Number, Port).


/* url_host_text(+Split, -Text) <-
      extracts Host from URL in splitted form Split as text. */

url_host_text(Split, Text) :-
   xml_pillow_www_support:url_host(Split, Host),
   atom_codes(Host, Text).


/* url_protocol_text(+Split, -Text) <-
      extracts Protocol from URL in splitted form Split as text. */

url_protocol_text(Split, Text) :-
   xml_pillow_www_support:url_protocol(Split, Protocol),
   atom_codes(Protocol, Raw_Text),
   append(Raw_Text, "://", Text).


/* url_split(+URL, -Split) <-
      splits URL into a list of its atomic components protocol,
      host, port, path, file name, query string, and anchor. */

url_split(URL, [http, Host, Port, Path, File_Name, Query, Anchor]) :-
   url_info(URL, URL_Term),
   !,
   arg(1, URL_Term, Host),
   arg(2, URL_Term, Port),
   arg(3, URL_Term, File),
   xml_pillow_w_s_file_split(File, [Path, File_Name, Query, Anchor]).
url_split(URL, Split) :-
   xml_pillow_w_s_resource_protocol(Protocol, Length, Short_Protocol),
   ascii_text:to_lower_case(URL, Lower_URL),
   ascii_text:start(Lower_URL, Length, Protocol, Rest),
   append("http://", Rest, Test_URL),
   url_info(Test_URL, _),
   xml_pillow_www_support:url_split(Test_URL, Raw_Split),
   xml_pillow_www_support:url_protocol_set(Raw_Split, Short_Protocol,
      Split).


/* url_compose(+Split, -URL) <-
      composes URL basing on the splitted form Split. */

url_compose(Split, URL) :-
   is_list(Split),
   length(Split, 7),
   xml_pillow_www_support:url_anchor_text(Split, Anchor_Text),
   xml_pillow_www_support:url_querystring_text(Split, Query_Text),
   xml_pillow_www_support:url_file_name(Split, File_Text),
   xml_pillow_www_support:url_select_path(Split, Path_Text),
   xml_pillow_www_support:url_port_text(Split, Port_Text),
   xml_pillow_www_support:url_host_text(Split, Host_Text),
   xml_pillow_www_support:url_protocol_text(Split, Protocol),
   flatten([Protocol, Host_Text, Port_Text, Path_Text, File_Text,
      Query_Text, Anchor_Text], URL).


/*** implementation ***********************************************/


xml_pillow_w_s_http_is_redirect_code(301).
xml_pillow_w_s_http_is_redirect_code(302).

xml_pillow_w_s_http_is_hypertext(xml).
xml_pillow_w_s_http_is_hypertext(html).

xml_pillow_w_s_encode_loop([], []).

xml_pillow_w_s_encode_loop([First|Rest], [First_Trans|Rest_Text]) :-
   xml_pillow_w_s_url_char_encode(First, First_Trans),
   xml_pillow_w_s_encode_loop(Rest, Rest_Text).

xml_pillow_w_s_encode_loop([First|Rest], [First|Rest_Text]) :-
   \+ xml_pillow_w_s_url_char_encode(First, _),
   xml_pillow_w_s_encode_loop(Rest, Rest_Text).

xml_pillow_w_s_url_char_encode(32, [43]).

xml_pillow_w_s_url_char_encode(Code, [37, First, Second]) :-
   var(Code),
   !,
   xml_pillow_w_s_decimal_to_hex_ascii(Code, [First, Second]).

xml_pillow_w_s_url_char_encode(Code, [37, First, Second]) :-
   xml_pillow_w_s_critical_url_char(Code),
   xml_pillow_w_s_decimal_to_hex_ascii(Code, [First, Second]).

xml_pillow_w_s_decimal_to_hex_ascii(Decimal, [First, Second]) :-
   xml_pillow_w_s_compute_decimal(First, Second, Decimal).

xml_pillow_w_s_critical_url_char(34).
xml_pillow_w_s_critical_url_char(35).
xml_pillow_w_s_critical_url_char(37).
xml_pillow_w_s_critical_url_char(38).
xml_pillow_w_s_critical_url_char(39).
xml_pillow_w_s_critical_url_char(43).
xml_pillow_w_s_critical_url_char(47).
xml_pillow_w_s_critical_url_char(58).
xml_pillow_w_s_critical_url_char(59).
xml_pillow_w_s_critical_url_char(60).
xml_pillow_w_s_critical_url_char(61).
xml_pillow_w_s_critical_url_char(62).
xml_pillow_w_s_critical_url_char(63).
xml_pillow_w_s_critical_url_char(64).
xml_pillow_w_s_critical_url_char(91).
xml_pillow_w_s_critical_url_char(92).
xml_pillow_w_s_critical_url_char(93).
xml_pillow_w_s_critical_url_char(94).
xml_pillow_w_s_critical_url_char(121).
xml_pillow_w_s_critical_url_char(123).
xml_pillow_w_s_critical_url_char(124).
xml_pillow_w_s_critical_url_char(126).
xml_pillow_w_s_critical_url_char(Code) :-
   Code > 126.
xml_pillow_w_s_critical_url_char(Code) :-
   Code < 32.

xml_pillow_w_s_compute_decimal(First, Second, Decimal) :-
   xml_pillow_w_s_dec_ascii(First_Number, First),
   xml_pillow_w_s_dec_ascii(Second_Number, Second),
   Decimal is First_Number * 16 + Second_Number.

xml_pillow_w_s_dec_hex(0, '0').
xml_pillow_w_s_dec_hex(1, '1').
xml_pillow_w_s_dec_hex(2, '2').
xml_pillow_w_s_dec_hex(3, '3').
xml_pillow_w_s_dec_hex(4, '4').
xml_pillow_w_s_dec_hex(5, '5').
xml_pillow_w_s_dec_hex(6, '6').
xml_pillow_w_s_dec_hex(7, '7').
xml_pillow_w_s_dec_hex(8, '8').
xml_pillow_w_s_dec_hex(9, '9').
xml_pillow_w_s_dec_hex(10, 'A').
xml_pillow_w_s_dec_hex(11, 'B').
xml_pillow_w_s_dec_hex(12, 'C').
xml_pillow_w_s_dec_hex(13, 'D').
xml_pillow_w_s_dec_hex(14, 'E').
xml_pillow_w_s_dec_hex(15, 'F').

xml_pillow_w_s_dec_ascii(0, 48).
xml_pillow_w_s_dec_ascii(1, 49).
xml_pillow_w_s_dec_ascii(2, 50).
xml_pillow_w_s_dec_ascii(3, 51).
xml_pillow_w_s_dec_ascii(4, 52).
xml_pillow_w_s_dec_ascii(5, 53).
xml_pillow_w_s_dec_ascii(6, 54).
xml_pillow_w_s_dec_ascii(7, 55).
xml_pillow_w_s_dec_ascii(8, 56).
xml_pillow_w_s_dec_ascii(9, 57).
xml_pillow_w_s_dec_ascii(10, 65).
xml_pillow_w_s_dec_ascii(11, 66).
xml_pillow_w_s_dec_ascii(12, 67).
xml_pillow_w_s_dec_ascii(13, 68).
xml_pillow_w_s_dec_ascii(14, 69).
xml_pillow_w_s_dec_ascii(15, 70).

xml_pillow_w_s_dec_to_ascii(Decimal, Hex_Ascii) :-
   number(decimal),
   var(Hex_Ascii),
   xml_pillow_w_s_compute_hex_ascii(Decimal, Hex_Ascii).

xml_pillow_w_s_compute_hex_ascii(Decimal, [First, Second]) :-
   First_Number is Decimal // 16,
   Second_Number is Decimal mod 16,
   xml_pillow_w_s_dec_ascii(First_Number, First),
   xml_pillow_w_s_dec_ascii(Second_Number, Second).

xml_pillow_w_s_decode_loop([], []).

xml_pillow_w_s_decode_loop([37, Second, Third|Rest],
      [First_Trans|Rest_Text]) :-
   xml_pillow_w_s_url_char_encode(First_Trans, [37, Second, Third]),
   !,
   xml_pillow_w_s_decode_loop(Rest, Rest_Text).

xml_pillow_w_s_decode_loop([43|Rest], [32, Rest_Text]) :-
   !,
   xml_pillow_w_s_decode_loop(Rest, Rest_Text).

xml_pillow_w_s_decode_loop([First|Rest], [First|Rest_Text]) :-
   xml_pillow_w_s_decode_loop(Rest, Rest_Text).

xml_pillow_w_s_file_split([], [[], [], [], []]) :-
   !.

xml_pillow_w_s_file_split([47|Rest], [[47|Rest_Dir], File_Name, Query,
       Anchor]) :-
   !,
   xml_pillow_w_s_file_split(Rest,
      [Rest_Dir, File_Name, Query, Anchor]).

xml_pillow_w_s_file_split(File, [Path, File_Name, Query, Anchor]) :-
   xml_pillow_w_s_path_info(File, Path, File_Name, Query, Anchor).

xml_pillow_w_s_path_info([], [], [], [], []).

xml_pillow_w_s_path_info(File, Path, File_Name, Query, Anchor) :-
   ascii_text:last_index_of(File, 47, Index),
   ascii_text:start(File, Index, Path, Name_Rest),
   xml_pillow_w_s_url_file_name(Name_Rest, File_Name, End_Signal,
      Param_Rest),
   xml_pillow_w_s_url_param(Param_Rest, End_Signal, Query, Anchor).

xml_pillow_w_s_url_file_name([], [], 0, []).

xml_pillow_w_s_url_file_name([First|Rest], [], First, Rest) :-
   xml_pillow_w_s_url_file_end(First),
   !.

xml_pillow_w_s_url_file_name([First|Rest], [First|Rest_Name], Signal,
      Rest_Text) :-
   xml_pillow_w_s_url_file_name(Rest, Rest_Name, Signal, Rest_Text).

xml_pillow_w_s_url_param([], _, [], []).

xml_pillow_w_s_url_param(_, 0, [], []).

xml_pillow_w_s_url_param(Text, Signal, [], Text) :-
   xml_pillow_w_s_url_anchor_char(Signal).

xml_pillow_w_s_url_param(Text, Signal, Query, Anchor) :-
   xml_pillow_w_s_url_querystring_char(Signal),
   ascii_text:split(Text, 35, [Query|Anchor]),
   length(Anchor, Length),
   Length =< 1.

xml_pillow_w_s_url_file_end(63).        % ?
xml_pillow_w_s_url_file_end(35).        % #

xml_pillow_w_s_url_anchor_char(35).           % #
xml_pillow_w_s_url_querystring_char(63).      % ?

xml_pillow_w_s_resource_protocol("http://", 7, http).
xml_pillow_w_s_resource_protocol("https://", 8, https).
xml_pillow_w_s_resource_protocol("ftp://", 6, ftp).


/******************************************************************/


