

/******************************************************************/
/***                                                            ***/
/***           XUL:  Updates                                    ***/
/***                                                            ***/
/******************************************************************/


xul_example_get(File, Xul) :-
   dislog_variable_get(xul_directory, File, Path),
   dread(xml, Path, Xs),
   last(Xs, Xul).


/*** interface ****************************************************/


/* xul_update_file(Directory, File_Assigns, File_1, File_2) <-
      */

xul_update_file(Directory, File_Assigns, File_1, File_2) :-
   concat(Directory, File_Assigns, Path),
   concat(Directory, File_1, Path_1),
   concat(Directory, File_2, Path_2),
   dread(xml, Path, [Item]),
   Assigns := Item/content::'*',
   dread(xml, Path_1, [Item_1]),
   xul_update(Assigns, Item_1, Item_2),
   dwrite(xml, Path_2, Item_2).


/* xul_update(Assigns, Item_1, Item_2) <-
      */

test(xul:xul_update, assigns(1)) :-
   xul_example_get('examples/test/xul_for_update.xul', Item_1),
   Assigns = [
      assign:[value:'88',
         db_table:'User_2', db_attribute:'Gruppen_ID']:[] ],
   xul_update(Assigns, Item_1, Item_2),
   dwrite(xml, Item_2).
test(xul:xul_update, assigns(2)) :-
   xul_example_get('examples/test/m.xul', Item_1),
   Assigns = [
      assign:[value:'date(2010, 7, 5)', db_table:'R', db_attribute:'A']:[],
      assign:[value:'1', db_table:'R', db_attribute:'E']:[],
      assign:[value:'2', db_table:'S', db_attribute:'B']:[] ],
   xul_update(Assigns, Item_1, Item_2),
   dwrite(xml, Item_2).

xul_update(Assigns, Item_1, Item_2) :-
   xul_update(Assigns, [], Item_1, Item_2).

xul_update(Assigns, Loc, Item_1, Item_2) :-
   T := Item_1/tag::'*',
   member(T, [menuitem, checkbox]),
   !,
   xul_update(T, Assigns, Loc, Item_1, Item_2).
xul_update(Assigns, Loc, Item_1, Item_2) :-
   fn_item_parse(Item_1, T:As_1:Es_1),
   xul_location_update(Item_1, Loc, Loc_2),
   Pairs = [
      menulist - menuitem,
      menupopup - menuitem,
      listbox - listitem,
      richlistbox - richlistitem,
      radiogroup - radio ],
   ( member(T-Tag, Pairs) ->
%    As_2 = As_1,
     xul_update_attribute_list(
        Assigns, _, Loc_2, As_1, As_2),
     maplist( xul_update(Tag, Assigns, Loc_2),
        Es_1, Es_2)
   ; xul_update_attribute_list(
        Assigns, Assigns_2, Loc_2, As_1, As_2),
     maplist( xul_update(Assigns_2, Loc_2),
        Es_1, Es_2) ),
   Item_2 = T:As_2:Es_2.

xul_update(Tag, Assigns, Loc, Item_1, Item_2) :-
   Tag := Item_1/tag::'*',
   !,
   xul_location_update(Item_1, Loc, Loc_2),
   xul_update_selected_or_checked(
      Assigns, Loc_2, Item_1, Item_2).
xul_update(Tag, Assigns, Loc, Item_1, Item_2) :-
   fn_item_parse(Item_1, T:As:Es_1),
   xul_location_update(Item_1, Loc, Loc_2),
   maplist( xul_update(Tag, Assigns, Loc_2),
      Es_1, Es_2 ),
   Item_2 = T:As:Es_2.

xul_update_attribute_list(
      Assigns_1, Assigns_2, Loc, As_1, As_2) :-
   member(Assign, Assigns_1),
   xul_update_applies_to_location(Assign, Loc, V),
   !,
   As_2 := As_1*[value:V],
   delete(Assigns_1, Assign, Assigns_2).
xul_update_attribute_list(Assigns, Assigns, _, As, As).

xul_update_selected_or_checked(
      Assigns, Loc, Item_1, Item_2) :-
   member(Assign, Assigns),
   xul_update_applies_to_location(Assign, Loc, V),
   !,
   ( checkbox := Item_1/tag::'*' ->
     ( V = '1' ->
       Item_2 := Item_1*[@checked:true]
     ; Item_2 := Item_1*[@checked:false] )
   ; ( V := Item_1@value ->
       Item_2 := Item_1*[@selected:true]
     ; Item_2 := Item_1*[@selected:false] ) ).
xul_update_selected_or_checked(
      _, _, Item, Item).


/* xul_location_update(Item, Loc_1, Loc_2) <-
      */

xul_location_update(Item, Loc_1, Loc_2) :-
   xul_item_select_attribute_values(
      Item, [db_table, db_attribute], Loc),
   xul_attribute_lists_join(Loc_1, Loc, Loc_2).


/* xul_update_applies_to_location(Assign, Loc, V) <-
      */

xul_update_applies_to_location(Assign, Loc, V) :-
   T := Assign@db_table,
   A := Assign@db_attribute,
   Value := Assign@value,
   xul_update_convert_xul_attribute_value(Value, V),
   subset([db_table:T, db_attribute:A], Loc).


/* xul_update_convert_xul_attribute_value(Value_1, Value_2) <-
      */

xul_update_convert_xul_attribute_value(Value, Date) :-
   ( Term = Value
   ; atomic(Value),
     catch(term_to_atom(Term, Value), _, fail) ),
   date_swi_to_sql(Term, Date),
   !.
xul_update_convert_xul_attribute_value(Value, Value).


/******************************************************************/


