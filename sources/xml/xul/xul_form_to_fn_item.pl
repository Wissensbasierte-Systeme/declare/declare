

/******************************************************************/
/***                                                            ***/
/***           XUL:  Form to FN Item                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xul_form_to_fn_item(Xul, Item) <-
      */

xul_form_to_fn_item(Xul, Item) :-
   xul_item_to_assigns(Xul, Assigns),
   xul_assigns_to_fn_item(Assigns, Item).


/*** implementation ***********************************************/


/* xul_item_to_assigns(Xul, Assigns) <-
      */

test(xul:xul_update, xul_item_to_assigns) :-
   dislog_variable_get(xul_directory, Dir),
   concat(Dir, 'examples/test/m.xul', File),
   dread(xml, File, [Xul]),
   xul_item_to_assigns(Xul, _).

xul_item_to_assigns(Xul, Assigns) :-
   fn_item_transform(xul_item_transform, Xul, Item),
   findall( Assign,
      Assign := Item/descendant_or_self::assign,
      Assigns ),
   dwrite(xml, assigns:Assigns).


/* xul_item_transform(X, Y) <-
      */

xul_item_transform(X, Y) :-
   xul_assigns_enrich(X, Tag:[]:Assigns),
   ( xul_item_select_assign(X, Assign) ->
%    dwrite(xml, Assign),
     ( Tag = menulist ->
       Es = [Assign]
     ; Es = [Assign|Assigns] )
   ; Es = Assigns ),
   Y = Tag:[]:Es.


/* xul_item_select_assign(Item, Assign) <-
      */

xul_item_select_assign(Item, Assign) :-
   xul_item_select_value(Item, V),
   xul_item_is_active(Item),
   xul_item_select_attribute_values(
      Item, [db_table, db_attribute], As),
   Assign = assign:[value:V|As]:[].

xul_item_select_value(Item, V) :-
   checkbox := Item/tag::'*',
   !,
   ( true := Item@checked ->
     V = '1'
   ; V = '0' ).
xul_item_select_value(Item, V) :-
   V := Item@value,
   \+ member(V, ['', undefined]).

xul_item_is_active(Xul) :-
   Tag := Xul/tag::'*',
   ( member(Tag, [listitem, menuitem, richlistitem, radio]) ->
     true := Xul@selected
   ; true ).

%  ; member(Tag, [checkbox]) ->
%    true := Xul@checked
%  ; true ).

xul_item_select_attribute_values(Xul, Attributes, Pairs) :-
   findall( A:V,
      ( member(A, Attributes),
        V := Xul@A ),
      Pairs ).


/* xul_assigns_enrich(Item_1, Item_2) <-
      */

xul_assigns_enrich(Item_1, Item_2) :-
   xul_item_select_attribute_values(
      Item_1, [db_table, db_attribute], As),
   Tag := Item_1/tag::'*',
   findall( Assign_2,
      ( Item := Item_1/child::'*',
        Assign_1 := Item/descendant_or_self::assign,
        fn_item_parse(Assign_1, assign:As_1:[]),
        xul_attribute_lists_join(As, As_1, As_2),
        Assign_2 := assign:As_2:[] ),
      Assigns ),
   Item_2 = Tag:[]:Assigns.

xul_attribute_lists_join(As, As_1, As_2) :-
   findall( A:V,
      ( member(A:V, As),
        \+ member(A:_, As_1) ),
      As_3 ),
   append(As_1, As_3, As_2).


/* xul_assigns_to_fn_item(Assigns, Item) <-
      */

xul_assigns_to_fn_item(Assigns, Item) :-
   findall( B:V,
      ( member(Assign, Assigns),
        V := Assign@value,
        T := Assign@db_table,
        A := Assign@db_attribute,
        concat([T, ':', A], B) ),
      As ),
   xul_association_list_normalize(As, Bs),
   Item = row:Bs:[].


/******************************************************************/


