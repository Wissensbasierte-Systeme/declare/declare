

/******************************************************************/
/***                                                            ***/
/***           Container Item to Database                       ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* mysql_database_to_container_item(Database, Item) <-
      */

test(xul:container, mysql_database_to_container_item) :-
   Connection = mysql,
   Database = test_db_for_container_item,
   mysql_database_to_container_item(
      Connection, Database, Item),
   dwrite(xml, Item).

mysql_database_to_container_item(
      Connection, Database, Item) :-
   mysql_database_schema_to_fk_edges(Database, Edges),
   edges_to_ugraph(Edges, Graph),
   ugraphs:top_sort(Graph, Tables),
   mysql_database_to_container_item(
      Connection, Database, Tables, Item).


/* mysql_database_to_container_item(
         Connection, Database, Tables, Item) <-
      */

mysql_database_to_container_item(
      Connection, Database, Tables, Item) :-
   mysql_database_to_container_items(
      Connection, Database, Tables, Items),
   Item = 'Container':[name:Database]:Items.

mysql_database_to_container_items(
      Connection, Database, Tables, Items) :-
   mysql_database_to_container_items(
      Connection, Database, Tables, Tables, Items).

mysql_database_to_container_items(
      Connection, Database, _, [Table], Items) :-
   !,
   mysql_database_table_to_fn_items(
      Connection, Database:Table, Items).
mysql_database_to_container_items(
      Connection, Database, Ts, [Table|Tables], Items) :-
   mysql_database_to_container_items(
      Connection, Database, Ts, Tables, Is_1),
   mysql_database_table_to_fn_items(
      Connection, Database:Table, Is_2),
   maplist( container_item_assign_subelements(Ts, Table, Is_1),
      Is_2, Items ).

container_item_assign_subelements(Ts, Table, Is, I1, I2) :-
   append(Xs, [Table|_], Ts),
   append(Xs, [Table], Ys),
   fn_item_to_attribute_values(I1, Xs, Vs),
   fn_item_to_attribute_value(I1, id, Id),
   append(Vs, [Id], Ws),
   findall( J,
      ( member(I, Is),
        fn_item_to_attribute_values(I, Ys, Ws),
        fn_item_to_pruned_container(I, Ys, Ws, J) ),
      Js ),
   fn_item_parse(I1, T:As:_),
   I2 = T:As:Js.

fn_item_to_pruned_container(I, Ys, Ws, J) :-
   fn_item_parse(I, T:As:Es),
   pair_lists(:, Ys, Ws, Pairs),
   subtract(As, Pairs, As_2),
   J = T:As_2:Es.


/* fn_item_to_attribute_values(Xml, Attributes, Values) <-
      */

fn_item_to_attribute_values(Xml, Attributes, Values) :-
   maplist( fn_item_to_attribute_value(Xml),
      Attributes, Values ).

fn_item_to_attribute_value(Xml, A, V) :-
   V := Xml@A,
   !.
fn_item_to_attribute_value(_, _, '$null$').


/******************************************************************/


