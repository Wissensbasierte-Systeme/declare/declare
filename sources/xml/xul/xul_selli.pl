

/******************************************************************/
/***                                                            ***/
/***           XUL:  Selli                                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xul_selli_gui <-
      */

xul_selli_gui :-
%  mysql_odbc_connect,
   dislog_variable_get(xul_directory,
      'examples/Selli/selli.xul', File),
   xul_file_open(File),
   !.


/* xul_selli_... <-
      */

xul_selli_form_to_values :-
   Attributes = ['Date', 'Number', 'Total', 'Sid', 'Item', 'Price'],
   xul_form_to_values('1', 'myvbox', Attributes, Values),
   writeq(user, Values), nl(user), ttyflush.

xul_selli_enter(Date, Number, Total, Sid, Item, Price) :-
   term_to_atom(Total_2, Total),
   term_to_atom(Price_2, Price),
   ddk_timestamp(Timestamp),
   Tuple = [Date, Number, Sid, Item, Price, Timestamp],
   Attributes = [
      'Date', 'Number', 'Sid', 'Item', 'Price', 'Timestamp'],
   mysql_insert_tuples(mysql, selli:purchase, Attributes, [Tuple]),
   writeln(user, Tuple),
   Total_3 is Total_2 + Price_2,
   Fn_Term = textbox:[id:'Total', flex:1, value:Total_3]:[],
   fn_term_to_xml_string(Fn_Term, String),
   format(String).

xul_selli_show_purchases :-
   mysql_table_select(mysql, selli:purchase, Tuples),
   mysql_database_table_describe(selli:purchase, Pairs),
   findall( A,
      member([A, _], Pairs),
      Attributes ),
   xpce_display_table(Attributes, Tuples).


/******************************************************************/


