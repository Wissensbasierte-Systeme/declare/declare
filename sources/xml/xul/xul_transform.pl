

/******************************************************************/
/***                                                            ***/
/***           XUL:  Transformations                            ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* cul_to_xul(File_1, File_2) <-
      */

cul_to_xul(File_1, File_2) :-
   dislog_variable_get(xul_prolog, 'xul_from_cul.fng', File),
   fn_transform([file, fng(File)], File_1, File_2).


/* xul_to_cul(File_1, File_2) <-
      */

xul_to_cul(File_1, File_2) :-
   dislog_variable_get(xul_prolog, 'xul_to_cul.fng', File),
   fn_transform([file, fng(File)], File_1, File_2).


/******************************************************************/


