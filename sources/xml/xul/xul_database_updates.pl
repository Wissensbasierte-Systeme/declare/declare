

/******************************************************************/
/***                                                            ***/
/***           XUL:  Database Updates                           ***/
/***                                                            ***/
/******************************************************************/


xul_update_test_form(N, Xul) :-
   Pairs = [
      1 - 'examples/Selli/selli_form.xul',
      2 - 'examples/test/t.xul',
      3 - 'examples/test/input.xml',
      4 - 'examples/test/arbeitsgruppe.xml' ],
   member(N-X, Pairs),
   dislog_variable_get(xul_directory, X, File),
   dread(xml, File, Items),
   last(Items, Xul).


/*** interface ****************************************************/


/* xul_item_to_insert_statements(
         Connection, Database, Xul, Statements) <-
      */

test(xul:db_update, xul_item_to_insert_statements) :-
%  N = 3, Database = selli,
   N = 4, Database = test_vtrp,
   xul_update_test_form(N, Xul),
   xul_item_to_insert_statements(
      mysql, Database, Xul, Statements),
   writelnq_list(Statements).

xul_item_to_insert_statements(
      Connection, Database, Xul, Statements) :-
   xul_item_to_inserts(Xul, Inserts),
   maplist(
      fn_item_to_insert_statement(Connection, Database),
      Inserts, Statements ).


/* xul_item_to_delete_statements(
         Connection, Database, Xul, Statements) <-
      */

test(xul:db_update, xul_item_to_delete_statements) :-
   N = 2, Database = test_vtrp,
   xul_update_test_form(N, Xul),
   xul_item_to_delete_statements(
      mysql, Database, Xul, Statements),
   writelnq_list(Statements).

xul_item_to_delete_statements(
      Connection, Database, Xul, Statements) :-
   mysql_database_schema_to_xml(Database, DB_Schema),
   xul_item_to_modifications(DB_Schema, Xul, Items),
   maplist(
      fn_item_to_delete_statement(Connection, Database),
      Items, Statements ).


/* xul_item_to_update_statements(
         Connection, Database, Xul, Statements) <-
      */

test(xul:db_update, xul_item_to_update_statements) :-
%  N = 2, Database = test_vtrp,
   N = 4, Database = vtrp,
   xul_update_test_form(N, Xul),
   xul_item_to_update_statements(
      mysql, Database, Xul, Statements),
   writelnq_list(Statements).

xul_item_to_update_statements(
      Connection, Database, Xul, Statements) :-
   mysql_database_schema_to_xml(Database, DB_Schema),
   xul_item_to_modifications(DB_Schema, Xul, Items),
   maplist(
      fn_item_to_update_statement(Connection, DB_Schema, Database),
      Items, Statements ).


/* xul_item_to_insert_or_update_statements(
         Connection, Database, Xul, Statements) <-
      */

test(xul:db_update, xul_item_to_insert_or_update_statements) :-
%  Database = test_vtrp, N = 2,
   Database = vtrp, N = 4,
   xul_update_test_form(N, Xul),
   xul_item_to_insert_or_update_statements(
      mysql, Database, Xul, Statements),
   writelnq_list(Statements).

xul_item_to_insert_or_update_statements(
      Connection, Database, Xul, Statements) :-
   mysql_database_schema_to_xml(Database, DB_Schema),
%  xul_item_to_modifications(DB_Schema, Xul, Items),
   xul_item_to_inserts(Xul, Items),
   findall( Statement,
      ( member(Item, Items),
        fn_item_to_insert_or_update_statement(
           Connection, DB_Schema, Database, Item, Statement) ),
      Statements ).


/*** implementation ***********************************************/


/* xul_item_to_modifications(DB_Schema, Xul, Modifications) <-
      */

xul_item_to_modifications(DB_Schema, Xul, Modifications) :-
   xul_item_to_inserts(Xul, Inserts),
   sublist( fn_item_includes_primary_key(DB_Schema),
      Inserts, Modifications ).


/* xul_item_to_inserts(Xul, Inserts) <-
      */

test(xul:db_update, xul_item_to_inserts(N)) :-
   nth(N, ['Selli/selli_form.xul', 'test/m.xul'], F),
   concat('examples/', F, G),
   dislog_variable_get(xul_directory, G, File),
   dread(xml, File, X),
   last(X, Xul),
   xul_item_to_inserts(Xul, Row, Items),
   dwrite(xml, Row),
   dwrite(xml, inserts:Items).

xul_item_to_inserts(Xul, Items) :-
   xul_item_to_inserts(Xul, _, Items).

xul_item_to_inserts(Xul, Row, Items) :-
   xul_form_to_fn_item(Xul, Row),
   xml_row_to_inserts(Row, Items),
   !.


/* xul_form_to_fn_item_2(Xul, Item) <-
      */

xul_form_to_fn_item_2(Xul, Item) :-
   D = descendant_or_self,
   findall( B:V,
      ( X := Xul/D::'*'::[@db_table=Table],
        Y := X/D::'*'::[@db_attribute=Attribute]/D::Tag,
        ( member(Tag, [listitem, menuitem, richlistitem, radio]) ->
          true := Y@selected
        ; member(Tag, [checkbox]) ->
          true := Y@checked
        ; true ),
        xul_element_to_table_and_attribute(
           Y, Table->T, Attribute->A),
        V := Y@value,
        concat([T, ':', A], B) ),
      As ),
   xul_association_list_normalize(As, Bs),
   Item = row:Bs:[],
   !.


/* xul_association_list_normalize(As, Bs) <-
      */

xul_association_list_normalize(As, Bs) :-
   ddbase_aggregate( [A, concat_with_separator_blank(V)],
      member(A:V, As),
      Cs ),
   pair_lists(_, Xs, Ys, Cs),
   pair_lists(:, Xs, Ys, Bs).

concat_with_separator_blank(Names, Name) :-
   concat_with_separator(Names, ' ', Name).


/* xml_row_to_inserts(Row, Inserts) <-
      */

xml_row_to_inserts(Row, Inserts) :-
   findall( T:As:[],
      xml_row_to_insert(Row, T, As),
      Inserts ).

xml_row_to_insert(Row, Table, Pairs) :-
   setof( A:V,
      TA^( V := Row@TA,
         name_split_at_position([":"], TA, [Table, A]) ),
      Pairs ).


/* xul_element_to_table_and_attribute(Xul, T1->T2, A1->A2) <-
      */

xul_element_to_table_and_attribute(Xul, T1->T2, A1->A2) :-
   xul_element_to_table(Xul, T1->T2),
   xul_element_to_attribute(Xul, A1->A2).

xul_element_to_table(Xul, _->Table) :-
   ( Ts := Xul@db_table
   ; Ts := Xul@db_tables ),
   !,
   name_split_at_position([","], Ts, Tables),
   member(Table, Tables).
xul_element_to_table(_, Table->Table).

xul_element_to_attribute(Xul, _->Attribute) :-
   ( As := Xul@db_attribute
   ; As := Xul@db_attributes ),
   !,
   name_split_at_position([","], As, Attributes),
   member(Attribute, Attributes).
xul_element_to_attribute(_, Attribute->Attribute).


/******************************************************************/


