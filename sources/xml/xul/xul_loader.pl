

/******************************************************************/
/***                                                            ***/
/***           XUL:  Loader                                     ***/
/***                                                            ***/
/******************************************************************/


:- dislog_variable_get(source_path, 'xml/xul/', Dir_1),
   dislog_variable_set(xul_prolog, Dir_1),
   dislog_variable_get(home, '/projects/XUL/', Dir_2),
   dislog_variable_set(xul_directory, Dir_2),
   dislog_variable_get(home, '/projects/XUL/xul/', Dir_3),
   dislog_variable_set(xul_server, Dir_3).


:- ( getenv('USER', seipel) ->
     dislog_variable_get(xul_server, Dir),
     Files = [ 'xul.pl', 'xul_http.pl', 'demo.pl' ],
     maplist( concat(Dir),
        Files, Paths ),
     consult(Paths),
     start_server
   ; true ).


/******************************************************************/


