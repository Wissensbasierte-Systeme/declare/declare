

/******************************************************************/
/***                                                            ***/
/***           XUL:  Database Statements                        ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xul_file_to_database_table(Database, File) <-
      */

xul_file_to_database_table(Database, File) :-
   mysql_use_database(Database),
   xul_file_to_create_statement(File, Statement),
   odbc_query(mysql, Statement, Row, []),
   writeln(user, Row).


/* xul_file_to_create_statement(File, Statement) <-
      */

test(xul:db_schema, xul_to_create_statement) :-
   dislog_variable_get(xul_directory,
      'examples/test/b.xml', File),
   xul_file_to_create_statement(File, Statement),
   writeln(user, Statement).

xul_file_to_create_statement(File, Statement) :-
   dread(xml, File, X),
   last(X, Xul),
   xul_to_create_statement(Xul, Statement).


/* xul_to_create_statement(Xul, Statement) <-
      */

xul_to_create_statement(Xul, Statement) :-
   xul_to_table_name(Xul, Table),
   xul_to_attributes(Xul, Attributes),
   xul_tables_and_attributes_to_create_statement(
      Table, Attributes, Statement),
   xul_to_attribute_ids(Xul, Ids),
   list_to_multiset(Ids, Multiset),
   writeln(user, Multiset),
   findall( Id:N,
      ( member(Id:N, Multiset),
        N > 1 ),
      Pairs ),
   ( Pairs \= [] ->
     write_list(user, ['duplicate ids: \n'|Pairs]), nl(user)
   ; true ).


/* xul_tables_and_attributes_to_create_statement(
         Table, Attributes, Statement) <-
      */

xul_tables_and_attributes_to_create_statement(
      Table, Attributes, Statement) :-
   concat([
      'CREATE TABLE ', Table, ' (\n   '], A),
   names_append_with_separator(Attributes, ',\n   ', B),
   concat(
      '   Auto_Key  int not null auto_increment,\n',
      '   primary key (Auto_Key)\n', Key),
   concat([A, B, ',\n', Key, ')'], Statement).


/* xul_to_insert_statement(Xul, Ids, Statement) <-
      */

test(xul:db_update, xul_to_insert_statement) :-
   dislog_variable_get(xul_directory, 'examples/test/', Dir),
   concat(Dir, 'b.xml', File_1),
   concat(Dir, 'a.xml', File_2),
   dread(xml, File_1, [Xul_1]),
   dread(xml, File_2, [Xul_2]),
   xul_to_attribute_ids(Xul_1, Ids),
   xul_to_insert_statement(Xul_2, Ids, Statement),
   writeln(user, Statement).

xul_to_insert_statement(Xul, Ids, Statement) :-
   findall( A:V,
      ( member(A, Ids),
        xul_id_to_value(Xul, A, V) ),
      Pairs ),
   pair_lists(_, As, Vs, Pairs),
   xul_to_table_name(Xul, Table),
   concat(['INSERT INTO ', Table, '\n   ('], A),
   names_append_with_separator(As, ', ', B),
   names_append_with_separator(Vs, ', ', C),
   concat([A, B, ')\nVALUES\n   (', C, ')'], Statement).

     
/*** implementation ***********************************************/


xul_to_table_name(Xul, Table) :-
   Table := Xul/descendant::tabpanel@id.
xul_to_table_name(Xul, Table) :-
   Table := Xul/descendant::'*'@db_table.

xul_to_attributes(Xul, Attributes) :-
   findall( Attribute,
      xul_to_attribute(Xul, Attribute),
      Attributes ).

xul_to_attribute(Xul, Attribute) :-
   Id := Xul/descendant::attribute@id,
   concat(['   ', Id, '  ', 'varchar(255)'], Attribute).
xul_to_attribute(Xul, Attribute) :-
   Id := Xul/descendant::'*'::[@db_attribute=true]@id,
   concat(['   ', Id, '  ', 'varchar(255)'], Attribute).

xul_to_attribute_ids(Xul, Ids) :-
   findall( Id,
      Id := Xul/descendant::attribute@id,
      Ids ).

xul_id_to_value(Xul, Id, Value) :-
   Value := Xul/descendant::'*'::[@id = Id]@value.


/******************************************************************/


