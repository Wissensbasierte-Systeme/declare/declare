

/******************************************************************/
/***                                                            ***/
/***           XUL:  Forms                                      ***/
/***                                                            ***/
/******************************************************************/



/*** interface ****************************************************/


/* xul_file_open(File) <-
      */

xul_file_open(File) :-
   dislog_variable_get(xul_server, '/demo/', Path),
   concat(['cp ', File, ' ', Path,
      'chrome/content/demo3.xul'], Copy),
   us(Copy),
   concat(['xulrunner ', Path, 'application.ini &'], Run),
   us(Run).


/* xul_field_update(User, Field=Value) <-
      */

xul_field_update(User, Field=Value) :-
   create_message(get, User, Field, Xul),
   Xul_2 := Xul*[@value:Value],
   create_message(send, User, Field, Xul_2),
   !.


/* xul_form_update(User, Form, Substitution) <-
      */

xul_form_update(User, Form, Substitution) :-
   create_message(get, User, Form, Xul_1),
   xul_item_update(Substitution, Xul_1, Xul_2),
   create_message(send, User, Form, Xul_2).


/* xul_item_update(Substitution, Item_1, Item_2) <-
      */

xul_item_update(Substitution, Item_1, Item_2) :-
   P = xul_transform_predicate(Substitution),
   fn_transform([item, predicate(P)], Item_1, Item_2).

xul_transform_predicate(Substitution, Item_1, Item_2) :-
   member(A=V, Substitution),
   A := Item_1@id,
   !,
   Item_2 := Item_1*[@value:V].
xul_transform_predicate(_, Item, Item).


/* xul_form_to_values(User, Form, Attributes, Values) <-
      */

xul_form_to_values(User, Form, Attributes, Values) :-
   create_message(get, User, Form, Xul),
   maplist( xul_item_to_value(Xul),
      Attributes, Values ).

xul_item_to_value(Xul, A, V) :-
   V := Xul/descendant::'*'::[@id=A]@value,
   !.
xul_item_to_value(_, _, '').


/*** tests ********************************************************/


test(xul:forms, xul_form_change) :-
   Xul = grid:[id:myvbox]:[
      columns:[
         column:[id:text]:[], column:[id:fields]:[] ],
      rows:[
         row:[ description:[value:'Attribute: ']:[],
            textbox:[id:'A', flex:1, value:16]:[] ] ] ],
   create_message(send, '1', 'myvbox', Xul).


/******************************************************************/


