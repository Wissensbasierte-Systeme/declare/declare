

/******************************************************************/
/***                                                            ***/
/***           Container Item to Database                       ***/
/***                                                            ***/
/******************************************************************/


container_item(Xml) :-
   dislog_variable_get(xul_directory,
      'examples/Container/Container.xml', File),
   dread(xml, File, [Xml]).


/*** interface ****************************************************/


/* container_item_to_mysql_database(Connection, Xml, Database) <-
      */

test(xul:container, container_item_to_mysql_database) :-
   container_item(Xml),
   Connection = mysql,
   Database = test_db_for_container_item,
   container_item_to_mysql_database(Connection, Xml, Database).

container_item_to_mysql_database(Connection, Xml, Database) :-
   mysql_create_database(Connection, Database),
   container_item_create_database_schema(
      Connection, Xml, Database),
   container_item_mysql_insert(Connection, Xml, Database).


/* container_item_create_database_schema(
         Connection, Xml, Database) <-
      */

container_item_create_database_schema(
      Connection, Xml, Database) :-
   container_item_to_create_statements(Xml, Tables, Statements),
   mysql_create_tables(Connection, Database:Tables, Statements).


/* container_item_to_create_statements(
         Xml, Tables, Statements) <-
      */

test(xul:container, container_item_to_create_statements) :-
   container_item(Xml),
   container_item_to_create_statements(Xml, _, Statements),
   writeln_list(user, Statements).

container_item_to_create_statements(Xml, Tables, Statements) :-
   container_item_to_tables(Xml, Tables),
   container_item_and_tables_to_create_statements(
      Xml, Tables, Statements).

container_item_and_tables_to_create_statements(
      Xml, Tables, Statements) :-
   maplist( container_item_to_create_statement(Xml, Tables),
      Tables, Statements ).


/* container_item_to_create_statement(
         Xml, Tables, Table, Statement) <-
      */

container_item_to_create_statement(
      Xml, Tables, Table, Statement) :-
   container_item_to_attributes_key_and_fks(
      Xml, Tables, Table, Attributes, Key, Fks),
   Type = 'varchar(100)',
   table_attributes_type_key_and_fks_to_create_statement(
      Table, Attributes, Type, Key, Fks, Statement).

container_item_to_attributes_key_and_fks(
      Xml, Tables, Table, Attributes, Key, Fks) :-
   fn_item_to_attributes_of_tag(Xml, Table, As),
   append(Xs, [Table|_], Tables),
   append(Xs, As, Attributes),
   append(Xs, [id], Key),
   ( append(Ys, [T], Xs) ->
     append(Ys, [id], Ref),
     Fks = [Xs->T:Ref]
   ; Fks = [] ).


/* table_attributes_type_key_and_fks_to_create_statement(
         Table, Attributes, Type, Key, Fks, Statement) <-
      */

table_attributes_type_key_and_fks_to_create_statement(
      Table, Attributes, Type, Key, Fks, Statement) :-
   concat(['CREATE TABLE ', Table, ' (\n   '], A),
   extend_attributes_by_type(Type, Attributes, As),
   names_append_with_separator(As, ',\n   ', B),
   names_append_with_separator(Key, ', ', C),
   concat(['   primary key (', C, ')'], Pk),
   maplist( fk_term_to_string,
      Fks, Strings ),
   names_append_with_separator([Pk|Strings], ',\n   ', D),
   concat([A, B, ',\n', D, '\n', ') ENGINE=InnoDB'], Statement).

extend_attributes_by_type(Type, Attributes_1, Attributes_2) :-
   foreach(A1, Attributes_1), foreach(A2, Attributes_2) do
      concat([A1, '  ', Type], A2).


/* container_item_mysql_insert(Connection, Xml, Database) <-
      */

container_item_mysql_insert(Connection, Xml, Database) :-
   container_item_to_tables(Xml, Tables),
   ( foreach(Table, Tables) do
        container_item_mysql_insert(
           Connection, Xml, Tables, Database:Table) ).

container_item_mysql_insert(Connection, Xml, Database:Table) :-
   container_item_to_tables(Xml, Tables),
   container_item_mysql_insert(
      Connection, Xml, Tables, Database:Table).

container_item_mysql_insert(
      Connection, Xml, Tables, Database:Table) :-
   container_item_to_relations(Xml, Relations),
   nth(N, Tables, Table),
   nth(N, Relations, Tuples),
   container_item_to_attributes_key_and_fks(
      Xml, Tables, Table, Attributes, _, _),
   mysql_insert_tuples(
      Connection, Database:Table, Attributes, Tuples).


/* container_item_to_relations(Xml, Relations) <-
      */

test(xul:container, container_item_to_relations) :-
   container_item(Xml),
   container_item_to_relations(Xml, Relations),
   writeln_list(Relations).

container_item_to_relations(Xml, Relations) :-
   container_item_to_tables(Xml, Tables),
   maplist( container_item_to_relation(Xml),
      Tables, Relations ).


/* container_item_to_relation(Xml, Table, Tuples) <-
      */

container_item_to_relation(Xml, Table, Tuples) :-
   fn_item_to_attributes_of_tag(Xml, Table, Attributes),
   findall( Tuple,
      container_item_to_tuple(Table, Attributes, Xml, [], Tuple),
      Tuples ).

container_item_to_tuple(Table, Attributes, Xml, Values, Tuple) :-
   Yml := Xml/child::Table,
   reverse(Values, Tuple_1),
   fn_item_to_attribute_values(Yml, Attributes, Tuple_2),
   append(Tuple_1, Tuple_2, Tuple).
container_item_to_tuple(Table, Attributes, Xml, Values, Tuple) :-
   Yml := Xml/child::T,
   T \= Table,
   Value := Yml@id,
   container_item_to_tuple(
      Table, Attributes, Yml, [Value|Values], Tuple).
   

/* fn_item_to_attributes_of_tag(Xml, Tag, Attributes) <-
      */

fn_item_to_attributes_of_tag(Xml, Tag, Attributes) :-
   findall( A,
      _ := Xml/descendant::Tag@A,
      As_1 ),
   sort(As_1, As_2),
   ( member(id, As_2),
     delete(As_2, id, As_3),
     Attributes = [id|As_3]
   ; Attributes = As_2 ).


/* container_item_to_tables(Xml, Tables) <-
      */

container_item_to_tables(Xml, Tables) :-
   findall( T,
      T := Xml/descendant::'*'/tag::'*',
      Ts ),
   findall( T,
      ( append(Xs, [T|_], Ts),
        \+ member(T, Xs) ),
      Tables ).


/******************************************************************/


