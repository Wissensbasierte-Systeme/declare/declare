
subClassOf(C1, C2) :- isa(C1, C2).
subClassOf(C1, C2) :- isa(C1, C), subClassOf(C, C2).

circularity(C1, C2) :- isa(C1, C2), subClassOf(C2, C1).
partition_error(C, C1, C2) :-
   disjointWith(C1, C2), subClassOf(C, C1), subClassOf(C, C2).
incompleteness(C, C1, C2, C3) :-
   isa(C1, C), isa(C2, C), isa(C3, C), disjointWith(C1, C2),
   \+ disjointWith(C2, C3).
redundant_isa(C1->C2->C3) :-
   isa(C1, C3), subClassOf(C3, C2), isa(C1, C2),
   \+ subClassOf(C2, C3).

class(C) :- isa(C1, C2), member(C, [C1, C2]).

user_rdf(Class_1, Type, Class_2) :-
   user:rdf(Class_1, Type, Class_2).

user_rdf_has(Class_1, Type, Class_2) :-
   user:rdf_has(Class_1, Type, Class_2).

rdfs_subClassOf(C1, C2) :-
   user:rdf(Class_1, rdfs:subClassOf, Class_2),
   \+ owl_restriction(Class_1),
   \+ owl_restriction(Class_2),
   Class_1 \= Class_2,
   rdf_class_strip(Class_1, C1),
   rdf_class_strip(Class_2, C2).

subClassOf(C1, C2) :-
%  rdf_subject(Class_1),
   user:rdf_has(Class_1, rdfs:subClassOf, Class_2),
   \+ owl_restriction(Class_1),
   \+ owl_restriction(Class_2),
   Class_1 \= Class_2,
   rdf_class_strip(Class_1, C1),
   rdf_class_strip(Class_2, C2).

isa(C1, C2) :-
   user:rdf(Class_1, rdfs:subClassOf, Class_2),
   \+ owl_restriction(Class_1),
   \+ owl_restriction(Class_2),
   Class_1 \= Class_2,
   rdf_class_strip(Class_1, C1),
   rdf_class_strip(Class_2, C2).

disjointWith(C1, C2) :-
%  rdf_subject(Class_1),
   user:rdf(Class_1, owl:disjointWith, Class_2),
   \+ owl_restriction(Class_1),
   \+ owl_restriction(Class_2), 
   Class_1 \= Class_2,
   rdf_class_strip(Class_1, C1),
   rdf_class_strip(Class_2, C2).

property(P, S, O) :-
   user_rdf(Subject, Predicate, Object),
%  rdf_subject(Predicate),
   rdf_class_strip(Subject, S),
   rdf_class_strip(Predicate, P),
   rdf_class_strip(Object, O).

instanceOf(R, C) :-
%  rdf_subject(Resource),
   user_rdf_has(Resource, rdf:type, Class),
   rdf_class_strip(Class, C),
   class(C),
   rdf_class_strip(Resource, R).

