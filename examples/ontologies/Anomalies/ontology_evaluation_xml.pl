:- discontiguous owl_to_rule/3.


Owl ---> rules:Rules :-
   findall( Rule,
      owl_to_rule(_, Owl, Rule),
      Rules_2 ),
   ontology_evaluation(Rules_2, Rules),
   writeln_list(user, Rules).


owl_to_rule(class, Owl, [class(C)]) :-
   C := Owl^'owl:Class'@'rdf:ID'.


owl_to_rule(instanceOf, Owl, [I:C]) :-
   I := Owl^C@'rdf:ID',
   \+ ( C = _^_ ),
   \+ name_contains_name(C, ':').


owl_to_rule(propertyOf, Owl, [(I1,I2):P]) :-
   R2 := Owl^C::[@'rdf:ID'=I1]^P@'rdf:resource',
   \+ name_contains_name(C, ':'),
   \+ name_contains_name(P, ':'),
   owl_reference_to_id(R2, I2).


owl_to_rule(isa, Owl, [isa(C1, C2)]) :-
   S := Owl^'owl:Class'::[@'rdf:ID'=C1]
      ^'rdfs:subClassOf',
   ( R2 := S@'rdf:resource'
   ; R2 := S@'rdf:ID'
   ; R2 := S^'owl:Class'@'rdf:ID' ),
   owl_reference_to_id(R2, C2).

owl_reference_to_id(Reference, Id) :-
   concat('#', Id, Reference),
   !.
owl_reference_to_id(Id, Id).


owl_to_rule(intersectionOf, Owl, Rule) :-
   Restriction := Owl^'owl:Class'::[@'rdf:ID'=C1]
      ^'owl:intersectionOf'::[^'owl:Class'@'rdf:about'=R2]
      ^'owl:Restriction',
   owl_reference_to_id(R2, C2),
   owl_restriction_parse(has_value, Restriction, P=V),
   Rule = [X:C1]-[X:C2, (X,V):P].

owl_restriction_parse(has_value, Restriction, P=V) :-
   R := Restriction^'owl:onProperty'@'rdf:resource',
   owl_reference_to_id(R, P),
   ( Type = 'xsd:string'
   ; Type = 'xsd:integer' ),
   V := Restriction^'owl:hasValue'^Type@'rdf:value'.


owl_to_rule(disjointWith, Owl, [disjointWith(C1, C2)]) :-
   R2 := Owl^'owl:Class'::[@'rdf:about'=R1]
      ^'owl:disjointWith'@'rdf:resource',
   owl_reference_to_id(R1, C1),
   owl_reference_to_id(R2, C2).


ontology_evaluation(Rules_1, Rules_2) :-
%  writeq(user, Rules_1),
   ontology_evaluation_program(Rules_a),
   append(Rules_1, Rules_a, Rules_b),
   perfect_models(Rules_b, [Model]),
   list_of_elements_to_relation(Model, Facts),
   append(Rules_1, Facts, Rules_c),
   sort(Rules_c, Rules_2),
   ontology_rules_visualize(Rules_2).

ontology_evaluation_program([
   [X:C2]-[X:C1, subClassOf(C1, C2)],
   [subClassOf(C1, C2)]-[isa(C1, C2)],
   [subClassOf(C1, C2)]-[isa(C1, C), subClassOf(C, C2)],
   [circularity(C1, C2)]-[isa(C1, C2), subClassOf(C2, C1)],
   [partition_error(C, C1, C2)]-
      [disjointWith(C1, C2), subClassOf(C, C1), subClassOf(C, C2)],
   [incompleteness(C, C1, C2, C3)]-
      [isa(C1, C), isa(C2, C), isa(C3, C), disjointWith(C1, C2)]-
      [disjointWith(C2, C3)],
   [redundant_isa(C1->C2->C3)]-
      [isa(C1, C3), subClassOf(C3, C2), isa(C1, C2)]-
      [subClassOf(C2, C3)]]).


ontology_rules_visualize(Rules) :-
   findall( C1-C2,
      member([isa(C1, C2)], Rules),
      Edges_2 ),
   findall( X-C-blue,
      ( member([X:C], Rules),
        \+ X = (_, _) ),
      Edges_3 ),
   findall( C1-C2-orange,
      ( member([disjointWith(C1, C2)], Rules)
      ; member([disjointWith(C2, C1)], Rules) ),
      Edges_4 ), 
   append([Edges_2, Edges_3, Edges_4], Edges),
   edges_to_picture_bfs(Edges).


