:-   discontiguous(/('statement', 4)).


triple_exists_quantifier(_).

statement('personalization', O, P, V) :-
   statement('sun_tutorial', O, P, V).
statement('personalization', O, P, V) :-
   statement('java_ontology', O, P, V).

learning_resource('personalization', D) :-
   ( statement('personalization', D, 'type', 'LO')
   ; statement('personalization', D, 'type', 'Book') ).

concept('personalization', C) :-
   statement('personalization', C, 'type', 'Class').

quiz('personalization', E) :-
   statement('personalization', E, 'type', 'Quiz').

summary('personalization', S) :-
   statement('personalization', S, 'type', 'Summary').

concepts_of_LO('personalization', LO, C) :-
   statement('personalization', LO, 'subject', C).

detail_concepts('personalization', C, C_DETAIL) :-
   statement('personalization', C_DETAIL, 'subClassOf', C),
   concept('personalization', C),
   concept('personalization', C_DETAIL).

detail_learningobject('personalization', LO, LO_DETAIL) :-
   ( ( ( triple_exists_quantifier([C, C_DETAIL]), detail_concepts('personalization', C, C_DETAIL), concepts_of_LO('personalization', LO, C), concepts_of_LO('personalization', LO_DETAIL, C_DETAIL) ), learning_resource('personalization', LO_DETAIL), not(unify('personalization', LO, LO_DETAIL)) )
   ; upperlevel('personalization', LO_DETAIL, LO) ).

general_learningobject('personalization', LO, LO_GENERAL) :-
   ( ( ( triple_exists_quantifier([C, C_DETAIL]), detail_concepts('personalization', C, C_DETAIL), concepts_of_LO('personalization', LO, C_DETAIL), concepts_of_LO('personalization', LO_GENERAL, C) ), learning_resource('personalization', LO_GENERAL), not(unify('personalization', LO, LO_GENERAL)) )
   ; upperlevel('personalization', LO, LO_GENERAL) ).

quiz('personalization', LO, E) :-
   triple_exists_quantifier([C]),
   quiz('personalization', E),
   concepts_of_LO('personalization', LO, C),
   concepts_of_LO('personalization', E, C).

context_summary('personalization', LO, S) :-
   triple_exists_quantifier([C]),
   summary('personalization', S),
   concepts_of_LO('personalization', LO, C),
   concepts_of_LO('personalization', S, C).

toplevelconcept('personalization', C) :-
   concept('personalization', C),
   not( triple_exists_quantifier([CTOP]), concept('personalization', CTOP), detail_concepts('personalization', CTOP, C) ).

statement('personalization', O, P, V) :-
   statement('user', O, P, V).

user('personalization', U) :-
   statement('personalization', U, 'type', 'User').

p_obs('personalization', LO, U, 'Learned') :-
   statement('personalization', U, 'hasVisited', LO),
   user('personalization', U),
   learning_resource('personalization', LO).
p_obs('personalization', C, U, 'Learned') :-
   concepts_of_LO('personalization', LO, C),
   p_obs('personalization', LO, U, 'Learned').

upperlevel('personalization', LO1, LO2) :-
   statement('personalization', LO1, 'isPartOf', LO2),
   learning_resource('personalization', LO1),
   learning_resource('personalization', LO2).

learning_state('personalization', LO, U, 'alreadyVisited') :-
   p_obs('personalization', LO, U, 'Learned'),
   learning_resource('personalization', LO).
learning_state('personalization', LO, U, 'recommended') :-
   triple_exists_quantifier([UpperLevelLO]),
   upperlevel('personalization', LO, UpperLevelLO),
   p_obs('personalization', UpperLevelLO, U, 'Learned'),
   not( learning_state('personalization', LO, U, 'alreadyVisited') ).
learning_state('personalization', LO, U, 'notRecommended') :-
   user('personalization', U),
   triple_exists_quantifier([UpperLevelLO]),
   upperlevel('personalization', LO, UpperLevelLO),
   not( p_obs('personalization', UpperLevelLO, U, 'Learned') ; ( learning_state('personalization', LO, U, 'recommended') ; learning_state('personalization', LO, U, 'alreadyVisited') ) ).

statement('query_answering', O, P, V) :-
   statement('query', O, P, V).

querydetails('query_answering', ID, LO, U, R) :-
   statement('query_answering', ID, 'type', 'Query'),
   statement('query_answering', ID, 'aboutUser', U),
   statement('query_answering', ID, 'currentPage', LO),
   statement('query_answering', ID, 'queryFor', R).

learning_state('query_answering', ID, LO, U, S) :-
   querydetails('query_answering', ID, LO2, U, 'learningState'),
   learning_state('personalization', LO, U, S).

learning_state_for('query_answering', ID, LO, U, S) :-
   querydetails('query_answering', ID, LO, U, 'thislearningState'),
   learning_state('personalization', LO, U, S).

learning_state_specific('query_answering', ID, LO, U, S) :-
   querydetails('query_answering', ID, LO2, U, S),
   learning_state('personalization', LO, U, S).

detail_learningobject('query_answering', ID, LO, U, LO_DETAIL) :-
   querydetails('query_answering', ID, LO, U, 'details'),
   detail_learningobject('personalization', LO, LO_DETAIL).

general_learningobject('query_answering', ID, LO, U, LO_GENERAL) :-
   querydetails('query_answering', ID, LO, U, 'general'),
   general_learningobject('personalization', LO, LO_GENERAL).

context_summary('query_answering', ID, LO, U, S) :-
   querydetails('query_answering', ID, LO, U, 'summary'),
   context_summary('personalization', LO, S).

quiz('query_answering', ID, LO, U, Q) :-
   quiz('personalization', LO, Q),
   querydetails('query_answering', ID, LO, U, 'quiz').

done('query_answering', ID, LO, U) :-
   statement('user', U, 'done', LO),
   triple_exists_quantifier([Z, LO2]),
   querydetails('query_answering', ID, LO2, U, Z).

needAgain('query_answering', ID, LO, U) :-
   statement('user', U, 'needAgain', LO),
   triple_exists_quantifier([Z, LO2]),
   querydetails('query_answering', ID, LO2, U, Z).

statement('result', LO, 'details', LO_DETAIL) :-
   triple_exists_quantifier([ID, U]),
   detail_learningobject('query_answering', ID, LO, U, LO_DETAIL).
statement('result', LO, 'general', LO_GENERAL) :-
   triple_exists_quantifier([ID, U]),
   general_learningobject('query_answering', ID, LO, U, LO_GENERAL).
statement('result', LO, 'quiz', QUIZ) :-
   triple_exists_quantifier([ID, U]),
   quiz('query_answering', ID, LO, U, QUIZ).
statement('result', LO, 'summary', CONTEXT_SUMMARY) :-
   triple_exists_quantifier([ID, U]),
   context_summary('query_answering', ID, LO, U, CONTEXT_SUMMARY).
statement('result', U, S, LO) :-
   triple_exists_quantifier([ID]),
   learning_state('query_answering', ID, LO, U, S).
statement('result', U, S, LO) :-
   triple_exists_quantifier([ID]),
   learning_state_for('query_answering', ID, LO, U, S).
statement('result', U, S, LO) :-
   triple_exists_quantifier([ID]),
   learning_state_specific('query_answering', ID, LO, U, S).
statement('result', U, 'done', LO) :-
   triple_exists_quantifier([ID]),
   done('query_answering', ID, LO, U).
statement('result', U, 'needAgain', LO) :-
   triple_exists_quantifier([ID]),
   needAgain('query_answering', ID, LO, U).
