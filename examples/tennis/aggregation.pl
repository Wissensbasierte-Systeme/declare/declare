

X --->  aggregation:Zs :-
   ddbase_aggregate( [H, T, length([H, T])],
      [H, T] := X/set/game/point/hit@[hand, type],
      Ys ),
   writeln_list(Ys),
   xpce_display_table(aggregation, [hand, type, count], Ys),
   ( foreach(Y, Ys), foreach(Z, Zs) do
        ( Y = [H, T, L],
          Z = hit:[hand:H, type:T, count:L]:[] ) ).


