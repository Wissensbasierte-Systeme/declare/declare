

X --->  players:Ys :-
   findall( Y,
      Y := X/player,
      Ys ).

/*
?- Y := doc('examples/tennis/2002_sampras_agassi_final.xml')/player,
   dwrite(xml, Y).
Y = player:[id:'A', name:'Sampras']:[] ;
Y = player:[id:'B', name:'Agassi']:[] ;
*/


