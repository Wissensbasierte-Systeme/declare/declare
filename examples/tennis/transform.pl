

% 4.115 = 8.23 / 2.
% 5.485
% 11.885


hit:As_1:Es ---> hit:As_2:Es :-
   Width = 4.115, Length = 11.885,
   append(_, [x:X, y:Y], As_1),
   name_to_number(X, X_2),
   name_to_number(Y, Y_2),
   round(X_2/Width,  2, X_3),
   round(Y_2/Length, 2, Y_3),
   append(As_1, [xx:X_3, yy:Y_3], As_2).
X ---> X.

set:As:Es ---> set:As:[].
X ---> X.

X ---> sets:Sets :-
   findall( set:As:[],
      ( Set := X/set, Set = set:As:_ ),
      Sets ).

X ---> points:Points :-
   findall( point:As_2:Es,
      ( point:As_1:Es := X/set::[@id=S]/
           game::[@id=G]/point::[@id=P],
        append([set:S, game:G, point:P], As_1, As_2) ),
      Points ).

X ---> points:Points :-
   findall( point:As_2:Es_2,
      ( point:As_1:Es_1 := X/set::[@id=S]/
           game::[@id=G]/point::[@id=P],
        hit_list_transform(Es_1, Es_2),
        append([set:S, game:G, point:P], As_1, As_2) ),
      Points ).

/* hit_list_transform(Width, Hits_1, Hits_2) <-
      */

hit_list_transform(Hits_1, Hits_2) :-
   Width = 4.115,
   hit_list_transform(Width, Hits_1, Hits_2).

hit_list_transform(Width, [H1, H2|Hits_1], [H3|Hits_2]) :-
   !,
   X1 := H1@x, name_to_number(X1, XX1),
   X2 := H2@x, name_to_number(X2, XX2),
   ( abs(XX1 - XX2) >= Width ->
     H1 = hit:As_1:Es_1,
     H3 = hit:[way:cross|As_1]:Es_1
   ; H3 = H1 ),
   hit_list_transform(Width, [H2|Hits_1], Hits_2).
hit_list_transform(_, Hits, Hits).


