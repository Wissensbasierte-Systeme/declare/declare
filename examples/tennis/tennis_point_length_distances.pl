

Match ---> table:Tuples :-
   ddbase_aggregate( [S, G, P, avg(L), avg(D)],
      ( Point := Match/set::[@id=S]/
           game::[@id=G, @service=Service]/point,
        P := Match/player::[@id=Service]@name,
        point_to_length_and_distance(Point, L, D) ),
      Tuples_2 ),
   Attributes = [set, game, service, hits, dist],
   ( foreach(T2, Tuples_2), foreach(T, Tuples) do
        ( pair_lists(':', Attributes, T2, As),
          T = row:As:[] ) ).


