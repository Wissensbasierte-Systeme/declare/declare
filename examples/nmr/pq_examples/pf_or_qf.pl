:- disjunctive.

p(f(X)); q(f(X)) :-
   p(X).
p(f(X)); q(f(X)) :-
   q(X).
p(a); q(a).

:- prolog.
