
sink(X) :-
   node(X),
   not(no_sink(X)).

no_sink(X) :-
   disconnected(_, X).

disconnected(X, Y) :-
   node(X), node(Y),
   not(tc(X, Y)).

tc(X, Y) :-
   arc(X, Y).
tc(X, Y) :-
   arc(X, Z), tc(Z, Y).

node(X) :-
   arc(X, _).
node(X) :-
   arc(_, X).

arc(a, a).

node(b).

