
graduate(Student, Courses, Date, Total) :-
   ddbase_aggregate( [Student, list(C), max_date(D), sum(E)],
      exam_course(Student, C, D, under_graduate, E),
      Tuples ),
   member([Student, Courses, Date, Total], Tuples),
   Total >= 15.

exam_course(Student, Course, Date, Level, ECTS) :-
   exam(Student, Course, Date),
   course(Course, Level, ECTS).

course('ADS', under_graduate, 10).
course('SWT', under_graduate, 10).
course('DB1', under_graduate, 5).
course('DB2', graduate, 5).
course('DDB', graduate, 9).

exam('Mary', 'ADS', 2007-02-05).
exam('Mary', 'SWT', 2007-07-05).
exam('Mary', 'DB2', 2009-07-23).
exam('John', 'ADS', 2007-02-05).
exam('John', 'DB1', 2008-02-06).
exam('John', 'DDB', 2009-07-26).

