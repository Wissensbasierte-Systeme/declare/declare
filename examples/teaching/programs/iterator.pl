

/* itlist(Predicate,List,Input,Output) <-
      */

itlist(Predicate,[X|Xs],Input,Output) :-
   apply(Predicate,[X,Input,Intermediate]),
   itlist(Predicate,Xs,Intermediate,Output).
itlist(_,[],Input,Input).

my_apply(Predicate,Arguments) :-
   Predicate =.. List_1,
   append(List_1,Arguments,List_2),
   Goal =.. List_2,
   call(Goal).


/* Examples:

?- itlist(add,[2,3,4],0,L).

L = 9 

Yes

?- tree_for_test(Tree),
   itlist( insert_into_tree, [7,8,9,10,23],
      Tree, New_Tree ),
   pp_tree(Tree),
   pp_tree(New_Tree).

Tree = [5, [4], [9, [6], [10]]]
New_Tree = [5, [4], [9, [6, [], [7, [], [8]]], [10, [], [23]]]] 

Yes
*/


