

factorial(N,1) :-
   N =< 0,
   !.
factorial(N,F) :-
   M is N - 1,
   factorial(M,G),
   F is N * G.


