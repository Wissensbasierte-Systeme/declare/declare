

test(tp_operator,1) :-
   dconsult('Vorl_Beispiele/ancestor.dl',Program),
   dportray(lp,Program),
   tp_fixpoint_iteration(Program,[],_).


tp_fixpoint_iteration(Program,I,J) :-
   tp_operator(Program,I,K),
   dportray(chs,[K]),
   ( ( I = K, !, J = K )
   ; tp_fixpoint_iteration(Program,K,J) ).

tp_operator(Program,I,J) :-
   findall( A,
      ( member(Rule,Program),
        parse_dislog_rule(Rule,[A],Body,[]),
        checklist( resolve_atom(I), Body ) ),
%       tp_resolvable(I,Body) ),
      L ),
   list_to_ord_set(L,J),
%  remove_duplicates(L,J),
   !.

resolve_atom(I,B) :-
   member(B,I).

tp_resolvable(I,[B|Bs]) :-
   member(B,I),
   tp_resolvable(I,Bs).
tp_resolvable(_,[]).


