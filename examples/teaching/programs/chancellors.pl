
/* Chancellors in Prolog */

/*
?- between('Kiesinger','Kohl',N).
N = 3 
Yes
?- between_2('Kiesinger','Kohl',Zs).
Zs = ['Brandt', 'Schmidt', 'Kohl'] 
Yes
*/

between(X,Y,N) :-
   between_2(X,Y,Zs), length(Zs,N).

between_2(X,X,[]) :-
   !.
between_2(X,Y,[Z|Zs]) :-
   chancellor(X,Z,_,_), between_2(Z,Y,Zs).

/*
?- most_chancellors_party(Party).
[4-CDU, 3-SPD]
Party = 'CDU' 
Yes
?- chancellor_parties(Parties).
Parties = ['CDU', 'SPD'] 
Yes
?- party_to_chancellors('SPD',Cs).
Cs = ['Brandt', 'Schmidt', 'Schroeder'] 
Yes
*/

most_chancellors_party(Best_Party) :-   
   most_chancellors_party(Best_Party,_).

most_chancellors_party(Best_Party,Max) :-   
   chancellor_parties(Parties),
   findall( N-Party,
      ( member(Party,Parties),
        party_to_chancellors(Party,Chancellors),
        length(Chancellors,N) ),
      Pairs ),
   writeln(Pairs),
   sort(Pairs,Pairs_2),
   reverse(Pairs_2,[Max-Best_Party|_]).

chancellor_parties(Parties) :-
   findall( Party,
      ( chancellor(_,_,Party,_)
      ; chancellor(_,_,_,Party) ),
      Parties_2 ),
   sort(Parties_2,Parties).

party_to_chancellors(Party,Chancellors) :-
   findall( Chancellor,
      ( chancellor(Chancellor,_,Party,_)
      ; chancellor(_,Chancellor,_,Party) ),
      Chancellors_2 ),
   sort(Chancellors_2,Chancellors).

   
chancellor('Kohl','Schroeder','CDU','SPD').
chancellor('Schmidt','Kohl','SPD','CDU').
chancellor('Brandt','Schmidt','SPD','SPD').
chancellor('Kiesinger','Brandt','CDU','SPD').
chancellor('Erhard','Kiesinger','CDU','CDU').
chancellor('Adenauer','Erhard','CDU','CDU').

