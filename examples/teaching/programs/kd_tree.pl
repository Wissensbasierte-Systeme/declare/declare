

kd_search(K,Tree,Tuple) :-
   kd_search(1,K,Tree,Tuple).

kd_search(_,_,[Tuple,_,_],Tuple).
kd_search(_,_,[Tuple],Tuple).
kd_search(N,K,[_,T1,T2],Tuple) :-
   kd_var(N,Tuple),
   !,
   kd_modulo(N+1,K,M),
   ( kd_search(M,K,T1,Tuple)
   ; kd_search(M,K,T2,Tuple) ).
kd_search(N,K,[V,T1,T2],Tuple) :-
   kd_modulo(N+1,K,M),
   ( ( kd_compare(N,@=<,Tuple,V),
       !,
       kd_search(M,K,T1,Tuple) )
   ; ( kd_compare(N,@>,Tuple,V),
       !,
       kd_search(M,K,T2,Tuple) ) ).

kd_modulo(N,K,M) :-
   modulo(N,K,L),
   ( ( L = 0,
       !,
       M = K )
   ; M = L ).

kd_compare(N,Operator,Tuple_1,Tuple_2) :-
   nth(N,Tuple_1,X_1),
   nth(N,Tuple_2,X_2),
   apply(Operator,[X_1,X_2]).

kd_var(N,Tuple) :-
   nth(N,Tuple,X),
   var(X).

test(kd_search,a(Y,Z)) :-
   kd_search(3,
      [ [b,c,d],
           [ [a,b,c],
              [ [a,e,g] ],
              [ [a,f,g] ] ],
           [ [a,b,g] ] ],
      [a,Y,Z] ).
test(kd_search,b(X,Y)) :-
   kd_search(3,
      [ [b,c,d],
           [ [a,b,c],
              [ [a,b,g] ],
              [ [a,f,g] ] ],
           [ [c,b,g] ] ],
      [X,Y,g] ).


