

/* findtrust(Market, Company, Total) <-
      */

findtrust(Market, Company, Total) :-
   trust_limit(Market, Threshold),
   market(Market, Company, Total),
   Total > Threshold.


/* market(Market, Company, Total) <-
      */

market(Market, Company, Total) :-
   company(Company, Market, Quota),
   findall( Comp, 
      controlled(Comp, Company),
      Companies ),
   maplist( market(Market),
      Companies, Quotas ),
   sum([Quota|Quotas], Total).

controlled(Company_2, Company_1) :-
   has_shares(Company_1, Company_2, N),
   N > 50.


/* has_shares(Company_2, Company_1) <-
      */

has_shares(violet, hyacinth, 51).
has_shares(violet, orchid, 20).
has_shares(violet, gardenia, 30).
has_shares(violet, lily, 51).
has_shares(hyacinth, daisy, 30).
has_shares(hyacinth, iris, 70).
has_shares(gardenia, orchid, 51).
has_shares(fiat, opel, 2).
has_shares(gardenia, tulip, 60).
has_shares(lily, rose, 59).
has_shares(tulip, azalea, 20).
has_shares(tulip, begonia, 25).
has_shares(begonia, azalea, 60).


/* company(Company, Market, Quota) <-
      */

company(violet, toys, 8).
company(hyacinth, toys, 7).
company(opel, cars, 33).
company(orchid, toys, 13).
company(gardenia, toys, 0).
company(tulip, toys, 12).
company(ford, cars, 30).
company(daisy, toys, 8).
company(iris, toys, 12).
company(lily, toys, 2).
company(rose, toys, 4).
company(azalea, toys, 19).
company(begonia, toys, 15).
company(fiat, cars, 35).


/* trust_limit(Market, Limit) <-
      */

trust_limit(toys, 20).
trust_limit(cars, 38).
trust_limit(gasoline, 40).	


/* sum(Xs, Sum) <-

sum([X|Xs], Sum):-
   sum(Xs, Sum2),
   Sum is X + Sum2.
sum([],0).

      */


