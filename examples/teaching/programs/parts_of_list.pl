

% parts_of_list(+Component:+Quantity, ?Parts) <-

parts_of_list(C:Q, [C:Q]) :-
   not(components(C, _, _)),
   !.
parts_of_list(C:Q, Parts) :-
   findall( Ps,
      ( components(C, C1, Q1),
        Q2 is Q * Q1,
        parts_of_list(C1:Q2, Ps) ),
      Pss ),
   append(Pss, Parts_2),
   multiset_normalize(Parts_2, Parts).


