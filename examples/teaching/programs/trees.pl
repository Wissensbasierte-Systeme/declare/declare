

/* search_in_tree(Key,Tree) <-
      */

search_in_tree(Key,[Key|_]).

search_in_tree(Key,[Root,Left,Right]) :-
   ( ( Key < Root,
       !,
       Tree = Left )
   ; ( Key > Root,
       !,
       Tree = Right ) ),
       search_in_tree(Key,Tree).


tree_vs_root_and_subtrees([Root], Root,[],[]).
tree_vs_root_and_subtrees([Root,Left,Right],Root,Left,Right).

tree_empty([]).

tree_for_test( [5,[4],[9,[6],[10]]] ).


/* insert_into_tree(Key,Tree,New_Tree) <-
      */

insert_into_tree(Key,Tree,Tree) :-              /* Key schon im Baum */
   tree_vs_root_and_subtrees(Tree,Key,_,_),
   !.

insert_into_tree(Key,Tree,New_Tree) :-
   tree_vs_root_and_subtrees(Tree,Root,Left,Right),
   !,
   ( ( Key < Root,
       insert_into_tree(Key,Left,Left_1),
       tree_vs_root_and_subtrees(New_Tree,Root,Left_1,Right) )
   ; ( Key > Root,
       insert_into_tree(Key,Right,Right_1),
       tree_vs_root_and_subtrees(New_Tree,Root,Left,Right_1) ) ).

insert_into_tree(Key,_,New_Tree) :-             /* Key wird eingefuegt */
   tree_empty(Empty),
   tree_vs_root_and_subtrees(New_Tree,Key,Empty,Empty).


