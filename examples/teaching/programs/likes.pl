
:- dynamic nice/1.

likes(george, Y) :-
   woman(Y), likes(Y, books).
likes(mary, Y) :-
   ( nice(Y)
   ; loves(Y, cats) ).

likes(mary, books).
woman(mary).
loves(george, cats).

liker(X) :-
   likes(X, _).

not_liker(X) :-
   member(X, [mary, george, cats, books]),
   not(liker(X)).

