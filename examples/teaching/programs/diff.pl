

diff(X, X, 1) :-
   !.
diff(F, _, 0) :-
   atomic(F).
diff(X^0, X, 0).
diff(X^N, X, N*X^M) :-
   N \= 0,
   M is N - 1.
diff(sin(X), X, cos(X)).
diff(e^X, X, e^X).
diff(F+G, X, DF+DG) :-
   diff(F, X, DF),
   diff(G, X, DG).
diff(F-G, X, DF-DG) :-
   diff(F, X, DF),
   diff(G, X, DG).
diff(C*F, X, C*DF) :-
   atomic(C),
   !,
   diff(F, X, DF).
diff(-F, X, -DF) :-
   !,
   diff(F, X, DF).

diff(cos(X), X, -sin(X)).
diff(log(X), X, X^(-1)).
diff(F*G, X, F*DG+DF*G) :-
   diff(F, X, DF),
   diff(G, X, DG).


