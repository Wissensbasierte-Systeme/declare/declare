

employee(A, B, C, D, E) :-
   employee(A, B, C, D, _, _, E).

employee_works_on_outside_project(E:De, P:Dp) :-
   employee_department(E, De),
   works_on(E, P, _),
   project(_, P, _, Dp),
   De \= Dp.

employee_department(E, D) :-
   employee(_, E, _, _, _, _, D).

department_name(Employee, Dname) :-
   employee(_, Employee, _, _, _, _, Dno),
   department(Dname, Dno, _, _).

% employee(Name, SSN, BDate, Sex, Salary, SuperSSN, DNO)
employee('Borg',    '1111', date(1927,11,10), 'M', 55000, '$null$', 1).
employee('Wong',    '2222', date(1945,12,08), 'M', 40000, '1111', 5).
employee('Wallace', '3333', date(1931,6,20),  'F', 43000, '1111', 4).
employee('Smith',   '4444', date(1955,1,09),  'M', 30000, '2222', 5).
employee('Narayan', '5555', date(1952,9,15),  'M', 38000, '2222', 5).
employee('English', '6666', date(1962,7,31),  'F', 25000, '2222', 5).
employee('Zelaya',  '7777', date(1958,7,19),  'F', 25000, '3333', 4).
employee('Jabbar',  '8888', date(1959,3,29),  'M', 25000, '3333', 4).

% department(Name, DNO, ManagerSSN, Founded)
department('Headquarters',   1, '1111', date(1971,6,19)).
department('Administration', 4, '3333', date(1985,1,1)).
department('Research',       5, '2222', date(1978,5,22)).

% project(Name, ProjectID, Location, DNO)
project('ProductX', 1, 'Bellaire', 5).
project('ProductY', 2, 'Sugarland', 5).
project('ProductZ', 3, 'Houston', 5).
project('Computerization', 10, 'Stafford', 4).
project('Reorganization', 20, 'Houston', 1).
project('Newbenefits', 30, 'Stafford', 4).

% works_on(SSN, ProjectID, Hours)
works_on('1111', 20,  0.0).
works_on('2222',  2, 10.0).
works_on('2222',  3, 10.0).
works_on('3333', 20, 15.0).
works_on('3333', 30, 20.0).
works_on('4444',  1, 32.5).
works_on('4444',  2,  7.5).
works_on('5555',  3, 40.0).
works_on('6666',  1, 20.0).
works_on('6666',  2, 20.0).
works_on('7777', 10, 10.0).
works_on('7777', 30, 30.0).
works_on('8888', 10, 35.5).
works_on('8888', 30,  5.0).

% dependent(SSN, Name, Sex, BDate, Rel)
dependent('2222', 'Alice', 'F', date(1976, 4, 5), 'DAUGHTER').
dependent('2222', 'Joy', 'F', date(1948, 5, 3), 'SPOUSE').
dependent('2222', 'Theodore', 'M', date(1973, 10, 25), 'SON').
dependent('3333', 'Abner', 'M', date(1932, 2, 29), 'SPOUSE').
dependent('4444', 'Alice', 'F', date(1978, 12, 31), 'DAUGHTER').
dependent('4444', 'Elizabeth', 'F', date(1957, 5, 5), 'SPOUSE').
dependent('4444', 'Michael', 'M', date(1978, 1, 1), 'SON').

% dept_locations(DNO, Location)
dept_locations(1, 'Houston').
dept_locations(4, 'Stafford').
dept_locations(5, 'Bellaire').
dept_locations(5, 'Houston').
dept_locations(5, 'Sugarland').
