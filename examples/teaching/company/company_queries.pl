

direct_supervisor(A, B) :-
   findall( [A,B],
      ( employee(A, _, _, _, _, S, _),
        employee(B, S, _, _, _, _, _) ),
      Tuples ),
   member([A,B], Tuples).

superior(Emp, Sup) :-
   ( direct_supervisor(Emp, Sup)
   ; direct_supervisor(Emp, X), superior(X, Sup) ).

ds(1, 2).
ds(2, 1).

s(Emp, Sup) :-
   ( ds(Emp, Sup)
   ; ds(Emp, X), s(X, Sup) ).

/*

ds(1, 2).
ds(2, 1).
ds(2, 3).

s(Emp, Sup) :-
   ds(Emp, Sup).
s(Emp, Sup) :-
   ds(Emp, X),
   s(X, Sup), Emp \= Sup.

*/


