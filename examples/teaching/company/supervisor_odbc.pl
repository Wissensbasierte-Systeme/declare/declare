
:- ddbase_connect(odbc(mysql), company:employee).

supervisor(SSN_1, SSN_2) :-
   direct_supervisor(SSN_1, SSN_2).
supervisor(SSN_1, SSN_2) :-
   direct_supervisor(SSN_1, SSN_3),
   supervisor(SSN_3, SSN_2).

direct_supervisor(SSN_1, SSN_2) :-
   employee(_,_,_, SSN_2, _,_,_,_, SSN_1, _),
   SSN_1 \= '$null$'.

employee_department(E, D) :-
   employee(_, _, _, E, _, _, _, _, _, D).

supervisor_is_in_another_department(E:De, S:Ds) :-
   supervisor(E, S),
   employee_department(E, De),
   employee_department(S, Ds),
   De \= Ds.

supervisor_name(E_Name, S_Name) :-
   employee(_, _, E_Name, _, _, _, _, _, Ssn, _),
   employee(_, _, S_Name, Ssn, _, _, _, _, _, _).

