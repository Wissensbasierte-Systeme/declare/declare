
works_on(X, Y) :-
   ddbase_aggregate( [SSN, list((ProjectID, Hours))],
      works_on(SSN, ProjectID, Hours),
      Tuples ),
   xpce_display_table(['SSN', '[(ProjectID, Hours)]'], Tuples),
   member([X, Y], Tuples).

works_on('1111', 20,  0.0).
works_on('2222',  2, 10.0).
works_on('2222',  3, 10.0).
works_on('3333', 20, 15.0).
works_on('3333', 30, 20.0).
works_on('4444',  1, 32.5).
works_on('4444',  2,  7.5).
works_on('5555',  3, 40.0).
works_on('6666',  1, 20.0).
works_on('6666',  2, 20.0).
works_on('7777', 10, 10.0).
works_on('7777', 30, 30.0).
works_on('8888', 10, 35.5).
works_on('8888', 30,  5.0).

