
:- ddbase_connect_(odbc(mysql), company:employee).
:- ddbase_connect_(odbc(mysql), company:department).
:- stratify
      department/4 < employee/10, employee/10 < department/1,
      department/1 < foreign_key_violation/2,
      foreign_key_violation/2 < primary_key_violation/3.

primary_key_violation(employee, X, Y) :-
   X = employee(_,_,_, SSN, _,_,_,_,_,_),
   Y = employee(_,_,_, SSN, _,_,_,_,_,_),
   call(X), call(Y),
   compare(<, X, Y).

foreign_key_violation(employee('DNO'), X) :-
   X = employee(_,_,_,_,_,_,_,_,_, DNO),
   call(X),
   not(department(DNO)).

department(DNO) :-
   department(_, DNO, _,_).

employee('****','*','*****','444444444',
   date(1955,1,9),'*** *******, *******, **','M',30000.0,'222222222',6).

