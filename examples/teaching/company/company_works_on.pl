

:- ddbase_connect(odbc(mysql), company:works_on).

works_on(X, Y) :-
   ddbase_aggregate( [SSN, list((ProjectID, Hours))],
      works_on(SSN, ProjectID, Hours),
      Tuples ),
   xpce_display_table(['SSN', '[(ProjectID, Hours)]'], Tuples),
   member([X, Y], Tuples).


