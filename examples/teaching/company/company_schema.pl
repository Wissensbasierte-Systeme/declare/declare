
schema( table:[name:employee, database:company]:[
   attributes:['Fname', 'Minit', 'Lname', 'SSN', 'BDATE',
      'Address', 'Sex', 'Salary', 'Superssn', 'DNO'],
   primary_key:['SSN'],
   foreign_keys:['Superssn'->employee:'SSN', 'DNO'->department:'DNO']] ).

schema( table:[name:works_on, database:company]:[
   attributes:['ESSN', 'PNO', 'HOURS'],
   primary_key:['ESSN', 'PNO'],
   foreign_keys:['ESSN'->employee:'SSN', 'PNO'->project:'PNO']] ).

schema( table:[name:department, database:company]:[
   attributes:['DNAME', 'DNO', 'MGRSSN', 'MGRSTARTDATE'],
   primary_key:['DNO'],
   not_null:['DNO', 'DNAME'],
   foreign_keys:['MGRSSN'->employee:'SSN']] ).

schema( table:[name:project, database:company]:[
   attributes:['PNAME', 'PNO', 'PLOCATION', 'DNO'],
   primary_key:['PNO'],
   not_null:['PNO', 'PNAME'],
   foreign_keys:['DNO'->department:'DNO']] ).

