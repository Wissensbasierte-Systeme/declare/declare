
end_station(X) :-
   station(X),
   not(transit_station(X)).

transit_station(Y) :-
   neighbor_station(X, Y),
   neighbor_station(Y, Z),
   Z \= X,
   line(X, L), line(Y, L), line(Z, L). 

neighbor_station(X, Y) :-
   neighbor_station(Y, X).

station(X) :-
   line(X, _).

% neighbor_station(Station_1, Station_2) <-
neighbor_station('Olching', 'Groebenzell').
neighbor_station('Groebenzell', 'Lochhausen').
neighbor_station('Lochhausen', 'Langwied').
neighbor_station('Langwied', 'Pasing').
neighbor_station('Pasing', 'Laim').
neighbor_station('Leienfelsstrasse', 'Pasing').
neighbor_station('Obermenzing', 'Laim').
neighbor_station('Allach', 'Obermenzing').
neighbor_station('Siemenswerke', 'Solln').
neighbor_station('Solln', 'Deisenhofen').

% line(Station, Line) <-
line('Olching', 'S3').
line('Groebenzell', 'S3').
line('Lochhausen', 'S3').
line('Langwied', 'S3').
line('Pasing', 'S3').
line('Laim', 'S3').
line('Pasing', 'S4').
line('Laim', 'S4').
line('Leienfelsstrasse', 'S4').
line('Laim', 'S2').
line('Obermenzing', 'S2').
line('Allach', 'S2').
line('Siemenswerke', 'S27').
line('Solln', 'S27').
line('Deisenhofen', 'S27').

