
linked(X,Y) :- ( link(X,Y) ; link(Y,X) ).
station(S) :- linked(S,_).

connected(X,Y) :- linked(X,Y).
connected(X,Y) :- connected(X,Z), linked(Z,Y), X \= Y.

circumvent(X,Y,S) :- linked(X,Y), station(S), X \= S, Y \= S.
circumvent(X,Y,S) :- circumvent(X,Z,S), circumvent(Z,Y,S), X \= Y.

cutpoint(X,Y,S) :- connected(X,Y), station(S), X \= S, Y \= S, not(circumvent(X,Y,S)).

exists_cutpoint(X,Y) :- cutpoint(X,Y,_).

doubly_connected(X,Y) :- connected(X,Y), not(exists_cutpoint(X,Y)).

link(a,b).
link(a,c).
link(b,d).
link(c,d).
link(d,e).

