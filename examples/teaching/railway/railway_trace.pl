
linked(X,Y) :- ( link(X,Y) ; link(Y,X) ).
station(S) :- linked(S,_).

c(X,Y) :- connected(U,V), sort([U,V], [X,Y]).
connected(X,Y) :- linked(X,Y).
connected(X,Y) :- connected(X,Z), linked(Z,Y), X \= Y.

cv(X,Y,S) :- circumvent(U,V,S), sort([U,V], [X,Y]).
circumvent(X,Y,S) :- linked(X,Y), station(S), X \= S, Y \= S.
circumvent(X,Y,S) :- circumvent(X,Z,S), circumvent(Z,Y,S), X \= Y.

cutpoint(X,Y,S) :- connected(X,Y), station(S), X \= S, Y \= S, not(circumvent(X,Y,S)).

exists_cutpoint(X,Y) :- cutpoint(X,Y,_).

dc(X,Y) :- doubly_connected(U,V), sort([U,V], [X,Y]).
doubly_connected(X,Y) :- connected(X,Y), not(exists_cutpoint(X,Y)).

link(a,b).
link(a,c).
link(b,d).
link(c,d).
link(d,e).

