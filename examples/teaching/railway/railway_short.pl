
linked(X,Y) :- ( link(X,Y) ; link(Y,X) ).
station(S) :- linked(S,_).

c(X,Y) :- linked(X,Y).
c(X,Y) :- c(X,Z), linked(Z,Y), X \= Y.

cv(X,Y,S) :- linked(X,Y), station(S), X \= S, Y \= S.
cv(X,Y,S) :- cv(X,Z,S), cv(Z,Y,S), X \= Y.

cp(X,Y,S) :- c(X,Y), station(S), X \= S, Y \= S, not(cv(X,Y,S)).

ecp(X,Y) :- cp(X,Y,_).

dc(X,Y) :- c(X,Y), not(ecp(X,Y)).

link(a,b).
link(a,c).
link(b,d).
link(c,d).
link(d,e).

