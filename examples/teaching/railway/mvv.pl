
% rules

link(X, Y) :-
   line(_, Stations),
   append(_, [X, Y|_], Stations).

linked(X, Y) :-
   ( link(X, Y) ; link(Y, X) ).

station(S) :-
   line(_, Stations),
   member(S, Stations).

connected(X, Y) :-
   linked(X, Y).
connected(X, Y) :-
   connected(X, Z), linked(Z, Y), X \= Y.

circumvent(X, Y, S) :-
   linked(X,Y), station(S), X \= S, Y \= S.
circumvent(X, Y, S) :-
   circumvent(X, Z, S), circumvent(Z, Y, S), X \= Y.

cutpoint(X, Y, S) :-
   connected(X, Y), station(S), X \= S, Y \= S,
   not(circumvent(X, Y, S)).

exists_cutpoint(X, Y) :-
   cutpoint(X, Y, _).

doubly_connected(X, Y) :-
   connected(X, Y),
   not(exists_cutpoint(X, Y)).

% lines

line(r1, [
   donnersbergerbruecke, hauptbahnhof, stachus,
   marienplatz, ostbahnhof ]).

line(s1, [
   freising, moosach, laim, donnersbergerbruecke, ostbahnhof,
   giesing, neuperlachsued, kreuzstrasse ]).
line(s2, [
   petershausen, laim, donnersbergerbruecke, ostbahnhof,
   giesing, deisenhofen, holzkirchen ]).
line(s3, [
   nannhofen, pasing, laim, donnersbergerbruecke,
   ostbahnhof, leuchtenbergring, ismaning ]).
line(s4, [
   geltendorf, pasing, laim, donnersbergerbruecke,
   ostbahnhof, leuchtenbergring, bergamlaim, ebersberg ]).
line(s5, [
   herrsching, westkreuz, pasing, laim,
   donnersbergerbruecke, ostbahnhof ]).
line(s6, [
   tutzing, westkreuz, pasing, laim, donnersbergerbruecke,
   ostbahnhof, leuchtenbergring, bergamlaim, erding ]).
line(s7, [
   wolfratshausen, solln, harras, heimeranplatz,
   donnersbergerbruecke, ostbahnhof ]).
line(s8, [
   olympiastadion, moosach, laim, donnersbergerbruecke,
   ostbahnhof ]).
line(s27, [
   hauptbahnhof, donnersbergerbruecke, heimeranplatz,
   harras, solln, deisenhofen ]).

line(u1, [
   rotkreuzplatz, hauptbahnhof, sendlingertor,
   giesing, innsbruckerring ]).
line(u3, [
   holzapfelkreuth, harras, sendlingertor,
   marienplatz, odeonsplatz, muenchenerfreiheit,
   scheidplatz, olympiazentrum ]).
line(u5, [
   westendstrasse, heimeranplatz, hauptbahnhof,
   stachus, odeonsplatz ]).
line(u6, [
   holzapfelkreuth, harras, sendlingertor, marienplatz,
   odeonsplatz, muenchenerfreiheit, kieferngarten ]).
line(u8, [
   olympiazentrum, scheidplatz, hauptbahnhof,
   sendlingertor, giesing, innsbruckerring,
   neuperlachsued ]).

