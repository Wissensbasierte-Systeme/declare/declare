/*****************************************************************************/
/* in dieser Datei werden alle Dialoge definiert.                            */
/*****************************************************************************/

dialog(buttons,
       [ object        :=
	   Buttons,
	 parts         :=
	   [ Buttons   := dialog('Buttons'),
	     Abbrechen := button(abbrechen),
	     Weiter    := button(weiter)
	   ],
	 modifications :=
	   [ Abbrechen := [ alignment := right
			  ],
	     Weiter    := [ alignment := right
			  ]
	   ],
	 layout        :=
	   [ right(Weiter, Abbrechen)
	   ],
	 behaviour     :=
	   [ Abbrechen := [ message := message(Buttons, return, @nil)
			  ],
	     Weiter    := [ message := message(Buttons, return, @on)
			  ]
	   ]
       ]).


dialog(spezialbogen_huefte,
       [ object        :=
	   Spezialbogen_Huefte,
	 parts         :=
	   [ Spezialbogen_Huefte :=
	       dialog('Spezialbogen Huefte'),
	     Patient_Code        :=
	       text_item(patient_code),
	     Behandelnder_Arzt   :=
	       text_item(depp_behandelnder_arzt),
	     Datum               :=
	       text_item(datum),
	     Erst_Folge          :=
	       menu('erst/folge', choice)
	   ],
	 modifications :=
	   [ Patient_Code      :=
	       [ length := 30
	       ],
	     Behandelnder_Arzt :=
	       [ length := 30
	       ],
	     Datum             :=
	       [ length := 30
	       ],
	     Erst_Folge        :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Erstvorstellung',
					  @default,
					  'Erstvorstellung',
					  @off,
					  @nil,
					  e),
				menu_item('Folgeuntersuchung',
					  @default,
					  'Folgeuntersuchung',
					  @off,
					  @nil,
					  f)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Behandelnder_Arzt, Patient_Code),
	     below(Datum, Behandelnder_Arzt),
	     below(Erst_Folge, Datum)
	   ]
       ]).


dialog(spezialbogen_knie,
       [ object        :=
	   Spezialbogen_Knie,
	 parts         :=
	   [ Spezialbogen_Knie :=
	       dialog('Spezialbogen Knie'),
	     Patient_Code      :=
	       text_item(patient_code),
	     Behandelnder_Arzt :=
	       text_item(behandelnder_arzt),
	     Datum             :=
	       text_item(datum),
	     Erst_Folge        :=
	       menu('erst/folge', choice)
	   ],
	 modifications :=
	   [ Patient_Code      :=
	       [ length := 30
	       ],
	     Behandelnder_Arzt :=
	       [ length := 30
	       ],
	     Datum             :=
	       [ length := 30
	       ],
	     Erst_Folge        :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Erstvorstellung',
					  @default,
					  'Erstvorstellung',
					  @off,
					  @nil,
					  e),
				menu_item('Folgeuntersuchung',
					  @default,
					  'Folgeuntersuchung',
					  @off,
					  @nil,
					  f)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Behandelnder_Arzt, Patient_Code),
	     below(Datum, Behandelnder_Arzt),
	     below(Erst_Folge, Datum)
	   ]
       ]).


dialog(spezialbogen_obere_extremitaet,
       [ object        :=
	   Spezialbogen_Obere_Extremitaet,
	 parts         :=
	   [ Spezialbogen_Obere_Extremitaet :=
	       dialog('Spezialbogen Obere Extremitaet'),
	     Patient_Code                   :=
	       text_item(patient_code),
	     Behandelnder_Arzt              :=
	       text_item(behandelnder_arzt),
	     Datum                          :=
	       text_item(datum),
	     Erst_Folge                     :=
	       menu('erst/folge', choice)
	   ],
	 modifications :=
	   [ Patient_Code      :=
	       [ length := 30
	       ],
	     Behandelnder_Arzt :=
	       [ length := 30
	       ],
	     Datum             :=
	       [ length := 30
	       ],
	     Erst_Folge        :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Erstvorstellung',
					  @default,
					  'Erstvorstellung',
					  @off,
					  @nil,
					  e),
				menu_item('Folgeuntersuchung',
					  @default,
					  'Folgeuntersuchung',
					  @off,
					  @nil,
					  f)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Behandelnder_Arzt, Patient_Code),
	     below(Datum, Behandelnder_Arzt),
	     below(Erst_Folge, Datum)
	   ]
       ]).


dialog(spezialbogen_untere_extremitaet,
       [ object        :=
	   Spezialbogen_Untere_Extremitaet,
	 parts         :=
	   [ Spezialbogen_Untere_Extremitaet :=
	       dialog('Spezialbogen Untere Extremitaet'),
	     Patient_Code                    :=
	       text_item(patient_code),
	     Behandelnder_Arzt               :=
	       text_item(behandelnder_arzt),
	     Datum                           :=
	       text_item(datum),
	     Erst_Folge                      :=
	       menu('erst/folge', choice)
	   ],
	 modifications :=
	   [ Patient_Code      :=
	       [ length := 30
	       ],
	     Behandelnder_Arzt :=
	       [ length := 30
	       ],
	     Datum             :=
	       [ length := 30
	       ],
	     Erst_Folge        :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Erstvorstellung',
					  @default,
					  'Erstvorstellung',
					  @off,
					  @nil,
					  e),
				menu_item('Folgeuntersuchung',
					  @default,
					  'Folgeuntersuchung',
					  @off,
					  @nil,
					  f)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Behandelnder_Arzt, Patient_Code),
	     below(Datum, Behandelnder_Arzt),
	     below(Erst_Folge, Datum)
	   ]
       ]).


dialog(spezialbogen_wirbelsaeule,
       [ object        :=
	   Spezialbogen_Wirbelsaeule,
	 parts         :=
	   [ Spezialbogen_Wirbelsaeule :=
	       dialog('Spezialbogen Wirbelsaeule'),
	     Patient_Code              :=
	       text_item(patient_code),
	     Behandelnder_Arzt         :=
	       text_item(behandelnder_arzt),
	     Datum                     :=
	       text_item(datum),
	     Erst_Folge                :=
	       menu('erst/folge', choice)
	   ],
	 modifications :=
	   [ Patient_Code      :=
	       [ length := 30
	       ],
	     Behandelnder_Arzt :=
	       [ length := 30
	       ],
	     Datum             :=
	       [ length := 30
	       ],
	     Erst_Folge        :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Erstvorstellung',
					  @default,
					  'Erstvorstellung',
					  @off,
					  @nil,
					  e),
				menu_item('Folgeuntersuchung',
					  @default,
					  'Folgeuntersuchung',
					  @off,
					  @nil,
					  f)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Behandelnder_Arzt, Patient_Code),
	     below(Datum, Behandelnder_Arzt),
	     below(Erst_Folge, Datum)
	   ]
       ]).



dialog(set_mode,
       [ object        :=
	   Set_Mode,
	 parts         :=
	   [ Set_Mode  := dialog('Set Mode'),
	     Mode      := menu(mode, choice),
	     Abbrechen := button(abbrechen),
	     Ok        := button(ok)
	   ],
	 modifications :=
	   [ Mode      := [ layout := vertical,
			    append := [ menu_item(entity_mode,
						  @default,
						  'Entity Mode',
						  @off,
						  @nil,
						  e),
					menu_item(attribute_mode,
						  @default,
						  'Attribute Mode',
						  @off,
						  @nil,
						  a),
					menu_item(attribute_list_mode,
						  @default,
						  'Attribute List Mode',
						  @off,
						  @nil,
						  t)
				      ]
			  ],
	     Abbrechen := [ alignment := column
			  ],
	     Ok        := [ alignment := right
			  ]
	   ],
	 layout        :=
	   [ below(Abbrechen, Mode),
	     right(Ok, Abbrechen)
	   ],
	 behaviour     :=
	   [ Abbrechen := [ message := message(Set_Mode, return, @nil)
			  ],
	     Ok        := [ message := message(Set_Mode,
					       return,
					       Mode?selection)
			  ]
	   ]
       ]).


/*
dialog(buttons,
       [ object        :=
	   Buttons,
	 parts         :=
	   [ Buttons   := dialog('Buttons'),
	     Zur_Ck    := button(zur�ck),
	     Abbrechen := button(abbrechen),
	     Weiter    := button(weiter)
	   ],
	 modifications :=
	   [ Zur_Ck := [ alignment := left
		       ],
	     Weiter := [ alignment := right
		       ]
	   ],
	 layout        :=
	   [ right(Abbrechen, Zur_Ck),
	     right(Weiter, Abbrechen)
	   ],
	 behaviour     :=
	   [ Zur_Ck    := [ message := message(Buttons, return, @off)
			  ],
	     Abbrechen := [ message := message(Buttons, return, @nil)
			  ],
	     Weiter    := [ message := message(Buttons, return, @on)
			  ]
	   ]
       ]).

*/


dialog(ellbogen,
       [ object        :=
	   Ellbogen,
	 parts         :=
	   [ Ellbogen             :=
	       dialog('Ellbogen'),
	     Flex_____aktiv___    :=
	       slider('Flex.    aktiv  :', 0, 180, 0),
	     Ext_____aktiv        :=
	       slider('Ext.    aktiv', -91, 15, -91),
	     Pronation__aktiv     :=
	       slider('Pronation  aktiv', 0, 90, 0),
	     Supination_aktiv     :=
	       slider('Supination aktiv', 0, 90, 0),
	     Kraft_in_Flexion____ :=
	       slider('kraft in Flexion   : ', 1, 10, 1),
	     Kraft_in_Extension   :=
	       slider('kraft in Extension', 1, 10, 1),
	     Instabilitaet        :=
	       menu('Instabilitaet', toggle),
	     Atrophien            :=
	       menu(atrophien, choice),
	     HWS_Beschwerden      :=
	       menu('HWS-Beschwerden', choice)
	   ],
	 modifications :=
	   [ Flex_____aktiv___    :=
	       [ width := 191
	       ],
	     Ext_____aktiv        :=
	       [ width := 188
	       ],
	     Pronation__aktiv     :=
	       [ width := 191
	       ],
	     Supination_aktiv     :=
	       [ width := 191
	       ],
	     Kraft_in_Flexion____ :=
	       [ label := 'kraft in Flexion   : ',
		 width := 188
	       ],
	     Kraft_in_Extension   :=
	       [ width := 191
	       ],
	     Instabilitaet        :=
	       [ alignment := left,
		 reference := point(0, 13),
		 append    := [ menu_item(medial, @default, 'Medial', @off, @nil, m),
				menu_item(lateral,
					  @default,
					  'Lateral',
					  @off,
					  @nil,
					  l)
			      ]
	       ],
	     Atrophien            :=
	       [ feedback  := show_selection_only,
		 alignment := left,
		 reference := point(0, 13),
		 append    := [ menu_item('Ja', @default, 'Ja', @off, @nil, j),
				menu_item('Nein', @default, 'Nein', @off, @nil, n)
			      ]
	       ],
	     HWS_Beschwerden      :=
	       [ feedback  := show_selection_only,
		 alignment := right,
		 reference := point(0, 13),
		 append    := [ menu_item('Ja', @default, 'Ja', @off, @nil, j),
				menu_item('Nein', @default, 'Nein', @off, @nil, n)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Ext_____aktiv, Flex_____aktiv___),
	     below(Pronation__aktiv, Ext_____aktiv),
	     below(Supination_aktiv, Pronation__aktiv),
	     below(Kraft_in_Flexion____, Supination_aktiv),
	     below(Kraft_in_Extension, Kraft_in_Flexion____),
	     below(Instabilitaet, Kraft_in_Extension),
	     below(Atrophien, Instabilitaet),
	     right(HWS_Beschwerden, Atrophien)
	   ]
       ]).


dialog(schulter,
       [ object        :=
	   Schulter,
	 parts         :=
	   [ Schulter            :=
	       dialog('Schulter'),
	     Flex__aktiv         :=
	       slider('flex. aktiv', 0, 180, 0),
	     Abd___Aktiv         :=
	       slider('abd   aktiv', 0, 180, 0),
	     ARO_aktiv           :=
	       menu('ARO aktiv', toggle),
	     IRO_aktiv           :=
	       menu('IRO aktiv', toggle),
	     ABD_passiv          :=
	       slider('ABD passiv', -1, 90, -1),
	     ARO_passiv_         :=
	       slider('ARO passiv:', -1, 90, -1),
	     Kraft___in_kp__     :=
	       slider('Kraft  (in kp):', 1, 10, 1),
	     Instabilitaet       :=
	       menu('Instabilitaet', toggle),
	     Atrophien           :=
	       menu('atrophien                  ', choice),
	     Schmerzhafter_Bogen :=
	       menu('Schmerzhafter Bogen', choice),
	     HWS_Beschwerden     :=
	       menu('HWS-Beschwerden    ', choice),
	     ACG_Schmerz         :=
	       menu('ACG-Schmerz           ', choice),
	     Torticollis         :=
	       menu('Torticollis                  ', choice)
	   ],
	 modifications :=
	   [ Flex__aktiv         :=
	       [ width := 183
	       ],
	     Abd___Aktiv         :=
	       [ label := 'ABD  aktiv:',
		 width := 185
	       ],
	     ARO_aktiv           :=
	       [ multiple_selection :=
		   @off,
		 feedback           :=
		   show_selection_only,
		 value_font         :=
		   @times_roman_12,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item('H. am Hk., ELL. nach vorn',
			       @default,
			       'H. am Hk., ELL. nach vorn',
			       @off,
			       @nil,
			       h),
		     menu_item('H. am Hk., Ell. nach hinten',
			       @default,
			       'H. am Hk., Ell. nach hinten',
			       @off,
			       @nil,
			       a),
		     menu_item('H. auf dem K., Ell. nach vorn',
			       @default,
			       'H. auf dem K., Ell. nach vorn',
			       @off,
			       @nil,
			       d),
		     menu_item('H. auf dem K., Ell. nach hinten',
			       @default,
			       'H. auf dem K., Ell. nach hinten',
			       @off,
			       @nil,
			       k),
		     menu_item('Vollst. Elevation vom K. ausgehend',
			       @default,
			       'Vollst. Elevation vom K. ausgehend',
			       @off,
			       @nil,
			       v)
		   ]
	       ],
	     IRO_aktiv           :=
	       [ multiple_selection :=
		   @off,
		 feedback           :=
		   show_selection_only,
		 value_font         :=
		   @times_roman_12,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item('trochanter major',
			       @default,
			       'Trochanter major',
			       @off,
			       @nil,
			       t),
		     menu_item(gesaess, @default, 'Gesaess', @off, @nil, g),
		     menu_item('ISG', @default, 'ISG', @off, @nil, i),
		     menu_item('LWK 3', @default, 'LWK 3', @off, @nil, l),
		     menu_item('BWK 12', @default, 'BWK 12', @off, @nil, b),
		     menu_item('Interscapulaer',
			       @default,
			       'Interscapulaer',
			       @off,
			       @nil,
			       n)
		   ]
	       ],
	     ABD_passiv          :=
	       [ width := 192
	       ],
	     ARO_passiv_         :=
	       [ drag  := @on,
		 width := 192
	       ],
	     Kraft___in_kp__     :=
	       [ width := 192
	       ],
	     Instabilitaet       :=
	       [ multiple_selection :=
		   @off,
		 feedback           :=
		   show_selection_only,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item('Apprehension positiv',
			       @default,
			       'Apprehension positiv',
			       @off,
			       @nil,
			       a),
		     menu_item('Schublade positiv',
			       @default,
			       'Schublade positiv',
			       @off,
			       @nil,
			       s),
		     menu_item('Sulcuszeichen positiv',
			       @default,
			       'Sulcuszeichen positiv',
			       @off,
			       @nil,
			       u)
		   ]
	       ],
	     Atrophien           :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Ja', @default, 'Ja', @off, @nil, j),
				menu_item('Nein', @default, 'Nein', @off, @nil, n)
			      ]
	       ],
	     Schmerzhafter_Bogen :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Ja', @default, 'Ja', @off, @nil, j),
				menu_item('Nein', @default, 'Nein', @off, @nil, n)
			      ]
	       ],
	     HWS_Beschwerden     :=
	       [ label     := 'HWS-Beschwerden   :',
		 reference := point(0, 13),
		 append    := [ menu_item('Ja', @default, 'Ja', @off, @nil, j),
				menu_item('Nein', @default, 'Nein', @off, @nil, n)
			      ]
	       ],
	     ACG_Schmerz         :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Ja', @default, 'Ja', @off, @nil, j),
				menu_item('Nein', @default, 'Nein', @off, @nil, n)
			      ]
	       ],
	     Torticollis         :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Ja', @default, 'Ja', @off, @nil, j),
				menu_item('Nein', @default, 'Nein', @off, @nil, n)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Abd___Aktiv, Flex__aktiv),
	     below(ARO_aktiv, Abd___Aktiv),
	     below(IRO_aktiv, ARO_aktiv),
	     below(ABD_passiv, IRO_aktiv),
	     below(ARO_passiv_, ABD_passiv),
	     below(Kraft___in_kp__, ARO_passiv_),
	     below(Instabilitaet, Kraft___in_kp__),
	     below(Atrophien, Instabilitaet),
	     below(Schmerzhafter_Bogen, Atrophien),
	     below(HWS_Beschwerden, Schmerzhafter_Bogen),
	     below(ACG_Schmerz, HWS_Beschwerden),
	     below(Torticollis, ACG_Schmerz)
	   ]
       ]).



dialog(handgelenk,
       [ object        :=
	   Handgelenk,
	 parts         :=
	   [ Handgelenk       :=
	       dialog('Handgelenk'),
	     Flex_____Aktiv   :=
	       slider('flex.    aktiv', 0, 90, 0),
	     Ext______aktiv   :=
	       slider('ext.     aktiv', 0, 90, 0),
	     Pronation_Aktiv  :=
	       slider('pronation aktiv', 0, 90, 0),
	     Supination_Aktiv :=
	       slider('supination aktiv', 0, 90, 0),
	     Ulnar__Deviation :=
	       slider('Ulnar  Deviation', 0, 30, 0),
	     Radial_Deviation :=
	       slider('Radial Deviation', 0, 30, 0),
	     Instabilitaet    :=
	       menu('Instabilitaet', choice)
	   ],
	 modifications :=
	   [ Flex_____Aktiv   :=
	       [ label := 'Flex.    aktiv    :',
		 width := 194
	       ],
	     Ext______aktiv   :=
	       [ label := 'Ext.     aktiv    :',
		 width := 192
	       ],
	     Pronation_Aktiv  :=
	       [ width := 191
	       ],
	     Supination_Aktiv :=
	       [ width := 187
	       ],
	     Ulnar__Deviation :=
	       [ width := 192
	       ],
	     Radial_Deviation :=
	       [ width := 187
	       ],
	     Instabilitaet    :=
	       [ multiple_selection :=
		   @on,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item(dorsal, @default, 'Dorsal', @off, @nil, d),
		     menu_item(volar, @default, 'Volar', @off, @nil, v)
		   ]
	       ]
	   ],
	 layout        :=
	   [ below(Ext______aktiv, Flex_____Aktiv),
	     below(Pronation_Aktiv, Ext______aktiv),
	     below(Supination_Aktiv, Pronation_Aktiv),
	     below(Ulnar__Deviation, Supination_Aktiv),
	     below(Radial_Deviation, Ulnar__Deviation),
	     below(Instabilitaet, Radial_Deviation)
	   ]
       ]).



dialog(allgemeinbogen,
       [ object        :=
	   Allgemein_Untersuchung,
	 parts         :=
	   [ Allgemein_Untersuchung :=
	       dialog('Allgemein Untersuchung'),
	     Patient_Code           :=
	       text_item(patient_code),
	     Behandelnder_Arzt      :=
	       text_item(behandelnder_arzt),
	     Datum                  :=
	       text_item(datum),
	     Erst_Folge             :=
	       menu(erst_folge, choice)
	   ],
	 modifications :=
	   [ Patient_Code      :=
	       [ length := 39
	       ],
	     Behandelnder_Arzt :=
	       [ length := 39
	       ],
	     Datum             :=
	       [ length := 39
	       ],
	     Erst_Folge        :=
	       [ show_label :=
		   @off,
		 reference  :=
		   point(0, 13),
		 append     :=
		   [ menu_item(erstvorstellung,
			       @default,
			       'Erstvorstellung',
			       @off,
			       @nil,
			       e),
		     menu_item(folgeuntersuchung,
			       @default,
			       'Folgeuntersuchung',
			       @off,
			       @nil,
			       f)
		   ]
	       ]
	   ],
	 layout        :=
	   [ below(Behandelnder_Arzt, Patient_Code),
	     below(Datum, Behandelnder_Arzt),
	     below(Erst_Folge, Datum)
	   ]
       ]).




dialog(allgemein_bewegungsbefund_untere_extremitaet,
       [ object        :=
	   Bewegungsbefunde_Unt_Extr,
	 parts         :=
	   [ Bewegungsbefunde_Unt_Extr :=
	       dialog('Bewegungsbefunde Untere Extremit�t'),
	     Knie_H_Fte                :=
	       label(knie_h�fte, 'Knie H�fte'),
	     Flexion_Re                :=
	       slider(flexion_re, 0, 150, 0),
	     Flexion_Li                :=
	       slider(flexion_li, 0, 150, 0),
	     Extension_Re              :=
	       slider(extension_re, 0, 20, 0),
	     Extension_Li              :=
	       slider(extension_li, 0, 20, 0),
	     Knie_Gebeugte_H_Fte       :=
	       label(knie_gebeugte_h�fte,
		     'Knie Extension bei gebeugter H�fte'),
	     Ext_Geb_Re                :=
	       slider(ext_geb_re, 0, 90, 0),
	     Ext_Geb_Li                :=
	       slider(ext_geb_li, 0, 90, 0),
	     Osg_Bei_Gebeugtem_Knie    :=
	       label(osg_bei_gebeugtem_knie, 'OSG bei gebeugtem Knie'),
	     Osg_Flex_Geb_Re           :=
	       slider(osg_flex_geb_re, 0, 50, 0),
	     Osg_Flex_Geb_Li           :=
	       slider(osg_flex_geb_li, 0, 50, 0),
	     Osg_Ext_Geb_Re            :=
	       slider(osg_ext_geb_re, 0, 20, 0),
	     Osg_Ext_Geb_Li            :=
	       slider(osg_ext_geb_li, 0, 20, 0),
	     Osg_Bei_Gestrecktem_Knie  :=
	       label(osg_bei_gestrecktem_knie, 'OSG bei gestrecktem Knie'),
	     Osg_Flex_Gest_Re          :=
	       slider(osg_flex_gest_re, 0, 50, 0),
	     Osg_Flex_Gest_Li          :=
	       slider(osg_flex_gest_li, 0, 50, 0),
	     Osg_Ext_Gest_Re           :=
	       slider(osg_ext_gest_re, 0, 20, 0),
	     Osg_Ext_Gest_Li           :=
	       slider(osg_ext_gest_li, 0, 20, 0),
	     Usg                       :=
	       label(usg, 'USG Evers./Invers.'),
	     Evers_Re                  :=
	       slider(evers_re, 0, 20, 0),
	     Evers_Li                  :=
	       slider(evers_li, 0, 20, 0),
	     Invers_Re                 :=
	       slider(invers_re, 0, 30, 0),
	     Invers_Li                 :=
	       slider(invers_li, 0, 30, 0)
	   ],
	 modifications :=
	   [ Ext_Geb_Re       :=
	       [ label := 'Extension Re:'
	       ],
	     Ext_Geb_Li       :=
	       [ label     := 'Extension Li:',
		 alignment := right
	       ],
	     Osg_Flex_Geb_Re  :=
	       [ label := 'Flexion Re:'
	       ],
	     Osg_Flex_Geb_Li  :=
	       [ label := 'Flexion Li:'
	       ],
	     Osg_Ext_Geb_Re   :=
	       [ label := 'Extension Re:'
	       ],
	     Osg_Ext_Geb_Li   :=
	       [ label := 'Extension Li:'
	       ],
	     Osg_Flex_Gest_Re :=
	       [ label := 'Flexion Re:'
	       ],
	     Osg_Flex_Gest_Li :=
	       [ label := 'Flexion Li:'
	       ],
	     Osg_Ext_Gest_Re  :=
	       [ label := 'Extension Re:'
	       ],
	     Osg_Ext_Gest_Li  :=
	       [ label := 'Extension Li:'
	       ],
	     Evers_Re         :=
	       [ label := 'Evers. Re:'
	       ],
	     Evers_Li         :=
	       [ label := 'Evers. Li:'
	       ],
	     Invers_Re        :=
	       [ label := 'Invers. Re:'
	       ],
	     Invers_Li        :=
	       [ label := 'Invers. Li:'
	       ]
	   ],
	 layout        :=
	   [ below(Flexion_Re, Knie_H_Fte),
	     right(Flexion_Li, Flexion_Re),
	     below(Extension_Re, Flexion_Re),
	     below(Extension_Li, Flexion_Li),
	     right(Extension_Li, Extension_Re),
	     below(Knie_Gebeugte_H_Fte, Extension_Re),
	     below(Ext_Geb_Re, Knie_Gebeugte_H_Fte),
	     right(Ext_Geb_Li, Ext_Geb_Re),
	     below(Osg_Bei_Gebeugtem_Knie, Ext_Geb_Re),
	     below(Osg_Flex_Geb_Re, Osg_Bei_Gebeugtem_Knie),
	     right(Osg_Flex_Geb_Li, Osg_Flex_Geb_Re),
	     below(Osg_Ext_Geb_Re, Osg_Flex_Geb_Re),
	     below(Osg_Ext_Geb_Li, Osg_Flex_Geb_Li),
	     right(Osg_Ext_Geb_Li, Osg_Ext_Geb_Re),
	     below(Osg_Bei_Gestrecktem_Knie, Osg_Ext_Geb_Re),
	     below(Osg_Flex_Gest_Re, Osg_Bei_Gestrecktem_Knie),
	     right(Osg_Flex_Gest_Li, Osg_Flex_Gest_Re),
	     below(Osg_Ext_Gest_Re, Osg_Flex_Gest_Re),
	     below(Osg_Ext_Gest_Li, Osg_Flex_Gest_Li),
	     right(Osg_Ext_Gest_Li, Osg_Ext_Gest_Re),
	     below(Usg, Osg_Ext_Gest_Re),
	     below(Evers_Re, Usg),
	     right(Evers_Li, Evers_Re),
	     below(Invers_Re, Evers_Re),
	     below(Invers_Li, Evers_Li),
	     right(Invers_Li, Invers_Re)
	   ]
       ]).



dialog(allgemein_diagnose,
       [ object        :=
	   Vorl_Diagnose,
	 parts         :=
	   [ Vorl_Diagnose       :=
	       dialog('Vorl�ufige Diagnose'),
	     Vorl_Ufige_Diagnose :=
	       text_item(vorl�ufige_diagnose),
	     Weitere_Abkl_Rungen :=
	       menu(weitere_abkl�rungen, toggle),
	     Sonstiges           :=
	       text_item(sonstiges),
	     Spezialbogen        :=
	       menu(spezialbogen, toggle),
	     Sonstige            :=
	       text_item(sonstige)
	   ],
	 modifications :=
	   [ Vorl_Ufige_Diagnose :=
	       [ label  := 'Vorl�ufige Diagnose (= 1. Arbeitshypothese):',
		 length := 30
	       ],
	     Weitere_Abkl_Rungen :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(mri, @default, 'MRI', @off, @nil, m),
				menu_item(szinti, @default, 'SZINTI', @off, @nil, s),
				menu_item(ct, @default, 'CT', @off, @nil, c),
				menu_item(tomo, @default, 'TOMO', @off, @nil, t),
				menu_item(keine, @default, 'Keine', @off, @nil, k)
			      ],
		 alignment := left
	       ],
	     Sonstiges           :=
	       [ length := 26
	       ],
	     Spezialbogen        :=
	       [ columns   := 2,
		 reference := point(0, 13),
		 append    := [ menu_item(ws, @default, 'WS', @off, @nil, w),
				menu_item(neuro, @default, 'Neuro', @off, @nil, n),
				menu_item(h�fte, @default, 'H�fte', @off, @nil, h),
				menu_item(fu�, @default, 'Fu�', @off, @nil, f),
				menu_item(obere_e,
					  @default,
					  'Obere E',
					  @off,
					  @nil,
					  o),
				menu_item(l�ngendiff,
					  @default,
					  'L�ngendiff',
					  @off,
					  @nil,
					  l),
				menu_item(knie, @default, 'Knie', @off, @nil, k),
				menu_item(tumoren,
					  @default,
					  'Tumoren',
					  @off,
					  @nil,
					  t),
				menu_item(sonstige,
					  @default,
					  'Sonstige',
					  @off,
					  @nil,
					  s),
				menu_item(keiner, @default, 'Keiner', @off, @nil, e)
			      ],
		 alignment := left
	       ],
	     Sonstige            :=
	       [ length := 26
	       ]
	   ],
	 layout        :=
	   [ below(Weitere_Abkl_Rungen, Vorl_Ufige_Diagnose),
	     right(Sonstiges, Weitere_Abkl_Rungen),
	     below(Spezialbogen, Weitere_Abkl_Rungen),
	     below(Sonstige, Sonstiges),
	     right(Sonstige, Spezialbogen)
	   ]
       ]).




dialog(allgemein_bewegungsbefund_huefte,
       [ object        :=
	   Bewegungsbefunde_H_Fte,
	 parts         :=
	   [ Bewegungsbefunde_H_Fte :=
	       dialog('Bewegungsbefunde H�fte'),
	     Flexion_Re             :=
	       slider(flexion_re, 0, 100, 0),
	     Flexion_Li             :=
	       slider(flexion_li, 0, 100, 0),
	     Extension_Re           :=
	       slider(extension_re, 0, 30, 0),
	     Extension_Li           :=
	       slider(extension_li, 0, 30, 0),
	     Abduktion_Re           :=
	       slider(abduktion_re, 0, 90, 0),
	     Abduktion_Li           :=
	       slider(abduktion_li, 0, 90, 0),
	     Adduktion_Re           :=
	       slider(adduktion_re, 0, 90, 0),
	     Adduktion_Li           :=
	       slider(adduktion_li, 0, 90, 0),
	     Ir_Re                  :=
	       slider(ir_re, 0, 90, 0),
	     Ir_Li                  :=
	       slider(ir_li, 0, 90, 0),
	     Ar_Re                  :=
	       slider(ar_re, 0, 90, 0),
	     Ar_Li                  :=
	       slider(ar_li, 0, 90, 0),
	     At_Re                  :=
	       slider(at_re, 0, 50, 0),
	     At_Li                  :=
	       slider(at_li, 0, 50, 0)
	   ],
	 modifications :=
	   [ Ir_Re := [ label := 'IR Re:'
		      ],
	     Ir_Li := [ label := 'IR Li:'
		      ],
	     Ar_Re := [ label := 'AR Re:'
		      ],
	     Ar_Li := [ label := 'AR Li:'
		      ],
	     At_Re := [ label := 'AT Re:'
		      ],
	     At_Li := [ label := 'AT Li:'
		      ]
	   ],
	 layout        :=
	   [ right(Flexion_Li, Flexion_Re),
	     below(Extension_Re, Flexion_Re),
	     below(Extension_Li, Flexion_Li),
	     right(Extension_Li, Extension_Re),
	     below(Abduktion_Re, Extension_Re),
	     below(Abduktion_Li, Extension_Li),
	     right(Abduktion_Li, Abduktion_Re),
	     below(Adduktion_Re, Abduktion_Re),
	     below(Adduktion_Li, Abduktion_Li),
	     right(Adduktion_Li, Adduktion_Re),
	     below(Ir_Re, Adduktion_Re),
	     below(Ir_Li, Adduktion_Li),
	     right(Ir_Li, Ir_Re),
	     below(Ar_Re, Ir_Re),
	     below(Ar_Li, Ir_Li),
	     right(Ar_Li, Ar_Re),
	     below(At_Re, Ar_Re),
	     below(At_Li, Ar_Li),
	     right(At_Li, At_Re)
	   ]
       ]).





dialog(allgemein_bewegungsbefund_ellbogen,
       [ object        :=
	   Bewegungsbefunde_Ellbogen,
	 parts         :=
	   [ Bewegungsbefunde_Ellbogen :=
	       dialog('Bewegungsbefunde Ellbogen'),
	     Flexion_Re                :=
	       slider(flexion_re, 0, 170, 0),
	     Flexion_Li                :=
	       slider(flexion_li, 0, 170, 0),
	     Extension_Re              :=
	       slider(extension_re, 0, 30, 0),
	     Extension_Li              :=
	       slider(extension_li, 0, 30, 0),
	     Pronation_Re              :=
	       slider(pronation_re, 0, 90, 0),
	     Pronation_Li              :=
	       slider(pronation_li, 0, 90, 0),
	     Supination_Re             :=
	       slider(supination_re, 0, 90, 0),
	     Supination_Li             :=
	       slider(supination_li, 0, 90, 0)
	   ],
	 modifications :=
	   [],
	 layout        :=
	   [ right(Flexion_Li, Flexion_Re),
	     below(Extension_Re, Flexion_Re),
	     below(Extension_Li, Flexion_Li),
	     right(Extension_Li, Extension_Re),
	     below(Pronation_Re, Extension_Re),
	     below(Pronation_Li, Extension_Li),
	     right(Pronation_Li, Pronation_Re),
	     below(Supination_Re, Pronation_Re),
	     below(Supination_Li, Pronation_Li),
	     right(Supination_Li, Supination_Re)
	   ]
       ]).


dialog(allgemein_bewegungsbefund_schulter,
       [ object        :=
	   Bewegungsbefunde_Schulter,
	 parts         :=
	   [ Bewegungsbefunde_Schulter :=
	       dialog('Bewegungsbefunde Schulter'),
	     Ir___Re                   :=
	       slider(ir_0_re, 0, 90, 0),
	     Ir___Li                   :=
	       slider(ir_0_li, 0, 90, 0),
	     Ir____re                  :=
	       slider(ir_90_re, 0, 90, 0),
	     Ir____li                  :=
	       slider(ir_90_li, 0, 90, 0),
	     Ar___Re                   :=
	       slider(ar_0_re, 0, 90, 0),
	     Ar___Li                   :=
	       slider(ar_0_li, 0, 90, 0),
	     Ar____re                  :=
	       slider(ar_90_re, 0, 90, 0),
	     Ar____li                  :=
	       slider(ar_90_li, 0, 90, 0),
	     Vorheben_Re               :=
	       slider(vorheben_re, 0, 170, 0),
	     Vorheben_Li               :=
	       slider(vorheben_li, 0, 170, 0),
	     Seitheben_Re              :=
	       slider(seitheben_re, 0, 170, 0),
	     Seitheben_Li              :=
	       slider(seitheben_li, 0, 170, 0),
	     R_Ckheben_Re              :=
	       slider(r�ckheben_re, 0, 170, 0),
	     R_Ckheben_Li              :=
	       slider(r�ckheben_li, 0, 170, 0),
	     Hinterhauptsgriff_Re      :=
	       menu(hinterhauptsgriff_re, cycle),
	     Hinterhauptsgriff_Li      :=
	       menu(hinterhauptsgriff_li, cycle),
	     Sch_Rzengriff_Re          :=
	       menu(sch�rzengriff_re, cycle),
	     Sch_Rzengriff_Li          :=
	       menu(sch�rzengriff_li, cycle)
	   ],
	 modifications :=
	   [ Ir___Re              :=
	       [ label := 'IR 0� Re:'
	       ],
	     Ir___Li              :=
	       [ label := 'IR 0� Li:'
	       ],
	     Ir____re             :=
	       [ label := 'IR 90� Re:'
	       ],
	     Ir____li             :=
	       [ label := 'IR 90� Li:'
	       ],
	     Ar___Re              :=
	       [ label := 'AR 0� Re:'
	       ],
	     Ar___Li              :=
	       [ label := 'AR 0� Li:'
	       ],
	     Ar____re             :=
	       [ label := 'AR 90� Re:'
	       ],
	     Ar____li             :=
	       [ label := 'AR 90� Li:'
	       ],
	     Hinterhauptsgriff_Re :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(frei, @default, 'Frei', @off, @nil, f),
				menu_item(eingeschr�nkt,
					  @default,
					  'Eingeschr�nkt',
					  @off,
					  @nil,
					  e)
			      ]
	       ],
	     Hinterhauptsgriff_Li :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(frei, @default, 'Frei', @off, @nil, f),
				menu_item(eingeschr�nkt,
					  @default,
					  'Eingeschr�nkt',
					  @off,
					  @nil,
					  e)
			      ]
	       ],
	     Sch_Rzengriff_Re     :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(frei, @default, 'Frei', @off, @nil, f),
				menu_item(eingeschr�nkt,
					  @default,
					  'Eingeschr�nkt',
					  @off,
					  @nil,
					  e)
			      ]
	       ],
	     Sch_Rzengriff_Li     :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(frei, @default, 'Frei', @off, @nil, f),
				menu_item(eingeschr�nkt,
					  @default,
					  'Eingeschr�nkt',
					  @off,
					  @nil,
					  e)
			      ]
	       ]
	   ],
	 layout        :=
	   [ right(Ir___Li, Ir___Re),
	     below(Ir____re, Ir___Re),
	     below(Ir____li, Ir___Li),
	     right(Ir____li, Ir____re),
	     below(Ar___Re, Ir____re),
	     below(Ar___Li, Ir____li),
	     right(Ar___Li, Ar___Re),
	     below(Ar____re, Ar___Re),
	     below(Ar____li, Ar___Li),
	     right(Ar____li, Ar____re),
	     below(Vorheben_Re, Ar____re),
	     below(Vorheben_Li, Ar____li),
	     right(Vorheben_Li, Vorheben_Re),
	     below(Seitheben_Re, Vorheben_Re),
	     below(Seitheben_Li, Vorheben_Li),
	     right(Seitheben_Li, Seitheben_Re),
	     below(R_Ckheben_Re, Seitheben_Re),
	     below(R_Ckheben_Li, Seitheben_Li),
	     right(R_Ckheben_Li, R_Ckheben_Re),
	     below(Hinterhauptsgriff_Re, R_Ckheben_Re),
	     right(Hinterhauptsgriff_Li, Hinterhauptsgriff_Re),
	     below(Sch_Rzengriff_Re, Hinterhauptsgriff_Re),
	     below(Sch_Rzengriff_Li, Hinterhauptsgriff_Li),
	     right(Sch_Rzengriff_Li, Sch_Rzengriff_Re)
	   ]
       ]).


dialog(allgemein_neurologie,
       [ object        :=
	   Neurologie_Gef_Sse,
	 parts         :=
	   [ Neurologie_Gef_Sse :=
	       dialog('Neurologie und Gef�sse'),
	     Biceps_Sr_Re       :=
	       menu(biceps_sr_re, cycle),
	     Biceps_Sr_Li       :=
	       menu(biceps_sr_li, cycle),
	     Rediuperisost_R_Re :=
	       menu(rediuperisost_r_re, cycle),
	     Radiuperisost_R_Li :=
	       menu(radiuperisost_r_li, cycle),
	     Psr_Re             :=
	       menu(psr_re, cycle),
	     Psr_Li             :=
	       menu(psr_li, cycle),
	     Asr_Re             :=
	       menu(asr_re, cycle),
	     Asr_Li             :=
	       menu(asr_li, cycle),
	     Babinsky           :=
	       menu(babinsky, cycle),
	     Lasegue            :=
	       menu(lasegue, cycle),
	     Lasegue_Slider     :=
	       slider(lasegue_slider, 0, 100, 0),
	     Sensibilit_T       :=
	       menu(sensibilit�t, cycle),
	     Welcher_Nerv       :=
	       text_item(welcher_nerv),
	     Peripherie_Hand_Re :=
	       menu(peripherie_hand_re, cycle),
	     Peripherie_Hand_Li :=
	       menu(peripherie_hand_li, cycle),
	     Peripherie_Fu__re  :=
	       menu(peripherie_fu�_re, cycle),
	     Peripherie_Fu__li  :=
	       menu(peripherie_fu�_li, cycle)
	   ],
	 modifications :=
	   [ Biceps_Sr_Re       :=
	       [ label     := 'BicepsSR Re:',
		 reference := point(0, 13),
		 append    := [ menu_item(normal_ausl�sbar,
					  @default,
					  'Normal Ausl�sbar',
					  @off,
					  @nil,
					  n),
				menu_item(abgeschw�cht,
					  @default,
					  'Abgeschw�cht',
					  @off,
					  @nil,
					  a),
				menu_item(gesteigert,
					  @default,
					  'Gesteigert',
					  @off,
					  @nil,
					  g),
				menu_item(nicht_ausl�sbar,
					  @default,
					  'Nicht Ausl�sbar',
					  @off,
					  @nil,
					  i)
			      ]
	       ],
	     Biceps_Sr_Li       :=
	       [ label     := 'BicepsSR Li:',
		 reference := point(0, 13),
		 append    := [ menu_item(normal_ausl�sbar,
					  @default,
					  'Normal Ausl�sbar',
					  @off,
					  @nil,
					  n),
				menu_item(abgeschw�cht,
					  @default,
					  'Abgeschw�cht',
					  @off,
					  @nil,
					  a),
				menu_item(gesteigert,
					  @default,
					  'Gesteigert',
					  @off,
					  @nil,
					  g),
				menu_item(nicht_ausl�sbar,
					  @default,
					  'Nicht Ausl�sbar',
					  @off,
					  @nil,
					  i)
			      ]
	       ],
	     Rediuperisost_R_Re :=
	       [ label     := 'RediuperisostR Re:',
		 reference := point(0, 13),
		 append    := [ menu_item(normal_ausl�sbar,
					  @default,
					  'Normal Ausl�sbar',
					  @off,
					  @nil,
					  n),
				menu_item(abgeschw�cht,
					  @default,
					  'Abgeschw�cht',
					  @off,
					  @nil,
					  a),
				menu_item(gesteigert,
					  @default,
					  'Gesteigert',
					  @off,
					  @nil,
					  g),
				menu_item(nicht_ausl�sbar,
					  @default,
					  'Nicht Ausl�sbar',
					  @off,
					  @nil,
					  i)
			      ]
	       ],
	     Radiuperisost_R_Li :=
	       [ label     := 'RadiuperisostR Li:',
		 reference := point(0, 13),
		 append    := [ menu_item(normal_ausl�sbar,
					  @default,
					  'Normal Ausl�sbar',
					  @off,
					  @nil,
					  n),
				menu_item(abgeschw�cht,
					  @default,
					  'Abgeschw�cht',
					  @off,
					  @nil,
					  a),
				menu_item(gesteigert,
					  @default,
					  'Gesteigert',
					  @off,
					  @nil,
					  g),
				menu_item(nicht_ausl�sbar,
					  @default,
					  'Nicht Ausl�sbar',
					  @off,
					  @nil,
					  i)
			      ]
	       ],
	     Psr_Re             :=
	       [ label     := 'PSR Re:',
		 reference := point(0, 13),
		 append    := [ menu_item(normal_ausl�sbar,
					  @default,
					  'Normal Ausl�sbar',
					  @off,
					  @nil,
					  n),
				menu_item(abgeschw�cht,
					  @default,
					  'Abgeschw�cht',
					  @off,
					  @nil,
					  a),
				menu_item(gesteigert,
					  @default,
					  'Gesteigert',
					  @off,
					  @nil,
					  g),
				menu_item(nicht_ausl�sbar,
					  @default,
					  'Nicht Ausl�sbar',
					  @off,
					  @nil,
					  i)
			      ]
	       ],
	     Psr_Li             :=
	       [ label     := 'PSR Li:',
		 reference := point(0, 13),
		 append    := [ menu_item(normal_ausl�sbar,
					  @default,
					  'Normal Ausl�sbar',
					  @off,
					  @nil,
					  n),
				menu_item(abgeschw�cht,
					  @default,
					  'Abgeschw�cht',
					  @off,
					  @nil,
					  a),
				menu_item(gesteigert,
					  @default,
					  'Gesteigert',
					  @off,
					  @nil,
					  g),
				menu_item(nicht_ausl�sbar,
					  @default,
					  'Nicht Ausl�sbar',
					  @off,
					  @nil,
					  i)
			      ]
	       ],
	     Asr_Re             :=
	       [ label     := 'ASR Re:',
		 reference := point(0, 13),
		 append    := [ menu_item(normal_ausl�sbar,
					  @default,
					  'Normal Ausl�sbar',
					  @off,
					  @nil,
					  n),
				menu_item(abgeschw�cht,
					  @default,
					  'Abgeschw�cht',
					  @off,
					  @nil,
					  a),
				menu_item(gesteigert,
					  @default,
					  'Gesteigert',
					  @off,
					  @nil,
					  g),
				menu_item(nicht_ausl�sbar,
					  @default,
					  'Nicht Ausl�sbar',
					  @off,
					  @nil,
					  i)
			      ]
	       ],
	     Asr_Li             :=
	       [ label     := 'ASR Li:',
		 reference := point(0, 13),
		 append    := [ menu_item(normal_ausl�sbar,
					  @default,
					  'Normal Ausl�sbar',
					  @off,
					  @nil,
					  n),
				menu_item(abgeschw�cht,
					  @default,
					  'Abgeschw�cht',
					  @off,
					  @nil,
					  a),
				menu_item(gesteigert,
					  @default,
					  'Gesteigert',
					  @off,
					  @nil,
					  g),
				menu_item(nicht_ausl�sbar,
					  @default,
					  'Nicht Ausl�sbar',
					  @off,
					  @nil,
					  i)
			      ]
	       ],
	     Babinsky           :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(negativ,
					  @default,
					  'Negativ',
					  @off,
					  @nil,
					  n),
				menu_item(positiv,
					  @default,
					  'Positiv',
					  @off,
					  @nil,
					  p)
			      ]
	       ],
	     Lasegue            :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(negativ,
					  @default,
					  'Negativ',
					  @off,
					  @nil,
					  n),
				menu_item(positiv,
					  @default,
					  'Positiv',
					  @off,
					  @nil,
					  p)
			      ]
	       ],
	     Lasegue_Slider     :=
	       [ show_label := @off
	       ],
	     Sensibilit_T       :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(normal, @default, 'Normal', @off, @nil, o),
				menu_item(gest�rt,
					  @default,
					  'Gest�rt',
					  @off,
					  @nil,
					  g)
			      ]
	       ],
	     Welcher_Nerv       :=
	       [ label  := 'welcher Nerv:',
		 length := 26
	       ],
	     Peripherie_Hand_Re :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(normal, @default, 'Normal', @off, @nil, o),
				menu_item(gest�rt,
					  @default,
					  'Gest�rt',
					  @off,
					  @nil,
					  g)
			      ]
	       ],
	     Peripherie_Hand_Li :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(normal, @default, 'Normal', @off, @nil, o),
				menu_item(gest�rt,
					  @default,
					  'Gest�rt',
					  @off,
					  @nil,
					  g)
			      ]
	       ],
	     Peripherie_Fu__re  :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(normal, @default, 'Normal', @off, @nil, o),
				menu_item(gest�rt,
					  @default,
					  'Gest�rt',
					  @off,
					  @nil,
					  g)
			      ]
	       ],
	     Peripherie_Fu__li  :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(normal, @default, 'Normal', @off, @nil, o),
				menu_item(gest�rt,
					  @default,
					  'Gest�rt',
					  @off,
					  @nil,
					  g)
			      ]
	       ]
	   ],
	 layout        :=
	   [ right(Biceps_Sr_Li, Biceps_Sr_Re),
	     below(Rediuperisost_R_Re, Biceps_Sr_Re),
	     below(Radiuperisost_R_Li, Biceps_Sr_Li),
	     right(Radiuperisost_R_Li, Rediuperisost_R_Re),
	     below(Psr_Re, Rediuperisost_R_Re),
	     below(Psr_Li, Radiuperisost_R_Li),
	     right(Psr_Li, Psr_Re),
	     below(Asr_Re, Psr_Re),
	     below(Asr_Li, Psr_Li),
	     right(Asr_Li, Asr_Re),
	     below(Babinsky, Asr_Re),
	     below(Lasegue, Babinsky),
	     right(Lasegue_Slider, Lasegue),
	     below(Sensibilit_T, Lasegue),
	     below(Welcher_Nerv, Lasegue_Slider),
	     right(Welcher_Nerv, Sensibilit_T),
	     below(Peripherie_Hand_Re, Sensibilit_T),
	     right(Peripherie_Hand_Li, Peripherie_Hand_Re),
	     below(Peripherie_Fu__re, Peripherie_Hand_Re),
	     below(Peripherie_Fu__li, Peripherie_Hand_Li),
	     right(Peripherie_Fu__li, Peripherie_Fu__re)
	   ]
       ]).


dialog(allgemein_anamnese,
       [ object        :=
	   Allgemein,
	 parts         :=
	   [ Allgemein           :=
	       dialog('Allgemein'),
	     Familienanamnese    :=
	       label(familienanamnese, 'Familienanamnese:'),
	     Deformitaeten       :=
	       menu(deformitaeten, toggle),
	     Generation          :=
	       menu(generation, toggle),
	     Eigenanamnese       :=
	       label(eigenanamnese, 'Eigenanamnese:'),
	     Geburt              :=
	       menu(geburt, choice),
	     Termin              :=
	       menu(termin, choice),
	     Wochen              :=
	       slider(wochen, 0, 20, 0),
	     Sitzbeginn          :=
	       slider(sitzbeginn, 0, 60, 0),
	     Monaten__1          :=
	       label(monaten_1, 'Monaten'),
	     Stehbeginn          :=
	       slider(stehbeginn, 0, 60, 0),
	     Monate__            :=
	       label(monate_2, 'Monaten'),
	     Gehbeginn           :=
	       slider(gehbeginn, 0, 60, 0),
	     Monaten__2          :=
	       label(monaten_3, 'Monaten'),
	     Jetzige_Beschwerden :=
	       label(jetzige_beschwerden, 'Jetzige Beschwerden:'),
	     Grund               :=
	       menu(grund, cycle),
	     Veranlassung        :=
	       menu(veranlassung, cycle)
	   ],
	 modifications :=
	   [ Familienanamnese    :=
	       [ font := @helvetica_roman_14
	       ],
	     Deformitaeten       :=
	       [ show_label :=
		   @off,
		 reference  :=
		   point(0, 13),
		 append     :=
		   [ menu_item(hueftdysplasie,
			       @default,
			       'H�ftdysplasie',
			       @off,
			       @nil,
			       h),
		     menu_item(ws_deformitaeten,
			       @default,
			       'WS Deformit�ten',
			       @off,
			       @nil,
			       w),
		     menu_item(fussdeformitaet,
			       @default,
			       'Fussdeformit�t',
			       @off,
			       @nil,
			       f),
		     menu_item(keine, @default, keine, @off, @nil, k)
		   ]
	       ],
	     Generation          :=
	       [ show_label :=
		   @off,
		 reference  :=
		   point(0, 13),
		 append     :=
		   [ menu_item(elterngeneration,
			       @default,
			       'Elterngeneration',
			       @off,
			       @nil,
			       e),
		     menu_item(grosselterngeneration,
			       @default,
			       'Grosselterngeneration',
			       @off,
			       @nil,
			       g),
		     menu_item(sonstige, @default, 'Sonstige', @off, @nil, s)
		   ]
	       ],
	     Eigenanamnese       :=
	       [ font := @helvetica_roman_14
	       ],
	     Geburt              :=
	       [ show_label :=
		   @off,
		 reference  :=
		   point(0, 13),
		 append     :=
		   [ menu_item(geburt_normal,
			       @default,
			       'Geburt Normal',
			       @off,
			       @nil,
			       g),
		     menu_item(steissgeburt,
			       @default,
			       'Steissgeburt',
			       @off,
			       @nil,
			       s)
		   ]
	       ],
	     Termin              :=
	       [ show_label :=
		   @off,
		 reference  :=
		   point(0, 13),
		 append     :=
		   [ menu_item(geburtstermin_normal,
			       @default,
			       'Geburtstermin Normal',
			       @off,
			       @nil,
			       g),
		     menu_item(zu_fr�h, @default, 'Zu Fr�h', @off, @nil, z)
		   ]
	       ],
	     Wochen              :=
	       [ alignment := right
	       ],
	     Sitzbeginn          :=
	       [ label     := 'Sitzbeginn mit:',
		 alignment := left
	       ],
	     Monaten__2          :=
	       [ font      := @helvetica_bold_12,
		 alignment := left
	       ],
	     Stehbeginn          :=
	       [ label     := 'Stehbeginn mit:',
		 alignment := left
	       ],
	     Monate__            :=
	       [ font      := @helvetica_bold_12,
		 alignment := left
	       ],
	     Gehbeginn           :=
	       [ label     := 'Gehbeginn mit:',
		 alignment := left
	       ],
	     Monaten__1          :=
	       [ font      := @helvetica_bold_12,
		 alignment := left
	       ],
	     Jetzige_Beschwerden :=
	       [ font := @helvetica_roman_14
	       ],
	     Grund               :=
	       [ label     := 'Grund des Kommens:',
		 reference := point(0, 13),
		 append    := [ menu_item(schmerzen,
					  @default,
					  'Schmerzen',
					  @off,
					  @nil,
					  s),
				menu_item(hinken, @default, 'Hinken', @off, @nil, h),
				menu_item(ganganomalie,
					  @default,
					  'Ganganomalie',
					  @off,
					  @nil,
					  g),
				menu_item(fussanomalie,
					  @default,
					  'Fussanomalie',
					  @off,
					  @nil,
					  f),
				menu_item(deformit�t,
					  @default,
					  'Deformit�t',
					  @off,
					  @nil,
					  d),
				menu_item(haltung,
					  @default,
					  'Haltung',
					  @off,
					  @nil,
					  a)
			      ]
	       ],
	     Veranlassung        :=
	       [ label     := 'auf Veranlassung von:',
		 reference := point(0, 13),
		 append    := [ menu_item(eltern, @default, 'Eltern', @off, @nil, e),
				menu_item(grosseltern,
					  @default,
					  'Grosseltern',
					  @off,
					  @nil,
					  g),
				menu_item(lehrer, @default, 'Lehrer', @off, @nil, l),
				menu_item(nachbar,
					  @default,
					  'Nachbar',
					  @off,
					  @nil,
					  n),
				menu_item(andere, @default, 'Andere', @off, @nil, a)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Deformitaeten, Familienanamnese),
	     below(Generation, Deformitaeten),
	     below(Eigenanamnese, Generation),
	     below(Geburt, Eigenanamnese),
	     below(Termin, Geburt),
	     right(Wochen, Termin),
	     below(Sitzbeginn, Termin),
	     right(Monaten__2, Sitzbeginn),
	     below(Stehbeginn, Sitzbeginn),
	     right(Monate__, Stehbeginn),
	     below(Gehbeginn, Stehbeginn),
	     right(Monaten__1, Gehbeginn),
	     below(Jetzige_Beschwerden, Gehbeginn),
	     below(Grund, Jetzige_Beschwerden),
	     below(Veranlassung, Grund)
	   ]
       ]).


/*

dialog(allgemein_anamnese,
       [ object        :=
	   Allgemein,
	 parts         :=
	   [ Allgemein           :=
	       dialog('Allgemein'),
	     Familienanamnese    :=
	       label(familienanamnese, 'Familienanamnese:'),
	     Toggle_1            :=
	       menu(toggle1, toggle),
	     Toggle_2            :=
	       menu(toggle2, toggle),
	     Name1               :=
	       label(name, 'Eigenanamnese:'),
	     Choice_1            :=
	       menu(choice1, choice),
	     Choice_2            :=
	       menu(choice2, choice),
	     Wochen              :=
	       slider(wochen, 0, 20, 0),
	     Sitzbeginn          :=
	       slider(sitzbeginn, 0, 60, 0),
	     Monaten1            :=
	       label(monaten, 'Monaten'),
	     Stehbeginn          :=
	       slider(stehbeginn, 0, 60, 0),
	     Name2               :=
	       label(name, 'Monaten'),
	     Gehbeginn           :=
	       slider(gehbeginn, 0, 60, 0),
	     Monaten2            :=
	       label(monaten, 'Monaten'),
	     Jetzige_Beschwerden :=
	       label(jetzige_beschwerden, 'Jetzige Beschwerden:'),
	     Grund               :=
	       menu(grund, cycle),
	     Veranlassung        :=
	       menu(veranlassung, cycle)
	   ],
	 modifications :=
	   [ Familienanamnese    :=
	       [ font := @helvetica_roman_14
	       ],
	     Toggle_2            :=
	       [ label      :=
		   '',
		 show_label :=
		   @off,
		 reference  :=
		   point(0, 13),
		 append     :=
		   [ menu_item(hueftdysplasie,
			       @default,
			       'H�ftdysplasie',
			       @off,
			       @nil,
			       h),
		     menu_item(ws_deformitaeten,
			       @default,
			       'WS Deformit�ten',
			       @off,
			       @nil,
			       w),
		     menu_item(fussdeformitaet,
			       @default,
			       'Fussdeformit�t',
			       @off,
			       @nil,
			       f),
		     menu_item(keine, @default, keine, @off, @nil, k)
		   ]
	       ],
	     Toggle_1            :=
	       [ show_label :=
		   @off,
		 reference  :=
		   point(0, 13),
		 append     :=
		   [ menu_item(elterngeneration,
			       @default,
			       'Elterngeneration',
			       @off,
			       @nil,
			       e),
		     menu_item(grosselterngeneration,
			       @default,
			       'Grosselterngeneration',
			       @off,
			       @nil,
			       g),
		     menu_item(sonstige, @default, 'Sonstige', @off, @nil, s)
		   ]
	       ],
	     Name2               :=
	       [ font := @helvetica_roman_14
	       ],
	     Choice_2            :=
	       [ show_label :=
		   @off,
		 reference  :=
		   point(0, 13),
		 append     :=
		   [ menu_item(geburt_normal,
			       @default,
			       'Geburt Normal',
			       @off,
			       @nil,
			       g),
		     menu_item(steissgeburt,
			       @default,
			       'Steissgeburt',
			       @off,
			       @nil,
			       s)
		   ]
	       ],
	     Choice_1            :=
	       [ show_label :=
		   @off,
		 reference  :=
		   point(0, 13),
		 append     :=
		   [ menu_item(geburtstermin_normal,
			       @default,
			       'Geburtstermin Normal',
			       @off,
			       @nil,
			       g),
		     menu_item(zu_fr�h, @default, 'Zu Fr�h', @off, @nil, z)
		   ]
	       ],
	     Wochen              :=
	       [ alignment := right
	       ],
	     Sitzbeginn          :=
	       [ label     := 'Sitzbeginn mit:',
		 alignment := left
	       ],
	     Monaten2            :=
	       [ font      := @helvetica_bold_12,
		 alignment := left
	       ],
	     Stehbeginn          :=
	       [ label     := 'Stehbeginn mit:',
		 alignment := left
	       ],
	     Name1               :=
	       [ font      := @helvetica_bold_12,
		 alignment := left
	       ],
	     Gehbeginn           :=
	       [ label     := 'Gehbeginn mit:',
		 alignment := left
	       ],
	     Monaten1            :=
	       [ font      := @helvetica_bold_12,
		 alignment := left
	       ],
	     Jetzige_Beschwerden :=
	       [ font := @helvetica_roman_14
	       ],
	     Grund               :=
	       [ label     := 'Grund des Kommens:',
		 reference := point(0, 13),
		 append    := [ menu_item(schmerzen,
					  @default,
					  'Schmerzen',
					  @off,
					  @nil,
					  s),
				menu_item(hinken, @default, 'Hinken', @off, @nil, h),
				menu_item(ganganomalie,
					  @default,
					  'Ganganomalie',
					  @off,
					  @nil,
					  g),
				menu_item(fussanomalie,
					  @default,
					  'Fussanomalie',
					  @off,
					  @nil,
					  f),
				menu_item(deformit�t,
					  @default,
					  'Deformit�t',
					  @off,
					  @nil,
					  d),
				menu_item(haltung,
					  @default,
					  'Haltung',
					  @off,
					  @nil,
					  a)
			      ]
	       ],
	     Veranlassung        :=
	       [ label     := 'auf Veranlassung von:',
		 reference := point(0, 13),
		 append    := [ menu_item(eltern, @default, 'Eltern', @off, @nil, e),
				menu_item(grosseltern,
					  @default,
					  'Grosseltern',
					  @off,
					  @nil,
					  g),
				menu_item(lehrer, @default, 'Lehrer', @off, @nil, l),
				menu_item(nachbar,
					  @default,
					  'Nachbar',
					  @off,
					  @nil,
					  n),
				menu_item(andere, @default, 'Andere', @off, @nil, a)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Toggle_2, Familienanamnese),
	     below(Toggle_1, Toggle_2),
	     below(Name2, Toggle_1),
	     below(Choice_2, Name2),
	     below(Choice_1, Choice_2),
	     right(Wochen, Choice_1),
	     below(Sitzbeginn, Choice_1),
	     right(Monaten2, Sitzbeginn),
	     below(Stehbeginn, Sitzbeginn),
	     right(Name1, Stehbeginn),
	     below(Gehbeginn, Stehbeginn),
	     right(Monaten1, Gehbeginn),
	     below(Jetzige_Beschwerden, Gehbeginn),
	     below(Grund, Jetzige_Beschwerden),
	     below(Veranlassung, Grund)
	   ]
       ]).


*/

dialog(allgemein_befund,
       [ object        :=
	   Allgemein_Befund,
	 parts         :=
	   [ Allgemein_Befund  :=
	       dialog('Allgemein Befund'),
	     Hand              :=
	       menu(hand, cycle),
	     Fu_               :=
	       menu(fu�, cycle),
	     Gewicht           :=
	       slider(gewicht, 0, 100, 0),
	     Gr__e             :=
	       slider(gr��e, 0, 200, 0),
	     Schultern         :=
	       menu(schultern, cycle),
	     Becken            :=
	       menu(becken, cycle),
	     Brettchen         :=
	       slider(brettchen, 0, 50, 0),
	     Obere_Extr_Re     :=
	       menu(obere_extr_re, cycle),
	     Obere_Extr_Li     :=
	       menu(obere_extr_li, cycle),
	     Ellbogenachsen_Re :=
	       menu(ellbogenachsen_re, cycle),
	     Ellbogenachsen_Li :=
	       menu(ellbogenachsen_li, cycle),
	     Untere_Extr_Re    :=
	       menu(untere_extr_re, cycle),
	     Untere_Extr_Li    :=
	       menu(untere_extr_li, cycle),
	     Knieachsen_Re     :=
	       menu(knieachsen_re, cycle),
	     Knieachsen_Li     :=
	       menu(knieachsen_li, cycle),
	     R_Ckfu_Achsen_Re  :=
	       menu(r�ckfu�achsen_re, cycle),
	     R_Ckfu_Achsen_Li  :=
	       menu(r�ckfu�achsen_li, cycle),
	     Ws_Haltung        :=
	       menu(ws_haltung, cycle),
	     Gesamt_Ws         :=
	       menu(gesamt_ws, toggle),
	     Brust_Ws          :=
	       menu(brust_ws, toggle),
	     Lenden_Ws         :=
	       menu(lenden_ws, toggle),
	     Seitlich          :=
	       menu(seitlich, cycle),
	     Ws                :=
	       menu(ws, cycle),
	     Fba               :=
	       slider(fba, -50, 50, 0),
	     Gang_Auf_Ebene    :=
	       menu(gang_auf_ebene, toggle),
	     Fersengang        :=
	       menu(fersengang, toggle),
	     Zehengang         :=
	       menu(zehengang, toggle)
	   ],
	 modifications :=
	   [ Hand              :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(rechtsh�nder,
					  @default,
					  'Rechtsh�nder',
					  @off,
					  @nil,
					  r),
				menu_item(linksh�nder,
					  @default,
					  'Linksh�nder',
					  @off,
					  @nil,
					  l),
				menu_item(unbekannt,
					  @default,
					  'Unbekannt',
					  @off,
					  @nil,
					  u)
			      ]
	       ],
	     Fu_               :=
	       [ alignment := left,
		 reference := point(0, 13),
		 append    := [ menu_item(rechtsf�sser,
					  @default,
					  'Rechtsf�sser',
					  @off,
					  @nil,
					  r),
				menu_item(linksf�sser,
					  @default,
					  'Linksf�sser',
					  @off,
					  @nil,
					  l),
				menu_item(unbekannt,
					  @default,
					  'Unbekannt',
					  @off,
					  @nil,
					  u)
			      ]
	       ],
	     Gewicht           :=
	       [ label := 'Gewicht (kg):'
	       ],
	     Gr__e             :=
	       [ label := 'Gr��e (cm):'
	       ],
	     Schultern         :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(gerade, @default, 'Gerade', @off, @nil, g),
				menu_item(schulterhochstand_re,
					  @default,
					  'Schulterhochstand Re',
					  @off,
					  @nil,
					  s),
				menu_item(schulterhochstand_li,
					  @default,
					  'Schulterhochstand Li',
					  @off,
					  @nil,
					  c),
				menu_item(scapula_alata_re,
					  @default,
					  'Scapula Alata Re',
					  @off,
					  @nil,
					  a),
				menu_item(scapula_alata_li,
					  @default,
					  'Scapula Alata Li',
					  @off,
					  @nil,
					  l)
			      ]
	       ],
	     Becken            :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(gerade, @default, 'Gerade', @off, @nil, g),
				menu_item(beckenhochstand_re,
					  @default,
					  'Beckenhochstand Re',
					  @off,
					  @nil,
					  b),
				menu_item(beckenhochstand_li,
					  @default,
					  'Beckenhochstand Li',
					  @off,
					  @nil,
					  e)
			      ]
	       ],
	     Brettchen         :=
	       [ label     := 'Brettchen (mm):',
		 alignment := center
	       ],
	     Obere_Extr_Re     :=
	       [ alignment := left,
		 reference := point(0, 13),
		 append    := [ menu_item(gerade, @default, 'Gerade', @off, @nil, g),
				menu_item(oberarm_deform,
					  @default,
					  'Oberarm Deform',
					  @off,
					  @nil,
					  o),
				menu_item(unterarm_deform,
					  @default,
					  'Unterarm Deform',
					  @off,
					  @nil,
					  u),
				menu_item(beide_deform,
					  @default,
					  'Beide Deform',
					  @off,
					  @nil,
					  b)
			      ]
	       ],
	     Obere_Extr_Li     :=
	       [ alignment := right,
		 reference := point(0, 13),
		 append    := [ menu_item(gerade, @default, 'Gerade', @off, @nil, g),
				menu_item(oberarm_deform,
					  @default,
					  'Oberarm Deform',
					  @off,
					  @nil,
					  o),
				menu_item(unterarm_deform,
					  @default,
					  'Unterarm Deform',
					  @off,
					  @nil,
					  u),
				menu_item(beide_deform,
					  @default,
					  'Beide Deform',
					  @off,
					  @nil,
					  b)
			      ]
	       ],
	     Ellbogenachsen_Re :=
	       [ alignment := left,
		 reference := point(0, 13),
		 append    := [ menu_item(normal, @default, 'Normal', @off, @nil, n),
				menu_item(varus_deform,
					  @default,
					  'Varus Deform',
					  @off,
					  @nil,
					  v),
				menu_item(valgus_deform,
					  @default,
					  'Valgus Deform',
					  @off,
					  @nil,
					  a)
			      ]
	       ],
	     Ellbogenachsen_Li :=
	       [ alignment := right,
		 reference := point(0, 13),
		 append    := [ menu_item(normal, @default, 'Normal', @off, @nil, n),
				menu_item(varus_deform,
					  @default,
					  'Varus Deform',
					  @off,
					  @nil,
					  v),
				menu_item(valgus_deform,
					  @default,
					  'Valgus Deform',
					  @off,
					  @nil,
					  a)
			      ]
	       ],
	     Untere_Extr_Re    :=
	       [ alignment := left,
		 reference := point(0, 13),
		 append    := [ menu_item(gerade, @default, 'Gerade', @off, @nil, g),
				menu_item(obersch_deform,
					  @default,
					  'Obersch Deform',
					  @off,
					  @nil,
					  o),
				menu_item(untersch_deform,
					  @default,
					  'Untersch Deform',
					  @off,
					  @nil,
					  u),
				menu_item(beide_deform,
					  @default,
					  'Beide Deform',
					  @off,
					  @nil,
					  b)
			      ]
	       ],
	     Untere_Extr_Li    :=
	       [ alignment := right,
		 reference := point(0, 13),
		 append    := [ menu_item(gerade, @default, 'Gerade', @off, @nil, g),
				menu_item(obersch_deform,
					  @default,
					  'Obersch Deform',
					  @off,
					  @nil,
					  o),
				menu_item(untersch_deform,
					  @default,
					  'Untersch Deform',
					  @off,
					  @nil,
					  u),
				menu_item(beide_deform,
					  @default,
					  'Beide Deform',
					  @off,
					  @nil,
					  b)
			      ]
	       ],
	     Knieachsen_Re     :=
	       [ alignment := left,
		 reference := point(0, 13),
		 append    := [ menu_item(normal, @default, 'Normal', @off, @nil, n),
				menu_item(varus_deform,
					  @default,
					  'Varus Deform',
					  @off,
					  @nil,
					  v),
				menu_item(valgus_deform,
					  @default,
					  'Valgus Deform',
					  @off,
					  @nil,
					  a)
			      ]
	       ],
	     Knieachsen_Li     :=
	       [ alignment := right,
		 reference := point(0, 13),
		 append    := [ menu_item(normal, @default, 'Normal', @off, @nil, n),
				menu_item(varus_deform,
					  @default,
					  'Varus Deform',
					  @off,
					  @nil,
					  v),
				menu_item(valgus_deform,
					  @default,
					  'Valgus Deform',
					  @off,
					  @nil,
					  a)
			      ]
	       ],
	     R_Ckfu_Achsen_Re  :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(normal, @default, 'Normal', @off, @nil, n),
				menu_item(varus_deform,
					  @default,
					  'Varus Deform',
					  @off,
					  @nil,
					  v),
				menu_item(valgus_deform,
					  @default,
					  'Valgus Deform',
					  @off,
					  @nil,
					  d),
				menu_item(aktiv_aufrichtbar,
					  @default,
					  'Aktiv Aufrichtbar',
					  @off,
					  @nil,
					  a)
			      ]
	       ],
	     R_Ckfu_Achsen_Li  :=
	       [ alignment := right,
		 reference := point(0, 13),
		 append    := [ menu_item(normal, @default, 'Normal', @off, @nil, n),
				menu_item(varus_deform,
					  @default,
					  'Varus Deform',
					  @off,
					  @nil,
					  v),
				menu_item(valgus_deform,
					  @default,
					  'Valgus Deform',
					  @off,
					  @nil,
					  d),
				menu_item(aktiv_aufrichtbar,
					  @default,
					  'Aktiv Aufrichtbar',
					  @off,
					  @nil,
					  a)
			      ]
	       ],
	     Ws_Haltung        :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(gerade, @default, 'Gerade', @off, @nil, g),
				menu_item(schlaff,
					  @default,
					  'Schlaff',
					  @off,
					  @nil,
					  s),
				menu_item(straff, @default, 'Straff', @off, @nil, t),
				menu_item(deformiert,
					  @default,
					  'Deformiert',
					  @off,
					  @nil,
					  d)
			      ]
	       ],
	     Gesamt_Ws         :=
	       [ columns   := 2,
		 reference := point(0, 13),
		 append    := [ menu_item(gerade, @default, 'Gerade', @off, @nil, g),
				menu_item(skoliose,
					  @default,
					  'Skoliose',
					  @off,
					  @nil,
					  s),
				menu_item(skoliosierung,
					  @default,
					  'Skoliosierung',
					  @off,
					  @nil,
					  k),
				menu_item(im_lot, @default, 'Im Lot', @off, @nil, i),
				menu_item(aus_dem_lot_re,
					  @default,
					  'aus dem Lot Re',
					  @off,
					  @nil,
					  a),
				menu_item(aus_dem_lot_li,
					  @default,
					  'aus dem Lot Li',
					  @off,
					  @nil,
					  u)
			      ]
	       ],
	     Brust_Ws          :=
	       [ columns   := 2,
		 reference := point(0, 13),
		 append    := [ menu_item(gerade, @default, 'Gerade', @off, @nil, g),
				menu_item(skoliose,
					  @default,
					  'Skoliose',
					  @off,
					  @nil,
					  s),
				menu_item(reconv, @default, 'Reconv', @off, @nil, r),
				menu_item(liconv, @default, 'Liconv', @off, @nil, l),
				menu_item(rippenbuckel_re,
					  @default,
					  'Rippenbuckel Re',
					  @off,
					  @nil,
					  i),
				menu_item(rippenbuckel_li,
					  @default,
					  'Rippenbuckel Li',
					  @off,
					  @nil,
					  p)
			      ]
	       ],
	     Lenden_Ws         :=
	       [ columns   := 3,
		 reference := point(0, 13),
		 append    := [ menu_item(gerade, @default, 'Gerade', @off, @nil, g),
				menu_item(skoliose,
					  @default,
					  'Skoliose',
					  @off,
					  @nil,
					  s),
				menu_item(reconv, @default, 'Reconv', @off, @nil, r),
				menu_item(liconv, @default, 'Liconv', @off, @nil, l),
				menu_item(lendenwulst_re,
					  @default,
					  'Lendenwulst Re',
					  @off,
					  @nil,
					  e),
				menu_item(lendenwulst_li,
					  @default,
					  'Lendenwulst Li',
					  @off,
					  @nil,
					  n),
				menu_item(rima_ani_gerade,
					  @default,
					  'Rima Ani Gerade',
					  @off,
					  @nil,
					  i),
				menu_item(abweichen_re,
					  @default,
					  'Abweichen Re',
					  @off,
					  @nil,
					  a),
				menu_item(abweichend_li,
					  @default,
					  'Abweichend Li',
					  @off,
					  @nil,
					  b)
			      ]
	       ],
	     Seitlich          :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(physiologisches_s,
					  @default,
					  'Physiologisches S',
					  @off,
					  @nil,
					  p),
				menu_item(hohl_rundr�cken,
					  @default,
					  'Hohl-Rundr�cken',
					  @off,
					  @nil,
					  h),
				menu_item(hohl_flachr�cken,
					  @default,
					  'Hohl-Flachr�cken',
					  @off,
					  @nil,
					  o)
			      ]
	       ],
	     Ws                :=
	       [ alignment := right,
		 reference := point(0, 13),
		 append    := [ menu_item(aktiv_aufrichtbar,
					  @default,
					  'Aktiv Aufrichtbar',
					  @off,
					  @nil,
					  a),
				menu_item(fixiert,
					  @default,
					  'Fixiert',
					  @off,
					  @nil,
					  f)
			      ]
	       ],
	     Fba               :=
	       [ label     := 'FBA:',
		 alignment := right
	       ],
	     Gang_Auf_Ebene    :=
	       [ label     := 'Gang auf Ebene:',
		 columns   := 3,
		 reference := point(0, 13),
		 append    := [ menu_item(frei, @default, 'Frei', @off, @nil, f),
				menu_item(schmerz_h,
					  @default,
					  'Schmerz-H.',
					  @off,
					  @nil,
					  s),
				menu_item(verk�rzungs_h,
					  @default,
					  'Verk�rzungs-H.',
					  @off,
					  @nil,
					  v),
				menu_item(l�hmungs_h,
					  @default,
					  'L�hmungs-H.',
					  @off,
					  @nil,
					  l),
				menu_item(duchenne_h,
					  @default,
					  'Duchenne-H.',
					  @off,
					  @nil,
					  d),
				menu_item(trendelenburg_h,
					  @default,
					  'Trendelenburg-H.',
					  @off,
					  @nil,
					  t),
				menu_item(diplegisch,
					  @default,
					  'Diplegisch',
					  @off,
					  @nil,
					  p),
				menu_item(ataktisch,
					  @default,
					  'Ataktisch',
					  @off,
					  @nil,
					  a),
				menu_item(anderes,
					  @default,
					  'Anderes',
					  @off,
					  @nil,
					  n),
				menu_item(rechts, @default, 'Rechts', @off, @nil, r),
				menu_item(links, @default, 'Links', @off, @nil, i),
				menu_item(beidseits,
					  @default,
					  'Beidseits',
					  @off,
					  @nil,
					  b)
			      ]
	       ],
	     Fersengang        :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(frei, @default, 'Frei', @off, @nil, f),
				menu_item(hinken, @default, 'Hinken', @off, @nil, h),
				menu_item(rechts, @default, 'Rechts', @off, @nil, r),
				menu_item(links, @default, 'Links', @off, @nil, l),
				menu_item(beidseits,
					  @default,
					  'Beidseits',
					  @off,
					  @nil,
					  b)
			      ]
	       ],
	     Zehengang         :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(frei, @default, 'Frei', @off, @nil, f),
				menu_item(hinken, @default, 'Hinken', @off, @nil, h),
				menu_item(rechts, @default, 'Rechts', @off, @nil, r),
				menu_item(links, @default, 'Links', @off, @nil, l),
				menu_item(beidseits,
					  @default,
					  'Beidseits',
					  @off,
					  @nil,
					  b)
			      ]
	       ]
	   ],
	 layout        :=
	   [ right(Fu_, Hand),
	     below(Gewicht, Hand),
	     below(Gr__e, Gewicht),
	     below(Schultern, Gr__e),
	     below(Becken, Schultern),
	     right(Brettchen, Becken),
	     below(Obere_Extr_Re, Becken),
	     right(Obere_Extr_Li, Obere_Extr_Re),
	     below(Ellbogenachsen_Re, Obere_Extr_Re),
	     right(Ellbogenachsen_Li, Ellbogenachsen_Re),
	     below(Untere_Extr_Re, Ellbogenachsen_Re),
	     right(Untere_Extr_Li, Untere_Extr_Re),
	     below(Knieachsen_Re, Untere_Extr_Re),
	     right(Knieachsen_Li, Knieachsen_Re),
	     below(R_Ckfu_Achsen_Re, Knieachsen_Re),
	     right(R_Ckfu_Achsen_Li, R_Ckfu_Achsen_Re),
	     below(Ws_Haltung, R_Ckfu_Achsen_Re),
	     below(Gesamt_Ws, Ws_Haltung),
	     below(Brust_Ws, Gesamt_Ws),
	     below(Lenden_Ws, Brust_Ws),
	     below(Seitlich, Lenden_Ws),
	     right(Ws, Seitlich),
	     below(Gang_Auf_Ebene, Seitlich),
	     right(Fba, Ws),
	     below(Fersengang, Gang_Auf_Ebene),
	     below(Zehengang, Fersengang)
	   ]
       ]).

dialog(stammdaten,
       [ object        :=
	   Patient,
	 parts         :=
	   [ Patient    :=
	       dialog('Patient'),
	     Name       :=
	       text_item(name),
	     Vorname    :=
	       text_item(vorname),
	     Geb_Datum  :=
	       text_item(geb_datum),
	     Geschlecht :=
	       menu(geschlecht, choice),
	     Code       :=
	       text_item(code)
	   ],
	 modifications :=
	   [ Name       :=
	       [ length := 30
	       ],
	     Vorname    :=
	       [ length := 30
	       ],
	     Geb_Datum  :=
	       [ label  := 'Geb. Datum:',
		 length := 10
	       ],
	     Geschlecht :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(m�nnlich,
					  @default,
					  'M�nnlich',
					  @off,
					  @nil,
					  m),
				menu_item(weiblich,
					  @default,
					  'Weiblich',
					  @off,
					  @nil,
					  w)
			      ]
	       ],
	     Code       :=
	       [ length := 30
	       ]
	   ],
	 layout        :=
	   [ below(Vorname, Name),
	     below(Geb_Datum, Vorname),
	     below(Geschlecht, Geb_Datum),
	     below(Code, Geschlecht)
	   ]

       ]).



	

	
dialog(frageb_obere_extr,
       [ object        :=
	   Frageb_Obere_Extr,
	 parts         :=
	   [ Frageb_Obere_Extr        :=
	       dialog('Frageb Obere Extr'),
	     Seite                    :=
	       menu(seite, choice),
	     Diagnose                 :=
	       label(diagnose, 'DIAGNOSE'),
	     Akute                    :=
	       menu(akute, toggle),
	     Chronisch                :=
	       menu(chronisch, toggle),
	     Andere_Chronisch         :=
	       text_item(andere_chronisch),
	     Allgemein                :=
	       menu(allgemein, toggle),
	     Bei_Kong__Deform__       :=
	       menu('bei Kong. Deform.:', choice),
	     Andere_Allgemein         :=
	       text_item(andere_allgemein),
	     Andere_Akute             :=
	       text_item(andere_akute),
	     Therapie                 :=
	       label(therapie, 'THERAPIE'),
	     Konserv__vorbehandelt    :=
	       menu('konserv. vorbehandelt', choice),
	     Geschlossene_Reposition  :=
	       menu('geschlossene Reposition', choice),
	     Operation_Auswaerts      :=
	       menu('operation auswaerts', choice),
	     Narkosemobilisation      :=
	       menu(narkosemobilisation, choice),
	     Wiederholter_Eingriff    :=
	       menu('wiederholter Eingriff', choice),
	     Komplikation             :=
	       menu(komplikation, choice),
	     Operation_Arthroskopisch :=
	       menu('operation arthroskopisch', choice),
	     Operation_Offen          :=
	       menu('operation offen', choice),
	     Andere_Therapie          :=
	       text_item('andere therapie')
	   ],
	 modifications :=
	   [ Seite                    :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(rechts, @default, rechts, @off, @nil, r),
				menu_item(links, @default, links, @off, @nil, l)
			      ]
	       ],
	     Diagnose                 :=
	       [ font := @helvetica_roman_18
	       ],
	     Akute                    :=
	       [ layout    := vertical,
		 reference := point(0, 13),
		 append    := [ menu_item('Frakfurt frisch < 7 Tage',
					  @default,
					  'Frakfurt frisch < 7 Tage',
					  @off,
					  @nil,
					  f),
				menu_item('Frakfurt alt >= 7 Tage',
					  @default,
					  'Frakfurt alt >= 7 Tage',
					  @off,
					  @nil,
					  r),
				menu_item('Salter Harris Type 1',
					  @default,
					  'Salter Harris Type 1',
					  @off,
					  @nil,
					  s),
				menu_item('Salter Harris Type 2',
					  @default,
					  'Salter Harris Type 2',
					  @off,
					  @nil,
					  h),
				menu_item('Salter Harris Type 3',
					  @default,
					  'Salter Harris Type 3',
					  @off,
					  @nil,
					  e),
				menu_item('Salter Harris Type 4',
					  @default,
					  'Salter Harris Type 4',
					  @off,
					  @nil,
					  i),
				menu_item('Salter Harris Type 5',
					  @default,
					  'Salter Harris Type 5',
					  @off,
					  @nil,
					  y),
				menu_item('Luxation/Subluxation',
					  @default,
					  'Luxation/Subluxation',
					  @off,
					  @nil,
					  l),
				menu_item('offene Fraktur',
					  @default,
					  'offene Fraktur',
					  @off,
					  @nil,
					  o),
				menu_item('traumat Weichteil Defekt',
					  @default,
					  'traumat Weichteil Defekt',
					  @off,
					  @nil,
					  t),
				menu_item('Ac-Sprengung',
					  @default,
					  'Ac-Sprengung',
					  @off,
					  @nil,
					  a),
				menu_item('akute Bursitis',
					  @default,
					  'akute Bursitis',
					  @off,
					  @nil,
					  k),
				menu_item('septisch Arthritis',
					  @default,
					  'septisch Arthritis',
					  @off,
					  @nil,
					  p),
				menu_item(andere, @default, 'Andere', @off, @nil, n)
			      ]
	       ],
	     Chronisch                :=
	       [ layout    := vertical,
		 alignment := center,
		 reference := point(0, 13),
		 append    := [ menu_item('Instabilitaet',
					  @default,
					  'Instabilitaet',
					  @off,
					  @nil,
					  i),
				menu_item('RM-Defekt',
					  @default,
					  'RM-Defekt',
					  @off,
					  @nil,
					  r),
				menu_item('Kontraktur',
					  @default,
					  'Kontraktur',
					  @off,
					  @nil,
					  k),
				menu_item('Verbrennung',
					  @default,
					  'Verbrennung',
					  @off,
					  @nil,
					  v),
				menu_item('Avaskulaere Nekrose',
					  @default,
					  'Avaskulaere Nekrose',
					  @off,
					  @nil,
					  a),
				menu_item('Osteochondrosis',
					  @default,
					  'Osteochondrosis',
					  @off,
					  @nil,
					  o),
				menu_item('Osteomyelitis',
					  @default,
					  'Osteomyelitis',
					  @off,
					  @nil,
					  s),
				menu_item('Tumor', @default, 'Tumor', @off, @nil, t),
				menu_item(andere, @default, 'Andere', @off, @nil, n)
			      ]
	       ],
	     Andere_Chronisch         :=
	       [ length    := 29,
		 alignment := center,
		 reference := point(0, -205)
	       ],
	     Allgemein                :=
	       [ layout    := vertical,
		 alignment := center,
		 reference := point(0, 13),
		 append    := [ menu_item('PcP', @default, 'PcP', @off, @nil, p),
				menu_item('Brach. Plex. Verl. Kongent. Deform. ',
					  @default,
					  'Brach. Plex. Verl. Kongent. Deform',
					  @off,
					  @nil,
					  b),
				menu_item(andere, @default, 'Andere', @off, @nil, a)
			      ]
	       ],
	     Bei_Kong__Deform__       :=
	       [ format    := center,
		 layout    := vertical,
		 alignment := center,
		 reference := point(0, 13),
		 append    := [ menu_item('Spontan',
					  @default,
					  'Spontan',
					  @off,
					  @nil,
					  s),
				menu_item('Familiaer',
					  @default,
					  'Familiaer',
					  @off,
					  @nil,
					  f),
				menu_item('Bilateral',
					  @default,
					  'Bilateral',
					  @off,
					  @nil,
					  b)
			      ]
	       ],
	     Andere_Allgemein         :=
	       [ length    := 20,
		 alignment := center,
		 reference := point(0, -205)
	       ],
	     Andere_Akute             :=
	       [ length := 29
	       ],
	     Therapie                 :=
	       [ font := @helvetica_roman_18
	       ],
	     Konserv__vorbehandelt    :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     Geschlossene_Reposition  :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     Operation_Auswaerts      :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     Narkosemobilisation      :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     Wiederholter_Eingriff    :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     Komplikation             :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     Operation_Arthroskopisch :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('rein diagnostisch',
					  @default,
					  'rein diagnostisch',
					  @off,
					  @nil,
					  r),
				menu_item(operativ,
					  @default,
					  operativ,
					  @off,
					  @nil,
					  o)
			      ]
	       ],
	     Operation_Offen          :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('Osteosynthese/Rekonstruktion',
					  @default,
					  'Osteosynthese/Rekonstruktion',
					  @off,
					  @nil,
					  o),
				menu_item('Weichteil Eingriff',
					  @default,
					  'Weichteil Eingriff',
					  @off,
					  @nil,
					  w)
			      ]
	       ],
	     Andere_Therapie          :=
	       [ label  := 'Andere Therapie:',
		 length := 31
	       ]
	   ],
	 layout        :=
	   [ area(Seite,
		  area(15, 8, 105, 18)),
	     area(Diagnose,
		  area(15, 34, 252, 23)),
	     area(Akute,
		  area(16, 64, 144, 253)),
	     area(Chronisch,
		  area(231, 82, 130, 168)),
	     area(Andere_Chronisch,
		  area(233, 257, 287, 18)),
	     area(Allgemein,
		  area(419, 86, 203, 66)),
	     area(Bei_Kong__Deform__,
		  area(421, 157, 110, 66)),
	     area(Andere_Allgemein,
		  area(421, 226, 236, 18)),
	     area(Andere_Akute,
		  area(15, 317, 263, 18)),
	     area(Therapie,
		  area(15, 342, 252, 23)),
	     area(Konserv__vorbehandelt,
		  area(29, 365, 199, 18)),
	     area(Geschlossene_Reposition,
		  area(377, 366, 209, 18)),
	     area(Operation_Auswaerts,
		  area(40, 392, 189, 18)),
	     area(Narkosemobilisation,
		  area(405, 391, 183, 18)),
	     area(Wiederholter_Eingriff,
		  area(45, 420, 184, 18)),
	     area(Komplikation,
		  area(450, 417, 140, 18)),
	     area(Operation_Arthroskopisch,
		  area(17, 448, 285, 18)),
	     area(Operation_Offen,
		  area(433, 445, 301, 18)),
	     area(Andere_Therapie,
		  area(68, 476, 292, 18))
	   ]
       ]).
dialog(schulter,
       [ object        :=
	   Schulter,
	 parts         :=
	   [ Schulter            :=
	       dialog('Schulter'),
	     Flex__aktiv_        :=
	       slider('Flex. aktiv:', 0, 180, 1),
	     ABD_aktiv_          :=
	       slider('ABD aktiv:', 0, 180, 1),
	     ARO_aktiv_          :=
	       menu('ARO aktiv:', toggle),
	     IRO_aktiv_          :=
	       menu('IRO aktiv:', toggle),
	     ABD_passiv_         :=
	       slider('ABD passiv:', -10, 90, 10),
	     ARO_passiv_         :=
	       slider('ARO passiv:', -10, 90, 10),
	     Kraft_kp            :=
	       slider('Kraft/kp', 1, 10, 1),
	     Instabilit_t        :=
	       menu('Instabilit�t', toggle),
	     Atrophien           :=
	       menu('Atrophien', choice),
	     Schmerzhafter_Bogen :=
	       menu('Schmerzhafter Bogen', choice),
	     ACG_Schmerz         :=
	       menu('ACG-Schmerz', choice),
	     HWS_Beschwerden     :=
	       menu('HWS-Beschwerden', choice),
	     Torticollis         :=
	       menu('Torticollis', choice)
	   ],
	 modifications :=
	   [ Flex__aktiv_        :=
	       [ width := 205
	       ],
	     ABD_aktiv_          :=
	       [ width := 204
	       ],
	     ARO_aktiv_          :=
	       [ layout    := vertical,
		 reference := point(0, 13),
		 append    := [ menu_item('Hand am Hinterkopf, Ellenbogen nach vorn',
					  @default,
					  'Hand am Hinterkopf, Ellenbogen nach vorn',
					  @off,
					  @nil,
					  h),
				menu_item('Hand am Hinterkopf, Ellenbogen nach hinten',
					  @default,
					  'Hand am Hinterkopf, Ellenbogen nach hinten',
					  @off,
					  @nil,
					  a),
				menu_item('Hand auf dem Kopf, Ellenbogen nach vorn',
					  @default,
					  'Hand auf dem Kopf, Ellenbogen nach vorn',
					  @off,
					  @nil,
					  d),
				menu_item('Hand auf dem Kopf, Ellenbogen nach hinten',
					  @default,
					  'Hand auf dem Kopf, Ellenbogen nach hinten',
					  @off,
					  @nil,
					  k),
				menu_item('Vollst�ndige Elevation vom Kopfausgehend',
					  @default,
					  'Vollst�ndige Elevation vom Kopfausgehend',
					  @off,
					  @nil,
					  v)
			      ]
	       ],
	     IRO_aktiv_          :=
	       [ layout    := vertical,
		 reference := point(0, 13),
		 append    := [ menu_item('Trochanter major',
					  @default,
					  'Trochanter major',
					  @off,
					  @nil,
					  t),
				menu_item('Ges��', @default, 'Ges��', @off, @nil, g),
				menu_item('ISG', @default, 'ISG', @off, @nil, i),
				menu_item('LWK 3', @default, 'LWK 3', @off, @nil, l),
				menu_item('BWK 12', @default, 'BWK 12', @off, @nil, b),
				menu_item('Interscapul�r',
					  @default,
					  'Interscapul�r',
					  @off,
					  @nil,
					  n)
			      ]
	       ],
	     ABD_passiv_         :=
	       [ width     := 205,
		 reference := point(0, 15)
	       ],
	     ARO_passiv_         :=
	       [ width     := 204,
		 reference := point(0, 15)
	       ],
	     Kraft_kp            :=
	       [ width     := 205,
		 reference := point(0, 13)
	       ],
	     Instabilit_t        :=
	       [ format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('Apprehension positiv',
					  @default,
					  'Apprehension positiv',
					  @off,
					  @nil,
					  a),
				menu_item('Schublade positiv',
					  @default,
					  'Schublade positiv',
					  @off,
					  @nil,
					  s),
				menu_item('Sulcuszeichen positiv',
					  @default,
					  'Sulcuszeichen positiv',
					  @off,
					  @nil,
					  u)
			      ]
	       ],
	     Atrophien           :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     Schmerzhafter_Bogen :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     ACG_Schmerz         :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     HWS_Beschwerden     :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     Torticollis         :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ]
	   ],
	 layout        :=
	   [ area(Flex__aktiv_,
		  area(15, 8, 346, 20)),
	     area(ABD_aktiv_,
		  area(488, 7, 342, 20)),
	     area(ARO_aktiv_,
		  area(13, 40, 261, 100)),
	     area(IRO_aktiv_,
		  area(491, 36, 103, 117)),
	     area(ABD_passiv_,
		  area(15, 161, 362, 20)),
	     area(ARO_passiv_,
		  area(491, 161, 362, 20)),
	     area(Kraft_kp,
		  area(35, 189, 319, 20)),
	     area(Instabilit_t,
		  area(498, 189, 468, 18)),
	     area(Atrophien,
		  area(28, 217, 119, 18)),
	     area(Schmerzhafter_Bogen,
		  area(433, 216, 193, 18)),
	     area(ACG_Schmerz,
		  area(0, 243, 149, 18)),
	     area(HWS_Beschwerden,
		  area(444, 240, 181, 18)),
	     area(Torticollis,
		  area(27, 269, 121, 18))
	   ]
       ]).
dialog(ellbogen,
       [ object        :=
	   Ellbogen,
	 parts         :=
	   [ Ellbogen                :=
	       dialog('Ellbogen'),
	     Flex__aktiv_            :=
	       slider('Flex. aktiv:', 0, 180, 1),
	     Ext__aktiv_             :=
	       slider('Ext. aktiv:', -91, 15, 1),
	     Pronation_aktiv_        :=
	       slider('Pronation aktiv:', 0, 90, 1),
	     Supination_aktiv_       :=
	       slider('Supination aktiv:', 0, 90, 1),
	     Instabilit_t_           :=
	       menu('Instabilit�t:', choice),
	     Kraft_in_Flexion___kp   :=
	       slider('Kraft in Flexion / kp', 1, 10, 1),
	     Kraft_in_Extension___kp :=
	       slider('Kraft in Extension / kp', 1, 10, 1),
	     Atrophien               :=
	       menu('Atrophien', choice),
	     HWS_Beschwerden         :=
	       menu('HWS-Beschwerden', choice)
	   ],
	 modifications :=
	   [ Instabilit_t_   :=
	       [ format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(medial, @default, medial, @off, @nil, m),
				menu_item(lateral,
					  @default,
					  lateral,
					  @off,
					  @nil,
					  l)
			      ]
	       ],
	     Atrophien       :=
	       [ format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     HWS_Beschwerden :=
	       [ format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Ext__aktiv_, Flex__aktiv_),
	     below(Pronation_aktiv_, Ext__aktiv_),
	     below(Supination_aktiv_, Pronation_aktiv_),
	     below(Instabilit_t_, Supination_aktiv_),
	     below(Kraft_in_Flexion___kp, Instabilit_t_),
	     below(Kraft_in_Extension___kp, Kraft_in_Flexion___kp),
	     below(Atrophien, Kraft_in_Extension___kp),
	     below(HWS_Beschwerden, Atrophien)
	   ]
       ]).
dialog(handgelenk,
       [ object        :=
	   Handgelenk,
	 parts         :=
	   [ Handgelenk        :=
	       dialog('Handgelenk'),
	     Flex__aktiv_      :=
	       slider('Flex. aktiv:', 0, 90, 1),
	     Ext__aktiv_       :=
	       slider('Ext. aktiv:', 0, 90, 1),
	     Pronation_aktiv_  :=
	       slider('Pronation aktiv:', 0, 90, 1),
	     Supination_aktiv_ :=
	       slider('Supination aktiv:', 0, 90, 1),
	     Ulnar_Deviation_  :=
	       slider('Ulnar Deviation:', 0, 30, 1),
	     Radial_Deviation_ :=
	       slider('Radial Deviation:', 0, 30, 1),
	     Instabilit_t_     :=
	       menu('Instabilit�t:', choice)
	   ],
	 modifications :=
	   [ Instabilit_t_ := [ reference := point(0, 13),
				append    := [ menu_item(dorsal,
							 @default,
							 dorsal,
							 @off,
							 @nil,
							 d),
					       menu_item(volar,
							 @default,
							 volar,
							 @off,
							 @nil,
							 v)
					     ]
			      ]
	   ],
	 layout        :=
	   [ below(Ext__aktiv_, Flex__aktiv_),
	     below(Pronation_aktiv_, Ext__aktiv_),
	     below(Supination_aktiv_, Pronation_aktiv_),
	     below(Ulnar_Deviation_, Supination_aktiv_),
	     below(Radial_Deviation_, Ulnar_Deviation_),
	     below(Instabilit_t_, Radial_Deviation_)
	   ]
       ]).
dialog(ergebnis,
       [ object        :=
	   Ergebnis,
	 parts         :=
	   [ Ergebnis                  :=
	       dialog('Ergebnis'),
	     Untersuchter_Arm          :=
	       menu('untersuchter Arm', choice),
	     Dominanter_Arm            :=
	       menu('dominanter Arm', choice),
	     Schmerz                   :=
	       label(schmerz, 'SCHMERZ'),
	     Schmerz_A                 :=
	       menu(schmerz_a, choice),
	     Schmerz_B                 :=
	       menu(schmerz_b, choice),
	     Schmerz_C                 :=
	       menu(schmerz_c, choice),
	     Funk                      :=
	       label(funk, 'Funktion'),
	     Funk__                    :=
	       menu(funk_1, toggle),
	     Schulfaehigkeit           :=
	       menu(schulfaehigkeit, choice),
	     Freizeit_Sportfaehigkeit  :=
	       menu(freizeit_Sportfaehigkeit, choice),
	     Schlaf                    :=
	       menu(schlaf, choice),
	     Taetigkeiten_Moeglich_Bis :=
	       menu('taetigkeiten moeglich bis', choice),
	     Diag                      :=
	       label(diag, 'Diagnostik'),
	     Diagnos                   :=
	       menu(diagnos, toggle)
	   ],
	 modifications :=
	   [ Untersuchter_Arm          :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(rechts, @default, rechts, @off, @nil, r),
				menu_item(links, @default, links, @off, @nil, l)
			      ]
	       ],
	     Dominanter_Arm            :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     Schmerz                   :=
	       [ font := @helvetica_roman_18
	       ],
	     Schmerz_A                 :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item(leicht, @default, leicht, @off, @nil, l),
				menu_item(maessig,
					  @default,
					  maessig,
					  @off,
					  @nil,
					  m),
				menu_item(stark, @default, stark, @off, @nil, s)
			      ]
	       ],
	     Schmerz_B                 :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 20),
		 append    := [ menu_item('Dauerschmerz, haeufig starke Analgetika',
					  @default,
					  'Dauerschmerz, haeufig starke Analgetika',
					  @off,
					  @nil,
					  d),
				menu_item('Dauerschmerz, gelegentlich starke Analgetika',
					  @default,
					  'Dauerschmerz, gelegentlich starke Analgetika',
					  @off,
					  @nil,
					  a),
				menu_item('Belastungsschmerz bei geringer Belastung, Analgetika haeufig',
					  @default,
					  'Belastungsschmerz bei geringer Belastung, Analgetika haeufig',
					  @off,
					  @nil,
					  b),
				menu_item('Belastungsschmerz nur bei starker oder besonderer
			Belastung, Analgetika gelegentlich',
					  @default,
					  'Belastungsschmerz nur bei starker oder besonderer
			Belastung, Analgetika gelegentlich',
					  @off,
					  @nil,
					  e),
				menu_item('gelegentlicher leicht Schmerz, aber Teilnahme am Schulsport',
					  @default,
					  'gelegentlicher leicht Schmerz, aber Teilnahme am Schulsport',
					  @off,
					  @nil,
					  g),
				menu_item('Kein Schmerz',
					  @default,
					  'Kein Schmerz',
					  @off,
					  @nil,
					  k)
			      ]
	       ],
	     Schmerz_C                 :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('Beschwerden deutlich gebessert nach Therapie',
					  @default,
					  'Beschwerden deutlich gebessert nach Therapie',
					  @off,
					  @nil,
					  b),
				menu_item('Beschwerden nicht gebessert nach Therapie',
					  @default,
					  'Beschwerden nicht gebessert nach Therapie',
					  @off,
					  @nil,
					  e)
			      ]
	       ],
	     Funk                      :=
	       [ font      := @helvetica_roman_18,
		 reference := point(0, 18)
	       ],
	     Funk__                    :=
	       [ label              :=
		   'Funktion:',
		 multiple_selection :=
		   @off,
		 feedback           :=
		   show_selection_only,
		 alignment          :=
		   left,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item('normale Aktivitaet moeglich',
			       @default,
			       'normale Aktivitaet moeglich',
			       @off,
			       @nil,
			       n),
		     menu_item('nur leichte Einschraenkungen, Ueberkopfaktivitaeten moeglich',
			       @default,
			       'nur leichte Einschraenkungen, Ueberkopfaktivitaeten moeglich',
			       @off,
			       @nil,
			       u),
		     menu_item('meiste Aktivitaeten, reduzierter Sport moeglich',
			       @default,
			       'meiste Aktivitaeten, reduzierter Sport moeglich',
			       @off,
			       @nil,
			       m),
		     menu_item('nur leichte Aktivitaeten, aber kein Sport moeglich',
			       @default,
			       'nur leichte Aktivitaeten, aber kein Sport moeglich',
			       @off,
			       @nil,
			       l),
		     menu_item('nur leichte Aktivitaeten noch moeglich',
			       @default,
			       'nur leichte Aktivitaeten noch moeglich',
			       @off,
			       @nil,
			       r),
		     menu_item('Arm nicht mehr einsetzbar',
			       @default,
			       'Arm nicht mehr einsetzbar',
			       @off,
			       @nil,
			       a)
		   ]
	       ],
	     Schulfaehigkeit           :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(vollstaendig,
					  @default,
					  vollstaendig,
					  @off,
					  @nil,
					  v),
				menu_item(weitgehend,
					  @default,
					  weitgehend,
					  @off,
					  @nil,
					  w),
				menu_item('zur Haelfte',
					  @default,
					  'zur Haelfte',
					  @off,
					  @nil,
					  z),
				menu_item(gering, @default, gering, @off, @nil, g),
				menu_item('nicht schulfaehig',
					  @default,
					  'nicht schulfaehig',
					  @off,
					  @nil,
					  n)
			      ]
	       ],
	     Freizeit_Sportfaehigkeit  :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(vollstaendig,
					  @default,
					  vollstaendig,
					  @off,
					  @nil,
					  v),
				menu_item(weitgehend,
					  @default,
					  weitgehend,
					  @off,
					  @nil,
					  w),
				menu_item('zur Haelfte',
					  @default,
					  'zur Haelfte',
					  @off,
					  @nil,
					  z),
				menu_item(gering, @default, gering, @off, @nil, g),
				menu_item('nicht sportfaehig',
					  @default,
					  'nicht sportfaehig',
					  @off,
					  @nil,
					  n)
			      ]
	       ],
	     Schlaf                    :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(normal, @default, normal, @off, @nil, n),
				menu_item('gelegentlich Unterbrechung',
					  @default,
					  'gelegentlich Unterbrechung',
					  @off,
					  @nil,
					  g),
				menu_item('dauerende Unterbrechung',
					  @default,
					  'dauerende Unterbrechung',
					  @off,
					  @nil,
					  d)
			      ]
	       ],
	     Taetigkeiten_Moeglich_Bis :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('Taille', @default, 'Taille', @off, @nil, t),
				menu_item('Brusthoehe',
					  @default,
					  'Brusthoehe',
					  @off,
					  @nil,
					  b),
				menu_item('Nacken', @default, 'Nacken', @off, @nil, n),
				menu_item('Kopf', @default, 'Kopf', @off, @nil, k),
				menu_item('Ueberkopf',
					  @default,
					  'Ueberkopf',
					  @off,
					  @nil,
					  u)
			      ]
	       ],
	     Diag                      :=
	       [ font := @helvetica_roman_18
	       ],
	     Diagnos                   :=
	       [ label     := 'Diagnostik:',
		 columns   := 3,
		 alignment := left,
		 reference := point(0, 13),
		 append    := [ menu_item('Sono', @default, 'Sono', @off, @nil, s),
				menu_item('Standard-Roentgen',
					  @default,
					  'Standard-Roentgen',
					  @off,
					  @nil,
					  t),
				menu_item('Spez.-Roentgen',
					  @default,
					  'Spez.-Roentgen',
					  @off,
					  @nil,
					  p),
				menu_item('Arthrographie',
					  @default,
					  'Arthrographie',
					  @off,
					  @nil,
					  a),
				menu_item('NMR', @default, 'NMR', @off, @nil, n),
				menu_item('CT', @default, 'CT', @off, @nil, c),
				menu_item('Szintigraphie',
					  @default,
					  'Szintigraphie',
					  @off,
					  @nil,
					  z)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Dominanter_Arm, Untersuchter_Arm),
	     below(Schmerz, Dominanter_Arm),
	     below(Schmerz_A, Schmerz),
	     below(Schmerz_B, Schmerz_A),
	     below(Schmerz_C, Schmerz_B),
	     below(Funk, Schmerz_C),
	     below(Funk__, Funk),
	     below(Schulfaehigkeit, Funk__),
	     below(Freizeit_Sportfaehigkeit, Schulfaehigkeit),
	     below(Schlaf, Freizeit_Sportfaehigkeit),
	     below(Taetigkeiten_Moeglich_Bis, Schlaf),
	     below(Diag, Taetigkeiten_Moeglich_Bis),
	     below(Diagnos, Diag)
	   ]
       ]).
dialog(kindliche_knie_anam,
       [ object        :=
	   Kindliche_Knie_Anam,
	 parts         :=
	   [ Kindliche_Knie_Anam :=
	       dialog('Kindliche Knie Anam'),
	     Knie                :=
	       menu(knie, choice),
	     Kontrolle           :=
	       menu(kontrolle, choice),
	     Ausl_sende_Ursache  :=
	       menu('ausl�sende Ursache', choice),
	     Aktuelles_Problem   :=
	       menu('aktuelles Problem', choice),
	     Bei_Schmerz         :=
	       menu(bei_schmerz, choice),
	     Schmerzlokalisation :=
	       menu(schmerzlokalisation, choice),
	     Schmerzintensitaet  :=
	       slider(schmerzintensitaet, 0, 100, 1),
	     Sportfaehigkeit     :=
	       menu(sportfaehigkeit, choice)
	   ],
	 modifications :=
	   [ Knie                :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(rechts, @default, rechts, @off, @nil, r),
				menu_item(links, @default, links, @off, @nil, l)
			      ]
	       ],
	     Kontrolle           :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('Erstvorstellung',
					  @default,
					  'Erstvorstellung',
					  @off,
					  @nil,
					  e),
				menu_item('Verlaufskontrolle / nach Operation',
					  @default,
					  'Verlaufskontrolle / nach Operation',
					  @off,
					  @nil,
					  v),
				menu_item('Verlaufskontrolle / nach kons. Behandlung',
					  @default,
					  'Verlaufskontrolle / nach kons. Behandlung',
					  @off,
					  @nil,
					  n)
			      ]
	       ],
	     Ausl_sende_Ursache  :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('Trauma', @default, 'Trauma', @off, @nil, t),
				menu_item(spontan,
					  @default,
					  spontan,
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Aktuelles_Problem   :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('Funktionsdefizit',
					  @default,
					  'Funktionsdefizit',
					  @off,
					  @nil,
					  f),
				menu_item(instabilit�t,
					  @default,
					  instabilit�t,
					  @off,
					  @nil,
					  i),
				menu_item('Schmerz',
					  @default,
					  'Schmerz',
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Bei_Schmerz         :=
	       [ multiple_selection :=
		   @on,
		 columns            :=
		   4,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item(zunehmend, @default, zunehmend, @off, @nil, z),
		     menu_item(gleichbleibend,
			       @default,
			       gleichbleibend,
			       @off,
			       @nil,
			       g),
		     menu_item(abnehmend, @default, abnehmend, @off, @nil, a),
		     menu_item(wellenfoermig,
			       @default,
			       wellenfoermig,
			       @off,
			       @nil,
			       w),
		     menu_item(gelegentlich,
			       @default,
			       gelegentlich,
			       @off,
			       @nil,
			       e),
		     menu_item(belastungsabhaengig,
			       @default,
			       belastungsabhaengig,
			       @off,
			       @nil,
			       b),
		     menu_item(bewegungsabhaengig,
			       @default,
			       bewegungsabhaengig,
			       @off,
			       @nil,
			       u),
		     menu_item('in Ruhe', @default, 'in Ruhe', @off, @nil, i)
		   ]
	       ],
	     Schmerzlokalisation :=
	       [ multiple_selection :=
		   @on,
		 columns            :=
		   3,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item('Tuberositas tibiae',
			       @default,
			       'Tuberositas tibiae',
			       @off,
			       @nil,
			       t),
		     menu_item('distaler Patellapol',
			       @default,
			       'distaler Patellapol',
			       @off,
			       @nil,
			       d),
		     menu_item('medialer Patellarand',
			       @default,
			       'medialer Patellarand',
			       @off,
			       @nil,
			       m),
		     menu_item('medialer Gelenkspalt',
			       @default,
			       'medialer Gelenkspalt',
			       @off,
			       @nil,
			       e),
		     menu_item('mediales Seitenband',
			       @default,
			       'mediales Seitenband',
			       @off,
			       @nil,
			       s)
		   ]
	       ],
	     Sportfaehigkeit     :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(eingeschraenkt,
					  @default,
					  eingeschraenkt,
					  @off,
					  @nil,
					  e),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Kontrolle, Knie),
	     below(Ausl_sende_Ursache, Kontrolle),
	     below(Aktuelles_Problem, Ausl_sende_Ursache),
	     below(Bei_Schmerz, Aktuelles_Problem),
	     below(Schmerzlokalisation, Bei_Schmerz),
	     below(Schmerzintensitaet, Schmerzlokalisation),
	     below(Sportfaehigkeit, Schmerzintensitaet)
	   ]
       ]).
dialog(kindliche_knie_klin_diag,
       [ object        :=
	   Kindliche_Knie_Klin_Diag,
	 parts         :=
	   [ Kindliche_Knie_Klin_Diag :=
	       dialog('Kindliche Knie Klin Diag'),
	     Beweglichkeit_rechts_    :=
	       slider('Beweglichkeit/rechts:', -20, 130, 5),
	     Beweglichkeit_links_     :=
	       slider('Beweglichkeit(links:', -20, 130, 5),
	     Beinachsen               :=
	       menu(beinachsen, choice),
	     Patella                  :=
	       menu(patella, toggle),
	     Meniskuszeichen          :=
	       menu('Meniskuszeichen', choice),
	     Seitenbandinstabilit_t   :=
	       menu('Seitenbandinstabilit�t', choice),
	     Kreuzbandinstabilit_t    :=
	       menu('Kreuzbandinstabilit�t', choice),
	     Weitere_Befunde          :=
	       menu('Weitere Befunde', toggle),
	     Diagnostek               :=
	       menu(diagnostek, toggle)
	   ],
	 modifications :=
	   [ Beinachsen             :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('Varus', @default, 'Varus', @off, @nil, v),
				menu_item('Valgus', @default, 'Valgus', @off, @nil, a),
				menu_item(normal, @default, normal, @off, @nil, n)
			      ]
	       ],
	     Patella                :=
	       [ columns   := 2,
		 reference := point(0, 13),
		 append    := [ menu_item('Rotationsfehler',
					  @default,
					  'Rotationsfehler',
					  @off,
					  @nil,
					  r),
				menu_item('Lateralisation',
					  @default,
					  'Lateralisation',
					  @off,
					  @nil,
					  l),
				menu_item('Luxation',
					  @default,
					  'Luxation',
					  @off,
					  @nil,
					  u),
				menu_item(normal, @default, normal, @off, @nil, n)
			      ]
	       ],
	     Meniskuszeichen        :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(medial, @default, medial, @off, @nil, m),
				menu_item(lateral,
					  @default,
					  lateral,
					  @off,
					  @nil,
					  l),
				menu_item(kein, @default, kein, @off, @nil, k)
			      ]
	       ],
	     Seitenbandinstabilit_t :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(medial, @default, medial, @off, @nil, m),
				menu_item(lateral,
					  @default,
					  lateral,
					  @off,
					  @nil,
					  l),
				menu_item(keine, @default, keine, @off, @nil, k)
			      ]
	       ],
	     Kreuzbandinstabilit_t  :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(anterior,
					  @default,
					  anterior,
					  @off,
					  @nil,
					  a),
				menu_item(posterior,
					  @default,
					  posterior,
					  @off,
					  @nil,
					  p),
				menu_item(keine, @default, keine, @off, @nil, k)
			      ]
	       ],
	     Weitere_Befunde        :=
	       [ columns   := 3,
		 reference := point(0, 13),
		 append    := [ menu_item('Ergu�', @default, 'Ergu�', @off, @nil, e),
				menu_item('Weichteilschwellung',
					  @default,
					  'Weichteilschwellung',
					  @off,
					  @nil,
					  w),
				menu_item('Poplitealzyste',
					  @default,
					  'Poplitealzyste',
					  @off,
					  @nil,
					  p),
				menu_item('Lokale Entz�ndungszeichen',
					  @default,
					  'Lokale Entz�ndungszeichen',
					  @off,
					  @nil,
					  l),
				menu_item('Quadricepsatrophie',
					  @default,
					  'Quadricepsatrophie',
					  @off,
					  @nil,
					  q),
				menu_item('Quadricepsverk�rzung',
					  @default,
					  'Quadricepsverk�rzung',
					  @off,
					  @nil,
					  u)
			      ]
	       ],
	     Diagnostek             :=
	       [ columns   := 8,
		 reference := point(0, 13),
		 append    := [ menu_item('Labor/Entz�ndung',
					  @default,
					  'Labor/Entz�ndung',
					  @off,
					  @nil,
					  l),
				menu_item('Labor/Rheuma',
					  @default,
					  'Labor/Rheuma',
					  @off,
					  @nil,
					  a),
				menu_item('Labor/Borrelien, Virusstatus',
					  @default,
					  'Labor/Borrelien, Virusstatus',
					  @off,
					  @nil,
					  v),
				menu_item('Punktion/Bakterologie',
					  @default,
					  'Punktion/Bakterologie',
					  @off,
					  @nil,
					  p),
				menu_item('Punktion/Zytologie',
					  @default,
					  'Punktion/Zytologie',
					  @off,
					  @nil,
					  u),
				menu_item('Sono', @default, 'Sono', @off, @nil, s),
				menu_item('R�ntgen',
					  @default,
					  'R�ntgen',
					  @off,
					  @nil,
					  r),
				menu_item('Tomo', @default, 'Tomo', @off, @nil, t),
				menu_item('CT mit Kontrastmittel',
					  @default,
					  'CT mit Kontrastmittel',
					  @off,
					  @nil,
					  c),
				menu_item('CT ohne Kontrastmittel',
					  @default,
					  'CT ohne Kontrastmittel',
					  @off,
					  @nil,
					  o),
				menu_item('Isokinetik',
					  @default,
					  'Isokinetik',
					  @off,
					  @nil,
					  i),
				menu_item('MR mit Kontrastmittel',
					  @default,
					  'MR mit Kontrastmittel',
					  @off,
					  @nil,
					  m),
				menu_item('MR ohne Kontrastmittel',
					  @default,
					  'MR ohne Kontrastmittel',
					  @off,
					  @nil,
					  k),
				menu_item('Szinti', @default, 'Szinti', @off, @nil, z),
				menu_item('Szinti/Leukozyten',
					  @default,
					  'Szinti/Leukozyten',
					  @off,
					  @nil,
					  n)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Beweglichkeit_links_, Beweglichkeit_rechts_),
	     below(Beinachsen, Beweglichkeit_links_),
	     below(Patella, Beinachsen),
	     below(Meniskuszeichen, Patella),
	     below(Seitenbandinstabilit_t, Meniskuszeichen),
	     below(Kreuzbandinstabilit_t, Seitenbandinstabilit_t),
	     below(Weitere_Befunde, Kreuzbandinstabilit_t),
	     below(Diagnostek, Weitere_Befunde)
	   ]
       ]).
dialog(kindliche_knie_diagn_ther,
       [ object        :=
	   Kindliche_Knie_Diagn_Ther,
	 parts         :=
	   [ Kindliche_Knie_Diagn_Ther :=
	       dialog('Kindliche Knie Diagn Ther'),
	     Diagnose                  :=
	       menu(diagnose, toggle),
	     Sonstige_Diagnose         :=
	       text_item('Sonstige Diagnose'),
	     Therapie                  :=
	       menu(therapie, choice)
	   ],
	 modifications :=
	   [ Diagnose          :=
	       [ columns   := 4,
		 reference := point(0, 13),
		 append    := [ menu_item('Osteochondr. diss.',
					  @default,
					  'Osteochondr. diss.',
					  @off,
					  @nil,
					  o),
				menu_item('Scheibenmeniskus',
					  @default,
					  'Scheibenmeniskus',
					  @off,
					  @nil,
					  s),
				menu_item('Poplitealcyste',
					  @default,
					  'Poplitealcyste',
					  @off,
					  @nil,
					  p),
				menu_item('Schlatter',
					  @default,
					  'Schlatter',
					  @off,
					  @nil,
					  c),
				menu_item('femuropatellarer Schmerz',
					  @default,
					  'femuropatellarer Schmerz',
					  @off,
					  @nil,
					  f),
				menu_item('Patellaluxtion',
					  @default,
					  'Patellaluxtion',
					  @off,
					  @nil,
					  a),
				menu_item('ICA', @default, 'ICA', @off, @nil, i),
				menu_item('Normalbefund',
					  @default,
					  'Normalbefund',
					  @off,
					  @nil,
					  n)
			      ]
	       ],
	     Sonstige_Diagnose :=
	       [ length := 30
	       ],
	     Therapie          :=
	       [ columns   := 2,
		 reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item('Observanz',
					  @default,
					  'Observanz',
					  @off,
					  @nil,
					  o),
				menu_item(konservativ,
					  @default,
					  konservativ,
					  @off,
					  @nil,
					  n),
				menu_item(operativ,
					  @default,
					  operativ,
					  @off,
					  @nil,
					  p)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Sonstige_Diagnose, Diagnose),
	     below(Therapie, Sonstige_Diagnose)
	   ]
       ]).
dialog(kindliche_huefte_1,
       [ object        :=
	   Kindliche_Huefte__,
	 parts         :=
	   [ Kindliche_Huefte__                  :=
	       dialog('Kindliche Huefte 1'),
	     Alter___JJ_MM                       :=
	       text_item('Alter / JJ-MM'),
	     Betroffene_H_fte_n_                 :=
	       menu('Betroffene H�fte(n)', choice),
	     Diagnose                            :=
	       menu(diagnose, toggle),
	     Bei_E___andere_Diagnose_            :=
	       text_item('Bei E / andere Diagnose:'),
	     Alter_bei_Erstdiagnose___JJ_MM      :=
	       text_item('Alter bei Erstdiagnose / JJ-MM'),
	     Alter_bei_Behandlungsbeginn___JJ_MM :=
	       text_item('Alter bei Behandlungsbeginn / JJ-MM')
	   ],
	 modifications :=
	   [ Alter___JJ_MM                       :=
	       [ length := 5
	       ],
	     Betroffene_H_fte_n_                 :=
	       [ columns   := 2,
		 reference := point(0, 13),
		 append    := [ menu_item('links/einseitig',
					  @default,
					  'links/einseitig',
					  @off,
					  @nil,
					  l),
				menu_item('links/beidseitig',
					  @default,
					  'links/beidseitig',
					  @off,
					  @nil,
					  i),
				menu_item('rechts/einseitig',
					  @default,
					  'rechts/einseitig',
					  @off,
					  @nil,
					  r),
				menu_item('rechts/beidseitig',
					  @default,
					  'rechts/beidseitig',
					  @off,
					  @nil,
					  e)
			      ]
	       ],
	     Diagnose                            :=
	       [ columns   := 3,
		 reference := point(0, 13),
		 append    := [ menu_item('A / DDH',
					  @default,
					  'A / DDH',
					  @off,
					  @nil,
					  a),
				menu_item('B / SCFE',
					  @default,
					  'B / SCFE',
					  @off,
					  @nil,
					  b),
				menu_item('C / Perthes',
					  @default,
					  'C / Perthes',
					  @off,
					  @nil,
					  c),
				menu_item('D / Sept. Arthritis H�fte',
					  @default,
					  'D / Sept. Arthritis H�fte',
					  @off,
					  @nil,
					  d),
				menu_item('E / andere',
					  @default,
					  'E / andere',
					  @off,
					  @nil,
					  e)
			      ]
	       ],
	     Bei_E___andere_Diagnose_            :=
	       [ length := 30
	       ],
	     Alter_bei_Erstdiagnose___JJ_MM      :=
	       [ length := 5
	       ],
	     Alter_bei_Behandlungsbeginn___JJ_MM :=
	       [ length := 5
	       ]
	   ],
	 layout        :=
	   [ below(Betroffene_H_fte_n_, Alter___JJ_MM),
	     below(Diagnose, Betroffene_H_fte_n_),
	     below(Bei_E___andere_Diagnose_, Diagnose),
	     below(Alter_bei_Erstdiagnose___JJ_MM,
		   Bei_E___andere_Diagnose_),
	     below(Alter_bei_Behandlungsbeginn___JJ_MM,
		   Alter_bei_Erstdiagnose___JJ_MM)
	   ]
       ]).
dialog(k_h_anamnese_1_a,
       [ object        :=
	   K_H_Anamnese___A,
	 parts         :=
	   [ K_H_Anamnese___A               :=
	       dialog('K H Anamnese 1 A'),
	     Vorbehandlung                  :=
	       menu(vorbehandlung, choice),
	     Diagnose_A                     :=
	       label(diagnose_a, 'A'),
	     Wievieltes_Kind                :=
	       menu('Wievieltes Kind', choice),
	     Intrauterine_Lage              :=
	       menu('Intrauterine Lage', choice),
	     Positive_H_fte_FA              :=
	       menu('Positive H�fte FA', choice),
	     Konservative_Therapie          :=
	       menu('Konservative Therapie', choice),
	     Chirurgische_Therapie          :=
	       menu('Chirurgische Therapie', choice),
	     Andere_Erkrankungen            :=
	       menu('Andere Erkrankungen', toggle),
	     Falls_andere_kons__Th___welche :=
	       text_item('Falls andere kons. Th., welche'),
	     Dauer_der_kons__Therapie___WW  :=
	       text_item('Dauer der kons. Therapie / WW'),
	     Falls_andere_Erkr___welche     :=
	       text_item('Falls andere Erkr., welche'),
	     Falls_multiple_Erkr___welche   :=
	       text_item('Falls multiple Erkr., welche'),
	     Falls_andere_chir__Th___welche :=
	       text_item('Falls andere chir. Th., welche')
	   ],
	 modifications :=
	   [ Vorbehandlung                  :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item('konservativ (s.d.)',
					  @default,
					  'konservativ (s.d.)',
					  @off,
					  @nil,
					  o),
				menu_item('chirurgisch (s.d.)',
					  @default,
					  'chirurgisch (s.d.)',
					  @off,
					  @nil,
					  c)
			      ]
	       ],
	     Diagnose_A                     :=
	       [ font := @helvetica_roman_18
	       ],
	     Wievieltes_Kind                :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('1. Kind',
					  @default,
					  '1. Kind',
					  @off,
					  @nil,
					  k),
				menu_item('2. Kind',
					  @default,
					  '2. Kind',
					  @off,
					  @nil,
					  i),
				menu_item('> 2. Kind',
					  @default,
					  '> 2. Kind',
					  @off,
					  @nil,
					  n)
			      ]
	       ],
	     Intrauterine_Lage              :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 alignment := center,
		 reference := point(0, 13),
		 append    := [ menu_item('Sch�dellage',
					  @default,
					  'Sch�dellage',
					  @off,
					  @nil,
					  s),
				menu_item('Stei�lage',
					  @default,
					  'Stei�lage',
					  @off,
					  @nil,
					  t)
			      ]
	       ],
	     Positive_H_fte_FA              :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 alignment := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     Konservative_Therapie          :=
	       [ multiple_selection :=
		   @on,
		 layout             :=
		   vertical,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item('Spreizhose', @default, 'Spreizhose', @off, @nil, s),
		     menu_item('Pavlik', @default, 'Pavlik', @off, @nil, p),
		     menu_item('Extension', @default, 'Extension', @off, @nil, e),
		     menu_item('Gipshose', @default, 'Gipshose', @off, @nil, g),
		     menu_item(andere, @default, andere, @off, @nil, a)
		   ]
	       ],
	     Chirurgische_Therapie          :=
	       [ multiple_selection :=
		   @on,
		 layout             :=
		   vertical,
		 alignment          :=
		   center,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item('Offene Reposition',
			       @default,
			       'Offene Reposition',
			       @off,
			       @nil,
			       o),
		     menu_item('Salter', @default, 'Salter', @off, @nil, s),
		     menu_item('Pemperton', @default, 'Pemperton', @off, @nil, p),
		     menu_item('Chiari', @default, 'Chiari', @off, @nil, c),
		     menu_item('Tripelosteotomie',
			       @default,
			       'Tripelosteotomie',
			       @off,
			       @nil,
			       t),
		     menu_item('Shelf', @default, 'Shelf', @off, @nil, h),
		     menu_item('VDO', @default, 'VDO', @off, @nil, v),
		     menu_item('Derotation', @default, 'Derotation', @off, @nil, d),
		     menu_item('Valg. Osteotomie',
			       @default,
			       'Valg. Osteotomie',
			       @off,
			       @nil,
			       l),
		     menu_item('Verk�rzung', @default, 'Verk�rzung', @off, @nil, r),
		     menu_item('SH-Verl�ngerung',
			       @default,
			       'SH-Verl�ngerung',
			       @off,
			       @nil,
			       e),
		     menu_item('Adduktorentenotomie',
			       @default,
			       'Adduktorentenotomie',
			       @off,
			       @nil,
			       a),
		     menu_item(andere, @default, andere, @off, @nil, n)
		   ]
	       ],
	     Andere_Erkrankungen            :=
	       [ layout    := vertical,
		 alignment := center,
		 reference := point(0, 13),
		 append    := [ menu_item('Keine', @default, 'Keine', @off, @nil, k),
				menu_item('Fu�deformit�t',
					  @default,
					  'Fu�deformit�t',
					  @off,
					  @nil,
					  f),
				menu_item('Torticollis',
					  @default,
					  'Torticollis',
					  @off,
					  @nil,
					  t),
				menu_item('Schr�glagesyndrom',
					  @default,
					  'Schr�glagesyndrom',
					  @off,
					  @nil,
					  s),
				menu_item('Auff�ll. Neuromotorik',
					  @default,
					  'Auff�ll. Neuromotorik',
					  @off,
					  @nil,
					  a),
				menu_item(andere, @default, andere, @off, @nil, n),
				menu_item('multipli Erkr. (Syndrom)',
					  @default,
					  'multipli Erkr. (Syndrom)',
					  @off,
					  @nil,
					  m)
			      ]
	       ],
	     Falls_andere_kons__Th___welche :=
	       [ label := 'andere, welche:'
	       ],
	     Dauer_der_kons__Therapie___WW  :=
	       [ length := 4
	       ],
	     Falls_andere_Erkr___welche     :=
	       [ label := 'andere, welche:'
	       ],
	     Falls_multiple_Erkr___welche   :=
	       [ label := 'multiple, welche:'
	       ],
	     Falls_andere_chir__Th___welche :=
	       [ label     := 'andere, welche:',
		 alignment := left
	       ]
	   ],
	 layout        :=
	   [ area(Vorbehandlung,
		  area(15, 8, 412, 18)),
	     area(Diagnose_A,
		  area(15, 34, 252, 23)),
	     area(Wievieltes_Kind,
		  area(15, 65, 179, 18)),
	     area(Intrauterine_Lage,
		  area(253, 64, 211, 18)),
	     area(Positive_H_fte_FA,
		  area(529, 65, 168, 18)),
	     area(Konservative_Therapie,
		  area(15, 143, 139, 100)),
	     area(Chirurgische_Therapie,
		  area(278, 139, 132, 236)),
	     area(Andere_Erkrankungen,
		  area(533, 147, 139, 134)),
	     area(Falls_andere_kons__Th___welche,
		  area(11, 250, 256, 18)),
	     area(Dauer_der_kons__Therapie___WW,
		  area(12, 277, 216, 18)),
	     area(Falls_andere_Erkr___welche,
		  area(534, 290, 256, 18)),
	     area(Falls_multiple_Erkr___welche,
		  area(535, 313, 262, 18)),
	     area(Falls_andere_chir__Th___welche,
		  area(276, 382, 256, 18))
	   ]
       ]).
dialog(k_h_anamnese_1_b,
       [ object        :=
	   K_H_Anamnese___B,
	 parts         :=
	   [ K_H_Anamnese___B                       :=
	       dialog('K H Anamnese 1 B'),
	     Vorbehandlung                          :=
	       menu(vorbehandlung, choice),
	     Diagnose_B                             :=
	       label(diagnose_b, 'B'),
	     Schmerzdauer                           :=
	       menu(schmerzdauer, choice),
	     Eingabe_des_genauen_Schmerzdauers___WW :=
	       text_item('Eingabe des genauen Schmerzdauers / WW'),
	     Hinken                                 :=
	       menu(hinken, choice),
	     Trauma                                 :=
	       menu(trauma, choice),
	     Dauer_bis____R_ntgen___WW              :=
	       text_item('Dauer bis 1. R�ntgen / WW'),
	     Dauer_bis____Diagnose___WW             :=
	       text_item('Dauer bis 1. Diagnose / WW'),
	     Konservative_Therapie                  :=
	       menu('Konservative Therapie', choice),
	     Chirurgische_Therapie                  :=
	       menu('Chirurgische Therapie', toggle),
	     Falls_andere_kons__Th___welche         :=
	       text_item('Falls andere kons. Th., welche'),
	     Falls_andere_chir__Th___welche         :=
	       text_item('Falls andere chir. Th., welche'),
	     Risikofaktoren                         :=
	       menu(risikofaktoren, toggle)
	   ],
	 modifications :=
	   [ Vorbehandlung                          :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item('konservativ (s.d.)',
					  @default,
					  'konservativ (s.d.)',
					  @off,
					  @nil,
					  o),
				menu_item('chirurgisch (s.d.)',
					  @default,
					  'chirurgisch (s.d.)',
					  @off,
					  @nil,
					  c)
			      ]
	       ],
	     Diagnose_B                             :=
	       [ font := @helvetica_roman_18
	       ],
	     Schmerzdauer                           :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('akut (<3 Wochen)',
					  @default,
					  'akut (<3 Wochen)',
					  @off,
					  @nil,
					  a),
				menu_item('chronisch (>3 Wochen)',
					  @default,
					  'chronisch (>3 Wochen)',
					  @off,
					  @nil,
					  c),
				menu_item('akut auf chronisch (ev. Trauma)',
					  @default,
					  'akut auf chronisch (ev. Trauma)',
					  @off,
					  @nil,
					  k)
			      ]
	       ],
	     Eingabe_des_genauen_Schmerzdauers___WW :=
	       [ length := 3
	       ],
	     Hinken                                 :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     Trauma                                 :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     Dauer_bis____R_ntgen___WW              :=
	       [ length := 3
	       ],
	     Dauer_bis____Diagnose___WW             :=
	       [ length := 3
	       ],
	     Konservative_Therapie                  :=
	       [ multiple_selection :=
		   @on,
		 layout             :=
		   vertical,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item('St�tzkr�cken',
			       @default,
			       'St�tzkr�cken',
			       @off,
			       @nil,
			       s),
		     menu_item(andere, @default, andere, @off, @nil, a)
		   ]
	       ],
	     Chirurgische_Therapie                  :=
	       [ layout    := vertical,
		 alignment := center,
		 reference := point(0, 13),
		 append    := [ menu_item('Verschraubung',
					  @default,
					  'Verschraubung',
					  @off,
					  @nil,
					  v),
				menu_item('Kirschner-Draht',
					  @default,
					  'Kirschner-Draht',
					  @off,
					  @nil,
					  k),
				menu_item('intertroch. Osteotomie',
					  @default,
					  'intertroch. Osteotomie',
					  @off,
					  @nil,
					  i),
				menu_item('SH-Osteotomie (Dunn)',
					  @default,
					  'SH-Osteotomie (Dunn)',
					  @off,
					  @nil,
					  s),
				menu_item(andere, @default, andere, @off, @nil, a)
			      ]
	       ],
	     Falls_andere_kons__Th___welche         :=
	       [ label := 'andere, welche:'
	       ],
	     Falls_andere_chir__Th___welche         :=
	       [ label     := 'andere, welche:',
		 alignment := left
	       ],
	     Risikofaktoren                         :=
	       [ layout    := vertical,
		 alignment := left,
		 reference := point(0, 13),
		 append    := [ menu_item('Adipositas',
					  @default,
					  'Adipositas',
					  @off,
					  @nil,
					  a),
				menu_item('pos. Familienanamnese',
					  @default,
					  'pos. Familienanamnese',
					  @off,
					  @nil,
					  p),
				menu_item('hormonell-endokrine St�rung',
					  @default,
					  'hormonell-endokrine St�rung',
					  @off,
					  @nil,
					  h)
			      ]
	       ]
	   ],
	 layout        :=
	   [ area(Vorbehandlung,
		  area(15, 8, 412, 18)),
	     area(Diagnose_B,
		  area(15, 34, 252, 23)),
	     area(Schmerzdauer,
		  area(15, 65, 303, 18)),
	     area(Eingabe_des_genauen_Schmerzdauers___WW,
		  area(14, 91, 281, 18)),
	     area(Hinken,
		  area(20, 119, 104, 18)),
	     area(Trauma,
		  area(15, 144, 108, 18)),
	     area(Dauer_bis____R_ntgen___WW,
		  area(13, 169, 185, 18)),
	     area(Dauer_bis____Diagnose___WW,
		  area(10, 194, 190, 18)),
	     area(Konservative_Therapie,
		  area(12, 236, 139, 49)),
	     area(Chirurgische_Therapie,
		  area(304, 236, 140, 100)),
	     area(Falls_andere_kons__Th___welche,
		  area(13, 293, 256, 18)),
	     area(Falls_andere_chir__Th___welche,
		  area(303, 345, 256, 18)),
	     area(Risikofaktoren,
		  area(10, 383, 174, 66))
	   ]
       ]).
dialog(k_h_anamnese_1_c,
       [ object        :=
	   K_H_Anamnese___C,
	 parts         :=
	   [ K_H_Anamnese___C                  :=
	       dialog('K H Anamnese 1 C'),
	     Vorbehandlung                     :=
	       menu(vorbehandlung, choice),
	     Diagnose_C                        :=
	       label(diagnose_c, 'C'),
	     Schmerzdauer___WW                 :=
	       text_item('Schmerzdauer / WW'),
	     Hinken                            :=
	       menu(hinken, choice),
	     Trauma                            :=
	       menu(trauma, choice),
	     Dauer_bis____R_ntgen___WW         :=
	       text_item('Dauer bis 1. R�ntgen / WW'),
	     Dauer_bis_Diagnose___WW           :=
	       text_item('Dauer bis Diagnose / WW'),
	     Konservative_Therapie             :=
	       menu('Konservative Therapie', choice),
	     Chirurgische_Therapie             :=
	       menu('Chirurgische Therapie', choice),
	     Falls_andere_kons__Th___welche    :=
	       text_item('Falls andere kons. Th., welche'),
	     Dauer_der_konserv__Therapie___WWW :=
	       text_item('Dauer der konserv. Therapie / WWW'),
	     Falls_andere_chir__Th___welche    :=
	       text_item('Falls andere chir. Th., welche'),
	     Andere_Erkrankungen               :=
	       menu('Andere Erkrankungen', toggle)
	   ],
	 modifications :=
	   [ Vorbehandlung                     :=
	       [ format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item('konservativ (s.d.)',
					  @default,
					  'konservativ (s.d.)',
					  @off,
					  @nil,
					  o),
				menu_item('chirurgisch (s.d.)',
					  @default,
					  'chirurgisch (s.d.)',
					  @off,
					  @nil,
					  c)
			      ]
	       ],
	     Diagnose_C                        :=
	       [ font := @helvetica_roman_18
	       ],
	     Schmerzdauer___WW                 :=
	       [ length := 3
	       ],
	     Hinken                            :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Trauma                            :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Dauer_bis____R_ntgen___WW         :=
	       [ length := 3
	       ],
	     Dauer_bis_Diagnose___WW           :=
	       [ length := 3
	       ],
	     Konservative_Therapie             :=
	       [ multiple_selection :=
		   @on,
		 layout             :=
		   vertical,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item('Atlantaschiene',
			       @default,
			       'Atlantaschiene',
			       @off,
			       @nil,
			       a),
		     menu_item('Thomassplint',
			       @default,
			       'Thomassplint',
			       @off,
			       @nil,
			       t),
		     menu_item('St�tzkr�cken',
			       @default,
			       'St�tzkr�cken',
			       @off,
			       @nil,
			       s),
		     menu_item(andere, @default, andere, @off, @nil, n)
		   ]
	       ],
	     Chirurgische_Therapie             :=
	       [ multiple_selection :=
		   @on,
		 layout             :=
		   vertical,
		 alignment          :=
		   center,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item('varis. OT', @default, 'varis. OT', @off, @nil, v),
		     menu_item('Salter', @default, 'Salter', @off, @nil, s),
		     menu_item('Chian', @default, 'Chian', @off, @nil, c),
		     menu_item('Pfannendachplastik',
			       @default,
			       'Pfannendachplastik',
			       @off,
			       @nil,
			       p),
		     menu_item(andere, @default, andere, @off, @nil, a)
		   ]
	       ],
	     Falls_andere_kons__Th___welche    :=
	       [ label := 'andere, welche:'
	       ],
	     Dauer_der_konserv__Therapie___WWW :=
	       [ length := 4
	       ],
	     Falls_andere_chir__Th___welche    :=
	       [ label     := 'andere, welche:',
		 alignment := right
	       ],
	     Andere_Erkrankungen               :=
	       [ layout    := vertical,
		 reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item('hamatolog. Erkrankung',
					  @default,
					  'hamatolog. Erkrankung',
					  @off,
					  @nil,
					  h),
				menu_item('transiente Synovitis',
					  @default,
					  'transiente Synovitis',
					  @off,
					  @nil,
					  t),
				menu_item('Epiphyseale Dysplasie',
					  @default,
					  'Epiphyseale Dysplasie',
					  @off,
					  @nil,
					  e)
			      ]
	       ]
	   ],
	 layout        :=
	   [ area(Vorbehandlung,
		  area(15, 8, 511, 18)),
	     area(Diagnose_C,
		  area(15, 34, 252, 23)),
	     area(Schmerzdauer___WW,
		  area(15, 65, 146, 18)),
	     area(Hinken,
		  area(89, 89, 104, 18)),
	     area(Trauma,
		  area(85, 115, 108, 18)),
	     area(Dauer_bis____R_ntgen___WW,
		  area(15, 143, 185, 18)),
	     area(Dauer_bis_Diagnose___WW,
		  area(26, 170, 175, 18)),
	     area(Konservative_Therapie,
		  area(13, 217, 139, 83)),
	     area(Chirurgische_Therapie,
		  area(323, 223, 132, 100)),
	     area(Falls_andere_kons__Th___welche,
		  area(12, 306, 256, 18)),
	     area(Dauer_der_konserv__Therapie___WWW,
		  area(10, 334, 246, 18)),
	     area(Falls_andere_chir__Th___welche,
		  area(321, 331, 256, 18)),
	     area(Andere_Erkrankungen,
		  area(11, 374, 138, 83))
	   ]
       ]).
dialog(k_h_anamnese_1_d,
       [ object        :=
	   K_H_Anamnese___D,
	 parts         :=
	   [ K_H_Anamnese___D         :=
	       dialog('K H Anamnese 1 D'),
	     Vorbehandlung            :=
	       menu(vorbehandlung, choice),
	     Diagnose_D               :=
	       label(diagnose_d, 'D'),
	     Geb                      :=
	       label(geb, 'Geburt:'),
	     Gestationsalter___WW     :=
	       text_item('Gestationsalter / WW'),
	     Gewicht_____Stell__in_g  :=
	       text_item('Gewicht / 4-Stell. in g'),
	     Gr__e_____Stell__in_cm   :=
	       text_item('Gr��e / 2-Stell. in cm'),
	     Ven_ser_Zugang           :=
	       menu('Ven�ser Zugang', choice),
	     Respirator               :=
	       menu(respirator, choice),
	     Respiratordauer___WW     :=
	       text_item('Respiratordauer / WW'),
	     Befall_sonstiger_Gelenke :=
	       menu('Befall sonstiger Gelenke', choice),
	     Falls_ja__welche         :=
	       text_item('Falls ja, welche'),
	     Andere_Infekte           :=
	       menu('Andere Infekte', toggle),
	     Andere_Erkrankungen      :=
	       menu('Andere Erkrankungen', toggle),
	     Falls_Syndrom__welches   :=
	       text_item('Falls Syndrom, welches'),
	     Bisherige_Therapie       :=
	       menu('Bisherige Therapie', choice),
	     Falls_andere__welche1    :=
	       text_item('Falls andere, welche'),
	     Keimnachweis             :=
	       menu(keimnachweis, choice),
	     Keim                     :=
	       menu(keim, choice),
	     Falls_andere__welche2    :=
	       text_item('Falls andere, welche')
	   ],
	 modifications :=
	   [ Vorbehandlung            :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item('konservativ (s.d.)',
					  @default,
					  'konservativ (s.d.)',
					  @off,
					  @nil,
					  o),
				menu_item('chirurgisch (s.d.)',
					  @default,
					  'chirurgisch (s.d.)',
					  @off,
					  @nil,
					  c)
			      ]
	       ],
	     Diagnose_D               :=
	       [ font := @helvetica_roman_18
	       ],
	     Geb                      :=
	       [ font := @helvetica_roman_18
	       ],
	     Gestationsalter___WW     :=
	       [ length := 2
	       ],
	     Gewicht_____Stell__in_g  :=
	       [ length    := 4,
		 alignment := center
	       ],
	     Gr__e_____Stell__in_cm   :=
	       [ length    := 2,
		 alignment := right
	       ],
	     Ven_ser_Zugang           :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j),
				menu_item('zentral (NAK,NVK)',
					  @default,
					  'zentral (NAK,NVK)',
					  @off,
					  @nil,
					  z)
			      ]
	       ],
	     Respirator               :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Respiratordauer___WW     :=
	       [ length := 2
	       ],
	     Befall_sonstiger_Gelenke :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Andere_Infekte           :=
	       [ layout    := vertical,
		 columns   := 2,
		 reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item('Nabel', @default, 'Nabel', @off, @nil, n),
				menu_item('HNO', @default, 'HNO', @off, @nil, h),
				menu_item('Pulmo', @default, 'Pulmo', @off, @nil, p),
				menu_item('ZNS', @default, 'ZNS', @off, @nil, z),
				menu_item('Darm', @default, 'Darm', @off, @nil, d),
				menu_item('Harnwege',
					  @default,
					  'Harnwege',
					  @off,
					  @nil,
					  r),
				menu_item('Haut', @default, 'Haut', @off, @nil, u),
				menu_item('Neugeborenensepsis',
					  @default,
					  'Neugeborenensepsis',
					  @off,
					  @nil,
					  e),
				menu_item(andere, @default, andere, @off, @nil, a)
			      ]
	       ],
	     Andere_Erkrankungen      :=
	       [ layout    := vertical,
		 alignment := center,
		 reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item('Immundefekt',
					  @default,
					  'Immundefekt',
					  @off,
					  @nil,
					  i),
				menu_item('Syndrom',
					  @default,
					  'Syndrom',
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Falls_Syndrom__welches   :=
	       [ label     := 'Syndrom, welches:',
		 alignment := center,
		 reference := point(0, -69)
	       ],
	     Bisherige_Therapie       :=
	       [ multiple_selection :=
		   @on,
		 layout             :=
		   vertical,
		 alignment          :=
		   right,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item(keine, @default, keine, @off, @nil, k),
		     menu_item('Gipshose bzw. Ruhigstellung',
			       @default,
			       'Gipshose bzw. Ruhigstellung',
			       @off,
			       @nil,
			       g),
		     menu_item('Antibiotika', @default, 'Antibiotika', @off, @nil, a),
		     menu_item('Punktion', @default, 'Punktion', @off, @nil, p),
		     menu_item('Gelenkser�ffnung',
			       @default,
			       'Gelenkser�ffnung',
			       @off,
			       @nil,
			       e)
		   ]
	       ],
	     Falls_andere__welche2    :=
	       [ label := 'andere, welche:'
	       ],
	     Keimnachweis             :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('nicht durchgef�hrt',
					  @default,
					  'nicht durchgef�hrt',
					  @off,
					  @nil,
					  n),
				menu_item('kein Wachstum',
					  @default,
					  'kein Wachstum',
					  @off,
					  @nil,
					  k),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Keim                     :=
	       [ multiple_selection :=
		   @on,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item('Staphylokokken',
			       @default,
			       'Staphylokokken',
			       @off,
			       @nil,
			       s),
		     menu_item('Streptokokken',
			       @default,
			       'Streptokokken',
			       @off,
			       @nil,
			       t),
		     menu_item('H�mophilus', @default, 'H�mophilus', @off, @nil, h),
		     menu_item('E. coli', @default, 'E. coli', @off, @nil, e),
		     menu_item(andere, @default, andere, @off, @nil, a)
		   ]
	       ],
	     Falls_andere__welche1    :=
	       [ label := 'andere, welche:'
	       ]
	   ],
	 layout        :=
	   [ area(Vorbehandlung,
		  area(15, 8, 481, 18)),
	     area(Diagnose_D,
		  area(14, 39, 252, 23)),
	     area(Geb,
		  area(13, 67, 252, 23)),
	     area(Gestationsalter___WW,
		  area(15, 96, 153, 18)),
	     area(Gewicht_____Stell__in_g,
		  area(367, 97, 174, 18)),
	     area(Gr__e_____Stell__in_cm,
		  area(684, 92, 160, 18)),
	     area(Ven_ser_Zugang,
		  area(15, 122, 239, 18)),
	     area(Respirator,
		  area(99, 148, 126, 18)),
	     area(Respiratordauer___WW,
		  area(364, 148, 156, 18)),
	     area(Befall_sonstiger_Gelenke,
		  area(15, 174, 209, 18)),
	     area(Falls_ja__welche,
		  area(365, 175, 258, 18)),
	     area(Andere_Infekte,
		  area(32, 227, 255, 100)),
	     area(Andere_Erkrankungen,
		  area(369, 225, 129, 66)),
	     area(Falls_Syndrom__welches,
		  area(371, 301, 278, 18)),
	     area(Bisherige_Therapie,
		  area(696, 217, 170, 100)),
	     area(Falls_andere__welche2,
		  area(30, 335, 256, 18)),
	     area(Keimnachweis,
		  area(34, 379, 228, 18)),
	     area(Keim,
		  area(92, 416, 682, 18)),
	     area(Falls_andere__welche1,
		  area(30, 453, 256, 18))
	   ]
       ]).
dialog(k_h_anamnese_1_e,
       [ object        :=
	   K_H_Anamnese___E,
	 parts         :=
	   [ K_H_Anamnese___E          :=
	       dialog('K H Anamnese 1 E'),
	     Vorbehandlung             :=
	       menu(vorbehandlung, choice),
	     Diagnose_E                :=
	       label(diagnose_e, 'E'),
	     Schmerzdauer___WW         :=
	       text_item('Schmerzdauer / WW'),
	     Hinken                    :=
	       menu(hinken, choice),
	     Histo                     :=
	       menu(histo, choice),
	     Falls_ja__welche          :=
	       text_item('Falls ja, welche'),
	     Genese                    :=
	       menu(genese, choice),
	     Falls_anderes__welches    :=
	       text_item('Falls anderes, welches'),
	     Lokalisation              :=
	       menu(lokalisation, choice),
	     Dauer_bis____R_ntgen___WW :=
	       text_item('Dauer bis 1. R�ntgen / WW'),
	     Dauer_bis_Diagnose___WW   :=
	       text_item('Dauer bis Diagnose / WW'),
	     Konservative_Therapie     :=
	       text_item('Konservative Therapie'),
	     Chirurgische_Therapie     :=
	       text_item('Chirurgische Therapie'),
	     Befunde                   :=
	       text_item(befunde)
	   ],
	 modifications :=
	   [ Vorbehandlung             :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item('konservativ (s.d.)',
					  @default,
					  'konservativ (s.d.)',
					  @off,
					  @nil,
					  o),
				menu_item('chirurgisch (s.d.)',
					  @default,
					  'chirurgisch (s.d.)',
					  @off,
					  @nil,
					  c)
			      ]
	       ],
	     Diagnose_E                :=
	       [ font := @helvetica_roman_18
	       ],
	     Schmerzdauer___WW         :=
	       [ length := 2
	       ],
	     Hinken                    :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Histo                     :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Falls_ja__welche          :=
	       [ alignment := center
	       ],
	     Genese                    :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('Trauma', @default, 'Trauma', @off, @nil, t),
				menu_item('Tumor', @default, 'Tumor', @off, @nil, u),
				menu_item('Entz�ndung',
					  @default,
					  'Entz�ndung',
					  @off,
					  @nil,
					  e),
				menu_item(anderes,
					  @default,
					  anderes,
					  @off,
					  @nil,
					  a)
			      ]
	       ],
	     Falls_anderes__welches    :=
	       [ alignment := right
	       ],
	     Lokalisation              :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('Ilium', @default, 'Ilium', @off, @nil, i),
				menu_item('Pubis', @default, 'Pubis', @off, @nil, p),
				menu_item('Ischium',
					  @default,
					  'Ischium',
					  @off,
					  @nil,
					  c),
				menu_item('Kopf', @default, 'Kopf', @off, @nil, k),
				menu_item('SH-Hals',
					  @default,
					  'SH-Hals',
					  @off,
					  @nil,
					  s),
				menu_item('intertroch.',
					  @default,
					  'intertroch.',
					  @off,
					  @nil,
					  n),
				menu_item('Gelenk bzw. Synovialis',
					  @default,
					  'Gelenk bzw. Synovialis',
					  @off,
					  @nil,
					  g),
				menu_item('Weichteile',
					  @default,
					  'Weichteile',
					  @off,
					  @nil,
					  w)
			      ]
	       ],
	     Dauer_bis____R_ntgen___WW :=
	       [ length := 2
	       ],
	     Dauer_bis_Diagnose___WW   :=
	       [ length := 2
	       ]
	   ],
	 layout        :=
	   [ below(Diagnose_E, Vorbehandlung),
	     below(Schmerzdauer___WW, Diagnose_E),
	     below(Hinken, Schmerzdauer___WW),
	     below(Histo, Hinken),
	     right(Falls_ja__welche, Histo),
	     below(Genese, Histo),
	     right(Falls_anderes__welches, Genese),
	     below(Lokalisation, Genese),
	     below(Dauer_bis____R_ntgen___WW, Lokalisation),
	     below(Dauer_bis_Diagnose___WW, Dauer_bis____R_ntgen___WW),
	     below(Konservative_Therapie, Dauer_bis_Diagnose___WW),
	     below(Chirurgische_Therapie, Konservative_Therapie),
	     below(Befunde, Chirurgische_Therapie)
	   ]
       ]).
dialog(k_h_klinik_1_vor,
       [ object        :=
	   K_H_Klinik___Vor,
	 parts         :=
	   [ K_H_Klinik___Vor      :=
	       dialog('K H Klinik 1 Vor'),
	     Vor_Gehbeginn         :=
	       label(vor_gehbeginn, 'Vor Gehbeginn'),
	     A__klinische_Zeichen_ :=
	       menu('A (klinische Zeichen)', toggle),
	     D                     :=
	       menu('D', toggle),
	     Falls_andere__welche  :=
	       text_item('Falls andere, welche'),
	     Abduktion_pr_op       :=
	       menu('Abduktion pr�op', choice)
	   ],
	 modifications :=
	   [ Vor_Gehbeginn         :=
	       [ font := @helvetica_roman_18
	       ],
	     A__klinische_Zeichen_ :=
	       [ columns   := 2,
		 reference := point(0, 13),
		 append    := [ menu_item('Ortolani pos.',
					  @default,
					  'Ortolani pos.',
					  @off,
					  @nil,
					  o),
				menu_item('auff�llige Faltenasymmetrie',
					  @default,
					  'auff�llige Faltenasymmetrie',
					  @off,
					  @nil,
					  a),
				menu_item('auff�llige Beinverk�rzung',
					  @default,
					  'auff�llige Beinverk�rzung',
					  @off,
					  @nil,
					  u)
			      ]
	       ],
	     D                     :=
	       [ columns   := 3,
		 reference := point(0, 13),
		 append    := [ menu_item('Schonhaltung, Pseudoparalyse',
					  @default,
					  'Schonhaltung, Pseudoparalyse',
					  @off,
					  @nil,
					  s),
				menu_item('lokaler Schmerz',
					  @default,
					  'lokaler Schmerz',
					  @off,
					  @nil,
					  l),
				menu_item('reduzierter AZ (Appetit,Exsikkose...)',
					  @default,
					  'reduzierter AZ (Appetit,Exsikkose...)',
					  @off,
					  @nil,
					  r),
				menu_item('Fieber', @default, 'Fieber', @off, @nil, f),
				menu_item(andere, @default, andere, @off, @nil, a)
			      ]
	       ],
	     Falls_andere__welche  :=
	       [ length := 30
	       ],
	     Abduktion_pr_op       :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item(' 3 normal ( >70� )',
					  @default,
					  ' 3 normal ( >70� )',
					  @off,
					  @nil,
					  n),
				menu_item('2 ( 30-70� )',
					  @default,
					  '2 ( 30-70� )',
					  @off,
					  @nil,
					  �),
				'1 ( <30� )'
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(A__klinische_Zeichen_, Vor_Gehbeginn),
	     below(D, A__klinische_Zeichen_),
	     below(Falls_andere__welche, D),
	     below(Abduktion_pr_op, Falls_andere__welche)
	   ]
       ]).
dialog(k_h_klinik_1_nach,
       [ object        :=
	   K_H_Klinik___Nach,
	 parts         :=
	   [ K_H_Klinik___Nach            :=
	       dialog('K H Klinik 1 Nach'),
	     Nach_Gehbeginn               :=
	       label(nach_gehbeginn, 'Nach Gehbeginn'),
	     Schmerz_pr_op                :=
	       menu('Schmerz pr�op', choice),
	     Hinken_pr_op                 :=
	       menu('Hinken pr�op', choice),
	     Flexion_pr_op                :=
	       menu('Flexion pr�op', choice),
	     Trendelenburg_pr_op          :=
	       menu('Trendelenburg pr�op', choice),
	     Extens_def__pr_op            :=
	       menu('Extens.def. pr�op', choice),
	     Beinl_ngendiff__pr_op        :=
	       menu('Beinl�ngendiff. pr�op', choice),
	     Add__pr_op                   :=
	       menu('Add. pr�op', choice),
	     Gehsrecke_pr_op              :=
	       menu('Gehsrecke pr�op', choice),
	     Abd__pr_op                   :=
	       menu('Abd. pr�op', choice),
	     Gehhilfe_pr_op               :=
	       menu('Gehhilfe pr�op', choice),
	     IR_pr_op                     :=
	       menu('IR pr�op', choice),
	     Sitzen_pr_op                 :=
	       menu('Sitzen pr�op', choice),
	     AR_pr_op                     :=
	       menu('AR pr�op', choice),
	     Sport_pr_op                  :=
	       menu('Sport pr�op', choice),
	     Klin__Scor_pr_op______Stell_ :=
	       text_item('Klin. Scor pr�op: / 3-Stell.')
	   ],
	 modifications :=
	   [ Nach_Gehbeginn               :=
	       [ font := @helvetica_roman_18
	       ],
	     Schmerz_pr_op                :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('12 keiner',
					  @default,
					  '12 keiner',
					  @off,
					  @nil,
					  k),
				menu_item('6 leicht',
					  @default,
					  '6 leicht',
					  @off,
					  @nil,
					  l),
				menu_item('0 stark',
					  @default,
					  '0 stark',
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Hinken_pr_op                 :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('6 kein', @default, '6 kein', @off, @nil, k),
				menu_item('4 m��ig',
					  @default,
					  '4 m��ig',
					  @off,
					  @nil,
					  m),
				menu_item('1 stark',
					  @default,
					  '1 stark',
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Flexion_pr_op                :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 90-120�',
					  @default,
					  '1 90-120�',
					  @off,
					  @nil,
					  �),
				'0 <90�'
			      ]
	       ],
	     Trendelenburg_pr_op          :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('6 negativ',
					  @default,
					  '6 negativ',
					  @off,
					  @nil,
					  n),
				menu_item('4 positiv n. 30 sec.',
					  @default,
					  '4 positiv n. 30 sec.',
					  @off,
					  @nil,
					  p),
				menu_item('1 sofort positiv',
					  @default,
					  '1 sofort positiv',
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Extens_def__pr_op            :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 10-30�',
					  @default,
					  '1 10-30�',
					  @off,
					  @nil,
					  �),
				'0 >30�'
			      ]
	       ],
	     Beinl_ngendiff__pr_op        :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('6 keine',
					  @default,
					  '6 keine',
					  @off,
					  @nil,
					  k),
				menu_item('4 1-2cm',
					  @default,
					  '4 1-2cm',
					  @off,
					  @nil,
					  c),
				menu_item('1 >2cm', @default, '1 >2cm', @off, @nil, m)
			      ]
	       ],
	     Add__pr_op                   :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 10-30�',
					  @default,
					  '1 10-30�',
					  @off,
					  @nil,
					  �),
				'0 <10�'
			      ]
	       ],
	     Gehsrecke_pr_op              :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('12 unbegrenzt',
					  @default,
					  '12 unbegrenzt',
					  @off,
					  @nil,
					  u),
				menu_item('8 eingeschr�nkt',
					  @default,
					  '8 eingeschr�nkt',
					  @off,
					  @nil,
					  e),
				menu_item('4 nur zu Hause',
					  @default,
					  '4 nur zu Hause',
					  @off,
					  @nil,
					  n),
				menu_item('0 nicht gehf�hig',
					  @default,
					  '0 nicht gehf�hig',
					  @off,
					  @nil,
					  i)
			      ]
	       ],
	     Abd__pr_op                   :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 10-30�',
					  @default,
					  '1 10-30�',
					  @off,
					  @nil,
					  �),
				'0 <10�'
			      ]
	       ],
	     Gehhilfe_pr_op               :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('12 keine',
					  @default,
					  '12 keine',
					  @off,
					  @nil,
					  k),
				menu_item('8 1-Kr�cke',
					  @default,
					  '8 1-Kr�cke',
					  @off,
					  @nil,
					  �),
				menu_item('4 2-Kr�cke',
					  @default,
					  '4 2-Kr�cke',
					  @off,
					  @nil,
					  c),
				menu_item('0 Rollst�hl',
					  @default,
					  '0 Rollst�hl',
					  @off,
					  @nil,
					  r)
			      ]
	       ],
	     IR_pr_op                     :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 10-30�',
					  @default,
					  '1 10-30�',
					  @off,
					  @nil,
					  �),
				'0 <10�'
			      ]
	       ],
	     Sitzen_pr_op                 :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 spezielle (erh�hter) Sitz',
					  @default,
					  '1 spezielle (erh�hter) Sitz',
					  @off,
					  @nil,
					  s),
				menu_item('0 unm�glich',
					  @default,
					  '0 unm�glich',
					  @off,
					  @nil,
					  u)
			      ]
	       ],
	     AR_pr_op                     :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 10-30�',
					  @default,
					  '1 10-30�',
					  @off,
					  @nil,
					  �),
				'0 <10�'
			      ]
	       ],
	     Sport_pr_op                  :=
	       [ feedback  := show_selection_only,
		 format    := center,
		 reference := point(0, 13),
		 append    := [ menu_item('3 ja', @default, '3 ja', @off, @nil, j),
				menu_item('1 eingeschr�nkt',
					  @default,
					  '1 eingeschr�nkt',
					  @off,
					  @nil,
					  e),
				menu_item('0 keiner',
					  @default,
					  '0 keiner',
					  @off,
					  @nil,
					  k)
			      ]
	       ],
	     Klin__Scor_pr_op______Stell_ :=
	       [ length := 3
	       ]
	   ],
	 layout        :=
	   [ below(Schmerz_pr_op, Nach_Gehbeginn),
	     right(Hinken_pr_op, Schmerz_pr_op),
	     below(Flexion_pr_op, Schmerz_pr_op),
	     below(Trendelenburg_pr_op, Hinken_pr_op),
	     right(Trendelenburg_pr_op, Flexion_pr_op),
	     below(Extens_def__pr_op, Flexion_pr_op),
	     below(Beinl_ngendiff__pr_op, Trendelenburg_pr_op),
	     right(Beinl_ngendiff__pr_op, Extens_def__pr_op),
	     below(Add__pr_op, Extens_def__pr_op),
	     below(Gehsrecke_pr_op, Beinl_ngendiff__pr_op),
	     right(Gehsrecke_pr_op, Add__pr_op),
	     below(Abd__pr_op, Add__pr_op),
	     below(Gehhilfe_pr_op, Gehsrecke_pr_op),
	     right(Gehhilfe_pr_op, Abd__pr_op),
	     below(IR_pr_op, Abd__pr_op),
	     below(Sitzen_pr_op, Gehhilfe_pr_op),
	     right(Sitzen_pr_op, IR_pr_op),
	     below(AR_pr_op, IR_pr_op),
	     below(Sport_pr_op, Sitzen_pr_op),
	     right(Sport_pr_op, AR_pr_op),
	     below(Klin__Scor_pr_op______Stell_, AR_pr_op)
	   ]
       ]).
dialog(k_h_s_u_r_1_vor,
       [ object        :=
	   K_H_S_U_R___Vor,
	 parts         :=
	   [ K_H_S_U_R___Vor             :=
	       dialog('K H S U R 1 Vor'),
	     Vor_Gehbeginn__             :=
	       label(vor_gehbeginn_1, 'Vor Gehbeginn'),
	     A_Sono_Vor__                :=
	       label(a_sono_vor_1, 'A'),
	     Sono_Huefttyp_Pr            :=
	       label(sono_huefttyp_pr, 'Sonographie H�fttyp pr�op'),
	     ___US_1                     :=
	       menu('1. US:', choice),
	     ___US_2                     :=
	       menu('2. US:', choice),
	     ___US_3                     :=
	       menu('3. US:', choice),
	     AC_pr_op_____Stell__in_Grad :=
	       text_item('AC pr�op / 2-Stell. in Grad'),
	     CE_pr_op_____Stell__in_Grad :=
	       text_item('CE pr�op / 2-Stell. in Grad')
	   ],
	 modifications :=
	   [ Vor_Gehbeginn__             :=
	       [ font := @helvetica_roman_18
	       ],
	     A_Sono_Vor__                :=
	       [ font := @helvetica_roman_18
	       ],
	     Sono_Huefttyp_Pr            :=
	       [ font := @helvetica_roman_14
	       ],
	     ___US_3                     :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Ia, Ib od. IIa+',
					  @default,
					  'Ia, Ib od. IIa+',
					  @off,
					  @nil,
					  i),
				menu_item('IIa- od. IIb',
					  @default,
					  'IIa- od. IIb',
					  @off,
					  @nil,
					  o),
				menu_item('IIc od. g',
					  @default,
					  'IIc od. g',
					  @off,
					  @nil,
					  g),
				menu_item('D', @default, 'D', @off, @nil, d),
				menu_item('III', @default, 'III'),
				menu_item('IV', @default, 'IV', @off, @nil, v)
			      ]
	       ],
	     ___US_2                     :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Ia, Ib od. IIa+',
					  @default,
					  'Ia, Ib od. IIa+',
					  @off,
					  @nil,
					  i),
				menu_item('IIa- od. IIb',
					  @default,
					  'IIa- od. IIb',
					  @off,
					  @nil,
					  o),
				menu_item('IIc od. g',
					  @default,
					  'IIc od. g',
					  @off,
					  @nil,
					  g),
				menu_item('D', @default, 'D', @off, @nil, d),
				menu_item('III', @default, 'III'),
				menu_item('IV', @default, 'IV', @off, @nil, v)
			      ]
	       ],
	     ___US_1                     :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Ia, Ib od. IIa+',
					  @default,
					  'Ia, Ib od. IIa+',
					  @off,
					  @nil,
					  i),
				menu_item('IIa- od. IIb',
					  @default,
					  'IIa- od. IIb',
					  @off,
					  @nil,
					  o),
				menu_item('IIC od. g',
					  @default,
					  'IIC od. g',
					  @off,
					  @nil,
					  g),
				menu_item('D', @default, 'D', @off, @nil, d),
				menu_item('III', @default, 'III'),
				menu_item('IV', @default, 'IV', @off, @nil, v)
			      ]
	       ],
	     AC_pr_op_____Stell__in_Grad :=
	       [ length := 2
	       ],
	     CE_pr_op_____Stell__in_Grad :=
	       [ length := 2
	       ]
	   ],
	 layout        :=
	   [ below(A_Sono_Vor__, Vor_Gehbeginn__),
	     below(Sono_Huefttyp_Pr, A_Sono_Vor__),
	     below(___US_3, Sono_Huefttyp_Pr),
	     below(___US_2, ___US_3),
	     below(___US_1, ___US_2),
	     below(AC_pr_op_____Stell__in_Grad, ___US_1),
	     below(CE_pr_op_____Stell__in_Grad,
		   AC_pr_op_____Stell__in_Grad)
	   ]
       ]).
dialog(k_h_s_u_r_1_nach,
       [ object        :=
	   K_H_S_U_R___Nach,
	 parts         :=
	   [ K_H_S_U_R___Nach                                  :=
	       dialog('K H S U R 1 Nach'),
	     Nach_Gehbeginn__                                  :=
	       label(nach_gehbeginn_2, 'Nach Gehbeginn'),
	     A_E_Sono_Nach__                                   :=
	       label(a_e_sono_nach_1, 'A-E'),
	     Ullmann_Sharp_pr_op_____Stell__in_Grad            :=
	       text_item('Ullmann/Sharp pr�op / 2-Stell. in Grad'),
	     CE_pr_op                                          :=
	       menu('CE pr�op', choice),
	     Eingabe_von_CE_pr_op_____Stell__in_Grad           :=
	       text_item('Eingabe von CE pr�op / 2-Stell. in Grad'),
	     Eingabe_von_CCD_pr_op_____Stell__in_Grad          :=
	       text_item('Eingabe von CCD pr�op / 3-Stell. in Grad'),
	     Eingabe_von_ATD_pt_op_____Stell__in_mm            :=
	       text_item('Eingabe von ATD pt�op / 2-Stell. in mm'),
	     Osteoarthritis_pr_op                              :=
	       menu('Osteoarthritis pr�op', choice),
	     Sonographie_Ergu__pr_op                           :=
	       menu('Sonographie Ergu� pr�op', choice),
	     Falls_ja__wieviel_Seitendiff______Stell__in____mm :=
	       text_item('Falls ja, wieviel Seitendiff. / 2-Stell. in
			mm'),
	     Arthrographie_pr_op                               :=
	       menu('Arthrographie pr�op', choice)
	   ],
	 modifications :=
	   [ Nach_Gehbeginn__                                  :=
	       [ font := @helvetica_roman_18
	       ],
	     A_E_Sono_Nach__                                   :=
	       [ font := @helvetica_roman_18
	       ],
	     Ullmann_Sharp_pr_op_____Stell__in_Grad            :=
	       [ length := 2
	       ],
	     CE_pr_op                                          :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('normal (>25�)',
					  @default,
					  'normal (>25�)',
					  @off,
					  @nil,
					  n),
				menu_item('20-25�', @default, '20-25�', @off, @nil, �),
				'<20�'
			      ]
	       ],
	     Eingabe_von_CE_pr_op_____Stell__in_Grad           :=
	       [ length := 2
	       ],
	     Eingabe_von_CCD_pr_op_____Stell__in_Grad          :=
	       [ length := 3
	       ],
	     Eingabe_von_ATD_pt_op_____Stell__in_mm            :=
	       [ length := 2
	       ],
	     Osteoarthritis_pr_op                              :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('2 m��ig',
					  @default,
					  '2 m��ig',
					  @off,
					  @nil,
					  m),
				menu_item('1 schwer',
					  @default,
					  '1 schwer',
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Sonographie_Ergu__pr_op                           :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Falls_ja__wieviel_Seitendiff______Stell__in____mm :=
	       [ length := 2
	       ],
	     Arthrographie_pr_op                               :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(A_E_Sono_Nach__, Nach_Gehbeginn__),
	     below(Ullmann_Sharp_pr_op_____Stell__in_Grad,
		   A_E_Sono_Nach__),
	     below(CE_pr_op, Ullmann_Sharp_pr_op_____Stell__in_Grad),
	     below(Eingabe_von_CE_pr_op_____Stell__in_Grad, CE_pr_op),
	     below(Eingabe_von_CCD_pr_op_____Stell__in_Grad,
		   Eingabe_von_CE_pr_op_____Stell__in_Grad),
	     below(Eingabe_von_ATD_pt_op_____Stell__in_mm,
		   Eingabe_von_CCD_pr_op_____Stell__in_Grad),
	     below(Osteoarthritis_pr_op,
		   Eingabe_von_ATD_pt_op_____Stell__in_mm),
	     below(Sonographie_Ergu__pr_op, Osteoarthritis_pr_op),
	     below(Falls_ja__wieviel_Seitendiff______Stell__in____mm,
		   Sonographie_Ergu__pr_op),
	     below(Arthrographie_pr_op,
		   Falls_ja__wieviel_Seitendiff______Stell__in____mm)
	   ]
       ]).
dialog(k_h_s_u_r_1_a,
       [ object        :=
	   K_H_S_U_R___A,
	 parts         :=
	   [ K_H_S_U_R___A                               :=
	       dialog('K H S U R 1 A'),
	     A_Sono__                                    :=
	       label(a_sono_1, 'A'),
	     H_ftlux__n__T_nnis                          :=
	       menu('H�ftlux. n. T�nnis', choice),
	     HKN_n__T_nnis_pr_op                         :=
	       menu('HKN n. T�nnis pr�op', choice),
	     Repos__grad_n__T_nnis_pr_op__Arthrographie_ :=
	       menu('Repos.-grad n. T�nnis pr�op (Arthrographie)', choice),
	     AVN_Ogden_Buchholz                          :=
	       menu('AVN Ogden-Buchholz', choice),
	     Lat_Index_pr_op_____Stell_                  :=
	       text_item('Lat.Index pr�op / 2-Stell.'),
	     Migr___pr_op_____Stell__in_Prozente         :=
	       text_item('Migr. % pr�op / 2-Stell. in Prozente'),
	     Tr_nenfigur_verbr_                          :=
	       menu('Tr�nenfigur verbr.', choice),
	     Falls_ja__wieviel_____Stell__in_mm          :=
	       text_item('Falls ja, wieviel / 2-Stell. in mm'),
	     Kopf_Tr_nenf__Dist______Stell__in_mm        :=
	       text_item('Kopf-Tr�nenf.-Dist. / 2-Stell. in mm')
	   ],
	 modifications :=
	   [ A_Sono__                                    :=
	       [ font := @helvetica_roman_18
	       ],
	     H_ftlux__n__T_nnis                          :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('I�', @default, 'I�', @off, @nil, i),
				menu_item('II�', @default, 'II�', @off, @nil, �),
				'III�',
				menu_item('IV�', @default, 'IV�', @off, @nil, v)
			      ]
	       ],
	     HKN_n__T_nnis_pr_op                         :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(0, @default, '0', @off, @nil, '0'),
				menu_item('I�', @default, 'I�', @off, @nil, i),
				menu_item('II�', @default, 'II�', @off, @nil, �),
				'III�',
				menu_item('IV�', @default, 'IV�', @off, @nil, v)
			      ]
	       ],
	     Repos__grad_n__T_nnis_pr_op__Arthrographie_ :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Grad I', @default, 'Grad I', @off, @nil, g),
				menu_item('Grad II',
					  @default,
					  'Grad II',
					  @off,
					  @nil,
					  r),
				menu_item('Grad III',
					  @default,
					  'Grad III',
					  @off,
					  @nil,
					  i)
			      ]
	       ],
	     AVN_Ogden_Buchholz                          :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Typ 0', @default, 'Typ 0', @off, @nil, t),
				menu_item('Typ I', @default, 'Typ I', @off, @nil, y),
				menu_item('Typ II', @default, 'Typ II', @off, @nil, i),
				menu_item('Typ III',
					  @default,
					  'Typ III',
					  @off,
					  @nil,
					  p),
				menu_item('Typ IV', @default, 'Typ IV', @off, @nil, v)
			      ]
	       ],
	     Lat_Index_pr_op_____Stell_                  :=
	       [ length := 2
	       ],
	     Migr___pr_op_____Stell__in_Prozente         :=
	       [ length := 2
	       ],
	     Tr_nenfigur_verbr_                          :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Falls_ja__wieviel_____Stell__in_mm          :=
	       [ length := 2
	       ],
	     Kopf_Tr_nenf__Dist______Stell__in_mm        :=
	       [ length := 2
	       ]
	   ],
	 layout        :=
	   [ below(H_ftlux__n__T_nnis, A_Sono__),
	     below(HKN_n__T_nnis_pr_op, H_ftlux__n__T_nnis),
	     below(Repos__grad_n__T_nnis_pr_op__Arthrographie_,
		   HKN_n__T_nnis_pr_op),
	     below(AVN_Ogden_Buchholz,
		   Repos__grad_n__T_nnis_pr_op__Arthrographie_),
	     below(Lat_Index_pr_op_____Stell_, AVN_Ogden_Buchholz),
	     below(Migr___pr_op_____Stell__in_Prozente,
		   Lat_Index_pr_op_____Stell_),
	     below(Tr_nenfigur_verbr_,
		   Migr___pr_op_____Stell__in_Prozente),
	     below(Falls_ja__wieviel_____Stell__in_mm, Tr_nenfigur_verbr_),
	     below(Kopf_Tr_nenf__Dist______Stell__in_mm,
		   Falls_ja__wieviel_____Stell__in_mm)
	   ]
       ]).
dialog(k_h_s_u_r_1_b,
       [ object        :=
	   K_H_S_U_R___B,
	 parts         :=
	   [ K_H_S_U_R___B                                            :=
	       dialog('K H S U R 1 B'),
	     B_Sono__                                                 :=
	       label(b_sono_1, 'B'),
	     K_H_Winkel_pr_op_axial                                   :=
	       menu('K-H-Winkel pr�op axial', choice),
	     Eingabe_von_K_H_Winkel_pr_op_axial_____Stell__in____Grad :=
	       text_item('Eingabe von K-H-Winkel pr�op axial / 2-Stell. in
			Grad'),
	     Eingabe_von_KE_Y_Winkel_ap_____Stell__in_____Grad        :=
	       text_item('Eingabe von KE-Y-Winkel ap / 2-Stell. in 
			Grad')
	   ],
	 modifications :=
	   [ B_Sono__                                                 :=
	       [ font := @helvetica_roman_18
	       ],
	     K_H_Winkel_pr_op_axial                                   :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('leicht (<30�)',
					  @default,
					  'leicht (<30�)',
					  @off,
					  @nil,
					  l),
				menu_item('mittel (30-60�)',
					  @default,
					  'mittel (30-60�)',
					  @off,
					  @nil,
					  m),
				menu_item('schwer (>60�)',
					  @default,
					  'schwer (>60�)',
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Eingabe_von_K_H_Winkel_pr_op_axial_____Stell__in____Grad :=
	       [ length := 2
	       ],
	     Eingabe_von_KE_Y_Winkel_ap_____Stell__in_____Grad        :=
	       [ length := 2
	       ]
	   ],
	 layout        :=
	   [ below(K_H_Winkel_pr_op_axial, B_Sono__),
	     below(Eingabe_von_K_H_Winkel_pr_op_axial_____Stell__in____Grad,
		   K_H_Winkel_pr_op_axial),
	     below(Eingabe_von_KE_Y_Winkel_ap_____Stell__in_____Grad,
		   Eingabe_von_K_H_Winkel_pr_op_axial_____Stell__in____Grad)
	   ]
       ]).
dialog(k_h_s_u_r_1_c,
       [ object        :=
	   K_H_S_U_R___C,
	 parts         :=
	   [ K_H_S_U_R___C                   :=
	       dialog('K H S U R 1 C'),
	     C_Sono__                        :=
	       label(c_sono_1, 'C'),
	     Catterall_pr_op                 :=
	       menu('Catterall pr�op', choice),
	     Salter_Thompson_pr_op           :=
	       menu('Salter Thompson pr�op', choice),
	     LP_Classif__n__Herring          :=
	       menu('LP Classif. n. Herring', choice),
	     Tr_nenfigur_verbr_              :=
	       menu('Tr�nenfigur verbr.', choice),
	     Falls_ja__wieviel_____Stell__mm :=
	       text_item('Falls ja, wieviel / 2-Stell. mm'),
	     Mose_Krit__pr_op                :=
	       menu('Mose Krit. pr�op', choice)
	   ],
	 modifications :=
	   [ C_Sono__                        :=
	       [ font := @helvetica_roman_18
	       ],
	     Catterall_pr_op                 :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('I', @default, 'I', @off, @nil, i),
				menu_item('II', @default, 'II'),
				menu_item('III', @default, 'III'),
				menu_item('IV', @default, 'IV', @off, @nil, v)
			      ]
	       ],
	     Salter_Thompson_pr_op           :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('Typ A', @default, 'Typ A', @off, @nil, t),
				menu_item('Typ B', @default, 'Typ B', @off, @nil, y)
			      ]
	       ],
	     LP_Classif__n__Herring          :=
	       [ reference := point(0, 13),
		 append    := [ menu_item('A', @default, 'A', @off, @nil, a),
				menu_item('B <9 J', @default, 'B <9 J', @off, @nil, b),
				menu_item('B >9 J', @default, 'B >9 J', @off, @nil, j),
				menu_item('C', @default, 'C', @off, @nil, c)
			      ]
	       ],
	     Tr_nenfigur_verbr_              :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Falls_ja__wieviel_____Stell__mm :=
	       [ length := 2
	       ],
	     Mose_Krit__pr_op                :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(gut, @default, gut, @off, @nil, g),
				menu_item(m��ig, @default, m��ig, @off, @nil, m),
				menu_item(schlecht,
					  @default,
					  schlecht,
					  @off,
					  @nil,
					  s)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Catterall_pr_op, C_Sono__),
	     below(Salter_Thompson_pr_op, Catterall_pr_op),
	     below(LP_Classif__n__Herring, Salter_Thompson_pr_op),
	     below(Tr_nenfigur_verbr_, LP_Classif__n__Herring),
	     below(Falls_ja__wieviel_____Stell__mm, Tr_nenfigur_verbr_),
	     below(Mose_Krit__pr_op, Falls_ja__wieviel_____Stell__mm)
	   ]
       ]).
dialog(k_h_s_u_r_1_d,
       [ object        :=
	   K_H_S_U_R___D,
	 parts         :=
	   [ K_H_S_U_R___D              :=
	       dialog('K H S U R 1 D'),
	     D_Sono__                   :=
	       label(d_sono_1, 'D'),
	     Hunka_Klassifikation_pr_op :=
	       menu('Hunka Klassifikation pr�op', choice)
	   ],
	 modifications :=
	   [ D_Sono__                   := [ font := @helvetica_roman_18
					   ],
	     Hunka_Klassifikation_pr_op := [ reference := point(0, 13),
					     append    := [ menu_item('I',
								      @default,
								      'I',
								      @off,
								      @nil,
								      i),
							    menu_item('IIA',
								      @default,
								      'IIA',
								      @off,
								      @nil,
								      a),
							    menu_item('IIB',
								      @default,
								      'IIB',
								      @off,
								      @nil,
								      b),
							    menu_item('III',
								      @default,
								      'III'),
							    menu_item('IVA',
								      @default,
								      'IVA'),
							    menu_item('IVB',
								      @default,
								      'IVB'),
							    menu_item('V',
								      @default,
								      'V',
								      @off,
								      @nil,
								      v)
							  ]
					   ]
	   ],
	 layout        :=
	   [ below(Hunka_Klassifikation_pr_op, D_Sono__)
	   ]
       ]).
dialog(k_h_s_u_r_1_ae,
       [ object        :=
	   K_H_S_U_R___Ae,
	 parts         :=
	   [ K_H_S_U_R___Ae :=
	       dialog('K H S U R 1 Ae'),
	     A_E_Sono       :=
	       label(a_e_sono, 'A-E'),
	     MRI_pr_op      :=
	       menu('MRI pr�op', choice),
	     CT_pr_op       :=
	       menu('CT pr�op', choice),
	     Szinti         :=
	       menu('Szinti', choice)
	   ],
	 modifications :=
	   [ A_E_Sono  := [ font := @helvetica_roman_18
			  ],
	     MRI_pr_op := [ reference := point(0, 13),
			    append    := [ menu_item(nein,
						     @default,
						     nein,
						     @off,
						     @nil,
						     n),
					   menu_item(ja,
						     @default,
						     ja,
						     @off,
						     @nil,
						     j)
					 ]
			  ],
	     CT_pr_op  := [ reference := point(0, 13),
			    append    := [ menu_item(nein,
						     @default,
						     nein,
						     @off,
						     @nil,
						     n),
					   menu_item(ja,
						     @default,
						     ja,
						     @off,
						     @nil,
						     j)
					 ]
			  ],
	     Szinti    := [ reference := point(0, 13),
			    append    := [ menu_item(nein,
						     @default,
						     nein,
						     @off,
						     @nil,
						     n),
					   menu_item(ja,
						     @default,
						     ja,
						     @off,
						     @nil,
						     j)
					 ]
			  ]
	   ],
	 layout        :=
	   [ below(MRI_pr_op, A_E_Sono),
	     below(CT_pr_op, MRI_pr_op),
	     below(Szinti, CT_pr_op)
	   ]
       ]).
dialog(k_h_klinik_2,
       [ object        :=
	   K_H_Klinik__,
	 parts         :=
	   [ K_H_Klinik__             :=
	       dialog('K H Klinik 2'),
	     Beweglichkeit            :=
	       label(beweglichkeit, 'BEWEGLICHKEIT'),
	     Klin__Score_______Stell_ :=
	       text_item('klin. Score : / 3-Stell.'),
	     Flexion                  :=
	       menu('Flexion', choice),
	     Beinl_ngendifferenz      :=
	       menu('Beinl�ngendifferenz', choice),
	     Extens_def_              :=
	       menu('Extens.def.', choice),
	     Hinken                   :=
	       menu('Hinken', choice),
	     Add_                     :=
	       menu('Add.', choice),
	     Gehhilfe                 :=
	       menu('Gehhilfe', choice),
	     Abd_                     :=
	       menu('Abd.', choice),
	     Schmerz                  :=
	       menu('Schmerz', choice),
	     Ir                       :=
	       menu(ir, choice),
	     Gehsrecke                :=
	       menu('Gehsrecke', choice),
	     Ar                       :=
	       menu(ar, choice),
	     Sitzen                   :=
	       menu('Sitzen', choice),
	     Trendelenburg            :=
	       menu('Trendelenburg', choice),
	     Sport                    :=
	       menu('Sport', choice)
	   ],
	 modifications :=
	   [ Beweglichkeit            :=
	       [ font := @helvetica_roman_18
	       ],
	     Klin__Score_______Stell_ :=
	       [ length := 3
	       ],
	     Flexion                  :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 90-120�',
					  @default,
					  '1 90-120�',
					  @off,
					  @nil,
					  �),
				'0 <90�'
			      ]
	       ],
	     Beinl_ngendifferenz      :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('6 keine',
					  @default,
					  '6 keine',
					  @off,
					  @nil,
					  k),
				menu_item('4 1-2cm',
					  @default,
					  '4 1-2cm',
					  @off,
					  @nil,
					  c),
				menu_item('1 >2cm', @default, '1 >2cm', @off, @nil, m)
			      ]
	       ],
	     Extens_def_              :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 10-30�',
					  @default,
					  '1 10-30�',
					  @off,
					  @nil,
					  �),
				'0 >30�'
			      ]
	       ],
	     Hinken                   :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('6 kein', @default, '6 kein', @off, @nil, k),
				menu_item('4 m��ig',
					  @default,
					  '4 m��ig',
					  @off,
					  @nil,
					  m),
				menu_item('1 stark',
					  @default,
					  '1 stark',
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Add_                     :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 10-30�',
					  @default,
					  '1 10-30�',
					  @off,
					  @nil,
					  �),
				'0 <10�'
			      ]
	       ],
	     Gehhilfe                 :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('12 keine',
					  @default,
					  '12 keine',
					  @off,
					  @nil,
					  k),
				menu_item('8 1-Kr�cke',
					  @default,
					  '8 1-Kr�cke',
					  @off,
					  @nil,
					  �),
				menu_item('4 2-Kr�cke',
					  @default,
					  '4 2-Kr�cke',
					  @off,
					  @nil,
					  c),
				menu_item('0 Rollst�hl',
					  @default,
					  '0 Rollst�hl',
					  @off,
					  @nil,
					  r)
			      ]
	       ],
	     Abd_                     :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 10-30�',
					  @default,
					  '1 10-30�',
					  @off,
					  @nil,
					  �),
				'0 <10�'
			      ]
	       ],
	     Schmerz                  :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('12 keiner',
					  @default,
					  '12 keiner',
					  @off,
					  @nil,
					  k),
				menu_item('6 leicht',
					  @default,
					  '6 leicht',
					  @off,
					  @nil,
					  l),
				menu_item('0 stark',
					  @default,
					  '0 stark',
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Ir                       :=
	       [ label     := 'IR:',
		 feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 10-30�',
					  @default,
					  '1 10-30�',
					  @off,
					  @nil,
					  �),
				'0 <10�'
			      ]
	       ],
	     Gehsrecke                :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('12 unbegrenzt',
					  @default,
					  '12 unbegrenzt',
					  @off,
					  @nil,
					  u),
				menu_item('8 eingeschr�nkt',
					  @default,
					  '8 eingeschr�nkt',
					  @off,
					  @nil,
					  e),
				menu_item('4 nur zu Hause',
					  @default,
					  '4 nur zu Hause',
					  @off,
					  @nil,
					  n),
				menu_item('0 nicht gehf�hig',
					  @default,
					  '0 nicht gehf�hig',
					  @off,
					  @nil,
					  i)
			      ]
	       ],
	     Ar                       :=
	       [ label     := 'AR:',
		 feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 10-30�',
					  @default,
					  '1 10-30�',
					  @off,
					  @nil,
					  �),
				'0 <10�'
			      ]
	       ],
	     Sitzen                   :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('1 spezielle (erh�hter) Sitz',
					  @default,
					  '1 spezielle (erh�hter) Sitz',
					  @off,
					  @nil,
					  s),
				menu_item('0 unm�glich',
					  @default,
					  '0 unm�glich',
					  @off,
					  @nil,
					  u)
			      ]
	       ],
	     Trendelenburg            :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('6 negativ',
					  @default,
					  '6 negativ',
					  @off,
					  @nil,
					  n),
				menu_item('4 positiv n. 30 sec.',
					  @default,
					  '4 positiv n. 30 sec.',
					  @off,
					  @nil,
					  p),
				menu_item('1 sofort positiv',
					  @default,
					  '1 sofort positiv',
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Sport                    :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('3 ja', @default, '3 ja', @off, @nil, j),
				menu_item('1 eingeschr�nkt',
					  @default,
					  '1 eingeschr�nkt',
					  @off,
					  @nil,
					  e),
				menu_item('0 keiner',
					  @default,
					  '0 keiner',
					  @off,
					  @nil,
					  k)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Klin__Score_______Stell_, Beweglichkeit),
	     below(Flexion, Klin__Score_______Stell_),
	     right(Beinl_ngendifferenz, Flexion),
	     below(Extens_def_, Flexion),
	     below(Hinken, Beinl_ngendifferenz),
	     right(Hinken, Extens_def_),
	     below(Add_, Extens_def_),
	     below(Gehhilfe, Hinken),
	     right(Gehhilfe, Add_),
	     below(Abd_, Add_),
	     below(Schmerz, Gehhilfe),
	     right(Schmerz, Abd_),
	     below(Ir, Abd_),
	     below(Gehsrecke, Schmerz),
	     right(Gehsrecke, Ir),
	     below(Ar, Ir),
	     below(Sitzen, Gehsrecke),
	     right(Sitzen, Ar),
	     below(Trendelenburg, Ar),
	     below(Sport, Sitzen),
	     right(Sport, Trendelenburg)
	   ]
       ]).
dialog(k_h_sono_u_roent_2_ae,
       [ object        :=
	   K_H_Sono_U_Roent___Ae,
	 parts         :=
	   [ K_H_Sono_U_Roent___Ae                             :=
	       dialog('K H Sono U Roent 2 Ae'),
	     A_E_Sono_U_Roent__                                :=
	       label(a_e_sono_u_roent_2, 'A-E'),
	     Ullmann_Sharp_____Stell__in_Grad                  :=
	       text_item('Ullmann/Sharp / 2-Stell. in Grad'),
	     Ce                                                :=
	       menu('CE', choice),
	     Eingabe_von_CE_____Stell__in_Grad                 :=
	       text_item('Eingabe von CE / 2-Stell. in Grad'),
	     Eingabe_von_CCD_____Stell__in_Grad                :=
	       text_item('Eingabe von CCD / 3-Stell. in Grad'),
	     Eingabe_von_ATD_____Stell__in_mm                  :=
	       text_item('Eingabe von ATD / 2-Stell. in mm'),
	     Osteoarthritis                                    :=
	       menu('Osteoarthritis', choice),
	     Sonographie_Ergu_                                 :=
	       menu('Sonographie Ergu�', choice),
	     Falls_ja__wieviel_Seitendiff______Stell__in____mm :=
	       text_item('Falls ja, wieviel Seitendiff. / 2-Stell. in
			mm'),
	     Arthrographie                                     :=
	       menu('Arthrographie', choice)
	   ],
	 modifications :=
	   [ A_E_Sono_U_Roent__                                :=
	       [ font := @helvetica_roman_18
	       ],
	     Ullmann_Sharp_____Stell__in_Grad                  :=
	       [ length := 2
	       ],
	     Ce                                                :=
	       [ label     := 'CE:',
		 feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('normal (>25�)',
					  @default,
					  'normal (>25�)',
					  @off,
					  @nil,
					  n),
				menu_item('20-25�', @default, '20-25�', @off, @nil, �),
				'<20�'
			      ]
	       ],
	     Eingabe_von_CE_____Stell__in_Grad                 :=
	       [ length := 2
	       ],
	     Eingabe_von_CCD_____Stell__in_Grad                :=
	       [ length := 3
	       ],
	     Eingabe_von_ATD_____Stell__in_mm                  :=
	       [ length := 2
	       ],
	     Osteoarthritis                                    :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('2 m��ig',
					  @default,
					  '2 m��ig',
					  @off,
					  @nil,
					  m),
				menu_item('1 schwer',
					  @default,
					  '1 schwer',
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Sonographie_Ergu_                                 :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Falls_ja__wieviel_Seitendiff______Stell__in____mm :=
	       [ length := 2
	       ],
	     Arthrographie                                     :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Ullmann_Sharp_____Stell__in_Grad, A_E_Sono_U_Roent__),
	     below(Ce, Ullmann_Sharp_____Stell__in_Grad),
	     below(Eingabe_von_CE_____Stell__in_Grad, Ce),
	     below(Eingabe_von_CCD_____Stell__in_Grad,
		   Eingabe_von_CE_____Stell__in_Grad),
	     below(Eingabe_von_ATD_____Stell__in_mm,
		   Eingabe_von_CCD_____Stell__in_Grad),
	     below(Osteoarthritis, Eingabe_von_ATD_____Stell__in_mm),
	     below(Sonographie_Ergu_, Osteoarthritis),
	     below(Falls_ja__wieviel_Seitendiff______Stell__in____mm,
		   Sonographie_Ergu_),
	     below(Arthrographie,
		   Falls_ja__wieviel_Seitendiff______Stell__in____mm)
	   ]
       ]).
dialog(k_h_s_u_r_2_a,
       [ object        :=
	   K_H_S_U_R___A,
	 parts         :=
	   [ K_H_S_U_R___A                        :=
	       dialog('K H S U R 2 A'),
	     A_Sono__                             :=
	       label(a_sono_2, 'A'),
	     H_ftlux__n__T_nnis                   :=
	       menu('H�ftlux. n. T�nnis', choice),
	     HKN_n__T_nnis                        :=
	       menu('HKN n. T�nnis', choice),
	     Repos__grad_n__T_nnis                :=
	       menu('Repos.-grad n. T�nnis', choice),
	     AVN_Ogden_Buchholz                   :=
	       menu('AVN Ogden-Buchholz', choice),
	     Lat_Index_____Stell_                 :=
	       text_item('Lat.Index / 2-Stell.'),
	     Migr_______Stell__in_Prozente        :=
	       text_item('Migr. % / 2-Stell. in Prozente'),
	     Tr_nenfigur_verbr_                   :=
	       menu('Tr�nenfigur verbr.', choice),
	     Falls_ja__wieviel_____Stell__in_mm   :=
	       text_item('Falls ja, wieviel / 2-Stell. in mm'),
	     Kopf_Tr_nenf__Dist______Stell__in_mm :=
	       text_item('Kopf-Tr�nenf.-Dist. / 2-Stell. in mm')
	   ],
	 modifications :=
	   [ A_Sono__                             :=
	       [ font := @helvetica_roman_18
	       ],
	     H_ftlux__n__T_nnis                   :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('I�', @default, 'I�', @off, @nil, i),
				menu_item('II�', @default, 'II�', @off, @nil, �),
				'III�',
				menu_item('IV�', @default, 'IV�', @off, @nil, v)
			      ]
	       ],
	     HKN_n__T_nnis                        :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(0, @default, '0', @off, @nil, '0'),
				menu_item('I�', @default, 'I�', @off, @nil, i),
				menu_item('II�', @default, 'II�', @off, @nil, �),
				'III�',
				menu_item('IV�', @default, 'IV�', @off, @nil, v)
			      ]
	       ],
	     Repos__grad_n__T_nnis                :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('Grad I', @default, 'Grad I', @off, @nil, g),
				menu_item('Grad II',
					  @default,
					  'Grad II',
					  @off,
					  @nil,
					  r),
				menu_item('Grad III',
					  @default,
					  'Grad III',
					  @off,
					  @nil,
					  i)
			      ]
	       ],
	     AVN_Ogden_Buchholz                   :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('Typ 0', @default, 'Typ 0', @off, @nil, t),
				menu_item('Typ I', @default, 'Typ I', @off, @nil, y),
				menu_item('Typ II', @default, 'Typ II', @off, @nil, i),
				menu_item('Typ III',
					  @default,
					  'Typ III',
					  @off,
					  @nil,
					  p),
				menu_item('Typ IV', @default, 'Typ IV', @off, @nil, v)
			      ]
	       ],
	     Lat_Index_____Stell_                 :=
	       [ length := 2
	       ],
	     Migr_______Stell__in_Prozente        :=
	       [ length := 2
	       ],
	     Tr_nenfigur_verbr_                   :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Falls_ja__wieviel_____Stell__in_mm   :=
	       [ length := 2
	       ],
	     Kopf_Tr_nenf__Dist______Stell__in_mm :=
	       [ length := 2
	       ]
	   ],
	 layout        :=
	   [ below(H_ftlux__n__T_nnis, A_Sono__),
	     below(HKN_n__T_nnis, H_ftlux__n__T_nnis),
	     below(Repos__grad_n__T_nnis, HKN_n__T_nnis),
	     below(AVN_Ogden_Buchholz, Repos__grad_n__T_nnis),
	     below(Lat_Index_____Stell_, AVN_Ogden_Buchholz),
	     below(Migr_______Stell__in_Prozente, Lat_Index_____Stell_),
	     below(Tr_nenfigur_verbr_, Migr_______Stell__in_Prozente),
	     below(Falls_ja__wieviel_____Stell__in_mm, Tr_nenfigur_verbr_),
	     below(Kopf_Tr_nenf__Dist______Stell__in_mm,
		   Falls_ja__wieviel_____Stell__in_mm)
	   ]
       ]).
dialog(k_h_s_u_r_2_b,
       [ object        :=
	   K_H_S_U_R___B,
	 parts         :=
	   [ K_H_S_U_R___B                                      :=
	       dialog('K H S U R 2 B'),
	     B_Sono__                                           :=
	       label(b_sono_2, 'B'),
	     Kopf_Hals_Winkel                                   :=
	       menu('Kopf-Hals-Winkel', choice),
	     Eingabe_von_Kopf_Hals_Winkel_____Stell__in____Grad :=
	       text_item('Eingabe von Kopf-Hals-Winkel / 2-Stell. in
			Grad'),
	     Eingabe_von_KE_Y_Winkel_ap_____Stell__in_____Grad  :=
	       text_item('Eingabe von KE-Y-Winkel ap / 2-Stell. in 
			Grad')
	   ],
	 modifications :=
	   [ B_Sono__                                           :=
	       [ font := @helvetica_roman_18
	       ],
	     Kopf_Hals_Winkel                                   :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('leicht (<30�)',
					  @default,
					  'leicht (<30�)',
					  @off,
					  @nil,
					  l),
				menu_item('mittel (30-60�)',
					  @default,
					  'mittel (30-60�)',
					  @off,
					  @nil,
					  m),
				menu_item('schwer (>60�)',
					  @default,
					  'schwer (>60�)',
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Eingabe_von_Kopf_Hals_Winkel_____Stell__in____Grad :=
	       [ length := 2
	       ],
	     Eingabe_von_KE_Y_Winkel_ap_____Stell__in_____Grad  :=
	       [ length := 2
	       ]
	   ],
	 layout        :=
	   [ below(Kopf_Hals_Winkel, B_Sono__),
	     below(Eingabe_von_Kopf_Hals_Winkel_____Stell__in____Grad,
		   Kopf_Hals_Winkel),
	     below(Eingabe_von_KE_Y_Winkel_ap_____Stell__in_____Grad,
		   Eingabe_von_Kopf_Hals_Winkel_____Stell__in____Grad)
	   ]
       ]).
dialog(k_h_s_u_r_2_c,
       [ object        :=
	   K_H_S_U_R___C,
	 parts         :=
	   [ K_H_S_U_R___C                   :=
	       dialog('K H S U R 2 C'),
	     C_Sono__                        :=
	       label(c_sono_2, 'C'),
	     Catterall                       :=
	       menu('Catterall', choice),
	     Salter_Thompson                 :=
	       menu('Salter Thompson', choice),
	     LP_Classif__n__Herring          :=
	       menu('LP Classif. n. Herring', choice),
	     Tr_nenfigur_verbr_              :=
	       menu('Tr�nenfigur verbr.', choice),
	     Falls_ja__wieviel_____Stell__mm :=
	       text_item('Falls ja, wieviel / 2-Stell. mm'),
	     Mose_Krit_                      :=
	       menu('Mose Krit.', choice)
	   ],
	 modifications :=
	   [ C_Sono__                        :=
	       [ font := @helvetica_roman_18
	       ],
	     Catterall                       :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('I', @default, 'I', @off, @nil, i),
				menu_item('II', @default, 'II'),
				menu_item('III', @default, 'III'),
				menu_item('IV', @default, 'IV', @off, @nil, v)
			      ]
	       ],
	     Salter_Thompson                 :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('Typ A', @default, 'Typ A', @off, @nil, t),
				menu_item('Typ B', @default, 'Typ B', @off, @nil, y)
			      ]
	       ],
	     LP_Classif__n__Herring          :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('A', @default, 'A', @off, @nil, a),
				menu_item('B <9 J', @default, 'B <9 J', @off, @nil, b),
				menu_item('B >9 J', @default, 'B >9 J', @off, @nil, j),
				menu_item('C', @default, 'C', @off, @nil, c)
			      ]
	       ],
	     Tr_nenfigur_verbr_              :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Falls_ja__wieviel_____Stell__mm :=
	       [ length := 2
	       ],
	     Mose_Krit_                      :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(gut, @default, gut, @off, @nil, g),
				menu_item(m��ig, @default, m��ig, @off, @nil, m),
				menu_item(schlecht,
					  @default,
					  schlecht,
					  @off,
					  @nil,
					  s)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Catterall, C_Sono__),
	     below(Salter_Thompson, Catterall),
	     below(LP_Classif__n__Herring, Salter_Thompson),
	     below(Tr_nenfigur_verbr_, LP_Classif__n__Herring),
	     below(Falls_ja__wieviel_____Stell__mm, Tr_nenfigur_verbr_),
	     below(Mose_Krit_, Falls_ja__wieviel_____Stell__mm)
	   ]
       ]).
dialog(k_h_s_u_r_2_d,
       [ object        :=
	   K_H_S_U_R___D,
	 parts         :=
	   [ K_H_S_U_R___D        :=
	       dialog('K H S U R 2 D'),
	     D_Sono__             :=
	       label(d_sono_2, 'D'),
	     Hunka_Klassifikation :=
	       menu('Hunka Klassifikation', choice)
	   ],
	 modifications :=
	   [ D_Sono__             := [ font := @helvetica_roman_18
				     ],
	     Hunka_Klassifikation := [ reference := point(0, 13),
				       append    := [ menu_item('I',
								@default,
								'I',
								@off,
								@nil,
								i),
						      menu_item('IIA',
								@default,
								'IIA',
								@off,
								@nil,
								a),
						      menu_item('IIB',
								@default,
								'IIB',
								@off,
								@nil,
								b),
						      menu_item('III',
								@default,
								'III'),
						      menu_item('IVA',
								@default,
								'IVA'),
						      menu_item('IVB',
								@default,
								'IVB'),
						      menu_item('V',
								@default,
								'V',
								@off,
								@nil,
								v)
						    ]
				     ]
	   ],
	 layout        :=
	   [ below(Hunka_Klassifikation, D_Sono__)
	   ]
       ]).
dialog(k_h_therapie_2_a,
       [ object        :=
	   K_H_Therapie___A,
	 parts         :=
	   [ K_H_Therapie___A           :=
	       dialog('K H Therapie 2 A'),
	     Durchgef_hrte_Therapie     :=
	       menu('durchgef�hrte Therapie', choice),
	     Therap_A                   :=
	       label(therap_a, 'A'),
	     Konservative_Therapie      :=
	       menu('konservative Therapie', choice),
	     Chirurgische_Therapie      :=
	       menu('chirurgische Therapie', choice),
	     Falls_andere__welche1      :=
	       text_item('Falls andere, welche'),
	     Dauer_der_kons__Ther____WW :=
	       text_item('Dauer der kons. Ther. / WW'),
	     Falls_andere__welche2      :=
	       text_item('Falls andere, welche'),
	     Operateur                  :=
	       text_item(operateur),
	     OP_Datum___MM_JJ           :=
	       text_item('OP Datum / MM.JJ')
	   ],
	 modifications :=
	   [ Durchgef_hrte_Therapie     :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item('konservativ (s.d.)',
					  @default,
					  'konservativ (s.d.)',
					  @off,
					  @nil,
					  o),
				menu_item('chirurgisch (s.d.)',
					  @default,
					  'chirurgisch (s.d.)',
					  @off,
					  @nil,
					  c)
			      ]
	       ],
	     Therap_A                   :=
	       [ font := @helvetica_roman_18
	       ],
	     Konservative_Therapie      :=
	       [ multiple_selection :=
		   @on,
		 layout             :=
		   vertical,
		 append             :=
		   [ menu_item('Spreizhose', @default, 'Spreizhose', @off, @nil, s),
		     menu_item('Pavlik', @default, 'Pavlik', @off, @nil, p),
		     menu_item('Extension', @default, 'Extension', @off, @nil, e),
		     menu_item('Gipshose', @default, 'Gipshose', @off, @nil, g),
		     menu_item(andere, @default, andere, @off, @nil, a)
		   ]
	       ],
	     Chirurgische_Therapie      :=
	       [ multiple_selection :=
		   @on,
		 layout             :=
		   vertical,
		 columns            :=
		   2,
		 alignment          :=
		   right,
		 append             :=
		   [ menu_item('Offene Reposition',
			       @default,
			       'Offene Reposition',
			       @off,
			       @nil,
			       o),
		     menu_item('Salter', @default, 'Salter', @off, @nil, s),
		     menu_item('Pemberton', @default, 'Pemberton', @off, @nil, p),
		     menu_item('Chiari', @default, 'Chiari', @off, @nil, c),
		     menu_item('Tripelosteotomie',
			       @default,
			       'Tripelosteotomie',
			       @off,
			       @nil,
			       t),
		     menu_item('Shelf', @default, 'Shelf', @off, @nil, h),
		     menu_item('VDO', @default, 'VDO', @off, @nil, v),
		     menu_item('Derotation', @default, 'Derotation', @off, @nil, d),
		     menu_item('Valg. Osteotomie',
			       @default,
			       'Valg. Osteotomie',
			       @off,
			       @nil,
			       l),
		     menu_item('Verk�rzung', @default, 'Verk�rzung', @off, @nil, r),
		     menu_item('SH-Verl�ngerung',
			       @default,
			       'SH-Verl�ngerung',
			       @off,
			       @nil,
			       e),
		     menu_item('Adduktorentenotomie',
			       @default,
			       'Adduktorentenotomie',
			       @off,
			       @nil,
			       a),
		     menu_item('Psoastenotomie',
			       @default,
			       'Psoastenotomie',
			       @off,
			       @nil,
			       n),
		     menu_item(andere, @default, andere)
		   ]
	       ],
	     Falls_andere__welche2      :=
	       [ label := 'andere, welche:'
	       ],
	     Dauer_der_kons__Ther____WW :=
	       [ length := 2
	       ],
	     Falls_andere__welche1      :=
	       [ label := 'andere, welche:'
	       ],
	     OP_Datum___MM_JJ           :=
	       [ length := 7
	       ]
	   ],
	 layout        :=
	   [ area(Durchgef_hrte_Therapie,
		  area(15, 8, 464, 18)),
	     area(Therap_A,
		  area(15, 34, 252, 23)),
	     area(Konservative_Therapie,
		  area(15, 65, 136, 100)),
	     area(Chirurgische_Therapie,
		  area(328, 69, 253, 134)),
	     area(Falls_andere__welche2,
		  area(16, 176, 256, 18)),
	     area(Dauer_der_kons__Ther____WW,
		  area(17, 205, 190, 18)),
	     area(Falls_andere__welche1,
		  area(329, 212, 256, 18)),
	     area(Operateur,
		  area(15, 285, 227, 18)),
	     area(OP_Datum___MM_JJ,
		  area(329, 285, 172, 18))
	   ]
       ]).
dialog(k_h_therapie_2_b,
       [ object        :=
	   K_H_Therapie___B,
	 parts         :=
	   [ K_H_Therapie___B       :=
	       dialog('K H Therapie 2 B'),
	     Durchgef_hrte_Therapie :=
	       menu('durchgef�hrte Therapie', choice),
	     Therap_B               :=
	       label(therap_b, 'B'),
	     Konservative_Therapie  :=
	       menu('konservative Therapie', choice),
	     Chirurgische_Therapie  :=
	       menu('chirurgische Therapie', choice),
	     Falls_andere__welche1  :=
	       text_item('Falls andere, welche'),
	     Falls_andere__welche2  :=
	       text_item('Falls andere, welche'),
	     Operateur              :=
	       text_item(operateur),
	     OP_Datum___MM_JJ       :=
	       text_item('OP Datum / MM.JJ')
	   ],
	 modifications :=
	   [ Durchgef_hrte_Therapie :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item('konservativ (s.d.)',
					  @default,
					  'konservativ (s.d.)',
					  @off,
					  @nil,
					  o),
				menu_item('chirurgisch (s.d.)',
					  @default,
					  'chirurgisch (s.d.)',
					  @off,
					  @nil,
					  c)
			      ]
	       ],
	     Therap_B               :=
	       [ font := @helvetica_roman_18
	       ],
	     Konservative_Therapie  :=
	       [ multiple_selection :=
		   @on,
		 layout             :=
		   vertical,
		 append             :=
		   [ menu_item('St�tzkr�cken',
			       @default,
			       'St�tzkr�cken',
			       @off,
			       @nil,
			       s),
		     menu_item(andere, @default, andere, @off, @nil, a)
		   ]
	       ],
	     Chirurgische_Therapie  :=
	       [ multiple_selection :=
		   @on,
		 layout             :=
		   vertical,
		 columns            :=
		   2,
		 alignment          :=
		   right,
		 append             :=
		   [ menu_item('Verschraubung',
			       @default,
			       'Verschraubung',
			       @off,
			       @nil,
			       v),
		     menu_item('Kirschner-Draht',
			       @default,
			       'Kirschner-Draht',
			       @off,
			       @nil,
			       k),
		     menu_item('intertroch. Osteotomie',
			       @default,
			       'intertroch. Osteotomie',
			       @off,
			       @nil,
			       i),
		     menu_item('SH-Osteotomie (Dunn)',
			       @default,
			       'SH-Osteotomie (Dunn)',
			       @off,
			       @nil,
			       s),
		     menu_item(andere, @default, andere, @off, @nil, a)
		   ]
	       ],
	     Falls_andere__welche2  :=
	       [ label := 'andere, welche:'
	       ],
	     Falls_andere__welche1  :=
	       [ label     := 'andere, welche:',
		 length    := 18,
		 alignment := left
	       ],
	     Operateur              :=
	       [ alignment := left
	       ],
	     OP_Datum___MM_JJ       :=
	       [ length    := 7,
		 alignment := center
	       ]
	   ],
	 layout        :=
	   [ area(Durchgef_hrte_Therapie,
		  area(15, 8, 464, 18)),
	     area(Therap_B,
		  area(15, 34, 252, 23)),
	     area(Konservative_Therapie,
		  area(15, 58, 136, 49)),
	     area(Chirurgische_Therapie,
		  area(333, 56, 279, 66)),
	     area(Falls_andere__welche2,
		  area(13, 116, 256, 18)),
	     area(Falls_andere__welche1,
		  area(332, 130, 213, 18)),
	     area(Operateur,
		  area(13, 181, 224, 18)),
	     area(OP_Datum___MM_JJ,
		  area(332, 181, 172, 18))
	   ]
       ]).
dialog(k_h_therapie_2_c,
       [ object        :=
	   K_H_Therapie___C,
	 parts         :=
	   [ K_H_Therapie___C            :=
	       dialog('K H Therapie 2 C'),
	     Durchgef_hrte_Therapie      :=
	       menu('durchgef�hrte Therapie', choice),
	     Therap_C                    :=
	       label(therap_c, 'C'),
	     Konservative_Therapie       :=
	       menu('konservative Therapie', choice),
	     Chirurgische_Therapie       :=
	       menu('chirurgische Therapie', choice),
	     Falls_andere__welche1       :=
	       text_item('Falls andere, welche'),
	     Falls_andere__welche2       :=
	       text_item('Falls andere, welche'),
	     Dauer_der_kons__Ther____WWW :=
	       text_item('Dauer der kons. Ther. / WWW'),
	     Operateur                   :=
	       text_item(operateur),
	     OP_Datum___MM_JJ            :=
	       text_item('OP Datum / MM.JJ')
	   ],
	 modifications :=
	   [ Durchgef_hrte_Therapie      :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item('konservativ (s.d.)',
					  @default,
					  'konservativ (s.d.)',
					  @off,
					  @nil,
					  o),
				menu_item('chirurgisch (s.d.)',
					  @default,
					  'chirurgisch (s.d.)',
					  @off,
					  @nil,
					  c)
			      ]
	       ],
	     Therap_C                    :=
	       [ font := @helvetica_roman_18
	       ],
	     Konservative_Therapie       :=
	       [ multiple_selection :=
		   @on,
		 layout             :=
		   vertical,
		 append             :=
		   [ menu_item('Atlantaschiene',
			       @default,
			       'Atlantaschiene',
			       @off,
			       @nil,
			       a),
		     menu_item('Thomassplint',
			       @default,
			       'Thomassplint',
			       @off,
			       @nil,
			       t),
		     menu_item('St�tzkr�cken',
			       @default,
			       'St�tzkr�cken',
			       @off,
			       @nil,
			       s),
		     menu_item(andere, @default, andere, @off, @nil, n)
		   ]
	       ],
	     Chirurgische_Therapie       :=
	       [ multiple_selection :=
		   @on,
		 layout             :=
		   vertical,
		 columns            :=
		   2,
		 append             :=
		   [ menu_item('varis. OT', @default, 'varis. OT', @off, @nil, v),
		     menu_item('Salter', @default, 'Salter', @off, @nil, s),
		     menu_item('Chiari', @default, 'Chiari', @off, @nil, c),
		     menu_item('Pfannendachplastik',
			       @default,
			       'Pfannendachplastik',
			       @off,
			       @nil,
			       p),
		     menu_item('Adduktorentenotomie',
			       @default,
			       'Adduktorentenotomie',
			       @off,
			       @nil,
			       a),
		     menu_item('Psoastenotomie',
			       @default,
			       'Psoastenotomie',
			       @off,
			       @nil,
			       o),
		     menu_item(andere, @default, andere, @off, @nil, n)
		   ]
	       ],
	     Falls_andere__welche2       :=
	       [ label := 'andere, welche:'
	       ],
	     Falls_andere__welche1       :=
	       [ label := 'andere, welche:'
	       ],
	     Dauer_der_kons__Ther____WWW :=
	       [ length := 3
	       ],
	     OP_Datum___MM_JJ            :=
	       [ length    := 7,
		 alignment := center
	       ]
	   ],
	 layout        :=
	   [ area(Durchgef_hrte_Therapie,
		  area(15, 8, 464, 18)),
	     area(Therap_C,
		  area(15, 34, 252, 23)),
	     area(Konservative_Therapie,
		  area(15, 65, 136, 83)),
	     area(Chirurgische_Therapie,
		  area(316, 65, 253, 83)),
	     area(Falls_andere__welche2,
		  area(15, 156, 256, 18)),
	     area(Falls_andere__welche1,
		  area(320, 156, 256, 18)),
	     area(Dauer_der_kons__Ther____WWW,
		  area(15, 182, 206, 18)),
	     area(Operateur,
		  area(15, 231, 227, 18)),
	     area(OP_Datum___MM_JJ,
		  area(322, 230, 172, 18))
	   ]
       ]).
dialog(k_h_therapie_2_d,
       [ object        :=
	   K_H_Therapie___D,
	 parts         :=
	   [ K_H_Therapie___D       :=
	       dialog('K H Therapie 2 D'),
	     Durchgef_hrte_Therapie :=
	       menu('durchgef�hrte Therapie', choice),
	     Ther_D                 :=
	       label(ther_d, 'D'),
	     Therapie               :=
	       menu(therapie, choice),
	     Operateur              :=
	       text_item(operateur),
	     OP_Datum___MM_JJ       :=
	       text_item('OP Datum / MM.JJ')
	   ],
	 modifications :=
	   [ Durchgef_hrte_Therapie :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item('konservativ (s.d.)',
					  @default,
					  'konservativ (s.d.)',
					  @off,
					  @nil,
					  o),
				menu_item('chirurgisch (s.d.)',
					  @default,
					  'chirurgisch (s.d.)',
					  @off,
					  @nil,
					  c)
			      ]
	       ],
	     Ther_D                 :=
	       [ font := @helvetica_roman_18
	       ],
	     Therapie               :=
	       [ multiple_selection :=
		   @on,
		 layout             :=
		   vertical,
		 append             :=
		   [ menu_item('Gipshose', @default, 'Gipshose', @off, @nil, g),
		     menu_item('Antibiotika', @default, 'Antibiotika', @off, @nil, a),
		     menu_item('Punktion', @default, 'Punktion', @off, @nil, p),
		     menu_item('Gelenkse', @default, 'Gelenkse', @off, @nil, e)
		   ]
	       ],
	     Operateur              :=
	       [ length := 26
	       ],
	     OP_Datum___MM_JJ       :=
	       [ length    := 8,
		 alignment := right
	       ]
	   ],
	 layout        :=
	   [ area(Durchgef_hrte_Therapie,
		  area(15, 8, 464, 18)),
	     area(Ther_D,
		  area(15, 34, 252, 23)),
	     area(Therapie,
		  area(15, 65, 107, 83)),
	     area(Operateur,
		  area(15, 190, 227, 18)),
	     area(OP_Datum___MM_JJ,
		  area(307, 188, 172, 18))
	   ]
       ]).
dialog(k_h_s_u_r_postoper_vor,
       [ object        :=
	   K_H_S_U_R_Postoper_Vor,
	 parts         :=
	   [ K_H_S_U_R_Postoper_Vor       :=
	       dialog('K H S U R Postoper Vor'),
	     Vor_Gehbeginn__              :=
	       label(vor_gehbeginn_2, 'VOR GEHBEGINN'),
	     A_D_Sono_P__                 :=
	       label(a_d_sono_p_2, 'A+D'),
	     AC_postop_____Stell__in_Grad :=
	       text_item('AC postop / 2-Stell. in Grad'),
	     CE_postop_____Stell__in_Grad :=
	       text_item('CE postop / 2-Stell. in Grad'),
	     H_ftlux__n__T_nnis           :=
	       menu('H�ftlux. n. T�nnis', choice)
	   ],
	 modifications :=
	   [ Vor_Gehbeginn__              :=
	       [ font := @helvetica_roman_18
	       ],
	     A_D_Sono_P__                 :=
	       [ font := @helvetica_roman_18
	       ],
	     AC_postop_____Stell__in_Grad :=
	       [ length := 2
	       ],
	     CE_postop_____Stell__in_Grad :=
	       [ length := 2
	       ],
	     H_ftlux__n__T_nnis           :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('I�', @default, 'I�', @off, @nil, i),
				menu_item('II�', @default, 'II�', @off, @nil, �),
				'III�',
				menu_item('IV�', @default, 'IV�', @off, @nil, v)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(A_D_Sono_P__, Vor_Gehbeginn__),
	     below(AC_postop_____Stell__in_Grad, A_D_Sono_P__),
	     below(CE_postop_____Stell__in_Grad,
		   AC_postop_____Stell__in_Grad),
	     below(H_ftlux__n__T_nnis, CE_postop_____Stell__in_Grad)
	   ]
       ]).
dialog(k_h_s_u_r_postoper_nach,
       [ object        :=
	   K_H_S_U_R_Postoper_Nach,
	 parts         :=
	   [ K_H_S_U_R_Postoper_Nach                   :=
	       dialog('K H S U R Postoper Nach'),
	     Nach_Gehbeginn__                          :=
	       label(nach_gehbeginn_2, 'NACH GEHBEGINN'),
	     A_E_Sono_Nach__                           :=
	       label(a_e_sono_nach_2, 'A-E'),
	     Ullmann_Sharp_postop_____Stell__in_Grad   :=
	       text_item('Ullmann/Sharp postop / 2-Stell. in Grad'),
	     CE_postop                                 :=
	       menu('CE postop', choice),
	     Eingabe_von_CE_postop_____Stell__in_Grad  :=
	       text_item('Eingabe von CE postop / 2-Stell. in Grad'),
	     Eingabe_von_CCD_postop_____Stell__in_Grad :=
	       text_item('Eingabe von CCD postop / 3-Stell. in Grad'),
	     Eingabe_von_ATD_postop_____Stell__in_mm   :=
	       text_item('Eingabe von ATD postop / 2-Stell. in mm'),
	     Osteoarthritis_postop                     :=
	       menu('Osteoarthritis postop', choice)
	   ],
	 modifications :=
	   [ Nach_Gehbeginn__                          :=
	       [ font := @helvetica_roman_18
	       ],
	     A_E_Sono_Nach__                           :=
	       [ font := @helvetica_roman_18
	       ],
	     Ullmann_Sharp_postop_____Stell__in_Grad   :=
	       [ length := 2
	       ],
	     CE_postop                                 :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('normal (>25�)',
					  @default,
					  'normal (>25�)',
					  @off,
					  @nil,
					  n),
				menu_item('20-25�', @default, '20-25�', @off, @nil, �),
				'<20�'
			      ]
	       ],
	     Eingabe_von_CE_postop_____Stell__in_Grad  :=
	       [ length := 2
	       ],
	     Eingabe_von_CCD_postop_____Stell__in_Grad :=
	       [ length := 3
	       ],
	     Eingabe_von_ATD_postop_____Stell__in_mm   :=
	       [ length := 2
	       ],
	     Osteoarthritis_postop                     :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('3 normal',
					  @default,
					  '3 normal',
					  @off,
					  @nil,
					  n),
				menu_item('2 m��ig',
					  @default,
					  '2 m��ig',
					  @off,
					  @nil,
					  m),
				menu_item('1 schwer',
					  @default,
					  '1 schwer',
					  @off,
					  @nil,
					  s)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(A_E_Sono_Nach__, Nach_Gehbeginn__),
	     below(Ullmann_Sharp_postop_____Stell__in_Grad,
		   A_E_Sono_Nach__),
	     below(CE_postop, Ullmann_Sharp_postop_____Stell__in_Grad),
	     below(Eingabe_von_CE_postop_____Stell__in_Grad, CE_postop),
	     below(Eingabe_von_CCD_postop_____Stell__in_Grad,
		   Eingabe_von_CE_postop_____Stell__in_Grad),
	     below(Eingabe_von_ATD_postop_____Stell__in_mm,
		   Eingabe_von_CCD_postop_____Stell__in_Grad),
	     below(Osteoarthritis_postop,
		   Eingabe_von_ATD_postop_____Stell__in_mm)
	   ]
       ]).
dialog(k_h_s_u_r_postoper_a,
       [ object        :=
	   K_H_S_U_R_Postoper_A,
	 parts         :=
	   [ K_H_S_U_R_Postoper_A                 :=
	       dialog('K H S U R Postoper A'),
	     A_Sono__                             :=
	       label(a_sono_2, 'A'),
	     AVN_Ogden_Buchholz                   :=
	       menu('AVN Ogden-Buchholz', choice),
	     Lat_Index_postop_____Stell_          :=
	       text_item('Lat.Index postop / 2-Stell.'),
	     Migr___postop_____Stell__in_Prozente :=
	       text_item('Migr. % postop / 2-Stell. in Prozente'),
	     Tr_nenfigur_verbr_                   :=
	       menu('Tr�nenfigur verbr.', choice),
	     Falls_ja__wieviel_____Stell__in_mm   :=
	       text_item('Falls ja, wieviel / 2-Stell. in mm'),
	     Kopf_Tr_nenf__Dist______Stell__in_mm :=
	       text_item('Kopf-Tr�nenf.-Dist. / 2-Stell. in mm')
	   ],
	 modifications :=
	   [ A_Sono__                             :=
	       [ font := @helvetica_roman_18
	       ],
	     AVN_Ogden_Buchholz                   :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('Typ 0', @default, 'Typ 0', @off, @nil, t),
				menu_item('Typ I', @default, 'Typ I', @off, @nil, y),
				menu_item('Typ II', @default, 'Typ II', @off, @nil, i),
				menu_item('Typ III',
					  @default,
					  'Typ III',
					  @off,
					  @nil,
					  p),
				menu_item('Typ IV', @default, 'Typ IV', @off, @nil, v)
			      ]
	       ],
	     Lat_Index_postop_____Stell_          :=
	       [ length := 2
	       ],
	     Migr___postop_____Stell__in_Prozente :=
	       [ length := 2
	       ],
	     Tr_nenfigur_verbr_                   :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Falls_ja__wieviel_____Stell__in_mm   :=
	       [ length := 2
	       ],
	     Kopf_Tr_nenf__Dist______Stell__in_mm :=
	       [ length := 2
	       ]
	   ],
	 layout        :=
	   [ below(AVN_Ogden_Buchholz, A_Sono__),
	     below(Lat_Index_postop_____Stell_, AVN_Ogden_Buchholz),
	     below(Migr___postop_____Stell__in_Prozente,
		   Lat_Index_postop_____Stell_),
	     below(Tr_nenfigur_verbr_,
		   Migr___postop_____Stell__in_Prozente),
	     below(Falls_ja__wieviel_____Stell__in_mm, Tr_nenfigur_verbr_),
	     below(Kopf_Tr_nenf__Dist______Stell__in_mm,
		   Falls_ja__wieviel_____Stell__in_mm)
	   ]
       ]).
dialog(k_h_s_u_r_postoper_b,
       [ object        :=
	   K_H_S_U_R_Postoper_B,
	 parts         :=
	   [ K_H_S_U_R_Postoper_B                                      :=
	       dialog('K H S U R Postoper B'),
	     B_Sono__                                                  :=
	       label(b_sono_2, 'B'),
	     Kopf_Hals_Winkel_postop                                   :=
	       menu('Kopf-Hals-Winkel postop', choice),
	     Eingabe_von_Kopf_Hals_Winkel_postop_____Stell__in____Grad :=
	       text_item('Eingabe von Kopf-Hals-Winkel postop / 2-Stell. in
			Grad'),
	     Eingabe_von_KE_Y_Winkel_ap_____Stell__in_____Grad         :=
	       text_item('Eingabe von KE-Y-Winkel ap / 2-Stell. in 
			Grad')
	   ],
	 modifications :=
	   [ B_Sono__                                                  :=
	       [ font := @helvetica_roman_18
	       ],
	     Kopf_Hals_Winkel_postop                                   :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('leicht (<30�)',
					  @default,
					  'leicht (<30�)',
					  @off,
					  @nil,
					  l),
				menu_item('mittel (30-60�)',
					  @default,
					  'mittel (30-60�)',
					  @off,
					  @nil,
					  m),
				menu_item('schwer (>60�)',
					  @default,
					  'schwer (>60�)',
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Eingabe_von_Kopf_Hals_Winkel_postop_____Stell__in____Grad :=
	       [ length := 2
	       ],
	     Eingabe_von_KE_Y_Winkel_ap_____Stell__in_____Grad         :=
	       [ length := 2
	       ]
	   ],
	 layout        :=
	   [ below(Kopf_Hals_Winkel_postop, B_Sono__),
	     below(Eingabe_von_Kopf_Hals_Winkel_postop_____Stell__in____Grad,
		   Kopf_Hals_Winkel_postop),
	     below(Eingabe_von_KE_Y_Winkel_ap_____Stell__in_____Grad,
		   Eingabe_von_Kopf_Hals_Winkel_postop_____Stell__in____Grad)
	   ]
       ]).
dialog(k_h_s_u_r_postoper_c,
       [ object        :=
	   K_H_S_U_R_Postoper_C,
	 parts         :=
	   [ K_H_S_U_R_Postoper_C            :=
	       dialog('K H S U R Postoper C'),
	     C_Sono__                        :=
	       label(c_sono_2, 'C'),
	     Catterall_postop                :=
	       menu('Catterall postop', choice),
	     Salter_Thompson_postop          :=
	       menu('Salter Thompson postop', choice),
	     LP_n__Herring_postop            :=
	       menu('LP n. Herring postop', choice),
	     Tr_nenfigur_verbr_              :=
	       menu('Tr�nenfigur verbr.', choice),
	     Falls_ja__wieviel_____Stell__mm :=
	       text_item('Falls ja, wieviel / 2-Stell. mm'),
	     Mose_Krit__postop               :=
	       menu('Mose Krit. postop', choice)
	   ],
	 modifications :=
	   [ C_Sono__                        :=
	       [ font := @helvetica_roman_18
	       ],
	     Catterall_postop                :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('I', @default, 'I', @off, @nil, i),
				menu_item('II', @default, 'II'),
				menu_item('III', @default, 'III'),
				menu_item('IV', @default, 'IV', @off, @nil, v)
			      ]
	       ],
	     Salter_Thompson_postop          :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('Typ A', @default, 'Typ A', @off, @nil, t),
				menu_item('Typ B', @default, 'Typ B', @off, @nil, y)
			      ]
	       ],
	     LP_n__Herring_postop            :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('A', @default, 'A', @off, @nil, a),
				menu_item('B <9 J', @default, 'B <9 J', @off, @nil, b),
				menu_item('B >9 J', @default, 'B >9 J', @off, @nil, j),
				menu_item('C', @default, 'C', @off, @nil, c)
			      ]
	       ],
	     Tr_nenfigur_verbr_              :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(nein, @default, nein, @off, @nil, n),
				menu_item(ja, @default, ja, @off, @nil, j)
			      ]
	       ],
	     Falls_ja__wieviel_____Stell__mm :=
	       [ length := 2
	       ],
	     Mose_Krit__postop               :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(gut, @default, gut, @off, @nil, g),
				menu_item(m��ig, @default, m��ig, @off, @nil, m),
				menu_item(schlecht,
					  @default,
					  schlecht,
					  @off,
					  @nil,
					  s)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Catterall_postop, C_Sono__),
	     below(Salter_Thompson_postop, Catterall_postop),
	     below(LP_n__Herring_postop, Salter_Thompson_postop),
	     below(Tr_nenfigur_verbr_, LP_n__Herring_postop),
	     below(Falls_ja__wieviel_____Stell__mm, Tr_nenfigur_verbr_),
	     below(Mose_Krit__postop, Falls_ja__wieviel_____Stell__mm)
	   ]
       ]).
dialog(k_h_s_u_r_postoper_d,
       [ object        :=
	   K_H_S_U_R_Postoper_D,
	 parts         :=
	   [ K_H_S_U_R_Postoper_D        :=
	       dialog('K H S U R Postoper D'),
	     D_Sono__                    :=
	       label(d_sono_2, 'D'),
	     Hunka_Klassifikation_postop :=
	       menu('Hunka Klassifikation postop', choice)
	   ],
	 modifications :=
	   [ D_Sono__                    := [ font := @helvetica_roman_18
					    ],
	     Hunka_Klassifikation_postop := [ reference := point(0, 13),
					      append    := [ menu_item('I',
								       @default,
								       'I',
								       @off,
								       @nil,
								       i),
							     menu_item('IIA',
								       @default,
								       'IIA',
								       @off,
								       @nil,
								       a),
							     menu_item('IIB',
								       @default,
								       'IIB',
								       @off,
								       @nil,
								       b),
							     menu_item('III',
								       @default,
								       'III'),
							     menu_item('IVA',
								       @default,
								       'IVA'),
							     menu_item('IVB',
								       @default,
								       'IVB'),
							     menu_item('V',
								       @default,
								       'V',
								       @off,
								       @nil,
								       v)
							   ]
					    ]
	   ],
	 layout        :=
	   [ below(Hunka_Klassifikation_postop, D_Sono__)
	   ]
       ]).
dialog(kindliche_wirbels,
       [ object        :=
	   Kindliche_Wirbels,
	 parts         :=
	   [ Kindliche_Wirbels                :=
	       dialog('Kindliche Wirbels'),
	     Vorstellungsgrund                :=
	       menu(vorstellungsgrund, choice),
	     Aktuelles_Problem                :=
	       menu('aktuelles Problem', choice),
	     Anamn                            :=
	       label(anamn, 'Anamnese'),
	     Erste_Symptome_seit___in_Monaten :=
	       text_item('erste Symptome seit / in Monaten'),
	     Bisherige_Behandlung             :=
	       menu('bisherige Behandlung', choice),
	     Menarche                         :=
	       menu(menarche, choice),
	     Falls_ja__seit_wann___MMJJ       :=
	       text_item('Falls ja, seit wann / MMJJ')
	   ],
	 modifications :=
	   [ Vorstellungsgrund                :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('Erstvorstellung',
					  @default,
					  'Erstvorstellung',
					  @off,
					  @nil,
					  e),
				menu_item('Verlaufskontrolle',
					  @default,
					  'Verlaufskontrolle',
					  @off,
					  @nil,
					  v),
				menu_item('nach Operation',
					  @default,
					  'nach Operation',
					  @off,
					  @nil,
					  n),
				menu_item('nach konserv. Behandlung',
					  @default,
					  'nach konserv. Behandlung',
					  @off,
					  @nil,
					  a)
			      ]
	       ],
	     Aktuelles_Problem                :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item('Schmerzen',
					  @default,
					  'Schmerzen',
					  @off,
					  @nil,
					  s),
				menu_item('Deformit�t',
					  @default,
					  'Deformit�t',
					  @off,
					  @nil,
					  d),
				menu_item('Funktionsst�rung',
					  @default,
					  'Funktionsst�rung',
					  @off,
					  @nil,
					  f)
			      ]
	       ],
	     Anamn                            :=
	       [ font := @helvetica_roman_18
	       ],
	     Erste_Symptome_seit___in_Monaten :=
	       [ length := 3
	       ],
	     Bisherige_Behandlung             :=
	       [ columns   := 3,
		 reference := point(0, 13),
		 append    := [ menu_item(konserativ,
					  @default,
					  konserativ,
					  @off,
					  @nil,
					  k),
				menu_item(operativ,
					  @default,
					  operativ,
					  @off,
					  @nil,
					  o),
				menu_item(keine, @default, keine, @off, @nil, e)
			      ]
	       ],
	     Menarche                         :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     Falls_ja__seit_wann___MMJJ       :=
	       [ length := 7
	       ]
	   ],
	 layout        :=
	   [ below(Aktuelles_Problem, Vorstellungsgrund),
	     below(Anamn, Aktuelles_Problem),
	     below(Erste_Symptome_seit___in_Monaten, Anamn),
	     below(Bisherige_Behandlung, Erste_Symptome_seit___in_Monaten),
	     below(Menarche, Bisherige_Behandlung),
	     below(Falls_ja__seit_wann___MMJJ, Menarche)
	   ]
       ]).
dialog(k_ws_klinische_befunde,
       [ object        :=
	   K_Ws_Klinische_Befunde,
	 parts         :=
	   [ K_Ws_Klinische_Befunde                         :=
	       dialog('K Ws Klinische Befunde'),
	     Kl_Befunde                                     :=
	       label(kl_befunde, 'Klinische Befunde'),
	     Sitzgr__e___cm                                 :=
	       text_item('Sitzgr��e / cm'),
	     Stehgr__e___cm                                 :=
	       text_item('Stehgr��e / cm'),
	     Schmerzlokalisation__p_m_____HWK               :=
	       menu('Schmerzlokalisation (p.m.) / HWK', toggle),
	     Schmerzlokalisation__p_m____BWK                :=
	       menu('Schmerzlokalisation (p.m.) /BWK', toggle),
	     Schmerzlokalisation__p_m_____LWK               :=
	       menu('Schmerzlokalisation (p.m.) / LWK', toggle),
	     Bew__HWS                                       :=
	       label('Bew._HWS', 'Beweglichkeit / HWS'),
	     Kinnspitzen_Jugulum_Abstand__cm____Vorneigung  :=
	       text_item('Kinnspitzen_Jugulum_Abstand (cm) / Vorneigung'),
	     Kinnspitzen_Jugulum_Abstand__cm____R_ckneigung :=
	       text_item('Kinnspitzen_Jugulum_Abstand (cm) / R�ckneigung'),
	     Linksrotation____                              :=
	       text_item('Linksrotation (�)'),
	     Rechtsrotation____                             :=
	       text_item('Rechtsrotation (�)'),
	     Linksneigung____                               :=
	       text_item('Linksneigung (�)'),
	     Rechtsneigung____                              :=
	       text_item('Rechtsneigung (�)'),
	     Bew__BWS                                       :=
	       label('Bew._BWS', 'Beweglichkeit / BWS'),
	     Zeichen_nach_OTT__cm_                          :=
	       text_item('Zeichen nach OTT (cm)'),
	     Bew__LWS                                       :=
	       label('Bew._LWS', 'Beweglichkeit / LWS'),
	     Zeichen_nach_SCHOBER__cm_                      :=
	       text_item('Zeichen nach SCHOBER (cm)'),
	     Finger_Boden_Abstand__cm_                      :=
	       text_item('Finger_Boden_Abstand (cm)'),
	     Beckentiefstand                                :=
	       menu('Beckentiefstand', choice),
	     Beckentiefstand__cm_                           :=
	       text_item('Beckentiefstand (cm)'),
	     Schultertiefstand                              :=
	       menu('Schultertiefstand', choice),
	     Schultertiefstand__cm_                         :=
	       text_item('Schultertiefstand (cm)'),
	     Rumpf_behang                                   :=
	       menu('Rumpf�behang', choice),
	     Rumpf_berhang__cm_                             :=
	       text_item('Rumpf�berhang (cm)'),
	     Rippenbuckel                                   :=
	       menu('Rippenbuckel', choice),
	     Lendenwulst                                    :=
	       menu('Lendenwulst', choice),
	     Thorakale_Hyperkyphose                         :=
	       menu('thorakale Hyperkyphose', choice),
	     H_ftlendenstrecksteife                         :=
	       menu('H�ftlendenstrecksteife', choice)
	   ],
	 modifications :=
	   [ Kl_Befunde                                     :=
	       [ font := @helvetica_roman_18
	       ],
	     Sitzgr__e___cm                                 :=
	       [ length := 4
	       ],
	     Stehgr__e___cm                                 :=
	       [ length    := 4,
		 alignment := center
	       ],
	     Schmerzlokalisation__p_m_____HWK               :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(1, @default, '1', @off, @nil, '1'),
				menu_item(2, @default, '2', @off, @nil, '2'),
				menu_item(3, @default, '3', @off, @nil, '3'),
				menu_item(4, @default, '4', @off, @nil, '4'),
				menu_item(5, @default, '5', @off, @nil, '5'),
				menu_item(6, @default, '6', @off, @nil, '6'),
				menu_item(7, @default, '7', @off, @nil, '7')
			      ]
	       ],
	     Schmerzlokalisation__p_m____BWK                :=
	       [ columns   := 2,
		 reference := point(0, 13),
		 append    := [ menu_item(1, @default, '1', @off, @nil, '1'),
				menu_item(2, @default, '2', @off, @nil, '2'),
				menu_item(3, @default, '3', @off, @nil, '3'),
				menu_item(4, @default, '4', @off, @nil, '4'),
				menu_item(5, @default, '5', @off, @nil, '5'),
				menu_item(6, @default, '6', @off, @nil, '6'),
				menu_item(7, @default, '7', @off, @nil, '7'),
				menu_item(8, @default, '8', @off, @nil, '8'),
				menu_item(9, @default, '9', @off, @nil, '9'),
				menu_item(10, @default, '10', @off, @nil, '0'),
				11,
				12
			      ]
	       ],
	     Schmerzlokalisation__p_m_____LWK               :=
	       [ reference := point(0, 13),
		 append    := [ menu_item(1, @default, '1', @off, @nil, '1'),
				menu_item(2, @default, '2', @off, @nil, '2'),
				menu_item(3, @default, '3', @off, @nil, '3'),
				menu_item(4, @default, '4', @off, @nil, '4'),
				menu_item(5, @default, '5', @off, @nil, '5'),
				menu_item(6, @default, '6', @off, @nil, '6')
			      ]
	       ],
	     Bew__HWS                                       :=
	       [ font := @helvetica_roman_18
	       ],
	     Kinnspitzen_Jugulum_Abstand__cm____Vorneigung  :=
	       [ length := 3
	       ],
	     Kinnspitzen_Jugulum_Abstand__cm____R_ckneigung :=
	       [ length    := 3,
		 alignment := right
	       ],
	     Linksrotation____                              :=
	       [ length := 3
	       ],
	     Rechtsrotation____                             :=
	       [ length := 3
	       ],
	     Linksneigung____                               :=
	       [ length := 3
	       ],
	     Rechtsneigung____                              :=
	       [ length := 3
	       ],
	     Bew__BWS                                       :=
	       [ font := @helvetica_roman_18
	       ],
	     Zeichen_nach_OTT__cm_                          :=
	       [ length := 3
	       ],
	     Bew__LWS                                       :=
	       [ font := @helvetica_roman_18
	       ],
	     Zeichen_nach_SCHOBER__cm_                      :=
	       [ length := 3
	       ],
	     Finger_Boden_Abstand__cm_                      :=
	       [ length    := 3,
		 alignment := center
	       ],
	     Beckentiefstand                                :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(links, @default, links, @off, @nil, l),
				menu_item(rechts, @default, rechts, @off, @nil, r)
			      ]
	       ],
	     Beckentiefstand__cm_                           :=
	       [ length := 3
	       ],
	     Schultertiefstand                              :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(links, @default, links, @off, @nil, l),
				menu_item(rechts, @default, rechts, @off, @nil, r)
			      ]
	       ],
	     Schultertiefstand__cm_                         :=
	       [ length := 3
	       ],
	     Rumpf_behang                                   :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(links, @default, links, @off, @nil, l),
				menu_item(rechts, @default, rechts, @off, @nil, r)
			      ]
	       ],
	     Rumpf_berhang__cm_                             :=
	       [ length := 3
	       ],
	     Rippenbuckel                                   :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(links, @default, links, @off, @nil, l),
				menu_item(rechts, @default, rechts, @off, @nil, r)
			      ]
	       ],
	     Lendenwulst                                    :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(links, @default, links, @off, @nil, l),
				menu_item(rechts, @default, rechts, @off, @nil, r)
			      ]
	       ],
	     Thorakale_Hyperkyphose                         :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ],
	     H_ftlendenstrecksteife                         :=
	       [ feedback  := show_selection_only,
		 reference := point(0, 13),
		 append    := [ menu_item(ja, @default, ja, @off, @nil, j),
				menu_item(nein, @default, nein, @off, @nil, n)
			      ]
	       ]
	   ],
	 layout        :=
	   [ below(Sitzgr__e___cm, Kl_Befunde),
	     right(Stehgr__e___cm, Sitzgr__e___cm),
	     below(Schmerzlokalisation__p_m_____HWK, Sitzgr__e___cm),
	     below(Schmerzlokalisation__p_m____BWK,
		   Schmerzlokalisation__p_m_____HWK),
	     below(Schmerzlokalisation__p_m_____LWK,
		   Schmerzlokalisation__p_m____BWK),
	     below(Bew__HWS, Schmerzlokalisation__p_m_____LWK),
	     below(Kinnspitzen_Jugulum_Abstand__cm____Vorneigung,
		   Bew__HWS),
	     right(Kinnspitzen_Jugulum_Abstand__cm____R_ckneigung,
		   Kinnspitzen_Jugulum_Abstand__cm____Vorneigung),
	     below(Linksrotation____,
		   Kinnspitzen_Jugulum_Abstand__cm____Vorneigung),
	     right(Rechtsrotation____, Linksrotation____),
	     below(Linksneigung____, Linksrotation____),
	     below(Rechtsneigung____, Rechtsrotation____),
	     right(Rechtsneigung____, Linksneigung____),
	     below(Bew__BWS, Linksneigung____),
	     below(Zeichen_nach_OTT__cm_, Bew__BWS),
	     below(Bew__LWS, Zeichen_nach_OTT__cm_),
	     below(Zeichen_nach_SCHOBER__cm_, Bew__LWS),
	     right(Finger_Boden_Abstand__cm_, Zeichen_nach_SCHOBER__cm_),
	     below(Beckentiefstand, Zeichen_nach_SCHOBER__cm_),
	     right(Beckentiefstand__cm_, Beckentiefstand),
	     below(Schultertiefstand, Beckentiefstand),
	     below(Schultertiefstand__cm_, Beckentiefstand__cm_),
	     right(Schultertiefstand__cm_, Schultertiefstand),
	     below(Rumpf_behang, Schultertiefstand),
	     below(Rumpf_berhang__cm_, Schultertiefstand__cm_),
	     right(Rumpf_berhang__cm_, Rumpf_behang),
	     below(Rippenbuckel, Rumpf_behang),
	     below(Lendenwulst, Rumpf_berhang__cm_),
	     right(Lendenwulst, Rippenbuckel),
	     below(Thorakale_Hyperkyphose, Rippenbuckel),
	     below(H_ftlendenstrecksteife, Lendenwulst),
	     right(H_ftlendenstrecksteife, Thorakale_Hyperkyphose)
	   ]
       ]).
dialog(k_ws_app_unter,
       [ object        :=
	   K_Ws_App_Unter,
	 parts         :=
	   [ K_Ws_App_Unter                      :=
	       dialog('K Ws App Unter'),
	     Apparative_Untersuchungen___Rest    :=
	       menu('apparative Untersuchungen - Rest', toggle),
	     Falls_andere__Rest___welche         :=
	       text_item('Falls andere (Rest), welche'),
	     Apparative_Untersuchungen___Labor   :=
	       menu('apparative Untersuchungen - Labor', toggle),
	     Falls_andere_Labor___welche         :=
	       text_item('Falls andere(Labor), welche'),
	     Apparative_Untersuchungen___R_ntgen :=
	       menu('apparative Untersuchungen - R�ntgen', toggle),
	     Falls_andere__R_ntgen___welche      :=
	       text_item('Falls andere (R�ntgen), welche')
	   ],
	 modifications :=
	   [ Apparative_Untersuchungen___Rest    :=
	       [ columns   := 2,
		 reference := point(0, 13),
		 append    := [ menu_item('Szinti', @default, 'Szinti', @off, @nil, s),
				menu_item('CT', @default, 'CT', @off, @nil, c),
				menu_item('MRI', @default, 'MRI', @off, @nil, m),
				menu_item(andere, @default, andere, @off, @nil, a)
			      ]
	       ],
	     Falls_andere__Rest___welche         :=
	       [ length := 30
	       ],
	     Apparative_Untersuchungen___Labor   :=
	       [ columns   := 2,
		 reference := point(0, 20),
		 append    := [ menu_item('rheumat.
			Arthritis',
					  @default,
					  'rheumat.
			Arthritis',
					  @off,
					  @nil,
					  r),
				menu_item('Infektion',
					  @default,
					  'Infektion',
					  @off,
					  @nil,
					  i),
				menu_item('Tumor', @default, 'Tumor', @off, @nil, t),
				menu_item(andere, @default, andere, @off, @nil, a)
			      ]
	       ],
	     Falls_andere_Labor___welche         :=
	       [ length := 30
	       ],
	     Apparative_Untersuchungen___R_ntgen :=
	       [ columns   := 5,
		 reference := point(0, 13),
		 append    := [ menu_item('HWS 2E /schr�g',
					  @default,
					  'HWS 2E /schr�g',
					  @off,
					  @nil,
					  h),
				menu_item('HWS 2E / Funktion (vor)',
					  @default,
					  'HWS 2E / Funktion (vor)',
					  @off,
					  @nil,
					  w),
				menu_item('HWS 2E / Funktion (r�ck)',
					  @default,
					  'HWS 2E / Funktion (r�ck)',
					  @off,
					  @nil,
					  f),
				menu_item('BWS 2E', @default, 'BWS 2E', @off, @nil, b),
				menu_item('LWS 2E / schr�g',
					  @default,
					  'LWS 2E / schr�g',
					  @off,
					  @nil,
					  l),
				menu_item('LWS 2E / Funktion (vor)',
					  @default,
					  'LWS 2E / Funktion (vor)',
					  @off,
					  @nil,
					  s),
				menu_item('LWS 2E / Funktion (r�ck)',
					  @default,
					  'LWS 2E / Funktion (r�ck)',
					  @off,
					  @nil,
					  e),
				menu_item('ganze WS im Stand 2E',
					  @default,
					  'ganze WS im Stand 2E',
					  @off,
					  @nil,
					  g),
				menu_item(andere, @default, andere, @off, @nil, a)
			      ]
	       ],
	     Falls_andere__R_ntgen___welche      :=
	       [ length := 31
	       ]
	   ],
	 layout        :=
	   [ below(Falls_andere__Rest___welche,
		   Apparative_Untersuchungen___Rest),
	     below(Apparative_Untersuchungen___Labor,
		   Falls_andere__Rest___welche),
	     below(Falls_andere_Labor___welche,
		   Apparative_Untersuchungen___Labor),
	     below(Apparative_Untersuchungen___R_ntgen,
		   Falls_andere_Labor___welche),
	     below(Falls_andere__R_ntgen___welche,
		   Apparative_Untersuchungen___R_ntgen)
	   ]
       ]).
dialog(k_ws_diag_ther,
       [ object        :=
	   K_Ws_Diag_Ther,
	 parts         :=
	   [ K_Ws_Diag_Ther                                :=
	       dialog('K Ws Diag Ther'),
	     K_Wsdiag                                      :=
	       label(k_wsdiag, 'Diagnose'),
	     Hws                                           :=
	       menu('HWS', toggle),
	     Falls_andere__HWS___welche                    :=
	       text_item('Falls andere (HWS), welche'),
	     Bws__lws                                      :=
	       menu('BWS /LWS', toggle),
	     Falls_andere__BWS_LWS___welche                :=
	       text_item('Falls andere (BWS/LWS), welche'),
	     Skoliose                                      :=
	       menu('Skoliose', toggle),
	     Falls_andere__Skoliose___welche               :=
	       text_item('Falls andere (Skoliose), welche'),
	     Ther                                          :=
	       label(ther, 'Therapie'),
	     Therapie                                      :=
	       menu('Therapie', choice),
	     Falls_Beobachtung___Eingabe_von_WV_in_Monaten :=
	       text_item('Falls Beobachtung : Eingabe von WV in Monaten'),
	     Bemerkungen__                                 :=
	       text_item('Bemerkungen :')
	   ],
	 modifications :=
	   [ K_Wsdiag                                      :=
	       [ font := @helvetica_roman_18
	       ],
	     Hws                                           :=
	       [ label     := 'HWS:',
		 columns   := 2,
		 reference := point(0, 13),
		 append    := [ menu_item('Schiefhals / links',
					  @default,
					  'Schiefhals / links',
					  @off,
					  @nil,
					  s),
				menu_item('Schiefhals / rechts',
					  @default,
					  'Schiefhals / rechts',
					  @off,
					  @nil,
					  c),
				menu_item('Klippel-Feil',
					  @default,
					  'Klippel-Feil',
					  @off,
					  @nil,
					  k),
				menu_item(andere, @default, andere, @off, @nil, a)
			      ]
	       ],
	     Bws__lws                                      :=
	       [ label     := 'BWS / LWS:',
		 columns   := 4,
		 reference := point(0, 13),
		 append    := [ menu_item('M.Scheuermann',
					  @default,
					  'M.Scheuermann',
					  @off,
					  @nil,
					  m),
				menu_item('Spondylolyse',
					  @default,
					  'Spondylolyse',
					  @off,
					  @nil,
					  s),
				menu_item('Spondylolisthese',
					  @default,
					  'Spondylolisthese',
					  @off,
					  @nil,
					  p),
				menu_item('Bandscheibenvorfall',
					  @default,
					  'Bandscheibenvorfall',
					  @off,
					  @nil,
					  b),
				menu_item('Spondylitis',
					  @default,
					  'Spondylitis',
					  @off,
					  @nil,
					  o),
				menu_item('Spondylodiscitis',
					  @default,
					  'Spondylodiscitis',
					  @off,
					  @nil,
					  n),
				menu_item(andere, @default, andere, @off, @nil, a)
			      ]
	       ],
	     Skoliose                                      :=
	       [ columns   := 4,
		 reference := point(0, 13),
		 append    := [ menu_item(idiopatisch,
					  @default,
					  idiopatisch,
					  @off,
					  @nil,
					  i),
				menu_item(neuromuskul�r,
					  @default,
					  neuromuskul�r,
					  @off,
					  @nil,
					  n),
				menu_item(kongenital,
					  @default,
					  kongenital,
					  @off,
					  @nil,
					  k),
				menu_item('Tumor', @default, 'Tumor', @off, @nil, t),
				menu_item('Haltungsst�rung',
					  @default,
					  'Haltungsst�rung',
					  @off,
					  @nil,
					  h),
				menu_item('Haltungsvariante',
					  @default,
					  'Haltungsvariante',
					  @off,
					  @nil,
					  l),
				menu_item(andere, @default, andere, @off, @nil, a)
			      ]
	       ],
	     Falls_andere__Skoliose___welche               :=
	       [ length := 30
	       ],
	     Ther                                          :=
	       [ font := @helvetica_roman_18
	       ],
	     Therapie                                      :=
	       [ columns   := 2,
		 reference := point(0, 13),
		 append    := [ menu_item(keine, @default, keine, @off, @nil, k),
				menu_item(konservativ,
					  @default,
					  konservativ,
					  @off,
					  @nil,
					  n),
				menu_item(operativ,
					  @default,
					  operativ,
					  @off,
					  @nil,
					  o),
				menu_item('Beobachtung !!!',
					  @default,
					  'Beobachtung !!!',
					  @off,
					  @nil,
					  b)
			      ]
	       ],
	     Falls_Beobachtung___Eingabe_von_WV_in_Monaten :=
	       [ label := 'Beobachtung : Eingabe von WV in Monaten:'
	       ],
	     Bemerkungen__                                 :=
	       [ length := 45
	       ]
	   ],
	 layout        :=
	   [ below(Hws, K_Wsdiag),
	     below(Falls_andere__HWS___welche, Hws),
	     below(Bws__lws, Falls_andere__HWS___welche),
	     below(Falls_andere__BWS_LWS___welche, Bws__lws),
	     below(Skoliose, Falls_andere__BWS_LWS___welche),
	     below(Falls_andere__Skoliose___welche, Skoliose),
	     below(Ther, Falls_andere__Skoliose___welche),
	     below(Therapie, Ther),
	     below(Falls_Beobachtung___Eingabe_von_WV_in_Monaten,
		   Therapie),
	     below(Bemerkungen__,
		   Falls_Beobachtung___Eingabe_von_WV_in_Monaten)
	   ]
       ]).
dialog(deform_d_u_extr_erstdok,
       [ object        :=
	   Deform_D_U_Extr_Erstdok,
	 parts         :=
	   [ Deform_D_U_Extr_Erstdok                      :=
	       dialog('Deform D U Extr Erstdok'),
	     Erstdok                                      :=
	       label(erstdok, 'Erstdokumentation'),
	     Ausgeschriebene_Diagnose_mit_Grunderkrankung :=
	       text_item('ausgeschriebene Diagnose mit Grunderkrankung'),
	     _tiologie                                    :=
	       menu(�tiologie, choice),
	     Sonstiges__tiologie__                        :=
	       text_item('sonstiges �tiologie :'),
	     Risikofaktoren                               :=
	       menu('Risikofaktoren', toggle),
	     Sonstige_Risikofaktoren__                    :=
	       text_item('sonstige Risikofaktoren :'),
	     Diagnosegrundlage                            :=
	       menu('Diagnosegrundlage', toggle),
	     Sonstige_Diagnosegrundlage__                 :=
	       text_item('sonstige Diagnosegrundlage :'),
	     Gelenke___Weichteile                         :=
	       menu('Gelenke / Weichteile', toggle),
	     Behand_Ziel                                  :=
	       label(behand_ziel, 'Behandlungsziel'),
	     Verl_ngerung__cm_                            :=
	       text_item('Verl�ngerung (cm)'),
	     Achskorrektur_Grad                           :=
	       text_item('Achskorrektur Grad'),
	     Derotation_Grad                              :=
	       text_item('Derotation Grad')
	   ],
	 modifications :=
	   [ Erstdok                                      :=
	       [ font := @helvetica_roman_18
	       ],
	     Ausgeschriebene_Diagnose_mit_Grunderkrankung :=
	       [ length := 30
	       ],
	     _tiologie                                    :=
	       [ multiple_selection :=
		   @on,
		 columns            :=
		   3,
		 alignment          :=
		   left,
		 reference          :=
		   point(0, 13),
		 append             :=
		   [ menu_item(idiopatisch, @default, idiopatisch, @off, @nil, i),
		     menu_item('Dysmetie', @default, 'Dysmetie', @off, @nil, d),
		     menu_item('Trauma', @default, 'Trauma', @off, @nil, t),
		     menu_item('Infekt', @default, 'Infekt', @off, @nil, n),
		     menu_item(degenerativ, @default, degenerativ, @off, @nil, e),
		     menu_item(sonstiges, @default, sonstiges, @off, @nil, s)
		   ]
	       ],
	     Sonstiges__tiologie__                        :=
	       [ label     := 'sonstiges �tiologie :',
		 length    := 26,
		 alignment := left
	       ],
	     Risikofaktoren                               :=
	       [ columns   := 3,
		 alignment := left,
		 reference := point(0, 13),
		 append    := [ menu_item('Nichtraucher',
					  @default,
					  'Nichtraucher',
					  @off,
					  @nil,
					  n),
				menu_item('1-15 Zigaretten',
					  @default,
					  '1-15 Zigaretten',
					  @off,
					  @nil,
					  z),
				menu_item('>15 Zigaretten',
					  @default,
					  '>15 Zigaretten',
					  @off,
					  @nil,
					  i),
				menu_item('Diabetes',
					  @default,
					  'Diabetes',
					  @off,
					  @nil,
					  d),
				menu_item('Knochendefekt',
					  @default,
					  'Knochendefekt',
					  @off,
					  @nil,
					  k),
				menu_item(sonstige,
					  @default,
					  sonstige,
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Sonstige_Risikofaktoren__                    :=
	       [ length    := 26,
		 alignment := left
	       ],
	     Diagnosegrundlage                            :=
	       [ columns   := 3,
		 alignment := left,
		 reference := point(0, 13),
		 append    := [ menu_item('Ganzbeinstandaufnahme',
					  @default,
					  'Ganzbeinstandaufnahme',
					  @off,
					  @nil,
					  g),
				menu_item('Seitenaufnahme',
					  @default,
					  'Seitenaufnahme',
					  @off,
					  @nil,
					  s),
				menu_item('Ausschnittaufnahme',
					  @default,
					  'Ausschnittaufnahme',
					  @off,
					  @nil,
					  a),
				menu_item('Becken�bersicht stehend',
					  @default,
					  'Becken�bersicht stehend',
					  @off,
					  @nil,
					  b),
				menu_item('SpiralCT',
					  @default,
					  'SpiralCT',
					  @off,
					  @nil,
					  p),
				menu_item(sonstige,
					  @default,
					  sonstige,
					  @off,
					  @nil,
					  o)
			      ]
	       ],
	     Sonstige_Diagnosegrundlage__                 :=
	       [ length    := 26,
		 alignment := left
	       ],
	     Gelenke___Weichteile                         :=
	       [ columns   := 3,
		 alignment := left,
		 reference := point(0, 13),
		 append    := [ menu_item('Gelenkdysplasie',
					  @default,
					  'Gelenkdysplasie',
					  @off,
					  @nil,
					  g),
				menu_item('Gelenk (sub)luxation',
					  @default,
					  'Gelenk (sub)luxation',
					  @off,
					  @nil,
					  e),
				menu_item('Gelenkarthrose',
					  @default,
					  'Gelenkarthrose',
					  @off,
					  @nil,
					  l),
				menu_item('Weicht.-verk�rzung',
					  @default,
					  'Weicht.-verk�rzung',
					  @off,
					  @nil,
					  w),
				menu_item('Weicht.-fehlanlage',
					  @default,
					  'Weicht.-fehlanlage',
					  @off,
					  @nil,
					  i),
				menu_item('Gelenke-Weicht.ob',
					  @default,
					  'Gelenke-Weicht.ob',
					  @off,
					  @nil,
					  n)
			      ]
	       ],
	     Behand_Ziel                                  :=
	       [ font      := @helvetica_roman_18,
		 alignment := left
	       ],
	     Verl_ngerung__cm_                            :=
	       [ length := 4
	       ],
	     Achskorrektur_Grad                           :=
	       [ length := 4
	       ],
	     Derotation_Grad                              :=
	       [ length := 4
	       ]
	   ],
	 layout        :=
	   [ area(Erstdok,
		  area(15, 8, 252, 23)),
	     area(Ausgeschriebene_Diagnose_mit_Grunderkrankung,
		  area(15, 39, 482, 18)),
	     area(_tiologie,
		  area(133, 73, 363, 52)),
	     area(Sonstiges__tiologie__,
		  area(213, 138, 281, 18)),
	     area(Risikofaktoren,
		  area(101, 179, 400, 52)),
	     area(Sonstige_Risikofaktoren__,
		  area(193, 243, 310, 18)),
	     area(Diagnosegrundlage,
		  area(83, 283, 423, 52)),
	     area(Sonstige_Diagnosegrundlage__,
		  area(171, 348, 335, 18)),
	     area(Gelenke___Weichteile,
		  area(75, 385, 433, 52)),
	     area(Behand_Ziel,
		  area(14, 449, 252, 23)),
	     area(Verl_ngerung__cm_,
		  area(104, 481, 145, 18)),
	     area(Achskorrektur_Grad,
		  area(99, 507, 151, 18)),
	     area(Derotation_Grad,
		  area(121, 535, 130, 18))
	   ]
       ]).
dialog(deform_d_u_extr_verlafsdok_1,
       [ object        :=
	   Deform_D_U_Extr_Verlafsdok__,
	 parts         :=
	   [ Deform_D_U_Extr_Verlafsdok__ :=
	       dialog('Deform D U Extr Verlafsdok 1'),
	     Gel_Strek_Beug               :=
	       label(gel_strek_beug, 'Gelenke Streckung/Beugung'),
	     Huef                         :=
	       label(huef, 'H�fte'),
	     Normalwert_____1             :=
	       slider('Normalwert (10)', -10, 20, 1),
	     Komplikationen               :=
	       menu('Komplikationen', toggle),
	     Normalwert____1              :=
	       slider('Normalwert (0)', -10, 20, 1),
	     Normalwert______1            :=
	       slider('Normalwert (130)', 100, 160, 1),
	     Kni                          :=
	       label(kni, 'Knie'),
	     Normalwert____2              :=
	       slider('Normalwert (5)', -10, 20, 1),
	     Normalwert____3              :=
	       slider('Normalwert (0)', -10, 20, 1),
	     Normalwert______2            :=
	       slider('Normalwert (140)', 100, 160, 1),
	     Normalwert_____2             :=
	       slider('Normalwert (30)', -10, 60, 1),
	     Sprunggel                    :=
	       label(sprunggel, 'Sprunggelenk'),
	     Normalwert____4              :=
	       slider('Normalwert (0)', -10, 20, 1),
	     Intervention                 :=
	       menu('Intervention', toggle),
	     Normalwert_____3             :=
	       slider('Normalwert (40)', 20, 60, 1),
	     Sonstige_Intervention__      :=
	       text_item('sonstige Intervention :'),
	     Distraktionsrate_Tage        :=
	       slider('Distraktionsrate/Tage', 1, 31, 1)
	   ],
	 modifications :=
	   [ Gel_Strek_Beug          :=
	       [ font := @helvetica_roman_18
	       ],
	     Huef                    :=
	       [ font := @helvetica_roman_18
	       ],
	     Komplikationen          :=
	       [ columns   := 4,
		 alignment := right,
		 reference := point(0, 13),
		 append    := [ menu_item('Gelenke',
					  @default,
					  'Gelenke',
					  @off,
					  @nil,
					  g),
				menu_item('Nerven', @default, 'Nerven', @off, @nil, n),
				menu_item('Kallus', @default, 'Kallus', @off, @nil, k),
				menu_item('Gef��e', @default, 'Gef��e', @off, @nil, e),
				menu_item('Infektion oberfl.',
					  @default,
					  'Infektion oberfl.',
					  @off,
					  @nil,
					  i),
				menu_item('Infektion tief',
					  @default,
					  'Infektion tief',
					  @off,
					  @nil,
					  t),
				menu_item('Fixateur',
					  @default,
					  'Fixateur',
					  @off,
					  @nil,
					  f),
				menu_item(keine, @default, keine)
			      ]
	       ],
	     Kni                     :=
	       [ font := @helvetica_roman_18
	       ],
	     Sprunggel               :=
	       [ font      := @helvetica_roman_18,
		 alignment := right
	       ],
	     Normalwert____1         :=
	       [ alignment := right
	       ],
	     Intervention            :=
	       [ columns   := 4,
		 alignment := right,
		 reference := point(0, 13),
		 append    := [ menu_item('Neuinstrumentierung',
					  @default,
					  'Neuinstrumentierung',
					  @off,
					  @nil,
					  n),
				menu_item('Systemwechsel',
					  @default,
					  'Systemwechsel',
					  @off,
					  @nil,
					  s),
				menu_item('Weichteiloperation',
					  @default,
					  'Weichteiloperation',
					  @off,
					  @nil,
					  w),
				menu_item('Septische Operation',
					  @default,
					  'Septische Operation',
					  @off,
					  @nil,
					  e),
				menu_item('Rekortikotomie',
					  @default,
					  'Rekortikotomie',
					  @off,
					  @nil,
					  r),
				menu_item('Abbruch',
					  @default,
					  'Abbruch',
					  @off,
					  @nil,
					  a),
				menu_item('Narkosenmobilisation',
					  @default,
					  'Narkosenmobilisation',
					  @off,
					  @nil,
					  k),
				menu_item(sonstige,
					  @default,
					  sonstige,
					  @off,
					  @nil,
					  o)
			      ]
	       ],
	     Sonstige_Intervention__ :=
	       [ length    := 31,
		 alignment := right
	       ]
	   ],
	 layout        :=
	   [ area(Gel_Strek_Beug,
		  area(15, 8, 252, 23)),
	     area(Huef,
		  area(15, 39, 252, 23)),
	     area(Normalwert_____3,
		  area(15, 70, 389, 20)),
	     area(Komplikationen,
		  area(448, 71, 358, 69)),
	     area(Normalwert____4,
		  area(21, 98, 382, 20)),
	     area(Normalwert______2,
		  area(8, 127, 400, 20)),
	     area(Kni,
		  area(16, 158, 252, 23)),
	     area(Normalwert____3,
		  area(26, 182, 382, 20)),
	     area(Normalwert____2,
		  area(25, 208, 382, 20)),
	     area(Normalwert______1,
		  area(11, 231, 400, 20)),
	     area(Normalwert_____2,
		  area(17, 285, 389, 20)),
	     area(Sprunggel,
		  area(18, 261, 252, 23)),
	     area(Normalwert____1,
		  area(25, 308, 382, 20)),
	     area(Intervention,
		  area(468, 256, 338, 69)),
	     area(Normalwert_____1,
		  area(19, 330, 372, 20)),
	     area(Sonstige_Intervention__,
		  area(477, 330, 326, 18)),
	     area(Distraktionsrate_Tage,
		  area(15, 384, 394, 20))
	   ]
       ]).
dialog(deform_d_u_extr_verlaufsdok_2,
       [ object        :=
	   Deform_D_U_Extr_Verlaufsdok__,
	 parts         :=
	   [ Deform_D_U_Extr_Verlaufsdok__          :=
	       dialog('Deform D U Extr Verlaufsdok 2'),
	     OP_Datum___TTMMJJ                      :=
	       text_item('OP Datum / TTMMJJ'),
	     OP_Lokalisation                        :=
	       text_item('OP Lokalisation'),
	     Operationstechnik                      :=
	       menu('Operationstechnik', choice),
	     Fixationsmethode                       :=
	       menu('Fixationsmethode', choice),
	     Sonstige_Fixationsmethode__            :=
	       text_item('sonstige Fixationsmethode :'),
	     Sonstige_Operationstechnik__           :=
	       text_item('sonstige Operationstechnik :'),
	     Besonderheiten__                       :=
	       text_item('Besonderheiten :'),
	     Datum_f_r_Metallentfernung__           :=
	       text_item('Datum f�r Metallentfernung :'),
	     Datum_f_r_Gips_Hilfsmittelentfernung__ :=
	       text_item('Datum f�r Gips/Hilfsmittelentfernung :'),
	     Datum_f_r_Vollbelastung__              :=
	       text_item('Datum f�r Vollbelastung :'),
	     Sp_tsch_den                            :=
	       menu('Sp�tsch�den', toggle),
	     Sonstiges__                            :=
	       text_item('sonstiges :'),
	     Anzahl_Narkosen                        :=
	       menu('Anzahl Narkosen', choice),
	     Ziel_erreicht__                        :=
	       menu('Ziel erreicht :', choice),
	     Sonstige_Sp_tsch_den__                 :=
	       text_item('sonstige Sp�tsch�den :')
	   ],
	 modifications :=
	   [ OP_Datum___TTMMJJ                      :=
	       [ length := 10
	       ],
	     OP_Lokalisation                        :=
	       [ length := 26
	       ],
	     Operationstechnik                      :=
	       [ columns   := 4,
		 reference := point(0, 13),
		 append    := [ menu_item(monofokal,
					  @default,
					  monofokal,
					  @off,
					  @nil,
					  m),
				menu_item(bifokal,
					  @default,
					  bifokal,
					  @off,
					  @nil,
					  b),
				menu_item(simultan,
					  @default,
					  simultan,
					  @off,
					  @nil,
					  s),
				menu_item('Akutkorrektur',
					  @default,
					  'Akutkorrektur',
					  @off,
					  @nil,
					  a),
				menu_item('Kallosdistraktion',
					  @default,
					  'Kallosdistraktion',
					  @off,
					  @nil,
					  k),
				menu_item('Epiphysendistraktion',
					  @default,
					  'Epiphysendistraktion',
					  @off,
					  @nil,
					  e),
				menu_item(sonstige,
					  @default,
					  sonstige,
					  @off,
					  @nil,
					  o)
			      ]
	       ],
	     Fixationsmethode                       :=
	       [ columns   := 4,
		 reference := point(0, 13),
		 append    := [ menu_item('Ringfixateur',
					  @default,
					  'Ringfixateur',
					  @off,
					  @nil,
					  r),
				menu_item('unilateraler Fixat.',
					  @default,
					  'unilateraler Fixat.',
					  @off,
					  @nil,
					  u),
				menu_item('Nagel', @default, 'Nagel', @off, @nil, n),
				menu_item('Platte', @default, 'Platte', @off, @nil, p),
				menu_item(sonstige,
					  @default,
					  sonstige,
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Sonstige_Fixationsmethode__            :=
	       [ length    := 26,
		 alignment := right
	       ],
	     Sonstige_Operationstechnik__           :=
	       [ length := 26
	       ],
	     Besonderheiten__                       :=
	       [ length := 26
	       ],
	     Datum_f_r_Metallentfernung__           :=
	       [ length := 10
	       ],
	     Datum_f_r_Gips_Hilfsmittelentfernung__ :=
	       [ length := 9
	       ],
	     Datum_f_r_Vollbelastung__              :=
	       [ length := 9
	       ],
	     Sp_tsch_den                            :=
	       [ columns   := 4,
		 reference := point(0, 13),
		 append    := [ menu_item('Achse', @default, 'Achse', @off, @nil, a),
				menu_item('Gelenke',
					  @default,
					  'Gelenke',
					  @off,
					  @nil,
					  g),
				menu_item('Nerven', @default, 'Nerven', @off, @nil, n),
				menu_item('L�nge', @default, 'L�nge', @off, @nil, l),
				menu_item('Pseudarthrose',
					  @default,
					  'Pseudarthrose',
					  @off,
					  @nil,
					  p),
				menu_item(sonstige,
					  @default,
					  sonstige,
					  @off,
					  @nil,
					  s)
			      ]
	       ],
	     Sonstiges__                            :=
	       [ alignment := center
	       ],
	     Anzahl_Narkosen                        :=
	       [ feedback  := show_selection_only,
		 alignment := center,
		 reference := point(0, 13),
		 append    := [ menu_item('geplant (min 1)',
					  @default,
					  'geplant (min 1)',
					  @off,
					  @nil,
					  g),
				menu_item(ben�tigt,
					  @default,
					  ben�tigt,
					  @off,
					  @nil,
					  b),
				menu_item(ausstehend,
					  @default,
					  ausstehend,
					  @off,
					  @nil,
					  a)
			      ]
	       ],
	     Ziel_erreicht__                        :=
	       [ feedback  := show_selection_only,
		 alignment := center,
		 reference := point(0, 13),
		 append    := [ menu_item(ganz, @default, ganz, @off, @nil, g),
				menu_item(teilweise,
					  @default,
					  teilweise,
					  @off,
					  @nil,
					  t),
				menu_item(nicht, @default, nicht, @off, @nil, n),
				menu_item('bez. Achse',
					  @default,
					  'bez. Achse',
					  @off,
					  @nil,
					  b),
				menu_item('bez. Rotation',
					  @default,
					  'bez. Rotation',
					  @off,
					  @nil,
					  e),
				menu_item('bez. L�nge',
					  @default,
					  'bez. L�nge',
					  @off,
					  @nil,
					  l)
			      ]
	       ],
	     Sonstige_Sp_tsch_den__                 :=
	       [ length := 24
	       ]
	   ],
	 layout        :=
	   [ area(OP_Datum___TTMMJJ,
		  area(15, 8, 201, 18)),
	     area(OP_Lokalisation,
		  area(458, 10, 259, 18)),
	     area(Operationstechnik,
		  area(24, 50, 368, 69)),
	     area(Fixationsmethode,
		  area(460, 52, 366, 69)),
	     area(Sonstige_Fixationsmethode__,
		  area(459, 108, 329, 18)),
	     area(Sonstige_Operationstechnik__,
		  area(21, 140, 332, 18)),
	     area(Besonderheiten__,
		  area(460, 137, 263, 18)),
	     area(Datum_f_r_Metallentfernung__,
		  area(22, 195, 241, 18)),
	     area(Datum_f_r_Gips_Hilfsmittelentfernung__,
		  area(23, 220, 293, 18)),
	     area(Datum_f_r_Vollbelastung__,
		  area(23, 244, 216, 18)),
	     area(Sp_tsch_den,
		  area(23, 297, 339, 69)),
	     area(Sonstiges__,
		  area(465, 358, 228, 18)),
	     area(Anzahl_Narkosen,
		  area(466, 329, 221, 18)),
	     area(Ziel_erreicht__,
		  area(467, 297, 185, 18)),
	     area(Sonstige_Sp_tsch_den__,
		  area(22, 371, 290, 18))
	   ]
       ]).
