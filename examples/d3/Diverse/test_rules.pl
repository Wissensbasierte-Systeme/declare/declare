d3_finding(indicated_question, 'Q1').

d3_finding(indicated_question, 'Q2') :-
   d3_finding(indicated_question, 'Q1').
d3_finding(indicated_question, 'Mf1') :-
   d3_finding(indicated_question, 'Q2'),
   assert(d3_finding(indicated_question, 'Mf1')).
d3_finding(indicated_question, 'Mf2') :-
   d3_finding(indicated_question, 'Q2').
d3_finding(indicated_question, 'Mf3') :-
   d3_finding(indicated_question, 'Q2').

d3_finding('Msi1', 'Msi1a1') :-
   ( d3_condition(equal, 'Mf1', 'Mf1a1')
   ; d3_condition(equal, 'Mf2', 'Mf2a2') ).
d3_finding('P1', 'P4') :-
   d3_finding('Msi1', 'Msi1a1').

d3_finding('Msi3', X) :-
   d3_condition(equal, 'Mf3', X).

works(SSN, Projects, Hours_List=Hours_Sum) :-
   d3_finding('Msi3', 'Mf3_works'),
   ddbase_aggregate(
      [SSN, list(PNO), list_to_sum_for_test(Hours), sum(Hours)],
      ddbase_call( odbc(mysql),
         company:works_on(SSN, PNO, Hours) ),
      Pairs ),
   member([SSN, Projects, Hours_List, Hours_Sum], Pairs).

work_on_same_project(X,Y,P) :-
   d3_condition(equal, 'Mf3', 'Mf3_works_on_same_project'),
   Rule = ( work_on_same_project(X,Y,P) :-
      works_on(X,P,_), works_on(Y,P,_), X \= Y, P < 3 ),
   ddbase_query( odbc(mysql),
      company:Rule ).

