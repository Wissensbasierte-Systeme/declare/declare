KB ---> edges:Edges :-
   findall( Q1-Q2,
      ( QContainer := KB^_^'QContainer',
        Q1 := QContainer@'ID',
        Q2 := QContainer^'Children'^'Child'@'ID' ),
      Edges_2 ),
   list_to_ord_set(Edges_2, Edges_3),
   reachable_edges('Qcl600', Edges_3, Edges_4),
   findall( edge:[source:Q1, target:Q2]:[],
      member(Q1-Q2, Edges_4),
      Edges ).
