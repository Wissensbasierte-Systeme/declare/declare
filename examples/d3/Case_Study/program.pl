

d3_diagnosis('Absicherung', X1) :-
   d3_score('Q1' = X2, [0, 6]),
   d3_score('Q2' = X3, [3, 3, 6]),
   d3_score('Q3' = X4, [0, 3, 3, 0]),
   d3_score('Q4' = X5, [2, 1]),
   d3_score('Q5' = X6, [2, 0]),
   d3_score('Q6' = X7, [2, 0]),
   d3_score('Q7' = X8, [0, 2]),
   add([X2, X3, X4, X5, X6, X7, X8], X1).
d3_diagnosis('Aktien', X1) :-
   d3_score('Q1' = X2, [12, 6]),
   d3_score('Q2' = X3, [5, 4, 1]),
   d3_score('Q3' = X4, [4, '1.5', 1, 1]),
   d3_score('Q4' = X5, [2, 7]),
   d3_score('Q5' = X6, [0, 8]),
   d3_score('Q6' = X7, [0, 8]),
   d3_score('Q7' = X8, [8, 0]),
   add([X2, X3, X4, X5, X6, X7, X8], X1).
d3_diagnosis('Altersvorsorge', X1) :-
   d3_score('Q1' = X2, [0, 6]),
   d3_score('Q2' = X3, [2, 2, 1]),
   d3_score('Q3' = X4, [0, 2, 3, 0]),
   d3_score('Q4' = X5, [0, 0]),
   d3_score('Q5' = X6, [2, 0]),
   d3_score('Q6' = X7, [2, 0]),
   d3_score('Q7' = X8, [0, 2]),
   add([X2, X3, X4, X5, X6, X7, X8], X1).
d3_diagnosis('Bausparen', X1) :-
   d3_score('Q1' = X2, [0, 6]),
   d3_score('Q2' = X3, [3, 4, 5]),
   d3_score('Q3' = X4, [1, 1, 0, 0]),
   d3_score('Q4' = X5, [0, 0]),
   d3_score('Q5' = X6, ['0.5', 0]),
   d3_score('Q6' = X7, ['0.5', 0]),
   d3_score('Q7' = X8, [0, '0.5']),
   add([X2, X3, X4, X5, X6, X7, X8], X1).
d3_diagnosis('Festgeld', X1) :-
   d3_score('Q1' = X2, ['13.5', 6]),
   d3_score('Q2' = X3, [5, 4, 5]),
   d3_score('Q3' = X4, [5, '1.5', 1, '6.5']),
   d3_score('Q4' = X5, [6, 2]),
   d3_score('Q5' = X6, ['5.5', 0]),
   d3_score('Q6' = X7, ['5.5', 0]),
   d3_score('Q7' = X8, [0, '5.5']),
   add([X2, X3, X4, X5, X6, X7, X8], X1).
d3_diagnosis('Immobilie', X1) :-
   d3_score('Q1' = X2, ['4.5', 0]),
   d3_score('Q2' = X3, [2, 3, 2]),
   d3_score('Q3' = X4, [0, 1, 2, '2.5']),
   d3_score('Q4' = X5, [0, 0]),
   d3_score('Q5' = X6, [0, 2]),
   d3_score('Q6' = X7, [0, 2]),
   d3_score('Q7' = X8, [2, 0]),
   add([X2, X3, X4, X5, X6, X7, X8], X1).


