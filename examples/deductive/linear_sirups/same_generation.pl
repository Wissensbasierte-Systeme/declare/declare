
sg(X_1, X_2) :-
   a(X_1, U_2), sg(U_1, U_2), b(X_2, U_1).
sg(X_1, X_2) :-
   c(X_1, X_2).

a(X, Y) :-
   par(X, Y).
b(X, Y) :-
   par(X, Y).
c(X_1, X_2) :-
   par(X_1, Y_1), par(X_2, Y_1).

par(anne, elizabeth).
par(anne, philip).
par(andrew, elizabeth).
par(andrew, philip).
par(charles, elizabeth).
par(charles, philip).
par(edward, elizabeth).
par(edward, philip).
par(harry, diana).
par(harry, charles).
par(william, diana).
par(william, charles).

