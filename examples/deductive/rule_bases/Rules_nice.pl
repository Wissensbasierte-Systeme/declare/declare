:-   discontiguous(/(finding, 2)).


'Anstroemung RL Fuehler' :-
   'Kessel'([aus]),
   'Vorlauftemperatur'([nicht, parabelfoermig, fallend]),
   'T_RL'([parabelfoermig, fallend]).

'Anstroemung VL_T Fuehler' :-
   'Kessel'([aus]),
   'T_VL'([parabelfoermig, fallend]),
   'RL_Temp'([nicht, parabelfoermig, fallend]).

'Legionellengefahr' :-
   'Temperatur'(['WW<', '45°C']).

'Ruecklaufanhebung gestoerter Ladezyklus' :-
   'Integral der HZG Puffer prim T_RL > 55°C waehrend HZG Pufferladung 30% ueberschreitet'.

found :-
   'Vorlauftemperatur'([]),
   'RL_T'(['Kessel', parabelfoermig, fallend]).

'hydraulischen Abgleich des Brauchwassernetzes pruefen' :-
   'Differenz zwischen Temperatur Zirku',
   'Temperatur'(['WW', >, '5K']).

'kein Brennwertbetrieb' :-
   'Kessel'(['Brenner', an]),
   'Integral der Kessel T_RL > 55°C im Definitionszeitraum 20% von Gesamtintegral der Temperatur ueberschreitet'.

'rueckwaerts Fremddurchstroemung des Kessels' :-
   'Kesselvolumenstrom'([<, null]).

'steigt WWT prim T_VL an' :-
   'TWE'(['DWM', oeffnet]).

anforderung(todo, ['Warmwasserladung']) :-
   'Temperatur'(['TWE', 'Puffer', unten, <, '58°C']).
anforderung(todo, ['Warmwasserladung', aus]) :-
   'Temperatur'(['TWE', 'Puffer', unten, >, '62°C']).

ansonsten(finding('Ursache:', ['Kesselausfall']), finding('TWE', ['Pumpen', ausgefallen])) :-
   'WWB'([ausgefallen]),
   '30 Minuten Kessel T_VL < 45°C'.

finding(X1, X2) :-
   und(X3, finding(X1, X2)).
finding(X1, X2) :-
   und(finding(X1, X2), X3).

'Fehler:'(['Durchstroemung', des, 'Kessels']) :-
   'Kessel'([aus]),
   'T_VL== T_RL > Grenzwert parabelfoermige Kurve + Sicherheitsfaktor (5K)'.

'HZG'(['Pufferladung', an]) :-
   'Brenner'([an]),
   'Volumenstrom Kessel > null'.
'HZG'(['Pufferladung', aus]) :-
   'Brenner'([aus]),
   'Volumenstrom Kessel == null'.

'KVS des Ventils'(['ggf.', zu, klein]) :-
   'HK'(['T_VL', periodisch, schwankt, 'T_Au�\237\en', '>15°C']).
'KVS des Ventils'([zu, klein]) :-
   'HK'(['T_VL', periodisch, schwankt, 'T_Au�\237\en', <, '10°C']).

'Kessel'(['Brenner', an]) :-
   'Kesselpumpe'([an]),
   'HZG'(['Puffer', 'T_RL', <, '65°C']).
'Kessel'(['Brenner', aus]) :-
   'Gasverbrauch'([ueber, fuenf, 'Messintervalle', konstant, null]).
'Kessel'(['STW', ausgeloest]) :-
   'Kessel'(['T_VL', >, '100°C']),
   'Kessel'([fuer, '5', 'Messintervalle', aus]).
'Kessel'([taktet]) :-
   'Kessel'(['Brenner', wechsel, zwischen, an, und, aus, >, '5', 'Mal', in, '20', 'Min']).

'Kesselpumpe'([an]) :-
   'HZG'(['Puffer', prim, 'T_RL', <, '65°C']).
'Kesselpumpe'([an]) :-
   'Kessel'([aus]),
   'Kessel'(['T_VL', ==, 'Kessel', 'T_RL', ==, 'System', 'T_RL']).
'Kesselpumpe'([an]) :-
   'Kesselvolumenstrom'([ueber, 'Messintervall', >, null]).
'Kesselpumpe'([aus]) :-
   'Kesselvolumenstrom'([ueber, zwei, 'Messintervalle', ==, konstant, null]).

'Speicherladepumpe'([ein]) :-
   anforderung(done, ['Warmwasserladung', angefordert]).

'TWE'(['DWM', oeffnet]) :-
   anforderung(done, ['Warmwasserladung', angefordert]).
'TWE'([ausgefallen]) :-
   'Temperatur'(['WW', <, '30°C', fuer, '30', 'Minuten']).

'Waermeverluste'([durch, 'Kesseldurchstroemung', mit, 'Fremdwaerme']) :-
   'Kessel'([aus]),
   'T_AG nicht parabelfoermig gegen Raumtemperatur fallend'.

'Zirkulationspumpe'([aus]) :-
   'Temperatur'(['Zirku', <, '30°C']),
   'Temperatur'(['WW', >, '50']).

oder(finding('Fehler:', ['Zubringerpumpe', aus]), finding('DWM', [defekt])) :-
   'TWE'(['DWM', oeffnet]),
   'WWT'([prim, 'T_VL', nicht, ansteigt]),
   'Puffer sek T_VL > WWT prim T_VL'.
oder(oder(finding('Mischer', [ohne, 'Funktion']), finding('Kennlinie', [ohne, 'Steigung'])), finding('Au�\237\entemperaturfuehler', [defekt])) :-
   'HK'(['T_VL', '!=', 'f(T_Au�\237\en)']).

'Fehler:'([unnoetiger, 'Betrieb', 'Kesselpumpe']) :-
   'Brenner'([aus]),
   'Volumenstrom Kessel > null',
   'HZG'(['Puffer', prim, 'T_VL', fallend]).

'Waermeverlust ueber AG-System' :-
   'Brenner'([aus]),
   'Volumenstrom Kessel > null',
   'HZG'(['Puffer', prim, 'T_VL', fallend]).

'STB'(['Grenze', ermittelt]) :-
   'Brenner'(['5', 'Mal', wiederholend, innerhalb, von, '30', 'Minuten', bei, gleicher, 'Kessel', 'T_VL', abschaltet]),
   'Kessel'(['T_VL', laeuft, gegen, 'Kessel', 'T_RL']).

'Fehler:'(['STB', 'Grenzabschaltung']) :-
   'Brenner'(['5', 'Mal', wiederholend, innerhalb, von, '30', 'Minuten', bei, gleicher, 'Kessel', 'T_VL', abschaltet]),
   'Kessel'(['T_VL', laeuft, gegen, 'Kessel', 'T_RL']).

'Vorlauftemperatur'([]) :-
   'Kessel'([aus]).

'RL_T'(['Kessel', parabelfoermig, fallend]) :-
   'Kessel'([aus]).
