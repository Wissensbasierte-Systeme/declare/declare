

% 37 rules


:- discontiguous finding/2.


% finding('Kessel', [aus]).

finding(A1, A2) :-
   und(finding(A1, A2), _).
finding(B1, B2) :-
   und(_, finding(B1, B2)).

finding('Kessel', ['Brenner', aus]) :-
   finding('Gasverbrauch', [ueber, fuenf, 'Messintervalle', konstant, null]).

found :-
   finding('Vorlauftemperatur', []),
   finding('RL_T', ['Kessel', parabelfoermig, fallend]).

und(finding('Vorlauftemperatur', []), finding('RL_T', ['Kessel', parabelfoermig, fallend])) :-
   finding('Kessel', [aus]).

'Anstroemung RL Fuehler' :-
   finding('Kessel', [aus]),
   finding('Vorlauftemperatur', [nicht, parabelfoermig, fallend]),
   finding('T_RL', [parabelfoermig, fallend]).

'Anstroemung VL_T Fuehler' :-
   finding('Kessel', [aus]),
   finding('T_VL', [parabelfoermig, fallend]),
   finding('RL_Temp', [nicht, parabelfoermig, fallend]).

finding('Fehler:', ['Durchstroemung', des, 'Kessels']) :-
   finding('Kessel', [aus]),
   'T_VL== T_RL > Grenzwert parabelfoermige Kurve + Sicherheitsfaktor (5K)'.
finding('Kesselpumpe', [an]) :-
   finding('Kessel', [aus]),
   finding('Kessel', ['T_VL', ==, 'Kessel', 'T_RL', ==, 'System', 'T_RL']).
finding('Waermeverluste', [durch, 'Kesseldurchstroemung', mit, 'Fremdwaerme']) :-
   finding('Kessel', [aus]),
   'T_AG nicht parabelfoermig gegen Raumtemperatur fallend'.
finding('Kesselpumpe', [aus]) :-
   finding('Kesselvolumenstrom', [ueber, zwei, 'Messintervalle', ==, konstant, null]).
finding('Kesselpumpe', [an]) :-
   finding('Kesselvolumenstrom', [ueber, 'Messintervall', >, null]).

'rueckwaerts Fremddurchstroemung des Kessels' :-
   finding('Kesselvolumenstrom', [<, null]).

und(finding('STB', ['Grenze', ermittelt]), finding('Fehler:', ['STB', 'Grenzabschaltung'])) :-
   finding('Brenner', ['5', 'Mal', wiederholend, innerhalb, von, '30', 'Minuten', bei, gleicher, 'Kessel', 'T_VL', abschaltet]),
   finding('Kessel', ['T_VL', laeuft, gegen, 'Kessel', 'T_RL']).

finding('Kessel', ['STW', ausgeloest]) :-
   finding('Kessel', ['T_VL', >, '100°C']),
   finding('Kessel', [fuer, '5', 'Messintervalle', aus]).
finding('Kessel', [taktet]) :-
   finding('Kessel', ['Brenner', wechsel, zwischen, an, und, aus, >, '5', 'Mal', in, '20', 'Min']).

'kein Brennwertbetrieb' :-
   finding('Kessel', ['Brenner', an]),
   'Integral der Kessel T_RL > 55°C im Definitionszeitraum 20% von Gesamtintegral der Temperatur ueberschreitet'.

finding('Kesselpumpe', [an]) :-
   finding('HZG', ['Puffer', prim, 'T_RL', <, '65°C']).
finding('Kessel', ['Brenner', an]) :-
   finding('Kesselpumpe', [an]),
   finding('HZG', ['Puffer', 'T_RL', <, '65°C']).
finding('HZG', ['Pufferladung', an]) :-
   finding('Brenner', [an]),
   'Volumenstrom Kessel > null'.
finding('HZG', ['Pufferladung', aus]) :-
   finding('Brenner', [aus]),
   'Volumenstrom Kessel == null'.

und(finding('Fehler:', [unnoetiger, 'Betrieb', 'Kesselpumpe']), 'Waermeverlust ueber AG-System') :-
   finding('Brenner', [aus]),
   'Volumenstrom Kessel > null',
   finding('HZG', ['Puffer', prim, 'T_VL', fallend]).

'Ruecklaufanhebung gestoerter Ladezyklus' :-
   'Integral der HZG Puffer prim T_RL > 55°C waehrend HZG Pufferladung 30% ueberschreitet'.

anforderung(todo, ['Warmwasserladung']) :-
   finding('Temperatur', ['TWE', 'Puffer', unten, <, '58°C']).

finding('TWE', ['DWM', oeffnet]) :-
   anforderung(done, ['Warmwasserladung', angefordert]).

'steigt WWT prim T_VL an' :-
   finding('TWE', ['DWM', oeffnet]).

finding('Fehler:', ['Zubringerpumpe', aus]) oder finding('DWM', [defekt]) :-
   finding('TWE', ['DWM', oeffnet]),
   finding('WWT', [prim, 'T_VL', nicht, ansteigt]),
   'Puffer sek T_VL > WWT prim T_VL'.

anforderung(todo, ['Warmwasserladung', aus]) :-
   finding('Temperatur', ['TWE', 'Puffer', unten, >, '62°C']).

finding('Speicherladepumpe', [ein]) :-
   anforderung(done, ['Warmwasserladung', angefordert]).
finding('Zirkulationspumpe', [aus]) :-
   finding('Temperatur', ['Zirku', <, '30°C']),
   finding('Temperatur', ['WW', >, '50']).

'hydraulischen Abgleich des Brauchwassernetzes pruefen' :-
   'Differenz zwischen Temperatur Zirku',
   finding('Temperatur', ['WW', >, '5K']).

'Legionellengefahr' :-
   finding('Temperatur', ['WW<', '45°C']).

finding('TWE', [ausgefallen]) :-
   finding('Temperatur', ['WW', <, '30°C', fuer, '30', 'Minuten']).

ansonsten(finding('Ursache:', ['Kesselausfall']), finding('TWE', ['Pumpen', ausgefallen])) :-
   finding('WWB', [ausgefallen]),
   '30 Minuten Kessel T_VL < 45°C'.

finding('Mischer', [ohne, 'Funktion']) oder finding('Kennlinie', [ohne, 'Steigung']) oder finding('Außentemperaturfuehler', [defekt]) :-
   finding('HK', ['T_VL', '!=', 'f(T_Außen)']).
finding('KVS des Ventils', ['ggf.', zu, klein]) :-
   finding('HK', ['T_VL', periodisch, schwankt, 'T_Außen', '>15°C']).
finding('KVS des Ventils', [zu, klein]) :-
   finding('HK', ['T_VL', periodisch, schwankt, 'T_Außen', <, '10°C']).
