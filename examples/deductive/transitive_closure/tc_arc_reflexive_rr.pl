
tc(X, Y) :-
   arc(X, Z), tc(Z, Y).
tc(X, X) :-
   node(X).

node(X) :-
   arc(X, _).
node(X) :-
   arc(_, X).

arc(a, b).
arc(b, c).
arc(c, d).

