

mother(X, Y) :-
   parents(X, Y, _).
father(X,Y) :-
   parents(X, _, Y).

parent(X, Y) :-
   mother(X, Y).
parent(X, Y) :-
   father(X, Y).

grandparent(X, Z) :-
   parent(X, Y), parent(Y, Z).

sibling(X,Y) :-
   parent(X, Z), parent(Y, Z), X \= Y.

ancestor(X,Y) :-
   parent(X, Y).
ancestor(X, Y) :-
   parent(X, Z), ancestor(Z, Y).

parents('Anne', 'Elizabeth', 'Philip').
parents('Andrew', 'Elizabeth', 'Philip').
parents('Charles', 'Elizabeth', 'Philip').
parents('Edward', 'Elizabeth', 'Philip').
parents('Harry', 'Diana', 'Charles').
parents('William', 'Diana', 'Charles').

person('Anne', 1950, female).
person('Andrew', 1960, male).
person('Charles', 1948, male).
person('Diana', 1961, female).
person('Edward', 1964, male).
person('Elizabeth', 1926, female).
person('Harry', 1984, male).
person('Philip', 1921, male).
person('William', 1982, male).


