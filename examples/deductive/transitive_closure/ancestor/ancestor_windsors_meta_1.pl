
ancestor_facts(Facts) :-
   File = 'examples/deductive/transitive_closure/ancestor/ancestor_windsors_1.pl',
   dread(pl, File, Program),
   tp_iteration_prolog(Program, prolog_module, [], Facts).

ancestor_pairs(Pairs) :-
   ancestor_facts(Facts),
   findall( [X, Y],
      member(ancestor(X, Y), Facts),
      Pairs ),
   xpce_display_table([young, old], Pairs).

