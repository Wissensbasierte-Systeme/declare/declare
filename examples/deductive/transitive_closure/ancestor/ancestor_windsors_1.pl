
ancestor(X, Y) :-
   parent(X, Z), ancestor(Z, Y).
ancestor(X,Y) :-
   parent(X, Y).

parent('Elizabeth', 'George').
parent('Charles', 'Elizabeth').
parent('Charles', 'Philip').
parent('William', 'Charles').

