
ancestor(X,Y) :- parent(X, Y).
ancestor(X, Y) :- parent(X, Z), ancestor(Z, Y).

sibling(X,Y) :- parent(X, Z), parent(Y, Z), X \= Y.
grandparent(X, Z) :- parent(X, Y), parent(Y, Z).
parent(X, Y) :- ( mother(X, Y) ; father(X, Y) ).

mother(X, M) :- person(X, _, _, [M, _]).
father(X, F) :- person(X, _, _, [_, F]).

person('Anne', 1950, female, ['Elizabeth', 'Philip']).
person('Andrew', 1960, male, ['Elizabeth', 'Philip']).
person('Charles', 1948, male, ['Elizabeth', 'Philip']).
person('Edward', 1964, male, ['Elizabeth', 'Philip']).
person('Harry', 1984, male, ['Diana', 'Charles']).
person('William', 1982, male, ['Diana', 'Charles']).
person('Diana', 1961, female, []).
person('Elizabeth', 1926, female, []).
person('Philip', 1921, male, []).


