X ---> calls:[] :-
   findall( A-B,
      ( D := X^_^'method-declaration',
        A := D@name,
        I := D^_^'method-invocation',
        B := I@name ),
      Edges ),
   edges_to_picture(Edges).
