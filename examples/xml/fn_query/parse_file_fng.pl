

X ---> text:[]:XML :-
   name(X, L),
   text_to_sentences(L, '', Out_1),
   concat_atom(['[', Out_1, ']'], Out_2),
   term_to_atom(XML, Out_2).


sentence(S) -->
   words(Ws),
   endsymbol(E),
   { atom_chars(A, [E]),
     append(Ws, [A], S) }.


letterboy(L) -->
   [L],
   { atom_chars(A, [L]),
     \+ end_symbol(A) }.


words([]) -->
   \+ letterboy(_), % bricht ab, wenn als n�chstes ein Endesymbol kommt
   \+ endexception(_),
   !.
words([W|R]) -->
   word(W),
   !,
   words(R).
words([]) -->
   [].


word(W) -->
   [L],
   { atom_chars(W, [L]),
     \+ end_symbol(W),
     separator(W) }.
word(W) -->
   letters(Ls),
   { atom_chars(W, Ls) }.


letters(X) -->
   endexception(Exc),
   { name(Exc, Ls) },
   !,
   letters(T),
   { append(Ls, T, X) }.
letters([L|T]) -->
   letter(L),
   !,
   letters(T).
letters([]) -->
   [].
letter(L) -->
   [L],
   { atom_chars(A, [L]),
     \+ separator(A) }.


endsymbol(E) -->
   [E],
   { atom_chars(A, [E]),
     end_symbol(A),
%    ???
     \+ end_exception(A) }.


endexception(Exception) -->
   [L],
   { atom_chars(A, [L]),
     starts_exception(A) },
   endexception(A, Exception),
   !.
endexception(Old, Exception) -->
   { end_exception(Old),
     Exception = Old },
   !.
endexception(Old, Exception) -->
   [L],
   { atom_chars(A, [L]),
     concat_atom([Old, A], New),
     starts_exception(New) },
   !,
   endexception(New, Exception).


