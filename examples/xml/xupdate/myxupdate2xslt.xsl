<?xml version="1.0" encoding="iso-8859-1"?>
<!--<?xml version="1.0" encoding="UTF-8"?>-->
<!-- 

XUPDATE-IF UND XUPDATE-VARIABLE MUSS NOCH GEMACHT WERDEN,
IST ABER IM WORKING DRAFT AUCH NICHT BESCHRIEBEN!!!

NOCH NICHT IMPLEMENTIERT:
namespace-Attribut bei xupdate: element, xupdate:attribute, ...

ALLGEMEINES PROBLEM: NICHT BEHEBBAR: CDATA sections werden nicht als
cdata uebernommen, sondern zu normalem text. das laesst sich leider
bei xslt nicht verhindern:
http://www.dpawson.co.uk/xsl/sect2/cdata.html#d982e216 und
http://www.biglist.com/lists/xsl-list/archives/200203/msg00065.html und
http://www.biglist.com/lists/xsl-list/archives/200203/msg00064.html und 

PROBLEM: XUPDATE:IF GEHT NICHT RICHTIG -
FUNKTIONIERT NICHT MIT BEISPIEL 5.8 aus script!!!
stattdessen lieber die test-klausel an alle darunter liegenden
update-emulations-funktionen uebergeben und diese in xpath-ausdruecke
in eckigen klammern einbauen lassen. oder <xsl:if test=""></xsl:if>
INNERHALB der durch die emulation erzeugten templates einruecken

PROBLEM MIT XUPDATE:VALUE-OF BEI BEISPIEL 5.1 AUS SCRIPT

EINSCHRAENKUNGEN:

//Protein/parent::* als select-Attribut bei beliebigen xupdate-befehlen ausser value-of und variable funktioniert nicht wegen parent-achse. Analog bei anderen achsen ausser child, attribute und descendant-or-self bei ausschreibung mit //
/addresses/address[@name='Andreas']/@name | /addresses/address[@name='Lars']/@name als xupdate:insert-after select-Attribut funktioniert nicht. 
(/addresses/address[@name='Andreas']/@name) select-Attribut funktioniert nicht. 

inhalt von cdata sections wird nach bedarf umgewandelt in character entities.


WEITERER NACHTEIL VON LEXUS: BUGGY! das geht nicht auf xhive demosite von lexus:

<?xml version="1.0"?>
<lexus:modifications version="1.0" xmlns:lexus="http://www.xmldb.org/xupdate">
  <lexus:insert-after select="/addresses"><test/></lexus:insert-after>
</lexus:modifications>


Auch das geht nicht:

<?xml version="1.0"?>
<lexus:modifications version="1.0" xmlns:lexus="http://www.xmldb.org/xupdate">
  <lexus:insert-before select="/addresses/address[@name='Andreas']/@name">
    <lexus:attribute name="id">2</lexus:attribute>
  </lexus:insert-before>
</lexus:modifications>

obwohl das hier geht:

<?xml version="1.0"?>
<lexus:modifications version="1.0" xmlns:lexus="http://www.xmldb.org/xupdate">
   <lexus:insert-after select="/addresses/address[@name='Andreas']/@name">
    <lexus:attribute name="id">2</lexus:attribute>
  </lexus:insert-after>
</lexus:modifications>
            

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xupdate="http://www.xmldb.org/xupdate" xmlns:msxsl-extension="urn:schemas-microsoft-com:xslt" xmlns:xalan="http://xml.apache.org/xalan" xmlns:exslt="http://exslt.org/common">
  <xsl:output method="xml" encoding="utf-8" indent="yes" />
  <!-- zum XSLT-Prozessor passende node-set Konversionsfunktion finden und als globale Variable festhalten, da sie mehrmals im Translator-Stylesheet gebraucht wird-->
  <xsl:variable name="nodeconversionfunction">
    <xsl:call-template name="GetNodeConversionFunction"/>
  </xsl:variable>
  <!-- 
    
  Haupt-Template-Regel zum Verarbeiten des XUpdate-Headers, zur Erschaffung der  und den passenden XSL-Headers, sowie gegebenenfalls zum Serialisieren der einzelnen Befehle des XUpdate-Programms-->
  <xsl:template match="//xupdate:modifications">
    <!--Enth�lt <xupdate:modifications> mehrere operationen, sind diese potentiell konfliktgef�rdet und m�ssen hintereinander ausgef�hrt werden. um den dadurch entstehenden overhead zu vermeiden, wird f�r den wichtigen sonderfall von einer operation eine sondervariante compiliert.-->
    <xsl:choose>
      <!-- 
      
      Einfache �bersetzungsvariante bei nur einer XUpdate-Operation-->
      <xsl:when test="count(child::*[starts-with(name(),'xupdate:')])&lt;2">
        <xsl:element name="xsl:stylesheet">
          <xsl:call-template name="OutputStylesheetAttributes"/>
          <xsl:apply-templates/>
          <xsl:comment>Hilfsfunktion zum Kopieren nicht ver�nderter Knoten</xsl:comment>
          <xsl:call-template name="CreateMatchAllTemplate"/>
        </xsl:element>
      </xsl:when>
      <!-- 
        
        Serialisierte Variante ohne Pufferung des Zwischenergebnisses in Variablen bei 2 oder mehr xupdate-Operationen
        Setzt umgebendeJjava-Klasse voraus, die die Stylesheets nacheinander anwendet und funktioniert nur bei xupdate-Programmen ohne         globable Variablen-->
      <xsl:when test="count(child::*[starts-with(name(),'xupdate:')])&gt;1 and not(xupdate:variable)">
        <xsl:element name="MultipleStylesheets">
          <xsl:element name="NrOfTotalSteps">
            <xsl:value-of select="count(node()[starts-with(name(),'xupdate:')])"/>
          </xsl:element>
          <xsl:for-each select="node()[starts-with(name(),'xupdate:')]">
            <xsl:element name="step{position()}">
              <xsl:element name="xsl:stylesheet">
                <xsl:call-template name="OutputStylesheetAttributes"/>
                <xsl:apply-templates select="."/>
                <xsl:comment>Hilfsfunktion zum Kopieren nicht ver�nderter Knoten</xsl:comment>
                <xsl:call-template name="CreateMatchAllTemplate"/>
              </xsl:element>
            </xsl:element>
          </xsl:for-each>
        </xsl:element>
      </xsl:when>
      <!-- 
        
        Serialisierte Variante mit Pufferung des Zwischenergebnisses in Variablen bei 2 oder mehr XUpdate-Operationen funktioniert immer-->
      <xsl:otherwise>
        <xsl:element name="xsl:stylesheet">
          <xsl:call-template name="OutputStylesheetAttributes"/>
          <!--erste globale Variable erschaffen, die alle Nodes des Ursprungsdokumentes beinhaltet-->
          <xsl:element name="xsl:variable">
            <xsl:attribute name="name">step0_result</xsl:attribute>
            <xsl:attribute name="select">/</xsl:attribute>
          </xsl:element>
          <!-- Operationen eine nach der anderen durchparsen.-->
          <xsl:for-each select="node()[starts-with(name(),'xupdate:')]">
            <!--Erst eine eindeudige ID fuer die Emulation des naechsten xupdate-Befehls ermitteln-->
            <xsl:variable name="step_id" select="concat('step',position())"/>
            <!-- die fuer diesen Schritt passenden Regeln erschaffen-->
            <!--die direkte �bersetzung des xupdate-befehls-->
            <xsl:comment>Anfang der Template-Regeln f�r Schritt <xsl:value-of select="position()"/>
            </xsl:comment>
            <xsl:apply-templates select=".">
              <xsl:with-param name="use-mode">
                <xsl:value-of select="$step_id"/>
              </xsl:with-param>
            </xsl:apply-templates>
            <!-- Die Hilfs-Regel zum Kopieren von XML-Bl�cken und Kommentaren mit zum Schritt passenden mode-Attribut-->
            <xsl:call-template name="CreateMatchAllTemplate">
              <xsl:with-param name="use-mode">
                <xsl:value-of select="$step_id"/>
              </xsl:with-param>
            </xsl:call-template>
            <xsl:comment>Ende der Template-Regeln f�r Schritt <xsl:value-of select="position()"/>
            </xsl:comment>
            <xsl:comment>Globale Variable f�r Zwischenergebnis Schritt <xsl:value-of select="position()"/>
            </xsl:comment>
            <!-- Nun f�r den n�chsten Schritt die tempor�re globale Variable erschaffen, die das Zwischenergebnis fasst.-->
            <xsl:element name="xsl:variable">
              <xsl:attribute name="name"><xsl:value-of select="$step_id"/>_result</xsl:attribute>
              <!-- Der Variablen als Wert den Output der fuer diesen Schritt passenden templates zuweisen-->
              <xsl:element name="xsl:apply-templates">
                <xsl:choose>
                  <!-- Der erste Schritt wird auf das Quelldokument angewendet -->
                  <xsl:when test="position()=1">
                    <xsl:attribute name="select">/</xsl:attribute>
                  </xsl:when>
                  <!-- Der n-te Schritt wird auf das Zwischenergebnis vom Schritt n-1 angewendet, vorher noch result tree fragment nach node set konvertieren -->
                  <xsl:otherwise>
                    <xsl:attribute name="select"><xsl:value-of select="$nodeconversionfunction"/>($step<xsl:value-of select="position()-1"/>_result)</xsl:attribute>
                  </xsl:otherwise>
                </xsl:choose>
                <xsl:attribute name="mode"><xsl:value-of select="$step_id"/></xsl:attribute>
              </xsl:element>
            </xsl:element>
          </xsl:for-each>
          <!-- das letzte Zwischenergebnis ist gleichzeitig das Endergebnis und muss ausgegeben werden-->
          <xsl:comment>Letztes Zwischenergebnis ausgeben </xsl:comment>
          <xsl:element name="xsl:template">
            <xsl:attribute name="match">/</xsl:attribute>
            <xsl:element name="xsl:copy-of">
              <xsl:attribute name="select"><xsl:value-of select="$nodeconversionfunction"/>($step<xsl:value-of select="count(child::*)"/>_result)</xsl:attribute>
            </xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!-- 
  
  
  xupdate:insert-before und insert-after
  insert-before (bzw. insert-after bestehen aus insgesamt 5 template-Regeln.

-->
  <!-- Haupt-template-Regel zum Verarbeiten von <xupdate:insert-after> und <xupdate:insert-before> -->
  <xsl:template match="//xupdate:insert-after | //xupdate:insert-before">
    <xsl:call-template name="InsertNodesBeforeOrAfterSelectedNode"/>
    <xsl:call-template name="InsertNodesIntoParentNode"/>
  </xsl:template>
  <xsl:template name="InsertNodesBeforeOrAfterSelectedNode">
    <xsl:element name="xsl:template">
      <xsl:attribute name="match"><xsl:value-of select="@select"/></xsl:attribute>
      <!--Neue Attributknoten hinter / vor Kontext-Attributknoten einf�gen -->
      <xsl:element name="xsl:if">
        <xsl:attribute name="test">count(. | ../@*) = count(../@*)</xsl:attribute>
        <xsl:if test="name()='xupdate:insert-before'">
          <xsl:apply-templates select="xupdate:attribute"/>
        </xsl:if>
        <xsl:element name="xsl:copy">
          <xsl:element name="xsl:apply-templates">
            <xsl:attribute name="select">attribute::*|child::node()</xsl:attribute>
          </xsl:element>
        </xsl:element>
        <xsl:if test="name()='xupdate:insert-after'">
          <xsl:apply-templates select="xupdate:attribute"/>
        </xsl:if>
      </xsl:element>
      <!--Neue Nicht-Attributknoten hinter / vor Kontext- Nicht-Attributknoten einf�gen -->
      <xsl:element name="xsl:if">
        <xsl:attribute name="test">not(count(. | ../@*) = count(../@*))</xsl:attribute>
        <xsl:if test="name()='xupdate:insert-before'">
          <xsl:apply-templates select="*[not(name()='xupdate:attribute')]"/>
        </xsl:if>
        <xsl:element name="xsl:copy">
          <xsl:element name="xsl:apply-templates">
            <xsl:attribute name="select">attribute::*|child::node()</xsl:attribute>
          </xsl:element>
        </xsl:element>
        <xsl:if test="name()='xupdate:insert-after'">
          <xsl:apply-templates select="*[not(name()='xupdate:attribute')]"/>
        </xsl:if>
      </xsl:element>
    </xsl:element>
  </xsl:template>
  <xsl:template name="InsertNodesIntoParentNode">
    <!-- Pfadausdruck aus dem select-Attribut um den letzten Schritt verk�rzen -->
    <xsl:variable name="PathWithoutLastElement">
      <xsl:call-template name="GetPathWithoutLastElement">
        <xsl:with-param name="InputPath" select="@select"/>
        <xsl:with-param name="OpenBracketCount" select="0"/>
        <xsl:with-param name="CloseBracketCount" select="0"/>
      </xsl:call-template>
    </xsl:variable>
    <!-- Letztes Element des Pfadausdrucks -->
    <xsl:variable name="LastElementOfPath">
      <xsl:value-of select="substring-after(@select,$PathWithoutLastElement)"/>
    </xsl:variable>
    <!-- Kompletter Pfad zum parent-Element des selektierten Knotens-->
    <xsl:variable name="PathToParentNode">
      <xsl:call-template name="GetPathToParentNode">
        <xsl:with-param name="PathWithoutLastElement" select="$PathWithoutLastElement"/>
        <xsl:with-param name="LastElementOfPath" select="$LastElementOfPath"/>
      </xsl:call-template>
    </xsl:variable>
    <!-- template-Regel mit match-Attribut zum parent-Knoten des selektierten Knotens erschaffen -->
    <xsl:element name="xsl:template">
      <xsl:attribute name="match"><xsl:value-of select="$PathToParentNode"/></xsl:attribute>
      <xsl:element name="xsl:copy">
        <!--Falls Attributknoten selektiert wurden, dann hier eventuelle neue child-Knoten einf�gen. -->
        <xsl:element name="xsl:if">
          <xsl:attribute name="test">count(<xsl:value-of select="$LastElementOfPath"/>) and (count(<xsl:value-of select="$LastElementOfPath"/> |   @*) = count(@*))</xsl:attribute>
          <!-- Attributknoten des parent-Knotens verarbeiten -->
          <xsl:element name="xsl:apply-templates">
            <xsl:attribute name="select">attribute::*</xsl:attribute>
          </xsl:element>
          <!-- neue child-Knoten an erster Stelle im parent-Knoten einf�gen, da sie allerh�chstens hinter dem letzten Attribut, also vor allen   anderen child-Knoten eingef�gt werden sollten -->
          <xsl:apply-templates select="*[not(name()='xupdate:attribute')]"/>
          <!-- child-Knoten des parent-Knotens verarbeiten -->
          <xsl:element name="xsl:apply-templates">
            <xsl:attribute name="select">child::node()</xsl:attribute>
          </xsl:element>
        </xsl:element>
        <!--Falls child-Knoten selektiert wurden, dann hier eventuelle neue Attributknoten einf�gen. -->
        <xsl:element name="xsl:if">
          <xsl:attribute name="test">count(<xsl:value-of select="$LastElementOfPath"/>) and not(count(<xsl:value-of select="$LastElementOfPath"/> |   @*) = count(@*))</xsl:attribute>
          <!-- Attributknoten des parent-Knotens verarbeiten -->
          <xsl:element name="xsl:apply-templates">
            <xsl:attribute name="select">attribute::*</xsl:attribute>
          </xsl:element>
          <!-- neue Attributknoten an erster Stelle im parent-Knoten einf�gen, da sie allerh�chstens vor dem ersten child-Knoten, also hinter allen anderen Attributknoten eingef�gt werden sollten -->
          <xsl:apply-templates select="xupdate:attribute"/>
          <!-- child-Knoten des parent-Knotens verarbeiten -->
          <xsl:element name="xsl:apply-templates">
            <xsl:attribute name="select">child::node()</xsl:attribute>
          </xsl:element>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
  <!-- Hilfs-Template-Regel zum ermitteln des XPath-Ausdruck zum parent-knoten eines selektierten knotens 
geh�rt zu <xupdate:insert-after> und <xupdate:insert-before>

-->
  <xsl:template name="GetPathToParentNode">
    <xsl:param name="PathWithoutLastElement"/>
    <xsl:param name="LastElementOfPath"/>
    <xsl:variable name="WholePath" select="@select"/>
    <xsl:variable name="NewPathPredicate">
      <xsl:choose>
        <xsl:when test="substring ($LastElementOfPath,1,1)='@'">[attribute::<xsl:value-of select="substring($WholePath,string-length($PathWithoutLastElement)+2,string-length($WholePath))"/>]</xsl:when>
        <xsl:otherwise>[child::<xsl:value-of select="substring-after($WholePath,$PathWithoutLastElement)"/>]</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="LastTwoCharacters" select="substring($PathWithoutLastElement,string-length($PathWithoutLastElement)-1,string-length($PathWithoutLastElement))"/>
    <xsl:choose>
      <!-- falls das letzte element mit // eingeleitet wird, dann muss das uebergeordnete element ueber die descendant-or-self achse bestimmt werden, da es auch genau das vom vorhergehenden string bezeichnete element sein koennte. descendant-or-self-achse geht nicht, also emulieren mit vereinigung aus child-Achse und //-->
      <xsl:when test="$LastTwoCharacters = '//'">
        <xsl:value-of select="substring($PathWithoutLastElement,1,string-length($PathWithoutLastElement)-2)"/>//*<xsl:value-of select="$NewPathPredicate"/>
        <!-- 

f�r das "self"-element bei dieser descendant-or-self-emulation muss man erst pr�fen, ob es �berhaupt ein self-element gibt...

-->
        <xsl:if test="string-length(substring($PathWithoutLastElement,1,string-length($PathWithoutLastElement)-2))"> | <xsl:value-of select="substring($PathWithoutLastElement,1,string-length($PathWithoutLastElement)-2)"/>
          <xsl:value-of select="$NewPathPredicate"/>
        </xsl:if>
      </xsl:when>
      <!-- 
      Hier wird der fall verarbeitet, dass das letzte element des select-attributes mit / (also nicht mit //) eingeleitet wurde
      ausserdem noch den Sonderfall "/" (stringlength 1)  ausschliesen, dann gibt es naemlich kein uebergeordnetes Element, und man kann folglich auch kein Attribut dort einfuegen.-->
      <xsl:when test="  not(substring ($LastTwoCharacters,1,1) = '/') 
                and substring ($LastTwoCharacters,2,2) = '/' 
                and not(string-length($PathWithoutLastElement) = 1)">
        <xsl:value-of select="substring($PathWithoutLastElement,1,string-length($PathWithoutLastElement)-1)"/>
        <xsl:value-of select="$NewPathPredicate"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">Fatal Error: <xsl:value-of select="name()"/> failed as the select attribute could not be transformed to the parent node. Match attribute <xsl:value-of select="@select"/> was transformed to <xsl:value-of select="$PathWithoutLastElement"/>.
        </xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!-- Extrahieren des Strings, der den pfad ohne das letzte element enth�lt (nicht zwangsl�ufig das parent-element...). 
  geh�rt zu <xupdate:insert-after> und <xupdate:insert-before>
    
  Dieser ansatz funktioniert nicht bei pfaden nach dem muster //Protein[1] | //Protein[2] oder auch (Protein1), daher warnung ausgeben und ausfuehrung beenden. -->
  <xsl:template name="GetPathWithoutLastElement">
    <xsl:param name="InputPath"/>
    <xsl:param name="OpenBracketCount"/>
    <xsl:param name="CloseBracketCount"/>
    <!-- Pfadausdr�cke die aus Vereinigungen bestehen (z.b. /* | /*/*) k�nnen diesen Algorithmus durcheinanderbringen, da dann m�glicherweise nur     ein Teil des Pfadausdrucks reduziert wird. daher lieber kontrolliert abbrechen. -->
    <xsl:if test="contains(InputPath,'|')">
      <xsl:message terminate="yes">Fatal Error: &lt;xupdate-insert-after> and  &lt;xupdate-insert-before> with select statements containing '|' are not supported by XUpdate2XSLT.</xsl:message>
    </xsl:if>
    <xsl:variable name="LastCharacter" select="substring($InputPath,string-length($InputPath),string-length($InputPath))"/>
    <xsl:choose>
      <!--Gew�hnliche Klammern machen den verk�rzten Pfadausdruck zu einem ung�ltigen Pfadausdruck, weswegen
      dieser Ansatz so nicht funktioniert. Daher lieber kontrolliert abbrechen.-->
      <xsl:when test="$LastCharacter = ')' and $CloseBracketCount = $OpenBracketCount">
        <xsl:message terminate="yes">Fatal Error: &lt;xupdate-insert-after> and  &lt;xupdate-insert-before> with select
         statements containing brackets () after the last XPath step and outside of predicates are not supported by XUpdate2XSLT
         ("e.g. (/SomeElement/SomeChildelement).")
        '</xsl:message>
      </xsl:when>
      <!-- Das erste / das nicht innerhalb eines Pr�dikats steht markiert den Punkt, ab dem der Letzte Schritt des Pfadausdrucks
      beginnt. Abbrechen und verk�rzten Pfadausdruck zur�ckliefern. -->
      <xsl:when test="$LastCharacter = '/' and $CloseBracketCount = $OpenBracketCount">
        <xsl:value-of select="$InputPath"/>
      </xsl:when>
      <!-- Ende (von hinten gesehen) einer Klausel. Pfadausdruck reduzieren, OpenBracketCounter decrementieren und rekursiv weitermachen -->
      <xsl:when test="$LastCharacter = '['">
        <xsl:call-template name="GetPathWithoutLastElement">
          <xsl:with-param name="InputPath" select="substring($InputPath,1,string-length($InputPath)-1)"/>
          <xsl:with-param name="OpenBracketCount" select="$OpenBracketCount+1"/>
          <xsl:with-param name="CloseBracketCount" select="$CloseBracketCount"/>
        </xsl:call-template>
      </xsl:when>
      <!-- Anfang (von hinten gesehen) einer Klausel. Pfadausdruck reduzieren, CloseBracketCounter incrementieren und rekursiv weitermachen -->
      <xsl:when test="$LastCharacter = ']'">
        <xsl:call-template name="GetPathWithoutLastElement">
          <xsl:with-param name="InputPath" select="substring($InputPath,1,string-length($InputPath)-1)"/>
          <xsl:with-param name="OpenBracketCount" select="$OpenBracketCount"/>
          <xsl:with-param name="CloseBracketCount" select="$CloseBracketCount+1"/>
        </xsl:call-template>
      </xsl:when>
      <!--Falls nichts anderes zutreffend war, befindet man sich innerhalb des letzten Schrittes des Pfadausdrucks. 
      Pfadausdruck reduzieren und rekursiv weitermachen-->
      <xsl:otherwise>
        <xsl:call-template name="GetPathWithoutLastElement">
          <xsl:with-param name="InputPath" select="substring($InputPath,1,string-length($InputPath)-1)"/>
          <xsl:with-param name="OpenBracketCount" select="$OpenBracketCount"/>
          <xsl:with-param name="CloseBracketCount" select="$CloseBracketCount"/>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!-- 
  
  xupdate:append-->
  <xsl:template match="//xupdate:append">
    <xsl:param name="use-mode"/>
    <xsl:element name="xsl:template">
      <xsl:attribute name="match"><xsl:value-of select="@select"/></xsl:attribute>
      <!-- falls XUpdate-Befehle in mehreren Schritten verarbeitet werden, dann wird das passende mode-Attribut zum template hinzugef�gt-->
      <xsl:if test="$use-mode">
        <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
      </xsl:if>
      <!-- falls eine explizite Position zum einfuegen angegeben ist, werden die child-elemente einzeln iteriert, um die passende Einf�geposition einzuhalten.-->
      <xsl:if test="@child">
        <!-- ab hier wird das Kontextelement kopiert-->
        <xsl:element name="xsl:copy">
          <!-- matchen der im element enthaltenen Attribute
            Es ist wichtig, dass diese zuerst kopiert werden, sonst besteht die gefahr, 
            dass attribute durch zu frueh ausgegebene elementknoten verschwinden.
             -->
          <xsl:element name="xsl:apply-templates">
            <xsl:attribute name="select">attribute::*</xsl:attribute>
            <xsl:if test="$use-mode">
              <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
            </xsl:if>
          </xsl:element>
          <!--bestehende Attribut-elemente kopieren-->
          <xsl:apply-templates select="xupdate:attribute"/>
          <!-- nun die child-Knoten uebernehmen, und an der geeigneten Stelle die neuen Knoten einfuegen.-->
          <xsl:element name="xsl:for-each">
            <xsl:attribute name="select">child::node()</xsl:attribute>
            <xsl:element name="xsl:if">
              <!-- Falls die richtige Einf�geposition gefunden ist, dann die xupdate-befehle zu Einf�gen der neuen Knoten ausf�hren. Bei einem child-Attribut mit Wert last() gibt es eine Sonderbehandlung, da mit dem child-Sttribut nicht der index eines Elementes aus dem Ausgangsdokument bezeichnet wird, sondern die Position, die das neue Element in dem outputelement erhalten soll. last als child-Attrbut bei xupdate:append zaehlt also wie der vor der Operation enthaltene wert von last() plus 1. -->
              <xsl:attribute name="test">position()=<xsl:value-of select="@child"/> and not(string(&quot;<xsl:value-of select="@child"/>&quot;) = string(&quot;last()&quot;))</xsl:attribute>
              <!-- Einfuegen der neuen Knoten, au�er den Attributknoten, denn die wurden ja schon eingef�gt.-->
              <xsl:apply-templates select="node()[not(name()='xupdate:attribute')]"/>
            </xsl:element>
            <!-- Kopieren der restlichen in dem element enthaltenen knoten -->
            <xsl:element name="xsl:apply-templates">
              <xsl:attribute name="select">self::*</xsl:attribute>
              <xsl:if test="$use-mode">
                <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
              </xsl:if>
            </xsl:element>
            <!-- workaround nur fuer den spezialfall das als position last() oder anzahl der elemente (vor dem einfuegen) + 1 angegeben  wurde. xupdate bezeichnet mit dem index naemlich die position des einzufuegenden elementes NACHDEM es eingefuegt wurde. In xsl arbeitet man allerdings mit dem ist-zustand des Dokumentes VOR der Transformation. Somit ist last() beim xsl-durchlauf nicht das gleiche wie child="last()". daher ist diese sonderbehandlung noetig.
          1. falls das der letzte durchlauf ist
          2. und falls als einfuegepostion "last()" angegeben wurde oder falls als einfuegeposition das jetzige last() +1 angegeben wurde
          dann fuege ein.
          -->
            <xsl:element name="xsl:if">
              <xsl:attribute name="test">position() = last() and ( string(&quot;last()&quot;) = string(&quot;<xsl:value-of select="string(@child)"/>&quot;) or (last()+1 = <xsl:value-of select="@child"/>))</xsl:attribute>
              <!-- einf�gen aller neuen knoten -->
              <xsl:apply-templates select="node()[not(name()='xupdate:attribute')]"/>
            </xsl:element>
          </xsl:element>
          <!-- Spezialfall behandeln, dass zwar ein child-Attribut angegeben wurde, aber das Kontextelement noch keine Knoten enth�lt. dann wurde n�mlich in der for-each-Schleife nichts eingef�gt.-->
          <xsl:element name="xsl:if">
            <xsl:attribute name="test">not(child::node())  </xsl:attribute>
            <!--bestehende Attribut-elemente kopieren-->
            <xsl:element name="xsl:apply-templates">
              <xsl:attribute name="select">attribute::*</xsl:attribute>
              <xsl:if test="$use-mode">
                <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
              </xsl:if>
            </xsl:element>
            <!-- Neue Attribut-Knoten einf�gen-->
            <xsl:for-each select="./xupdate:attribute">
              <xsl:call-template name="transform-xupdate-attribute"/>
            </xsl:for-each>
            <!-- Einf�gen aller sonstigen neuen Knoten -->
            <xsl:apply-templates select="node()[not(name()='xupdate:attribute')]"/>
          </xsl:element>
        </xsl:element>
      </xsl:if>
      <!-- Falls keine explizite Einfuegeposition gegeben ist, dann fuege alles  einfach am ende ein-->
      <xsl:if test="not(@child)">
        <!-- ab hier wird das Kontextelement kopiert-->
        <xsl:element name="xsl:copy">
          <!--Bestehende Attribut-Elemente kopieren-->
          <xsl:element name="xsl:apply-templates">
            <xsl:attribute name="select">attribute::*</xsl:attribute>
            <xsl:if test="$use-mode">
              <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
            </xsl:if>
          </xsl:element>
          <!-- Neue Attribut-Knoten einf�gen-->
          <xsl:for-each select="./xupdate:attribute">
            <xsl:call-template name="transform-xupdate-attribute"/>
          </xsl:for-each>
          <!-- Kopieren aller child-Knoten -->
          <xsl:element name="xsl:apply-templates">
            <xsl:attribute name="select">child::node()</xsl:attribute>
            <xsl:if test="$use-mode">
              <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
            </xsl:if>
          </xsl:element>
          <!-- Einf�gen aller sonstigen neuen Knoten -->
          <xsl:apply-templates select="node()[not(name()='xupdate:attribute')]"/>
        </xsl:element>
      </xsl:if>
    </xsl:element>
  </xsl:template>
  <!-- 
  
  xupdate:update

-->
  <xsl:template match="//xupdate:update">
    <xsl:param name="use-mode"/>
    <xsl:element name="xsl:template">
      <xsl:if test="$use-mode">
        <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
      </xsl:if>
      <xsl:attribute name="match"><xsl:value-of select="@select"/></xsl:attribute>
      <!-- ab hier abschnitt fuer Updates von Attributknoten-->
      <!-- dieser test liefert true zurueck, falls der gematchte node ein attribut ist. das liegt daran, dass in xpath nodes in einem node set  nicht         mehrmals aufgef�hrt werden, auch wenn man ihn mehrmals einfuegt. -->
      <xsl:element name="xsl:if">
        <xsl:attribute name="test">count(. | ../@*) = count(../@*)</xsl:attribute>
        <xsl:element name="xsl:attribute">
          <xsl:attribute name="name">{local-name()}</xsl:attribute>
          <xsl:apply-templates/>
        </xsl:element>
      </xsl:element>
      <!-- Abschnitt fuer Updates von Element-Knoten -->
      <xsl:element name="xsl:if">
        <xsl:attribute name="test">self::*</xsl:attribute>
        <!-- ab hier wird das Kontextelement kopiert-->
        <xsl:element name="xsl:copy">
          <!-- matchen der im element enthaltenen Attribute
Es ist wichtig, dass diese zuerst gematcht werden, sonst besteht die gefahr, 
dass attribute durch zu frueh ausgegebene elementknoten verschwinden.
-->
          <xsl:element name="xsl:apply-templates">
            <xsl:attribute name="select">attribute::*</xsl:attribute>
            <xsl:if test="$use-mode">
              <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
            </xsl:if>
          </xsl:element>
          <!-- neuen Inhalt des Knotens einfuegen -->
          <xsl:apply-templates/>
        </xsl:element>
      </xsl:element>
      <!-- Abschnitt fuer Updates von text-Knoten  -->
      <xsl:element name="xsl:if">
        <xsl:attribute name="test">self::text()</xsl:attribute>
        <!-- ab hier wird das Kontextelement kopiert-->
        <xsl:element name="xsl:text">
          <!-- neuen inhalt des knotens einfuegen -->
          <xsl:apply-templates/>
        </xsl:element>
      </xsl:element>
      <!-- Abschnitt fuer Updates von Kommentarknoten  -->
      <xsl:element name="xsl:if">
        <xsl:attribute name="test">self::comment()</xsl:attribute>
        <!-- ab hier wird das Kontextelement kopiert-->
        <xsl:element name="xsl:comment">
          <!-- neuen inhalt des knotens einfuegen -->
          <xsl:apply-templates/>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
  <!--
  
   xupdate:rename -->
  <xsl:template match="//xupdate:rename">
    <xsl:param name="use-mode"/>
    <xsl:param name="test"/>
    <xsl:element name="xsl:template">
      <xsl:if test="$use-mode">
        <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
      </xsl:if>
      <xsl:attribute name="match"><xsl:value-of select="@select"/><xsl:if test="$test">[<xsl:value-of select="$test"/>]</xsl:if></xsl:attribute>
      <!-- ab hier Abschnitt zum Umbenennen von XML-Attributen -->
      <xsl:element name="xsl:if">
        <xsl:attribute name="test">not(self::*)</xsl:attribute>
        <xsl:element name="xsl:attribute">
          <xsl:attribute name="name"><xsl:value-of select="."/></xsl:attribute>
          <!-- alter Wert des Attributs muss auch �bernommen werden -->
          <xsl:element name="xsl:value-of">
            <xsl:attribute name="select">.</xsl:attribute>
          </xsl:element>
        </xsl:element>
      </xsl:element>
      <!-- ab hier Abschnitt zum Umbenennen von XML-Elementen -->
      <xsl:element name="xsl:if">
        <xsl:attribute name="test">self::*</xsl:attribute>
        <xsl:element name="xsl:element">
          <xsl:attribute name="name"><xsl:value-of select="."/></xsl:attribute>
          <!-- meta-apply-templates zum �bernehmen der untergeordneten Elemente des Kontextelements.-->
          <xsl:element name="xsl:apply-templates">
            <xsl:attribute name="select">attribute::* | child::node()</xsl:attribute>
            <xsl:if test="$use-mode">
              <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
            </xsl:if>
          </xsl:element>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
  <!-- 
  
  xupdate:variable-->
  <xsl:template match="//xupdate:variable">
    <xsl:param name="use-mode"/>
    <xsl:element name="xsl:variable">
      <xsl:if test="@select">
        <xsl:attribute name="select"><xsl:value-of select="@select"/></xsl:attribute>
      </xsl:if>
      <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
      <!-- Inhalt von xupdate:variable verarbeiten, da es auch ohne select-Attribut und stattdessen mit impliziter Definition benutzt werden k�nnte-->
      <xsl:apply-templates>
        <xsl:with-param name="use-mode">
          <xsl:value-of select="$use-mode"/>
        </xsl:with-param>
      </xsl:apply-templates>
    </xsl:element>
  </xsl:template>
  <!--  xupdate:value-of -->
  <xsl:template match="//xupdate:value-of">
    <xsl:element name="xsl:copy-of">
      <xsl:attribute name="select"><xsl:if test="$nodeconversionfunction"><xsl:value-of select="$nodeconversionfunction"/>(</xsl:if><xsl:value-of select="@select"/><xsl:if test="$nodeconversionfunction">)</xsl:if></xsl:attribute>
    </xsl:element>
  </xsl:template>
  <!-- 
    
  xupdate:remove -->
  <xsl:template match="//xupdate:remove">
    <xsl:param name="use-mode"/>
    <xsl:element name="xsl:template">
      <xsl:if test="$use-mode">
        <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
      </xsl:if>
      <xsl:attribute name="match"><xsl:value-of select="@select"/></xsl:attribute>
      <!-- falls xupdate-befehle in mehreren Schritten verarbeitet werden, dann wird das passende mode-attribut zum template hinzugef�gt-->
      <xsl:if test="$use-mode">
        <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
      </xsl:if>
    </xsl:element>
  </xsl:template>
  <!--
  
  xupdate:comment -->
  <xsl:template match="xupdate:comment">
    <xsl:param name="use-mode"/>
    <xsl:element name="xsl:comment">
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>
  <!-- 
  
  xupdate:element-->
  <xsl:template match="xupdate:element">
    <xsl:param name="use-mode"/>
    <xsl:element name="xsl:element">
      <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>
  <!-- 
  
  xupdate:text-->
  <xsl:template match="xupdate:text">
    <xsl:param name="use-mode"/>
    <xsl:value-of select="."/>
  </xsl:template>
  <!-- 
  
  xupdate:attribute , es wird sowohl implizit (durch matchen) als auch explizit in xupdate:append aufgerufen.  -->
  <xsl:template match="xupdate:attribute" name="transform-xupdate-attribute">
    <xsl:param name="use-mode"/>
    <xsl:element name="xsl:attribute">
      <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>
  <!-- 
  
  xupdate:processing-instruction -->
  <xsl:template match="xupdate:processing-instruction">
    <xsl:param name="use-mode"/>
    <xsl:element name="xsl:processing-instruction">
      <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>
  <!--   
  
  <xupdate:if>,
  -->
  <xsl:template match="xupdate:if">
    <xsl:param name="test"/>
    <!-- <xsl:if> darf nur mit abosluten Pfadausdr�cken benutzt werden,  um seiteneffekte durch den verschobenen Ausf�hrungskotext zu vermeiden -->
    <xsl:if test="not(starts-with($test,'/'))">
      <xsl:message terminate="yes">xsl:if may only be used with absolute test attributes.</xsl:message>
    </xsl:if>
    <xsl:variable name="TestParameter">
      <xsl:value-of select="@test"/>
      <!-- Eventuelle Verschachtelung von <xupdate:if> ber�cksichtigen -->
      <xsl:if test="$test">and (<xsl:value-of select="$test"/>)</xsl:if>
    </xsl:variable>
    <xsl:apply-templates>
      <xsl:with-param name="test" select="$TestParameter"/>
    </xsl:apply-templates>
  </xsl:template>

  
  <!-- 
  
  
  Extrahierte Hilfs-Template-Regel zum Ausgeben der Attribute in den erzeugten <xsl:stylesheet> Elementen. 
-->
  <xsl:template name="OutputStylesheetAttributes">
    <!-- ein explizites xsl:attribute sorgt dafuer, dass das attribut nicht auch den namespace xsl bekommt (so wie alle elemente)-->
    <xsl:attribute name="version">1.0</xsl:attribute>
    <!--dieses pseudo-attribut hat zwar keine direkte Wirkung, zwingt den XSLT-Prozessor aber die ensprechenden Namespacedefinition der node set Funktionserweiterung in dem <xsl:stylesheet> Element einzuf�gen. Eine direkte Definition per xsl:attribute ist nicht m�glich, da eine namespacedefinition kein xml-attribut ist.-->
    <xsl:attribute name="{substring-before($nodeconversionfunction,':')}:bogus"/>
    <!-- Der soeben f�r das Stylesheet-Element definierte Namensraum darf aber nicht vom generierten Stylesheet ausgegeben werden. Genau das wird mit diesem Attribut verhindert. So vermeidet man, dass XML-Bl�cke im XUpdate-Code diesen Pr�fix erhalten -->
    <xsl:attribute name="exclude-result-prefixes"><xsl:value-of select="substring-before($nodeconversionfunction,':')"/></xsl:attribute>
    <xsl:element name="xsl:output">
      <xsl:attribute name="method">xml</xsl:attribute>
      <xsl:attribute name="indent">yes</xsl:attribute>
      <xsl:attribute name="encoding">utf-8</xsl:attribute>
    </xsl:element>
  </xsl:template>
  <!--
  
  
  
  
  Extrahierte Hilfs-Template-Regel zum erzeugen der folgenden Universal-Kopiertemplateregel:
  
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

-->
  <!--
das folgende meta-template erzeugt diese:
-->
  <xsl:template name="CreateMatchAllTemplate">
    <xsl:param name="use-mode"/>
    <xsl:element name="xsl:template">
      <xsl:attribute name="match">@*|node()</xsl:attribute>
      <!-- falls xupdate-befehle in mehreren Schritten verarbeitet werden, dann wird das passende mode-attribut zum template hinzugef�gt-->
      <xsl:if test="$use-mode">
        <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
      </xsl:if>
      <xsl:element name="xsl:copy">
        <xsl:element name="xsl:apply-templates">
          <xsl:attribute name="select">attribute::* | child::node()</xsl:attribute>
          <xsl:if test="$use-mode">
            <xsl:attribute name="mode"><xsl:value-of select="$use-mode"/></xsl:attribute>
          </xsl:if>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
  <!--

die  folgenden 3 templates �bernehmen xml-bl�cke als teil von xupdate-befehlen in das xsl-dokument und sorgt somit daf�r da� Sie z.b. bei einf�gungen und variablendefinitionen verwendet werden
ich verwende mit absicht nicht das gleiche template wie das oben erzeugte, da diese variante keine redundanten namespaces bei xml-bl�cken erzeugt.
-->
  <xsl:template match="comment()">
    <xsl:copy>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="*">
    <xsl:element name="{local-name()}">
      <xsl:apply-templates select="@*|node()"/>
    </xsl:element>
  </xsl:template>
  <xsl:template match="@*">
    <xsl:attribute name="{local-name()}"><xsl:value-of select="."/></xsl:attribute>
  </xsl:template>
  <xsl:template match="text()">
    <xsl:value-of select="."/>
  </xsl:template>
  <!-- 
  
  hilfs-template-regel zum finden des zum XSLT-Prozessor passenden funktionsnamens f�r konversion von RTF nach node-set
  
  
  -->
  <xsl:template name="GetNodeConversionFunction">
    <xsl:choose>
      <!-- nur xalan (alte versionen)-->
      <xsl:when test="function-available('xalan:node-set')">xalan:node-set</xsl:when>
      <!-- neuere versionen von xalan, sowie  saxon,  jd.xsl und nxslt -->
      <xsl:when test="function-available('exslt:node-set')">exslt:node-set</xsl:when>
      <!-- ms-xsl erweiterung-->
      <xsl:when test="function-available('msxsl-extension:node-set')">msxsl-extension:node-set</xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">Could not find suitable function for node conversion. Terminating now.</xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <!--   OBSOLETER KRAM:
  
   hilfstemplate zum herausloesen des attributnamens eines durch xpath gematchten attributs. gibt null zurueck, falls kein attribut, sondern ein anderer knotentyp gematcht wurde

 -->
  <xsl:template name="ResolveAttributeName">
    <xsl:param name="wholestring"/>
    <!-- falls noch ein @ im string enthalten ist, dann ist der string noch nicht weit genug reduziert worden.-->
    <xsl:if test="contains($wholestring,'@')">
      <xsl:call-template name="ResolveAttributeName">
        <xsl:with-param name="wholestring">
          <xsl:value-of select="substring-after($wholestring,'@')"/>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
    <!-- am ende der rekursion den string ausgeben -->
    <xsl:if test="not(contains($wholestring,'@'))">
      <xsl:value-of select="$wholestring"/>
    </xsl:if>
  </xsl:template>
  <xsl:template name="CopyParameters">
    <xsl:param name="exclude"/>
    <xsl:for-each select="@*">
      <xsl:if test="$exclude!=name()">
        <xsl:copy/>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
  <!-- xupdate:insert-after falsch, aber gut zum einsteigen.-->
  <xsl:template match="//xupdate:insert-after-simple-but-wrong | //xupdate:insert-before-simple-but-wrong">
    <xsl:element name="xsl:template">
      <xsl:attribute name="match"><xsl:value-of select="@select"/></xsl:attribute>
      <xsl:if test="name()='xupdate:insert-before'">
        <xsl:apply-templates/>
      </xsl:if>
      <!-- ab hier wird das Kontextelement kopiert-->
      <xsl:element name="xsl:copy">
        <!-- matchen der im element enthaltenen Attribute und der restlichen darin enthaltenen Knoten-->
        <xsl:element name="xsl:apply-templates">
          <xsl:attribute name="select">attribute::*|child::node()</xsl:attribute>
        </xsl:element>
      </xsl:element>
      <!-- einf�gen der neuen Knoten -->
      <xsl:if test="name()='xupdate:insert-after'">
        <xsl:apply-templates/>
      </xsl:if>
    </xsl:element>
  </xsl:template>
  <!-- ende des hilfstemplates -->
</xsl:stylesheet>
