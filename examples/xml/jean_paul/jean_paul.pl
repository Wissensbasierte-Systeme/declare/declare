
html:_:Es ---> comment:_:Es.
head:_:_ ---> '' :-
   assert(commentHead).

body:_:Es_1 ---> comment:_:Es_2 :-
   jean_paul_comment(comment:_:Es_2, Es_1, []).
body:_:Es ---> Es.

em:_:Es ---> commentHead:Es :-
   retract(commentHead).
em:_:Es ---> page:[]:Es.

p:_:[''] ---> notep:[]:[].
p:_:[font:_:Es] ---> notep:[]:Es.
p:_:Es ---> notep:[]:Es.

font:As:Es ---> lat:Es :-
   'small-caps' := As^'font-variant'.
font:_:[X] ---> X.
font:_:[] ---> ''.

strong:_:Es ---> lemma:Es.
u:_:Es ---> spaced:Es.

T:As:Es ---> T:As:Es.


jean_paul_comment(comment:Es) -->
   jean_paul_comment_header(H),
   jean_paul_comment_block('Überlieferung', X),
   jean_paul_comment_block('Erläuterungen', Y),
   { append(H, [X, Y], Es) }.

jean_paul_comment_header([notep:[]:[commentHead:Es_1]|Es_2]) -->
   sequence_of_notep([notep:[]:[page:_:Es_1]|Es_2]).

jean_paul_comment_block(Type, ednote:[type:Type]:Es) -->
   sequence_of_notep([notep:_:[Type]|Es]).

sequence_of_notep([notep:As:Es]) -->
   [notep:As:Es].
sequence_of_notep([notep:As:Es|Xs]) -->
   [notep:As:Es],
   sequence_of_notep(Xs).


