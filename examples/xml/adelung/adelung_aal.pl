X ---> 'DEFS':Defs :-
   findall( Def,
      ( Def_2 := X^'TEXT'^'BODY'^'DIV0'^'ENTRY'^'DEF',
        process_def(Def_2, Def) ),
      Defs ).

process_def(Def_1, Def_2) :-
   Ns := Def_1^content::'*',
   length(Ns, L),
   writeln(user, L),
   maplist( modify_part,
      Ns, Ms ),
   append(Ms, Ks),
   write(user, Ks),
   !,
   items(Items, Ks, []),
   Def = 'DEF':Items,
   !,
   fn_triple_condense(Def, Def_2),
   nl(user).

modify_part(Name, Names) :-
   atomic(Name),
   !,
   writeln(user, Name),
   name_exchange_sublist([[".", ". "]], Name, Name_2),
   name_exchange_sublist([["  ", " "]], Name_2, Name_3),
   name_split_at_position([" "], Name_3, Names_3),
   list_exchange_elements(
      [['Anm.', remark:['Anm.']]], Names_3, Names).
modify_part(Xml, [Xml]).

items([]) -->
   { true }.
items([I|Is]) -->
   item(I),
   items(Is).

item(I) -->
   xml_element(I).
item(I) -->
   token(I).
item(I) -->
   list_item(I).

list_item(item:[number:N]:Ts) -->
   [S],
   { list_item_start(S, N) },
   tokens(Ts),
   { ! }.

xml_element(E) -->
   [E],
   { _ := E^'REF',
     wait }.

tokens([T|Ts]) -->
   token(T),
   { \+ ( remark := T^tag::'*' ) },
   tokens(Ts).
tokens([]) -->
   { true }.

token(T) -->
   [T],
   { write(user, '.'), ttyflush,
     \+ list_item_start(T, _) }.

list_item_start(X, Y) :-
   atomic(X),
   name(X, Zs),
   append(Ys, ")", Zs),
   name(Y, Ys),
   number(Y).

fn_triple_condense(Xml_1, Xml_2) :-
   fn_item_parse(Xml_1, T:As:Es_1),
   !,
   maplist( fn_triple_condense,
      Es_1, Es ),
   fn_text_merge('', Es, Es_2),
   Xml_2 = T:As:Es_2.
fn_triple_condense(Xml, Xml).

fn_text_merge(Sofar_1, [X|Xs], Ys) :-
   atomic(X),
   !,
   concat([Sofar_1, ' ', X], Sofar_2),
   fn_text_merge(Sofar_2, Xs, Ys).
fn_text_merge('', [X|Xs], [X|Ys]) :-
   !,
   fn_text_merge('', Xs, Ys).
fn_text_merge(Sofar, [X|Xs], [Sofar, X|Ys]) :-
   !,
   fn_text_merge('', Xs, Ys).
fn_text_merge(Sofar, [], [Sofar]).








