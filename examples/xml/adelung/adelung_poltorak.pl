
X ---> entry:[]:[Ys2] :-
   retract_all(user:elements_found(_)),
   Ys := X^'TEXT'^'BODY'^'DIV0'^content::'*',
   length(Ys, N), write_list(user, [N, ' elements\n']),
   maplist( adapt,
      Ys, Ys2 ),
   star_line,
   findall( E,
      retract(user:elements_found(E)),
      Es ),
   length(Es, M),
   write_list(user, [M, ' lists to flatten\n']),
   append_2_better(Es, E),
   list_to_multiset(E, E2),
   sort_multiset(E2, E3),
   writeln(user, E3).

