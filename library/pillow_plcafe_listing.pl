

:- op(150,xfx,'$').
:- op(150,fx,'$').

'$append'(A, B, C) :-
   append(A, B, C).

atom_codes(Name, List) :-
   name(Name, List).

atom_concat(A, B, C) :-
   name(A, X),
   name(B, Y),
   append(X, Y, Z),
   name(C, Z).


read_to_close(A, B) :-
	get_code(A, C),
	read_to_close1(C, A, B).


http_date(date(A, B, C, D, E), F, G) :-
	(   http_internet_date(A, B, C, D, E, F, G)
	;   http_asctime_date(A, B, C, D, E, F, G)
	).


html_option($A, B, C) :- !,
	'$append'([60, 111, 112, 116, 105, 111, 110, 32, 115, 101, 108, 101, 99, 116, 101, 100, 62], D, B),
	atomic_or_string(A, D, E),
	'$append'([60, 47, 111, 112, 116, 105, 111, 110, 62], C, E).
html_option(A, B, C) :-
	'$append'([60, 111, 112, 116, 105, 111, 110, 62], D, B),
	atomic_or_string(A, D, E),
	'$append'([60, 47, 111, 112, 116, 105, 111, 110, 62], C, E).


write_string([]).
write_string([A|B]) :-
	put_code(A),
	write_string(B).


preformatted_lines([], A, A).
preformatted_lines([A|B], C, D) :-
	html_term(A, C, E),
	newline(E, F),
	preformatted_lines(B, F, D).


http_req([], A, B) :-
	http_crlf(A, B).
http_req([A|B], C, D) :-
	http_request_option(A, C, E), !,
	http_req(B, E, D).


to_value_([], [], '$empty') :- !.
to_value_([], A, B) :- !,
	name(B, A).
to_value_(A, B, [B|A]).


xml_quoted_string(A, [], B, C) :-
	'C'(B, A, C), !.
xml_quoted_string(A, [38, 113, 117, 111, 116, 59|B], C, D) :-
	'C'(C, 34, E),
	xml_quoted_string(A, B, E, D).
xml_quoted_string(A, [B|C], D, E) :-
	'C'(D, B, F),
	xml_quoted_string(A, C, F, E).


http_token_symb(33, [33|A], A).
http_token_symb(35, [35|A], A).
http_token_symb(36, [36|A], A).
http_token_symb(37, [37|A], A).
http_token_symb(38, [38|A], A).
http_token_symb(39, [39|A], A).
http_token_symb(42, [42|A], A).
http_token_symb(43, [43|A], A).
http_token_symb(45, [45|A], A).
http_token_symb(46, [46|A], A).
http_token_symb(94, [94|A], A).
http_token_symb(95, [95|A], A).
http_token_symb(96, [96|A], A).
http_token_symb(124, [124|A], A).
http_token_symb(126, [126|A], A).


http_day(A, B, C) :-
	'$append'([D, E], F, B),
	number_codes(A, [D, E]),
	C=F.
http_day(A, B, C) :-
	'$append'([48, D], E, B),
	number_codes(A, [D]),
	C=E.
http_day(A, B, C) :-
	http_sp(B, D),
	'C'(D, E, F),
	number_codes(A, [E]),
	C=F.


html_tag_rest([A|B], C, D) :-
	html_tag_char(A, C, E), !,
	html_tag_rest(B, E, D).
html_tag_rest([], A, A).


http_last_modified(last_modified(A), B, C) :-
	http_field([108, 97, 115, 116, 45, 109, 111, 100, 105, 102, 105, 101, 100], B, D),
	http_date(A, D, E),
	http_crlf(E, C).


to_value([A|B], C) :-
	to_value_(B, A, C).


parse_integer(A, B, C) :-
	digit(D, B, E),
	parse_integer_rest(F, E, G),
	number_codes(A, [D|F]),
	C=G.


http_transaction(A, B, C, D, E) :-
	tcp_socket(F),
	tcp_connect(F, A:B),
	tcp_open_socket(F, G, H),
	write_string(H, C),
	flush_output(H),
	wait_for_input([H], I, D),
	I\==[],
	read_to_close(G, E),
	close(H),
	close(G).


html_tag_atts([], A, B, B).
html_tag_atts([A|B], C, D, E) :-
	whitespace(D, F),
	html_tag_att(A, C, F, G),
	html_tag_atts(B, C, G, E).


http_type_param(A=B, C, D) :-
	http_lo_up_token(A, C, E),
	'C'(E, 61, F),
	http_token_or_quoted(B, F, D).


display_list([A|B]) :- !,
	display(A),
	display_list(B).
display_list([]) :- !.
display_list(A) :-
	display(A).


html_env_atts(A, B, C, D, E) :-
	'C'(D, 60, F),
	string(A, F, G),
	html_atts(B, G, H),
	'C'(H, 62, I),
	html_term(C, I, J),
	'$append'([60, 47], K, J),
	string(A, K, L),
	'C'(L, 62, E).


http_credential_param(A=B, C, D) :-
	atom_codes(A, E),
	F=C,
	string(E, F, G),
	'$append'([61, 34], H, G),
	string(B, H, I),
	'C'(I, 34, D).


html_atts([], A, A).
html_atts([A|B], C, D) :-
	'C'(C, 32, E),
	html_att(A, E, F),
	html_atts(B, F, D).


http_type_params([A|B], C, D) :-
	'C'(C, 59, E),
	http_lws0(E, F),
	http_type_param(A, F, G),
	http_lws0(G, H),
	http_type_params(B, H, D).
http_type_params([], A, A).


html_lax_value([A|B], C, D) :-
	'C'(C, A, E),
	A\==62,
	A>32,
	F=E,
	html_lax_value(B, F, D).
html_lax_value([], A, A).


extract_name(A, B) :-
	member(name=C, A), !,
	atom_codes(B, C).


html_value(A, B, C, D) :-
	'C'(C, 95, E),
	html_tag(F, E, G),
	list_lookup(B, =, F, A),
	D=G.
html_value(A, B, C, D) :-
	http_quoted_string(A, C, D).
html_value(A, B, C, D) :-
	html_lax_value(A, C, D).


parse_xml(A, B, C, D, E) :-
	'C'(D, 60, F),
	tidy_string(A, G),
	H=F,
	xml_unit(G, I, C, H, J), !,
	parse_xml(I, B, C, J, E).
parse_xml([A|B], C, D, E, F) :-
	nonvar(A),
	A=string(G, H),
	I=E,
	'C'(I, J, K), !,
	H=[J|L],
	M=K,
	parse_xml([string(G, L)|B], C, D, M, F).
parse_xml(A, B, C, D, E) :-
	'C'(D, F, G), !,
	parse_xml([string([F|H], H)|A], B, C, G, E).
parse_xml(A, B, C, D, E) :-
	D=F,
	tidy_string(A, G),
	reverse(G, B),
	E=F.


loalpha(A, B, C) :-
	'C'(B, A, D),
	A>=97,
	A=<122,
	C=D.


http_more_challenges([A|B], C, D) :-
	http_commas(C, E),
	http_challenge(A, E, F),
	http_more_challenges(B, F, D).
http_more_challenges([], A, B) :-
	http_lws0(A, C),
	http_crlf(C, B).

%   Foreign: tcp_accept/3


html_nice_items([], A, B, B).
html_nice_items([A|B], C, D, E) :-
	'$append'([60, 100, 100, 62, 60, 105, 109, 103, 32, 115, 114, 99, 61, 34], F, D),
	string(C, F, G),
	'$append'([34, 32, 97, 108, 105, 103, 110, 61, 34, 98, 111, 116, 116, 111, 109, 34, 32, 97, 108, 116, 61, 34, 42, 34, 62], H, G),
	html_term(A, H, I),
	'$append'([60, 47, 100, 100, 62], J, I),
	newline(J, K),
	html_nice_items(B, C, K, E).


html_term(A, B, C) :-
	var(A),
	D=B, !,
	'$append'([60, 98, 62, 42, 42, 87, 97, 114, 110, 105, 110, 103, 32, 102, 114, 101, 101, 32, 118, 97, 114, 105, 97, 98, 108, 101, 42, 42, 60, 47, 98, 62], C, D).
html_term(A, B, C) :-
	html_expansion(A, D),
	E=B, !,
	html_term(D, E, C).
html_term(start, A, B) :- !,
	'$append'([60, 104, 116, 109, 108, 62], B, A).
html_term(end, A, B) :- !,
	'$append'([60, 47, 104, 116, 109, 108, 62], B, A).
html_term(--, A, B) :- !,
	newline(A, C),
	'$append'([60, 104, 114, 62], D, C),
	newline(D, B).
html_term(\\, A, B) :- !,
	'$append'([60, 98, 114, 62], C, A),
	newline(C, B).
html_term($, A, B) :- !,
	newline(A, C),
	'$append'([60, 112, 62], B, C).
html_term(comment(A), B, C) :- !,
	'$append'([60, 33, 45, 45, 32], D, B),
	atomic_or_string(A, D, E),
	'$append'([32, 45, 45, 62], F, E),
	newline(F, C).
html_term(declare(A), B, C) :- !,
	'$append'([60, 33], D, B),
	atomic_or_string(A, D, E),
	'C'(E, 62, F),
	newline(F, C).
html_term(xmldecl(A), B, C) :- !,
	'$append'([60, 63, 120, 109, 108], D, B),
	html_atts(A, D, E),
	'$append'([63, 62], C, E).
html_term(image(A), B, C) :- !,
	'$append'([60, 105, 109, 103], D, B),
	html_atts([src=A], D, E),
	'C'(E, 62, C).
html_term(image(A, B), C, D) :- !,
	'$append'([60, 105, 109, 103], E, C),
	html_atts([src=A|B], E, F),
	'C'(F, 62, D).
html_term(ref(A, B), C, D) :- !,
	'$append'([60, 97], E, C),
	html_atts([href=A], E, F),
	'C'(F, 62, G),
	html_term(B, G, H),
	'$append'([60, 47, 97, 62], D, H).
html_term(label(A, B), C, D) :- !,
	'$append'([60, 97], E, C),
	html_atts([name=A], E, F),
	'C'(F, 62, G),
	html_term(B, G, H),
	'$append'([60, 47, 97, 62], D, H).
html_term(heading(A, B), C, D) :-
	number_codes(A, [E]),
	F=C, !,
	html_env([104, E], B, F, G),
	newline(G, D).
html_term(itemize(A), B, C) :- !,
	'$append'([60, 117, 108, 62], D, B),
	newline(D, E),
	html_items(A, E, F),
	'$append'([60, 47, 117, 108, 62], C, F).
html_term(enumerate(A), B, C) :- !,
	'$append'([60, 111, 108, 62], D, B),
	newline(D, E),
	html_items(A, E, F),
	'$append'([60, 47, 111, 108, 62], C, F).
html_term(description(A), B, C) :- !,
	'$append'([60, 100, 108, 62], D, B),
	newline(D, E),
	html_descriptions(A, E, F),
	'$append'([60, 47, 100, 108, 62], C, F).
html_term(nice_itemize(A, B), C, D) :- !,
	'$append'([60, 100, 108, 62], E, C),
	newline(E, F),
	(   atom(A)
	->  atom_codes(A, G)
	;   G=A
	),
	H=F,
	html_nice_items(B, G, H, I),
	'$append'([60, 47, 100, 108, 62], D, I).
html_term(preformatted(A), B, C) :- !,
	'$append'([60, 112, 114, 101, 62], D, B),
	newline(D, E),
	preformatted_lines(A, E, F),
	'$append'([60, 47, 112, 114, 101, 62], C, F).
html_term(entity(A), B, C) :- !,
	'C'(B, 38, D),
	atomic_or_string(A, D, E),
	'C'(E, 59, C).
html_term(start_form, A, B) :- !,
	'$append'([60, 102, 111, 114, 109], C, A),
	html_atts([method=[80, 79, 83, 84]], C, D),
	'C'(D, 62, E),
	newline(E, B).
html_term(start_form(A), B, C) :- !,
	'$append'([60, 102, 111, 114, 109], D, B),
	html_atts([method=[80, 79, 83, 84], action=A], D, E),
	'C'(E, 62, F),
	newline(F, C).
html_term(start_form(A, B), C, D) :- !,
	'$append'([60, 102, 111, 114, 109], E, C),
	html_atts([action=A|B], E, F),
	'C'(F, 62, G),
	newline(G, D).
html_term(end_form, A, B) :- !,
	'$append'([60, 47, 102, 111, 114, 109, 62], C, A),
	newline(C, B).
html_term(checkbox(A, on), B, C) :- !,
	'$append'([60, 105, 110, 112, 117, 116], D, B),
	html_atts([name=A, type=checkbox, checked], D, E),
	'C'(E, 62, C).
html_term(checkbox(A, B), C, D) :- !,
	'$append'([60, 105, 110, 112, 117, 116], E, C),
	html_atts([name=A, type=checkbox], E, F),
	'C'(F, 62, D).
html_term(radio(A, B, B), C, D) :- !,
	'$append'([60, 105, 110, 112, 117, 116], E, C),
	html_atts([name=A, type=radio, value=B, checked], E, F),
	'C'(F, 62, D).
html_term(radio(A, B, C), D, E) :- !,
	'$append'([60, 105, 110, 112, 117, 116], F, D),
	html_atts([name=A, type=radio, value=B], F, G),
	'C'(G, 62, E).
html_term(input(A, B), C, D) :- !,
	'$append'([60, 105, 110, 112, 117, 116], E, C),
	html_atts([type=A|B], E, F),
	'C'(F, 62, D).
html_term(textinput(A, B, C), D, E) :- !,
	'$append'([60, 116, 101, 120, 116, 97, 114, 101, 97], F, D),
	html_atts([name=A|B], F, G),
	'C'(G, 62, H),
	textarea_data(C, H, I),
	'$append'([60, 47, 116, 101, 120, 116, 97, 114, 101, 97, 62], E, I).
html_term(menu(A, B, C), D, E) :- !,
	'$append'([60, 115, 101, 108, 101, 99, 116], F, D),
	html_atts([name=A|B], F, G),
	'C'(G, 62, H),
	newline(H, I),
	html_options(C, I, J),
	'$append'([60, 47, 115, 101, 108, 101, 99, 116, 62], E, J).
html_term(option(A, B, C), D, E) :- !,
	'$append'([60, 115, 101, 108, 101, 99, 116], F, D),
	html_atts([name=A], F, G),
	'C'(G, 62, H),
	newline(H, I),
	html_one_option(C, B, I, J),
	'$append'([60, 47, 115, 101, 108, 101, 99, 116, 62], E, J).
html_term(form_reply, A, B) :- !,
	'$append'([67, 111, 110, 116, 101, 110, 116, 45, 116, 121, 112, 101, 58, 32, 116, 101, 120, 116, 47, 104, 116, 109, 108], C, A),
	newline(C, D),
	newline(D, B).
html_term(cgi_reply, A, B) :- !,
	'$append'([67, 111, 110, 116, 101, 110, 116, 45, 116, 121, 112, 101, 58, 32, 116, 101, 120, 116, 47, 104, 116, 109, 108], C, A),
	newline(C, D),
	newline(D, B).
html_term(prolog_term(A), B, C) :- !,
	prolog_term(A, B, C).
html_term(verbatim(A), B, C) :- !,
	html_quoted(A, B, C).
html_term(nl, A, B) :- !,
	newline(A, B).
html_term([], A, A) :- !.
html_term([A|B], C, D) :- !,
	html_term(A, C, E),
	html_term(B, E, D).
html_term(begin(A), B, C) :-
	atom(A),
	atom_codes(A, D),
	E=B, !,
	'C'(E, 60, F),
	string(D, F, G),
	'C'(G, 62, C).
html_term(begin(A, B), C, D) :-
	atom(A),
	atom_codes(A, E),
	F=C, !,
	'C'(F, 60, G),
	string(E, G, H),
	html_atts(B, H, I),
	'C'(I, 62, D).
html_term(end(A), B, C) :-
	atom(A),
	atom_codes(A, D),
	E=B, !,
	'$append'([60, 47], F, E),
	string(D, F, G),
	'C'(G, 62, C).
html_term(env(A, B, C), D, E) :-
	atom(A),
	atom_codes(A, F),
	G=D, !,
	html_env_atts(F, B, C, G, E).
html_term(A$B, C, D) :-
	atom(A),
	atom_codes(A, E),
	F=C, !,
	'C'(F, 60, G),
	string(E, G, H),
	html_atts(B, H, I),
	'C'(I, 62, D).
html_term(elem(A, B), C, D) :-
	atom(A),
	atom_codes(A, E),
	F=C, !,
	'C'(F, 60, G),
	string(E, G, H),
	html_atts(B, H, I),
	'$append'([47, 62], D, I).
html_term(A, B, C) :-
	A=..[D, E],
	atom_codes(D, F),
	G=B, !,
	html_env(F, E, G, C).
html_term(A, B, C) :-
	A=..[D, E, F],
	atom_codes(D, G),
	H=B, !,
	html_env_atts(G, E, F, H, C).
html_term(A, B, C) :-
	integer(A),
	A>=0,
	A=<255,
	D=B, !,
	'C'(D, A, C).
html_term(A, B, C) :-
	prolog_term(A, B, C).


legal_cookie_char(A, B, C) :-
	'C'(B, A, D),
	A\==59,
	A\==61,
	C=D.

%   Foreign: tcp_debug/1


http_month('January', [74, 97, 110|A], A).
http_month('February', [70, 101, 98|A], A).
http_month('March', [77, 97, 114|A], A).
http_month('April', [65, 112, 114|A], A).
http_month('May', [77, 97, 121|A], A).
http_month('June', [74, 117, 110|A], A).
http_month('July', [74, 117, 108|A], A).
http_month('August', [65, 117, 103|A], A).
http_month('September', [83, 101, 112|A], A).
http_month('October', [79, 99, 116|A], A).
http_month('November', [78, 111, 118|A], A).
http_month('December', [68, 101, 99|A], A).


http_status_line(status(A, B, C), D, E) :-
	'$append'([72, 84, 84, 80, 47], F, D),
	parse_integer(G, F, H),
	'C'(H, 46, I),
	parse_integer(J, I, K),
	http_sp(K, L),
	http_status_code(A, B, L, M),
	http_sp(M, N),
	http_line(C, N, E), !.


http_asctime_date(A, B, C, D, E, F, G) :-
	http_weekday(A, F, H),
	http_sp(H, I),
	http_month(C, I, J),
	http_sp(J, K),
	http_day(B, K, L),
	http_sp(L, M),
	http_time(E, M, N),
	http_sp(N, O),
	http_year(D, O, G).


html_tag(A, B, C) :-
	loupalpha(D, B, E),
	html_tag_rest(F, E, G),
	atom_codes(A, [D|F]),
	C=G.

/*
:- module_transparent (meta_predicate)/1.

meta_predicate A, B :- !,
	meta_predicate A,
	meta_predicate B.
meta_predicate A :-
	'$strip_module'(A, B, C),
	meta_predicate(B, C).
*/


chars_to([], A, [A|B], B).
chars_to([A|B], C, D, E) :-
	'C'(D, A, F),
	A\==C,
	G=F,
	chars_to(B, C, G, E).


http_content_encoding(content_encoding(A), B, C) :-
	http_field([99, 111, 110, 116, 101, 110, 116, 45, 101, 110, 99, 111, 100, 105, 110, 103], B, D),
	http_lo_up_token(A, D, E),
	http_lws0(E, F),
	http_crlf(F, C).


pop_ts(A, [B|C], D, E, F) :-
	(   nonvar(B),
	    B=A$G
	->  elem_or_template_var(A, G, D, H, F),
	    E=[H|C]
	;   pop_ts(A, C, [B|D], E, F)
	).


html_one_option([], A, B, B).
html_one_option([A|B], C, D, E) :-
	'$append'([60, 111, 112, 116, 105, 111, 110], F, D),
	html_one_option_sel(A, C, F, G),
	'C'(G, 62, H),
	atomic_or_string(A, H, I),
	'$append'([60, 47, 111, 112, 116, 105, 111, 110, 62], J, I),
	newline(J, K),
	html_one_option(B, C, K, E).


http_lws0(A, A).
http_lws0(A, B) :-
	http_lws(A, B).


get_form_value([], A, '').
get_form_value([A=B|C], A, B) :- !.
get_form_value([A|B], C, D) :-
	get_form_value(B, C, D).


get_form_input_of_type(application, 'x-www-form-urlencoded', A, B) :-
	getenvstr('CONTENT_LENGTH', C),
	number_codes(D, C),
	(   D>0
	->  read_all(D, E, [38]),
	    form_urlencoded_to_dic(B, E, [])
	;   B=[]
	).
get_form_input_of_type(multipart, 'form-data', A, B) :-
	member(boundary=C, A),
	name(C, D),
	E=[45, 45|D],
	get_lines_to_boundary(E, F, G),
	get_multipart_form_data(G, E, B).
get_form_input_of_type(A, B, C, D) :-
	html_report_error(['Unknown Content-type ', tt([A, [47], B]), ' or bad request.']).


http_token(A, B, C) :-
	http_token_char(D, B, E),
	http_token_rest(F, E, G),
	atom_codes(A, [D|F]),
	C=G.


html_unit(A, B, C, D, E) :-
	'C'(D, 47, F),
	html_tag(G, F, H),
	whitespace0(H, I),
	'C'(I, 62, J),
	poptokenstack(G, A, B, C),
	E=J, !.
html_unit(A, [comment(B)|A], C, D, E) :-
	'$append'([33, 45, 45], F, D),
	string(B, F, G),
	'$append'([45, 45, 62], E, G), !.
html_unit(A, [declare(B)|A], C, D, E) :-
	'C'(D, 33, F),
	string(B, F, G),
	'C'(G, 62, E), !.
html_unit(A, [B$C|A], D, E, F) :-
	html_tag(B, E, G),
	html_tag_atts(C, D, G, H),
	whitespace0(H, I),
	'C'(I, 62, F), !.


http_location(location(A), B, C) :-
	http_field([108, 111, 99, 97, 116, 105, 111, 110], B, D),
	http_line(E, D, F),
	atom_codes(A, E),
	C=F.


textarea_data('$empty', A, A).
textarea_data(A, B, C) :-
	atomic(A),
	name(A, D),
	E=B, !,
	string(D, E, C).
textarea_data(A, B, C) :-
	http_lines(A, B, C), !.
textarea_data(A, B, C) :-
	string(A, B, C).


http_token_list([A|B], C, D) :-
	http_maybe_commas(C, E),
	http_token(A, E, F),
	http_token_list0(B, F, D).


extract_type(A, B) :-
	(   member(filename=C, A)
	->  atom_codes(D, C),
	    B=file(D)
	;   B=data
	).


extract_name_type(A, B, C) :-
	member(D, A),
	content_disposition_header(E, D, []),
	extract_name(E, B),
	extract_type(E, C).

%   Foreign: tcp_host_to_address/2


http_internet_date(A, B, C, D, E, F, G) :-
	http_weekday(A, F, H),
	'C'(H, 44, I),
	http_sp(I, J),
	http_day(B, J, K),
	(   http_sp(K, L)
	;   'C'(K, 45, L)
	),
	http_month(C, L, M),
	(   http_sp(M, N)
	;   'C'(M, 45, N)
	),
	http_year(D, N, O),
	http_sp(O, P),
	http_time(E, P, Q),
	http_sp(Q, R),
	'$append'([71, 77, 84], G, R).


http_maybe_commas(A, A).
http_maybe_commas(A, B) :-
	'C'(A, 44, C),
	http_lws0(C, D),
	http_maybe_commas(D, B).


http_response_headers([A|B], C, D, E) :-
	http_response_header(A, D, F), !,
	http_response_headers(B, C, F, E).
http_response_headers(A, A, B, B).


http_request_option(user_agent(A), B, C) :- !,
	atom_codes(A, D),
	pillow_version(E),
	F=B,
	'$append'([85, 115, 101, 114, 45, 65, 103, 101, 110, 116, 58, 32], G, F),
	string(D, G, H),
	'$append'([32, 80, 105, 76, 76, 111, 87, 47], I, H),
	string(E, I, J),
	http_crlf(J, C).
http_request_option(if_modified_since(date(A, B, C, D, E)), F, G) :- !,
	'$append'([73, 102, 45, 77, 111, 100, 105, 102, 105, 101, 100, 45, 83, 105, 110, 99, 101, 58, 32], H, F),
	http_internet_date(A, B, C, D, E, H, I),
	http_crlf(I, G).
http_request_option(authorization(A, B), C, D) :- !,
	'$append'([65, 117, 116, 104, 111, 114, 105, 122, 97, 116, 105, 111, 110, 58, 32], E, C),
	http_credentials(A, B, E, F),
	http_crlf(F, D).
http_request_option(A, B, C) :-
	functor(A, D, 1),
	atom_codes(D, E),
	arg(1, A, F),
	atom_codes(F, G),
	H=B, !,
	string(E, H, I),
	'$append'([58, 32], J, I),
	string(G, J, K),
	http_crlf(K, C).
http_request_option(A, B, C) :-
	B=D,
	warning(['Invalid http_request_param ', A]),
	C=D.


whitespace(A, B) :-
	whitespace_char(A, C),
	whitespace0(C, B).


write_string(A, B) :-
	current_output(C),
	set_output(A),
	write_string(B),
	set_output(C).


html_description((A, B), C, D) :- !,
	'$append'([60, 100, 116, 62], E, C),
	html_term(A, E, F),
	'$append'([60, 47, 100, 116, 62], G, F),
	newline(G, H),
	html_description(B, H, D).
html_description(A, B, C) :-
	'$append'([60, 100, 100, 62], D, B),
	html_term(A, D, E),
	'$append'([60, 47, 100, 100, 62], F, E),
	newline(F, C).


http_request_method(A, B, C, D) :-
	select(head, A, B),
	E=C, !,
	'$append'([72, 69, 65, 68], D, E).
http_request_method(A, A, [71, 69, 84|B], B).


http_qs_text([], A, B) :-
	'C'(A, 34, B), !.
http_qs_text([A|B], C, D) :-
	'C'(C, A, E),
	http_qs_text(B, E, D).

%   Foreign: tcp_bind/2


http_year(A, B, C) :-
	'$append'([D, E, F, G], H, B),
	number_codes(A, [D, E, F, G]),
	C=H.
http_year(A, B, C) :-
	'$append'([D, E], F, B),
	number_codes(G, [D, E]),
	(   G>=70
	->  A is 1900+G
	;   A is 2000+G
	),
	C=F.


html_tag_char(A, B, C) :-
	loupalpha(A, B, C).
html_tag_char(A, B, C) :-
	digit(A, B, C).
html_tag_char(46, [46|A], A).
html_tag_char(45, [45|A], A).


rest(A, A, []).

%   Foreign: tcp_socket/1


http_expires(expires(A), B, C) :-
	http_field([101, 120, 112, 105, 114, 101, 115], B, D),
	http_date(A, D, E),
	http_crlf(E, C).


elem_envbeg(elem(A, B), A, B, C, D) :-
	'C'(C, 47, D), !.
elem_envbeg(A$B, A, B, C, C).


html_items([], A, A).
html_items([A|B], C, D) :-
	'$append'([60, 108, 105, 62], E, C),
	html_term(A, E, F),
	'$append'([60, 47, 108, 105, 62], G, F),
	newline(G, H),
	html_items(B, H, D).


http_token_or_quoted(A, B, C) :-
	http_token(A, B, C).
http_token_or_quoted(A, B, C) :-
	http_quoted_string(A, B, C).


icon_img(warning, 'warning_large.gif').
icon_img(dot, 'redball.gif').
icon_img(clip, 'clip.gif').
icon_img(pillow, 'pillow_d.gif').


get_multipart_form_data(end, A, []).
get_multipart_form_data(continue, A, [B=C|D]) :-
	get_lines_to_boundary(A, E, F),
	extract_name_value(E, B, C),
	get_multipart_form_data(F, A, D).


text_lines('$empty', []) :- !.
text_lines(A, [B]) :-
	atomic(A), !,
	name(A, B).
text_lines(A, A).


http_media_type(A, B, C, D, E) :-
	http_lo_up_token(A, D, F),
	'C'(F, 47, G),
	http_lo_up_token(B, G, H),
	http_lws0(H, I),
	http_type_params(C, I, E).


elem_or_template_var(v, A, [B], C, D) :-
	catch(atom_codes(E, B), F, fail),
	list_lookup(D, =, E, C), !.
elem_or_template_var(A, B, C, env(A, B, C), D).


http_token_list0([A|B], C, D) :-
	http_commas(C, E),
	http_token(A, E, F),
	http_token_list0(B, F, D).
http_token_list0([], A, B) :-
	http_maybe_commas(A, B).


http_status_code(A, B, C, D) :-
	'$append'([E, F, G], H, C),
	type_of_status_code(E, A), !,
	number_codes(B, [E, F, G]),
	D=H.


html_one_option_sel(A, A, B, C) :- !,
	'$append'([32, 115, 101, 108, 101, 99, 116, 101, 100], C, B).
html_one_option_sel(A, B, C, C).


html_quoted_char(62, A, B) :- !,
	'$append'([38, 103, 116, 59], B, A).
html_quoted_char(60, A, B) :- !,
	'$append'([38, 108, 116, 59], B, A).
html_quoted_char(38, A, B) :- !,
	'$append'([38, 97, 109, 112, 59], B, A).
html_quoted_char(34, A, B) :- !,
	'$append'([38, 113, 117, 111, 116, 59], B, A).
html_quoted_char(32, A, B) :- !,
	'$append'([38, 110, 98, 115, 112, 59], B, A).
html_quoted_char(A, [A|B], B).


info_to_url(http(A, B, C), D) :- !,
	atom(A),
	integer(B),
	atom_codes(A, E),
	port_codes(B, F),
	mappend([[104, 116, 116, 112, 58, 47, 47], E, F, C], D).


html_opt_value(A=B, A, C, D, E) :-
	whitespace0(D, F),
	'C'(F, 61, G),
	whitespace0(G, H),
	html_value(B, C, H, E), !.
html_opt_value(A, A, B, C, C).


upalpha(A, B, C) :-
	'C'(B, A, D),
	A>=65,
	A=<90,
	C=D.


http_sp0(A, A).
http_sp0(A, B) :-
	http_sp(A, B).

%   Foreign: tcp_fcntl/3


http_field([A|B], C, D) :-
	http_lo_up_token_char(A, C, E),
	http_lo_up_token_rest(B, E, F),
	'C'(F, 58, G),
	http_lws(G, D).


html_report_error(A) :-
	(   icon_address(warning, B)
	->  C=image(B)
	;   C=[]
	),
	output_html([cgi_reply, start, title([69, 114, 114, 111, 114, 32, 82, 101, 112, 111, 114, 116]), --, h1([C, ' Error:']), --, A, --, end]),
	flush_output,
	halt.

%   Foreign: tcp_connect/2


http_time(A, B, C) :-
	'$append'([D, E, 58, F, G, 58, H, I], J, B),
	atom_codes(A, [D, E, 58, F, G, 58, H, I]),
	C=J.


cookie_str([A], B, C) :-
	legal_cookie_char(A, B, C).
cookie_str([A|B], C, D) :-
	legal_cookie_char(A, C, E),
	cookie_str(B, E, D).


html_tag_att(A, B, C, D) :-
	'C'(C, 95, E),
	html_tag(F, E, G),
	list_lookup(B, =, F, H),
	I=G,
	html_opt_value(A, H, B, I, D).
html_tag_att(A, B, C, D) :-
	html_tag(E, C, F),
	html_opt_value(A, E, B, F, D).


http_response(A, B, C) :-
	http_full_response(A, B, C), !.
http_response(A, B, C) :-
	http_simple_response(A, B, C).


html_env(A, B, C, D) :-
	'C'(C, 60, E),
	string(A, E, F),
	'C'(F, 62, G),
	html_term(B, G, H),
	'$append'([60, 47], I, H),
	string(A, I, J),
	'C'(J, 62, D).


icon_address(A, B) :-
	icon_base_address(C),
	icon_img(A, D),
	atom_concat(C, D, B).


my_url(A) :-
	getenvstr('SERVER_NAME', B),
	getenvstr('SCRIPT_NAME', C),
	getenvstr('SERVER_PORT', D),
	(   D=[56, 48]
	->  mappend([[104, 116, 116, 112, 58, 47, 47], B, C], A)
	;   mappend([[104, 116, 116, 112, 58, 47, 47], B, [58|D], C], A)
	).


http_lws(A, B) :-
	http_sp(A, B).
http_lws(A, B) :-
	http_crlf(A, C),
	http_sp(C, B).


http_credential_params_rest([], A, A).
http_credential_params_rest([A|B], C, D) :-
	'$append'([44, 32], E, C),
	http_credential_param(A, E, F),
	http_credential_params_rest(B, F, D).


http_allow(allow(A), B, C) :-
	http_field([97, 108, 108, 111, 119], B, D),
	http_token_list(A, D, E),
	http_crlf(E, C).


prolog_term(A, B, C) :-
	var(A),
	D=B, !,
	'C'(D, 95, C).
prolog_term(A, B, C) :-
	functor(A, D, E),
	name(D, F),
	G=B,
	string(F, G, H),
	prolog_term_maybe_args(E, A, H, C).


form_urlencoded_to_dic([], A, A).
form_urlencoded_to_dic([A=B|C], D, E) :-
	chars_to(F, 61, D, G),
	name(A, F),
	H=G,
	chars_to(I, 38, H, J),
	expand_esc_plus(I, K, [13, 10]),
	http_lines(L, K, []),
	to_value(L, B),
	M=J,
	form_urlencoded_to_dic(C, M, E).


http_message_date(message_date(A), B, C) :-
	http_field([100, 97, 116, 101], B, D),
	http_date(A, D, E),
	http_crlf(E, C).


html2terms(A, B) :-
	var(A), !,
	html_term(B, A, []).
html2terms(A, B) :-
	parse_html([], B, [], A, []).


type_of_status_code(49, informational).
type_of_status_code(50, success).
type_of_status_code(51, redirection).
type_of_status_code(52, request_error).
type_of_status_code(53, server_error).
type_of_status_code(A, extension_code).


hex_chars(A, B, C) :-
	D is A>>4,
	hex_char(D, B),
	E is A/\15,
	hex_char(E, C).


extract_name_value([A|B], C, D) :-
	head_and_body_lines(A, B, E, F),
	extract_name_type(E, C, G),
	extract_value(G, F, D).


xml_tag_att(A=B, C, D, E) :-
	xml_tag(A, D, F),
	whitespace0(F, G),
	'C'(G, 61, H),
	whitespace0(H, I),
	xml_value(B, C, I, E).


parse_integer_rest([A|B], C, D) :-
	digit(A, C, E),
	parse_integer_rest(B, E, D).
parse_integer_rest([], A, A).


no_conversion(42).
no_conversion(45).
no_conversion(46).
no_conversion(95).
no_conversion(A) :-
	A>=48,
	A=<57.
no_conversion(A) :-
	A>=64,
	A=<90.
no_conversion(A) :-
	A>=97,
	A=<122.

%   Foreign: tcp_listen/2


digit(A, B, C) :-
	'C'(B, A, D),
	A>=48,
	A=<57,
	C=D.


html_att(A=B, C, D) :-
	atom_codes(A, E),
	F=C, !,
	string(E, F, G),
	'$append'([61, 34], H, G),
	atomic_or_string(B, H, I),
	'C'(I, 34, D).
html_att(A, B, C) :-
	atom_codes(A, D),
	E=B,
	string(D, E, C).


url_query(A, B) :-
	params_to_string(A, 63, B).

%   Foreign: tcp_open_socket/3


url_info_relative(A, B, C) :-
	atom(A), !,
	atom_codes(A, D),
	url_info_relative(D, B, C).
url_info_relative(A, B, C) :-
	url_info(A, C), !.
url_info_relative(A, http(B, C, D), http(B, C, A)) :-
	A=[47|E], !.
url_info_relative(A, http(B, C, D), http(B, C, E)) :-
	\+member(58, A),
	append(F, G, D),
	\+member(47, G), !,
	append(F, A, E).


poptokenstack(A, B, C, D) :-
	pop_ts(A, B, [], C, D), !.
poptokenstack(A, B, [C$[]|B], D) :-
	atom_concat(/, A, C).


xml_tag_atts([], A, B, B).
xml_tag_atts([A|B], C, D, E) :-
	whitespace(D, F),
	xml_tag_att(A, C, F, G),
	xml_tag_atts(B, C, G, E).


http_content_type(content_type(A, B, C), D, E) :-
	http_field([99, 111, 110, 116, 101, 110, 116, 45, 116, 121, 112, 101], D, F),
	http_media_type(A, B, C, F, G),
	http_crlf(G, E).


pillow_version([49, 46, 49]).


icon_base_address('/images/').


empty_lines([]).
empty_lines([A|B]) :-
	whitespace0(A, []),
	empty_lines(B), !.


atomic_or_string(A, B, C) :-
	atomic(A),
	name(A, D),
	E=B, !,
	string(D, E, C).
atomic_or_string(A, B, C) :-
	string(A, B, C).


getenvstr(A, B) :-
	getenv(A, C),
	atom_codes(C, B).


list_lookup(A, B, C, D) :-
	var(A), !,
	functor(E, B, 2),
	arg(1, E, C),
	arg(2, E, D),
	A=[E|F].
list_lookup([A|B], C, D, E) :-
	functor(A, C, 2),
	arg(1, A, F),
	F==D, !,
	arg(2, A, E).
list_lookup([A|B], C, D, E) :-
	list_lookup(B, C, D, E).


head_and_body_lines([], A, [], A) :- !.
head_and_body_lines(A, [B|C], [A|D], E) :-
	head_and_body_lines(B, C, D, E).


xml_tag_char(A, B, C) :-
	loalpha(A, B, C).
xml_tag_char(A, B, C) :-
	upalpha(A, B, C).
xml_tag_char(A, B, C) :-
	digit(A, B, C).
xml_tag_char(95, [95|A], A).
xml_tag_char(58, [58|A], A).
xml_tag_char(46, [46|A], A).
xml_tag_char(45, [45|A], A).


http_auth_params([A|B], C, D) :-
	'C'(C, 44, E),
	http_lws0(E, F),
	http_auth_param(A, F, G),
	http_lws0(G, H),
	http_auth_params(B, H, D).
http_auth_params([], A, A).

%   Foreign: gethostname/1


http_challenges([A|B], C, D) :-
	http_maybe_commas(C, E),
	http_challenge(A, E, F),
	http_more_challenges(B, F, D).


html_quoted_chars([], A, A).
html_quoted_chars([A|B], C, D) :-
	html_quoted_char(A, C, E),
	html_quoted_chars(B, E, D).


url_to_info(A, http(B, C, D)) :-
	http_url(B, C, D, A, []), !.


http_url(A, B, C, D, E) :-
	'$append'([104, 116, 116, 112, 58, 47, 47], F, D),
	internet_host(A, F, G),
	optional_port(B, G, H),
	http_document(C, H, E).


whitespace_char([10|A], A).
whitespace_char([13|A], A).
whitespace_char([32|A], A).
whitespace_char([9|A], A).


prolog_term_maybe_args(0, A, B, C) :- !,
	B=C.
prolog_term_maybe_args(A, B, C, D) :-
	'C'(C, 40, E),
	prolog_term_args(1, A, B, E, F),
	'C'(F, 41, D).


read_to_close1(-1, A, []) :- !.
read_to_close1(A, B, [A|C]) :-
	get_code(B, D),
	read_to_close1(D, B, C).


http_sp(A, B) :-
	'C'(A, 32, C), !,
	http_sp0(C, B).
http_sp(A, B) :-
	'C'(A, 9, C),
	http_sp0(C, B).


http_crlf(A, B) :-
	'$append'([13, 10], B, A), !.
http_crlf([10|A], A).


get_line(A) :-
	get_code(B),
	get_line_after(B, C),
	A=C.


http_response_header(A, B, C) :-
	http_pragma(A, B, C).
http_response_header(A, B, C) :-
	http_message_date(A, B, C).
http_response_header(A, B, C) :-
	http_location(A, B, C).
http_response_header(A, B, C) :-
	http_server(A, B, C).
http_response_header(A, B, C) :-
	http_authenticate(A, B, C).
http_response_header(A, B, C) :-
	http_allow(A, B, C).
http_response_header(A, B, C) :-
	http_content_encoding(A, B, C).
http_response_header(A, B, C) :-
	http_content_length(A, B, C).
http_response_header(A, B, C) :-
	http_content_type(A, B, C).
http_response_header(A, B, C) :-
	http_expires(A, B, C).
http_response_header(A, B, C) :-
	http_last_modified(A, B, C).
http_response_header(A, B, C) :-
	http_extension_header(A, B, C).

/*
:- module_transparent html_protect/1.

html_protect(A) :-
	catch(A, B, html_report_error(B)).
html_protect(A) :-
	html_report_error('Sorry, application failed.').
*/

%   Foreign: tcp_close_socket/1

:- multifile html_expansion/2.

html_expansion(bf(A), b(A)).
html_expansion(it(A), i(A)).
html_expansion(pr, ref([104, 116, 116, 112, 58, 47, 47, 119, 119, 119, 46, 99, 108, 105, 112, 46, 100, 105, 97, 46, 102, 105, 46, 117, 112, 109, 46, 101, 115, 47, 83, 111, 102, 116, 119, 97, 114, 101, 47, 112, 105, 108, 108, 111, 119, 47, 112, 105, 108, 108, 111, 119, 46, 104, 116, 109, 108], image(A, [alt=[100, 101, 118, 101, 108, 111, 112, 101, 100, 32, 119, 105, 116, 104, 32, 80, 105, 76, 76, 111, 87], border=0, align=bottom]))) :-
	icon_address(pillow, A).


http_simple_response(A, B, C) :-
	http_entity_body(A, B, C).


http_request(A, B, C, D) :-
	http_request_method(B, E, C, F),
	'C'(F, 32, G),
	string(A, G, H),
	'$append'([32, 72, 84, 84, 80, 47, 49, 46, 48], I, H),
	http_crlf(I, J),
	http_req(E, J, D), !.


set_cookie(A, B) :-
	display_list(['Set-Cookie: ', A, =, B, '\n']).


string([], A, A).
string([A|B], C, D) :-
	'C'(C, A, E),
	string(B, E, D).


internet_host_char(A, B, C) :-
	digit(A, B, C).
internet_host_char(A, B, C) :-
	loupalpha(A, B, C).
internet_host_char(45, [45|A], A).
internet_host_char(46, [46|A], A).


form_default(A, B, C) :-
	(   A==''
	->  C=B
	;   C=A
	).


http_document([47|A], B, C) :-
	'C'(B, 47, D), !,
	rest(A, D, C).
http_document([47], A, A).


http_lo_up_token_char(A, B, C) :-
	loupalpha(A, B, C).
http_lo_up_token_char(A, B, C) :-
	digit(A, B, C).
http_lo_up_token_char(A, B, C) :-
	http_token_symb(A, B, C).


http_authenticate(authenticate(A), B, C) :-
	http_field([119, 119, 119, 45, 97, 117, 116, 104, 101, 110, 116, 105, 99, 97, 116, 101], B, D),
	http_challenges(A, D, C).


xml_unit(A, B, C, D, E) :-
	'C'(D, 47, F),
	xml_tag(G, F, H),
	whitespace0(H, I),
	'C'(I, 62, J),
	poptokenstack(G, A, B, C),
	E=J, !.
xml_unit(A, [comment(B)|A], C, D, E) :-
	'$append'([33, 45, 45], F, D),
	string(B, F, G),
	'$append'([45, 45, 62], E, G), !.
xml_unit(A, [declare(B)|A], C, D, E) :-
	'C'(D, 33, F),
	string(B, F, G),
	'C'(G, 62, E), !.
xml_unit(A, [xmldecl(B)|A], C, D, E) :-
	'$append'([63, 120, 109, 108], F, D),
	xml_tag_atts(B, C, F, G),
	whitespace0(G, H),
	'$append'([63, 62], E, H), !.
xml_unit(A, [B|A], C, D, E) :-
	xml_tag(F, D, G),
	xml_tag_atts(H, C, G, I),
	whitespace0(I, J),
	elem_envbeg(B, F, H, J, K),
	'C'(K, 62, E), !.


warning(A) :-
	current_output(B),
	set_output(user_error),
	display_list(['WARNING: '|A]),
	set_output(B).


html_template(A, B, C) :-
	parse_html([], B, C, A, []).


get_form_input_method('GET', A) :-
	(   getenvstr('QUERY_STRING', B),
	    B\==[]
	->  append(B, [38], C),
	    form_urlencoded_to_dic(A, C, [])
	;   A=[]
	).
get_form_input_method('POST', A) :-
	getenvstr('CONTENT_TYPE', B),
	http_media_type(C, D, E, B, []),
	get_form_input_of_type(C, D, E, A).
get_form_input_method(A, B) :-
	html_report_error(['Unknown request method ', tt(A), ' or bad request.']).


xml2terms(A, B) :-
	var(A), !,
	html_term(B, A, []).
xml2terms(A, B) :-
	parse_xml([], B, [], A, []).


http_pragma(pragma(A), B, C) :-
	http_field([112, 114, 97, 103, 109, 97], B, D),
	http_line(A, D, C).


url_info(A, B) :-
	atom(A), !,
	atom_codes(A, C),
	url_to_info(C, B).
url_info(A, B) :-
	instantiated_string(A), !,
	url_to_info(A, B).
url_info(A, B) :-
	info_to_url(B, A).

%   Foreign: rl_read_init_file/1


get_line_after(-1, []) :- !.
get_line_after(10, []) :- !.
get_line_after(13, A) :- !,
	get_code(B),
	get_line_after(B, C),
	(   C=[]
	->  A=[]
	;   A=[13|C]
	).
get_line_after(A, [A|B]) :-
	get_code(C),
	get_line_after(C, B).


http_commas(A, B) :-
	http_lws0(A, C),
	'C'(C, 44, D),
	http_lws0(D, E),
	http_maybe_commas(E, B).


xml_bad_value([], A, A).
xml_bad_value([A|B], C, D) :-
	'C'(C, A, E),
	xml_bad_value(B, E, D).

%   Foreign: tcp_setopt/2


encoded_value([], A, A).
encoded_value([32|A], B, C) :- !,
	'C'(B, 43, D),
	encoded_value(A, D, C).
encoded_value([A|B], C, D) :-
	no_conversion(A),
	E=C, !,
	'C'(E, A, F),
	encoded_value(B, F, D).
encoded_value([A|B], C, D) :-
	hex_chars(A, E, F),
	G=C,
	'$append'([37, E, F], H, G),
	encoded_value(B, H, D).


xml_value(A, B, C, D) :-
	'C'(C, 95, E),
	xml_tag(F, E, G),
	list_lookup(B, =, F, A),
	D=G.
xml_value(A, B, C, D) :-
	'C'(C, 34, E),
	xml_quoted_string(34, A, E, D).
xml_value(A, B, C, D) :-
	'C'(C, 39, E),
	xml_quoted_string(39, A, E, D).
xml_value(A, B, C, D) :-
	xml_bad_value(A, C, D).


http_extension_header(A, B, C) :-
	http_field(D, B, E),
	http_line(F, E, G),
	atom_codes(H, D),
	functor(A, H, 1),
	arg(1, A, F),
	C=G.


output_html(A) :-
	html_term(A, B, []),
	write_string(B).


html_descriptions([], A, A).
html_descriptions([A|B], C, D) :-
	html_description(A, C, E),
	html_descriptions(B, E, D).


http_token_rest([A|B], C, D) :-
	http_token_char(A, C, E),
	http_token_rest(B, E, D).
http_token_rest([], A, A).


xml_tag_rest([A|B], C, D) :-
	xml_tag_char(A, C, E), !,
	xml_tag_rest(B, E, D).
xml_tag_rest([], A, A).


cookies([], A, A).
cookies([A=B|C], D, E) :-
	'$append'([59, 32], F, D),
	cookie_str(G, F, H),
	'C'(H, 61, I),
	cookie_str(J, I, K),
	atom_codes(A, G),
	name(B, J),
	L=K,
	cookies(C, L, E).


expand_esc_plus([], A, A).
expand_esc_plus([43|A], B, C) :- !,
	'C'(B, 32, D),
	expand_esc_plus(A, D, C).
expand_esc_plus([37, A, B|C], D, E) :- !,
	hex_digit(A, F),
	hex_digit(B, G),
	H is F*16+G,
	I=D,
	'C'(I, H, J),
	expand_esc_plus(C, J, E).
expand_esc_plus([A|B], C, D) :-
	'C'(C, A, E),
	expand_esc_plus(B, E, D).


timeout_option(A, B, C) :-
	select(timeout(B), A, C), !.
timeout_option(A, 300, A).


whitespace0(A, B) :-
	whitespace_char(A, C),
	whitespace0(C, B).
whitespace0(A, A).


internet_host_char_rest([A|B], C, D) :-
	internet_host_char(A, C, E),
	internet_host_char_rest(B, E, D).
internet_host_char_rest([], A, A).


http_content_length(content_length(A), B, C) :-
	http_field([99, 111, 110, 116, 101, 110, 116, 45, 108, 101, 110, 103, 116, 104], B, D),
	parse_integer(A, D, E),
	http_lws0(E, F),
	http_crlf(F, C).


mappend([], []).
mappend([A|B], C) :-
	append(A, D, C),
	mappend(B, D).


get_lines_to_boundary(A, B, C) :-
	get_line(D),
	get_lines_to_boundary_(D, A, B, C).


prolog_term_args(A, A, B, C, D) :- !,
	arg(A, B, E),
	F=C,
	prolog_term(E, F, D).
prolog_term_args(A, B, C, D, E) :-
	arg(A, C, F),
	G=D,
	prolog_term(F, G, H),
	'C'(H, 44, I),
	J is A+1,
	K=I,
	prolog_term_args(J, B, C, K, E).


http_lo_up_token_rest([A|B], C, D) :-
	http_lo_up_token_char(A, C, E),
	http_lo_up_token_rest(B, E, D).
http_lo_up_token_rest([], A, A).


optional_port(A, B, C) :-
	'C'(B, 58, D), !,
	parse_integer(A, D, C).
optional_port(80, A, A).


http_lo_up_token(A, B, C) :-
	http_lo_up_token_char(D, B, E),
	http_lo_up_token_rest(F, E, G),
	atom_codes(A, [D|F]),
	C=G.


get_lines_to_boundary_(A, B, C, D) :-
	append(B, E, A),
	check_end(E, D), !,
	C=[].
get_lines_to_boundary_(A, B, [A|C], D) :-
	get_line(E),
	get_lines_to_boundary_(E, B, C, D).


http_auth_param(A=B, C, D) :-
	http_lo_up_token(A, C, E),
	'C'(E, 61, F),
	http_quoted_string(B, F, D).


http_quoted_string(A, B, C) :-
	'C'(B, 34, D),
	http_qs_text(A, D, C).


newline([10|A], A).


form_request_method(A) :-
	getenvstr('REQUEST_METHOD', B),
	atom_codes(A, B).


parse_html(A, B, C, D, E) :-
	'C'(D, 60, F),
	tidy_string(A, G),
	H=F,
	html_unit(G, I, C, H, J), !,
	parse_html(I, B, C, J, E).
parse_html([A|B], C, D, E, F) :-
	nonvar(A),
	A=string(G, H),
	I=E,
	'C'(I, J, K), !,
	H=[J|L],
	M=K,
	parse_html([string(G, L)|B], C, D, M, F).
parse_html(A, B, C, D, E) :-
	'C'(D, F, G), !,
	parse_html([string([F|H], H)|A], B, C, G, E).
parse_html(A, B, C, D, E) :-
	D=F,
	tidy_string(A, G),
	reverse(G, B),
	E=F.


extract_value(data, [A|B], C) :-
	to_value_(B, A, C).
extract_value(file(A), B, file(A, B)).


http_credentials(basic, A, B, C) :- !,
	'$append'([66, 97, 115, 105, 99, 32], D, B),
	string(A, D, C).
http_credentials(A, B, C, D) :- !,
	atom_codes(A, E),
	F=C,
	string(E, F, G),
	'C'(G, 32, H),
	http_credential_params(B, H, D).


hex_char(A, B) :-
	A<10, !,
	B is A+48.
hex_char(A, B) :-
	B is A-10+65.

%   Foreign: rl_add_history/1


http_challenge(challenge(A, B, C), D, E) :-
	http_lo_up_token(A, D, F),
	http_sp(F, G),
	http_lo_up_token(realm, G, H),
	'C'(H, 61, I),
	http_quoted_string(B, I, J),
	http_lws0(J, K),
	http_auth_params(C, K, E).


http_line([], A, B) :-
	http_crlf(A, B), !.
http_line([A|B], C, D) :-
	'C'(C, A, E),
	http_line(B, E, D).


params_to_string([], A, []).
params_to_string([A=B|C], D, [D|E]) :-
	name(A, F),
	name(B, G),
	encoded_value(G, H, I),
	params_to_string(C, 38, I),
	append(F, [61|H], E).


http_entity_body([content(A)], A, []).


hex_digit(A, B) :-
	(   A>=65
	->  B is A/\223-65+10
	;   B is A-48
	).


http_weekday('Monday', [77, 111, 110|A], A).
http_weekday('Tuesday', [84, 117, 101|A], A).
http_weekday('Wednesday', [87, 101, 100|A], A).
http_weekday('Thursday', [84, 104, 117|A], A).
http_weekday('Friday', [70, 114, 105|A], A).
http_weekday('Saturday', [83, 97, 116|A], A).
http_weekday('Sunday', [83, 117, 110|A], A).
http_weekday('Monday', [77, 111, 110, 100, 97, 121|A], A).
http_weekday('Tuesday', [84, 117, 101, 115, 100, 97, 121|A], A).
http_weekday('Wednesday', [87, 101, 100, 110, 101, 115, 100, 97, 121|A], A).
http_weekday('Thursday', [84, 104, 117, 114, 115, 100, 97, 121|A], A).
http_weekday('Friday', [70, 114, 105, 100, 97, 121|A], A).
http_weekday('Saturday', [83, 97, 116, 117, 114, 100, 97, 121|A], A).
http_weekday('Sunday', [83, 117, 110, 100, 97, 121|A], A).


loupalpha(A, B, C) :-
	loalpha(A, B, C), !.
loupalpha(A, B, C) :-
	upalpha(D, B, E),
	A is D+97-65,
	C=E.


http_token_char(A, B, C) :-
	loalpha(A, B, C).
http_token_char(A, B, C) :-
	upalpha(A, B, C).
http_token_char(A, B, C) :-
	digit(A, B, C).
http_token_char(A, B, C) :-
	http_token_symb(A, B, C).


port_codes(80, []) :- !.
port_codes(A, [58|B]) :-
	number_codes(A, B).


get_cookies(A) :-
	getenvstr('HTTP_COOKIE', B),
	cookies(A, [59, 32|B], []), !.
get_cookies([]).


xml_tag_start(A, B, C) :-
	loalpha(A, B, C).
xml_tag_start(A, B, C) :-
	upalpha(A, B, C).
xml_tag_start(95, [95|A], A).
xml_tag_start(58, [58|A], A).


http_lines([A|B], C, D) :-
	http_line(A, C, E), !,
	http_lines(B, E, D).
http_lines([], A, A).


fetch_url(http(A, B, C), D, E) :-
	timeout_option(D, F, G),
	http_request(C, G, H, []), !,
	http_transaction(A, B, H, F, I),
	http_response(E, I, []).


http_full_response([A|B], C, D) :-
	http_status_line(A, C, E),
	http_response_headers(B, F, E, G),
	http_crlf(G, H),
	http_entity_body(F, H, D).


html_quoted(A, B, C) :-
	(   atom(A)
	->  atom_codes(A, D)
	;   D=A
	),
	E=B,
	html_quoted_chars(D, E, C).


form_empty_value(A) :-
	text_lines(A, B),
	empty_lines(B).


xml_tag(A, B, C) :-
	xml_tag_start(D, B, E),
	xml_tag_rest(F, E, G),
	atom_codes(A, [D|F]),
	C=G.


read_all(0, A, B) :- !,
	A=B.
read_all(A, B, C) :-
	get_code(D),
	E=B,
	'C'(E, D, F),
	G is A-1,
	H=F,
	read_all(G, H, C).


tidy_string([A|B], [C|B]) :-
	nonvar(A),
	A=string(C, D), !,
	D=[].
tidy_string(A, A).


internet_host(A, B, C) :-
	internet_host_char(D, B, E),
	internet_host_char_rest(F, E, G),
	atom_codes(A, [D|F]),
	C=G.


http_credential_params([], A, A).
http_credential_params([A|B], C, D) :-
	http_credential_param(A, C, E),
	http_credential_params_rest(B, E, D).


http_server(http_server(A), B, C) :-
	http_field([115, 101, 114, 118, 101, 114], B, D),
	http_line(A, D, C).


html_options([], A, A).
html_options([A|B], C, D) :-
	html_option(A, C, E),
	newline(E, F),
	html_options(B, F, D).


instantiated_string(A) :-
	var(A), !,
	fail.
instantiated_string([]).
instantiated_string([A|B]) :-
	integer(A),
	instantiated_string(B).


content_disposition_header(A, B, C) :-
	'$append'([67, 111, 110, 116, 101, 110, 116, 45, 68, 105, 115, 112, 111, 115, 105, 116, 105, 111, 110, 58, 32, 102, 111, 114, 109, 45, 100, 97, 116, 97], D, B),
	http_type_params(A, D, C).


ls :-
	ls('.').


get_form_input(A) :-
	form_request_method(B),
	get_form_input_method(B, A), !.
get_form_input([]).


check_end([], continue).
check_end([45, 45], end).
