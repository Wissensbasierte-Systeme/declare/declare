/* Copyright(C) 1989, Swedish Institute of Computer Science */

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   File   : random.pl                                                        %
%   Author : Lena Flood                                                       %
%   Date   : 20 June 1989                                                     %
%   Purpose: To provide a random number generator                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- module(random, [
        getrand/1,
        setrand/1,
        random/1,
        random/3,
        randseq/3,
        randset/3
	]).


%   Adapted from shared code written by Richard A O'Keefe

%  return current state

getrand(rand(X,Y,Z)) :-		
   cgetrand(X,Y,Z).

%set a new state
setrand(rand(X,Y,Z)) :-
   integer(X), X > 0, X < 30269,
   integer(Y), Y > 0, Y < 30307,
   integer(Z), Z > 0, Z < 30323,
   cputrand(X,Y,Z).


%   random(-R) 
%   binds R to a new random number in [0.0,1.0)

random(R) :-
        crandom(R).



%   random(+L, +U, -R) 
%   binds R to a random integer in [L,U) when L and U are integers 
%   (note that U will NEVER be generated), or to a random floating 
%   number in [L,U] otherwise.

random(L, U, R) :-
	integer(L), integer(U),
	random(X), !,
	R is L+integer((U-L)*X).
random(L, U, R) :-
	number(L), number(U),
	random(X), !,
	R is L+((U-L)*X).

	
%   randseq(K, N, L)
%   generates a random sequence of K integers int the range 1..N. The 
%   result is in random order.

randseq(K, N, S) :-
	K >= 0,
	K =< N,
	randseq(K, N, L, []),
	keysort(L, R),
	strip_keys(R, S).

randseq(0, _, S, S) :- !.
randseq(K, N, [Y-N|Si], So) :-
	random(X),
	X * N < K, !,
	random(Y),
	J is K-1,
	M is N-1,
	randseq(J, M, Si, So).
randseq(K, N, Si, So) :-
	M is N-1,
	randseq(K, M, Si, So).


strip_keys([], []) :- !.
strip_keys([_-K|L], [K|S]) :-
	strip_keys(L, S).



%   randset(K, N, S)
%   generates a random set of K integers in the range 1..N.
%   The result is an ordered list, with the largest element first
%   and the smallest last in the set.


randset(K, N, S) :-
	K >= 0,
	K =< N,
	randset(K, N, S, []).

randset(0, _, S, S) :- !.
randset(K, N, [N|Si], So) :-
	random(X),
	X * N < K, !,
	J is K-1,
	M is N-1,
	randset(J, M, Si, So).
randset(K, N, Si, So) :-
	M is N-1,
	randset(K, M, Si, So).


%   Load the c function files

:- dynamic foreign/3, foreign_file/2.

foreign_file('random.o',
      [cgetrand,
       cputrand,
       crandom]).

foreign(cgetrand, c, cgetrand(-integer, -integer, -integer)).
foreign(cputrand, c, cputrand(+integer, +integer, +integer)).
foreign(crandom, c, crandom([-float])).

:- load_foreign_files(['random.o'],[]),
   abolish(foreign_file, 2),
   abolish(foreign, 2).

   
