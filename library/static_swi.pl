
:- module(static_swi,[
        static/1,
        static_models_operator/2]).

%==============================================================================
% Project:	Implementation of Static
% Module:	static.pl
% Last Change:	19.01.1996
% Language:	Prolog (ECLiPSe or XSB)
% Author:	Stefan Brass
% Email:	sb@informatik.uni-hannover.de
% Address:	Universitaet Hannover, Lange Laube 22, 30159 Hannover, Germany
% Copyright:	(C) 1996  Stefan Brass
% Copying:	Permitted under the GNU General Public Licence.
% Note:		Based on paper by Teodor Przymusinski, Juergen Dix, and myself.
%==============================================================================

%------------------------------------------------------------------------------
%    I wrote this program in a hurry, so:
%    - It probably contains still a number of bugs
%      (I am interested to hear about them if you find them).
%    - No attempt was made to use more efficient data structures.
%    - The programming style and comments are not optimal.
%    I hope that I can improve this program later.
%    Please send me an email if you want to here about future versions.
%
%    This program is free software; you can redistribute it and/or
%    modify it under the terms of the GNU General Public License
%    as published by the Free Software Foundation; either version 2
%    of the License, or (at your option) any later version.
%    
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%    
%    You should have received a copy of the GNU General Public License
%    along with this program; if not, write to the Free Software
%    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
%------------------------------------------------------------------------------

%------------------------------------------------------------------------------
% Please select whether your Prolog uses "open" or "see":
%------------------------------------------------------------------------------

% For XSB Prolog:
% prolog_use_see(yes).

% For ECLiPSe, SWI and Quintus Prolog:
  prolog_use_see(no).

%------------------------------------------------------------------------------
% Please select how a prompt can be set (or printed with 'write'):
%------------------------------------------------------------------------------

% For SWI-Prolog:
  prolog_use_prompt(swi).	% prompt(-Old, +New)

% For ECLiPSe Prolog:
% prolog_use_prompt(eclipse).	% get_prompt(input, Old, Out),
				% set_prompt(input, New, Out).

% For XSB and Quintus Prolog:
% prolog_use_prompt(write).	% write(New) every time.

%------------------------------------------------------------------------------
% Main Predicate: static(+Filename):
%------------------------------------------------------------------------------

static(Filename) :-
	load_file(Filename, Program),
        nl,
        static_models_operator(Program,Minimal_Models),
        nl,
        write('Enter queries (or "halt."):'),
        nl,
        prolog_set_prompt('STATIC> ', System_Prompt),
        query_loop(Minimal_Models),
        prolog_set_prompt(System_Prompt, _).
 
static_models_operator(Program,Minimal_Models) :-
	write('Input Program (after normalization):'),
	nl,
	print_program(Program),
	nl,
	write('Residual Program:'),
	nl,
	derived_cond_facts(Program, Cond_Facts),
	static_reduce(Cond_Facts, Residual_Prog),
	print_cond_facts(Residual_Prog),
	belief_atoms(Residual_Prog, Belief_Atoms),
	nl,
	(Belief_Atoms = [] ->
		write('No Belief Atoms in Residual Program.'),
		nl,
		Crit_Bel = [bel_int([],[])]
	;
		belief_interpretations(Belief_Atoms, All_Bel_Ints),
		write('All Belief Interpretations:'),
		nl,
		print_belief_ints(All_Bel_Ints),
		theta_iteration(All_Bel_Ints, 1, Residual_Prog, Crit_Bel)),
	phi(Crit_Bel, Residual_Prog, Minimal_Models),
	nl,
	write('Final minimal models:'),
	nl,
	print_obj_models(Minimal_Models), !.

%------------------------------------------------------------------------------
% theta_iteration(+Current_Bel_Int, +Round_Number, +Residual_Prog, -Result):
%------------------------------------------------------------------------------

theta_iteration(Belief_Interpretations, Round, Residual_Prog, Result) :-
	theta(Belief_Interpretations, Residual_Prog, Remaining_Bel_Int),
	write(Round),
	write('. Application of Theta:'),
	nl,
	print_belief_ints(Remaining_Bel_Int),
	(Belief_Interpretations = Remaining_Bel_Int ->
		Result = Belief_Interpretations,
		write('Fixpoint reached.'),
		nl
	;
		Next_Round is Round + 1,
		theta_iteration(Remaining_Bel_Int, Next_Round, Residual_Prog,
					Result)).

%==============================================================================
% Parser for Logic Programs:
%==============================================================================

% Eample of acceptable syntax:
% ---------------------------
%	goto_australia v goto_europe.
%	happy <- goto_australia.
%	happy <- goto_europe.
%	bankrupt <- goto_australia & goto_europe.
%	prudent <- not(goto_australia & goto_europe).
%	disappointed <- not(goto_australia) & not(goto_europe).

%------------------------------------------------------------------------------
% Operators:
%------------------------------------------------------------------------------

:-  op(1200, xfx, '<-').

:- op(990, xfy, 'v').

%  op(1000, xfy, ',').
:- op( 995, xfy, '&').	% Priority 1000 gives problems with not( & ).
			% Note that 'v' binds stronger than '&', this is for
			% queries.

:- op( 900, fy,  not).	% Predefined in many Prolog's, but not Quintus.

%------------------------------------------------------------------------------
% load_file(+Filename, -Program):
%------------------------------------------------------------------------------

load_file(Filename, Program) :-
	prolog_open(Filename, read, InStream),
	(load_stream(InStream, Program) ->
		prolog_close(InStream)
	;
		write('Aborted.'),
		nl,
		prolog_close(InStream),
		fail).

%------------------------------------------------------------------------------
% load_stream(+Stream, -Program):
%------------------------------------------------------------------------------

load_stream(InStream, Program) :-
	prolog_read(InStream, Line),
	(Line == end_of_file ->
		Program = []
	;
		parse(Line, Head, Body),
		cons_rule(Head, Body, Rule),
		Program = [Rule|Rest_Program],
		!,
		load_stream(InStream, Rest_Program)).

%------------------------------------------------------------------------------
% parse(+Input_Line, -List_of_Head_Atoms, -List_of_Body_Literals):
%------------------------------------------------------------------------------

parse(Input_Line, Head, Body) :-
	parse_line(Input_Line, Head, Body),
	!.

parse(Input_Line, _, _) :-
	write('Syntax Error in Input Line: '),
	write(Input_Line),
	nl,
	fail.

%------------------------------------------------------------------------------
% parse_line(+Input_Line,-List_of_Head_Atoms,-List_of_Body_Literals):
%------------------------------------------------------------------------------

parse_line((Input_Head <- Input_Body), Head, Body) :-
	!,
	parse_head(Input_Head, Head),
	parse_body(Input_Body, Body).

parse_line(Input_Head, Head, []) :-
	parse_head(Input_Head, Head).

%------------------------------------------------------------------------------
% parse_head(+Input_Head, -List_of_Atoms):
%------------------------------------------------------------------------------

parse_head(v(Atom,Input_Head), [Atom|Head]) :-
	!,
	parse_atom(Atom),
	parse_head(Input_Head,Head).

parse_head(Atom, [Atom]) :-
	parse_atom(Atom).

%------------------------------------------------------------------------------
% parse_body(+Input_Body, -List_of_Body_Literals):
%------------------------------------------------------------------------------

parse_body(&(Input_Literal,Input_Body),[Literal|Body]) :-
	!,
	parse_literal(Input_Literal, Literal),
	parse_body(Input_Body, Body).

parse_body(','(Input_Literal,Input_Body),[Literal|Body]) :-
	!,
	parse_literal(Input_Literal, Literal),
	parse_body(Input_Body, Body).

parse_body(Input_Literal, [Literal]) :-
	parse_literal(Input_Literal, Literal).

%------------------------------------------------------------------------------
% parse_literal(+Input_Literal, -Literal):
%------------------------------------------------------------------------------

parse_literal(not(Input_Belief), not(Sorted_Belief)) :-
	!,
	parse_belief(Input_Belief, Belief),
	list_sort(Belief, Sorted_Belief).

parse_literal(Atom, Atom) :-
	parse_atom(Atom).

%------------------------------------------------------------------------------
% parse_belief(+Input_Belief, -List_of_atoms):
%------------------------------------------------------------------------------

parse_belief(&(Atom,Input_Belief), [Atom|Belief]) :-
	!,
	parse_atom(Atom),
	parse_belief(Input_Belief, Belief).

% Note: not(g,h) is NOT not(','(g,h)). Therefore, we allow only & inside not.

parse_belief(Atom, [Atom]) :-
	parse_atom(Atom).

%------------------------------------------------------------------------------
% parse_atom(+External_Representation, -Atom):
%------------------------------------------------------------------------------

parse_atom(Atom) :-
	atom(Atom),
	Atom \== 'v',
	Atom \== ('not').

%------------------------------------------------------------------------------
% cons_rule(+Head, +Body, -Rule):
%------------------------------------------------------------------------------

cons_rule(Head, Body, rule(Sorted_Head,Sorted_Obj_Body,Sorted_Bel_Body)) :-
	split_body(Body, Obj_Body, Bel_Body),
	list_sort(Head, Sorted_Head),
	list_sort(Obj_Body, Sorted_Obj_Body),
	list_sort(Bel_Body, Sorted_Bel_Body).

%------------------------------------------------------------------------------
% split_body(+List_of_Body_Atoms, -Objective_Atoms, -Belief_Atoms):
%------------------------------------------------------------------------------

split_body([], [], []).

split_body([not(Belief)|Rest], Objective_Rest, [not(Belief)|Belief_Rest]) :-
	!,
	split_body(Rest, Objective_Rest, Belief_Rest).

split_body([Atom|Rest], [Atom|Objective_Rest], Belief_Rest) :-
	split_body(Rest, Objective_Rest, Belief_Rest).

%==============================================================================
% Unparser for Programs and Sets of Conditional Facts:
%==============================================================================

%------------------------------------------------------------------------------
% print_program(+Program):
%------------------------------------------------------------------------------

print_program([]).

print_program([rule(Head,Obj_Body,Bel_Body)|Rest]) :-
	list_append(Obj_Body, Bel_Body, Body),
	write('	'), % <- This is a TAB
	print_head(Head),
	(Body=[] ->
		true
	;
		write(' <- '),
		print_body(Body)),
	write('.'),
	nl,
	print_program(Rest).

%------------------------------------------------------------------------------
% print_cond_facts(+Cond_Facts):
%------------------------------------------------------------------------------

print_cond_facts([]).

print_cond_facts([cond_fact(Head,Cond)|Rest]) :-
	write('	'), % <- This is a TAB
	print_head(Head),
	(Cond=[] ->
		true
	;
		write(' <- '),
		print_body(Cond)),
	write('.'),
	nl,
	print_cond_facts(Rest).

%------------------------------------------------------------------------------
% print_head(+Nonempty_List_of_Head_Atoms):
%------------------------------------------------------------------------------

print_head([]) :-
	impossible(print_head, 'Empty list').

print_head([Atom]) :-
	!,
	write(Atom).

print_head([Atom|Head]) :-
	write(Atom),
	write(' v '),
	print_head(Head).

%------------------------------------------------------------------------------
% print_body(+Nonempty_List_of_Body_Literals):
%------------------------------------------------------------------------------

print_body([]) :-
	impossible(print_body, 'Empty list').

print_body([Literal]) :-
	!,
	print_literal(Literal).

print_body([Literal|Body]) :-
	print_literal(Literal),
	write(' & '),
	print_body(Body).

%------------------------------------------------------------------------------
% print_literal(+Literal):
%------------------------------------------------------------------------------

print_literal(not(Belief)) :-
	!,
	write('not('),
	print_belief(Belief),
	write(')').

print_literal(Atom) :-
	write(Atom).

%------------------------------------------------------------------------------
% print_belief(+Nonempty_List_of_Atoms):
%------------------------------------------------------------------------------

print_belief([]) :-
	impossible(print_belief, 'Empty list').

print_belief([Atom]) :-
	!,
	write(Atom).

print_belief([Atom|Belief]) :-
	write(Atom),
	write('&'),
	print_belief(Belief).

%==============================================================================
% Computation of Derived Conditional Facts:
%==============================================================================

%------------------------------------------------------------------------------
% derived_cond_facts(+Program, -Cond_Facts):
%------------------------------------------------------------------------------

derived_cond_facts(Program, Cond_Facts) :-
	split_program(Program, Prog_Facts, Prog_Rules),
	compute_derived(Prog_Facts, Prog_Rules, Cond_Facts).

%------------------------------------------------------------------------------
% split_program(+Program, -Rules_Without_Objective_Body_Atoms, -Proper_Rules):
%------------------------------------------------------------------------------

split_program([], [], []).

split_program([rule(Head,Obj_Body,Bel_Body)|Rest], Cond_Facts, Rules) :-
	(Obj_Body = [] ->
		Cond_Facts = [cond_fact(Head,Bel_Body)|Rest_Facts],
		Rules = Rest_Rules
	;
		Cond_Facts = Rest_Facts,
		Rules = [rule(Head,Obj_Body,Bel_Body)|Rest_Rules]),
	split_program(Rest, Rest_Facts, Rest_Rules).

%------------------------------------------------------------------------------
% compute_derived(+Given_Cond_Facts, +Rules, -Derived_Cond_Facts):
%------------------------------------------------------------------------------

compute_derived(Cond_Facts, Rules, Result) :-
	derived(Cond_Facts, Rules, New),
	\+ is_duplicate(New, Cond_Facts),
	!,
	compute_derived([New|Cond_Facts], Rules, Result).

compute_derived(Fixpoint, _, Fixpoint).

%------------------------------------------------------------------------------
% derived(+Cond_Facts, +Rules, -New_Cond_Fact):
%------------------------------------------------------------------------------

derived(Cond_Facts, Rules, cond_fact(New_Head,New_Cond)) :-
	list_member(rule(Rule_Head,Rule_Body,Rule_Cond), Rules),
	evaluate(Rule_Body, Cond_Facts, Context_Head, Context_Cond),
	list_merge(Rule_Head, Context_Head, New_Head),
	list_merge(Rule_Cond, Context_Cond, New_Cond).

%------------------------------------------------------------------------------
% evaluate(+Body, +Cond_Facts, -Context_Head, -Context_Cond):
%------------------------------------------------------------------------------

evaluate([], _, [], []).

evaluate([Body_Atom|Body], Cond_Facts, Context_Head, Context_Cond) :-
	list_member(cond_fact(Fact_Head,Fact_Cond), Cond_Facts),
	list_delete(Body_Atom, Fact_Head, Fact_RestHead),
	evaluate(Body, Cond_Facts, More_Head, More_Cond),
	list_merge(Fact_RestHead, More_Head, Context_Head),
	list_merge(Fact_Cond, More_Cond, Context_Cond).

%------------------------------------------------------------------------------
% is_duplicate(+Cond_Fact, +List_of_Conditional_Facts):
%------------------------------------------------------------------------------

is_duplicate(cond_fact(Head1,Cond1), [cond_fact(Head2,Cond2)|_]) :-
	list_subseteq(Head2, Head1),
	list_subseteq(Cond2,Cond1).

is_duplicate(Cond_Fact, [_|Rest]) :-
	is_duplicate(Cond_Fact, Rest).

%==============================================================================
% Reduction Steps:
%==============================================================================

%------------------------------------------------------------------------------
% static_reduce(+Cond_Facts, -Residual_Prog):
%------------------------------------------------------------------------------

static_reduce(Cond_Facts, Residual_Prog) :-
	reduction_step(Cond_Facts, Cond_Facts, Reduced_Cond_Facts),
	!,
	static_reduce(Reduced_Cond_Facts, Residual_Prog).

static_reduce(Fixpoint, Fixpoint).

%------------------------------------------------------------------------------
% reduction_step(+Cond_Facts, +All_Cond_Facts, -Reduced_Cond_Facts):
%------------------------------------------------------------------------------

reduction_step([Cond_Fact|More], All, More) :-
	non_minimal(Cond_Fact, All).

reduction_step([Cond_Fact|More], All, More) :-
	neg_reduction(Cond_Fact, All).

reduction_step([Cond_Fact|More], All, [New|More]) :-
	pos_reduction(Cond_Fact, All, New).

reduction_step([Cond_Fact|More], All, [Cond_Fact|Reduced_More]) :-
	reduction_step(More, [Cond_Fact|All], Reduced_More).

%------------------------------------------------------------------------------
% non_minimal(+Cond_Fact, +Derived_Cond_Facts):
%------------------------------------------------------------------------------

non_minimal(cond_fact(Head1,Cond1), Derived_Cond_Facts) :-
	list_member(cond_fact(Head2,Cond2), Derived_Cond_Facts),
	(	list_subset(Head2, Head1),
		list_subseteq(Cond2, Cond1)
	;	list_subset(Cond2, Cond1),
		list_subseteq(Head2, Head1)).

%------------------------------------------------------------------------------
% neg_reduction(+Cond_Fact, +Derived_Cond_Facts):
%------------------------------------------------------------------------------

neg_reduction(cond_fact(_,Cond), Derived_Cond_Facts) :-
	list_member(cond_fact(Head,[]), Derived_Cond_Facts),
	negate(Head, Negated_Head),
	list_sort(Negated_Head, Sorted_Negated_Head), % Superflous?
	list_subseteq(Sorted_Negated_Head, Cond).

%------------------------------------------------------------------------------
% negate(+Atom_List, -Belief_Literal_List):
%------------------------------------------------------------------------------

negate([], []).

negate([Atom|More_Atoms], [not([Atom])|More_Belief_Literals]) :-
	negate(More_Atoms, More_Belief_Literals).

%------------------------------------------------------------------------------
% pos_reduction(+Cond_Fact, +Derived_Cond_Facts, -New_Cond_Fact):
%------------------------------------------------------------------------------

pos_reduction(cond_fact(Head,Cond), Derived_Cond_Facts, cond_fact(Head,New)) :-
	possibly_true(Derived_Cond_Facts, [], Possibly_True),
	list_member(not(Belief), Cond),
	list_member(Atom, Belief),
	\+ list_member(Atom, Possibly_True),
	list_delete(not(Belief), Cond, New).

%------------------------------------------------------------------------------
% possibly_true(+Derived_Cond_Facts, +Possibly_True_In, -Possibly_True_Out):
%------------------------------------------------------------------------------

possibly_true([], Possibly_True, Possibly_True).

possibly_true([cond_fact(Head,_)|More_Cond_Facts], In, Possibly_True) :-
	list_merge(Head, In, New_In),
	possibly_true(More_Cond_Facts, New_In, Possibly_True).

%==============================================================================
% Compute List of All Belief Atoms Occurring in the Residual Program:
%==============================================================================

%------------------------------------------------------------------------------
% belief_atoms(+Residual_Program, -Belief_Atoms):
%------------------------------------------------------------------------------

belief_atoms(Residual_Program, Belief_Atoms) :-
	belief_atoms(Residual_Program, [], Belief_Atoms).

%------------------------------------------------------------------------------
% belief_atoms(+Residual_Program, +Belief_Atoms_In, -Belief_Atoms_Out):
%------------------------------------------------------------------------------

belief_atoms([], Belief_Atoms, Belief_Atoms).

belief_atoms([cond_fact(_,Cond)|Rest], Belief_Atoms_In, Belief_Atoms_Out) :-
	list_merge(Cond, Belief_Atoms_In, Belief_Atoms),
	belief_atoms(Rest, Belief_Atoms, Belief_Atoms_Out).

%------------------------------------------------------------------------------
% belief_interpretations(+Belief_Atoms, -All_Belief_Interpretations):
%------------------------------------------------------------------------------

belief_interpretations(Belief_Atoms, All_Bel_Int) :-
	findall(Bel_Int, belief_int(Belief_Atoms,Bel_Int), All_Bel_Int).

%------------------------------------------------------------------------------
% belief_int(+Belief_Atoms, -Belief_Interpretation):
%------------------------------------------------------------------------------

belief_int([], bel_int([],[])).

belief_int([Bel_Atom|More], bel_int([Bel_Atom|True],False)) :-
	belief_int(More, bel_int(True,False)).

belief_int([Bel_Atom|More], bel_int(True,[Bel_Atom|False])) :-
	belief_int(More, bel_int(True,False)).

%------------------------------------------------------------------------------
% print_belief_ints(+List_of_Belief_Interpretations):
%------------------------------------------------------------------------------

print_belief_ints([]).

print_belief_ints([Bel_Int|More]) :-
	print_bel_int(Bel_Int),
	print_belief_ints(More).

%------------------------------------------------------------------------------
% print_bel_int(+Bel_Int):
%------------------------------------------------------------------------------

print_bel_int(bel_int(True_Beliefs,False_Beliefs)) :-
	list_merge(True_Beliefs, False_Beliefs, All_Beliefs),
	write('	'), % <- this is a TAB
	print_bel_int(All_Beliefs, True_Beliefs).

print_bel_int([], _) :-
	nl.

print_bel_int([Belief|More], True_Beliefs) :-
	print_literal(Belief),
	(list_member(Belief, True_Beliefs) ->
		write(':TRUE   ')
	;
		write(':FALSE  ')),
	print_bel_int(More, True_Beliefs).

%==============================================================================
% Computation of Minimal Models:
%==============================================================================

%------------------------------------------------------------------------------
% min_mod(+Residual_Prog, +Belief_Int, -Min_Mod):
%------------------------------------------------------------------------------

min_mod(Residual_Prog, bel_int(True_Beliefs,_), Min_Mod) :-
	select_heads(Residual_Prog, True_Beliefs, Heads),
	list_delete_nonmin(Heads, Disjunctions),
	completion(Disjunctions, Comp_Rules),
	list_delete_nonmin(Comp_Rules, Completion),
	generate(Disjunctions, Completion, Min_Mod).

%------------------------------------------------------------------------------
% select_heads(+Residual_Prog, +True_Beliefs, -Heads):
%------------------------------------------------------------------------------

select_heads([], _, []).

select_heads([cond_fact(Head,Cond)|Rest_Prog], True_Beliefs, [Head|More]) :-
	list_subseteq(Cond, True_Beliefs),
	!,
	select_heads(Rest_Prog, True_Beliefs, More).

select_heads([_|Rest_Prog], True_Beliefs, More) :-
	select_heads(Rest_Prog, True_Beliefs, More).

%------------------------------------------------------------------------------
% generate(+Disjunctions, +Comp_Rules, -True):
%------------------------------------------------------------------------------

generate(Disjunctions, Comp_Rules, True) :-
	hyperres(Disjunctions, Comp_Rules, Derived_Dis),
	list_delete_nonmin(Derived_Dis, Minimal_Dis),
	select_definite(Minimal_Dis, Definite, Indefinite),
	gen_model(Indefinite, Definite, Comp_Rules, True).

%------------------------------------------------------------------------------
% gen_model(+Indefinite, +Definite, +Comp_Rules, -True):
%------------------------------------------------------------------------------

gen_model([], Definite, _, True) :-
	!,
	list_flatten(Definite, True).

gen_model(Indefinite, Definite, Comp_Rules, True) :-
	[[Atom|_]|_] = Indefinite,
	list_append(Definite, Indefinite, Disjunctions),
	(	generate([[Atom]|Disjunctions], Comp_Rules, True)
	;	generate(Disjunctions, [[Atom]|Comp_Rules], True)).

%------------------------------------------------------------------------------
% select_definite(+List_of_nonempty_lists, -One_Element_Lists, ?Other_Lists):
%------------------------------------------------------------------------------

select_definite([], [], []).

select_definite([[]|_], _, _) :-
	impossible(select_definite, 'inconsistent'),
	nl.

select_definite([[Elem]|More_Lists], [[Elem]|More_Definite], More_Other) :-
	!,
	select_definite(More_Lists, More_Definite, More_Other).

select_definite([Other|More_Lists], More_Definite, [Other|More_Other]) :-
	select_definite(More_Lists, More_Definite, More_Other).

%------------------------------------------------------------------------------
% print_obj_models(+List_of_Lists_of_True_Atoms):
%------------------------------------------------------------------------------

print_obj_models([]).

print_obj_models([Model|More]) :-
	write('	'), % This is a TAB
	print_model(Model),
	nl,
	print_obj_models(More).

%------------------------------------------------------------------------------
% print_model(+List_of_True_Atoms):
%------------------------------------------------------------------------------

print_model([]) :-
	write('(all propositions false).').

print_model([Atom]) :-
	!,
	write(Atom),
	write('.').

print_model([Atom|More]) :-
	write(Atom),
	write(', '),
	print_model(More).

%==============================================================================
% Computation of Completion:
%==============================================================================

%------------------------------------------------------------------------------
% completion(+Disjunctions, -Comp_Rules):
%------------------------------------------------------------------------------

completion(Disjunctions, Comp_Rules) :-
	list_flatten(Disjunctions, Atoms),
	comp_loop(Atoms, Disjunctions, Comp_Rules).

%------------------------------------------------------------------------------
% comp_loop(+Atoms, +Disjunctions, -Comp_Rules):
%------------------------------------------------------------------------------

comp_loop([], _, []).

comp_loop([Atom|More_Atoms], Disjunctions, Comp_Rules) :-
	comp_rules_for_atom(Disjunctions, Atom, Atom_Rules),
	comp_loop(More_Atoms, Disjunctions, More_Rules),
	list_append(Atom_Rules, More_Rules, Comp_Rules).

%------------------------------------------------------------------------------
% comp_rules_for_atom(+Disjunctions, +Atom, -Comp_Rules):
%------------------------------------------------------------------------------

comp_rules_for_atom(Disjunctions, Atom, Comp_Rules) :-
	findall(Implies, implies(Disjunctions, Atom, Implies), List),
	findall(Comp_Rule, comp_rule(Atom, List, Comp_Rule), Comp_Rules).

%------------------------------------------------------------------------------
% implies(+Disjunctions, +Atom, -Implies):
%------------------------------------------------------------------------------

implies(Disjunctions, Atom, Implies) :-
	list_member(Dis, Disjunctions),
	list_delete(Atom, Dis, Implies).

%------------------------------------------------------------------------------
% comp_rule(+Atom, -List_of_Bodies, -Comp_Rule):
%------------------------------------------------------------------------------

comp_rule(Atom, List_of_Bodies, Comp_Rule) :-
	one_of_each(List_of_Bodies, Multiplied_Out_Condition),
	list_sort([Atom|Multiplied_Out_Condition], Comp_Rule).

%------------------------------------------------------------------------------
% one_of_each(+List_of_lists, -List_containing_one_element_of_each_list):
%------------------------------------------------------------------------------

one_of_each([], []).

one_of_each([First_List|More_Lists], [First_Elem|More_Elems]) :-
	list_member(First_Elem, First_List),
	one_of_each(More_Lists, More_Elems).

%==============================================================================
% Hyperresolution on Completion:
%==============================================================================

%------------------------------------------------------------------------------
% hyperres(+Disjunctions, +Comp_Rules, -Result):
%------------------------------------------------------------------------------

hyperres(Disjunctions, Comp_Rules, Result) :-
	list_member(Rule, Comp_Rules),
	static_resolve(Rule, Disjunctions, New_Dis),
	\+ list_is_superflous(New_Dis, Disjunctions),
	!,
	hyperres([New_Dis|Disjunctions], Comp_Rules, Result).

hyperres(Facts, _, Facts).

%------------------------------------------------------------------------------
% static_resolve(+Comp_Rule, +Disjunctions, -New_Dis):
%------------------------------------------------------------------------------

static_resolve([], _, []).

static_resolve([BodyAtom|Body], Disjunctions, New_Dis) :-
	list_member(Dis, Disjunctions),
	list_delete(BodyAtom, Dis, Context),
	static_resolve(Body, Disjunctions, More_Contexts),
	list_merge(Context, More_Contexts, New_Dis).

%==============================================================================
% Fixpoint Computation with Psi and Phi:
%==============================================================================

%------------------------------------------------------------------------------
% phi(+Belief_Interpretations, +Res_Prog, -Objective_Parts_of_Minimal_Models):
%------------------------------------------------------------------------------

% Given a set/list of interpretations of the belief atoms occurring in a
% residual program, this predicate returns a list of the objective parts
% of the minimal models based on these interpretations of the belief atoms.
% For instance, if the residual program is
%	p v q.				cond_fact([p,q], []).
%	q v r.				cond_fact([q,r], []).
%	s v t <- not(p).		cond_fact([s,t], [not([p])]).
%	t <- not(r).			cond_fact([t], [not([r])]).
% and the belief interpretations are
%	not(p):TRUE   not(r):TRUE   	bel_int([not([p]),not([r])], [])
%	not(p):FALSE  not(r):FALSE  	bel_int([], [not([p]),not([r])])
% the following minimal models are returned:
%	p, r.				[p, r].
%	p, r, t.			[p, r, t].
%	q.				[q].
%	q, t.				[q, t].
% (p, r) and (q) result from the second belief interpretation (both belief
% atoms false) and (p, r, t) and (q, t) result from the first (both true).

phi(Belief_Ints, Res_Prog, Min_Mods) :-
	phi_loop(Belief_Ints, Res_Prog, [], Min_Mods).

%------------------------------------------------------------------------------
% phi_loop(+Belief_Ints, +Min_Mods_In, -Min_Mods_Out):
%------------------------------------------------------------------------------

phi_loop([], _, Min_Mods, Min_Mods).

phi_loop([Bel_Int|More_Bel_Ints], Res_Prog, Min_Mods_In, Min_Mods_Out) :-
	findall(Min_Mod, min_mod(Res_Prog, Bel_Int, Min_Mod), Min_Mods),
	list_sort(Min_Mods, Sorted_Min_Mods),
	list_merge(Sorted_Min_Mods, Min_Mods_In, New_Min_Mods),
	phi_loop(More_Bel_Ints, Res_Prog, New_Min_Mods, Min_Mods_Out).

%------------------------------------------------------------------------------
% theta(+Belief_Interpretations_In, +Res_Prog, -Belief_Interpretations_Out):
%-----------------------------------------------------------------------------

% This is the theta-operator of the paper, the combination of phi and psi.
% It takes a set/list of interpretations of the belief atoms occurring in
% a residual program, and returns the subset which remain possible given
% on the minimal models based on the input belief interpretations.
% "Possible" means that there is a set of minimal models, such that the true
% belief atoms are exactly the intersection of the corresponding true objective
% parts in the minimal models. I.e. B(~p1 v ... v ~pn) holds iff
% ~p1 v ... v ~pn holds in all minimal models of this (arbitrary, but
% non-empty) set.

theta(Bel_Ints_In, Res_Prog, Bel_Ints_Out) :-
	phi(Bel_Ints_In, Res_Prog, Min_Mods),
	theta_filter(Bel_Ints_In, Min_Mods, Bel_Ints_Out).

%------------------------------------------------------------------------------
% theta_filter(+Bel_Ints_In, +Min_Mods, -Bel_Ints_Out):
%-----------------------------------------------------------------------------

theta_filter([], _, []).

theta_filter([Bel_Int|More_In], Min_Mods, [Bel_Int|More_Out]) :-
	psi_ok(Bel_Int, Min_Mods),
	!,
	theta_filter(More_In, Min_Mods, More_Out).

theta_filter([_|More_In], Min_Mods, More_Out) :-
	theta_filter(More_In, Min_Mods, More_Out).

%------------------------------------------------------------------------------
% psi_ok(+Bel_Int, +Min_Mods):
%------------------------------------------------------------------------------

% Here we check wether a given (partial) interpretation of the belief atoms
% is possible given a set of minimal models.
% First, we select those minimal models in which all true belief atoms are
% satisfied (i.e. for every true belief atom B(~p1 v ... v ~pn) at least one pi
% must be false in the minimal model).
% This is the maximal set of worlds to which the current world can be linked.
% This set is called "Support" below.
% Second, due to the consistency axiom, we must require that this set is
% non-empty.
% Third, we must check that for every belief atom B(~p1 v ... v ~pn) false
% in the given (partial) interpretation, there is a world, to which the
% current world is linked (i.e. in Support), in which ~p1 v ... v ~pn is false.
% Obviously, if this condition is not satisfied for the maximal set of worlds,
% to which the current world can be linked, it cannot be satisfied for any
% subset.

psi_ok(bel_int(True_Beliefs,False_Beliefs), Min_Mods) :-
	select_support_for_true_beliefs(Min_Mods, True_Beliefs, Support),
	Support \== [],
	check_support_for_false_beliefs(False_Beliefs, Support).

%------------------------------------------------------------------------------
% select_support_for_true_beliefs(+Min_Mods, +True_Beliefs, -Support):
%------------------------------------------------------------------------------

select_support_for_true_beliefs([], _, []).

select_support_for_true_beliefs([Min_Mod|In], True_Beliefs, [Min_Mod|Out]) :-
	beliefs_supported(True_Beliefs, Min_Mod),
	!,
	select_support_for_true_beliefs(In, True_Beliefs, Out).

select_support_for_true_beliefs([_|In], True_Beliefs, Out) :-
	select_support_for_true_beliefs(In, True_Beliefs, Out).

%------------------------------------------------------------------------------
% beliefs_supported(+Bel_Int, +Min_Mod):
%------------------------------------------------------------------------------

beliefs_supported([], _).

beliefs_supported([not(Beliefs)|More_Bel], Min_Mod) :-
	\+ list_subseteq(Beliefs, Min_Mod),
	beliefs_supported(More_Bel, Min_Mod).

%------------------------------------------------------------------------------
% check_support_for_false_beliefs(+False_Beliefs, +Support):
%------------------------------------------------------------------------------

check_support_for_false_beliefs([], _).

check_support_for_false_beliefs([not(Belief)|More], Support) :-
	list_member(Min_Mod, Support),
	list_subseteq(Belief, Min_Mod),
	!,
	check_support_for_false_beliefs(More, Support).

%==============================================================================
% Query Interface to Static Models:
%==============================================================================

%------------------------------------------------------------------------------
% query_loop(+Minimal_Models):
%------------------------------------------------------------------------------

query_loop(Minimal_Models) :-
	prolog_print_prompt('STATIC> '),
	read(Line),
	((Line = halt; Line = end_of_file) ->
		true
	;
		(parse_query(Line, Query) ->
			answer_query(Query, Minimal_Models)
		;
			write('	Syntax error in query.'),
			nl),
		!,
		query_loop(Minimal_Models)).

%------------------------------------------------------------------------------
% parse_query(+Input_Line, -List_of_Disjunctions_of_Objective_or_Belief_Atoms):
%------------------------------------------------------------------------------

parse_query(','(Input_Dis,More_Input), [Dis|More_Dis]) :-
	parse_dis(Input_Dis, Dis),
	!,
	parse_query(More_Input, More_Dis).

parse_query(&(Input_Dis,More_Input), [Dis|More_Dis]) :-
	parse_dis(Input_Dis, Dis),
	!,
	parse_query(More_Input, More_Dis).

parse_query(Input_Dis, [Dis]) :-
	parse_dis(Input_Dis, Dis).

%------------------------------------------------------------------------------
% parse_dis(+Input, -Disjunction_of_Objective_or_Belief_Atom):
%------------------------------------------------------------------------------

parse_dis(Input_Disjunction, objective(Sorted_Disjunction)) :-
	parse_head(Input_Disjunction, Disjunction),
	list_sort(Disjunction, Sorted_Disjunction).

parse_dis(Input_Disjunction, belief(Belief_Disjunction)) :-
	parse_bel_dis(Input_Disjunction, Belief_Disjunction).

%------------------------------------------------------------------------------
% parse_bel_dis(+Input_Disjunction, -Belief_Disjunction):
%------------------------------------------------------------------------------

parse_bel_dis(v(Input_Bel, More_Input), [Bel|More_Bel]) :-
	!,
	parse_bel_lit(Input_Bel, Bel),
	parse_bel_dis(More_Input, More_Bel).

parse_bel_dis(Input_Bel, [Bel]) :-
	parse_bel_lit(Input_Bel, Bel).

%------------------------------------------------------------------------------
% parse_bel_lit(+Input_Lit, -Belief):
%------------------------------------------------------------------------------

parse_bel_lit(not(Input_Belief), not(Sorted_Belief)) :-
	!,
	parse_belief(Input_Belief, Belief),
	list_sort(Belief, Sorted_Belief).

%------------------------------------------------------------------------------
% answer_query(+Query, +Minimal_Models):
%------------------------------------------------------------------------------

answer_query(Query, Minimal_Models) :-
	(prove_query(Query, Minimal_Models)->
		write('	yes.'),
		nl
	;
		write('	no.'),
		nl).

%------------------------------------------------------------------------------
% prove_query(+Query, +Minimal_Models):
%------------------------------------------------------------------------------

prove_query([], _).

prove_query([Dis|More], Minimal_Models) :-
	(Dis = objective(Obj_Dis) ->
		prove_obj_dis(Minimal_Models, Obj_Dis)
	;
		Dis = belief(Bel_Dis),
		prove_bel_dis(Minimal_Models, Bel_Dis)),
	prove_query(More, Minimal_Models).

%------------------------------------------------------------------------------
% prove_obj_dis(+Minimal_Models, +Dis):
%------------------------------------------------------------------------------

% p1 v ... v pk holds in all static models iff it holds in all minimal models.

prove_obj_dis([], _).

prove_obj_dis([Min_Mod|More], Dis) :-
	list_overlap(Min_Mod, Dis),
	prove_obj_dis(More, Dis).

%------------------------------------------------------------------------------
% prove_bel_dis(+Minimal_Models, +Bel_Dis):
%------------------------------------------------------------------------------

% not(C1) v ... v not(Cn) holds in all static models iff at least one
% not(Ci) holds in all static models.
% Note that in the static semantics, not(p) v not(q) follows only iff one of
% the two follows.

prove_bel_dis(Minimal_Models, [not(Belief)|_]) :-
	prove_belief(Minimal_Models, Belief),
	!.

prove_bel_dis(Minimal_Models, [_|More]) :-
	prove_bel_dis(Minimal_Models, More).

%------------------------------------------------------------------------------
% prove_belief(+Minimal_Models, +Belief):
%------------------------------------------------------------------------------

% not(p1 & ... &pk) holds in all static models iff there is not a minimal
% model in which (p1 & ... &pk) is true.

prove_belief([], _).

prove_belief([Min_Mod|More], Belief) :-
	\+ list_subseteq(Belief, Min_Mod),
	prove_belief(More, Belief).

%==============================================================================
% Assertions:
%==============================================================================

%------------------------------------------------------------------------------
% impossible(+Predicate, +Error_Message):
%------------------------------------------------------------------------------

impossible(Predicate, Error_Message) :-
	nl,
	write('*** BUG DETECTED! '),
	write(Predicate),
	write(': '),
	write(Error_Message),
	write('***'),
	nl,
	fail.

%==============================================================================
% List Functions:
%==============================================================================

%------------------------------------------------------------------------------
% list_sort(+List, -Sorted_list_without_duplicates):
%------------------------------------------------------------------------------

list_sort([], []).

list_sort([Limit|Rest], SortedList) :-
	list_split(Limit, Rest, Smaller, Greater),
	list_sort(Smaller, SortedSmaller),
	list_sort(Greater, SortedGreater),
	list_merge(SortedSmaller, [Limit|SortedGreater], SortedList).

%------------------------------------------------------------------------------
% list_split(+Limit, +List, -Smaller, -Greater):
%------------------------------------------------------------------------------

list_split(_, [], [], []).

list_split(Limit, [First|Rest], Smaller, Greater) :-
	Limit == First,
	!,
	list_split(Limit, Rest, Smaller, Greater).

list_split(Limit, [First|Rest], [First|Smaller], Greater) :-
	First @=< Limit,
	!,
	list_split(Limit, Rest, Smaller, Greater).

list_split(Limit, [First|Rest], Smaller, [First|Greater]) :-
	Limit @=< First,
	!,
	list_split(Limit, Rest, Smaller, Greater).

%------------------------------------------------------------------------------
% list_merge(+Sorted_list_1, +Sorted_list_2, -Merged_List):
%------------------------------------------------------------------------------

list_merge([], List, List) :-
	!.

list_merge(List, [], List) :-
	!.

list_merge([Elem1|List1], [Elem2|List2], [Elem1|List]) :-
	Elem1 == Elem2,
	!,
	list_merge(List1, List2, List).

list_merge([Elem1|List1], [Elem2|List2], [Elem1|List]) :-
	Elem1 @=< Elem2,
	!,
	list_merge(List1, [Elem2|List2], List).

list_merge([Elem1|List1], [Elem2|List2], [Elem2|List]) :-
	Elem2 @=< Elem1,
	list_merge([Elem1|List1], List2, List).

%------------------------------------------------------------------------------
% list_delete(+Element, +List, -Rest):
%------------------------------------------------------------------------------

list_delete(Element, [Element|Rest], Rest) :-
	!.

list_delete(Element, [Other|List], [Other|Rest]) :-
	list_delete(Element, List, Rest).

%------------------------------------------------------------------------------
% list_subset(+Ordered_list_1, +Proper_superset_of_ordered_list_1):
%------------------------------------------------------------------------------

list_subset([], [_|_]).

list_subset([E1|L1], [E2|L2]) :-
	(E1 = E2 ->
		list_subset(L1, L2)
	;	E2 @=< E1,
		list_subseteq([E1|L1], L2)).

%------------------------------------------------------------------------------
% list_subseteq(+Ordered_list_1, +superset_of_ordered_list_1_or_equal):
%------------------------------------------------------------------------------

list_subseteq([], _).

list_subseteq([E1|L1], [E2|L2]) :-
	(E1 = E2 ->
		list_subseteq(L1, L2)
	;	E2 @=< E1,
		list_subseteq([E1|L1], L2)).

%------------------------------------------------------------------------------
% list_append(+L1, +L2, -Concatenation_of_L1_and_L2):
%------------------------------------------------------------------------------

list_append([], L, L).

list_append([E|L1], L2, [E|L1L2]) :-
	list_append(L1, L2, L1L2).

%------------------------------------------------------------------------------
% list_member(-Member, +List):
%------------------------------------------------------------------------------

list_member(E, [E|_]).

list_member(E, [_|R]) :-
	list_member(E, R).

%------------------------------------------------------------------------------
% list_flatten(+List_of_ordered_lists, -Union_of_all_these_lists):
%------------------------------------------------------------------------------

list_flatten(List_of_Lists, Union) :-
	list_flatten(List_of_Lists, [], Union).

list_flatten([], Union, Union).

list_flatten([List|More], Union_In, Union_Out) :-
	list_merge(List, Union_In, Union),
	list_flatten(More, Union, Union_Out).

%------------------------------------------------------------------------------
% list_delete_nonmin(+List_of_Ordered_Lists, -List_of_Minimal_Lists):
%------------------------------------------------------------------------------

list_delete_nonmin(Lists, Min_Lists) :-
	list_delete_nonmin(Lists, [], Min_Lists).
	
list_delete_nonmin([], _, []).

list_delete_nonmin([List|More_Lists], Previous_Lists, Result) :-
	list_is_superflous(List, More_Lists),
	!,
	list_delete_nonmin(More_Lists, Previous_Lists, Result).

list_delete_nonmin([List|More_Lists], Previous_Lists, Result) :-
	list_is_superflous(List, Previous_Lists),
	!,
	list_delete_nonmin(More_Lists, Previous_Lists, Result).

list_delete_nonmin([List|More_Lists], Previous_Lists, [List|Result]) :-
	list_delete_nonmin(More_Lists, [List|Previous_Lists], Result).

%------------------------------------------------------------------------------
% list_is_superflous(+Ordered_list, +List_of_lists_containing_nonstrict_subset):
%------------------------------------------------------------------------------

list_is_superflous(List1, [List2|_]) :-
	list_subseteq(List2, List1).

list_is_superflous(List1, [_|More_Lists]) :-
	list_is_superflous(List1, More_Lists).

%------------------------------------------------------------------------------
% list_overlap(+Sorted_list_1, +Sorted_list_2):
%------------------------------------------------------------------------------

list_overlap([Elem|_], [Elem|_]) :-
	!.

list_overlap([Elem1|List1], [Elem2|List2]) :-
	Elem1 @=< Elem2,
	!,
	list_overlap(List1, [Elem2|List2]).

list_overlap([Elem1|List1], [Elem2|List2]) :-
	Elem2 @=< Elem1,
	list_overlap([Elem1|List1], List2).

%==============================================================================
% Portability:
%==============================================================================

%------------------------------------------------------------------------------
% prolog_open(+Filename, +Mode, -Stream):
%------------------------------------------------------------------------------

prolog_open(Filename, read, Stream) :-
	(prolog_use_see(yes) ->
		see(Filename),
		Stream = dummy
	;
		open(Filename, read, Stream)).

%------------------------------------------------------------------------------
% prolog_read(+Stream, -Term):
%------------------------------------------------------------------------------

prolog_read(Stream, Term) :-
	(prolog_use_see(yes) ->
		read(Term)
	;
		read(Stream, Term)).

%------------------------------------------------------------------------------
% prolog_close(+Stream):
%------------------------------------------------------------------------------

prolog_close(Stream) :-
	(prolog_use_see(yes) ->
		seen
	;
		close(Stream)).

%------------------------------------------------------------------------------
% prolog_set_prompt(+New, -Old)
%------------------------------------------------------------------------------

prolog_set_prompt(New, Old) :-
	(prolog_use_prompt(swi) ->
		prompt(Old, New)
	;prolog_use_prompt(eclipse) ->
		get_prompt(input, Old, Out_Stream),
		set_prompt(input, New, Out_Stream)
	;
		true).

%------------------------------------------------------------------------------
% prolog_print_prompt(+Prompt):
%------------------------------------------------------------------------------

prolog_print_prompt(Prompt) :-
	(prolog_use_prompt(write) ->
		write(Prompt)
	;
		true).

