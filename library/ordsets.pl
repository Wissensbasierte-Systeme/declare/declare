/* Copyright(C) 1988, Swedish Institute of Computer Science */

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   File   : ORDSETS.PL							      %
%   Author : Lena Flood							      %
%   Updated: 9 September 1988						      %
%   Purpose: Ordered set manipulation utilities				      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- module(ordsets_2, [
	is_ordset/1,
	list_to_ord_set/2,
	ord_add_element/3,
	ord_del_element/3,
	ord_disjoint/2,
	ord_intersect/2,
	ord_intersection/3,
	ord_intersection/2,
	ord_seteq/2,
	ord_setproduct/3,
	ord_subset/2,
	ord_subtract/3,
	ord_symdiff/3,
	ord_union/3,
	ord_union/2,
	ord_union/4
		   ]).

%   Adapted from shared code written by Richard A O'Keefe. 

%   In this package, sets are represented by ordered lists with no
%   duplicates.	 Thus {c,r,a,f,t} would be [a,c,f,r,t].	 The ordering
%   is defined by the @< family of term comparison predicates, which
%   is the ordering used by sort/2 and setof/3.

%   The benefit of the ordered representation is that the elementary
%   set operations can be done in time proportional to the Sum of the
%   argument sizes rather than their Product.  

/* :- mode		manual: "has no effect at all"
	is_ordset(+),
	    is_ordset(+, ?),
	list_to_ord_set(+, ?),
	ord_add_element(+, +, ?),
	    ord_add_element(+, +, +, +, ?),
	ord_del_element(+, +, ?),
	    ord_del_element(+, +, +, +, ?),
	ord_disjoint(+, +),
	    ord_disjoint(+, +, +, +, +),
	ord_intersect(+, +),
	    ord_intersect(+, +, +, +, +),
	ord_intersection(+, +, ?),
	    ord_intersection(+, +, +, +, +, ?),
	ord_intersection(+, ?),
	    ord_intersection(+, +, +, +, ?),
	ord_seteq(+, +),
	ord_setproduct(+, +, ?),
	    ord_setproduct(+, +, ?, -),
	ord_subset(+, +),
	    ord_subset(+, +, +, +),
	ord_subtract(+, +, ?),
	    ord_subtract(+, +, +, +, +, ?),
	ord_symdiff(+, +, ?),
	    ord_symdiff(+, +, +, +, +, ?),
	ord_union(+, +, ?),
	    ord_union(+, +, +, +, +, ?),
	ord_union(+, ?),
	    ord_union_all(+, +, +, ?),
	ord_union(+, +, ?, ?),
	    ord_union(+, +, +, +, +, ?, ?).
*/

%   is_ordset(+Set)
%   is true when Set is an ordered set.

is_ordset(X) :- var(X), !, fail.
is_ordset([]).
is_ordset([Head|Tail]) :-
	is_ordset(Tail, Head).

is_ordset(X, _) :- var(X), !, fail.
is_ordset([], _).
is_ordset([Head|Tail], Left) :-
	Left @< Head,
	is_ordset(Tail, Head).


%   list_to_ord_set(+List, ?Set)
%   is true when Set is the ordered representation of the set represented
%   by the unordered representation List.  

list_to_ord_set(List, Set) :-
	sort(List, Set).


%   ord_add_element(+Set1, +Element -Set2)
%   is true when Set2 is Set1 with Element inserted in it, preserving
%   the order.

ord_add_element([], Element, [Element]).
ord_add_element([Head|Tail], Element, Set) :-
	compare(Order, Head, Element),
	ord_add_element(Order, Head, Tail, Element, Set).

ord_add_element(<, Head, Tail, Element, [Head|Set]) :-
	ord_add_element(Tail, Element, Set).
ord_add_element(=, Head, Tail, _, [Head|Tail]).
ord_add_element(>, Head, Tail, Element, [Element,Head|Tail]).


%   ord_del_element(+Set1, +Element, ?Set2)
%   is true when Set2 is Set1 but with Element removed.

ord_del_element([], _, []).
ord_del_element([Head|Tail], Element, Set) :-
	compare(Order, Head, Element),
	ord_del_element(Order, Head, Tail, Element, Set).

ord_del_element(<, Head, Tail, Element, [Head|Set]) :-
	ord_del_element(Tail, Element, Set).
ord_del_element(=, _, Tail, _, Tail).
ord_del_element(>, Head, Tail, _, [Head|Tail]).

%   ord_disjoint(+Set1, +Set2)
%   is true when the two ordered sets have no element in common.  


ord_disjoint([], _) :- !.
ord_disjoint(_, []) :- !.
ord_disjoint([Head1|Tail1], [Head2|Tail2]) :-
	compare(Order, Head1, Head2),
	ord_disjoint(Order, Head1, Tail1, Head2, Tail2).

ord_disjoint(<, _, [], _, _) :- !.
ord_disjoint(<, _, [Head1|Tail1], Head2, Tail2) :-
	compare(Order, Head1, Head2),
	ord_disjoint(Order, Head1, Tail1, Head2, Tail2).
ord_disjoint(>, _, _, _, []) :- !.
ord_disjoint(>, Head1, Tail1, _, [Head2|Tail2]) :-
	compare(Order, Head1, Head2),
	ord_disjoint(Order, Head1, Tail1, Head2, Tail2).



%   ord_intersect(+Set1, +Set2)
%   is true when the two ordered sets have at least one element in common.

ord_intersect([Head1|Tail1], [Head2|Tail2]) :-
	compare(Order, Head1, Head2),
	ord_intersect(Order, Head1, Tail1, Head2, Tail2).

ord_intersect(<, _, [Head1|Tail1], Head2, Tail2) :-
	compare(Order, Head1, Head2),
	ord_intersect(Order, Head1, Tail1, Head2, Tail2).
ord_intersect(=, _, _, _, _).
ord_intersect(>, Head1, Tail1, _, [Head2|Tail2]) :-
	compare(Order, Head1, Head2),
	ord_intersect(Order, Head1, Tail1, Head2, Tail2).



%   ord_intersection(+Set1, +Set2, ?Intersection)
%   is true when Intersection is the ordered representation of Set1
%   and Set2, provided that Set1 and Set2 are ordered sets.

ord_intersection([], _, []) :- !.
ord_intersection(_, [], []) :- !.
ord_intersection([Head1|Tail1], [Head2|Tail2], Intersection) :-
	compare(Order, Head1, Head2),
	ord_intersection(Order, Head1, Tail1, Head2, Tail2, Intersection).

ord_intersection(<, _, [], _, _, []) :- !.
ord_intersection(<, _, [Head1|Tail1], Head2, Tail2, Intersection) :-
	compare(Order, Head1, Head2),
	ord_intersection(Order, Head1, Tail1, Head2, Tail2, Intersection).
ord_intersection(=, Head, Tail1, _, Tail2, [Head|Intersection]) :-
	ord_intersection(Tail1, Tail2, Intersection).
ord_intersection(>, _, _, _, [], []) :- !.
ord_intersection(>, Head1, Tail1, _, [Head2|Tail2], Intersection) :-
	compare(Order, Head1, Head2),
	ord_intersection(Order, Head1, Tail1, Head2, Tail2, Intersection).


%   ord_intersection(+Sets, ?Intersection)
%   is true when Intersection is the ordered set representation of the
%   intersection of all the sets in Sets.

ord_intersection([], Intersection) :- !, Intersection = [].
ord_intersection(Sets, Intersection) :- 
	length(Sets, NumberOfSets),
	ord_intersection(NumberOfSets, Sets, Intersection, []).

ord_intersection(1, [Set|Sets], Set, Sets) :- !.
ord_intersection(2, [Set,Set2|Sets], Intersection, Sets) :- !,
	ord_intersection(Set, Set2, Intersection).
ord_intersection(N, Sets0, Intersection, Sets) :-
	A is N>>1,
	Z is N-A,
	ord_intersection(A, Sets0, X, Sets1),
	ord_intersection(Z, Sets1, Y, Sets),
	ord_intersection(X, Y, Intersection).

%   ord_seteq(+Set1, +Set2)
%   is true when the two arguments represent the same set.  Since they
%   are assumed to be ordered representations, they must be identical.


ord_seteq(Set1, Set2) :-
	Set1 == Set2.


%   ord_setproduct(+Set1, +Set2, ?SetProduct)
%   is true when SetProduct is the cartesian product of Set1 and Set2. The
%   product is represented as pairs Elem1-Elem2, where Elem1 is an element
%   from Set1 and Elem2 is an element from Set2.

ord_setproduct([], _, []).
ord_setproduct([Head|Tail], Set, SetProduct)  :-
	ord_setproduct(Set, Head, SetProduct, Rest),
	ord_setproduct(Tail, Set, Rest).

ord_setproduct([], _, Set, Set).
ord_setproduct([Head|Tail], X, [X-Head|TailX], Tl) :-
	ord_setproduct(Tail, X, TailX, Tl).


%   ord_subset(+Set1, +Set2)
%   is true when every element of the ordered set Set1 appears in the
%   ordered set Set2.

ord_subset([], _).
ord_subset([Head1|Tail1], [Head2|Tail2]) :-
	compare(Order, Head1, Head2),
	ord_subset(Order, Head1, Tail1, Tail2).

ord_subset(=, _, Tail1, Tail2) :-
	ord_subset(Tail1, Tail2).
ord_subset(>, Head1, Tail1, [Head2|Tail2]) :-
	compare(Order, Head1, Head2),
	ord_subset(Order, Head1, Tail1, Tail2).



%   ord_subtract(+Set1, +Set2, ?Difference)
%   is true when Difference contains all and only the elements of Set1
%   which are not also in Set2.


ord_subtract([], _, []) :- !.
ord_subtract(Set1, [], Set1) :- !.
ord_subtract([Head1|Tail1], [Head2|Tail2], Difference) :-
	compare(Order, Head1, Head2),
	ord_subtract(Order, Head1, Tail1, Head2, Tail2, Difference).

ord_subtract(<, Head1, [], _, _, [Head1]) :- !.
ord_subtract(<, Head0, [Head1|Tail1], Head2, Tail2, [Head0|Difference]) :-
	compare(Order, Head1, Head2),
	ord_subtract(Order, Head1, Tail1, Head2, Tail2, Difference).
ord_subtract(=, _, Tail1, _, Tail2, Difference) :-
	ord_subtract(Tail1, Tail2, Difference).
ord_subtract(>, Head1, Tail1, _, [], [Head1|Tail1]) :- !.
ord_subtract(>, Head1, Tail1, _, [Head2|Tail2], Difference) :-
	compare(Order, Head1, Head2),
	ord_subtract(Order, Head1, Tail1, Head2, Tail2, Difference).


%   ord_symdiff(+Set1, +Set2, ?Difference)
%   is true when Difference is the symmetric difference of Set1 and Set2.

ord_symdiff([], Set2, Set2) :- !.
ord_symdiff(Set1, [], Set1) :- !.
ord_symdiff([Head1|Tail1], [Head2|Tail2], Difference) :-
	compare(Order, Head1, Head2),
	ord_symdiff(Order, Head1, Tail1, Head2, Tail2, Difference).

ord_symdiff(<, Head0, [], Head2, Tail2, [Head0,Head2|Tail2]) :- !.
ord_symdiff(<, Head0, [Head1|Tail1], Head2, Tail2, [Head0|Difference]) :-
	compare(Order, Head1, Head2),
	ord_symdiff(Order, Head1, Tail1, Head2, Tail2, Difference).
ord_symdiff(=, _,     Tail1, _,	    Tail2, Difference) :-
	ord_symdiff(Tail1, Tail2, Difference).
ord_symdiff(>, Head1, Tail1, Head0, [], [Head0,Head1|Tail1]) :- !.
ord_symdiff(>, Head1, Tail1, Head0, [Head2|Tail2], [Head0|Difference]) :-
	compare(Order, Head1, Head2),
	ord_symdiff(Order, Head1, Tail1, Head2, Tail2, Difference).



%   ord_union(+Set1, +Set2, ?Union)
%   is true when Union is the union of Set1 and Set2.  Note that when
%   something occurs in both sets, we want to retain only one copy.

ord_union([], Set2, Set2) :- !.
ord_union(Set1, [], Set1) :- !.
ord_union([Head1|Tail1], [Head2|Tail2], Union) :-
	compare(Order, Head1, Head2),
	ord_union(Order, Head1, Tail1, Head2, Tail2, Union).

ord_union(<, Head0, [], Head2, Tail2, [Head0,Head2|Tail2]) :- !.
ord_union(<, Head0, [Head1|Tail1], Head2, Tail2, [Head0|Union]) :-
	compare(Order, Head1, Head2),
	ord_union(Order, Head1, Tail1, Head2, Tail2, Union).
ord_union(=, Head,  Tail1, _,	  Tail2, [Head|Union]) :-
	ord_union(Tail1, Tail2, Union).
ord_union(>, Head1, Tail1, Head0, [], [Head0,Head1|Tail1]) :- !.
ord_union(>, Head1, Tail1, Head0, [Head2|Tail2], [Head0|Union]) :-
	compare(Order, Head1, Head2),
	ord_union(Order, Head1, Tail1, Head2, Tail2, Union).


%   ord_union(+Sets, ?Union) 
%   is true when Union is the union of all the sets in Sets. 

ord_union([], Union) :- !, Union = [].
ord_union(Sets, Union) :-
	length(Sets, NumberOfSets),
	ord_union_all(NumberOfSets, Sets, Union, []).

ord_union_all(1, [Set|Sets], Set, Sets) :- !.
ord_union_all(2, [Set,Set2|Sets], Union, Sets) :- !,
	ord_union(Set, Set2, Union).
ord_union_all(N, Sets0, Union, Sets) :-
	A is N>>1,
	Z is N-A,
	ord_union_all(A, Sets0, X, Sets1),
	ord_union_all(Z, Sets1, Y, Sets),
	ord_union(X, Y, Union).



%   ord_union(+Set1, +Set2, ?Union, ?New)
%   is true when Union is the union of Set1 and Set2, and New is
%   the difference between Set2 and Set1.  This is useful if you
%   are accumulating members of a set and you want to process new
%   elements as they are added to the set.

ord_union([], Set, Set, Set) :- !.
ord_union(Set, [], Set, []) :- !.
ord_union([O|Os], [N|Ns], Set, New) :-
	compare(C, O, N), 
	ord_union(C, O, Os, N, Ns, Set, New).
	
ord_union(<, O, [], N, Ns, [O,N|Ns], [N|Ns]) :- !.
ord_union(<, O1, [O|Os], N, Ns, [O1|Set], New) :-
	compare(C, O, N), 
	ord_union(C, O, Os, N, Ns, Set, New).
ord_union(=, _, Os, N, Ns, [N|Set], New) :-
	ord_union(Os, Ns, Set, New).
ord_union(>, O, Os, N, [], [N,O|Os], [N]) :- !.
ord_union(>, O, Os, N1, [N|Ns], [N1|Set], [N1|New]) :-
	compare(C, O, N), 
	ord_union(C, O, Os, N, Ns, Set, New).
