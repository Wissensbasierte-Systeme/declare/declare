/* 
 * $Id: module_test.pl,v 1.6 2001/03/22 14:32:13 faulstic Exp faulstic $
 *
 * Description: Utilities for module test suites
 * 
 Test suites are encoded in the same module as the predicate to be tested 
 using the predicate test/5 in the form
 	test(Functor/Arity, TestID, Goal, ResultTemplate, Results).

 the test predicates provided by this module succeed if failures have
 occurred and fail if all tests have succeeded.

 * (c) Lukas Faulstich 2000 <faulstic@inf.fu-berlin.de> 
 *
 * This software is covered by the GNU General Public License (GPL)
 * <http://www.gnu.org/copyleft/gpl.html>
 *
 * $Log: module_test.pl,v $
 * Revision 1.6  2001/03/22 14:32:13  faulstic
 * report_module always succeeds now.
 *
 * Revision 1.5  2000/09/23 18:51:12  faulstic
 * + tail/3
 *
 * Revision 1.4  2000/05/02 08:30:49  faulstic
 * + report_module now prints all tests, fails if the whole test suite passes
 * + report_case now fails if the test succeeds
 *
 * Revision 1.3  2000/04/27 17:26:37  faulstic
 * + test_module/1
 * + report_module/2
 *
 * Revision 1.2  2000/04/27 12:35:13  faulstic
 * + GPL
 *
 *
 * Revision 1.1  2000/04/27 11:56:48  faulstic
 * Initial revision
 *
 */

:- module(module_test, [
	failures/1, 
	failures/2,
	report_module/2, 
	predicate_failures/3,
	case_failures/3, 
	report_case/3
    ]). 

% test(Functor/Arity, TestID, Goal, Template, Results). 
% 	the test is run by executing Goal using findall and collecting 
% 	Template in a list of results. 
%	Results specifies the expected results. 

:- discontiguous test/5.

% example tests of some popular predicates
test(member/2, test1, member(X, [a,b,c]), X, [a,b,c]). 
test(member/2, expected_failure1, member(X, [a,b,c]), X, [a,x,c,d]). 
test(append/3, test1, append(X, Y, [a,b,c]), X:Y, [
	[]:[a,b,c],
	[a]:[b,c], 
	[a,b]:[c],
	[a,b,c]:[]
]). 
test(append/3, expected_failure1, append(X, [d], [a,b,c]), X, [[a,b]]). 



% failures(+Module) run test suite for Module. 
% Succeeds iff there have been any failures. 

failures(Module):- 
	failures(Module, _).

% DO NOT USE THIS TEST: INFINITE RECURSION!!!!
% test(failures/1, test1, failures(module_test), true, [true]). 

	
% failures(+Module, ?Failures) run test suite for Module.
% Succeeds iff there have been any failures. 
% Failures is a list of elements Predicate/Arity:TestID

failures(Module, Failures):- 
	bagof(Predicate:TestID, case_failures(Module,Predicate,TestID),Failures). 

% DO NOT USE THIS TEST: INFINITE RECURSION!!!!
% test(failures/2, test1, failures(module_test, Failures), Failures, [
% 	[member/2:expected_failure1]
%     ]). 
% test(predicate_failures/3, test1, 
% 	predicate_failures(module_test, predicate_failures/3, Failures), Failures, [[]]). 

% report_module(+Module, ?Failures) run test suite for Module.
% Failures is a list of elements Predicate/Arity:TestID
% Succeeds in any case. 

report_module(Module, Failures):- 
	findall(Predicate:TestID,
		(report_case(Module,Predicate,TestID),nl),
		Failures), 
	(Failures = [] -> 
	    format('~n~nPASS: Module ~w successfully tested.~n~n', [Module])
	;
	    format('~n~nFAIL: Module ~w failed the tests ~w.~n~n', 
	    	[Module, Failures])
	).    




% predicate_failures(+Module, ?Functor/Arity, ?Failures) run tests for all 
% predicates Functor/Arity in Module. 
% Succeeds iff there have been any failures. 
% Failures is the list of IDs of the failed tests. 

predicate_failures(Module, Predicate, Failures):- 
	bagof(TestID,Predicate^case_failures(Module,Predicate,TestID),Failures). 

test(predicate_failures/3, test1, 
	predicate_failures(module_test, member/2, Failures), Failures, [[expected_failure1]]). 
test(predicate_failures/3, test2, 
	predicate_failures(module_test, append/3, Failures), Failures, [[expected_failure1]]). 
% DO NOT USE THIS TEST: INFINITE RECURSION!!!!
% test(predicate_failures/3, test3, 
% 	predicate_failures(module_test, predicate_failures/3, Failures), Failures, [[]]). 



% case_failures(+Module, ?Functor/Arity, ?TestID) succeeds iff 
% the test TestID for a predicate Functor/Arity in Module fails. 

case_failures(Module, Predicate, TestID):- 
	case_failures(Module, Predicate, TestID, Results, SpecifiedResults), 
	Results \= SpecifiedResults. 

test(case_failures/3, test3, 
 	case_failures(module_test, member/2, Failure), Failure, [expected_failure1]). 
test(case_failures/3, test3, 
 	case_failures(module_test, member/2, test1), fail, []). 


% report_case(+Module, ?Functor/Arity, ?TestID) 
% reports the expected and the actual results of test TestID  
% for a predicate Functor/Arity in Module.


report_case(Module, Predicate, TestID):- 
	case_failures(Module, Predicate, TestID, Results, SpecifiedResults), 
	report_results(Module, Predicate, TestID, SpecifiedResults, Results). 



case_failures(Module, Predicate, TestID, Results, SpecifiedResults):- 
	Module:test(Predicate, TestID, Goal, Template, SpecifiedResults),
	findall(Template,Module:Goal,Results).  

report_results(Module, Pred, TestID, Specs, Results):- 
	Specs \= Results -> 
		format('FAIL: test ~w of ~w:~w : 
results
\t~w
should be 
\t~w ~n~n',
 		[TestID, Module, Pred, Results, Specs])
	    ;
	    	format('pass: test ~w of ~w:~w : 
results
\t~w
as expected.~n~n',
 		[TestID, Module, Pred, Results]), 
		fail. 


