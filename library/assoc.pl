%   Copyright(C) 1993, Swedish Institute of Computer Science
%   Adapted from shared code written by Richard A. O'Keefe

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   File   : ASSOC.PL                                                  %
%   Author : R.A.O'Keefe                                               %
%   Updated: 9 November 1983                                           %
%   Purpose: Binary tree implementation of "association lists".        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- module(assoc, [
	assoc_to_list/2,
	gen_assoc/3,
	get_assoc/3,
	get_assoc/5,
	list_to_assoc/2,
	map_assoc/3,
	ord_list_to_assoc/2,
	put_assoc/4
   ]).

%   :- meta_predicate
%   	map_assoc(:, ?, ?).



%   gen_assoc(?Key, +Assoc, ?Value)
%   assumes that Assoc is a proper "assoc" tree, and is true when
%   Key is associated with Value in Assoc.  Can be used to enumerate
%   all Values by ascending Keys.

gen_assoc(Key, t(_,_,L,_), Val) :- gen_assoc(Key, L, Val).
gen_assoc(Key, t(Key,Val,_,_), Val).
gen_assoc(Key, t(_,_,_,R), Val) :- gen_assoc(Key, R, Val).



%   get_assoc(+Key, +Assoc, ?Value)
%   assumes that Assoc is a proper "assoc" tree.  It is true when
%   Key is identical to (==) one of the keys in Assoc, and Value
%   unifies with the associated value.

get_assoc(Key, t(K,V,L,R), Val) :-
	compare(Rel, Key, K),
	get_assoc(Rel, Key, V, L, R, Val).

get_assoc(=, _, Val, _, _, Val).
get_assoc(<, Key, _, Tree, _, Val) :- get_assoc(Key, Tree, Val).
get_assoc(>, Key, _, _, Tree, Val) :- get_assoc(Key, Tree, Val).



%   get_assoc(+Key, +OldAssoc, ?OldValue, ?NewAssoc, ?NewValue)
%   is true when OldAssoc and NewAssoc are "assoc" trees of the same
%   shape having the same elements except that the value for Key in
%   OldAssoc is OldValue and the value for Key in NewAssoc is NewValue.

get_assoc(Key, t(K0,V0,L0,R0), Val0, t(K,V,L,R), Val) :-
	compare(Rel, Key, K0),
	get_assoc(Rel, Key, K0, V0, L0, R0, Val0, K, V, L, R, Val).

get_assoc(=, _, K, Val0, L, R, Val0, K, Val, L, R, Val).
get_assoc(<, Key, K, V, Tree0, R, Val0, K, V, Tree, R, Val) :-
	get_assoc(Key, Tree0, Val0, Tree, Val).
get_assoc(>, Key, K, V, L, Tree0, Val0, K, V, L, Tree, Val) :-
	get_assoc(Key, Tree0, Val0, Tree, Val).



%   assoc_to_list(+Assoc, ?List)
%   assumes that Assoc is a proper "assoc" tree, and is true when
%   List is a list of Key-Value pairs in ascending order with no
%   duplicate Keys specifying the same finite function as Assoc.
%   Use this to convert an assoc to a list.

assoc_to_list(Assoc, List) :-
	assoc_to_list(Assoc, List, []).

assoc_to_list(t) --> [].
assoc_to_list(t(Key,Val,L,R)) -->
	assoc_to_list(L),
	[Key-Val],
	assoc_to_list(R).



%   list_to_assoc(+List, ?Assoc)
%   is true when List is a proper list of Key-Val pairs (in any order)
%   and Assoc is an association tree specifying the same finite function
%   from Keys to Values.

list_to_assoc(List, Assoc) :-
	keysort(List, Keys),
	length(Keys, N),
	list_to_assoc(N, Keys, Assoc, []).

list_to_assoc(0, List, t, List) :- !.
list_to_assoc(N, List, t(Key,Val,L,R), Rest) :-
	A is (N-1) >> 1,
	Z is (N-1)-A,
	list_to_assoc(A, List, L, [Key-Val|More]),
	list_to_assoc(Z, More, R, Rest).



%   ord_list_to_assoc(+List, ?Assoc)
%   is true when List is a proper list of Key-Val pairs (keysorted)
%   and Assoc is an association tree specifying the same finite function
%   from Keys to Values.

ord_list_to_assoc(List, Assoc) :-
	length(List, N),
	list_to_assoc(N, List, Assoc, []).



%   map_assoc(:Pred, ?OldAssoc, ?NewAssoc)
%   is true when OldAssoc and NewAssoc are association trees of the
%   same shape, and for each Key, if Key is associated with Old in
%   OldAssoc and with New in NewAssoc, Pred(Old,New) is true.

map_assoc(MPred, OldAssoc, NewAssoc) :-
	prolog:get_module(MPred, Pred, M),
	map_assoc(OldAssoc, NewAssoc, M, Pred).

map_assoc(t, t, _, _).
map_assoc(t(Key,Old,L0,R0), t(Key,New,L1,R1), M, Pred) :-
	map_assoc(L0, L1, M, Pred),
	prolog:dcg_translate_dcg_atom(Pred, Goal, Old, New),
	prolog:call_module(Goal, M),
	map_assoc(R0, R1, M, Pred).



%   put_assoc(+Key, +OldAssoc, +Val, -NewAssoc)
%   is true when OldAssoc and NewAssoc define the same finite function,
%   except that NewAssoc associates Val with Key.  OldAssoc need not
%   have associated any value at all with Key.

put_assoc(Key, OldAssoc, Val, NewAssoc) :-
	put_assoc1(OldAssoc, Key, Val, NewAssoc).

put_assoc1(t, Key, Val, t(Key,Val,t,t)).
put_assoc1(t(K,V,L,R), Key, Val, New) :-
	compare(Rel, Key, K),
	put_assoc1(Rel, Key, Val, New, K, V, L, R).

put_assoc1(=, Key, Val, t(Key,Val,L,R), _, _, L, R).
put_assoc1(<, Key, Val, t(K,V,Tree,R), K, V, L, R) :-
	put_assoc1(L, Key, Val, Tree).
put_assoc1(>, Key, Val, t(K,V,L,Tree), K, V, L, R) :-
	put_assoc1(R, Key, Val, Tree).
