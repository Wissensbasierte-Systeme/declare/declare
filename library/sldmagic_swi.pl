%==============================================================================
% Project:	An Improved Magic Set Technique
% Version:	0.1
% Module:	sldmagic.pl
% Purpose:	Computation of Nodetypes and Rewritten Program
% Created:	05.03.1996
% Last Change:	15.03.1996
% Language:	Prolog (ECLiPSe 3.5.1, SWI-Prolog 2.5.0, Quintus-Prolog 3.2)
% Author:	Stefan Brass
% Email:	sb@informatik.uni-hannover.de
% Address:	Universitaet Hannover, Lange Laube 22, 30159 Hannover, Germany
% Copyright:	(C) 1996  Stefan Brass
% Copying:	Permitted under the GNU General Public Licence.
%==============================================================================

%------------------------------------------------------------------------------
%    I wrote this program in a hurry, so:
%    - It probably contains still a number of bugs
%      (I am interested to hear about them if you find them).
%    - No attempt was made to use more efficient data structures.
%    - The programming style and comments are not optimal.
%    I hope that I can improve this program later.
%    Please send me an email if you want to here about future versions.
%
%    This program is free software; you can redistribute it and/or
%    modify it under the terms of the GNU General Public License
%    as published by the Free Software Foundation; either version 2
%    of the License, or (at your option) any later version.
%    
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%    
%    You should have received a copy of the GNU General Public License
%    along with this program; if not, write to the Free Software
%    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
%------------------------------------------------------------------------------

sldmagic(Filename) :-
	clear,
	load_program(Filename),
	rewrite_loop,
	nl,
	print_all_queries,
	print_all_nodes,
	print_all_answers,
	print_all_sld_rules.

% Uncomment this for ECLiPSe:
% retract_all(Head) :- retractall(Head).

% This is for ECLiPSe and SWI-Prolog:
% write_quote :- write('\'').
% This is for Quintus-Prolog (also understood by SWI-Prolog):
write_quote :- write('''').

%==============================================================================
% Dynamic Database:
%==============================================================================

%------------------------------------------------------------------------------
% List of Option Values:
%------------------------------------------------------------------------------

:- dynamic option/2.

%------------------------------------------------------------------------------
% List of EDB-Predicates:
%------------------------------------------------------------------------------

:- dynamic edb_pred/2.

%------------------------------------------------------------------------------
% List of IDB-Predicates:
%------------------------------------------------------------------------------

:- dynamic idb_pred/2.

%------------------------------------------------------------------------------
% List of Program Rules:
%------------------------------------------------------------------------------

:- dynamic rule/2.

%------------------------------------------------------------------------------
% List of Query Patterns:
%------------------------------------------------------------------------------

:- dynamic query/1.

%------------------------------------------------------------------------------
% List of Node Patterns:
%------------------------------------------------------------------------------

:- dynamic node/2.

%------------------------------------------------------------------------------
% List of Answer Patterns:
%------------------------------------------------------------------------------

:- dynamic answer/1.

%------------------------------------------------------------------------------
% List of Rewritten Rules:
%------------------------------------------------------------------------------

:- dynamic sld/2.

%------------------------------------------------------------------------------
% clear:				(clear dynamic database)
%------------------------------------------------------------------------------

clear :-
	retractall(option(_,_)),
	retractall(edb_pred(_,_)),
	retractall(idb_pred(_,_)),
	retractall(rule(_,_)),
	retractall(query(_)),
	retractall(node(_,_)),
	retractall(answer(_)),
	retractall(sld(_,_)).

%------------------------------------------------------------------------------
% save_option(+Option, +Value):
%------------------------------------------------------------------------------

save_option(Option, Value) :-
	option(Option, Defined_Value),
	!,
	Value = Defined_Value.

save_option(Option, Value) :-
	assert(option(Option, Value)).

%------------------------------------------------------------------------------
% use_option(+Option, +Value):
%------------------------------------------------------------------------------

use_option(Option, Value) :-
	option(Option, Defined_Value),
	!,
	Value = Defined_Value.

use_option(Option, Value) :-
	save_option(Option, no),
	Value = no.

%------------------------------------------------------------------------------
% save_edb(+Pred, +Arity):
%------------------------------------------------------------------------------

save_edb(Pred, Arity) :-
	edb_pred(Pred, Arity),
	!.

save_edb(Pred, Arity) :-
	\+ idb_pred(Pred, Arity),
	assert(edb_pred(Pred, Arity)).

%------------------------------------------------------------------------------
% save_pred(+Pred, +Arity):
%------------------------------------------------------------------------------

save_pred(Pred, Arity) :-
	idb_pred(Pred, Arity),
	!.

save_pred(Pred, Arity) :-
	edb_pred(Pred, Arity),
	!.

save_pred(Pred, Arity) :-
	assert(idb_pred(Pred, Arity)).

%------------------------------------------------------------------------------
% save_rule(+Internal_Head, +Internal_Body):
%------------------------------------------------------------------------------

save_rule(Internal_Head, Internal_Body) :-
	assert(rule(Internal_Head, Internal_Body)).

%------------------------------------------------------------------------------
% save_query(+Query_Pattern):
%------------------------------------------------------------------------------

save_query(Query_Pattern) :-
	query(Query_Pattern),
	!.

save_query(Query_Pattern) :-
	assert(query(Query_Pattern)).

%------------------------------------------------------------------------------
% save_node(+Query_Pattern, +Goal_Pattern):
%------------------------------------------------------------------------------

save_node(Query_Pattern, Goal_Pattern) :-
	node(Query_Pattern, Goal_Pattern),
	!.

save_node(Query_Pattern, Goal_Pattern) :-
	assert(node(Query_Pattern, Goal_Pattern)).

%------------------------------------------------------------------------------
% save_answer(+Answer_Pattern):
%------------------------------------------------------------------------------

save_answer(Answer_Pattern) :-
	answer(Answer_Pattern),
	!.

save_answer(Answer_Pattern) :-
	assert(answer(Answer_Pattern)).

%------------------------------------------------------------------------------
% save_sld(+Head, Body):
%------------------------------------------------------------------------------

save_sld(Head, Body) :-
	sld(Head, Body),
	!.

save_sld(Head, Body) :-
	assert(sld(Head, Body)).

%==============================================================================
% Parser for Input Programs:
%==============================================================================

%------------------------------------------------------------------------------
% Operator for Marking "Subproof"-Literals:
%------------------------------------------------------------------------------

:- op(900, fy, &).

%------------------------------------------------------------------------------
% Operator for Defining Database Literals:
%------------------------------------------------------------------------------

:- op(1000, fy, db).

%------------------------------------------------------------------------------
% Operators for Defining Options:
%------------------------------------------------------------------------------

:- op(1000, fy, use_query_const).
:- op(1000, fy, derive_idb_literals).

%------------------------------------------------------------------------------
% load_program(+Filename):		(fails on Syntax Error)
%------------------------------------------------------------------------------

load_program(Filename) :-
	open(Filename, read, In_Stream),
	(read_rules(In_Stream) ->
		close(In_Stream)
	;
		close(In_Stream),
		fail).

%------------------------------------------------------------------------------
% read_rules(+In_Stream):		(fails on Syntax Error)
%------------------------------------------------------------------------------

read_rules(In_Stream) :-
	read(In_Stream, Line),
	(Line == end_of_file ->
		true
	;
		process_line(Line),
		!,
		read_rules(In_Stream)).

%------------------------------------------------------------------------------
% process_line(+Line):			(fails upon syntax error)
%------------------------------------------------------------------------------

process_line(db(Pred/Arity)) :-
	save_edb(Pred, Arity),
	!.

process_line(use_query_const(Yes_Or_No)) :-
	parse_yes_or_no(Yes_Or_No),
	save_option(use_query_const, Yes_Or_No).

process_line(derive_idb_literals(Yes_Or_No)) :-
	parse_yes_or_no(Yes_Or_No),
	save_option(derive_idb_literals, Yes_Or_No).

process_line((Head :- Body)) :-
	parse_head(Head, Internal_Head),
	parse_body(Body, Internal_Body),
	rep_vars_body(Internal_Body),
	check_safety(Internal_Head),
	!,
	save_rule(Internal_Head, Internal_Body).

process_line((:- Query)) :-
	parse_query(Query, Query_Pattern),
	save_query(Query_Pattern).

process_line(Line) :-
	write('Error in Line: '),
	write(Line),
	write('.'),
	nl,
	fail.

%------------------------------------------------------------------------------
% parse_yes_or_no(+Yes_Or_No):
%------------------------------------------------------------------------------

parse_yes_or_no(yes).

parse_yes_or_no(no).

%------------------------------------------------------------------------------
% parse_head(+Head, -Internal_Head):
%------------------------------------------------------------------------------

parse_head(Head, Head) :-
	functor(Head, Pred, Arity),
	\+ edb_pred(Pred, Arity),
	save_pred(Pred, Arity),
	Head =.. [Pred|Args],
	parse_args(Args).

%------------------------------------------------------------------------------
% parse_body(+Body, -Internal_Body):
%------------------------------------------------------------------------------

parse_body(','(Atom,Body), [Internal_Atom|Internal_Body]) :-
	!,
	parse_atom(Atom, Internal_Atom),
	parse_body(Body, Internal_Body).

parse_body(Atom, [Internal_Atom]) :-
	parse_atom(Atom, Internal_Atom).

%------------------------------------------------------------------------------
% parse_atom(+Atom, -Internal_Atom):
%------------------------------------------------------------------------------

parse_atom(&(Atom), sub(Atom)) :-
	!,
	functor(Atom, Pred, Arity),
	\+ edb_pred(Pred, Arity),
	save_pred(Pred, Arity),
	Atom =.. [Pred|Args],
	parse_args(Args).

parse_atom(Atom, edb(Atom)) :-
	functor(Atom, Pred, Arity),
	edb_pred(Pred, Arity),
	!,
	Atom =.. [Pred|Args],
	parse_args(Args).

parse_atom(Atom, cont(Atom)) :-
	functor(Atom, Pred, Arity),
	\+ edb_pred(Pred, Arity),
	save_pred(Pred, Arity),
	Atom =.. [Pred|Args],
	parse_args(Args).

%------------------------------------------------------------------------------
% parse_args(+Args):
%------------------------------------------------------------------------------

parse_args([]).

parse_args([Arg|More]) :-
	parse_arg(Arg),
	parse_args(More).

%------------------------------------------------------------------------------
% parse_arg(?Arg):
%------------------------------------------------------------------------------

parse_arg(Arg) :-
	atomic(Arg).

parse_arg(Arg) :-
	var(Arg).

%------------------------------------------------------------------------------
% rep_vars_body(+List_of_Atoms):
%------------------------------------------------------------------------------

rep_vars_body(List_of_Atoms) :-
	rep_vars_body(List_of_Atoms, 0).

rep_vars_body([], _).

rep_vars_body([Atom|Body], Var_No) :-
	rep_vars_atom(Atom, Var_No, Next_Var_No),
	rep_vars_body(Body, Next_Var_No).

%------------------------------------------------------------------------------
% rep_vars_atom(+Atom, +Var_No, -Next_Var_No):
%------------------------------------------------------------------------------

rep_vars_atom(cont(Atom), Var_No, Next_Var_No) :-
	!,
	Atom =.. [_|Args],
	rep_vars_args(Args, Var_No, Next_Var_No).

rep_vars_atom(sub(Atom), Var_No, Next_Var_No) :-
	!,
	Atom =.. [_|Args],
	rep_vars_args(Args, Var_No, Next_Var_No).

rep_vars_atom(edb(Atom), Var_No, Next_Var_No) :-
	Atom =.. [_|Args],
	rep_vars_args(Args, Var_No, Next_Var_No).

%------------------------------------------------------------------------------
% rep_vars_args(+Args, +Var_No, -Next_Var_No):
%------------------------------------------------------------------------------

rep_vars_args([], Var_No, Var_No).

rep_vars_args([Arg|Args], Var_No_0, Var_No_2) :-
	rep_vars_arg(Arg, Var_No_0, Var_No_1),
	rep_vars_args(Args, Var_No_1, Var_No_2).

%------------------------------------------------------------------------------
% rep_vars_arg(+Arg, +Var_No, -Next_Var_No):
%------------------------------------------------------------------------------

rep_vars_arg(Arg, Var_No, Next_Var_No) :-
	var(Arg),
	Arg = var(Var_No),
	Next_Var_No is Var_No + 1.

rep_vars_arg(Arg, Var_No, Var_No) :-
	atomic(Arg).

rep_vars_arg(var(No), Var_No, Var_No) :-
	number(No).

%------------------------------------------------------------------------------
% check_safety(+Head):
%------------------------------------------------------------------------------

check_safety(Head) :-
	Head =.. [_|Args],
	check_safety_args(Args).

%------------------------------------------------------------------------------
% check_safety_args(+Args):
%------------------------------------------------------------------------------

check_safety_args([]).

check_safety_args([Arg|Args]) :-
	check_safety_arg(Arg),
	check_safety_args(Args).

%------------------------------------------------------------------------------
% check_safety_arg(+Arg):
%------------------------------------------------------------------------------

check_safety_arg(Arg) :-
	atomic(Arg).

check_safety_arg(Arg) :-
	\+ var(Arg),
	Arg = var(No),
	number(No).

%------------------------------------------------------------------------------
% parse_query(+Query, -Query_Pattern):
%------------------------------------------------------------------------------

parse_query(Query, Query_Pattern) :-
	use_option(use_query_const, no),
	functor(Query, Pred, Arity),
	\+edb_pred(Pred, Arity),
	save_pred(Pred, Arity),
	Query =.. [Pred|Args],
	parse_query_args(Args, Args_Pattern, Args_Params),
	Query_Pattern =.. [Pred|Args_Pattern],
	save_sld(query_atom(Query_Pattern,Args_Params), []).

parse_query(Query, Query) :-
	use_option(use_query_const, yes),
	functor(Query, Pred, Arity),
	\+edb_pred(Pred, Arity),
	save_pred(Pred, Arity),
	Query =.. [Pred|Args],
	parse_args(Args),
	rep_vars_args(Args, 0, _),
	save_sld(query_atom(Query,[]), []).

%------------------------------------------------------------------------------
% parse_query_args(+Args, -Args_Pattern, -Args_Params):
%------------------------------------------------------------------------------

parse_query_args(Args, Args_Pattern, Args_Params) :-
	parse_query_args(Args, Args_Pattern, Args_Params, 0, 0).

parse_query_args([], [], [], _, _).

parse_query_args([Arg|Args], [Arg|Args_Pattern], Args_Params, Var_No,Par_No) :-
	var(Arg),
	Arg = var(Var_No),
	!,
	Next_Var_No is Var_No + 1,
	parse_query_args(Args, Args_Pattern, Args_Params, Next_Var_No, Par_No).

parse_query_args([Arg|Args], [par(Par_No)|Args_Pattern], [Arg|Args_Params],
		Var_No, Par_No) :-
	atomic(Arg),
	!,
	Next_Par_No is Par_No + 1,
	parse_query_args(Args, Args_Pattern, Args_Params, Var_No, Next_Par_No).

%==============================================================================
% Computation of Rewritten Program:
%==============================================================================

%------------------------------------------------------------------------------
% rewrite_loop:
%------------------------------------------------------------------------------

rewrite_loop :-
	rewrite_step,
	!,
	rewrite_loop.

rewrite_loop.

%------------------------------------------------------------------------------
% rewrite_step:
%------------------------------------------------------------------------------

rewrite_step :-
	sld_rule(Head, Body),
	\+ sld(Head, Body),
	save_sld(Head, Body),
	save_head_pred(Head).

%------------------------------------------------------------------------------
% save_head_pred(+Head):
%------------------------------------------------------------------------------

save_head_pred(node_atom(Query,Goal,_)) :-
	save_node(Query, Goal).

save_head_pred(query_atom(Query,_)) :-
	save_query(Query).

save_head_pred(answer_atom(Answer,_)) :-
	save_answer(Answer).

save_head_pred(db_atom(_)) :-
	write('*** THIS SHOULD NEVER HAPPEN').

%------------------------------------------------------------------------------
% sld_rule(-Head, -Body_Lit):
%------------------------------------------------------------------------------

% Meta-Interpreter Rule #1:
%	node(Query, Body) :-
%		query(Query),
%		rule(Query, Body).

sld_rule(node_atom(Norm_Query,Norm_Body,Norm_Result_Params),
		[query_atom(Query,Norm_Query_Params)]) :-
	query(Query),
	atom_params(Query, Query_Params),
	maxno_atom(Query, Max_Var, Max_Par),
	fresh_rule(Max_Var, Max_Par, Head, Body),
	unify(Query, Head, Unifier),
	apply_subst_atom(Query, Unifier, Uni_Query),
	apply_subst_goal(Body, Unifier, Uni_Body),
	apply_subst_args(Query_Params, Unifier, Uni_Query_Params),
	node_params(Uni_Query, Uni_Body, Result_Params),
	normalize_node(Uni_Query, Uni_Body, Norm_Query, Norm_Body),
	normalize_params(Uni_Query_Params, Result_Params, [],
		Norm_Query_Params, Norm_Result_Params, _).


% Meta-Interpreter Rule #2:
%	node(Query, Child) :-
%		node(Query, [cont(Lit)|Rest]),
%		rule(Lit, Body),
%		append(Body, Rest, Child).

sld_rule(node_atom(Norm_Query,Norm_Child,Norm_Head_Params),
		[node_atom(Query,[cont(Lit1)|Rest],Norm_Subst_Params)]) :-
	node(Query, [cont(Lit1)|Rest]),
	node_params(Query, [cont(Lit1)|Rest], Params),
	maxno_node(Query, [cont(Lit1)|Rest], Max_Var, Max_Par),
	fresh_rule(Max_Var, Max_Par, Lit2, Body),
	unify(Lit1, Lit2, Subst),
	list_append(Body, Rest, Child),
	apply_subst_goal(Child, Subst, Subst_Child),
	apply_subst_atom(Query, Subst, Subst_Query),
	apply_subst_args(Params, Subst, Subst_Params),
	node_params(Subst_Query, Subst_Child, Head_Params),
	normalize_node(Subst_Query, Subst_Child, Norm_Query, Norm_Child),
	normalize_params(Subst_Params, Head_Params, [],
		Norm_Subst_Params, Norm_Head_Params, _).

% Meta-Interpreter Rule #3:
%	query(Lit) :-
%		node(_, [sub(Lit)|_]).

sld_rule(query_atom(Norm_Lit,Lit_Params),
		[node_atom(Query,[sub(Lit)|Rest],Params)]) :-
	node(Query, [sub(Lit)|Rest]),
	node_params(Query, [sub(Lit)|Rest], Params),
	atom_params(Lit, Lit_Params),
	normalize_atom(Lit, Norm_Lit).

% Meta-Interpreter Rule #4:
%	node(Query, Rest) :-
%		node(Query, [sub(Lit)|Rest]),
%		answer(Lit).

sld_rule(node_atom(Norm_Query,Norm_Rest,Norm_Head_Params),
		[node_atom(Query,[sub(Lit)|Rest],Norm_Node_Params),
		answer_atom(Answer,Norm_Answer_Params)]) :-
	node(Query, [sub(Lit)|Rest]),
	node_params(Query, [sub(Lit)|Rest], Params),
	maxno_node(Query, [sub(Lit)|Rest], Max_Var, Max_Par),
	answer(Answer),
	fresh_atom(Answer, Max_Var, Max_Par, Fresh_Answer),
	atom_params(Fresh_Answer, Answer_Params),
	unify(Lit, Fresh_Answer, Subst),
	apply_subst_atom(Query, Subst, Subst_Query),
	apply_subst_goal(Rest, Subst, Subst_Rest),
	apply_subst_args(Params, Subst, Subst_Params),
	apply_subst_args(Answer_Params, Subst, Subst_Answer_Params),
	node_params(Subst_Query, Subst_Rest, Head_Params),
	normalize_node(Subst_Query, Subst_Rest, Norm_Query, Norm_Rest),
	normalize_params(Subst_Params, Subst_Answer_Params, Head_Params,
		Norm_Node_Params, Norm_Answer_Params, Norm_Head_Params).

% Meta-Interpreter Rule #5:
%	node(Query, Rest) :-
%		node(Query, [edb(Lit)|Rest]),
%		db(Lit).

sld_rule(node_atom(Norm_Query,Norm_Rest,Norm_Head_Params),
		[node_atom(Query,[edb(Lit)|Rest],Norm_Node_Params),
		db_atom(Norm_Fact)]) :-
	node(Query, [edb(Lit)|Rest]),
	node_params(Query, [edb(Lit)|Rest], Params),
	maxno_node(Query, [edb(Lit)|Rest], _, Max_Par),
	functor(Lit, Pred, Arity),
	fresh_fact(Pred, Arity, Max_Par, Fact),
	unify(Fact, Lit, Subst),
	apply_subst_atom(Query, Subst, Subst_Query),
	apply_subst_goal(Rest, Subst, Subst_Rest),
	apply_subst_args(Params, Subst, Subst_Params),
	apply_subst_atom(Fact, Subst, Subst_Fact),
	node_params(Subst_Query, Subst_Rest, Head_Params),
	normalize_node(Subst_Query, Subst_Rest, Norm_Query, Norm_Rest),
	Subst_Fact =.. [Pred|Fact_Params],
	normalize_params(Subst_Params, Fact_Params, Head_Params,
		Norm_Node_Params, Norm_Fact_Params, Norm_Head_Params),
	Norm_Fact =.. [Pred|Norm_Fact_Params].

% Meta-Interpreter Rule #6:
%	answer(Query) :-
%		node(Query, []).

sld_rule(Answer_Atom, [node_atom(Answer,[],Params)]) :-
	node(Answer, []),
	atom_params(Answer, Params),
	answer_atom(Answer, Answer_Atom).

%------------------------------------------------------------------------------
% answer_args(+Args, +Par_No, -Params, +Real_Args):
%------------------------------------------------------------------------------

answer_atom(Answer, answer_atom(Answer,Params)) :-
	use_option(derive_idb_literals, no),
	atom_params(Answer, Params).

answer_atom(Answer, answer_atom(IDB_Lit, Args)) :-
	use_option(derive_idb_literals, yes),
	functor(Answer, Pred, Arity),
	fresh_fact(Pred, Arity, 0, IDB_Lit),
	Answer =.. [Pred|Args].

%==============================================================================
% Fresh Variables:
%==============================================================================

% These predicates renumber variables and parameters in such a way that
% the used numbers are disjoint from the numbers in a given node.
% This is important before unification.

%------------------------------------------------------------------------------
% fresh_rule(+Var_Offset, +Par_Offset, -Head, -Body):
%------------------------------------------------------------------------------

fresh_rule(Var_Offset, Par_Offset, Head, Body) :-
	rule(Stored_Head, Stored_Body),
	fresh_atom(Stored_Head, Var_Offset, Par_Offset, Head),
	fresh_body(Stored_Body, Var_Offset, Par_Offset, Body).

%------------------------------------------------------------------------------
% fresh_body(+Body, +Var_Offset, +Par_Offset, -Fresh_Body):
%------------------------------------------------------------------------------

fresh_body([], _, _, []).

fresh_body([Lit|Body], Var_Offset, Par_Offset, [Fresh_Lit|Fresh_Body]) :-
	fresh_lit(Lit, Var_Offset, Par_Offset, Fresh_Lit),
	fresh_body(Body, Var_Offset, Par_Offset, Fresh_Body).

%------------------------------------------------------------------------------
% fresh_lit(+Lit, +Var_Offset, +Par_Offset, -Fresh_Lit):
%------------------------------------------------------------------------------

fresh_lit(edb(Atom), Var_Offset, Par_Offset, edb(Fresh_Atom)) :-
	fresh_atom(Atom, Var_Offset, Par_Offset, Fresh_Atom).

fresh_lit(cont(Atom), Var_Offset, Par_Offset, cont(Fresh_Atom)) :-
	fresh_atom(Atom, Var_Offset, Par_Offset, Fresh_Atom).

fresh_lit(sub(Atom), Var_Offset, Par_Offset, sub(Fresh_Atom)) :-
	fresh_atom(Atom, Var_Offset, Par_Offset, Fresh_Atom).

%------------------------------------------------------------------------------
% fresh_atom(+Atom, +Var_Offset, +Par_Offset, -Fresh_Atom):
%------------------------------------------------------------------------------

fresh_atom(Atom, Var_Offset, Par_Offset, Fresh_Atom) :-
	Atom =.. [Pred|Args],
	fresh_args(Args, Var_Offset, Par_Offset, Fresh_Args),
	Fresh_Atom =.. [Pred|Fresh_Args].

%------------------------------------------------------------------------------
% fresh_args(+Args, +Var_Offset, +Par_Offset, -Fresh_Args):
%------------------------------------------------------------------------------

fresh_args([], _, _, []).

fresh_args([Arg|More_Args], Var_Offset, Par_Offset,
		[Fresh_Arg|More_Fresh_Args]) :-
	fresh_arg(Arg, Var_Offset, Par_Offset, Fresh_Arg),
	fresh_args(More_Args, Var_Offset, Par_Offset, More_Fresh_Args).

%------------------------------------------------------------------------------
% fresh_arg(+Arg, +Var_Offset, +Par_Offset, -Fresh_Arg):
%------------------------------------------------------------------------------

fresh_arg(var(No), Var_Offset, _, var(Fresh_No)) :-
	!,
	Fresh_No is No + Var_Offset.

fresh_arg(par(No), _, Par_Offset, par(Fresh_No)) :-
	!,
	Fresh_No is No + Par_Offset.

fresh_arg(Const, _, _, Const) :-
	atomic(Const).

%------------------------------------------------------------------------------
% fresh_fact(+Pred, +Arity, +Par_Offset, -Fact):
%------------------------------------------------------------------------------

fresh_fact(Pred, Arity, Par_Offset, Fact) :-
	fresh_par_list(Arity, Par_Offset, Args),
	Fact =.. [Pred|Args].

%------------------------------------------------------------------------------
% fresh_par_list(+Arity, +Par_No, -Par_List):
%------------------------------------------------------------------------------

fresh_par_list(0, _, []).

fresh_par_list(Arity, Par_No, [par(Par_No)|Par_List]) :-
	Arity > 0,
	Next_Arity is Arity - 1,
	Next_Par_No is Par_No + 1,
	fresh_par_list(Next_Arity, Next_Par_No, Par_List).

%==============================================================================
% Maximal Used Parameter/Variable Numbers:
%==============================================================================

% These predicates return the first free variable and parameter numbers,
% i.e. one above the highest used numbers.

%------------------------------------------------------------------------------
% maxno_node(+Query, +Goal, -Max_Var, -Max_Par):
%------------------------------------------------------------------------------

maxno_node(Query, Goal, Max_Var, Max_Par) :-
	maxno_atom(Query, Query_Var, Query_Par),
	maxno_goal(Goal, Goal_Var, Goal_Par),
	int_max(Query_Var, Goal_Var, Max_Var),
	int_max(Query_Par, Goal_Par, Max_Par).

%------------------------------------------------------------------------------
% maxno_goal(+Goal, -Max_Var, -Max_Par):
%------------------------------------------------------------------------------

maxno_goal([], 0, 0).

maxno_goal([Lit|Goal], Max_Var, Max_Par) :-
	maxno_lit(Lit, Lit_Var, Lit_Par),
	maxno_goal(Goal, Goal_Var, Goal_Par),
	int_max(Lit_Var, Goal_Var, Max_Var),
	int_max(Lit_Par, Goal_Par, Max_Par).

%------------------------------------------------------------------------------
% maxno_lit(+Lit, -Max_Var, -Max_Par):
%------------------------------------------------------------------------------

maxno_lit(edb(Atom), Max_Var, Max_Par) :-
	maxno_atom(Atom, Max_Var, Max_Par).

maxno_lit(cont(Atom), Max_Var, Max_Par) :-
	maxno_atom(Atom, Max_Var, Max_Par).

maxno_lit(sub(Atom), Max_Var, Max_Par) :-
	maxno_atom(Atom, Max_Var, Max_Par).

%------------------------------------------------------------------------------
% maxno_atom(+Atom, -Max_Var, -Max_Par):
%------------------------------------------------------------------------------

maxno_atom(Atom, Max_Var, Max_Par) :-
	Atom =.. [_|Args],
	maxno_args(Args, Max_Var, Max_Par).

%------------------------------------------------------------------------------
% maxno_args(+Args, -Max_Var, -Max_Par):
%------------------------------------------------------------------------------

maxno_args([], 0, 0).

maxno_args([Arg|Args], Max_Var, Max_Par) :-
	maxno_arg(Arg, Arg_Var, Arg_Par),
	maxno_args(Args, Args_Var, Args_Par),
	int_max(Arg_Var, Args_Var, Max_Var),
	int_max(Arg_Par, Args_Par, Max_Par).

%------------------------------------------------------------------------------
% maxno_arg(+Arg, -Max_Var, -Max_Par):
%------------------------------------------------------------------------------

maxno_arg(Const, 0, 0) :-
	atomic(Const).

maxno_arg(var(Var_No), Next_Var_No, 0) :-
	Next_Var_No is Var_No + 1.

maxno_arg(par(Par_No), 0, Next_Par_No) :-
	Next_Par_No is Par_No + 1.

%==============================================================================
% Unification:
%==============================================================================

%------------------------------------------------------------------------------
% unify(+Atom1, +Atom2, -Subst):
%------------------------------------------------------------------------------

unify(Atom1, Atom2, Subst) :-
	Atom1 =.. [Pred|Args1],
	Atom2 =.. [Pred|Args2],
	unify_args(Args1, Args2, [], Subst).

%------------------------------------------------------------------------------
% unify_args(+Args1, +Args2, +Subst_In, -Subst_Out):
%------------------------------------------------------------------------------

unify_args([], [], Subst, Subst).

unify_args([Arg1|Args1], [Arg2|Args2], Subst_In, Subst_Out) :-
	unify_arg(Arg1, Arg2, Subst_In, Subst),
	unify_args(Args1, Args2, Subst, Subst_Out).

%------------------------------------------------------------------------------
% unify_arg(+Arg1, +Arg2, +Subst_In, -Subst_Out):
%------------------------------------------------------------------------------

unify_arg(Arg1, Arg2, Subst_In, Subst_Out) :-
	map_arg(Subst_In, Arg1, Real_Arg1),
	map_arg(Subst_In, Arg2, Real_Arg2),
	unify_step(Real_Arg1, Real_Arg2, Subst_In, Subst_Out).

%------------------------------------------------------------------------------
% unify_step(+Arg1, +Arg2, +Subst_In, -Subst_Out):
%------------------------------------------------------------------------------

% It is forbidden to replace a parameter by a variable,
% but this is the only constraint,
% otherwise both var(X) and par(X) act like variables.
% Note that there also might be constants, but no structured terms.

unify_step(Arg, Arg, Subst, Subst) :-
	!.

unify_step(var(X), Val, Subst_In, Subst_Out) :-
	!,
	subst_add(Subst_In, var(X), Val, Subst_Out).

unify_step(Val, var(X), Subst_In, Subst_Out) :-
	!,
	subst_add(Subst_In, var(X), Val, Subst_Out).

unify_step(par(X), Val, Subst_In, Subst_Out) :-
	!,
	subst_add(Subst_In, par(X), Val, Subst_Out).

unify_step(Val, par(X), Subst_In, Subst_Out) :-
	subst_add(Subst_In, par(X), Val, Subst_Out).

%==============================================================================
% Substitutions:
%==============================================================================

% This is an abstract datatype, substitutions should only be constructed and
% applied via these predicates (the implementation may change).
% The current implementation is a list of pairs "map(Var,Val)".
% We do not really compose substitutions, but simply add the replacement at the
% end of the list. This means that we must process the whole list when applying
% a substitution (instead of stopping at the first match).

%------------------------------------------------------------------------------
% empty_subst(-Empty):
%------------------------------------------------------------------------------

empty_subst([]).

%------------------------------------------------------------------------------
% subst_add(+Subst_In, +Var, +Val, -Subst_Out):
%------------------------------------------------------------------------------

subst_add([], Var, Val, [map(Var,Val)]).

subst_add([Map|Subst_In], Var, Val, [Map|Subst_Out]) :-
	subst_add(Subst_In, Var, Val, Subst_Out).

%------------------------------------------------------------------------------
% apply_subst_goal(+Goal_In, +Subst, -Goal_Out):
%------------------------------------------------------------------------------

apply_subst_goal([], _, []).

apply_subst_goal([Lit_In|Goal_In], Subst, [Lit_Out|Goal_Out]) :-
	apply_subst_lit(Lit_In, Subst, Lit_Out),
	apply_subst_goal(Goal_In, Subst, Goal_Out).
	
%------------------------------------------------------------------------------
% apply_subst_lit(+Lit_In, +Subst, -Lit_Out):
%------------------------------------------------------------------------------

apply_subst_lit(edb(Atom_In), Subst, edb(Atom_Out)) :-
	apply_subst_atom(Atom_In, Subst, Atom_Out).

apply_subst_lit(cont(Atom_In), Subst, cont(Atom_Out)) :-
	apply_subst_atom(Atom_In, Subst, Atom_Out).

apply_subst_lit(sub(Atom_In), Subst, sub(Atom_Out)) :-
	apply_subst_atom(Atom_In, Subst, Atom_Out).

%------------------------------------------------------------------------------
% apply_subst_atom(+Atom_In, +Subst, -Atom_Out):
%------------------------------------------------------------------------------

apply_subst_atom(Atom_In, Subst, Atom_Out) :-
	Atom_In =.. [Pred|Args_In],
	apply_subst_args(Args_In, Subst, Args_Out),
	Atom_Out =.. [Pred|Args_Out].

%------------------------------------------------------------------------------
% apply_subst_args(+Args_In, +Subst, -Args_Out):
%------------------------------------------------------------------------------

apply_subst_args([], _, []).

apply_subst_args([Arg_In|Args_In], Subst, [Arg_Out|Args_Out]) :-
	map_arg(Subst, Arg_In, Arg_Out),
	apply_subst_args(Args_In, Subst, Args_Out).

%------------------------------------------------------------------------------
% map_arg(+Subst, +Arg_In, -Arg_Out):
%------------------------------------------------------------------------------

map_arg([], Arg, Arg).

map_arg([map(Arg_In,Arg)|Subst], Arg_In, Arg_Out) :-
	!,
	map_arg(Subst, Arg, Arg_Out).

map_arg([_|Subst], Arg_In, Arg_Out) :-
	map_arg(Subst, Arg_In, Arg_Out).

%==============================================================================
% Normalization:
%==============================================================================

% The goal of this predicates is to number variables and parameters
% consecutively beginning with 0.
% This is important because otherwise we would not detect that a node type
% is equal to one we already have.

%------------------------------------------------------------------------------
% normalize_node(+Query, +Goal, -Norm_Query, -Norm_Goal):
%------------------------------------------------------------------------------

normalize_node(Query, Goal, Norm_Query, Norm_Goal) :-
	norm_atom(Query, state(0,0,[]), Norm, Norm_Query),
	norm_goal(Goal, Norm, _, Norm_Goal).

%------------------------------------------------------------------------------
% normalize_atom(+Atom, -Norm_Atom):
%------------------------------------------------------------------------------

normalize_atom(Atom, Norm_Atom) :-
	norm_atom(Atom, state(0,0,[]), _, Norm_Atom).

%------------------------------------------------------------------------------
% normalize_params(+Par1, +Par2, +Par3, -Norm1, -Norm2, -Norm3):
%------------------------------------------------------------------------------

normalize_params(Par1, Par2, Par3, Norm1, Norm2, Norm3) :-
	norm_args(Par1, state(0,0,[]), State1, Norm1),
	norm_args(Par2, State1, State2, Norm2),
	norm_args(Par3, State2, _, Norm3).

%------------------------------------------------------------------------------
% norm_goal(+Goal, +State_In, -State_Out, -Norm_Goal):
%------------------------------------------------------------------------------

norm_goal([], State, State, []).

norm_goal([Lit|Goal], State_In, State_Out, [Norm_Lit|Norm_Goal]) :-
	norm_lit(Lit, State_In, State, Norm_Lit),
	norm_goal(Goal, State, State_Out, Norm_Goal).

%------------------------------------------------------------------------------
% norm_lit(+Lit, +State_In, -State_Out, -Norm_Lit):
%------------------------------------------------------------------------------

norm_lit(edb(Atom), State_In, State_Out, edb(Norm_Atom)) :-
	norm_atom(Atom, State_In, State_Out, Norm_Atom).

norm_lit(cont(Atom), State_In, State_Out, cont(Norm_Atom)) :-
	norm_atom(Atom, State_In, State_Out, Norm_Atom).

norm_lit(sub(Atom), State_In, State_Out, sub(Norm_Atom)) :-
	norm_atom(Atom, State_In, State_Out, Norm_Atom).

%------------------------------------------------------------------------------
% norm_atom(+Atom, +State_In, -State_Out, -Norm_Atom):
%------------------------------------------------------------------------------

norm_atom(Atom, State_In, State_Out, Norm_Atom) :-
	Atom =.. [Pred|Args],
	norm_args(Args, State_In, State_Out, Norm_Args),
	Norm_Atom =.. [Pred|Norm_Args].

%------------------------------------------------------------------------------
% norm_args(+Args, +State_in -State_Out, -Norm_Args):
%------------------------------------------------------------------------------

norm_args([], State, State, []).

norm_args([Arg|Args], State_In, State_Out, [Norm_Arg|Norm_Args]) :-
	norm_arg(Arg, State_In, State, Norm_Arg),
	norm_args(Args, State, State_Out, Norm_Args).

%------------------------------------------------------------------------------
% norm_arg(+Arg, +Subst_In, +Var_In, +Par_In, -Subst_Out, -Var_Out, -Par_Out):
%------------------------------------------------------------------------------

norm_arg(Const, State, State, Const) :-
	atomic(Const),
	!.

norm_arg(Arg_In, State, State, Arg_Out) :-
	state_maps(State, Arg_In, Arg_Out),
	!.

norm_arg(var(No_In),
		state(Var_No,Par_No,Norm_Map),
		state(Next_Var_No,Par_No,
			[norm_map(var(No_In),var(Var_No))|Norm_Map]),
		var(Var_No)) :-
	Next_Var_No is Var_No + 1.

norm_arg(par(No_In),
		state(Var_No,Par_No,Norm_Map),
		state(Var_No,Next_Par_No,
			[norm_map(par(No_In),par(Par_No))|Norm_Map]),
		par(Par_No)) :-
	Next_Par_No is Par_No + 1.

%------------------------------------------------------------------------------
% state_maps(+State, +Arg_In, -Arg_Out):
%------------------------------------------------------------------------------

state_maps(state(_,_,Map), Arg_In, Arg_Out) :-
	state_search(Map, Arg_In, Arg_Out).

%------------------------------------------------------------------------------
% state_search(+Map, +Arg_In, -Arg_Out):
%------------------------------------------------------------------------------

state_search([norm_map(Arg_In,Arg_Out)|_], Arg_In, Arg_Out).

state_search([_|Rest], Arg_In, Arg_Out) :-
	state_search(Rest, Arg_In, Arg_Out).

%==============================================================================
% Select Parameters:
%==============================================================================

% These predicates produce a list of parameters occurring in a node type
% (in order of occurrence, but no parameter twice).

%------------------------------------------------------------------------------
% node_params(+Query, +Goal, -Params):
%------------------------------------------------------------------------------

node_params(Query, Goal, Params) :-
	params_atom(Query, [], Query_Params),
	params_goal(Goal, Query_Params, Params).

%------------------------------------------------------------------------------
% atom_params(+Atom, -Params):
%------------------------------------------------------------------------------

atom_params(Atom, Params) :-
	params_atom(Atom, [], Params).

%------------------------------------------------------------------------------
% params_goal(+Goal, +Params_In, -Params_Out):
%------------------------------------------------------------------------------

params_goal([], Params, Params).

params_goal([Lit|Goal], Params_In, Params_Out) :-
	params_lit(Lit, Params_In, Params),
	params_goal(Goal, Params, Params_Out).

%------------------------------------------------------------------------------
% params_lit(+Lit, -Params_In, +Params_Out):
%------------------------------------------------------------------------------

params_lit(edb(Atom), Params_In, Params_Out) :-
	params_atom(Atom, Params_In, Params_Out).

params_lit(cont(Atom), Params_In, Params_Out) :-
	params_atom(Atom, Params_In, Params_Out).

params_lit(sub(Atom), Params_In, Params_Out) :-
	params_atom(Atom, Params_In, Params_Out).

%------------------------------------------------------------------------------
% params_atom(+Atom, -Params_In, +Params_Out):
%------------------------------------------------------------------------------

params_atom(Atom, Params_In, Params_Out) :-
	Atom =.. [_|Args],
	params_args(Args, Params_In, Params_Out).

%------------------------------------------------------------------------------
% params_args(+Args, -Params_In, +Params_Out):
%------------------------------------------------------------------------------

params_args([], Params, Params).

params_args([Arg|Args], Params_In, Params_Out) :-
	params_arg(Arg, Params_In, Params),
	params_args(Args, Params, Params_Out).

%------------------------------------------------------------------------------
% param_arg(+Arg, -Params_In, +Params_Out):
%------------------------------------------------------------------------------

params_arg(par(No), Params, Params) :-
	list_member(par(No), Params),
	!.

params_arg(par(No), Params_In, Params_Out) :-
	!,
	list_append(Params_In, [par(No)], Params_Out).

params_arg(var(_), Params, Params) :-
	!.

params_arg(Const, Params, Params) :-
	atomic(Const).

%==============================================================================
% Unparser:
%==============================================================================

%------------------------------------------------------------------------------
% print_all_queries:
%------------------------------------------------------------------------------

print_all_queries :-
	write('Query Types:'),
	nl,
	query(Query),
	write('	'),	% This is a TAB
	print_atom(Query),
	nl,
	fail.

print_all_queries.

%------------------------------------------------------------------------------
% print_all_nodes:
%------------------------------------------------------------------------------

print_all_nodes :-
	write('Node Types:'),
	nl,
	node(Query, Goal),
	write('	'),	% This is a TAB
	print_node(Query, Goal),
	nl,
	fail.

print_all_nodes.

%------------------------------------------------------------------------------
% print_all_answers:
%------------------------------------------------------------------------------

print_all_answers :-
	write('Answer Types:'),
	nl,
	answer(Atom),
	write('	'),	% This is a TAB
	print_atom(Atom),
	nl,
	fail.

print_all_answers.

%------------------------------------------------------------------------------
% print_all_sld_rules:
%------------------------------------------------------------------------------

print_all_sld_rules :-
	nl,
	write('Rewritten Rules:'),
	nl,
	write('==============='),
	nl,
	nl,
	sld(Head, Body),
	print_sld_rule(Head, Body),
	nl,
	fail.

print_all_sld_rules.

%------------------------------------------------------------------------------
% print_node(+Query, +Goal):
%------------------------------------------------------------------------------

print_node(Query, Goal) :-
	print_atom(Query),
	write(':'),
	print_goal(Goal).

%------------------------------------------------------------------------------
% print_goal(+Goal):
%------------------------------------------------------------------------------

print_goal([]) :-
	write('true').

print_goal([Lit|Goal]) :-
	print_lit(Lit),
	print_goal_rest(Goal).

%------------------------------------------------------------------------------
% print_goal_rest(+Goal):
%------------------------------------------------------------------------------

print_goal_rest([]).

print_goal_rest([Lit|Goal]) :-
	write(','),
	print_lit(Lit),
	print_goal_rest(Goal).

%------------------------------------------------------------------------------
% print_lit(+Lit):
%------------------------------------------------------------------------------

print_lit(edb(Atom)) :-
	print_atom(Atom).

print_lit(cont(Atom)) :-
	print_atom(Atom).

print_lit(sub(Atom)) :-
	write('&'),
	print_atom(Atom).

%------------------------------------------------------------------------------
% print_atom(+Atom):
%------------------------------------------------------------------------------

print_atom(Atom) :-
	Atom =.. [Pred|Args],
	write(Pred),
	print_args(Args).

%------------------------------------------------------------------------------
% print_args(+Args):
%------------------------------------------------------------------------------

print_args([]).

print_args([Arg|Args]) :-
	write('('),
	print_arg(Arg),
	print_args_rest(Args),
	write(')').

%------------------------------------------------------------------------------
% print_args_rest(+Args):
%------------------------------------------------------------------------------

print_args_rest([]).

print_args_rest([Arg|Args]) :-
	write(','),
	print_arg(Arg),
	print_args_rest(Args).

%------------------------------------------------------------------------------
% print_arg(+Arg):
%------------------------------------------------------------------------------

print_arg(var(Var_No)) :-
	!,
	write('X'),
	write(Var_No).

print_arg(par(Par_No)) :-
	!,
	write('C'),
	write(Par_No).

print_arg(Const) :-
	atomic(Const),
	write(Const).

%------------------------------------------------------------------------------
% print_sld_rule(+SLD_Head, +SLD_Body):
%------------------------------------------------------------------------------

print_sld_rule(Head, Body) :-
	print_sld_lit(Head),
	print_sld_body(Body),
	write('.'),
	nl.

%------------------------------------------------------------------------------
% print_sld_body(+SLD_Body):
%------------------------------------------------------------------------------

print_sld_body([]).

print_sld_body([Lit|Rest]) :-
	write(' :- '),
	nl,
	write('	'), % This is a TAB
	print_sld_lit(Lit),
	print_sld_body_rest(Rest).

%------------------------------------------------------------------------------
% print_sld_body_rest(+SLD_Body):
%------------------------------------------------------------------------------

print_sld_body_rest([]).

print_sld_body_rest([Lit|Rest]) :-
	write(', '),
	nl,
	write('	'), % This is a TAB
	print_sld_lit(Lit),
	print_sld_body_rest(Rest).

%------------------------------------------------------------------------------
% print_sld_lit(+SLD_Lit):
%------------------------------------------------------------------------------

print_sld_lit(node_atom(Query,Goal,Params)) :-
	write_quote,
	write('node{'),
	print_node(Query, Goal),
	write('}'),
	write_quote,
	print_args(Params).

print_sld_lit(query_atom(Query,Params)) :-
	write_quote,
	write('query{'),
	print_atom(Query),
	write('}'),
	write_quote,
	print_args(Params).

print_sld_lit(answer_atom(Answer,Params)) :-
	(use_option(derive_idb_literals, no) ->
		write_quote,
		write('answer{'),
		print_atom(Answer),
		write('}'),
		write_quote,
		print_args(Params)
	;
		Answer =.. [Pred|_],
		write(Pred),
		print_args(Params)).

print_sld_lit(db_atom(Atom)) :-
	print_atom(Atom).

%==============================================================================
% Portability Definitions:
%==============================================================================

%------------------------------------------------------------------------------
% list_append(+List1, +List2, -List12):
%------------------------------------------------------------------------------

list_append([], List, List).

list_append([Head|Tail], List, [Head|Tail_List]) :-
	list_append(Tail, List, Tail_List).

%------------------------------------------------------------------------------
% list_member(+Elem, +List):
%------------------------------------------------------------------------------

list_member(Elem, [Elem|_]) :-
	!.

list_member(Elem, [_|Rest]) :-
	list_member(Elem, Rest).

%------------------------------------------------------------------------------
% int_max(+N +M, -Max):
%------------------------------------------------------------------------------

int_max(N, M, N) :-
	N >= M,
	!.

int_max(N, M, M) :-
	N < M.

