<?xml version="1.0"?>
<document>
  <TITLE>
    Visur/RAR Manual
  </TITLE>


<body>
  <section name='Managing the Source Hierarchy'>
    <subsection name='Declaring the Source Hierarchy'>
      <predicate name='rar:source_hierarchy_set(+Tree)'>
        <explanation>
          Sets a user-defined hierarchy. This hierarchy contains
          the path to each file, that should be concernd.
        </explanation>
        <legend name='Parameter'>
          <parameter name='Tree'/>
        </legend>
        <legend name='Meaning'>
          <explanation>
            The hierarchy in XML field notation.
          </explanation>
        </legend>
        <example name='Example'>
          <pre>
  ?- rar:source_hierarchy_set([
        unit:[name:rar]:[
           module:[name:test]:[
              file:[
                 name:'sources/rar/test/cross',
                 path:'sources/rar/test/cross']:[],
              file:[
                 name:'sources/rar/test/desc',
                 path:'sources/rar/test/desc']:[],
              file:[
                 name:'sources/rar/test/necess',
                 path:'sources/rar/test/necess']:[] ] ] ]).

  Yes
          </pre>
        </example>
      </predicate>

      <predicate name='rar:source_hierarchy_get(-Tree)'>
        <explanation>
           Gets the hierarchy for the filenames and paths.
        </explanation>
        <legend name='Parameter'>
          <parameter name='Tree'/>
        </legend>
        <legend name='Meaning'>
          <explanation>
            Returns the hierarchy.
          </explanation>
        </legend>
      </predicate>
    </subsection>

    <subsection name='Computing the Source Hierarchy of DisLog'>
      <predicate name='dislog_to_source_hierarchy(-Tree)'>
        <explanation>
          Converts the filenames and paths of the files written in
          the file &quot;dislog&quot; in a hierarchy using
          the xml field-notation.
          In this hierarchy, there stands the units,
          modules and files of the system <em>DisLog</em>.
        </explanation>
        <legend name='Parameter'>
          <parameter name='Tree'/>
        </legend>
        <legend name='Meaning'>
          <explanation>
            Returns the DisLog hierachy.
          </explanation>
        </legend>
        <example name='Example'>
          <pre>
  ?- dislog_to_source_hierarchy(Tree).

  Tree = [
     system:[name:dislog_system]:[
        unit:[name:basic_algebra]:[
           module:[name:basics]:[
              file:[
                 name:operators,
                 path: 'sources/basic_algebra/basics']:[],
              file:[
                 name:substitutions,
                 path: 'sources/basic_algebra/basics']:[],
              ... ],
           module:[name:graphs]:[...] ],
        unit:[name:rar]:[
           module:[name:test]:[
              file:[
                 name:cross,
                 path: 'sources/rar/test/cross']:[],
              file:[
                 name:desc,
                 path: 'sources/rar/test/desc']:[],
              file:[
                 name:necess,
                 path: 'sources/rar/test/necess']:[] ] ]

  Yes
          </pre>
        </example>
      </predicate>
    </subsection>

    <subsection name='Parsing the Source Hierarchy'>
      <predicate name='rar_parser:source_hierarchy_to_rar_database(+File_List)'>
        <explanation>
          Parses the files in File_List into the
          internal database; all queries will only be made on the
          parsed files.
        </explanation>
        <legend name='Parameter'>
          <parameter name='File_List'/>
        </legend>
        <legend name='Meaning'>
          <explanation>
            File_List is a list with prolog-source-code files
            with full paths.
          </explanation>
        </legend>
        <example name='Example'>
          <pre>
  ?- rar_parser:source_hierarchy_to_rar_database([module:test]).

  Yes
          </pre>
        </example>
      </predicate>
    </subsection>
    
    <subsection name='Saving and Loading the System Status'>
      <predicate name='rar:save_system_status(+File_Name)'>
        <explanation>
          Saves the internal database and
          the hierarchy tree in order to
          load it faster next time.
        </explanation>
        <legend name='Parameter'>
          <parameter name='File_Name'/>
        </legend>
        <legend name='Meaning'>
          <explanation>
            File_Name is the Name of a file.
          </explanation>
        </legend>
        <example name='Example'>
          <pre>
  rar:save_system_status(rar_dislog_system).

  Saving...
  All saved.

  Yes
          </pre>
        </example>
      </predicate>

      <predicate name='rar:load_system_status(+File_Name)'>
        <explanation>
          Loads the internal database and the hierarchy 
          tree in order to
          load it faster next time.
        </explanation>
        <legend name='Parameter'>
          <parameter name='File_Name'/>
        </legend>
        <legend name='Meaning'>
          <explanation>
            File_Name is the Name of a file.
          </explanation>
        </legend>
        <example name='Example'>
          <pre>
  rar:load_system_status(rar_dislog_system).

  Loading ...
  % rar_dislog_system compiled into rar 1.49 sec, 7,319,616 bytes
  Loading completed.

  Yes

  ?- rar:dislog_sources_to_rar_database.

  Yes
  ?- rar:save_system_status.

  Yes
  ?- rar:load_system_status.

  Yes
          </pre>
        </example>
      </predicate>
    </subsection>
  </section>
  <section name='Querying the Internal Database'>
    <subsection name='The Predicate contains/2'>
      <predicate name='contains(?Type1:X1, ?Type2:X2).'>
        <explanation>
          the hierarchy Type1 with name X1
          contains the hierarchy Type2 with name X2
        </explanation>
        <legend name='Parameter'>
          <parameter name='Type1:X1, Type2:X2'/>
        </legend>
        <legend name='Meaning'>
          <explanation>
            Type_ is a hierarchy level;
            e.g., in DisLog, it could be
            system, unit, module or
            file.
            X_ is the name of the hierarchy level.
          </explanation>
        </legend>
        <example name='Example'>
          <pre>
  ?- rar:contains(module:test, file:File).

  File = 'sources/rar/test/cross';
  File = 'sources/rar/test/desc';
  File = 'sources/rar/test/necess';
  ...

  Yes
  ?- rar:contains(
        file:'sources/rar/test/cross',
        predicate:Predicate).

  Predicate = (user:in_same_module)/2;
  Predicate = (user:rar_predicate_to_cross_calls)/2

  Yes
  ?- rar:contains(
        module:Module,
        predicate:(user:rar_predicate_to_cross_calls)/2).

  Module = test

  Yes

          </pre>
        </example>
      </predicate>
    </subsection>

    <subsection name='The Predicate calls/2'>
      <predicate name='calls(?Type1:X1, ?Type2:X2)'>
        <explanation>
          Gets the predicate (M2:P2)/A2 or the hierarchy
          H_Level2:Name2 which is called from
          the hierarchy H_Level1 with name Name1 or the
          predicate (M1:P1)/A1.
        </explanation>
        <legend name='Parameter'>
          <parameter name='(M1:P1)/A1'/>
          <parameter name='H_Level'/>
        </legend>
        <legend name='Meaning'>
          <explanation>
            (M1:P1)/A1 is the predicate, from which you want the
            successors (you get all by using backtracking);
          </explanation>
          <explanation>
            H_Level is the successor's hierarchy level
           (you get all by using backtracking);
          </explanation>
        </legend>
        <example name='Example'>
          <pre>
  ?- calls(
        predicate:(user:rar_predicate_to_cross_calls)/2,
        predicate:Predicate).

  Predicate = (user:rar_predicate_to_descendants)/2 ;
  Predicate = (meta:findall)/3 ;
  Predicate = (user:member)/2 ;
  Predicate = (user: (\+))/1

  Yes
  ?- calls(
        predicate:(user:rar_predicate_to_cross_calls)/2,
        file:File).

  File = 'sources/rar/test/desc'

  Yes
          </pre>
        </example>
      </predicate>
    </subsection>
  </section>
  <section name='Auxilliary Predicates'>
    <subsection>
      <predicate name='rar:get_full_file_name(+File_Name, -File_with_Path)'>
        <explanation>
          Looks in the hierarchy for the complete
          Filename (including the path).
        </explanation>
        <legend name='Parameter'>
          <parameter name='File_with_Path'/>
        </legend>
        <legend name='Meaning'>
          <explanation>
            File_with_Path is the complete name with the
            path of the file.
          </explanation>
        </legend>
        <example name='Example'>
          <pre>
  ?- get_full_file_name(desc, File_with_Path).

  File_with_Path = 'sources/rar/test/desc'

  Yes
  ?- get_full_file_names(desc, Files_with_Path).

  Files_with_Path = ['sources/rar/test/desc']

  Yes
          </pre>
        </example>
      </predicate>
    </subsection>
  </section>

  <section name='The Main Predicates of RAR'>
    <subsection name='Basic Predicates'>
      <subsubsection name='calls'>
        <pre>
           rar:calls(?predicate:P, ?rule:R)
        </pre>
        <explanation>
          the predicate P is in the head of the rule R
        </explanation>
        <pre>
          rar:calls(?rule:R, ?predicate:P)
        </pre>
        <explanation>
          the predicate P is in the body of the rule R
        </explanation>
      </subsubsection>
      <subsubsection name='contains'>
        <pre>
          rar:contains(?file:F, ?rule:R)
        </pre>
        <explanation>
          the file F contains the rule R
        </explanation>
        <pre>
          rar:contains(?T1:X1, ?T2:X2)
        </pre>
        <explanation>
          the hierarchy element T1:X1 contains
          the hierarchy element T2:X2
        </explanation>
      </subsubsection>
    </subsection>
    <subsection name='Derived Predicates'>
      <subsubsection name='calls'>
        <pre>
          rar:calls(predicate:P1, predicate:P2) :-
             rar:calls(predicate:P1, rule:R),
             rar:calls(rule:R, predicate:P2).
        </pre>
        <pre>
          rar:calls(T1:X1, predicate:P2) :-
             rar:contains(T1:X1, predicate:P1),
             rar:calls(predicate:P1, predicate:P2).
        </pre>
        <pre>
          rar:calls(T1:X1, T2:X2) :-
             rar:calls(T1:X1, predicate:P2),
             rar:contains(T2:X2, predicate:P2).
        </pre>
      </subsubsection>
      <subsubsection name='defines'>
        <pre>
          rar:defines(?file:F, ?predicate:P) :-
             rar:contains(?file:F, ?rule:R),
             rar:calls(?rule:R, ?predicate:P).
        </pre>
      </subsubsection>
      <subsubsection name='contains'>
        <pre>
          rar:contains(T1:X1, predicate:P1) :-
             rar:contains(T1:X1, file:F1),
             rar:defines(file:F1, predicate:P1).
        </pre>
      </subsubsection>
    </subsection>

    <subsection name='Queries'>
      <subsubsection name='a predicate (M:P)/A is defined in two different elements
        T:X1 and T:X2 of the same level T:'>
        <pre>
          rar:contains_double(predicate:(M:P)/A, T:X1, T:X2) :-
             rar:contains(T:X1, predicate:(M:P)/A),
             rar:contains(T:X2, predicate:(M:P)/A),
             X1 \= X2.
        </pre>
      </subsubsection>
      <subsubsection name='the predicate P1, which is defined by a rule R in T1:X1,
        calls the predicate P2, which is contained in T2:X2:'>
        <pre>
          rar:calls(T1:X1, T2:X2, predicate:P1, predicate:P2) :-
             rar:contains(T1:X1, rule:R),
             rar:calls(predicate:P1, rule:R),
             rar:calls(rule:R, predicate:P2),
             rar:contains(T2:X2, predicate:P2).
        </pre>
      </subsubsection>
    </subsection>
  </section>
</body>

</document>

