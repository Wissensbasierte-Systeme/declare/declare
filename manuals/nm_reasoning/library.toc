\chapentry {Introduction}{1}{1}
\chapentry {Herbrand States and Coins}{2}{3}
\chapentry {The Clause Tree -- A Data Structure}{3}{5}
\chapentry {Disjunctive Consequence Operators}{4}{7}
\chapentry {Program Transformations}{5}{13}
\chapentry {Model--Based Semantics}{6}{15}
\chapentry {Closed--World--Assumptions}{7}{21}
\chapentry {Well--Founded Semantics}{8}{23}
\chapentry {Input and Output of {\smallcaps DisLog}--Objects}{9}{27}
\unnumbchapentry {Predicate Index}{31}
