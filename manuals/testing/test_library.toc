\chapentry {Introduction}{1}{1}
\chapentry {Herbrand States}{2}{3}
\chapentry {The Clause Tree -- A Data Structure}{3}{5}
\chapentry {Disjunctive Consequence Operators}{4}{7}
\chapentry {Model--Based Semantics}{5}{9}
\chapentry {Closed--World--Assumptions}{6}{13}
\chapentry {Well--Founded Semantics}{7}{15}
\chapentry {Pretty--Printers for {\smallcaps DisLog}--Objects}{8}{17}
\unnumbchapentry {Predicate Index}{19}
