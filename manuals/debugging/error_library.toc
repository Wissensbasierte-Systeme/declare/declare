\chapentry {General Herbrand States}{1}{1}
\chapentry {Clause Trees -- A Data Structure}{2}{3}
\chapentry {Disjunctive Consequence Operators}{3}{5}
\chapentry {Minimal and Stable Models}{4}{7}
\chapentry {Closed--World--Assumptions}{5}{9}
\chapentry {Well--Founded Semantics}{6}{11}
\chapentry {Negation--As--Failure}{7}{13}
\chapentry {Pretty--Printers for {\smallcaps DisLog}--Objects}{8}{15}
\unnumbchapentry {Predicate Index}{17}
